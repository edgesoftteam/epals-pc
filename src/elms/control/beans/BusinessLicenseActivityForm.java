package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.app.bl.BusinessLicenseActivity;
import elms.app.common.MultiAddress;
import elms.util.StringUtils;

/**
 * @author Gayathri and ManjuPrasad Business License Activity Form
 */
public class BusinessLicenseActivityForm extends ActionForm {
	/*
	 * The Logger
	 */

	static Logger logger = Logger.getLogger(BusinessLicenseActivityForm.class.getName());

	protected BusinessLicenseActivity businessLicenseActivity;
	protected String activityId;
	protected String lsoId;
	
	protected boolean businessLocation;
	protected String applicationType;
	protected String outStandingFees;
	protected String addressStreetNumber;
	protected String addressStreetFraction;
	protected String addressStreetName;
	protected String addressUnitNumber;
	protected String addressCity;
	protected String addressState;
	protected String addressZip;
	protected String addressZip4;
	protected String outOfTownStreetNumber;
	protected String outOfTownStreetName;
	protected String outOfTownUnitNumber;
	protected String outOfTownCity;
	protected String outOfTownState;
	protected String outOfTownZip;
	protected String outOfTownZip4;
	protected String activityNumber;
	protected String activityStatus;
	protected String creationDate;
	protected String renewalDate;
	protected String renewalDt;
	protected String businessName;
	protected String businessAccountNumber;
	protected String corporateName;
	protected String businessPhone;
	protected String businessExtension;
	protected String businessFax;
	protected String activityType;
	protected String activityTypeDescription;
	protected String activitySubType;
	protected String muncipalCode;
	protected String sicCode;
	protected String descOfBusiness;
	protected String classCode;
	protected boolean homeOccupation;
	protected boolean decalCode;
	protected String applicationDate;
	protected String issueDate;
	protected String startingDate;
	protected String outOfBusinessDate;
	protected String ownershipType;
	protected String federalIdNumber;
	protected String emailAddress;
	protected String socialSecurityNumber;
	protected String mailStreetNumber;
	protected String mailStreetName;
	protected String mailUnitNumber;
	protected String mailCity;
	protected String mailState;
	protected String mailZip;
	protected String mailZip4;
	protected String prevStreetNumber;
	protected String prevStreetName;
	protected String prevUnitNumber;
	protected String prevCity;
	protected String prevState;
	protected String prevZip;
	protected String prevZip4;
	protected String insuranceExpDate;
	protected String bondExpDate;
	protected String deptOfJusticeExpDate;
	protected String federalFirearmsLiscExpDate;
	protected String squareFootage;
	protected String quantity;
	protected String quantityNum;
	protected String typeOfExemptions;
	protected String subProjectId;
	protected String displayContent;
	protected int createdBy;
	protected int updatedBy;
	protected MultiAddress[] multiAddress;
	/**
	 * @return Returns the addressCity.
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * @param addressCity
	 *            The addressCity to set.
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * @return Returns the addressState.
	 */
	public String getAddressState() {
		return addressState;
	}

	/**
	 * @param addressState
	 *            The addressState to set.
	 */
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	/**
	 * @return Returns the addressStreetName.
	 */
	public String getAddressStreetName() {
		return addressStreetName;
	}

	/**
	 * @param addressStreetName
	 *            The addressStreetName to set.
	 */
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}

	/**
	 * @return Returns the addressStreetNumber.
	 */
	public String getAddressStreetNumber() {
		return addressStreetNumber;
	}

	/**
	 * @param addressStreetNumber
	 *            The addressStreetNumber to set.
	 */
	public void setAddressStreetNumber(String addressStreetNumber) {
		this.addressStreetNumber = addressStreetNumber;
	}

	/**
	 * @return Returns the addressStreetFraction.
	 */
	public String getAddressStreetFraction() {
		return addressStreetFraction;
	}

	/**
	 * @param addressStreetFraction
	 *            The addressStreetFraction to set.
	 */
	public void setAddressStreetFraction(String addressStreetFraction) {
		this.addressStreetFraction = addressStreetFraction;
	}

	/**
	 * @return Returns the addressUnitNumber.
	 */
	public String getAddressUnitNumber() {
		return addressUnitNumber;
	}

	/**
	 * @param addressUnitNumber
	 *            The addressUnitNumber to set.
	 */
	public void setAddressUnitNumber(String addressUnitNumber) {
		this.addressUnitNumber = addressUnitNumber;
	}

	/**
	 * @return Returns the addressZip.
	 */
	public String getAddressZip() {
		return addressZip;
	}

	/**
	 * @param addressZip
	 *            The addressZip to set.
	 */
	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}

	/**
	 * @return Returns the addressZip4.
	 */
	public String getAddressZip4() {
		return addressZip4;
	}

	/**
	 * @param addressZip4
	 *            The addressZip4 to set.
	 */
	public void setAddressZip4(String addressZip4) {
		this.addressZip4 = addressZip4;
	}

	/**
	 * @return Returns the activityNumber.
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * @param activityNumber
	 *            The activityNumber to set.
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * @return Returns the activityStatus.
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            The activityStatus to set.
	 */
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return Returns the activitySubType.
	 */
	public String getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param activitySubType
	 *            The activitySubType to set.
	 */
	public void setActivitySubType(String activitySubType) {
		this.activitySubType = activitySubType;
	}

	/**
	 * @return Returns the activityType.
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @param activityType
	 *            The activityType to set.
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * @return Returns the activityTypeDescription.
	 */
	public String getActivityTypeDescription() {
		return activityTypeDescription;
	}

	/**
	 * @param activityTypeDescription
	 *            The activityTypeDescription to set.
	 */
	public void setActivityTypeDescription(String activityTypeDescription) {
		this.activityTypeDescription = activityTypeDescription;
	}

	/**
	 * @return Returns the applicationDate.
	 */
	public String getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate
	 *            The applicationDate to set.
	 */
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return Returns the applicationType.
	 */
	public String getApplicationType() {
		return applicationType;
	}

	/**
	 * @param applicationType
	 *            The applicationType to set.
	 */
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	/**
	 * @return Returns the bondExpDate.
	 */
	public String getBondExpDate() {
		return bondExpDate;
	}

	/**
	 * @param bondExpDate
	 *            The bondExpDate to set.
	 */
	public void setBondExpDate(String bondExpDate) {
		this.bondExpDate = bondExpDate;
	}

	/**
	 * @return Returns the businessAccountNumber.
	 */
	public String getBusinessAccountNumber() {
		return businessAccountNumber;
	}

	/**
	 * @param businessAccountNumber
	 *            The businessAccountNumber to set.
	 */
	public void setBusinessAccountNumber(String businessAccountNumber) {
		this.businessAccountNumber = businessAccountNumber;
	}

	/**
	 * @return Returns the businessExtension.
	 */
	public String getBusinessExtension() {
		return businessExtension;
	}

	/**
	 * @param businessExtension
	 *            The businessExtension to set.
	 */
	public void setBusinessExtension(String businessExtension) {
		this.businessExtension = businessExtension;
	}

	/**
	 * @return Returns the businessFax.
	 */
	public String getBusinessFax() {
		return businessFax;
	}

	/**
	 * @param businessFax
	 *            The businessFax to set.
	 */
	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax;
	}

	/**
	 * @return Returns the businessLocation.
	 */
	public boolean isBusinessLocation() {
		return businessLocation;
	}

	/**
	 * @param businessLocation
	 *            The businessLocation to set.
	 */
	public void setBusinessLocation(boolean businessLocation) {
		this.businessLocation = businessLocation;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the businessPhone.
	 */
	public String getBusinessPhone() {
		return businessPhone;
	}

	/**
	 * @param businessPhone
	 *            The businessPhone to set.
	 */
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	/**
	 * @return Returns the classCode.
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * @param classCode
	 *            The classCode to set.
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * @return Returns the corporateName.
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * @param corporateName
	 *            The corporateName to set.
	 */
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	/**
	 * @return Returns the creationDate.
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            The creationDate to set.
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return Returns the decalCode.
	 */
	public boolean isDecalCode() {
		return decalCode;
	}

	/**
	 * @param decalCode
	 *            The decalCode to set.
	 */
	public void setDecalCode(boolean decalCode) {
		this.decalCode = decalCode;
	}

	/**
	 * @return Returns the deptOfJusticeExpDate.
	 */
	public String getDeptOfJusticeExpDate() {
		return deptOfJusticeExpDate;
	}

	/**
	 * @param deptOfJusticeExpDate
	 *            The deptOfJusticeExpDate to set.
	 */
	public void setDeptOfJusticeExpDate(String deptOfJusticeExpDate) {
		this.deptOfJusticeExpDate = deptOfJusticeExpDate;
	}

	/**
	 * @return Returns the descOfBusiness.
	 */
	public String getDescOfBusiness() {
		return descOfBusiness;
	}

	/**
	 * @param descOfBusiness
	 *            The descOfBusiness to set.
	 */
	public void setDescOfBusiness(String descOfBusiness) {
		this.descOfBusiness = descOfBusiness;
	}

	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the federalFirearmsLiscExpDate.
	 */
	public String getFederalFirearmsLiscExpDate() {
		return federalFirearmsLiscExpDate;
	}

	/**
	 * @param federalFirearmsLiscExpDate
	 *            The federalFirearmsLiscExpDate to set.
	 */
	public void setFederalFirearmsLiscExpDate(String federalFirearmsLiscExpDate) {
		this.federalFirearmsLiscExpDate = federalFirearmsLiscExpDate;
	}

	/**
	 * @return Returns the federalIdNumber.
	 */
	public String getFederalIdNumber() {
		return federalIdNumber;
	}

	/**
	 * @param federalIdNumber
	 *            The federalIdNumber to set.
	 */
	public void setFederalIdNumber(String federalIdNumber) {
		this.federalIdNumber = federalIdNumber;
	}

	/**
	 * @return Returns the homeOccupation.
	 */
	public boolean isHomeOccupation() {
		return homeOccupation;
	}

	/**
	 * @param homeOccupation
	 *            The homeOccupation to set.
	 */
	public void setHomeOccupation(boolean homeOccupation) {
		this.homeOccupation = homeOccupation;
	}

	/**
	 * @return Returns the insuranceExpDate.
	 */
	public String getInsuranceExpDate() {
		return insuranceExpDate;
	}

	/**
	 * @param insuranceExpDate
	 *            The insuranceExpDate to set.
	 */
	public void setInsuranceExpDate(String insuranceExpDate) {
		this.insuranceExpDate = insuranceExpDate;
	}

	/**
	 * @return Returns the issueDate.
	 */
	public String getIssueDate() {
		return issueDate;
	}

	/**
	 * @param issueDate
	 *            The issueDate to set.
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * @return Returns the mailCity.
	 */
	public String getMailCity() {
		return mailCity;
	}

	/**
	 * @param mailCity
	 *            The mailCity to set.
	 */
	public void setMailCity(String mailCity) {
		this.mailCity = mailCity;
	}

	/**
	 * @return Returns the mailState.
	 */
	public String getMailState() {
		return mailState;
	}

	/**
	 * @param mailState
	 *            The mailState to set.
	 */
	public void setMailState(String mailState) {
		this.mailState = mailState;
	}

	/**
	 * @return Returns the mailStreetName.
	 */
	public String getMailStreetName() {
		return mailStreetName;
	}

	/**
	 * @param mailStreetName
	 *            The mailStreetName to set.
	 */
	public void setMailStreetName(String mailStreetName) {
		this.mailStreetName = mailStreetName;
	}

	/**
	 * @return Returns the mailStreetNumber.
	 */
	public String getMailStreetNumber() {
		return mailStreetNumber;
	}

	/**
	 * @param mailStreetNumber
	 *            The mailStreetNumber to set.
	 */
	public void setMailStreetNumber(String mailStreetNumber) {
		this.mailStreetNumber = mailStreetNumber;
	}

	/**
	 * @return Returns the mailUnitNumber.
	 */
	public String getMailUnitNumber() {
		return mailUnitNumber;
	}

	/**
	 * @param mailUnitNumber
	 *            The mailUnitNumber to set.
	 */
	public void setMailUnitNumber(String mailUnitNumber) {
		this.mailUnitNumber = mailUnitNumber;
	}

	/**
	 * @return Returns the mailZip.
	 */
	public String getMailZip() {
		return mailZip;
	}

	/**
	 * @param mailZip
	 *            The mailZip to set.
	 */
	public void setMailZip(String mailZip) {
		this.mailZip = mailZip;
	}

	/**
	 * @return Returns the mailZip4.
	 */
	public String getMailZip4() {
		return mailZip4;
	}

	/**
	 * @param mailZip4
	 *            The mailZip4 to set.
	 */
	public void setMailZip4(String mailZip4) {
		this.mailZip4 = mailZip4;
	}

	/**
	 * @return Returns the muncipalCode.
	 */
	public String getMuncipalCode() {
		return muncipalCode;
	}

	/**
	 * @param muncipalCode
	 *            The muncipalCode to set.
	 */
	public void setMuncipalCode(String muncipalCode) {
		this.muncipalCode = muncipalCode;
	}

	/**
	 * @return Returns the outOfBusinessDate.
	 */
	public String getOutOfBusinessDate() {
		return outOfBusinessDate;
	}

	/**
	 * @param outOfBusinessDate
	 *            The outOfBusinessDate to set.
	 */
	public void setOutOfBusinessDate(String outOfBusinessDate) {
		this.outOfBusinessDate = outOfBusinessDate;
	}

	/**
	 * @return Returns the outOfTownCity.
	 */
	public String getOutOfTownCity() {
		return outOfTownCity;
	}

	/**
	 * @param outOfTownCity
	 *            The outOfTownCity to set.
	 */
	public void setOutOfTownCity(String outOfTownCity) {
		this.outOfTownCity = outOfTownCity;
	}

	/**
	 * @return Returns the outOfTownState.
	 */
	public String getOutOfTownState() {
		return outOfTownState;
	}

	/**
	 * @param outOfTownState
	 *            The outOfTownState to set.
	 */
	public void setOutOfTownState(String outOfTownState) {
		this.outOfTownState = outOfTownState;
	}

	/**
	 * @return Returns the outOfTownStreetName.
	 */
	public String getOutOfTownStreetName() {
		return outOfTownStreetName;
	}

	/**
	 * @param outOfTownStreetName
	 *            The outOfTownStreetName to set.
	 */
	public void setOutOfTownStreetName(String outOfTownStreetName) {
		this.outOfTownStreetName = outOfTownStreetName;
	}

	/**
	 * @return Returns the outOfTownStreetNumber.
	 */
	public String getOutOfTownStreetNumber() {
		return outOfTownStreetNumber;
	}

	/**
	 * @param outOfTownStreetNumber
	 *            The outOfTownStreetNumber to set.
	 */
	public void setOutOfTownStreetNumber(String outOfTownStreetNumber) {
		this.outOfTownStreetNumber = outOfTownStreetNumber;
	}

	/**
	 * @return Returns the outOfTownUnitNumber.
	 */
	public String getOutOfTownUnitNumber() {
		return outOfTownUnitNumber;
	}

	/**
	 * @param outOfTownUnitNumber
	 *            The outOfTownUnitNumber to set.
	 */
	public void setOutOfTownUnitNumber(String outOfTownUnitNumber) {
		this.outOfTownUnitNumber = outOfTownUnitNumber;
	}

	/**
	 * @return Returns the outOfTownZip.
	 */
	public String getOutOfTownZip() {
		return outOfTownZip;
	}

	/**
	 * @param outOfTownZip
	 *            The outOfTownZip to set.
	 */
	public void setOutOfTownZip(String outOfTownZip) {
		this.outOfTownZip = outOfTownZip;
	}

	/**
	 * @return Returns the outOfTownZip4.
	 */
	public String getOutOfTownZip4() {
		return outOfTownZip4;
	}

	/**
	 * @param outOfTownZip4
	 *            The outOfTownZip4 to set.
	 */
	public void setOutOfTownZip4(String outOfTownZip4) {
		this.outOfTownZip4 = outOfTownZip4;
	}

	/**
	 * @return Returns the ownershipType.
	 */
	public String getOwnershipType() {
		return ownershipType;
	}

	/**
	 * @param ownershipType
	 *            The ownershipType to set.
	 */
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	/**
	 * @return Returns the prevCity.
	 */
	public String getPrevCity() {
		return prevCity;
	}

	/**
	 * @param prevCity
	 *            The prevCity to set.
	 */
	public void setPrevCity(String prevCity) {
		this.prevCity = prevCity;
	}

	/**
	 * @return Returns the prevState.
	 */
	public String getPrevState() {
		return prevState;
	}

	/**
	 * @param prevState
	 *            The prevState to set.
	 */
	public void setPrevState(String prevState) {
		this.prevState = prevState;
	}

	/**
	 * @return Returns the prevStreetName.
	 */
	public String getPrevStreetName() {
		return prevStreetName;
	}

	/**
	 * @param prevStreetName
	 *            The prevStreetName to set.
	 */
	public void setPrevStreetName(String prevStreetName) {
		this.prevStreetName = prevStreetName;
	}

	/**
	 * @return Returns the prevStreetNumber.
	 */
	public String getPrevStreetNumber() {
		return prevStreetNumber;
	}

	/**
	 * @param prevStreetNumber
	 *            The prevStreetNumber to set.
	 */
	public void setPrevStreetNumber(String prevStreetNumber) {
		this.prevStreetNumber = prevStreetNumber;
	}

	/**
	 * @return Returns the prevUnitNumber.
	 */
	public String getPrevUnitNumber() {
		return prevUnitNumber;
	}

	/**
	 * @param prevUnitNumber
	 *            The prevUnitNumber to set.
	 */
	public void setPrevUnitNumber(String prevUnitNumber) {
		this.prevUnitNumber = prevUnitNumber;
	}

	/**
	 * @return Returns the prevZip.
	 */
	public String getPrevZip() {
		return prevZip;
	}

	/**
	 * @param prevZip
	 *            The prevZip to set.
	 */
	public void setPrevZip(String prevZip) {
		this.prevZip = prevZip;
	}

	/**
	 * @return Returns the prevZip4.
	 */
	public String getPrevZip4() {
		return prevZip4;
	}

	/**
	 * @param prevZip4
	 *            The prevZip4 to set.
	 */
	public void setPrevZip4(String prevZip4) {
		this.prevZip4 = prevZip4;
	}

	/**
	 * @return Returns the quantity.
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            The quantity to set.
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return Returns the quantityNum.
	 */
	public String getQuantityNum() {
		return quantityNum;
	}

	/**
	 * @param quantityNum
	 *            The quantityNum to set.
	 */
	public void setQuantityNum(String quantityNum) {
		this.quantityNum = quantityNum;
	}

	/**
	 * @return Returns the renewalDate.
	 */
	public String getRenewalDate() {
		return renewalDate;
	}

	/**
	 * @return Returns the renewalDt.
	 */
	public String getRenewalDt() {
		return renewalDt;
	}

	/**
	 * @param renewalDt
	 *            The renewalDt to set.
	 */
	public void setRenewalDt(String renewalDt) {
		this.renewalDt = renewalDt;
	}

	/**
	 * @param renewalDate
	 *            The renewalDate to set.
	 */
	public void setRenewalDate(String renewalDate) {
		this.renewalDate = renewalDate;
	}

	/**
	 * @return Returns the sicCode.
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            The sicCode to set.
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	/**
	 * @return Returns the socialSecurityNumber.
	 */
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	/**
	 * @param socialSecurityNumber
	 *            The socialSecurityNumber to set.
	 */
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	/**
	 * @return Returns the squareFootage.
	 */
	public String getSquareFootage() {
		return squareFootage;
	}

	/**
	 * @param squareFootage
	 *            The squareFootage to set.
	 */
	public void setSquareFootage(String squareFootage) {
		this.squareFootage = squareFootage;
	}

	/**
	 * @return Returns the startingDate.
	 */
	public String getStartingDate() {
		return startingDate;
	}

	/**
	 * @param startingDate
	 *            The startingDate to set.
	 */
	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(String subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return Returns the typeOfExemptions.
	 */
	public String getTypeOfExemptions() {
		return typeOfExemptions;
	}

	/**
	 * @param typeOfExemptions
	 *            The typeOfExemptions to set.
	 */
	public void setTypeOfExemptions(String typeOfExemptions) {
		logger.debug("In setTypeOfExemptions " + typeOfExemptions);
		this.typeOfExemptions = typeOfExemptions;
	}

	/**
	 * @return Returns the displayContent.
	 */
	public String getDisplayContent() {
		return displayContent;
	}

	/**
	 * @param displayContent
	 *            The displayContent to set.
	 */
	public void setDisplayContent(String displayContent) {
		this.displayContent = displayContent;
	}

	/**
	 * Gets the BusinessLicenseActivity
	 * 
	 * @return Returns a BusinessLicenseActivity
	 */
	public BusinessLicenseActivity getBusinessLicenseActivity() throws Exception {

		logger.debug("In getBusinessLicenseActivity " + businessLicenseActivity);
		businessLicenseActivity = new BusinessLicenseActivity();
		businessLicenseActivity.setBusinessName(this.businessName);
		businessLicenseActivity.setApplicationType(LookupAgent.getApplicationType(StringUtils.s2i(this.getApplicationType())));
		businessLicenseActivity.setActivityStatus(LookupAgent.getActivityStatus(StringUtils.s2i(this.getActivityStatus())));
		businessLicenseActivity.setOwnershipType(LookupAgent.getOwnershipType(StringUtils.s2i(this.getOwnershipType())));
		businessLicenseActivity.setActivityType(LookupAgent.getActivityType(this.getActivityType()));
		businessLicenseActivity.setActivitySubType(LookupAgent.getActivitySubType(this.getActivitySubType()));
		businessLicenseActivity.setQuantity(LookupAgent.getQuantityType(StringUtils.s2i(this.getQuantity())));
		businessLicenseActivity.setTypeOfExemptions(LookupAgent.getExemptionType(StringUtils.s2i(this.getTypeOfExemptions())));
		businessLicenseActivity.setStreet(LookupAgent.getStreet(this.getAddressStreetName()));
		businessLicenseActivity.setSubProjectId(StringUtils.s2i(this.getSubProjectId()));
		businessLicenseActivity.setBusinessLocation(this.isBusinessLocation());
		businessLicenseActivity.setAddressStreetNumber(this.getAddressStreetNumber());
		businessLicenseActivity.setAddressStreetFraction(this.getAddressStreetFraction());
		businessLicenseActivity.setAddressUnitNumber(this.getAddressUnitNumber());
		businessLicenseActivity.setAddressCity(this.getAddressCity());
		businessLicenseActivity.setAddressState(this.getAddressState());
		businessLicenseActivity.setAddressZip(this.getAddressZip());
		businessLicenseActivity.setAddressZip4(this.getAddressZip4());
		businessLicenseActivity.setOutOfTownStreetNumber(this.getOutOfTownStreetNumber());
		businessLicenseActivity.setOutOfTownStreetName(this.getOutOfTownStreetName());
		businessLicenseActivity.setOutOfTownUnitNumber(this.getOutOfTownUnitNumber());
		businessLicenseActivity.setOutOfTownCity(this.getOutOfTownCity());
		businessLicenseActivity.setOutOfTownState(this.getOutOfTownState());
		businessLicenseActivity.setOutOfTownZip(this.getOutOfTownZip());
		businessLicenseActivity.setOutOfTownZip4(this.getOutOfTownZip4());
		businessLicenseActivity.setActivityNumber(this.getActivityNumber());
		businessLicenseActivity.setBusinessName(this.getBusinessName());
		businessLicenseActivity.setBusinessAccountNumber(this.getBusinessAccountNumber());
		businessLicenseActivity.setCorporateName(this.getCorporateName());
		businessLicenseActivity.setBusinessPhone(this.getBusinessPhone());
		businessLicenseActivity.setBusinessExtension(this.getBusinessExtension());
		businessLicenseActivity.setBusinessFax(this.getBusinessFax());
		businessLicenseActivity.setMuncipalCode(this.getMuncipalCode());
		businessLicenseActivity.setSicCode(this.getSicCode());
		businessLicenseActivity.setDescOfBusiness(this.getDescOfBusiness());
		businessLicenseActivity.setClassCode(this.getClassCode());
		businessLicenseActivity.setHomeOccupation(this.isHomeOccupation());
		businessLicenseActivity.setDecalCode(this.isDecalCode());
		businessLicenseActivity.setFederalIdNumber(this.getFederalIdNumber());
		businessLicenseActivity.setEmailAddress(this.getEmailAddress());
		businessLicenseActivity.setSocialSecurityNumber(this.getSocialSecurityNumber());
		businessLicenseActivity.setMailStreetNumber(this.getMailStreetNumber());
		businessLicenseActivity.setMailStreetName(this.getMailStreetName());
		businessLicenseActivity.setMailUnitNumber(this.getMailUnitNumber());
		businessLicenseActivity.setMailCity(this.getMailCity());
		businessLicenseActivity.setMailState(this.getMailState());
		businessLicenseActivity.setMailZip(this.getMailZip());
		businessLicenseActivity.setMailZip4(this.getMailZip4());
		businessLicenseActivity.setPrevStreetNumber(this.getPrevStreetNumber());
		businessLicenseActivity.setPrevStreetName(this.getPrevStreetName());
		businessLicenseActivity.setPrevUnitNumber(this.getPrevUnitNumber());
		businessLicenseActivity.setPrevCity(this.getPrevCity());
		businessLicenseActivity.setPrevState(this.getPrevState());
		businessLicenseActivity.setPrevZip(this.getPrevZip());
		businessLicenseActivity.setPrevZip4(this.getPrevZip4());
		businessLicenseActivity.setSquareFootage(this.getSquareFootage());
		businessLicenseActivity.setQuantityNum(this.getQuantityNum());
		businessLicenseActivity.setDisplayContent(this.getDisplayContent());
		businessLicenseActivity.setCreationDate(StringUtils.str2cal(this.getCreationDate()));
		businessLicenseActivity.setRenewalDate(StringUtils.str2cal(this.getRenewalDate()));
		businessLicenseActivity.setInsuranceExpDate(StringUtils.str2cal(this.getInsuranceExpDate()));
		businessLicenseActivity.setBondExpDate(StringUtils.str2cal(this.getBondExpDate()));
		businessLicenseActivity.setDeptOfJusticeExpDate(StringUtils.str2cal(this.getDeptOfJusticeExpDate()));
		businessLicenseActivity.setApplicationDate(StringUtils.str2cal(this.getApplicationDate()));
		businessLicenseActivity.setIssueDate(StringUtils.str2cal(this.getIssueDate()));
		businessLicenseActivity.setStartingDate(StringUtils.str2cal(this.getStartingDate()));
		businessLicenseActivity.setOutOfBusinessDate(StringUtils.str2cal(this.getOutOfBusinessDate()));
		businessLicenseActivity.setFederalFirearmsLiscExpDate(StringUtils.str2cal(this.getFederalFirearmsLiscExpDate()));
		businessLicenseActivity.setMultiAddress(this.getMultiAddress());

		logger.debug("In getBusinessLicenseActivity " + businessLicenseActivity);

		return businessLicenseActivity;
	}

	public BusinessLicenseActivityForm setBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity) throws Exception {

		BusinessLicenseActivityForm businessLicenseActivityForm = new BusinessLicenseActivityForm();
		logger.debug("In setBusinessLicenseActivity " + businessLicenseActivity);

		if (businessLicenseActivity.getActivityType() != null) {
			businessLicenseActivityForm.setActivityType(businessLicenseActivity.getActivityType().getTypeId());
		} else {
			businessLicenseActivityForm.setActivityType("");
		}
		if (businessLicenseActivity.getActivityStatus() != null) {
			businessLicenseActivityForm.setActivityStatus(StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()));
		} else {
			businessLicenseActivityForm.setActivityStatus("");
		}
		if (businessLicenseActivity.getOwnershipType() != null) {
			businessLicenseActivityForm.setOwnershipType(StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()));
		} else {
			businessLicenseActivityForm.setOwnershipType("");
		}
		if (businessLicenseActivity.getQuantity() != null) {
			businessLicenseActivityForm.setQuantity(StringUtils.i2s(businessLicenseActivity.getQuantity().getId()));
		} else {
			businessLicenseActivityForm.setQuantity("");
		}
		if (businessLicenseActivity.getTypeOfExemptions() != null) {
			businessLicenseActivityForm.setTypeOfExemptions(StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()));
		} else {
			businessLicenseActivityForm.setTypeOfExemptions("");
		}
		if(businessLicenseActivity.getMultiAddress() != null){
			businessLicenseActivityForm.setMultiAddress(businessLicenseActivity.getMultiAddress());
		}else{
			businessLicenseActivityForm.setMultiAddress(new MultiAddress[0]);
		}
		
		
		businessLicenseActivityForm.setActivityTypeDescription(businessLicenseActivity.getActivityType().getDescription());
		businessLicenseActivityForm.setAddressStreetNumber(businessLicenseActivity.getAddressStreetNumber());
		businessLicenseActivityForm.setAddressStreetFraction(businessLicenseActivity.getAddressStreetFraction());
		businessLicenseActivityForm.setAddressStreetName(businessLicenseActivity.getAddressStreetName());
		businessLicenseActivityForm.setAddressUnitNumber(businessLicenseActivity.getAddressUnitNumber());
		businessLicenseActivityForm.setAddressCity(businessLicenseActivity.getAddressCity());
		businessLicenseActivityForm.setAddressState(businessLicenseActivity.getAddressState());
		businessLicenseActivityForm.setAddressZip(businessLicenseActivity.getAddressZip());
		businessLicenseActivityForm.setAddressZip4(businessLicenseActivity.getAddressZip4());
		businessLicenseActivityForm.setOutOfTownStreetNumber(businessLicenseActivity.getOutOfTownStreetNumber());
		businessLicenseActivityForm.setOutOfTownStreetName(businessLicenseActivity.getOutOfTownStreetName());
		businessLicenseActivityForm.setOutOfTownUnitNumber(businessLicenseActivity.getOutOfTownUnitNumber());
		businessLicenseActivityForm.setOutOfTownCity(businessLicenseActivity.getOutOfTownCity());
		businessLicenseActivityForm.setOutOfTownState(businessLicenseActivity.getOutOfTownState());
		businessLicenseActivityForm.setOutOfTownZip(businessLicenseActivity.getOutOfTownZip());
		businessLicenseActivityForm.setOutOfTownZip4(businessLicenseActivity.getOutOfTownZip4());
		businessLicenseActivityForm.setActivityNumber(businessLicenseActivity.getActivityNumber());
		businessLicenseActivityForm.setBusinessName(businessLicenseActivity.getBusinessName());
		businessLicenseActivityForm.setBusinessAccountNumber(businessLicenseActivity.getBusinessAccountNumber());
		businessLicenseActivityForm.setCorporateName(businessLicenseActivity.getCorporateName());
		businessLicenseActivityForm.setBusinessPhone(businessLicenseActivity.getBusinessPhone());
		businessLicenseActivityForm.setBusinessExtension(businessLicenseActivity.getBusinessExtension());
		businessLicenseActivityForm.setBusinessFax(businessLicenseActivity.getBusinessFax());

		businessLicenseActivityForm.setDescOfBusiness(businessLicenseActivity.getDescOfBusiness());
		businessLicenseActivityForm.setHomeOccupation(businessLicenseActivity.isHomeOccupation());
		businessLicenseActivityForm.setDecalCode(businessLicenseActivity.isDecalCode());
		businessLicenseActivityForm.setFederalIdNumber(businessLicenseActivity.getFederalIdNumber());
		businessLicenseActivityForm.setEmailAddress(businessLicenseActivity.getEmailAddress());
		businessLicenseActivityForm.setSocialSecurityNumber(businessLicenseActivity.getSocialSecurityNumber());
		businessLicenseActivityForm.setMailStreetNumber(businessLicenseActivity.getMailStreetNumber());
		businessLicenseActivityForm.setMailStreetName(businessLicenseActivity.getMailStreetName());
		businessLicenseActivityForm.setMailUnitNumber(businessLicenseActivity.getMailUnitNumber());
		businessLicenseActivityForm.setMailCity(businessLicenseActivity.getMailCity());
		businessLicenseActivityForm.setMailState(businessLicenseActivity.getMailState());
		businessLicenseActivityForm.setMailZip(businessLicenseActivity.getMailZip());
		businessLicenseActivityForm.setMailZip4(businessLicenseActivity.getMailZip4());
		businessLicenseActivityForm.setPrevStreetNumber(businessLicenseActivity.getPrevStreetNumber());
		businessLicenseActivityForm.setPrevStreetName(businessLicenseActivity.getPrevStreetName());
		businessLicenseActivityForm.setPrevUnitNumber(businessLicenseActivity.getPrevUnitNumber());
		businessLicenseActivityForm.setPrevCity(businessLicenseActivity.getPrevCity());
		businessLicenseActivityForm.setPrevState(businessLicenseActivity.getPrevState());
		businessLicenseActivityForm.setPrevZip(businessLicenseActivity.getPrevZip());
		businessLicenseActivityForm.setPrevZip4(businessLicenseActivity.getPrevZip4());
		businessLicenseActivityForm.setSquareFootage(businessLicenseActivity.getSquareFootage());
		businessLicenseActivityForm.setQuantityNum(businessLicenseActivity.getQuantityNum());

		businessLicenseActivityForm.setCreationDate(StringUtils.cal2str(businessLicenseActivity.getCreationDate()));
		businessLicenseActivityForm.setRenewalDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate()));
		businessLicenseActivityForm.setInsuranceExpDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate()));
		businessLicenseActivityForm.setBondExpDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate()));
		businessLicenseActivityForm.setDeptOfJusticeExpDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate()));
		businessLicenseActivityForm.setApplicationDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate()));
		businessLicenseActivityForm.setIssueDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate()));
		businessLicenseActivityForm.setStartingDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate()));
		businessLicenseActivityForm.setOutOfBusinessDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate()));
		businessLicenseActivityForm.setFederalFirearmsLiscExpDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate()));

		return businessLicenseActivityForm;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		addressStreetNumber = null;
		addressStreetFraction = null;
		addressUnitNumber = null;
		addressCity = null;
		addressState = null;
		addressZip = null;
		addressZip4 = null;
		applicationType = null;
		outOfTownStreetNumber = null;
		outOfTownStreetName = null;
		outOfTownUnitNumber = null;
		outOfTownCity = null;
		outOfTownState = null;
		outOfTownZip = null;
		outOfTownZip4 = null;
		activityNumber = null;
		activityStatus = null;
		creationDate = null;
		renewalDate = null;
		businessName = null;
		businessAccountNumber = null;
		corporateName = null;
		businessPhone = null;
		businessExtension = null;
		businessFax = null;
		activityType = null;
		activitySubType = null;
		muncipalCode = null;
		sicCode = null;
		descOfBusiness = null;
		classCode = null;
		applicationDate = null;
		startingDate = null;
		outOfBusinessDate = null;
		ownershipType = null;
		federalIdNumber = null;
		emailAddress = null;
		socialSecurityNumber = null;
		mailStreetNumber = null;
		mailStreetName = null;
		mailUnitNumber = null;
		mailCity = null;
		mailState = null;
		mailZip = null;
		mailZip4 = null;
		prevStreetNumber = null;
		prevStreetName = null;
		prevUnitNumber = null;
		prevCity = null;
		prevState = null;
		prevZip = null;
		prevZip4 = null;
		insuranceExpDate = null;
		bondExpDate = null;
		deptOfJusticeExpDate = null;
		federalFirearmsLiscExpDate = null;
		squareFootage = null;
		quantity = null;
		quantityNum = null;
		typeOfExemptions = null;
		subProjectId = null;
		displayContent = null;
	}

	/**
	 * @author Gayathri The reset method is to reset form values when it changes its application type
	 */

	public void reset() {
		this.addressStreetNumber = null;
		this.addressStreetFraction = null;
		this.addressUnitNumber = null;
		this.addressCity = null;
		this.addressState = null;
		this.addressZip = null;
		this.addressZip4 = null;
		this.outOfTownStreetNumber = null;
		this.outOfTownStreetName = null;
		this.outOfTownUnitNumber = null;
		this.outOfTownCity = null;
		this.outOfTownState = null;
		this.outOfTownZip = null;
		this.outOfTownZip4 = null;
		this.activityNumber = null;
		this.creationDate = null;
		this.renewalDate = null;
		this.businessName = null;
		this.businessAccountNumber = null;
		this.corporateName = null;
		this.businessPhone = null;
		this.businessExtension = null;
		this.businessFax = null;
		this.activityType = null;
		this.activitySubType = null;
		this.muncipalCode = null;
		this.sicCode = null;
		this.descOfBusiness = null;
		this.classCode = null;
		this.decalCode = false;
		this.homeOccupation = false;
		this.startingDate = null;
		this.outOfBusinessDate = null;
		this.ownershipType = null;
		this.federalIdNumber = null;
		this.emailAddress = null;
		this.socialSecurityNumber = null;
		this.issueDate = null;
		this.mailStreetNumber = null;
		this.mailStreetName = null;
		this.mailUnitNumber = null;
		this.mailCity = null;
		this.mailState = null;
		this.mailZip = null;
		this.mailZip4 = null;
		this.prevStreetNumber = null;
		this.prevStreetName = null;
		this.prevUnitNumber = null;
		this.prevCity = null;
		this.prevState = null;
		this.prevZip = null;
		this.prevZip4 = null;
		this.insuranceExpDate = null;
		this.bondExpDate = null;
		this.deptOfJusticeExpDate = null;
		this.federalFirearmsLiscExpDate = null;
		this.squareFootage = null;
		this.quantity = null;
		this.quantityNum = null;
		this.typeOfExemptions = null;
		this.subProjectId = null;
		this.displayContent = null;
	}

	/**
	 * @return Returns the createdBy.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            The createdBy to set.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the updatedBy.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            The updatedBy to set.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return Returns the outStandingFees.
	 */
	public String getOutStandingFees() {
		return outStandingFees;
	}

	/**
	 * @param outStandingFees
	 *            The outStandingFees to set.
	 */
	public void setOutStandingFees(String outStandingFees) {
		this.outStandingFees = outStandingFees;
	}

	public MultiAddress[] getMultiAddress() {
		return multiAddress;
	}

	public void setMultiAddress(MultiAddress[] multiAddress) {
		this.multiAddress = multiAddress;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getLsoId() {
		return lsoId;
	}

	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}
}
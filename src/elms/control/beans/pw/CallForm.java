/*
 * Created on Sep 26, 2003
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.beans.pw;

import elms.app.pw.ActivityDetail;
import elms.app.pw.CallLogDetail;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * call form
 * @author abelaguly
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class CallForm extends ActionForm {
    protected String action;

    protected int activityId;
    protected int subProjectId;
    protected int projectId;
    protected int userId;

    protected String activityNbr;
    protected String enteredBy;
    protected String enteredTime;
    protected boolean internal;
    protected String firstName;
    protected String lastName;
    protected String division;
    protected String title;
    protected String streetNbr;
    protected String streetFraction;
    protected String streetName;
    protected String unitNbr;
    protected String city;
    protected String state;
    protected String zip;
    protected String phoneNbr;
    protected String internalLocation;
    protected String externalLocation;
    protected String otherNbr;
    protected String referenceNbr;
    protected String crossStreetName1;
    protected String crossStreetName2;
    protected String priority;
    protected String description;
    protected String notes;
    protected boolean traffic;
    protected boolean parking;
    protected String selectedActivityType;

    protected ActivityDetail[] availableActivities;
    protected CallLogDetail[] callLogs;

    protected String displayText;
    protected int serviceRequestCount;

    public CallForm() {
        super();
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        if (this.availableActivities != null)
            for (int i = 0; i < availableActivities.length; i++) {
                this.availableActivities[i].setType("");
            }
    }


    public ActionErrors validate(
            ActionMapping mapping,
            HttpServletRequest request) {

        ActionErrors errors = new ActionErrors();
        return errors;
    }

    /**
     * @return
     */
    public String getAction() {
        return action;
    }


    /**
     * @return
     */
    public int getActivityId() {
        return activityId;
    }

    /**
     * @return
     */
    public String getActivityNbr() {
        return activityNbr;
    }

    /**
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return
     */
    public String getDivision() {
        return division;
    }

    /**
     * @return
     */
    public String getEnteredBy() {
        return enteredBy;
    }

    /**
     * @return
     */
    public String getEnteredTime() {
        return enteredTime;
    }

    /**
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @return
     */
    public String getLastName() {
        return lastName;
    }


    /**
     * @return
     */
    public String getNotes() {
        return notes;
    }

    /**
     * @return
     */
    public String getOtherNbr() {
        return otherNbr;
    }


    /**
     * @return
     */
    public String getPriority() {
        return priority;
    }

    /**
     * @return
     */
    public int getProjectId() {
        return projectId;
    }

    /**
     * @return
     */
    public String getReferenceNbr() {
        return referenceNbr;
    }

    /**
     * @return
     */
    public String getState() {
        return state;
    }

    /**
     * @return
     */
    public String getStreetFraction() {
        return streetFraction;
    }

    /**
     * @return
     */
    public String getStreetNbr() {
        return streetNbr;
    }

    /**
     * @return
     */
    public int getSubProjectId() {
        return subProjectId;
    }

    /**
     * @return
     */
    public String getUnitNbr() {
        return unitNbr;
    }

    /**
     * @return
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param string
     */
    public void setAction(String string) {
        action = string;
    }


    /**
     * @param i
     */
    public void setActivityId(int i) {
        activityId = i;
    }

    /**
     * @param string
     */
    public void setActivityNbr(String string) {
        activityNbr = string;
    }

    /**
     * @param string
     */
    public void setCity(String string) {
        city = string;
    }

    /**
     * @param string
     */
    public void setDescription(String string) {
        description = string;
    }

    /**
     * @param string
     */
    public void setDivision(String string) {
        division = string;
    }

    /**
     * @param string
     */
    public void setEnteredBy(String string) {
        enteredBy = string;
    }

    /**
     * @param string
     */
    public void setEnteredTime(String string) {
        enteredTime = string;
    }

    /**
     * @param string
     */
    public void setFirstName(String string) {
        firstName = string;
    }

    /**
     * @param string
     */
    public void setLastName(String string) {
        lastName = string;
    }


    /**
     * @param string
     */
    public void setNotes(String string) {
        notes = string;
    }

    /**
     * @param string
     */
    public void setOtherNbr(String string) {
        otherNbr = string;
    }

    /**
     * @param string
     */
    public void setPriority(String string) {
        priority = string;
    }

    /**
     * @param i
     */
    public void setProjectId(int i) {
        projectId = i;
    }

    /**
     * @param string
     */
    public void setReferenceNbr(String string) {
        referenceNbr = string;
    }


    /**
     * @param string
     */
    public void setState(String string) {
        state = string;
    }

    /**
     * @param string
     */
    public void setStreetFraction(String string) {
        streetFraction = string;
    }

    /**
     * @param string
     */
    public void setStreetNbr(String string) {
        streetNbr = string;
    }

    /**
     * @param i
     */
    public void setSubProjectId(int i) {
        subProjectId = i;
    }

    /**
     * @param string
     */
    public void setUnitNbr(String string) {
        unitNbr = string;
    }

    /**
     * @param string
     */
    public void setZip(String string) {
        zip = string;
    }

    /**
     * Returns the title.
     * @return String
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     * @param title The title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Returns the phoneNbr.
     * @return String
     */
    public String getPhoneNbr() {
        return phoneNbr;
    }

    /**
     * Sets the phoneNbr.
     * @param phoneNbr The phoneNbr to set
     */
    public void setPhoneNbr(String phoneNbr) {
        this.phoneNbr = phoneNbr;
    }

    /**
     * Returns the streetName.
     * @return String
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the streetName.
     * @param streetName The streetName to set
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * Returns the externalLocation.
     * @return String
     */
    public String getExternalLocation() {
        return externalLocation;
    }

    /**
     * Returns the internalLocation.
     * @return String
     */
    public String getInternalLocation() {
        return internalLocation;
    }

    /**
     * Sets the externalLocation.
     * @param externalLocation The externalLocation to set
     */
    public void setExternalLocation(String externalLocation) {
        this.externalLocation = externalLocation;
    }

    /**
     * Sets the internalLocation.
     * @param internalLocation The internalLocation to set
     */
    public void setInternalLocation(String internalLocation) {
        this.internalLocation = internalLocation;
    }

    /**
     * @return
     */
    public CallLogDetail[] getCallLogs() {
        return callLogs;
    }

    /**
     * @param details
     */
    public void setCallLogs(CallLogDetail[] details) {
        callLogs = details;
    }

    public void setCallLogs(List activities) {
        if (activities == null) activities = new ArrayList();
        CallLogDetail[] callLogDetailArray = new CallLogDetail[activities.size()];
        Iterator iter = activities.iterator();
        int index = 0;
        while (iter.hasNext()) {
            callLogDetailArray[index] = (CallLogDetail) iter.next();
            index++;
        }
        this.callLogs = callLogDetailArray;
    }

    /**
     * @return
     */
    public ActivityDetail[] getAvailableActivities() {
        return availableActivities;
    }

    /**
     * @param details
     */
    public void setAvailableActivities(ActivityDetail[] details) {
        availableActivities = details;
    }

    public void setAvailableActivities(List details) {
        if (details == null) details = new ArrayList();
        ActivityDetail[] detailArray = new ActivityDetail[details.size()];
        Iterator iter = details.iterator();
        int index = 0;
        while (iter.hasNext()) {
            detailArray[index] = (ActivityDetail) iter.next();
            index++;
        }
        this.availableActivities = detailArray;

    }

    /**
     * @return
     */
    public String getCrossStreetName1() {
        return crossStreetName1;
    }

    /**
     * @return
     */
    public String getCrossStreetName2() {
        return crossStreetName2;
    }

    /**
     * @param string
     */
    public void setCrossStreetName1(String string) {
        crossStreetName1 = string;
    }

    /**
     * @param string
     */
    public void setCrossStreetName2(String string) {
        crossStreetName2 = string;
    }

    /**
     * @return
     */
    public boolean isParking() {
        return parking;
    }

    /**
     * @return
     */
    public boolean isTraffic() {
        return traffic;
    }

    /**
     * @param b
     */
    public void setParking(boolean b) {
        parking = b;
    }

    /**
     * @param b
     */
    public void setTraffic(boolean b) {
        traffic = b;
    }

    /**
     * @return
     */
    public String getDisplayText() {
        return displayText;
    }

    /**
     * @param string
     */
    public void setDisplayText(String string) {
        displayText = string;
    }

    /**
     * @return
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param i
     */
    public void setUserId(int i) {
        userId = i;
    }

    /**
     * @return
     */
    public boolean getInternal() {
        return internal;
    }

    /**
     * @param b
     */
    public void setInternal(boolean b) {
        internal = b;
    }

    /**
     * @return
     */
    public int getServiceRequestCount() {
        return serviceRequestCount;
    }

    /**
     * @param i
     */
    public void setServiceRequestCount(int i) {
        serviceRequestCount = i;
    }

    /**
     * @return
     */
    public String getSelectedActivityType() {
        return selectedActivityType;
    }

    /**
     * @param string
     */
    public void setSelectedActivityType(String string) {
        selectedActivityType = string;
    }

}

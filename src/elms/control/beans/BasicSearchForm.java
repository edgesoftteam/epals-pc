package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class BasicSearchForm extends ActionForm {
	/**
	 * Member variable declaration
	 */
	protected String activityNumber;
	protected String projectTeam;
	protected String personType;
	protected String streetName;
	protected String streetName1;
	protected String streetName2;
	protected String streetNumber;
	protected String unitNbr;
	protected String apn;
	protected String ownerName;
	protected String owner1;
	protected String owner2;
	protected String projectNumber;
	protected String subProjectNumber;
	protected String streetFraction;
	protected String subProjectSubType;
	protected String subProjectType;
	protected String personInfoType;
	protected String[] activityType;
	protected String[] activityStatus;// added by aman
	protected String[] activitySubType;
	protected String projectType;
	protected String projectName;
	protected String streetId;
	protected String name;
	protected String lastName;
	protected String peopleName;
	protected String peopleLic;
	protected int lsoId;
	protected int activityId;
	protected int projectId;
	protected List activityList;
	protected String activityListSize;
	protected List userList;
	protected String query;
	protected String table;
	protected String activityColumns;
	protected String activityOperators;
	protected String activityValue;
	protected String add;
	protected String projectColumns;
	protected String projectOperators;
	protected String projectValue;
	protected String inspectionColumns;
	protected String inspectionOperators;
	protected String inspectionValue;
	protected String and;
	protected String or;
	protected String not;
	protected String brackets;
	protected String search;
	protected String cancel;
	protected String foreignAddressLine1;
	protected String foreignAddressLine2;
	protected String foreignAddressLine3;
	protected String foreignAddressLine4;
	protected String foreignAddressState;
	protected String foreignAddressCountry;
	protected String foreignAddressZip;
	protected String landmark;

	// for GIS Search
	protected boolean allInspectors = false;
	protected String inspector;
	protected String zone;
	protected String activityDate;
	protected String fromDate;
	protected String toDate;

	// for Business License Search
	protected String businessAcNumber;
	protected String businessName;
	protected String businessOwnerName;
	protected String outofTownAddress1;
	protected String outofTownAddress2;
	protected String outofTownAddress3;
	protected String outofTownAddress4;
	protected String outofTownAddressState;
	protected String outofTownAddressCountry;
	protected String outofTownAddressZip;

	// For combo permit

	protected String comboPermit;

	protected String crossStreetName1;
	protected String crossStreetName2;

	public String getCrossStreetName1() {
		return crossStreetName1;
	}

	public void setCrossStreetName1(String crossStreetName1) {
		this.crossStreetName1 = crossStreetName1;
	}

	public String getCrossStreetName2() {
		return crossStreetName2;
	}

	public void setCrossStreetName2(String crossStreetName2) {
		this.crossStreetName2 = crossStreetName2;
	}

	/**
	 * @return Returns the comboList.
	 */
	public List getComboList() {
		return comboList;
	}

	/**
	 * @param comboList
	 *            The comboList to set.
	 */
	public void setComboList(List comboList) {
		this.comboList = comboList;
	}

	/**
	 * @return Returns the comboListSize.
	 */
	public String getComboListSize() {
		return comboListSize;
	}

	/**
	 * @param comboListSize
	 *            The comboListSize to set.
	 */
	public void setComboListSize(String comboListSize) {
		this.comboListSize = comboListSize;
	}

	/**
	 * @return Returns the comboName.
	 */
	public String getComboName() {
		return comboName;
	}

	/**
	 * @param comboName
	 *            The comboName to set.
	 */
	public void setComboName(String comboName) {
		this.comboName = comboName;
	}

	/**
	 * @return Returns the comboPermit.
	 */
	public String getComboPermit() {
		return comboPermit;
	}

	/**
	 * @param comboPermit
	 *            The comboPermit to set.
	 */
	public void setComboPermit(String comboPermit) {
		this.comboPermit = comboPermit;
	}

	/**
	 * @return Returns the online.
	 */
	public String getOnline() {
		return online;
	}

	/**
	 * @param online
	 *            The online to set.
	 */
	public void setOnline(String online) {
		this.online = online;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	protected String comboName;
	protected List comboList;
	protected String comboListSize;
	protected int subProjectId;
	protected String online;

	/**
	 * Gets the activityNumber
	 * 
	 * @return Returns a String
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * Sets the activityNumber
	 * 
	 * @param activityNumber
	 *            The activityNumber to set
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * Gets the projectTeam
	 * 
	 * @return Returns a String
	 */
	public String getProjectTeam() {
		return projectTeam;
	}

	/**
	 * Sets the projectTeam
	 * 
	 * @param projectTeam
	 *            The projectTeam to set
	 */
	public void setProjectTeam(String projectTeam) {
		this.projectTeam = projectTeam;
	}

	/**
	 * Gets the projectNumber
	 * 
	 * @return Returns a String
	 */
	public String getProjectNumber() {
		return projectNumber;
	}

	/**
	 * Sets the projectNumber
	 * 
	 * @param projectNumber
	 *            The projectNumber to set
	 */
	public void setProjectNumber(String projectNumber) {
		this.projectNumber = projectNumber;
	}

	/**
	 * Gets the streetFraction
	 * 
	 * @return Returns a String
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * Sets the streetFraction
	 * 
	 * @param streetFraction
	 *            The streetFraction to set
	 */
	public void setStreetFraction(String streetFraction) {
		this.streetFraction = streetFraction;
	}

	/**
	 * Gets the subProjectSubType
	 * 
	 * @return Returns a String
	 */
	public String getSubProjectSubType() {
		return subProjectSubType;
	}

	/**
	 * Sets the subProjectSubType
	 * 
	 * @param subProjectSubType
	 *            The subProjectSubType to set
	 */
	public void setSubProjectSubType(String subProjectSubType) {
		this.subProjectSubType = subProjectSubType;
	}

	/**
	 * Gets the subProjectType
	 * 
	 * @return Returns a String
	 */
	public String getSubProjectType() {
		return subProjectType;
	}

	/**
	 * Sets the subProjectType
	 * 
	 * @param subProjectType
	 *            The subProjectType to set
	 */
	public void setSubProjectType(String subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * Gets the personInfoType
	 * 
	 * @return Returns a String
	 */
	public String getPersonInfoType() {
		return personInfoType;
	}

	/**
	 * Sets the personInfoType
	 * 
	 * @param personInfoType
	 *            The personInfoType to set
	 */
	public void setPersonInfoType(String personInfoType) {
		this.personInfoType = personInfoType;
	}

	/**
	 * Gets the projectName
	 * 
	 * @return Returns a String
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Sets the projectName
	 * 
	 * @param projectName
	 *            The projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Gets the streetNumber
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the streetNumber
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Gets the streetId
	 * 
	 * @return Returns a String
	 */
	public String getStreetId() {
		return streetId;
	}

	/**
	 * Sets the streetId
	 * 
	 * @param streetId
	 *            The streetId to set
	 */
	public void setStreetId(String streetId) {
		this.streetId = streetId;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		activityNumber = null;
		projectTeam = null;
		streetNumber = null;
		streetName = null;
		projectNumber = null;
		streetFraction = null;
		subProjectSubType = null;
		subProjectType = null;
		personInfoType = null;
		activityType = null;
		activityStatus = null;
		activitySubType = null;
		projectType = null;
		projectName = null;
		streetId = null;
		allInspectors = false;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		// delcare validations for the form

		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the projectType
	 * 
	 * @return Returns a String
	 */
	public String getProjectType() {
		return projectType;
	}

	/**
	 * Sets the projectType
	 * 
	 * @param projectType
	 *            The projectType to set
	 */
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * Gets the personType
	 * 
	 * @return Returns a String
	 */
	public String getPersonType() {
		return personType;
	}

	/**
	 * Sets the personType
	 * 
	 * @param personType
	 *            The personType to set
	 */
	public void setPersonType(String personType) {
		this.personType = personType;
	}

	/**
	 * Gets the lastName
	 * 
	 * @return Returns a String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName
	 * 
	 * @param lastName
	 *            The lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the peopleName
	 * 
	 * @return Returns a String
	 */
	public String getPeopleName() {
		return peopleName;
	}

	/**
	 * Sets the peopleName
	 * 
	 * @param peopleName
	 *            The peopleName to set
	 */
	public void setPeopleName(String peopleName) {
		this.peopleName = peopleName;
	}

	/**
	 * Gets the peopleLic
	 * 
	 * @return Returns a String
	 */
	public String getPeopleLic() {
		return peopleLic;
	}

	/**
	 * Sets the peopleLic
	 * 
	 * @param peopleLic
	 *            The peopleLic to set
	 */
	public void setPeopleLic(String peopleLic) {
		this.peopleLic = peopleLic;
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a String[]
	 */
	public String[] getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String[] activityType) {
		this.activityType = activityType;
	}

	/**
	 * Gets the activityStatus
	 * 
	 * @return Returns a String[]
	 */
	public String[] getActivityStatus() {
		return activityStatus;
	}

	/**
	 * Sets the activityStatus
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityStatus(String[] activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * Gets the ownerName
	 * 
	 * @return Returns a String
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * Sets the ownerName
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Returns the unitNbr.
	 * 
	 * @return String
	 */
	public String getUnitNbr() {
		return unitNbr;
	}

	/**
	 * Sets the unitNbr.
	 * 
	 * @param unitNbr
	 *            The unitNbr to set
	 */
	public void setUnitNbr(String unitNbr) {
		this.unitNbr = unitNbr;
	}

	/**
	 * Returns the activityList.
	 * 
	 * @return List
	 */
	public List getActivityList() {
		return activityList;
	}

	/**
	 * Sets the activityList.
	 * 
	 * @param activityList
	 *            The activityList to set
	 */
	public void setActivityList(List activityList) {
		this.activityList = activityList;
	}

	/**
	 * Returns the userList.
	 * 
	 * @return List
	 */
	public List getUserList() {
		return userList;
	}

	/**
	 * Sets the userList.
	 * 
	 * @param userList
	 *            The userList to set
	 */
	public void setUserList(List userList) {
		this.userList = userList;
	}

	/**
	 * Returns the name.
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the activityListSize.
	 * 
	 * @return String
	 */
	public String getActivityListSize() {
		return activityListSize;
	}

	/**
	 * Sets the activityListSize.
	 * 
	 * @param activityListSize
	 *            The activityListSize to set
	 */
	public void setActivityListSize(String activityListSize) {
		this.activityListSize = activityListSize;
	}

	/**
	 * Returns the activityId.
	 * 
	 * @return int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Returns the lsoId.
	 * 
	 * @return int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Returns the projectId.
	 * 
	 * @return int
	 */
	public int getProjectId() {
		return projectId;
	}

	/**
	 * Sets the activityId.
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * Sets the lsoId.
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Sets the projectId.
	 * 
	 * @param projectId
	 *            The projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	/**
	 * Returns the activityColumns.
	 * 
	 * @return String
	 */
	public String getActivityColumns() {
		return activityColumns;
	}

	/**
	 * Returns the activityOperators.
	 * 
	 * @return String
	 */
	public String getActivityOperators() {
		return activityOperators;
	}

	/**
	 * Returns the activityValue.
	 * 
	 * @return String
	 */
	public String getActivityValue() {
		return activityValue;
	}

	/**
	 * Returns the add.
	 * 
	 * @return String
	 */
	public String getAdd() {
		return add;
	}

	/**
	 * Returns the and.
	 * 
	 * @return String
	 */
	public String getAnd() {
		return and;
	}

	/**
	 * Returns the brackets.
	 * 
	 * @return String
	 */
	public String getBrackets() {
		return brackets;
	}

	/**
	 * Returns the cancel.
	 * 
	 * @return String
	 */
	public String getCancel() {
		return cancel;
	}

	/**
	 * Returns the inspectionColumns.
	 * 
	 * @return String
	 */
	public String getInspectionColumns() {
		return inspectionColumns;
	}

	/**
	 * Returns the inspectionOperators.
	 * 
	 * @return String
	 */
	public String getInspectionOperators() {
		return inspectionOperators;
	}

	/**
	 * Returns the inspectionValue.
	 * 
	 * @return String
	 */
	public String getInspectionValue() {
		return inspectionValue;
	}

	/**
	 * Returns the not.
	 * 
	 * @return String
	 */
	public String getNot() {
		return not;
	}

	/**
	 * Returns the or.
	 * 
	 * @return String
	 */
	public String getOr() {
		return or;
	}

	/**
	 * Returns the projectColumns.
	 * 
	 * @return String
	 */
	public String getProjectColumns() {
		return projectColumns;
	}

	/**
	 * Returns the projectOperators.
	 * 
	 * @return String
	 */
	public String getProjectOperators() {
		return projectOperators;
	}

	/**
	 * Returns the projectValue.
	 * 
	 * @return String
	 */
	public String getProjectValue() {
		return projectValue;
	}

	/**
	 * Returns the query.
	 * 
	 * @return String
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Returns the search.
	 * 
	 * @return String
	 */
	public String getSearch() {
		return search;
	}

	/**
	 * Returns the table.
	 * 
	 * @return String
	 */
	public String getTable() {
		return table;
	}

	/**
	 * Sets the activityColumns.
	 * 
	 * @param activityColumns
	 *            The activityColumns to set
	 */
	public void setActivityColumns(String activityColumns) {
		this.activityColumns = activityColumns;
	}

	/**
	 * Sets the activityOperators.
	 * 
	 * @param activityOperators
	 *            The activityOperators to set
	 */
	public void setActivityOperators(String activityOperators) {
		this.activityOperators = activityOperators;
	}

	/**
	 * Sets the activityValue.
	 * 
	 * @param activityValue
	 *            The activityValue to set
	 */
	public void setActivityValue(String activityValue) {
		this.activityValue = activityValue;
	}

	/**
	 * Sets the add.
	 * 
	 * @param add
	 *            The add to set
	 */
	public void setAdd(String add) {
		this.add = add;
	}

	/**
	 * Sets the and.
	 * 
	 * @param and
	 *            The and to set
	 */
	public void setAnd(String and) {
		this.and = and;
	}

	/**
	 * Sets the brackets.
	 * 
	 * @param brackets
	 *            The brackets to set
	 */
	public void setBrackets(String brackets) {
		this.brackets = brackets;
	}

	/**
	 * Sets the cancel.
	 * 
	 * @param cancel
	 *            The cancel to set
	 */
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	/**
	 * Sets the inspectionColumns.
	 * 
	 * @param inspectionColumns
	 *            The inspectionColumns to set
	 */
	public void setInspectionColumns(String inspectionColumns) {
		this.inspectionColumns = inspectionColumns;
	}

	/**
	 * Sets the inspectionOperators.
	 * 
	 * @param inspectionOperators
	 *            The inspectionOperators to set
	 */
	public void setInspectionOperators(String inspectionOperators) {
		this.inspectionOperators = inspectionOperators;
	}

	/**
	 * Sets the inspectionValue.
	 * 
	 * @param inspectionValue
	 *            The inspectionValue to set
	 */
	public void setInspectionValue(String inspectionValue) {
		this.inspectionValue = inspectionValue;
	}

	/**
	 * Sets the not.
	 * 
	 * @param not
	 *            The not to set
	 */
	public void setNot(String not) {
		this.not = not;
	}

	/**
	 * Sets the or.
	 * 
	 * @param or
	 *            The or to set
	 */
	public void setOr(String or) {
		this.or = or;
	}

	/**
	 * Sets the projectColumns.
	 * 
	 * @param projectColumns
	 *            The projectColumns to set
	 */
	public void setProjectColumns(String projectColumns) {
		this.projectColumns = projectColumns;
	}

	/**
	 * Sets the projectOperators.
	 * 
	 * @param projectOperators
	 *            The projectOperators to set
	 */
	public void setProjectOperators(String projectOperators) {
		this.projectOperators = projectOperators;
	}

	/**
	 * Sets the projectValue.
	 * 
	 * @param projectValue
	 *            The projectValue to set
	 */
	public void setProjectValue(String projectValue) {
		this.projectValue = projectValue;
	}

	/**
	 * Sets the query.
	 * 
	 * @param query
	 *            The query to set
	 */
	public void setQuery(String query) {
		this.query = query;
	}

	/**
	 * Sets the search.
	 * 
	 * @param search
	 *            The search to set
	 */
	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * Sets the table.
	 * 
	 * @param table
	 *            The table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * Returns the streetName1.
	 * 
	 * @return String
	 */
	public String getStreetName1() {
		return streetName1;
	}

	/**
	 * Sets the streetName1.
	 * 
	 * @param streetName1
	 *            The streetName1 to set
	 */
	public void setStreetName1(String streetName1) {
		this.streetName1 = streetName1;
	}

	/**
	 * @return Returns the streetName2.
	 */
	public String getStreetName2() {
		return streetName2;
	}

	/**
	 * @param streetName2
	 *            The streetName2 to set.
	 */
	public void setStreetName2(String streetName2) {
		this.streetName2 = streetName2;
	}

	/**
	 * Returns the zone.
	 * 
	 * @return String
	 */
	public String getZone() {
		return zone;
	}

	/**
	 * Sets the zone.
	 * 
	 * @param zone
	 *            The zone to set
	 */
	public void setZone(String zone) {
		this.zone = zone;
	}

	/**
	 * Returns the inspector.
	 * 
	 * @return String
	 */
	public String getInspector() {
		return inspector;
	}

	/**
	 * Sets the inspector.
	 * 
	 * @param inspector
	 *            The inspector to set
	 */
	public void setInspector(String inspector) {
		this.inspector = inspector;
	}

	/**
	 * Returns the activityDate.
	 * 
	 * @return String
	 */
	public String getActivityDate() {
		return activityDate;
	}

	/**
	 * Returns the fromDate.
	 * 
	 * @return String
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * Returns the toDate.
	 * 
	 * @return String
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * Sets the activityDate.
	 * 
	 * @param activityDate
	 *            The activityDate to set
	 */
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	/**
	 * Sets the fromDate.
	 * 
	 * @param fromDate
	 *            The fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * Sets the toDate.
	 * 
	 * @param toDate
	 *            The toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * Returns the allInspectors.
	 * 
	 * @return boolean
	 */
	public boolean getAllInspectors() {
		return allInspectors;
	}

	/**
	 * Sets the allInspectors.
	 * 
	 * @param allInspectors
	 *            The allInspectors to set
	 */
	public void setAllInspectors(boolean allInspectors) {
		this.allInspectors = allInspectors;
	}

	/**
	 * @return
	 */
	public String getSubProjectNumber() {
		return subProjectNumber;
	}

	/**
	 * @param string
	 */
	public void setSubProjectNumber(String string) {
		subProjectNumber = string;
	}

	public void setForeignAddressLine1(String string) {
		foreignAddressLine1 = string;
	}

	public String getForeignAddressLine1() {
		return foreignAddressLine1;
	}

	public void setForeignAddressLine2(String string) {
		foreignAddressLine2 = string;
	}

	public String getForeignAddressLine2() {
		return foreignAddressLine2;
	}

	public void setForeignAddressLine3(String string) {
		foreignAddressLine3 = string;
	}

	public String getForeignAddressLine3() {
		return foreignAddressLine3;
	}

	public void setForeignAddressLine4(String string) {
		foreignAddressLine4 = string;
	}

	public String getForeignAddressLine4() {
		return foreignAddressLine4;
	}

	public void setForeignAddressState(String string) {
		foreignAddressState = string;
	}

	public String getForeignAddressState() {
		return foreignAddressState;
	}

	public void setForeignAddressCountry(String string) {
		foreignAddressCountry = string;
	}

	public String getForeignAddressCountry() {
		return foreignAddressCountry;
	}

	public void setForeignAddressZip(String string) {
		foreignAddressZip = string;
	}

	public String getForeignAddressZip() {
		return foreignAddressZip;
	}

	/**
	 * @return
	 */

	// Added for Business Search on 08/13/2007 by Aman

	/**
	 * Gets the Business Account Number
	 * 
	 * @return Returns a String
	 */
	public String getBusinessAcNumber() {
		return businessAcNumber;
	}

	/**
	 * Sets the Business Account Number
	 */
	public void setBusinessAcNumber(String businessAcNumber) {
		this.businessAcNumber = businessAcNumber;
	}

	/**
	 * Gets the Business Name
	 * 
	 * @return Returns a String
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * Sets the Business Name
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * Gets the Business Owners Name
	 * 
	 * @return Returns a String
	 */
	public String getBusinessOwnerName() {
		return businessOwnerName;
	}

	/**
	 * Sets the Business Owners Name
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setBusinessOwnerName(String businessOwnerName) {
		this.businessOwnerName = businessOwnerName;
	}

	public void setOutofTownAddress1(String string) {
		outofTownAddress1 = string;
	}

	public String getOutofTownAddress1() {
		return outofTownAddress1;
	}

	public void setOutofTownAddress2(String string) {
		outofTownAddress2 = string;
	}

	public String getOutofTownAddress2() {
		return outofTownAddress2;
	}

	public void setOutofTownAddress3(String string) {
		outofTownAddress3 = string;
	}

	public String getOutofTownAddress3() {
		return outofTownAddress3;
	}

	public void setOutofTownAddress4(String string) {
		outofTownAddress4 = string;
	}

	public String getOutofTownAddress4() {
		return outofTownAddress4;
	}

	public void setOutofTownAddressState(String string) {
		outofTownAddressState = string;
	}

	public String getOutofTownAddressState() {
		return outofTownAddressState;
	}

	public void setOutofTownAddressCountry(String string) {
		outofTownAddressCountry = string;
	}

	public String getOutofTownAddressCountry() {
		return outofTownAddressCountry;
	}

	public void setOutofTownAddressZip(String string) {
		outofTownAddressZip = string;
	}

	public String getOutofTownAddressZip() {
		return outofTownAddressZip;
	}

	public String[] getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param strings
	 */
	public void setActivitySubType(String[] strings) {
		activitySubType = strings;
	}

	/**
	 * @return Returns the owner1.
	 */
	public String getOwner1() {
		return owner1;
	}

	/**
	 * @param owner1
	 *            The owner1 to set.
	 */
	public void setOwner1(String owner1) {
		this.owner1 = owner1;
	}

	/**
	 * @return Returns the owner2.
	 */
	public String getOwner2() {
		return owner2;
	}

	/**
	 * @param owner2
	 *            The owner2 to set.
	 */
	public void setOwner2(String owner2) {
		this.owner2 = owner2;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
}

// End class

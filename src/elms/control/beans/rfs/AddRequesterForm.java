/*
 * Created on Mar 20, 2008
 * @author Akash
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.rfs;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import elms.util.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AddRequesterForm extends ActionForm{
	protected int requesterid;
	protected String action;
	protected String requestsource;
	protected boolean contact;
	protected boolean cityemployee;
	protected String requesterdepartment;
	protected String lastname="";
	protected String firstname="";
	protected String requesterstreetnumber="";
	protected String requesterstreetname;
	protected String requesterunit="";
	protected String requesterzip="91202";
	protected String requestercity="Glendale";
	protected String requesterstate="CA";
	protected String phonenumber="";
	protected String extension="";
	protected String altphonenumber="";
	protected String altextension="";
	protected String email="";
	protected String clear;
		
	protected List incidentList=new ArrayList();
	protected addIncidentForm[] incidentArray=(addIncidentForm[]) incidentList.toArray(new addIncidentForm[incidentList.size()]);
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
    }

    public void reset() {
    	incidentList.clear();
    	incidentArray = (addIncidentForm[]) incidentList.toArray(new addIncidentForm[incidentList.size()]);
    	requestsource = "";
    	/*contact ="";
    	cityemployee="";*/
    	contact =false;
    	cityemployee=false;
    	requesterdepartment="";
    	lastname="";
    	firstname="";
    	requesterstreetnumber="";
    	requesterstreetname="";
    	requesterunit="";
    	requesterzip="91202";
    	requestercity="Glendale";
    	requesterstate="CA";
    	phonenumber="";
    	extension="";
    	altphonenumber="";
    	altextension="";
    	email="";
    	clear="";
    }
		
	/**
	 * @return Returns the altextension.
	 */
	public String getAltextension() {
		return altextension;
	}
	/**
	 * @param altextension The altextension to set.
	 */
	public void setAltextension(String altextension) {
		this.altextension = altextension;
	}
	/**
	 * @return Returns the altphonenumber.
	 */
	public String getAltphonenumber() {
		return altphonenumber;
	}
	/**
	 * @param altphonenumber The altphonenumber to set.
	 */
	public void setAltphonenumber(String altphonenumber) {
		this.altphonenumber = altphonenumber;
	}
	
	/**
	 * @return Returns the cityemployee.
	 *//*
	public String getCityemployee() {
		return cityemployee;
	}
	*//**
	 * @param cityemployee The cityemployee to set.
	 *//*
	public void setCityemployee(String cityemployee) {
		this.cityemployee = cityemployee;
	}
	*//**
	 * @return Returns the contact.
	 *//*
	public String getContact() {
		return contact;
	}
	*//**
	 * @param contact The contact to set.
	 *//*
	public void setContact(String contact) {
		this.contact = contact;
	}
	*/
	
	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return Returns the extension.
	 */
	public String getExtension() {
		return extension;
	}
	/**
	 * @param extension The extension to set.
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}
	/**
	 * @return Returns the firstname.
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname The firstname to set.
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	
	/**
	 * @return Returns the lastname.
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname The lastname to set.
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	/**
	 * @return Returns the phonenumber.
	 */
	public String getPhonenumber() {
		return phonenumber;
	}
	/**
	 * @param phonenumber The phonenumber to set.
	 */
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	
	/**
	 * @return Returns the requesterdepartment.
	 */
	public String getRequesterdepartment() {
		return requesterdepartment;
	}
	/**
	 * @param requesterdepartment The requesterdepartment to set.
	 */
	public void setRequesterdepartment(String requesterdepartment) {
		this.requesterdepartment = requesterdepartment;
	}
	/**
	 * @return Returns the requesterid.
	 */
	public int getRequesterid() {
		return requesterid;
	}
	/**
	 * @param requesterid The requesterid to set.
	 */
	public void setRequesterid(int requesterid) {
		this.requesterid = requesterid;
	}
	/**
	 * @return Returns the requesterstreetname.
	 */
	public String getRequesterstreetname() {
		return requesterstreetname;
	}
	/**
	 * @param requesterstreetname The requesterstreetname to set.
	 */
	public void setRequesterstreetname(String requesterstreetname) {
		this.requesterstreetname = requesterstreetname;
	}
	/**
	 * @return Returns the requesterstreetnumber.
	 */
	public String getRequesterstreetnumber() {
		return requesterstreetnumber;
	}
	/**
	 * @param requesterstreetnumber The requesterstreetnumber to set.
	 */
	public void setRequesterstreetnumber(String requesterstreetnumber) {
		this.requesterstreetnumber = requesterstreetnumber;
	}
	/**
	 * @return Returns the requesterunit.
	 */
	public String getRequesterunit() {
		return requesterunit;
	}
	/**
	 * @param requesterunit The requesterunit to set.
	 */
	public void setRequesterunit(String requesterunit) {
		this.requesterunit = requesterunit;
	}
	/**
	 * @return Returns the requestsource.
	 */
	public String getRequestsource() {
		return requestsource;
	}
	/**
	 * @param requestsource The requestsource to set.
	 */
	public void setRequestsource(String requestsource) {
		this.requestsource = requestsource;
	}
	
	/**
	 * @return Returns the requestercity.
	 */
	public String getRequestercity() {
		return requestercity;
	}
	/**
	 * @param requestercity The requestercity to set.
	 */
	public void setRequestercity(String requestercity) {
		this.requestercity = requestercity;
	}
	/**
	 * @return Returns the requesterstate.
	 */
	public String getRequesterstate() {
		return requesterstate;
	}
	/**
	 * @param requesterstate The requesterstate to set.
	 */
	public void setRequesterstate(String requesterstate) {
		this.requesterstate = requesterstate;
	}
	/**
	 * @return Returns the requesterzip.
	 */
	public String getRequesterzip() {
		return requesterzip;
	}
	/**
	 * @param requesterzip The requesterzip to set.
	 */
	public void setRequesterzip(String requesterzip) {
		this.requesterzip = requesterzip;
	}
	/**
	 * @return Returns the incidentArray.
	 */
	public addIncidentForm[] getIncidentArray() {
		return incidentArray;
	}
	/**
	 * @param incidentArray The incidentArray to set.
	 */
	public void setIncidentArray(addIncidentForm[] incidentArray) {
		this.incidentArray = incidentArray;
		incidentList = StringUtils.arrayToList(incidentArray);
	}
	/**
	 * @return Returns the incidentList.
	 */
	public List getIncidentList() {
		return incidentList;
	}
	/**
	 * @param incidentList The incidentList to set.
	 */
	public void setIncidentList(List incidentList) {
		this.incidentList = incidentList;
		incidentArray = (addIncidentForm[]) incidentList.toArray();
	}
	/**
	 * @return Returns the action.
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action The action to set.
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
     * @param list
     */
    public void addIncidentList(Object o) {
        incidentList.add(o);
        incidentArray = (addIncidentForm[]) incidentList.toArray(new addIncidentForm[incidentList.size()]);
    }
    public void removeIncidentList(Object o) {
        incidentList.remove(o);
        incidentArray = (addIncidentForm[]) incidentList.toArray(new addIncidentForm[incidentList.size()]);
    }
	/**
	 * @return Returns the cityemployee.
	 */
	public boolean isCityemployee() {
		return cityemployee;
	}
	/**
	 * @param cityemployee The cityemployee to set.
	 */
	public void setCityemployee(boolean cityemployee) {
		this.cityemployee = cityemployee;
	}
	/**
	 * @return Returns the contact.
	 */
	public boolean isContact() {
		return contact;
	}
	/**
	 * @param contact The contact to set.
	 */
	public void setContact(boolean contact) {
		this.contact = contact;
	}
}

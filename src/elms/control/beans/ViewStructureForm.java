package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ViewStructureForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String apn;
	protected String lsoId;

	protected String addressStreetNumber;
	protected String addressStreetModifier;
	protected String addressStreetName;
	protected String addressCity;
	protected String addressState;
	protected String addressUnit;
	protected String addressZip;
	protected String addressZip4;
	protected String addressPrimary;
	protected String addressActive;
	protected String addressDescription;

	protected String apnOwnerId;
	protected String apnOwnerName;
	protected String apnStreetNumber;
	protected String apnStreetModifier;
	protected String apnStreetName;
	protected String apnCity;
	protected String apnState;
	protected String apnZip;
	protected String apnZip4;
	protected String apnEmail;
	protected String apnPhone;
	protected String apnFax;
	protected String apnforeignflag = "N";
	protected String apnForeignAddress1;
	protected String apnForeignAddress2;
	protected String apnForeignAddress3;
	protected String apnForeignAddress4;
	protected String apnCountry;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the addressStreetNumber
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetNumber() {
		return addressStreetNumber;
	}

	/**
	 * Sets the addressStreetNumber
	 * 
	 * @param addressStreetNumber
	 *            The addressStreetNumber to set
	 */
	public void setAddressStreetNumber(String addressStreetNumber) {
		this.addressStreetNumber = addressStreetNumber;
	}

	/**
	 * Gets the addressStreetModifier
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetModifier() {
		return addressStreetModifier;
	}

	/**
	 * Sets the addressStreetModifier
	 * 
	 * @param addressStreetModifier
	 *            The addressStreetModifier to set
	 */
	public void setAddressStreetModifier(String addressStreetModifier) {
		this.addressStreetModifier = addressStreetModifier;
	}

	/**
	 * Gets the addressStreetName
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetName() {
		return addressStreetName;
	}

	/**
	 * Sets the addressStreetName
	 * 
	 * @param addressStreetName
	 *            The addressStreetName to set
	 */
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}

	/**
	 * Gets the addressCity
	 * 
	 * @return Returns a String
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * Sets the addressCity
	 * 
	 * @param addressCity
	 *            The addressCity to set
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * Gets the addressState
	 * 
	 * @return Returns a String
	 */
	public String getAddressState() {
		return addressState;
	}

	/**
	 * Sets the addressState
	 * 
	 * @param addressState
	 *            The addressState to set
	 */
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	/**
	 * Gets the addressUnit
	 * 
	 * @return Returns a String
	 */
	public String getAddressUnit() {
		return addressUnit;
	}

	/**
	 * Sets the addressUnit
	 * 
	 * @param addressUnit
	 *            The addressUnit to set
	 */
	public void setAddressUnit(String addressUnit) {
		this.addressUnit = addressUnit;
	}

	/**
	 * Gets the addressZip
	 * 
	 * @return Returns a String
	 */
	public String getAddressZip() {
		return addressZip;
	}

	/**
	 * Sets the addressZip
	 * 
	 * @param addressZip
	 *            The addressZip to set
	 */
	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}

	/**
	 * Gets the addressZip4
	 * 
	 * @return Returns a String
	 */
	public String getAddressZip4() {
		return addressZip4;
	}

	/**
	 * Sets the addressZip4
	 * 
	 * @param addressZip4
	 *            The addressZip4 to set
	 */
	public void setAddressZip4(String addressZip4) {
		this.addressZip4 = addressZip4;
	}

	/**
	 * Gets the addressPrimary
	 * 
	 * @return Returns a String
	 */
	public String getAddressPrimary() {
		return addressPrimary;
	}

	/**
	 * Sets the addressPrimary
	 * 
	 * @param addressPrimary
	 *            The addressPrimary to set
	 */
	public void setAddressPrimary(String addressPrimary) {
		this.addressPrimary = addressPrimary;
	}

	/**
	 * Gets the addressActive
	 * 
	 * @return Returns a String
	 */
	public String getAddressActive() {
		return addressActive;
	}

	/**
	 * Sets the addressActive
	 * 
	 * @param addressActive
	 *            The addressActive to set
	 */
	public void setAddressActive(String addressActive) {
		this.addressActive = addressActive;
	}

	/**
	 * Gets the addressDescription
	 * 
	 * @return Returns a String
	 */
	public String getAddressDescription() {
		return addressDescription;
	}

	/**
	 * Sets the addressDescription
	 * 
	 * @param addressDescription
	 *            The addressDescription to set
	 */
	public void setAddressDescription(String addressDescription) {
		this.addressDescription = addressDescription;
	}

	/**
	 * @return
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * @return
	 */
	public String getApnCity() {
		return apnCity;
	}

	/**
	 * @return
	 */
	public String getApnCountry() {
		return apnCountry;
	}

	/**
	 * @return
	 */
	public String getApnEmail() {
		return apnEmail;
	}

	/**
	 * @return
	 */
	public String getApnFax() {
		return apnFax;
	}

	/**
	 * @return
	 */
	public String getApnForeignAddress1() {
		return apnForeignAddress1;
	}

	/**
	 * @return
	 */
	public String getApnForeignAddress2() {
		return apnForeignAddress2;
	}

	/**
	 * @return
	 */
	public String getApnForeignAddress3() {
		return apnForeignAddress3;
	}

	/**
	 * @return
	 */
	public String getApnForeignAddress4() {
		return apnForeignAddress4;
	}

	/**
	 * @return
	 */
	public String getApnforeignflag() {
		return apnforeignflag;
	}

	/**
	 * @return
	 */
	public String getApnOwnerId() {
		return apnOwnerId;
	}

	/**
	 * @return
	 */
	public String getApnOwnerName() {
		return apnOwnerName;
	}

	/**
	 * @return
	 */
	public String getApnPhone() {
		return apnPhone;
	}

	/**
	 * @return
	 */
	public String getApnState() {
		return apnState;
	}

	/**
	 * @return
	 */
	public String getApnStreetModifier() {
		return apnStreetModifier;
	}

	/**
	 * @return
	 */
	public String getApnStreetName() {
		return apnStreetName;
	}

	/**
	 * @return
	 */
	public String getApnStreetNumber() {
		return apnStreetNumber;
	}

	/**
	 * @return
	 */
	public String getApnZip() {
		return apnZip;
	}

	/**
	 * @return
	 */
	public String getApnZip4() {
		return apnZip4;
	}

	/**
	 * @param string
	 */
	public void setApn(String string) {
		apn = string;
	}

	/**
	 * @param string
	 */
	public void setApnCity(String string) {
		apnCity = string;
	}

	/**
	 * @param string
	 */
	public void setApnCountry(String string) {
		apnCountry = string;
	}

	/**
	 * @param string
	 */
	public void setApnEmail(String string) {
		apnEmail = string;
	}

	/**
	 * @param string
	 */
	public void setApnFax(String string) {
		apnFax = string;
	}

	/**
	 * @param string
	 */
	public void setApnForeignAddress1(String string) {
		apnForeignAddress1 = string;
	}

	/**
	 * @param string
	 */
	public void setApnForeignAddress2(String string) {
		apnForeignAddress2 = string;
	}

	/**
	 * @param string
	 */
	public void setApnForeignAddress3(String string) {
		apnForeignAddress3 = string;
	}

	/**
	 * @param string
	 */
	public void setApnForeignAddress4(String string) {
		apnForeignAddress4 = string;
	}

	/**
	 * @param string
	 */
	public void setApnforeignflag(String string) {
		apnforeignflag = string;
	}

	/**
	 * @param string
	 */
	public void setApnOwnerId(String string) {
		apnOwnerId = string;
	}

	/**
	 * @param string
	 */
	public void setApnOwnerName(String string) {
		apnOwnerName = string;
	}

	/**
	 * @param string
	 */
	public void setApnPhone(String string) {
		apnPhone = string;
	}

	/**
	 * @param string
	 */
	public void setApnState(String string) {
		apnState = string;
	}

	/**
	 * @param string
	 */
	public void setApnStreetModifier(String string) {
		apnStreetModifier = string;
	}

	/**
	 * @param string
	 */
	public void setApnStreetName(String string) {
		apnStreetName = string;
	}

	/**
	 * @param string
	 */
	public void setApnStreetNumber(String string) {
		apnStreetNumber = string;
	}

	/**
	 * @param string
	 */
	public void setApnZip(String string) {
		apnZip = string;
	}

	/**
	 * @param string
	 */
	public void setApnZip4(String string) {
		apnZip4 = string;
	}

} // End class

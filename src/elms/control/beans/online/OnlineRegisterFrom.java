package elms.control.beans.online;

import java.util.Calendar;
import java.util.Date;

import org.apache.struts.action.ActionForm;

public class OnlineRegisterFrom extends ActionForm {

	/**
	 * Member variable declaration
	 */
	protected int userId;
	protected int accountNo;
	protected String firstName;
	protected String lastName;
	protected String emailAddress;
	protected String pwd;
	protected String confirmPwd;
	protected String address;
	protected String phoneNbr;
	protected String streetNumber;
	protected String streetId;
	protected String streetFraction;
	protected String unitNumber;
	protected String streetName;
	protected String licenseNbr = null;
	protected String licExpDate = null;
	protected String obc;
	protected String dot;
	protected String peopleType;
	public Calendar licenseExpires = Calendar.getInstance();
	protected String companyName;
	protected String phoneExt;
	protected String workPhone;
	protected String workExt;
	protected String fax;
	protected String city;
	protected String state;
	protected String zip;
	protected String strWorkersCompExpires;
	protected String workersCompensationWaive;
	protected String message;
	protected String vehicleNo;
	protected String dlNo;
	protected String householdIncome;
	

	/**
	 * @return
	 */
	public int getAccountNo() {
		return accountNo;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return
	 */
	public String getConfirmPwd() {
		return confirmPwd;
	}

	/**
	 * @return
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return
	 */
	public String getPhoneNbr() {
		return phoneNbr;
	}

	/**
	 * @return
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * @param i
	 */
	public void setAccountNo(int i) {
		accountNo = i;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @param string
	 */
	public void setConfirmPwd(String string) {
		confirmPwd = string;
	}

	/**
	 * @param string
	 */
	public void setEmailAddress(String string) {
		emailAddress = string;
	}

	/**
	 * @param string
	 */
	public void setFirstName(String string) {
		firstName = string;
	}

	/**
	 * @param string
	 */
	public void setLastName(String string) {
		lastName = string;
	}

	/**
	 * @param string
	 */
	public void setPhoneNbr(String string) {
		phoneNbr = string;
	}

	/**
	 * @param string
	 */
	public void setPwd(String string) {
		pwd = string;
	}

	/**
	 * @return
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * @return
	 */
	public String getStreetId() {
		return streetId;
	}

	/**
	 * @return
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @return
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * @param string
	 */
	public void setStreetFraction(String string) {
		streetFraction = string;
	}

	/**
	 * @param string
	 */
	public void setStreetId(String string) {
		streetId = string;
	}

	/**
	 * @param string
	 */
	public void setStreetNumber(String string) {
		streetNumber = string;
	}

	/**
	 * @param string
	 */
	public void setUnitNumber(String string) {
		unitNumber = string;
	}

	/**
	 * @return
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * @param string
	 */
	public void setStreetName(String string) {
		streetName = string;
	}

	/**
	 * @return
	 */
	public String getLicenseNbr() {
		return licenseNbr;
	}

	/**
	 * @return
	 */
	public String getLicExpDate() {
		return licExpDate;
	}

	/**
	 * @param string
	 */
	public void setLicenseNbr(String string) {
		licenseNbr = string;
	}

	/**
	 * @param string
	 */
	public void setLicExpDate(String string) {
		licExpDate = string;
	}

	/**
	 * @return
	 */
	public String getDot() {
		return dot;
	}

	/**
	 * @return
	 */
	public String getObc() {
		return obc;
	}

	/**
	 * @param string
	 */
	public void setDot(String string) {
		dot = string;
	}

	/**
	 * @param string
	 */
	public void setObc(String string) {
		obc = string;
	}

	/**
	 * @return
	 */
	public String getPeopleType() {
		return peopleType;
	}

	/**
	 * @param string
	 */
	public void setPeopleType(String string) {
		peopleType = string;
	}

	/**
	 * @return
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param i
	 */
	public void setUserId(int i) {
		userId = i;
	}

	/**
	 * @return
	 */
	public Calendar getLicenseExpires() {
		return licenseExpires;
	}

	/**
	 * @param calendar
	 */
	public void setLicenseExpires(Calendar calendar) {
		licenseExpires = calendar;
	}

	public void setLicenseExpires(Date aLicenseExpires) {
		if (aLicenseExpires != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aLicenseExpires);
			this.licenseExpires = cal;
		}
	}

	/**
	 * @return
	 */
	public String getCompanyName() {
		return companyName;
	}

	/**
	 * @param string
	 */
	public void setCompanyName(String string) {
		companyName = string;
	}

	/**
	 * @return
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @return
	 */
	public String getPhoneExt() {
		return phoneExt;
	}

	/**
	 * @return
	 */
	public String getWorkExt() {
		return workExt;
	}

	/**
	 * @return
	 */
	public String getWorkPhone() {
		return workPhone;
	}

	/**
	 * @param string
	 */
	public void setFax(String string) {
		fax = string;
	}

	/**
	 * @param string
	 */
	public void setPhoneExt(String string) {
		phoneExt = string;
	}

	/**
	 * @param string
	 */
	public void setWorkExt(String string) {
		workExt = string;
	}

	/**
	 * @param string
	 */
	public void setWorkPhone(String string) {
		workPhone = string;
	}

	/**
	 * @return Returns the city.
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            The city to set.
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return Returns the state.
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            The state to set.
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return Returns the zip.
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 *            The zip to set.
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return Returns the strWorkersCompExpires.
	 */
	public String getStrWorkersCompExpires() {
		return strWorkersCompExpires;
	}

	/**
	 * @param strWorkersCompExpires
	 *            The strWorkersCompExpires to set.
	 */
	public void setStrWorkersCompExpires(String strWorkersCompExpires) {
		this.strWorkersCompExpires = strWorkersCompExpires;
	}

	/**
	 * @return Returns the workersCompensationWaive.
	 */
	public String getWorkersCompensationWaive() {
		return workersCompensationWaive;
	}

	/**
	 * @param workersCompensationWaive
	 *            The workersCompensationWaive to set.
	 */
	public void setWorkersCompensationWaive(String workersCompensationWaive) {
		this.workersCompensationWaive = workersCompensationWaive;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getDlNo() {
		return dlNo;
	}

	public void setDlNo(String dlNo) {
		this.dlNo = dlNo;
	}

	/**
	 * @return the householdIncome
	 */
	public String getHouseholdIncome() {
		return householdIncome;
	}

	/**
	 * @param householdIncome the householdIncome to set
	 */
	public void setHouseholdIncome(String householdIncome) {
		this.householdIncome = householdIncome;
	}
}

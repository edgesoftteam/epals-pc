/**
 * 
 */
package elms.control.beans.online;

import org.apache.struts.action.ActionForm;

/**
 * @author Gayathri Turlapati
 *
 */
public class EmailTemplateAdminForm  extends ActionForm{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3971553402898073260L;
	
	private int emailTempId; //EMAIL_TEMPLATE_ID
	private int emailTempTypeId; // EMAIL_TEMP_TYPE_ID

	private int refEmailTempId;//REF_EMAIL_TEMP_ID
	private String title; // EMAIL_TEMP_TYPE
	
	private String emailSubject; // EMAIL_SUBJECT
	private String emailMessage; // EMAIL_BODY
	private int action;
	private String[] selectedItems = {};
	private String peopleManager;
	private String activityTeam;
	private String toAddress;
	/**
	 * @return the emailTempId
	 */
	public int getEmailTempId() {
		return emailTempId;
	}
	/**
	 * @param emailTempId the emailTempId to set
	 */
	public void setEmailTempId(int emailTempId) {
		this.emailTempId = emailTempId;
	}
	/**
	 * @return the emailTempTypeId
	 */
	public int getEmailTempTypeId() {
		return emailTempTypeId;
	}
	/**
	 * @param emailTempTypeId the emailTempTypeId to set
	 */
	public void setEmailTempTypeId(int emailTempTypeId) {
		this.emailTempTypeId = emailTempTypeId;
	}
	/**
	 * @return the refEmailTempId
	 */
	public int getRefEmailTempId() {
		return refEmailTempId;
	}
	/**
	 * @param refEmailTempId the refEmailTempId to set
	 */
	public void setRefEmailTempId(int refEmailTempId) {
		this.refEmailTempId = refEmailTempId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the subject
	 */
	public String getEmailSubject() {
		return emailSubject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setEmailSubject(String emailSubject) {
		this.emailSubject = emailSubject;
	}
	/**
	 * @return the message
	 */
	public String getEmailMessage() {
		return emailMessage;
	}
	/**
	 * @param message the message to set
	 */
	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}
	/**
	 * @return the action
	 */
	public int getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(int action) {
		this.action = action;
	}
	/**
	 * @return the selectedItems
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}
	/**
	 * @param selectedItems the selectedItems to set
	 */
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}
	/**
	 * @return the peopleManager
	 */
	public String getPeopleManager() {
		return peopleManager;
	}
	/**
	 * @param peopleManager the peopleManager to set
	 */
	public void setPeopleManager(String peopleManager) {
		this.peopleManager = peopleManager;
	}
	/**
	 * @return the activityTeam
	 */
	public String getActivityTeam() {
		return activityTeam;
	}
	/**
	 * @param activityTeam the activityTeam to set
	 */
	public void setActivityTeam(String activityTeam) {
		this.activityTeam = activityTeam;
	}
	/**
	 * @return the toAddress
	 */
	public String getToAddress() {
		return toAddress;
	}
	/**
	 * @param toAddress the toAddress to set
	 */
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	} 
}

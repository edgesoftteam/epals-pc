package elms.control.beans.online;

import org.apache.struts.action.ActionForm;

/**
 * @author Sunil
 */
public class MyPermitForm extends ActionForm {

	protected String activityNo;
	protected String activityType;
	protected String activityStatus;

	protected String activityDate;

	protected String address;
	protected String fullAddress;
	protected String activityId;
	protected String sprojId;
	protected String activityTypeCode;
	protected String print;
	protected String actSubType;
	protected String amount;

	/**
	 * 
	 */
	public MyPermitForm() {

		// TODO Auto-generated constructor stub
	}

	/**
	 * @return
	 */
	public String getActivityDate() {
		return activityDate;
	}

	/**
	 * @return
	 */
	public String getActivityNo() {
		return activityNo;
	}

	/**
	 * @return
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @return
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param string
	 */
	public void setActivityDate(String string) {
		activityDate = string;
	}

	/**
	 * @param string
	 */
	public void setActivityNo(String string) {
		activityNo = string;
	}

	/**
	 * @param string
	 */
	public void setActivityStatus(String string) {
		activityStatus = string;
	}

	/**
	 * @param string
	 */
	public void setActivityType(String string) {
		activityType = string;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @return
	 */
	public String getFullAddress() {
		return fullAddress;
	}

	/**
	 * @param string
	 */
	public void setFullAddress(String string) {
		fullAddress = string;
	}

	/**
	 * @return
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param string
	 */
	public void setActivityId(String string) {
		activityId = string;
	}

	/**
	 * @return
	 */
	public String getSprojId() {
		return sprojId;
	}

	/**
	 * @param string
	 */
	public void setSprojId(String string) {
		sprojId = string;
	}

	/**
	 * @return
	 */
	public String getActivityTypeCode() {
		return activityTypeCode;
	}

	/**
	 * @param string
	 */
	public void setActivityTypeCode(String string) {
		activityTypeCode = string;
	}

	/**
	 * @return
	 */
	public String getPrint() {
		return print;
	}

	/**
	 * @param string
	 */
	public void setPrint(String string) {
		print = string;
	}

	/**
	 * @return Returns the actSubType.
	 */
	public String getActSubType() {
		return actSubType;
	}

	/**
	 * @param actSubType
	 *            The actSubType to set.
	 */
	public void setActSubType(String actSubType) {
		this.actSubType = actSubType;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
}

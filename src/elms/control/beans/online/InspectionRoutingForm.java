package elms.control.beans.online;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Anand Belaguly
 */
public class InspectionRoutingForm extends ActionForm {

	protected String permitNumber;
	protected String contractorName;
	protected String inspectionDate;
	protected String inspectionCode;
	protected List inspectionItemCodeList;
	protected String comments;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	public String getInspectionDate() {
		return inspectionDate;
	}

	/**
	 * @return
	 */
	public String getPermitNumber() {
		return permitNumber;
	}

	public void setContractorName(String string) {
		contractorName = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionDate(String string) {
		inspectionDate = string;
	}

	/**
	 * @param string
	 */
	public void setPermitNumber(String string) {
		permitNumber = string;
	}

	/**
	 * @return
	 */
	public String getContractorName() {
		return contractorName;
	}

	/**
	 * @return
	 */
	public String getInspectionCode() {
		return inspectionCode;
	}

	/**
	 * @param string
	 */
	public void setInspectionCode(String string) {
		inspectionCode = string;
	}

	/**
	 * @return
	 */
	public List getInspectionItemCodeList() {
		return inspectionItemCodeList;
	}

	/**
	 * @param list
	 */
	public void setInspectionItemCodeList(List list) {
		inspectionItemCodeList = list;
	}

	/**
	 * @return
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param strings
	 */
	public void setComments(String strings) {
		comments = strings;
	}

}
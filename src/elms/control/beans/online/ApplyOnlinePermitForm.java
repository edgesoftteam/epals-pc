package elms.control.beans.online;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.lso.Street;

public class ApplyOnlinePermitForm extends ActionForm {

	protected String streetNumber;
	protected String address;
	protected String streetName;
	protected String unitNumber;
	protected String streetFraction;
	protected Street street;
	protected String subProjectName;
	
	protected int useMapId;
	protected int stypeId;
	protected int tempOnlineID;
	protected int selectPeopleType;
	protected int selectOwnerType;
	
	protected String iAgree;

	protected String valuation;
	protected String squareFootage;
	protected String numberOfDwellingUnits;
	protected String numberOfFloors;

	
	protected int lsoId;
	protected String forcePC;
	protected String lsoType;
	protected String chkExistProjFlag="N";
	protected String projectName;
	protected String projectNumber;
	protected String tempQuestionId;
	protected String questionaireDescription;
	protected String tempAcknowledgementId;
	protected String acknowledgementDescription;
	protected String checkQuestionFlag;
	protected int subProjectId;

	protected String checkArrayQuestionSize;

	// PeopleSearch
	protected String chkExistPeopleFlag;
	protected String searchEntry;
	protected String searchBased;
	protected String startName;

	protected String name;
	protected String agentName;
	protected String phoneNo;
	protected String licNo;
	protected String emailAddr;
	protected int peopleId;
	protected String peopleType;

	private String selectedId;

	protected String selectProject;

	protected String structureName;
	protected int structureId;
	protected String engineerName;
	protected String ownerName;
	protected String architectName;
	protected String contractorName;

	protected String checkOwnerOrPeople;

	protected String comboNumber;
	protected String comboName;

	protected List comboActList = new ArrayList();
	protected int comboActSize;
	protected String description;

	protected int feeUnitNo;
	
	protected String url;
	protected String created;
	protected String updated;


	/**
	 * @return Returns the print1.
	 */
	public String getPrint1() {
		return print1;
	}

	/**
	 * @param print1
	 *            The print1 to set.
	 */
	public void setPrint1(String print1) {
		this.print1 = print1;
	}

	protected String termsCondition;
	protected String startDate;
	protected String print;
	protected String print1;
	
	private String questionAnswered;

	
	
	public String getAcknowledgementDescription() {
		return acknowledgementDescription;
	}

	public void setAcknowledgementDescription(String acknowledgementDescription) {
		this.acknowledgementDescription = acknowledgementDescription;
	}

	/**
	 * 
	 */
	public ApplyOnlinePermitForm() {

		// TODO Auto-generated constructor stub
	}

	
	
	public int getSelectOwnerType() {
		return selectOwnerType;
	}

	public void setSelectOwnerType(int selectOwnerType) {
		this.selectOwnerType = selectOwnerType;
	}

	/**
	 * 
	 */
	public ApplyOnlinePermitForm(String structureName, int structureId) {

		this.structureName = structureName;
		this.structureId = structureId;

		// TODO Auto-generated constructor stub
	}

	
	public String getiAgree() {
		return iAgree;
	}

	public void setiAgree(String iAgree) {
		this.iAgree = iAgree;
	}

	/**
	 * 
	 */
	public ApplyOnlinePermitForm(int lsoId, String valuation, String projectName, String subProjectName, String forcePC) {

		this.lsoId = lsoId;
		this.valuation = valuation;
		this.projectName = projectName;
		this.subProjectName = subProjectName;
		this.forcePC = forcePC;

		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */

	public ApplyOnlinePermitForm(String projectName, String projectNumber, int tempOnlineID) {
		this.projectName = projectName;
		this.projectNumber = projectNumber;
		this.tempOnlineID = tempOnlineID;

	}

	public ApplyOnlinePermitForm(int useMapId, String subProjectName) {
		this.useMapId = useMapId;
		this.subProjectName = subProjectName;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * @return
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * @param string
	 */
	public void setStreetName(String string) {
		streetName = string;
	}

	/**
	 * @param string
	 */
	public void setUnitNumber(String string) {
		unitNumber = string;
	}

	/**
	 * @return
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @param string
	 */
	public void setStreetNumber(String string) {
		streetNumber = string;
	}

	/**
	 * @return
	 */
	public Street getStreet() {
		return street;
	}

	/**
	 * @param street
	 */
	public void setStreet(Street street) {
		this.street = street;
	}

	
	
	public int getSelectPeopleType() {
		return selectPeopleType;
	}

	public void setSelectPeopleType(int selectPeopleType) {
		this.selectPeopleType = selectPeopleType;
	}

	/**
	 * @return
	 */
	public String getSubProjectName() {
		return subProjectName;
	}

	/**
	 * @param string
	 */
	public void setSubProjectName(String string) {
		subProjectName = string;
	}

	/**
	 * @return
	 */
	public int getStypeId() {
		return stypeId;
	}

	/**
	 * @param i
	 */
	public void setStypeId(int i) {
		stypeId = i;
	}

	/**
	 * @return
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * @param string
	 */
	public void setStreetFraction(String string) {
		streetFraction = string;
	}

	/**
	 * @return
	 */
	public int getTempOnlineID() {
		return tempOnlineID;
	}

	/**
	 * @param i
	 */
	public void setTempOnlineID(int i) {
		tempOnlineID = i;
	}

	/**
	 * @return
	 */
	public int getUseMapId() {
		return useMapId;
	}

	/**
	 * @param i
	 */
	public void setUseMapId(int i) {
		useMapId = i;
	}

	/**
	 * @return
	 */
	public String getValuation() {
		return valuation;
	}

	/**
	 * @param string
	 */
	public void setValuation(String string) {
		valuation = string;
	}

	/**
	 * @return
	 */
	public String getChkExistProjFlag() {
		return chkExistProjFlag;
	}

	/**
	 * @return
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @return
	 */
	public String getProjectNumber() {
		return projectNumber;
	}

	/**
	 * @param string
	 */
	public void setChkExistProjFlag(String string) {
		chkExistProjFlag = string;
	}

	/**
	 * @param string
	 */
	public void setProjectName(String string) {
		projectName = string;
	}

	/**
	 * @param string
	 */
	public void setProjectNumber(String string) {
		projectNumber = string;
	}

	/**
	 * @return
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * @param string
	 */
	public void setLsoId(int number) {
		lsoId = number;
	}

	/**
	 * @return
	 */
	public String getForcePC() {
		return forcePC;
	}

	/**
	 * @param string
	 */
	public void setForcePC(String string) {
		forcePC = string;
	}

	/**
	 * @return
	 */
	public String getLsoType() {
		return lsoType;
	}

	/**
	 * @param string
	 */
	public void setLsoType(String string) {
		lsoType = string;
	}

	/**
	 * @return
	 */
	public String getQuestionaireDescription() {
		return questionaireDescription;
	}

	/**
	 * @param string
	 */
	public void setQuestionaireDescription(String string) {
		questionaireDescription = string;
	}

	/**
	 * @return
	 */
	public String getTempQuestionId() {
		return tempQuestionId;
	}

	/**
	 * @param string
	 */
	public void setTempQuestionId(String string) {
		tempQuestionId = string;
	}

	/**
	 * @return
	 */
	public String getCheckArrayQuestionSize() {
		return checkArrayQuestionSize;
	}

	/**
	 * @return
	 */
	public String getCheckQuestionFlag() {
		return checkQuestionFlag;
	}

	/**
	 * @param string
	 */
	public void setCheckArrayQuestionSize(String string) {
		checkArrayQuestionSize = string;
	}

	/**
	 * @param string
	 */
	public void setCheckQuestionFlag(String string) {
		checkQuestionFlag = string;
	}

	/**
	 * @return
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param i
	 */
	public void setSubProjectId(int i) {
		subProjectId = i;
	}

	/**
	 * @return
	 */
	public String getChkExistPeopleFlag() {
		return chkExistPeopleFlag;
	}

	/**
	 * @param string
	 */
	public void setChkExistPeopleFlag(String string) {
		chkExistPeopleFlag = string;
	}

	/**
	 * @return
	 */
	public String getSearchBased() {
		return searchBased;
	}

	/**
	 * @return
	 */
	public String getSearchEntry() {
		return searchEntry;
	}

	/**
	 * @param string
	 */
	public void setSearchBased(String string) {
		searchBased = string;
	}

	/**
	 * @param string
	 */
	public void setSearchEntry(String string) {
		searchEntry = string;
	}

	/**
	 * @return
	 */
	public String getStartName() {
		return startName;
	}

	/**
	 * @param string
	 */
	public void setStartName(String string) {
		startName = string;
	}

	/**
	 * @return
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @return
	 */
	public String getEmailAddr() {
		return emailAddr;
	}

	/**
	 * @return
	 */
	public String getLicNo() {
		return licNo;
	}

	/**
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * @return
	 */
	public String getPeopleType() {
		return peopleType;
	}

	/**
	 * @return
	 */
	public String getPhoneNo() {
		return phoneNo;
	}

	/**
	 * @param string
	 */
	public void setAgentName(String string) {
		agentName = string;
	}

	/**
	 * @param string
	 */
	public void setEmailAddr(String string) {
		emailAddr = string;
	}

	/**
	 * @param string
	 */
	public void setLicNo(String string) {
		licNo = string;
	}

	/**
	 * @param string
	 */
	public void setName(String string) {
		name = string;
	}

	/**
	 * @param i
	 */
	public void setPeopleId(int i) {
		peopleId = i;
	}

	/**
	 * @param string
	 */
	public void setPeopleType(String string) {
		peopleType = string;
	}

	/**
	 * @param string
	 */
	public void setPhoneNo(String string) {
		phoneNo = string;
	}

	/**
	 * @return
	 */
	public String getSelectedId() {
		return selectedId;
	}

	/**
	 * @param string
	 */
	public void setSelectedId(String string) {
		selectedId = string;
	}

	/**
	 * @return
	 */
	public String getSelectProject() {
		return selectProject;
	}

	/**
	 * @param string
	 */
	public void setSelectProject(String string) {
		selectProject = string;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @return
	 */
	public String getStructureName() {
		return structureName;
	}

	/**
	 * @param string
	 */
	public void setStructureName(String string) {
		structureName = string;
	}

	/**
	 * @return
	 */
	public String getArchitectName() {
		return architectName;
	}

	/**
	 * @return
	 */
	public String getContractorName() {
		return contractorName;
	}

	/**
	 * @return
	 */
	public String getEngineerName() {
		return engineerName;
	}

	/**
	 * @return
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param string
	 */
	public void setArchitectName(String string) {
		architectName = string;
	}

	/**
	 * @param string
	 */
	public void setContractorName(String string) {
		contractorName = string;
	}

	/**
	 * @param string
	 */
	public void setEngineerName(String string) {
		engineerName = string;
	}

	/**
	 * @param string
	 */
	public void setOwnerName(String string) {
		ownerName = string;
	}

	/**
	 * @return
	 */
	public String getCheckOwnerOrPeople() {
		return checkOwnerOrPeople;
	}

	/**
	 * @param string
	 */
	public void setCheckOwnerOrPeople(String string) {
		checkOwnerOrPeople = string;
	}

	/**
	 * @return
	 */
	public String getComboName() {
		return comboName;
	}

	/**
	 * @return
	 */
	public String getComboNumber() {
		return comboNumber;
	}

	/**
	 * @param string
	 */
	public void setComboName(String string) {
		comboName = string;
	}

	/**
	 * @param string
	 */
	public void setComboNumber(String string) {
		comboNumber = string;
	}

	/**
	 * @return
	 */
	public List getComboActList() {
		return comboActList;
	}

	/**
	 * @param list
	 */
	public void setComboActList(List list) {
		comboActList = list;
	}

	/**
	 * @return
	 */
	public int getComboActSize() {
		return comboActSize;
	}

	/**
	 * @param i
	 */
	public void setComboActSize(int i) {
		comboActSize = i;
	}

	/**
	 * @return
	 */
	public String getTermsCondition() {
		return termsCondition;
	}

	/**
	 * @param string
	 */
	public void setTermsCondition(String string) {
		termsCondition = string;
	}

	/**
	 * @return
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * @param string
	 */
	public void setStartDate(String string) {
		startDate = string;
	}

	/**
	 * @return
	 */
	public String getPrint() {
		return print;
	}

	/**
	 * @param string
	 */
	public void setPrint(String string) {
		print = string;
	}

	/**
	 * @return
	 */
	public int getStructureId() {
		return structureId;
	}

	/**
	 * @param i
	 */
	public void setStructureId(int i) {
		structureId = i;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public int getFeeUnitNo() {
		return feeUnitNo;
	}

	public void setFeeUnitNo(int feeUnitNo) {
		this.feeUnitNo = feeUnitNo;
	}

	public String getSquareFootage() {
		return squareFootage;
	}

	public void setSquareFootage(String squareFootage) {
		this.squareFootage = squareFootage;
	}

	public String getNumberOfDwellingUnits() {
		return numberOfDwellingUnits;
	}

	public void setNumberOfDwellingUnits(String numberOfDwellingUnits) {
		this.numberOfDwellingUnits = numberOfDwellingUnits;
	}

	public String getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(String numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}

	public String getTempAcknowledgementId() {
		return tempAcknowledgementId;
	}

	public void setTempAcknowledgementId(String tempAcknowledgementId) {
		this.tempAcknowledgementId = tempAcknowledgementId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getQuestionAnswered() {
		return questionAnswered;
	}

	public void setQuestionAnswered(String questionAnswered) {
		this.questionAnswered = questionAnswered;
	}
}

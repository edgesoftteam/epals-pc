package elms.control.beans.online;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gayathri Turlapati
 * Email Template
 */
public class EmailTemplateTypeAdminForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1170718392281544887L;
	private int emailTemplateTypeId;
	private String emailTemplateType;
	private String emailTemplateDesc;
	
	private int createdBy;
	private int updatedBy;
	
	List emailTempTypeList;
	
	private String displayEmailTemplateType;
	protected String[] selectedItems = {};
	
	 	public void reset(ActionMapping mapping, HttpServletRequest request) {
	        this.emailTemplateType = null;
	        this.emailTemplateDesc = null;
	        this.createdBy = 0;
	        this.updatedBy = 0;
	    }
	    
	    /**
		 * @return the createdBy
		 */
		public int getCreatedBy() {
			return createdBy;
		}

		/**
		 * @param createdBy the createdBy to set
		 */
		public void setCreatedBy(int createdBy) {
			this.createdBy = createdBy;
		}

		/**
		 * @return the updatedBy
		 */
		public int getUpdatedBy() {
			return updatedBy;
		}

		/**
		 * @param updatedBy the updatedBy to set
		 */
		public void setUpdatedBy(int updatedBy) {
			this.updatedBy = updatedBy;
		}

		/**
		 * @return the emailTempTypeList
		 */
		public List getEmailTempTypeList() {
			return emailTempTypeList;
		}

		/**
		 * @param emailTempTypeList the emailTempTypeList to set
		 */
		public void setEmailTempTypeList(List emailTempTypeList) {
			this.emailTempTypeList = emailTempTypeList;
		}

		/**
		 * @return the displayEmailTemplateType
		 */
		public String getDisplayEmailTemplateType() {
			return displayEmailTemplateType;
		}

		/**
		 * @param displayEmailTemplateType the displayEmailTemplateType to set
		 */
		public void setDisplayEmailTemplateType(String displayEmailTemplateType) {
			this.displayEmailTemplateType = displayEmailTemplateType;
		}
		
		/**
	     * Returns the selectedItems.
	     * @return String[]
	     */
	    public String[] getSelectedItems() {
	        return selectedItems;
	    }

	    /**
	     * Sets the selectedItems.
	     * @param selectedItems The selectedItems to set
	     */
	    public void setSelectedItems(String[] selectedItems) {
	        this.selectedItems = selectedItems;
	    }

		/**
		 * @param id
		 * @param emailTemplateType
		 * @param emailTemplateDesc
		 * @param active
		 */
		public EmailTemplateTypeAdminForm(int emailTemplateTypeId, String emailTemplateType, String emailTemplateDesc, int createdBy, int updatedBy, List emailTempTypeList) {
			super();
			this.emailTemplateTypeId = emailTemplateTypeId;
			this.emailTemplateType = emailTemplateType;
			this.emailTemplateDesc = emailTemplateDesc;
			this.createdBy = createdBy;
			this.updatedBy = updatedBy;
			this.emailTempTypeList = emailTempTypeList;
		}

		public EmailTemplateTypeAdminForm() {}

		/**
		 * @return the id
		 */
		public int getEmailTemplateTypeId() {
			return emailTemplateTypeId;
		}

		/**
		 * @param id the id to set
		 */
		public void setEmailTemplateTypeId(int emailTemplateTypeId) {
			this.emailTemplateTypeId = emailTemplateTypeId;
		}

		/**
		 * @return the emailTemplateType
		 */
		public String getEmailTemplateType() {
			return emailTemplateType;
		}

		/**
		 * @param emailTemplateType the emailTemplateType to set
		 */
		public void setEmailTemplateType(String emailTemplateType) {
			this.emailTemplateType = emailTemplateType;
		}

		/**
		 * @return the emailTemplateDesc
		 */
		public String getEmailTemplateDesc() {
			return emailTemplateDesc;
		}

		/**
		 * @param emailTemplateDesc the emailTemplateDesc to set
		 */
		public void setEmailTemplateDesc(String emailTemplateDesc) {
			this.emailTemplateDesc = emailTemplateDesc;
		}
}
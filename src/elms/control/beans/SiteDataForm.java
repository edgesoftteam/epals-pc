package elms.control.beans;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.sitedata.SiteData;

public class SiteDataForm extends ActionForm {
	SiteData siteData;
	static Logger logger = Logger.getLogger(SiteDataForm.class.getName());

	public SiteDataForm() {
		siteData = new SiteData();
	}

	/**
	 * Gets the siteData
	 * 
	 * @return Returns a SiteData
	 */
	public SiteData getSiteData() {
		return siteData;
	}

	/**
	 * Sets the siteData
	 * 
	 * @param siteData
	 *            The siteData to set
	 */
	public void setSiteData(SiteData siteData) {

		try {
			if (siteData != null) {
				PropertyUtils.copyProperties(this.siteData, siteData);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

} // End class
package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.lso.AddressSummary;

/**
 * @author shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class AddressSummaryForm extends ActionForm {

	protected String lsoId;
	protected String lsoMatch;
	protected String landFunction;
	protected String address;
	protected AddressSummary addressSummary;

	/**
	 * Constructor for AddressSearchForm.
	 */
	public AddressSummaryForm() {
		super();
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		return errors;
	}

	/**
	 * Returns the lsoId.
	 * 
	 * @return String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId.
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Returns the function.
	 * 
	 * @return String
	 */
	public String getLandFunction() {
		return landFunction;
	}

	/**
	 * Sets the function.
	 * 
	 * @param function
	 *            The function to set
	 */
	public void setLandFunction(String function) {
		this.landFunction = function;
	}

	/**
	 * Returns the addressSummary.
	 * 
	 * @return AddressSummary
	 */
	public AddressSummary getAddressSummary() {
		return addressSummary;
	}

	/**
	 * Sets the addressSummary.
	 * 
	 * @param addressSummary
	 *            The addressSummary to set
	 */
	public void setAddressSummary(AddressSummary addressSummary) {
		this.addressSummary = addressSummary;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Returns the lsoMatch.
	 * 
	 * @return String
	 */
	public String getLsoMatch() {
		return lsoMatch;
	}

	/**
	 * Sets the lsoMatch.
	 * 
	 * @param lsoMatch
	 *            The lsoMatch to set
	 */
	public void setLsoMatch(String lsoMatch) {
		this.lsoMatch = lsoMatch;
	}

}

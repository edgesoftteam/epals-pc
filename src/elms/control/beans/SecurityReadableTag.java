/*
 * Created on Aug 22, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.common.Constants;
import elms.security.User;

/**
 * @author rdhanapal
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style - Code Templates
 */
public class SecurityReadableTag extends TagSupport {
	static Logger logger = Logger.getLogger(SecurityReadableTag.class.getName());
	private String redLevelId;
	private String redLevelType;
	private String readProperty = "";

	public int doStartTag() throws JspException {
		logger.debug("readProperty " + readProperty);
		int flag = SKIP_BODY;
		try {

			if ("title".equals(readProperty)) {

				if (checkForEditable()) {// if true skip the body
					// logger.debug("SKIP_BODY");
					flag = SKIP_BODY;
				} else {
					// logger.debug("EVAL_PAGE");

					flag = EVAL_PAGE; // if false eval body
				}

				return flag;
			}// title

			else if ("field".equals(readProperty)) {
				if (checkForProjectNameType()) {
					flag = EVAL_PAGE;
				} else {
					flag = SKIP_BODY;
				}

				return flag;

			}// field
			else if ("button".equals(readProperty)) {

				if (checkForDepartmentCode()) {
					flag = EVAL_PAGE;
				} else {
					flag = SKIP_BODY;
				}

				return flag;
			} // button
			else {
				return flag;
			}

			/*
			 * if(checkForEditable()){// if true skip the body //logger.debug("SKIP_BODY"); return SKIP_BODY; } else{ //logger.debug("EVAL_PAGE");
			 * 
			 * return (EVAL_PAGE); // if false eval body }
			 */
		} catch (Exception exc) {
			throw new JspException(exc);
		}

	}

	private boolean checkForEditable() throws Exception {
		boolean status = true;
		HttpSession session = pageContext.getSession();
		logger.debug("LevelId: " + redLevelId + ", Level: " + redLevelType);
		try {
			int tmpLevelId = Integer.parseInt(redLevelId);
			status = new CommonAgent().editable(tmpLevelId, redLevelType);
			// logger.debug("status: "+status);
		} catch (Exception e) {
			throw e;
		}

		return status;
	}

	private boolean checkForProjectNameType() throws Exception {
		boolean flag = false;
		HttpSession session = pageContext.getSession();
		logger.debug("LevelId: " + redLevelId + ", Level: " + redLevelType);
		String projectNameType;

		try {
			int tmpLevelId = Integer.parseInt(redLevelId);
			int projNameId = LookupAgent.getProjectNameId(redLevelType, tmpLevelId);
			projectNameType = LookupAgent.getProjectName(projNameId);
			logger.debug("project name is " + projectNameType);

			if (projectNameType.equals(Constants.PROJECT_NAME_PLANNING)) {
				flag = true;
			}
			// logger.debug("flag: "+flag);
		} catch (Exception e) {
			throw e;
		}

		return flag;
	}

	private boolean checkForDepartmentCode() throws Exception {
		boolean flag = false;
		HttpSession session = pageContext.getSession();
		logger.debug("LevelId: " + redLevelId + ", Level: " + redLevelType);

		try {
			User user = (User) session.getAttribute(Constants.USER_KEY);
			String departmentCode = user.getDepartment().getDepartmentCode();
			logger.debug("department code is " + departmentCode);
			if (departmentCode.equalsIgnoreCase("PL")) {
				flag = true;
			}

		} catch (Exception e) {
			throw e;
		}

		return flag;
	}

	/**
	 * @return Returns the redLevelId.
	 */
	public String getRedLevelId() {
		return redLevelId;
	}

	/**
	 * @param redLevelId
	 *            The redLevelId to set.
	 */
	public void setRedLevelId(String redLevelId) {
		this.redLevelId = redLevelId;
	}

	/**
	 * @return Returns the redLevelType.
	 */
	public String getRedLevelType() {
		return redLevelType;
	}

	/**
	 * @param redLevelType
	 *            The redLevelType to set.
	 */
	public void setRedLevelType(String redLevelType) {
		this.redLevelType = redLevelType;
	}

	/**
	 * @return Returns the readProperty.
	 */
	public String getReadProperty() {
		return readProperty;
	}

	/**
	 * @param readProperty
	 *            The readProperty to set.
	 */
	public void setReadProperty(String readProperty) {
		this.readProperty = readProperty;
	}
}

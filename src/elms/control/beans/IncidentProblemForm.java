/*
 * Created on Jun 2, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans;

import java.io.Serializable;

/**
 * @author Akash
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class IncidentProblemForm implements Serializable{

	private String problem;
    private int dept_id;

    public IncidentProblemForm(String problem,int dept_id ) {
        this.problem = problem;
        this.dept_id = dept_id;
       
    }
	/**
	 * @return Returns the dept_id.
	 */
	public int getDept_id() {
		return dept_id;
	}
	/**
	 * @param dept_id The dept_id to set.
	 */
	public void setDept_id(int dept_id) {
		this.dept_id = dept_id;
	}
	/**
	 * @return Returns the problem.
	 */
	public String getProblem() {
		return problem;
	}
	/**
	 * @param problem The problem to set.
	 */
	public void setProblem(String problem) {
		this.problem = problem;
	}
}

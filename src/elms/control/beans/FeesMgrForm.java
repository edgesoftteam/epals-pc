package elms.control.beans;

import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.util.StringUtils;

public class FeesMgrForm extends ActionForm {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(FeesMgrForm.class.getName());
	protected int activityId;
	protected String valuation;
	protected String permitFeeDate;
	protected String planCheckFeeDate;
	protected String planCheckFeesRequired;
	protected String developmentFeeDate; // added by Gayathri on 15thDec'08
	protected String developmentFeesRequired;// added by Gayathri on 15thDec'08
	protected FinanceSummary financeSummary;
	protected ActivityFeeEdit[] activityFeeList;
	protected ActivityFeeEdit[] planCheckFeeList;
	protected ActivityFeeEdit[] developmentFeeList;
	protected ActivityFeeEdit[] businessTaxList;
	protected String backUrl;
	protected ActivityFeeEdit[] penaltyFeeList;// added for penalty
	protected String readyToPay;
	protected String greenHalo;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (request.getAttribute("fromPage") == null) {
			if (!(activityFeeList == null)) {
				for (int i = 0; i < activityFeeList.length; i++) {
					activityFeeList[i].setChecked("");
				}
			}

			if (!(planCheckFeeList == null)) {
				for (int i = 0; i < planCheckFeeList.length; i++) {
					planCheckFeeList[i].setChecked("");
				}
			}
			if (!(developmentFeeList == null)) {
				for (int i = 0; i < developmentFeeList.length; i++) {
					developmentFeeList[i].setChecked("");
				}
			}// adde for penalty
			if (!(penaltyFeeList == null)) {
				for (int i = 0; i < penaltyFeeList.length; i++) {
					penaltyFeeList[i].setChecked("");
				}
			}

		}
		readyToPay = "off";
		
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * @return Returns the developmentFeeList.
	 */
	public ActivityFeeEdit[] getDevelopmentFeeList() {
		return developmentFeeList;
	}

	/**
	 * @param developmentFeeList
	 *            The developmentFeeList to set.
	 */
	public void setDevelopmentFeeList(ActivityFeeEdit[] developmentFeeList) {
		this.developmentFeeList = developmentFeeList;
	}

	public void setDevelopmentFeeList(RowSet rs) {
		int i = 0;

		try {
			rs.beforeFirst();

			while (rs.next()) {
				if ((rs.getString("feeFlagFour").equals("3")) && (!rs.getString("feeFlagFour").equals("1")) && (!rs.getString("feeFlagFour").equals("2")) && (!rs.getString("feeFlagFour").equals("4"))) {
					i++;
				}
			}

			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i];
			i = -1;

			while (rs.next()) {
				if ((rs.getString("feeFlagFour").equals("3")) && (!rs.getString("feeFlagFour").equals("1")) && (!rs.getString("feeFlagFour").equals("2")) && (!rs.getString("feeFlagFour").equals("4"))) {

					logger.debug("feesmgrform of setDevelopmentFeeList(RowSet rs)-->");
					ActivityFeeEdit af = new ActivityFeeEdit();
					af.setFeeId(rs.getString("fee_id"));
					af.setFeePc(rs.getString("fee_pc"));
					af.setChecked(rs.getString("feeChecked"));
					af.setLsoType(rs.getString("LSO_USE"));
					af.setFeeDescription(rs.getString("fee_desc"));
					af.setFeeUnits(rs.getString("feeUnits"));
					af.setFeeAmount(StringUtils.dbl2$(rs.getDouble("feeAmount")));
					af.setInput(rs.getString("input"));
					af.setRequired(rs.getString("required"));
					af.setSubtotalLevel(rs.getString("subtotalLevel"));
					af.setFeeCalcTwo(rs.getString("fee_calc_2"));
					// af.setFeeFlagFour(rs.getString("feeFlagFour"));
					af.setFeeInit(rs.getString("feeInit"));
					af.setFactor(rs.getString("factor"));
					af.setFeeFactor(rs.getString("fee_factor"));

					logger.debug("factor : " + af.getFactor());
					logger.debug("subTotalLevel : " + af.getSubtotalLevel());
					logger.debug("feeFlagFour : " + af.getFeeFlagFour());
					logger.debug("feeInit : " + af.getFeeInit());

					if (rs.getDouble("fee_factor") > 0.0) {
						if (rs.getString("fee_calc_1").equalsIgnoreCase("Y")) {
							af.setUnitPrice("(" + StringUtils.dbl2prct(rs.getDouble("fee_factor")) + ")");
						} else {
							af.setUnitPrice("(" + StringUtils.dbl2$(rs.getDouble("fee_factor")) + ")");
						}
					} else {
						af.setUnitPrice("");
					}

					afAry[++i] = af;
				}
			}

			setDevelopmentFeeList(afAry);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	/**
	 * @return Returns the developmentFeesRequired.
	 */
	public String getDevelopmentFeesRequired() {
		return developmentFeesRequired;
	}

	/**
	 * @param developmentFeesRequired
	 *            The developmentFeesRequired to set.
	 */
	public void setDevelopmentFeesRequired(String developmentFeesRequired) {
		this.developmentFeesRequired = developmentFeesRequired;
	}

	/**
	 * Gets the permitFeeDate
	 * 
	 * @return Returns a String
	 */
	public String getPermitFeeDate() {
		return permitFeeDate;
	}

	/**
	 * Sets the permitFeeDate
	 * 
	 * @param permitFeeDate
	 *            The permitFeeDate to set
	 */
	public void setPermitFeeDate(String permitFeeDate) {
		this.permitFeeDate = permitFeeDate;
	}

	/**
	 * Gets the planCheckFeeDate
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckFeeDate() {
		return planCheckFeeDate;
	}

	/**
	 * Sets the planCheckFeeDate
	 * 
	 * @param planCheckFeeDate
	 *            The planCheckFeeDate to set
	 */
	public void setPlanCheckFeeDate(String planCheckFeeDate) {
		this.planCheckFeeDate = planCheckFeeDate;
	}

	/**
	 * @return Returns the developmentFeeDate. added by Gayathri
	 */
	public String getDevelopmentFeeDate() {
		return developmentFeeDate;
	}

	/**
	 * @param developmentFeeDate
	 *            The developmentFeeDate to set. added by Gayathri
	 */
	public void setDevelopmentFeeDate(String developmentFeeDate) {
		this.developmentFeeDate = developmentFeeDate;
	}

	/**
	 * Gets the activityFeeList
	 * 
	 * @return Returns a ActivityFeeEdit[]
	 */
	public ActivityFeeEdit[] getActivityFeeList() {
		return activityFeeList;
	}

	/**
	 * Sets the activityFeeList
	 * 
	 * @param activityFeeList
	 *            The activityFeeList to set
	 */
	public void setActivityFeeList(ActivityFeeEdit[] activityFeeList) {
		this.activityFeeList = activityFeeList;
	}

	public void setActivityFeeList(RowSet rs) {
		int i = 0;

		try {
			rs.beforeFirst();

			while (rs.next()) {
				if ((rs.getString("feeFlagFour").equals("2")) && (!rs.getString("feeFlagFour").equals("1")) && (!rs.getString("feeFlagFour").equals("3")) && (!rs.getString("feeFlagFour").equals("4"))) {
					i++;
				}
			}

			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i];

			i = -1;

			while (rs.next()) {
				if ((rs.getString("feeFlagFour").equals("2")) && (!rs.getString("feeFlagFour").equals("1")) && (!rs.getString("feeFlagFour").equals("3")) && (!rs.getString("feeFlagFour").equals("4"))) {
					ActivityFeeEdit af = new ActivityFeeEdit();
					af.setFeeId(rs.getString("fee_id"));
					af.setFeePc(rs.getString("fee_pc"));
					af.setChecked(rs.getString("feeChecked"));
					af.setLsoType(rs.getString("lso_use"));
					af.setFeeDescription(rs.getString("fee_desc"));

					logger.debug("## rs.getString(fee_desc)## :: " + rs.getString("fee_desc"));

					if (rs.getString("fee_desc").toUpperCase().startsWith("PLAN MAINT")) {
						af.setPlanCheckFees("Y");
					} else {
						af.setPlanCheckFees("N");
					}

					af.setFeeUnits(StringUtils.d2s(rs.getDouble("feeUnits")));
					af.setFeeAmount(StringUtils.dbl2$(rs.getDouble("feeAmount")));
					af.setInput(rs.getString("input"));
					af.setRequired(rs.getString("required"));
					af.setSubtotalLevel(rs.getString("subtotalLevel"));
					af.setFeeFlagFour(rs.getString("feeFlagFour"));
					af.setFeeCalcOne(rs.getString("fee_calc_1"));
					af.setFeeCalcTwo(rs.getString("fee_calc_2"));
					af.setFeeInit(rs.getString("feeInit"));
					af.setFactor(rs.getString("factor"));
					af.setFeeFactor(rs.getString("fee_factor"));
					af.setTax(rs.getString("tax_flag"));

					logger.debug("----------------- ");
					logger.debug("description : " + af.getFeeDescription());
					logger.debug("factor : " + af.getFactor());
					logger.debug("subTotalLevel : " + af.getSubtotalLevel());
					logger.debug("feeFlagFour : " + af.getFeeFlagFour());
					logger.debug("feeInit : " + af.getFeeInit());
					logger.debug("feeAmount : " + af.getFeeAmount());

					if (rs.getDouble("fee_factor") > 0.0) {
						if (rs.getString("fee_calc_1").equalsIgnoreCase("Y")) {
							af.setUnitPrice("(" + StringUtils.dbl2prct(rs.getDouble("fee_factor")) + ")");
						} else {
							af.setUnitPrice("(" + StringUtils.dbl2$(rs.getDouble("fee_factor")) + ")");
						}
					} else {
						af.setUnitPrice("");
					}

					afAry[++i] = af;
				}
			}

			setActivityFeeList(afAry);
			logger.debug("size of activityFeeList::" + getActivityFeeList().length);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	/**
	 * Gets the businessTaxList
	 * 
	 * @return Returns a ActivityFeeEdit[]
	 */
	public ActivityFeeEdit[] getBusinessTaxList() {
		return businessTaxList;
	}

	/**
	 * Sets the businessTaxList
	 * 
	 * @param businessTaxList
	 *            The businessTaxList to set
	 */
	public void setBusinessTaxList(ActivityFeeEdit[] businessTaxList) {
		this.businessTaxList = businessTaxList;
	}

	public void setBusinessTaxList(RowSet rs) {
		int i = 0;

		try {
			rs.beforeFirst();

			while (rs.next()) {
				i++;
			}

			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i + 1];

			i = -1;

			while (rs.next()) {

				ActivityFeeEdit af = new ActivityFeeEdit();
				af.setFeeId(rs.getString("fee_id"));
				af.setPeopleId(rs.getString("people_id"));

				String feeValuation = StringUtils.dbl2$(rs.getDouble("fee_valuation"));
				af.setFeeValuation(feeValuation);

				if (feeValuation.equals(valuation)) {
					af.setChecked("on");
					logger.debug("Setting the checkbox to on");
				} else {
					af.setChecked("");
				}

				af.setComments(rs.getString("comments"));
				af.setFeeAmount(StringUtils.dbl2$(rs.getDouble("fee_amnt")));
				af.setName(rs.getString("name"));
				af.setLicenseNbr(rs.getString("lic_no"));
				afAry[++i] = af;
			}

			ActivityFeeEdit af = new ActivityFeeEdit();
			afAry[++i] = af;
			setBusinessTaxList(afAry);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	/**
	 * Gets the valuation
	 * 
	 * @return Returns a String
	 */
	public String getValuation() {
		return valuation;
	}

	/**
	 * Sets the valuation
	 * 
	 * @param valuation
	 *            The valuation to set
	 */
	public void setValuation(String valuation) {
		this.valuation = valuation;
	}

	/**
	 * Gets the financeSummary
	 * 
	 * @return Returns a FinanceSummary
	 */
	public FinanceSummary getFinanceSummary() {
		return financeSummary;
	}

	/**
	 * Sets the financeSummary
	 * 
	 * @param financeSummary
	 *            The financeSummary to set
	 */
	public void setFinanceSummary(FinanceSummary financeSummary) {
		this.financeSummary = financeSummary;
	}

	/**
	 * Gets the planCheckFeeList
	 * 
	 * @return Returns a ActivityFeeEdit[]
	 */
	public ActivityFeeEdit[] getPlanCheckFeeList() {
		return planCheckFeeList;
	}

	/**
	 * Sets the planCheckFeeList
	 * 
	 * @param planCheckFeeList
	 *            The planCheckFeeList to set
	 */
	public void setPlanCheckFeeList(ActivityFeeEdit[] planCheckFeeList) {
		this.planCheckFeeList = planCheckFeeList;
	}

	public void setPlanCheckFeeList(RowSet rs) {
		int i = 0;

		try {
			rs.beforeFirst();

			while (rs.next()) {
				if ((rs.getString("feeFlagFour").equals("1")) && (!rs.getString("feeFlagFour").equals("2")) && (!rs.getString("feeFlagFour").equals("3")) && (!rs.getString("feeFlagFour").equals("4"))) {
					i++;
				}
			}

			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i];
			i = -1;

			while (rs.next()) {
				if ((rs.getString("feeFlagFour").equals("1")) && (!rs.getString("feeFlagFour").equals("2")) && (!rs.getString("feeFlagFour").equals("3")) && (!rs.getString("feeFlagFour").equals("4"))) {
					ActivityFeeEdit af = new ActivityFeeEdit();
					af.setFeeId(rs.getString("fee_id"));
					af.setFeePc(rs.getString("fee_pc"));
					af.setChecked(rs.getString("feeChecked"));
					af.setLsoType(rs.getString("LSO_USE"));
					af.setFeeDescription(rs.getString("fee_desc"));
					af.setFeeUnits(rs.getString("feeUnits"));
					af.setFeeAmount(StringUtils.dbl2$(rs.getDouble("feeAmount")));
					af.setInput(rs.getString("input"));
					af.setRequired(rs.getString("required"));
					af.setSubtotalLevel(rs.getString("subtotalLevel"));
					af.setFeeCalcTwo(rs.getString("fee_calc_2"));
					af.setFeeFlagFour(rs.getString("feeFlagFour"));
					af.setFeeInit(rs.getString("feeInit"));
					af.setFactor(rs.getString("factor"));
					af.setFeeFactor(rs.getString("fee_factor"));

					logger.debug("factor : " + af.getFactor());
					logger.debug("subTotalLevel : " + af.getSubtotalLevel());
					logger.debug("feeFlagFour : " + af.getFeeFlagFour());
					logger.debug("feeInit : " + af.getFeeInit());

					if (rs.getDouble("fee_factor") > 0.0) {
						if (rs.getString("fee_calc_1").equalsIgnoreCase("Y")) {
							af.setUnitPrice("(" + StringUtils.dbl2prct(rs.getDouble("fee_factor")) + ")");
						} else {
							af.setUnitPrice("(" + StringUtils.dbl2$(rs.getDouble("fee_factor")) + ")");
						}
					} else {
						af.setUnitPrice("");
					}

					afAry[++i] = af;
				}
			}

			setPlanCheckFeeList(afAry);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	/**
	 * Gets the planCheckFeesRequired
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckFeesRequired() {
		return planCheckFeesRequired;
	}

	/**
	 * Sets the planCheckFeesRequired
	 * 
	 * @param planCheckFeesRequired
	 *            The planCheckFeesRequired to set
	 */
	public void setPlanCheckFeesRequired(String planCheckFeesRequired) {
		this.planCheckFeesRequired = planCheckFeesRequired;

	}

	/**
	 * Returns the activityId.
	 * 
	 * @return int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId.
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return
	 */
	public String getBackUrl() {
		return backUrl;
	}

	/**
	 * @param string
	 */
	public void setBackUrl(String string) {
		backUrl = string;
	}

	/**
	 * @return Returns the penaltyFeeList.
	 */
	public ActivityFeeEdit[] getPenaltyFeeList() {
		return penaltyFeeList;
	}

	/**
	 * @param penaltyFeeList
	 *            The penaltyFeeList to set.
	 */
	public void setPenaltyFeeList(ActivityFeeEdit[] penaltyFeeList) {
		this.penaltyFeeList = penaltyFeeList;
	}

	/**
	 * @param Rowset
	 *            . The penaltyFeeList to set.
	 */
	public void setPenaltyFeeList(RowSet rs) {
		int i = 0;
		double feeAmt = 0.00;
		double feePaid = 0.00;

		try {
			rs.beforeFirst();

			while (rs.next()) {
				if (rs.getString("feeFlagFour").equals("4")) {
					i++;
				}
			}

			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i];

			i = -1;

			while (rs.next()) {
				if (rs.getString("feeFlagFour").equals("4")) {
					ActivityFeeEdit af = new ActivityFeeEdit();
					af.setFeeId(rs.getString("fee_id"));
					af.setFeePc(rs.getString("fee_pc"));
					// af.setChecked(rs.getString("feeChecked"));
					af.setLsoType(rs.getString("lso_use"));
					af.setFeeDescription(rs.getString("fee_desc"));

					if (rs.getString("fee_desc").toUpperCase().startsWith("PLAN MAINT")) {
						af.setPlanCheckFees("Y");
					} else {
						af.setPlanCheckFees("N");
					}

					// af.setFeeUnits(StringUtils.d2s(rs.getDouble("feeUnits")));
					// af.setFeeAmount(StringUtils.dbl2$(rs.getDouble("feeAmount")));

					feeAmt = rs.getDouble("fee_amnt");
					feePaid = rs.getDouble("fee_paid");

					if ((feeAmt > 0.00) && ((feeAmt > feePaid) || (feeAmt < feePaid))) {
						af.setChecked(rs.getString("feeChecked"));
					} else {
						af.setChecked("");
					}
					if ((feeAmt > 0.00) && ((feeAmt > feePaid) || (feeAmt < feePaid))) {
						af.setFeeUnits(StringUtils.d2s(rs.getDouble("feeUnits")));
					} else {
						af.setFeeUnits("");
					}

					if ((feeAmt > 0.00) && ((feeAmt > feePaid) || (feeAmt < feePaid))) {
						af.setFeeAmount(StringUtils.dbl2$(rs.getDouble("feeAmount")));
					} else {
						af.setFeeAmount("$0.00");
					}

					af.setInput(rs.getString("input"));
					af.setRequired(rs.getString("required"));
					af.setSubtotalLevel(rs.getString("subtotalLevel"));
					af.setFeeFlagFour(rs.getString("feeFlagFour"));
					af.setFeeCalcOne(rs.getString("fee_calc_1"));
					af.setFeeCalcTwo(rs.getString("fee_calc_2"));
					af.setFeeInit(rs.getString("feeInit"));
					af.setFactor(rs.getString("factor"));
					af.setFeeFactor(rs.getString("fee_factor"));
					af.setTax(rs.getString("tax_flag"));

					logger.debug("----------------- ");
					logger.debug("Penalty description : " + af.getFeeDescription());
					logger.debug("Penalty factor : " + af.getFactor());
					logger.debug("Penalty subTotalLevel : " + af.getSubtotalLevel());
					logger.debug("Penalty feeFlagFour : " + af.getFeeFlagFour());
					logger.debug("Penalty feeInit : " + af.getFeeInit());
					logger.debug("Penalty feeAmount : " + af.getFeeAmount());

					if (rs.getDouble("fee_factor") > 0.0) {
						if (rs.getString("fee_calc_1").equalsIgnoreCase("Y")) {
							af.setUnitPrice("(" + StringUtils.dbl2prct(rs.getDouble("fee_factor")) + ")");
						} else {
							af.setUnitPrice("(" + StringUtils.dbl2$(rs.getDouble("fee_factor")) + ")");
						}
					} else {
						af.setUnitPrice("");
					}

					afAry[++i] = af;

				}
			}

			setPenaltyFeeList(afAry);
			logger.debug("size of penalyFeeList::" + getPenaltyFeeList().length);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	public String getReadyToPay() {
		return readyToPay;
	}

	public void setReadyToPay(String readyToPay) {
		this.readyToPay = readyToPay;
	}

	/**
	 * @return the greenHalo
	 */
	public String getGreenHalo() {
		return greenHalo;
	}

	/**
	 * @param greenHalo the greenHalo to set
	 */
	public void setGreenHalo(String greenHalo) {
		this.greenHalo = greenHalo;
	}
}

package elms.control.beans;

import java.util.List;

import elms.app.inspection.Inspector;
import elms.app.project.PlanCheck;
import elms.app.project.PlanCheckHistory;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Shekhar Jain
 */
public class PlanCheckForm extends ActionForm {

    //Declare the variable and object declarations used by the class here
    protected String planCheckStatus;
    protected String planCheckId;
    protected String planCheckDate;
    protected String enginner;
    protected String engineerId;
    protected String comments;
    protected String activityId;
    protected PlanCheck[] planChecks;
    private PlanCheckHistory[] planChecksHistory;
    protected String[] selectedPlanCheck = {};
    protected String targetDate;
    protected String completionDate;
   // protected String unit;
    protected String unitHours;
    protected String unitMinutes;
    protected String pcApproved;
    protected String permitFinal;
    protected String todaysDate; 
    protected List planCheckList;
    protected String department;
    protected String statusDesc;
    private String created;
    
    private int processType;
    public String selectAllCheckBox;
    protected List sorting;
										   
    private String emailflag;
    
    
    
	
    public String getUnitHours() {
		return unitHours;
	}
	public void setUnitHours(String unitHours) {
		this.unitHours = unitHours;
	}
	public String getUnitMinutes() {
		return unitMinutes;
	}
	public void setUnitMinutes(String unitMinutes) {
		this.unitMinutes = unitMinutes;
	}
	public List getSorting() {
		return sorting;
	}
	public void setSorting(List sorting) {
		this.sorting = sorting;
	}
	/**
     * @return Returns the statusDesc.
     */
    public String getStatusDesc() {
        return statusDesc;
    }
    /**
     * @param statusDesc The statusDesc to set.
     */
    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
    /**
     * @return Returns the department.
     */
    public String getDepartment() {
        return department;
    }
    /**
     * @param department The department to set.
     */
    public void setDepartment(String department) {
        this.department = department;
    }
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        planCheckStatus = null;
        planCheckDate = null;
        targetDate = null;
        completionDate = null;
      //  unit = null;
        engineerId = null;
        comments = null;
        emailflag = "off";
					
    }

    public PlanCheckForm() {
        planCheckStatus = null;
        planCheckDate = null;
        targetDate = null;
        completionDate = null;
       // unit = null;
        engineerId = null;
        comments = null;
        
    }

    public ActionErrors validate(
            ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        //delcare validations for the form
        /*if ((firstName == null) || (firstName.length() < 1))
            errors.add("firstName",new ActionError("error.firstname.required"));

        if ((lastName == null) || (lastName.length() < 1))
            errors.add("lastName",new ActionError("error.lastname.required"));
        */
        return errors;
    }

    
    
    
	/**
	 * @return Returns the todaysDate.
	 */
	public String getTodaysDate() {
		return todaysDate;
	}
	/**
	 * @param todaysDate The todaysDate to set.
	 */
	public void setTodaysDate(String todaysDate) {
		this.todaysDate = todaysDate;
	}
    /**
     * Gets the planCheckStatus
	
     * @return Returns a String
     */
    public String getPlanCheckStatus() {
        return planCheckStatus;
    }

    /**
     * Sets the planCheckStatus
	
						  
     * @param planCheckStatus The planCheckStatus to set
     */
    public void setPlanCheckStatus(String planCheckStatus) {
        this.planCheckStatus = planCheckStatus;
    }

    /**
     * Gets the date
	
     * @return Returns a String
     */
    public String getPlanCheckDate() {
        return planCheckDate;
    }

    /**
     * Sets the date
	
			   
     * @param date The date to set
     */
    public void setPlanCheckDate(String planCheckdate) {
        this.planCheckDate = planCheckdate;
    }

    /**
     * Gets the comments
	
     * @return Returns a String
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the comments
	
				   
     * @param comments The comments to set
     */
    public void setComments(String comments) {
        this.comments = comments;
    }

    /**
     * Gets the activityId
	
     * @return Returns a String
     */
    public String getActivityId() {
        return activityId;
    }

    /**
     * Sets the activityId
	
					 
     * @param activityId The activityId to set
     */
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    /**
     * Gets the planChecks
	
     * @return Returns an Array
     */
    public PlanCheck[] getPlanChecks() {
        return planChecks;
    }

    /**
     * Sets the planChecks
	
					 
     * @param planChecks The planChecks to set
     */
    public void setPlanChecks(PlanCheck[] planChecks) {
        this.planChecks = planChecks;
    }


    /**
     * Returns the engineerId.
	
     * @return String
     */
    public String getEngineerId() {
        return engineerId;
    }

    /**
     * Sets the engineerId.
	
					 
     * @param engineerId The engineerId to set
     */
    public void setEngineerId(String engineerId) {
        this.engineerId = engineerId;
    }

    /**
     * Returns the enginner.
	
     * @return String
     */
    public String getEnginner() {
        return enginner;
    }

    /**
     * Sets the enginner.
	
				   
     * @param enginner The enginner to set
     */
    public void setEnginner(String enginner) {
        this.enginner = enginner;
    }

    /**
     * Returns the selectedPlanCheck.
	
     * @return String[]
     */
    public String[] getSelectedPlanCheck() {
        return selectedPlanCheck;
    }

    /**
     * Sets the selectedPlanCheck.
	
							
     * @param selectedPlanCheck The selectedPlanCheck to set
     */
    public void setSelectedPlanCheck(String[] selectedPlanCheck) {
        this.selectedPlanCheck = selectedPlanCheck;
    }

    /**
     * Returns the planCheckId.
	
     * @return String
     */
    public String getPlanCheckId() {
        return planCheckId;
    }

    /**
     * Sets the planCheckId.
	
					  
     * @param planCheckId The planCheckId to set
     */
    public void setPlanCheckId(String planCheckId) {
        this.planCheckId = planCheckId;
    }

								 
					   
  

	/**
	 * @return Returns the unit.
	 */
	/*public String getUnit() {
		return unit;
					
  

											   
							   
	}
	*//**
	 * @param unit The unit to set.
	 *//*
	public void setUnit(String unit) {
		this.unit = unit;
	}*/
	/**
	 * @return Returns the completionDate.
	 */
	public String getCompletionDate() {
		return completionDate;
	}
	/**
	 * @param completionDate The completionDate to set.
	 */
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

							   
				   
  

											 
							 
  
	/**
	 * @return Returns the targetDate.
	 */
	public String getTargetDate() {
		return targetDate;
	}
	/**
	 * @param targetDate The targetDate to set.
	 */
	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	/**
	 * @return Returns the pcApproved.
	 */
	public String getPcApproved() {
		return pcApproved;
	}
	/**
	 * @param pcApproved The pcApproved to set.
	 */
	public void setPcApproved(String pcApproved) {
		this.pcApproved = pcApproved;
	}
	/**
	 * @return Returns the permitFinal.
	 */
	public String getPermitFinal() {
		return permitFinal;
	}
	/**
	 * @param permitFinal The permitFinal to set.
	 */
	public void setPermitFinal(String permitFinal) {
		this.permitFinal = permitFinal;
	}
    /**
     * @return Returns the planCheckList.
     */
    public List getPlanCheckList() {
        return planCheckList;
    }
    /**
     * @param planCheckList The planCheckList to set.
     */
    public void setPlanCheckList(List planCheckList) {
        this.planCheckList = planCheckList;
    }
	/**
	 * @return the planChecksHistory
	 */

	/**
	 * @return the processType
	 */
	public int getProcessType() {
		return processType;
	}
	/**
	 * @param processType the processType to set
	 */
	public void setProcessType(int processType) {
		this.processType = processType;
	}
	/**
	 * @return the created
	 */
	public String getCreated() {
		return created;
	}
	/**
	 * @param created the created to set
	 */
	public void setCreated(String created) {
		this.created = created;
	}

								 
					 
  
	/**
	 * @return the selectAllCheckBox
	 */
  

	public String getSelectAllCheckBox() {
		return selectAllCheckBox;
	}
	/**
	 * @param selectAllCheckBox the selectAllCheckBox to set
	 */
	public void setSelectAllCheckBox(String selectAllCheckBox) {
		this.selectAllCheckBox = selectAllCheckBox;
	}
	public PlanCheckHistory[] getPlanChecksHistory() {
		return planChecksHistory;
	}
	public void setPlanChecksHistory(PlanCheckHistory[] planChecksHistory) {
		this.planChecksHistory = planChecksHistory;
	}
	public String getEmailflag() {
		return emailflag;
	}
	public void setEmailflag(String emailflag) {
		this.emailflag = emailflag;
	}

						   
				 
  

									   
						 
  

											
						   
}

																  
											 
  

 
/*
 * Created on Nov 1, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.beans.planning;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class EnvReviewSummary {

	private String level;
	private int levelId;
	private String insert;
	private String status;

	private String adcActualDate;
	private String adcLimitDate;
	private String decisionActualDate;
	private String decisionLimitDate;
	private String pisActualDate;
	private String pisLimitDate;
	private String pndActualDate;
	private String pndLimitDate;
	private String publicNoticeActualDate;
	private String publicNoticeLimitDate;
	private String decEarliestActualDate;
	private String decEarliestLimitDate;
	private String decLastActualDate;
	private String decLastLimitDate;
	private String nodActualDate;
	private String nodLimitDate;
	private String nopActualDate;
	private String nopLimitDate;
	private String nocActualDate;
	private String nocLimitDate;

	private String stateReview;

	public EnvReviewSummary(String level, int id) {
		this.levelId = id;
		this.level = level;
		this.insert = "Y";
	}

	/**
	 * @return
	 */
	public String getAdcActualDate() {
		return adcActualDate;
	}

	/**
	 * @return
	 */
	public String getAdcLimitDate() {
		return adcLimitDate;
	}

	/**
	 * @return
	 */
	public String getDecEarliestActualDate() {
		return decEarliestActualDate;
	}

	/**
	 * @return
	 */
	public String getDecEarliestLimitDate() {
		return decEarliestLimitDate;
	}

	/**
	 * @return
	 */
	public String getDecisionActualDate() {
		return decisionActualDate;
	}

	/**
	 * @return
	 */
	public String getDecisionLimitDate() {
		return decisionLimitDate;
	}

	/**
	 * @return
	 */
	public String getDecLastActualDate() {
		return decLastActualDate;
	}

	/**
	 * @return
	 */
	public String getDecLastLimitDate() {
		return decLastLimitDate;
	}

	/**
	 * @return
	 */
	public String getNocActualDate() {
		return nocActualDate;
	}

	/**
	 * @return
	 */
	public String getNocLimitDate() {
		return nocLimitDate;
	}

	/**
	 * @return
	 */
	public String getNodActualDate() {
		return nodActualDate;
	}

	/**
	 * @return
	 */
	public String getNodLimitDate() {
		return nodLimitDate;
	}

	/**
	 * @return
	 */
	public String getNopActualDate() {
		return nopActualDate;
	}

	/**
	 * @return
	 */
	public String getNopLimitDate() {
		return nopLimitDate;
	}

	/**
	 * @return
	 */
	public String getPisActualDate() {
		return pisActualDate;
	}

	/**
	 * @return
	 */
	public String getPisLimitDate() {
		return pisLimitDate;
	}

	/**
	 * @return
	 */
	public String getPndActualDate() {
		return pndActualDate;
	}

	/**
	 * @return
	 */
	public String getPndLimitDate() {
		return pndLimitDate;
	}

	/**
	 * @return
	 */
	public String getPublicNoticeActualDate() {
		return publicNoticeActualDate;
	}

	/**
	 * @return
	 */
	public String getPublicNoticeLimitDate() {
		return publicNoticeLimitDate;
	}

	/**
	 * @return
	 */
	public String getStateReview() {
		return stateReview;
	}

	/**
	 * @param string
	 */
	public void setAdcActualDate(String string) {
		adcActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setAdcLimitDate(String string) {
		adcLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setDecEarliestActualDate(String string) {
		decEarliestActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setDecEarliestLimitDate(String string) {
		decEarliestLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setDecisionActualDate(String string) {
		decisionActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setDecisionLimitDate(String string) {
		decisionLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setDecLastActualDate(String string) {
		decLastActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setDecLastLimitDate(String string) {
		decLastLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setNocActualDate(String string) {
		nocActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setNocLimitDate(String string) {
		nocLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setNodActualDate(String string) {
		nodActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setNodLimitDate(String string) {
		nodLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setNopActualDate(String string) {
		nopActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setNopLimitDate(String string) {
		nopLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setPisActualDate(String string) {
		pisActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setPisLimitDate(String string) {
		pisLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setPndActualDate(String string) {
		pndActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setPndLimitDate(String string) {
		pndLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setPublicNoticeActualDate(String string) {
		publicNoticeActualDate = string;
	}

	/**
	 * @param string
	 */
	public void setPublicNoticeLimitDate(String string) {
		publicNoticeLimitDate = string;
	}

	/**
	 * @param string
	 */
	public void setStateReview(String string) {
		stateReview = string;
	}

	/**
	 * @return
	 */
	public String getInsert() {
		return insert;
	}

	/**
	 * @return
	 */
	public String getLevel() {
		return level;
	}

	/**
	 * @return
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * @param string
	 */
	public void setInsert(String string) {
		insert = string;
	}

	/**
	 * @param string
	 */
	public void setLevel(String string) {
		level = string;
	}

	/**
	 * @param i
	 */
	public void setLevelId(int i) {
		levelId = i;
	}

	/**
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

}

package elms.control.beans.planning;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

import elms.app.planning.CalendarFeature;

public class CalendarFeatureForm extends ActionForm {
	protected CalendarFeature[] calendarFeatures;
	protected String meetingDate;
	protected String meetingType;
	protected String actionType;
	protected String comments;
	protected String[] selectedRecord = {};
	protected String spCalendarFeatureId;
	protected String subProjectId;
	protected List meetingTypeList = new ArrayList();
	protected List actionTypeList = new ArrayList();

	public CalendarFeatureForm() {
		calendarFeatures = new CalendarFeature[0];
		meetingDate = "";
		meetingType = "";
		actionType = "";
		comments = "";
	}

	/**
	 * @return Returns the actionType.
	 */
	public String getActionType() {
		return actionType;
	}

	/**
	 * @param actionType
	 *            The actionType to set.
	 */
	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	/**
	 * @return Returns the comments.
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments
	 *            The comments to set.
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return Returns the meetingDate.
	 */
	public String getMeetingDate() {
		return meetingDate;
	}

	/**
	 * @param meetingDate
	 *            The meetingDate to set.
	 */
	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}

	/**
	 * @return Returns the meetingType.
	 */
	public String getMeetingType() {
		return meetingType;
	}

	/**
	 * @param meetingType
	 *            The meetingType to set.
	 */
	public void setMeetingType(String meetingType) {
		this.meetingType = meetingType;
	}

	/**
	 * @return Returns the calendarFeatures.
	 */
	public CalendarFeature[] getCalendarFeatures() {
		return calendarFeatures;
	}

	/**
	 * @param calendarFeatures
	 *            The calendarFeatures to set.
	 */
	public void setCalendarFeatures(CalendarFeature[] calendarFeatures) {
		this.calendarFeatures = calendarFeatures;
	}

	/**
	 * @return Returns the selectedRecord.
	 */
	public String[] getSelectedRecord() {
		return selectedRecord;
	}

	/**
	 * @param selectedRecord
	 *            The selectedRecord to set.
	 */
	public void setSelectedRecord(String[] selectedRecord) {
		this.selectedRecord = selectedRecord;
	}

	/**
	 * @return Returns the spCalendarFeatureId.
	 */
	public String getSpCalendarFeatureId() {
		return spCalendarFeatureId;
	}

	/**
	 * @param spCalendarFeatureId
	 *            The spCalendarFeatureId to set.
	 */
	public void setSpCalendarFeatureId(String spCalendarFeatureId) {
		this.spCalendarFeatureId = spCalendarFeatureId;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(String subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return Returns the actionTypeList.
	 */
	public List getActionTypeList() {
		return actionTypeList;
	}

	/**
	 * @param actionTypeList
	 *            The actionTypeList to set.
	 */
	public void setActionTypeList(List actionTypeList) {
		this.actionTypeList = actionTypeList;
	}

	/**
	 * @return Returns the meetingTypeList.
	 */
	public List getMeetingTypeList() {
		return meetingTypeList;
	}

	/**
	 * @param meetingTypeList
	 *            The meetingTypeList to set.
	 */
	public void setMeetingTypeList(List meetingTypeList) {
		this.meetingTypeList = meetingTypeList;
	}
}

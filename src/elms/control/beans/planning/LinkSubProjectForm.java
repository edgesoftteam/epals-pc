package elms.control.beans.planning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.planning.LinkSubProjectEdit;

public class LinkSubProjectForm extends ActionForm {

	static Logger logger = Logger.getLogger(ResolutionForm.class.getName());

	private int subProjectId;
	private String editable;
	private String address;
	private LinkSubProjectEdit[] subProjectList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (this.subProjectList != null)
			for (int i = 0; i < subProjectList.length; i++) {
				this.subProjectList[i].setLinked("");
			}
	}

	/**
	 * Returns the logger.
	 * 
	 * @return Logger
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Returns the subProjectId.
	 * 
	 * @return int
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * Returns the subProjectList.
	 * 
	 * @return LinkSubProjectEdit[]
	 */
	public LinkSubProjectEdit[] getSubProjectList() {
		return subProjectList;
	}

	/**
	 * Sets the logger.
	 * 
	 * @param logger
	 *            The logger to set
	 */
	public static void setLogger(Logger logger) {
		LinkSubProjectForm.logger = logger;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Sets the subProjectId.
	 * 
	 * @param subProjectId
	 *            The subProjectId to set
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * Sets the subProjectList.
	 * 
	 * @param subProjectList
	 *            The subProjectList to set
	 */
	public void setSubProjectList(LinkSubProjectEdit[] subProjectList) {
		this.subProjectList = subProjectList;
	}

	public void setSubProjectList(List subProjects) {
		if (subProjects == null)
			subProjects = new ArrayList();
		LinkSubProjectEdit[] subProjectEditArray = new LinkSubProjectEdit[subProjects.size()];
		Iterator iter = subProjects.iterator();
		int index = 0;
		while (iter.hasNext()) {
			subProjectEditArray[index] = (LinkSubProjectEdit) iter.next();
			index++;
		}
		this.subProjectList = subProjectEditArray;
	}

	/**
	 * @return
	 */
	public String getEditable() {
		return editable;
	}

	/**
	 * @param string
	 */
	public void setEditable(String string) {
		editable = string;
	}

} // End class
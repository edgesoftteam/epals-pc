/*
 * Created on Sep 13, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.beans.planning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.planning.LinkActivityEdit;

/**
 * @author rdhanapal
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LinkActivityForm extends ActionForm {

	static Logger logger = Logger.getLogger(ResolutionForm.class.getName());

	private int activityId;
	private int deptId;
	private String editable;
	private String address;
	private String active;
	String message;
	private LinkActivityEdit[] allLinkedActivitiesList;
	private String[] prjList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (this.allLinkedActivitiesList != null)
			for (int i = 0; i < allLinkedActivitiesList.length; i++) {
				this.allLinkedActivitiesList[i].setLinked("");
			}
	}

	/**
	 * @return Returns the message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            The message to set.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @return
	 */
	public LinkActivityEdit[] getAllLinkedActivitiesList() {
		return allLinkedActivitiesList;
	}

	/**
	 * @return
	 */
	public String getEditable() {
		return editable;
	}

	/**
	 * @param i
	 */
	public void setActivityId(int i) {
		activityId = i;
	}

	/**
	 * @param string
	 */
	public void setAddress(String string) {
		address = string;
	}

	/**
	 * @param edits
	 */
	public void setAllLinkedActivitiesList(LinkActivityEdit[] edits) {
		allLinkedActivitiesList = edits;
	}

	public void setAllLinkedActivitiesList(List allLinkedActivitiesList) {
		if (allLinkedActivitiesList == null)
			allLinkedActivitiesList = new ArrayList();
		LinkActivityEdit[] subProjectEditArray = new LinkActivityEdit[allLinkedActivitiesList.size()];
		Iterator iter = allLinkedActivitiesList.iterator();
		int index = 0;
		while (iter.hasNext()) {
			subProjectEditArray[index] = (LinkActivityEdit) iter.next();
			index++;
		}
		this.allLinkedActivitiesList = subProjectEditArray;
	}

	/**
	 * @param string
	 */
	public void setEditable(String string) {
		editable = string;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public String[] getPrjList() {
		return prjList;
	}

	public void setPrjList(String[] prjList) {
		this.prjList = prjList;
	}

}

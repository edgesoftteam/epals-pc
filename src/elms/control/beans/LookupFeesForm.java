package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class LookupFeesForm extends ActionForm {

	protected String name;
	protected String insertFee;
	protected String duplicate;
	protected String preview;
	protected String percent;
	protected String addFee;
	protected String change;
	protected String edit;
	protected String delete;
	protected String cancel;

	protected LookupFeeEdit[] lkupFeeEditList;

	protected String selectedfee;
	protected String lookupId;
	protected String selectcheck;
	protected String upcheck;

	protected String feesLookUpValue;
	protected String addFeeLook;

	protected String rowcount;
	protected String addRow;
	protected String addLookup;

	protected int busTaxMin;
	protected int busTaxMax;
	protected String busTax;
	private List taxList;

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the insertFee
	 * 
	 * @return Returns a String
	 */
	public String getInsertFee() {
		return insertFee;
	}

	/**
	 * Sets the insertFee
	 * 
	 * @param insertFee
	 *            The insertFee to set
	 */
	public void setInsertFee(String insertFee) {
		this.insertFee = insertFee;
	}

	/**
	 * Gets the duplicate
	 * 
	 * @return Returns a String
	 */
	public String getDuplicate() {
		return duplicate;
	}

	/**
	 * Sets the duplicate
	 * 
	 * @param duplicate
	 *            The duplicate to set
	 */
	public void setDuplicate(String duplicate) {
		this.duplicate = duplicate;
	}

	/**
	 * Gets the preview
	 * 
	 * @return Returns a String
	 */
	public String getPreview() {
		return preview;
	}

	/**
	 * Sets the preview
	 * 
	 * @param preview
	 *            The preview to set
	 */
	public void setPreview(String preview) {
		this.preview = preview;
	}

	/**
	 * Gets the percent
	 * 
	 * @return Returns a String
	 */
	public String getPercent() {
		return percent;
	}

	/**
	 * Sets the percent
	 * 
	 * @param percent
	 *            The percent to set
	 */
	public void setPercent(String percent) {
		this.percent = percent;
	}

	/**
	 * Gets the addFee
	 * 
	 * @return Returns a String
	 */
	public String getAddFee() {
		return addFee;
	}

	/**
	 * Sets the addFee
	 * 
	 * @param addFee
	 *            The addFee to set
	 */
	public void setAddFee(String addFee) {
		this.addFee = addFee;
	}

	/**
	 * Gets the change
	 * 
	 * @return Returns a String
	 */
	public String getChange() {
		return change;
	}

	/**
	 * Sets the change
	 * 
	 * @param change
	 *            The change to set
	 */
	public void setChange(String change) {
		this.change = change;
	}

	/**
	 * Gets the edit
	 * 
	 * @return Returns a String
	 */
	public String getEdit() {
		return edit;
	}

	/**
	 * Sets the edit
	 * 
	 * @param edit
	 *            The edit to set
	 */
	public void setEdit(String edit) {
		this.edit = edit;
	}

	/**
	 * Gets the delete
	 * 
	 * @return Returns a String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Sets the delete
	 * 
	 * @param delete
	 *            The delete to set
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}

	/**
	 * Gets the cancel
	 * 
	 * @return Returns a String
	 */
	public String getCancel() {
		return cancel;
	}

	/**
	 * Sets the cancel
	 * 
	 * @param cancel
	 *            The cancel to set
	 */
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	/**
	 * Gets the selectedfee
	 * 
	 * @return Returns a String
	 */
	public String getSelectedfee() {
		return selectedfee;
	}

	/**
	 * Sets the selectedfee
	 * 
	 * @param selectedfee
	 *            The selectedfee to set
	 */
	public void setSelectedfee(String selectedfee) {
		this.selectedfee = selectedfee;
	}

	/**
	 * Gets the lookupId
	 * 
	 * @return Returns a String
	 */
	public String getLookupId() {
		return lookupId;
	}

	/**
	 * Sets the lookupId
	 * 
	 * @param lookupId
	 *            The lookupId to set
	 */
	public void setLookupId(String lookupId) {
		this.lookupId = lookupId;
	}

	/**
	 * Gets the selectcheck
	 * 
	 * @return Returns a boolean
	 */
	/**
	 * Gets the feesLookUpValue
	 * 
	 * @return Returns a String
	 */
	public String getFeesLookUpValue() {
		return feesLookUpValue;
	}

	/**
	 * Sets the feesLookUpValue
	 * 
	 * @param feesLookUpValue
	 *            The feesLookUpValue to set
	 */
	public void setFeesLookUpValue(String feesLookUpValue) {
		this.feesLookUpValue = feesLookUpValue;
	}

	/**
	 * Gets the addFeeLook
	 * 
	 * @return Returns a String
	 */
	public String getAddFeeLook() {
		return addFeeLook;
	}

	/**
	 * Sets the addFeeLook
	 * 
	 * @param addFeeLook
	 *            The addFeeLook to set
	 */
	public void setAddFeeLook(String addFeeLook) {
		this.addFeeLook = addFeeLook;
	}

	/**
	 * Gets the rowcount
	 * 
	 * @return Returns a String
	 */
	public String getRowcount() {
		return rowcount;
	}

	/**
	 * Sets the rowcount
	 * 
	 * @param rowcount
	 *            The rowcount to set
	 */
	public void setRowcount(String rowcount) {
		this.rowcount = rowcount;
	}

	/**
	 * Gets the addRow
	 * 
	 * @return Returns a String
	 */
	public String getAddRow() {
		return addRow;
	}

	/**
	 * Sets the addRow
	 * 
	 * @param addRow
	 *            The addRow to set
	 */
	public void setAddRow(String addRow) {
		this.addRow = addRow;
	}

	/**
	 * Gets the addLookup
	 * 
	 * @return Returns a String
	 */
	public String getAddLookup() {
		return addLookup;
	}

	/**
	 * Sets the addLookup
	 * 
	 * @param addLookup
	 *            The addLookup to set
	 */
	public void setAddLookup(String addLookup) {
		this.addLookup = addLookup;
	}

	/**
	 * Gets the lkupFeeEditList
	 * 
	 * @return Returns a List
	 */
	public LookupFeeEdit[] getLkupFeeEditList() {
		return lkupFeeEditList;
	}

	/**
	 * Sets the lkupFeeEditList
	 * 
	 * @param lkupFeeEditList
	 *            The lkupFeeEditList to set
	 */
	public void setLkupFeeEditList(LookupFeeEdit[] aLkupFeeEditList) {
		this.lkupFeeEditList = aLkupFeeEditList;
	}

	/**
	 * Gets the selectcheck
	 * 
	 * @return Returns a String
	 */
	public String getSelectcheck() {
		return selectcheck;
	}

	/**
	 * Sets the selectcheck
	 * 
	 * @param selectcheck
	 *            The selectcheck to set
	 */
	public void setSelectcheck(String selectcheck) {
		this.selectcheck = selectcheck;
	}

	/**
	 * Gets the upcheck
	 * 
	 * @return Returns a String
	 */
	public String getUpcheck() {
		return upcheck;
	}

	/**
	 * Sets the upcheck
	 * 
	 * @param upcheck
	 *            The upcheck to set
	 */
	public void setUpcheck(String upcheck) {
		this.upcheck = upcheck;
	}

	public String getBusTax() {
		return busTax;
	}

	public void setBusTax(String busTax) {
		this.busTax = busTax;
	}

	public int getBusTaxMin() {
		return busTaxMin;
	}

	public void setBusTaxMin(int busTaxMin) {
		this.busTaxMin = busTaxMin;
	}

	public int getBusTaxMax() {
		return busTaxMax;
	}

	public void setBusTaxMax(int busTaxMax) {
		this.busTaxMax = busTaxMax;
	}

	public List getTaxList() {
		return taxList;
	}

	public void setTaxList(List taxList) {
		this.taxList = taxList;
	}

} // End class
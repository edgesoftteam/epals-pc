package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class SearchFeesForm extends ActionForm {

	protected String feeDate;
	protected String Submit;

	/**
	 * Gets the feeDate
	 * 
	 * @return Returns a String
	 */
	public String getFeeDate() {
		return feeDate;
	}

	/**
	 * Sets the feeDate
	 * 
	 * @param feeDate
	 *            The feeDate to set
	 */
	public void setFeeDate(String feeDate) {
		this.feeDate = feeDate;
	}

	/**
	 * Gets the submit
	 * 
	 * @return Returns a String
	 */
	public String getSubmit() {
		return Submit;
	}

	/**
	 * Sets the submit
	 * 
	 * @param submit
	 *            The submit to set
	 */
	public void setSubmit(String submit) {
		Submit = submit;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		feeDate = null;
		Submit = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

} // End class

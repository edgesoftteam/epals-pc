package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.inspection.InspectionLibraryRecord;

public class SearchLibForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String category;
	protected String subCategory;
	protected String categoryDesc;
	protected String subCategoryDesc;
	protected String buttonNew;
	protected InspectionLibraryRecord[] libRecordList;

	public SearchLibForm() {

		category = "";
		subCategory = "";
		buttonNew = "";
		libRecordList = new InspectionLibraryRecord[0];

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		category = "";
		subCategory = "";

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((category == null) || (category.length() < 1)) errors.add("category",new ActionError("error.category.required"));
		 * 
		 * if ((subCategory == null) || (subCategory.length() < 1)) errors.add("subCategory",new ActionError("error.subCategory.required"));
		 */
		return errors;
	}

	/**
	 * Gets the category
	 * 
	 * @return Returns a String
	 */
	public String getCategory() {
		// System.out.println("GETCategory called in search form, category-"+category);
		return category;
	}

	/**
	 * Sets the category
	 * 
	 * @param category
	 *            The category to set
	 */
	public void setCategory(String category) {
		// System.out.println("SETCategory called in search form, category-"+category);
		this.category = category;
	}

	/**
	 * Gets the subCategory
	 * 
	 * @return Returns a String
	 */
	public String getSubCategory() {
		// System.out.println("getSubcategory called in search form, subCategory-"+subCategory);
		return subCategory;
	}

	/**
	 * Sets the subCategory
	 * 
	 * @param subCategory
	 *            The subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		// System.out.println("SETsubcategory called in search form, subCategory-"+subCategory);
		this.subCategory = subCategory;
	}

	/**
	 * Gets the libRecordList
	 * 
	 * @return Returns a List
	 */
	public InspectionLibraryRecord[] getLibRecordList() {
		// System.out.println("getLibrecordList called in search form. # of elements -" + libRecordList.length);
		return libRecordList;
	}

	/**
	 * Sets the libRecordList
	 * 
	 * @param libRecordList
	 *            The libRecordList to set
	 */
	public void setLibRecordList(InspectionLibraryRecord[] libRecordList) {
		// System.out.println("setLibrecordList called in search form. # of elements -" + libRecordList.length);
		this.libRecordList = libRecordList;
	}

	/**
	 * Gets the buttonNew
	 * 
	 * @return Returns a String
	 */
	public String getButtonNew() {
		// System.out.println("getButtonNew called");
		return buttonNew;
	}

	/**
	 * Sets the buttonNew
	 * 
	 * @param buttonNew
	 *            The buttonNew to set
	 */
	public void setButtonNew(String buttonNew) {
		this.buttonNew = buttonNew;
	}

	/**
	 * Gets the categoryDesc
	 * 
	 * @return Returns a String
	 */
	public String getCategoryDesc() {
		return categoryDesc;
	}

	/**
	 * Sets the categoryDesc
	 * 
	 * @param categoryDesc
	 *            The categoryDesc to set
	 */
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}

	/**
	 * Gets the subCategoryDesc
	 * 
	 * @return Returns a String
	 */
	public String getSubCategoryDesc() {
		return subCategoryDesc;
	}

	/**
	 * Sets the subCategoryDesc
	 * 
	 * @param subCategoryDesc
	 *            The subCategoryDesc to set
	 */
	public void setSubCategoryDesc(String subCategoryDesc) {
		this.subCategoryDesc = subCategoryDesc;
	}

}
package elms.control.beans;

import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.admin.CustomField;
import elms.app.lso.Owner;
import elms.app.people.People;
import elms.util.StringUtils;

public class ActivityForm extends ActionForm {

	static Logger logger = Logger.getLogger(ActivityForm.class.getName());
	private static final long serialVersionUID = 1L;
	protected String submitted = "N";

	protected String lsoId;
	protected String streetNumber;
	protected String streetFraction;
	protected String streetName;
	protected String unit;
	protected String address;
	protected String parcel1;
	protected String parcel2;
	
	protected String subProjectId;
	protected String subProjectName;
	protected String subProjectType;

	protected String rangeStreetNumberStart1;
	protected String rangeStreetNumberEnd1;
	protected String rangeStreetName1;
	protected String rangeStreetNumberStart2;
	protected String rangeStreetNumberEnd2;
	protected String rangeStreetName2;
	protected String rangeStreetNumberStart3;
	protected String rangeStreetNumberEnd3;
	protected String rangeStreetName3;
	protected String rangeStreetNumberStart4;
	protected String rangeStreetNumberEnd4;
	protected String rangeStreetName4;
	protected String rangeStreetNumberStart5;
	protected String rangeStreetNumberEnd5;
	protected String rangeStreetName5;

	protected String crossStreetName1;
	protected String crossStreetName2;
	protected String landmark;

	protected String activityId;
	protected String activityNumber;
	protected String activityTypeDesc;
	protected String activityType;
	protected String[] activitySubTypes = {};
	protected String activitySubTypesString;
	protected String activitySubTypeDesc;
	protected String description;
	protected String expirationDate;
	protected String valuation;
	protected String microfilm;
	protected String microfilmDesc;
	protected boolean checkbox;
	protected boolean checkbox2;
	protected boolean planCheckRequired;
	protected boolean tempPlanCheckRequired;
	protected boolean developmentFeeRequired;
	protected String issueDate;
	protected String planCheckStatus;
	protected String activityStatus;
	protected String oldActivityStatus;
	protected String completionDate;
	protected String startDate;
	protected String comments;
	protected String[] selectedFee = {};
	protected String completionDateLabel;
	protected String expirationDateLabel;
	protected String origin;
	protected String sixMonthsLaterDate;
	protected String oldExpirationDate;
	protected String oldIssueDate;
	protected String sixMonthsOfIssueDate;
	protected String outStandingFees;
	protected String displayContent;
	protected String landUseId;
	protected boolean ownerFromCoActivitiesSelected;
	protected boolean ownerFromAssessorDataSelected;
	protected String displayAssessor;
	protected String displayPeople;
	protected People ownerFromPeople;
	protected Owner ownerFromAssessor;

	protected String newUnits;
	protected String existingsUnits;
	protected String demolishedUnits;

	protected int pName;

	protected String peopleId;
	protected String planNo;
	protected String subDivision;

	protected String locationType;
	protected boolean multiAddress;

	protected String label;

	protected String parkingZone;

	protected CustomField[] customFieldList;// added for CustomField
	
	private String wmRequired;
	
	private String greenHalo;
	
	private String extRefNumber;

	public ActivityForm() {
		super();

	}
	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.planCheckRequired = false;
	}

	
	public String getParcel1() {
		return parcel1;
	}


	public void setParcel1(String parcel1) {
		this.parcel1 = parcel1;
	}


	public String getParcel2() {
		return parcel2;
	}


	public void setParcel2(String parcel2) {
		this.parcel2 = parcel2;
	}


	public String getParkingZone() {
		return parkingZone;
	}

	public void setParkingZone(String parkingZone) {
		this.parkingZone = parkingZone;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public boolean isMultiAddress() {
		return multiAddress;
	}

	public void setMultiAddress(boolean multiAddress) {
		this.multiAddress = multiAddress;
	}

	public String getRangeStreetNumberStart1() {
		return rangeStreetNumberStart1;
	}

	public void setRangeStreetNumberStart1(String rangeStreetNumberStart1) {
		this.rangeStreetNumberStart1 = rangeStreetNumberStart1;
	}

	public String getRangeStreetNumberEnd1() {
		return rangeStreetNumberEnd1;
	}

	public void setRangeStreetNumberEnd1(String rangeStreetNumberEnd1) {
		this.rangeStreetNumberEnd1 = rangeStreetNumberEnd1;
	}

	public String getRangeStreetName1() {
		return rangeStreetName1;
	}

	public void setRangeStreetName1(String rangeStreetName1) {
		this.rangeStreetName1 = rangeStreetName1;
	}

	public String getRangeStreetNumberStart2() {
		return rangeStreetNumberStart2;
	}

	public void setRangeStreetNumberStart2(String rangeStreetNumberStart2) {
		this.rangeStreetNumberStart2 = rangeStreetNumberStart2;
	}

	public String getRangeStreetNumberEnd2() {
		return rangeStreetNumberEnd2;
	}

	public void setRangeStreetNumberEnd2(String rangeStreetNumberEnd2) {
		this.rangeStreetNumberEnd2 = rangeStreetNumberEnd2;
	}

	public String getRangeStreetName2() {
		return rangeStreetName2;
	}

	public void setRangeStreetName2(String rangeStreetName2) {
		this.rangeStreetName2 = rangeStreetName2;
	}

	public String getRangeStreetNumberStart3() {
		return rangeStreetNumberStart3;
	}

	public void setRangeStreetNumberStart3(String rangeStreetNumberStart3) {
		this.rangeStreetNumberStart3 = rangeStreetNumberStart3;
	}

	public String getRangeStreetNumberEnd3() {
		return rangeStreetNumberEnd3;
	}

	public void setRangeStreetNumberEnd3(String rangeStreetNumberEnd3) {
		this.rangeStreetNumberEnd3 = rangeStreetNumberEnd3;
	}

	public String getRangeStreetName3() {
		return rangeStreetName3;
	}

	public void setRangeStreetName3(String rangeStreetName3) {
		this.rangeStreetName3 = rangeStreetName3;
	}

	public String getRangeStreetNumberStart4() {
		return rangeStreetNumberStart4;
	}

	public void setRangeStreetNumberStart4(String rangeStreetNumberStart4) {
		this.rangeStreetNumberStart4 = rangeStreetNumberStart4;
	}

	public String getRangeStreetNumberEnd4() {
		return rangeStreetNumberEnd4;
	}

	public void setRangeStreetNumberEnd4(String rangeStreetNumberEnd4) {
		this.rangeStreetNumberEnd4 = rangeStreetNumberEnd4;
	}

	public String getRangeStreetName4() {
		return rangeStreetName4;
	}

	public void setRangeStreetName4(String rangeStreetName4) {
		this.rangeStreetName4 = rangeStreetName4;
	}

	public String getRangeStreetNumberStart5() {
		return rangeStreetNumberStart5;
	}

	public void setRangeStreetNumberStart5(String rangeStreetNumberStart5) {
		this.rangeStreetNumberStart5 = rangeStreetNumberStart5;
	}

	public String getRangeStreetNumberEnd5() {
		return rangeStreetNumberEnd5;
	}

	public void setRangeStreetNumberEnd5(String rangeStreetNumberEnd5) {
		this.rangeStreetNumberEnd5 = rangeStreetNumberEnd5;
	}

	public String getRangeStreetName5() {
		return rangeStreetName5;
	}

	public void setRangeStreetName5(String rangeStreetName5) {
		this.rangeStreetName5 = rangeStreetName5;
	}

	public String getCrossStreetName1() {
		return crossStreetName1;
	}

	public void setCrossStreetName1(String crossStreetName1) {
		this.crossStreetName1 = crossStreetName1;
	}

	public String getCrossStreetName2() {
		return crossStreetName2;
	}

	public void setCrossStreetName2(String crossStreetName2) {
		this.crossStreetName2 = crossStreetName2;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getLocationType() {
		return locationType;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public int getpName() {
		return pName;
	}

	public void setpName(int pName) {
		this.pName = pName;
	}

	public String getPeopleId() {
		return peopleId;
	}

	public void setPeopleId(String peopleId) {
		this.peopleId = peopleId;
	}

	public String getPlanNo() {
		return planNo;
	}

	public void setPlanNo(String planNo) {
		this.planNo = planNo;
	}

	public String getSubDivision() {
		return subDivision;
	}

	public void setSubDivision(String subDivision) {
		this.subDivision = subDivision;
	}

	public String getLsoId() {
		return lsoId;
	}

	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	public String getStreetNumber() {
		return streetNumber;
	}

	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	public String getStreetFraction() {
		return streetFraction;
	}

	public void setStreetFraction(String streetFraction) {
		this.streetFraction = streetFraction;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getSubProjectName() {
		return subProjectName;
	}

	public void setSubProjectName(String subProjectName) {
		this.subProjectName = subProjectName;
	}

	public String getSubProjectType() {
		return subProjectType;
	}

	public void setSubProjectType(String subProjectType) {
		this.subProjectType = subProjectType;
	}

	public String getSubmitted() {
		return submitted;
	}

	public void setSubmitted(String submitted) {
		this.submitted = submitted;
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the expirationDate
	 * 
	 * @return Returns a String
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the valuation
	 * 
	 * @return Returns a String
	 */
	public String getValuation() {
		return valuation;
	}

	/**
	 * Sets the valuation
	 * 
	 * @param valuation
	 *            The valuation to set
	 */
	public void setValuation(String valuation) {
		this.valuation = valuation;
	}

	/**
	 * Gets the checkbox
	 * 
	 * @return Returns a boolean
	 */
	public boolean getCheckbox() {
		return checkbox;
	}

	/**
	 * Sets the checkbox
	 * 
	 * @param checkbox
	 *            The checkbox to set
	 */
	public void setCheckbox(boolean checkbox) {
		this.checkbox = checkbox;
	}

	/**
	 * Gets the checkbox2
	 * 
	 * @return Returns a boolean
	 */
	public boolean getCheckbox2() {
		return checkbox2;
	}

	/**
	 * Sets the checkbox2
	 * 
	 * @param checkbox2
	 *            The checkbox2 to set
	 */
	public void setCheckbox2(boolean checkbox2) {
		this.checkbox2 = checkbox2;
	}

	/**
	 * Gets the issueDate
	 * 
	 * @return Returns a String
	 */
	public String getIssueDate() {
		return issueDate;
	}

	/**
	 * Sets the issueDate
	 * 
	 * @param issueDate
	 *            The issueDate to set
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * Gets the planCheckStatus
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckStatus() {
		return planCheckStatus;
	}

	/**
	 * Sets the planCheckStatus
	 * 
	 * @param planCheckStatus
	 *            The planCheckStatus to set
	 */
	public void setPlanCheckStatus(String planCheckStatus) {
		this.planCheckStatus = planCheckStatus;
	}

	/**
	 * Gets the activityStatus
	 * 
	 * @return Returns a String
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * Sets the activityStatus
	 * 
	 * @param activityStatus
	 *            The activityStatus to set
	 */
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * Gets the completionDate
	 * 
	 * @return Returns a String
	 */
	public String getCompletionDate() {
		return completionDate;
	}

	/**
	 * Sets the completionDate
	 * 
	 * @param completionDate
	 *            The completionDate to set
	 */
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	/**
	 * Gets the startDate
	 * 
	 * @return Returns a String
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the planCheckRequired
	 * 
	 * @return Returns a boolean
	 */
	public boolean getPlanCheckRequired() {
		return planCheckRequired;
	}

	/**
	 * Sets the planCheckRequired
	 * 
	 * @param planCheckRequired
	 *            The planCheckRequired to set
	 */
	public void setPlanCheckRequired(boolean planCheckRequired) {
		this.planCheckRequired = planCheckRequired;
	}

	/**
	 * Gets the selectedFee
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedFee() {
		return selectedFee;
	}

	/**
	 * Sets the selectedFee
	 * 
	 * @param selectedFee
	 *            The selectedFee to set
	 */
	public void setSelectedFee(String[] selectedFee) {
		this.selectedFee = selectedFee;
	}

	/**
	 * Gets the origin
	 * 
	 * @return Returns a String
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * Sets the origin
	 * 
	 * @param origin
	 *            The origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * Gets the oldActivityStatus
	 * 
	 * @return Returns a String
	 */
	public String getOldActivityStatus() {
		return oldActivityStatus;
	}

	/**
	 * Sets the oldActivityStatus
	 * 
	 * @param oldActivityStatus
	 *            The oldActivityStatus to set
	 */
	public void setOldActivityStatus(String oldActivityStatus) {
		this.oldActivityStatus = oldActivityStatus;
	}

	/**
	 * Gets the sixMonthsLaterDate
	 * 
	 * @return Returns a String
	 */
	public String getSixMonthsLaterDate() {
		return sixMonthsLaterDate;
	}

	/**
	 * Sets the sixMonthsLaterDate
	 * 
	 * @param sixMonthsLaterDate
	 *            The sixMonthsLaterDate to set
	 */
	public void setSixMonthsLaterDate(String sixMonthsLaterDate) {
		this.sixMonthsLaterDate = sixMonthsLaterDate;
	}

	/**
	 * Gets the oldExpirationDate
	 * 
	 * @return Returns a String
	 */
	public String getOldExpirationDate() {
		return oldExpirationDate;
	}

	/**
	 * Sets the oldExpirationDate
	 * 
	 * @param oldExpirationDate
	 *            The oldExpirationDate to set
	 */
	public void setOldExpirationDate(String oldExpirationDate) {
		this.oldExpirationDate = oldExpirationDate;
	}

	/**
	 * Gets the outStandingFees
	 * 
	 * @return Returns a String
	 */
	public String getOutStandingFees() {
		return outStandingFees;
	}

	/**
	 * Sets the outStandingFees
	 * 
	 * @param outStandingFees
	 *            The outStandingFees to set
	 */
	public void setOutStandingFees(String outStandingFees) {
		this.outStandingFees = outStandingFees;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Returns the activityId.
	 * 
	 * @return String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Returns the subProjectId.
	 * 
	 * @return String
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * Sets the activityId.
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Sets the subProjectId.
	 * 
	 * @param subProjectId
	 *            The subProjectId to set
	 */
	public void setSubProjectId(String subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * Returns the dispalyContent.
	 * 
	 * @return String
	 */
	public String getDisplayContent() {
		return displayContent;
	}

	/**
	 * Sets the dispalyContent.
	 * 
	 * @param dispalyContent
	 *            The dispalyContent to set
	 */
	public void setDisplayContent(String displayContent) {
		this.displayContent = displayContent;
	}

	/**
	 * Returns the activityNumber.
	 * 
	 * @return String
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * Returns the activityTypeDesc.
	 * 
	 * @return String
	 */
	public String getActivityTypeDesc() {
		return activityTypeDesc;
	}

	/**
	 * Sets the activityNumber.
	 * 
	 * @param activityNumber
	 *            The activityNumber to set
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * Sets the activityTypeDesc.
	 * 
	 * @param activityTypeDesc
	 *            The activityTypeDesc to set
	 */
	public void setActivityTypeDesc(String activityTypeDesc) {
		this.activityTypeDesc = activityTypeDesc;
	}

	/**
	 * Returns the microfilm.
	 * 
	 * @return String
	 */
	public String getMicrofilm() {
		return microfilm;
	}

	/**
	 * Sets the microfilm.
	 * 
	 * @param microfilm
	 *            The microfilm to set
	 */
	public void setMicrofilm(String microfilm) {
		this.microfilm = microfilm;
	}

	/**
	 * Returns the oldIssueDate.
	 * 
	 * @return String
	 */
	public String getOldIssueDate() {
		return oldIssueDate;
	}

	/**
	 * Sets the oldIssueDate.
	 * 
	 * @param oldIssueDate
	 *            The oldIssueDate to set
	 */
	public void setOldIssueDate(String oldIssueDate) {
		this.oldIssueDate = oldIssueDate;
	}

	/**
	 * Returns the sixMonthsOfIssueDate.
	 * 
	 * @return String
	 */
	public String getSixMonthsOfIssueDate() {
		return sixMonthsOfIssueDate;
	}

	/**
	 * Sets the sixMonthsOfIssueDate.
	 * 
	 * @param sixMonthsOfIssueDate
	 *            The sixMonthsOfIssueDate to set
	 */
	public void setSixMonthsOfIssueDate(String sixMonthsOfIssueDate) {
		this.sixMonthsOfIssueDate = sixMonthsOfIssueDate;
	}

	/**
	 * Returns the activitySubTypeDesc.
	 * 
	 * @return String
	 */
	public String getActivitySubTypeDesc() {
		return activitySubTypeDesc;
	}

	/**
	 * Sets the activitySubTypeDesc.
	 * 
	 * @param activitySubTypeDesc
	 *            The activitySubTypeDesc to set
	 */
	public void setActivitySubTypeDesc(String activitySubTypeDesc) {
		this.activitySubTypeDesc = activitySubTypeDesc;
	}

	/**
	 * Returns the landUseId.
	 * 
	 * @return String
	 */
	public String getLandUseId() {
		return landUseId;
	}

	/**
	 * Sets the landUseId.
	 * 
	 * @param landUseId
	 *            The landUseId to set
	 */
	public void setLandUseId(String landUseId) {
		this.landUseId = landUseId;
	}

	/**
	 * @return
	 */
	public String getCompletionDateLabel() {
		return completionDateLabel;
	}

	/**
	 * @return
	 */
	public String getExpirationDateLabel() {
		return expirationDateLabel;
	}

	/**
	 * @param string
	 */
	public void setCompletionDateLabel(String string) {
		completionDateLabel = string;
	}

	/**
	 * @param string
	 */
	public void setExpirationDateLabel(String string) {
		expirationDateLabel = string;
	}

	/**
	 * @return
	 */
	public String getMicrofilmDesc() {
		return microfilmDesc;
	}

	/**
	 * @param string
	 */
	public void setMicrofilmDesc(String string) {
		microfilmDesc = string;
	}

	/**
	 * @return
	 */
	public People getOwnerFromPeople() {
		return ownerFromPeople;
	}

	/**
	 * @return
	 */
	public Owner getOwnerFromAssessor() {
		return ownerFromAssessor;
	}

	/**
	 * @return
	 */
	public boolean getOwnerFromAssessorDataSelected() {
		return ownerFromAssessorDataSelected;
	}

	/**
	 * @return
	 */
	public boolean getOwnerFromCoActivitiesSelected() {
		return ownerFromCoActivitiesSelected;
	}

	/**
	 * @param people
	 */
	public void setOwnerFromPeople(People people) {
		ownerFromPeople = people;
	}

	/**
	 * @param owner
	 */
	public void setOwnerFromAssessor(Owner owner) {
		ownerFromAssessor = owner;
	}

	/**
	 * @param b
	 */
	public void setOwnerFromAssessorDataSelected(boolean b) {
		ownerFromAssessorDataSelected = b;
	}

	/**
	 * @param b
	 */
	public void setOwnerFromCoActivitiesSelected(boolean b) {
		ownerFromCoActivitiesSelected = b;
	}

	/**
	 * @return
	 */
	public String getDisplayAssessor() {
		return displayAssessor;
	}

	/**
	 * @return
	 */
	public String getDisplayPeople() {
		return displayPeople;
	}

	/**
	 * @param string
	 */
	public void setDisplayAssessor(String string) {
		displayAssessor = string;
	}

	/**
	 * @param string
	 */
	public void setDisplayPeople(String string) {
		displayPeople = string;
	}

	/**
	 * @return
	 */
	public String[] getActivitySubTypes() {
		return activitySubTypes;
	}

	/**
	 * @param strings
	 */
	public void setActivitySubTypes(String[] strings) {
		activitySubTypes = strings;
	}

	/**
	 * @return
	 */
	public String getActivitySubTypesString() {
		activitySubTypesString = "";
		for (int i = 0; i < activitySubTypes.length; i++) {
			if (i < activitySubTypes.length - 1) {
				activitySubTypesString += activitySubTypes[i] + ",";
			} else {
				activitySubTypesString += activitySubTypes[i];
			}
		}
		return activitySubTypesString;
	}

	/**
	 * @param string
	 */
	public void setActivitySubTypesString(String string) {
		activitySubTypesString = string;
	}

	/**
	 * @return
	 */
	public boolean getDevelopmentFeeRequired() {
		return developmentFeeRequired;
	}

	/**
	 * @param b
	 */
	public void setDevelopmentFeeRequired(boolean developmentFeeRequired) {
		this.developmentFeeRequired = developmentFeeRequired;
	}

	/**
	 * @return Returns the demolishedUnits.
	 */
	public String getDemolishedUnits() {
		return demolishedUnits;
	}

	/**
	 * @param demolishedUnits
	 *            The demolishedUnits to set.
	 */
	public void setDemolishedUnits(String demolishedUnits) {
		this.demolishedUnits = demolishedUnits;
	}

	/**
	 * @return Returns the existingsUnits.
	 */
	public String getExistingsUnits() {
		return existingsUnits;
	}

	/**
	 * @param existingsUnits
	 *            The existingsUnits to set.
	 */
	public void setExistingsUnits(String existingsUnits) {
		this.existingsUnits = existingsUnits;
	}

	/**
	 * @return Returns the newUnits.
	 */
	public String getNewUnits() {
		return newUnits;
	}

	/**
	 * @param newUnits
	 *            The newUnits to set.
	 */
	public void setNewUnits(String newUnits) {
		this.newUnits = newUnits;
	}

	public CustomField[] getCustomFieldList() {
		return customFieldList;
	}

	public void setCustomFieldList(CustomField[] customFieldList) {
		this.customFieldList = customFieldList;
	}

	public void setCustomFieldList(RowSet rs, String check) {
		int i = 0;

		try {
			rs.beforeFirst();
			while (rs.next())
				i++;
			CustomField[] cfs = new CustomField[i];
			rs.beforeFirst();

			i = 0;

			while (rs.next()) {
				CustomField cf = new CustomField();
				cf.setFieldId(rs.getInt("FIELD_ID"));
				cf.setFieldLabel(rs.getString("FIELD_LABEL"));
				cf.setFieldDesc(rs.getString("FIELD_DESC"));
				cf.setFieldType(rs.getString("FIELD_TYPE"));
				cf.setLevelType(rs.getString("LEVEL_TYPE"));
				cf.setLevelTypeId(rs.getInt("LEVEL_TYPE_ID"));
				cf.setFieldRequired(rs.getString("FIELD_REQ"));
				cf.setFieldValue(StringUtils.nullReplaceWithEmpty(rs.getString("FIELD_VALUE")));
				logger.debug("CustomField FIELD_ID : " + cf.getFieldId());
				logger.debug("CustomField FIELD_LABEL : " + cf.getFieldLabel());
				logger.debug("CustomField FIELD_DESC : " + cf.getFieldDesc());
				logger.debug("CustomField FIELD_TYPE : " + cf.getFieldType());
				logger.debug("CustomField LEVEL_TYPE : " + cf.getLevelType());
				logger.debug("CustomField LEVEL_TYPE_ID : " + cf.getLevelTypeId());
				logger.debug("CustomField FIELD_REQ : " + cf.getFieldRequired());
				logger.debug("CustomField FIELD_VALUE : " + cf.getFieldValue());

				cfs[i++] = cf;

			}

			this.setCustomFieldList(cfs);
			logger.debug("size of getCustomFieldList::" + getCustomFieldList().length);
		}

		catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}


	public boolean isTempPlanCheckRequired() {
		return tempPlanCheckRequired;
	}


	public void setTempPlanCheckRequired(boolean tempPlanCheckRequired) {
		this.tempPlanCheckRequired = tempPlanCheckRequired;
	}
	public String getWmRequired() {
		return wmRequired;
	}
	public void setWmRequired(String wmRequired) {
		this.wmRequired = wmRequired;
	}
	public String getGreenHalo() {
		return greenHalo;
	}

	public void setGreenHalo(String greenHalo) {
		this.greenHalo = greenHalo;
	}
	
	public String getExtRefNumber() {
		return extRefNumber;
	}
	public void setExtRefNumber(String extRefNumber) {
		this.extRefNumber = extRefNumber;
	}
}
package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReportParameterForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String streetDirCheck;
	protected String activityType;
	protected String streetNumberCheck;
	protected String streetFractionCond;
	protected String activityTypeDescCond;
	protected String streetNameCheck;
	protected String streetNumberCond;
	protected String streetName;
	protected String streetDirCond;
	protected String streetNameCond;
	protected String fromDate;
	protected String activityTypeDescCheck;
	protected String sortBy;
	protected String activityTypeCheck;
	protected String activityTypeCond;
	protected String activityNumberCond;
	protected String streetFractionCheck;
	protected String toDate;
	protected String descriptionCheck;
	protected String activityNumberCheck;
	protected String descriptionCond;

	/**
	 * Gets the streetDirCheck
	 * 
	 * @return Returns a String
	 */
	public String getStreetDirCheck() {
		return streetDirCheck;
	}

	/**
	 * Sets the streetDirCheck
	 * 
	 * @param streetDirCheck
	 *            The streetDirCheck to set
	 */
	public void setStreetDirCheck(String streetDirCheck) {
		this.streetDirCheck = streetDirCheck;
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * Gets the streetNumberCheck
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumberCheck() {
		return streetNumberCheck;
	}

	/**
	 * Sets the streetNumberCheck
	 * 
	 * @param streetNumberCheck
	 *            The streetNumberCheck to set
	 */
	public void setStreetNumberCheck(String streetNumberCheck) {
		this.streetNumberCheck = streetNumberCheck;
	}

	/**
	 * Gets the streetFractionCond
	 * 
	 * @return Returns a String
	 */
	public String getStreetFractionCond() {
		return streetFractionCond;
	}

	/**
	 * Sets the streetFractionCond
	 * 
	 * @param streetFractionCond
	 *            The streetFractionCond to set
	 */
	public void setStreetFractionCond(String streetFractionCond) {
		this.streetFractionCond = streetFractionCond;
	}

	/**
	 * Gets the activityTypeDescCond
	 * 
	 * @return Returns a String
	 */
	public String getActivityTypeDescCond() {
		return activityTypeDescCond;
	}

	/**
	 * Sets the activityTypeDescCond
	 * 
	 * @param activityTypeDescCond
	 *            The activityTypeDescCond to set
	 */
	public void setActivityTypeDescCond(String activityTypeDescCond) {
		this.activityTypeDescCond = activityTypeDescCond;
	}

	/**
	 * Gets the streetNameCheck
	 * 
	 * @return Returns a String
	 */
	public String getStreetNameCheck() {
		return streetNameCheck;
	}

	/**
	 * Sets the streetNameCheck
	 * 
	 * @param streetNameCheck
	 *            The streetNameCheck to set
	 */
	public void setStreetNameCheck(String streetNameCheck) {
		this.streetNameCheck = streetNameCheck;
	}

	/**
	 * Gets the streetNumberCond
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumberCond() {
		return streetNumberCond;
	}

	/**
	 * Sets the streetNumberCond
	 * 
	 * @param streetNumberCond
	 *            The streetNumberCond to set
	 */
	public void setStreetNumberCond(String streetNumberCond) {
		this.streetNumberCond = streetNumberCond;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the streetDirCond
	 * 
	 * @return Returns a String
	 */
	public String getStreetDirCond() {
		return streetDirCond;
	}

	/**
	 * Sets the streetDirCond
	 * 
	 * @param streetDirCond
	 *            The streetDirCond to set
	 */
	public void setStreetDirCond(String streetDirCond) {
		this.streetDirCond = streetDirCond;
	}

	/**
	 * Gets the streetNameCond
	 * 
	 * @return Returns a String
	 */
	public String getStreetNameCond() {
		return streetNameCond;
	}

	/**
	 * Sets the streetNameCond
	 * 
	 * @param streetNameCond
	 *            The streetNameCond to set
	 */
	public void setStreetNameCond(String streetNameCond) {
		this.streetNameCond = streetNameCond;
	}

	/**
	 * Gets the fromDate
	 * 
	 * @return Returns a String
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * Sets the fromDate
	 * 
	 * @param fromDate
	 *            The fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * Gets the activityTypeDescCheck
	 * 
	 * @return Returns a String
	 */
	public String getActivityTypeDescCheck() {
		return activityTypeDescCheck;
	}

	/**
	 * Sets the activityTypeDescCheck
	 * 
	 * @param activityTypeDescCheck
	 *            The activityTypeDescCheck to set
	 */
	public void setActivityTypeDescCheck(String activityTypeDescCheck) {
		this.activityTypeDescCheck = activityTypeDescCheck;
	}

	/**
	 * Gets the sortBy
	 * 
	 * @return Returns a String
	 */
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * Sets the sortBy
	 * 
	 * @param sortBy
	 *            The sortBy to set
	 */
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * Gets the activityTypeCheck
	 * 
	 * @return Returns a String
	 */
	public String getActivityTypeCheck() {
		return activityTypeCheck;
	}

	/**
	 * Sets the activityTypeCheck
	 * 
	 * @param activityTypeCheck
	 *            The activityTypeCheck to set
	 */
	public void setActivityTypeCheck(String activityTypeCheck) {
		this.activityTypeCheck = activityTypeCheck;
	}

	/**
	 * Gets the activityTypeCond
	 * 
	 * @return Returns a String
	 */
	public String getActivityTypeCond() {
		return activityTypeCond;
	}

	/**
	 * Sets the activityTypeCond
	 * 
	 * @param activityTypeCond
	 *            The activityTypeCond to set
	 */
	public void setActivityTypeCond(String activityTypeCond) {
		this.activityTypeCond = activityTypeCond;
	}

	/**
	 * Gets the activityNumberCond
	 * 
	 * @return Returns a String
	 */
	public String getActivityNumberCond() {
		return activityNumberCond;
	}

	/**
	 * Sets the activityNumberCond
	 * 
	 * @param activityNumberCond
	 *            The activityNumberCond to set
	 */
	public void setActivityNumberCond(String activityNumberCond) {
		this.activityNumberCond = activityNumberCond;
	}

	/**
	 * Gets the streetFractionCheck
	 * 
	 * @return Returns a String
	 */
	public String getStreetFractionCheck() {
		return streetFractionCheck;
	}

	/**
	 * Sets the streetFractionCheck
	 * 
	 * @param streetFractionCheck
	 *            The streetFractionCheck to set
	 */
	public void setStreetFractionCheck(String streetFractionCheck) {
		this.streetFractionCheck = streetFractionCheck;
	}

	/**
	 * Gets the toDate
	 * 
	 * @return Returns a String
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * Sets the toDate
	 * 
	 * @param toDate
	 *            The toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	/**
	 * Gets the descriptionCheck
	 * 
	 * @return Returns a String
	 */
	public String getDescriptionCheck() {
		return descriptionCheck;
	}

	/**
	 * Sets the descriptionCheck
	 * 
	 * @param descriptionCheck
	 *            The descriptionCheck to set
	 */
	public void setDescriptionCheck(String descriptionCheck) {
		this.descriptionCheck = descriptionCheck;
	}

	/**
	 * Gets the activityNumberCheck
	 * 
	 * @return Returns a String
	 */
	public String getActivityNumberCheck() {
		return activityNumberCheck;
	}

	/**
	 * Sets the activityNumberCheck
	 * 
	 * @param activityNumberCheck
	 *            The activityNumberCheck to set
	 */
	public void setActivityNumberCheck(String activityNumberCheck) {
		this.activityNumberCheck = activityNumberCheck;
	}

	/**
	 * Gets the descriptionCond
	 * 
	 * @return Returns a String
	 */
	public String getDescriptionCond() {
		return descriptionCond;
	}

	/**
	 * Sets the descriptionCond
	 * 
	 * @param descriptionCond
	 *            The descriptionCond to set
	 */
	public void setDescriptionCond(String descriptionCond) {
		this.descriptionCond = descriptionCond;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		streetDirCheck = null;
		activityType = null;
		streetNumberCheck = null;
		streetFractionCond = null;
		activityTypeDescCond = null;
		streetNameCheck = null;
		streetNumberCond = null;
		streetName = null;
		streetDirCond = null;
		streetNameCond = null;
		fromDate = null;
		activityTypeDescCheck = null;
		sortBy = null;
		activityTypeCheck = null;
		activityTypeCond = null;
		activityNumberCond = null;
		streetFractionCheck = null;
		toDate = null;
		descriptionCheck = null;
		activityNumberCheck = null;
		descriptionCond = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

} // End class

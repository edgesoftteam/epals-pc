package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

public class StructureForm extends LsoForm {

	protected String lsoId;
	protected String alias;
	protected String description;
	protected String totalFloors;
	protected String use;
	protected boolean active;

	protected String apn;
	protected String ownerName;
	protected String ownerId;
	protected String apnStreetNumber;
	protected String apnPreDir;
	protected String apnStreetMod;
	protected String apnStreetName;
	protected String apnCity;
	protected String apnState;
	protected String apnZip;
	protected String foreignAddress = "N";
	protected String line1;
	protected String line4;
	protected String line3;
	protected String line2;
	protected String country;
	protected String email;
	protected String phone;
	protected String fax;
	protected String label;

	protected String[] selectedUse = {};

	public void reset(ActionMapping mapping, HttpServletRequest request) {

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the alias
	 * 
	 * @return Returns a String
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the alias
	 * 
	 * @param alias
	 *            The alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the totalFloors
	 * 
	 * @return Returns a String
	 */
	public String getTotalFloors() {
		return totalFloors;
	}

	/**
	 * Sets the totalFloors
	 * 
	 * @param totalFloors
	 *            The totalFloors to set
	 */
	public void setTotalFloors(String totalFloors) {
		this.totalFloors = totalFloors;
	}

	/**
	 * Gets the use
	 * 
	 * @return Returns a String
	 */
	public String getUse() {
		return use;
	}

	/**
	 * Sets the use
	 * 
	 * @param use
	 *            The use to set
	 */
	public void setUse(String use) {
		this.use = use;
	}

	/**
	 * Gets the active
	 * 
	 * @return Returns a boolean
	 */
	public boolean getActive() {
		return active;
	}

	/**
	 * Sets the active
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the selectedUse
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedUse() {
		return selectedUse;
	}

	/**
	 * Sets the selectedUse
	 * 
	 * @param selectedUse
	 *            The selectedUse to set
	 */
	public void setSelectedUse(String[] selectedUse) {
		this.selectedUse = selectedUse;
	}

	/**
	 * @return
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * @return
	 */
	public String getApnCity() {
		return apnCity;
	}

	/**
	 * @return
	 */
	public String getApnPreDir() {
		return apnPreDir;
	}

	/**
	 * @return
	 */
	public String getApnState() {
		return apnState;
	}

	/**
	 * @return
	 */
	public String getApnStreetMod() {
		return apnStreetMod;
	}

	/**
	 * @return
	 */
	public String getApnStreetName() {
		return apnStreetName;
	}

	/**
	 * @return
	 */
	public String getApnStreetNumber() {
		return apnStreetNumber;
	}

	/**
	 * @return
	 */
	public String getApnZip() {
		return apnZip;
	}

	/**
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @return
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @return
	 */
	public String getForeignAddress() {
		return foreignAddress;
	}

	/**
	 * @return
	 */
	public String getLine1() {
		return line1;
	}

	/**
	 * @return
	 */
	public String getLine2() {
		return line2;
	}

	/**
	 * @return
	 */
	public String getLine3() {
		return line3;
	}

	/**
	 * @return
	 */
	public String getLine4() {
		return line4;
	}

	/**
	 * @return
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * @return
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @return
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param string
	 */
	public void setApn(String string) {
		apn = string;
	}

	/**
	 * @param string
	 */
	public void setApnCity(String string) {
		apnCity = string;
	}

	/**
	 * @param string
	 */
	public void setApnPreDir(String string) {
		apnPreDir = string;
	}

	/**
	 * @param string
	 */
	public void setApnState(String string) {
		apnState = string;
	}

	/**
	 * @param string
	 */
	public void setApnStreetMod(String string) {
		apnStreetMod = string;
	}

	/**
	 * @param string
	 */
	public void setApnStreetName(String string) {
		apnStreetName = string;
	}

	/**
	 * @param string
	 */
	public void setApnStreetNumber(String string) {
		apnStreetNumber = string;
	}

	/**
	 * @param string
	 */
	public void setApnZip(String string) {
		apnZip = string;
	}

	/**
	 * @param string
	 */
	public void setCountry(String string) {
		country = string;
	}

	/**
	 * @param string
	 */
	public void setEmail(String string) {
		email = string;
	}

	/**
	 * @param string
	 */
	public void setFax(String string) {
		fax = string;
	}

	/**
	 * @param string
	 */
	public void setForeignAddress(String string) {
		foreignAddress = string;
	}

	/**
	 * @param string
	 */
	public void setLine1(String string) {
		line1 = string;
	}

	/**
	 * @param string
	 */
	public void setLine2(String string) {
		line2 = string;
	}

	/**
	 * @param string
	 */
	public void setLine3(String string) {
		line3 = string;
	}

	/**
	 * @param string
	 */
	public void setLine4(String string) {
		line4 = string;
	}

	/**
	 * @param string
	 */
	public void setOwnerId(String string) {
		ownerId = string;
	}

	/**
	 * @param string
	 */
	public void setOwnerName(String string) {
		ownerName = string;
	}

	/**
	 * @param string
	 */
	public void setPhone(String string) {
		phone = string;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

} // End class
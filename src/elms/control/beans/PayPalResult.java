package elms.control.beans;

import org.apache.struts.action.ActionForm;

public class PayPalResult extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String operation;
	private String tranId;
	private String amount;
	private String status;
	private String paymentStatus;
	private String pendingReason;
	private String cvv2code;
	private String avscode;

	private String errCode;
	private String errShortMsg;
	private String errLongMsg;
	private String currencyId;
	private String ack;

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getTranId() {
		return tranId;
	}

	public void setTranId(String tranId) {
		this.tranId = tranId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPendingReason() {
		return pendingReason;
	}

	public void setPendingReason(String pendingReason) {
		this.pendingReason = pendingReason;
	}

	public String getCvv2code() {
		return cvv2code;
	}

	public void setCvv2code(String cvv2code) {
		this.cvv2code = cvv2code;
	}

	public String getAvscode() {
		return avscode;
	}

	public void setAvscode(String avscode) {
		this.avscode = avscode;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrShortMsg() {
		return errShortMsg;
	}

	public void setErrShortMsg(String errShortMsg) {
		this.errShortMsg = errShortMsg;
	}

	public String getErrLongMsg() {
		return errLongMsg;
	}

	public void setErrLongMsg(String errLongMsg) {
		this.errLongMsg = errLongMsg;
	}

	public String getCurrencyId() {
		return currencyId;
	}

	public void setCurrencyId(String currencyId) {
		this.currencyId = currencyId;
	}

	public String getAck() {
		return ack;
	}

	public void setAck(String ack) {
		this.ack = ack;
	}
}

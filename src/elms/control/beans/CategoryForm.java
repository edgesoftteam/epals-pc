/*
 * Created on May 27, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans;

import java.io.Serializable;

/**
 * @author Akash
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CategoryForm implements Serializable {

	private int categoryId;
    private String categoryType;
    private String categoryValue;
    
    public CategoryForm(int categoryId, String categoryType, String categoryValue) {
        this.categoryId = categoryId;
        this.categoryType = categoryType;
        this.categoryValue = categoryValue;
    }
    
	/**
	 * @return Returns the categoryId.
	 */
	public int getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId The categoryId to set.
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return Returns the categoryType.
	 */
	public String getCategoryType() {
		return categoryType;
	}
	/**
	 * @param categoryType The categoryType to set.
	 */
	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}
	/**
	 * @return Returns the categoryValue.
	 */
	public String getCategoryValue() {
		return categoryValue;
	}
	/**
	 * @param categoryValue The categoryValue to set.
	 */
	public void setCategoryValue(String categoryValue) {
		this.categoryValue = categoryValue;
	}
}

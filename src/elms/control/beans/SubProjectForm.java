package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class SubProjectForm extends ActionForm {

	protected String subProject;
	protected String type;
	protected String subType;
	protected String description;
	protected String cancel;
	protected String status;
	protected String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	protected String caseLogId;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * Gets the subProject
	 * 
	 * @return Returns a String
	 */
	public String getSubProject() {
		return subProject;
	}

	/**
	 * Sets the subProject
	 * 
	 * @param subProject
	 *            The subProject to set
	 */
	public void setSubProject(String subProject) {
		this.subProject = subProject;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the subType
	 * 
	 * @return Returns a String
	 */
	public String getSubType() {
		return subType;
	}

	/**
	 * Sets the subType
	 * 
	 * @param subType
	 *            The subType to set
	 */
	public void setSubType(String subType) {
		this.subType = subType;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the cancel
	 * 
	 * @return Returns a String
	 */
	public String getCancel() {
		return cancel;
	}

	/**
	 * Sets the cancel
	 * 
	 * @param cancel
	 *            The cancel to set
	 */
	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return
	 */
	public String getCaseLogId() {
		return caseLogId;
	}

	/**
	 * @param string
	 */
	public void setCaseLogId(String string) {
		caseLogId = string;
	}

}
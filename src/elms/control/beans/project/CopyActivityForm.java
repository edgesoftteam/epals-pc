package elms.control.beans.project;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.project.CopyActivity;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class CopyActivityForm extends ActionForm {

	protected CopyActivity copyActivity;

	public CopyActivityForm() {
		super();
		this.copyActivity = new CopyActivity();
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (!(this.copyActivity == null)) {
			copyActivity.setHolds(false);
			copyActivity.setComments(false);
			copyActivity.setConditions(false);
		}
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		return errors;
	}

	/**
	 * Returns the copyActivity.
	 * 
	 * @return CopyActivity
	 */
	public CopyActivity getCopyActivity() {
		return copyActivity;
	}

	/**
	 * Sets the copyActivity.
	 * 
	 * @param copyActivity
	 *            The copyActivity to set
	 */
	public void setCopyActivity(CopyActivity copyActivity) {
		this.copyActivity = copyActivity;
	}

}

/*
 * Created on Apr 2, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.project;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import elms.app.project.ActivityHistory;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author 
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

	public class ActivityHistoryForm extends ActionForm {

	    static Logger logger = Logger.getLogger(ActivityHistoryForm.class.getName());

	    private String action;
	    private int levelId;
	    private String levelType;
	    private String address;
	    private String editable;
	    private String userId;
	    private String lastStatusId;
	    protected String pcApproved;
	    protected String permitFinal;
	    private String outStandingFees;
	    private String expirationDateLabel;
	    private String completionDateLabel;
	    protected String completionDate;
	    

		
		
	    private ActivityHistory[] statusList;
	    private ActivityHistory[] displayList;
	    
	    

		
	    public void reset(ActionMapping mapping, HttpServletRequest request) {
	        if (this.statusList != null)
	            for (int i = 0; i < statusList.length; i++) {
	                this.statusList[i].setUpdate("");
	                this.statusList[i].setUpdateActivities("");
	                this.statusList[i].setUpdateSubProject("");
	                this.statusList[i].setDeleteFlag("");
	            }
	    }

	    /**
	     * Returns the statusList.
	     * @return ActivityHistory[]]
	     */
	    public ActivityHistory[] getStatusList() {
	        return statusList;
	    }

	    /**
	     * Sets the statusList.
	     * @param statusList The statusList to set
	     */
	    public void setStatusList(ActivityHistory[] statusList) {
	        this.statusList = statusList;
	    }

	    public void setStatusList(List statuses) {
	        if (statuses == null) statuses = new ArrayList();
	        ActivityHistory[] activityHistoryArray = new ActivityHistory[statuses.size()];
	        Iterator iter = statuses.iterator();
	        int index = 0;
	        while (iter.hasNext()) {
	        	activityHistoryArray[index] = (ActivityHistory) iter.next();
	            index++;
	        }
	        this.statusList = activityHistoryArray;
	    }

	    /**
	     * @return
	     */
	    public String getAddress() {
	        return address;
	    }

	    /**
	     * @return
	     */
	    public int getLevelId() {
	        return levelId;
	    }

	    /**
	     * @return
	     */
	    public String getLevelType() {
	        return levelType;
	    }

	    /**
	     * @param string
	     */
	    public void setAddress(String string) {
	        address = string;
	    }

	    /**
	     * @param i
	     */
	    public void setLevelId(int i) {
	        levelId = i;
	    }

	    /**
	     * @param i
	     */
	    public void setLevelType(String string) {
	        levelType = string;
	    }

	    /**
	     * @return
	     */
	    public String getEditable() {
	        return editable;
	    }

	    /**
	     * @param string
	     */
	    public void setEditable(String string) {
	        editable = string;
	    }

	    /**
	     * @return
	     */
	    public String getUserId() {
	        return userId;
	    }

	    /**
	     * @param string
	     */
	    public void setUserId(String string) {
	        userId = string;
	    }

	    /**
	     * @return
	     */
	    public ActivityHistory[] getDisplayList() {
	        return displayList;
	    }

	    /**
	     * @param edits
	     */
	    public void setDisplayList(ActivityHistory[] edits) {
	        displayList = edits;
	    }

	    public void setDisplayList(List statuses) {
	        if (statuses == null) statuses = new ArrayList();
	        ActivityHistory[] activityHistoryArray = new ActivityHistory[statuses.size()];
	        Iterator iter = statuses.iterator();
	        int index = 0;
	        while (iter.hasNext()) {
	        	activityHistoryArray[index] = (ActivityHistory) iter.next();
	            index++;
	        }
	        this.displayList = activityHistoryArray;
	    }

	    /**
	     * @return
	     */
	    public String getLastStatusId() {
	        return lastStatusId;
	    }

	    /**
	     * @param string
	     */
	    public void setLastStatusId(String string) {
	        lastStatusId = string;
	    }

	    /**
	     * @return
	     */
	    public String getAction() {
	        return action;
	    }

	    /**
	     * @param string
	     */
	    public void setAction(String string) {
	        action = string;
	    }
	    /**
		 * @return Returns the pcApproved.
		 */
		public String getPcApproved() {
			return pcApproved;
		}
		/**
		 * @param pcApproved The pcApproved to set.
		 */
		public void setPcApproved(String pcApproved) {
			this.pcApproved = pcApproved;
		}
		/**
		 * @return Returns the permitFinal.
		 */
		public String getPermitFinal() {
			return permitFinal;
		}
		/**
		 * @param permitFinal The permitFinal to set.
		 */
		public void setPermitFinal(String permitFinal) {
			this.permitFinal = permitFinal;
		}
		
		/**
		 * @return Returns the outStandingFees.
		 */
		public String getOutStandingFees() {
			return outStandingFees;
		}
		/**
		 * @param outStandingFees The outStandingFees to set.
		 */
		public void setOutStandingFees(String outStandingFees) {
			this.outStandingFees = outStandingFees;
		}
		/**
		 * @return Returns the completionDateLabel.
		 */
		public String getCompletionDateLabel() {
			return completionDateLabel;
		}
		/**
		 * @param completionDateLabel The completionDateLabel to set.
		 */
		public void setCompletionDateLabel(String completionDateLabel) {
			this.completionDateLabel = completionDateLabel;
		}
		/**
		 * @return Returns the expirationDateLabel.
		 */
		public String getExpirationDateLabel() {
			return expirationDateLabel;
		}
		/**
		 * @param expirationDateLabel The expirationDateLabel to set.
		 */
		public void setExpirationDateLabel(String expirationDateLabel) {
			this.expirationDateLabel = expirationDateLabel;
		}
		
		/**
		 * @return Returns the completionDate.
		 */
		public String getCompletionDate() {
			return completionDate;
		}
		/**
		 * @param completionDate The completionDate to set.
		 */
		public void setCompletionDate(String completionDate) {
			this.completionDate = completionDate;
		}
	} //End class



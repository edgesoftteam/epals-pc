package elms.control.beans.project;

import elms.app.project.PlanCheckEngineer;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class PlanCheckReassignForm extends ActionForm{
	private PlanCheckEngineer[] pcEngineerArray;
	private int engineerId;
	private String deptDesc;
	private String manager;
    protected String fromDate;
    protected String toDate;
	

    
	public PlanCheckReassignForm() {
		this.engineerId = 0;
		this.deptDesc ="";
		this.manager = "N";
		this.fromDate = "";
		this.toDate = "";
		this.pcEngineerArray = new PlanCheckEngineer[0];
	}

    public void reset(ActionMapping mapping, HttpServletRequest request) {

    }

	/**
	 * @return the pcEngineerArray
	 */
	public PlanCheckEngineer[] getPcEngineerArray() {
		return pcEngineerArray;
	}

	/**
	 * @param pcEngineerArray the pcEngineerArray to set
	 */
	public void setPcEngineerArray(PlanCheckEngineer[] pcEngineerArray) {
		this.pcEngineerArray = pcEngineerArray;
	}

	/**
	 * @return the engineerId
	 */
	public int getEngineerId() {
		return engineerId;
	}

	/**
	 * @param engineerId the engineerId to set
	 */
	public void setEngineerId(int engineerId) {
		this.engineerId = engineerId;
	}

	/**
	 * @return the deptDesc
	 */
	public String getDeptDesc() {
		return deptDesc;
	}

	/**
	 * @param deptDesc the deptDesc to set
	 */
	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}

	/**
	 * @return the manager
	 */
	public String getManager() {
		return manager;
	}

	/**
	 * @param manager the manager to set
	 */
	public void setManager(String manager) {
		this.manager = manager;
	}

	/**
	 * @return the fromDate
	 */
	public String getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public String getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	 

}

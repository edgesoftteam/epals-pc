package elms.control.beans;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.util.StringUtils;

public class PaymentMgrForm extends ActionForm {

	static Logger logger = Logger.getLogger(PaymentMgrForm.class.getName());

	protected String levelId;
	protected String levelType;
	protected String method;
	protected String checkNoConfirmation;
	protected String planCheck;
	protected String developmentFees;
	protected String amount;
	protected String comments;

	protected String authorizedBy;
	protected String other;

	protected String permitFees;
	protected String penaltyFees;
	protected String businessTax;
	protected String otherText;
	protected String paidBy;
	protected String transactionType;

	protected FinanceSummary financeSummary;
	protected ActivityFeeEdit[] activityFeeList;
	protected ActivityFeeEdit[] businessTaxList;

	protected String backUrl;

	// Virtual Merchant Payment Procces for Online Hidden Variable declaration
	protected String ssl_merchant_id;
	protected String ssl_pin;
	protected String ssl_amount;
	protected String ssl_card_number;
	protected String creditCardType;
	protected String ssl_exp_date_month;
	protected String ssl_exp_date_year;
	protected String ssl_exp_date;
	protected String tempOnlineID;
	protected String comboNo;
	protected String termsCondition;

	protected String ssl_show_form;
	protected String ssl_transaction_type;
	protected String ssl_cvv2_indicator;
	protected String ssl_cvv2cvc2;

	// result
	protected String ssl_result_format;
	protected String ssl_receipt_decl_method;
	protected String ssl_receipt_decl_post_url;
	protected String ssl_receipt_apprvl_method;
	protected String ssl_receipt_apprvl_get_url;
	protected String ssl_receipt_link_text;

	// we recieve
	protected String ssl_result;
	protected String ssl_result_message;
	protected String ssl_txn_id;
	protected String ssl_approval_code;

	protected String ssl_user_id;
	protected String ssl_test_mode;
	protected String ssl_invoice_number;

	protected String ssl_avs_address;
	protected String ssl_avs_zip;
	protected String comboName;
	protected String pymntDate;
	protected String actStatus;
	protected String authorizedById;
	protected HashMap pcMap;
	protected String pcString;

	protected String ssl_city = "";
	protected String ssl_country = "";
	protected String ssl_state = "";
	protected String ssl_ownerFName = "";
	protected String ssl_ownerLName = "";

	
	protected String onlineTxnId = "";
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {

		otherText = null;
		paidBy = null;
		method = null;
		checkNoConfirmation = null;
		transactionType = null;

		planCheck = null;
		businessTax = null;
		amount = null;
		comments = null;
		authorizedBy = null;

		other = null;
		permitFees = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the method
	 * 
	 * @return Returns a String
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Sets the method
	 * 
	 * @param method
	 *            The method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Gets the checkNoConfirmation
	 * 
	 * @return Returns a String
	 */
	public String getCheckNoConfirmation() {
		return checkNoConfirmation;
	}

	/**
	 * Sets the checkNoConfirmation
	 * 
	 * @param checkNoConfirmation
	 *            The checkNoConfirmation to set
	 */
	public void setCheckNoConfirmation(String checkNoConfirmation) {
		this.checkNoConfirmation = checkNoConfirmation;
	}

	/**
	 * Gets the planCheck
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheck() {
		return planCheck;
	}

	/**
	 * Sets the planCheck
	 * 
	 * @param planCheck
	 *            The planCheck to set
	 */
	public void setPlanCheck(String planCheck) {
		this.planCheck = planCheck;
	}

	/**
	 * @return Returns the developmentFees.
	 */
	public String getDevelopmentFees() {
		return developmentFees;
	}

	/**
	 * @param developmentFees
	 *            The developmentFees to set.
	 */
	public void setDevelopmentFees(String developmentFees) {
		this.developmentFees = developmentFees;
	}

	/**
	 * Gets the amount
	 * 
	 * @return Returns a String
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Sets the amount
	 * 
	 * @param amount
	 *            The amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the authorizedBy
	 * 
	 * @return Returns a String
	 */
	public String getAuthorizedBy() {
		return authorizedBy;
	}

	/**
	 * Sets the authorizedBy
	 * 
	 * @param authorizedBy
	 *            The authorizedBy to set
	 */
	public void setAuthorizedBy(String authorizedBy) {
		this.authorizedBy = authorizedBy;
	}

	/**
	 * Gets the other
	 * 
	 * @return Returns a String
	 */
	public String getOther() {
		return other;
	}

	/**
	 * Sets the other
	 * 
	 * @param other
	 *            The other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}

	/**
	 * Gets the permitFees
	 * 
	 * @return Returns a String
	 */
	public String getPermitFees() {
		return permitFees;
	}

	/**
	 * Sets the permitFees
	 * 
	 * @param permitFees
	 *            The permitFees to set
	 */
	public void setPermitFees(String permitFees) {
		this.permitFees = permitFees;
	}

	/**
	 * @return Returns the penaltyFees.
	 */
	public String getPenaltyFees() {
		return penaltyFees;
	}

	/**
	 * @param penaltyFees
	 *            The penaltyFees to set.
	 */
	public void setPenaltyFees(String penaltyFees) {
		this.penaltyFees = penaltyFees;
	}

	/**
	 * Gets the businessTax
	 * 
	 * @return Returns a String
	 */
	public String getBusinessTax() {
		return businessTax;
	}

	/**
	 * Sets the businessTax
	 * 
	 * @param businessTax
	 *            The businessTax to set
	 */
	public void setBusinessTax(String businessTax) {
		this.businessTax = businessTax;
	}

	/**
	 * Gets the otherText
	 * 
	 * @return Returns a String
	 */
	public String getOtherText() {
		return otherText;
	}

	/**
	 * Sets the otherText
	 * 
	 * @param otherText
	 *            The otherText to set
	 */
	public void setOtherText(String otherText) {
		this.otherText = otherText;
	}

	/**
	 * Gets the paidBy
	 * 
	 * @return Returns a String
	 */
	public String getPaidBy() {
		return paidBy;
	}

	/**
	 * Sets the paidBy
	 * 
	 * @param paidBy
	 *            The paidBy to set
	 */
	public void setPaidBy(String paidBy) {
		this.paidBy = paidBy;
	}

	/**
	 * Gets the transactionType
	 * 
	 * @return Returns a String
	 */
	public String getTransactionType() {
		return transactionType;
	}

	/**
	 * Sets the transactionType
	 * 
	 * @param transactionType
	 *            The transactionType to set
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a String
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the levelType
	 * 
	 * @return Returns a String
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * Sets the levelType
	 * 
	 * @param levelType
	 *            The levelType to set
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	/**
	 * Gets the financeSummary
	 * 
	 * @return Returns a FinanceSummary
	 */
	public FinanceSummary getFinanceSummary() {
		return financeSummary;
	}

	/**
	 * Sets the financeSummary
	 * 
	 * @param financeSummary
	 *            The financeSummary to set
	 */
	public void setFinanceSummary(FinanceSummary financeSummary) {
		this.financeSummary = financeSummary;
	}

	/**
	 * Gets the activityFeeList
	 * 
	 * @return Returns a ActivityFeeEdit[]
	 */
	public ActivityFeeEdit[] getActivityFeeList() {
		return activityFeeList;
	}

	/**
	 * Sets the activityFeeList
	 * 
	 * @param activityFeeList
	 *            The activityFeeList to set
	 */
	public void setActivityFeeList(ActivityFeeEdit[] activityFeeList) {
		this.activityFeeList = activityFeeList;
	}

	public void setActivityFeeList(RowSet rs) {
		logger.info("setActivityFeeList(RowSet rs)");

		int i = 0;
		try {
			rs.beforeFirst();
			while (rs.next())
				i++;
			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i];
			i = -1;
			while (rs.next()) {
				ActivityFeeEdit af = new ActivityFeeEdit();
				af.setFeeId(rs.getString("fee_id"));
				af.setFeeDescription(rs.getString("fee_desc"));
				af.setLsoType(rs.getString("lso_use"));
				float amtDue = rs.getFloat("fee_amnt") - rs.getFloat("fee_paid") - rs.getFloat("fee_credit") - rs.getFloat("fee_adjustment") + rs.getFloat("bounced_amnt");
				af.setFeeAmount(StringUtils.f2$(amtDue));
				af.setFeeAmountValue(StringUtils.d2str(amtDue));
				af.setFeePaid(rs.getBigDecimal("fee_paid").toString());
				af.setBouncedAmount(rs.getBigDecimal("bounced_amnt").toString());
				af.setPaymentAmount("0.00");
				af.setFeeFlagFour(rs.getString("fee_fl_4"));
				af.setFeeAccount(rs.getString("fee_account"));
				afAry[++i] = af;
			}
			setActivityFeeList(afAry);
		} catch (Exception e) {
			logger.error("problem while setting activity fee list " + e.getMessage());
		}
	}

	/**
	 * Gets the businessTaxList
	 * 
	 * @return Returns a ActivityFeeEdit[]
	 */
	public ActivityFeeEdit[] getBusinessTaxList() {
		return businessTaxList;
	}

	/**
	 * Sets the businessTaxList
	 * 
	 * @param businessTaxList
	 *            The businessTaxList to set
	 */
	public void setBusinessTaxList(ActivityFeeEdit[] businessTaxList) {
		this.businessTaxList = businessTaxList;
	}

	public void setBusinessTaxList(RowSet rs) {
		logger.info("setBusinessTaxList(RowSet rs)");

		int i = 0;
		try {
			rs.beforeFirst();
			while (rs.next())
				i++;
			rs.beforeFirst();

			ActivityFeeEdit[] afAry = new ActivityFeeEdit[i];
			i = -1;
			while (rs.next()) {
				ActivityFeeEdit af = new ActivityFeeEdit();
				af.setActivityId(rs.getString("activity_id"));
				af.setFeeId(rs.getString("fee_id"));
				af.setPeopleId(rs.getString("people_id"));
				af.setName(rs.getString("name"));
				af.setLicenseNbr(rs.getString("lic_no"));
				float amtDue = rs.getFloat("fee_amnt") - rs.getFloat("fee_paid") - rs.getFloat("fee_credit") - rs.getFloat("fee_adjustment");
				af.setFeeAmount(StringUtils.f2$(amtDue));
				af.setFeeAmountValue(StringUtils.d2str(amtDue));
				// af.setFeePaid(StringUtils.f2$(rs.getFloat("fee_paid")));
				af.setFeePaid(rs.getBigDecimal("fee_paid").toString());
				af.setPaymentAmount("0.00");
				af.setFeeAccount(rs.getString("fee_account"));
				afAry[++i] = af;
			}
			setBusinessTaxList(afAry);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	/**
	 * @return
	 */
	public String getBackUrl() {
		return backUrl;
	}

	/**
	 * @param string
	 */
	public void setBackUrl(String string) {
		backUrl = string;
	}

	/**
	 * @return Returns the logger.
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger
	 *            The logger to set.
	 */
	public static void setLogger(Logger logger) {
		PaymentMgrForm.logger = logger;
	}

	/**
	 * @return Returns the actStatus.
	 */
	public String getActStatus() {
		return actStatus;
	}

	/**
	 * @param actStatus
	 *            The actStatus to set.
	 */
	public void setActStatus(String actStatus) {
		this.actStatus = actStatus;
	}

	/**
	 * @return Returns the authorizedById.
	 */
	public String getAuthorizedById() {
		return authorizedById;
	}

	/**
	 * @param authorizedById
	 *            The authorizedById to set.
	 */
	public void setAuthorizedById(String authorizedById) {
		this.authorizedById = authorizedById;
	}

	/**
	 * @return Returns the comboName.
	 */
	public String getComboName() {
		return comboName;
	}

	/**
	 * @param comboName
	 *            The comboName to set.
	 */
	public void setComboName(String comboName) {
		this.comboName = comboName;
	}

	/**
	 * @return Returns the comboNo.
	 */
	public String getComboNo() {
		return comboNo;
	}

	/**
	 * @param comboNo
	 *            The comboNo to set.
	 */
	public void setComboNo(String comboNo) {
		this.comboNo = comboNo;
	}

	/**
	 * @return Returns the creditCardType.
	 */
	public String getCreditCardType() {
		return creditCardType;
	}

	/**
	 * @param creditCardType
	 *            The creditCardType to set.
	 */
	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}

	/**
	 * @return Returns the pcMap.
	 */
	public HashMap getPcMap() {
		return pcMap;
	}

	/**
	 * @param pcMap
	 *            The pcMap to set.
	 */
	public void setPcMap(HashMap pcMap) {
		this.pcMap = pcMap;
	}

	/**
	 * @return Returns the pcString.
	 */
	public String getPcString() {
		return pcString;
	}

	/**
	 * @param pcString
	 *            The pcString to set.
	 */
	public void setPcString(String pcString) {
		this.pcString = pcString;
	}

	/**
	 * @return Returns the pymntDate.
	 */
	public String getPymntDate() {
		return pymntDate;
	}

	/**
	 * @param pymntDate
	 *            The pymntDate to set.
	 */
	public void setPymntDate(String pymntDate) {
		this.pymntDate = pymntDate;
	}

	/**
	 * @return Returns the ssl_amount.
	 */
	public String getSsl_amount() {
		return ssl_amount;
	}

	/**
	 * @param ssl_amount
	 *            The ssl_amount to set.
	 */
	public void setSsl_amount(String ssl_amount) {
		this.ssl_amount = ssl_amount;
	}

	/**
	 * @return Returns the ssl_approval_code.
	 */
	public String getSsl_approval_code() {
		return ssl_approval_code;
	}

	/**
	 * @param ssl_approval_code
	 *            The ssl_approval_code to set.
	 */
	public void setSsl_approval_code(String ssl_approval_code) {
		this.ssl_approval_code = ssl_approval_code;
	}

	/**
	 * @return Returns the ssl_avs_address.
	 */
	public String getSsl_avs_address() {
		return ssl_avs_address;
	}

	/**
	 * @param ssl_avs_address
	 *            The ssl_avs_address to set.
	 */
	public void setSsl_avs_address(String ssl_avs_address) {
		this.ssl_avs_address = ssl_avs_address;
	}

	/**
	 * @return Returns the ssl_avs_zip.
	 */
	public String getSsl_avs_zip() {
		return ssl_avs_zip;
	}

	/**
	 * @param ssl_avs_zip
	 *            The ssl_avs_zip to set.
	 */
	public void setSsl_avs_zip(String ssl_avs_zip) {
		this.ssl_avs_zip = ssl_avs_zip;
	}

	/**
	 * @return Returns the ssl_card_number.
	 */
	public String getSsl_card_number() {
		return ssl_card_number;
	}

	/**
	 * @param ssl_card_number
	 *            The ssl_card_number to set.
	 */
	public void setSsl_card_number(String ssl_card_number) {
		this.ssl_card_number = ssl_card_number;
	}

	/**
	 * @return Returns the ssl_cvv2_indicator.
	 */
	public String getSsl_cvv2_indicator() {
		return ssl_cvv2_indicator;
	}

	/**
	 * @param ssl_cvv2_indicator
	 *            The ssl_cvv2_indicator to set.
	 */
	public void setSsl_cvv2_indicator(String ssl_cvv2_indicator) {
		this.ssl_cvv2_indicator = ssl_cvv2_indicator;
	}

	/**
	 * @return Returns the ssl_cvv2cvc2.
	 */
	public String getSsl_cvv2cvc2() {
		return ssl_cvv2cvc2;
	}

	/**
	 * @param ssl_cvv2cvc2
	 *            The ssl_cvv2cvc2 to set.
	 */
	public void setSsl_cvv2cvc2(String ssl_cvv2cvc2) {
		this.ssl_cvv2cvc2 = ssl_cvv2cvc2;
	}

	/**
	 * @return Returns the ssl_exp_date.
	 */
	public String getSsl_exp_date() {
		return ssl_exp_date;
	}

	/**
	 * @param ssl_exp_date
	 *            The ssl_exp_date to set.
	 */
	public void setSsl_exp_date(String ssl_exp_date) {
		this.ssl_exp_date = ssl_exp_date;
	}

	/**
	 * @return Returns the ssl_invoice_number.
	 */
	public String getSsl_invoice_number() {
		return ssl_invoice_number;
	}

	/**
	 * @param ssl_invoice_number
	 *            The ssl_invoice_number to set.
	 */
	public void setSsl_invoice_number(String ssl_invoice_number) {
		this.ssl_invoice_number = ssl_invoice_number;
	}

	/**
	 * @return Returns the ssl_merchant_id.
	 */
	public String getSsl_merchant_id() {
		return ssl_merchant_id;
	}

	/**
	 * @param ssl_merchant_id
	 *            The ssl_merchant_id to set.
	 */
	public void setSsl_merchant_id(String ssl_merchant_id) {
		this.ssl_merchant_id = ssl_merchant_id;
	}

	/**
	 * @return Returns the ssl_pin.
	 */
	public String getSsl_pin() {
		return ssl_pin;
	}

	/**
	 * @param ssl_pin
	 *            The ssl_pin to set.
	 */
	public void setSsl_pin(String ssl_pin) {
		this.ssl_pin = ssl_pin;
	}

	/**
	 * @return Returns the ssl_receipt_apprvl_get_url.
	 */
	public String getSsl_receipt_apprvl_get_url() {
		return ssl_receipt_apprvl_get_url;
	}

	/**
	 * @param ssl_receipt_apprvl_get_url
	 *            The ssl_receipt_apprvl_get_url to set.
	 */
	public void setSsl_receipt_apprvl_get_url(String ssl_receipt_apprvl_get_url) {
		this.ssl_receipt_apprvl_get_url = ssl_receipt_apprvl_get_url;
	}

	/**
	 * @return Returns the ssl_receipt_apprvl_method.
	 */
	public String getSsl_receipt_apprvl_method() {
		return ssl_receipt_apprvl_method;
	}

	/**
	 * @param ssl_receipt_apprvl_method
	 *            The ssl_receipt_apprvl_method to set.
	 */
	public void setSsl_receipt_apprvl_method(String ssl_receipt_apprvl_method) {
		this.ssl_receipt_apprvl_method = ssl_receipt_apprvl_method;
	}

	/**
	 * @return Returns the ssl_receipt_decl_method.
	 */
	public String getSsl_receipt_decl_method() {
		return ssl_receipt_decl_method;
	}

	/**
	 * @param ssl_receipt_decl_method
	 *            The ssl_receipt_decl_method to set.
	 */
	public void setSsl_receipt_decl_method(String ssl_receipt_decl_method) {
		this.ssl_receipt_decl_method = ssl_receipt_decl_method;
	}

	/**
	 * @return Returns the ssl_receipt_decl_post_url.
	 */
	public String getSsl_receipt_decl_post_url() {
		return ssl_receipt_decl_post_url;
	}

	/**
	 * @param ssl_receipt_decl_post_url
	 *            The ssl_receipt_decl_post_url to set.
	 */
	public void setSsl_receipt_decl_post_url(String ssl_receipt_decl_post_url) {
		this.ssl_receipt_decl_post_url = ssl_receipt_decl_post_url;
	}

	/**
	 * @return Returns the ssl_receipt_link_text.
	 */
	public String getSsl_receipt_link_text() {
		return ssl_receipt_link_text;
	}

	/**
	 * @param ssl_receipt_link_text
	 *            The ssl_receipt_link_text to set.
	 */
	public void setSsl_receipt_link_text(String ssl_receipt_link_text) {
		this.ssl_receipt_link_text = ssl_receipt_link_text;
	}

	/**
	 * @return Returns the ssl_result.
	 */
	public String getSsl_result() {
		return ssl_result;
	}

	/**
	 * @param ssl_result
	 *            The ssl_result to set.
	 */
	public void setSsl_result(String ssl_result) {
		this.ssl_result = ssl_result;
	}

	/**
	 * @return Returns the ssl_result_format.
	 */
	public String getSsl_result_format() {
		return ssl_result_format;
	}

	/**
	 * @param ssl_result_format
	 *            The ssl_result_format to set.
	 */
	public void setSsl_result_format(String ssl_result_format) {
		this.ssl_result_format = ssl_result_format;
	}

	/**
	 * @return Returns the ssl_result_message.
	 */
	public String getSsl_result_message() {
		return ssl_result_message;
	}

	/**
	 * @param ssl_result_message
	 *            The ssl_result_message to set.
	 */
	public void setSsl_result_message(String ssl_result_message) {
		this.ssl_result_message = ssl_result_message;
	}

	/**
	 * @return Returns the ssl_show_form.
	 */
	public String getSsl_show_form() {
		return ssl_show_form;
	}

	/**
	 * @param ssl_show_form
	 *            The ssl_show_form to set.
	 */
	public void setSsl_show_form(String ssl_show_form) {
		this.ssl_show_form = ssl_show_form;
	}

	/**
	 * @return Returns the ssl_test_mode.
	 */
	public String getSsl_test_mode() {
		return ssl_test_mode;
	}

	/**
	 * @param ssl_test_mode
	 *            The ssl_test_mode to set.
	 */
	public void setSsl_test_mode(String ssl_test_mode) {
		this.ssl_test_mode = ssl_test_mode;
	}

	/**
	 * @return Returns the ssl_transaction_type.
	 */
	public String getSsl_transaction_type() {
		return ssl_transaction_type;
	}

	/**
	 * @param ssl_transaction_type
	 *            The ssl_transaction_type to set.
	 */
	public void setSsl_transaction_type(String ssl_transaction_type) {
		this.ssl_transaction_type = ssl_transaction_type;
	}

	/**
	 * @return Returns the ssl_txn_id.
	 */
	public String getSsl_txn_id() {
		return ssl_txn_id;
	}

	/**
	 * @param ssl_txn_id
	 *            The ssl_txn_id to set.
	 */
	public void setSsl_txn_id(String ssl_txn_id) {
		this.ssl_txn_id = ssl_txn_id;
	}

	/**
	 * @return Returns the ssl_user_id.
	 */
	public String getSsl_user_id() {
		return ssl_user_id;
	}

	/**
	 * @param ssl_user_id
	 *            The ssl_user_id to set.
	 */
	public void setSsl_user_id(String ssl_user_id) {
		this.ssl_user_id = ssl_user_id;
	}

	/**
	 * @return Returns the tempOnlineID.
	 */
	public String getTempOnlineID() {
		return tempOnlineID;
	}

	/**
	 * @param tempOnlineID
	 *            The tempOnlineID to set.
	 */
	public void setTempOnlineID(String tempOnlineID) {
		this.tempOnlineID = tempOnlineID;
	}

	/**
	 * @return Returns the termsCondition.
	 */
	public String getTermsCondition() {
		return termsCondition;
	}

	/**
	 * @param termsCondition
	 *            The termsCondition to set.
	 */
	public void setTermsCondition(String termsCondition) {
		this.termsCondition = termsCondition;
	}

	public String getSsl_exp_date_month() {
		return ssl_exp_date_month;
	}

	public void setSsl_exp_date_month(String ssl_exp_date_month) {
		this.ssl_exp_date_month = ssl_exp_date_month;
	}

	public String getSsl_exp_date_year() {
		return ssl_exp_date_year;
	}

	public void setSsl_exp_date_year(String ssl_exp_date_year) {
		this.ssl_exp_date_year = ssl_exp_date_year;
	}

	public String getSsl_city() {
		return ssl_city;
	}

	public void setSsl_city(String ssl_city) {
		this.ssl_city = ssl_city;
	}

	public String getSsl_country() {
		return ssl_country;
	}

	public void setSsl_country(String ssl_country) {
		this.ssl_country = ssl_country;
	}

	public String getSsl_ownerFName() {
		return ssl_ownerFName;
	}

	public void setSsl_ownerFName(String ssl_ownerFName) {
		this.ssl_ownerFName = ssl_ownerFName;
	}

	public String getSsl_ownerLName() {
		return ssl_ownerLName;
	}

	public void setSsl_ownerLName(String ssl_ownerLName) {
		this.ssl_ownerLName = ssl_ownerLName;
	}

	public String getSsl_state() {
		return ssl_state;
	}

	public void setSsl_state(String ssl_state) {
		this.ssl_state = ssl_state;
	}

	public String getOnlineTxnId() {
		return onlineTxnId;
	}

	public void setOnlineTxnId(String onlineTxnId) {
		this.onlineTxnId = onlineTxnId;
	}
} // End class
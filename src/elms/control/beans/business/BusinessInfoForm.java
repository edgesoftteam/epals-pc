/**
 * 
 */
package elms.control.beans.business;

import org.apache.struts.action.ActionForm;

/**
 * @author Gaurav Garg
 * 
 */

public class BusinessInfoForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// Common Business Information

	private int id;
	private String lsoId;
	private String dbaName;
	private String specUse;
	private String use;
	private String hourStart;
	private String hourEnd;
	private String mailingAddr;
	private String stateIdNo;
	private String type;
	private String noE;
	private String sicCode;
	private String facNo;
	private String gfdPin;
	private String epaId;
	private String businessCreateDate;
	private String businessUpdateDate;
	private String businessCloseDate;

	// Owner Information

	private int ownerId;
	private String ownerName;
	private String ownerAddress;
	private String ownerCity;
	private String ownerState = "CA";
	private String ownerZip;
	private String ownerPhoneHome;
	private String ownerPhoneHomeExt;
	private String ownerPhoneWork;
	private String ownerPhoneWorkExt;
	private String ownerPhoneCell;
	private String ownerPhoneCellExt;
	private String ownerPhonePager;
	private String ownerPhonePagerExt;
	private String ownerFax;
	private String ownerMailingAddress;
	private String ownerEmail;

	// Manager Information

	private int mgrId;
	private String mgrName;
	private String mgrAddress;
	private String mgrCity;
	private String mgrState = "CA";
	private String mgrZip;
	private String mgrPhoneHome;
	private String mgrPhoneHomeExt;
	private String mgrPhoneWork;
	private String mgrPhoneWorkExt;
	private String mgrPhoneCell;
	private String mgrPhoneCellExt;
	private String mgrPhonePager;
	private String mgrPhonePagerExt;
	private String mgrFax;
	private String mgrMailingAddress;
	private String mgrEmail;

	// Emergency Contact 1 Information

	private int ec1Id;
	private String ec1Name;
	private String ec1Address;
	private String ec1City;
	private String ec1State = "CA";
	private String ec1Zip;
	private String ec1PhoneHome;
	private String ec1PhoneHomeExt;
	private String ec1PhoneWork;
	private String ec1PhoneWorkExt;
	private String ec1PhoneCell;
	private String ec1PhoneCellExt;
	private String ec1PhonePager;
	private String ec1PhonePagerExt;
	private String ec1Fax;
	private String ec1MailingAddress;
	private String ec1Email;

	// Emergency Contact 2 Information

	private int ec2Id;
	private String ec2Name;
	private String ec2Address;
	private String ec2City;
	private String ec2State = "CA";
	private String ec2Zip;
	private String ec2PhoneHome;
	private String ec2PhoneHomeExt;
	private String ec2PhoneWork;
	private String ec2PhoneWorkExt;
	private String ec2PhoneCell;
	private String ec2PhoneCellExt;
	private String ec2PhonePager;
	private String ec2PhonePagerExt;
	private String ec2Fax;
	private String ec2MailingAddress;
	private String ec2Email;

	// Environmental Contact Information
	private int envId;
	private String envName;
	private String envAddress;
	private String envCity;
	private String envState = "CA";
	private String envZip;
	private String envPhoneHome;
	private String envPhoneHomeExt;
	private String envPhoneWork;
	private String envPhoneWorkExt;
	private String envPhoneCell;
	private String envPhoneCellExt;
	private String envPhonePager;
	private String envPhonePagerExt;
	private String envMailingAddress;
	private String envEmail;

	// Mailing contact Detail
	private String mailingName;
	private String mailingAddress;
	private String mailingCity;
	private String mailingState = "CA";
	private String mailingZip;
	private String mailingPhoneHome;
	private String mailingPhoneHomeExt;
	private String mailingPhoneWork;
	private String mailingPhoneWorkExt;
	private String mailingPhoneCell;
	private String mailingPhoneCellExt;
	private String mailingPhonePager;
	private String mailingFax;
	private String mailingPhonePagerExt;
	private String mailingEmail;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the lsoId
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * @param lsoId
	 *            the lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * @return the dbaName
	 */
	public String getDbaName() {
		return dbaName;
	}

	/**
	 * @param dbaName
	 *            the dbaName to set
	 */
	public void setDbaName(String dbaName) {
		this.dbaName = dbaName;
	}

	/**
	 * @return the specUse
	 */
	public String getSpecUse() {
		return specUse;
	}

	/**
	 * @param specUse
	 *            the specUse to set
	 */
	public void setSpecUse(String specUse) {
		this.specUse = specUse;
	}

	/**
	 * @return the use
	 */
	public String getUse() {
		return use;
	}

	/**
	 * @param use
	 *            the use to set
	 */
	public void setUse(String use) {
		this.use = use;
	}

	/**
	 * @return the hourStart
	 */
	public String getHourStart() {
		return hourStart;
	}

	/**
	 * @param hourStart
	 *            the hourStart to set
	 */
	public void setHourStart(String hourStart) {
		this.hourStart = hourStart;
	}

	/**
	 * @return the hourEnd
	 */
	public String getHourEnd() {
		return hourEnd;
	}

	/**
	 * @param hourEnd
	 *            the hourEnd to set
	 */
	public void setHourEnd(String hourEnd) {
		this.hourEnd = hourEnd;
	}

	/**
	 * @return the mailingAddr
	 */
	public String getMailingAddr() {
		return mailingAddr;
	}

	/**
	 * @param mailingAddr
	 *            the mailingAddr to set
	 */
	public void setMailingAddr(String mailingAddr) {
		this.mailingAddr = mailingAddr;
	}

	/**
	 * @return the stateIdNo
	 */
	public String getStateIdNo() {
		return stateIdNo;
	}

	/**
	 * @param stateIdNo
	 *            the stateIdNo to set
	 */
	public void setStateIdNo(String stateIdNo) {
		this.stateIdNo = stateIdNo;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the noE
	 */
	public String getNoE() {
		return noE;
	}

	/**
	 * @param noE
	 *            the noE to set
	 */
	public void setNoE(String noE) {
		this.noE = noE;
	}

	/**
	 * @return the sicCode
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            the sicCode to set
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	/**
	 * @return the facNo
	 */
	public String getFacNo() {
		return facNo;
	}

	/**
	 * @param facNo
	 *            the facNo to set
	 */
	public void setFacNo(String facNo) {
		this.facNo = facNo;
	}

	/**
	 * @return the gfdPin
	 */
	public String getGfdPin() {
		return gfdPin;
	}

	/**
	 * @param gfdPin
	 *            the gfdPin to set
	 */
	public void setGfdPin(String gfdPin) {
		this.gfdPin = gfdPin;
	}

	/**
	 * @return the ownerId
	 */
	public int getOwnerId() {
		return ownerId;
	}

	/**
	 * @param ownerId
	 *            the ownerId to set
	 */
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * @return the ownerName
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param ownerName
	 *            the ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * @return the ownerAddress
	 */
	public String getOwnerAddress() {
		return ownerAddress;
	}

	/**
	 * @param ownerAddress
	 *            the ownerAddress to set
	 */
	public void setOwnerAddress(String ownerAddress) {
		this.ownerAddress = ownerAddress;
	}

	/**
	 * @return the ownerCity
	 */
	public String getOwnerCity() {
		return ownerCity;
	}

	/**
	 * @param ownerCity
	 *            the ownerCity to set
	 */
	public void setOwnerCity(String ownerCity) {
		this.ownerCity = ownerCity;
	}

	/**
	 * @return the ownerState
	 */
	public String getOwnerState() {
		return ownerState;
	}

	/**
	 * @param ownerState
	 *            the ownerState to set
	 */
	public void setOwnerState(String ownerState) {
		this.ownerState = ownerState;
	}

	/**
	 * @return the ownerZip
	 */
	public String getOwnerZip() {
		return ownerZip;
	}

	/**
	 * @param ownerZip
	 *            the ownerZip to set
	 */
	public void setOwnerZip(String ownerZip) {
		this.ownerZip = ownerZip;
	}

	/**
	 * @return the ownerPhoneHome
	 */
	public String getOwnerPhoneHome() {
		return ownerPhoneHome;
	}

	/**
	 * @param ownerPhoneHome
	 *            the ownerPhoneHome to set
	 */
	public void setOwnerPhoneHome(String ownerPhoneHome) {
		this.ownerPhoneHome = ownerPhoneHome;
	}

	/**
	 * @return the ownerPhoneHomeExt
	 */
	public String getOwnerPhoneHomeExt() {
		return ownerPhoneHomeExt;
	}

	/**
	 * @param ownerPhoneHomeExt
	 *            the ownerPhoneHomeExt to set
	 */
	public void setOwnerPhoneHomeExt(String ownerPhoneHomeExt) {
		this.ownerPhoneHomeExt = ownerPhoneHomeExt;
	}

	/**
	 * @return the ownerPhoneWork
	 */
	public String getOwnerPhoneWork() {
		return ownerPhoneWork;
	}

	/**
	 * @param ownerPhoneWork
	 *            the ownerPhoneWork to set
	 */
	public void setOwnerPhoneWork(String ownerPhoneWork) {
		this.ownerPhoneWork = ownerPhoneWork;
	}

	/**
	 * @return the ownerPhoneWorkExt
	 */
	public String getOwnerPhoneWorkExt() {
		return ownerPhoneWorkExt;
	}

	/**
	 * @param ownerPhoneWorkExt
	 *            the ownerPhoneWorkExt to set
	 */
	public void setOwnerPhoneWorkExt(String ownerPhoneWorkExt) {
		this.ownerPhoneWorkExt = ownerPhoneWorkExt;
	}

	/**
	 * @return the ownerPhoneCell
	 */
	public String getOwnerPhoneCell() {
		return ownerPhoneCell;
	}

	/**
	 * @param ownerPhoneCell
	 *            the ownerPhoneCell to set
	 */
	public void setOwnerPhoneCell(String ownerPhoneCell) {
		this.ownerPhoneCell = ownerPhoneCell;
	}

	/**
	 * @return the ownerPhoneCellExt
	 */
	public String getOwnerPhoneCellExt() {
		return ownerPhoneCellExt;
	}

	/**
	 * @param ownerPhoneCellExt
	 *            the ownerPhoneCellExt to set
	 */
	public void setOwnerPhoneCellExt(String ownerPhoneCellExt) {
		this.ownerPhoneCellExt = ownerPhoneCellExt;
	}

	/**
	 * @return the ownerPhonePager
	 */
	public String getOwnerPhonePager() {
		return ownerPhonePager;
	}

	/**
	 * @param ownerPhonePager
	 *            the ownerPhonePager to set
	 */
	public void setOwnerPhonePager(String ownerPhonePager) {
		this.ownerPhonePager = ownerPhonePager;
	}

	/**
	 * @return the ownerPhonePagerExt
	 */
	public String getOwnerPhonePagerExt() {
		return ownerPhonePagerExt;
	}

	/**
	 * @param ownerPhonePagerExt
	 *            the ownerPhonePagerExt to set
	 */
	public void setOwnerPhonePagerExt(String ownerPhonePagerExt) {
		this.ownerPhonePagerExt = ownerPhonePagerExt;
	}

	/**
	 * @return the ownerFax
	 */
	public String getOwnerFax() {
		return ownerFax;
	}

	/**
	 * @param ownerFax
	 *            the ownerFax to set
	 */
	public void setOwnerFax(String ownerFax) {
		this.ownerFax = ownerFax;
	}

	/**
	 * @return the ownerMailingAddress
	 */
	public String getOwnerMailingAddress() {
		return ownerMailingAddress;
	}

	/**
	 * @param ownerMailingAddress
	 *            the ownerMailingAddress to set
	 */
	public void setOwnerMailingAddress(String ownerMailingAddress) {
		this.ownerMailingAddress = ownerMailingAddress;
	}

	/**
	 * @return the mgrId
	 */
	public int getMgrId() {
		return mgrId;
	}

	/**
	 * @param mgrId
	 *            the mgrId to set
	 */
	public void setMgrId(int mgrId) {
		this.mgrId = mgrId;
	}

	/**
	 * @return the mgrName
	 */
	public String getMgrName() {
		return mgrName;
	}

	/**
	 * @param mgrName
	 *            the mgrName to set
	 */
	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	/**
	 * @return the mgrAddress
	 */
	public String getMgrAddress() {
		return mgrAddress;
	}

	/**
	 * @param mgrAddress
	 *            the mgrAddress to set
	 */
	public void setMgrAddress(String mgrAddress) {
		this.mgrAddress = mgrAddress;
	}

	/**
	 * @return the mgrCity
	 */
	public String getMgrCity() {
		return mgrCity;
	}

	/**
	 * @param mgrCity
	 *            the mgrCity to set
	 */
	public void setMgrCity(String mgrCity) {
		this.mgrCity = mgrCity;
	}

	/**
	 * @return the mgrState
	 */
	public String getMgrState() {
		return mgrState;
	}

	/**
	 * @param mgrState
	 *            the mgrState to set
	 */
	public void setMgrState(String mgrState) {
		this.mgrState = mgrState;
	}

	/**
	 * @return the mgrZip
	 */
	public String getMgrZip() {
		return mgrZip;
	}

	/**
	 * @param mgrZip
	 *            the mgrZip to set
	 */
	public void setMgrZip(String mgrZip) {
		this.mgrZip = mgrZip;
	}

	/**
	 * @return the mgrPhoneHome
	 */
	public String getMgrPhoneHome() {
		return mgrPhoneHome;
	}

	/**
	 * @param mgrPhoneHome
	 *            the mgrPhoneHome to set
	 */
	public void setMgrPhoneHome(String mgrPhoneHome) {
		this.mgrPhoneHome = mgrPhoneHome;
	}

	/**
	 * @return the mgrPhoneHomeExt
	 */
	public String getMgrPhoneHomeExt() {
		return mgrPhoneHomeExt;
	}

	/**
	 * @param mgrPhoneHomeExt
	 *            the mgrPhoneHomeExt to set
	 */
	public void setMgrPhoneHomeExt(String mgrPhoneHomeExt) {
		this.mgrPhoneHomeExt = mgrPhoneHomeExt;
	}

	/**
	 * @return the mgrPhoneWork
	 */
	public String getMgrPhoneWork() {
		return mgrPhoneWork;
	}

	/**
	 * @param mgrPhoneWork
	 *            the mgrPhoneWork to set
	 */
	public void setMgrPhoneWork(String mgrPhoneWork) {
		this.mgrPhoneWork = mgrPhoneWork;
	}

	/**
	 * @return the mgrPhoneWorkExt
	 */
	public String getMgrPhoneWorkExt() {
		return mgrPhoneWorkExt;
	}

	/**
	 * @param mgrPhoneWorkExt
	 *            the mgrPhoneWorkExt to set
	 */
	public void setMgrPhoneWorkExt(String mgrPhoneWorkExt) {
		this.mgrPhoneWorkExt = mgrPhoneWorkExt;
	}

	/**
	 * @return the mgrPhoneCell
	 */
	public String getMgrPhoneCell() {
		return mgrPhoneCell;
	}

	/**
	 * @param mgrPhoneCell
	 *            the mgrPhoneCell to set
	 */
	public void setMgrPhoneCell(String mgrPhoneCell) {
		this.mgrPhoneCell = mgrPhoneCell;
	}

	/**
	 * @return the mgrPhoneCellExt
	 */
	public String getMgrPhoneCellExt() {
		return mgrPhoneCellExt;
	}

	/**
	 * @param mgrPhoneCellExt
	 *            the mgrPhoneCellExt to set
	 */
	public void setMgrPhoneCellExt(String mgrPhoneCellExt) {
		this.mgrPhoneCellExt = mgrPhoneCellExt;
	}

	/**
	 * @return the mgrPhonePager
	 */
	public String getMgrPhonePager() {
		return mgrPhonePager;
	}

	/**
	 * @param mgrPhonePager
	 *            the mgrPhonePager to set
	 */
	public void setMgrPhonePager(String mgrPhonePager) {
		this.mgrPhonePager = mgrPhonePager;
	}

	/**
	 * @return the mgrPhonePagerExt
	 */
	public String getMgrPhonePagerExt() {
		return mgrPhonePagerExt;
	}

	/**
	 * @param mgrPhonePagerExt
	 *            the mgrPhonePagerExt to set
	 */
	public void setMgrPhonePagerExt(String mgrPhonePagerExt) {
		this.mgrPhonePagerExt = mgrPhonePagerExt;
	}

	/**
	 * @return the mgrFax
	 */
	public String getMgrFax() {
		return mgrFax;
	}

	/**
	 * @param mgrFax
	 *            the mgrFax to set
	 */
	public void setMgrFax(String mgrFax) {
		this.mgrFax = mgrFax;
	}

	/**
	 * @return the mgrMailingAddress
	 */
	public String getMgrMailingAddress() {
		return mgrMailingAddress;
	}

	/**
	 * @param mgrMailingAddress
	 *            the mgrMailingAddress to set
	 */
	public void setMgrMailingAddress(String mgrMailingAddress) {
		this.mgrMailingAddress = mgrMailingAddress;
	}

	/**
	 * @return the ec1Id
	 */
	public int getEc1Id() {
		return ec1Id;
	}

	/**
	 * @param ec1Id
	 *            the ec1Id to set
	 */
	public void setEc1Id(int ec1Id) {
		this.ec1Id = ec1Id;
	}

	/**
	 * @return the ec1Name
	 */
	public String getEc1Name() {
		return ec1Name;
	}

	/**
	 * @param ec1Name
	 *            the ec1Name to set
	 */
	public void setEc1Name(String ec1Name) {
		this.ec1Name = ec1Name;
	}

	/**
	 * @return the ec1Address
	 */
	public String getEc1Address() {
		return ec1Address;
	}

	/**
	 * @param ec1Address
	 *            the ec1Address to set
	 */
	public void setEc1Address(String ec1Address) {
		this.ec1Address = ec1Address;
	}

	/**
	 * @return the ec1City
	 */
	public String getEc1City() {
		return ec1City;
	}

	/**
	 * @param ec1City
	 *            the ec1City to set
	 */
	public void setEc1City(String ec1City) {
		this.ec1City = ec1City;
	}

	/**
	 * @return the ec1State
	 */
	public String getEc1State() {
		return ec1State;
	}

	/**
	 * @param ec1State
	 *            the ec1State to set
	 */
	public void setEc1State(String ec1State) {
		this.ec1State = ec1State;
	}

	/**
	 * @return the ec1Zip
	 */
	public String getEc1Zip() {
		return ec1Zip;
	}

	/**
	 * @param ec1Zip
	 *            the ec1Zip to set
	 */
	public void setEc1Zip(String ec1Zip) {
		this.ec1Zip = ec1Zip;
	}

	/**
	 * @return the ec1PhoneHome
	 */
	public String getEc1PhoneHome() {
		return ec1PhoneHome;
	}

	/**
	 * @param ec1PhoneHome
	 *            the ec1PhoneHome to set
	 */
	public void setEc1PhoneHome(String ec1PhoneHome) {
		this.ec1PhoneHome = ec1PhoneHome;
	}

	/**
	 * @return the ec1PhoneHomeExt
	 */
	public String getEc1PhoneHomeExt() {
		return ec1PhoneHomeExt;
	}

	/**
	 * @param ec1PhoneHomeExt
	 *            the ec1PhoneHomeExt to set
	 */
	public void setEc1PhoneHomeExt(String ec1PhoneHomeExt) {
		this.ec1PhoneHomeExt = ec1PhoneHomeExt;
	}

	/**
	 * @return the ec1PhoneWork
	 */
	public String getEc1PhoneWork() {
		return ec1PhoneWork;
	}

	/**
	 * @param ec1PhoneWork
	 *            the ec1PhoneWork to set
	 */
	public void setEc1PhoneWork(String ec1PhoneWork) {
		this.ec1PhoneWork = ec1PhoneWork;
	}

	/**
	 * @return the ec1PhoneWorkExt
	 */
	public String getEc1PhoneWorkExt() {
		return ec1PhoneWorkExt;
	}

	/**
	 * @param ec1PhoneWorkExt
	 *            the ec1PhoneWorkExt to set
	 */
	public void setEc1PhoneWorkExt(String ec1PhoneWorkExt) {
		this.ec1PhoneWorkExt = ec1PhoneWorkExt;
	}

	/**
	 * @return the ec1PhoneCell
	 */
	public String getEc1PhoneCell() {
		return ec1PhoneCell;
	}

	/**
	 * @param ec1PhoneCell
	 *            the ec1PhoneCell to set
	 */
	public void setEc1PhoneCell(String ec1PhoneCell) {
		this.ec1PhoneCell = ec1PhoneCell;
	}

	/**
	 * @return the ec1PhoneCellExt
	 */
	public String getEc1PhoneCellExt() {
		return ec1PhoneCellExt;
	}

	/**
	 * @param ec1PhoneCellExt
	 *            the ec1PhoneCellExt to set
	 */
	public void setEc1PhoneCellExt(String ec1PhoneCellExt) {
		this.ec1PhoneCellExt = ec1PhoneCellExt;
	}

	/**
	 * @return the ec1PhonePager
	 */
	public String getEc1PhonePager() {
		return ec1PhonePager;
	}

	/**
	 * @param ec1PhonePager
	 *            the ec1PhonePager to set
	 */
	public void setEc1PhonePager(String ec1PhonePager) {
		this.ec1PhonePager = ec1PhonePager;
	}

	/**
	 * @return the ec1PhonePagerExt
	 */
	public String getEc1PhonePagerExt() {
		return ec1PhonePagerExt;
	}

	/**
	 * @param ec1PhonePagerExt
	 *            the ec1PhonePagerExt to set
	 */
	public void setEc1PhonePagerExt(String ec1PhonePagerExt) {
		this.ec1PhonePagerExt = ec1PhonePagerExt;
	}

	/**
	 * @return the ec1Fax
	 */
	public String getEc1Fax() {
		return ec1Fax;
	}

	/**
	 * @param ec1Fax
	 *            the ec1Fax to set
	 */
	public void setEc1Fax(String ec1Fax) {
		this.ec1Fax = ec1Fax;
	}

	/**
	 * @return the ec1MailingAddress
	 */
	public String getEc1MailingAddress() {
		return ec1MailingAddress;
	}

	/**
	 * @param ec1MailingAddress
	 *            the ec1MailingAddress to set
	 */
	public void setEc1MailingAddress(String ec1MailingAddress) {
		this.ec1MailingAddress = ec1MailingAddress;
	}

	/**
	 * @return the ec2Id
	 */
	public int getEc2Id() {
		return ec2Id;
	}

	/**
	 * @param ec2Id
	 *            the ec2Id to set
	 */
	public void setEc2Id(int ec2Id) {
		this.ec2Id = ec2Id;
	}

	/**
	 * @return the ec2Name
	 */
	public String getEc2Name() {
		return ec2Name;
	}

	/**
	 * @param ec2Name
	 *            the ec2Name to set
	 */
	public void setEc2Name(String ec2Name) {
		this.ec2Name = ec2Name;
	}

	/**
	 * @return the ec2Address
	 */
	public String getEc2Address() {
		return ec2Address;
	}

	/**
	 * @param ec2Address
	 *            the ec2Address to set
	 */
	public void setEc2Address(String ec2Address) {
		this.ec2Address = ec2Address;
	}

	/**
	 * @return the ec2City
	 */
	public String getEc2City() {
		return ec2City;
	}

	/**
	 * @param ec2City
	 *            the ec2City to set
	 */
	public void setEc2City(String ec2City) {
		this.ec2City = ec2City;
	}

	/**
	 * @return the ec2State
	 */
	public String getEc2State() {
		return ec2State;
	}

	/**
	 * @param ec2State
	 *            the ec2State to set
	 */
	public void setEc2State(String ec2State) {
		this.ec2State = ec2State;
	}

	/**
	 * @return the ec2Zip
	 */
	public String getEc2Zip() {
		return ec2Zip;
	}

	/**
	 * @param ec2Zip
	 *            the ec2Zip to set
	 */
	public void setEc2Zip(String ec2Zip) {
		this.ec2Zip = ec2Zip;
	}

	/**
	 * @return the ec2PhoneHome
	 */
	public String getEc2PhoneHome() {
		return ec2PhoneHome;
	}

	/**
	 * @param ec2PhoneHome
	 *            the ec2PhoneHome to set
	 */
	public void setEc2PhoneHome(String ec2PhoneHome) {
		this.ec2PhoneHome = ec2PhoneHome;
	}

	/**
	 * @return the ec2PhoneHomeExt
	 */
	public String getEc2PhoneHomeExt() {
		return ec2PhoneHomeExt;
	}

	/**
	 * @param ec2PhoneHomeExt
	 *            the ec2PhoneHomeExt to set
	 */
	public void setEc2PhoneHomeExt(String ec2PhoneHomeExt) {
		this.ec2PhoneHomeExt = ec2PhoneHomeExt;
	}

	/**
	 * @return the ec2PhoneWork
	 */
	public String getEc2PhoneWork() {
		return ec2PhoneWork;
	}

	/**
	 * @param ec2PhoneWork
	 *            the ec2PhoneWork to set
	 */
	public void setEc2PhoneWork(String ec2PhoneWork) {
		this.ec2PhoneWork = ec2PhoneWork;
	}

	/**
	 * @return the ec2PhoneWorkExt
	 */
	public String getEc2PhoneWorkExt() {
		return ec2PhoneWorkExt;
	}

	/**
	 * @param ec2PhoneWorkExt
	 *            the ec2PhoneWorkExt to set
	 */
	public void setEc2PhoneWorkExt(String ec2PhoneWorkExt) {
		this.ec2PhoneWorkExt = ec2PhoneWorkExt;
	}

	/**
	 * @return the ec2PhoneCell
	 */
	public String getEc2PhoneCell() {
		return ec2PhoneCell;
	}

	/**
	 * @param ec2PhoneCell
	 *            the ec2PhoneCell to set
	 */
	public void setEc2PhoneCell(String ec2PhoneCell) {
		this.ec2PhoneCell = ec2PhoneCell;
	}

	/**
	 * @return the ec2PhoneCellExt
	 */
	public String getEc2PhoneCellExt() {
		return ec2PhoneCellExt;
	}

	/**
	 * @param ec2PhoneCellExt
	 *            the ec2PhoneCellExt to set
	 */
	public void setEc2PhoneCellExt(String ec2PhoneCellExt) {
		this.ec2PhoneCellExt = ec2PhoneCellExt;
	}

	/**
	 * @return the ec2PhonePager
	 */
	public String getEc2PhonePager() {
		return ec2PhonePager;
	}

	/**
	 * @param ec2PhonePager
	 *            the ec2PhonePager to set
	 */
	public void setEc2PhonePager(String ec2PhonePager) {
		this.ec2PhonePager = ec2PhonePager;
	}

	/**
	 * @return the ec2PhonePagerExt
	 */
	public String getEc2PhonePagerExt() {
		return ec2PhonePagerExt;
	}

	/**
	 * @param ec2PhonePagerExt
	 *            the ec2PhonePagerExt to set
	 */
	public void setEc2PhonePagerExt(String ec2PhonePagerExt) {
		this.ec2PhonePagerExt = ec2PhonePagerExt;
	}

	/**
	 * @return the ec2Fax
	 */
	public String getEc2Fax() {
		return ec2Fax;
	}

	/**
	 * @param ec2Fax
	 *            the ec2Fax to set
	 */
	public void setEc2Fax(String ec2Fax) {
		this.ec2Fax = ec2Fax;
	}

	/**
	 * @return the ec2MailingAddress
	 */
	public String getEc2MailingAddress() {
		return ec2MailingAddress;
	}

	/**
	 * @param ec2MailingAddress
	 *            the ec2MailingAddress to set
	 */
	public void setEc2MailingAddress(String ec2MailingAddress) {
		this.ec2MailingAddress = ec2MailingAddress;
	}

	/**
	 * @return the serialVersionUID
	 */
	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	/**
	 * @return the envId
	 */
	public int getEnvId() {
		return envId;
	}

	/**
	 * @param envId
	 *            the envId to set
	 */
	public void setEnvId(int envId) {
		this.envId = envId;
	}

	/**
	 * @return the envName
	 */
	public String getEnvName() {
		return envName;
	}

	/**
	 * @param envName
	 *            the envName to set
	 */
	public void setEnvName(String envName) {
		this.envName = envName;
	}

	/**
	 * @return the envAddress
	 */
	public String getEnvAddress() {
		return envAddress;
	}

	/**
	 * @param envAddress
	 *            the envAddress to set
	 */
	public void setEnvAddress(String envAddress) {
		this.envAddress = envAddress;
	}

	/**
	 * @return the envCity
	 */
	public String getEnvCity() {
		return envCity;
	}

	/**
	 * @param envCity
	 *            the envCity to set
	 */
	public void setEnvCity(String envCity) {
		this.envCity = envCity;
	}

	/**
	 * @return the envState
	 */
	public String getEnvState() {
		return envState;
	}

	/**
	 * @param envState
	 *            the envState to set
	 */
	public void setEnvState(String envState) {
		this.envState = envState;
	}

	/**
	 * @return the envZip
	 */
	public String getEnvZip() {
		return envZip;
	}

	/**
	 * @param envZip
	 *            the envZip to set
	 */
	public void setEnvZip(String envZip) {
		this.envZip = envZip;
	}

	/**
	 * @return the envPhoneHome
	 */
	public String getEnvPhoneHome() {
		return envPhoneHome;
	}

	/**
	 * @param envPhoneHome
	 *            the envPhoneHome to set
	 */
	public void setEnvPhoneHome(String envPhoneHome) {
		this.envPhoneHome = envPhoneHome;
	}

	/**
	 * @return the envPhoneHomeExt
	 */
	public String getEnvPhoneHomeExt() {
		return envPhoneHomeExt;
	}

	/**
	 * @param envPhoneHomeExt
	 *            the envPhoneHomeExt to set
	 */
	public void setEnvPhoneHomeExt(String envPhoneHomeExt) {
		this.envPhoneHomeExt = envPhoneHomeExt;
	}

	/**
	 * @return the envPhoneWork
	 */
	public String getEnvPhoneWork() {
		return envPhoneWork;
	}

	/**
	 * @param envPhoneWork
	 *            the envPhoneWork to set
	 */
	public void setEnvPhoneWork(String envPhoneWork) {
		this.envPhoneWork = envPhoneWork;
	}

	/**
	 * @return the envPhoneWorkExt
	 */
	public String getEnvPhoneWorkExt() {
		return envPhoneWorkExt;
	}

	/**
	 * @param envPhoneWorkExt
	 *            the envPhoneWorkExt to set
	 */
	public void setEnvPhoneWorkExt(String envPhoneWorkExt) {
		this.envPhoneWorkExt = envPhoneWorkExt;
	}

	/**
	 * @return the envPhoneCell
	 */
	public String getEnvPhoneCell() {
		return envPhoneCell;
	}

	/**
	 * @param envPhoneCell
	 *            the envPhoneCell to set
	 */
	public void setEnvPhoneCell(String envPhoneCell) {
		this.envPhoneCell = envPhoneCell;
	}

	/**
	 * @return the envPhoneCellExt
	 */
	public String getEnvPhoneCellExt() {
		return envPhoneCellExt;
	}

	/**
	 * @param envPhoneCellExt
	 *            the envPhoneCellExt to set
	 */
	public void setEnvPhoneCellExt(String envPhoneCellExt) {
		this.envPhoneCellExt = envPhoneCellExt;
	}

	/**
	 * @return the envPhonePager
	 */
	public String getEnvPhonePager() {
		return envPhonePager;
	}

	/**
	 * @param envPhonePager
	 *            the envPhonePager to set
	 */
	public void setEnvPhonePager(String envPhonePager) {
		this.envPhonePager = envPhonePager;
	}

	/**
	 * @return the envPhonePagerExt
	 */
	public String getEnvPhonePagerExt() {
		return envPhonePagerExt;
	}

	/**
	 * @param envPhonePagerExt
	 *            the envPhonePagerExt to set
	 */
	public void setEnvPhonePagerExt(String envPhonePagerExt) {
		this.envPhonePagerExt = envPhonePagerExt;
	}

	/**
	 * @return the envMailingAddress
	 */
	public String getEnvMailingAddress() {
		return envMailingAddress;
	}

	/**
	 * @param envMailingAddress
	 *            the envMailingAddress to set
	 */
	public void setEnvMailingAddress(String envMailingAddress) {
		this.envMailingAddress = envMailingAddress;
	}

	/**
	 * @return the epaId
	 */
	public String getEpaId() {
		return epaId;
	}

	/**
	 * @param epaId
	 *            the epaId to set
	 */
	public void setEpaId(String epaId) {
		this.epaId = epaId;
	}

	/**
	 * @return the mailingName
	 */
	public String getMailingName() {
		return mailingName;
	}

	/**
	 * @param mailingName
	 *            the mailingName to set
	 */
	public void setMailingName(String mailingName) {
		this.mailingName = mailingName;
	}

	/**
	 * @return the mailingAddress
	 */
	public String getMailingAddress() {
		return mailingAddress;
	}

	/**
	 * @param mailingAddress
	 *            the mailingAddress to set
	 */
	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	/**
	 * @return the mailingCity
	 */
	public String getMailingCity() {
		return mailingCity;
	}

	/**
	 * @param mailingCity
	 *            the mailingCity to set
	 */
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	/**
	 * @return the mailingState
	 */
	public String getMailingState() {
		return mailingState;
	}

	/**
	 * @param mailingState
	 *            the mailingState to set
	 */
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	/**
	 * @return the mailingZip
	 */
	public String getMailingZip() {
		return mailingZip;
	}

	/**
	 * @param mailingZip
	 *            the mailingZip to set
	 */
	public void setMailingZip(String mailingZip) {
		this.mailingZip = mailingZip;
	}

	/**
	 * @return the mailingPhoneHome
	 */
	public String getMailingPhoneHome() {
		return mailingPhoneHome;
	}

	/**
	 * @param mailingPhoneHome
	 *            the mailingPhoneHome to set
	 */
	public void setMailingPhoneHome(String mailingPhoneHome) {
		this.mailingPhoneHome = mailingPhoneHome;
	}

	/**
	 * @return the mailingPhoneHomeExt
	 */
	public String getMailingPhoneHomeExt() {
		return mailingPhoneHomeExt;
	}

	/**
	 * @param mailingPhoneHomeExt
	 *            the mailingPhoneHomeExt to set
	 */
	public void setMailingPhoneHomeExt(String mailingPhoneHomeExt) {
		this.mailingPhoneHomeExt = mailingPhoneHomeExt;
	}

	/**
	 * @return the mailingPhoneWork
	 */
	public String getMailingPhoneWork() {
		return mailingPhoneWork;
	}

	/**
	 * @param mailingPhoneWork
	 *            the mailingPhoneWork to set
	 */
	public void setMailingPhoneWork(String mailingPhoneWork) {
		this.mailingPhoneWork = mailingPhoneWork;
	}

	/**
	 * @return the mailingPhoneWorkExt
	 */
	public String getMailingPhoneWorkExt() {
		return mailingPhoneWorkExt;
	}

	/**
	 * @param mailingPhoneWorkExt
	 *            the mailingPhoneWorkExt to set
	 */
	public void setMailingPhoneWorkExt(String mailingPhoneWorkExt) {
		this.mailingPhoneWorkExt = mailingPhoneWorkExt;
	}

	/**
	 * @return the mailingPhoneCell
	 */
	public String getMailingPhoneCell() {
		return mailingPhoneCell;
	}

	/**
	 * @param mailingPhoneCell
	 *            the mailingPhoneCell to set
	 */
	public void setMailingPhoneCell(String mailingPhoneCell) {
		this.mailingPhoneCell = mailingPhoneCell;
	}

	/**
	 * @return the mailingPhoneCellExt
	 */
	public String getMailingPhoneCellExt() {
		return mailingPhoneCellExt;
	}

	/**
	 * @param mailingPhoneCellExt
	 *            the mailingPhoneCellExt to set
	 */
	public void setMailingPhoneCellExt(String mailingPhoneCellExt) {
		this.mailingPhoneCellExt = mailingPhoneCellExt;
	}

	/**
	 * @return the mailingPhonePager
	 */
	public String getMailingPhonePager() {
		return mailingPhonePager;
	}

	/**
	 * @param mailingPhonePager
	 *            the mailingPhonePager to set
	 */
	public void setMailingPhonePager(String mailingPhonePager) {
		this.mailingPhonePager = mailingPhonePager;
	}

	/**
	 * @return the mailingPhonePagerExt
	 */
	public String getMailingPhonePagerExt() {
		return mailingPhonePagerExt;
	}

	/**
	 * @param mailingPhonePagerExt
	 *            the mailingPhonePagerExt to set
	 */
	public void setMailingPhonePagerExt(String mailingPhonePagerExt) {
		this.mailingPhonePagerExt = mailingPhonePagerExt;
	}

	/**
	 * @return the mailingFax
	 */
	public String getMailingFax() {
		return mailingFax;
	}

	/**
	 * @param mailingFax
	 *            the mailingFax to set
	 */
	public void setMailingFax(String mailingFax) {
		this.mailingFax = mailingFax;
	}

	/**
	 * @return the ownerEmail
	 */
	public String getOwnerEmail() {
		return ownerEmail;
	}

	/**
	 * @param ownerEmail
	 *            the ownerEmail to set
	 */
	public void setOwnerEmail(String ownerEmail) {
		this.ownerEmail = ownerEmail;
	}

	/**
	 * @return the mgrEmail
	 */
	public String getMgrEmail() {
		return mgrEmail;
	}

	/**
	 * @param mgrEmail
	 *            the mgrEmail to set
	 */
	public void setMgrEmail(String mgrEmail) {
		this.mgrEmail = mgrEmail;
	}

	/**
	 * @return the ec1Email
	 */
	public String getEc1Email() {
		return ec1Email;
	}

	/**
	 * @param ec1Email
	 *            the ec1Email to set
	 */
	public void setEc1Email(String ec1Email) {
		this.ec1Email = ec1Email;
	}

	/**
	 * @return the ec2Email
	 */
	public String getEc2Email() {
		return ec2Email;
	}

	/**
	 * @param ec2Email
	 *            the ec2Email to set
	 */
	public void setEc2Email(String ec2Email) {
		this.ec2Email = ec2Email;
	}

	/**
	 * @return the envEmail
	 */
	public String getEnvEmail() {
		return envEmail;
	}

	/**
	 * @param envEmail
	 *            the envEmail to set
	 */
	public void setEnvEmail(String envEmail) {
		this.envEmail = envEmail;
	}

	/**
	 * @return the mailingEmail
	 */
	public String getMailingEmail() {
		return mailingEmail;
	}

	/**
	 * @param mailingEmail
	 *            the mailingEmail to set
	 */
	public void setMailingEmail(String mailingEmail) {
		this.mailingEmail = mailingEmail;
	}

	/**
	 * @return the businessCreateDate
	 */
	public String getBusinessCreateDate() {
		return businessCreateDate;
	}

	/**
	 * @param businessCreateDate
	 *            the businessCreateDate to set
	 */
	public void setBusinessCreateDate(String businessCreateDate) {
		this.businessCreateDate = businessCreateDate;
	}

	/**
	 * @return the businessUpdateDate
	 */
	public String getBusinessUpdateDate() {
		return businessUpdateDate;
	}

	/**
	 * @param businessUpdateDate
	 *            the businessUpdateDate to set
	 */
	public void setBusinessUpdateDate(String businessUpdateDate) {
		this.businessUpdateDate = businessUpdateDate;
	}

	/**
	 * @return the businessCloseDate
	 */
	public String getBusinessCloseDate() {
		return businessCloseDate;
	}

	/**
	 * @param businessCloseDate
	 *            the businessCloseDate to set
	 */
	public void setBusinessCloseDate(String businessCloseDate) {
		this.businessCloseDate = businessCloseDate;
	}

}
package elms.control.beans;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.admin.CustomField;
import elms.app.project.GreenHalo;

public class GreenHaloForm extends ActionForm {
	static Logger logger = Logger.getLogger(GreenHaloForm.class.getName());
	/**
	 * Member variable declaration
	 */
	protected GreenHalo greenHalo;
	
	protected CustomField[] customFieldList;

	/**
	 * @return greenHalo.
	 */
	public GreenHalo getGreenHalo() {
		return greenHalo;
	}

	/**
	 * @param greenHalo.
	 */
	public void setGreenHalo(GreenHalo greenHalo) {
		this.greenHalo = greenHalo;
	}

	/**
	 * @return the customFieldList
	 */
	public CustomField[] getCustomFieldList() {
		return customFieldList;
	}

	/**
	 * @param customFieldList the customFieldList to set
	 */
	public void setCustomFieldList(CustomField[] customFieldList) {
		this.customFieldList = customFieldList;
	}
}

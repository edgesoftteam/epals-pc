/*
 * Created on Sep 28, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.beans.sitedata;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.sitedata.SiteUnitData;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SiteUnitForm extends ActionForm {
	static Logger logger = Logger.getLogger(SiteUnitForm.class.getName());
	private long lsoId;
	private long structureId;
	private String action;
	private List unitList;
	private SiteUnitData[] unitArray;

	public SiteUnitForm() {
		logger.info("Constructor: SiteUnitForm");
	}

	/**
	 * @return
	 */
	public long getLsoId() {
		return lsoId;
	}

	/**
	 * @return
	 */
	public long getStructureId() {
		return structureId;
	}

	/**
	 * @return
	 */
	public List getUnitList() {
		return unitList;
	}

	/**
	 * @param l
	 */
	public void setLsoId(long l) {
		lsoId = l;
	}

	/**
	 * @param l
	 */
	public void setStructureId(long l) {
		structureId = l;
	}

	/**
	 * @param list
	 */
	public void setUnitList(List list) {
		try {
			unitList = list;
			unitArray = new SiteUnitData[list.size()];
			for (int i = 0; i < list.size(); i++)
				unitArray[i] = (SiteUnitData) unitList.get(i);
		} catch (Exception e) {
			logger.error("Error on List to Array Conversion");
			logger.error(e.getLocalizedMessage());
		}
	}

	/**
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param string
	 */
	public void setAction(String string) {
		action = string;
	}

	/**
	 * @return
	 */
	public SiteUnitData[] getUnitArray() {
		return unitArray;
	}

}

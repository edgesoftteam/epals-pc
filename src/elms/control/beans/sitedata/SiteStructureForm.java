package elms.control.beans.sitedata;

import java.util.List;

import org.apache.struts.action.ActionForm;

import elms.app.sitedata.SiteSetback;
import elms.app.sitedata.SiteStructureData;

public class SiteStructureForm extends ActionForm {

	protected long lsoId;
	protected long structureId;
	protected String action;

	protected long levelId;
	protected String levelType;

	protected String activeSite;

	protected List structureList;
	protected SiteStructureData siteStructure;

	protected SiteSetback setback;

	protected List structureHistory;

	/**
	 * 3D73E0110376
	 */
	public SiteStructureForm() {
		setback = new SiteSetback();
	}

	public List getStructureHistory() {
		return structureHistory;
	}

	public void setStructureHistory(List structureHistory) {
		this.structureHistory = structureHistory;
	}

	/**
	 * @return
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @return
	 */
	public long getLsoId() {
		return lsoId;
	}

	/**
	 * @return
	 */
	public SiteSetback getSetback() {
		return setback;
	}

	/**
	 * @return
	 */
	public SiteStructureData getSiteStructure() {
		return siteStructure;
	}

	/**
	 * @return
	 */
	public long getStructureId() {
		return structureId;
	}

	/**
	 * @return
	 */
	public List getStructureList() {
		return structureList;
	}

	/**
	 * @param string
	 */
	public void setAction(String string) {
		action = string;
	}

	/**
	 * @param l
	 */
	public void setLsoId(long l) {
		lsoId = l;
	}

	/**
	 * @param setback
	 */
	public void setSetback(SiteSetback setback) {
		this.setback = setback;
	}

	/**
	 * @param data
	 */
	public void setSiteStructure(SiteStructureData data) {
		siteStructure = data;
	}

	/**
	 * @param l
	 */
	public void setStructureId(long l) {
		structureId = l;
	}

	/**
	 * @param list
	 */
	public void setStructureList(List list) {
		structureList = list;
	}

	/**
	 * @return
	 */
	public long getLevelId() {
		return levelId;
	}

	/**
	 * @return
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * @param l
	 */
	public void setLevelId(long l) {
		levelId = l;
	}

	/**
	 * @param string
	 */
	public void setLevelType(String string) {
		levelType = string;
	}

	public String getActiveSite() {
		return activeSite;
	}

	public void setActiveSite(String activeSite) {
		this.activeSite = activeSite;
	}

}
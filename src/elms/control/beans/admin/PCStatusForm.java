/**
 * 
 */
package elms.control.beans.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import elms.app.admin.PlanCheckStatus;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gaurav Garg
 *
 */
public class PCStatusForm extends ActionForm{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int code;
	private int actStatusCode;
	private String description;
	private boolean active;
	private int moduleId;
	private boolean visible;
	protected List pcStatusList;
	protected String displayPCStatus;
	PlanCheckStatus[] pcStatusArray;
	
	public void reset(ActionMapping mapping, HttpServletRequest request){
		if(pcStatusArray != null){
			for(int i=0;i<pcStatusArray.length;i++){
				pcStatusArray[i].setDescription("");
				pcStatusArray[i].setActive("");
				pcStatusArray[i].setVisible("");
			}
		}
		this.displayPCStatus = "";
	}


	/**
	 * @return the pcStatusArray
	 */
	public PlanCheckStatus[] getPcStatusArray() {
		return pcStatusArray;
	}

	/**
	 * @param pcStatusArray the pcStatusArray to set
	 */
	public void setPcStatusArray(PlanCheckStatus[] pcStatusArray) {
		this.pcStatusArray = pcStatusArray;
	}


	public int getCode() {
		return code;
	}


	public void setCode(int code) {
		this.code = code;
	}


	public int getActStatusCode() {
		return actStatusCode;
	}


	public void setActStatusCode(int actStatusCode) {
		this.actStatusCode = actStatusCode;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	


	public int getModuleId() {
		return moduleId;
	}


	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}


	


	public String getDisplayPCStatus() {
		return displayPCStatus;
	}


	public void setDisplayPCStatus(String displayPCStatus) {
		this.displayPCStatus = displayPCStatus;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public boolean isVisible() {
		return visible;
	}


	public void setVisible(boolean visible) {
		this.visible = visible;
	}


	public List getPcStatusList() {
		return pcStatusList;
	}


	public void setPcStatusList(List pcStatusList) {
		this.pcStatusList = pcStatusList;
	}
	

}

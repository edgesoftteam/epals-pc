package elms.control.beans.admin;

import org.apache.struts.action.ActionForm;

public class AttachmentTypeMappingForm extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int attachmentTypeId;
	private String description;
	private int refId;
	private int actTypeId;
	private String actType;
	protected String[] selectedItems = {};
	
	
	/**
	 * @return the attachmentTypeId
	 */
	public int getAttachmentTypeId() {
		return attachmentTypeId;
	}
	/**
	 * @param attachmentTypeId the attachmentTypeId to set
	 */
	public void setAttachmentTypeId(int attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the refId
	 */
	public int getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(int refId) {
		this.refId = refId;
	}
	/**
	 * @return the actTypeId
	 */
	public int getActTypeId() {
		return actTypeId;
	}
	/**
	 * @param actTypeId the actTypeId to set
	 */
	public void setActTypeId(int actTypeId) {
		this.actTypeId = actTypeId;
	}
	/**
	 * @return the actType
	 */
	public String getActType() {
		return actType;
	}
	/**
	 * @param actType the actType to set
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}
	/**
	 * @return the selectedItems
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}
	/**
	 * @param selectedItems the selectedItems to set
	 */
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}
	
	
}

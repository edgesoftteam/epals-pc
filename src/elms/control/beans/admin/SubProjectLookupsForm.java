//Source file: D:\\OBC\\war\\src\\main\\classes\\elms\\control\\beans\\ActivityForm.java

package elms.control.beans.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;

/**
 * @author Venkata Kastala
 */
public class SubProjectLookupsForm extends ActionForm {

	private int subProjectTypeId;
	private String description;
	private String department;
	private String type;
	private String subProjectName;
	private String subProjectType;
	private String activityType;

	protected List subProjectNameList;
	protected List subProjectTypeList;
	protected List subProjectSubTypeList;
	protected List activityTypeList;

	private ProjectType[] subProjectNames;
	private SubProjectType[] subProjectTypes;
	private SubProjectSubType[] subProjectSubTypes;
	private SubProjectType[] activityTypes;
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {
	}

	/**
	 * Returns the subProjectNames.
	 * 
	 * @return ProjectType[]
	 */
	public ProjectType[] getSubProjectNames() {
		return subProjectNames;
	}

	/**
	 * Sets the subProjectNames.
	 * 
	 * @param subProjectNames
	 *            The subProjectNames to set
	 */
	public void setSubProjectNames(ProjectType[] subProjectNames) {
		this.subProjectNames = subProjectNames;
	}

	public void setSubProjectNames(List subProjectNames) {
		if (subProjectNames == null)
			subProjectNames = new ArrayList();
		ProjectType[] projectTypeArray = new ProjectType[subProjectNames.size()];
		Iterator iter = subProjectNames.iterator();
		int index = 0;
		while (iter.hasNext()) {
			projectTypeArray[index] = (ProjectType) iter.next();
			index++;
		}
		this.subProjectNames = projectTypeArray;
	}

	/**
	 * Returns the subProjectSubTypes.
	 * 
	 * @return SubProjectSubType[]
	 */
	public SubProjectSubType[] getSubProjectSubTypes() {
		return subProjectSubTypes;
	}

	/**
	 * Returns the subProjectTypes.
	 * 
	 * @return SubProjectType[]
	 */
	public SubProjectType[] getSubProjectTypes() {
		return subProjectTypes;
	}

	/**
	 * Sets the subProjectSubTypes.
	 * 
	 * @param subProjectSubTypes
	 *            The subProjectSubTypes to set
	 */
	public void setSubProjectSubTypes(SubProjectSubType[] subProjectSubTypes) {
		this.subProjectSubTypes = subProjectSubTypes;
	}

	public void setSubProjectSubTypes(List subProjectSubTypes) {
		if (subProjectSubTypes == null)
			subProjectSubTypes = new ArrayList();
		SubProjectSubType[] subProjectSubTypeArray = new SubProjectSubType[subProjectSubTypes.size()];
		Iterator iter = subProjectSubTypes.iterator();
		int index = 0;
		while (iter.hasNext()) {
			subProjectSubTypeArray[index] = (SubProjectSubType) iter.next();
			index++;
		}
		this.subProjectSubTypes = subProjectSubTypeArray;
	}

	/**
	 * Sets the subProjectTypes.
	 * 
	 * @param subProjectTypes
	 *            The subProjectTypes to set
	 */
	public void setSubProjectTypes(SubProjectType[] subProjectTypes) {
		this.subProjectTypes = subProjectTypes;
	}

	public void setSubProjectTypes(List subProjectTypes) {
		if (subProjectTypes == null)
			subProjectTypes = new ArrayList();
		SubProjectType[] subProjectTypeArray = new SubProjectType[subProjectTypes.size()];
		Iterator iter = subProjectTypes.iterator();
		int index = 0;
		while (iter.hasNext()) {
			subProjectTypeArray[index] = (SubProjectType) iter.next();
			index++;
		}
		this.subProjectTypes = subProjectTypeArray;
	}

	/**
	 * @return Returns the department.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department
	 *            The department to set.
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the subProjectTypeId.
	 */
	public int getSubProjectTypeId() {
		return subProjectTypeId;
	}

	/**
	 * @param subProjectTypeId
	 *            The subProjectTypeId to set.
	 */
	public void setSubProjectTypeId(int subProjectTypeId) {
		this.subProjectTypeId = subProjectTypeId;
	}

	/**
	 * @return Returns the subProjectNameList.
	 */
	public List getSubProjectNameList() {
		return subProjectNameList;
	}

	/**
	 * @param subProjectNameList
	 *            The subProjectNameList to set.
	 */
	public void setSubProjectNameList(List subProjectNameList) {
		this.subProjectNameList = subProjectNameList;
	}

	/**
	 * @return Returns the subProjectSubTypeList.
	 */
	public List getSubProjectSubTypeList() {
		return subProjectSubTypeList;
	}

	/**
	 * @param subProjectSubTypeList
	 *            The subProjectSubTypeList to set.
	 */
	public void setSubProjectSubTypeList(List subProjectSubTypeList) {
		this.subProjectSubTypeList = subProjectSubTypeList;
	}

	/**
	 * @return Returns the type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            The type to set.
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return Returns the subProjectName.
	 */
	public String getSubProjectName() {
		return subProjectName;
	}

	/**
	 * @param subProjectName
	 *            The subProjectName to set.
	 */
	public void setSubProjectName(String subProjectName) {
		this.subProjectName = subProjectName;
	}

	/**
	 * @return Returns the subProjectTypeList.
	 */
	public List getSubProjectTypeList() {
		return subProjectTypeList;
	}

	/**
	 * @param subProjectTypeList
	 *            The subProjectTypeList to set.
	 */
	public void setSubProjectTypeList(List subProjectTypeList) {
		this.subProjectTypeList = subProjectTypeList;
	}

	/**
	 * @return the activityTypeList
	 */
	public List getActivityTypeList() {
		return activityTypeList;
	}

	/**
	 * @param activityTypeList the activityTypeList to set
	 */
	public void setActivityTypeList(List activityTypeList) {
		this.activityTypeList = activityTypeList;
	}

	/**
	 * @return the activityTypes
	 */
	public SubProjectType[] getActivityTypes() {
		return activityTypes;
	}

	/**
	 * @param activityTypes the activityTypes to set
	 */
	public void setActivityTypes(SubProjectType[] activityTypes) {
		this.activityTypes = activityTypes;
	}

	/**
	 * @return the subProjectType
	 */
	public String getSubProjectType() {
		return subProjectType;
	}

	/**
	 * @param subProjectType the subProjectType to set
	 */
	public void setSubProjectType(String subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * @return the activityType
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @param activityType the activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	
}

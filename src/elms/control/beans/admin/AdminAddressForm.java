package elms.control.beans.admin;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.admin.lso.AddressRangeEdit;
import elms.app.admin.lso.StructureEdit;

public class AdminAddressForm extends ActionForm {

	static Logger logger = Logger.getLogger(AdminAddressForm.class.getName());

	protected String lsoId;
	protected String holds;

	protected AddressRangeEdit[] addressRangeList;
	protected StructureEdit[] structureList;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Returns the addressRangeList.
	 * 
	 * @return AddressRangeEdit[]
	 */
	public AddressRangeEdit[] getAddressRangeList() {
		return addressRangeList;
	}

	/**
	 * Returns the holds.
	 * 
	 * @return String
	 */
	public String getHolds() {
		return holds;
	}

	/**
	 * Returns the lsoId.
	 * 
	 * @return String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Returns the structureList.
	 * 
	 * @return StructureEdit[]
	 */
	public StructureEdit[] getStructureList() {
		return structureList;
	}

	/**
	 * Sets the addressRangeList.
	 * 
	 * @param addressRangeList
	 *            The addressRangeList to set
	 */
	public void setAddressRangeList(AddressRangeEdit[] addressRangeList) {
		this.addressRangeList = addressRangeList;
	}

	/**
	 * Sets the holds.
	 * 
	 * @param holds
	 *            The holds to set
	 */
	public void setHolds(String holds) {
		this.holds = holds;
	}

	/**
	 * Sets the lsoId.
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Sets the structureList.
	 * 
	 * @param structureList
	 *            The structureList to set
	 */
	public void setStructureList(StructureEdit[] structureList) {
		this.structureList = structureList;
	}

} // End class
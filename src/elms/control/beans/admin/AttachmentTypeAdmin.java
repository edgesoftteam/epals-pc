package elms.control.beans.admin;

import java.io.Serializable;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMapping;

public class AttachmentTypeAdmin implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int attachmentTypeId;
	private String attachmentTypeDesc;
	private String documentUrl;
	private String bmcPopupWindowUrl;
	private String uploadOrDownloadType;
	private String comments;	
	private String bmcdescription;
	private String displayAttachmentType;	
	/*private String[] department = {};
	private String[] level = {};*/
	private String department;
	private String level;
	List attachmentTypeList;
	private int actTypeId;
	private String actTypeDesc;
	
	private String departmentIds;
	private String levelIds;
	private int refId;
	
	
	public void reset(ActionMapping mapping, HttpServletRequest request) {       
        this.attachmentTypeDesc = null;        
    }
	/**
	 * @return the attachmentTypeId
	 */
	public int getAttachmentTypeId() {
		return attachmentTypeId;
	}
	/**
	 * @param attachmentTypeId the attachmentTypeId to set
	 */
	public void setAttachmentTypeId(int attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}
	/**
	 * @return the attachmentTypeDesc
	 */
	public String getAttachmentTypeDesc() {
		return attachmentTypeDesc;
	}
	/**
	 * @param attachmentTypeDesc the attachmentTypeDesc to set
	 */
	public void setAttachmentTypeDesc(String attachmentTypeDesc) {
		this.attachmentTypeDesc = attachmentTypeDesc;
	}
	/**
	 * @return the documentUrl
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}
	/**
	 * @param documentUrl the documentUrl to set
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}
	/**
	 * @return the bmcPopupWindowUrl
	 */
	public String getBmcPopupWindowUrl() {
		return bmcPopupWindowUrl;
	}
	/**
	 * @param bmcPopupWindowUrl the bmcPopupWindowUrl to set
	 */
	public void setBmcPopupWindowUrl(String bmcPopupWindowUrl) {
		this.bmcPopupWindowUrl = bmcPopupWindowUrl;
	}
	/**
	 * @return the uploadOrDownloadType
	 */
	public String getUploadOrDownloadType() {
		return uploadOrDownloadType;
	}
	/**
	 * @param uploadOrDownloadType the uploadOrDownloadType to set
	 */
	public void setUploadOrDownloadType(String uploadOrDownloadType) {
		this.uploadOrDownloadType = uploadOrDownloadType;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the attachmentTypeList
	 */
	public List getAttachmentTypeList() {
		return attachmentTypeList;
	}
	/**
	 * @param attachmentTypeList the attachmentTypeList to set
	 */
	public void setAttachmentTypeList(List attachmentTypeList) {
		this.attachmentTypeList = attachmentTypeList;
	}
	/**
	 * @return the displayAttachmentType
	 */
	public String getDisplayAttachmentType() {
		return displayAttachmentType;
	}
	/**
	 * @param displayAttachmentType the displayAttachmentType to set
	 */
	public void setDisplayAttachmentType(String displayAttachmentType) {
		this.displayAttachmentType = displayAttachmentType;
	}
	/**
	 * @return the bmcdescription
	 */
	public String getBmcdescription() {
		return bmcdescription;
	}
	/**
	 * @param bmcdescription the bmcdescription to set
	 */
	public void setBmcdescription(String bmcdescription) {
		this.bmcdescription = bmcdescription;
	}
/*	*//**
	 * @return the department
	 *//*
	public String[] getDepartment() {
		return department;
	}
	*//**
	 * @param department the department to set
	 *//*
	public void setDepartment(String[] department) {
		this.department = department;
	}
	*//**
	 * @return the level
	 *//*
	public String[] getLevel() {
		return level;
	}
	*//**
	 * @param level the level to set
	 *//*
	public void setLevel(String[] level) {
		this.level = level;
	}*/
	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}
	/**
	 * @return the level
	 */
	public String getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(String level) {
		this.level = level;
	}
	
	/**
	 * @return the actTypeId
	 */
	public int getActTypeId() {
		return actTypeId;
	}
	/**
	 * @param actTypeId the actTypeId to set
	 */
	public void setActTypeId(int actTypeId) {
		this.actTypeId = actTypeId;
	}
	/**
	 * @return the actTypeDesc
	 */
	public String getActTypeDesc() {
		return actTypeDesc;
	}
	/**
	 * @param actTypeDesc the actTypeDesc to set
	 */
	public void setActTypeDesc(String actTypeDesc) {
		this.actTypeDesc = actTypeDesc;
	}
	/**
	 * @return the departmentIds
	 */
	public String getDepartmentIds() {
		return departmentIds;
	}
	/**
	 * @param departmentIds the departmentIds to set
	 */
	public void setDepartmentIds(String departmentIds) {
		this.departmentIds = departmentIds;
	}
	/**
	 * @return the levelIds
	 */
	public String getLevelIds() {
		return levelIds;
	}
	/**
	 * @param levelIds the levelIds to set
	 */
	public void setLevelIds(String levelIds) {
		this.levelIds = levelIds;
	}
	/**
	 * @return the refId
	 */
	public int getRefId() {
		return refId;
	}
	/**
	 * @param refId the refId to set
	 */
	public void setRefId(int refId) {
		this.refId = refId;
	}
	
	
	
}

/**
 * 
 */
package elms.control.beans.admin;

import javax.servlet.http.HttpServletRequest;

import elms.app.admin.InspectionCode;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gaurav Garg
 *
 */
public class InspectionCodeForm extends ActionForm{
	
	private int code;
	private String description;
	private boolean active;
	private String moduleId;
	private String deptId;
	private String displayStatus;
	
	
	
	public InspectionCodeForm() {
		super();
	}



	public InspectionCodeForm(int code, String description, boolean active, String moduleId, String deptId) {
		super();
		this.code = code;
		this.description = description;
		this.active = active;
		this.moduleId = moduleId;
		this.deptId = deptId;
	}



	public int getCode() {
		return code;
	}



	public void setCode(int code) {
		this.code = code;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public boolean isActive() {
		return active;
	}



	public void setActive(boolean active) {
		this.active = active;
	}



	public String getModuleId() {
		return moduleId;
	}



	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}



	public String getDeptId() {
		return deptId;
	}



	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}



	public String getDisplayStatus() {
		return displayStatus;
	}



	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

	
}

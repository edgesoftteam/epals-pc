package elms.control.beans.admin;

import org.apache.struts.action.ActionForm;

public class InspectionItemForm extends ActionForm {

	private int code;
	private String description;
	private int id;
	private String actType;
	private String actTypeDesc;
	private String displayInspectItems;
	private String displayStatus;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getActType() {
		return actType;
	}

	public void setActType(String actType) {
		this.actType = actType;
	}

	public String getActTypeDesc() {
		return actTypeDesc;
	}

	public void setActTypeDesc(String actTypeDesc) {
		this.actTypeDesc = actTypeDesc;
	}


	public String getDisplayInspectItems() {
		return displayInspectItems;
	}

	public void setDisplayInspectItems(String displayInspectItems) {
		this.displayInspectItems = displayInspectItems;
	}

	public String getDisplayStatus() {
		return displayStatus;
	}

	public void setDisplayStatus(String displayStatus) {
		this.displayStatus = displayStatus;
	}

}

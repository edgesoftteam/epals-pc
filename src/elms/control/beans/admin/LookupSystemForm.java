package elms.control.beans.admin;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gayathri Turlapati
 */
public class LookupSystemForm extends ActionForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6174442093510039364L;
	// Declare the variable and object declarations used by the class here
		private String name;
		private String value;
		private int systemId;
		protected String[] selectedMaping = {};
		
		protected List lookupSystemTypes;
		protected String displayLookupSystems;
		
		public void reset(ActionMapping mapping, HttpServletRequest request) {
			this.name = null;
			this.value = null;
			this.systemId = 0;
		}		
		
		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "LookupSystemForm [name=" + name + ", value=" + value + ", systemId=" + systemId
					+ ", selectedMaping=" + Arrays.toString(selectedMaping) + "]";
		}
		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}
		/**
		 * @param value the value to set
		 */
		public void setValue(String value) {
			this.value = value;
		}
		/**
		 * @return the systemId
		 */
		public int getSystemId() {
			return systemId;
		}
		/**
		 * @param systemId the systemId to set
		 */
		public void setSystemId(int systemId) {
			this.systemId = systemId;
		}
		/**
		 * @return the selectedMaping
		 */
		public String[] getSelectedMaping() {
			return selectedMaping;
		}
		/**
		 * @param selectedMaping the selectedMaping to set
		 */
		public void setSelectedMaping(String[] selectedMaping) {
			this.selectedMaping = selectedMaping;
		}

		/**
		 * @return the lookupSystemTypes
		 */
		public List getLookupSystemTypes() {
			return lookupSystemTypes;
		}

		/**
		 * @param lookupSystemTypes the lookupSystemTypes to set
		 */
		public void setLookupSystemTypes(List lookupSystemTypes) {
			this.lookupSystemTypes = lookupSystemTypes;
		}

		/**
		 * @return the displayLookupSystems
		 */
		public String getDisplayLookupSystems() {
			return displayLookupSystems;
		}

		/**
		 * @param displayLookupSystems the displayLookupSystems to set
		 */
		public void setDisplayLookupSystems(String displayLookupSystems) {
			this.displayLookupSystems = displayLookupSystems;
		}		
		
}
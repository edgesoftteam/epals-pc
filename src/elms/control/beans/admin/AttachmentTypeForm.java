package elms.control.beans.admin;

import java.util.List;

import org.apache.struts.action.ActionForm;

import elms.security.Department;

/*
 * @Krishna
 */
public class AttachmentTypeForm extends ActionForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int attachmentTypeId;
	private String attachmentTypeDesc;
	private String documentUrl;
	private String bmcPopupWindowUrl;
	private String uploadOrDownloadType;
	private String comments;
	private String displayAttachmentType;
	private String bmcDescription;
	private String onlineFlag;
	/*private String departmentId;*/
	private String[] department = {};
	private String[] level = {};
	
	private String[] departmentIds = {};
	private String[] levelIds = {};
	List attachmentTypeList;
	List<Department> departmentsList;
	protected String[] selectedItems = {};	
	
	private String moduleId;
	private String activityType;
	private String other;	
	private String attachmentDesc;
	//private String attachmentType;
	
	
	/**
	 * @return the attachmentTypeId
	 */
	public int getAttachmentTypeId() {
		return attachmentTypeId;
	}
	/**
	 * @param attachmentTypeId the attachmentTypeId to set
	 */
	public void setAttachmentTypeId(int attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}
	/**
	 * @return the attachmentTypeDesc
	 */
	public String getAttachmentTypeDesc() {
		return attachmentTypeDesc;
	}
	/**
	 * @param attachmentTypeDesc the attachmentTypeDesc to set
	 */
	public void setAttachmentTypeDesc(String attachmentTypeDesc) {
		this.attachmentTypeDesc = attachmentTypeDesc;
	}
	/**
	 * @return the documentUrl
	 */
	public String getDocumentUrl() {
		return documentUrl;
	}
	/**
	 * @param documentUrl the documentUrl to set
	 */
	public void setDocumentUrl(String documentUrl) {
		this.documentUrl = documentUrl;
	}
	/**
	 * @return the bmcPopupWindowUrl
	 */
	public String getBmcPopupWindowUrl() {
		return bmcPopupWindowUrl;
	}
	/**
	 * @param bmcPopupWindowUrl the bmcPopupWindowUrl to set
	 */
	public void setBmcPopupWindowUrl(String bmcPopupWindowUrl) {
		this.bmcPopupWindowUrl = bmcPopupWindowUrl;
	}
	/**
	 * @return the uploadOrDownloadType
	 */
	public String getUploadOrDownloadType() {
		return uploadOrDownloadType;
	}
	/**
	 * @param uploadOrDownloadType the uploadOrDownloadType to set
	 */
	public void setUploadOrDownloadType(String uploadOrDownloadType) {
		this.uploadOrDownloadType = uploadOrDownloadType;
	}
	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}
	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}
	/**
	 * @return the displayEmailTemplateType
	 */
	/*public String getDisplayEmailTemplateType() {
		return displayEmailTemplateType;
	}
	*//**
	 * @param displayEmailTemplateType the displayEmailTemplateType to set
	 *//*
	public void setDisplayEmailTemplateType(String displayEmailTemplateType) {
		this.displayEmailTemplateType = displayEmailTemplateType;
	}*/
	/**
	 * @return the attachmentTypeList
	 */
	public List getAttachmentTypeList() {
		return attachmentTypeList;
	}
	/**
	 * @param attachmentTypeList the attachmentTypeList to set
	 */
	public void setAttachmentTypeList(List attachmentTypeList) {
		this.attachmentTypeList = attachmentTypeList;
	}
	/**
	 * @return the selectedItems
	 */
	/*public String[] getSelectedItems() {
		return selectedItems;
	}
	*//**
	 * @param selectedItems the selectedItems to set
	 *//*
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}*/
	/**
	 * @return the displayAttachmentType
	 */
	public String getDisplayAttachmentType() {
		return displayAttachmentType;
	}
	/**
	 * @param displayAttachmentType the displayAttachmentType to set
	 */
	public void setDisplayAttachmentType(String displayAttachmentType) {
		this.displayAttachmentType = displayAttachmentType;
	}
	/**
	 * @return the bmcDescription
	 */
	public String getBmcDescription() {
		return bmcDescription;
	}
	/**
	 * @param bmcDescription the bmcDescription to set
	 */
	public void setBmcDescription(String bmcDescription) {
		this.bmcDescription = bmcDescription;
	}
	/**
	 * @return the selectedItems
	 */
	public String[] getSelectedItems() {
		return selectedItems;
	}
	/**
	 * @param selectedItems the selectedItems to set
	 */
	public void setSelectedItems(String[] selectedItems) {
		this.selectedItems = selectedItems;
	}
	/**
	 * @return the onlineFlag
	 */
	public String getOnlineFlag() {
		return onlineFlag;
	}
	/**
	 * @param onlineFlag the onlineFlag to set
	 */
	public void setOnlineFlag(String onlineFlag) {
		this.onlineFlag = onlineFlag;
	}
	/**
	 * @return the departmentsList
	 */
	public List<Department> getDepartmentsList() {
		return departmentsList;
	}
	/**
	 * @param departmentsList the departmentsList to set
	 */
	public void setDepartmentsList(List<Department> departmentsList) {
		this.departmentsList = departmentsList;
	}
	/**
	 * @return the departmentId
	 */
	/*public String getDepartmentId() {
		return departmentId;
	}
	*//**
	 * @param departmentId the departmentId to set
	 *//*
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}*/
	/**
	 * @return the department
	 */
	/**
	 * @return the department
	 */
	public String[] getDepartment() {
		return department;
	}
	/**
	 * @param department the department to set
	 */
	public void setDepartment(String[] department) {
		this.department = department;
	}
	/**
	 * @return the level
	 */
	public String[] getLevel() {
		return level;
	}
	/**
	 * @param level the level to set
	 */
	public void setLevel(String[] level) {
		this.level = level;
	}
	/**
	 * @return the departmentIds
	 */
	public String[] getDepartmentIds() {
		return departmentIds;
	}
	/**
	 * @param departmentIds the departmentIds to set
	 */
	public void setDepartmentIds(String[] departmentIds) {
		this.departmentIds = departmentIds;
	}
	/**
	 * @return the levelIds
	 */
	public String[] getLevelIds() {
		return levelIds;
	}
	/**
	 * @param levelIds the levelIds to set
	 */
	public void setLevelIds(String[] levelIds) {
		this.levelIds = levelIds;
	}
	/**
	 * @return the moduleId
	 */
	public String getModuleId() {
		return moduleId;
	}
	/**
	 * @param moduleId the moduleId to set
	 */
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	/**
	 * @return the activityType
	 */
	public String getActivityType() {
		return activityType;
	}
	/**
	 * @param activityType the activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	/**
	 * @return the other
	 */
	public String getOther() {
		return other;
	}
	/**
	 * @param other the other to set
	 */
	public void setOther(String other) {
		this.other = other;
	}
	/**
	 * @return the attachmentDesc
	 */
	public String getAttachmentDesc() {
		return attachmentDesc;
	}
	/**
	 * @param attachmentDesc the attachmentDesc to set
	 */
	public void setAttachmentDesc(String attachmentDesc) {
		this.attachmentDesc = attachmentDesc;
	}
	/**
	 * @return the attachmentType
	 */
	/*public String getAttachmentType() {
		return attachmentType;
	}*/
	/**
	 * @param attachmentType the attachmentType to set
	 */
	/*public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}*/
	
	
	
}

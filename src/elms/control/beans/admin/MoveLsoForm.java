//Source file: C:\\Datafile\\OBCSource\\elms\\control\\beans\\EditHoldForm.java

package elms.control.beans.admin;

import org.apache.struts.action.ActionForm;

/**
 * @author Shekhar Jain
 */
public class MoveLsoForm extends ActionForm {
	protected String moveNumber;
	protected String moveFraction;
	protected String moveName;
	protected String moveUnit;
	protected String toNumber;
	protected String toFraction;
	protected String toName;
	protected String moveType;

	/**
	 * @return
	 */
	public String getMoveFraction() {
		return moveFraction;
	}

	/**
	 * @return
	 */
	public String getMoveName() {
		return moveName;
	}

	/**
	 * @return
	 */
	public String getMoveNumber() {
		return moveNumber;
	}

	/**
	 * @return
	 */
	public String getMoveType() {
		return moveType;
	}

	/**
	 * @return
	 */
	public String getMoveUnit() {
		return moveUnit;
	}

	/**
	 * @return
	 */
	public String getToFraction() {
		return toFraction;
	}

	/**
	 * @return
	 */
	public String getToName() {
		return toName;
	}

	/**
	 * @return
	 */
	public String getToNumber() {
		return toNumber;
	}

	/**
	 * @param string
	 */
	public void setMoveFraction(String string) {
		moveFraction = string;
	}

	/**
	 * @param string
	 */
	public void setMoveName(String string) {
		moveName = string;
	}

	/**
	 * @param string
	 */
	public void setMoveNumber(String string) {
		moveNumber = string;
	}

	/**
	 * @param string
	 */
	public void setMoveType(String string) {
		moveType = string;
	}

	/**
	 * @param string
	 */
	public void setMoveUnit(String string) {
		moveUnit = string;
	}

	/**
	 * @param string
	 */
	public void setToFraction(String string) {
		toFraction = string;
	}

	/**
	 * @param string
	 */
	public void setToName(String string) {
		toName = string;
	}

	/**
	 * @param string
	 */
	public void setToNumber(String string) {
		toNumber = string;
	}

}

package elms.control.beans.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
* @author Gai3
*/
public class NoticeTypeAdminForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	private int noticeTypeId;

	private String category;
	private String description;
	protected String[] selectedMaping = {};

	protected List NoticeTypes;
	protected String displayNoticeTypes;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		this.description = null;
		this.category = null;
	}


	/**
	 * @return the noticeTypeId
	 */
	public int getNoticeTypeId() {
		return noticeTypeId;
	}


	/**
	 * @param noticeTypeId the noticeTypeId to set
	 */
	public void setNoticeTypeId(int noticeTypeId) {
		this.noticeTypeId = noticeTypeId;
	}


	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}


	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the noticeTypes
	 */
	public List getNoticeTypes() {
		return NoticeTypes;
	}


	/**
	 * @param noticeTypes the noticeTypes to set
	 */
	public void setNoticeTypes(List noticeTypes) {
		NoticeTypes = noticeTypes;
	}


	/**
	 * @return the displayNoticeTypes
	 */
	public String getDisplayNoticeTypes() {
		return displayNoticeTypes;
	}


	/**
	 * @param displayNoticeTypes the displayNoticeTypes to set
	 */
	public void setDisplayNoticeTypes(String displayNoticeTypes) {
		this.displayNoticeTypes = displayNoticeTypes;
	}


	/**
	 * @return Returns the selectedMaping.
	 */
	public String[] getSelectedMaping() {
		return selectedMaping;
	}

	/**
	 * @param selectedMaping
	 *            The selectedMaping to set.
	 */
	public void setSelectedMaping(String[] selectedMaping) {
		this.selectedMaping = selectedMaping;
	}
}

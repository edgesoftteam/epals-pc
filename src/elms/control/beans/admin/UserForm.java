package elms.control.beans.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */
public class UserForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	private String userId;
	private String userName;
	private String password;
	private String employeeNumber;
	private String firstName;
	private String lastName;
	private String mi;
	private String departmentId;
	private String departmentName;
	private String title;
	private String[] userGroups;
	private String role;
	private boolean active = false;
	private List users;
	private String displayUserList;

	private String userEmail;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.userName = null;
		this.firstName = null;
		this.lastName = null;
		this.mi = null;
		this.departmentId = null;
		this.title = null;
		this.userGroups = null;
		this.role = null;
		this.active = false;
	}

	/**
	 * Returns the departmentId.
	 * 
	 * @return String
	 */
	public String getDepartmentId() {

		return departmentId;
	}

	/**
	 * Returns the firstName.
	 * 
	 * @return String
	 */
	public String getFirstName() {

		return firstName;
	}

	/**
	 * Returns the lastName.
	 * 
	 * @return String
	 */
	public String getLastName() {

		return lastName;
	}

	/**
	 * Returns the mi.
	 * 
	 * @return String
	 */
	public String getMi() {

		return mi;
	}

	/**
	 * Returns the userGroups.
	 * 
	 * @return String[]
	 */
	public String[] getUserGroups() {

		return userGroups;
	}

	/**
	 * Returns the userName.
	 * 
	 * @return String
	 */
	public String getUserName() {

		return userName;
	}

	/**
	 * Sets the departmentId.
	 * 
	 * @param departmentId
	 *            The departmentId to set
	 */
	public void setDepartmentId(String departmentId) {

		this.departmentId = departmentId;
	}

	/**
	 * Sets the firstName.
	 * 
	 * @param firstName
	 *            The firstName to set
	 */
	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	/**
	 * Sets the lastName.
	 * 
	 * @param lastName
	 *            The lastName to set
	 */
	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	/**
	 * Sets the mi.
	 * 
	 * @param mi
	 *            The mi to set
	 */
	public void setMi(String mi) {

		this.mi = mi;
	}

	/**
	 * Sets the userGroups.
	 * 
	 * @param userGroups
	 *            The userGroups to set
	 */
	public void setUserGroups(String[] userGroups) {

		this.userGroups = userGroups;
	}

	public void setUserGroups(List userGroups) {

		if (userGroups == null) {
			userGroups = new ArrayList();
		}

		String[] userGroupsArray = new String[userGroups.size()];
		Iterator iter = userGroups.iterator();
		int index = 0;
		while (iter.hasNext()) {

			userGroupsArray[index] = (String) iter.next();
			index++;
		}

		this.userGroups = userGroupsArray;
	}

	/**
	 * Sets the userName.
	 * 
	 * @param userName
	 *            The userName to set
	 */
	public void setUserName(String userName) {

		this.userName = userName;
	}

	/**
	 * Returns the title.
	 * 
	 * @return String
	 */
	public String getTitle() {

		return title;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            The title to set
	 */
	public void setTitle(String title) {

		this.title = title;
	}

	/**
	 * Returns the employeeNumber.
	 * 
	 * @return String
	 */
	public String getEmployeeNumber() {

		return employeeNumber;
	}

	/**
	 * Returns the password.
	 * 
	 * @return String
	 */
	public String getPassword() {

		return password;
	}

	/**
	 * Sets the employeeNumber.
	 * 
	 * @param employeeNumber
	 *            The employeeNumber to set
	 */
	public void setEmployeeNumber(String employeeNumber) {

		this.employeeNumber = employeeNumber;
	}

	/**
	 * Sets the password.
	 * 
	 * @param password
	 *            The password to set
	 */
	public void setPassword(String password) {

		this.password = password;
	}

	/**
	 * Returns the userId.
	 * 
	 * @return String
	 */
	public String getUserId() {

		return userId;
	}

	/**
	 * Sets the userId.
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(String userId) {

		this.userId = userId;
	}

	/**
	 * Returns the role.
	 * 
	 * @return String
	 */
	public String getRole() {

		return role;
	}

	/**
	 * Sets the role.
	 * 
	 * @param role
	 *            The role to set
	 */
	public void setRole(String role) {

		this.role = role;
	}

	/**
	 * Returns the displayUserList.
	 * 
	 * @return String
	 */
	public String getDisplayUserList() {

		return displayUserList;
	}

	/**
	 * Sets the displayUserList.
	 * 
	 * @param displayUserList
	 *            The displayUserList to set
	 */
	public void setDisplayUserList(String displayUserList) {

		this.displayUserList = displayUserList;
	}

	/**
	 * Returns the users.
	 * 
	 * @return List
	 */
	public List getUsers() {

		return users;
	}

	/**
	 * Sets the users.
	 * 
	 * @param users
	 *            The users to set
	 */
	public void setUsers(List users) {

		this.users = users;
	}

	/**
	 * Returns the departmentName.
	 * 
	 * @return String
	 */
	public String getDepartmentName() {

		return departmentName;
	}

	/**
	 * Sets the departmentName.
	 * 
	 * @param departmentName
	 *            The departmentName to set
	 */
	public void setDepartmentName(String departmentName) {

		this.departmentName = departmentName;
	}

	/**
	 * Returns the active.
	 * 
	 * @return boolean
	 */
	public boolean getActive() {

		return active;
	}

	/**
	 * Sets the active.
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(boolean active) {

		this.active = active;
	}

	/**
	 * @return Returns the userEmail.
	 */
	public String getUserEmail() {
		return userEmail;
	}

	/**
	 * @param userEmail
	 *            The userEmail to set.
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
}

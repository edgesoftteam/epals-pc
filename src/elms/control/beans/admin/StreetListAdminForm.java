/*
 * Created on Mar 24, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.admin;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.control.actions.admin.StreetListAdminAction;

/**
 * @author Hemavathi
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style - Code Templates
 */
public class StreetListAdminForm extends ActionForm {
	static Logger logger = Logger.getLogger(StreetListAdminAction.class.getName());
	protected String streetName;
	protected String streetPrefix;
	protected String streetSuffix;
	protected String streetType;
	protected String[] selectedMaping;
	protected List streetList;
	protected int streetListId;

	/**
	 * @return Returns the streetListId.
	 */
	public int getStreetListId() {
		return streetListId;
	}

	/**
	 * @param streetListId
	 *            The streetListId to set.
	 */
	public void setStreetListId(int streetListId) {
		this.streetListId = streetListId;
	}

	/**
	 * @return Returns the streetName.
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * @param streetName
	 *            The streetName to set.
	 */
	public void setStreetName(String streetName) {
		logger.debug("streetName*******" + streetName);
		this.streetName = streetName;
	}

	/**
	 * @return Returns the streetPrefix.
	 */
	public String getStreetPrefix() {
		return streetPrefix;
	}

	/**
	 * @param streetPrefix
	 *            The streetPrefix to set.
	 */
	public void setStreetPrefix(String streetPrefix) {
		this.streetPrefix = streetPrefix;
	}

	/**
	 * @return Returns the streetSuffix.
	 */
	public String getStreetSuffix() {
		return streetSuffix;
	}

	/**
	 * @param streetSuffix
	 *            The streetSuffix to set.
	 */
	public void setStreetSuffix(String streetSuffix) {
		this.streetSuffix = streetSuffix;
	}

	/**
	 * @return Returns the streetType.
	 */
	public String getStreetType() {
		return streetType;
	}

	/**
	 * @param streetType
	 *            The streetType to set.
	 */
	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}

	/**
	 * @return Returns the selectedMaping.
	 */
	public String[] getSelectedMaping() {
		return selectedMaping;
	}

	/**
	 * @param selectedMaping
	 *            The selectedMaping to set.
	 */
	public void setSelectedMaping(String[] selectedMaping) {
		this.selectedMaping = selectedMaping;
	}

	/**
	 * @return Returns the streetList.
	 */
	public List getStreetList() {
		return streetList;
	}

	/**
	 * @param streetList
	 *            The streetList to set.
	 */
	public void setStreetList(List streetList) {
		this.streetList = streetList;
	}
}

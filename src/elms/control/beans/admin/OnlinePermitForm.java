package elms.control.beans.admin;

import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * @author Akshay
 */

public class OnlinePermitForm extends ActionForm {

	private String actType;
	private String actSubName;
	private String actCode;
	private int stypeId;
	private int feeId;
	private String description;
	private List actSubFeeMap;
	private String[] selectedFeeId;
	
	private int questionId;
	
	private int lkId;
	
	private String planCheck;
	private String[] deleteSelectedId;


	/**
	 * @return Returns the actCode.
	 */
	public String getActCode() {
		return actCode;
	}

	/**
	 * @param actCode
	 *            The actCode to set.
	 */
	public void setActCode(String actCode) {
		this.actCode = actCode;
	}

	/**
	 * @return Returns the actSubName.
	 */
	public String getActSubName() {
		return actSubName;
	}

	/**
	 * @param actSubName
	 *            The actSubName to set.
	 */
	public void setActSubName(String actSubName) {
		this.actSubName = actSubName;
	}

	/**
	 * @return Returns the actType.
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * @param actType
	 *            The actType to set.
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the feeId.
	 */
	public int getFeeId() {
		return feeId;
	}

	/**
	 * @param feeId
	 *            The feeId to set.
	 */
	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	/**
	 * @return Returns the stypeId.
	 */
	public int getStypeId() {
		return stypeId;
	}

	/**
	 * @param stypeId
	 *            The stypeId to set.
	 */
	public void setStypeId(int stypeId) {
		this.stypeId = stypeId;
	}

	/**
	 * @return Returns the actSubFeeMap.
	 */
	public List getActSubFeeMap() {
		return actSubFeeMap;
	}

	/**
	 * @param actSubFeeMap
	 *            The actSubFeeMap to set.
	 */
	public void setActSubFeeMap(List actSubFeeMap) {
		this.actSubFeeMap = actSubFeeMap;
	}

	/**
	 * @return Returns the selectedFeeId.
	 */
	public String[] getSelectedFeeId() {
		return selectedFeeId;
	}

	/**
	 * @param selectedFeeId
	 *            The selectedFeeId to set.
	 */
	public void setSelectedFeeId(String[] selectedFeeId) {
		this.selectedFeeId = selectedFeeId;
	}

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public int getLkId() {
		return lkId;
	}

	public void setLkId(int lkId) {
		this.lkId = lkId;
	}

	public String getPlanCheck() {
		return planCheck;
	}

	public void setPlanCheck(String planCheck) {
		this.planCheck = planCheck;
	}

	public String[] getDeleteSelectedId() {
		return deleteSelectedId;
	}

	public void setDeleteSelectedId(String[] deleteSelectedId) {
		this.deleteSelectedId = deleteSelectedId;
	}
}

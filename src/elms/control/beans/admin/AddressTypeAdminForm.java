/**
 * 
 */
package elms.control.beans.admin;

import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * @author Arjun
 *
 */
public class AddressTypeAdminForm extends ActionForm{

	private String id;
	private String addressType;
	private boolean btFlag;
	private boolean blFlag;
	protected List addressTypes;
	protected String displayAddressType;
	protected String[] selectedMaping = {};

	public AddressTypeAdminForm() {
		super();
	}
	public AddressTypeAdminForm(String id, String addressType, boolean btFlag, boolean blFlag) {
		super();
		this.id = id;
		this.addressType = addressType;
		this.btFlag = btFlag;
		this.blFlag = blFlag;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getAddressType() {
		return addressType;
	}
	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}
	public boolean isBtFlag() {
		return btFlag;
	}
	public void setBtFlag(boolean btFlag) {
		this.btFlag = btFlag;
	}
	public boolean isBlFlag() {
		return blFlag;
	}
	public void setBlFlag(boolean blFlag) {
		this.blFlag = blFlag;
	}
	public List getAddressTypes() {
		return addressTypes;
	}
	public void setAddressTypes(List addressTypes) {
		this.addressTypes = addressTypes;
	}
	public String getDisplayAddressType() {
		return displayAddressType;
	}
	public void setDisplayAddressType(String displayAddressType) {
		this.displayAddressType = displayAddressType;
	}
	public String[] getSelectedMaping() {
		return selectedMaping;
	}
	public void setSelectedMaping(String[] selectedMaping) {
		this.selectedMaping = selectedMaping;
	}
	
	
}

package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AddSitedataForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String primarySystemOfAddition;
	protected String followUp;
	protected String streetName;
	protected String poorConditionS;
	protected String basicScoreP;
	protected String torsionPA;
	protected String designCode;
	protected String poorConditionP;
	protected String largeHeavyCldgPA;
	protected String sl2S;
	protected String largeHeavyCldgS;
	protected String sl2P;
	protected String largeHeavyCldgP;
	protected String softSotrySA;
	protected String mezzanineCheck;
	protected String elevatorEnclosureCheck;
	protected String sl38StoriesPA;
	protected String parcelNo;
	protected String onsiteParkingCheck;
	protected String shortColumnsPA;
	protected String finalScorePA;
	protected String softStoryS;
	protected String numberOfStoriesAboveGrade;
	protected String cansusTract;
	protected String softStoryP;
	protected String areaOfAparking;
	protected String retrofittedSA;
	protected String estimateOfWidthIn;
	protected String totalFloorAreaBldgCode;
	protected String sl3PA;
	protected String retrofittedS;
	protected String postBanchmarkS;
	protected String zonePermit;
	protected String buildingName;
	protected String retrofittedP;
	protected String postBanchmarkP;
	protected String numberOfStoriesAboveGradeZone;
	protected String sl2PA;
	protected String currentPrimaryUse;
	protected String highRisePA;
	protected String softStoryPA;
	protected String subterraneanCheck;
	protected String plansOnFileCheck;
	protected String parcel1;
	protected String poorConditionSA;
	protected String poundingPA;
	protected String verticalIrregularityS;
	protected String secondarySystem;
	protected String verticalIrregularityP;
	protected String covenantOffSite;
	protected String primarySystem;
	protected String occupantLoad;
	protected String basicSocrePA;
	protected String currentPrimaryUseDate;
	protected String radiobutton;
	protected String Submit;
	protected String parkingProvidedCheck;
	protected String select2;
	protected String in;
	protected String NoOfResUnits;
	protected String postBanchmarkYrSA;
	protected String torsionS;
	protected String otherZoningDeterminations;
	protected String noOfElevators;
	protected String torsionP;
	protected String planIrregularitySA;
	protected String onGradeCheck;
	protected String majorStructuralAlterationsCheck;
	protected String shortColumnsS;
	protected String sprinkleredCheck;
	protected String vertivalIrregularitySA;
	protected String basicSocreS;
	protected String highRiseS;
	protected String shortColumnsP;
	protected String commentSecondarySystemAddition;
	protected String highRiseP;
	protected String dateOfBldgPermit;
	protected String notes;
	protected String liquefactionCheck;
	protected String FinalScoreS;
	protected String dir1;
	protected String urmCheck;
	protected String torsionSA;
	protected String largeHeavyCldgSA;
	protected String panthouseCheck;
	protected String secondarySystemOfAddition;
	protected String noOfStructuresOnSite;
	protected String commentPrimarySystemAddition;
	protected String sl38StoriesSA;
	protected String commentSecondarySystem;
	protected String shortColumnsSA;
	protected String finalSocreP;
	protected String finalScoreSA;
	protected String estimateOfWidthFt;
	protected String structAddressCheck1;
	protected String retrofittedPA;
	protected String inLieuParking;
	protected String occupancyDiv;
	protected String highFireSeverityZoneCheck;
	protected String bldgPermitNo;
	protected String majorStructuralAdditionsDate;
	protected String withinStructureCheck;
	protected String sl38StoriesS;
	protected String zoneCurrent;
	protected String majorStructuralAlterations;
	protected String sl38StoriesP;
	protected String sl3SA;
	protected String localFaultZoneCheck;
	protected String numberOfStoriesBelowGrade;
	protected String sl2SA;
	protected String planIrregularityS;
	protected String low1;
	protected String hazardousMeterialsCheck;
	protected String planIrregularityP;
	protected String parkingRequired;
	protected String poorConditionPA;
	protected String highRiseSA;
	protected String verticalIrregularityPA;
	protected String occupantLoadCurrent;
	protected String poundingSA;
	protected String ft;
	protected String alarmsCheck;
	protected String basicSocreSA;
	protected String poundingS;
	protected String postBanchmarkYrPA;
	protected String stripedCheck;
	protected String covenantOnSite;
	protected String sl3S;
	protected String poundingP;
	protected String sl3P;
	protected String index;
	protected String bldgConstTypeCode;
	protected String planIrregularityPA;
	protected String retrofittedCheck;
	protected String commentPrimarySystem;
	protected String yearPermitAppliedCheck;
	protected String majorStructuralAdditionsCheck;
	protected String yearPermitAppliedDate;
	protected String floodAreaCheck;
	protected String majorStructuralAlterationsDate;
	protected String address;
	protected String totalFloorArea;

	/**
	 * Gets the primarySystemOfAddition
	 * 
	 * @return Returns a String
	 */
	public String getPrimarySystemOfAddition() {
		return primarySystemOfAddition;
	}

	/**
	 * Sets the primarySystemOfAddition
	 * 
	 * @param primarySystemOfAddition
	 *            The primarySystemOfAddition to set
	 */
	public void setPrimarySystemOfAddition(String primarySystemOfAddition) {
		this.primarySystemOfAddition = primarySystemOfAddition;
	}

	/**
	 * Gets the followUp
	 * 
	 * @return Returns a String
	 */
	public String getFollowUp() {
		return followUp;
	}

	/**
	 * Sets the followUp
	 * 
	 * @param followUp
	 *            The followUp to set
	 */
	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the poorConditionS
	 * 
	 * @return Returns a String
	 */
	public String getPoorConditionS() {
		return poorConditionS;
	}

	/**
	 * Sets the poorConditionS
	 * 
	 * @param poorConditionS
	 *            The poorConditionS to set
	 */
	public void setPoorConditionS(String poorConditionS) {
		this.poorConditionS = poorConditionS;
	}

	/**
	 * Gets the basicScoreP
	 * 
	 * @return Returns a String
	 */
	public String getBasicScoreP() {
		return basicScoreP;
	}

	/**
	 * Sets the basicScoreP
	 * 
	 * @param basicScoreP
	 *            The basicScoreP to set
	 */
	public void setBasicScoreP(String basicScoreP) {
		this.basicScoreP = basicScoreP;
	}

	/**
	 * Gets the torsionPA
	 * 
	 * @return Returns a String
	 */
	public String getTorsionPA() {
		return torsionPA;
	}

	/**
	 * Sets the torsionPA
	 * 
	 * @param torsionPA
	 *            The torsionPA to set
	 */
	public void setTorsionPA(String torsionPA) {
		this.torsionPA = torsionPA;
	}

	/**
	 * Gets the designCode
	 * 
	 * @return Returns a String
	 */
	public String getDesignCode() {
		return designCode;
	}

	/**
	 * Sets the designCode
	 * 
	 * @param designCode
	 *            The designCode to set
	 */
	public void setDesignCode(String designCode) {
		this.designCode = designCode;
	}

	/**
	 * Gets the poorConditionP
	 * 
	 * @return Returns a String
	 */
	public String getPoorConditionP() {
		return poorConditionP;
	}

	/**
	 * Sets the poorConditionP
	 * 
	 * @param poorConditionP
	 *            The poorConditionP to set
	 */
	public void setPoorConditionP(String poorConditionP) {
		this.poorConditionP = poorConditionP;
	}

	/**
	 * Gets the largeHeavyCldgPA
	 * 
	 * @return Returns a String
	 */
	public String getLargeHeavyCldgPA() {
		return largeHeavyCldgPA;
	}

	/**
	 * Sets the largeHeavyCldgPA
	 * 
	 * @param largeHeavyCldgPA
	 *            The largeHeavyCldgPA to set
	 */
	public void setLargeHeavyCldgPA(String largeHeavyCldgPA) {
		this.largeHeavyCldgPA = largeHeavyCldgPA;
	}

	/**
	 * Gets the sl2S
	 * 
	 * @return Returns a String
	 */
	public String getSl2S() {
		return sl2S;
	}

	/**
	 * Sets the sl2S
	 * 
	 * @param sl2S
	 *            The sl2S to set
	 */
	public void setSl2S(String sl2S) {
		this.sl2S = sl2S;
	}

	/**
	 * Gets the largeHeavyCldgS
	 * 
	 * @return Returns a String
	 */
	public String getLargeHeavyCldgS() {
		return largeHeavyCldgS;
	}

	/**
	 * Sets the largeHeavyCldgS
	 * 
	 * @param largeHeavyCldgS
	 *            The largeHeavyCldgS to set
	 */
	public void setLargeHeavyCldgS(String largeHeavyCldgS) {
		this.largeHeavyCldgS = largeHeavyCldgS;
	}

	/**
	 * Gets the sl2P
	 * 
	 * @return Returns a String
	 */
	public String getSl2P() {
		return sl2P;
	}

	/**
	 * Sets the sl2P
	 * 
	 * @param sl2P
	 *            The sl2P to set
	 */
	public void setSl2P(String sl2P) {
		this.sl2P = sl2P;
	}

	/**
	 * Gets the largeHeavyCldgP
	 * 
	 * @return Returns a String
	 */
	public String getLargeHeavyCldgP() {
		return largeHeavyCldgP;
	}

	/**
	 * Sets the largeHeavyCldgP
	 * 
	 * @param largeHeavyCldgP
	 *            The largeHeavyCldgP to set
	 */
	public void setLargeHeavyCldgP(String largeHeavyCldgP) {
		this.largeHeavyCldgP = largeHeavyCldgP;
	}

	/**
	 * Gets the softSotrySA
	 * 
	 * @return Returns a String
	 */
	public String getSoftSotrySA() {
		return softSotrySA;
	}

	/**
	 * Sets the softSotrySA
	 * 
	 * @param softSotrySA
	 *            The softSotrySA to set
	 */
	public void setSoftSotrySA(String softSotrySA) {
		this.softSotrySA = softSotrySA;
	}

	/**
	 * Gets the mezzanineCheck
	 * 
	 * @return Returns a String
	 */
	public String getMezzanineCheck() {
		return mezzanineCheck;
	}

	/**
	 * Sets the mezzanineCheck
	 * 
	 * @param mezzanineCheck
	 *            The mezzanineCheck to set
	 */
	public void setMezzanineCheck(String mezzanineCheck) {
		this.mezzanineCheck = mezzanineCheck;
	}

	/**
	 * Gets the elevatorEnclosureCheck
	 * 
	 * @return Returns a String
	 */
	public String getElevatorEnclosureCheck() {
		return elevatorEnclosureCheck;
	}

	/**
	 * Sets the elevatorEnclosureCheck
	 * 
	 * @param elevatorEnclosureCheck
	 *            The elevatorEnclosureCheck to set
	 */
	public void setElevatorEnclosureCheck(String elevatorEnclosureCheck) {
		this.elevatorEnclosureCheck = elevatorEnclosureCheck;
	}

	/**
	 * Gets the sl38StoriesPA
	 * 
	 * @return Returns a String
	 */
	public String getSl38StoriesPA() {
		return sl38StoriesPA;
	}

	/**
	 * Sets the sl38StoriesPA
	 * 
	 * @param sl38StoriesPA
	 *            The sl38StoriesPA to set
	 */
	public void setSl38StoriesPA(String sl38StoriesPA) {
		this.sl38StoriesPA = sl38StoriesPA;
	}

	/**
	 * Gets the parcelNo
	 * 
	 * @return Returns a String
	 */
	public String getParcelNo() {
		return parcelNo;
	}

	/**
	 * Sets the parcelNo
	 * 
	 * @param parcelNo
	 *            The parcelNo to set
	 */
	public void setParcelNo(String parcelNo) {
		this.parcelNo = parcelNo;
	}

	/**
	 * Gets the onsiteParkingCheck
	 * 
	 * @return Returns a String
	 */
	public String getOnsiteParkingCheck() {
		return onsiteParkingCheck;
	}

	/**
	 * Sets the onsiteParkingCheck
	 * 
	 * @param onsiteParkingCheck
	 *            The onsiteParkingCheck to set
	 */
	public void setOnsiteParkingCheck(String onsiteParkingCheck) {
		this.onsiteParkingCheck = onsiteParkingCheck;
	}

	/**
	 * Gets the shortColumnsPA
	 * 
	 * @return Returns a String
	 */
	public String getShortColumnsPA() {
		return shortColumnsPA;
	}

	/**
	 * Sets the shortColumnsPA
	 * 
	 * @param shortColumnsPA
	 *            The shortColumnsPA to set
	 */
	public void setShortColumnsPA(String shortColumnsPA) {
		this.shortColumnsPA = shortColumnsPA;
	}

	/**
	 * Gets the finalScorePA
	 * 
	 * @return Returns a String
	 */
	public String getFinalScorePA() {
		return finalScorePA;
	}

	/**
	 * Sets the finalScorePA
	 * 
	 * @param finalScorePA
	 *            The finalScorePA to set
	 */
	public void setFinalScorePA(String finalScorePA) {
		this.finalScorePA = finalScorePA;
	}

	/**
	 * Gets the softStoryS
	 * 
	 * @return Returns a String
	 */
	public String getSoftStoryS() {
		return softStoryS;
	}

	/**
	 * Sets the softStoryS
	 * 
	 * @param softStoryS
	 *            The softStoryS to set
	 */
	public void setSoftStoryS(String softStoryS) {
		this.softStoryS = softStoryS;
	}

	/**
	 * Gets the numberOfStoriesAboveGrade
	 * 
	 * @return Returns a String
	 */
	public String getNumberOfStoriesAboveGrade() {
		return numberOfStoriesAboveGrade;
	}

	/**
	 * Sets the numberOfStoriesAboveGrade
	 * 
	 * @param numberOfStoriesAboveGrade
	 *            The numberOfStoriesAboveGrade to set
	 */
	public void setNumberOfStoriesAboveGrade(String numberOfStoriesAboveGrade) {
		this.numberOfStoriesAboveGrade = numberOfStoriesAboveGrade;
	}

	/**
	 * Gets the cansusTract
	 * 
	 * @return Returns a String
	 */
	public String getCansusTract() {
		return cansusTract;
	}

	/**
	 * Sets the cansusTract
	 * 
	 * @param cansusTract
	 *            The cansusTract to set
	 */
	public void setCansusTract(String cansusTract) {
		this.cansusTract = cansusTract;
	}

	/**
	 * Gets the softStoryP
	 * 
	 * @return Returns a String
	 */
	public String getSoftStoryP() {
		return softStoryP;
	}

	/**
	 * Sets the softStoryP
	 * 
	 * @param softStoryP
	 *            The softStoryP to set
	 */
	public void setSoftStoryP(String softStoryP) {
		this.softStoryP = softStoryP;
	}

	/**
	 * Gets the areaOfAparking
	 * 
	 * @return Returns a String
	 */
	public String getAreaOfAparking() {
		return areaOfAparking;
	}

	/**
	 * Sets the areaOfAparking
	 * 
	 * @param areaOfAparking
	 *            The areaOfAparking to set
	 */
	public void setAreaOfAparking(String areaOfAparking) {
		this.areaOfAparking = areaOfAparking;
	}

	/**
	 * Gets the retrofittedSA
	 * 
	 * @return Returns a String
	 */
	public String getRetrofittedSA() {
		return retrofittedSA;
	}

	/**
	 * Sets the retrofittedSA
	 * 
	 * @param retrofittedSA
	 *            The retrofittedSA to set
	 */
	public void setRetrofittedSA(String retrofittedSA) {
		this.retrofittedSA = retrofittedSA;
	}

	/**
	 * Gets the estimateOfWidthIn
	 * 
	 * @return Returns a String
	 */
	public String getEstimateOfWidthIn() {
		return estimateOfWidthIn;
	}

	/**
	 * Sets the estimateOfWidthIn
	 * 
	 * @param estimateOfWidthIn
	 *            The estimateOfWidthIn to set
	 */
	public void setEstimateOfWidthIn(String estimateOfWidthIn) {
		this.estimateOfWidthIn = estimateOfWidthIn;
	}

	/**
	 * Gets the totalFloorAreaBldgCode
	 * 
	 * @return Returns a String
	 */
	public String getTotalFloorAreaBldgCode() {
		return totalFloorAreaBldgCode;
	}

	/**
	 * Sets the totalFloorAreaBldgCode
	 * 
	 * @param totalFloorAreaBldgCode
	 *            The totalFloorAreaBldgCode to set
	 */
	public void setTotalFloorAreaBldgCode(String totalFloorAreaBldgCode) {
		this.totalFloorAreaBldgCode = totalFloorAreaBldgCode;
	}

	/**
	 * Gets the sl3PA
	 * 
	 * @return Returns a String
	 */
	public String getSl3PA() {
		return sl3PA;
	}

	/**
	 * Sets the sl3PA
	 * 
	 * @param sl3PA
	 *            The sl3PA to set
	 */
	public void setSl3PA(String sl3PA) {
		this.sl3PA = sl3PA;
	}

	/**
	 * Gets the retrofittedS
	 * 
	 * @return Returns a String
	 */
	public String getRetrofittedS() {
		return retrofittedS;
	}

	/**
	 * Sets the retrofittedS
	 * 
	 * @param retrofittedS
	 *            The retrofittedS to set
	 */
	public void setRetrofittedS(String retrofittedS) {
		this.retrofittedS = retrofittedS;
	}

	/**
	 * Gets the postBanchmarkS
	 * 
	 * @return Returns a String
	 */
	public String getPostBanchmarkS() {
		return postBanchmarkS;
	}

	/**
	 * Sets the postBanchmarkS
	 * 
	 * @param postBanchmarkS
	 *            The postBanchmarkS to set
	 */
	public void setPostBanchmarkS(String postBanchmarkS) {
		this.postBanchmarkS = postBanchmarkS;
	}

	/**
	 * Gets the zonePermit
	 * 
	 * @return Returns a String
	 */
	public String getZonePermit() {
		return zonePermit;
	}

	/**
	 * Sets the zonePermit
	 * 
	 * @param zonePermit
	 *            The zonePermit to set
	 */
	public void setZonePermit(String zonePermit) {
		this.zonePermit = zonePermit;
	}

	/**
	 * Gets the buildingName
	 * 
	 * @return Returns a String
	 */
	public String getBuildingName() {
		return buildingName;
	}

	/**
	 * Sets the buildingName
	 * 
	 * @param buildingName
	 *            The buildingName to set
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	/**
	 * Gets the retrofittedP
	 * 
	 * @return Returns a String
	 */
	public String getRetrofittedP() {
		return retrofittedP;
	}

	/**
	 * Sets the retrofittedP
	 * 
	 * @param retrofittedP
	 *            The retrofittedP to set
	 */
	public void setRetrofittedP(String retrofittedP) {
		this.retrofittedP = retrofittedP;
	}

	/**
	 * Gets the postBanchmarkP
	 * 
	 * @return Returns a String
	 */
	public String getPostBanchmarkP() {
		return postBanchmarkP;
	}

	/**
	 * Sets the postBanchmarkP
	 * 
	 * @param postBanchmarkP
	 *            The postBanchmarkP to set
	 */
	public void setPostBanchmarkP(String postBanchmarkP) {
		this.postBanchmarkP = postBanchmarkP;
	}

	/**
	 * Gets the numberOfStoriesAboveGradeZone
	 * 
	 * @return Returns a String
	 */
	public String getNumberOfStoriesAboveGradeZone() {
		return numberOfStoriesAboveGradeZone;
	}

	/**
	 * Sets the numberOfStoriesAboveGradeZone
	 * 
	 * @param numberOfStoriesAboveGradeZone
	 *            The numberOfStoriesAboveGradeZone to set
	 */
	public void setNumberOfStoriesAboveGradeZone(String numberOfStoriesAboveGradeZone) {
		this.numberOfStoriesAboveGradeZone = numberOfStoriesAboveGradeZone;
	}

	/**
	 * Gets the sl2PA
	 * 
	 * @return Returns a String
	 */
	public String getSl2PA() {
		return sl2PA;
	}

	/**
	 * Sets the sl2PA
	 * 
	 * @param sl2PA
	 *            The sl2PA to set
	 */
	public void setSl2PA(String sl2PA) {
		this.sl2PA = sl2PA;
	}

	/**
	 * Gets the currentPrimaryUse
	 * 
	 * @return Returns a String
	 */
	public String getCurrentPrimaryUse() {
		return currentPrimaryUse;
	}

	/**
	 * Sets the currentPrimaryUse
	 * 
	 * @param currentPrimaryUse
	 *            The currentPrimaryUse to set
	 */
	public void setCurrentPrimaryUse(String currentPrimaryUse) {
		this.currentPrimaryUse = currentPrimaryUse;
	}

	/**
	 * Gets the highRisePA
	 * 
	 * @return Returns a String
	 */
	public String getHighRisePA() {
		return highRisePA;
	}

	/**
	 * Sets the highRisePA
	 * 
	 * @param highRisePA
	 *            The highRisePA to set
	 */
	public void setHighRisePA(String highRisePA) {
		this.highRisePA = highRisePA;
	}

	/**
	 * Gets the softStoryPA
	 * 
	 * @return Returns a String
	 */
	public String getSoftStoryPA() {
		return softStoryPA;
	}

	/**
	 * Sets the softStoryPA
	 * 
	 * @param softStoryPA
	 *            The softStoryPA to set
	 */
	public void setSoftStoryPA(String softStoryPA) {
		this.softStoryPA = softStoryPA;
	}

	/**
	 * Gets the subterraneanCheck
	 * 
	 * @return Returns a String
	 */
	public String getSubterraneanCheck() {
		return subterraneanCheck;
	}

	/**
	 * Sets the subterraneanCheck
	 * 
	 * @param subterraneanCheck
	 *            The subterraneanCheck to set
	 */
	public void setSubterraneanCheck(String subterraneanCheck) {
		this.subterraneanCheck = subterraneanCheck;
	}

	/**
	 * Gets the plansOnFileCheck
	 * 
	 * @return Returns a String
	 */
	public String getPlansOnFileCheck() {
		return plansOnFileCheck;
	}

	/**
	 * Sets the plansOnFileCheck
	 * 
	 * @param plansOnFileCheck
	 *            The plansOnFileCheck to set
	 */
	public void setPlansOnFileCheck(String plansOnFileCheck) {
		this.plansOnFileCheck = plansOnFileCheck;
	}

	/**
	 * Gets the parcel1
	 * 
	 * @return Returns a String
	 */
	public String getParcel1() {
		return parcel1;
	}

	/**
	 * Sets the parcel1
	 * 
	 * @param parcel1
	 *            The parcel1 to set
	 */
	public void setParcel1(String parcel1) {
		this.parcel1 = parcel1;
	}

	/**
	 * Gets the poorConditionSA
	 * 
	 * @return Returns a String
	 */
	public String getPoorConditionSA() {
		return poorConditionSA;
	}

	/**
	 * Sets the poorConditionSA
	 * 
	 * @param poorConditionSA
	 *            The poorConditionSA to set
	 */
	public void setPoorConditionSA(String poorConditionSA) {
		this.poorConditionSA = poorConditionSA;
	}

	/**
	 * Gets the poundingPA
	 * 
	 * @return Returns a String
	 */
	public String getPoundingPA() {
		return poundingPA;
	}

	/**
	 * Sets the poundingPA
	 * 
	 * @param poundingPA
	 *            The poundingPA to set
	 */
	public void setPoundingPA(String poundingPA) {
		this.poundingPA = poundingPA;
	}

	/**
	 * Gets the verticalIrregularityS
	 * 
	 * @return Returns a String
	 */
	public String getVerticalIrregularityS() {
		return verticalIrregularityS;
	}

	/**
	 * Sets the verticalIrregularityS
	 * 
	 * @param verticalIrregularityS
	 *            The verticalIrregularityS to set
	 */
	public void setVerticalIrregularityS(String verticalIrregularityS) {
		this.verticalIrregularityS = verticalIrregularityS;
	}

	/**
	 * Gets the secondarySystem
	 * 
	 * @return Returns a String
	 */
	public String getSecondarySystem() {
		return secondarySystem;
	}

	/**
	 * Sets the secondarySystem
	 * 
	 * @param secondarySystem
	 *            The secondarySystem to set
	 */
	public void setSecondarySystem(String secondarySystem) {
		this.secondarySystem = secondarySystem;
	}

	/**
	 * Gets the verticalIrregularityP
	 * 
	 * @return Returns a String
	 */
	public String getVerticalIrregularityP() {
		return verticalIrregularityP;
	}

	/**
	 * Sets the verticalIrregularityP
	 * 
	 * @param verticalIrregularityP
	 *            The verticalIrregularityP to set
	 */
	public void setVerticalIrregularityP(String verticalIrregularityP) {
		this.verticalIrregularityP = verticalIrregularityP;
	}

	/**
	 * Gets the covenantOffSite
	 * 
	 * @return Returns a String
	 */
	public String getCovenantOffSite() {
		return covenantOffSite;
	}

	/**
	 * Sets the covenantOffSite
	 * 
	 * @param covenantOffSite
	 *            The covenantOffSite to set
	 */
	public void setCovenantOffSite(String covenantOffSite) {
		this.covenantOffSite = covenantOffSite;
	}

	/**
	 * Gets the primarySystem
	 * 
	 * @return Returns a String
	 */
	public String getPrimarySystem() {
		return primarySystem;
	}

	/**
	 * Sets the primarySystem
	 * 
	 * @param primarySystem
	 *            The primarySystem to set
	 */
	public void setPrimarySystem(String primarySystem) {
		this.primarySystem = primarySystem;
	}

	/**
	 * Gets the occupantLoad
	 * 
	 * @return Returns a String
	 */
	public String getOccupantLoad() {
		return occupantLoad;
	}

	/**
	 * Sets the occupantLoad
	 * 
	 * @param occupantLoad
	 *            The occupantLoad to set
	 */
	public void setOccupantLoad(String occupantLoad) {
		this.occupantLoad = occupantLoad;
	}

	/**
	 * Gets the basicSocrePA
	 * 
	 * @return Returns a String
	 */
	public String getBasicSocrePA() {
		return basicSocrePA;
	}

	/**
	 * Sets the basicSocrePA
	 * 
	 * @param basicSocrePA
	 *            The basicSocrePA to set
	 */
	public void setBasicSocrePA(String basicSocrePA) {
		this.basicSocrePA = basicSocrePA;
	}

	/**
	 * Gets the currentPrimaryUseDate
	 * 
	 * @return Returns a String
	 */
	public String getCurrentPrimaryUseDate() {
		return currentPrimaryUseDate;
	}

	/**
	 * Sets the currentPrimaryUseDate
	 * 
	 * @param currentPrimaryUseDate
	 *            The currentPrimaryUseDate to set
	 */
	public void setCurrentPrimaryUseDate(String currentPrimaryUseDate) {
		this.currentPrimaryUseDate = currentPrimaryUseDate;
	}

	/**
	 * Gets the radiobutton
	 * 
	 * @return Returns a String
	 */
	public String getRadiobutton() {
		return radiobutton;
	}

	/**
	 * Sets the radiobutton
	 * 
	 * @param radiobutton
	 *            The radiobutton to set
	 */
	public void setRadiobutton(String radiobutton) {
		this.radiobutton = radiobutton;
	}

	/**
	 * Gets the submit
	 * 
	 * @return Returns a String
	 */
	public String getSubmit() {
		return Submit;
	}

	/**
	 * Sets the submit
	 * 
	 * @param submit
	 *            The submit to set
	 */
	public void setSubmit(String submit) {
		Submit = submit;
	}

	/**
	 * Gets the parkingProvidedCheck
	 * 
	 * @return Returns a String
	 */
	public String getParkingProvidedCheck() {
		return parkingProvidedCheck;
	}

	/**
	 * Sets the parkingProvidedCheck
	 * 
	 * @param parkingProvidedCheck
	 *            The parkingProvidedCheck to set
	 */
	public void setParkingProvidedCheck(String parkingProvidedCheck) {
		this.parkingProvidedCheck = parkingProvidedCheck;
	}

	/**
	 * Gets the select2
	 * 
	 * @return Returns a String
	 */
	public String getSelect2() {
		return select2;
	}

	/**
	 * Sets the select2
	 * 
	 * @param select2
	 *            The select2 to set
	 */
	public void setSelect2(String select2) {
		this.select2 = select2;
	}

	/**
	 * Gets the in
	 * 
	 * @return Returns a String
	 */
	public String getIn() {
		return in;
	}

	/**
	 * Sets the in
	 * 
	 * @param in
	 *            The in to set
	 */
	public void setIn(String in) {
		this.in = in;
	}

	/**
	 * Gets the noOfResUnits
	 * 
	 * @return Returns a String
	 */
	public String getNoOfResUnits() {
		return NoOfResUnits;
	}

	/**
	 * Sets the noOfResUnits
	 * 
	 * @param noOfResUnits
	 *            The noOfResUnits to set
	 */
	public void setNoOfResUnits(String noOfResUnits) {
		NoOfResUnits = noOfResUnits;
	}

	/**
	 * Gets the postBanchmarkYrSA
	 * 
	 * @return Returns a String
	 */
	public String getPostBanchmarkYrSA() {
		return postBanchmarkYrSA;
	}

	/**
	 * Sets the postBanchmarkYrSA
	 * 
	 * @param postBanchmarkYrSA
	 *            The postBanchmarkYrSA to set
	 */
	public void setPostBanchmarkYrSA(String postBanchmarkYrSA) {
		this.postBanchmarkYrSA = postBanchmarkYrSA;
	}

	/**
	 * Gets the torsionS
	 * 
	 * @return Returns a String
	 */
	public String getTorsionS() {
		return torsionS;
	}

	/**
	 * Sets the torsionS
	 * 
	 * @param torsionS
	 *            The torsionS to set
	 */
	public void setTorsionS(String torsionS) {
		this.torsionS = torsionS;
	}

	/**
	 * Gets the otherZoningDeterminations
	 * 
	 * @return Returns a String
	 */
	public String getOtherZoningDeterminations() {
		return otherZoningDeterminations;
	}

	/**
	 * Sets the otherZoningDeterminations
	 * 
	 * @param otherZoningDeterminations
	 *            The otherZoningDeterminations to set
	 */
	public void setOtherZoningDeterminations(String otherZoningDeterminations) {
		this.otherZoningDeterminations = otherZoningDeterminations;
	}

	/**
	 * Gets the noOfElevators
	 * 
	 * @return Returns a String
	 */
	public String getNoOfElevators() {
		return noOfElevators;
	}

	/**
	 * Sets the noOfElevators
	 * 
	 * @param noOfElevators
	 *            The noOfElevators to set
	 */
	public void setNoOfElevators(String noOfElevators) {
		this.noOfElevators = noOfElevators;
	}

	/**
	 * Gets the torsionP
	 * 
	 * @return Returns a String
	 */
	public String getTorsionP() {
		return torsionP;
	}

	/**
	 * Sets the torsionP
	 * 
	 * @param torsionP
	 *            The torsionP to set
	 */
	public void setTorsionP(String torsionP) {
		this.torsionP = torsionP;
	}

	/**
	 * Gets the planIrregularitySA
	 * 
	 * @return Returns a String
	 */
	public String getPlanIrregularitySA() {
		return planIrregularitySA;
	}

	/**
	 * Sets the planIrregularitySA
	 * 
	 * @param planIrregularitySA
	 *            The planIrregularitySA to set
	 */
	public void setPlanIrregularitySA(String planIrregularitySA) {
		this.planIrregularitySA = planIrregularitySA;
	}

	/**
	 * Gets the onGradeCheck
	 * 
	 * @return Returns a String
	 */
	public String getOnGradeCheck() {
		return onGradeCheck;
	}

	/**
	 * Sets the onGradeCheck
	 * 
	 * @param onGradeCheck
	 *            The onGradeCheck to set
	 */
	public void setOnGradeCheck(String onGradeCheck) {
		this.onGradeCheck = onGradeCheck;
	}

	/**
	 * Gets the majorStructuralAlterationsCheck
	 * 
	 * @return Returns a String
	 */
	public String getMajorStructuralAlterationsCheck() {
		return majorStructuralAlterationsCheck;
	}

	/**
	 * Sets the majorStructuralAlterationsCheck
	 * 
	 * @param majorStructuralAlterationsCheck
	 *            The majorStructuralAlterationsCheck to set
	 */
	public void setMajorStructuralAlterationsCheck(String majorStructuralAlterationsCheck) {
		this.majorStructuralAlterationsCheck = majorStructuralAlterationsCheck;
	}

	/**
	 * Gets the shortColumnsS
	 * 
	 * @return Returns a String
	 */
	public String getShortColumnsS() {
		return shortColumnsS;
	}

	/**
	 * Sets the shortColumnsS
	 * 
	 * @param shortColumnsS
	 *            The shortColumnsS to set
	 */
	public void setShortColumnsS(String shortColumnsS) {
		this.shortColumnsS = shortColumnsS;
	}

	/**
	 * Gets the sprinkleredCheck
	 * 
	 * @return Returns a String
	 */
	public String getSprinkleredCheck() {
		return sprinkleredCheck;
	}

	/**
	 * Sets the sprinkleredCheck
	 * 
	 * @param sprinkleredCheck
	 *            The sprinkleredCheck to set
	 */
	public void setSprinkleredCheck(String sprinkleredCheck) {
		this.sprinkleredCheck = sprinkleredCheck;
	}

	/**
	 * Gets the vertivalIrregularitySA
	 * 
	 * @return Returns a String
	 */
	public String getVertivalIrregularitySA() {
		return vertivalIrregularitySA;
	}

	/**
	 * Sets the vertivalIrregularitySA
	 * 
	 * @param vertivalIrregularitySA
	 *            The vertivalIrregularitySA to set
	 */
	public void setVertivalIrregularitySA(String vertivalIrregularitySA) {
		this.vertivalIrregularitySA = vertivalIrregularitySA;
	}

	/**
	 * Gets the basicSocreS
	 * 
	 * @return Returns a String
	 */
	public String getBasicSocreS() {
		return basicSocreS;
	}

	/**
	 * Sets the basicSocreS
	 * 
	 * @param basicSocreS
	 *            The basicSocreS to set
	 */
	public void setBasicSocreS(String basicSocreS) {
		this.basicSocreS = basicSocreS;
	}

	/**
	 * Gets the highRiseS
	 * 
	 * @return Returns a String
	 */
	public String getHighRiseS() {
		return highRiseS;
	}

	/**
	 * Sets the highRiseS
	 * 
	 * @param highRiseS
	 *            The highRiseS to set
	 */
	public void setHighRiseS(String highRiseS) {
		this.highRiseS = highRiseS;
	}

	/**
	 * Gets the shortColumnsP
	 * 
	 * @return Returns a String
	 */
	public String getShortColumnsP() {
		return shortColumnsP;
	}

	/**
	 * Sets the shortColumnsP
	 * 
	 * @param shortColumnsP
	 *            The shortColumnsP to set
	 */
	public void setShortColumnsP(String shortColumnsP) {
		this.shortColumnsP = shortColumnsP;
	}

	/**
	 * Gets the commentSecondarySystemAddition
	 * 
	 * @return Returns a String
	 */
	public String getCommentSecondarySystemAddition() {
		return commentSecondarySystemAddition;
	}

	/**
	 * Sets the commentSecondarySystemAddition
	 * 
	 * @param commentSecondarySystemAddition
	 *            The commentSecondarySystemAddition to set
	 */
	public void setCommentSecondarySystemAddition(String commentSecondarySystemAddition) {
		this.commentSecondarySystemAddition = commentSecondarySystemAddition;
	}

	/**
	 * Gets the highRiseP
	 * 
	 * @return Returns a String
	 */
	public String getHighRiseP() {
		return highRiseP;
	}

	/**
	 * Sets the highRiseP
	 * 
	 * @param highRiseP
	 *            The highRiseP to set
	 */
	public void setHighRiseP(String highRiseP) {
		this.highRiseP = highRiseP;
	}

	/**
	 * Gets the dateOfBldgPermit
	 * 
	 * @return Returns a String
	 */
	public String getDateOfBldgPermit() {
		return dateOfBldgPermit;
	}

	/**
	 * Sets the dateOfBldgPermit
	 * 
	 * @param dateOfBldgPermit
	 *            The dateOfBldgPermit to set
	 */
	public void setDateOfBldgPermit(String dateOfBldgPermit) {
		this.dateOfBldgPermit = dateOfBldgPermit;
	}

	/**
	 * Gets the notes
	 * 
	 * @return Returns a String
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * Sets the notes
	 * 
	 * @param notes
	 *            The notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * Gets the liquefactionCheck
	 * 
	 * @return Returns a String
	 */
	public String getLiquefactionCheck() {
		return liquefactionCheck;
	}

	/**
	 * Sets the liquefactionCheck
	 * 
	 * @param liquefactionCheck
	 *            The liquefactionCheck to set
	 */
	public void setLiquefactionCheck(String liquefactionCheck) {
		this.liquefactionCheck = liquefactionCheck;
	}

	/**
	 * Gets the finalScoreS
	 * 
	 * @return Returns a String
	 */
	public String getFinalScoreS() {
		return FinalScoreS;
	}

	/**
	 * Sets the finalScoreS
	 * 
	 * @param finalScoreS
	 *            The finalScoreS to set
	 */
	public void setFinalScoreS(String finalScoreS) {
		FinalScoreS = finalScoreS;
	}

	/**
	 * Gets the dir1
	 * 
	 * @return Returns a String
	 */
	public String getDir1() {
		return dir1;
	}

	/**
	 * Sets the dir1
	 * 
	 * @param dir1
	 *            The dir1 to set
	 */
	public void setDir1(String dir1) {
		this.dir1 = dir1;
	}

	/**
	 * Gets the urmCheck
	 * 
	 * @return Returns a String
	 */
	public String getUrmCheck() {
		return urmCheck;
	}

	/**
	 * Sets the urmCheck
	 * 
	 * @param urmCheck
	 *            The urmCheck to set
	 */
	public void setUrmCheck(String urmCheck) {
		this.urmCheck = urmCheck;
	}

	/**
	 * Gets the torsionSA
	 * 
	 * @return Returns a String
	 */
	public String getTorsionSA() {
		return torsionSA;
	}

	/**
	 * Sets the torsionSA
	 * 
	 * @param torsionSA
	 *            The torsionSA to set
	 */
	public void setTorsionSA(String torsionSA) {
		this.torsionSA = torsionSA;
	}

	/**
	 * Gets the largeHeavyCldgSA
	 * 
	 * @return Returns a String
	 */
	public String getLargeHeavyCldgSA() {
		return largeHeavyCldgSA;
	}

	/**
	 * Sets the largeHeavyCldgSA
	 * 
	 * @param largeHeavyCldgSA
	 *            The largeHeavyCldgSA to set
	 */
	public void setLargeHeavyCldgSA(String largeHeavyCldgSA) {
		this.largeHeavyCldgSA = largeHeavyCldgSA;
	}

	/**
	 * Gets the panthouseCheck
	 * 
	 * @return Returns a String
	 */
	public String getPanthouseCheck() {
		return panthouseCheck;
	}

	/**
	 * Sets the panthouseCheck
	 * 
	 * @param panthouseCheck
	 *            The panthouseCheck to set
	 */
	public void setPanthouseCheck(String panthouseCheck) {
		this.panthouseCheck = panthouseCheck;
	}

	/**
	 * Gets the secondarySystemOfAddition
	 * 
	 * @return Returns a String
	 */
	public String getSecondarySystemOfAddition() {
		return secondarySystemOfAddition;
	}

	/**
	 * Sets the secondarySystemOfAddition
	 * 
	 * @param secondarySystemOfAddition
	 *            The secondarySystemOfAddition to set
	 */
	public void setSecondarySystemOfAddition(String secondarySystemOfAddition) {
		this.secondarySystemOfAddition = secondarySystemOfAddition;
	}

	/**
	 * Gets the noOfStructuresOnSite
	 * 
	 * @return Returns a String
	 */
	public String getNoOfStructuresOnSite() {
		return noOfStructuresOnSite;
	}

	/**
	 * Sets the noOfStructuresOnSite
	 * 
	 * @param noOfStructuresOnSite
	 *            The noOfStructuresOnSite to set
	 */
	public void setNoOfStructuresOnSite(String noOfStructuresOnSite) {
		this.noOfStructuresOnSite = noOfStructuresOnSite;
	}

	/**
	 * Gets the commentPrimarySystemAddition
	 * 
	 * @return Returns a String
	 */
	public String getCommentPrimarySystemAddition() {
		return commentPrimarySystemAddition;
	}

	/**
	 * Sets the commentPrimarySystemAddition
	 * 
	 * @param commentPrimarySystemAddition
	 *            The commentPrimarySystemAddition to set
	 */
	public void setCommentPrimarySystemAddition(String commentPrimarySystemAddition) {
		this.commentPrimarySystemAddition = commentPrimarySystemAddition;
	}

	/**
	 * Gets the sl38StoriesSA
	 * 
	 * @return Returns a String
	 */
	public String getSl38StoriesSA() {
		return sl38StoriesSA;
	}

	/**
	 * Sets the sl38StoriesSA
	 * 
	 * @param sl38StoriesSA
	 *            The sl38StoriesSA to set
	 */
	public void setSl38StoriesSA(String sl38StoriesSA) {
		this.sl38StoriesSA = sl38StoriesSA;
	}

	/**
	 * Gets the commentSecondarySystem
	 * 
	 * @return Returns a String
	 */
	public String getCommentSecondarySystem() {
		return commentSecondarySystem;
	}

	/**
	 * Sets the commentSecondarySystem
	 * 
	 * @param commentSecondarySystem
	 *            The commentSecondarySystem to set
	 */
	public void setCommentSecondarySystem(String commentSecondarySystem) {
		this.commentSecondarySystem = commentSecondarySystem;
	}

	/**
	 * Gets the shortColumnsSA
	 * 
	 * @return Returns a String
	 */
	public String getShortColumnsSA() {
		return shortColumnsSA;
	}

	/**
	 * Sets the shortColumnsSA
	 * 
	 * @param shortColumnsSA
	 *            The shortColumnsSA to set
	 */
	public void setShortColumnsSA(String shortColumnsSA) {
		this.shortColumnsSA = shortColumnsSA;
	}

	/**
	 * Gets the finalSocreP
	 * 
	 * @return Returns a String
	 */
	public String getFinalSocreP() {
		return finalSocreP;
	}

	/**
	 * Sets the finalSocreP
	 * 
	 * @param finalSocreP
	 *            The finalSocreP to set
	 */
	public void setFinalSocreP(String finalSocreP) {
		this.finalSocreP = finalSocreP;
	}

	/**
	 * Gets the finalScoreSA
	 * 
	 * @return Returns a String
	 */
	public String getFinalScoreSA() {
		return finalScoreSA;
	}

	/**
	 * Sets the finalScoreSA
	 * 
	 * @param finalScoreSA
	 *            The finalScoreSA to set
	 */
	public void setFinalScoreSA(String finalScoreSA) {
		this.finalScoreSA = finalScoreSA;
	}

	/**
	 * Gets the estimateOfWidthFt
	 * 
	 * @return Returns a String
	 */
	public String getEstimateOfWidthFt() {
		return estimateOfWidthFt;
	}

	/**
	 * Sets the estimateOfWidthFt
	 * 
	 * @param estimateOfWidthFt
	 *            The estimateOfWidthFt to set
	 */
	public void setEstimateOfWidthFt(String estimateOfWidthFt) {
		this.estimateOfWidthFt = estimateOfWidthFt;
	}

	/**
	 * Gets the structAddressCheck1
	 * 
	 * @return Returns a String
	 */
	public String getStructAddressCheck1() {
		return structAddressCheck1;
	}

	/**
	 * Sets the structAddressCheck1
	 * 
	 * @param structAddressCheck1
	 *            The structAddressCheck1 to set
	 */
	public void setStructAddressCheck1(String structAddressCheck1) {
		this.structAddressCheck1 = structAddressCheck1;
	}

	/**
	 * Gets the retrofittedPA
	 * 
	 * @return Returns a String
	 */
	public String getRetrofittedPA() {
		return retrofittedPA;
	}

	/**
	 * Sets the retrofittedPA
	 * 
	 * @param retrofittedPA
	 *            The retrofittedPA to set
	 */
	public void setRetrofittedPA(String retrofittedPA) {
		this.retrofittedPA = retrofittedPA;
	}

	/**
	 * Gets the inLieuParking
	 * 
	 * @return Returns a String
	 */
	public String getInLieuParking() {
		return inLieuParking;
	}

	/**
	 * Sets the inLieuParking
	 * 
	 * @param inLieuParking
	 *            The inLieuParking to set
	 */
	public void setInLieuParking(String inLieuParking) {
		this.inLieuParking = inLieuParking;
	}

	/**
	 * Gets the occupancyDiv
	 * 
	 * @return Returns a String
	 */
	public String getOccupancyDiv() {
		return occupancyDiv;
	}

	/**
	 * Sets the occupancyDiv
	 * 
	 * @param occupancyDiv
	 *            The occupancyDiv to set
	 */
	public void setOccupancyDiv(String occupancyDiv) {
		this.occupancyDiv = occupancyDiv;
	}

	/**
	 * Gets the highFireSeverityZoneCheck
	 * 
	 * @return Returns a String
	 */
	public String getHighFireSeverityZoneCheck() {
		return highFireSeverityZoneCheck;
	}

	/**
	 * Sets the highFireSeverityZoneCheck
	 * 
	 * @param highFireSeverityZoneCheck
	 *            The highFireSeverityZoneCheck to set
	 */
	public void setHighFireSeverityZoneCheck(String highFireSeverityZoneCheck) {
		this.highFireSeverityZoneCheck = highFireSeverityZoneCheck;
	}

	/**
	 * Gets the bldgPermitNo
	 * 
	 * @return Returns a String
	 */
	public String getBldgPermitNo() {
		return bldgPermitNo;
	}

	/**
	 * Sets the bldgPermitNo
	 * 
	 * @param bldgPermitNo
	 *            The bldgPermitNo to set
	 */
	public void setBldgPermitNo(String bldgPermitNo) {
		this.bldgPermitNo = bldgPermitNo;
	}

	/**
	 * Gets the majorStructuralAdditionsDate
	 * 
	 * @return Returns a String
	 */
	public String getMajorStructuralAdditionsDate() {
		return majorStructuralAdditionsDate;
	}

	/**
	 * Sets the majorStructuralAdditionsDate
	 * 
	 * @param majorStructuralAdditionsDate
	 *            The majorStructuralAdditionsDate to set
	 */
	public void setMajorStructuralAdditionsDate(String majorStructuralAdditionsDate) {
		this.majorStructuralAdditionsDate = majorStructuralAdditionsDate;
	}

	/**
	 * Gets the withinStructureCheck
	 * 
	 * @return Returns a String
	 */
	public String getWithinStructureCheck() {
		return withinStructureCheck;
	}

	/**
	 * Sets the withinStructureCheck
	 * 
	 * @param withinStructureCheck
	 *            The withinStructureCheck to set
	 */
	public void setWithinStructureCheck(String withinStructureCheck) {
		this.withinStructureCheck = withinStructureCheck;
	}

	/**
	 * Gets the sl38StoriesS
	 * 
	 * @return Returns a String
	 */
	public String getSl38StoriesS() {
		return sl38StoriesS;
	}

	/**
	 * Sets the sl38StoriesS
	 * 
	 * @param sl38StoriesS
	 *            The sl38StoriesS to set
	 */
	public void setSl38StoriesS(String sl38StoriesS) {
		this.sl38StoriesS = sl38StoriesS;
	}

	/**
	 * Gets the zoneCurrent
	 * 
	 * @return Returns a String
	 */
	public String getZoneCurrent() {
		return zoneCurrent;
	}

	/**
	 * Sets the zoneCurrent
	 * 
	 * @param zoneCurrent
	 *            The zoneCurrent to set
	 */
	public void setZoneCurrent(String zoneCurrent) {
		this.zoneCurrent = zoneCurrent;
	}

	/**
	 * Gets the majorStructuralAlterations
	 * 
	 * @return Returns a String
	 */
	public String getMajorStructuralAlterations() {
		return majorStructuralAlterations;
	}

	/**
	 * Sets the majorStructuralAlterations
	 * 
	 * @param majorStructuralAlterations
	 *            The majorStructuralAlterations to set
	 */
	public void setMajorStructuralAlterations(String majorStructuralAlterations) {
		this.majorStructuralAlterations = majorStructuralAlterations;
	}

	/**
	 * Gets the sl38StoriesP
	 * 
	 * @return Returns a String
	 */
	public String getSl38StoriesP() {
		return sl38StoriesP;
	}

	/**
	 * Sets the sl38StoriesP
	 * 
	 * @param sl38StoriesP
	 *            The sl38StoriesP to set
	 */
	public void setSl38StoriesP(String sl38StoriesP) {
		this.sl38StoriesP = sl38StoriesP;
	}

	/**
	 * Gets the sl3SA
	 * 
	 * @return Returns a String
	 */
	public String getSl3SA() {
		return sl3SA;
	}

	/**
	 * Sets the sl3SA
	 * 
	 * @param sl3SA
	 *            The sl3SA to set
	 */
	public void setSl3SA(String sl3SA) {
		this.sl3SA = sl3SA;
	}

	/**
	 * Gets the localFaultZoneCheck
	 * 
	 * @return Returns a String
	 */
	public String getLocalFaultZoneCheck() {
		return localFaultZoneCheck;
	}

	/**
	 * Sets the localFaultZoneCheck
	 * 
	 * @param localFaultZoneCheck
	 *            The localFaultZoneCheck to set
	 */
	public void setLocalFaultZoneCheck(String localFaultZoneCheck) {
		this.localFaultZoneCheck = localFaultZoneCheck;
	}

	/**
	 * Gets the sl2SA
	 * 
	 * @return Returns a String
	 */
	public String getSl2SA() {
		return sl2SA;
	}

	/**
	 * Sets the sl2SA
	 * 
	 * @param sl2SA
	 *            The sl2SA to set
	 */
	public void setSl2SA(String sl2SA) {
		this.sl2SA = sl2SA;
	}

	/**
	 * Gets the planIrregularityS
	 * 
	 * @return Returns a String
	 */
	public String getPlanIrregularityS() {
		return planIrregularityS;
	}

	/**
	 * Sets the planIrregularityS
	 * 
	 * @param planIrregularityS
	 *            The planIrregularityS to set
	 */
	public void setPlanIrregularityS(String planIrregularityS) {
		this.planIrregularityS = planIrregularityS;
	}

	/**
	 * Gets the low1
	 * 
	 * @return Returns a String
	 */
	public String getLow1() {
		return low1;
	}

	/**
	 * Sets the low1
	 * 
	 * @param low1
	 *            The low1 to set
	 */
	public void setLow1(String low1) {
		this.low1 = low1;
	}

	/**
	 * Gets the hazardousMeterialsCheck
	 * 
	 * @return Returns a String
	 */
	public String getHazardousMeterialsCheck() {
		return hazardousMeterialsCheck;
	}

	/**
	 * Sets the hazardousMeterialsCheck
	 * 
	 * @param hazardousMeterialsCheck
	 *            The hazardousMeterialsCheck to set
	 */
	public void setHazardousMeterialsCheck(String hazardousMeterialsCheck) {
		this.hazardousMeterialsCheck = hazardousMeterialsCheck;
	}

	/**
	 * Gets the planIrregularityP
	 * 
	 * @return Returns a String
	 */
	public String getPlanIrregularityP() {
		return planIrregularityP;
	}

	/**
	 * Sets the planIrregularityP
	 * 
	 * @param planIrregularityP
	 *            The planIrregularityP to set
	 */
	public void setPlanIrregularityP(String planIrregularityP) {
		this.planIrregularityP = planIrregularityP;
	}

	/**
	 * Gets the parkingRequired
	 * 
	 * @return Returns a String
	 */
	public String getParkingRequired() {
		return parkingRequired;
	}

	/**
	 * Sets the parkingRequired
	 * 
	 * @param parkingRequired
	 *            The parkingRequired to set
	 */
	public void setParkingRequired(String parkingRequired) {
		this.parkingRequired = parkingRequired;
	}

	/**
	 * Gets the poorConditionPA
	 * 
	 * @return Returns a String
	 */
	public String getPoorConditionPA() {
		return poorConditionPA;
	}

	/**
	 * Sets the poorConditionPA
	 * 
	 * @param poorConditionPA
	 *            The poorConditionPA to set
	 */
	public void setPoorConditionPA(String poorConditionPA) {
		this.poorConditionPA = poorConditionPA;
	}

	/**
	 * Gets the highRiseSA
	 * 
	 * @return Returns a String
	 */
	public String getHighRiseSA() {
		return highRiseSA;
	}

	/**
	 * Sets the highRiseSA
	 * 
	 * @param highRiseSA
	 *            The highRiseSA to set
	 */
	public void setHighRiseSA(String highRiseSA) {
		this.highRiseSA = highRiseSA;
	}

	/**
	 * Gets the verticalIrregularityPA
	 * 
	 * @return Returns a String
	 */
	public String getVerticalIrregularityPA() {
		return verticalIrregularityPA;
	}

	/**
	 * Sets the verticalIrregularityPA
	 * 
	 * @param verticalIrregularityPA
	 *            The verticalIrregularityPA to set
	 */
	public void setVerticalIrregularityPA(String verticalIrregularityPA) {
		this.verticalIrregularityPA = verticalIrregularityPA;
	}

	/**
	 * Gets the occupantLoadCurrent
	 * 
	 * @return Returns a String
	 */
	public String getOccupantLoadCurrent() {
		return occupantLoadCurrent;
	}

	/**
	 * Sets the occupantLoadCurrent
	 * 
	 * @param occupantLoadCurrent
	 *            The occupantLoadCurrent to set
	 */
	public void setOccupantLoadCurrent(String occupantLoadCurrent) {
		this.occupantLoadCurrent = occupantLoadCurrent;
	}

	/**
	 * Gets the poundingSA
	 * 
	 * @return Returns a String
	 */
	public String getPoundingSA() {
		return poundingSA;
	}

	/**
	 * Sets the poundingSA
	 * 
	 * @param poundingSA
	 *            The poundingSA to set
	 */
	public void setPoundingSA(String poundingSA) {
		this.poundingSA = poundingSA;
	}

	/**
	 * Gets the ft
	 * 
	 * @return Returns a String
	 */
	public String getFt() {
		return ft;
	}

	/**
	 * Sets the ft
	 * 
	 * @param ft
	 *            The ft to set
	 */
	public void setFt(String ft) {
		this.ft = ft;
	}

	/**
	 * Gets the alarmsCheck
	 * 
	 * @return Returns a String
	 */
	public String getAlarmsCheck() {
		return alarmsCheck;
	}

	/**
	 * Sets the alarmsCheck
	 * 
	 * @param alarmsCheck
	 *            The alarmsCheck to set
	 */
	public void setAlarmsCheck(String alarmsCheck) {
		this.alarmsCheck = alarmsCheck;
	}

	/**
	 * Gets the basicSocreSA
	 * 
	 * @return Returns a String
	 */
	public String getBasicSocreSA() {
		return basicSocreSA;
	}

	/**
	 * Sets the basicSocreSA
	 * 
	 * @param basicSocreSA
	 *            The basicSocreSA to set
	 */
	public void setBasicSocreSA(String basicSocreSA) {
		this.basicSocreSA = basicSocreSA;
	}

	/**
	 * Gets the poundingS
	 * 
	 * @return Returns a String
	 */
	public String getPoundingS() {
		return poundingS;
	}

	/**
	 * Sets the poundingS
	 * 
	 * @param poundingS
	 *            The poundingS to set
	 */
	public void setPoundingS(String poundingS) {
		this.poundingS = poundingS;
	}

	/**
	 * Gets the postBanchmarkYrPA
	 * 
	 * @return Returns a String
	 */
	public String getPostBanchmarkYrPA() {
		return postBanchmarkYrPA;
	}

	/**
	 * Sets the postBanchmarkYrPA
	 * 
	 * @param postBanchmarkYrPA
	 *            The postBanchmarkYrPA to set
	 */
	public void setPostBanchmarkYrPA(String postBanchmarkYrPA) {
		this.postBanchmarkYrPA = postBanchmarkYrPA;
	}

	/**
	 * Gets the stripedCheck
	 * 
	 * @return Returns a String
	 */
	public String getStripedCheck() {
		return stripedCheck;
	}

	/**
	 * Sets the stripedCheck
	 * 
	 * @param stripedCheck
	 *            The stripedCheck to set
	 */
	public void setStripedCheck(String stripedCheck) {
		this.stripedCheck = stripedCheck;
	}

	/**
	 * Gets the covenantOnSite
	 * 
	 * @return Returns a String
	 */
	public String getCovenantOnSite() {
		return covenantOnSite;
	}

	/**
	 * Sets the covenantOnSite
	 * 
	 * @param covenantOnSite
	 *            The covenantOnSite to set
	 */
	public void setCovenantOnSite(String covenantOnSite) {
		this.covenantOnSite = covenantOnSite;
	}

	/**
	 * Gets the sl3S
	 * 
	 * @return Returns a String
	 */
	public String getSl3S() {
		return sl3S;
	}

	/**
	 * Sets the sl3S
	 * 
	 * @param sl3S
	 *            The sl3S to set
	 */
	public void setSl3S(String sl3S) {
		this.sl3S = sl3S;
	}

	/**
	 * Gets the poundingP
	 * 
	 * @return Returns a String
	 */
	public String getPoundingP() {
		return poundingP;
	}

	/**
	 * Sets the poundingP
	 * 
	 * @param poundingP
	 *            The poundingP to set
	 */
	public void setPoundingP(String poundingP) {
		this.poundingP = poundingP;
	}

	/**
	 * Gets the sl3P
	 * 
	 * @return Returns a String
	 */
	public String getSl3P() {
		return sl3P;
	}

	/**
	 * Sets the sl3P
	 * 
	 * @param sl3P
	 *            The sl3P to set
	 */
	public void setSl3P(String sl3P) {
		this.sl3P = sl3P;
	}

	/**
	 * Gets the index
	 * 
	 * @return Returns a String
	 */
	public String getIndex() {
		return index;
	}

	/**
	 * Sets the index
	 * 
	 * @param index
	 *            The index to set
	 */
	public void setIndex(String index) {
		this.index = index;
	}

	/**
	 * Gets the bldgConstTypeCode
	 * 
	 * @return Returns a String
	 */
	public String getBldgConstTypeCode() {
		return bldgConstTypeCode;
	}

	/**
	 * Sets the bldgConstTypeCode
	 * 
	 * @param bldgConstTypeCode
	 *            The bldgConstTypeCode to set
	 */
	public void setBldgConstTypeCode(String bldgConstTypeCode) {
		this.bldgConstTypeCode = bldgConstTypeCode;
	}

	/**
	 * Gets the planIrregularityPA
	 * 
	 * @return Returns a String
	 */
	public String getPlanIrregularityPA() {
		return planIrregularityPA;
	}

	/**
	 * Sets the planIrregularityPA
	 * 
	 * @param planIrregularityPA
	 *            The planIrregularityPA to set
	 */
	public void setPlanIrregularityPA(String planIrregularityPA) {
		this.planIrregularityPA = planIrregularityPA;
	}

	/**
	 * Gets the retrofittedCheck
	 * 
	 * @return Returns a String
	 */
	public String getRetrofittedCheck() {
		return retrofittedCheck;
	}

	/**
	 * Sets the retrofittedCheck
	 * 
	 * @param retrofittedCheck
	 *            The retrofittedCheck to set
	 */
	public void setRetrofittedCheck(String retrofittedCheck) {
		this.retrofittedCheck = retrofittedCheck;
	}

	/**
	 * Gets the commentPrimarySystem
	 * 
	 * @return Returns a String
	 */
	public String getCommentPrimarySystem() {
		return commentPrimarySystem;
	}

	/**
	 * Sets the commentPrimarySystem
	 * 
	 * @param commentPrimarySystem
	 *            The commentPrimarySystem to set
	 */
	public void setCommentPrimarySystem(String commentPrimarySystem) {
		this.commentPrimarySystem = commentPrimarySystem;
	}

	/**
	 * Gets the yearPermitAppliedCheck
	 * 
	 * @return Returns a String
	 */
	public String getYearPermitAppliedCheck() {
		return yearPermitAppliedCheck;
	}

	/**
	 * Sets the yearPermitAppliedCheck
	 * 
	 * @param yearPermitAppliedCheck
	 *            The yearPermitAppliedCheck to set
	 */
	public void setYearPermitAppliedCheck(String yearPermitAppliedCheck) {
		this.yearPermitAppliedCheck = yearPermitAppliedCheck;
	}

	/**
	 * Gets the majorStructuralAdditionsCheck
	 * 
	 * @return Returns a String
	 */
	public String getMajorStructuralAdditionsCheck() {
		return majorStructuralAdditionsCheck;
	}

	/**
	 * Sets the majorStructuralAdditionsCheck
	 * 
	 * @param majorStructuralAdditionsCheck
	 *            The majorStructuralAdditionsCheck to set
	 */
	public void setMajorStructuralAdditionsCheck(String majorStructuralAdditionsCheck) {
		this.majorStructuralAdditionsCheck = majorStructuralAdditionsCheck;
	}

	/**
	 * Gets the yearPermitAppliedDate
	 * 
	 * @return Returns a String
	 */
	public String getYearPermitAppliedDate() {
		return yearPermitAppliedDate;
	}

	/**
	 * Sets the yearPermitAppliedDate
	 * 
	 * @param yearPermitAppliedDate
	 *            The yearPermitAppliedDate to set
	 */
	public void setYearPermitAppliedDate(String yearPermitAppliedDate) {
		this.yearPermitAppliedDate = yearPermitAppliedDate;
	}

	/**
	 * Gets the floodAreaCheck
	 * 
	 * @return Returns a String
	 */
	public String getFloodAreaCheck() {
		return floodAreaCheck;
	}

	/**
	 * Sets the floodAreaCheck
	 * 
	 * @param floodAreaCheck
	 *            The floodAreaCheck to set
	 */
	public void setFloodAreaCheck(String floodAreaCheck) {
		this.floodAreaCheck = floodAreaCheck;
	}

	/**
	 * Gets the majorStructuralAlterationsDate
	 * 
	 * @return Returns a String
	 */
	public String getMajorStructuralAlterationsDate() {
		return majorStructuralAlterationsDate;
	}

	/**
	 * Sets the majorStructuralAlterationsDate
	 * 
	 * @param majorStructuralAlterationsDate
	 *            The majorStructuralAlterationsDate to set
	 */
	public void setMajorStructuralAlterationsDate(String majorStructuralAlterationsDate) {
		this.majorStructuralAlterationsDate = majorStructuralAlterationsDate;
	}

	/**
	 * Gets the address
	 * 
	 * @return Returns a String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the totalFloorArea
	 * 
	 * @return Returns a String
	 */
	public String getTotalFloorArea() {
		return totalFloorArea;
	}

	/**
	 * Sets the totalFloorArea
	 * 
	 * @param totalFloorArea
	 *            The totalFloorArea to set
	 */
	public void setTotalFloorArea(String totalFloorArea) {
		this.totalFloorArea = totalFloorArea;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		primarySystemOfAddition = null;
		followUp = null;
		streetName = null;
		poorConditionS = null;
		basicScoreP = null;
		torsionPA = null;
		designCode = null;
		poorConditionP = null;
		largeHeavyCldgPA = null;
		sl2S = null;
		largeHeavyCldgS = null;
		sl2P = null;
		largeHeavyCldgP = null;
		softSotrySA = null;
		mezzanineCheck = null;
		elevatorEnclosureCheck = null;
		sl38StoriesPA = null;
		parcelNo = null;
		onsiteParkingCheck = null;
		shortColumnsPA = null;
		finalScorePA = null;
		softStoryS = null;
		numberOfStoriesAboveGrade = null;
		cansusTract = null;
		softStoryP = null;
		areaOfAparking = null;
		retrofittedSA = null;
		estimateOfWidthIn = null;
		totalFloorAreaBldgCode = null;
		sl3PA = null;
		retrofittedS = null;
		postBanchmarkS = null;
		zonePermit = null;
		buildingName = null;
		retrofittedP = null;
		postBanchmarkP = null;
		numberOfStoriesAboveGradeZone = null;
		sl2PA = null;
		currentPrimaryUse = null;
		highRisePA = null;
		softStoryPA = null;
		subterraneanCheck = null;
		plansOnFileCheck = null;
		parcel1 = null;
		poorConditionSA = null;
		poundingPA = null;
		verticalIrregularityS = null;
		secondarySystem = null;
		verticalIrregularityP = null;
		covenantOffSite = null;
		primarySystem = null;
		occupantLoad = null;
		basicSocrePA = null;
		currentPrimaryUseDate = null;
		radiobutton = null;
		Submit = null;
		parkingProvidedCheck = null;
		select2 = null;
		in = null;
		NoOfResUnits = null;
		postBanchmarkYrSA = null;
		torsionS = null;
		otherZoningDeterminations = null;
		noOfElevators = null;
		torsionP = null;
		planIrregularitySA = null;
		onGradeCheck = null;
		majorStructuralAlterationsCheck = null;
		shortColumnsS = null;
		sprinkleredCheck = null;
		vertivalIrregularitySA = null;
		basicSocreS = null;
		highRiseS = null;
		shortColumnsP = null;
		commentSecondarySystemAddition = null;
		highRiseP = null;
		dateOfBldgPermit = null;
		notes = null;
		liquefactionCheck = null;
		FinalScoreS = null;
		dir1 = null;
		urmCheck = null;
		torsionSA = null;
		largeHeavyCldgSA = null;
		panthouseCheck = null;
		secondarySystemOfAddition = null;
		noOfStructuresOnSite = null;
		commentPrimarySystemAddition = null;
		sl38StoriesSA = null;
		commentSecondarySystem = null;
		shortColumnsSA = null;
		finalSocreP = null;
		finalScoreSA = null;
		estimateOfWidthFt = null;
		structAddressCheck1 = null;
		retrofittedPA = null;
		inLieuParking = null;
		occupancyDiv = null;
		highFireSeverityZoneCheck = null;
		bldgPermitNo = null;
		majorStructuralAdditionsDate = null;
		withinStructureCheck = null;
		sl38StoriesS = null;
		zoneCurrent = null;
		majorStructuralAlterations = null;
		sl38StoriesP = null;
		sl3SA = null;
		localFaultZoneCheck = null;
		numberOfStoriesBelowGrade = null;
		sl2SA = null;
		planIrregularityS = null;
		low1 = null;
		hazardousMeterialsCheck = null;
		planIrregularityP = null;
		parkingRequired = null;
		poorConditionPA = null;
		highRiseSA = null;
		verticalIrregularityPA = null;
		occupantLoadCurrent = null;
		poundingSA = null;
		ft = null;
		alarmsCheck = null;
		basicSocreSA = null;
		poundingS = null;
		postBanchmarkYrPA = null;
		stripedCheck = null;
		covenantOnSite = null;
		sl3S = null;
		poundingP = null;
		sl3P = null;
		index = null;
		bldgConstTypeCode = null;
		planIrregularityPA = null;
		retrofittedCheck = null;
		commentPrimarySystem = null;
		yearPermitAppliedCheck = null;
		majorStructuralAdditionsCheck = null;
		yearPermitAppliedDate = null;
		floodAreaCheck = null;
		majorStructuralAlterationsDate = null;
		address = null;
		totalFloorArea = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

} // End class

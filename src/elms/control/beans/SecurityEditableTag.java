package elms.control.beans;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.Tag;

import org.apache.log4j.Logger;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.common.Constants;
import elms.security.Group;
import elms.security.User;

public class SecurityEditableTag implements Tag {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SecurityEditableTag.class.getName());
	private PageContext pageContext;
	private Tag parentTag;
	private String levelId;
	private String levelType;
	private String editProperty = "";

	public void setPageContext(PageContext thePageContext) {
		pageContext = thePageContext;
	}

	public void setParent(Tag theParentTag) {
		parentTag = theParentTag;
	}

	public Tag getParent() {
		return parentTag;
	}

	public int doStartTag() throws JspException {
		HttpSession session = pageContext.getSession();

		// logger.debug("readProperty " + editProperty);
		int flag = SKIP_BODY;

		try {
			JspWriter out = pageContext.getOut();

			if ("editable".equals(editProperty)) {
				if (checkForEditable()) {
					flag = EVAL_PAGE;
				} else {
					flag = SKIP_BODY;
				}

				return flag;
			} else if ("allProjectAndDept".equals(editProperty)) {
				if (checkForProjectNameTypeAndDepartmentCode()) {
					flag = EVAL_PAGE;
				} else {
					flag = SKIP_BODY;
				}

				return flag;
			} else if ("checkUser".equals(editProperty)) {
				if (checkUserLevel()) {
					flag = EVAL_PAGE;
				} else {
					flag = SKIP_BODY;
				}

				return flag;
			} else {
				return flag;
			}
		} catch (Exception exc) {
			throw new JspException(exc);
		}
	}

	public boolean checkUserLevel() throws Exception {
		boolean flag = true;
		HttpSession session = pageContext.getSession();

		try {
			User user1 = (User) session.getAttribute(Constants.USER_KEY);

			if (4 == user1.getRole().getRoleId()) { // 4 means view Only user
				flag = false;
			}
		} catch (Exception e) {
			logger.error("Exception In checkUserLevel(): " + e.getMessage());
			throw e;
		}

		return flag;
	}

	public boolean checkForEditable() throws Exception {
		logger.debug("LevelId: " + levelId + ", Level: " + levelType);

		boolean flag = false;

		try {
			int tmpLevelId = Integer.parseInt(levelId);
			flag = new CommonAgent().editable(tmpLevelId, levelType);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flag;
	}

	public boolean checkForProjectNameTypeAndDepartmentCode() throws Exception {
		boolean status = false;

		String projectNameType;
		HttpSession session = pageContext.getSession();
		logger.debug("LevelId: " + levelId + ", Level: " + levelType);

		try {
			User user = (User) session.getAttribute(Constants.USER_KEY);
			String departmentCode = user.getDepartment().getDepartmentCode();
			logger.debug("department code is " + departmentCode);

			boolean businessLicenseUser = false;
			boolean businessTaxUser = false;
			boolean regulatoryPermitUser = false;
			boolean codeInspectorUser = false;
			boolean parkingUser = false;
			boolean housingUser = false;
			
			List<Group> groups = user.getGroups();
			Iterator itr = groups.iterator();

			while (itr.hasNext()) {
				elms.security.Group group = (elms.security.Group) itr.next();

				if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) {
					businessLicenseUser = true;
				}
				if (group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) {
					businessTaxUser = true;
				}
				if (group.groupId == Constants.GROUPS_REGULATORY_PERMIT) {
					regulatoryPermitUser = true;
				}
				if (group.groupId == Constants.GROUPS_CODE_INSPECTOR) {
					codeInspectorUser = true;
				}
				if (group.groupId == Constants.GROUPS_PARKING) {
					parkingUser = true;
				}
				if (group.groupId == Constants.GROUPS_HOUSING_USER) {
					housingUser = true;
				}
			}

			int tmpLevelId = Integer.parseInt(levelId);
			int projNameId = LookupAgent.getProjectNameId(levelType, tmpLevelId);
			projectNameType = LookupAgent.getProjectName(projNameId);
			logger.debug("project name is " + projectNameType);

			String subProjectName = LookupAgent.getSubProjectNameForActivityId("" + tmpLevelId);
			logger.debug("sub project name is " + subProjectName);
			logger.debug("projectNameType is " + projectNameType);
			logger.debug("departmentCode is " + departmentCode);

			if (((projectNameType.equals(Constants.PROJECT_NAME_PARKING)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_PARKING_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_BUILDING)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_BUILDING_SAFETY_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_PLANNING)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_PLANNING_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_PUBLIC_WORKS)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_PUBLIC_WORKS_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_LICENSE_AND_CODE)) && codeInspectorUser) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES)) && (businessLicenseUser)) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES)) && (businessTaxUser)) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_HOUSING)) && (housingUser)) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES)) && (regulatoryPermitUser))) 
			{
				status = true;
				logger.debug("Status is " + status);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return status;
	}

	public boolean securityPermission() throws Exception {
		boolean status = false;
		String projectNameType;
		HttpSession session = pageContext.getSession();
		logger.debug("LevelId: " + levelId + ", Level: " + levelType);

		try {
			User user = (User) session.getAttribute(Constants.USER_KEY);
			String departmentCode = user.getDepartment().getDepartmentCode();
			logger.debug("department code is " + departmentCode);

			projectNameType = LookupAgent.getProjectNameForProjectId(levelId);
			logger.debug("project name is " + projectNameType);

			int tmpLevelId = Integer.parseInt(levelId);
			status = new CommonAgent().editable(tmpLevelId, levelType);

					if (((projectNameType.equals(Constants.PROJECT_NAME_BUILDING)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_BUILDING_SAFETY_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_PLANNING)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_PLANNING_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_LICENSE_AND_CODE)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_LICENSE_CODE_SERVICES_CODE))) 
					|| ((projectNameType.equals(Constants.PROJECT_NAME_PUBLIC_WORKS)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_PUBLIC_WORKS_CODE)))
					|| ((projectNameType.equals(Constants.PROJECT_NAME_HOUSING)) && (departmentCode.equalsIgnoreCase(Constants.DEPARTMENT_HOUSING_CODE))))
			
			{
				status = true;
			}
		} catch (Exception e) {
			logger.error("Exception occured with security permission " + e.getMessage());
		}

		return status;
	}

	public int doEndTag() {
		return EVAL_PAGE;
	}

	public void release() // Needs to be there because implements TAG
	{
	}

	/**
	 * @return Returns the levelId.
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * @param levelId
	 *            The levelId to set.
	 */
	public void setLevelId(String levelId) {
		this.levelId = levelId;
	}

	/**
	 * @return Returns the levelType.
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * @param levelType
	 *            The levelType to set.
	 */
	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	/**
	 * @return Returns the editProperty.
	 */
	public String getEditProperty() {
		return editProperty;
	}

	/**
	 * @param editProperty
	 *            The editProperty to set.
	 */
	public void setEditProperty(String editProperty) {
		this.editProperty = editProperty;
	}
}

package elms.control.beans;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.finance.Fee;
import elms.util.StringUtils;

public class FeesForm extends ActionForm {

	static Logger logger = Logger.getLogger(FeesForm.class.getName());

	Fee fee;
	protected String strCreationDate;
	protected String strFactor;
	protected String strFeeFactor;

	public FeesForm() {
		fee = new Fee();
	}

	/**
	 * Gets the fee
	 * 
	 * @return Returns a Fee
	 */
	public Fee getFee() {
		return fee;
	}

	/**
	 * Sets the fee
	 * 
	 * @param fee
	 *            The fee to set
	 */
	public void setFee(Fee fee) {
		try {
			if (fee != null) {
				PropertyUtils.copyProperties(this.fee, fee);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Gets the strCreationDate
	 * 
	 * @return Returns a String
	 */
	public String getStrCreationDate() {
		strCreationDate = StringUtils.cal2str(fee.creationDate);
		return strCreationDate;
	}

	/**
	 * Sets the strCreationDate
	 * 
	 * @param strCreationDate
	 *            The strCreationDate to set
	 */
	public void setStrCreationDate(String strCreationDate) {
		this.fee.creationDate = StringUtils.str2cal(strCreationDate);
		this.strCreationDate = strCreationDate;
	}

	/**
	 * Gets the strFactor
	 * 
	 * @return Returns a String
	 */
	public String getStrFactor() {
		strFactor = "" + fee.factor;
		return strFactor;
	}

	/**
	 * Sets the strFactor
	 * 
	 * @param strFactor
	 *            The strFactor to set
	 */
	public void setStrFactor(String strFactor) {
		this.fee.factor = StringUtils.s2bd(strFactor);
		this.strFactor = strFactor;
	}

	/**
	 * Gets the strFeeFactor
	 * 
	 * @return Returns a String
	 */
	public String getStrFeeFactor() {
		strFeeFactor = "" + fee.feeFactor;
		return strFeeFactor;
	}

	/**
	 * Sets the strFeeFactor
	 * 
	 * @param strFeeFactor
	 *            The strFeeFactor to set
	 */
	public void setStrFeeFactor(String strFeeFactor) {
		this.fee.feeFactor = StringUtils.s2bd(strFeeFactor);
		this.strFeeFactor = strFeeFactor;
	}

}
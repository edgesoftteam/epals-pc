package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ListCommentForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String internal;

	/**
	 * method to set the value
	 * 
	 * @param String
	 * @return void
	 */
	public void setInternal(String internal) {
		this.internal = internal;
	} // End method setInternal

	/**
	 * method to get the value
	 * 
	 * @return String
	 * 
	 */
	public String getInternal() {
		return this.internal;
	} // End method getInternal

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		internal = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

} // End class

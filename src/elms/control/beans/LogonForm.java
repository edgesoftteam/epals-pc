package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Anand Belaguly
 */
public class LogonForm extends ActionForm {

	protected String password;
	protected String username;
	protected boolean changePassword = false;
	protected String newPassword;
	protected String confirmPassword;

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		this.username = null;
		this.password = null;
		this.changePassword = false;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		if ((username == null) || (username.length() < 1))
			errors.add("username", new ActionError("error.username.required"));

		if ((password == null) || (password.length() < 1))
			errors.add("password", new ActionError("error.password.required"));

		return errors;
	}

	/**
	 * method to set the value
	 * 
	 * @param String
	 * @return void
	 */
	public void setPassword(String password) {
		this.password = password;
	} // End method setPassword

	/**
	 * method to get the value
	 * 
	 * @return String
	 * 
	 */
	public String getPassword() {
		return this.password;
	} // End method getPassword

	/**
	 * method to set the value
	 * 
	 * @param String
	 * @return void
	 */
	public void setUsername(String username) {
		this.username = username;
	} // End method setUsername

	/**
	 * method to get the value
	 * 
	 * @return String
	 * 
	 */
	public String getUsername() {
		return this.username;
	} // End method getUsername

	/**
	 * @return
	 */
	public boolean isChangePassword() {
		return changePassword;
	}

	/**
	 * @param b
	 */
	public void setChangePassword(boolean b) {
		changePassword = b;
	}

	/**
	 * @return
	 */
	public String getConfirmPassword() {
		return confirmPassword;
	}

	/**
	 * @return
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param string
	 */
	public void setConfirmPassword(String string) {
		confirmPassword = string;
	}

	/**
	 * @param string
	 */
	public void setNewPassword(String string) {
		newPassword = string;
	}

}
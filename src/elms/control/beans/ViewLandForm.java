package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ViewLandForm extends ActionForm {

	/**
	 * Member variable declaration
	 */

	protected String apn;
	protected String lsoId;
	protected String alias;
	protected String description;
	protected String zone;
	protected String use;
	protected String coordinateX;
	protected String coordinateY;
	protected String active;
	protected List addressRangeList;
	protected String addressStreetNumber;
	protected String addressStreetModifier;
	protected String addressStreetName;
	protected String addressCity;
	protected String addressState;
	protected String addressUnit;
	protected String addressZip;
	protected String addressZip4;
	protected String addressPrimary;
	protected String addressActive;
	protected String addressDescription;

	protected String apnOwnerId;
	protected String apnOwnerName;
	protected String apnStreetNumber;
	protected String apnStreetModifier;
	protected String apnStreetName;
	protected String apnCity;
	protected String apnState;
	protected String apnZip;
	protected String apnZip4;
	protected String apnEmail;
	protected String apnPhone;
	protected String apnFax;
	protected String apnForeignFlag = "N";
	protected String apnForeignAddress1;
	protected String apnForeignAddress2;
	protected String apnForeignAddress3;
	protected String apnForeignAddress4;
	protected String apnCountry;
	protected String parkingZone;
	protected String resZone;
	protected String label;
	protected String highFireArea;
	protected String gMapAddressString;

	
	
	public String getgMapAddressString() {
		gMapAddressString = addressStreetNumber+"+"+addressStreetName+"+"+addressCity+","+addressState;
		return gMapAddressString;
	}

	public void setgMapAddressString(String gMapAddressString) {
		this.gMapAddressString = gMapAddressString;
	}

	/**
	 * method to set the value
	 * 
	 * @param String
	 * @return void
	 */
	public void setApn(String apn) {
		this.apn = apn;
	} // End method setApn

	/**
	 * method to get the value
	 * 
	 * @return String
	 * 
	 */
	public String getApn() {
		return this.apn;
	} // End method getApn

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the addressStreetNumber
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetNumber() {
		return addressStreetNumber;
	}

	/**
	 * Sets the addressStreetNumber
	 * 
	 * @param addressStreetNumber
	 *            The addressStreetNumber to set
	 */
	public void setAddressStreetNumber(String addressStreetNumber) {
		this.addressStreetNumber = addressStreetNumber;
	}

	/**
	 * Gets the addressStreetModifier
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetModifier() {
		return addressStreetModifier;
	}

	/**
	 * Sets the addressStreetModifier
	 * 
	 * @param addressStreetModifier
	 *            The addressStreetModifier to set
	 */
	public void setAddressStreetModifier(String addressStreetModifier) {
		this.addressStreetModifier = addressStreetModifier;
	}

	/**
	 * Gets the addressStreetName
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetName() {
		return addressStreetName;
	}

	/**
	 * Sets the addressStreetName
	 * 
	 * @param addressStreetName
	 *            The addressStreetName to set
	 */
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}

	/**
	 * Gets the addressCity
	 * 
	 * @return Returns a String
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * Sets the addressCity
	 * 
	 * @param addressCity
	 *            The addressCity to set
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * Gets the addressState
	 * 
	 * @return Returns a String
	 */
	public String getAddressState() {
		return addressState;
	}

	/**
	 * Sets the addressState
	 * 
	 * @param addressState
	 *            The addressState to set
	 */
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	/**
	 * Gets the addressUnit
	 * 
	 * @return Returns a String
	 */
	public String getAddressUnit() {
		return addressUnit;
	}

	/**
	 * Sets the addressUnit
	 * 
	 * @param addressUnit
	 *            The addressUnit to set
	 */
	public void setAddressUnit(String addressUnit) {
		this.addressUnit = addressUnit;
	}

	/**
	 * Gets the addressZip
	 * 
	 * @return Returns a String
	 */
	public String getAddressZip() {
		return addressZip;
	}

	/**
	 * Sets the addressZip
	 * 
	 * @param addressZip
	 *            The addressZip to set
	 */
	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}

	/**
	 * Gets the addressZip4
	 * 
	 * @return Returns a String
	 */
	public String getAddressZip4() {
		return addressZip4;
	}

	/**
	 * Sets the addressZip4
	 * 
	 * @param addressZip4
	 *            The addressZip4 to set
	 */
	public void setAddressZip4(String addressZip4) {
		this.addressZip4 = addressZip4;
	}

	/**
	 * Gets the addressPrimary
	 * 
	 * @return Returns a String
	 */
	public String getAddressPrimary() {
		return addressPrimary;
	}

	/**
	 * Sets the addressPrimary
	 * 
	 * @param addressPrimary
	 *            The addressPrimary to set
	 */
	public void setAddressPrimary(String addressPrimary) {
		this.addressPrimary = addressPrimary;
	}

	/**
	 * Gets the addressActive
	 * 
	 * @return Returns a String
	 */
	public String getAddressActive() {
		return addressActive;
	}

	/**
	 * Sets the addressActive
	 * 
	 * @param addressActive
	 *            The addressActive to set
	 */
	public void setAddressActive(String addressActive) {
		this.addressActive = addressActive;
	}

	/**
	 * Gets the addressDescription
	 * 
	 * @return Returns a String
	 */
	public String getAddressDescription() {
		return addressDescription;
	}

	/**
	 * Sets the addressDescription
	 * 
	 * @param addressDescription
	 *            The addressDescription to set
	 */
	public void setAddressDescription(String addressDescription) {
		this.addressDescription = addressDescription;
	}

	/**
	 * Gets the apnOwnerId
	 * 
	 * @return Returns a String
	 */
	public String getApnOwnerId() {
		return apnOwnerId;
	}

	/**
	 * Sets the apnOwnerId
	 * 
	 * @param apnOwnerId
	 *            The apnOwnerId to set
	 */
	public void setApnOwnerId(String apnOwnerId) {
		this.apnOwnerId = apnOwnerId;
	}

	/**
	 * Gets the apnOwnerName
	 * 
	 * @return Returns a String
	 */
	public String getApnOwnerName() {
		return apnOwnerName;
	}

	/**
	 * Sets the apnOwnerName
	 * 
	 * @param apnOwnerName
	 *            The apnOwnerName to set
	 */
	public void setApnOwnerName(String apnOwnerName) {
		this.apnOwnerName = apnOwnerName;
	}

	/**
	 * Gets the apnStreetNumber
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetNumber() {
		return apnStreetNumber;
	}

	/**
	 * Sets the apnStreetNumber
	 * 
	 * @param apnStreetNumber
	 *            The apnStreetNumber to set
	 */
	public void setApnStreetNumber(String apnStreetNumber) {
		this.apnStreetNumber = apnStreetNumber;
	}

	/**
	 * Gets the apnStreetModifier
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetModifier() {
		return apnStreetModifier;
	}

	/**
	 * Sets the apnStreetModifier
	 * 
	 * @param apnStreetModifier
	 *            The apnStreetModifier to set
	 */
	public void setApnStreetModifier(String apnStreetModifier) {
		this.apnStreetModifier = apnStreetModifier;
	}

	/**
	 * Gets the apnStreetName
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetName() {
		return apnStreetName;
	}

	/**
	 * Sets the apnStreetName
	 * 
	 * @param apnStreetName
	 *            The apnStreetName to set
	 */
	public void setApnStreetName(String apnStreetName) {
		this.apnStreetName = apnStreetName;
	}

	/**
	 * Gets the apnCity
	 * 
	 * @return Returns a String
	 */
	public String getApnCity() {
		return apnCity;
	}

	/**
	 * Sets the apnCity
	 * 
	 * @param apnCity
	 *            The apnCity to set
	 */
	public void setApnCity(String apnCity) {
		this.apnCity = apnCity;
	}

	/**
	 * Gets the apnState
	 * 
	 * @return Returns a String
	 */
	public String getApnState() {
		return apnState;
	}

	/**
	 * Sets the apnState
	 * 
	 * @param apnState
	 *            The apnState to set
	 */
	public void setApnState(String apnState) {
		this.apnState = apnState;
	}

	/**
	 * Gets the apnZip
	 * 
	 * @return Returns a String
	 */
	public String getApnZip() {
		return apnZip;
	}

	/**
	 * Sets the apnZip
	 * 
	 * @param apnZip
	 *            The apnZip to set
	 */
	public void setApnZip(String apnZip) {
		this.apnZip = apnZip;
	}

	/**
	 * Gets the apnZip4
	 * 
	 * @return Returns a String
	 */
	public String getApnZip4() {
		return apnZip4;
	}

	/**
	 * Sets the apnZip4
	 * 
	 * @param apnZip4
	 *            The apnZip4 to set
	 */
	public void setApnZip4(String apnZip4) {
		this.apnZip4 = apnZip4;
	}

	/**
	 * Gets the apnEmail
	 * 
	 * @return Returns a String
	 */
	public String getApnEmail() {
		return apnEmail;
	}

	/**
	 * Sets the apnEmail
	 * 
	 * @param apnEmail
	 *            The apnEmail to set
	 */
	public void setApnEmail(String apnEmail) {
		this.apnEmail = apnEmail;
	}

	/**
	 * Gets the apnPhone
	 * 
	 * @return Returns a String
	 */
	public String getApnPhone() {
		return apnPhone;
	}

	/**
	 * Sets the apnPhone
	 * 
	 * @param apnPhone
	 *            The apnPhone to set
	 */
	public void setApnPhone(String apnPhone) {
		this.apnPhone = apnPhone;
	}

	/**
	 * Gets the apnFax
	 * 
	 * @return Returns a String
	 */
	public String getApnFax() {
		return apnFax;
	}

	/**
	 * Sets the apnFax
	 * 
	 * @param apnFax
	 *            The apnFax to set
	 */
	public void setApnFax(String apnFax) {
		this.apnFax = apnFax;
	}

	/**
	 * Gets the apnForeignAddress1
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress1() {
		return apnForeignAddress1;
	}

	/**
	 * Sets the apnForeignAddress1
	 * 
	 * @param apnForeignAddress1
	 *            The apnForeignAddress1 to set
	 */
	public void setApnForeignAddress1(String apnForeignAddress1) {
		this.apnForeignAddress1 = apnForeignAddress1;
	}

	/**
	 * Gets the apnForeignAddress2
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress2() {
		return apnForeignAddress2;
	}

	/**
	 * Sets the apnForeignAddress2
	 * 
	 * @param apnForeignAddress2
	 *            The apnForeignAddress2 to set
	 */
	public void setApnForeignAddress2(String apnForeignAddress2) {
		this.apnForeignAddress2 = apnForeignAddress2;
	}

	/**
	 * Gets the apnForeignAddress3
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress3() {
		return apnForeignAddress3;
	}

	/**
	 * Sets the apnForeignAddress3
	 * 
	 * @param apnForeignAddress3
	 *            The apnForeignAddress3 to set
	 */
	public void setApnForeignAddress3(String apnForeignAddress3) {
		this.apnForeignAddress3 = apnForeignAddress3;
	}

	/**
	 * Gets the apnForeignAddress4
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress4() {
		return apnForeignAddress4;
	}

	/**
	 * Sets the apnForeignAddress4
	 * 
	 * @param apnForeignAddress4
	 *            The apnForeignAddress4 to set
	 */
	public void setApnForeignAddress4(String apnForeignAddress4) {
		this.apnForeignAddress4 = apnForeignAddress4;
	}

	/**
	 * Gets the apnCountry
	 * 
	 * @return Returns a String
	 */
	public String getApnCountry() {
		return apnCountry;
	}

	/**
	 * Sets the apnCountry
	 * 
	 * @param apnCountry
	 *            The apnCountry to set
	 */
	public void setApnCountry(String apnCountry) {
		this.apnCountry = apnCountry;
	}

	/**
	 * Gets the alias
	 * 
	 * @return Returns a String
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the alias
	 * 
	 * @param alias
	 *            The alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the zone
	 * 
	 * @return Returns a String
	 */
	public String getZone() {
		return zone;
	}

	/**
	 * Sets the zone
	 * 
	 * @param zone
	 *            The zone to set
	 */
	public void setZone(String zone) {
		this.zone = zone;
	}

	/**
	 * Gets the use
	 * 
	 * @return Returns a String
	 */
	public String getUse() {
		return use;
	}

	/**
	 * Sets the use
	 * 
	 * @param use
	 *            The use to set
	 */
	public void setUse(String use) {
		this.use = use;
	}

	/**
	 * Gets the coordinateX
	 * 
	 * @return Returns a String
	 */
	public String getCoordinateX() {
		return coordinateX;
	}

	/**
	 * Sets the coordinateX
	 * 
	 * @param coordinateX
	 *            The coordinateX to set
	 */
	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	/**
	 * Gets the coordinateY
	 * 
	 * @return Returns a String
	 */
	public String getCoordinateY() {
		return coordinateY;
	}

	/**
	 * Sets the coordinateY
	 * 
	 * @param coordinateY
	 *            The coordinateY to set
	 */
	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	/**
	 * Gets the active
	 * 
	 * @return Returns a String
	 */
	public String getActive() {
		return active;
	}

	/**
	 * Sets the active
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

	/**
	 * Gets the apnForeignFlag
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignFlag() {
		return apnForeignFlag;
	}

	/**
	 * Sets the apnForeignFlag
	 * 
	 * @param apnForeignFlag
	 *            The apnForeignFlag to set
	 */
	public void setApnForeignFlag(String apnForeignFlag) {
		this.apnForeignFlag = apnForeignFlag;
	}

	/**
	 * Returns the addressRangeList.
	 * 
	 * @return List
	 */
	public List getAddressRangeList() {
		return addressRangeList;
	}

	/**
	 * Sets the addressRangeList.
	 * 
	 * @param addressRangeList
	 *            The addressRangeList to set
	 */
	public void setAddressRangeList(List addressRangeList) {
		this.addressRangeList = addressRangeList;
	}

	/**
	 * Returns the parkingZone.
	 * 
	 * @return String
	 */
	public String getParkingZone() {
		return parkingZone;
	}

	/**
	 * Returns the resZone.
	 * 
	 * @return String
	 */
	public String getResZone() {
		return resZone;
	}

	/**
	 * Sets the parkingZone.
	 * 
	 * @param parkingZone
	 *            The parkingZone to set
	 */
	public void setParkingZone(String parkingZone) {
		this.parkingZone = parkingZone;
	}

	/**
	 * Sets the resZone.
	 * 
	 * @param resZone
	 *            The resZone to set
	 */
	public void setResZone(String resZone) {
		this.resZone = resZone;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getHighFireArea() {
		return highFireArea;
	}

	public void setHighFireArea(String highFireArea) {
		this.highFireArea = highFireArea;
	}

} // End class

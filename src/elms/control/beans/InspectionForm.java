package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class InspectionForm extends ActionForm {

	protected String inspectionDate;
	protected String comments;
	protected String actionCode;
	protected String inspectionItem;
	protected String inspectorId;
	protected String inspectorName;
	protected String inspectionItemDesc;
	protected String inspectionId;
	protected String activityId;
	protected String activityType;
	protected String addOrEdit;
	protected String libOrSave;
    private int updatedBy;
    protected String[] inspectionItems;
    
    protected String timeUnit;
    protected String noOfViolation1;
    protected String noOfViolation2;
    private String progElement;
    protected String noOfMinorViolation;
    

	public InspectionForm() {
		inspectionDate = "";
		comments = "";
		actionCode = "";
		inspectionItem = "";
		inspectorId = "";
		inspectionItemDesc = "";
		inspectionId = "";
		activityId = "";
		addOrEdit = "";
		libOrSave = "";
	}

	/**
	 * Gets the inspectionDate
	 * 
	 * @return Returns a String
	 */
	public String getInspectionDate() {
		return inspectionDate;
	}

	/**
	 * Sets the inspectionDate
	 * 
	 * @param inspectionDate
	 *            The inspectionDate to set
	 */
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		inspectionDate = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;

	}

	/**
	 * Gets the actionCode
	 * 
	 * @return Returns a String
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * Sets the actionCode
	 * 
	 * @param actionCode
	 *            The actionCode to set
	 */
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	/**
	 * Gets the inspectionItem
	 * 
	 * @return Returns a String
	 */
	public String getInspectionItem() {
		return inspectionItem;
	}

	/**
	 * Sets the inspectionItem
	 * 
	 * @param inspectionItem
	 *            The inspectionItem to set
	 */
	public void setInspectionItem(String inspectionItem) {
		this.inspectionItem = inspectionItem;
	}

	/**
	 * Gets the inspectorId
	 * 
	 * @return Returns a String or null
	 */
	public String getInspectorId() {
		try {
			int insp = Integer.parseInt(inspectorId);
			if (insp > 0)
				return inspectorId;
			else
				return "0";
		} catch (NumberFormatException e) {
			return "0";
		}
	}

	/**
	 * Sets the inspectorId
	 * 
	 * @param inspectorId
	 *            The inspectorId to set
	 */
	public void setInspectorId(String inspectorId) {
		this.inspectorId = inspectorId;
	}

	/**
	 * Gets the inspectionItemDesc
	 * 
	 * @return Returns a String
	 */
	public String getInspectionItemDesc() {
		return inspectionItemDesc;
	}

	/**
	 * Sets the inspectionItemDesc
	 * 
	 * @param inspectionItemDesc
	 *            The inspectionItemDesc to set
	 */
	public void setInspectionItemDesc(String inspectionItemDesc) {
		if (inspectionItemDesc == null)
			inspectionItemDesc = "";
		this.inspectionItemDesc = inspectionItemDesc;
	}

	/**
	 * Gets the inspectionId
	 * 
	 * @return Returns a String
	 */
	public String getInspectionId() {
		return inspectionId;
	}

	/**
	 * Sets the inspectionId
	 * 
	 * @param inspectionId
	 *            The inspectionId to set
	 */
	public void setInspectionId(String inspectionId) {
		this.inspectionId = inspectionId;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the inspectorName
	 * 
	 * @return Returns a String
	 */
	public String getInspectorName() {
		return inspectorName;
	}

	/**
	 * Sets the inspectorName
	 * 
	 * @param inspectorName
	 *            The inspectorName to set
	 */
	public void setInspectorName(String inspectorName) {
		this.inspectorName = inspectorName;
	}

	/**
	 * Gets the addOrEdit
	 * 
	 * @return Returns a String
	 */
	public String getAddOrEdit() {
		return addOrEdit;
	}

	/**
	 * Sets the addOrEdit
	 * 
	 * @param addOrEdit
	 *            The addOrEdit to set
	 */
	public void setAddOrEdit(String addOrEdit) {
		this.addOrEdit = addOrEdit;
	}

	/**
	 * Gets the libOrSave
	 * 
	 * @return Returns a String
	 */
	public String getLibOrSave() {
		return libOrSave;
	}

	/**
	 * Sets the libOrSave
	 * 
	 * @param libOrSave
	 *            The libOrSave to set
	 */
	public void setLibOrSave(String libOrSave) {
		this.libOrSave = libOrSave;
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a String
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}
	
	public int getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String[] getInspectionItems() {
		return inspectionItems;
	}

	public void setInspectionItems(String[] inspectionItems) {
		this.inspectionItems = inspectionItems;
	}
	/**
	 * @return the timeUnit
	 */
	public String getTimeUnit() {
		return timeUnit;
	}

	/**
	 * @param timeUnit the timeUnit to set
	 */
	public void setTimeUnit(String timeUnit) {
		this.timeUnit = timeUnit;
	}

	/**
	 * @return the noOfViolation1
	 */
	public String getNoOfViolation1() {
		return noOfViolation1;
	}

	/**
	 * @param noOfViolation1 the noOfViolation1 to set
	 */
	public void setNoOfViolation1(String noOfViolation1) {
		this.noOfViolation1 = noOfViolation1;
	}

	/**
	 * @return the noOfViolation2
	 */
	public String getNoOfViolation2() {
		return noOfViolation2;
	}

	/**
	 * @param noOfViolation2 the noOfViolation2 to set
	 */
	public void setNoOfViolation2(String noOfViolation2) {
		this.noOfViolation2 = noOfViolation2;
	}

	/**
	 * @return the progElement
	 */
	public String getProgElement() {
		return progElement;
	}

	/**
	 * @param progElement the progElement to set
	 */
	public void setProgElement(String progElement) {
		this.progElement = progElement;
	}

	/**
	 * @return the noOfMinorViolation
	 */
	public String getNoOfMinorViolation() {
		return noOfMinorViolation;
	}

	/**
	 * @param noOfMinorViolation the noOfMinorViolation to set
	 */
	public void setNoOfMinorViolation(String noOfMinorViolation) {
		this.noOfMinorViolation = noOfMinorViolation;
	}
	
}
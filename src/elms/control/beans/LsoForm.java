package elms.control.beans;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.lso.LsoAddress;
import elms.app.lso.Street;
import elms.util.StringUtils;

public class LsoForm extends ActionForm {

	static Logger logger = Logger.getLogger(LsoForm.class.getName());

	protected String origin;
	protected String lsoType;
	protected String lsoTypeFull;

	protected String addressId;
	protected String addressStreetNumber;
	protected String addressStreetModifier;
	protected String addressStreetName;
	protected String addressState;
	protected String addressUnit;
	protected String addressZip;
	protected String addressZip4;
	protected boolean addressPrimary;
	protected boolean addressActive;
	protected String addressDescription;
	protected String addressCity;

	public void setLsoAddress(LsoAddress lsoAddress) {

		logger.debug("setting form elements for lsoform");
		try {
			if (lsoAddress != null) {
				this.setAddressStreetNumber(StringUtils.i2s(lsoAddress.getStreetNbr()));
				logger.debug("set the address street number    " + lsoAddress.getStreetNbr());
				this.setAddressStreetModifier(lsoAddress.getStreetModifier());
				logger.debug("set the  street modifier " + lsoAddress.getStreetModifier());
				logger.debug("setting the address id " + lsoAddress.getAddressId());
				this.setAddressId(StringUtils.i2s(lsoAddress.getAddressId()));
				Street street = lsoAddress.getStreet();
				if (street != null) {
					this.setAddressStreetName(StringUtils.i2s(street.getStreetId()));
					logger.debug("set the  street name id   " + StringUtils.i2s(street.getStreetId()));
				}
				this.setAddressUnit(lsoAddress.getUnit());
				logger.debug("set the unit   " + lsoAddress.getUnit());
				this.setAddressCity(lsoAddress.getCity());
				logger.debug("set the city   " + lsoAddress.getCity());
				this.setAddressState(lsoAddress.getState());
				logger.debug("set the  state   " + lsoAddress.getState());
				this.setAddressZip(lsoAddress.getZip());
				logger.debug("set the zip    " + lsoAddress.getZip());
				this.setAddressZip4(lsoAddress.getZip4());
				logger.debug("set the  zip4   " + lsoAddress.getZip4());
				this.setAddressDescription(lsoAddress.getDescription());
				logger.debug("set the desc    " + lsoAddress.getDescription());
				this.setAddressPrimary(StringUtils.s2b(lsoAddress.getPrimary()));
				logger.debug("set the primary    " + StringUtils.s2b(lsoAddress.getPrimary()));
				this.setAddressActive(StringUtils.s2b(lsoAddress.getActive()));
				logger.debug("set the  active   " + StringUtils.s2b(lsoAddress.getActive()));
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Gets the addressId
	 * 
	 * @return Returns a String
	 */
	public String getAddressId() {
		return addressId;
	}

	/**
	 * Sets the addressId
	 * 
	 * @param addressId
	 *            The addressId to set
	 */
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	/**
	 * Gets the addressStreetNumber
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetNumber() {
		return addressStreetNumber;
	}

	/**
	 * Sets the addressStreetNumber
	 * 
	 * @param addressStreetNumber
	 *            The addressStreetNumber to set
	 */
	public void setAddressStreetNumber(String addressStreetNumber) {
		this.addressStreetNumber = addressStreetNumber;
	}

	/**
	 * Gets the addressStreetModifier
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetModifier() {
		return addressStreetModifier;
	}

	/**
	 * Sets the addressStreetModifier
	 * 
	 * @param addressStreetModifier
	 *            The addressStreetModifier to set
	 */
	public void setAddressStreetModifier(String addressStreetModifier) {
		this.addressStreetModifier = addressStreetModifier;
	}

	/**
	 * Gets the addressStreetName
	 * 
	 * @return Returns a String
	 */
	public String getAddressStreetName() {
		return addressStreetName;
	}

	/**
	 * Sets the addressStreetName
	 * 
	 * @param addressStreetName
	 *            The addressStreetName to set
	 */
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}

	/**
	 * Gets the addressState
	 * 
	 * @return Returns a String
	 */
	public String getAddressState() {
		return addressState;
	}

	/**
	 * Sets the addressState
	 * 
	 * @param addressState
	 *            The addressState to set
	 */
	public void setAddressState(String addressState) {
		this.addressState = addressState;
	}

	/**
	 * Gets the addressUnit
	 * 
	 * @return Returns a String
	 */
	public String getAddressUnit() {
		return addressUnit;
	}

	/**
	 * Sets the addressUnit
	 * 
	 * @param addressUnit
	 *            The addressUnit to set
	 */
	public void setAddressUnit(String addressUnit) {
		this.addressUnit = addressUnit;
	}

	/**
	 * Gets the addressZip
	 * 
	 * @return Returns a String
	 */
	public String getAddressZip() {
		return addressZip;
	}

	/**
	 * Sets the addressZip
	 * 
	 * @param addressZip
	 *            The addressZip to set
	 */
	public void setAddressZip(String addressZip) {
		this.addressZip = addressZip;
	}

	/**
	 * Gets the addressZip4
	 * 
	 * @return Returns a String
	 */
	public String getAddressZip4() {
		return addressZip4;
	}

	/**
	 * Sets the addressZip4
	 * 
	 * @param addressZip4
	 *            The addressZip4 to set
	 */
	public void setAddressZip4(String addressZip4) {
		this.addressZip4 = addressZip4;
	}

	/**
	 * Gets the addressPrimary
	 * 
	 * @return Returns a boolean
	 */
	public boolean getAddressPrimary() {
		return addressPrimary;
	}

	/**
	 * Sets the addressPrimary
	 * 
	 * @param addressPrimary
	 *            The addressPrimary to set
	 */
	public void setAddressPrimary(boolean addressPrimary) {
		this.addressPrimary = addressPrimary;
	}

	/**
	 * Gets the addressActive
	 * 
	 * @return Returns a boolean
	 */
	public boolean getAddressActive() {
		return addressActive;
	}

	/**
	 * Sets the addressActive
	 * 
	 * @param addressActive
	 *            The addressActive to set
	 */
	public void setAddressActive(boolean addressActive) {
		this.addressActive = addressActive;
	}

	/**
	 * Gets the addressDescription
	 * 
	 * @return Returns a String
	 */
	public String getAddressDescription() {
		return addressDescription;
	}

	/**
	 * Sets the addressDescription
	 * 
	 * @param addressDescription
	 *            The addressDescription to set
	 */
	public void setAddressDescription(String addressDescription) {
		this.addressDescription = addressDescription;
	}

	/**
	 * Gets the addressCity
	 * 
	 * @return Returns a String
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * Sets the addressCity
	 * 
	 * @param addressCity
	 *            The addressCity to set
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * Gets the origin
	 * 
	 * @return Returns a String
	 */
	public String getOrigin() {
		return origin;
	}

	/**
	 * Sets the origin
	 * 
	 * @param origin
	 *            The origin to set
	 */
	public void setOrigin(String origin) {
		this.origin = origin;
	}

	/**
	 * Gets the lsoType
	 * 
	 * @return Returns a String
	 */
	public String getLsoType() {
		return lsoType;
	}

	/**
	 * Sets the lsoType
	 * 
	 * @param lsoType
	 *            The lsoType to set
	 */
	public void setLsoType(String lsoType) {
		this.lsoType = lsoType;
	}

	/**
	 * Gets the lsoTypeFull
	 * 
	 * @return Returns a String
	 */
	public String getLsoTypeFull() {
		return lsoTypeFull;
	}

	/**
	 * Sets the lsoTypeFull
	 * 
	 * @param lsoTypeFull
	 *            The lsoTypeFull to set
	 */
	public void setLsoTypeFull(String lsoTypeFull) {
		this.lsoTypeFull = lsoTypeFull;
	}

}
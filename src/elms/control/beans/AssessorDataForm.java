package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class AssessorDataForm extends ActionForm {
	// Declare the variable and object declarations used by the class here
	protected String apn;
	protected String streetName;
	protected String streetId;
	protected String streetNumber;
	protected String streetFraction;
	protected String unitNbr;
	protected String useCode;
	protected String zoneCode;
	protected String numberOfUnits;
	protected String hazardAbatementCode;
	protected String dateOfLastSaleGT;
	protected String dateOfLastSaleLT;
	protected String improvementValueGT;
	protected String improvementValueLT;
	protected String totalAssessmentGT;
	protected String totalAssessmentLT;
	protected String ownerName;

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		// reset the declaration values...or rather set it to something default.
		this.apn = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();

		return errors;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * @return Returns the dateOfLastSaleGT.
	 */
	public String getDateOfLastSaleGT() {
		return dateOfLastSaleGT;
	}

	/**
	 * @param dateOfLastSaleGT
	 *            The dateOfLastSaleGT to set.
	 */
	public void setDateOfLastSaleGT(String dateOfLastSaleGT) {
		this.dateOfLastSaleGT = dateOfLastSaleGT;
	}

	/**
	 * @return Returns the dateOfLastSaleLT.
	 */
	public String getDateOfLastSaleLT() {
		return dateOfLastSaleLT;
	}

	/**
	 * @param dateOfLastSaleLT
	 *            The dateOfLastSaleLT to set.
	 */
	public void setDateOfLastSaleLT(String dateOfLastSaleLT) {
		this.dateOfLastSaleLT = dateOfLastSaleLT;
	}

	/**
	 * @return Returns the hazardAbatementCode.
	 */
	public String getHazardAbatementCode() {
		return hazardAbatementCode;
	}

	/**
	 * @param hazardAbatementCode
	 *            The hazardAbatementCode to set.
	 */
	public void setHazardAbatementCode(String hazardAbatementCode) {
		this.hazardAbatementCode = hazardAbatementCode;
	}

	/**
	 * @return Returns the improvementValueGT.
	 */
	public String getImprovementValueGT() {
		return improvementValueGT;
	}

	/**
	 * @param improvementValueGT
	 *            The improvementValueGT to set.
	 */
	public void setImprovementValueGT(String improvementValueGT) {
		this.improvementValueGT = improvementValueGT;
	}

	/**
	 * @return Returns the improvementValueLT.
	 */
	public String getImprovementValueLT() {
		return improvementValueLT;
	}

	/**
	 * @param improvementValueLT
	 *            The improvementValueLT to set.
	 */
	public void setImprovementValueLT(String improvementValueLT) {
		this.improvementValueLT = improvementValueLT;
	}

	/**
	 * @return Returns the numberOfUnits.
	 */
	public String getNumberOfUnits() {
		return numberOfUnits;
	}

	/**
	 * @param numberOfUnits
	 *            The numberOfUnits to set.
	 */
	public void setNumberOfUnits(String numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	/**
	 * @return Returns the totalAssessmentGT.
	 */
	public String getTotalAssessmentGT() {
		return totalAssessmentGT;
	}

	/**
	 * @param totalAssessmentGT
	 *            The totalAssessmentGT to set.
	 */
	public void setTotalAssessmentGT(String totalAssessmentGT) {
		this.totalAssessmentGT = totalAssessmentGT;
	}

	/**
	 * @return Returns the totalAssessmentLT.
	 */
	public String getTotalAssessmentLT() {
		return totalAssessmentLT;
	}

	/**
	 * @param totalAssessmentLT
	 *            The totalAssessmentLT to set.
	 */
	public void setTotalAssessmentLT(String totalAssessmentLT) {
		this.totalAssessmentLT = totalAssessmentLT;
	}

	/**
	 * @return Returns the useCode.
	 */
	public String getUseCode() {
		return useCode;
	}

	/**
	 * @param useCode
	 *            The useCode to set.
	 */
	public void setUseCode(String useCode) {
		this.useCode = useCode;
	}

	/**
	 * @return Returns the zoneCode.
	 */
	public String getZoneCode() {
		return zoneCode;
	}

	/**
	 * @param zoneCode
	 *            The zoneCode to set.
	 */
	public void setZoneCode(String zoneCode) {
		this.zoneCode = zoneCode;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * Returns the streetFraction.
	 * 
	 * @return String
	 */
	public String getStreetFraction() {
		return streetFraction;
	}

	/**
	 * Returns the streetName.
	 * 
	 * @return String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Returns the streetNumber.
	 * 
	 * @return String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Returns the unitNbr.
	 * 
	 * @return String
	 */
	public String getUnitNbr() {
		return unitNbr;
	}

	/**
	 * Sets the streetFraction.
	 * 
	 * @param streetFraction
	 *            The streetFraction to set
	 */
	public void setStreetFraction(String streetFraction) {
		this.streetFraction = streetFraction;
	}

	/**
	 * @return Returns the ownerName.
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * @param ownerName
	 *            The ownerName to set.
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Sets the streetName.
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Sets the streetNumber.
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Sets the unitNbr.
	 * 
	 * @param unitNbr
	 *            The unitNbr to set
	 */
	public void setUnitNbr(String unitNbr) {
		this.unitNbr = unitNbr;
	}

	/**
	 * Returns the streetId.
	 * 
	 * @return String
	 */
	public String getStreetId() {
		return streetId;
	}

	/**
	 * Sets the streetId.
	 * 
	 * @param streetId
	 *            The streetId to set
	 */
	public void setStreetId(String streetId) {
		this.streetId = streetId;
	}
}

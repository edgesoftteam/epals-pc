package elms.control.beans;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;

import elms.app.project.Certificate;

public class CertificateForm extends ActionForm {

	static Logger logger = Logger.getLogger(CertificateForm.class.getName());

	/**
	 * Member variable declaration
	 */

	protected Certificate certificate;

	/**
	 * @roseuid 3CAA2F42019B
	 */
	public CertificateForm() {
		certificate = new Certificate();

	}

	/**
	 * Returns the certificate.
	 * 
	 * @return Certificate
	 */
	public Certificate getCertificate() {
		return certificate;
	}

	/**
	 * Sets the certificate.
	 * 
	 * @param certificate
	 *            The certificate to set
	 */
	public void setCertificate(Certificate certificate) {
		this.certificate = certificate;
	}

}
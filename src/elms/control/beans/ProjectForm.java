package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author Shekhar Jain
 */

public class ProjectForm extends ActionForm {

	protected String lsoId;
	protected String projectId;
	protected String startDate;
	protected String completionDate;
	protected String projectName;
	protected String expirationDate;
	protected String use;
	protected String description;
	protected String valuation;
	protected String microfilm;
	protected String status;
	protected String ownerName;
	protected String streetNumber;
	protected String streetName;
	protected String unit;
	protected String finalStatusAllowed;
	protected String openActivitiesCount;
	protected String label;

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Gets the startDate
	 * 
	 * @return Returns a String
	 */
	public String getStartDate() {
		return startDate;
	}

	/**
	 * Sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the completionDate
	 * 
	 * @return Returns a String
	 */
	public String getCompletionDate() {
		return completionDate;
	}

	/**
	 * Sets the completionDate
	 * 
	 * @param completionDate
	 *            The completionDate to set
	 */
	public void setCompletionDate(String completionDate) {
		this.completionDate = completionDate;
	}

	/**
	 * Gets the projectName
	 * 
	 * @return Returns a String
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * Sets the projectName
	 * 
	 * @param projectName
	 *            The projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * Gets the expirationDate
	 * 
	 * @return Returns a String
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the project Id
	 * 
	 * @return Returns a String
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the project id
	 * 
	 * @param status
	 *            The project id to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		startDate = null;
		completionDate = null;
		projectName = null;
		expirationDate = null;
		use = null;
		description = null;
		valuation = null;
		status = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the ownerName
	 * 
	 * @return Returns a String
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * Sets the ownerName
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Gets the use
	 * 
	 * @return Returns a String
	 */
	public String getUse() {
		return use;
	}

	/**
	 * Sets the use
	 * 
	 * @param use
	 *            The use to set
	 */
	public void setUse(String use) {
		this.use = use;
	}

	/**
	 * Gets the valuation
	 * 
	 * @return Returns a String
	 */
	public String getValuation() {
		return valuation;
	}

	/**
	 * Sets the valuation
	 * 
	 * @param valuation
	 *            The valuation to set
	 */
	public void setValuation(String valuation) {
		this.valuation = valuation;
	}

	/**
	 * Gets the streetNumber
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the streetNumber
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the finalStatusAllowed
	 * 
	 * @return Returns a String
	 */
	public String getFinalStatusAllowed() {
		return finalStatusAllowed;
	}

	/**
	 * Sets the finalStatusAllowed
	 * 
	 * @param finalStatusAllowed
	 *            The finalStatusAllowed to set
	 */
	public void setFinalStatusAllowed(String finalStatusAllowed) {
		this.finalStatusAllowed = finalStatusAllowed;
	}

	/**
	 * Gets the openActivitiesCount
	 * 
	 * @return Returns a String
	 */
	public String getOpenActivitiesCount() {
		return openActivitiesCount;
	}

	/**
	 * Sets the openActivitiesCount
	 * 
	 * @param openActivitiesCount
	 *            The openActivitiesCount to set
	 */
	public void setOpenActivitiesCount(String openActivitiesCount) {
		this.openActivitiesCount = openActivitiesCount;
	}

	/**
	 * Returns the microfilm.
	 * 
	 * @return String
	 */
	public String getMicrofilm() {
		return microfilm;
	}

	/**
	 * Sets the microfilm.
	 * 
	 * @param microfilm
	 *            The microfilm to set
	 */
	public void setMicrofilm(String microfilm) {
		this.microfilm = microfilm;
	}

	/**
	 * @return Returns the unit.
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * @param unit
	 *            The unit to set.
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
}
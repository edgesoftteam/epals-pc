package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class LsoSearchForm extends ActionForm {

	protected String streetNumber;
	protected String streetName;
	protected String radiobutton = "radio_active";

	public void reset(ActionMapping mapping, HttpServletRequest request) {


	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		return errors;
	}


	/**
	 * Gets the streetNumber
	 * 
	 * @return Returns a String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the streetNumber
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Gets the streetName
	 * 
	 * @return Returns a String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Sets the streetName
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Gets the radiobutton
	 * 
	 * @return Returns a String
	 */
	public String getRadiobutton() {
		return radiobutton;
	}

	/**
	 * Sets the radiobutton
	 * 
	 * @param radiobutton
	 *            The radiobutton to set
	 */
	public void setRadiobutton(String radiobutton) {
		this.radiobutton = radiobutton;
	}

}

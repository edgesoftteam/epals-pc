package elms.control.beans;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.app.bt.BusinessTaxActivity;
import elms.app.common.MultiAddress;
import elms.util.StringUtils;

/**
 * @author Gayathri - Business Tax Activity Form
 */
public class BusinessTaxActivityForm extends ActionForm {
	/*
	 * The Logger
	 */

	static Logger logger = Logger.getLogger(BusinessTaxActivityForm.class.getName());

	protected BusinessTaxActivity businessTaxActivity;

	protected String activityId;
	protected String lsoId;
	
	protected boolean businessLocation;
	protected String applicationType;
	protected String businessAddressStreetNumber;
	protected String businessAddressStreetFraction;
	protected String businessAddressStreetName;
	protected String businessAddressUnitNumber;
	protected String businessAddressCity;
	protected String businessAddressState;
	protected String businessAddressZip;
	protected String businessAddressZip4;
	protected String outOfTownStreetNumber;
	protected String outOfTownStreetName;
	protected String outOfTownUnitNumber;
	protected String outOfTownCity;
	protected String outOfTownState;
	protected String outOfTownZip;
	protected String outOfTownZip4;
	protected String activityStatus;
	protected String businessName;
	protected String businessAccountNumber;
	protected String corporateName;
	protected String attn;
	protected String businessPhone;
	protected String businessExtension;
	protected String businessFax;
	protected String activityType;
	protected String activityTypeDescription;
	protected String activitySubType;
	protected String sicCode;
	protected String muncipalCode;
	protected String descOfBusiness;
	protected String classCode;
	protected boolean homeOccupation;
	protected boolean decalCode;
	protected boolean otherBusinessOccupancy;
	protected String applicationDate;
	protected String issueDate;
	protected String startingDate;
	protected String outOfBusinessDate;
	protected String ownershipType;
	protected String federalIdNumber;
	protected String emailAddress;
	protected String socialSecurityNumber;
	protected String mailStreetNumber;
	protected String mailStreetName;
	protected String mailUnitNumber;
	protected String mailCity;
	protected String mailState;
	protected String mailZip;
	protected String mailZip4;
	protected String prevStreetNumber;
	protected String prevStreetName;
	protected String prevUnitNumber;
	protected String prevCity;
	protected String prevState;
	protected String prevZip;
	protected String prevZip4;
	protected String name1;
	protected String title1;
	protected String owner1StreetNumber;
	protected String owner1StreetName;
	protected String owner1UnitNumber;
	protected String owner1City;
	protected String owner1State;
	protected String owner1Zip;
	protected String owner1Zip4;
	protected String name2;
	protected String title2;
	protected String owner2StreetNumber;
	protected String owner2StreetName;
	protected String owner2UnitNumber;
	protected String owner2City;
	protected String owner2State;
	protected String owner2Zip;
	protected String owner2Zip4;
	protected String squareFootage;
	protected String quantity;
	protected String quantityNum;
	protected String driverLicense;
	protected String subProjectId;
	protected String displayContent;
	protected int createdBy;
	protected int updatedBy;
	protected String creationDate;
	protected String activityNumber;
	protected String mailAttn;
	protected String preAttn;
	protected String owner1Attn;
	protected String owner2Attn;
	protected MultiAddress[] multiAddress;

	/**
	 * @return Returns the activityNumber.
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * @param activityNumber
	 *            The activityNumber to set.
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * @return Returns the activityStatus.
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            The activityStatus to set.
	 */
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return Returns the activitySubType.
	 */
	public String getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param activitySubType
	 *            The activitySubType to set.
	 */
	public void setActivitySubType(String activitySubType) {
		this.activitySubType = activitySubType;
	}

	/**
	 * @return Returns the activityType.
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @param activityType
	 *            The activityType to set.
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * @return Returns the activityTypeDescription.
	 */
	public String getActivityTypeDescription() {
		return activityTypeDescription;
	}

	/**
	 * @param activityTypeDescription
	 *            The activityTypeDescription to set.
	 */
	public void setActivityTypeDescription(String activityTypeDescription) {
		this.activityTypeDescription = activityTypeDescription;
	}

	/**
	 * @return Returns the applicationDate.
	 */
	public String getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate
	 *            The applicationDate to set.
	 */
	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return Returns the applicationType.
	 */
	public String getApplicationType() {
		return applicationType;
	}

	/**
	 * @param applicationType
	 *            The applicationType to set.
	 */
	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	/**
	 * @return Returns the businessAddressCity.
	 */
	public String getBusinessAddressCity() {
		return businessAddressCity;
	}

	/**
	 * @param businessAddressCity
	 *            The businessAddressCity to set.
	 */
	public void setBusinessAddressCity(String businessAddressCity) {
		this.businessAddressCity = businessAddressCity;
	}

	/**
	 * @return Returns the businessAddressState.
	 */
	public String getBusinessAddressState() {
		return businessAddressState;
	}

	/**
	 * @param businessAddressState
	 *            The businessAddressState to set.
	 */
	public void setBusinessAddressState(String businessAddressState) {
		this.businessAddressState = businessAddressState;
	}

	/**
	 * @return Returns the businessAddressStreetFraction.
	 */
	public String getBusinessAddressStreetFraction() {
		return businessAddressStreetFraction;
	}

	/**
	 * @param businessAddressStreetFraction
	 *            The businessAddressStreetFraction to set.
	 */
	public void setBusinessAddressStreetFraction(String businessAddressStreetFraction) {
		this.businessAddressStreetFraction = businessAddressStreetFraction;
	}

	/**
	 * @return Returns the businessAddressStreetName.
	 */
	public String getBusinessAddressStreetName() {
		return businessAddressStreetName;
	}

	/**
	 * @param businessAddressStreetName
	 *            The businessAddressStreetName to set.
	 */
	public void setBusinessAddressStreetName(String businessAddressStreetName) {
		this.businessAddressStreetName = businessAddressStreetName;
	}

	/**
	 * @return Returns the businessAddressStreetNumber.
	 */
	public String getBusinessAddressStreetNumber() {
		return businessAddressStreetNumber;
	}

	/**
	 * @param businessAddressStreetNumber
	 *            The businessAddressStreetNumber to set.
	 */
	public void setBusinessAddressStreetNumber(String businessAddressStreetNumber) {
		this.businessAddressStreetNumber = businessAddressStreetNumber;
	}

	/**
	 * @return Returns the businessAddressUnitNumber.
	 */
	public String getBusinessAddressUnitNumber() {
		return businessAddressUnitNumber;
	}

	/**
	 * @param businessAddressUnitNumber
	 *            The businessAddressUnitNumber to set.
	 */
	public void setBusinessAddressUnitNumber(String businessAddressUnitNumber) {
		this.businessAddressUnitNumber = businessAddressUnitNumber;
	}

	/**
	 * @return Returns the businessAddressZip.
	 */
	public String getBusinessAddressZip() {
		return businessAddressZip;
	}

	/**
	 * @param businessAddressZip
	 *            The businessAddressZip to set.
	 */
	public void setBusinessAddressZip(String businessAddressZip) {
		this.businessAddressZip = businessAddressZip;
	}

	/**
	 * @return Returns the businessAddressZip4.
	 */
	public String getBusinessAddressZip4() {
		return businessAddressZip4;
	}

	/**
	 * @param businessAddressZip4
	 *            The businessAddressZip4 to set.
	 */
	public void setBusinessAddressZip4(String businessAddressZip4) {
		this.businessAddressZip4 = businessAddressZip4;
	}

	/**
	 * @return Returns the businessExtension.
	 */
	public String getBusinessExtension() {
		return businessExtension;
	}

	/**
	 * @param businessExtension
	 *            The businessExtension to set.
	 */
	public void setBusinessExtension(String businessExtension) {
		this.businessExtension = businessExtension;
	}

	/**
	 * @return Returns the businessFax.
	 */
	public String getBusinessFax() {
		return businessFax;
	}

	/**
	 * @param businessFax
	 *            The businessFax to set.
	 */
	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax;
	}

	/**
	 * @return Returns the businessLocation.
	 */
	public boolean isBusinessLocation() {
		return businessLocation;
	}

	/**
	 * @param businessLocation
	 *            The businessLocation to set.
	 */
	public void setBusinessLocation(boolean businessLocation) {
		this.businessLocation = businessLocation;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the businessAccountNumber.
	 */
	public String getBusinessAccountNumber() {
		return businessAccountNumber;
	}

	/**
	 * @param businessAccountNumber
	 *            The businessAccountNumber to set.
	 */
	public void setBusinessAccountNumber(String businessAccountNumber) {
		this.businessAccountNumber = businessAccountNumber;
	}

	/**
	 * @return Returns the businessPhone.
	 */
	public String getBusinessPhone() {
		return businessPhone;
	}

	/**
	 * @param businessPhone
	 *            The businessPhone to set.
	 */
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	/**
	 * @return Returns the classCode.
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * @param classCode
	 *            The classCode to set.
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * @return Returns the corporateName.
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * @param corporateName
	 *            The corporateName to set.
	 */
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	/**
	 * @return Returns the attn.
	 */
	public String getAttn() {
		return attn;
	}

	/**
	 * @param attn
	 *            The attn to set.
	 */
	public void setAttn(String attn) {
		this.attn = attn;
	}

	/**
	 * @return Returns the createdBy.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            The createdBy to set.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the creationDate.
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            The creationDate to set.
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return Returns the decalCode.
	 */
	public boolean isDecalCode() {
		return decalCode;
	}

	/**
	 * @param decalCode
	 *            The decalCode to set.
	 */
	public void setDecalCode(boolean decalCode) {
		this.decalCode = decalCode;
	}

	/**
	 * @return Returns the descOfBusiness.
	 */
	public String getDescOfBusiness() {
		return descOfBusiness;
	}

	/**
	 * @param descOfBusiness
	 *            The descOfBusiness to set.
	 */
	public void setDescOfBusiness(String descOfBusiness) {
		this.descOfBusiness = descOfBusiness;
	}

	/**
	 * @return Returns the displayContent.
	 */
	public String getDisplayContent() {
		return displayContent;
	}

	/**
	 * @param displayContent
	 *            The displayContent to set.
	 */
	public void setDisplayContent(String displayContent) {
		this.displayContent = displayContent;
	}

	/**
	 * @return Returns the driverLicense.
	 */
	public String getDriverLicense() {
		return driverLicense;
	}

	/**
	 * @param driverLicense
	 *            The driverLicense to set.
	 */
	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}

	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the federalIdNumber.
	 */
	public String getFederalIdNumber() {
		return federalIdNumber;
	}

	/**
	 * @param federalIdNumber
	 *            The federalIdNumber to set.
	 */
	public void setFederalIdNumber(String federalIdNumber) {
		this.federalIdNumber = federalIdNumber;
	}

	/**
	 * @return Returns the homeOccupation.
	 */
	public boolean isHomeOccupation() {
		return homeOccupation;
	}

	/**
	 * @param homeOccupation
	 *            The homeOccupation to set.
	 */
	public void setHomeOccupation(boolean homeOccupation) {
		this.homeOccupation = homeOccupation;
	}

	/**
	 * @return Returns the issueDate.
	 */
	public String getIssueDate() {
		return issueDate;
	}

	/**
	 * @param issueDate
	 *            The issueDate to set.
	 */
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * @return Returns the mailCity.
	 */
	public String getMailCity() {
		return mailCity;
	}

	/**
	 * @param mailCity
	 *            The mailCity to set.
	 */
	public void setMailCity(String mailCity) {
		this.mailCity = mailCity;
	}

	/**
	 * @return Returns the mailState.
	 */
	public String getMailState() {
		return mailState;
	}

	/**
	 * @param mailState
	 *            The mailState to set.
	 */
	public void setMailState(String mailState) {
		this.mailState = mailState;
	}

	/**
	 * @return Returns the mailStreetName.
	 */
	public String getMailStreetName() {
		return mailStreetName;
	}

	/**
	 * @param mailStreetName
	 *            The mailStreetName to set.
	 */
	public void setMailStreetName(String mailStreetName) {
		this.mailStreetName = mailStreetName;
	}

	/**
	 * @return Returns the mailStreetNumber.
	 */
	public String getMailStreetNumber() {
		return mailStreetNumber;
	}

	/**
	 * @param mailStreetNumber
	 *            The mailStreetNumber to set.
	 */
	public void setMailStreetNumber(String mailStreetNumber) {
		this.mailStreetNumber = mailStreetNumber;
	}

	/**
	 * @return Returns the mailUnitNumber.
	 */
	public String getMailUnitNumber() {
		return mailUnitNumber;
	}

	/**
	 * @param mailUnitNumber
	 *            The mailUnitNumber to set.
	 */
	public void setMailUnitNumber(String mailUnitNumber) {
		this.mailUnitNumber = mailUnitNumber;
	}

	/**
	 * @return Returns the mailZip.
	 */
	public String getMailZip() {
		return mailZip;
	}

	/**
	 * @param mailZip
	 *            The mailZip to set.
	 */
	public void setMailZip(String mailZip) {
		this.mailZip = mailZip;
	}

	/**
	 * @return Returns the mailZip4.
	 */
	public String getMailZip4() {
		return mailZip4;
	}

	/**
	 * @param mailZip4
	 *            The mailZip4 to set.
	 */
	public void setMailZip4(String mailZip4) {
		this.mailZip4 = mailZip4;
	}

	/**
	 * @return Returns the muncipalCode.
	 */
	public String getMuncipalCode() {
		return muncipalCode;
	}

	/**
	 * @param muncipalCode
	 *            The muncipalCode to set.
	 */
	public void setMuncipalCode(String muncipalCode) {
		this.muncipalCode = muncipalCode;
	}

	/**
	 * @return Returns the name1.
	 */
	public String getName1() {
		return name1;
	}

	/**
	 * @param name1
	 *            The name1 to set.
	 */
	public void setName1(String name1) {
		this.name1 = name1;
	}

	/**
	 * @return Returns the name2.
	 */
	public String getName2() {
		return name2;
	}

	/**
	 * @param name2
	 *            The name2 to set.
	 */
	public void setName2(String name2) {
		this.name2 = name2;
	}

	/**
	 * @return Returns the otherBusinessOccupancy.
	 */
	public boolean isOtherBusinessOccupancy() {
		return otherBusinessOccupancy;
	}

	/**
	 * @param otherBusinessOccupancy
	 *            The otherBusinessOccupancy to set.
	 */
	public void setOtherBusinessOccupancy(boolean otherBusinessOccupancy) {
		this.otherBusinessOccupancy = otherBusinessOccupancy;
	}

	/**
	 * @return Returns the outOfBusinessDate.
	 */
	public String getOutOfBusinessDate() {
		return outOfBusinessDate;
	}

	/**
	 * @param outOfBusinessDate
	 *            The outOfBusinessDate to set.
	 */
	public void setOutOfBusinessDate(String outOfBusinessDate) {
		this.outOfBusinessDate = outOfBusinessDate;
	}

	/**
	 * @return Returns the outOfTownCity.
	 */
	public String getOutOfTownCity() {
		return outOfTownCity;
	}

	/**
	 * @param outOfTownCity
	 *            The outOfTownCity to set.
	 */
	public void setOutOfTownCity(String outOfTownCity) {
		this.outOfTownCity = outOfTownCity;
	}

	/**
	 * @return Returns the outOfTownState.
	 */
	public String getOutOfTownState() {
		return outOfTownState;
	}

	/**
	 * @param outOfTownState
	 *            The outOfTownState to set.
	 */
	public void setOutOfTownState(String outOfTownState) {
		this.outOfTownState = outOfTownState;
	}

	/**
	 * @return Returns the outOfTownStreetName.
	 */
	public String getOutOfTownStreetName() {
		return outOfTownStreetName;
	}

	/**
	 * @param outOfTownStreetName
	 *            The outOfTownStreetName to set.
	 */
	public void setOutOfTownStreetName(String outOfTownStreetName) {
		this.outOfTownStreetName = outOfTownStreetName;
	}

	/**
	 * @return Returns the outOfTownStreetNumber.
	 */
	public String getOutOfTownStreetNumber() {
		return outOfTownStreetNumber;
	}

	/**
	 * @param outOfTownStreetNumber
	 *            The outOfTownStreetNumber to set.
	 */
	public void setOutOfTownStreetNumber(String outOfTownStreetNumber) {
		this.outOfTownStreetNumber = outOfTownStreetNumber;
	}

	/**
	 * @return Returns the outOfTownUnitNumber.
	 */
	public String getOutOfTownUnitNumber() {
		return outOfTownUnitNumber;
	}

	/**
	 * @param outOfTownUnitNumber
	 *            The outOfTownUnitNumber to set.
	 */
	public void setOutOfTownUnitNumber(String outOfTownUnitNumber) {
		this.outOfTownUnitNumber = outOfTownUnitNumber;
	}

	/**
	 * @return Returns the outOfTownZip.
	 */
	public String getOutOfTownZip() {
		return outOfTownZip;
	}

	/**
	 * @param outOfTownZip
	 *            The outOfTownZip to set.
	 */
	public void setOutOfTownZip(String outOfTownZip) {
		this.outOfTownZip = outOfTownZip;
	}

	/**
	 * @return Returns the outOfTownZip4.
	 */
	public String getOutOfTownZip4() {
		return outOfTownZip4;
	}

	/**
	 * @param outOfTownZip4
	 *            The outOfTownZip4 to set.
	 */
	public void setOutOfTownZip4(String outOfTownZip4) {
		this.outOfTownZip4 = outOfTownZip4;
	}

	/**
	 * @return Returns the owner1City.
	 */
	public String getOwner1City() {
		return owner1City;
	}

	/**
	 * @param owner1City
	 *            The owner1City to set.
	 */
	public void setOwner1City(String owner1City) {
		this.owner1City = owner1City;
	}

	/**
	 * @return Returns the owner1State.
	 */
	public String getOwner1State() {
		return owner1State;
	}

	/**
	 * @param owner1State
	 *            The owner1State to set.
	 */
	public void setOwner1State(String owner1State) {
		this.owner1State = owner1State;
	}

	/**
	 * @return Returns the owner1StreetName.
	 */
	public String getOwner1StreetName() {
		return owner1StreetName;
	}

	/**
	 * @param owner1StreetName
	 *            The owner1StreetName to set.
	 */
	public void setOwner1StreetName(String owner1StreetName) {
		this.owner1StreetName = owner1StreetName;
	}

	/**
	 * @return Returns the owner1StreetNumber.
	 */
	public String getOwner1StreetNumber() {
		return owner1StreetNumber;
	}

	/**
	 * @param owner1StreetNumber
	 *            The owner1StreetNumber to set.
	 */
	public void setOwner1StreetNumber(String owner1StreetNumber) {
		this.owner1StreetNumber = owner1StreetNumber;
	}

	/**
	 * @return Returns the owner1UnitNumber.
	 */
	public String getOwner1UnitNumber() {
		return owner1UnitNumber;
	}

	/**
	 * @param owner1UnitNumber
	 *            The owner1UnitNumber to set.
	 */
	public void setOwner1UnitNumber(String owner1UnitNumber) {
		this.owner1UnitNumber = owner1UnitNumber;
	}

	/**
	 * @return Returns the owner1Zip.
	 */
	public String getOwner1Zip() {
		return owner1Zip;
	}

	/**
	 * @param owner1Zip
	 *            The owner1Zip to set.
	 */
	public void setOwner1Zip(String owner1Zip) {
		this.owner1Zip = owner1Zip;
	}

	/**
	 * @return Returns the owner1Zip4.
	 */
	public String getOwner1Zip4() {
		return owner1Zip4;
	}

	/**
	 * @param owner1Zip4
	 *            The owner1Zip4 to set.
	 */
	public void setOwner1Zip4(String owner1Zip4) {
		this.owner1Zip4 = owner1Zip4;
	}

	/**
	 * @return Returns the owner2City.
	 */
	public String getOwner2City() {
		return owner2City;
	}

	/**
	 * @param owner2City
	 *            The owner2City to set.
	 */
	public void setOwner2City(String owner2City) {
		this.owner2City = owner2City;
	}

	/**
	 * @return Returns the owner2State.
	 */
	public String getOwner2State() {
		return owner2State;
	}

	/**
	 * @param owner2State
	 *            The owner2State to set.
	 */
	public void setOwner2State(String owner2State) {
		this.owner2State = owner2State;
	}

	/**
	 * @return Returns the owner2StreetName.
	 */
	public String getOwner2StreetName() {
		return owner2StreetName;
	}

	/**
	 * @param owner2StreetName
	 *            The owner2StreetName to set.
	 */
	public void setOwner2StreetName(String owner2StreetName) {
		this.owner2StreetName = owner2StreetName;
	}

	/**
	 * @return Returns the owner2StreetNumber.
	 */
	public String getOwner2StreetNumber() {
		return owner2StreetNumber;
	}

	/**
	 * @param owner2StreetNumber
	 *            The owner2StreetNumber to set.
	 */
	public void setOwner2StreetNumber(String owner2StreetNumber) {
		this.owner2StreetNumber = owner2StreetNumber;
	}

	/**
	 * @return Returns the owner2UnitNumber.
	 */
	public String getOwner2UnitNumber() {
		return owner2UnitNumber;
	}

	/**
	 * @param owner2UnitNumber
	 *            The owner2UnitNumber to set.
	 */
	public void setOwner2UnitNumber(String owner2UnitNumber) {
		this.owner2UnitNumber = owner2UnitNumber;
	}

	/**
	 * @return Returns the owner2Zip.
	 */
	public String getOwner2Zip() {
		return owner2Zip;
	}

	/**
	 * @param owner2Zip
	 *            The owner2Zip to set.
	 */
	public void setOwner2Zip(String owner2Zip) {
		this.owner2Zip = owner2Zip;
	}

	/**
	 * @return Returns the owner2Zip4.
	 */
	public String getOwner2Zip4() {
		return owner2Zip4;
	}

	/**
	 * @param owner2Zip4
	 *            The owner2Zip4 to set.
	 */
	public void setOwner2Zip4(String owner2Zip4) {
		this.owner2Zip4 = owner2Zip4;
	}

	/**
	 * @return Returns the ownershipType.
	 */
	public String getOwnershipType() {
		return ownershipType;
	}

	/**
	 * @param ownershipType
	 *            The ownershipType to set.
	 */
	public void setOwnershipType(String ownershipType) {
		this.ownershipType = ownershipType;
	}

	/**
	 * @return Returns the prevCity.
	 */
	public String getPrevCity() {
		return prevCity;
	}

	/**
	 * @param prevCity
	 *            The prevCity to set.
	 */
	public void setPrevCity(String prevCity) {
		this.prevCity = prevCity;
	}

	/**
	 * @return Returns the prevState.
	 */
	public String getPrevState() {
		return prevState;
	}

	/**
	 * @param prevState
	 *            The prevState to set.
	 */
	public void setPrevState(String prevState) {
		this.prevState = prevState;
	}

	/**
	 * @return Returns the prevStreetName.
	 */
	public String getPrevStreetName() {
		return prevStreetName;
	}

	/**
	 * @param prevStreetName
	 *            The prevStreetName to set.
	 */
	public void setPrevStreetName(String prevStreetName) {
		this.prevStreetName = prevStreetName;
	}

	/**
	 * @return Returns the prevStreetNumber.
	 */
	public String getPrevStreetNumber() {
		return prevStreetNumber;
	}

	/**
	 * @param prevStreetNumber
	 *            The prevStreetNumber to set.
	 */
	public void setPrevStreetNumber(String prevStreetNumber) {
		this.prevStreetNumber = prevStreetNumber;
	}

	/**
	 * @return Returns the prevUnitNumber.
	 */
	public String getPrevUnitNumber() {
		return prevUnitNumber;
	}

	/**
	 * @param prevUnitNumber
	 *            The prevUnitNumber to set.
	 */
	public void setPrevUnitNumber(String prevUnitNumber) {
		this.prevUnitNumber = prevUnitNumber;
	}

	/**
	 * @return Returns the prevZip.
	 */
	public String getPrevZip() {
		return prevZip;
	}

	/**
	 * @param prevZip
	 *            The prevZip to set.
	 */
	public void setPrevZip(String prevZip) {
		this.prevZip = prevZip;
	}

	/**
	 * @return Returns the prevZip4.
	 */
	public String getPrevZip4() {
		return prevZip4;
	}

	/**
	 * @param prevZip4
	 *            The prevZip4 to set.
	 */
	public void setPrevZip4(String prevZip4) {
		this.prevZip4 = prevZip4;
	}

	/**
	 * @return Returns the quantity.
	 */
	public String getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            The quantity to set.
	 */
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return Returns the quantityNum.
	 */
	public String getQuantityNum() {
		return quantityNum;
	}

	/**
	 * @param quantityNum
	 *            The quantityNum to set.
	 */
	public void setQuantityNum(String quantityNum) {
		this.quantityNum = quantityNum;
	}

	/**
	 * @return Returns the sicCode.
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            The sicCode to set.
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	/**
	 * @return Returns the socialSecurityNumber.
	 */
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	/**
	 * @param socialSecurityNumber
	 *            The socialSecurityNumber to set.
	 */
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	/**
	 * @return Returns the squareFootage.
	 */
	public String getSquareFootage() {
		return squareFootage;
	}

	/**
	 * @param squareFootage
	 *            The squareFootage to set.
	 */
	public void setSquareFootage(String squareFootage) {
		this.squareFootage = squareFootage;
	}

	/**
	 * @return Returns the startingDate.
	 */
	public String getStartingDate() {
		return startingDate;
	}

	/**
	 * @param startingDate
	 *            The startingDate to set.
	 */
	public void setStartingDate(String startingDate) {
		this.startingDate = startingDate;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(String subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return Returns the title1.
	 */
	public String getTitle1() {
		return title1;
	}

	/**
	 * @param title1
	 *            The title1 to set.
	 */
	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	/**
	 * @return Returns the title2.
	 */
	public String getTitle2() {
		return title2;
	}

	/**
	 * @param title2
	 *            The title2 to set.
	 */
	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	/**
	 * @return Returns the updatedBy.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            The updatedBy to set.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the BusinessLicenseActivity
	 * 
	 * @return Returns a BusinessLicenseActivity
	 */
	public BusinessTaxActivity getBusinessTaxActivity() throws Exception {

		businessTaxActivity = new BusinessTaxActivity();

		businessTaxActivity.setApplicationType(LookupAgent.getApplicationType(StringUtils.s2i(this.getApplicationType())));
		businessTaxActivity.setActivityStatus(LookupAgent.getActivityStatus(StringUtils.s2i(this.getActivityStatus())));
		businessTaxActivity.setOwnershipType(LookupAgent.getOwnershipType(StringUtils.s2i(this.getOwnershipType())));
		businessTaxActivity.setActivityType(LookupAgent.getActivityType(this.getActivityType()));
		businessTaxActivity.setActivitySubType(LookupAgent.getActivitySubType(this.getActivitySubType()));
		businessTaxActivity.setQuantity(LookupAgent.getBtQuantityType(StringUtils.s2i(this.getQuantity())));
		businessTaxActivity.setStreet(LookupAgent.getStreet(this.getBusinessAddressStreetName()));
		businessTaxActivity.setSubProjectId(StringUtils.s2i(this.getSubProjectId()));
		businessTaxActivity.setBusinessLocation(this.isBusinessLocation());
		businessTaxActivity.setBusinessAddressStreetNumber(this.getBusinessAddressStreetNumber());
		businessTaxActivity.setBusinessAddressStreetFraction(this.getBusinessAddressStreetFraction());
		businessTaxActivity.setBusinessAddressUnitNumber(this.getBusinessAddressUnitNumber());
		businessTaxActivity.setBusinessAddressCity(this.getBusinessAddressCity());
		businessTaxActivity.setBusinessAddressState(this.getBusinessAddressState());
		businessTaxActivity.setBusinessAddressZip(this.getBusinessAddressZip());
		businessTaxActivity.setBusinessAddressZip4(this.getBusinessAddressZip4());
		businessTaxActivity.setOutOfTownStreetNumber(this.getOutOfTownStreetNumber());
		businessTaxActivity.setOutOfTownStreetName(this.getOutOfTownStreetName());
		businessTaxActivity.setOutOfTownUnitNumber(this.getOutOfTownUnitNumber());
		businessTaxActivity.setOutOfTownCity(this.getOutOfTownCity());
		businessTaxActivity.setOutOfTownState(this.getOutOfTownState());
		businessTaxActivity.setOutOfTownZip(this.getOutOfTownZip());
		businessTaxActivity.setOutOfTownZip4(this.getOutOfTownZip4());
		businessTaxActivity.setActivityNumber(this.getActivityNumber());
		businessTaxActivity.setBusinessName(this.getBusinessName());
		businessTaxActivity.setBusinessAccountNumber(this.getBusinessAccountNumber());
		businessTaxActivity.setCorporateName(this.getCorporateName());
		businessTaxActivity.setAttn(this.getAttn());
		businessTaxActivity.setBusinessPhone(this.getBusinessPhone());
		businessTaxActivity.setBusinessExtension(this.getBusinessExtension());
		businessTaxActivity.setBusinessFax(this.getBusinessFax());
		businessTaxActivity.setMuncipalCode(this.getMuncipalCode());
		businessTaxActivity.setSicCode(this.getSicCode());
		businessTaxActivity.setDescOfBusiness(this.getDescOfBusiness());
		businessTaxActivity.setClassCode(this.getClassCode());
		businessTaxActivity.setHomeOccupation(this.isHomeOccupation());
		businessTaxActivity.setDecalCode(this.isDecalCode());
		businessTaxActivity.setOtherBusinessOccupancy(this.isOtherBusinessOccupancy());
		businessTaxActivity.setFederalIdNumber(this.getFederalIdNumber());
		businessTaxActivity.setEmailAddress(this.getEmailAddress());
		businessTaxActivity.setSocialSecurityNumber(this.getSocialSecurityNumber());
		businessTaxActivity.setMailStreetNumber(this.getMailStreetNumber());
		businessTaxActivity.setMailStreetName(this.getMailStreetName());
		businessTaxActivity.setMailUnitNumber(this.getMailUnitNumber());
		businessTaxActivity.setMailCity(this.getMailCity());
		businessTaxActivity.setMailState(this.getMailState());
		businessTaxActivity.setMailZip(this.getMailZip());
		businessTaxActivity.setMailZip4(this.getMailZip4());
		businessTaxActivity.setPrevStreetNumber(this.getPrevStreetNumber());
		businessTaxActivity.setPrevStreetName(this.getPrevStreetName());
		businessTaxActivity.setPrevUnitNumber(this.getPrevUnitNumber());
		businessTaxActivity.setPrevCity(this.getPrevCity());
		businessTaxActivity.setPrevState(this.getPrevState());
		businessTaxActivity.setPrevZip(this.getPrevZip());
		businessTaxActivity.setPrevZip4(this.getPrevZip4());
		businessTaxActivity.setName1(this.getName1());
		businessTaxActivity.setTitle1(this.getTitle1());
		businessTaxActivity.setOwner1StreetNumber(this.getOwner1StreetNumber());
		businessTaxActivity.setOwner1StreetName(this.getOwner1StreetName());
		businessTaxActivity.setOwner1UnitNumber(this.getOwner1UnitNumber());
		businessTaxActivity.setOwner1City(this.getOwner1City());
		businessTaxActivity.setOwner1State(this.getOwner1State());
		businessTaxActivity.setOwner1Zip(this.getOwner1Zip());
		businessTaxActivity.setOwner1Zip4(this.getOwner1Zip4());
		businessTaxActivity.setName2(this.getName2());
		businessTaxActivity.setTitle2(this.getTitle2());
		businessTaxActivity.setOwner2StreetNumber(this.getOwner2StreetNumber());
		businessTaxActivity.setOwner2StreetName(this.getOwner2StreetName());
		businessTaxActivity.setOwner2UnitNumber(this.getOwner2UnitNumber());
		businessTaxActivity.setOwner2City(this.getOwner2City());
		businessTaxActivity.setOwner2State(this.getOwner2State());
		businessTaxActivity.setOwner2Zip(this.getOwner2Zip());
		businessTaxActivity.setOwner2Zip4(this.getOwner2Zip4());
		businessTaxActivity.setSquareFootage(this.getSquareFootage());
		businessTaxActivity.setQuantityNum(this.getQuantityNum());
		businessTaxActivity.setDriverLicense(this.getDriverLicense());
		businessTaxActivity.setDisplayContent(this.getDisplayContent());
		businessTaxActivity.setCreationDate(StringUtils.str2cal(this.getCreationDate()));
		businessTaxActivity.setApplicationDate(StringUtils.str2cal(this.getApplicationDate()));
		businessTaxActivity.setIssueDate(StringUtils.str2cal(this.getIssueDate()));
		businessTaxActivity.setStartingDate(StringUtils.str2cal(this.getStartingDate()));
		businessTaxActivity.setOutOfBusinessDate(StringUtils.str2cal(this.getOutOfBusinessDate()));
		businessTaxActivity.setMailAttn(this.getMailAttn());
		businessTaxActivity.setPreAttn(this.getPreAttn());
		businessTaxActivity.setOwner1Attn(this.getOwner1Attn());
		businessTaxActivity.setOwner2Attn(this.getOwner2Attn());
		businessTaxActivity.setMultiAddress(this.getMultiAddress());
		logger.debug("In getbusinessTaxActivity " + businessTaxActivity);

		return businessTaxActivity;
	}

	public BusinessTaxActivityForm setBusinessTaxActivity(BusinessTaxActivity businessTaxActivity) throws Exception {

		BusinessTaxActivityForm businessTaxActivityForm = new BusinessTaxActivityForm();
		logger.debug("In setbusinessTaxActivity " + businessTaxActivity);

		if (businessTaxActivity.getActivityType() != null) {
			businessTaxActivityForm.setActivityType(businessTaxActivity.getActivityType().getTypeId());
		} else {
			businessTaxActivityForm.setActivityType("");
		}
		if (businessTaxActivity.getActivityStatus() != null) {
			businessTaxActivityForm.setActivityStatus(StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()));
		} else {
			businessTaxActivityForm.setActivityStatus("");
		}
		if (businessTaxActivity.getOwnershipType() != null) {
			businessTaxActivityForm.setOwnershipType(StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()));
		} else {
			businessTaxActivityForm.setOwnershipType("");
		}
		if (businessTaxActivity.getQuantity() != null) {
			businessTaxActivityForm.setQuantity(StringUtils.i2s(businessTaxActivity.getQuantity().getId()));
		} else {
			businessTaxActivityForm.setQuantity("");
		}
		if (businessTaxActivity.getMailAttn() != null) {
			businessTaxActivityForm.setMailAttn(businessTaxActivity.getMailAttn());
		} else {
			businessTaxActivityForm.setMailAttn("");
		}
		if (businessTaxActivity.getPreAttn() != null) {
			businessTaxActivityForm.setPreAttn(businessTaxActivity.getPreAttn());
		} else {
			businessTaxActivityForm.setPreAttn("");
		}
		if (businessTaxActivity.getOwner1Attn() != null) {
			businessTaxActivityForm.setOwner1Attn(businessTaxActivity.getOwner1Attn());
		} else {
			businessTaxActivityForm.setOwner1Attn("");
		}
		if (businessTaxActivity.getOwner2Attn() != null) {
			businessTaxActivityForm.setOwner2Attn(businessTaxActivity.getOwner2Attn());
		} else {
			businessTaxActivityForm.setOwner2Attn("");
		}
		if(businessTaxActivity.getMultiAddress() != null){
			businessTaxActivityForm.setMultiAddress(businessTaxActivity.getMultiAddress());
		}else{
			businessTaxActivityForm.setMultiAddress(new MultiAddress[0]);
		}
		
		
		businessTaxActivityForm.setActivityTypeDescription(businessTaxActivity.getActivityType().getDescription());
		businessTaxActivityForm.setBusinessAddressStreetNumber(businessTaxActivity.getBusinessAddressStreetNumber());
		businessTaxActivityForm.setBusinessAddressStreetFraction(businessTaxActivity.getBusinessAddressStreetFraction());
		businessTaxActivityForm.setBusinessAddressStreetName(businessTaxActivity.getBusinessAddressStreetName());
		businessTaxActivityForm.setBusinessAddressUnitNumber(businessTaxActivity.getBusinessAddressUnitNumber());
		businessTaxActivityForm.setBusinessAddressCity(businessTaxActivity.getBusinessAddressCity());
		businessTaxActivityForm.setBusinessAddressState(businessTaxActivity.getBusinessAddressState());
		businessTaxActivityForm.setBusinessAddressZip(businessTaxActivity.getBusinessAddressZip());
		businessTaxActivityForm.setBusinessAddressZip4(businessTaxActivity.getBusinessAddressZip4());
		businessTaxActivityForm.setOutOfTownStreetNumber(businessTaxActivity.getOutOfTownStreetNumber());
		businessTaxActivityForm.setOutOfTownStreetName(businessTaxActivity.getOutOfTownStreetName());
		businessTaxActivityForm.setOutOfTownUnitNumber(businessTaxActivity.getOutOfTownUnitNumber());
		businessTaxActivityForm.setOutOfTownCity(businessTaxActivity.getOutOfTownCity());
		businessTaxActivityForm.setOutOfTownState(businessTaxActivity.getOutOfTownState());
		businessTaxActivityForm.setOutOfTownZip(businessTaxActivity.getOutOfTownZip());
		businessTaxActivityForm.setOutOfTownZip4(businessTaxActivity.getOutOfTownZip4());
		businessTaxActivityForm.setActivityNumber(businessTaxActivity.getActivityNumber());
		businessTaxActivityForm.setBusinessName(businessTaxActivity.getBusinessName());
		businessTaxActivityForm.setBusinessAccountNumber(businessTaxActivity.getBusinessAccountNumber());
		businessTaxActivityForm.setCorporateName(businessTaxActivity.getCorporateName());
		businessTaxActivityForm.setAttn(businessTaxActivity.getAttn());
		businessTaxActivityForm.setBusinessPhone(businessTaxActivity.getBusinessPhone());
		businessTaxActivityForm.setBusinessExtension(businessTaxActivity.getBusinessExtension());
		businessTaxActivityForm.setBusinessFax(businessTaxActivity.getBusinessFax());
		businessTaxActivityForm.setDescOfBusiness(businessTaxActivity.getDescOfBusiness());
		businessTaxActivityForm.setSicCode(businessTaxActivity.getSicCode());
		businessTaxActivityForm.setMuncipalCode(businessTaxActivity.getMuncipalCode());
		businessTaxActivityForm.setClassCode(businessTaxActivity.getClassCode());
		businessTaxActivityForm.setHomeOccupation(businessTaxActivity.isHomeOccupation());
		businessTaxActivityForm.setDecalCode(businessTaxActivity.isDecalCode());
		businessTaxActivityForm.setOtherBusinessOccupancy(businessTaxActivity.isOtherBusinessOccupancy());
		businessTaxActivityForm.setFederalIdNumber(businessTaxActivity.getFederalIdNumber());
		businessTaxActivityForm.setEmailAddress(businessTaxActivity.getEmailAddress());
		businessTaxActivityForm.setSocialSecurityNumber(businessTaxActivity.getSocialSecurityNumber());
		businessTaxActivityForm.setMailStreetNumber(businessTaxActivity.getMailStreetNumber());
		businessTaxActivityForm.setMailStreetName(businessTaxActivity.getMailStreetName());
		businessTaxActivityForm.setMailUnitNumber(businessTaxActivity.getMailUnitNumber());
		businessTaxActivityForm.setMailCity(businessTaxActivity.getMailCity());
		businessTaxActivityForm.setMailState(businessTaxActivity.getMailState());
		businessTaxActivityForm.setMailZip(businessTaxActivity.getMailZip());
		businessTaxActivityForm.setMailZip4(businessTaxActivity.getMailZip4());
		businessTaxActivityForm.setPrevStreetNumber(businessTaxActivity.getPrevStreetNumber());
		businessTaxActivityForm.setPrevStreetName(businessTaxActivity.getPrevStreetName());
		businessTaxActivityForm.setPrevUnitNumber(businessTaxActivity.getPrevUnitNumber());
		businessTaxActivityForm.setPrevCity(businessTaxActivity.getPrevCity());
		businessTaxActivityForm.setPrevState(businessTaxActivity.getPrevState());
		businessTaxActivityForm.setPrevZip(businessTaxActivity.getPrevZip());
		businessTaxActivityForm.setPrevZip4(businessTaxActivity.getPrevZip4());
		businessTaxActivityForm.setName1(businessTaxActivity.getName1());
		businessTaxActivityForm.setTitle1(businessTaxActivity.getTitle1());
		businessTaxActivityForm.setOwner1StreetNumber(businessTaxActivity.getOwner1StreetNumber());
		businessTaxActivityForm.setOwner1StreetName(businessTaxActivity.getOwner1StreetName());
		businessTaxActivityForm.setOwner1UnitNumber(businessTaxActivity.getOwner1UnitNumber());
		businessTaxActivityForm.setOwner1City(businessTaxActivity.getOwner1City());
		businessTaxActivityForm.setOwner1State(businessTaxActivity.getOwner1State());
		businessTaxActivityForm.setOwner1Zip(businessTaxActivity.getOwner1Zip());
		businessTaxActivityForm.setOwner1Zip4(businessTaxActivity.getOwner1Zip4());
		businessTaxActivityForm.setName2(businessTaxActivity.getName2());
		businessTaxActivityForm.setTitle2(businessTaxActivity.getTitle2());
		businessTaxActivityForm.setOwner2StreetNumber(businessTaxActivity.getOwner2StreetNumber());
		businessTaxActivityForm.setOwner2StreetName(businessTaxActivity.getOwner2StreetName());
		businessTaxActivityForm.setOwner2UnitNumber(businessTaxActivity.getOwner2UnitNumber());
		businessTaxActivityForm.setOwner2City(businessTaxActivity.getOwner2City());
		businessTaxActivityForm.setOwner2State(businessTaxActivity.getOwner2State());
		businessTaxActivityForm.setOwner2Zip(businessTaxActivity.getOwner2Zip());
		businessTaxActivityForm.setOwner2Zip4(businessTaxActivity.getOwner2Zip4());
		businessTaxActivityForm.setSquareFootage(businessTaxActivity.getSquareFootage());
		businessTaxActivityForm.setQuantityNum(businessTaxActivity.getQuantityNum());
		businessTaxActivityForm.setDriverLicense(businessTaxActivity.getDriverLicense());
		businessTaxActivityForm.setDisplayContent(businessTaxActivity.getDisplayContent());

		businessTaxActivityForm.setCreationDate(StringUtils.cal2str(businessTaxActivity.getCreationDate()));
		businessTaxActivityForm.setApplicationDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate()));
		businessTaxActivityForm.setIssueDate(StringUtils.cal2str(businessTaxActivity.getIssueDate()));
		businessTaxActivityForm.setStartingDate(StringUtils.cal2str(businessTaxActivity.getStartingDate()));
		businessTaxActivityForm.setOutOfBusinessDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate()));
		

		return businessTaxActivityForm;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		attn=null;
		businessAddressStreetName=null;
		businessAddressStreetNumber = null;
		businessAddressStreetFraction = null;
		businessAddressUnitNumber = null;
		businessAddressCity = null;
		businessAddressState = null;
		businessAddressZip = null;
		businessAddressZip4 = null;
		applicationType = null;
		outOfTownStreetNumber = null;
		outOfTownStreetName = null;
		outOfTownUnitNumber = null;
		outOfTownCity = null;
		outOfTownState = null;
		outOfTownZip = null;
		outOfTownZip4 = null;
		activityNumber = null;
		activityStatus = null;
		creationDate = null;
		businessName = null;
		businessAccountNumber = null;
		corporateName = null;
		businessPhone = null;
		businessExtension = null;
		businessFax = null;
		activityType = null;
		activitySubType = null;
		muncipalCode = null;
		sicCode = null;
		descOfBusiness = null;
		classCode = null;
		applicationDate = null;
		startingDate = null;
		outOfBusinessDate = null;
		ownershipType = null;
		federalIdNumber = null;
		emailAddress = null;
		socialSecurityNumber = null;
		mailStreetNumber = null;
		mailStreetName = null;
		mailUnitNumber = null;
		mailCity = null;
		mailState = null;
		mailZip = null;
		mailZip4 = null;
		prevStreetNumber = null;
		prevStreetName = null;
		prevUnitNumber = null;
		prevCity = null;
		prevState = null;
		prevZip = null;
		prevZip4 = null;
		name1 = null;
		title1 = null;
		owner1StreetNumber = null;
		owner1StreetName = null;
		owner1UnitNumber = null;
		owner1City = null;
		owner1State = null;
		owner1Zip = null;
		owner1Zip4 = null;
		name2 = null;
		title2 = null;
		owner2StreetNumber = null;
		owner2StreetName = null;
		owner2UnitNumber = null;
		owner2City = null;
		owner2State = null;
		owner2Zip = null;
		owner2Zip4 = null;
		squareFootage = null;
		driverLicense = null;
		quantity = null;
		quantityNum = null;
		subProjectId = null;
		displayContent = null;
	}

	/**
	 * @author Gayathri The reset method is to reset form values when it changes its application type
	 */

	public void reset() {
		this.attn = null; 
		this.creationDate = null;
		this.businessAddressStreetNumber = null;
		this.businessAddressStreetName = null;
		this.businessAddressStreetFraction = null;
		this.businessAddressUnitNumber = null;
		this.businessAddressCity = null;
		this.businessAddressState = null;
		this.businessAddressZip = null;
		this.businessAddressZip4 = null;
		this.outOfTownStreetNumber = null;
		this.outOfTownStreetName = null;
		this.outOfTownUnitNumber = null;
		this.outOfTownCity = null;
		this.outOfTownState = null;
		this.outOfTownZip = null;
		this.outOfTownZip4 = null;
		this.activityNumber = null;
		this.creationDate = null;
		this.businessName = null;
		this.businessAccountNumber = null;
		this.corporateName = null;
		this.businessPhone = null;
		this.businessExtension = null;
		this.businessFax = null;
		this.activityType = null;
		this.activitySubType = null;
		this.muncipalCode = null;
		this.sicCode = null;
		this.descOfBusiness = null;
		this.classCode = null;
		this.decalCode = false;
		this.homeOccupation = false;
		this.otherBusinessOccupancy = false;
		this.startingDate = null;
		this.outOfBusinessDate = null;
		this.ownershipType = null;
		this.federalIdNumber = null;
		this.emailAddress = null;
		this.socialSecurityNumber = null;
		this.issueDate = null;
		this.mailStreetNumber = null;
		this.mailStreetName = null;
		this.mailUnitNumber = null;
		this.mailCity = null;
		this.mailState = null;
		this.mailZip = null;
		this.mailZip4 = null;
		this.prevStreetNumber = null;
		this.prevStreetName = null;
		this.prevUnitNumber = null;
		this.prevCity = null;
		this.prevState = null;
		this.prevZip = null;
		this.prevZip4 = null;
		this.name1 = null;
		this.title1 = null;
		this.owner1StreetNumber = null;
		this.owner1StreetName = null;
		this.owner1UnitNumber = null;
		this.owner1City = null;
		this.owner1State = null;
		this.owner1Zip = null;
		this.owner1Zip4 = null;
		this.name2 = null;
		this.title2 = null;
		this.owner2StreetNumber = null;
		this.owner2StreetName = null;
		this.owner2UnitNumber = null;
		this.owner2City = null;
		this.owner2State = null;
		this.owner2Zip = null;
		this.owner2Zip4 = null;
		this.squareFootage = null;
		this.driverLicense = null;
		this.quantity = null;
		this.quantityNum = null;
		this.subProjectId = null;
		this.displayContent = null;
	}

	public String getMailAttn() {
		return mailAttn;
	}

	public void setMailAttn(String mailAttn) {
		this.mailAttn = mailAttn;
	}

	public String getPreAttn() {
		return preAttn;
	}

	public void setPreAttn(String preAttn) {
		this.preAttn = preAttn;
	}

	public String getOwner1Attn() {
		return owner1Attn;
	}

	public void setOwner1Attn(String owner1Attn) {
		this.owner1Attn = owner1Attn;
	}

	public String getOwner2Attn() {
		return owner2Attn;
	}

	public void setOwner2Attn(String owner2Attn) {
		this.owner2Attn = owner2Attn;
	}

	public MultiAddress[] getMultiAddress() {
		return multiAddress;
	}

	public void setMultiAddress(MultiAddress[] multiAddress) {
		this.multiAddress = multiAddress;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getLsoId() {
		return lsoId;
	}

	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}
}
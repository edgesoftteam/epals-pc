package elms.control.beans;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import elms.app.common.AttachmentEdit;

/**
 * The form part of the attacment module of OBC
 * 
 * @author abelaguly updated Apr 13, 2004 1:51:29 PM
 */
public class AttachmentForm extends ActionForm {

	protected int attachmentId;
	protected int levelId;
	protected String attachmentLevel;
	protected String description;
	protected String fileName;
	protected String location;
	protected int fileSize;
	protected String creationDate;
	protected int createdBy;
	protected String deleted;
	//protected FormFile theFile;
	protected String attachmentCount;

	protected String keyword1;
	protected String keyword2;
	protected String keyword3;
	protected String keyword4;

	protected AttachmentEdit[] attachmentList = {};
	protected String[] selectedAttachments = {};
  protected String attachmentType;
	
	
	
	 private FormFile theFile;
	    private ArrayList<FormFile> listFile = new ArrayList<FormFile>();
	    private int  index=0;


	    public FormFile getTheFile(int index)
	    {
	        return this.theFile;
	    }
	    public void setTheFile(int index,FormFile theFile )
	    {
	        System.out.println(index +" setter " + theFile.getFileName());
	        System.out.println(index +" setter " + theFile.getFileSize());
	        this.theFile=theFile;
	        
	        setListFile(theFile);
	        this.index++;
	    }
	    public ArrayList<FormFile> getListFile()
	    {
	        return this.listFile;
	    }
	    public void setListFile(FormFile theFile)
	    {
	        this.listFile.add(index, theFile);
	    }

	/**
	 * Gets the attachmentId
	 * 
	 * @return Returns a int
	 */
	public int getAttachmentId() {
		return attachmentId;
	}

	/**
	 * Sets the attachmentId
	 * 
	 * @param attachmentId
	 *            The attachmentId to set
	 */
	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the attachmentLevel
	 * 
	 * @return Returns a String
	 */
	public String getAttachmentLevel() {
		return attachmentLevel;
	}

	/**
	 * Sets the attachmentLevel
	 * 
	 * @param attachmentLevel
	 *            The attachmentLevel to set
	 */
	public void setAttachmentLevel(String attachmentLevel) {
		this.attachmentLevel = attachmentLevel;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the fileName
	 * 
	 * @return Returns a String
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the fileName
	 * 
	 * @param fileName
	 *            The fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the location
	 * 
	 * @return Returns a String
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Sets the location
	 * 
	 * @param location
	 *            The location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Gets the fileSize
	 * 
	 * @return Returns a int
	 */
	public int getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the fileSize
	 * 
	 * @param fileSize
	 *            The fileSize to set
	 */
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a int
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the deleted
	 * 
	 * @return Returns a String
	 */
	public String getDeleted() {
		return deleted;
	}

	/**
	 * Sets the deleted
	 * 
	 * @param deleted
	 *            The deleted to set
	 */
	public void setDeleted(String deleted) {
		this.deleted = deleted;
	}

	/**
	 * Gets the theFile
	 * 
	 * @return Returns a FormFile
	 */
	public FormFile getTheFile() {
		return theFile;
	}

	/**
	 * Sets the theFile
	 * 
	 * @param theFile
	 *            The theFile to set
	 */
	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the attachmentList
	 * 
	 * @return Returns a AttachmentEdit[]
	 */
	public AttachmentEdit[] getAttachmentList() {
		return attachmentList;
	}

	/**
	 * Sets the attachmentList
	 * 
	 * @param attachmentList
	 *            The attachmentList to set
	 */
	public void setAttachmentList(AttachmentEdit[] attachmentList) {
		this.attachmentList = attachmentList;
	}

	/**
	 * Gets the attachmentCount
	 * 
	 * @return Returns a String
	 */
	public String getAttachmentCount() {
		return attachmentCount;
	}

	/**
	 * Sets the attachmentCount
	 * 
	 * @param attachmentCount
	 *            The attachmentCount to set
	 */
	public void setAttachmentCount(String attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	/**
	 * Gets the selectedAttachments
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedAttachments() {
		return selectedAttachments;
	}

	/**
	 * Sets the selectedAttachments
	 * 
	 * @param selectedAttachments
	 *            The selectedAttachments to set
	 */
	public void setSelectedAttachments(String[] selectedAttachments) {
		this.selectedAttachments = selectedAttachments;
	}

	/**
	 * @return
	 */
	public String getKeyword1() {
		return keyword1;
	}

	/**
	 * @return
	 */
	public String getKeyword2() {
		return keyword2;
	}

	/**
	 * @return
	 */
	public String getKeyword3() {
		return keyword3;
	}

	/**
	 * @return
	 */
	public String getKeyword4() {
		return keyword4;
	}

	/**
	 * @param string
	 */
	public void setKeyword1(String string) {
		keyword1 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword2(String string) {
		keyword2 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword3(String string) {
		keyword3 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword4(String string) {
		keyword4 = string;
	}

	/**
	 * @return the attachmentType
	 */
	public String getAttachmentType() {
		return attachmentType;
	}

	/**
	 * @param attachmentType the attachmentType to set
	 */
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	

}
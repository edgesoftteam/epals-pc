package elms.control.beans;

import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.finance.LedgerEdit;
import elms.app.finance.PaymentDetailEdit;
import elms.util.StringUtils;

/**
 * @author Shekhar Jain
 */
public class LedgerForm extends ActionForm {

	// Declare the variable and object declarations used by the class here
	private int activityId;
	private int paymentId;
	private String activityNbr;

	private LedgerEdit[] transactionList;
	private PaymentDetailEdit[] detailList;
	static Logger logger = Logger.getLogger(LedgerForm.class.getName());

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		if (!(transactionList == null))
			for (int i = 0; i < transactionList.length; i++)
				transactionList[i].setHide("");
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		return errors;
	}

	public LedgerForm() {
		super();
		this.activityId = 0;
		this.paymentId = 0;
	}

	/**
	 * Returns the activityId.
	 * 
	 * @return int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Returns the transactionList.
	 * 
	 * @return LedgerEdit[]
	 */
	public LedgerEdit[] getTransactionList() {
		return transactionList;
	}

	/**
	 * Sets the activityId.
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * Sets the transactionList.
	 * 
	 * @param transactionList
	 *            The transactionList to set
	 */
	public void setTransactionList(LedgerEdit[] transactionList) {
		this.transactionList = transactionList;
	}

	public void setTransactionList(RowSet rs) throws Exception {

		int i = 0;
		try {
			rs.beforeFirst();
			while (rs.next())
				i++;
			LedgerEdit[] ledgers = new LedgerEdit[i];
			rs.beforeFirst();

			i = 0;
			while (rs.next()) {
				LedgerEdit ledger = new LedgerEdit();
				String hide = rs.getString("hide");
				hide = (hide == null || hide.equals("N")) ? "" : "on";
				ledger.setHide(hide);
				ledger.setTransactionId(rs.getString("pymnt_id"));
				ledger.setDate(StringUtils.date2str(rs.getDate("pymnt_dt")));
				ledger.setPostDate(StringUtils.date2str(rs.getDate("post_date")));
				ledger.setType(rs.getString("pay_type"));
				ledger.setAmount(StringUtils.dbl2$(rs.getDouble("pymnt_amnt")));
				ledger.setMethod(rs.getString("pymnt_method"));
				ledger.setComments(rs.getString("comnt"));
				ledger.setCheckNbr(rs.getString("accnt_no"));
				ledger.setDepositId(rs.getString("people_name"));
				ledger.setOtherPayee(rs.getString("payee"));
				if(ledger.getOtherPayee()==null){
				ledger.setOtherPayee(rs.getString("people_name"));
				}
				
				ledger.setEnteredBy(rs.getString("entered_by"));
				ledger.setAuthorizedBy(rs.getString("authorized_by"));
				ledger.setVoidBy(rs.getString("voided_by"));
				ledger.setVoidPayment(rs.getString("VOID"));
				ledger.setVoidDate(StringUtils.date2str(rs.getDate("VOID_DT")));
				ledgers[i++] = ledger;
			}
			this.setTransactionList(ledgers);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

	}

	/**
	 * Returns the detailList.
	 * 
	 * @return PaymentDetailEdit[]
	 */
	public PaymentDetailEdit[] getDetailList() {
		return detailList;
	}

	/**
	 * Sets the detailList.
	 * 
	 * @param detailList
	 *            The detailList to set
	 */
	public void setDetailList(PaymentDetailEdit[] detailList) {
		this.detailList = detailList;
	}

	public void setDetailList(RowSet rs) {

		int i = 0;
		String feeType;
		try {
			rs.beforeFirst();
			while (rs.next())
				i++;
			PaymentDetailEdit[] details = new PaymentDetailEdit[i];
			rs.beforeFirst();

			i = 0;
			while (rs.next()) {
				PaymentDetailEdit detail = new PaymentDetailEdit();
				switch (rs.getInt("fee_fl_4")) {
				case 1:
					feeType = "Plan Check Fee";
					break;
				case 2:
					feeType = "Permit Fees";
					break;
				case 3:
					feeType = "Development Fees";
					break;
				case 4:
					feeType = "Penalty Fees";
					break;
				case 5:
					feeType = "Business Tax";
					break;
				default:
					feeType = "";
					break;
				}
				detail.setTransactionId(rs.getString("trans_id"));
				detail.setAccountNbr(rs.getString("fee_account"));
				detail.setDescription(rs.getString("fee_desc"));
				detail.setAmount(StringUtils.dbl2$(rs.getDouble("amnt")));
				detail.setComment(rs.getString("comments"));
				detail.setFeeType(feeType);
				details[i++] = detail;
			}
			this.setDetailList(details);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
		}
	}

	/**
	 * Returns the paymentId.
	 * 
	 * @return int
	 */
	public int getPaymentId() {
		return paymentId;
	}

	/**
	 * Sets the paymentId.
	 * 
	 * @param paymentId
	 *            The paymentId to set
	 */
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}

	/**
	 * Returns the activityNbr.
	 * 
	 * @return String
	 */
	public String getActivityNbr() {
		return activityNbr;
	}

	/**
	 * Sets the activityNbr.
	 * 
	 * @param activityNbr
	 *            The activityNbr to set
	 */
	public void setActivityNbr(String activityNbr) {
		this.activityNbr = activityNbr;
	}

}

package elms.control.beans;

import java.util.Calendar;
import java.util.Date;

import elms.util.StringUtils;

public class LookupFeeEdit {

	protected String lookupFeeId = "";
	protected String lookupFee = "";
	protected String lowRange = "";
	protected String highRange = "";
	protected String creationDate = "";
	protected String result = "";
	protected String plus = "";
	protected String over = "";
	private String check = "";
	private String up = "";

	/**
	 * Gets the lookupFee
	 * 
	 * @return Returns a String
	 */
	public String getLookupFee() {
		return lookupFee;
	}

	/**
	 * Sets the lookupFee
	 * 
	 * @param lookupFee
	 *            The lookupFee to set
	 */
	public void setLookupFee(String lookupFee) {
		this.lookupFee = lookupFee;
	}

	/**
	 * Gets the lowRange
	 * 
	 * @return Returns a String
	 */
	public String getLowRange() {
		return lowRange;
	}

	/**
	 * Sets the lowRange
	 * 
	 * @param lowRange
	 *            The lowRange to set
	 */
	public void setLowRange(String lowRange) {
		this.lowRange = lowRange;
	}

	/**
	 * Gets the highRange
	 * 
	 * @return Returns a String
	 */
	public String getHighRange() {
		return highRange;
	}

	/**
	 * Sets the highRange
	 * 
	 * @param highRange
	 *            The highRange to set
	 */
	public void setHighRange(String highRange) {
		this.highRange = highRange;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		try {
			if (creationDate == null) {
				this.creationDate = "";
			} else {
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(creationDate);
				this.creationDate = StringUtils.cal2str(tmpCalendar);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Gets the result
	 * 
	 * @return Returns a String
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Sets the result
	 * 
	 * @param result
	 *            The result to set
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Gets the plus
	 * 
	 * @return Returns a String
	 */
	public String getPlus() {
		return plus;
	}

	/**
	 * Sets the plus
	 * 
	 * @param plus
	 *            The plus to set
	 */
	public void setPlus(String plus) {
		this.plus = plus;
	}

	/**
	 * Gets the over
	 * 
	 * @return Returns a String
	 */
	public String getOver() {
		return over;
	}

	/**
	 * Sets the over
	 * 
	 * @param over
	 *            The over to set
	 */
	public void setOver(String over) {
		this.over = over;
	}

	/**
	 * Gets the check
	 * 
	 * @return Returns a String
	 */
	public String getCheck() {
		return check;
	}

	/**
	 * Sets the check
	 * 
	 * @param check
	 *            The check to set
	 */
	public void setCheck(String check) {
		this.check = check;
	}

	/**
	 * Gets the up
	 * 
	 * @return Returns a String
	 */
	public String getUp() {
		return up;
	}

	/**
	 * Sets the up
	 * 
	 * @param up
	 *            The up to set
	 */
	public void setUp(String up) {
		this.up = up;
	}

	/**
	 * Gets the lookupFeeId
	 * 
	 * @return Returns a String
	 */
	public String getLookupFeeId() {
		return lookupFeeId;
	}

	/**
	 * Sets the lookupFeeId
	 * 
	 * @param lookupFeeId
	 *            The lookupFeeId to set
	 */
	public void setLookupFeeId(String lookupFeeId) {
		this.lookupFeeId = lookupFeeId;
	}

}

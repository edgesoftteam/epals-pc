package elms.control.beans;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import elms.app.common.Condition;

/**
 * @author Shekhar Jain
 */
public class ConditionForm extends ActionForm {

	protected int conditionId;
	protected int levelId;
	protected String conditionLevel;
	protected int libraryId;
	protected String conditionCode;
	protected String shortText;
	protected String rte1;
	protected String text;
	protected String tmpText;
	protected String inspectable;
	protected String warning;
	protected String dpa;
	protected String complete;
	protected int createdBy;
	protected String createdDate;
	protected int updatedBy;
	protected String updatedByName;
	protected String updatedDate;
	protected int approvedBy;
	protected String approvedDate;
	protected Condition[] condition;
	protected String[] selectedConditions;
	protected List conditions;

	public ConditionForm() {

		condition = new Condition[0];
		shortText = "";
		rte1 = "";
		tmpText = "";
		inspectable = "off";
		warning = "off";
		dpa = "off";
		complete = "off";

	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// firstName = null;
		// lastName = null;
		this.rte1 = null;
	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the conditionId
	 * 
	 * @return Returns a int
	 */
	public int getConditionId() {
		return conditionId;
	}

	/**
	 * Sets the conditionId
	 * 
	 * @param conditionId
	 *            The conditionId to set
	 */
	public void setConditionId(int aConditionId) {
		conditionId = aConditionId;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int aLevelId) {
		levelId = aLevelId;
	}

	/**
	 * Gets the conditionLevel
	 * 
	 * @return Returns a String
	 */
	public String getConditionLevel() {
		return conditionLevel;
	}

	/**
	 * Sets the conditionLevel
	 * 
	 * @param conditionLevel
	 *            The conditionLevel to set
	 */
	public void setConditionLevel(String aConditionLevel) {
		conditionLevel = aConditionLevel;
	}

	/**
	 * Gets the shortText
	 * 
	 * @return Returns a String
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the shortText
	 * 
	 * @param shortText
	 *            The shortText to set
	 */
	public void setShortText(String aShortText) {
		shortText = aShortText;
	}

	/**
	 * @return Returns the rte1.
	 */
	public String getRte1() {

		return rte1;
	}

	/**
	 * @param rte1
	 *            The rte1 to set.
	 */
	public void setRte1(String rte1) {
		this.rte1 = rte1;

	}

	/**
	 * Gets the inspectable
	 * 
	 * @return Returns a String
	 */
	public String getInspectable() {
		return inspectable;
	}

	/**
	 * Sets the inspectable
	 * 
	 * @param inspectable
	 *            The inspectable to set
	 */
	public void setInspectable(String aInspectable) {
		inspectable = aInspectable;
	}

	/**
	 * Gets the warning
	 * 
	 * @return Returns a String
	 */
	public String getWarning() {
		return warning;
	}

	/**
	 * Sets the warning
	 * 
	 * @param warning
	 *            The warning to set
	 */
	public void setWarning(String aWarning) {
		warning = aWarning;
	}

	/**
	 * Gets the dpa
	 * 
	 * @return Returns a String
	 */
	public String getDpa() {
		return dpa;
	}

	/**
	 * Sets the dpa
	 * 
	 * @param dpa
	 *            The dpa to set
	 */
	public void setDpa(String aDpa) {
		dpa = aDpa;
	}

	/**
	 * Gets the complete
	 * 
	 * @return Returns a String
	 */
	public String getComplete() {
		return complete;
	}

	/**
	 * Sets the complete
	 * 
	 * @param complete
	 *            The complete to set
	 */
	public void setComplete(String aComplete) {
		complete = aComplete;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a int
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the approvedBy
	 * 
	 * @return Returns a int
	 */
	public int getApprovedBy() {
		return approvedBy;
	}

	/**
	 * Sets the approvedBy
	 * 
	 * @param approvedBy
	 *            The approvedBy to set
	 */
	public void setApprovedBy(int approvedBy) {
		this.approvedBy = approvedBy;
	}

	/**
	 * Gets the createdDate
	 * 
	 * @return Returns a String
	 */
	public String getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the createdDate
	 * 
	 * @param createdDate
	 *            The createdDate to set
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * Gets the updatedDate
	 * 
	 * @return Returns a String
	 */
	public String getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Sets the updatedDate
	 * 
	 * @param updatedDate
	 *            The updatedDate to set
	 */
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	/**
	 * Gets the approvedDate
	 * 
	 * @return Returns a String
	 */
	public String getApprovedDate() {
		return approvedDate;
	}

	/**
	 * Sets the approvedDate
	 * 
	 * @param approvedDate
	 *            The approvedDate to set
	 */
	public void setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
	}

	/**
	 * Gets the condition
	 * 
	 * @return Returns a Condition[]
	 */
	public Condition[] getCondition() {
		return condition;
	}

	/**
	 * Sets the condition
	 * 
	 * @param condition
	 *            The condition to set
	 */
	public void setCondition(Condition[] condition) {
		this.condition = condition;
	}

	/**
	 * Gets the tmpText
	 * 
	 * @return Returns a String
	 */
	public String getTmpText() {
		return tmpText;
	}

	/**
	 * Sets the tmpText
	 * 
	 * @param tmpText
	 *            The tmpText to set
	 */
	public void setTmpText(String tmpText) {
		this.tmpText = tmpText;
	}

	/**
	 * Gets the libraryId
	 * 
	 * @return Returns a int
	 */
	public int getLibraryId() {
		return libraryId;
	}

	/**
	 * Sets the libraryId
	 * 
	 * @param libraryId
	 *            The libraryId to set
	 */
	public void setLibraryId(int libraryId) {
		this.libraryId = libraryId;
	}

	/**
	 * @return
	 */
	public String[] getSelectedConditions() {
		return selectedConditions;
	}

	/**
	 * @param strings
	 */
	public void setSelectedConditions(String[] strings) {
		selectedConditions = strings;
	}

	/**
	 * @return
	 */
	public List getConditions() {
		return conditions;
	}

	/**
	 * @param list
	 */
	public void setConditions(List list) {
		conditions = list;
	}

	/**
	 * @return
	 */
	public String getUpdatedByName() {
		return updatedByName;
	}

	/**
	 * @param string
	 */
	public void setUpdatedByName(String string) {
		updatedByName = string;
	}

	/**
	 * @return
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * @param string
	 */
	public void setConditionCode(String string) {
		conditionCode = string;
	}

	/**
	 * @return Returns the text.
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            The text to set.
	 */
	public void setText(String text) {
		this.text = text;
	}
}
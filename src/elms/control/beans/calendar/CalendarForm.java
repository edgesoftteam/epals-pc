/*
 * Created on Jul 14, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.beans.calendar;



import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import elms.app.Calendar.Calendar;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

/**
 * @author aman
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CalendarForm extends ActionForm {
	
	protected String startDate;
	protected String endDate;
	protected String description;
	protected Calendar[] calendar;
	protected int officerId;
	protected String inspectorName;
	private int hour;

	public void reset(ActionMapping mapping, HttpServletRequest request){
		System.out.println("calling reset");
		if(calendar != null){
			startDate = "";
			endDate = "";
			description = "";
			officerId = 0;
			inspectorName = "";
			hour = 9;
		for(int i=0;i<calendar.length;i++){
			calendar[i].setDescription("");
			calendar[i].setStartDate("");
			
		}
		}
		
	}
	
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
	/**
	 * @return Returns the endDate.
	 */
	public String getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate The endDate to set.
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return Returns the startDate.
	 */
	public String getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate The startDate to set.
	 */
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * @return Returns the calendar.
	 */
	public Calendar[] getCalendar() {
		return calendar;
	}
	/**
	 * @param calendar The calendar to set.
	 */
	public void setCalendar(Calendar[] calendar) {
		this.calendar = calendar;
	}
	/**
	 * @return Returns the officerId.
	 */
	public int getOfficerId() {
		return officerId;
	}
	/**
	 * @param officerId The officerId to set.
	 */
	public void setOfficerId(int officerId) {
		this.officerId = officerId;
	}
	
	
	/**
	 * @return Returns the inspectorName.
	 */
	public String getInspectorName() {
		return inspectorName;
	}
	/**
	 * @param inspectorName The inspectorName to set.
	 */
	public void setInspectorName(String inspectorName) {
		this.inspectorName = inspectorName;
	}
	

	/**
	 * @return Returns the hour.
	 */
	public int getHour() {
		return hour;
	}
	/**
	 * @param hour The hour to set.
	 */
	public void setHour(int hour) {
		this.hour = hour;
	}
}

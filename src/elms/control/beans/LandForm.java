package elms.control.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import elms.app.admin.lso.AddressRangeEdit;

public class LandForm extends LsoForm {

	static Logger logger = Logger.getLogger(LandForm.class.getName());

	/**
	 * Member variable declaration
	 */

	protected String lsoId;
	protected String ownerId;
	protected String description;
	protected String coordinateY;
	protected String coordinateX;
	protected boolean active = false;
	protected String alias;
	protected AddressRangeEdit[] addressRangeList;
	protected String apn;
	protected String oldApn;
	protected String apnOwnerName;
	protected String apnStreetNumber;
	protected String apnStreetModifier;
	protected String apnStreetName;
	protected String apnCity;
	protected String apnState;
	protected String apnZip;
	protected String apnForeignAddress = "N";
	protected String apnForeignAddress1;
	protected String apnForeignAddress2;
	protected String apnForeignAddress3;
	protected String apnForeignAddress4;
	protected String apnCountry;
	protected String apnEmail;
	protected String apnPhone;
	protected String apnFax;
	protected String parkingZone;
	protected String resZone;
	protected String label;

	protected String[] selectedZone = {};

	protected String[] selectedUse = {};

	public void reset(ActionMapping mapping, HttpServletRequest request) {

		// reset the declaration values...or rather set it to something default.
		// this.firstName = null;
		// this.lastName = null;

		lsoId = null;
		ownerId = null;
		addressCity = null;
		description = null;
		coordinateY = null;
		coordinateX = null;
		resZone = null;
		active = false;
		alias = null;

		addressStreetNumber = null;
		addressStreetModifier = null;
		addressStreetName = null;
		addressState = null;
		addressUnit = null;
		addressZip = null;
		addressZip4 = null;
		addressPrimary = false;
		addressActive = false;
		addressDescription = null;

		apn = null;
		apnOwnerName = null;
		apnStreetNumber = null;
		apnStreetModifier = null;
		apnStreetName = null;
		apnCity = null;
		apnState = null;
		apnZip = null;
		apnForeignAddress = "N";
		apnForeignAddress1 = null;
		apnForeignAddress2 = null;
		apnForeignAddress3 = null;
		apnForeignAddress4 = null;
		apnCountry = null;
		apnEmail = null;
		apnPhone = null;
		apnFax = null;

		selectedZone = null;
		selectedUse = null;

	}

	public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();

		// delcare validations for the form
		/*
		 * if ((firstName == null) || (firstName.length() < 1)) errors.add("firstName",new ActionError("error.firstname.required"));
		 * 
		 * if ((lastName == null) || (lastName.length() < 1)) errors.add("lastName",new ActionError("error.lastname.required"));
		 */
		return errors;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a String
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the ownerId
	 * 
	 * @return Returns a String
	 */
	public String getOwnerId() {
		return ownerId;
	}

	/**
	 * Sets the ownerId
	 * 
	 * @param ownerId
	 *            The ownerId to set
	 */
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Gets the addressCity
	 * 
	 * @return Returns a String
	 */
	public String getAddressCity() {
		return addressCity;
	}

	/**
	 * Sets the addressCity
	 * 
	 * @param addressCity
	 *            The addressCity to set
	 */
	public void setAddressCity(String addressCity) {
		this.addressCity = addressCity;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the coordinateY
	 * 
	 * @return Returns a String
	 */
	public String getCoordinateY() {
		return coordinateY;
	}

	/**
	 * Sets the coordinateY
	 * 
	 * @param coordinateY
	 *            The coordinateY to set
	 */
	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	/**
	 * Gets the coordinateX
	 * 
	 * @return Returns a String
	 */
	public String getCoordinateX() {
		return coordinateX;
	}

	/**
	 * Sets the coordinateX
	 * 
	 * @param coordinateX
	 *            The coordinateX to set
	 */
	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	/**
	 * Gets the active
	 * 
	 * @return Returns a boolean
	 */
	public boolean getActive() {
		return active;
	}

	/**
	 * Sets the active
	 * 
	 * @param active
	 *            The active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Gets the alias
	 * 
	 * @return Returns a String
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Sets the alias
	 * 
	 * @param alias
	 *            The alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Gets the apn
	 * 
	 * @return Returns a String
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * Sets the apn
	 * 
	 * @param apn
	 *            The apn to set
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * Gets the apnOwnerName
	 * 
	 * @return Returns a String
	 */
	public String getApnOwnerName() {
		return apnOwnerName;
	}

	/**
	 * Sets the apnOwnerName
	 * 
	 * @param apnOwnerName
	 *            The apnOwnerName to set
	 */
	public void setApnOwnerName(String apnOwnerName) {
		this.apnOwnerName = apnOwnerName;
	}

	/**
	 * Gets the apnStreetNumber
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetNumber() {
		return apnStreetNumber;
	}

	/**
	 * Sets the apnStreetNumber
	 * 
	 * @param apnStreetNumber
	 *            The apnStreetNumber to set
	 */
	public void setApnStreetNumber(String apnStreetNumber) {
		this.apnStreetNumber = apnStreetNumber;
	}

	/**
	 * Gets the apnStreetModifier
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetModifier() {
		return apnStreetModifier;
	}

	/**
	 * Sets the apnStreetModifier
	 * 
	 * @param apnStreetModifier
	 *            The apnStreetModifier to set
	 */
	public void setApnStreetModifier(String apnStreetModifier) {
		this.apnStreetModifier = apnStreetModifier;
	}

	/**
	 * Gets the apnStreetName
	 * 
	 * @return Returns a String
	 */
	public String getApnStreetName() {
		return apnStreetName;
	}

	/**
	 * Sets the apnStreetName
	 * 
	 * @param apnStreetName
	 *            The apnStreetName to set
	 */
	public void setApnStreetName(String apnStreetName) {
		this.apnStreetName = apnStreetName;
	}

	/**
	 * Gets the apnCity
	 * 
	 * @return Returns a String
	 */
	public String getApnCity() {
		return apnCity;
	}

	/**
	 * Sets the apnCity
	 * 
	 * @param apnCity
	 *            The apnCity to set
	 */
	public void setApnCity(String apnCity) {
		this.apnCity = apnCity;
	}

	/**
	 * Gets the apnState
	 * 
	 * @return Returns a String
	 */
	public String getApnState() {
		return apnState;
	}

	/**
	 * Sets the apnState
	 * 
	 * @param apnState
	 *            The apnState to set
	 */
	public void setApnState(String apnState) {
		this.apnState = apnState;
	}

	/**
	 * Gets the apnZip
	 * 
	 * @return Returns a String
	 */
	public String getApnZip() {
		return apnZip;
	}

	/**
	 * Sets the apnZip
	 * 
	 * @param apnZip
	 *            The apnZip to set
	 */
	public void setApnZip(String apnZip) {
		this.apnZip = apnZip;
	}

	/**
	 * Gets the apnForeignAddress
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress() {
		return apnForeignAddress;
	}

	/**
	 * Sets the apnForeignAddress
	 * 
	 * @param apnForeignAddress
	 *            The apnForeignAddress to set
	 */
	public void setApnForeignAddress(String apnForeignAddress) {
		this.apnForeignAddress = apnForeignAddress;
	}

	/**
	 * Gets the apnForeignAddress1
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress1() {
		return apnForeignAddress1;
	}

	/**
	 * Sets the apnForeignAddress1
	 * 
	 * @param apnForeignAddress1
	 *            The apnForeignAddress1 to set
	 */
	public void setApnForeignAddress1(String apnForeignAddress1) {
		this.apnForeignAddress1 = apnForeignAddress1;
	}

	/**
	 * Gets the apnForeignAddress2
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress2() {
		return apnForeignAddress2;
	}

	/**
	 * Sets the apnForeignAddress2
	 * 
	 * @param apnForeignAddress2
	 *            The apnForeignAddress2 to set
	 */
	public void setApnForeignAddress2(String apnForeignAddress2) {
		this.apnForeignAddress2 = apnForeignAddress2;
	}

	/**
	 * Gets the apnForeignAddress3
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress3() {
		return apnForeignAddress3;
	}

	/**
	 * Sets the apnForeignAddress3
	 * 
	 * @param apnForeignAddress3
	 *            The apnForeignAddress3 to set
	 */
	public void setApnForeignAddress3(String apnForeignAddress3) {
		this.apnForeignAddress3 = apnForeignAddress3;
	}

	/**
	 * Gets the apnForeignAddress4
	 * 
	 * @return Returns a String
	 */
	public String getApnForeignAddress4() {
		return apnForeignAddress4;
	}

	/**
	 * Sets the apnForeignAddress4
	 * 
	 * @param apnForeignAddress4
	 *            The apnForeignAddress4 to set
	 */
	public void setApnForeignAddress4(String apnForeignAddress4) {
		this.apnForeignAddress4 = apnForeignAddress4;
	}

	/**
	 * Gets the apnCountry
	 * 
	 * @return Returns a String
	 */
	public String getApnCountry() {
		return apnCountry;
	}

	/**
	 * Sets the apnCountry
	 * 
	 * @param apnCountry
	 *            The apnCountry to set
	 */
	public void setApnCountry(String apnCountry) {
		this.apnCountry = apnCountry;
	}

	/**
	 * Gets the apnEmail
	 * 
	 * @return Returns a String
	 */
	public String getApnEmail() {
		return apnEmail;
	}

	/**
	 * Sets the apnEmail
	 * 
	 * @param apnEmail
	 *            The apnEmail to set
	 */
	public void setApnEmail(String apnEmail) {
		this.apnEmail = apnEmail;
	}

	/**
	 * Gets the apnPhone
	 * 
	 * @return Returns a String
	 */
	public String getApnPhone() {
		return apnPhone;
	}

	/**
	 * Sets the apnPhone
	 * 
	 * @param apnPhone
	 *            The apnPhone to set
	 */
	public void setApnPhone(String apnPhone) {
		this.apnPhone = apnPhone;
	}

	/**
	 * Gets the apnFax
	 * 
	 * @return Returns a String
	 */
	public String getApnFax() {
		return apnFax;
	}

	/**
	 * Sets the apnFax
	 * 
	 * @param apnFax
	 *            The apnFax to set
	 */
	public void setApnFax(String apnFax) {
		this.apnFax = apnFax;
	}

	/**
	 * Gets the selectedZone
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedZone() {
		return selectedZone;
	}

	/**
	 * Sets the selectedZone
	 * 
	 * @param selectedZone
	 *            The selectedZone to set
	 */
	public void setSelectedZone(String[] selectedZone) {
		this.selectedZone = selectedZone;
	}

	/**
	 * Gets the selectedUse
	 * 
	 * @return Returns a String[]
	 */
	public String[] getSelectedUse() {
		return selectedUse;
	}

	/**
	 * Sets the selectedUse
	 * 
	 * @param selectedUse
	 *            The selectedUse to set
	 */
	public void setSelectedUse(String[] selectedUse) {
		this.selectedUse = selectedUse;
	}

	/**
	 * Returns the active.
	 * 
	 * @return boolean
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * Returns the addressRangeList.
	 * 
	 * @return AddressRangeEdit[]
	 */
	public AddressRangeEdit[] getAddressRangeList() {
		return addressRangeList;
	}

	/**
	 * Sets the addressRangeList.
	 * 
	 * @param addressRangeList
	 *            The addressRangeList to set
	 */
	public void setAddressRangeList(AddressRangeEdit[] addressRangeList) {
		this.addressRangeList = addressRangeList;
	}

	public void setAddressRangeList(List addressRangeList) {
		if (addressRangeList == null)
			addressRangeList = new ArrayList();
		AddressRangeEdit[] editArray = new AddressRangeEdit[addressRangeList.size()];
		Iterator iter = addressRangeList.iterator();
		int i = 0;
		while (iter.hasNext()) {
			editArray[i++] = (AddressRangeEdit) iter.next();
		}
		this.addressRangeList = editArray;
	}

	/**
	 * Returns the parkingZone.
	 * 
	 * @return String
	 */
	public String getParkingZone() {
		return parkingZone;
	}

	/**
	 * Returns the resZone.
	 * 
	 * @return String
	 */
	public String getResZone() {
		return resZone;
	}

	/**
	 * Sets the parkingZone.
	 * 
	 * @param parkingZone
	 *            The parkingZone to set
	 */
	public void setParkingZone(String parkingZone) {
		this.parkingZone = parkingZone;
	}

	/**
	 * Sets the resZone.
	 * 
	 * @param resZone
	 *            The resZone to set
	 */
	public void setResZone(String resZone) {
		this.resZone = resZone;
	}

	/**
	 * @return
	 */
	public String getOldApn() {
		return oldApn;
	}

	/**
	 * @param string
	 */
	public void setOldApn(String string) {
		oldApn = string;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

} // End class
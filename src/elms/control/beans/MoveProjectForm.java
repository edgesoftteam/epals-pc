//Source file: C:\\Datafile\\OBCSource\\elms\\control\\beans\\EditHoldForm.java

package elms.control.beans;

import org.apache.struts.action.ActionForm;

/**
 * @author Shekhar Jain
 */
public class MoveProjectForm extends ActionForm {
	protected String pprojectno;
	protected String plsono;
	protected String ssubprojectno;
	protected String sprojectno;
	protected String aactivityno;
	protected String asubprojectno;

	/**
	 * Gets the pprojectno
	 * 
	 * @return Returns a String
	 */
	public String getPprojectno() {
		return pprojectno;
	}

	/**
	 * Sets the pprojectno
	 * 
	 * @param pprojectno
	 *            The pprojectno to set
	 */
	public void setPprojectno(String pprojectno) {
		this.pprojectno = pprojectno;
	}

	/**
	 * Gets the plsono
	 * 
	 * @return Returns a String
	 */
	public String getPlsono() {
		return plsono;
	}

	/**
	 * Sets the plsono
	 * 
	 * @param plsono
	 *            The plsono to set
	 */
	public void setPlsono(String plsono) {
		this.plsono = plsono;
	}

	/**
	 * Gets the ssubprojectno
	 * 
	 * @return Returns a String
	 */
	public String getSsubprojectno() {
		return ssubprojectno;
	}

	/**
	 * Sets the ssubprojectno
	 * 
	 * @param ssubprojectno
	 *            The ssubprojectno to set
	 */
	public void setSsubprojectno(String ssubprojectno) {
		this.ssubprojectno = ssubprojectno;
	}

	/**
	 * Gets the sprojectno
	 * 
	 * @return Returns a String
	 */
	public String getSprojectno() {
		return sprojectno;
	}

	/**
	 * Sets the sprojectno
	 * 
	 * @param sprojectno
	 *            The sprojectno to set
	 */
	public void setSprojectno(String sprojectno) {
		this.sprojectno = sprojectno;
	}

	/**
	 * Gets the aactivityno
	 * 
	 * @return Returns a String
	 */
	public String getAactivityno() {
		return aactivityno;
	}

	/**
	 * Sets the aactivityno
	 * 
	 * @param aactivityno
	 *            The aactivityno to set
	 */
	public void setAactivityno(String aactivityno) {
		this.aactivityno = aactivityno;
	}

	/**
	 * Gets the asubprojectno
	 * 
	 * @return Returns a String
	 */
	public String getAsubprojectno() {
		return asubprojectno;
	}

	/**
	 * Sets the asubprojectno
	 * 
	 * @param asubprojectno
	 *            The asubprojectno to set
	 */
	public void setAsubprojectno(String asubprojectno) {
		this.asubprojectno = asubprojectno;
	}

	/**
	 * Returns the activity.
	 * 
	 * @return Activity
	 */

}

package elms.control.actions.online.inspections;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.HolidayEditorForm;
import elms.util.StringUtils;
import sun.jdbc.rowset.CachedRowSet;

public class HolidayEditorAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(HolidayEditorAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("action")!=null?request.getParameter("action"):request.getParameter("action");
		if(action == null) {
			action="list";
		}
		String occurance ="N"; 
		occurance = StringUtils.nullReplaceWithBlank((String)request.getAttribute("occurance"));
		if(occurance != null) {
			occurance = request.getParameter("occurance");
		}
		logger.debug("occurance... "+occurance);
		request.setAttribute("occurance", occurance);
		logger.debug("action.. "+action);
		try{
			if(action.equalsIgnoreCase("list")){
				logger.debug("holiday list selected");		
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();

				List<String> yearList = new ArrayList<String>();
				while(holidayList.next()) {
					Date date = holidayList.getDate("insp_cal_date");
					SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");
					 String currentYear = formatNowYear.format(date);
					 if(! yearList.contains(currentYear)) {
						 yearList.add(currentYear);
					 }
				}
				logger.debug("year.."+yearList.toString());
				logger.debug("year.."+Collections.max(yearList));

				holidayList = (CachedRowSet) new InspectionAgent().getHolidayListBasedOnYear(Collections.max(yearList,null).toString());
				logger.debug("obtained holiday list of size "+holidayList.size());
				request.setAttribute("holidayList", holidayList);
				request.setAttribute("yearList", yearList);
				return (mapping.findForward("list"));
			}else if(action.equalsIgnoreCase("ajaxList")){

				String year = request.getParameter("year")!=null?request.getParameter("year"):request.getParameter("year");
				logger.debug("year... "+year);
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();

				List<String> yearList = new ArrayList<String>();
				while(holidayList.next()) {
					Date date = holidayList.getDate("insp_cal_date");
					SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");
					 String currentYear = formatNowYear.format(date);
					 if(! yearList.contains(currentYear)) {
						 yearList.add(currentYear);
					 }
				}
				logger.debug("year.."+yearList.toString());
				logger.debug("year.."+Collections.max(yearList));

				holidayList = (CachedRowSet) new InspectionAgent().getHolidayListBasedOnYear(year);
				logger.debug("obtained holiday list of size "+holidayList.size());
				PrintWriter pw = response.getWriter();
				
				StringBuilder sb = new StringBuilder();
				sb.append("<TABLE id=\"indextableFromAjax\" background=\"../../images/site_bg_B7C1CB.jpg\" width=\"55%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">" );
				sb.append("<thead>" );
				sb.append("<tr>" );
				sb.append("<th class=\"tablelabel\" width=\"25%\" align=\"left\"><a href=\"javascript:SortTableDynamically(0,'D','mdy');\">Date</a></th>" ); 
				sb.append("<th class=\"tablelabel\" width=\"65%\" align=\"left\"><a href=\"javascript:SortTableDynamically(1,'T');\">Description</a></th>" ); 
				sb.append("<th class=\"tablelabel\" width=\"15%\" >Delete</th>" );
				sb.append("</tr>" );
				sb.append("</thead>"); 
				sb.append("<tbody>");
				while(holidayList.next()) {
					logger.debug("holiday list.. "+holidayList.getDate("insp_cal_date"));
					sb.append("<tr valign=\"top\">");
					sb.append("<TD class=\"tabletext\" width=\"25%\"><a  href=\"javascript:updateHoliday("+holidayList.getInt("insp_cal_id")+");\"> "+StringUtils.date2str(holidayList.getDate("insp_cal_date"))+"</a></td>");
					sb.append("<td class=\"tabletext\" width=\"65%\">"+holidayList.getString("description")+" </td>");
					sb.append("<td class=\"tabletext\" width=\"15%\" align=\"center\"><a href=\"javascript:deleteHoliday("+holidayList.getInt("insp_cal_id")+");\"><img src=\"../../images/delete.gif\" alt=\"Delete\" border=\"0\"></a></td>");
					sb.append("</tr>");
				}
				sb.append("</tbody></table>");
				pw.write(sb.toString());
				return null;
			}
			else if(action.equalsIgnoreCase("add")){
				logger.debug("Add action requested");
				return (mapping.findForward("add"));
			}
			else if(action.equalsIgnoreCase("update")){
				logger.debug("Add action requested");
				String holidayId = request.getParameter("holidayId");
				HolidayEditorForm holidayEditorForm = (HolidayEditorForm)form;
				holidayEditorForm = InspectionAgent.getHolidayList(holidayId);
				request.setAttribute("action", action);
				request.setAttribute("holidayEditorForm", holidayEditorForm);
				return (mapping.findForward("add"));
			}
			else if(action.equalsIgnoreCase("delete")){
				logger.debug("Delete action requested");
				String holidayId = request.getParameter("holidayId");
				logger.debug("holiday id is "+holidayId);
				String year = new InspectionAgent().getYearOnId(holidayId);
				new OnlineAgent().deleteHoliday(holidayId);
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();

				List<String> yearList = new ArrayList<String>();
				while(holidayList.next()) {
					Date date = holidayList.getDate("insp_cal_date");
					SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");
					 String currentYear = formatNowYear.format(date);
					 if(! yearList.contains(currentYear)) {
						 yearList.add(currentYear);
					 }
				}
				logger.debug("year.."+yearList.toString());
				logger.debug("year.."+Collections.max(yearList));
				if(year != null && yearList.contains(year)) {
					holidayList = (CachedRowSet) new InspectionAgent().getHolidayListBasedOnYear(year);					
				}else {
					holidayList = (CachedRowSet) new InspectionAgent().getHolidayListBasedOnYear(Collections.max(yearList,null).toString());
				}

				request.setAttribute("holidayList", holidayList);
				request.setAttribute("year", year);
				logger.debug("obtained holiday list of size "+holidayList.size());
				request.setAttribute("yearList", yearList);
				request.setAttribute("displayMsg", "Holiday deleted successfully");
				return (mapping.findForward("list"));
			}
			else if(action.equalsIgnoreCase("save")){
				logger.debug("Save action requested");
				HolidayEditorForm holidayEditorForm = (HolidayEditorForm)form;
				String displayMsg="";
				int id=0;
				String year="";
				if(StringUtils.s2i(holidayEditorForm.getId()) > 0) {
					id = new OnlineAgent().updateHoliday(holidayEditorForm);
					displayMsg = "Holiday updated successfully"; 
					year = new InspectionAgent().getYearOnId(holidayEditorForm.getId());
				}else {
					id = new OnlineAgent().saveHoliday(holidayEditorForm); 
					logger.debug("id... "+id);
					if(id > 0) {
						displayMsg = "Holiday already exists for the selected date";
						request.setAttribute("displayMsg", displayMsg);
						return (mapping.findForward("add"));
					}else {
						displayMsg = "Holiday added successfully";
					}
					
				}
				logger.debug("displaymsg.."+displayMsg);
				CachedRowSet holidayList = (CachedRowSet) new InspectionAgent().getHolidayList();

				List<String> yearList = new ArrayList<String>();
				while(holidayList.next()) {
					Date date = holidayList.getDate("insp_cal_date");
					SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");
					 String currentYear = formatNowYear.format(date);
					 if(! yearList.contains(currentYear)) {
						 yearList.add(currentYear);
					 }
				}
				logger.debug("year.."+yearList.toString());
				logger.debug("year.."+Collections.max(yearList));

				//get the list to display
				if(year == "") {
					holidayList = (CachedRowSet) new InspectionAgent().getHolidayListBasedOnYear(Collections.max(yearList,null).toString());	
				}else {
					holidayList = (CachedRowSet) new InspectionAgent().getHolidayListBasedOnYear(year);
				}
				request.setAttribute("year", year);
				request.setAttribute("yearList", yearList);
				request.setAttribute("holidayList", holidayList);
				request.setAttribute("displayMsg", displayMsg);
				logger.debug("obtained holiday list of size "+holidayList.size());
				return (mapping.findForward("list"));
			}
			else{
				logger.error("no mapping defined");
				return (mapping.findForward("error"));
			}	
		}
		catch(Exception e){
			logger.error("Exception happenned while saving holidays "+e.getMessage()+e);
			return (mapping.findForward("error"));
		}
	}
}

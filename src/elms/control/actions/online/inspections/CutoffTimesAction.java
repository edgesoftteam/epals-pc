package elms.control.actions.online.inspections;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.control.beans.online.CutoffTimesForm;

public class CutoffTimesAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(CutoffTimesAction.class.getName());
	String nextPage = "";
	String message = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("action")!=null?request.getParameter("action"):"view";
		logger.debug("action is "+action);
		
		try{
			CutoffTimesForm cutoffTimesForm = (CutoffTimesForm) form;
			OnlineAgent onlineAgent = new OnlineAgent();
			
			if(action.equalsIgnoreCase("view")){
				cutoffTimesForm = new CutoffTimesForm();
				onlineAgent.getCutoffTimes(cutoffTimesForm);
				message = "";
				
			}
			else if(action.equalsIgnoreCase("save")){
				onlineAgent.saveCutoffTimes(cutoffTimesForm);
				logger.debug("saved the cutoff times information successfully");
				message = "Cutoff Time information saved successfully";

			}
			else{
				logger.error("no mapping defined");
			}
			
			request.setAttribute("message",message);
			request.setAttribute("cutoffTimesForm",cutoffTimesForm);
			return (mapping.findForward("success"));
		}
		catch(Exception e){
			logger.error("Exception occured while setting cutoff timings "+e.getMessage());
			return (mapping.findForward("error"));
		}

		
	}
}

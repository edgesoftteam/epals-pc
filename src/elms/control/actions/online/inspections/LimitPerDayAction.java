package elms.control.actions.online.inspections;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.agent.OnlineAgent;
import elms.control.beans.online.LimitPerDayForm;

public class LimitPerDayAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(LimitPerDayAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String action = request.getParameter("action")!=null?request.getParameter("action"):"view";
		logger.debug("action is "+action);
		int limitPerDay = 0;
		
		try{
			LimitPerDayForm limitPerDayForm = (LimitPerDayForm) form;
			OnlineAgent onlineAgent = new OnlineAgent();
			
			Calendar cal = new GregorianCalendar();
			String inspectionDate = new InspectionAgent().getNextInspectionDate(cal);
			int maxLimitScheduled = onlineAgent.getMaxInspectionScheduledPerDay(inspectionDate);
			//request.setAttribute("currentSchedule",maxLimitScheduled+"");
			limitPerDayForm.setCurrentLimit(maxLimitScheduled);
			
			if(action.equalsIgnoreCase("view")){
				limitPerDay = onlineAgent.getLimitPerDay();
			}
			else if(action.equalsIgnoreCase("save")){
				limitPerDay = limitPerDayForm.getLimitPerDay();
				logger.debug("limitPerDay is "+limitPerDay);
				onlineAgent.saveLimitPerDay(limitPerDay);
				logger.debug("saved the limit per day information successfully");

			}
			else if(action.equalsIgnoreCase("true")){
				limitPerDay = onlineAgent.getLimitPerDay();
			request.getSession().setAttribute("enableAutoRefresh","enableAutoRefresh");
			
			}
			else{
				logger.error("no mapping defined");
			}
			
			limitPerDayForm.setLimitPerDay(limitPerDay);
			return (mapping.findForward("success"));
		}
		catch(Exception e){
			logger.error("Exception occured while setting limit per day "+e.getMessage());
			return (mapping.findForward("error"));
		}

		
	}
}

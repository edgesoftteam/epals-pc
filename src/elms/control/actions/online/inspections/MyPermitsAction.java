package elms.control.actions.online.inspections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.FinanceAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.agent.OnlineAgent;
import elms.app.admin.ActivityStatus;
import elms.app.admin.MicrofilmStatus;
import elms.app.finance.FinanceSummary;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.actions.finance.ViewPaymentMgrAction;
import elms.security.User;
import elms.util.StringUtils;

public class MyPermitsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(MyPermitsAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		String viewPermit = (request.getParameter("viewPermit") != null) ? request.getParameter("viewPermit") : "No";
		String action = (request.getParameter("action") != null) ? request.getParameter("action") : "view";
		String ref = (request.getParameter("ref") != null) ? request.getParameter("ref") : "";
		String inActive = (request.getParameter("inActive") != null) ? request.getParameter("inActive") : "No";
		
		logger.debug("action is " + action);
		logger.debug("inActive is " + inActive);
		InspectionAgent insAgent = new InspectionAgent();
		HttpSession session = request.getSession();
		List actList = new ArrayList();
		List comboList = new ArrayList();
		List dotList = new ArrayList();
		try {
			// for Hide/Unhide changes showing up Combo Name and its respective Activities
			if (viewPermit.equalsIgnoreCase("Yes")) {
				String isObc = request.getParameter("isObc");
				String isDot = request.getParameter("isDot");
				if (action.equalsIgnoreCase("view")) {
					if (isObc.equals("Y")) {
						comboList = insAgent.getComboList(((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername(), inActive);
						request.setAttribute("comboSize", StringUtils.i2s(comboList.size()));

					}
					if (isDot.equals("Y")) {
						// request.setAttribute("comboSize","");
						dotList = insAgent.getDotList(((User) session.getAttribute(elms.common.Constants.USER_KEY)).getAccountNbr(), action);
						request.setAttribute("dotListSize", StringUtils.i2s(dotList.size()));
					}
					request.setAttribute("dotList", dotList);
					request.setAttribute("comboList", comboList);
					request.setAttribute("inActive", inActive);
					session.setAttribute("online", "Y");
					return (mapping.findForward("success"));
				} else if (action.equalsIgnoreCase("explore")) {
					String activityId = request.getParameter("activityId");
					String activityType = request.getParameter("activityType");
					Activity act = new Activity();
					act.setActivityId(StringUtils.s2i(activityId));
					getActivity(request, session, act);
					return (mapping.findForward("view"));
				} else if (action.equalsIgnoreCase("pay")) {
					String router = (request.getParameter("action") != null) ? request.getParameter("action") : "Empty";
					router = "payment";
					String sprojId = request.getParameter("sprojId");
					// Checks if it is L S or O to identify the request is coming from which level
					request.setAttribute("level", "Q");
					request.setAttribute("levelId", sprojId);

					session.setAttribute("level", "Q");
					session.setAttribute("levelId", sprojId);

					if ("payment".equalsIgnoreCase(router)) {
						new ViewPaymentMgrAction().perform(mapping, form, request, response);
						// processPayment();
					}
					request.setAttribute("forcePC", new OnlineAgent().checkPlanCheck(sprojId));
					return (mapping.findForward("successQ"));
				}
				return null;
				// end of Hide/Unhide changes
			} else { // Else part Wont be used Left uncommented.

				if (action.equalsIgnoreCase("view")) {

					actList = insAgent.getMyPermits(((User) session.getAttribute(elms.common.Constants.USER_KEY)).getUsername(), action);
					session.setAttribute("actList", actList);

					return (mapping.findForward("success"));
				} else if (action.equalsIgnoreCase("explore")) {
					int ref1 = StringUtils.s2i(ref);
					logger.debug("In Explore method");

					actList = insAgent.getMyPermits(((User) session.getAttribute(elms.common.Constants.USER_KEY)).getFirstName(), action);
					session.setAttribute("actList", actList);
					logger.debug("Act List" + actList.size());

					// Activity act = (Activity) ((List) session.getAttribute("actList")).get(ref1);
					Activity act = (Activity) ((List) actList).get(ref1);
					logger.debug("1 Act 3 rd record" + act.getActivityId());

					getActivity(request, session, act);

					return (mapping.findForward("view"));
				}
				// Payment start

				else if (action.equalsIgnoreCase("pay")) {

					String router = (request.getParameter("action") != null) ? request.getParameter("action") : "Empty";
					router = "payment";
					String sprojId = request.getParameter("sprojId");

					logger.debug("sprojId ID ------------------" + sprojId);
					// Checks if it is L S or O to identify the request is coming from which level
					request.setAttribute("level", "Q");
					request.setAttribute("levelId", sprojId);

					session.setAttribute("level", "Q");
					session.setAttribute("levelId", sprojId);

					if ("payment".equalsIgnoreCase(router)) {
						new ViewPaymentMgrAction().perform(mapping, form, request, response);
						// processPayment();
					}

					nextPage = "successQ";
				}
				// Payment End
				return (mapping.findForward(nextPage));
			}
		} catch (Exception e) {
			logger.error("Exception occured while setting limit per day " + e.getMessage());

			return (mapping.findForward("error"));
		}
	}

	public void getActivity(HttpServletRequest request, HttpSession session, Activity act) throws Exception {
		try {
			int activityId = act.getActivityId();
			// String actType = act.getActivityDetail().getActivityType().getType(); # Variable actType is not used anywhere.

			Activity activity = null;
			ActivityDetail activityDetail = null;
			boolean editable = false;
			String lsoAddress = null;
			ActivityAgent activityAgent = new ActivityAgent();
			lsoAddress = activityAgent.getActivityAddressForId(activityId);
			request.setAttribute("lsoAddress", lsoAddress);
			logger.debug("Set the lso address to session as " + lsoAddress);
			activity = new ActivityAgent().getActivity(activityId);

			if (activity == null) {
				logger.debug("Null Activity Object is created with activityId .." + activityId);
			} else {
				activityDetail = activity.getActivityDetail();
			}

			if (activityDetail == null) {
				logger.debug("Null ActivityDetail  Object is created from activity  Object ..");
			} else {
				request.setAttribute("activityId", StringUtils.i2s(activity.getActivityId()));
				logger.debug("activityId:" + request.getAttribute("activityId"));
				request.setAttribute("subProjectId", StringUtils.i2s(activity.getSubProjectId()));
				logger.debug("subprojectId:" + request.getAttribute("subProjectId"));
				request.setAttribute("activityNumber", activityDetail.getActivityNumber());

				String psaInfo = " -- " + activityDetail.getActivityNumber();
				String actNumber = activityDetail.getActivityNumber();
				logger.debug("activityNumber:" + request.getAttribute("activityNumber"));

				if (activityDetail.getActivityType() != null) {
					request.setAttribute("activityType", activityDetail.getActivityType().getType());
					request.setAttribute("activityTypeDesc", activityDetail.getActivityType().getDescription());
					psaInfo = psaInfo + " - " + activityDetail.getActivityType().getDescription();
					logger.debug("activityType:" + request.getAttribute("activityType"));
					request.setAttribute("completionDateLabel", activityDetail.getFinalDateLabel());
					logger.debug("Completion Date Label is set to request  as  " + activityDetail.getFinalDateLabel());
					request.setAttribute("expirationDateLabel", activityDetail.getExpirationDateLabel());
					logger.debug("Expiration Date Label is set to request  as  " + activityDetail.getFinalDateLabel());
				}

				if (activityDetail.getActivitySubTypes() != null) {
					logger.debug("entering activityDetail.getActivitySubType" + activityDetail.getActivitySubTypes());
					request.setAttribute("activitySubTypes", activityDetail.getActivitySubTypes());
				}

				session.setAttribute("psaInfo", psaInfo);
				session.setAttribute("actNumber", actNumber);
				request.setAttribute("description", activityDetail.getDescription());
				logger.debug("description:" + request.getAttribute("description"));

				if (activityDetail.getAddress() == null) {
					request.setAttribute("address", "");
				} else {
					request.setAttribute("address", activityDetail.getAddress());
				}

				logger.debug("address:" + request.getAttribute("address"));

				// setting addressId in constants
				session.setAttribute(Constants.ADDR_ID, StringUtils.i2s(activityDetail.getAddressId()));
				request.setAttribute("valuation", StringUtils.dbl2$(activityDetail.getValuation()));
				logger.debug("Valuation:" + request.getAttribute("valuation"));
				request.setAttribute("planCheckRequired", activityDetail.getPlanCheckRequired());
				logger.debug("planCheckRequired:" + request.getAttribute("planCheckRequired"));
				request.setAttribute("inspectionRequired", activityDetail.getInspectionRequired());
				logger.debug("inspectionRequired:" + request.getAttribute("inspectionRequired"));

				ActivityStatus activityStatus = activityDetail.getStatus();

				if (activityStatus != null) {
					request.setAttribute("status", activityStatus.getDescription());
				}

				logger.debug("ViewActivityAction-- Parameter status is set to Request as " + request.getAttribute("status"));
				request.setAttribute("startDate", StringUtils.cal2str(activityDetail.getStartDate()));
				logger.debug("startDate:" + request.getAttribute("startDate"));
				request.setAttribute("appliedDate", StringUtils.cal2str(activityDetail.getAppliedDate()));
				logger.debug("appliedDate:" + request.getAttribute("appliedDate"));
				request.setAttribute("issueDate", StringUtils.cal2str(activityDetail.getIssueDate()));
				logger.debug("issue:" + request.getAttribute("issueDate"));
				request.setAttribute("expirationDate", StringUtils.cal2str(activityDetail.getExpirationDate()));
				logger.debug("expirationDate:" + request.getAttribute("expirationDate"));
				request.setAttribute("completionDate", StringUtils.cal2str(activityDetail.getFinaledDate()));
				logger.debug("completionDate:" + request.getAttribute("completionDate"));
				request.setAttribute("planCheckFeeDate", StringUtils.cal2str(activityDetail.getPlanCheckFeeDate()));
				logger.debug("planCheckFeeDate:" + request.getAttribute("planCheckFeeDate"));
				request.setAttribute("permitFeeDate", StringUtils.cal2str(activityDetail.getPermitFeeDate()));
				logger.debug("permitFeeDate:" + request.getAttribute("permitFeeDate"));

				MicrofilmStatus status = LookupAgent.getMicrofilmStatus(activityDetail.getMicrofilm());

				request.setAttribute("microfilm", status.getDescription());
				logger.debug("microFilm:" + request.getAttribute("microFilm"));

				String createdBy = "";

				if (activityDetail.getCreatedBy() != null) {
					String firstName = "";
					String lastName = "";

					if (activityDetail.getCreatedBy().getFirstName() != null) {
						firstName = activityDetail.getCreatedBy().getFirstName();
					}

					if (activityDetail.getCreatedBy().getLastName() != null) {
						lastName = activityDetail.getCreatedBy().getLastName();
					}

					createdBy = firstName + " " + lastName;
				}

				request.setAttribute("createdBy", createdBy);
				logger.debug("createdBy:" + request.getAttribute("createdBy"));
			}

			// PlanCheck latestPlanCheck = new PlanCheckAgent().getLatestPlanCheck(activityId);
			String disableFees = "N";

			logger.debug(" *** control is b4 block  ");

			// if ((latestPlanCheck == null) ||
			// (latestPlanCheck.getPlanCheckId() == 0)) {
			// latestPlanCheck = new PlanCheck();
			//
			// if ((activityDetail.getPlanCheckRequired() == null) ||
			// activityDetail.getPlanCheckRequired().equals("Y")) {
			// disableFees = "Y";
			// }
			// }
			//
			// logger.debug(" *** control is After block  ");
			request.setAttribute("disableFees", disableFees);
			logger.debug("disableFees:" + disableFees);

			// request.setAttribute("latestPlanCheck", latestPlanCheck);
			// list of attachments for this activity.
			// List attachmentList = new AttachmentAgent().getAttachments(activityId,
			// "A");
			// List attachmentList = new AttachmentAgent().onineAttachments(activityId);
			List attachmentList = new CommonAgent().getAttachments(activityId, "A");
			request.setAttribute("attachmentList", attachmentList);

			// list of Plan check comments for this activity.
			// List commentList = new CommentAgent().getCommentList(activityId, 3, "A");
			// List planCheckCommentList = new PlanCheckAgent().getPlanCheck(activityId);

			// request.setAttribute("planCheckCommentList", planCheckCommentList);

			// check if its code enfocement comments:

			// if(!new CommentAgent().getCodeEnforcementComments(activityId)) {

			// List of comments for this activity.
			// List commentList = new CommentAgent().getPlanCheckCommentsOnline(activityId);
			// request.setAttribute("commentList", commentList);
			// }else{
			request.setAttribute("commentList", new ArrayList());
			// }

			request.setAttribute("fromOnline", "fromOnline");
			// finance for this activity
			FinanceSummary financeSummary = new FinanceAgent().getActivityFinance(activityId);
			request.setAttribute("financeSummary", financeSummary);
			request.setAttribute("editable", "false");

			request.setAttribute("activity", activity);
			logger.info("Exiting ViewActivityAction");
		} catch (Exception e) {
			throw e;
		}
	}
}

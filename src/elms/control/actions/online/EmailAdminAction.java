package elms.control.actions.online;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.online.EmailAdminForm;

public class EmailAdminAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(EmailAdminAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ActionErrors errors = new ActionErrors();

		EmailAdminForm emailAdminForm = (EmailAdminForm) form;
		AdminAgent adminAgent = new AdminAgent();
		try {

			String action = request.getParameter("action") != null ? request.getParameter("action") : "display";

			HttpSession session = request.getSession();

			if (action.equalsIgnoreCase("display")) {
				// display action
				logger.debug("In display block");
				emailAdminForm = adminAgent.getEmailTemplates();
				request.setAttribute("emailAdminForm", emailAdminForm);
			} else if (action.equalsIgnoreCase("save")) {
				// save action
				logger.debug("In save block");
				adminAgent.saveEmailTemplates(emailAdminForm);
				emailAdminForm = adminAgent.getEmailTemplates();
				request.setAttribute("emailAdminForm", emailAdminForm);

			}

			// these are required for the menu navigation
			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the login page");
				saveErrors(request, errors);

				return (new ActionForward(mapping.getInput()));
			}

			return (mapping.findForward("success"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured while online logon " + e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}

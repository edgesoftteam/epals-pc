package elms.control.actions.common;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.common.Constants;
import elms.control.beans.LogonForm;
import elms.security.User;
import elms.util.jcrypt;

public final class ChangePasswordAction extends Action {

	/**
	 * The Logger.
	 */
	static Logger logger = Logger.getLogger(ChangePasswordAction.class.getName());

	/**
	 * Navigation keys.
	 */
	public final static String DISPLAY = "display";
	public final static String PERFORM = "perform";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String action = request.getParameter("action") != null ? request.getParameter("action") : DISPLAY;
		HttpSession session = request.getSession(true);

		if (action.equals(DISPLAY)) {
			return (mapping.findForward(DISPLAY));
		} else if (action.equals(PERFORM)) {

			try {
				User user = (User) session.getAttribute(Constants.USER_KEY);
				LogonForm logonForm = (LogonForm) form;
				String oldPassword = logonForm.getPassword();
				String encryptedOldPassword = jcrypt.crypt("X6", oldPassword);
				logger.debug("old password is " + encryptedOldPassword);
				String newPassword = logonForm.getNewPassword();
				String encryptedNewPassword = jcrypt.crypt("X6", newPassword);
				logger.debug("newPassword is " + encryptedNewPassword);
				String username = logonForm.getUsername();
				logger.debug("username is " + username);

				AdminAgent adminAgent = new AdminAgent();
				adminAgent.changePassword(username, encryptedOldPassword, encryptedNewPassword);
				logger.debug("Password updated successfully, going to success page");
				// set the message to the attribute.
				request.setAttribute("message", "Password changed successfully, Please login with your new password.");

				return (mapping.findForward(PERFORM));
			} catch (SQLException sqle) {
				logger.debug("unable to update database, redirecting to the same page." + sqle.getMessage());
				request.setAttribute("message", "Invalid Username/Password, Please try again");
				return (mapping.findForward(DISPLAY));
			} catch (Exception e) {
				logger.error("unknown exception occured " + e.getMessage());
				throw new ServletException(e.getMessage());
			}
		} else {
			logger.error("no mapping defined");
			throw new ServletException("No mapping defined");
		}

	}

}
package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.app.common.Condition;
import elms.control.beans.ConditionForm;
import elms.security.User;

public class SaveConditionAction extends Action {
	static Logger logger = Logger.getLogger(SaveConditionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		int condId = Integer.parseInt(request.getParameter("conditionId"));
		int levelId = Integer.parseInt(request.getParameter("levelId"));
		int libraryId = Integer.parseInt(request.getParameter("libraryId"));
		String level = request.getParameter("conditionLevel");

		try {
			logger.debug("Entering SaveConditionAction :: CondId : " + condId + " :: levelId : " + levelId);

			ConditionForm conditionForm = (ConditionForm) form;

			Condition condition = new Condition();

			condition.setConditionId(condId);
			condition.setLevelId(levelId);
			condition.setConditionLevel(level);
			condition.setLibraryId(libraryId);

			if (condId == 0) {
				condition.setShortText(conditionForm.getShortText());
			}

			condition.setText(conditionForm.getText());

			if (conditionForm.getInspectable().equals("on")) {
				condition.setInspectable("Y");
			} else {
				condition.setInspectable("N");
			}

			if (conditionForm.getWarning().equals("on")) {
				condition.setWarning("Y");
			} else {
				condition.setWarning("N");
			}

			if (conditionForm.getComplete().equals("on")) {
				condition.setComplete("Y");
			} else {
				condition.setComplete("N");
			}

			// get user information from session
			condition.setUpdatedBy(user.getUserId());
			condition.setCreatedBy(user.getUserId());
			logger.debug("UpdatedBy :" + condition.getUpdatedBy());

			CommonAgent conditionAgent = new CommonAgent();
			conditionAgent.saveCondition(condition);

			logger.info("Exiting SaveConditionAction");
		} catch (Exception e) {
			logger.error("Error in SaveConditionAction" + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

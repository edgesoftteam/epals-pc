package elms.control.actions.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AssessorAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.Assessor;
import elms.app.lso.Street;
import elms.control.beans.AssessorDataForm;
import elms.util.StringUtils;

public class AssessorDataAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AssessorDataAction.class.getName());
	String contextRoot = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// try{
		HttpSession session = request.getSession();
		Assessor assessor = new Assessor();
		ActionErrors errors = new ActionErrors();
		contextRoot = request.getContextPath();

		String nextPage = "search";
		// get data from the form
		AssessorDataForm assessorDataForm = (AssessorDataForm) form;

		// The parameters obtained from the action form.
		String streetNumber = (assessorDataForm.getStreetNumber() == null) ? "" : assessorDataForm.getStreetNumber();
		logger.debug("got streetNumber: " + streetNumber);

		String streetFraction = (assessorDataForm.getStreetFraction() == null) ? "" : assessorDataForm.getStreetFraction();
		logger.debug("got streetFraction: " + streetFraction);

		String streetId = (assessorDataForm.getStreetId() == null) ? "" : assessorDataForm.getStreetId();
		logger.debug("got streetId: " + streetId);

		String unitNbr = (assessorDataForm.getUnitNbr() == null) ? "" : assessorDataForm.getUnitNbr();
		logger.debug("got unitNbr: " + unitNbr);

		String apn = (assessorDataForm.getApn() == null) ? "" : assessorDataForm.getApn();
		logger.debug("got apn: " + apn);

		String ownerName = (assessorDataForm.getOwnerName() == null) ? "" : assessorDataForm.getOwnerName();
		logger.debug("got getOwnerName: " + ownerName);

		String action = (request.getParameter("action") != null) ? request.getParameter("action") : "search";
		logger.debug("The action is " + action);

		String lsoId = (request.getParameter("lsoId") == null) ? "" : request.getParameter("lsoId");
		logger.debug("got lsoId: " + lsoId);

		AddressAgent lsoAgent = new AddressAgent();
		List addressResults;

		// special codes
		String useCode = (assessorDataForm.getUseCode() == null) ? "" : assessorDataForm.getUseCode();
		logger.debug("got useCode: " + useCode);

		String zoneCode = (assessorDataForm.getZoneCode() == null) ? "" : assessorDataForm.getZoneCode();
		logger.debug("got zoneCode: " + zoneCode);

		String numberOfUnits = (assessorDataForm.getNumberOfUnits() == null) ? "" : assessorDataForm.getNumberOfUnits();
		logger.debug("got numberOfUnits: " + numberOfUnits);

		String hazardAbatementCode = (assessorDataForm.getHazardAbatementCode() == null) ? "" : assessorDataForm.getHazardAbatementCode();
		logger.debug("got hazardAbatementCode: " + hazardAbatementCode);

		String dateOfLastSaleGT = (assessorDataForm.getDateOfLastSaleGT() == null) ? "" : assessorDataForm.getDateOfLastSaleGT();
		logger.debug("got dateOfLastSaleGT: " + dateOfLastSaleGT);

		String dateOfLastSaleLT = (assessorDataForm.getDateOfLastSaleLT() == null) ? "" : assessorDataForm.getDateOfLastSaleLT();
		logger.debug("got dateOfLastSaleLT: " + dateOfLastSaleLT);

		String improvementValueGT = (assessorDataForm.getImprovementValueGT() == null) ? "" : assessorDataForm.getImprovementValueGT();
		logger.debug("got improvementValueGT: " + improvementValueGT);

		String improvementValueLT = (assessorDataForm.getImprovementValueLT() == null) ? "" : assessorDataForm.getImprovementValueLT();
		logger.debug("got improvementValueLT: " + improvementValueLT);

		String totalAssessmentGT = (assessorDataForm.getTotalAssessmentGT() == null) ? "" : assessorDataForm.getTotalAssessmentGT();
		logger.debug("got totalAssessmentGT: " + totalAssessmentGT);

		String totalAssessmentLT = (assessorDataForm.getTotalAssessmentLT() == null) ? "" : assessorDataForm.getTotalAssessmentLT();
		logger.debug("got totalAssessmentLT: " + totalAssessmentLT);

		if (action.equalsIgnoreCase("search")) {
			// search page
			return (mapping.findForward("search"));
		} else if (action.equalsIgnoreCase("results")) {
			// search results
			if (!((apn == null) || apn.equals(""))) {
				logger.debug("APN number exists, jump to the result page");
				// view assessor information
				logger.debug("apn value is " + apn);

				if ((apn == null) || apn.equals("")) {
					errors.add("Session APN  is required", new ActionError("error.noAssessorInfo.available"));
				} else {
					assessor = new AssessorAgent().getAssessor(apn);
					apn = assessor.getParcelNbr();

					if ((apn == null) || apn.equals("")) {
						errors.add("No Assessor Information", new ActionError("error.noAssessorInfo.available"));
					}
				}

				if (!errors.empty()) {
					saveErrors(request, errors);
				}

				request.setAttribute("assessor", assessor);
				request.setAttribute("assessorDataForm", assessorDataForm);
				logger.debug("assessor object is set to request");

				return (mapping.findForward("view"));
			} else if (!(ownerName.equals(""))) {
				logger.debug("Owner name has a value " + ownerName);
				lsoAgent = new AddressAgent();
				List ownerResults = new ArrayList();
				try {
					ownerResults = lsoAgent.searchOwner("", "", "", ownerName.toUpperCase());
				} catch (Exception e) {
					logger.error("Exception occured while searching owner " + e.getMessage());
				}
				request.setAttribute("ownerResults", ownerResults);
				return (mapping.findForward("searchResultsByOwner"));

			} else if ((!(streetNumber.equals(""))) || (!(streetId.equals("")))) {
				// first - address
				try {
					boolean active = false;
					AddressAgent lsoTreeAgent = new AddressAgent();

					/**
					 * First try to find the address in the Address Range Table. If found use that to display the tree. If not found then try to look thru the lso_address table for a matching entry. There are some lands being found in the address range table which don't have any entry in the lso_address table. This is a data issue
					 */
					List lsoIdList = new ArrayList();
					Street street = null;

					AddressAgent streetAgent = new AddressAgent();

					// if condition added by hema
					if ((!(streetNumber.equals(""))) && ((streetId.equals("")))) {
						List specialResultsStreet = new ArrayList();
						request.setAttribute("specialResults", specialResultsStreet);
						return (mapping.findForward("searchResultsBySpecial"));
					}

					street = streetAgent.getStreet(StringUtils.s2i(streetId));
					Address address = new Address(StringUtils.s2i(streetNumber), "", street, unitNbr);

					lsoIdList = lsoTreeAgent.getLandId(address, active);

					if (lsoIdList.size() == 0) {
						lsoIdList = lsoTreeAgent.getLsoIdList(streetNumber, StringUtils.s2i(streetId));
					}

					List lsoTreeList = lsoTreeAgent.getLsoTreeList(lsoIdList, active, contextRoot);

					logger.debug("lsoIdList size is " + lsoIdList.size());
					logger.debug("lsoTreeList size is " + lsoTreeList.size());

					request.setAttribute("lsoTreeList", lsoTreeList);

					request.setAttribute("assessorDataForm", assessorDataForm);

				} catch (Exception e) {
					logger.error("Exception occured while searching for address...throwing exception " + e.getMessage());
					throw new ServletException("Exception occured while searching for address --" + e.getMessage());
				}
			}

			else if ((assessorDataForm.getStreetNumber().equals("")) && (assessorDataForm.getStreetFraction().equals("")) && (assessorDataForm.getApn().equals("")) && (assessorDataForm.getOwnerName().equals("")) && (assessorDataForm.getUseCode().equals("")) && (assessorDataForm.getZoneCode().equals("")) && (assessorDataForm.getNumberOfUnits().equals("")) && (assessorDataForm.getHazardAbatementCode().equals("")) && (assessorDataForm.getDateOfLastSaleGT().equals("")) && dateOfLastSaleLT.equals("") && (assessorDataForm.getImprovementValueLT().equals("")) && (assessorDataForm.getImprovementValueGT().equals("")) && (assessorDataForm.getTotalAssessmentGT().equals("")) && (assessorDataForm.getTotalAssessmentLT().equals(""))) {
				// check for search atleat by one criteria.
				boolean val = true;

				try {// start of try
					request.setAttribute("assessorDataForm", assessorDataForm);
					session.setAttribute("nextPage", nextPage);
					if (nextPage.equalsIgnoreCase("search")) {
						errors.add("statusCodeExists", new ActionError("statusCodeExists", "Please enter some search criteria and try again."));
						saveErrors(request, errors);
					}
				}// end of try
				catch (Exception e) {
					logger.error("Exception occured while getting special results " + e.getMessage());
					throw new ServletException(e.getMessage());
				}
				request.setAttribute("assessor", assessor);
				request.setAttribute("assessorDataForm", assessorDataForm);
				return (mapping.findForward(nextPage));

			}// end of else if
			else {
				// it is one of the special characters
				List specialResults;
				try {
					specialResults = new AssessorAgent().searchAssessorData(useCode, zoneCode, numberOfUnits, hazardAbatementCode, dateOfLastSaleGT, dateOfLastSaleLT, improvementValueGT, improvementValueLT, totalAssessmentGT, totalAssessmentLT);
				} catch (Exception e) {
					logger.error("Exception occured while getting special results " + e.getMessage());
					throw new ServletException(e.getMessage());
				}
				request.setAttribute("specialResults", specialResults);
				return (mapping.findForward("searchResultsBySpecial"));

			}

			return (mapping.findForward("searchResults"));
		} else if (action.equalsIgnoreCase("view")) {
			logger.debug("obtained LSO id is " + lsoId);

			try {
				apn = LookupAgent.getApnForLsoId(lsoId);
			} catch (Exception e) {
				logger.error("Exception occured when getting APN for lso Id " + e.getMessage());
			}

			// view assessor information
			logger.debug("apn value is " + apn);

			if ((apn == null) || apn.equals("")) {
				errors.add("Session APN  is required", new ActionError("error.sessionApn.required"));
			} else {
				assessor = new AssessorAgent().getAssessor(StringUtils.apnWithoutHyphens(apn));
				apn = assessor.getParcelNbr();

				if ((apn == null) || apn.equals("")) {
					errors.add("No Assessor Information", new ActionError("error.noAssessorInfo.available"));
				}
			}

			if (!errors.empty()) {
				saveErrors(request, errors);
			}
			logger.debug("before setting assessor object: " + assessorDataForm.getApn());
			request.setAttribute("assessor", assessor);
			request.setAttribute("assessorDataForm", assessorDataForm);
			logger.debug("assessor object is set to request");

			return (mapping.findForward("view"));
		} else {
			// no mapping defined yet
			logger.error("No mapping defined.");

			return (mapping.findForward("error"));
		}
	}
}

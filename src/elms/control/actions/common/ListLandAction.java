package elms.control.actions.common;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AddressAgent;
import elms.control.beans.LandForm;

public class ListLandAction extends Action {

	static Logger logger = Logger.getLogger(ListHoldAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			LandForm landForm = (LandForm) form;

			String lsoId = request.getParameter("lsoId");
			logger.info("Entering ListLandAction with lsoId of " + lsoId);
			AddressAgent listLandAgent = new AddressAgent();

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			CachedRowSet landRowSet = (CachedRowSet) listLandAgent.getLandList(lsoId);

			request.setAttribute("landList", landRowSet);
			request.setAttribute("lsoId", lsoId);

			logger.info("Exiting ListLandAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}
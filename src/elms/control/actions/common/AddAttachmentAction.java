package elms.control.actions.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.app.bl.AttachmentType;

public class AddAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(AddAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {

			String levelId = (request.getParameter("levelId") != null) ? request.getParameter("levelId") : (String) request.getAttribute("levelId");
			logger.debug("levelId " + levelId);
			String level = (request.getParameter("level") != null) ? request.getParameter("level") : (String) request.getAttribute("level");
			logger.debug("level " + level);

			String actId = (request.getParameter("actId") != null) ? request.getParameter("actId") : (String) request.getAttribute("actId");
			logger.debug("actId " + actId);

			String fromWhere = (request.getParameter("fromWhere") != null) ? request.getParameter("fromWhere") : (String) request.getAttribute("fromWhere");
			logger.debug("fromWhere " + fromWhere);

			String department = (request.getParameter("department") != null) ? request.getParameter("department") : (String) request.getAttribute("department");
			logger.debug("department " + department);

			List attachments = new CommonAgent().getAttachments(Integer.parseInt(levelId), level);
			logger.debug("obtained attachments list of size " + attachments.size());			
			
			List<AttachmentType> attachmentTypes = new ArrayList<AttachmentType>();
			if(level != null && level.equalsIgnoreCase("F")) {
				attachmentTypes = LookupAgent.getAttachmentTypes(actId, level);			
			}else {
				attachmentTypes = LookupAgent.getAttachmentTypes(levelId, level);		
			}
			
			request.setAttribute("attachments", attachments);
			request.setAttribute("levelId", levelId);
			request.setAttribute("actId", actId);
			request.setAttribute("level", level);
			request.setAttribute("attachmentTypes", attachmentTypes);
			request.setAttribute("department", department);
			request.setAttribute("fromWhere", fromWhere);
			return (mapping.findForward("success"));
		} catch (Exception e) {

			logger.error(e.getMessage(),e);

			return (mapping.findForward("error"));
		}
	}
}

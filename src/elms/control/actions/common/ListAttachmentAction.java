package elms.control.actions.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.util.StringUtils;

public class ListAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(ListAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {
			logger.debug("inside list attachment action");
			int levelId = StringUtils.s2i(request.getParameter("levelId"));
			logger.debug("levelId from request object is " + levelId);
			String level = request.getParameter("level");
			logger.debug("attachmentLevel from request object is " + level);
			List attachments = new CommonAgent().getAttachments(levelId, level);
			if (attachments == null)
				attachments = new ArrayList();
			request.setAttribute("attachments", attachments);
			logger.debug("attachments is set to request with size is " + attachments.size());
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			logger.debug("Exiting ListAttachmentAction");
		} catch (Exception e) {
			logger.warn("" + e.getMessage());
		}
		return (mapping.findForward("success"));

	}

}
package elms.control.actions.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.common.Constants;
import elms.security.User;

public class ViewAdminAction extends Action {
	static Logger logger = Logger.getLogger(ViewAdminAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		resetToken(request);

		HttpSession session = request.getSession();

		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		String highlight = "search";
		request.setAttribute("hl", highlight);

		User user = (User) session.getAttribute("user");
		List groups = user.getGroups();
		if (groups == null)
			groups = new ArrayList();
		logger.debug("***** Groups size is set to " + groups.size());
		session.setAttribute("groups", groups);

		// the below value is to check peoplemanager link is from admin
		session.setAttribute(Constants.PSA_TYPE, "");
		session.setAttribute("lsoAddress", "");
		session.setAttribute("psaInfo", "");

		logger.debug("set PSATYPE to N for Admin people in the session ");

		// Forward control to the specified success URI
		return (mapping.findForward("success"));

	}

}
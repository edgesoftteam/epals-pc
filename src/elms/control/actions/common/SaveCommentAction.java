package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.Epals311Agent;
import elms.agent.LookupAgent;
import elms.app.common.Comment;
import elms.common.Constants;
import elms.control.beans.CommentForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveCommentAction extends Action {

	static Logger logger = Logger.getLogger(SaveCommentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();
		int commentId = Integer.parseInt(request.getParameter("commentId"));
		logger.debug("SaveCommentAction with commentId : " + commentId);

		int levelId = Integer.parseInt(request.getParameter("levelId"));
		logger.debug("levelId : " + levelId);
		String commentLevel = request.getParameter("commentLevel");

		try {
			CommentForm commentForm = (CommentForm) form;
			User user = (User) session.getAttribute("user");

			Comment comment = new Comment();
			comment.setCommentId(commentId);
			logger.debug("commentId :" + comment.getCommentId());
			comment.setLevelId(levelId);
			logger.debug("levelId :" + comment.getLevelId());
			comment.setCommentLevel(commentLevel);
			logger.debug("commentLevel :" + commentLevel);

			// this block of code is commented on 09/16/2004 to able to update existing comment
			// if (commentId == 0) { //for addition
			// comment.setComment(commentForm.getComments());
			// logger.debug(" Commnents after method replaceQuot " + StringUtils.checkString(commentForm.getComments()));
			// }
			// else { // for modification
			// comment.setComment("<BR>----------" + user.getUsername() + "--" + StringUtils.cal2str(Calendar.getInstance()) + "---------<BR>" + commentForm.getComments());
			// logger.debug(" Commnents after method replaceQuot " + "<BR>----------" + user.getUsername() + "--" + StringUtils.cal2str(Calendar.getInstance()) + "---------<BR>" + commentForm.getComments());
			// }

			comment.setComment(commentForm.getComments());
			comment.setInternal(commentForm.getInternal());
			logger.debug("Internal :" + comment.getInternal());
			comment.setEnteredBy(commentForm.getCreatedBy());
			logger.debug("EnteredBy :" + comment.getEnteredBy());
			comment.setEnterDate(StringUtils.str2cal(commentForm.getCreationDate()));
			logger.debug("EnteredDate :" + comment.getEnterDate());
			comment.setUpdatedBy(user.getUserId());
			logger.debug("UpdatedBy :" + comment.getUpdatedBy());

			comment.setUpdateSubProject(commentForm.getUptSubProject());
			logger.debug("uptSubProject " + comment.getUpdateSubProject());
			comment.setUpdateActivities(commentForm.getUptActivities());
			logger.debug("uptActivities " + comment.getUpdateActivities());

			CommonAgent commentAgent = new CommonAgent();
			commentAgent.saveComment(comment);

			String type = StringUtils.i2s(comment.getLevelId());
			String actType = LookupAgent.getActivityTypeForActId(type);
			logger.debug("Activity type : "+actType);
			
			//311 API Call
			if(actType.equalsIgnoreCase(Constants.THREE11_SERVICE_ACT_TYPE))
				Epals311Agent.addCommentIn311System(user,comment);
			
			logger.info("Exiting SaveCommentAction");
		} catch (Exception e) {
			logger.warn("Error in SaveCommentAction" + "/n" + e);
		}
		return (mapping.findForward("success"));

	}
}
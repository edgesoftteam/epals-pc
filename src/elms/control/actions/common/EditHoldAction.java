package elms.control.actions.common;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.control.beans.HoldForm;
import elms.util.StringUtils;

public class EditHoldAction extends Action {

	static Logger logger = Logger.getLogger(EditHoldAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();
		AdminAgent userAgent = new AdminAgent();

		try {
			int holdId = 0;
			String level = "";
			holdId = Integer.parseInt(request.getParameter("id"));
			if (holdId > 0) {
				level = request.getParameter("level");
			} else {
				holdId = StringUtils.s2i((String) request.getAttribute("id"));
				level = (String) request.getAttribute("level");
			}

			logger.info("Entering EditHoldAction with holdId of " + holdId);
			CommonAgent holdAgent = new CommonAgent();

			CachedRowSet holdRowSet = new CachedRowSet();
			holdRowSet = holdAgent.getHolds(holdId);

			request.setAttribute("holdList", holdRowSet);
			request.setAttribute("level", level);
			request.setAttribute("holdId", StringUtils.i2s(holdId));

			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new HoldForm();
			request.setAttribute("holdForm", frm);
			HoldForm holdForm = (HoldForm) frm;

			holdRowSet.beforeFirst();

			String holdType = "";
			String holdStatus = "";
			String title = "";
			String levelId = "";
			Calendar statusDt = Calendar.getInstance();

			while (holdRowSet.next()) {
				statusDt = StringUtils.dbDate2cal(holdRowSet.getString("hold_stat_dt"));
				logger.debug("status date #### " + holdRowSet.getString("hold_stat_dt"));
				holdType = holdRowSet.getString("hold_type");
				holdStatus = holdRowSet.getString("stat");
				title = holdRowSet.getString("title");
				levelId = StringUtils.i2s(holdRowSet.getInt("level_id"));
				if (title == null)
					title = "";
			}

			//if (holdRowSet != null) {
			//	holdRowSet.close();
			//}
			
			holdForm.setTitle(title);
			logger.debug(" setting default title in form as " + title);
			holdForm.setType(holdType);
			logger.debug(" setting default type in form as " + holdType);
			holdForm.setStatus(holdStatus);
			logger.debug(" setting default status in form as " + holdStatus);
			request.setAttribute("statusDate", formatter.format(statusDt.getTime()));
			logger.debug(" status date in editholdaction  " + formatter.format(statusDt.getTime()));
			request.setAttribute("levelId", levelId);
			logger.debug("levelId in editholdaction " + levelId);

			logger.info("Exiting EditHoldAction");
		} catch (Exception e) {
			logger.warn("Error in EditHoldAction");
		}

		return (mapping.findForward("success"));
	}

}
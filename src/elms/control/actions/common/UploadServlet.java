package elms.control.actions.common;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.SizeLimitExceededException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.struts.action.ActionForm;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.app.common.Attachment;
import elms.app.common.ObcFile;
import elms.control.actions.ApplicationScope;
import elms.control.beans.AttachmentForm;
import elms.gsearch.AttachmentSolrScheduler;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.Operator;
import elms.util.StringUtils;

public class UploadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean isMultipart;
	private String filePath ="c:/Temp/";
	private int maxFileSize = 250000000;
	private int maxMemSize = 4 * 1024;
	private File file;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, java.io.IOException {
		//System.out.println("Entered into servlet");
		HttpSession session = request.getSession();
		String realPath = getServletContext().getRealPath("/");
        String attachmentPath = realPath + "/attachments" + File.separator;
       // String attachmentPath = ApplicationScope.map.get("ATTACHMENTS_PATH");
        //System.out.println("attachmentPath"+attachmentPath);
		isMultipart = ServletFileUpload.isMultipartContent(request);
		response.setContentType("text/html");
		java.io.PrintWriter out = response.getWriter();
        String errors = "" ;

        CommonAgent attachmentAgent = new CommonAgent();//new AttachmentAgent(attachmentPath);
        String attachmentId = request.getParameter("attachmentId");
        String levelId = request.getParameter("levelId");
        String level = request.getParameter("level")!=null?request.getParameter("level"):"A";
        
		File fileSaveDir = new File(attachmentPath);
        if (!fileSaveDir.exists()) {
            fileSaveDir.mkdir();
        }
        
		if (!isMultipart) {
			return;
		}

		DiskFileItemFactory factory = new DiskFileItemFactory();
		factory.setSizeThreshold(maxMemSize);
		factory.setRepository(new File(attachmentPath));
		ServletFileUpload upload = new ServletFileUpload(factory);
		//upload.setSizeMax(Constants.attachmentMaxSize);

		try {
			List fileItems = upload.parseRequest(request);
			Iterator i = fileItems.iterator();

			List<Attachment> attachList = new ArrayList<Attachment>();
			String contentType = "";
			String description = request.getParameter("description");
			String keyword1 = null;
			String keyword2 = null;
			String keyword3 = null;
			String keyword4 = null;
			String fileName = "";
			int userId = ((User) session.getAttribute("user")).getUserId();
			long size = 0;
			Attachment attachment = null;
			while (i.hasNext()) {
				FileItem fi = (FileItem) i.next();
				//InputStream uploadedStream = fi.getInputStream();
				Calendar enterDate = null;
				ObcFile obcFile = null;
				InputStream stream = null;
				if (!fi.isFormField()) {
					// Get the uploaded file parameters
					//String fieldName = fi.getFieldName();
					fileName = fi.getName();
					contentType = fi.getContentType();
					//boolean isInMemory = fi.isInMemory();
					size = fi.getSize();

					// Write the file
					/*if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(attachmentPath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(attachmentPath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}*/
					obcFile = new ObcFile();
		            obcFile.setFileName(fileName.replaceAll("[^\\d\\w _.-]", ""));
		            obcFile.setFileLocation(attachmentPath);
		            obcFile.setFileSize(Operator.toString(size));
		            obcFile.setCreateDate(enterDate);
		            stream = fi.getInputStream();
					attachment = new Attachment(StringUtils.s2i(attachmentId), level, Integer.parseInt(levelId), obcFile, description, enterDate, userId, keyword1, keyword2, keyword3, keyword4);
					
					attachment.setStream(stream);
					attachList.add(attachment);
					//int addedAttachmentId = attachmentAgent.saveAttachment(attachment, contentType, null);
	                //System.out.println("Attachment is saved on the server and database with id " + addedAttachmentId);

					fi.delete();
				}else{
					String value = fi.getString();
					if(fi.getFieldName().equals("levelId"))
						levelId = value;
					else if(fi.getFieldName().equals("level"))
						level = value;
					else if(fi.getFieldName().equals("description"))
						description = value;
					else if(fi.getFieldName().equals("keyword1"))
						keyword1 = value;
					else if(fi.getFieldName().equals("keyword2"))
						keyword2 = value;
					else if(fi.getFieldName().equals("keyword3"))
						keyword3 = value;
					else if(fi.getFieldName().equals("keyword4"))
						keyword4 = value;
				}
			}
            //check if the filesize is valid.
            if (size <= 0) {
                errors = "The file size is zero or the file is not valid ! Enter valid file name.";
            }
            //check if duplicate attachment already exists
            if(attachmentAgent.checkDuplicateAttachment(attachment)){
            	errors = "The selected file already exists. Please remove the existing file OR select a different file to attach.";
            }
            //check if attachment file size and compare it to max allowed size
            /*if(!(attachmentAgent.checkAttachmentSize(attachment))){
            	errors = "You have exceeded the maximum allowed size for an attachment";
            }*/
            if (!errors.equals("")) {
            	request.setAttribute("errors", errors);
                
            }else{
				for(Attachment attachment1 : attachList){
					attachment1.setLevelId(Operator.toInt(levelId));;
					attachment1.setAttachmentLevel(level);
					attachment1.setDescription(description);
					attachment1.setKeyword1(keyword1);
					attachment1.setKeyword2(keyword2);
					attachment1.setKeyword3(keyword3);
					attachment1.setKeyword4(keyword4);
					attachment1.setEnteredBy(userId);
					attachmentAgent.saveAttachment(attachment1, contentType, null);
				}
            }
            try {
            	String attachmentsDeltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_ATTACHMENTS_DELTA_INDEX"));
				if(!attachmentsDeltaIndex.equals("") && attachmentsDeltaIndex.equalsIgnoreCase("YES")){
    			AttachmentSolrScheduler.deltaForAttachmentsLoadInitials();  // solr load initials for attachments
				}
    			} catch (Exception e) {
    				e.getMessage();
    		}
            
		}catch(SizeLimitExceededException e){
			errors = "You have exceeded the maximum allowed size for an attachment";
			request.setAttribute("errors", errors);
		}catch (Exception ex) {
			errors = "Error while uploading";
			request.setAttribute("errors", errors);
		}
		try {
			List<Attachment> attachments = new ArrayList<Attachment>();
			attachments = attachmentAgent.getAttachments(StringUtils.s2i(levelId), level);

			request.setAttribute("attachments", attachments);

			session.removeAttribute("attachmentForm");
			AttachmentForm attachmentForm = null;
			ActionForm frm = new AttachmentForm();
			attachmentForm = (AttachmentForm) frm;
			session.setAttribute("attachmentForm", attachmentForm);
			request.setAttribute("levelId", levelId);
			request.setAttribute("level", level);
			//request.setAttribute("attachmentTypeList", new LookupAgent().getAttachmentTypeList());
			RequestDispatcher rd=request.getRequestDispatcher("/jsp/common/addAttachment.jsp");  
			rd.forward(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, java.io.IOException {

		doPost(request, response);
	}
}

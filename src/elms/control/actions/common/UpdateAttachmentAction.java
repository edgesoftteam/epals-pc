package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.app.common.Attachment;
import elms.control.beans.AttachmentForm;
import elms.gsearch.AttachmentSolrScheduler;
import elms.gsearch.GlobalSearch;
import elms.util.StringUtils;

public class UpdateAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(UpdateAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			
			String actId = (request.getParameter("actId") != null) ? request.getParameter("actId") : (String) request.getAttribute("actId");
			logger.debug("actId " + actId);
			String department = (request.getParameter("department") != null) ? request.getParameter("department") : (String) request.getAttribute("department");
			logger.debug("department " + department);
			String fromWhere = (request.getParameter("fromWhere") != null) ? request.getParameter("fromWhere") : (String) request.getAttribute("fromWhere");
			logger.debug("fromWhere " + fromWhere);
			request.setAttribute("actId", actId);
			request.setAttribute("department", department);
			request.setAttribute("fromWhere", fromWhere);

			// get data from the attachment form.
			AttachmentForm attachmentForm = (AttachmentForm) form;

			// populate the attachment object from the attachment form.
			Attachment attachment = new Attachment();
			attachment.setAttachmentId(attachmentForm.getAttachmentId());
			logger.debug("Set the attachment id " + attachmentForm.getAttachmentId());
			attachment.setDescription(attachmentForm.getDescription());
			logger.debug("Set the attachment desc " + attachmentForm.getDescription());
			attachment.setKeyword1(attachmentForm.getKeyword1());
			logger.debug("Set the attachment keyword1 " + attachmentForm.getKeyword1());
			attachment.setKeyword2(attachmentForm.getKeyword2());
			logger.debug("Set the attachment keyword2 " + attachmentForm.getKeyword2());
			attachment.setKeyword3(attachmentForm.getKeyword3());
			logger.debug("Set the attachment keyword3 " + attachmentForm.getKeyword3());
			attachment.setKeyword4(attachmentForm.getKeyword4());
			logger.debug("Set the attachment keyword4 " + attachmentForm.getKeyword4());

			CommonAgent attachmentAgent = new CommonAgent();
			attachmentAgent.saveAttachment(attachment, "", null);
			try {
				String attachmentsDeltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_ATTACHMENTS_DELTA_INDEX"));
				if(!attachmentsDeltaIndex.equals("") && attachmentsDeltaIndex.equalsIgnoreCase("YES")){
				AttachmentSolrScheduler.deltaForAttachmentsLoadInitials();  // solr load initials for attachments
				}
			} catch (Exception e) {
					e.getMessage();
			}
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Exception occured when trying to update attachment data " + e.getMessage());
			return (mapping.findForward("error"));
		}
	}

}
package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.common.Constants;
import elms.security.User;

public class ViewHomeAction extends Action {
	static Logger logger = Logger.getLogger(ViewHomeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute(Constants.USER_KEY);
		request.setAttribute("deptCode", user.getDepartment().getDepartmentCode());

		String messageStr = (String) request.getParameter("str");
		if (messageStr == null)
			messageStr = "";

		request.setAttribute("str", messageStr);
		logger.debug("Message String :" + messageStr);

		logger.debug("User Type:" + (String) session.getAttribute(Constants.USER_TYPE));
		// Forward control to the specified success URI
		if (session.getAttribute(Constants.USER_TYPE).equals("V2")) {
			return (mapping.findForward("versionTwo"));
		} else {
			return (mapping.findForward("versionOne"));
		}
	}
}
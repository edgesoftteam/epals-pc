package elms.control.actions.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.app.common.Attachment;
import elms.control.beans.AttachmentForm;
import elms.util.StringUtils;

public class EditAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(EditAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {

			// get the level id as parameter on the url.
			String strLevelId = request.getParameter("levelId");
			logger.debug("The level id is " + strLevelId);
			// get the level as parameter from the url.
			String level = request.getParameter("level");
			logger.debug("The level is " + level);
			// get the attachment id as parameter from the url.
			String strAttachmentId = request.getParameter("attachmentId");
			logger.debug("The attachment id is " + strAttachmentId);
			int attachmentId = StringUtils.s2i(strAttachmentId);

			//Maintaining activityId for planchecks
			String actId = (request.getParameter("actId") != null) ? request.getParameter("actId") : (String) request.getAttribute("actId");
			logger.debug("actId " + actId);
			String department = (request.getParameter("department") != null) ? request.getParameter("department") : (String) request.getAttribute("department");
			logger.debug("department " + department);
			String fromWhere = (request.getParameter("fromWhere") != null) ? request.getParameter("fromWhere") : (String) request.getAttribute("fromWhere");
			logger.debug("fromWhere " + fromWhere);
			
			// make an instance of attachment form.
			AttachmentForm attachmentForm = new AttachmentForm();
			// make an instance of attachment agent.
			CommonAgent attachmentAgent = new CommonAgent();
			// get the attachment from the agent.
			Attachment attachment = attachmentAgent.getAttachment(attachmentId);
			logger.debug("obtained attachment details from the agent " + attachment);

			logger.debug("attachment id " + attachment.getAttachmentId());
			attachmentForm.setAttachmentId(attachment.getAttachmentId());
			logger.debug("level id " + strLevelId);
			attachmentForm.setLevelId(Integer.parseInt(strLevelId));
			logger.debug("file name " + attachment.getFile().getFileName());
			attachmentForm.setFileName(attachment.getFile().getFileName());
			logger.debug("description " + attachment.getDescription());
			attachmentForm.setDescription(attachment.getDescription());
			logger.debug("keyword1 " + attachment.getKeyword1());
			attachmentForm.setKeyword1(attachment.getKeyword1());
			logger.debug("keyword2 " + attachment.getKeyword2());
			attachmentForm.setKeyword2(attachment.getKeyword2());
			logger.debug("keyword3 " + attachment.getKeyword3());
			attachmentForm.setKeyword3(attachment.getKeyword3());
			logger.debug("keyword4 " + attachment.getKeyword4());
			attachmentForm.setKeyword4(attachment.getKeyword4());
			logger.debug("populated all the values to attachment form");

			// set stuff to the request.
			request.setAttribute("attachmentForm", attachmentForm);
			request.setAttribute("levelId", strLevelId);
			request.setAttribute("actId", actId);
			request.setAttribute("department", department);
			request.setAttribute("fromWhere", fromWhere);
			request.setAttribute("level", level);
			// everything done, go to the success mapping.
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return (mapping.findForward("error"));
		}
	}
}
package elms.control.actions.common;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.app.common.Comment;
import elms.control.beans.CommentForm;
import elms.security.User;
import elms.util.StringUtils;

public class EditCommentAction extends Action {

	static Logger logger = Logger.getLogger(EditCommentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		HttpSession session = request.getSession();
		AdminAgent userAgent = new AdminAgent();
		try {
			int commentId = Integer.parseInt(request.getParameter("id"));
			logger.info("EditCommentAction with commentId : " + commentId);

			CommonAgent commentAgent = new CommonAgent();
			Comment comment = commentAgent.getComment(commentId);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new CommentForm();
			request.setAttribute("commentForm", frm);
			CommentForm commentForm = (CommentForm) frm;

			if (comment != null) {
				commentForm.setCommentId(comment.getCommentId());
				commentForm.setLevelId(comment.getLevelId());
				commentForm.setCommentLevel(comment.getCommentLevel());
				commentForm.setComments(comment.getComment());
				if (comment.getInternal().equals("Y"))
					commentForm.setInternal("on");
				else
					commentForm.setInternal("off");
				commentForm.setCreatedBy(comment.getEnteredBy());
				commentForm.setUpdatedBy(comment.getUpdatedBy());
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				if(comment.getEnterDate()!=null)
				{
				commentForm.setCreationDate(formatter.format(comment.getEnterDate().getTime()));
				}
				User user = userAgent.getUser(comment.getEnteredBy());
				String userName = user.getFirstName() + " " + user.getLastName();
				commentForm.setCreatedUserName(userName);
				if(comment.getUpdateDate()!=null)
				{
				commentForm.setUpdatedDate(formatter.format(comment.getUpdateDate().getTime()));
				} 
				user = userAgent.getUser(comment.getUpdatedBy());
				userName = user.getFirstName() + " " + user.getLastName();
				commentForm.setUpdatedUserName(userName);

			}

			String url = request.getContextPath();
			if (commentForm.getCommentLevel().equals("L"))
				url = "/viewLand.do?lsoId=";
			else if (commentForm.getCommentLevel().equals("S"))
				url = "/viewStructure.do?lsoId=";
			else if (commentForm.getCommentLevel().equals("O"))
				url = "/viewOccupancy.do?lsoId=";
			else if (commentForm.getCommentLevel().equals("P"))
				url = "/viewProject.do?projectId=";
			else if (commentForm.getCommentLevel().equals("Q"))
				url = "/viewSubProject.do?subProjectId=";
			else if (commentForm.getCommentLevel().equals("A")) {
				String activityType = LookupAgent.getActivityTypeForActId(StringUtils.i2s(commentForm.getLevelId()));
				if (activityType == null)
					activityType = "";
				logger.debug("Activity Type is : " + activityType);
				if (activityType.equals("PWFAC"))
					url = "/pwViewFacilities.do?activityId=";
				else if (activityType.equals("PWINFO"))
					url = "/pwViewFacilities.do?activityId=";
				else if (activityType.equals("PWSAN"))
					url = "/pwViewSanitation.do.do?activityId=";
				else if (activityType.equals("PWSEW"))
					url = "/pwViewSewer.do?activityId=";
				else if (activityType.equals("PWSTR"))
					url = "/pwViewStreets.do?activityId=";
				else if (activityType.equals("PWWAT"))
					url = "/pwViewWater.do?activityId=";
				else if (activityType.equals("PWSANC"))
					url = "/pwViewSanitation.do?activityId=";
				else if (activityType.equals("PWTSSL"))
					url = "/pwViewTrafficSignals.do?activityId=";
				else if (activityType.equals("PWELEC"))
					url = "/pwViewElectrical.do?activityId=";
				else
					url = "/viewActivity.do?activityId=";
			} else if (commentForm.getCommentLevel().equals("Z"))
				url = "/editPeople.do?id=";
			url = request.getContextPath() + url + commentForm.getLevelId();

			logger.debug("Url to Back is : " + url);
			request.setAttribute("goBackUrl", url);

			logger.info("Exiting EditCommentAction");
		} catch (Exception e) {
			e.printStackTrace();
			logger.warn("Error in EditCommentAction");
		}

		return (mapping.findForward("success"));
	}

}
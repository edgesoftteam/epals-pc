package elms.control.actions.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.app.bl.AttachmentType;
import elms.app.common.Attachment;
import elms.app.common.ObcFile;
import elms.control.beans.AttachmentForm;
import elms.gsearch.AttachmentSolrScheduler;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveAttachmentAction extends Action {

	static Logger logger = Logger.getLogger(SaveAttachmentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		CommonAgent attachmentAgent = new CommonAgent();

		ActionErrors errors = new ActionErrors();

		// Extract attributes and parameters we will need
		String attachmentId = request.getParameter("attachmentId");
		logger.debug("attachmentId : " + attachmentId);

		String replaceFlag = request.getParameter("replaceFlag")!=null ? request.getParameter("replaceFlag") : "";
		logger.debug("replaceFlag : " + replaceFlag);

		String levelId = request.getParameter("levelId");
		logger.debug("levelId : " + levelId);

		String actId = (request.getParameter("actId") != null) ? request.getParameter("actId") : (String) request.getAttribute("actId");
		logger.debug("actId " + actId);

		String level = request.getParameter("level");
		logger.debug("level : " + level);

		String fromWhere = (request.getParameter("fromWhere") != null) ? request.getParameter("fromWhere") : (String) request.getAttribute("fromWhere");
		logger.debug("fromWhere " + fromWhere);
		request.setAttribute("actId", actId);
		request.setAttribute("fromWhere", fromWhere);

		try {
			AttachmentForm attachmentForm = (AttachmentForm) form;
			Attachment attachment = new Attachment();
			FormFile file = attachmentForm.getTheFile();
			String fileName = file.getFileName();
			logger.debug("filename before is " + fileName);
			int fileSize = file.getFileSize();
			logger.debug("filesize is " + fileSize);
			String contentType = file.getContentType();
			logger.debug("content type is " + contentType);
			String description = attachmentForm.getDescription();

			String size = (file.getFileSize() + "");
			String location = "";
			Calendar enterDate = Calendar.getInstance();

			ObcFile obcFile = new ObcFile();
			
			fileName = StringUtils.replaceSpecialCharWithUnderscore(fileName);
			logger.debug("filename after is " + fileName);
			
			obcFile.setFileName(fileName);
			obcFile.setFileLocation(location);
			obcFile.setFileSize(size);
			obcFile.setCreateDate(enterDate);
			InputStream stream = file.getInputStream();

			User user = (User) session.getAttribute("user");
			int userId = user.getUserId();

			// destroy the temporary file created
			file.destroy();

			attachment = new Attachment(Integer.parseInt(attachmentId), level, Integer.parseInt(levelId), obcFile, description, enterDate, userId, attachmentForm.getKeyword1(), attachmentForm.getKeyword2(), attachmentForm.getKeyword3(), attachmentForm.getKeyword4(),attachmentForm.getAttachmentType());

			// check if the file name is present and valid.
			if ((fileName == null) || fileName.equals("")) {

				errors.add("fileName", new ActionError("error.file.required"));
			}
			// check if the file size is valid.
			if (fileSize <= 0) {

				errors.add("fileSize", new ActionError("error.validfile.required"));
			}
			// check if description exists

			if ((description == null) || description.equals("")) {

				errors.add("description", new ActionError("error.description.required"));
			}

			// check if duplicate attachment already exists
			if (attachmentAgent.checkDuplicateAttachment(attachment)) {
				errors.add("duplicateAttachment", new ActionError("error.attachment.duplicate"));
			}

			// check if attachment file size and compare it to max allowed size
			if (!(attachmentAgent.checkAttachmentSize(attachment))) {
				errors.add("oversizeAttachment", new ActionError("error.attachment.oversize"));
			}
						
			List<AttachmentType> attachmentTypes = new ArrayList<AttachmentType>();
			if(level != null && level.equalsIgnoreCase("F")) {
				attachmentTypes = LookupAgent.getAttachmentTypes(actId, level);			
			}else {
				attachmentTypes = LookupAgent.getAttachmentTypes(levelId, level);		
			}

			request.setAttribute("attachmentTypes", attachmentTypes);

			// check if errors are empty and redirect back to the input page.
			if (!errors.empty()) {
				logger.debug("Going to validation error view");
				saveErrors(request, errors);
				request.setAttribute("levelId", levelId);
				request.setAttribute("level", level);
				List attachments = new CommonAgent().getAttachments(Integer.parseInt(levelId), level);
				logger.debug("size of attachments is " + attachments.size());
				request.setAttribute("attachments", attachments);
				return (new ActionForward(mapping.getInput()));
			} else {

				request.setAttribute("contentType", contentType);
				request.setAttribute("attachment", attachment);

				int addedAttachmentId = attachmentAgent.saveAttachment(attachment, contentType, stream);
				logger.debug("Attachment is saved on the server and database with id " + addedAttachmentId);

				if ((!("").equalsIgnoreCase(replaceFlag) || replaceFlag != null) && replaceFlag.equalsIgnoreCase("Y")) {
					boolean replacedExisitngFile = attachmentAgent.updateAttachmentNew(attachment, contentType, stream);
					logger.debug("replaced exisitng file:: " + replacedExisitngFile);
				}

				List attachments = new CommonAgent().getAttachments(Integer.parseInt(levelId), level);
				if (attachments == null) {

					attachments = new ArrayList();
				}

				request.setAttribute("attachments", attachments);
				logger.debug("The attachments are set to request with size " + attachments.size());

				session.removeAttribute("attachmentForm");
				attachmentForm = null;
				ActionForm frm = new AttachmentForm();
				attachmentForm = (AttachmentForm) frm;
				session.setAttribute("attachmentForm", attachmentForm);
				request.setAttribute("levelId", levelId);
				request.setAttribute("level", level);
				logger.info("Exiting SaveAttachmentAction");
			}
			try {
				String attachmentsDeltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_ATTACHMENTS_DELTA_INDEX"));
				if(!attachmentsDeltaIndex.equals("") && attachmentsDeltaIndex.equalsIgnoreCase("YES")){
			AttachmentSolrScheduler.deltaForAttachmentsLoadInitials();  // solr load initials for attachments
				}
			} catch (Exception e) {
				e.getMessage();
			}
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Error in SaveAttachmentAction" + e.getMessage());
			return (mapping.findForward("error"));
		}
	}
}

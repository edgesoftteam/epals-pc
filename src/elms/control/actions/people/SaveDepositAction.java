package elms.control.actions.people;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.agent.PeopleAgent;
import elms.app.admin.PayType;
import elms.app.finance.Payment;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.control.beans.PeopleForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveDepositAction extends Action {

	static Logger logger = Logger.getLogger(SaveDepositAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;

		try {

			// char voided ='n';
			String peopleId = "";
			String psaId = "";
			String psaType = "";
			String payee = "";
			// String feevalues ="";
			// //String somename ="";
			// String planCheck ="";
			// String permitFees="";
			// String businessTax ="";
			// String other="";
			String otherText = "";
			double amount = 0;
			// char feeType = 'x';
			String method = "";
			String checkNo = "";
			String comments = "";
			// int enteredBy =0;
			String enteredBy = "";
			int tType = 0;
			// int authorizedBy = 0;

			FinanceAgent financeAgent = new FinanceAgent();
			PayType payType = new PayType();
			method = paymentMgrForm.getMethod();
			amount = StringUtils.$2dbl(paymentMgrForm.getAmount());

			if (paymentMgrForm.getTransactionType().equalsIgnoreCase("deposit")) {
				payType.setPayTypeId(7);
			} else {
				payType.setPayTypeId(8);
				amount = amount * -1;
			}
			logger.debug("set payment object Trantype" + payType.getPayTypeId());

			checkNo = paymentMgrForm.getCheckNoConfirmation();
			comments = paymentMgrForm.getComments();

			peopleId = request.getParameter("peopleId");
			logger.debug("obtained peopleId from parameter" + peopleId);
			psaId = request.getParameter("psaId");
			logger.debug("obtained psaId from parameter" + psaId);
			psaType = request.getParameter("psaType");
			logger.debug("obtained psaType from perameter" + psaType);

			Payment payment = new Payment();
			payment.setPeopleId(StringUtils.s2i(peopleId));
			logger.debug("set payment object peopleId" + peopleId);
			payment.setMethod(method);
			logger.debug("set payment object method" + method);

			payment.setType(payType);
			logger.debug("set payment object type" + payType);

			User user = (User) session.getAttribute("user");
			enteredBy = user.getUsername(); // = lookupAgent.getUserDetails(enteredBy);

			payment.setEnteredBy(user);
			logger.debug("set payment object entered By" + user.getUserId());
			payment.setAmount(amount);
			logger.debug("set payment object amount" + amount);
			payment.setComment(comments);
			logger.debug("set payment object comments" + comments);
			payment.setAccountNbr(checkNo);
			logger.debug("set payment object checkno" + checkNo);
			payment.setDepartmentCode(user.getDepartment().getDepartmentCode());

			logger.debug("payment object" + payment);
			financeAgent.saveDeposit(payment);

			String peopleType = (String) session.getAttribute("peopleType");
			logger.debug("obtained people type from session " + peopleType);

			nextPage = "editPeople";

			PaymentMgrForm paymentForm = (PaymentMgrForm) form;

			String addEditFlag = Constants.EDIT;
			ActionForm frm = new PeopleForm();
			request.setAttribute("peopleForm", frm);
			PeopleForm peopleForm = (PeopleForm) frm;
			request.setAttribute("addEditFlag", addEditFlag);
			request.setAttribute("peopleType", peopleType);

			People people = new PeopleAgent().getPeople(StringUtils.s2i(peopleId));
			peopleForm.setPeople(people);
			session.setAttribute("peopleId", peopleId);
			session.setAttribute(Constants.PSA_ID, psaId);
			session.setAttribute(Constants.PSA_TYPE, psaType);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				}
			} catch (Exception e) {
				e.getMessage();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));

	}

}
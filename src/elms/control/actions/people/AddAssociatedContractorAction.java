package elms.control.actions.people;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.PeopleAgent;
import elms.app.people.People;
import elms.control.beans.PeopleForm;
import elms.security.User;
import elms.util.StringUtils;

public class AddAssociatedContractorAction extends Action {

	static Logger logger = Logger.getLogger(AddAssociatedContractorAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering AddAssociatedContractorAction");

		String nextPage = "success";
		HttpSession session = request.getSession();
		User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		
		try {
			PeopleForm peopleForm = (PeopleForm) form;
			
			String action = request.getParameter("action");
			String peopleId = null;
			String psaId = null;
			String psaType = null;
			String name = null;
			PeopleAgent peopleAgent = new PeopleAgent();
			logger.debug("action "+action);
			
			if(action.equalsIgnoreCase("add")){
				peopleId = request.getParameter("peopleId");
				psaId = request.getParameter("psaId");
				psaType = request.getParameter("psaType");
				name = request.getParameter("name");
				
				logger.debug("PeopleId :" + peopleId + " : PsaId: " + psaId + " : PsaType  : " + psaType);
				
				peopleForm.setPeople(new People(StringUtils.s2i(peopleId),name));
				peopleForm.setPsaId(psaId);
				peopleForm.setPsaType(psaType);
				
				session.setAttribute("peopleId", peopleId);
				session.setAttribute("psaId", psaId);
				session.setAttribute("psaType", psaType);
				session.setAttribute("name", name);
			} else if(action.equalsIgnoreCase("save")){
				String licenseNumber  = peopleForm.getPeople().getLicenseNbr();
				int agentId = peopleForm.getPeople().getPeopleId();
				List<People> contractorList = peopleAgent.getContractorListForLicNo(licenseNumber);
				logger.debug("Got contractorList as "+contractorList.size());
				if(contractorList.size() == 0){
					request.setAttribute("error", "Contractor not found.");
				} else if(contractorList.size() == 1){  
					int  contractorId = contractorList.get(0).getPeopleId();
					peopleAgent.addContractorAgent(agentId, contractorId);
					nextPage = "editPeople";
				} else if(contractorList.size() > 1) {
					request.setAttribute("contractorList", contractorList);
					request.setAttribute("option", "select");
				}
				
				request.setAttribute("name", peopleForm.getPeople().getName());
			} else if(action.equalsIgnoreCase("map")){
				String contractorId = request.getParameter("contractorId");
				String agentId = request.getParameter("agentId");
				logger.debug("contractorId : "+contractorId +" agentId :"+agentId);
				peopleAgent.addContractorAgent(StringUtils.s2i(agentId), StringUtils.s2i(contractorId));
				nextPage =  "editPeople";
			} else if(action.equalsIgnoreCase("verify")){
				String caId = request.getParameter("caId");
				String startDate = request.getParameter("startDate");
				String expDate = request.getParameter("expDate");
				
				logger.debug("verify ca id "+ caId +" start date "+startDate +" expp date "+expDate);
				peopleAgent.verfiyContractorAgent(caId, user.getUserId(), startDate, expDate);
				nextPage =  "editPeople";
			} else if(action.equalsIgnoreCase("delete")){
				String caId = request.getParameter("caId");
				logger.debug(" delete ca id "+ caId );
				peopleAgent.deleteContractorAgent(caId);
				nextPage =  "editPeople";
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));

	}

}
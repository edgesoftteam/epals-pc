package elms.control.actions.people;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.common.Constants;
import elms.control.beans.PeopleSearchForm;

public class SearchPeopleAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SearchPeopleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List peopleTypeList = new ArrayList();
		HttpSession session = request.getSession();

		try {
			PeopleSearchForm peopleSearchForm = (PeopleSearchForm) form;

			String level = peopleSearchForm.getLevel();
			String lev = (String) session.getAttribute("level");
			logger.debug("level"+lev);

			if (level.equalsIgnoreCase("A") || level.equalsIgnoreCase("P") || level.equalsIgnoreCase("Q") || level.equalsIgnoreCase("O")) {
				peopleSearchForm.setLsoAddress((String) session.getAttribute("lsoAddress"));
				peopleSearchForm.setPsaInfo((String) session.getAttribute("psaInfo"));

				String psaId = (String) session.getAttribute(Constants.PSA_ID);
				logger.debug("obtained psa id from session as " + psaId);

				String psaType = (String) session.getAttribute(Constants.PSA_TYPE);
				logger.debug("obtained psa type from session as " + psaType);

				session.setAttribute(Constants.PSA_ID, psaId);
				session.setAttribute(Constants.PSA_TYPE, psaType);

				boolean businessLicenseSubProject = false;
				boolean businessTaxSubProject = false;
				String subProjectName = LookupAgent.getSubProjectNameForActivityId(psaId);
				if (subProjectName.startsWith(Constants.SUB_PROJECT_NAME_STARTS_WITH))
					businessLicenseSubProject = true;
				if (subProjectName.startsWith(Constants.BT_SUB_PROJECT_NAME_STARTS_WITH))
					businessTaxSubProject = true;

				if (businessLicenseSubProject || businessTaxSubProject) {
					peopleTypeList = LookupAgent.getBLPeopleTypes();
				} else {
					peopleTypeList = LookupAgent.getPeopleTypes();
				}

				request.setAttribute("fromAdmin", "false");
			} else {
				logger.debug("from admin");
				session.removeAttribute(Constants.PSA_TYPE);
				session.removeAttribute(Constants.PSA_ID);
				request.setAttribute("fromAdmin", "true");
				peopleTypeList = LookupAgent.getPeopleTypes();
			}
			
			request.setAttribute("level", level);
			request.setAttribute("peopleTypeList", peopleTypeList);
			request.setAttribute("peopleSearchForm", peopleSearchForm);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}

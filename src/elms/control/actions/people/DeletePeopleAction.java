package elms.control.actions.people;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.PeopleAgent;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.security.User;
import elms.util.StringUtils;

public class DeletePeopleAction extends Action {

	static Logger logger = Logger.getLogger(DeletePeopleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		PeopleForm peopleForm = (PeopleForm) form;
		String psaType = "";

		try {

			String[] selectedPeople = peopleForm.getSelectedPeople();
			logger.debug("obtained array of selected people elememts" + selectedPeople);

			psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			// psaType = (String) request.getParameter("psaType");

			logger.debug("obtained psa type from session as " + psaType);

			String strPsaId = (String) session.getAttribute(Constants.PSA_ID);
			// String strPsaId = (String) request.getParameter("psaId");
			logger.debug("obtained psa id from session as " + strPsaId);
			int psaId = StringUtils.s2i(strPsaId);
			User user = (User) session.getAttribute(Constants.USER_KEY);
			PeopleAgent peopleAgent = new PeopleAgent();
			peopleAgent.deletePeople(selectedPeople);
			peopleAgent.updateActivity(psaId, user.getUserId());
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			List peopleList = new PeopleAgent().getPeoples(psaId, psaType, 0);
			request.setAttribute("peopleList", peopleList);
			request.setAttribute("psaId", strPsaId);
			request.setAttribute("psaType", psaType);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}
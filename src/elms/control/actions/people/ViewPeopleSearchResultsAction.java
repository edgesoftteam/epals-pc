package elms.control.actions.people;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.PeopleAgent;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.control.beans.PeopleSearchForm;
import elms.util.StringUtils;

public class ViewPeopleSearchResultsAction extends Action {
	static Logger logger = Logger.getLogger(ViewPeopleSearchResultsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewPeopleSearchResultsAction");

		HttpSession session = request.getSession();
		PeopleForm peopleForm = (PeopleForm) form;
		ActionErrors errors = new ActionErrors();

		String nextPage = "";
		String peopleType = "";

		try {
			String psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			logger.debug("psaType"+psaType);
			String psaId = (String) session.getAttribute(Constants.PSA_ID);

			logger.debug("obtained psa type from session as " + psaType);
			request.setAttribute("psaType", psaType);
			logger.debug("obtained psaId from session as " + psaId);
			request.setAttribute("psaId", psaId);

			String peopleTypeId = peopleForm.getPeople().getPeopleType().getCode();

			if (!(peopleTypeId == null) && !peopleTypeId.trim().equals("")) {
				peopleType = PeopleAgent.getPeopleType(peopleTypeId);
				session.setAttribute("peopleType", peopleType);
				session.setAttribute("peopleTypeId", peopleTypeId);
				logger.debug("People Type Id : " + peopleTypeId + " : People Type :" + peopleType);
			}

			String licenseNumber = peopleForm.getPeople().getLicenseNbr();
			logger.debug("Got license number as " + licenseNumber);

			String name = peopleForm.getPeople().getName();
			logger.debug("Got name as " + name);

			String phoneNumber = StringUtils.phoneFormat(peopleForm.getPeople().getPhoneNbr());
			logger.debug("Got phone number as " + phoneNumber);

			if (phoneNumber == null) {
				phoneNumber = "";
			}

			String phoneExt = peopleForm.getPeople().getExt();
			logger.debug("Got phone ext as " + phoneExt);

			if (name.equalsIgnoreCase("") && licenseNumber.equalsIgnoreCase("") && phoneNumber.equalsIgnoreCase("")) {
				String ownerId = "";

				List peopleOwner = null;
				String projectId = new ActivityAgent().getProjectId(psaId);

				People owner = new People();

				// first check people owner for same address
				if (psaType.equalsIgnoreCase("A")) {
					logger.debug("project id is: " + projectId);
					peopleOwner = new PeopleAgent().getPeopleOwnerList(projectId, psaId, peopleTypeId);
				} else {
					logger.debug("project level check of LSO owner");
					logger.debug("psaType is " + psaType);
				}

				if ((psaType.equalsIgnoreCase("P")) || (peopleOwner.size() == 0)) {
					logger.debug("inside 0 peopleList");

					// second check lso owner for same address
					String lsoId = (String) session.getAttribute(Constants.LSO_ID);

					if (lsoId == null) {
						lsoId = "-1";
					}

					logger.debug("####### lsoId : " + lsoId);

					String lsoType = (String) session.getAttribute(Constants.LSO_TYPE);

					if (lsoType == null) {
						lsoType = "-1";
					}

					logger.debug("######## lsoType :" + lsoType);

					List owners = new PeopleAgent().getLsoOwners(lsoId, lsoType, psaType, psaId);
					logger.debug("In owner size " + owners.size());

					if (owners.size() > 0) {
						String addEditFlag = Constants.ADD;
						logger.debug("people detail page flag is set to  " + addEditFlag);

						Iterator itr = owners.iterator();

						if (itr.hasNext()) {
							owner = (People) itr.next();
						}

						if (owners != null) {
							ownerId = StringUtils.i2s(owner.getPeopleId());
						}

						session.setAttribute("peopleId", ownerId);

						int peopleId = StringUtils.s2i(ownerId);
						logger.debug("People Id is Put in the session as " + ownerId);
						peopleType = owner.getPeopleType().getCode();
						logger.debug("got people type " + peopleType);
						session.setAttribute("peopleType", peopleType);

						PeopleForm frm = new PeopleForm();
						frm.setPeople(owner);
						frm.setPsaId(psaId);
						frm.setPsaType(psaType);

						request.setAttribute("peopleForm", frm);
						request.setAttribute("peopleId", ownerId);
						request.setAttribute("addEditFlag", addEditFlag);
						request.setAttribute("peopleType", peopleType);

						nextPage = peopleType;
						logger.debug("Next Page is set to  " + nextPage);

						if (owner.getDlExpiryDate() != null) {
							logger.debug("in first if");
							if ((owner.getDlExpiryDate()).before(Calendar.getInstance())) {
								logger.debug("in second if");
								owner.setDlExpiryDateString("Y");
							} else {
								owner.setDlExpiryDateString("N");
							}
						}

						request.setAttribute("people", owner);

						return (mapping.findForward(nextPage));
					}
				} else {

					if (owner.getDlExpiryDate() != null) {
						logger.debug("in first if");
						if ((owner.getDlExpiryDate()).before(Calendar.getInstance())) {
							logger.debug("in second if");
							owner.setDlExpiryDateString("Y");
						} else {
							owner.setDlExpiryDateString("N");
						}
					}
					logger.debug("size of list for peopleOwner is : " + peopleOwner.size());
					logger.debug("In people size != 1 condition");
					nextPage = "listPeople";
					request.setAttribute("peopleList", peopleOwner);
					logger.debug(" set selected PeopleList in the session ");

					return (mapping.findForward(nextPage));
				}
			}

			List peoples = new PeopleAgent().getSearchResults(peopleTypeId, licenseNumber, name, phoneNumber, phoneExt);
			logger.debug("obtained people search results of size " + peoples.size());

			People people = null;
			String strPeopleId = null;

			// No duplicate people for same activity
			if (peoples.size() == 1) {
				int peopleCnt = -1;
				peopleCnt = new PeopleAgent().checkPeople4PSAlevel(peopleTypeId, licenseNumber, StringUtils.s2i(psaId), psaType);

				if (peopleCnt > 0) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.people.duplicate"));
					logger.debug("Duplicate person for Activity/Project ");
				}

				if (!errors.empty()) {
					saveErrors(request, errors);
					logger.debug("Action mapping input :" + mapping.getInput());

					return (new ActionForward(mapping.getInput()));
				}

				logger.debug("In people size = 1 condition");

				String addEditFlag = Constants.EDIT;
				logger.debug("people detail page flag is set to  " + addEditFlag);

				Iterator itr = peoples.iterator();

				if (itr.hasNext()) {
					people = (People) itr.next();
				}

				if (people != null) {
					strPeopleId = StringUtils.i2s(people.getPeopleId());
				}

				session.setAttribute("peopleId", strPeopleId);

				int peopleId = StringUtils.s2i(strPeopleId);
				logger.debug("People Id is Put in the session as " + strPeopleId);
				peopleType = new PeopleAgent().getPeopleTypeAsString(peopleId);
				logger.debug("got people type " + peopleType);
				session.setAttribute("peopleType", peopleType);

				ActionForm frm = new PeopleForm();
				peopleForm = (PeopleForm) frm;
				people = new PeopleAgent().getPeople(peopleId);

				// peopleForm.setPeople(new PeopleAgent().getPeople(peopleId));
				peopleForm.setPeople(people);
				peopleForm.setPsaId(psaId);
				peopleForm.setPsaType(psaType);
				peopleForm.setStrLicenseExpires(StringUtils.cal2str(people.getLicenseExpires()));
				peopleForm.setStrBusinessLicenseExpires(StringUtils.cal2str(people.getBusinessLicenseExpires()));
				peopleForm.setStrGeneralLiabilityDate(StringUtils.cal2str(people.getGeneralLiabilityDate()));
				peopleForm.setStrAutoLiabilityDate(StringUtils.cal2str(people.getAutoLiabilityDate()));
				peopleForm.setStrWorkersCompExpires(StringUtils.cal2str(people.getWorkersCompExpires()));

				List<People> associatedContractorAgentList = new ArrayList<People>();
				if(peopleType.equalsIgnoreCase(Constants.AGENT) || peopleType.equalsIgnoreCase(Constants.CONTRACTOR)){
					associatedContractorAgentList = new PeopleAgent().getAssociatedContractorOrAgent(peopleId,peopleType);
				}
				
				request.setAttribute("associatedContractorAgent", associatedContractorAgentList);
				request.setAttribute("peopleForm", frm);
				request.setAttribute("peopleId", strPeopleId);
				request.setAttribute("addEditFlag", addEditFlag);
				request.setAttribute("peopleType", peopleType);
				nextPage = peopleType;
				logger.debug("Next Page is set to  " + nextPage);
				request.setAttribute("people", people);
			} else {
				logger.debug("In people size != 1 condition");
				nextPage = "listPeople";

				PeopleSearchForm peopleSearchForm = new PeopleSearchForm();
				peopleSearchForm.setPsaId(psaId);
				peopleSearchForm.setPsaType(psaType);
				peopleSearchForm.setPeopleList(peoples);

				request.setAttribute("peopleList", peoples);
				request.setAttribute("peopleSearchForm", peopleSearchForm);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Exiting to (" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}

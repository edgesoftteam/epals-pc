package elms.control.actions.people;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.PeopleAgent;
import elms.app.admin.PeopleType;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SavePeopleAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SavePeopleAction.class.getName());
	ActionErrors errors = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		PeopleForm peopleForm = (PeopleForm) form;
		String nextPage = "editPeople";
		boolean dupLicenseNbr = false;
		String addEditFlag = "";
		String peopleLevel = "";
		String projectId = "";
		String psaId = "";
		String psaType = "";
		boolean changed = true;
		int count = 0;
		try {
			// compare old people form and the new one
			PeopleForm oldPeopleForm = (PeopleForm) session.getAttribute("oldPeopleForm");
			logger.debug("obtained the old people form from session ");

			psaId = peopleForm.getPsaId();
			logger.debug("Got the PSA_ID : " + psaId);
			psaType = peopleForm.getPsaType();
			logger.debug("Got the PSA_TYPE : " + psaType);

			if (psaType.equalsIgnoreCase("A")) {
				projectId = new ActivityAgent().getProjectId(psaId);
			} else {
				projectId = psaId;
			}
		} catch (Exception e) {
			logger.debug("Error getting PSA_ID, PSA_TYPE");
		}

		try {

			PeopleAgent peopleAgent = new PeopleAgent();
			String peopleTypeId = (String) session.getAttribute("peopleTypeId");
			User user = (User) session.getAttribute("user");
			logger.debug("People Type Id : " + peopleTypeId);

			PeopleType peopleType = peopleAgent.getPeopleTypeObj(peopleTypeId);
			logger.debug("People Type : " + peopleType.getTypeId());

			People people = null;
			logger.debug("created a new people object");
			people = peopleForm.getPeople();

			if (peopleForm.getPeople().getHicFlag().equals("on")) {
				people.setHicFlag("Y");
			} else {
				people.setHicFlag("N");
			}

			people.setPeopleType(peopleType);

			int peopleId = people.getPeopleId();
			logger.debug("people id is " + peopleId);

			peopleLevel = psaType;

			errors = new ActionErrors();
			String licenseNbr = people.getLicenseNbr();
			String strName = people.getName();
			logger.debug("License Number, People Type:" + licenseNbr + "," + peopleTypeId);

			if (peopleId <= 0) {
				logger.debug("People id is <= 0");
				if (people.getLicenseNbr() == null) {
					people.setLicenseNbr("");

				}

				if ((peopleTypeId.equalsIgnoreCase("C")) || (peopleTypeId.equalsIgnoreCase("E")) || (peopleTypeId.equalsIgnoreCase("R")) || (peopleTypeId.equalsIgnoreCase("D") || (peopleTypeId.equalsIgnoreCase("I")))) {
					if (!(people.getLicenseNbr().equalsIgnoreCase("")) && (people.getLicenseNbr().length() > 1)) {
						if (people.getLicenseNbr().substring(1, 2).equalsIgnoreCase("Z")) {
							String newLicenseNbr = "Z" + peopleAgent.getLicenseNbr();
							people.setLicenseNbr(newLicenseNbr);
						}
					}

					if (people.getLicenseNbr().equalsIgnoreCase("")) {
						String newLicenseNbr = "Z" + peopleAgent.getLicenseNbr();
						people.setLicenseNbr(newLicenseNbr);
					}

					dupLicenseNbr = peopleAgent.checkLicenseNbr(peopleType.getTypeId(), people.getLicenseNbr());

					if (dupLicenseNbr) {
						addEditFlag = Constants.ADD;
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.license.duplicate"));
						logger.debug("Duplicate License Number");
					}
				}

				if (errors.empty()) {
					people.setMoveInDate(peopleForm.getStrMoveInDate());
					people.setMoveOutDate(peopleForm.getStrMoveOutDate());
					people = peopleAgent.addPeople(people,user.getUserId());
					if (psaType.equals("")) {
						nextPage = "successSearch";
						request.setAttribute("people.peopleType.code", peopleTypeId);

						logger.debug("Returning from Add People");

						return (mapping.findForward(nextPage));
					}

					if (people == null) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.people.addFailed"));
					}

					logger.debug("AFTER ADDING PEOPLE");
					logger.debug("people.getCopyApplicant : " + people.getCopyApplicant());
					logger.debug("peopleForm.getPeople().getCopyApplicant : " + peopleForm.getPeople().getCopyApplicant());

					nextPage = "successList";
					logger.debug("IN ADD - NO COPYAPPLICANT");

					String[] selectedPeople = { StringUtils.i2s(people.getPeopleId()) };
					peopleAgent.assignPeople(selectedPeople, psaId, peopleLevel);
					session.setAttribute("peopleId", selectedPeople[0]);
				}
			} else {
				logger.debug("People id > 0 proceeding to save the data");
//				dupLicenseNbr = peopleAgent.checkLicenseNbr(peopleId, peopleType.getTypeId(), licenseNbr);
//
//				if (dupLicenseNbr) {
//					logger.debug("license number already exists, throwing error");
//					addEditFlag = Constants.EDIT;
//					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.license.duplicate"));
//				} else {
					errors = new ActionErrors();

					People oldPeople = (People) session.getAttribute("oldPeople");
					PeopleForm oldPeopleForm = (PeopleForm) session.getAttribute("oldPeopleForm");

					if(oldPeople !=null && people!=null )
					{
					count = peopleAgent.checkDuplicateVersion(people, peopleTypeId, peopleForm, oldPeople, oldPeopleForm);
					}
					logger.debug("The People Information Matched is " + count);

					logger.debug("license number does not exist, going forward with save");
				
					people = peopleAgent.setPeople(people, count,user.getUserId());

					logger.debug("");
					nextPage = "successList";
					logger.debug("IN EDIT - NO COPYAPPLICANT");

					String[] selectedPeople = { StringUtils.i2s(people.getPeopleId()) };

					peopleAgent.assignPeople(selectedPeople, psaId, peopleLevel);
					session.setAttribute("peopleId", selectedPeople[0]);

					if (people == null) {
						addEditFlag = Constants.EDIT;
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.people.updateFailed"));
					}
			//	}
			}

			logger.debug("Assigned the people to " + psaType);

			if (!errors.empty()) {
				saveErrors(request, errors);
				logger.debug("Action mapping input :" + mapping.getInput());

				String peopleType1 = (String) session.getAttribute("peopleType");
				logger.debug("obtained people type from session " + peopleType);
				request.setAttribute("peopleType", peopleType1);
				request.setAttribute("addEditFlag", addEditFlag);

				logger.debug("obtained people typeId from session " + peopleTypeId);
				request.setAttribute("peopleTypeId", peopleTypeId);

				peopleId = people.getPeopleId();
				logger.debug("peopleId" + peopleId);
				session.setAttribute("peopleId", StringUtils.i2s(peopleId));
				request.setAttribute("peopleId", StringUtils.i2s(peopleId));

				logger.debug("peopleId : " + peopleId);
				session.setAttribute("psaId", psaId);
				logger.debug("pasId" + psaId);
				session.setAttribute("psaType", psaType);
				logger.debug("psaType " + psaType);

				return (new ActionForward(mapping.getInput()));

			}

			peopleId = people.getPeopleId();
			session.setAttribute("peopleId", StringUtils.i2s(peopleId));
			logger.debug("peopleId : " + peopleId);
			session.setAttribute("psaId", psaId);
			logger.debug("pasId" + psaId);
			session.setAttribute("psaType", psaType);
			logger.debug("psaType " + psaType);
			request.setAttribute("peopleId", StringUtils.i2s(peopleId));
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				}
			} catch (Exception e) {
				e.getMessage();
			}
			if (GenericValidator.isBlankOrNull(psaId)) {

				return (mapping.findForward("peopleEditSuccess"));
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in SavePeopleAction "+e.getMessage(),e);
		}
		return (mapping.findForward(nextPage));
	}
}

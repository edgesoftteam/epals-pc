package elms.control.actions.people;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.FinanceAgent;
import elms.control.beans.PaymentMgrForm;
import elms.util.StringUtils;

public class ViewTransactionAction extends Action {

	static Logger logger = Logger.getLogger(ViewTransactionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering ViewTransactionAction");
		HttpSession session = request.getSession();
		// PeopleForm peopleForm = (PeopleForm) form;

		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) form;
		FinanceAgent financeAgent = new FinanceAgent();
		try {
			String peopleId = request.getParameter("peopleId");
			String psaId = request.getParameter("psaId");
			String psaType = request.getParameter("psaType");

			logger.debug("PeopleId :" + peopleId + " : PsaId: " + psaId + " : PsaType  : " + psaType);

			List deposits = new FinanceAgent().getDepositHistory(StringUtils.s2i(peopleId));
			logger.debug("obtained size results of size " + deposits.size());
			request.setAttribute("depositList", deposits);
			logger.debug(" Set depositList in ViewTransaction.java ");
			CachedRowSet rs = null;

			rs = financeAgent.depositDetails(StringUtils.s2i(peopleId));
			request.setAttribute("rs", rs);
			logger.debug(" Set Cashed Result Set in viewTransaction.java ");

			request.setAttribute("peopleId", peopleId);
			request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
			logger.info("Exiting ViewTransactionAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}
package elms.control.actions.people;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.PeopleAgent;
import elms.common.Constants;
import elms.control.actions.project.ViewActivityAction;
import elms.control.actions.project.ViewProjectAction;
import elms.control.beans.PeopleForm;
import elms.gsearch.GlobalSearch;
import elms.util.StringUtils;

public class AssignPeopleAction extends Action {

	static Logger logger = Logger.getLogger(AssignPeopleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		PeopleForm peopleForm = (PeopleForm) form;
		String psaType = "";

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		try {

			String[] selectedPeople = peopleForm.getSelectedPeople();
			logger.debug("obtained array of selected people elememts");

			psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			logger.debug("obtained psa type from session as " + psaType);

			String strPsaId = (String) session.getAttribute(Constants.PSA_ID);
			logger.debug("obtained psa id from session as " + strPsaId);
			int psaId = StringUtils.s2i(strPsaId);

			PeopleAgent peopleAgent = new PeopleAgent();
			peopleAgent.assignPeople(selectedPeople, strPsaId, psaType);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			if (psaType.equalsIgnoreCase("P")) {
				logger.debug("setting project parameters to view project action with psa id of  " + strPsaId);
				ViewProjectAction viewProjectAction = new ViewProjectAction();
				viewProjectAction.getProject(psaId, request);
			}
			if (psaType.equalsIgnoreCase("A")) {
				logger.debug("setting project parameters to view activity action with psa id of  " + strPsaId);
				ViewActivityAction viewActivityAction = new ViewActivityAction();
				viewActivityAction.getActivity(psaId, request);
			}
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				}
			} catch (Exception e) {
				e.getMessage();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(psaType));

	}

}
package elms.control.actions.people;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.agent.PeopleAgent;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.util.StringUtils;

public class EditPeopleAction extends Action {
	static Logger logger = Logger.getLogger(EditPeopleAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered in EditPeopleAction");
		HttpSession session = request.getSession();
		try {
			String addEditFlag = Constants.EDIT;
			int peopleId = -1;
			PeopleAgent peopleAgent = new PeopleAgent();
			String strPeopleId = request.getParameter("peopleId") != null ? (String) request.getParameter("peopleId") : (String) session.getAttribute("peopleId");
			logger.debug("peopleId from pram or session is " + strPeopleId);
			request.setAttribute("peopleId", strPeopleId);

			peopleId = StringUtils.s2i(strPeopleId);

			String peopleType = peopleAgent.getPeopleTypeAsString(peopleId);
			String peopleName = peopleAgent.getPeopleName(peopleId);
			logger.debug("got people type " + peopleType);
			session.setAttribute("peopleType", peopleType);
			session.setAttribute("peopleTypeId", peopleType);
			session.setAttribute("peopleName", peopleName);
			logger.debug("people Name ###### : " + peopleName);

			String psaId = (String) request.getParameter("psaId");
			if ((psaId == null) || (psaId == "")) {
				psaId = (String) session.getAttribute(Constants.PSA_ID);
			}

			logger.debug("obtained psa id from request as " + psaId);
			String psaType = (String) request.getParameter("psaType");
			if ((psaType == null) || (psaType == "")) {
				psaType = (String) session.getAttribute(Constants.PSA_TYPE);
			}

			logger.debug("obtained psa type from request as " + psaType);

			PeopleForm frm = new PeopleForm();
			frm.setPsaId(psaId);
			frm.setPsaType(psaType);

			People people;
			people = peopleAgent.getPeople(peopleId);

			if (people == null) {
				people = new People();
				logger.debug("people is null");
			}
			// getting and setting old people and people form object
			People oldPeople = (People) people;

			PeopleForm oldPeopleForm = (PeopleForm) frm;

			session.setAttribute("oldPeopleForm", oldPeopleForm);

			session.setAttribute("oldPeople", oldPeople);

			String hic = "N";
			hic = people.getHicFlag();
			if (hic == null) {
				hic = "N";
			}

			logger.debug("after new People");
			if (hic.equals("Y")) {
				people.setHicFlag("on");
			} else {
				people.setHicFlag("off");
			}

			logger.debug("after if");

			// This sets the people level type if they have plan check checked or not.
			String levelType = (request.getParameter("levelType") != null) ? request.getParameter("levelType") : "";
			logger.debug("level type obtained from request is " + levelType);
			if (levelType.equals("C")) {
				people.setPcPeople(true);
				people.setLevelType("Plan Check");
			} else if (levelType.equals("A")) {
				people.setPcPeople(false);
				people.setLevelType("Activity");
			} else if (levelType.equals("P")) {
				people.setPcPeople(false);
				people.setLevelType("Project");
			}
			
			if(levelType==null || levelType==""){
				String lsoId=request.getParameter("lsoId");
				if(lsoId!=null){
					
					levelType=LookupAgent.getLSOType(Integer.parseInt(lsoId));
					if(levelType.equals("O"))
					{
						request.setAttribute("levelType", "O");
						logger.debug("levelType is : O");
					}
					else if(levelType.equals("S"))
					{
						request.setAttribute("levelType", "S");
						logger.debug("levelType is : S");
					}
				}
			}
			String bLicenseNbr = "";
			String hasHold = new CommonAgent().checkwithHolds(StringUtils.i2s(peopleId), "Z");

			if (hasHold.length() > 0) {
				frm.setHasHold(hasHold);
			} else {
				frm.setHasHold("");
			}

			bLicenseNbr = people.getBusinessLicenseNbr();
			logger.debug("####license No from people : " + bLicenseNbr);
			if (bLicenseNbr == null) {
				bLicenseNbr = "-1";
			}

			request.setAttribute("licenseNbr", bLicenseNbr);

			frm.setPeople(people);
			frm.setStrLicenseExpires(StringUtils.cal2str(people.getLicenseExpires()));
			frm.setStrBusinessLicenseExpires(StringUtils.cal2str(people.getBusinessLicenseExpires()));
			frm.setStrGeneralLiabilityDate(StringUtils.cal2str(people.getGeneralLiabilityDate()));
			
			frm.setStrAutoLiabilityDate(StringUtils.cal2str(people.getAutoLiabilityDate()));
			frm.setStrWorkersCompExpires(StringUtils.cal2str(people.getWorkersCompExpires()));

			List<People> associatedContractorAgentList = new ArrayList<People>();
			if(peopleType.equalsIgnoreCase(Constants.AGENT) || peopleType.equalsIgnoreCase(Constants.CONTRACTOR)){
				associatedContractorAgentList = new PeopleAgent().getAssociatedContractorOrAgent(peopleId,peopleType);
			}
			
			request.setAttribute("associatedContractorAgent", associatedContractorAgentList);
			logger.debug("isExpired: " + frm.getIsExpired());
			logger.debug("people.isExpired: " + frm.getPeople().getIsExpired());
			request.setAttribute("addEditFlag", addEditFlag);
			request.setAttribute("peopleType", peopleType);
			request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
			request.setAttribute("peopleForm", frm);
			session.setAttribute("oldPeopleForm", frm);

			nextPage = peopleType;
			logger.debug("nextPage : " + nextPage);

			/*
			 * Checking the Dl Expiration Date String
			 */
			if ((people.getDlExpiryDate() != null) || (StringUtils.cal2str(people.getDlExpiryDate()) != "")) {
				logger.debug("in first if");
				if ((people.getDlExpiryDate()).before(Calendar.getInstance())) {
					logger.debug("in second if");
					people.setDlExpiryDateString("Y");
				} else {
					people.setDlExpiryDateString("N");
				}
			}
			request.setAttribute("people", people);

			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			logger.error(e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}

package elms.control.actions.planning;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.common.Constants;
import elms.control.beans.planning.LinkActivityForm;
import elms.util.db.Wrapper;

public class SaveLinkActivityAction extends Action {
	static Logger logger = Logger.getLogger(SaveLinkActivityAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering SaveLinkActivityAction");

		String nextPage = "samePage";
		String sql = "";
		HttpSession session = request.getSession();
		LinkActivityForm linkActivityForm = (LinkActivityForm) session.getAttribute("linkActivityForm");
		int activityId = linkActivityForm.getActivityId();

		if (request.getParameter("save") != null) {
			Wrapper db = new Wrapper();
			try {
				sql = "delete from activity_links where activity_id_from =" + activityId;
				logger.debug(sql);
				db.update(sql);
			} catch (Exception e) {
				logger.error("Error on delete of links");
			}

			db.beginTransaction();
			for (int i = 0; i < linkActivityForm.getAllLinkedActivitiesList().length; i++) {
				if (linkActivityForm.getAllLinkedActivitiesList()[i].getLinked().equals("on")) {

					sql = "insert into activity_links(activity_id_from,activity_id_to) values (" + activityId + "," + linkActivityForm.getAllLinkedActivitiesList()[i].getActivityId() + ")";
					logger.debug(sql);
					db.addBatch(sql);
				}
			}

			try {
				db.executeBatch();
			} catch (Exception e1) {
				throw new ServletException("Unable to execute batch " + e1.getMessage());
			}
			request.setAttribute("levelId", "" + activityId);
			request.setAttribute("levelType", "A");
			request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
			nextPage = "viewPSA";
		} else if (request.getParameter("cancel") != null) {
			request.setAttribute("levelId", "" + activityId);
			request.setAttribute("levelType", "A");
			request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
			nextPage = "viewPSA";
		}

		logger.info("Exiting ProcessResolutionAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}

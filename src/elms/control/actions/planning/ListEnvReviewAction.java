package elms.control.actions.planning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.LookupAgent;
import elms.agent.ActivityAgent;
import elms.app.planning.EnvReviewEdit;
import elms.common.Constants;
import elms.control.beans.planning.EnvReviewForm;
import elms.control.beans.planning.EnvReviewSummary;
import elms.security.User;
import elms.util.StringUtils;

public class ListEnvReviewAction extends Action {

	static Logger logger = Logger.getLogger(ListEnvReviewAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String levelType = "A";
		int levelId = 0;
		boolean addRow;

		try {
			logger.info("Entering ListEnvReviewAction");
			EnvReviewForm frm = (EnvReviewForm) form;

			/*
			 * String editable = (String) request.getParameter("editable"); levelType = (String) request.getParameter("level"); levelId = StringUtils.s2i((String) request.getParameter("levelId"));
			 */

			String editable = frm.getEditable();
			levelType = frm.getLevel();
			levelId = frm.getLevelId();

			logger.debug("Type:Level:Id " + levelType + ":" + levelId + ":" + editable);

			User user = (User) session.getAttribute(Constants.USER_KEY);
			String title = (String) session.getAttribute("lsoAddress") + (String) session.getAttribute("psaInfo");

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			List envReviewList = ActivityAgent.getEnvReviewList(levelType, levelId);
			logger.debug("EnvReview List Size " + envReviewList.size());

			EnvReviewEdit envReview;
			String status = "OK";
			for (int i = 0; i < envReviewList.size(); i++) {
				envReview = (EnvReviewEdit) envReviewList.get(i);

				if ("Categorical Exemption".equalsIgnoreCase(envReview.getEnvDetermination())) {
					status = "CE";
				} else if ("Negative Declaration".equalsIgnoreCase(envReview.getEnvDetermination())) {
					status = "ND";
				}
				// if (envReview.getEnvDetermination().equalsIgnoreCase("Categorical Exemption"))
				// status = "CE";
				// else if (envReview.getEnvDetermination().equalsIgnoreCase("Negative Declaration"))
				// status = "ND";
			}

			List envDetList = LookupAgent.getEnvDetList();
			session.setAttribute("envDetList", envDetList);
			session.setAttribute("envDetActionList", new ArrayList());

			EnvReviewForm envReviewForm = new EnvReviewForm();
			envReviewForm.setLevelId(levelId);
			envReviewForm.setLevel(levelType);
			envReviewForm.setEditable(editable);
			envReviewForm.setUserId("" + user.getUserId());
			if (envReviewList.size() == 0) {
				String today = StringUtils.cal2str(GregorianCalendar.getInstance());
				envReviewForm.setDeterminationDate(today);
				envReviewForm.setRequiredDeterminationDate(today);
				envReviewForm.setSubmittedDate(today);
			} else {
				EnvReviewEdit review = (EnvReviewEdit) envReviewList.get(0);
				envReviewForm.setDeterminationDate(review.getDeterminationDate());
				envReviewForm.setRequiredDeterminationDate(review.getReqDeterminationDate());
				envReviewForm.setSubmittedDate(review.getSubmitDate());
				List envDetActionList = LookupAgent.getEnvDetActionList(review.getEnvDeterminationId());
				session.setAttribute("envDetActionList", envDetActionList);
			}
			envReviewForm.setAddress(title);

			// remove the Env Review Header record. Can't remove it in sql since it has the
			// dates required for the form
			ListIterator iter = envReviewList.listIterator();
			while (iter.hasNext()) {
				EnvReviewEdit review = (EnvReviewEdit) iter.next();
				if (!review.getEnvReviewId().equals("0") && "0".equals(review.getEnvDeterminationId()))
					iter.remove();
			}
			envReviewForm.setEnvReviewList(envReviewList);

			EnvReviewSummary summary = ActivityAgent.getEnvReviewSummary(levelType, levelId);
			summary.setStatus(status);
			logger.debug("EnvReviewSummary.Status : " + status);
			envReviewForm.setEnvReviewSummary(summary);
			envReviewForm.setEnableCheckBox("no");
			request.setAttribute("levelType", levelType);
			session.setAttribute("envReviewForm", envReviewForm);

			logger.info("Exiting ListEnvReviewAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}

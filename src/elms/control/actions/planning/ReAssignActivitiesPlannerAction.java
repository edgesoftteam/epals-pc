/*
 * Created on Aug 10, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.actions.planning;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.control.beans.PlannerUpdateForm;

/**
 * @author rdhanapal
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ReAssignActivitiesPlannerAction extends Action {
	static Logger logger = Logger.getLogger(ViewPlannersWorkLoadAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		try {
			ActivityAgent planningAgent = new ActivityAgent();
			logger.info("Entering ViewPlannersWorkLoadAction");
			PlannerUpdateForm plannerUpdateForm = (PlannerUpdateForm) form;
			logger.debug("print size of List: " + plannerUpdateForm.getPlannerUpdateList().size());
			plannerUpdateForm.assignActivitiesToPlanners();

			// this below code for Refresh list
			plannerUpdateForm.getPlannersActivities();// this method for viewPlanners activity
			session.setAttribute("plannerUpdateForm", plannerUpdateForm);

		} catch (Exception e) {
			throw new ServletException("Exception occured in ViewPlannersWorkLoadAction " + e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}

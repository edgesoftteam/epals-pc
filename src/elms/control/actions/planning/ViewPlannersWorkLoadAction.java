package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.common.Constants;
import elms.control.beans.PlannerUpdateForm;

public class ViewPlannersWorkLoadAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ViewPlannersWorkLoadAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		session.removeAttribute(Constants.APN);

		String action = (request.getParameter("action") != null) ? request.getParameter("action") : "view";
		logger.debug("action: " + action);

		ActivityAgent planningAgent = new ActivityAgent();

		PlannerUpdateForm plannerUpdateForm = (PlannerUpdateForm) form;
		String forward = "success";

		try {

			if (action.equalsIgnoreCase("view")) {
				List planners = ActivityAgent.getPlanners();
				session.setAttribute("planners", planners);
				plannerUpdateForm.getPlannersActivities(); // this method for viewPlanners activity
				session.setAttribute("plannerUpdateForm", plannerUpdateForm);
				logger.info("Exiting ViewPlannersWorkLoadAction");
			} else if (action.equalsIgnoreCase("reassign")) {
				logger.debug("reassign...");
				forward = "reload";
				logger.debug("Size of List: " + plannerUpdateForm.getPlannerUpdateList().size());
			} else {
				throw new ServletException("No Mapping Defined");
			}
		} catch (Exception e) {
			throw new ServletException("Exception occured in ViewPlannersWorkLoadAction " + e.getMessage());
		}

		return (mapping.findForward(forward));
	}
}

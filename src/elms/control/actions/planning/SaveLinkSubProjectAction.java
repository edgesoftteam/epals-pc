//Source file: c:\\elms\\control\\actions\\dot\\EditDOTActivityAction.java

package elms.control.actions.planning;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.common.Constants;
import elms.control.beans.planning.LinkSubProjectForm;
import elms.security.User;
import elms.util.db.Wrapper;

public class SaveLinkSubProjectAction extends Action {

	static Logger logger = Logger.getLogger(SaveLinkSubProjectAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering ProcessResolutionAction");

		String nextPage = "samePage";
		String sql = "";
		ActionErrors errors = new ActionErrors();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		LinkSubProjectForm subProjectForm = (LinkSubProjectForm) session.getAttribute("linkSubProjectForm");
		int subProjectId = subProjectForm.getSubProjectId();

		if (request.getParameter("save") != null) {
			Wrapper db = new Wrapper();
			try {
				sql = "delete from sproj_links where sproj_id_from =" + subProjectId;
				logger.debug(sql);
				db.update(sql);
			} catch (Exception e) {
				logger.error("Error on delete of links");
			}

			db.beginTransaction();
			for (int i = 0; i < subProjectForm.getSubProjectList().length; i++) {
				if (subProjectForm.getSubProjectList()[i].getLinked().equals("on")) {

					sql = "insert into sproj_links (sproj_id_from,sproj_id_to) values (" + subProjectId + "," + subProjectForm.getSubProjectList()[i].getSubProjectId() + ")";
					logger.debug(sql);
					db.addBatch(sql);
				}
			}

			try {
				db.executeBatch();
			} catch (Exception e1) {
				throw new ServletException("Unable to execute batch " + e1.getMessage());
			}
			nextPage = "samePage";
		} else if (request.getParameter("cancel") != null) {
			request.setAttribute("levelId", "" + subProjectId);
			request.setAttribute("levelType", "Q");
			request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
			nextPage = "viewPSA";
		}

		logger.info("Exiting ProcessResolutionAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}
package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.app.planning.ResolutionEdit;
import elms.control.beans.planning.ResolutionForm;
import elms.util.StringUtils;

public class ListResolutionAction extends Action {

	static Logger logger = Logger.getLogger(ListResolutionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String levelType = "A";
		int levelId = 0;
		boolean addRow;

		try {
			logger.info("Entering ListResolutionAction");
			levelType = (String) request.getParameter("levelType");
			levelId = StringUtils.s2i((String) request.getParameter("levelId"));
			addRow = (request.getAttribute("addRow") != null) ? true : false;
			logger.debug(levelType + "--" + levelId);
			ResolutionForm resolutionForm = new ResolutionForm();

			List resolutionList = ActivityAgent.getResolutionList(levelType, levelId);
			if (addRow)
				resolutionList.add(new ResolutionEdit());

			logger.debug("Resolution List Size " + resolutionList.size());
			resolutionForm.setLevelId(levelId);
			resolutionForm.setLevelType(levelType);
			resolutionForm.setResolutionList(resolutionList);
			String title = (String) session.getAttribute("lsoAddress") + (String) session.getAttribute("psaInfo");
			resolutionForm.setAddress(title);
			logger.debug("Address" + resolutionForm.getAddress());
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			resolutionForm.setEnableCheckBox("no");
			session.setAttribute("resolutionForm", resolutionForm);
			request.setAttribute("size", resolutionList.size() + "");
			logger.info("Exiting ListResolutionAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}

package elms.control.actions.planning;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.control.beans.planning.CalendarFeatureForm;
import elms.util.StringUtils;

public class DeleteCalendarFeatureAction extends Action {
	static Logger logger = Logger.getLogger(DeleteCalendarFeatureAction.class.getName());
	String strSubProjectId;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		strSubProjectId = request.getParameter("subProjectId");
		CalendarFeatureForm calendarFeatureForm = (CalendarFeatureForm) form;
		try {

			logger.debug("Print  subProjectId: " + calendarFeatureForm.getSubProjectId());

			String[] selectedCalendarFeature = calendarFeatureForm.getSelectedRecord();
			logger.debug("obtained array of records: " + selectedCalendarFeature);

			logger.debug("sub project id: " + strSubProjectId);
			new ActivityAgent().deleteCalendarFeature(selectedCalendarFeature, StringUtils.s2i(strSubProjectId));

		} catch (Exception e) {
			logger.error("Exception occured while deleting calendarFeature: " + e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}

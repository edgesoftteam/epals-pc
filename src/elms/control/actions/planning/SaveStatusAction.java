package elms.control.actions.planning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.planning.StatusEdit;
import elms.common.Constants;
import elms.control.beans.planning.StatusForm;
import elms.util.StringUtils;

public class SaveStatusAction extends Action {
	static Logger logger = Logger.getLogger(SaveStatusAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering SaveStatusAction");

		String nextPage = "samePage"; // Default forwarding
		HttpSession session = request.getSession();
		StatusForm frm = (StatusForm) session.getAttribute("statusForm");
		String action = frm.getAction();

		List statuses = null;

		try {
			statuses = StringUtils.arrayToList(frm.getStatusList());
			logger.debug("Status List size :" + statuses.size());
		} catch (RuntimeException e) {
			logger.warn(e.getMessage());
		}

		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		if (action.equals("cancel")) {
			session.removeAttribute("statusForm");
			request.setAttribute("levelId", "" + frm.getLevelId());
			request.setAttribute("levelType", frm.getLevelType());
			request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
			session.removeAttribute("statusForm");
			nextPage = "viewPSA";
		} else if (action.equals("addRow")) {
			List statusList = new ArrayList();
			StatusEdit status = new StatusEdit();
			status.setAdded("Yes");
			status.setStatusDate(StringUtils.cal2str(GregorianCalendar.getInstance()));
			statusList.add(status);
			statusList.addAll(statuses);
			frm.setStatusList(statusList);
			session.setAttribute("statusForm", frm);
		} else if (action.equals("save")) {
			try {
				ActivityAgent.saveStatusList(frm);
			} catch (Exception e1) {
				throw new ServletException("Unable to save status list " + e1.getMessage());
			}
			request.setAttribute("levelId", "" + frm.getLevelId());
			request.setAttribute("levelType", frm.getLevelType());
			request.setAttribute("editable", frm.getEditable());
			session.removeAttribute("statusForm");
			nextPage = "listStatus";
		}

		// Forward control to the specified success URI
		logger.info("Exiting SaveStatusAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}

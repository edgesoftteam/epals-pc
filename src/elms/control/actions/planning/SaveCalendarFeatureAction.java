package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.planning.CalendarFeature;
import elms.control.beans.planning.CalendarFeatureForm;
import elms.util.StringUtils;

public class SaveCalendarFeatureAction extends Action {
	static Logger logger = Logger.getLogger(SaveCalendarFeatureAction.class.getName());
	int subProjectId = -1;
	int spCalendarId = -1;
	String strSpCalendarId = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entering to SaveCalendarFeatureAction...");

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		CalendarFeatureForm calendarFeatureForm = (CalendarFeatureForm) form;
		ActivityAgent planningAgent = new ActivityAgent();

		try {
			CalendarFeature calendarFeature = new CalendarFeature();
			strSpCalendarId = calendarFeatureForm.getSpCalendarFeatureId();

			if (strSpCalendarId == null) {
				strSpCalendarId = request.getParameter("spCalendarId");
			}

			if (strSpCalendarId == null) {
				strSpCalendarId = "0";
			}

			logger.debug("spCalendarId is: " + strSpCalendarId);
			calendarFeature.setCalendarFeatureId(StringUtils.s2i(strSpCalendarId));
			subProjectId = StringUtils.s2i(calendarFeatureForm.getSubProjectId());
			logger.debug("got Parameter subProjectId as: " + subProjectId);
			calendarFeature.setSubProjectId(subProjectId);
			logger.debug("set subProjectId as: " + calendarFeature.getSubProjectId());
			calendarFeature.setMeetingType(calendarFeatureForm.getMeetingType());
			logger.debug("set Meeting Type as : " + calendarFeature.getMeetingType());
			calendarFeature.setMeetingDate(calendarFeatureForm.getMeetingDate());
			logger.debug("set Meeting Date as: " + calendarFeature.getMeetingDate());
			calendarFeature.setActionType(calendarFeatureForm.getActionType());
			logger.debug("set ActionType as: " + calendarFeature.getActionType());
			calendarFeature.setComments(calendarFeatureForm.getComments());
			logger.debug("set Comments as: " + calendarFeature.getComments());

			if (strSpCalendarId.equalsIgnoreCase("0")) {
				// start this is validation for comments in edit Calendar Feature
				String tmpComments = calendarFeatureForm.getComments();
				int charCounts = tmpComments.length();

				logger.debug(" length " + charCounts);

				if (charCounts > 3999) {
					logger.debug("Comments more than permissible column length");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.MeetingCalendar.commentsMoreThanAllowed"));
				}

				if (!errors.empty()) {
					saveErrors(request, errors);

					List meetingTypeList = planningAgent.getMeetingTypeList();
					request.setAttribute("meetingTypeList", meetingTypeList);

					List actionTypeList = planningAgent.getActionTypeList(calendarFeature.getMeetingType());
					request.setAttribute("actionTypeList", actionTypeList);
					logger.debug(" print spCalendarid: " + calendarFeatureForm.getSpCalendarFeatureId());
					request.setAttribute("spCalendarId", calendarFeatureForm.getSpCalendarFeatureId());
					request.setAttribute("calendarFeatureForm", calendarFeatureForm);
					session.setAttribute("calendarFeatureForm", calendarFeatureForm);

					return (mapping.findForward("success"));
				}

				// edit this is validation for comments in edit Calendar Feature
				spCalendarId = planningAgent.insertCalendarFeature(calendarFeature);
			} else {
				// start this is validation for comments in edit Calendar Feature
				String tmpComments = calendarFeatureForm.getComments();
				int charCounts = tmpComments.length();

				logger.debug(" length " + charCounts);

				if (charCounts > 3999) {
					logger.debug("Comments more than permissible column length");
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.MeetingCalendar.commentsMoreThanAllowed"));
				}

				if (!errors.empty()) {
					saveErrors(request, errors);

					List meetingTypeList = planningAgent.getMeetingTypeList();
					request.setAttribute("meetingTypeList", meetingTypeList);

					List actionTypeList = planningAgent.getActionTypeList(calendarFeature.getMeetingType());
					request.setAttribute("actionTypeList", actionTypeList);
					logger.debug(" print spCalendarid: " + calendarFeatureForm.getSpCalendarFeatureId());
					request.setAttribute("spCalendarId", calendarFeatureForm.getSpCalendarFeatureId());
					request.setAttribute("calendarFeatureForm", calendarFeatureForm);
					session.setAttribute("calendarFeatureForm", calendarFeatureForm);

					return (mapping.findForward("reload"));
				}

				// start this is validation for comments in edit Calendar Feature
				spCalendarId = planningAgent.editCalendarFeature(calendarFeature);
			}
		} catch (Exception e) {
			logger.error("Exception thrown " + e.getMessage());

			return (mapping.findForward("error"));
		}

		return (mapping.findForward("success"));
	}
}

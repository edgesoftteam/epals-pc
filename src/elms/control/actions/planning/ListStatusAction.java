package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.app.planning.StatusEdit;
import elms.common.Constants;
import elms.control.beans.planning.StatusForm;
import elms.security.User;
import elms.util.StringUtils;

public class ListStatusAction extends Action {

	static Logger logger = Logger.getLogger(ListStatusAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		String levelType = "A", editable = "";
		int levelId = 0;
		boolean addRow;

		try {
			logger.info("Entering ListStatusAction");
			if (request.getParameter("levelId") == null) {
				levelId = StringUtils.s2i((String) request.getAttribute("levelId"));
				levelType = (String) request.getAttribute("levelType");
				editable = (String) request.getAttribute("editable");
			} else {
				levelId = StringUtils.s2i(request.getParameter("levelId"));
				levelType = request.getParameter("levelType");
				editable = request.getParameter("editable");
			}

			logger.debug(levelType + "--" + levelId + "--" + editable);

			User user = (User) session.getAttribute(Constants.USER_KEY);
			String title = (String) session.getAttribute("lsoAddress") + (String) session.getAttribute("psaInfo");

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			List statusList = ActivityAgent.getStatusList(levelType, levelId);
			logger.debug("Status List Size " + statusList.size());

			List displayList = ActivityAgent.getStatusDisplay(levelType, levelId);
			logger.debug("Display List Size " + displayList.size());

			session.setAttribute("statuses", displayList);

			StatusForm statusForm = new StatusForm();
			statusForm.setLevelId(levelId);
			statusForm.setLevelType(levelType);
			statusForm.setEditable(editable);
			statusForm.setStatusList(statusList);
			if (statusList.size() > 0)
				statusForm.setLastStatusId(((StatusEdit) statusList.get(0)).getPlanStatusId());
			else
				statusForm.setLastStatusId("200");
			statusForm.setDisplayList(displayList);
			statusForm.setUserId("" + user.getUserId());

			statusForm.setAddress(title);
			session.setAttribute("statusForm", statusForm);

			logger.info("Exiting ListStatusAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}

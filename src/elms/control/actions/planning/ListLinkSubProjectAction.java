package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ProjectAgent;
import elms.app.planning.LinkSubProjectEdit;
import elms.control.beans.planning.LinkSubProjectForm;
import elms.util.StringUtils;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class ListLinkSubProjectAction extends Action {

	static Logger logger = Logger.getLogger(ListLinkSubProjectAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		int subProjectId = 0;
		boolean addRow;

		try {
			logger.info("Entering ListLinkSubProjectAction");
			subProjectId = StringUtils.s2i((String) request.getParameter("subProjectId"));
			String editable = (String) request.getParameter("editable");
			addRow = (request.getAttribute("addRow") != null) ? true : false;
			logger.debug("Sub Project Id : " + subProjectId);
			LinkSubProjectForm subProjectForm = new LinkSubProjectForm();

			List subProjectList = ProjectAgent.getSubProjectList(subProjectId);
			if (addRow)
				subProjectList.add(new LinkSubProjectEdit());

			logger.debug("Resolution List Size " + subProjectList.size());
			subProjectForm.setSubProjectId(subProjectId);
			subProjectForm.setEditable(editable);
			subProjectForm.setSubProjectList(subProjectList);
			subProjectForm.setAddress((String) session.getAttribute("lsoAddress") + " " + (String) session.getAttribute("psaInfo"));

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			session.setAttribute("linkSubProjectForm", subProjectForm);
			logger.info("Exiting ListLinkSubProjectAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}

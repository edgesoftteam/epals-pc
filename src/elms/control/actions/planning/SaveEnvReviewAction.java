package elms.control.actions.planning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.ActivityAgent;
import elms.app.planning.EnvReviewEdit;
import elms.common.Constants;
import elms.control.beans.planning.EnvReviewForm;
import elms.util.StringUtils;

public class SaveEnvReviewAction extends Action {

	static Logger logger = Logger.getLogger(SaveEnvReviewAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering SaveEnvReviewAction");
		String nextPage = "samePage"; // Default forwarding
		HttpSession session = request.getSession();
		EnvReviewForm frm = (EnvReviewForm) session.getAttribute("envReviewForm");
		String action = frm.getAction();
		logger.debug("Action Entered is :: " + action);

		List reviews = null;
		try {
			reviews = StringUtils.arrayToList(frm.getEnvReviewList());
			logger.debug("Reviews size :" + reviews.size());
		} catch (RuntimeException e) {
			logger.warn(e.getMessage());
		}

		if (mapping.getAttribute() != null)
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());

		if (action.equals("cancel")) {
			session.removeAttribute("envReviewForm");
			request.setAttribute("levelId", "" + frm.getLevelId());
			request.setAttribute("levelType", frm.getLevel());
			request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
			nextPage = "viewPSA";
		} else if (action.equals("addRow")) {
			EnvReviewEdit areview = new EnvReviewEdit();
			areview.setDeterminationDate(frm.getDeterminationDate());
			areview.setReqDeterminationDate(frm.getRequiredDeterminationDate());
			areview.setSubmitDate(frm.getSubmittedDate());
			areview.setAdded("Yes");
			frm.setEnableCheckBox("Yes");

			List envReviewList = new ArrayList();
			envReviewList.add(areview);
			envReviewList.addAll(reviews);
			frm.setEnvReviewList(envReviewList);
			session.setAttribute("envReviewForm", frm);
			if (envReviewList.size() > 0) {
				EnvReviewEdit review = (EnvReviewEdit) envReviewList.get(0);
				List envDetActionList = new ArrayList();
				try {
					if (!GenericValidator.isBlankOrNull(review.getEnvDeterminationId())) {
						envDetActionList = LookupAgent.getEnvDetActionList(review.getEnvDeterminationId());
					}

				} catch (Exception e1) {
					envDetActionList = new ArrayList();
					logger.warn("Could not find list of envDetActionList, initializing it to blank arraylist");
				}
				session.setAttribute("envDetActionList", envDetActionList);
			}
		} else if (action.equals("save")) {
			logger.debug("Save Action in Progress ....");
			try {
				ActivityAgent.tmpsaveEnvReviewList(frm);
			} catch (Exception e2) {
				throw new ServletException("Unable to save env review list " + e2.getMessage());
			}
			ActivityAgent.saveEnvReviewSummary(frm.getEnvReviewSummary());
			reviews = StringUtils.arrayToList(frm.getEnvReviewList());
			if (reviews.size() > 0) {
				EnvReviewEdit review = (EnvReviewEdit) reviews.get(0);

				List envDetActionList;
				try {
					envDetActionList = LookupAgent.getEnvDetActionList(review.getEnvDeterminationId());
				} catch (Exception e1) {
					envDetActionList = new ArrayList();
					logger.warn("Could not find list of envDetActionList, initializing it to blank arraylist");
				}
				session.setAttribute("envDetActionList", envDetActionList);
			}
			frm.setEnableCheckBox("no");
			session.setAttribute("envReviewForm", frm);
			nextPage = "refresh";
		} else {
			logger.debug("Null Action in Progress ....");
			session.setAttribute("envReviewForm", frm);

			for (int i = 0; i < reviews.size(); i++) {
				EnvReviewEdit rev = (EnvReviewEdit) reviews.get(i);
				if (rev.getDeleteFlag().equalsIgnoreCase("on")) {
					rev.setDeleteFlag("");
				}
			}

			if (reviews.size() > 0) {
				EnvReviewEdit review = (EnvReviewEdit) reviews.get(0);
				List envDetActionList;
				try {
					envDetActionList = LookupAgent.getEnvDetActionList(review.getEnvDeterminationId());
				} catch (Exception e1) {
					envDetActionList = new ArrayList();
					logger.warn("Could not find list of envDetActionList, initializing it to blank arraylist");
				}
				session.setAttribute("envDetActionList", envDetActionList);

			}

		}
		// Forward control to the specified success URI
		request.setAttribute("levelType", frm.getLevel());
		logger.info("Exiting SaveEnvReviewAction(" + nextPage + ")");
		return (mapping.findForward(nextPage));
	}
}

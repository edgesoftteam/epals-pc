/*
 * Created on Aug 11, 2006
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.actions.planning;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.control.beans.PlannerUpdateForm;

/**
 * @author rdhanapal
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class PlanningRoutingAction extends Action {
	static Logger logger = Logger.getLogger(PlanningRoutingAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering PlanningRoutingAction");

		HttpSession session = request.getSession();
		PlannerUpdateForm frm = (PlannerUpdateForm) form;
		String userId = request.getParameter("userId");
		String userName = request.getParameter("userName");
		ActivityAgent planningAgent = new ActivityAgent();
		logger.info("Entering ViewPlannersWorkLoadAction");
		PlannerUpdateForm plannerUpdateForm = (PlannerUpdateForm) form;

		try {
			logger.debug("userId: " + userId);
			logger.debug("userName: " + userName);
			plannerUpdateForm.activityDetailsByPlanner(userId, userName);// this method for viewPlanners activity
			session.setAttribute("plannerUpdateForm", plannerUpdateForm);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException(e.getMessage());
		}
	}
}

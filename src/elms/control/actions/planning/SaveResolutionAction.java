package elms.control.actions.planning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.planning.ResolutionEdit;
import elms.common.Constants;
import elms.control.beans.planning.ResolutionForm;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class SaveResolutionAction extends Action {

	static Logger logger = Logger.getLogger(SaveResolutionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering SaveResolutionAction");
		List resolutionList = new ArrayList();
		String nextPage = "samePage";
		ActionErrors errors = new ActionErrors();
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		ResolutionForm resolutionForm = (ResolutionForm) session.getAttribute("resolutionForm");
		int levelId = resolutionForm.getLevelId();
		String levelType = resolutionForm.getLevelType();

		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		if (request.getParameter("save") != null) {
			logger.debug("Save Action in progress...");
			Wrapper db = new Wrapper();
			db.beginTransaction();
			int userId = user.getUserId();
			resolutionForm.setUserId(userId);
			try {
				ActivityAgent.saveResoultion(resolutionForm);
			} catch (Exception e) {
				logger.error("Exception while saving Resolution " + e.getMessage());
			}

			nextPage = "samePage";
			resolutionForm.setEnableCheckBox("no");
			session.setAttribute("resolutionForm", resolutionForm);
		} else if (request.getParameter("add") != null) {
			logger.debug("Add Action in progress...");
			final Calendar today = new GregorianCalendar();
			int year = today.get(Calendar.YEAR);
			String startDate = today.get(Calendar.MONTH) + "/" + today.get(Calendar.DATE) + "/" + today.get(Calendar.YEAR);

			ResolutionEdit resolutionEdit = new ResolutionEdit();
			resolutionEdit.setAdded("Yes");
			resolutionList.add(resolutionEdit);
			resolutionList.addAll(StringUtils.arrayToList(resolutionForm.getResolutionList()));
			resolutionForm.setResolutionList(resolutionList);
			nextPage = "samePage";
			resolutionForm.setEnableCheckBox("Yes");
			session.setAttribute("resolutionForm", resolutionForm);

		} else if (request.getParameter("cancel") != null) {
			logger.debug("Cancel Action in progress...");
			request.setAttribute("levelId", "" + levelId);
			request.setAttribute("levelType", levelType);
			request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
			nextPage = "viewPSA";
		}
		request.setAttribute("size", resolutionList.size() + "");
		logger.info("Exiting SaveResolutionAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}
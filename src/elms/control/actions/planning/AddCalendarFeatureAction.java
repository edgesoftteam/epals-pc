package elms.control.actions.planning;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.common.Constants;
import elms.control.beans.planning.CalendarFeatureForm;
import elms.util.StringUtils;

public class AddCalendarFeatureAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AddCalendarFeatureAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into AddCalendarFeatureAction ....");
		String strSubProjectId = "";
		HttpSession session = request.getSession();
		int subProjectId;
		ActivityAgent planningAgent = new ActivityAgent();
		try {

			strSubProjectId = request.getParameter("subProjectId");

			if ((strSubProjectId == null) || strSubProjectId.equals("")) {
				strSubProjectId = (String) session.getAttribute(Constants.PSA_ID);
			}

			subProjectId = StringUtils.s2i(strSubProjectId);
			CalendarFeatureForm calendarFeatureForm = new CalendarFeatureForm();

			calendarFeatureForm.setSubProjectId(strSubProjectId);

			// It is to display the History of Calender Features
			calendarFeatureForm.setCalendarFeatures(planningAgent.getAllCalendarFeaturesArray(StringUtils.s2i(strSubProjectId), 0));
			logger.debug("Calendar Feature  is set to Calendar Feature form");

			List meetingTypeList = planningAgent.getMeetingTypeList();
			request.setAttribute("meetingTypeList", meetingTypeList);

			List actionTypeList = new ArrayList();
			request.setAttribute("actionTypeList", actionTypeList);

			//calendarFeatureForm.setMeetingDate(StringUtils.cal2str(Calendar.getInstance()));
			//logger.debug("meeting Date" + calendarFeatureForm.getMeetingDate());

			request.setAttribute("calendarFeatureForm", calendarFeatureForm);

			request.setAttribute("subProjectId", strSubProjectId);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			return (mapping.findForward("error"));
		}

	}

}

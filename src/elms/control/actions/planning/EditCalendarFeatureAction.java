package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.planning.CalendarFeature;
import elms.control.beans.planning.CalendarFeatureForm;
import elms.util.StringUtils;

public class EditCalendarFeatureAction extends Action {
	static Logger logger = Logger.getLogger(EditCalendarFeatureAction.class.getName());
	String strSpCalendarId = "";
	int subProjectId = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			HttpSession session = request.getSession();
			strSpCalendarId = request.getParameter("spCalendarId");
			logger.debug("ygjkshdhlakshdadj" + strSpCalendarId);

			CalendarFeatureForm calendarFeatureForm = (CalendarFeatureForm) form;
			CalendarFeature calendarFeature = new CalendarFeature();
			ActivityAgent planningAgent = new ActivityAgent();

			String flag = request.getParameter("edit");
			flag = StringUtils.nullReplaceWithEmpty(flag);

			if (strSpCalendarId == null) {
				strSpCalendarId = (String) session.getAttribute("spCalendarId");
			}

			if (strSpCalendarId == null) {
				strSpCalendarId = (String) request.getAttribute("spCalendarId");
			}

			calendarFeatureForm.setSpCalendarFeatureId(strSpCalendarId);
			logger.debug("spCalendarFeatureId: " + calendarFeatureForm.getSpCalendarFeatureId());

			calendarFeature = planningAgent.getCalendarFeature(StringUtils.s2i(strSpCalendarId));
			calendarFeatureForm.setSpCalendarFeatureId(StringUtils.i2s(calendarFeature.getCalendarFeatureId()));
			logger.debug("calendarFeatureId: " + calendarFeatureForm.getSpCalendarFeatureId());

			calendarFeatureForm.setSubProjectId(StringUtils.i2s(calendarFeature.getSubProjectId()));
			logger.debug("subProjectId: " + calendarFeatureForm.getSubProjectId());

			if (flag.equals("edit")) {
				List actionTypeList = planningAgent.getActionTypeList(calendarFeatureForm.getMeetingType());
				request.setAttribute("actionTypeList", actionTypeList);
			} else {
				calendarFeatureForm.setMeetingType(calendarFeature.getMeetingType());
				logger.debug("meeting Type: " + calendarFeatureForm.getMeetingType());

				calendarFeatureForm.setActionType(calendarFeature.getActionType());
				logger.debug("action Type: " + calendarFeatureForm.getActionType());
			}

			calendarFeatureForm.setMeetingDate(calendarFeature.getMeetingDate());
			logger.debug("meeting Date: " + calendarFeatureForm.getMeetingDate());

			calendarFeatureForm.setComments(StringUtils.nullReplaceWithEmpty(calendarFeature.getComments()));
			logger.debug("coemments: " + calendarFeatureForm.getComments());

			List meetingTypeList = planningAgent.getMeetingTypeList();
			request.setAttribute("meetingTypeList", meetingTypeList);

			if (!flag.equals("edit")) {
				List actionTypeList = planningAgent.getActionTypeList(calendarFeature.getMeetingType());
				request.setAttribute("actionTypeList", actionTypeList);
			}

			request.setAttribute("calendarFeatureForm", calendarFeatureForm);
			request.setAttribute("spCalendarId", strSpCalendarId);
			request.setAttribute("subProjectId", StringUtils.i2s(calendarFeature.getSubProjectId()));

		} catch (Exception e) {
			logger.error("Exception Occured in EditCalendarFeature: " + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

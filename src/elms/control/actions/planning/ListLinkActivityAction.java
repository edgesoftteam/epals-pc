package elms.control.actions.planning;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.app.planning.LinkActivityEdit;
import elms.common.Constants;
import elms.control.beans.planning.LinkActivityForm;
import elms.util.StringUtils;

public class ListLinkActivityAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ListLinkActivityAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		int activityId = 0;

		boolean addRow;

		try {
			activityId = StringUtils.s2i((String) request.getParameter("activityId"));

			String editable = (String) request.getParameter("editable");
			addRow = (request.getAttribute("addRow") != null) ? true : false;
			logger.debug("activityId : " + activityId);
			String active = (String) request.getParameter("active") != null ? request.getParameter("active") : "N";
			String val = Constants.PROJECT_NAME_BUILDING_ID + "," + Constants.PROJECT_NAME_PLANNING_ID + "," + Constants.PROJECT_NAME_LICENSE_AND_CODE_ID + "," + Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES_ID + "," + Constants.PROJECT_NAME_PUBLIC_WORKS_ID + "," + Constants.PROJECT_NAME_PARKING_ID;
			String prjNameId = (String) request.getParameter("prjNameId") != null ? request.getParameter("prjNameId") : val;

			LinkActivityForm linkActivityForm = new LinkActivityForm();

			List allLinkedActivitiesList = ProjectAgent.getAllLinkedActivitiesList(activityId, prjNameId, active);

			if (addRow) {
				allLinkedActivitiesList.add(new LinkActivityEdit());
			}

			logger.debug("Resolution List Size " + allLinkedActivitiesList.size());
			linkActivityForm.setActivityId(activityId);
			linkActivityForm.setEditable(editable);
			linkActivityForm.setActive(active);

			String[] prList = StringUtils.stringtoArray(prjNameId, ",");
			linkActivityForm.setPrjList(prList);
			linkActivityForm.setAllLinkedActivitiesList(allLinkedActivitiesList);
			linkActivityForm.setAddress((String) session.getAttribute("lsoAddress") + " " + (String) session.getAttribute("psaInfo"));

			List projectNames = LookupAgent.getProjectNames();
			request.setAttribute("projectNames", projectNames);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			session.setAttribute("linkActivityForm", linkActivityForm);
			logger.info("Exiting ListLinkActivityAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

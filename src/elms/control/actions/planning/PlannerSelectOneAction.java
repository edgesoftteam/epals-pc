package elms.control.actions.planning;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.app.planning.Planner;
import elms.app.planning.PlannerUpdateRecord;
import elms.control.beans.PlannerUpdateForm;

public class PlannerSelectOneAction extends Action {

	public PlannerSelectOneAction() {
	}

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		org.apache.struts.util.MessageResources messages = getResources();
		HttpSession session = request.getSession();
		logger.info("Entering PlannerSelectAllAction");
		PlannerUpdateForm plannerUpdateForm = (PlannerUpdateForm) form;
		try {
			Planner plannerArray[] = plannerUpdateForm.getPlannerArray();
			int count = 0;
			for (int i = 0; i < plannerArray.length; i++) {
				count = 0;
				PlannerUpdateRecord plannerUpdateRecord[] = plannerArray[i].getPlannerUpdateRecord();
				for (int j = 0; j < plannerUpdateRecord.length; j++)
					if (plannerUpdateRecord[j].getCheck().equals("true"))
						count++;
					else
						plannerArray[i].setSelectAllCheckBox("false");

				if (count > 0 && plannerUpdateRecord.length > 0)
					if (count == plannerUpdateRecord.length)
						plannerArray[i].setSelectAllCheckBox("on");
					else
						plannerArray[i].setSelectAllCheckBox("false");
			}

			plannerUpdateForm.setPlannerArray(plannerArray);
			session.setAttribute("plannerUpdateForm", plannerUpdateForm);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return mapping.findForward("success");
	}

	static Logger logger;

	static {
		logger = Logger.getLogger(elms.control.actions.planning.PlannerSelectOneAction.class.getName());
	}
}

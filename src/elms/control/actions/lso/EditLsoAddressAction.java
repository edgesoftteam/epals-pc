package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.LsoAddress;
import elms.control.beans.LsoForm;
import elms.util.StringUtils;

public class EditLsoAddressAction extends Action {

	static Logger logger = Logger.getLogger(EditLsoAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {

			int addressId = -1;
			String strAddressId = request.getParameter("addressId");
			addressId = StringUtils.s2i(strAddressId);
			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);
			LsoAddress lsoAddress = null;

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			LsoForm frm = new LsoForm();

			request.setAttribute("lsoId", strLsoId);
			request.setAttribute("addressId", strAddressId);
			logger.debug("setting address id " + strAddressId);

			logger.debug("before getting land address, lsoId=" + lsoId);
			lsoAddress = new AddressAgent().getLsoAddress(addressId);
			frm.setLsoAddress(lsoAddress);

			String lsoType = new AddressAgent().getLsoType(lsoId);
			String lsoTypeFull = "";
			if (lsoType.equalsIgnoreCase("L"))
				lsoTypeFull = "Land";
			else if (lsoType.equalsIgnoreCase("S"))
				lsoTypeFull = "Structure";
			else if (lsoType.equalsIgnoreCase("O"))
				lsoTypeFull = "Occupancy";

			frm.setLsoType(lsoType);
			frm.setLsoTypeFull(lsoTypeFull);

			request.setAttribute("lsoForm", frm);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}
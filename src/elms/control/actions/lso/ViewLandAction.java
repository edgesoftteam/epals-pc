package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.AddressAgent;
import elms.app.common.Condition;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.LandDetails;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.common.Constants;
import elms.control.beans.ConditionForm;
import elms.control.beans.ViewLandForm;
import elms.util.StringUtils;

public class ViewLandAction extends Action {

	static Logger logger = Logger.getLogger(ViewLandAction.class.getName());
	protected int landId;
	String apn = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ViewLandForm viewLandForm = (ViewLandForm) form;

		landId = StringUtils.s2i(request.getParameter("lsoId"));

		try {

			apn = viewLandForm.getApn();
			logger.debug("obtained apn from form is " + apn);
			processLand(request, landId, apn);
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

	public void processLand(HttpServletRequest request, int intLsoId, String apn) throws Exception {

		HttpSession session = request.getSession(true);
		session.setAttribute(Constants.LSO_ID, StringUtils.i2s(intLsoId));
		session.setAttribute(Constants.LSO_TYPE, Constants.LAND);
		//Added by Mukesh for setting the id,type and level.
		session.setAttribute(Constants.PSA_ID, StringUtils.i2s(intLsoId));
		session.setAttribute(Constants.PSA_TYPE, Constants.LAND);
		session.setAttribute("level", Constants.LAND);
		
		logger.debug("LSO_ID is set inthe session as  " + (String) session.getAttribute(Constants.LSO_ID));
		session.removeAttribute("siteStructureForm");

		ViewLandForm viewLandForm = new ViewLandForm();
		viewLandForm.setApn(apn);
		viewLandForm.setLsoId(StringUtils.i2s(intLsoId));

		try {

			Land land = new AddressAgent().getLand(intLsoId);
			if (land == null) {
				land = new Land();
			} else {

				String lsoId = StringUtils.i2s(land.getLsoId());
				logger.debug("setting lsoId to the request " + lsoId);
				request.setAttribute("lsoId", lsoId);
				AddressAgent lsoAgent = new AddressAgent();

				LandDetails landDetails = land.getLandDetails();
				if (landDetails == null) {
					landDetails = new LandDetails();
				} else {

					viewLandForm.setAlias(landDetails.getAlias());
					viewLandForm.setDescription(landDetails.getDescription());
					viewLandForm.setZone(landDetails.getZoneAsString());
					viewLandForm.setUse(landDetails.getUseAsString());
					viewLandForm.setCoordinateX(landDetails.getXCordinate());
					viewLandForm.setCoordinateY(landDetails.getYCordinate());
					viewLandForm.setActive(StringUtils.yn(landDetails.getActive()));
					viewLandForm.setAddressRangeList(lsoAgent.getAddressRangeList(StringUtils.s2i(lsoId)));
					viewLandForm.setLabel(landDetails.getLabel());
					viewLandForm.setHighFireArea(landDetails.getHighFireArea());
					String pZone = "";
					if (landDetails.getParkingZone() != null) {
						pZone = landDetails.getParkingZone().getName();
					}

					viewLandForm.setParkingZone(pZone);
					String rzone = landDetails.getRZone();
					if (rzone == null) {
						rzone = "";
					}

					if (rzone.equals("66")) {
						viewLandForm.setResZone("R-1");
					} else if (rzone.equals("67")) {
						viewLandForm.setResZone("R-4");
					} else if (rzone.equals("68")) {
						viewLandForm.setResZone("None");
					} else if (rzone.equals("69")) {
						viewLandForm.setResZone("C-1");
					}
				}

				logger.debug("Setting landAddress " + lsoId);
				List landAddress = land.getLandAddress();
				if (landAddress != null) {

					if (landAddress.size() > 0) {

						LandAddress landAddressObj = (LandAddress) landAddress.get(0);
						if (landAddressObj != null) {

							viewLandForm.setAddressStreetNumber(StringUtils.i2s(landAddressObj.getStreetNbr()));
							viewLandForm.setAddressStreetModifier(landAddressObj.getStreetModifier());
							Street addressStreet = landAddressObj.getStreet();
							if (addressStreet != null) {

								viewLandForm.setAddressStreetName(StringUtils.nullReplaceWithEmpty(addressStreet.getStreetName()));
								logger.debug(" Street Name " + viewLandForm.getAddressStreetName());
								String lsoAddress = landAddressObj.getStreetNbr() + " " + StringUtils.nullReplaceWithEmpty(landAddressObj.getStreetModifier()) + " " + StringUtils.nullReplaceWithEmpty(addressStreet.getStreetName());
								session.setAttribute("lsoAddress", lsoAddress);
								logger.debug("The Attribute lsoAddress is set in the session as " + lsoAddress);
								session.removeAttribute("psaInfo");
							}

							viewLandForm.setAddressCity(landAddressObj.getCity());
							viewLandForm.setAddressState(landAddressObj.getState());
							viewLandForm.setAddressUnit(landAddressObj.getUnit());
							logger.debug(" got the unit number as " + viewLandForm.getAddressUnit());
							viewLandForm.setAddressZip(landAddressObj.getZip());
							viewLandForm.setAddressZip4(landAddressObj.getZip4());
							viewLandForm.setAddressPrimary(StringUtils.yn(landAddressObj.getPrimary()));
							viewLandForm.setAddressActive(StringUtils.yn(landAddressObj.getActive()));
							viewLandForm.setAddressDescription(landAddressObj.getDescription());
						}
					}
				}

				List landApn = land.getLandApn();
				if (landApn != null) {

					if (landApn.size() == 0) {

						request.setAttribute("landApnList", new ArrayList());
					} else {

						request.setAttribute("landApnList", landApn);
						logger.debug("set land apn list to request with size" + landApn.size());
						int j = 0;
						for (int i = 0; i < landApn.size(); i++) {

							LandApn landApnObj = (LandApn) landApn.get(i);
							if ((landApnObj.getApn()).equals(apn)) {
								j = i;
							}
						}

						logger.debug("selecting the element number of " + j + " from land list");
						LandApn landApnObj = (LandApn) landApn.get(j);
						if (landApnObj != null) {

							apn = landApnObj.getApn();
							viewLandForm.setApn(apn);
							session.setAttribute(Constants.APN, apn);
							Owner owner = landApnObj.getOwner();
							if (owner != null) {

								viewLandForm.setApnOwnerId(StringUtils.i2s(owner.getOwnerId()));
								viewLandForm.setApnOwnerName(owner.getName());
								viewLandForm.setApnEmail(owner.getEmail());
								viewLandForm.setApnPhone(owner.getPhone());
								viewLandForm.setApnFax(owner.getFax());
								viewLandForm.setApnForeignFlag(owner.getForeignFlag());

								Address localAddress = owner.getLocalAddress();
								if (localAddress != null) {

									viewLandForm.setApnStreetNumber(StringUtils.zeroReplaceWithEmpty(localAddress.getStreetNbr()));

									viewLandForm.setApnStreetModifier(localAddress.getStreetModifier());
									viewLandForm.setApnCity(localAddress.getCity());
									viewLandForm.setApnState(localAddress.getState());
									viewLandForm.setApnZip(localAddress.getZip());
									Street localAddressStreet = localAddress.getStreet();
									if (localAddressStreet != null) {

										viewLandForm.setApnStreetName(StringUtils.nullReplaceWithEmpty(localAddressStreet.getStreetName()));
									}
								}

								ForeignAddress foreignAddress = owner.getForeignAddress();
								if (foreignAddress != null) {

									viewLandForm.setApnForeignAddress1(foreignAddress.getLineOne());
									viewLandForm.setApnForeignAddress2(foreignAddress.getLineTwo());
									viewLandForm.setApnForeignAddress3(foreignAddress.getLineThree());
									viewLandForm.setApnForeignAddress4(foreignAddress.getLineFour());
									viewLandForm.setApnCountry(foreignAddress.getCountry());
								}
							}
						}
					}
				}
				String mapAddressString = viewLandForm.getgMapAddressString();
				request.setAttribute("mapAddressString", mapAddressString);


				request.setAttribute("viewLandForm", viewLandForm);

				List holds = new ArrayList();
				holds = land.getLsoHold();
				request.setAttribute("holds", holds);
				logger.debug("set Holds to request with size " + holds.size());
				if (new CommonAgent().editable(intLsoId, "L")) {
					request.setAttribute("editable", "true");
				} else {
					request.setAttribute("editable", "false");
				}
				
				// setting Tenant List
				List tenants = new ArrayList();
				tenants = land.getLsoTenant();
				request.setAttribute("tenants", tenants);
				logger.debug("set tenants to request with size " + tenants.size());


				// **** Condition List
				ConditionForm condForm = new ConditionForm();
				CommonAgent condAgent = new CommonAgent();
				condForm.setLevelId(StringUtils.s2i(lsoId));
				condForm.setConditionLevel("L");
				condForm = condAgent.populateConditionForm(condForm);
				Condition[] conditions = condForm.getCondition();
				List condList = Arrays.asList(conditions);
				if (condList.size() > 3) {

					condList = condList.subList(0, 3);
				}

				request.setAttribute("condList", condList);

				// list of comments for this lso.
				List commentList = new CommonAgent().getCommentList(intLsoId, 3, "L");
				request.setAttribute("commentList", commentList);

				// list of attachments for this lso.
				List attachmentList = new CommonAgent().getAttachments(intLsoId, "L");
				request.setAttribute("attachmentList", attachmentList);

				logger.debug("set conditions to request with size " + condList.size());

				String onloadAction = (String) session.getAttribute("onloadAction");
				onloadAction = "parent.f_lso.location.href='" + request.getContextPath() + "/lsoSearch.do?lsoNodeId=" + intLsoId + "'";
				String from = request.getParameter("from")!=null ? request.getParameter("from") : "";
				if(from.equalsIgnoreCase("tree")){
					onloadAction = "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + intLsoId + "'"; 
				}
				logger.debug("Setting the refresh tree flag to " + onloadAction);
				session.setAttribute("onloadAction", onloadAction);
				session.removeAttribute("siteStructureForm");

			}
		} catch (Exception e) {

			logger.error("Error in viewlandAction"+e+e.getMessage());
			throw e;
		}
	}
}

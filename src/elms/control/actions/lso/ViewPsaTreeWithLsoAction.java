package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ProjectAgent;
import elms.app.lso.PsaTree;
import elms.control.beans.PsaTreeForm;

public class ViewPsaTreeWithLsoAction extends Action {

	static Logger logger = Logger.getLogger(ViewPsaTreeWithLsoAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewPsaTreeWithLsoAction");

		HttpSession session = request.getSession();

		// setting the onloadAction of next page not to refresh PSA tree
		session.setAttribute("onloadAction", "");

		ProjectAgent psaTreeAgent = new ProjectAgent();

		PsaTreeForm frm = (PsaTreeForm) session.getAttribute("psaTreeForm");
		if (frm == null)
			frm = new PsaTreeForm();

		String lsoStr = request.getParameter("lsoId");

		long lsoId = 0;

		try {
			lsoId = new Long(lsoStr).longValue();
		} catch (NumberFormatException e) {
			// do nothing
		}

		frm.setRadiobutton("radio_all");

		boolean active = !(frm.getRadiobutton().equals("radio_all"));
		// true gets only active projects
		// false gets all the projects
		List psaTreeList = null;
		try {
			psaTreeList = psaTreeAgent.getPsaTreeList(lsoId, active, request.getContextPath()); // getPsaTree(long lsoId, boolean active)
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}

		logger.debug("lsoId is:" + lsoId);
		logger.debug("boolean is:" + active);

		// Create all the javascript out here instead of in the browser
		List treeLinks = new ArrayList();
		StringBuffer link = new StringBuffer();
		for (int i = 0; i < psaTreeList.size(); i++) {
			PsaTree item = (PsaTree) psaTreeList.get(i);
			link = new StringBuffer();
			link.append("Tree[" + i + "]=\"" + item.getNodeId() + "|");
			link.append(item.getNodeLevel() + "|" + "");
			link.append((item.isOnHold() == true ? "<font color='" + (item.getHoldColor() != null ? item.getHoldColor() : "black") + "'><b>(H)</b></font>" : ""));
			link.append(item.getNodeText() + "|");
			link.append(item.getNodeLink() + "|");
			link.append(item.getNodeAlt() + "|");
			link.append(item.getLsoType() + "|");
			link.append(item.getStatus() + "|");
			link.append(item.getId() + "|\";");
			treeLinks.add(link.toString());
			logger.debug(link);
		}

		// request.setAttribute("lsoId", lsoStr);
		session.setAttribute("psaTreeForm", frm);
		request.setAttribute("psaTreeList", treeLinks);

		logger.info("Exiting ViewPsaTreeWithLsoAction (" + psaTreeList.size() + ")");
		return (mapping.findForward("success"));

	}

}
package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.admin.ParkingZone;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.OccupancyDetails;
import elms.app.lso.OccupancySiteData;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.app.lso.Use;
import elms.common.Constants;
import elms.control.beans.OccupancyForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveOccupancyAction extends SaveLsoAction {

	static Logger logger = Logger.getLogger(SaveOccupancyAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		OccupancyForm occupancyForm = (OccupancyForm) form;

		ActionErrors errors = new ActionErrors();
		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			return (new ActionForward(mapping.getInput()));
		} else {
			try {

				String structureId = request.getParameter("parentId");
				int intStructureId = StringUtils.s2i(structureId);

				OccupancyDetails occupancyDetails = this.getOccupancyDetails(occupancyForm);
				logger.debug("After getOccupancyDetails ");
				List occupancyAddress = this.getOccupancyAddressList(occupancyForm, session);
				logger.debug("After getOccupancyAddressList ");
				List occupancyApn = this.getOccupancyApnList(request, occupancyForm);
				logger.debug("After getOccupancyApnList");
				
				logger.debug("getHouseholdSize  "+occupancyForm.getHouseholdSize());

				List occupancyHold = new ArrayList();
				List occupancyCondition = new ArrayList();
				List occupancyAttachment = new ArrayList();
				List occupancyComment = new ArrayList();
				OccupancySiteData occupancySiteData = new OccupancySiteData();
				Occupancy occupancy = new Occupancy(0, intStructureId, occupancyDetails, occupancyAddress, occupancyApn, occupancyHold, occupancyCondition, occupancyAttachment, occupancySiteData, occupancyComment);
				logger.debug("Occupancy object created for new occupancy creation");

				AddressAgent occupancyAgent = new AddressAgent();
				int occupancyId = occupancyAgent.addOccupancy(occupancy);

				// occupancy = occupancyAgent.getOccupancy(occupancyId);

				if (occupancy != null) {
					String apn = occupancyForm.getApn(); // check for use
					int lsoId = occupancy.getLsoId();
					logger.debug("obtained occupancy for id as " + lsoId);
					ViewOccupancyAction viewOccupancyAction = new ViewOccupancyAction();
					viewOccupancyAction.processOccupancy(request, lsoId, apn);
					logger.debug("set view occupancy action with reqd values");

				}

				logger.debug("request set to the page with occupancy object");
			} catch (Exception e) {
				logger.error("Problem while adding occupancy :" + e.getMessage());
			}
		}

		return (mapping.findForward("success"));

	}

	public OccupancyDetails getOccupancyDetails(OccupancyForm occupancyForm) {
		OccupancyDetails occupancyDetails = null;
		try {

			// use list object here
			List useList = new ArrayList();
			String[] use = occupancyForm.getSelectedUse();
			logger.debug("Length of Use : " + use.length);
			for (int i = 0; i < use.length; i++) {

				AddressAgent useAgent = new AddressAgent();
				Use useObj = useAgent.getUse(use[i], "O");
				useList.add(useObj);
			}
			logger.debug("Use List created for new occupancy creation");

			// String active = (occupancyForm.getActive())?"Y":"N";
			occupancyDetails = new OccupancyDetails(occupancyForm.getAlias(), occupancyForm.getDescription(), useList, StringUtils.b2s(occupancyForm.getActive()), occupancyForm.getUnitNo(), occupancyForm.getBeginningFloor(), occupancyForm.getEndingFloor(), occupancyForm.getLabel());
			ParkingZone pzone = new ParkingZone();
			pzone.setPzoneId(occupancyForm.getParkingZone());
			occupancyDetails.setParkingZone(pzone);
			occupancyDetails.setRZone(occupancyForm.getResZone());
			occupancyDetails.setHouseholdSize(occupancyForm.getHouseholdSize());
			occupancyDetails.setBedroomSize(occupancyForm.getBedroomSize());
			occupancyDetails.setGrossRent(occupancyForm.getGrossRent());
			occupancyDetails.setUtilityAllowance(occupancyForm.getUtilityAllowance());
			occupancyDetails.setPropertyManager(occupancyForm.getPropertyManager());
			occupancyDetails.setRestrictedUnits(occupancyForm.getRestrictedUnits());

			logger.debug("Occupancy details object created for new occupancy creation");

		} catch (Exception e) {
			logger.error("Thrown in getOccupancy Details of save occupancy action");
		}
		return occupancyDetails;
	}

	public List getOccupancyAddressList(OccupancyForm occupancyForm, HttpSession session) {
		List occupancyAddress = new ArrayList();
		Street street = new Street();
		try {
			int streetNumber = 0;
			String strAddressStreetNumber = occupancyForm.getStreetNumber();
			String strAddressStreetName = occupancyForm.getStreetName();

			if ((strAddressStreetNumber != null) && !(strAddressStreetNumber.equals("")))
				streetNumber = Integer.parseInt(strAddressStreetNumber);

			if ((strAddressStreetName != null) && !(strAddressStreetName.equals(""))) {
				AddressAgent streetAgent = new AddressAgent();
				street = streetAgent.getStreet(Integer.parseInt(strAddressStreetName));
			}
			OccupancyAddress occupancyAddressObj = new OccupancyAddress(0, 0, "O", streetNumber, occupancyForm.getStreetMod(), street, occupancyForm.getUnit(), occupancyForm.getCity(), occupancyForm.getState(), occupancyForm.getZip(), occupancyForm.getZip4(), occupancyForm.getAddrDescription(), StringUtils.b2s(occupancyForm.getPrimary()), StringUtils.b2s(occupancyForm.getActiveCheck()));
			occupancyAddressObj.setUpdatedBy((User) session.getAttribute("user"));

			occupancyAddress.add(occupancyAddressObj);
			logger.debug("Occupancy address object created for new occupancy creation");
		} catch (Exception e) {
			logger.error("Thrown in getOccupancy address of save occupancy action");
		}
		return occupancyAddress;
	}

	public List getOccupancyApnList(HttpServletRequest request, OccupancyForm occupancyForm) {
		HttpSession session = request.getSession();
		List occupancyApn = new ArrayList();
		try {
			OccupancyApn occupancyApnObj = new OccupancyApn();
			if ((occupancyForm.getApn() != null) && !(occupancyForm.getApn().equals(""))) {
				logger.debug(" Apn is not null or empty ");
				occupancyApnObj.setApn(occupancyForm.getApn());
				int apnStreetNumber = 0;
				String strApnStreetNumber = occupancyForm.getApnStreetNumber();
				if ((strApnStreetNumber != null) && !(strApnStreetNumber.equals("")))
					apnStreetNumber = Integer.parseInt(strApnStreetNumber);

				Street apnStreet = new Street();
				AddressAgent streetAgent = new AddressAgent();
				apnStreet.setStreetName(occupancyForm.getApnStreetName());

				String apnZip4 = "";
				Address localAddress = new Address(apnStreetNumber, occupancyForm.getApnStreetMod(), apnStreet, occupancyForm.getApnCity(), occupancyForm.getApnState(), occupancyForm.getApnZip(), apnZip4);
				ForeignAddress foreignAddress = new ForeignAddress(occupancyForm.getLine1(), occupancyForm.getLine2(), occupancyForm.getLine3(), occupancyForm.getLine4(), occupancyForm.getCountry());
				String foreignFlag = occupancyForm.getForeignAddress();
				Owner owner = new Owner(0, occupancyForm.getOwnerName(), localAddress, foreignFlag, foreignAddress, occupancyForm.getPhone(), occupancyForm.getFax(), occupancyForm.getEmail());

				occupancyApnObj.setOwner(owner);
				User user = (User) session.getAttribute(Constants.USER_KEY);

				occupancyApnObj.setUpdatedBy(user.getUserId());
			}
			occupancyApn.add(occupancyApnObj);
			logger.debug("added occupancy apn object to occupancy apn array");
		} catch (Exception e) {
			logger.error("Thrown in getOccupancy apn of save occupancy action");
		}
		return occupancyApn;
	}

}
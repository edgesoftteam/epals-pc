package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.admin.ParkingZone;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyDetails;
import elms.app.lso.Use;
import elms.control.beans.OccupancyForm;
import elms.util.StringUtils;

public class SaveOccupancyDetailsAction extends Action {

	static Logger logger = Logger.getLogger(SaveOccupancyDetailsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		OccupancyForm occupancyForm = (OccupancyForm) form;

		try {

			int lsoId = Integer.parseInt(occupancyForm.getLsoId());

			logger.debug("lsoId" + lsoId);

			List useList = new ArrayList();
			String[] use = occupancyForm.getSelectedUse();
			logger.debug("Selected use obtained is " + use);
			for (int i = 0; i < use.length; i++) {
				AddressAgent useAgent = new AddressAgent();
				Use useObj = useAgent.getUse(use[i], "O");
				useList.add(useObj);
			}
			logger.debug("use list obtained from the form " + useList);

			OccupancyDetails occupancyDetails = new OccupancyDetails();

			occupancyDetails.setAlias(occupancyForm.getAlias());
			occupancyDetails.setDescription(occupancyForm.getDescription());
			occupancyDetails.setUse(useList);
			occupancyDetails.setActive(StringUtils.b2s(occupancyForm.getActive()));
			occupancyDetails.setUnitNumber(occupancyForm.getUnitNo());
			occupancyDetails.setBeginFloor(occupancyForm.getBeginningFloor());
			occupancyDetails.setEndFloor(occupancyForm.getEndingFloor());
			occupancyDetails.setLabel(occupancyForm.getLabel());
			occupancyDetails.setHouseholdSize(occupancyForm.getHouseholdSize());
			occupancyDetails.setBedroomSize(occupancyForm.getBedroomSize());
			occupancyDetails.setGrossRent(occupancyForm.getGrossRent());
			occupancyDetails.setUtilityAllowance(occupancyForm.getUtilityAllowance());
			occupancyDetails.setPropertyManager(occupancyForm.getPropertyManager());
			occupancyDetails.setRestrictedUnits(occupancyForm.getRestrictedUnits());

			ParkingZone pzone = new ParkingZone();
			pzone.setPzoneId(occupancyForm.getParkingZone());
			occupancyDetails.setParkingZone(pzone);
			occupancyDetails.setRZone(occupancyForm.getResZone());

			AddressAgent occupancyAgent = new AddressAgent();

			int occupancyId = occupancyAgent.setOccupancyDetails(occupancyDetails, lsoId);

			logger.debug("The occupancy id to get view data is :" + occupancyId);
			Occupancy newOccupancy = occupancyAgent.getOccupancy(occupancyId);
			if (newOccupancy != null) {
				if (mapping.getAttribute() != null) {
					if ("request".equals(mapping.getScope()))
						request.removeAttribute(mapping.getAttribute());
					else
						session.removeAttribute(mapping.getAttribute());
				}
				lsoId = newOccupancy.getLsoId();
				String apn = "";
				logger.debug("Created new occupancy and obtained new occupancy object with id" + lsoId);
				ViewOccupancyAction viewOccupancyAction = new ViewOccupancyAction();
				viewOccupancyAction.processOccupancy(request, lsoId, apn);
			}
			return (mapping.findForward("success"));

		} catch (Exception e) {
			logger.error(e.getMessage());
			return (mapping.findForward("error"));
		}

	}

}
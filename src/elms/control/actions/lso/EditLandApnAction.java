package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.LandApn;
import elms.control.beans.LandForm;
import elms.util.StringUtils;

public class EditLandApnAction extends Action {
	static Logger logger = Logger.getLogger(EditLandApnAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			int lsoId = 0;
			String strLsoId = request.getParameter("lsoId");
			if ((strLsoId != null) && (!strLsoId.equals(""))) {
				lsoId = StringUtils.s2i(request.getParameter("lsoId"));
			}

			String apn = request.getParameter("apn");

			// Remove the obsolete form bean
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			ActionForm frm = new LandForm();
			request.setAttribute("landForm", frm);
			request.setAttribute("lsoId", strLsoId);
			LandForm landForm = (LandForm) frm;
			logger.debug("created land form");

			// list of addresses for that land
			logger.debug("before getting land apn info, lsoId=" + lsoId);
			AddressAgent LsoAgent = new AddressAgent();
			LandApn landApnObj = LsoAgent.getLandApn(apn);
			logger.debug("Obtained land apn data for lsoId=" + lsoId);

			if (landApnObj != null) {
				landForm.setApn(landApnObj.getApn());
				landForm.setOldApn(landApnObj.getApn());
				landForm.setApnOwnerName(landApnObj.getOwner().getName());
				landForm.setApnStreetNumber(StringUtils.i2s(landApnObj.getOwner().getLocalAddress().getStreetNbr()));
				landForm.setApnStreetModifier(landApnObj.getOwner().getLocalAddress().getStreetModifier());
				landForm.setApnStreetName(landApnObj.getOwner().getLocalAddress().getStreet().getStreetName());
				landForm.setApnCity(landApnObj.getOwner().getLocalAddress().getCity());
				landForm.setApnState(landApnObj.getOwner().getLocalAddress().getState());
				landForm.setApnZip(StringUtils.zeroReplaceWithEmpty(StringUtils.s2i(landApnObj.getOwner().getLocalAddress().getZip())));
				logger.debug(" zip code deva as " + landForm.getApnZip());
				landForm.setApnForeignAddress(landApnObj.getOwner().getForeignFlag());
				landForm.setApnForeignAddress1(landApnObj.getOwner().getForeignAddress().getLineOne());
				landForm.setApnForeignAddress2(landApnObj.getOwner().getForeignAddress().getLineTwo());
				landForm.setApnForeignAddress3(landApnObj.getOwner().getForeignAddress().getLineThree());
				landForm.setApnForeignAddress4(landApnObj.getOwner().getForeignAddress().getLineFour());
				landForm.setApnCountry(landApnObj.getOwner().getForeignAddress().getCountry());
				landForm.setApnEmail(landApnObj.getOwner().getEmail());
				landForm.setApnPhone(landApnObj.getOwner().getPhone());
				landForm.setOwnerId(StringUtils.i2s(landApnObj.getOwner().getOwnerId()));
				logger.debug("got owner id as" + StringUtils.i2s(landApnObj.getOwner().getOwnerId()));
				String ownerId = StringUtils.i2s(landApnObj.getOwner().getOwnerId());
				request.setAttribute("ownerId", ownerId);
				landForm.setApnFax(landApnObj.getOwner().getFax());
				logger.debug("end of land apn loop");
			}
		} catch (Exception e) {
			logger.error("*****Error in Edit Land apn Action" + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

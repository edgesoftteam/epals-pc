package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.AddressAgent;
import elms.app.common.Condition;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.StructureDetails;
import elms.common.Constants;
import elms.control.beans.ConditionForm;
import elms.control.beans.ViewStructureForm;
import elms.util.StringUtils;

public class ViewStructureAction extends Action {
	static Logger logger = Logger.getLogger(ViewStructureAction.class.getName());
	String apn = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String lsoId = null;

		try {
			ViewStructureForm frm = (ViewStructureForm) form;
			lsoId = frm.getLsoId();
			logger.debug("The lso id is " + lsoId);

			if (lsoId == null) {
				logger.debug("form not posted, inspecting the request parameter...");
				lsoId = request.getParameter("lsoId");
				logger.debug("The lso id from parameter is " + lsoId);
			}

			apn = frm.getApn();
			logger.debug("obtained apn from form is " + apn);

			processStructure(request, lsoId, apn);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

	public void processStructure(HttpServletRequest request, String lsoId, String apn) {
		int intLsoId = StringUtils.s2i(lsoId);
		HttpSession session = request.getSession();
		session.setAttribute(Constants.LSO_ID, lsoId);
		session.setAttribute(Constants.LSO_TYPE, Constants.STRUCTURE);

		logger.debug("LSO_ID is set inthe session as  " + (String) session.getAttribute(Constants.LSO_ID));

		String onloadAction = "parent.f_lso.location.href='" + request.getContextPath() + "/lsoSearch.do?lsoNodeId=" + lsoId + "'";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		String from = request.getParameter("from")!=null ? request.getParameter("from") : "";
		if(from.equalsIgnoreCase("tree")){
			onloadAction ="parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + lsoId + "'";
		}

		session.setAttribute("onloadAction", onloadAction);
		session.removeAttribute("siteStructureForm");

		ViewStructureForm viewStructureForm = new ViewStructureForm();
		viewStructureForm.setLsoId(lsoId);

		try {
			Structure structure = null;
			structure = new AddressAgent().getStructure(intLsoId);

			StructureDetails structureDetails = structure.getStructureDetails();

			if (structureDetails != null) {
				lsoId = StringUtils.i2s(structure.getLsoId());
				logger.debug("setting lsoId to the request " + lsoId);
				request.setAttribute("lsoId", lsoId);

				String alias = structureDetails.getAlias();
				logger.debug("setting alias to the request " + alias);
				request.setAttribute("alias", alias);

				String description = structureDetails.getDescription();
				logger.debug("setting description to the request " + description);
				request.setAttribute("description", description);

				String totalFloors = structureDetails.getTotalFloors();
				logger.debug("setting totalFloors to the request " + totalFloors);
				request.setAttribute("totalFloors", totalFloors);

				String use = structureDetails.getUseAsString();
				logger.debug("setting use to the request " + use);
				request.setAttribute("use", use);

				String active = structureDetails.getActive();
				logger.debug("setting active to the request " + active);
				request.setAttribute("active", active);

				String label = structureDetails.getLabel();
				logger.debug("setting label to the request " + label);
				request.setAttribute("label", label);
			}

			List structureAddress = structure.getStructureAddress();

			if (structureAddress != null) {
				if (structureAddress.size() > 0) {
					StructureAddress structureAddressObj = (StructureAddress) structureAddress.get(0);

					if (structureAddressObj != null) {
						viewStructureForm.setAddressStreetNumber(StringUtils.i2s(structureAddressObj.getStreetNbr()));
						viewStructureForm.setAddressStreetModifier(structureAddressObj.getStreetModifier());

						Street addressStreet = structureAddressObj.getStreet();

						if (addressStreet != null) {
							viewStructureForm.setAddressStreetName(addressStreet.getStreetName());

							String lsoAddress = structureAddressObj.getStreetNbr() + " " + StringUtils.nullReplaceWithEmpty(structureAddressObj.getStreetModifier()) + " " + StringUtils.nullReplaceWithEmpty(addressStreet.getStreetName());
							session.setAttribute("lsoAddress", lsoAddress);
							logger.debug("The Attribute lsoAddress is set in the session as " + lsoAddress);
							session.removeAttribute("psaInfo");
						}

						viewStructureForm.setAddressCity(structureAddressObj.getCity());
						viewStructureForm.setAddressState(structureAddressObj.getState());
						viewStructureForm.setAddressUnit(structureAddressObj.getUnit());
						viewStructureForm.setAddressZip(structureAddressObj.getZip());
						viewStructureForm.setAddressZip4(structureAddressObj.getZip4());
						viewStructureForm.setAddressPrimary(StringUtils.yn(structureAddressObj.getPrimary()));
						viewStructureForm.setAddressActive(StringUtils.yn(structureAddressObj.getActive()));
						viewStructureForm.setAddressDescription(structureAddressObj.getDescription());
					}
				}
			}

			List occupancyApn = structure.getOccupancyApn();

			if (occupancyApn != null) {
				if (occupancyApn.size() == 0) {
					request.setAttribute("occupancyApnList", new ArrayList());
				} else {
					request.setAttribute("occupancyApnList", occupancyApn);
					logger.debug("set occupancy apn list to request with size" + occupancyApn.size());

					int j = 0;

					for (int i = 0; i < occupancyApn.size(); i++) {
						OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApn.get(i);

						if ((occupancyApnObj.getApn()).equals(apn)) {
							j = i;
						}
					}

					logger.debug("selecting the element number of " + j + " from occupancy list");

					OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApn.get(j);

					if (occupancyApnObj != null) {
						apn = occupancyApnObj.getApn();
						viewStructureForm.setApn(apn);
						session.setAttribute(Constants.APN, apn);
						logger.debug("Occupancy APN set to " + apn);

						Owner owner = occupancyApnObj.getOwner();

						if (owner != null) {
							viewStructureForm.setApnOwnerId(StringUtils.i2s(owner.getOwnerId()));
							viewStructureForm.setApnOwnerName(owner.getName());
							viewStructureForm.setApnEmail(owner.getEmail());
							viewStructureForm.setApnPhone(owner.getPhone());
							viewStructureForm.setApnFax(owner.getFax());
							String foreignFlag = owner.getForeignFlag() != null ? owner.getForeignFlag() : "N";
							viewStructureForm.setApnforeignflag(foreignFlag);

							Address localAddress = owner.getLocalAddress();

							if (localAddress != null) {
								viewStructureForm.setApnStreetNumber(StringUtils.zeroReplaceWithEmpty(localAddress.getStreetNbr()));
								logger.debug(" got street nbr " + viewStructureForm.getApnStreetNumber());
								viewStructureForm.setApnStreetModifier(localAddress.getStreetModifier());
								viewStructureForm.setApnCity(localAddress.getCity());
								viewStructureForm.setApnState(localAddress.getState());
								viewStructureForm.setApnZip(localAddress.getZip());

								Street localAddressStreet = localAddress.getStreet();

								if (localAddressStreet != null) {
									viewStructureForm.setApnStreetName(StringUtils.nullReplaceWithEmpty(localAddressStreet.getStreetName()));
									logger.debug(" got apn street name as " + StringUtils.nullReplaceWithEmpty(localAddressStreet.getStreetName()));
								}
							}

							ForeignAddress foreignAddress = owner.getForeignAddress();

							if (foreignAddress != null) {
								viewStructureForm.setApnForeignAddress1(foreignAddress.getLineOne());
								viewStructureForm.setApnForeignAddress2(foreignAddress.getLineTwo());
								viewStructureForm.setApnForeignAddress3(foreignAddress.getLineThree());
								viewStructureForm.setApnForeignAddress4(foreignAddress.getLineFour());
								viewStructureForm.setApnCountry(foreignAddress.getCountry());
							}
						}
					}
				}
			}

			request.setAttribute("viewStructureForm", viewStructureForm);

			// **** Condition List
			ConditionForm condForm = new ConditionForm();
			CommonAgent condAgent = new CommonAgent();
			condForm.setLevelId(intLsoId);
			condForm.setConditionLevel("S");
			condForm = condAgent.populateConditionForm(condForm);

			Condition[] conditions = condForm.getCondition();
			List condList = Arrays.asList(conditions);

			if (condList.size() > 3) {
				condList = condList.subList(0, 3);
			}

			request.setAttribute("condList", condList);

			// setting Holdlist
			List holds = new ArrayList();
			holds = structure.getLsoHold();
			request.setAttribute("holds", holds);
			logger.debug("set Holds to request with size " + holds.size());

			if (new CommonAgent().editable(StringUtils.s2i(lsoId), "S")) {
				request.setAttribute("editable", "true");
			} else {
				request.setAttribute("editable", "false");
			}

			// list of comments for this lso.
			List commentList = new CommonAgent().getCommentList(intLsoId, 3, "S");
			request.setAttribute("commentList", commentList);

			// list of attachments for this lso.
			List attachmentList = new CommonAgent().getAttachments(intLsoId, "S");
			request.setAttribute("attachmentList", attachmentList);

			logger.debug("End of ViewStructure Action");
		} catch (Exception e) {
			logger.error("error thrown in view structure action " + e.getMessage());
		}
	}
}

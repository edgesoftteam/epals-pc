package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Land;
import elms.app.lso.LandApn;
import elms.app.lso.LandDetails;
import elms.app.lso.LandSiteData;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.control.beans.LandForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveLandApnAction extends Action {

	/**
	 * The logger.
	 */
	static Logger logger = Logger.getLogger(SaveLandApnAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		LandForm landForm = (LandForm) form;

		try {
			int lsoId = 0;
			String strLsoId = request.getParameter("lsoId");
			if ((strLsoId != null) && (!strLsoId.equals(""))) {
				lsoId = StringUtils.s2i(strLsoId);
			} else {
				lsoId = 0;
			}

			int ownerId = 0;
			String strOwnerId = request.getParameter("ownerId");
			if ((strOwnerId != null) && (!strOwnerId.equals(""))) {
				ownerId = StringUtils.s2i(strOwnerId);
			} else {
				ownerId = 0;
			}

			LandDetails landDetails = new LandDetails();
			List landAddress = new ArrayList();
			List landHold = new ArrayList();
			List landCondition = new ArrayList();
			List landAttachment = new ArrayList();
			List landComment = new ArrayList();
			List landApn = new ArrayList();

			LandApn landApnObj = new LandApn();
			landApnObj.setApn(landForm.getApn());
			landApnObj.setOldApn(landForm.getOldApn());
			Owner owner = new Owner();
			owner.setEmail(landForm.getApnEmail());
			owner.setFax(landForm.getApnFax());
			ForeignAddress foreignAddress = new ForeignAddress();
			foreignAddress.setCountry(landForm.getApnCountry());
			foreignAddress.setLineOne(landForm.getApnForeignAddress1());
			foreignAddress.setLineTwo(landForm.getApnForeignAddress2());
			foreignAddress.setLineThree(landForm.getApnForeignAddress3());
			foreignAddress.setLineFour(landForm.getApnForeignAddress4());
			owner.setForeignAddress(foreignAddress);
			owner.setForeignFlag(landForm.getApnForeignAddress());
			Address localAddress = new Address();
			localAddress.setCity(landForm.getApnCity());
			localAddress.setState(landForm.getApnState());
			Street localAddressStreet = new Street();
			String streetName = "" + landForm.getApnStreetName();
			localAddressStreet.setStreetName(streetName);
			localAddress.setStreet(localAddressStreet);
			localAddress.setStreetModifier(landForm.getApnStreetModifier());
			localAddress.setStreetNbr(StringUtils.s2i(landForm.getApnStreetNumber()));
			localAddress.setZip(landForm.getApnZip());
			owner.setLocalAddress(localAddress);
			owner.setName(landForm.getApnOwnerName());
			owner.setPhone(landForm.getApnPhone());
			owner.setOwnerId(StringUtils.s2i(landForm.getOwnerId()));
			logger.debug("got the owner id as " + StringUtils.s2i(landForm.getOwnerId()));
			landApnObj.setOwner(owner);
			User user = (User) session.getAttribute("user");

			landApnObj.setUpdatedBy(user.getUserId());
			landApn.add(landApnObj);

			LandSiteData landSiteData = new LandSiteData();
			Land land = new Land(lsoId, landDetails, landApn, landApn, landHold, landCondition, landAttachment, landSiteData, landComment);
			logger.debug("Land object created for new land creation");

			AddressAgent LsoAgent = new AddressAgent();
			logger.debug("Before sending to the land agent");
			Land newLand = null;

			if ((ownerId == 0) & (lsoId > 0)) {
				newLand = LsoAgent.addLandApn(land);
			} else {
				newLand = LsoAgent.updateLandApn(land);
			}

			if (newLand != null) {
				if (mapping.getAttribute() != null) {
					if ("request".equals(mapping.getScope())) {
						request.removeAttribute(mapping.getAttribute());
					} else {
						session.removeAttribute(mapping.getAttribute());
					}
				}

				String apn = "";
				lsoId = newLand.getLsoId();
				logger.debug("Created new land and obtained new occupancy object with id" + lsoId);
				ViewLandAction viewLandAction = new ViewLandAction();
				viewLandAction.processLand(request, lsoId, apn);
			}
		} catch (Exception e) {
			logger.error("Error thrown, not successful in saveLand address" + e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

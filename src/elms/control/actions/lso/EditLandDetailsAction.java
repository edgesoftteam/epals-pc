package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Land;
import elms.app.lso.LandDetails;
import elms.control.beans.LandForm;
import elms.util.StringUtils;

public class EditLandDetailsAction extends Action {
	static Logger logger = Logger.getLogger(EditLandDetailsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			LandForm lf = (LandForm) form;
			int lsoId = -1;
			String strLsoId = lf.getLsoId();
			lsoId = StringUtils.s2i(strLsoId);
			logger.debug("Got the value of lso id from form as :" + lsoId);

			Land land = null;

			land = new AddressAgent().getLand(lsoId);
			logger.debug("Got the land object for above lso id as " + land.toString());

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			ActionForm frm = new LandForm();
			session.setAttribute("lsoId", strLsoId);

			LandForm landForm = (LandForm) frm;
			boolean active = false;

			AddressAgent useAgent = new AddressAgent();
			request.setAttribute("useAgent", useAgent);

			AddressAgent zoneAgent = new AddressAgent();
			request.setAttribute("zoneAgent", zoneAgent);

			if (land != null) {
				LandDetails landDetails = land.getLandDetails();

				if (landDetails != null) {
					active = StringUtils.s2b(landDetails.getActive());
					landForm.setActive(active);
					logger.debug("set active to land form as " + active);
					landForm.setAlias(landDetails.getAlias());
					logger.debug("set alias to land form as " + landDetails.getAlias());
					landForm.setDescription(landDetails.getDescription());
					logger.debug("set description to land form as " + landDetails.getDescription());
					landForm.setSelectedZone(landDetails.getZoneAsArray());
					logger.debug("set selected zone to land form ");
					landForm.setSelectedUse(landDetails.getUseAsArray());
					logger.debug("set selected use to land form ");
					landForm.setCoordinateX(landDetails.getXCordinate());
					logger.debug("set x coordinate to land form as " + landDetails.getXCordinate());
					landForm.setCoordinateY(landDetails.getYCordinate());
					logger.debug("set y coordinate to land form as " + landDetails.getYCordinate());
					landForm.setAddressRangeList(landDetails.getAddressRangeList());

					if (landDetails.getParkingZone() != null) {
						landForm.setParkingZone(landDetails.getParkingZone().getPzoneId());
					}

					logger.debug("set pzoneid to land form as " + landDetails.getParkingZone());
					landForm.setResZone(landDetails.getRZone());
					logger.debug("set rzone to land form as " + landDetails.getRZone());
					landForm.setLabel(landDetails.getLabel());
					logger.debug("set label to land form as " + landDetails.getLabel());
				}
			}

			session.setAttribute("landForm", landForm);
			logger.debug("Exiting EditLandDetailsAction");

			return (mapping.findForward("success"));

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException(e.getMessage());
		}

	}
}

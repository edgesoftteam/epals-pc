package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;

public class AddOccupancyAddressAction extends Action {

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		Logger logger = Logger.getLogger(AddOccupancyAddressAction.class.getName());

		HttpSession session = request.getSession();

		try {

			String lsoId = request.getParameter("lsoId"); // 21762
			String addressId = "0"; // for new address

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			request.setAttribute("lsoId", lsoId);
			request.setAttribute("addressId", addressId);

			AddressAgent streetAgent = new AddressAgent();
			java.util.List streetList = streetAgent.getStreetArrayList();
			request.setAttribute("streetList", streetList);

		} catch (Exception e) {
			logger.error("Error in EditOccupancyAddressAction");
		}
		return (mapping.findForward("success"));

	}
}
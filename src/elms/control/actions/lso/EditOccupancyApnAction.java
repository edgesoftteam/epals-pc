package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.OccupancyApn;
import elms.control.beans.OccupancyForm;
import elms.util.StringUtils;

public class EditOccupancyApnAction extends Action {

	static Logger logger = Logger.getLogger(EditOccupancyApnAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		HttpSession session = request.getSession();

		try {

			String lsoId = request.getParameter("lsoId");
			logger.info("LSO Id: " + lsoId);
			int intLsoId = StringUtils.s2i(lsoId);

			String apn = request.getParameter("apn");
			logger.info("APN : " + apn);

			OccupancyApn occupancyApn = null;

			AddressAgent occupancyAgent = new AddressAgent();
			occupancyApn = occupancyAgent.getOccupancyApn(apn);

			// Remove the obsolete form bean
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			request.setAttribute("lsoId", lsoId);
			request.setAttribute("apn", apn);

			ActionForm frm = new OccupancyForm();
			request.setAttribute("occupancyForm", frm);
			OccupancyForm occupancyForm = (OccupancyForm) frm;

			occupancyForm.setApn(occupancyApn.getApn());
			occupancyForm.setOwnerName(occupancyApn.getOwner().getName());
			occupancyForm.setOwnerId("" + occupancyApn.getOwner().getOwnerId());
			occupancyForm.setApnStreetNumber("" + StringUtils.zeroReplaceWithEmpty(occupancyApn.getOwner().getLocalAddress().getStreetNbr()));

			occupancyForm.setApnStreetMod(occupancyApn.getOwner().getLocalAddress().getStreetModifier());
			occupancyForm.setApnStreetName(occupancyApn.getOwner().getLocalAddress().getStreet().getStreetName());
			occupancyForm.setApnCity(occupancyApn.getOwner().getLocalAddress().getCity());
			occupancyForm.setApnState(occupancyApn.getOwner().getLocalAddress().getState());
			occupancyForm.setApnZip(StringUtils.zeroReplaceWithEmpty(StringUtils.s2i(occupancyApn.getOwner().getLocalAddress().getZip())));
			logger.debug(" zip code apn as " + occupancyForm.getApnZip());
			occupancyForm.setForeignAddress(occupancyApn.getOwner().getForeignFlag());
			occupancyForm.setLine1(occupancyApn.getOwner().getForeignAddress().getLineOne());
			occupancyForm.setLine2(occupancyApn.getOwner().getForeignAddress().getLineTwo());
			occupancyForm.setLine3(occupancyApn.getOwner().getForeignAddress().getLineThree());
			occupancyForm.setLine4(occupancyApn.getOwner().getForeignAddress().getLineFour());
			occupancyForm.setCountry(occupancyApn.getOwner().getForeignAddress().getCountry());
			occupancyForm.setEmail(occupancyApn.getOwner().getEmail());
			occupancyForm.setPhone(occupancyApn.getOwner().getPhone());
			occupancyForm.setFax(occupancyApn.getOwner().getFax());

			request.setAttribute("ownerId", "" + occupancyApn.getOwner().getOwnerId());

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}
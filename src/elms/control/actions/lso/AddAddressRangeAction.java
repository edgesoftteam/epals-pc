package elms.control.actions.lso;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.admin.lso.AddressRangeEdit;
import elms.control.beans.LandForm;

public class AddAddressRangeAction extends Action {

	static Logger logger = Logger.getLogger(AddAddressRangeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {
			logger.info("Entering AddAddressRangeAction");

			LandForm landForm = (LandForm) session.getAttribute("landForm");

			AddressRangeEdit[] bRange = landForm.getAddressRangeList();
			int i = bRange.length;
			AddressRangeEdit[] aRange = new AddressRangeEdit[i + 1];
			for (int j = 0; j < i; j++) {
				aRange[j] = bRange[j];
			}
			aRange[i] = new AddressRangeEdit();
			landForm.setAddressRangeList(aRange);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			session.setAttribute("landForm", landForm);

			List streets = AddressAgent.getStreetArrayList();
			request.setAttribute("streets", streets);

			logger.info("Exiting AddAddressRangeAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}
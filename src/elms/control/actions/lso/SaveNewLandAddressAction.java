package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.LandDetails;
import elms.app.lso.LandSiteData;
import elms.app.lso.Owner;
import elms.app.lso.Street;
import elms.control.beans.LandForm;
import elms.util.StringUtils;

public class SaveNewLandAddressAction extends Action {

	static Logger logger = Logger.getLogger(SaveNewLandAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		LandForm landForm = (LandForm) form;

		try {

			int addressId = -1;
			String strAddressId = request.getParameter("addressId");
			addressId = StringUtils.s2i(strAddressId);

			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);

			LandDetails landDetails = new LandDetails();
			List landAddress = new ArrayList();
			LandAddress landAddressObj = new LandAddress();
			landAddressObj.setAddressId(addressId);
			logger.debug("set address id " + addressId);
			landAddressObj.setStreetModifier(landForm.getAddressStreetModifier());
			logger.debug("got street mod " + landForm.getAddressStreetModifier());
			landAddressObj.setStreetNbr(StringUtils.s2i(landForm.getAddressStreetNumber()));
			logger.debug("got street no");
			landAddressObj.setCity(landForm.getAddressCity());
			logger.debug("got city " + landForm.getAddressCity());
			landAddressObj.setState(landForm.getAddressState());
			logger.debug("got state");
			landAddressObj.setZip(landForm.getAddressZip());
			logger.debug("got zip");
			landAddressObj.setZip4(landForm.getAddressZip4());
			logger.debug("got zip4");
			landAddressObj.setDescription(landForm.getAddressDescription());
			logger.debug("got desc");
			landAddressObj.setActive(StringUtils.b2s(landForm.getAddressActive()));
			logger.debug("got active");
			landAddressObj.setPrimary(StringUtils.b2s(landForm.getAddressPrimary()));
			logger.debug("got primary");
			Street street = new Street();
			street.setStreetId(StringUtils.s2i(landForm.getAddressStreetName()));
			logger.debug("set the street name value is " + landForm.getAddressStreetName());
			landAddressObj.setStreet(street);
			landAddress.add(landAddressObj);

			List landHold = new ArrayList();
			List landCondition = new ArrayList();
			List landAttachment = new ArrayList();
			List landComment = new ArrayList();
			List landApn = new ArrayList();
			LandSiteData landSiteData = new LandSiteData();
			Land land = new Land(lsoId, landDetails, landAddress, landApn, landHold, landCondition, landAttachment, landSiteData, landComment);
			logger.debug("Land object created for new land creation");

			AddressAgent landAgent = new AddressAgent();
			logger.debug("before sending to the land agent");
			land = landAgent.updateLandAddress(land);
			if (land != null) {

				strLsoId = StringUtils.i2s(land.getLsoId());
				logger.debug("setting lsoId to the request " + strLsoId);
				request.setAttribute("lsoId", strLsoId);

				landDetails = land.getLandDetails();
				if (landDetails != null) {
					String alias = landDetails.getAlias();
					logger.debug("setting alias to the request " + alias);
					request.setAttribute("alias", alias);
					String description = landDetails.getDescription();
					logger.debug("setting description to the request " + description);
					request.setAttribute("description", description);
					String zoneObj = landDetails.getZoneAsString();
					logger.debug("setting zone to the request " + zoneObj);
					request.setAttribute("zone", zoneObj);
					String useObj = landDetails.getUseAsString();
					logger.debug("setting use to the request " + useObj);
					request.setAttribute("use", useObj);
					String coordinateX = landDetails.getXCordinate();
					logger.debug("setting coordinateX to the request " + coordinateX);
					request.setAttribute("coordinateX", coordinateX);
					String coordinateY = landDetails.getYCordinate();
					logger.debug("setting coordinateY to the request " + coordinateY);
					request.setAttribute("coordinateY", coordinateY);
					String active = StringUtils.yn(landDetails.getActive());
					logger.debug("setting active to the request " + active);
					request.setAttribute("active", active);
				}

				landAddress = land.getLandAddress();
				if (landAddress != null) {
					if (landAddress.size() > 0) {
						landAddressObj = (LandAddress) landAddress.get(0);
						if (landAddressObj != null) {
							String addressStreetNumber = StringUtils.i2s(landAddressObj.getStreetNbr());
							logger.debug("setting addressStreetNumber to the request " + addressStreetNumber);
							request.setAttribute("addressStreetNumber", addressStreetNumber);
							String addressStreetModifier = landAddressObj.getStreetModifier();
							logger.debug("setting addressStreetModifier to the request " + addressStreetModifier);
							request.setAttribute("addressStreetModifier", addressStreetModifier);
							Street addressStreet = landAddressObj.getStreet();
							if (addressStreet != null) {
								String addressStreetName = addressStreet.getStreetName();
								logger.debug("setting addressStreetName to the request " + addressStreetName);
								request.setAttribute("addressStreetName", addressStreetName);
							}
							String addressCity = landAddressObj.getCity();
							logger.debug("setting addressCity to the request " + addressCity);
							request.setAttribute("addressCity", addressCity);
							String addressState = landAddressObj.getState();
							logger.debug("setting addressState to the request " + addressState);
							request.setAttribute("addressState", addressState);
							String addressUnit = landAddressObj.getUnit();
							logger.debug("setting addressUnit to the request " + addressUnit);
							request.setAttribute("addressUnit", addressUnit);
							String addressZip = landAddressObj.getZip();
							logger.debug("setting addressZip to the request " + addressZip);
							request.setAttribute("addressZip", addressZip);
							String addressZip4 = landAddressObj.getZip4();
							logger.debug("setting addressZip4 to the request " + addressZip4);
							request.setAttribute("addressZip4", addressZip4);
							String addressPrimary = StringUtils.yn(landAddressObj.getPrimary());
							logger.debug("setting addressPrimary to the request " + addressPrimary);
							request.setAttribute("addressPrimary", addressPrimary);
							String addressActive = StringUtils.yn(landAddressObj.getActive());
							logger.debug("setting addressActive to the request " + addressActive);
							request.setAttribute("addressActive", addressActive);
							String addressDescription = landAddressObj.getDescription();
							logger.debug("setting addressDescription to the request " + addressDescription);
							request.setAttribute("addressDescription", addressDescription);
						}
					}
				}
				landApn = land.getLandApn();
				if (landApn != null) {
					if (landApn.size() > 0) {
						LandApn landApnObj = (LandApn) landApn.get(0);
						if (landApnObj != null) {
							String apn = landApnObj.getApn();
							Owner owner = landApnObj.getOwner();
							if (owner != null) {
								String ownerId = StringUtils.i2s(owner.getOwnerId());
								logger.debug("setting ownerId to the request " + ownerId);
								request.setAttribute("ownerId", ownerId);
								String apnOwnerName = owner.getName();
								logger.debug("setting apnOwnerName to the request " + apnOwnerName);
								request.setAttribute("apnOwnerName", apnOwnerName);
								String apnEmail = owner.getEmail();
								logger.debug("setting apnEmail to the request " + apnEmail);
								request.setAttribute("apnEmail", apnEmail);
								String apnPhone = owner.getPhone();
								logger.debug("setting apnPhone to the request " + apnPhone);
								request.setAttribute("apnPhone", apnPhone);
								String apnFax = owner.getFax();
								logger.debug("setting apnFax to the request " + apnFax);
								request.setAttribute("apnFax", apnFax);
								Address localAddress = owner.getLocalAddress();
								if (localAddress != null) {
									String apnStreetNumber = StringUtils.i2s(localAddress.getStreetNbr());
									logger.debug("setting apnStreetNumber to the request " + apnStreetNumber);
									request.setAttribute("apnStreetNumber", apnStreetNumber);
									String apnStreetModifier = localAddress.getStreetModifier();
									logger.debug("setting apnStreetModifier to the request " + apnStreetModifier);
									request.setAttribute("apnStreetModifier", apnStreetModifier);
									String apnCity = localAddress.getCity();
									logger.debug("setting apnCity to the request " + apnCity);
									request.setAttribute("apnCity", apnCity);
									String apnState = localAddress.getState();
									logger.debug("setting apnState to the request " + apnState);
									request.setAttribute("apnState", apnState);
									String apnZip = localAddress.getZip();
									logger.debug("setting apnZip to the request " + apnZip);
									request.setAttribute("apnZip", apnZip);
									Street localAddressStreet = localAddress.getStreet();
									if (localAddressStreet != null) {
										String apnStreetName = localAddressStreet.getStreetName();
										logger.debug("setting apnStreetName to the request " + apnStreetName);
										request.setAttribute("apnStreetName", apnStreetName);
									}
								}
								ForeignAddress foreignAddress = owner.getForeignAddress();
								if (foreignAddress != null) {
									String apnForeignAddress1 = foreignAddress.getLineOne();
									logger.debug("setting apnForeignAddress1 to the request " + apnForeignAddress1);
									request.setAttribute("apnForeignAddress1", apnForeignAddress1);
									String apnForeignAddress2 = foreignAddress.getLineTwo();
									logger.debug("setting apnForeignAddress2 to the request " + apnForeignAddress2);
									request.setAttribute("apnForeignAddress2", apnForeignAddress2);
									String apnForeignAddress3 = foreignAddress.getLineThree();
									logger.debug("setting apnForeignAddress3 to the request " + apnForeignAddress3);
									request.setAttribute("apnForeignAddress3", apnForeignAddress3);
									String apnForeignAddress4 = foreignAddress.getLineFour();
									logger.debug("setting apnForeignAddress4 to the request " + apnForeignAddress4);
									request.setAttribute("apnForeignAddress4", apnForeignAddress4);
									String apnCountry = foreignAddress.getCountry();
									logger.debug("setting apnCountry to the request " + apnCountry);
									request.setAttribute("apnCountry", apnCountry);
								}
							}

						}
					}
				}

			}
			request.setAttribute("lsoId", strLsoId);

			logger.debug("Forward path is- " + mapping.findForward("success").getPath());
		} catch (Exception e) {
			logger.error("Error thrown, not successful in saveLand address" + e.getMessage());
		}
		return (mapping.findForward("success"));

	}

}
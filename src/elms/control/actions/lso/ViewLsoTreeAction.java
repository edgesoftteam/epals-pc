package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.Street;
import elms.control.beans.LsoSearchForm;
import elms.util.StringUtils;

public class ViewLsoTreeAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ViewLsoTreeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewLsoTreeAction() ");

		HttpSession session = request.getSession();
		/*
		 * If the input is from frameLso or frameLsoMenu the content frame and project explorer should be refreshed.
		 */
		String clear = request.getParameter("clear");
		logger.debug("The Value of clear is " + clear);

		String onloadAction = "";
		if (clear == null) {
			clear = "";
		}

		if (clear.equals("YES")) {
			onloadAction = "changeContent";
		}

		session.setAttribute("onloadAction", onloadAction);

		LsoSearchForm lsoSearchForm = (LsoSearchForm) form;
		String streetNumber = lsoSearchForm.getStreetNumber();
		logger.debug("setting streetNumber in the request as :" + streetNumber);
		session.setAttribute("streetNbr", streetNumber);
		String streetId = lsoSearchForm.getStreetName();
		streetId = StringUtils.nullReplaceWithEmpty(streetId);
		logger.debug("obtained street id from form as : " + streetId);
		logger.debug("setting streetId to the session as : " + streetId);
		session.setAttribute("streetId", streetId);
		String radio = lsoSearchForm.getRadiobutton();
		boolean active = false;
		AddressAgent lsoTreeAgent = new AddressAgent();

		/**
		 * First try to find the address in the Address Range Table. If found use that to display the tree. If not found then try to look thru the lso_address table for a matching entry. There are some lands being found in the address range table which don't have any entry in the lso_address table. This is a data issue
		 */
		List lsoIdList = new ArrayList();
		List lsoIdListRange = new ArrayList();
		List lsoIdListAddress = new ArrayList();
		try {
			if (!(request.getParameter("lsoNodeId") == null)) {
				lsoIdList = lsoTreeAgent.getLandLsoIdList(new Long(request.getParameter("lsoNodeId")).longValue());
				logger.debug("Received Value from lsoNodeId: " + request.getParameter("lsoNodeId"));
				lsoSearchForm.setStreetNumber("");
				lsoSearchForm.setStreetName("-1");
				lsoSearchForm.setRadiobutton("radio_active");
				session.removeAttribute("streetId");
				session.removeAttribute("streetNumber");
			}
		} catch (Exception e) {
			lsoIdList = new ArrayList();
		}
		logger.debug("obtained the lso id list of size " + lsoIdList.size());

		if (lsoIdList.size() == 0) {
			logger.debug("lso id list size is 0");
			AddressAgent streetAgent = new AddressAgent();
			Street street = new Street();

			if (streetId.equals("-1") && StringUtils.s2i(streetId) == 1) {
				logger.info("Street id is -1");
			} else {
				street = streetAgent.getStreet(StringUtils.s2i(streetId));
			}
			Address address = new Address();
			address.setStreetNbr(StringUtils.s2i(streetNumber));
			address.setStreet(street);
			if ((radio == null) || radio.equals("")) {
				active = true;
				lsoSearchForm.setRadiobutton("radio_active");
			} else {
				active = lsoSearchForm.getRadiobutton().equals("radio_active");
			}
			logger.debug("trying to get land id");
			try {
				lsoIdListRange = lsoTreeAgent.getLandId(address, active);
			} catch (Exception e1) {
				logger.debug("exception caught while getting land id");
			}
		}

	//	if (lsoIdList.size() == 0) {
			if ((radio == null) || radio.equals("")) {
				lsoSearchForm.setRadiobutton("radio_active");
				active = true;
				try {
					if (!streetId.equals("-1")) {
						lsoIdListAddress = lsoTreeAgent.getLsoIdList(streetNumber, StringUtils.s2i(streetId), active, "search", "");
					}
				} catch (Exception e1) {
					throw new ServletException("couldn not get lso id list " + e1.getMessage());
				}
				logger.debug("Radio button null and lsoIdList size is " + lsoIdList.size());

				if (lsoIdList.size() == 0) {
					logger.debug("Radio button Null:Lsoidlist size =" + lsoIdList.size());
					active = false;
					try {
						if (!streetId.equals("-1")) {
							lsoIdListAddress = lsoTreeAgent.getLsoIdList(streetNumber, StringUtils.s2i(streetId), active, "search", "");
						}
					} catch (Exception e2) {
						throw new ServletException("couldn not get lso id list " + e2.getMessage());
					}
					lsoSearchForm.setRadiobutton("radio_all");
				}
			} else {
				active = lsoSearchForm.getRadiobutton().equals("radio_active");
				try {
					if (!streetId.equals("-1")) {
						lsoIdListAddress = lsoTreeAgent.getLsoIdList(streetNumber, StringUtils.s2i(streetId), active, "search", "");
					}
				} catch (Exception e1) {
					throw new ServletException("couldn not get lso id list " + e1.getMessage());
				}
				logger.debug("The Value of active is " + active);
				logger.debug("Radio Button: Not Null :lsoIdList size :" + lsoIdList.size());

				if (lsoIdListAddress.size() == 0) {
					lsoSearchForm.setRadiobutton("");
				} else if (active == true) {
					lsoSearchForm.setRadiobutton("radio_active");
				} else {
					lsoSearchForm.setRadiobutton("radio_all");
				}
			}
		//}
		lsoIdList.addAll(lsoIdListRange);
		lsoIdList.addAll(lsoIdListAddress);


		List lsoTreeList;
		try {
			lsoTreeList = lsoTreeAgent.getLsoTreeList(lsoIdList, active, request.getContextPath());
		} catch (Exception e1) {
			throw new ServletException("couldn not get lso tree list " + e1.getMessage());
		}

		ArrayList treeList = new ArrayList();
		String tree;

		for (int i = 0; (lsoTreeList != null) && (i < lsoTreeList.size()); i++) {
			elms.app.lso.LsoTree lsoTree = (elms.app.lso.LsoTree) lsoTreeList.get(i);

			tree = "	Tree[" + i + "] = \"" + lsoTree.getNodeId() + "|" + lsoTree.getNodeLevel() + "|" + ((lsoTree.isOnHold() == true) ? ("<font color='" + ((lsoTree.getHoldColor() != null) ? lsoTree.getHoldColor() : "black") + "'><b>(H)</b></font>") : "") + lsoTree.getNodeText() + "|" + lsoTree.getNodeLink() + "|" + lsoTree.getNodeAlt() + "|" + lsoTree.getId() + "\"";

			logger.debug("lsoTree.getNodeId()" + lsoTree.getNodeId() + "lsoTree.getNodeLevel()" + lsoTree.getNodeLevel() + " lsoTree.getId() " + lsoTree.getId());

			logger.debug(" Tree string  is " + tree);
			treeList.add(tree);
		}

		session.setAttribute("treeList", treeList);

		session.setAttribute("lsoTreeList", lsoTreeList);
		if (lsoTreeList.size() != 0) {
			session.setAttribute("lsoSearchForm", lsoSearchForm);
		}

		logger.debug("Tree Created & Set into session. " + lsoTreeList.size() + " elements");

		request.setAttribute("hl", "home");
		logger.debug("Radio Button is " + lsoSearchForm.getRadiobutton());
		logger.info("Exiting ViewLsoTreeAction() ");

		return (mapping.findForward("success"));
	}
}

package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyDetails;
import elms.control.beans.OccupancyForm;
import elms.util.StringUtils;

public class EditOccupancyAction extends EditLsoAction {

	static Logger logger = Logger.getLogger(EditOccupancyAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		try {

			String lsoId = request.getParameter("lsoId"); // 21762
			logger.info("Lso id	is : " + lsoId);

			Occupancy occupancy = null;
			OccupancyDetails occupancyDetails = null;

			AddressAgent occupancyAgent = new AddressAgent();
			occupancy = occupancyAgent.getOccupancy(StringUtils.s2i(lsoId));
			logger.warn("occupancy is" + occupancy);

			// Remove the obsolete form bean
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			request.setAttribute("lsoId", lsoId);

			ActionForm frm = new OccupancyForm();
			request.setAttribute("occupancyForm", frm);
			OccupancyForm occupancyForm = (OccupancyForm) frm;
			logger.warn("out side	if in EOAA");

			if (occupancy != null) {
				occupancyDetails = occupancy.getOccupancyDetails();
				if (occupancyDetails != null) {

					occupancyForm.setAlias(occupancyDetails.getAlias());
					occupancyForm.setDescription(occupancyDetails.getDescription());
					occupancyForm.setUnitNo(occupancyDetails.getUnitNumber());
					occupancyForm.setSelectedUse(occupancyDetails.getUseAsArray());
					occupancyForm.setBeginningFloor(occupancyDetails.getBeginFloor());
					occupancyForm.setEndingFloor(occupancyDetails.getEndFloor());
					occupancyForm.setActive(StringUtils.s2b(occupancyDetails.getActive()));
					if (occupancyDetails.getParkingZone() != null)
						occupancyForm.setParkingZone(occupancyDetails.getParkingZone().getPzoneId());
					logger.debug("set pzoneid to land form as " + occupancyDetails.getParkingZone());
					occupancyForm.setResZone(occupancyDetails.getRZone());
					logger.debug("set rzone to land form as " + occupancyDetails.getRZone());
					occupancyForm.setLabel(occupancyDetails.getLabel());
					occupancyForm.setHouseholdSize(occupancyDetails.getHouseholdSize());
					occupancyForm.setBedroomSize(occupancyDetails.getBedroomSize());
					occupancyForm.setGrossRent(occupancyDetails.getGrossRent());
					occupancyForm.setUtilityAllowance(occupancyDetails.getUtilityAllowance());
					occupancyForm.setPropertyManager(occupancyDetails.getPropertyManager());
					occupancyForm.setRestrictedUnits(occupancyDetails.getRestrictedUnits());

				}

			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}
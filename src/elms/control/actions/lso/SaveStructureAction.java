package elms.control.actions.lso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.AddressAgent;
import elms.app.lso.Street;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.StructureDetails;
import elms.app.lso.StructureSiteData;
import elms.app.lso.Use;
import elms.common.Constants;
import elms.control.beans.StructureForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveStructureAction extends SaveLsoAction {

	static Logger logger = Logger.getLogger(SaveStructureAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		StructureForm structureForm = (StructureForm) form;

		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {

			saveErrors(request, errors);
			saveToken(request);

			return (new ActionForward(mapping.getInput()));
		} else {

			try {

				String landId = request.getParameter("lsoId");
				int intLandId = StringUtils.s2i(landId);

				StructureDetails structureDetails = this.getStructureDetails(structureForm);
				List structureAddressList = this.getStructureAddressList(structureForm, session);
				logger.debug("got the structure address list  as " + structureAddressList);
				List structureHoldList = new ArrayList();
				List structureConditionList = new ArrayList();
				List structureAttachmentList = new ArrayList();
				List structureCommentList = new ArrayList();
				StructureSiteData structureSiteData = new StructureSiteData();
				Structure structure = new Structure(0, intLandId, structureDetails, structureAddressList, structureHoldList, structureConditionList, structureAttachmentList, structureSiteData, structureCommentList);
				logger.debug("Structure object created for Land " + landId);

				structure = new AddressAgent().addStructure(structure);

				// structureDetails = structure.getStructureDetails(); //check for use
				if (structure != null) {

					int lsoId = structure.getLsoId();
					logger.debug("obtained structure for id as " + lsoId);
					ViewStructureAction viewStructureAction = new ViewStructureAction();
					viewStructureAction.processStructure(request, StringUtils.i2s(lsoId), "0");
					logger.debug("set view Structure action with reqd values");
				}

				logger.debug("request set to the page with structure object");
			} catch (Exception e) {

				logger.error(e.getMessage());
			}
		}

		return (mapping.findForward("success"));
	}

	public StructureDetails getStructureDetails(StructureForm structureForm) {

		StructureDetails structureDetails = null;
		try {

			// use list object here
			List useList = new ArrayList();
			String[] use = structureForm.getSelectedUse();
			for (int i = 0; i < use.length; i++) {

				AddressAgent useAgent = new AddressAgent();
				Use useObj = useAgent.getUse(use[i], "S");
				useList.add(useObj);
			}

			logger.debug("Use List created for new structure creation");

			String active = (structureForm.getActive()) ? "Y" : "N";
			String totalFloors = structureForm.getTotalFloors();
			structureDetails = new StructureDetails(structureForm.getAlias(), structureForm.getDescription(), useList, active, totalFloors, structureForm.getLabel());
			logger.debug("Structure details object created for new land creation");
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return structureDetails;
	}

	public List getStructureAddressList(StructureForm structureForm, HttpSession session) {

		logger.debug("in the get structure address list ");
		List structureAddress = new ArrayList();
		try {

			int addressStreetNumber = StringUtils.s2i(structureForm.getAddressStreetNumber());
			Street addressStreet = new AddressAgent().getStreet(StringUtils.s2i(structureForm.getAddressStreetName()));
			String addressActive = (structureForm.getAddressActive()) ? "Y" : "N";
			String addressPrimary = (structureForm.getAddressPrimary()) ? "Y" : "N";
			StructureAddress structureAddressObj = new StructureAddress(0, 0, addressStreetNumber, structureForm.getAddressStreetModifier(), addressStreet, structureForm.getAddressCity(), structureForm.getAddressState(), structureForm.getAddressZip(), structureForm.getAddressZip4(), structureForm.getAddressDescription(), addressPrimary, addressActive);
			logger.debug("structure formed :" + structureAddressObj.toString());
			structureAddressObj.setUpdatedBy((User) session.getAttribute(Constants.USER_KEY));
			logger.debug("Userid in SaveStructureAction is " + structureAddressObj.getUpdatedBy().getUserId());
			structureAddress.add(structureAddressObj);
			logger.debug("Structure address object created for new structure creation");
		} catch (Exception e) {

			logger.error(e.getMessage());
		}

		return structureAddress;
	}
}

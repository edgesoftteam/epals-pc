package elms.control.actions.lso;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.control.beans.OccupancyForm;
import elms.util.StringUtils;

public class EditOccupancyAddressAction extends Action {

	static Logger logger = Logger.getLogger(EditOccupancyAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		HttpSession session = request.getSession();

		try {

			String lsoId = request.getParameter("lsoId"); // 21762
			String addressId = request.getParameter("addressId"); // 21762

			logger.info("Lso id	is : " + lsoId);
			logger.info("Address Id is : " + addressId);

			Occupancy occupancy = null;
			List occupancyAddressList = null;
			OccupancyAddress occupancyAddress = null;

			AddressAgent occupancyAgent = new AddressAgent();
			occupancy = occupancyAgent.getOccupancy(Integer.parseInt(lsoId));
			logger.info("occupancy is" + occupancy);

			// Remove the obsolete form bean
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
			request.setAttribute("lsoId", lsoId);
			request.setAttribute("addressId", addressId);

			ActionForm frm = new OccupancyForm();
			request.setAttribute("occupancyForm", frm);
			OccupancyForm occupancyForm = (OccupancyForm) frm;
			logger.warn("out side	if in EOAA");

			if (occupancy != null) {
				occupancyAddressList = occupancy.getOccupancyAddress();
				occupancyAddress = (OccupancyAddress) occupancyAddressList.get(0);

				logger.warn("occupancy address is" + occupancyAddress);

				Iterator i = occupancyAddressList.iterator();
				while (i.hasNext()) {
					logger.warn("in side iterator");
					occupancyAddress = (OccupancyAddress) i.next();
					occupancyForm.setAddressId(occupancyAddress.getAddressId());
					occupancyForm.setStreetNumber("" + occupancyAddress.getStreetNbr());
					occupancyForm.setStreetName(StringUtils.i2s(occupancyAddress.getStreet().getStreetId()));
					occupancyForm.setStreetMod(occupancyAddress.getStreetModifier());
					occupancyForm.setCity(occupancyAddress.getCity());
					occupancyForm.setState(occupancyAddress.getState());
					occupancyForm.setZip(occupancyAddress.getZip());
					occupancyForm.setDescription(occupancyAddress.getDescription());
					occupancyForm.setPrimary(StringUtils.s2b(occupancyAddress.getPrimary()));
					occupancyForm.setActive(StringUtils.s2b(occupancyAddress.getActive()));

					// request.setAttribute("ownerId",occupancyApn.getOwnerId());
					// logger.warn(occupancyAddress.getStreetName()+" "+ occupancyAddress.getStreetMod()+" "+ occupancyAddress.getStreetName()+" "+occupancyAddress.getCity()+" "+occupancyAddress.getState()+" "+occupancyAddress.getZip()+" "+occupancyAddress.getDescription()+" "+occupancyAddress.getPrimary()+" "+occupancyAddress.getActive());
				}

			}

		} catch (Exception e) {
			// logger.warn("error anand"+ e.getMessage());
			System.out.println("Error in EditOccupancyAddressAction");
		}
		logger.warn("going for success page");
		return (mapping.findForward("success"));

	}

}
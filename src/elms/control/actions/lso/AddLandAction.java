package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class AddLandAction extends Action {
	static Logger logger = Logger.getLogger(AddLandAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		elms.control.beans.LandForm landForm = (elms.control.beans.LandForm) form;
		if (landForm.getLsoId() == null || ("").equalsIgnoreCase(landForm.getLsoId())) {
			landForm.reset(mapping, request);
		}
		return (mapping.findForward("success"));
	}
}

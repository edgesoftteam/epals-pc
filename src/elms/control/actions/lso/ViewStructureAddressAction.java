package elms.control.actions.lso;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

public class ViewStructureAddressAction extends Action {

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need
		Locale locale = getLocale(request);
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		// make the form like below -- if not coded, struts will assume a null form - AB(4-24-2002)
		// XYZForm xyzForm = (XYZForm) form;

		// make sure you forward it to the right place, need not be 'success' always- AB(4-24-2002)
		if (isCancelled(request)) {
			if (servlet.getDebug() >= 1)
				servlet.log(" Transaction was cancelled");
			if (mapping.getAttribute() != null)
				session.removeAttribute(mapping.getAttribute());
			return (mapping.findForward("success"));
		}

		ActionErrors errors = new ActionErrors();
		if (servlet.getDebug() >= 1) {
			servlet.log(" Checking transactional control token");
		}

		// Report any errors we have discovered back to the original form
		// This needs an input form to go back if it has errors..make sure you have
		// a input reference to this action - AB(4-24-2002)
		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				// user proper log4j logging API, the below is an example of default
				// servlet log, please dont use it...- AB(4-24-2002)
				servlet.log(" forwarding data to update user table...");
				// make appropriate instance of the agent class to update the database
				// shown below is just an example, nothing called UserAgent exists..- AB(4-24-2002)
				// UserAgent userAgent = new UserAgent();
				// int i = userAgent.populateDatabase(customer);
			} catch (Exception e) {
				e.printStackTrace();
				// handle errors appropriately and throw meaningful exceptions - AB(4-24-2002)
			}
		}

		// Remove the obsolete form bean
		// VERY IMPORTANT - else you are not releasing memory, you may be fired
		// if your boss comes to know that you are not removing objects from memory :-)
		// howzzzzat - AB(4-24-2002)
		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		// Forward control to the specified success URI
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to success page");
		return (mapping.findForward("success"));

	}

}
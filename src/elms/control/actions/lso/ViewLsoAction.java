package elms.control.actions.lso;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

public class ViewLsoAction extends Action {

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		if (isCancelled(request)) {
			if (servlet.getDebug() >= 1)
				servlet.log(" Transaction was cancelled");
			if (mapping.getAttribute() != null)
				session.removeAttribute(mapping.getAttribute());
			return (mapping.findForward("success"));
		}

		ActionErrors errors = new ActionErrors();
		if (servlet.getDebug() >= 1) {
			servlet.log(" Checking transactional control token");
		}

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				servlet.log(" forwarding data to update user table...");
			} catch (Exception e) {
			}
		}

		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope()))
				request.removeAttribute(mapping.getAttribute());
			else
				session.removeAttribute(mapping.getAttribute());
		}

		// Forward control to the specified success URI
		if (servlet.getDebug() >= 1)
			servlet.log(" Forwarding to success page");
		return (mapping.findForward("success"));

	}

}
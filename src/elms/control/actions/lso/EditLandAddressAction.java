package elms.control.actions.lso;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.LsoAddress;
import elms.control.beans.LandForm;
import elms.util.StringUtils;

public class EditLandAddressAction extends Action {

	static Logger logger = Logger.getLogger(EditLandAddressAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {

			int addressId = -1;
			String strAddressId = request.getParameter("addressId");
			addressId = StringUtils.s2i(strAddressId);
			int lsoId = -1;
			String strLsoId = request.getParameter("lsoId");
			lsoId = StringUtils.s2i(strLsoId);
			LsoAddress lsoAddress = null;

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LandForm();
			request.setAttribute("landForm", frm);
			request.setAttribute("lsoId", strLsoId);
			request.setAttribute("addressId", strAddressId);
			logger.debug("setting address id " + strAddressId);
			LandForm landForm = (LandForm) frm;
			logger.debug("created land form");

			logger.debug("before getting land address, lsoId=" + lsoId);
			lsoAddress = new AddressAgent().getLsoAddress(addressId);
			landForm.setLsoAddress(lsoAddress);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}
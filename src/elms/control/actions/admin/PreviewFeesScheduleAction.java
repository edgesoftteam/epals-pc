package elms.control.actions.admin;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.app.admin.FeeEdit;
import elms.control.beans.FeeScheduleForm;
import elms.control.beans.LookupFeesForm;
import elms.util.StringUtils;

public class PreviewFeesScheduleAction extends Action {

	static Logger logger = Logger.getLogger(PreviewFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			List feeList = new ArrayList();

			FeeScheduleForm subForm = (FeeScheduleForm) session.getAttribute("feeScheduleForm");

			double percentChange = (StringUtils.s2d(subForm.getPercent()) + 100.0d) / 100.0d;
			BigDecimal bdChange = new BigDecimal(percentChange);

			logger.info("percentChange is 100.00 + " + subForm.getPercent() + " = " + percentChange + "(" + bdChange + ")");

			BigDecimal bdFeeFactor = new BigDecimal("0.00");
			BigDecimal bdFactor = new BigDecimal("0.00");

			FeeEdit[] lst = subForm.getFeeEditList();

			for (int i = 0; i < lst.length; i++) {
				logger.info(i + "-" + lst[i].getCheck() + "-");
				if (lst[i].getUp().equals("on")) {
					bdFeeFactor = StringUtils.s2bd(lst[i].getFeeFactor()).multiply(bdChange).setScale(1, BigDecimal.ROUND_HALF_UP);
					bdFactor = StringUtils.s2bd(lst[i].getFactor()).multiply(bdChange).setScale(1, BigDecimal.ROUND_HALF_UP);
					lst[i].setFeeFactor(bdFeeFactor.toString());
					lst[i].setFactor(bdFactor.toString());
					lst[i].setEdited("1");
					lst[i].setUp("");
				}
				feeList.add(lst[i]);
			}

			FeeEdit[] aryFee = new FeeEdit[feeList.size()];
			for (int i = 0; i < feeList.size(); i++) {
				aryFee[i] = (FeeEdit) feeList.get(i);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LookupFeesForm();
			FeeScheduleForm frmFee = new FeeScheduleForm();

			frmFee.setFeeEditList(aryFee);
			session.setAttribute("feeScheduleForm", frmFee);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}
}
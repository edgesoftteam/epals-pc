package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.app.admin.LookupSystem;
import elms.app.common.DisplayItem;
import elms.control.actions.ApplicationScope;
import elms.control.beans.admin.LookupSystemForm;
import elms.util.StringUtils;

/**
 * @author Gayathri Turlapati
 * 
 */
public class LookupSystemAction extends Action {
	static Logger logger = Logger.getLogger(LookupSystemAction.class.getName());
	int result = -1;
	
	public ActionForward perform(ActionMapping mapping, ActionForm form,HttpServletRequest request, HttpServletResponse response)throws IOException, ServletException {
		
		logger.debug("Entered into LookupSystemAction");
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		LookupAgent lookupAgent = new LookupAgent();
		
		LookupSystemForm lookupSystemForm=(LookupSystemForm) form;		
		List<LookupSystem> lookupSystems = new ArrayList<LookupSystem>();
		
		try {
			
			if (request.getParameter("lookup") != null) {
				result = 0;
				lookupSystemForm.setName(StringUtils.nullReplaceWithEmpty(request.getParameter("nameStr")));
				lookupSystemForm.setValue(StringUtils.nullReplaceWithEmpty(request.getParameter("value")));
				
				lookupSystems = lookupAgent.getLookupSystemData(lookupSystemForm);

				session.removeAttribute("lookupSystemForm");
				session.setAttribute("oldLookupSystemForm", lookupSystemForm);

				lookupSystemForm.setLookupSystemTypes(lookupSystems);
				lookupSystemForm.setDisplayLookupSystems("YES");

				PrintWriter pw = response.getWriter();
				if (lookupSystems.size() == 0) {
					pw.write("No Records Found");
				} else {
					pw.write(innerTable(lookupSystems));
				}
				return null;
			} else if (request.getParameter("id") != null) {
				result = 0;
				LookupSystemForm lkupSysForm = lookupAgent.getLookupSystemData(StringUtils.s2i(request.getParameter("id")));

				lookupSystemForm.setSystemId(lkupSysForm.getSystemId());
				lookupSystemForm.setName(lkupSysForm.getName());
				lookupSystemForm.setValue(lkupSysForm.getValue());				
				lookupSystemForm.setDisplayLookupSystems("YES");
				
				String valueStr = lookupSystemForm.getValue();

				PrintWriter pw = response.getWriter();
				pw.write(lookupSystemForm.getName().toString() + ',' + (((valueStr == null) || (valueStr.equals(""))) ? StringUtils.nullReplaceWithEmpty(valueStr) : valueStr.toString())+ ',' + lookupSystemForm.getSystemId());
				return null;

			} else if (request.getParameter("save") != null) {

				LookupSystemForm lkupsysForm = (LookupSystemForm) session.getAttribute("oldLookupSystemForm");
				// session.removeAttribute("oldActTypeForm");

				
				lookupSystemForm.setName(request.getParameter("nameStr"));
				lookupSystemForm.setValue(request.getParameter("value"));
				lookupSystemForm.setSystemId(StringUtils.s2i(request.getParameter("systemId")));

				result = lookupAgent.saveLookupSystem(lookupSystemForm);
				lookupSystemForm.setDisplayLookupSystems("NO");

				if (result == 1) {
					PrintWriter pw = response.getWriter();
					pw.write("System Values Saved Successfully");
				} else if (result == 2) {
					lookupSystems = lookupAgent.getLookupSystemData(lkupsysForm);

					PrintWriter pw = response.getWriter();
					pw.write(innerTable(lookupSystems));
				} else {
					errors.add("statusCodeExists", new ActionError("statusCodeExists", " System List is not added"));
					saveErrors(request, errors);
				}
				if(lookupSystemForm.getName()!=null && (lookupSystemForm.getName().equalsIgnoreCase("SOLR_DELTA_INDEX")|| lookupSystemForm.getName().equalsIgnoreCase("SOLR_ATTACHMENTS_DELTA_INDEX"))){
					new ApplicationScope().deltaIndexing();
				}else if(lookupSystemForm.getName()!=null && (lookupSystemForm.getName().equalsIgnoreCase("SOLR_ATTACHMENTS_DELTA_INDEX_TIME")|| lookupSystemForm.getName().equalsIgnoreCase("SOLR_DELTA_INDEX_TIME"))){
					new ApplicationScope().deltaIndexing();
				}
				return null;
			} else if (request.getParameter("delete") != null) {			
				lookupSystemForm.setName(StringUtils.nullReplaceWithEmpty(request.getParameter("nameStr")));
				lookupSystemForm.setValue(StringUtils.nullReplaceWithEmpty(request.getParameter("value")));
				lookupSystemForm.setSystemId(StringUtils.s2i(request.getParameter("systemId")));

				String idList = (String) request.getParameter("checkboxArray");

				result = lookupAgent.deleteSystemId(idList);
				logger.debug("Result %%%%% " + result);

				LookupSystemForm lkupSysDelForm = (LookupSystemForm) session.getAttribute("oldLookupSystemForm");
				// session.removeAttribute("oldActTypeForm");

				lookupSystems = lookupAgent.getLookupSystemData(lkupSysDelForm);
				lookupSystemForm.setDisplayLookupSystems("YES");

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(lookupSystems));
				return null;
			} else {
				lookupSystemForm = new LookupSystemForm();
				lookupSystemForm.setLookupSystemTypes(lookupSystems);
				lookupSystemForm.setDisplayLookupSystems("NO");
				session.setAttribute("lookupSystemForm", lookupSystemForm);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return (mapping.findForward("success"));
	}
		
	
	
	public String innerTable(List lookupSystems) {
		String actTypesHtml = "";
		if (lookupSystems != null && lookupSystems.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem lookupSysItems = null;

			if (lookupSystems != null && lookupSystems.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">System List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				logger.debug("$#$#$#$#$#$ " + result);
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					logger.debug("$#$#$#$#$#$ ");
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">System Admin Types</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add System Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"45%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">System Name</font></td>");
				sb.append("<td width=\"45%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">System Value</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < lookupSystems.size(); i++) {
					lookupSysItems = (DisplayItem) lookupSystems.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (lookupSysItems.getFieldFour().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + lookupSysItems.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td></td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + lookupSysItems.getFieldOne() + "," + StringUtils.checkString(lookupSysItems.getFieldFour()) + ")\" >");
					sb.append(lookupSysItems.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td  style=\"word-break: break-all\" bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(lookupSysItems.getFieldThree());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td></td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\"></td>");
				sb.append("</tr><tr><td></td></tr></table></td></tr></table>");
			}
			actTypesHtml = sb.toString().replaceAll("&", "&amp;");
			//actTypesHtml = StringEscapeUtils.unescapeHtml(actTypesHtml);
			
			// logger.debug(actTypesHtml);
		}
		return actTypesHtml;
	}


}
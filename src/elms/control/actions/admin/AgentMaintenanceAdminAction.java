package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.PeopleAgent;
import elms.app.people.People;
import elms.security.User;

public class AgentMaintenanceAdminAction extends Action {
	String contextRoot = "";

	static Logger logger = Logger.getLogger(AgentMaintenanceAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into AgentMaintenanceAdminAction .. ");

		HttpSession session = request.getSession();
		PeopleAgent peopleAgent = new PeopleAgent();
		
		User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		
		String action = request.getParameter("action");
		
		if(action == null)
			action = "";
		
		logger.debug("action "+action);
		List<People> agentContractorList = new ArrayList<People>();
		try {
			String filter = "";
			if(action.equalsIgnoreCase("verify")){
				String caId = request.getParameter("caId");
				String startDate = request.getParameter("startDate");
				String expDate = request.getParameter("expDate");
				
				logger.debug("verify ca id "+ caId +" start date "+startDate +" expp date "+expDate);
				peopleAgent.verfiyContractorAgent(caId, user.getUserId(), startDate, expDate);
			} else if(action.equalsIgnoreCase("delete")){
				String caId = request.getParameter("caId");
				logger.debug(" delete ca id "+ caId );
				peopleAgent.deleteContractorAgent(caId);
			} else if(action.equalsIgnoreCase("filter")){
				filter = request.getParameter("filterData");
				logger.debug(" filter data : "+  filter);
			}
			agentContractorList = peopleAgent.getContractorAgentList("UNVERIFIED",filter);
			request.setAttribute("filter",filter);
		} catch (Exception e) {
			logger.error("Excpetion in Agent Maintenance Action."+e.getMessage());
			e.printStackTrace();
		}
		
		request.setAttribute("agentContractorList", agentContractorList);
		return (mapping.findForward("success"));
	}
}

package elms.control.actions.admin;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.control.beans.FeeInitializeForm;
import elms.util.StringUtils;

public class FeesIntializeAction extends Action {
	static Logger logger = Logger.getLogger(FeesIntializeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		boolean success = false;
		Calendar today = Calendar.getInstance();

		try {
			String feeDate = request.getParameter("feeDate");
			if (feeDate != null) {
				success = new FinanceAgent().initializeFees(StringUtils.str2cal(feeDate));
				logger.debug(" This is fee value " + success);
				new FinanceAgent().updateFeeActSubType();// update for table fee_actsubtype
			} else {
				feeDate = StringUtils.cal2str(today);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			FeeInitializeForm frmInit = new FeeInitializeForm();
			frmInit.setFeeDate(feeDate);
			if (success) {
				frmInit.setStrMessage("Fee Schedule has been Initialized");
			}

			request.setAttribute("feeInitializeForm", frmInit);
			return (mapping.findForward("success"));
		} catch (Exception e) {
			throw new ServletException("Fees did not initialize...error is " + e.getMessage());
		}

	}
}

// End class

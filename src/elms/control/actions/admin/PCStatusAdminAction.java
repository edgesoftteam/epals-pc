/**
 * 
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.PlanCheckStatus;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.PCStatusForm;
import elms.util.StringUtils;
import elms.util.Util;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gaurav Garg
 *
 */
public class PCStatusAdminAction extends Action {
	
	static Logger logger = Logger.getLogger(PCStatusAdminAction.class
			.getName());
	int result = -1;
	public ActionForward perform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
		logger.debug("in PCStatusAdminAction");
		String nextpage = "success";
		ActionErrors errors = new ActionErrors();
		HttpSession session = request.getSession();
		
		PCStatusForm pcStatusForm = (PCStatusForm) form;
		AdminAgent adminAgent = new AdminAgent();
	
		List pcStatusList = null;
		try{
		if (request.getParameter("lookup") != null) {
			result = 0;
			pcStatusForm.setCode(StringUtils.s2i(request.getParameter("code")));
			pcStatusForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			/*pcStatusForm.setModuleId(StringUtils.s2i(request.getParameter("moduleId")));*/
			pcStatusForm.setActive(StringUtils.s2b(request.getParameter("active")));
			pcStatusForm.setVisible(StringUtils.s2b(request.getParameter("visible")));

			pcStatusList = adminAgent.getPlanCheckStatus(pcStatusForm);
			
			session.removeAttribute("pcStatusForm");
			session.setAttribute("oldPcStatusForm", pcStatusForm);

			pcStatusForm.setPcStatusList(pcStatusList);;
			pcStatusForm.setDisplayPCStatus("YES");

			PrintWriter pw = response.getWriter();
			if (pcStatusList.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(pcStatusList));
			}
			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			PlanCheckStatus planCheckStatus;
			
			planCheckStatus = LookupAgent.getPlanCheckStatus(StringUtils.s2i(request.getParameter("id")));
			pcStatusForm.setCode(planCheckStatus.getCode());
			pcStatusForm.setDescription(planCheckStatus.getDescription());
			/*pcStatusForm.setModuleId(planCheckStatus.getModuleId());*/
			pcStatusForm.setActive(StringUtils.s2b(planCheckStatus.getActive()));
			pcStatusForm.setVisible(StringUtils.s2b(planCheckStatus.getVisible()));
			pcStatusForm.setDisplayPCStatus("YES");

			PrintWriter pw = response.getWriter();
			//pw.write( pcStatusForm.getCode()+ "," + pcStatusForm.getDescription().toString() + "," + pcStatusForm.getModuleId() + "," + pcStatusForm.isActive() + ","+ pcStatusForm.isVisible());

			pw.write( pcStatusForm.getCode()+ "," + pcStatusForm.getDescription().toString() + "," + pcStatusForm.isActive() + ","+ pcStatusForm.isVisible());
			return null;			
		} else if (request.getParameter("save") != null) {

			PCStatusForm oldPCStatusForm = (PCStatusForm) session.getAttribute("oldPcStatusForm");
			// session.removeAttribute("oldActTypeForm");

			pcStatusForm.setCode(StringUtils.s2i(request.getParameter("code")));
			pcStatusForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			//pcStatusForm.setModuleId(StringUtils.s2i(request.getParameter("moduleId")));
			pcStatusForm.setActive(StringUtils.s2b(request.getParameter("active")));
			pcStatusForm.setVisible(StringUtils.s2b(request.getParameter("visible")));

			result = adminAgent.savePCStatus(pcStatusForm.getCode(), pcStatusForm.getDescription(), pcStatusForm.isActive(), pcStatusForm.isVisible()); //, pcStatusForm.getModuleId()
			pcStatusForm.setDisplayPCStatus("NO");

			if (result == 1) {
				PrintWriter pw = response.getWriter();
				pw.write(" Plan Check Status Saved Successfully");
			} else if (result == 2) {
				pcStatusList = adminAgent.getPlanCheckStatus(oldPCStatusForm);

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(pcStatusList));
			} else {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Plan Check Status is not added"));
				saveErrors(request, errors);
			}
			return null;
		} else if (request.getParameter("delete") != null) {

			pcStatusForm.setCode(StringUtils.s2i(request.getParameter("code")));
			pcStatusForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			//pcStatusForm.setModuleId(StringUtils.s2i(StringUtils.nullReplaceWithEmpty(request.getParameter("moduleId"))));
			pcStatusForm.setActive(StringUtils.s2b(request.getParameter("active")));
			pcStatusForm.setVisible(StringUtils.s2b(request.getParameter("visible")));

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deletePCStatusList(idList);
			logger.debug("Result %%%%% " + result);

			PCStatusForm oldPCStatusForm = (PCStatusForm) session.getAttribute("oldPCStatusForm");
			// session.removeAttribute("oldActTypeForm");

			pcStatusList = adminAgent.getPlanCheckStatus(oldPCStatusForm);
			pcStatusForm.setDisplayPCStatus("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(pcStatusList));
			return null;
		} else {
			PCStatusForm pcStatusForm1= new PCStatusForm();
			//logger.debug("code::"+pcStatusForm1.getCode());
			pcStatusForm1.setPcStatusList(pcStatusList);;
			pcStatusForm1.setDisplayPCStatus("NO");
			
			logger.debug("code::"+pcStatusForm1.getCode());
			session.setAttribute("pcStatusForm", pcStatusForm1);			
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}	
		List modules;
	try {
		modules = LookupAgent.getModules();
	} catch (Exception e) {
		modules = new ArrayList();
		logger.warn("no module list obtained, so initializing it to blank arraylist");
	}
	request.setAttribute("modules", modules);
	//logger.debug("The modules set with size .. " + departments.size());
	
		return (mapping.findForward("success"));
}

	public String innerTable(List pcStatusList) {
		String actTypesHtml = "";
		if (pcStatusList != null && pcStatusList.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem planCheck = null;

			if (pcStatusList != null && pcStatusList.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">Plan Check Status List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				logger.debug("$#$#$#$#$#$ " + result);
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					logger.debug("$#$#$#$#$#$ ");
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Plan Check Status List</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Status\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"25%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Description</font></td>");
				//sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Module</font></td>");
				sb.append("<td width=\"35%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\"><nobr>Active</nobr></font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Visible</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < pcStatusList.size(); i++) {
					planCheck = (DisplayItem) pcStatusList.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (planCheck.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + planCheck.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + planCheck.getFieldOne() + "," + StringUtils.checkString(planCheck.getFieldSix()) + ")\" >");
					sb.append(planCheck.getFieldTwo());
					sb.append("</a></font></td>");
					//sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					//sb.append(planCheck.getFieldThree());
					//sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(StringUtils.nullReplaceWithEmpty(planCheck.getFieldFour()));
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(planCheck.getFieldFive());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actTypesHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return actTypesHtml;
	}


}

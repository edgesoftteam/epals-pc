package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.admin.OnlinePermitForm;
import elms.util.StringUtils;

public class OnlineAcknowledgementAction extends Action {
	static Logger logger = Logger.getLogger(OnlineAcknowledgementAction.class.getName());
	String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered OnlineAcknowledgementAction ...");

		OnlinePermitForm onlinePermitForm = (OnlinePermitForm) form;
		OnlineAgent opma = new OnlineAgent();
		String action = request.getParameter("action") != null ? (String) request.getParameter("action") : "";
		List actSubFeeMap = new ArrayList();
		List actSubtypeList = new ArrayList();
		List actFeeList = new ArrayList();
		
		List questionaireList = new ArrayList();
		List lkupQuestionaireList = new ArrayList();
		String message = "";

		try {

			if(onlinePermitForm.getPlanCheck() ==null){
				onlinePermitForm.setPlanCheck("N");
			}
			String type="";
			if(StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()).equalsIgnoreCase("200200")){
				type= "RONPER";
			}else if(StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()).equalsIgnoreCase("200201")){
				type= "MONPER";
			}else if(StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()).equalsIgnoreCase("200202")){
				type= "CONPER";
			}
			
			if (action.equals("edit")) {
				logger.info("get info"+onlinePermitForm.getStypeId());
				//actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
			} else if (action.equals("save")) {
					opma.saveLkupAcknowledgment(type,onlinePermitForm.getStypeId(), onlinePermitForm.getFeeId(),onlinePermitForm.getQuestionId(),onlinePermitForm.getPlanCheck());
				//actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
				message = "Online Acknowledgement Mapping was saved successfully";
			}else if (action.equals("del")) {
				
				String lkId[] = onlinePermitForm.getDeleteSelectedId();
				for (int i = 0; i < lkId.length; i++) {
					opma.deleteAcknowledgementId(StringUtils.s2i(lkId[i]));
				}
					
				//actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
				message = "Online Acknowledgement Mapping was deleted successfully";
			}
			//actSubtypeList = opma.getActSubTypes();
			
			//if(!type.equals("") || !type.equals("null")){
				actSubtypeList = opma.getActSubTypes(type);
			//}
			actFeeList = opma.getFeesList(onlinePermitForm.getStypeId() + "",type);
			questionaireList = opma.getAcknowledgementList();
			if(onlinePermitForm.getStypeId()>0){
				actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
			}
			lkupQuestionaireList = opma.getLkupAcknowledgement(type, onlinePermitForm.getStypeId(), onlinePermitForm.getFeeId(), onlinePermitForm.getQuestionId());

			request.setAttribute("message", message);
			request.setAttribute("actSubFeeMap", actSubFeeMap);
			request.setAttribute("actSubtypeList", actSubtypeList);
			request.setAttribute("actFeeList", actFeeList);
			request.setAttribute("questionaireList", questionaireList);
			request.setAttribute("lkupQuestionaireList", lkupQuestionaireList);
			onlinePermitForm.setActType(StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()));
			onlinePermitForm.setFeeId(onlinePermitForm.getFeeId());
			onlinePermitForm.setStypeId(onlinePermitForm.getStypeId());
			onlinePermitForm.setPlanCheck(onlinePermitForm.getPlanCheck());
			request.setAttribute("onlinePermitForm", onlinePermitForm);
			
			
		} catch (Exception e) {
			logger.error("error in OnlineAcknowledgementAction " + e.getMessage());
		}
		logger.debug("Exiting OnlineAcknowledgementAction ...");
		return (mapping.findForward(nextPage));
	}
}
package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.MoveVerifyForm;
import elms.util.StringUtils;

public class SaveMoveProjectAction extends Action {

	static Logger logger = Logger.getLogger(SaveMoveProjectAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		try {

			ActionErrors errors = new ActionErrors();

			MoveVerifyForm frm = (MoveVerifyForm) session.getAttribute("moveVerifyForm");
			String projectNbr = (String) session.getAttribute("projectNbr");
			logger.debug("Got Project No from session " + projectNbr);
			int lsoId = StringUtils.s2i((String) session.getAttribute("lsoId"));
			logger.debug("Got Lso No from session " + lsoId);

			AdminAgent moveProjectAgent = new AdminAgent();
			int updno = 0;

			List pNameList = new ArrayList();
			List subProjecNamesList = new ArrayList();
			String pType = "";
			String SubProjName = "";
			List pTypeList = moveProjectAgent.getpType(projectNbr);
			subProjecNamesList = moveProjectAgent.getsubProjectName(projectNbr);

			/* get list of project Names and Types */
			pType = moveProjectAgent.getLsoForOcc(StringUtils.i2s(lsoId));

			if (pType.equals("") || pTypeList.size() == 0) {
				frm.setMessage("Project cannot be moved.");
			}
			int count = 0;
			for (int i = 0; i < subProjecNamesList.size(); i++) {
				/* get sub project Name */
				SubProjName = subProjecNamesList.get(i).toString();

				/* check whether sub project name starts with BT,BL or RP */
				if (SubProjName.startsWith("BL") || SubProjName.startsWith("BT") || SubProjName.startsWith("RP")) {
					/* check whether project type is at of occupancy level and project name is License and Code Services */
					if (pType.equalsIgnoreCase("O")) {
						/* move project */
						count = i;

						if (i == 0) {
							updno = moveProjectAgent.moveProject(projectNbr, lsoId);
							if (updno == -1)
								frm.setMessage("Project cannot be moved.");
							else
								frm.setMessage("Move completed successfully");
						}

					} else if (SubProjName.startsWith("RP") && (!SubProjName.startsWith("BL") || !SubProjName.startsWith("BT"))) {
						/* move project */
						if (i == 0) {
							updno = moveProjectAgent.moveProject(projectNbr, lsoId);
							if (updno == -1)
								frm.setMessage("Project cannot be moved.");
							else
								frm.setMessage("Move completed successfully");
						}

					} else {
						// if errors exist, then forward to the input page.
						if (errors.empty()) {
							logger.debug("This project contains BT or BL subprojects with Regulatory permits, so it cannot be moved to Land /Structure level.Instead you can move  subproject of type Regulatory permit, by using the Move Sub Project Panel to the corresponding projectand try again. ");
							errors.add("statusCodeExists", new ActionError("statusCodeExists", "This project contains BT or BL subprojects with Regulatory permits, so it cannot be moved to Land /Structure level.Instead you can move  subproject of type Regulatory permit, by using the Move Sub Project Panel to the corresponding project and try again."));
						}

						saveErrors(request, errors);
					}
				} else {
					/* move project */
					if (i == 0) {
						updno = moveProjectAgent.moveProject(projectNbr, lsoId);
						if (updno == -1)
							frm.setMessage("Project cannot be moved.");
						else
							frm.setMessage("Move completed successfully");

					}

				}
			}
			if (pTypeList.size() != 0) {
				if (subProjecNamesList.size() == 0) {
					updno = moveProjectAgent.moveProject(projectNbr, lsoId);
					if (updno == -1)
						frm.setMessage("Project cannot be moved.");
					else
						frm.setMessage("Move completed successfully");
				}
			}
			session.setAttribute("moveVerifyForm", frm);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));

	}

}
package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.common.Constants;
import elms.control.beans.online.EmailTemplateAdminForm;
import elms.control.beans.online.EmailTemplateTypeAdminForm;
import elms.security.User;
import elms.util.StringUtils;

public class EmailTemplateAdminAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(EmailTemplateAdminAction.class.getName());
	String nextPage = "";

	@SuppressWarnings("rawtypes")
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ActionErrors errors = new ActionErrors();
		EmailTemplateAdminForm emailTemplateAdminForm = (EmailTemplateAdminForm) form;
		AdminAgent adminAgent = new AdminAgent();
		List emailTempList = new ArrayList();
		HttpSession session = request.getSession();
		List emailTempTypeList = new ArrayList();
		
		try {
			String action = request.getParameter("action") != null ? request.getParameter("action") : "display";
			User user = (User) session.getAttribute("user");
			if (action.equalsIgnoreCase("display")) {
				emailTempList = adminAgent.getEmailTemplateTypes();
				request.setAttribute("emailTempList", emailTempList);
				nextPage = "success";
			}else if (action.equalsIgnoreCase("new")) {
				request.setAttribute("addFlag", "Y");
				emailTemplateAdminForm.setRefEmailTempId(0);
				emailTempTypeList = adminAgent.getEmailTemplateTypesForAdd(new EmailTemplateTypeAdminForm());
				nextPage = "template";
			}else if (action.equalsIgnoreCase("edit")) {
				String id = request.getParameter("id") != null ? request.getParameter("id") : "0";
				emailTemplateAdminForm = adminAgent.getEmailTemplate(StringUtils.s2i(id), Constants.EMIAL_TMEPLATE_FLAG);
				emailTemplateAdminForm.setTitle(emailTemplateAdminForm.getEmailTempTypeId()+"");
				emailTempTypeList = adminAgent.getEmailTemplateTypes(new EmailTemplateTypeAdminForm());
				request.setAttribute("emailTemplateAdminForm", emailTemplateAdminForm);
				nextPage = "template";				
			}else if (action.equalsIgnoreCase("delete")) {
				String[] mapIds = emailTemplateAdminForm.getSelectedItems();
				for (int i = 0; i < mapIds.length; i++) {
					adminAgent.deleteEmailTemplate(StringUtils.s2i(mapIds[i]),user.getUserId());
					logger.debug("DELETE "+mapIds[i]);
				}
				emailTemplateAdminForm.setSelectedItems(null);
				emailTempList = adminAgent.getEmailTemplateTypes();
				request.setAttribute("emailTempList", emailTempList);
				nextPage = "success";				
			}else if (action.equalsIgnoreCase("save")) {
				String content = request.getParameter("CONTENT") != null ? request.getParameter("CONTENT") : "";
				
				content= content.replace("\"", "\\\"");
				emailTemplateAdminForm.setEmailMessage(content);
				
				if(emailTemplateAdminForm.getRefEmailTempId()>0){
					adminAgent.updateEmailTemplate(emailTemplateAdminForm,user.getUserId());
				}else {
					adminAgent.saveEmailTemplate(emailTemplateAdminForm,user.getUserId());
				}
				
				emailTempList = adminAgent.getEmailTemplateTypes();
				request.setAttribute("emailTempList", emailTempList);
				nextPage = "success";
			}

			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the login page");
				saveErrors(request, errors);

				return (new ActionForward(mapping.getInput()));
			}
			request.setAttribute("emailTempTypeList", emailTempTypeList);	
			
			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			logger.error("Exception occured while online logon " + e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}

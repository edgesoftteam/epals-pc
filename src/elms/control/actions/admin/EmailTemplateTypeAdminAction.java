package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.online.EmailTemplateTypeAdminForm;
import elms.util.StringUtils;

/**
 * @author Gayathri Turlapati
 *
 */
public class EmailTemplateTypeAdminAction  extends Action {

    static Logger logger = Logger.getLogger(EmailTemplateTypeAdminAction.class.getName());

    public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        logger.debug("Entered into EmailTemplateTypeAdminAction .. ");
        HttpSession session = request.getSession();
        ActionErrors errors = new ActionErrors();
        int deleteCount = 0;

        String action = request.getParameter("action")!=null?request.getParameter("action"):"";
        AdminAgent adminAgent = new AdminAgent();
        EmailTemplateTypeAdminForm emailTemplateTypeAdminForm = (EmailTemplateTypeAdminForm) form;
        elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
        emailTemplateTypeAdminForm.setCreatedBy(user.getUserId());
        emailTemplateTypeAdminForm.setUpdatedBy(user.getUserId());
        @SuppressWarnings("rawtypes")
		List emailTempTypeList;
                
        try {
	        if (action.equalsIgnoreCase("lookup")) {
	        	 emailTemplateTypeAdminForm.setDisplayEmailTemplateType("YES");
	        	 emailTempTypeList = adminAgent.getEmailTemplateTypes(emailTemplateTypeAdminForm);
	        	 emailTemplateTypeAdminForm.setEmailTempTypeList(emailTempTypeList);
	        }else if (action.equalsIgnoreCase("edit")) {
	        	 emailTemplateTypeAdminForm.setEmailTemplateTypeId(StringUtils.s2i(request.getParameter("emailTemplateTypeId")));	        	
	        	 emailTemplateTypeAdminForm.setDisplayEmailTemplateType("NO");
	        }else if (action.equalsIgnoreCase("Save")) {
	        	 emailTemplateTypeAdminForm.setDisplayEmailTemplateType("NO");
	        	 adminAgent.saveEmailTemplateType(emailTemplateTypeAdminForm);
	        	 errors.add("error", new ActionError("error.email.temp.type.add.success"));		        	 
	        }else if (request.getParameter("id") != null) {

	        	emailTemplateTypeAdminForm = adminAgent.getEmailTemplateType(StringUtils.s2i(request.getParameter("id")));
	        	emailTemplateTypeAdminForm.setDisplayEmailTemplateType("NO");
	        }else if (action.equalsIgnoreCase("delete")) {
	            //geting items to delete
	            String[] mapIds = emailTemplateTypeAdminForm.getSelectedItems();
	            for (int i = 0; i < mapIds.length; i++) {
	            	deleteCount = adminAgent.deleteEmailTemplateType(mapIds[i]);
	            	deleteCount ++;
	            }

	            if(deleteCount > 0) {
	            	errors.add("error", new ActionError("error.email.temp.type.delete.success"));	            	
	            }
	            emailTemplateTypeAdminForm.setDisplayEmailTemplateType("YES");
	            List emailTeTypeList = adminAgent.getEmailTemplateTypes(emailTemplateTypeAdminForm);
	        	emailTemplateTypeAdminForm.setEmailTempTypeList(emailTeTypeList);
	        }else{

	        	emailTemplateTypeAdminForm = new EmailTemplateTypeAdminForm();
	        	emailTemplateTypeAdminForm.setDisplayEmailTemplateType("NO");
	        }

	        saveErrors(request, errors);
	        session.setAttribute("emailTemplateTypeAdminForm", emailTemplateTypeAdminForm);
	        request.setAttribute("emailTemplateTypeAdminForm", emailTemplateTypeAdminForm);	       	       
        }catch (Exception e) {
        	e.printStackTrace();
        	logger.error(e.getMessage());
        }
        return (mapping.findForward("success"));
    }
}

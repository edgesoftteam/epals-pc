package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.control.beans.admin.ActivityTypeForm;

public class ActivityTypeMappingAction extends Action {

	static Logger logger = Logger.getLogger(ActivityTypeMappingAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into ActivityMapingAction .. ");
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();

		String lookup = request.getParameter("lookUp");
		AdminAgent adminAgent = new AdminAgent();
		String mappingId = "";
		if (request.getParameter("lookup") != null) {

			ActivityTypeForm activityTypeForm = (ActivityTypeForm) form;
			List mapings = adminAgent.getUseSPTypeSPSTypeActTypeMaping(activityTypeForm);
			request.setAttribute("mapings", mapings);
		} else if (request.getParameter("save") != null) {

			ActivityTypeForm activityTypeForm = (ActivityTypeForm) form;

			boolean del = false;
			// get maping ids for delete
			List useList = new ArrayList();
			String[] mapIds = activityTypeForm.getSelectedMaping();
			for (int i = 0; i < mapIds.length; i++) {

				adminAgent.deleteUseSPTypeSPSTypeActTypeMaping(mapIds[i]);
				del = true;
			}

			
			String activityType = activityTypeForm.getActivityType();
			String subProjectSubType = activityTypeForm.getSubProjectSubType();
			String subProjectSubSubType = activityTypeForm.getSubProjectSubSubType();
			
			if(!del){
				if (!subProjectSubType.equals("-1") && !subProjectSubSubType.equals("-1") && (activityType != null) && !activityType.equals("")) {
					
					String id = request.getParameter("id");
					
					int result = adminAgent.saveUseSPTypeSPSTypeActTypeMaping(id,subProjectSubType, subProjectSubSubType, activityType);
					if (result == 0) {
	
						errors.add("error", new ActionError("error.activityTypeMapping.exists"));
						saveErrors(request, errors);
					}
				}
			}

			List mapings = adminAgent.getUseSPTypeSPSTypeActTypeMaping(subProjectSubType, subProjectSubSubType);
			request.setAttribute("mapings", mapings);
		} else if (request.getParameter("id") != null){

			String activityType = request.getParameter("actType");
			String subProjectSubType = request.getParameter("subType"); 
			String subProjectSubSubType = request.getParameter("spst");
			
			ActivityTypeForm activityTypeForm = new ActivityTypeForm();
			activityTypeForm.setActivityType(activityType);
			activityTypeForm.setSubProjectSubSubType(subProjectSubSubType);
			activityTypeForm.setSubProjectSubType(subProjectSubType);
			session.setAttribute("activityTypeForm", activityTypeForm);
			List mapings = new ArrayList();
			request.setAttribute("mapings", mapings);
			
			mappingId = request.getParameter("id");
		}
		else {

			ActivityTypeForm activityTypeForm = new ActivityTypeForm();
			session.setAttribute("activityTypeForm", activityTypeForm);
			List mapings = new ArrayList();
			request.setAttribute("mapings", mapings);
		}

		List subProjectSubTypes;
		try {
			subProjectSubTypes = LookupAgent.getSubProjectTypes();
		} catch (Exception e) {
			subProjectSubTypes = new ArrayList();
			logger.warn("subProjectSubTypes is not available, initializing it to blank arraylist");
		}
		request.setAttribute("subProjectSubTypes", subProjectSubTypes);
		logger.debug("The Activity Type Maping Sub Project Sub Types set to request with size " + subProjectSubTypes.size());

		List subProjectSubSubTypes;
		try {
			subProjectSubSubTypes = LookupAgent.getSubProjectSubTypes();
		} catch (Exception e1) {
			subProjectSubSubTypes = new ArrayList();
			logger.warn("subProjectSubSubTypes is not available, initializing it to blank arraylist");
		}
		request.setAttribute("subProjectSubSubTypes", subProjectSubSubTypes);
		logger.debug("The Activity Type Maping Sub Project Sub Sub Types set to request with size " + subProjectSubSubTypes.size());

		List activityTypes;
		try {
			activityTypes = LookupAgent.getActivityTypesWithSubProjectType();
		} catch (Exception e2) {
			activityTypes = new ArrayList();
			logger.warn("activityTypes is not available, initializing it to blank arraylist");
		}
		request.setAttribute("mappingId", mappingId);
		request.setAttribute("activityTypes", activityTypes);
		logger.debug("The Activity Type Maping Activity Types set to request with size " + activityTypes.size());

		return (mapping.findForward("success"));
	}
}

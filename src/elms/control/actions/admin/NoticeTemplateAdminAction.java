package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.control.beans.admin.NoticeTemplateAdminForm;
import elms.security.User;
import elms.util.StringUtils;

public class NoticeTemplateAdminAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(NoticeTemplateAdminAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ActionErrors errors = new ActionErrors();
		NoticeTemplateAdminForm noticeTemplateAdminForm= (NoticeTemplateAdminForm) form;
		AdminAgent adminAgent = new AdminAgent();
		ArrayList noticeTemplateList = new ArrayList();
		HttpSession session = request.getSession();
		logger.debug("At Action NoticeTemplateAdminAction");
		
		try {
			String action = request.getParameter("action") != null ? request.getParameter("action") : "display";
			User user = (User) session.getAttribute("user");
			if (action.equalsIgnoreCase("display")) {
				logger.debug("At Action display");
				noticeTemplateList = adminAgent.getNoticeTemplatesList();
				request.setAttribute("noticeTemplateList", noticeTemplateList);
				nextPage = "success";
			} else if (action.equalsIgnoreCase("new")) {
				logger.debug("At Action new");
				LookupAgent lkupAgent = new LookupAgent();
				ArrayList actionList = lkupAgent.getActionList();
				request.setAttribute("actionList", actionList);
				ArrayList noticeTypeList = adminAgent.getNoticeTypes();
				request.setAttribute("noticeTypeList", noticeTypeList);
				noticeTemplateAdminForm.setId(0);
				nextPage = "template";
			}else if (action.equalsIgnoreCase("edit")) {
				logger.debug("At Action edit");
				LookupAgent lkupAgent = new LookupAgent();
				ArrayList actionList = lkupAgent.getActionList();
				request.setAttribute("actionList", actionList);
				ArrayList noticeTypeList = adminAgent.getNoticeTypes();
				request.setAttribute("noticeTypeList", noticeTypeList);
				String id = request.getParameter("id") != null ? request.getParameter("id") : "0";
				noticeTemplateAdminForm = adminAgent.getNoticeTemplate(StringUtils.s2i(id));
				request.setAttribute("noticeTemplateAdminForm", noticeTemplateAdminForm);
				nextPage = "template";
				
			}else if (action.equalsIgnoreCase("delete")) {
				// geting items to delete
				logger.debug("At Action delete"); 
				String[] mapIds = noticeTemplateAdminForm.getSelectedItems();
				for (int i = 0; i < mapIds.length; i++) {
					adminAgent.deleteNoticeTemplate(StringUtils.s2i(mapIds[i]),user.getUserId());
					logger.debug("DELETE "+mapIds[i]);
				}
				noticeTemplateAdminForm.setSelectedItems(null);
				noticeTemplateList = adminAgent.getNoticeTemplatesList();
				request.setAttribute("noticeTemplateList", noticeTemplateList);
				nextPage = "success";
				
			}else if (action.equalsIgnoreCase("save")) {
				String content = request.getParameter("CONTENT") != null ? request.getParameter("CONTENT") : "";
				//noticeTemplateAdminForm.setMessage(content);
				logger.debug("***********"+content);
				noticeTemplateAdminForm.setNoticeTemplate(content);
				if(noticeTemplateAdminForm.getId()>0){
					adminAgent.updateNoticeTemplate(noticeTemplateAdminForm,user.getUserId());
				}else {
					adminAgent.saveNoticeTemplate(noticeTemplateAdminForm,user.getUserId());
				}
				
				noticeTemplateList = adminAgent.getNoticeTemplatesList();
				request.setAttribute("noticeTemplateList", noticeTemplateList);
				nextPage = "success";
			}

			// these are required for the menu navigation
			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the login page");
				saveErrors(request, errors);

				return (new ActionForward(mapping.getInput()));
			}
		
		
			
			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured while online logon " + e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}

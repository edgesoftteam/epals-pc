package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.app.admin.FeeEdit;
import elms.control.beans.FeeScheduleForm;

/**
 * This action maintains the fee schedule
 * 
 * @autor = Anand Belaguly <anand@edgesoftinc.com>
 * @start date = Aug 4, 2002
 */

public class CancelFeesScheduleAction extends Action {

	static Logger logger = Logger.getLogger(MaintainFeesScheduleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		// Get the existing instance of the fees schedule form
		FeeScheduleForm feeScheduleForm = (FeeScheduleForm) form;

		try {
			// obtain the list of fees for a selected acivivity type
			List feeList = new FinanceAgent().getFees("");
			logger.debug("Activity Type () has size " + feeList.size());

			FeeEdit[] aryFee = new FeeEdit[feeList.size()];
			for (int i = 0; i < feeList.size(); i++) {
				aryFee[i] = (FeeEdit) feeList.get(i);
			}

			FeeScheduleForm frmFee = new FeeScheduleForm();

			frmFee.setFeeEditList(aryFee);
			frmFee.setActivityType("");
			session.setAttribute("feeScheduleForm", frmFee);
			session.setAttribute("activityType", "");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}
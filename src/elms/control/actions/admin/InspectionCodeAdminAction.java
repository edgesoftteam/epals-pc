/**
 * 
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.InspectionCode;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.ActivityStatusAdminForm;
import elms.control.beans.admin.InspectionCodeForm;
import elms.exception.AgentException;
import elms.util.StringUtils;
import elms.util.Util;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * @author Gaurav Garg
 *
 */
public class InspectionCodeAdminAction extends Action{
	static Logger logger = Logger.getLogger(InspectionCodeAdminAction.class.getName());
	int result = -1;
	
	public ActionForward perform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
		logger.debug("in InspectionCodeAdminAction");
		String nextpage = "success";

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		InspectionCodeForm inspectionCodeForm = (InspectionCodeForm) form;
		AdminAgent adminAgent = new AdminAgent();
		if(request.getParameter("firstTime") != null && request.getParameter("firstTime").equalsIgnoreCase("yes")){
			inspectionCodeForm = new InspectionCodeForm();
		}
		List inspCodes = new ArrayList();
		inspectionCodeForm.setDisplayStatus("NO");
		String id = request.getParameter("id");
		logger.debug("id : "+id);
		if (request.getParameter("lookup") != null) {
			result = 0;
			// getting and setting all the values to the form
			inspectionCodeForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			inspectionCodeForm.setModuleId(request.getParameter("moduleId"));
			inspectionCodeForm.setDeptId(request.getParameter("deptId"));

			inspectionCodeForm.setActive( StringUtils.s2b(request.getParameter("active")));

			inspCodes = adminAgent.getInspCodes(inspectionCodeForm);

			session.removeAttribute("inspectionCodeForm");
			session.setAttribute("oldInspectionCodeForm", inspectionCodeForm);

			inspectionCodeForm.setDisplayStatus("YES");

			PrintWriter pw = response.getWriter();
			if (inspCodes.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(inspCodes));
			}
			return null;
		}else if(id!=null){// condition to populate values to jsp page
			result = 0;
			inspectionCodeForm = adminAgent.getInspectionCode(StringUtils.s2i(request.getParameter("id")), inspectionCodeForm);
			inspectionCodeForm.setDisplayStatus("NO");

			PrintWriter pw = response.getWriter();
			pw.write(inspectionCodeForm.getDescription().toString() + ',' + inspectionCodeForm.isActive()+ ',' + inspectionCodeForm.getModuleId().toString() + ',' + inspectionCodeForm.getDeptId().toString()+','+inspectionCodeForm.getCode());
			return null;		
		}else if (request.getParameter("save") != null) { // condition to save

			InspectionCodeForm inspCodeForm = (InspectionCodeForm) session.getAttribute("oldInspectionCodeForm");

			inspectionCodeForm.setCode(StringUtils.s2i(request.getParameter("code")));
			inspectionCodeForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			inspectionCodeForm.setModuleId(request.getParameter("moduleId"));
			inspectionCodeForm.setDeptId(request.getParameter("deptId"));
			inspectionCodeForm.setActive( StringUtils.s2b(request.getParameter("active")));
			
			// save function called from Adminagent
			result = adminAgent.saveInspCode(inspectionCodeForm.getCode(), inspectionCodeForm.getDescription(), inspectionCodeForm.isActive(), inspectionCodeForm.getDeptId(), inspectionCodeForm.getModuleId());
			inspectionCodeForm.setDisplayStatus("NO");

			if (result == 1) {
				PrintWriter pw = response.getWriter();
				pw.write("Inspection Action Code Saved Successfully");
			} else if (result == 2) {
				inspCodes = adminAgent.getInspCodes(inspectionCodeForm);

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(inspCodes));
			} else {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Action Code is not added"));
				saveErrors(request, errors);
			}
			return null;
		}else if (request.getParameter("delete") != null) {// condition to delete

			// getting and setting all the values to the form
			inspectionCodeForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));

			boolean active = StringUtils.s2b(request.getParameter("active"));
			String idList = (String) request.getParameter("checkboxArray");

			inspectionCodeForm.setActive(active);
			inspectionCodeForm.setDeptId(request.getParameter("deptId"));
			inspectionCodeForm.setModuleId(request.getParameter("moduleId"));

			result = adminAgent.deleteInspCodes(idList);

			InspectionCodeForm oldInspCodeForm = (InspectionCodeForm) session.getAttribute("oldInspectionCodeForm");

			inspectionCodeForm.setDisplayStatus("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(adminAgent.getInspCodes(oldInspCodeForm)));
			return null;
		} else{
		
		try{
		setDropDownValues(request);
		}catch (Exception e) {
			e.printStackTrace();
		}
		inspectionCodeForm.setDisplayStatus("NO");
		session.setAttribute("inspectionCodeForm", inspectionCodeForm);
		}
		return (mapping.findForward(nextpage));
	}
	
	private void setDropDownValues(HttpServletRequest request) throws Exception{
		List moduleList = LookupAgent.getModules();
		request.setAttribute("moduleList", moduleList);

		List departmentList = LookupAgent.getDepartmentList();
		request.setAttribute("departmentList", departmentList);
	}
	public String innerTable(List inspCodes) {
		String actStatusHtml = "";
		if (inspCodes != null && inspCodes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem inspCode = null;

			if (inspCodes != null && inspCodes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"100%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><font class=\"con_hdr_3b\">Inspection Code List<br><br></font></td></tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"98%\" height=\"25\" bgcolor=\"#B7C1CB\"><font class=\"con_hdr_2b\">Action Code</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Action Code\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\">");
				sb.append("<tr bgcolor=\"#FFFFFF\">");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Action Code</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Department</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\"><nobr>Module</nobr></font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Active</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < inspCodes.size(); i++) {
					inspCode = (DisplayItem) inspCodes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (inspCode.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + inspCode.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + inspCode.getFieldOne() + "," + StringUtils.checkString(inspCode.getFieldSix()) + ")\" >");
					sb.append(inspCode.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(inspCode.getFieldFour() );
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(StringUtils.nullReplaceWithEmpty(inspCode.getFieldThree()));
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(inspCode.getFieldFive());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actStatusHtml = sb.toString();
			// logger.debug(actStatusHtml);
		}
		return actStatusHtml;
	}
}

package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.authorize.Environment;
import net.authorize.api.contract.v1.GetTransactionDetailsRequest;
import net.authorize.api.contract.v1.GetTransactionDetailsResponse;
import net.authorize.api.contract.v1.MerchantAuthenticationType;
import net.authorize.api.contract.v1.MessageTypeEnum;
import net.authorize.api.controller.GetTransactionDetailsController;
import net.authorize.api.controller.base.ApiOperationBase;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.Transaction;
import elms.security.User;
import elms.util.StringUtils;

public class ManageTransactionAdminAction extends Action {
	String contextRoot = "";

	static Logger logger = Logger.getLogger(ManageTransactionAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into ManageTransactionAdminAction .. ");

		HttpSession session = request.getSession();
		AdminAgent adminAgent = new AdminAgent();
		
		User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		
		String action = request.getParameter("action");
		
		if(action == null)
			action = "";
		
		logger.debug("action "+action);
		List<Transaction> transactionList = new ArrayList<Transaction>();
		try {
			String permitNumber = "";
			if(action.equalsIgnoreCase("filter")){
				permitNumber = request.getParameter("permitNumber");
				logger.debug("permitNumber "+permitNumber);
				transactionList = adminAgent.getTransactionList(permitNumber);
			} else if(action.equalsIgnoreCase("transaction")){
				String id = request.getParameter("id");
				String aid = request.getParameter("aid");
				logger.debug("id : "+id);
				logger.debug("aid "+aid);
				
				String buffer = getPaymentTransactionDetails(id,aid);
				logger.debug(buffer);
				response.getWriter().println(buffer);
				return (mapping.findForward(null));
			}
			request.setAttribute("permitNumber",permitNumber);
		} catch (Exception e) {
			logger.error("Excpetion in ManageTransactionAdminAction."+e.getMessage());
			e.printStackTrace();
		}
		
		request.setAttribute("transactionList", transactionList);
		return (mapping.findForward("success"));
	}
	

	private String getPaymentTransactionDetails(String tranId,String actId){ 
		ApiOperationBase.setEnvironment(Environment.PRODUCTION);
        MerchantAuthenticationType merchantAuthenticationType  = new MerchantAuthenticationType() ;
        StringBuffer data = new StringBuffer();
        
        try {
        	ActivityAgent agent = new ActivityAgent();
        	String deptCode = agent.getDepartmentCode(StringUtils.s2i(actId));
        	logger.debug("dept code : "+deptCode);
        	
        	String apiLogin = "";
			String transactionkey = "";
        	
        	if(deptCode.equalsIgnoreCase("BS")){
        		apiLogin = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_CDD_BLDG_PERMIT_STANDARD_API_LOGIN");
    			transactionkey = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_CDD_BLDG_PERMIT_STANDARD_TRANSACTIONKEY");
        	}
        	else if(deptCode.equalsIgnoreCase("PL")){
        		apiLogin = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_CDD_PLANNING_ECOM_API_LOGIN");
    			transactionkey = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_CDD_PLANNING_ECOM_TRANSACTIONKEY");
        	}
        	else if(deptCode.equalsIgnoreCase("PK")){
        		apiLogin = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_CDD_LNCV_PARKING_API_LOGIN");
    			transactionkey = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_CDD_LNCV_PARKING_TRANSACTIONKEY");
        	}
        	else if(deptCode.equalsIgnoreCase("PW")){
        		apiLogin = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_PUBLICWORKS_ECOM_API_LOGIN");
    			transactionkey = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_PUBLICWORKS_ECOM_TRANSACTIONKEY");
        	}
        	else if(deptCode.equalsIgnoreCase("LC")){
        		apiLogin = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_BLBT_ECOM_API_LOGIN");
    			transactionkey = LookupAgent.getKeyValue("PAYMENT_AUTHORIZE_BLBT_ECOM_TRANSACTIONKEY");
        	}
			
			merchantAuthenticationType.setName(apiLogin);
			merchantAuthenticationType.setTransactionKey(transactionkey);
        } catch (JSONException e) {
        	logger.error("Error in getting Merchant Credentails");
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        ApiOperationBase.setMerchantAuthentication(merchantAuthenticationType);
        GetTransactionDetailsRequest getRequest = new GetTransactionDetailsRequest();
        getRequest.setMerchantAuthentication(merchantAuthenticationType);
        getRequest.setTransId(tranId);
      
        GetTransactionDetailsController controller = new GetTransactionDetailsController(getRequest);
        controller.execute();
        GetTransactionDetailsResponse getResponse = controller.getApiResponse();
       if (getResponse!=null) {
    	 logger.debug(getResponse.getMessages().getMessage() );
    	 logger.debug("ResultCode : "+getResponse.getMessages().getResultCode());
    	 if (getResponse.getMessages().getResultCode() == MessageTypeEnum.OK) {
    		 
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getTransId()+"</font></td>");
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getOrder().getDescription()+"</font></td>");
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getTransactionStatus()+"</font></td>");
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getSettleAmount()+"</font></td>");
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getSubmitTimeLocal().toString().replace("T", " ") +"</font></td>");
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getResponseCode()+"</font></td>");
    		 data.append("<td class='tabletext' valign='top'><font class='con_text_1'>"+getResponse.getTransaction().getResponseReasonDescription()+"</font></td>");

    		 logger.debug(" Transaction Id : "+getResponse.getTransaction().getTransId());
    		 logger.debug("SubmitTimeLocal : "+getResponse.getTransaction().getSubmitTimeLocal() );
    		 logger.debug("AuthCode : "+getResponse.getTransaction().getAuthCode());        
    		 logger.debug("TransactionStatus : "+getResponse.getTransaction().getTransactionStatus());
             logger.debug("ResponseCode : "+getResponse.getTransaction().getResponseCode() );
    		 logger.debug(" ResponseReasonDescription : "+getResponse.getTransaction().getResponseReasonDescription() );
    		 logger.debug("SettleAmount : "+getResponse.getTransaction().getSettleAmount());
    		 logger.debug("TransactionType : "+getResponse.getTransaction().getTransactionType());
    		 logger.debug("Permit Number : "+getResponse.getTransaction().getOrder().getDescription() );
    		 logger.debug("activity Id : "+getResponse.getTransaction().getOrder().getInvoiceNumber());
             logger.debug(" ResponseReasonCode : "+getResponse.getTransaction().getResponseReasonCode() );
          }
          else {
        	  data.append("<td class='tabletext' valign='top' colspan='7'><font class='con_text_1'>Unable to find transaction details for "+tranId+"</font></td>");
        	  logger.debug("Failed to get transaction details:  " + getResponse.getMessages().getResultCode());
          }
      }
       
       return data.toString();
	}
}

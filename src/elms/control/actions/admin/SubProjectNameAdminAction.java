package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ProjectType;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.SubProjectLookupsForm;
import elms.util.StringUtils;

public class SubProjectNameAdminAction extends Action {
	static Logger logger = Logger.getLogger(SubProjectNameAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered into SubProjectNameAdminAction .. ");

		HttpSession session = request.getSession();
		AdminAgent adminAgent = new AdminAgent();
		SubProjectLookupsForm sprojLookupsForm = (SubProjectLookupsForm) form;

		List subProjectNames = new ArrayList();

		if (request.getParameter("lookup") != null) {
			result = 0;
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));
			sprojLookupsForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("dept")));

			subProjectNames = adminAgent.getSubProjectNames(sprojLookupsForm);

			session.removeAttribute("sprojLookupsForm");
			session.setAttribute("oldActSubTypeForm", sprojLookupsForm);

			sprojLookupsForm.setSubProjectNameList(subProjectNames);

			PrintWriter pw = response.getWriter();
			if (subProjectNames.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(subProjectNames));
			}
			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			ProjectType projectName = adminAgent.getSubProjectName(StringUtils.s2i(request.getParameter("id")));

			sprojLookupsForm.setSubProjectTypeId(projectName.getProjectTypeId());
			sprojLookupsForm.setDescription(projectName.getDescription());
			sprojLookupsForm.setDepartment(projectName.getDepartment());

			PrintWriter pw = response.getWriter();
			pw.write(sprojLookupsForm.getDescription().toString() + ',' + sprojLookupsForm.getDepartment().toString());
			return null;

		} else if (request.getParameter("save") != null) {

			SubProjectLookupsForm subProjForm = (SubProjectLookupsForm) session.getAttribute("oldActSubTypeForm");

			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));
			sprojLookupsForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("dept")));

			int spId = StringUtils.s2i(request.getParameter("subProjectId"));
			if (spId <= 0)
				spId = 0;
			sprojLookupsForm.setSubProjectTypeId(spId);

			result = adminAgent.saveSubProjectName(sprojLookupsForm.getDescription(), sprojLookupsForm.getDepartment(), sprojLookupsForm.getSubProjectTypeId());
			PrintWriter pw = response.getWriter();

			if (result == 1) {
				pw.write("Project Name Saved Successfully");
			} else if (result == 2) {
				subProjectNames = adminAgent.getSubProjectNames(subProjForm);

				pw.write(innerTable(subProjectNames));
			} else {
				pw.write("");
			}
			return null;

		} else if (request.getParameter("delete") != null) {

			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));
			sprojLookupsForm.setDepartment(StringUtils.nullReplaceWithEmpty(request.getParameter("dept")));

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deleteSubProjectName(idList);

			SubProjectLookupsForm subProjDelForm = (SubProjectLookupsForm) session.getAttribute("oldActSubTypeForm");
			// session.removeAttribute("oldActTypeForm");

			subProjectNames = adminAgent.getSubProjectNames(subProjDelForm);
			PrintWriter pw = response.getWriter();

			if (result == 3) {
				pw.write(innerTable(subProjectNames));
			} else {
				pw.write("");
			}
			return null;
		} else {
			sprojLookupsForm = new SubProjectLookupsForm();
			sprojLookupsForm.setSubProjectNames(subProjectNames);
		}
		List departments;
		try {
			departments = LookupAgent.getDepartmentList();
		} catch (Exception e) {
			departments = new ArrayList();
			logger.warn("no department list obtained, so initializing it to blank arraylist");
		}
		request.setAttribute("departments", departments);
		session.setAttribute("sprojLookupsForm", sprojLookupsForm);

		logger.debug("exit from the action ");

		return (mapping.findForward("success"));
	}

	public String innerTable(List subprojNames) {
		String subProjNameHtml = "";
		if (subprojNames != null && subprojNames.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem sprojNames = null;

			if (subprojNames != null && subprojNames.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">Sub Project Names List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Sub Project Names</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Sub Project Name</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Department</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < subprojNames.size(); i++) {
					sprojNames = (DisplayItem) subprojNames.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (sprojNames.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + sprojNames.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + sprojNames.getFieldOne() + "," + StringUtils.checkString(sprojNames.getFieldSix()) + ")\" >");
					sb.append(sprojNames.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(sprojNames.getFieldThree());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			subProjNameHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return subProjNameHtml;
	}
}

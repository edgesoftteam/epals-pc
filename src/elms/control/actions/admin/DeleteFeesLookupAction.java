package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.control.beans.LookupFeeEdit;
import elms.control.beans.LookupFeesForm;

public class DeleteFeesLookupAction extends Action {

	static Logger logger = Logger.getLogger(MaintainFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			String lookupId = (String) session.getAttribute("lookupId");
			List lookupFeeList = new ArrayList();

			LookupFeesForm subForm = (LookupFeesForm) session.getAttribute("lookupFeesForm");
			LookupFeeEdit[] lst = subForm.getLkupFeeEditList();

			for (int i = 0; i < lst.length; i++) {
				logger.info(i + "-" + lst[i].getCheck() + "-");
				if (!lst[i].getCheck().equals("on")) {
					lookupFeeList.add((LookupFeeEdit) lst[i]);
				}
			}

			LookupFeeEdit[] aryLookupFee = new LookupFeeEdit[lookupFeeList.size()];
			for (int i = 0; i < lookupFeeList.size(); i++) {
				aryLookupFee[i] = (LookupFeeEdit) lookupFeeList.get(i);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LookupFeesForm();
			LookupFeesForm frmFee = (LookupFeesForm) frm;

			try {
				frmFee.setLkupFeeEditList(aryLookupFee);
			} catch (Exception ex) {
				logger.info(ex.getMessage());
			}

			session.setAttribute("lookupFeesForm", frmFee);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

} // End class
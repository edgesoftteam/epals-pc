/*
 * Created on Oct 13, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.admin.OnlinePermitForm;
import elms.util.StringUtils;

public class OnlinePermitAction extends Action {
	static Logger logger = Logger.getLogger(OnlinePermitAction.class.getName());
	String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered OnlinePermitAction ...");

		OnlinePermitForm onlinePermitForm = (OnlinePermitForm) form;
		OnlineAgent opma = new OnlineAgent();
		String action = request.getParameter("action") != null ? (String) request.getParameter("action") : "";
		List actSubFeeMap = new ArrayList();
		List actSubtypeList = new ArrayList();
		List actFeeList = new ArrayList();
		String message = "";

		try {

			if (action.equals("edit")) {
				logger.info("get info"+onlinePermitForm.getStypeId());
				actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
			} else if (action.equals("save")) {
				opma.deleteActSubFeeId(onlinePermitForm.getStypeId());
				if (onlinePermitForm.getSelectedFeeId() != null)
					opma.saveFeeActSubId(onlinePermitForm.getStypeId(), onlinePermitForm.getSelectedFeeId());
				actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
				message = "Online Permit Mapping was saved successfully";
			}
			//actSubtypeList = opma.getActSubTypes();
			String type=StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType());
			if(type.equalsIgnoreCase("200200")){
				type= "RONPER";
			}else if(type.equalsIgnoreCase("200201")){
				type= "MONPER";
			}else if(type.equalsIgnoreCase("200202")){
				type= "CONPER";
			}
			//if(!type.equals("") || !type.equals("null")){
				actSubtypeList = opma.getActSubTypes(type);
			//}
			actFeeList = opma.getFeesList(onlinePermitForm.getStypeId() + "",type);

			request.setAttribute("message", message);
			request.setAttribute("actSubFeeMap", actSubFeeMap);
			request.setAttribute("actSubtypeList", actSubtypeList);
			request.setAttribute("actFeeList", actFeeList);
			onlinePermitForm.setActType(StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()));
			onlinePermitForm.setStypeId(onlinePermitForm.getStypeId());
			request.setAttribute("onlinePermitForm", onlinePermitForm);
			
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error in OnlinePermitAction " + e.getMessage());
		}
		logger.debug("Exiting OnlinePermitAction ...");
		return (mapping.findForward(nextPage));
	}
}
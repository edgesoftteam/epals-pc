package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.FinanceAgent;
import elms.app.finance.FinanceSummary;
import elms.control.beans.MoveVerifyForm;
import elms.util.StringUtils;

public class SaveMoveActivityAction extends Action {

	static Logger logger = Logger.getLogger(SaveMoveActivityAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering SaveMoveActivityAction");
		HttpSession session = request.getSession();
		try {
			ActionErrors errors = new ActionErrors();
			FinanceSummary financeSummary = new FinanceSummary();
			FinanceAgent financeAgent = new FinanceAgent();
			AdminAgent moveProjectAgent = new AdminAgent();

			MoveVerifyForm frm = (MoveVerifyForm) session.getAttribute("moveVerifyForm");
			String subProjectId = (String) request.getParameter("subProjectId");
			String activityId = (String) request.getParameter("activityId");
			String activityNbr = StringUtils.nullReplaceWithEmpty(frm.getActivityNbr());
			logger.debug("Got Parameters :" + activityId + ":" + subProjectId);

			financeSummary = financeAgent.getActivityFinance(StringUtils.s2i(activityId));
			logger.debug("Got financeSummary object " + financeSummary.toString());
			if (financeSummary == null) {
				financeSummary = new FinanceSummary();
			}

			String totalPaidAmt = financeSummary.getTotalPaidAmt();
			logger.debug(" value of paid is " + StringUtils.s2i(totalPaidAmt.substring(1)));
			logger.debug("subProjectId::==" + subProjectId + "activityId" + activityId);

			if (!subProjectId.equals("")) {
				String subProjectName = moveProjectAgent.getsubProjectNameBySPId(subProjectId);

				int updno = 0;

				/* check for activity of type BT and BL and move to respective subproject.That is BT activity to BT subproject */
				if (activityNbr.startsWith("BT") || activityNbr.startsWith("BL")) {
					if ((activityNbr.startsWith("BT") && subProjectName.startsWith("BT")) || (activityNbr.startsWith("BL") && subProjectName.startsWith("BL"))) {
						/* move activity */

						if (!subProjectId.equals("") && !activityId.equals(""))
							updno = moveProjectAgent.moveActivity(activityId, subProjectId);

						if (updno == 0)
							frm.setMessage("Activity cannot be moved.");
						else
							frm.setMessage("Move completed successfully");

					} else {
						logger.debug("The activity should be moved to respective BT/BL sub project.Hence try again. ");
						errors.add("statusCodeExists", new ActionError("statusCodeExists", "The activity should be moved to respective BT/BL sub project.Hence try  again."));
						saveErrors(request, errors);
					}
				} else {
					/* move activity */

					if (!subProjectId.equals("") && !activityId.equals(""))
						updno = moveProjectAgent.moveActivity(activityId, subProjectId);

					if (updno == 0)
						frm.setMessage("Activity cannot be moved.");
					else
						frm.setMessage("Move completed successfully");
				}
			} else {
				frm.setMessage("Activity cannot be moved.");
			}

			frm.setActivityId(activityId);
			frm.setSubProjectId(subProjectId);

			session.setAttribute("moveVerifyForm", frm);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.info("Exiting SaveMoveActivityAction");
		return (mapping.findForward(nextPage));

	}

}
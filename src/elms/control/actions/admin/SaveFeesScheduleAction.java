package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.app.admin.FeeEdit;
import elms.control.beans.FeeScheduleForm;
import elms.control.beans.LookupFeesForm;

public class SaveFeesScheduleAction extends Action {
	static Logger logger = Logger.getLogger(SaveFeesScheduleAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// Extract attributes and parameters we will need
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		boolean success = false;

		try {
			FeeScheduleForm subForm = (FeeScheduleForm) form;
			// FeeScheduleForm subForm = (FeeScheduleForm) session.getAttribute("feeScheduleForm");
			String activityType = (String) session.getAttribute("activityType");

			FeeEdit[] lst = subForm.getFeeEditList();

			logger.debug("Action is : " + subForm.getAction());
			for (int i = 0; i < lst.length; i++) {
				logger.debug("form values::" + lst[i].isRenewable() + "::" + lst[i].getRenewableFlag());
				if (lst[i].getRenewableFlag().equalsIgnoreCase("true")) {
					lst[i].setRenewable(true);
				} else if (lst[i].getRenewableFlag().equalsIgnoreCase("false")) {
					lst[i].setRenewable(false);
				} else {
					lst[i].setRenewable(false);
				}
				
			}

			if (!subForm.getAction().equalsIgnoreCase("Cancel")) {
				success = new FinanceAgent().saveFee(lst);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			// obtain the list of fees for a selected acivivity type
			if (activityType.equals(null) || activityType.equals("")) {
				activityType = lst[0].getActType();
				session.setAttribute("activityType", activityType);
			}

			List feeList = new FinanceAgent().getFees(activityType);
			logger.debug("Activity Type (" + activityType + ") has size " + feeList.size());

			FeeEdit[] aryFee = new FeeEdit[feeList.size()];

			for (int i = 0; i < feeList.size(); i++) {
				aryFee[i] = (FeeEdit) feeList.get(i);
			}

			ActionForm frm = new LookupFeesForm();
			FeeScheduleForm frmFee = new FeeScheduleForm();

			frmFee.setFeeEditList(aryFee);
			session.setAttribute("feeScheduleForm", frmFee);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}
}

// End class

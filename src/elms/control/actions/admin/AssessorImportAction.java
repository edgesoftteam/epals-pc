package elms.control.actions.admin;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.AssessorAgent;
import elms.util.db.Wrapper;

public class AssessorImportAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AssessorImportAction.class.getName());
	static ResourceBundle assessorProperties = null;
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Assessor import action");

		try {
			assessorProperties = Wrapper.getResourceBundle();
		} catch (Exception e) {
			logger.error("getObcResourceBundle" + e.getMessage());
		}

		String checkinFolderPath = null;

		try {
			checkinFolderPath = assessorProperties.getString("ASSESSOR_CHECKIN_FOLDER");

			File fp = new File(checkinFolderPath);
			boolean exists = fp.exists();

			if (!exists) {
				// It returns false if File or directory does not exist
				request.setAttribute("exists", "false");

				return (mapping.findForward("error"));
			} else {
				// It returns true if File or directory exists
			}
		} catch (Exception e) {
		}

		logger.debug("CONFIG: Reading check-in folder path as " + checkinFolderPath);

		HttpSession session = request.getSession(true);
		String action = request.getParameter("action");

		if (action == null) {
			action = "showStatus";
		}

		if (action.equals("showStatus")) {
			String latestStatus = new AssessorAgent().getLatestStatus();

			if (latestStatus == null) {
				logger.debug("latestStatus is null, showing start import");
				request.setAttribute("showStartImport", "yes");
				request.setAttribute("showClearLogs", "no");
				request.setAttribute("showStatusLogs", "no");
			} else if (latestStatus.equalsIgnoreCase("complete")) {
				logger.debug("latest status is complete, showing clear logs and hiding start import");
				request.setAttribute("showStartImport", "no");
				request.setAttribute("showClearLogs", "yes");
				request.setAttribute("showStatusLogs", "yes");
			} else {
				logger.debug("import is in progress, just show the status");
				request.setAttribute("showImportInProgress", "yes");
				request.setAttribute("showStartImport", "no");
				request.setAttribute("showClearLogs", "no");
				request.setAttribute("showStatusLogs", "yes");
			}

			if (request.getParameter("enableAutoRefresh") != null) {
				session.setAttribute("enableAutoRefresh", "yes");
			}

			CachedRowSet importStatus = (CachedRowSet) new AssessorAgent().getStatus();
			logger.debug("obtained log of assessor import and set it to the request, size :" + importStatus.size());
			request.setAttribute("importStatus", importStatus);

			return (mapping.findForward("success"));
		} else if (action.equals("startImport")) {
			String fileName = request.getParameter("fileName");

			if (fileName == null) {
				fileName = "";
			}

			logger.debug("filename is " + fileName);

			try {
				new AssessorAgent().insertAssessorImportLog("START", "START", "Starting Assessor Import...");
				startAssessorImportAsNewThread(fileName);
			} catch (Exception e) {
				logger.error("error occured with startAssessorImportAsNewThread");
			}

			return (mapping.findForward("showStatus"));
		} else if (action.equals("delete")) {
			new AssessorAgent().deleteLogs(); // delete all records
			request.setAttribute("message", "Assessor Import Log records Cleared Successfully");

			return (mapping.findForward("showStatus"));
		} else {
			throw new ServletException("No Mapping defined");
		}
	}

	/**
	 * 
	 * @param fileName
	 * @throws Exception
	 */
	public void startAssessorImportAsNewThread(String fileName) throws Exception {
		logger.debug("startAssessorImportAsNewThread(" + fileName + ")");

		final AssessorAgent importAssessorAgent = new AssessorAgent(fileName);

		// start process in new thread
		new Thread(new Runnable() {
			public void run() {
				try {
					importAssessorAgent.execute();
				} catch (Exception e) {
					logger.error("run: Exception thrown while attempting to run import process", e);
				}
			}
		}).start();
	}
}

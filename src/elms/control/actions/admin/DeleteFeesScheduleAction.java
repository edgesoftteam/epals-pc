package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.app.admin.FeeEdit;
import elms.control.beans.FeeScheduleForm;
import elms.control.beans.LookupFeesForm;

public class DeleteFeesScheduleAction extends Action {

	static Logger logger = Logger.getLogger(MaintainFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			List feeList = new ArrayList();
			List deleteList = new ArrayList();

			FeeScheduleForm subForm = (FeeScheduleForm) session.getAttribute("feeScheduleForm");
			FeeEdit[] lst = subForm.getFeeEditList();

			for (int i = 0; i < lst.length; i++) {
				logger.debug(i + "-" + lst[i].getCheck() + "-");
				if (lst[i].getCheck().equals("on"))
					deleteList.add(lst[i]);
				else
					feeList.add(lst[i]);
			}

			FeeEdit[] aryFee = new FeeEdit[feeList.size()];
			for (int i = 0; i < feeList.size(); i++) {
				aryFee[i] = (FeeEdit) feeList.get(i);
			}

			boolean success = new FinanceAgent().deleteFees(deleteList);
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LookupFeesForm();
			FeeScheduleForm frmFee = new FeeScheduleForm();
			if (success)
				frmFee.setFeeEditList(aryFee);
			else {
				frmFee.setFeeEditList(lst);
				request.setAttribute("error", "Cannot delete fees that are in use");
			}
			session.setAttribute("feeScheduleForm", frmFee);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}
} // End class

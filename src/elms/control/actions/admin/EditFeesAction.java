package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.agent.LookupAgent;
import elms.app.finance.Fee;
import elms.control.beans.FeesForm;
import elms.util.StringUtils;

public class EditFeesAction extends Action {

	static Logger logger = Logger.getLogger(EditFeesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {

			String strFeeId = request.getParameter("feeId");
			logger.debug("obtained fee id as string from request as " + strFeeId);
			int feeId = StringUtils.s2i(strFeeId);
			logger.debug("fee id as int value " + feeId);
			Fee fee = new FinanceAgent().getFee(feeId);
			logger.debug("Obtained the fee object for fee id of " + feeId);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			// get the static list of lookup fees from the lookup agent
			List activityTypes = LookupAgent.getActivityTypes();
			logger.debug("obtained list of size " + activityTypes.size());
			request.setAttribute("activityTypes", activityTypes);

			ActionForm frm = new FeesForm();
			request.setAttribute("feesForm", frm);
			FeesForm feesForm = (FeesForm) frm;
			feesForm.setFee(fee);
			logger.debug("set the fee object to the fee form");

		} catch (Exception e) {

		}

		return (mapping.findForward("success"));
	}

}
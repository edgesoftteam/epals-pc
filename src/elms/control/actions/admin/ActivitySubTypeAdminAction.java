/**@author Gayathri
 * Activity Sub Type data for Admin Module
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivitySubType;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.ActivitySubTypeForm;
import elms.util.StringUtils;

public class ActivitySubTypeAdminAction extends Action {
	String contextRoot = "";
	static Logger logger = Logger.getLogger(ActivitySubTypeAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into ActivityAdminAction .. ");
		HttpSession session = request.getSession();

		AdminAgent adminAgent = new AdminAgent();
		ActivitySubTypeForm activitySubTypeForm = (ActivitySubTypeForm) form;
		List activitySubTypes = new ArrayList();

		if (request.getParameter("lookup") != null) {
			result = 0;
			activitySubTypeForm.setActivityType(StringUtils.nullReplaceWithEmpty(request.getParameter("activityTypeStr")));
			activitySubTypeForm.setActivitySubType(StringUtils.nullReplaceWithEmpty(request.getParameter("activitySubType")));

			activitySubTypes = adminAgent.getActivitySubTypes(activitySubTypeForm);
			session.removeAttribute("activitySubTypeForm");
			session.setAttribute("oldActSubTypeForm", activitySubTypeForm);

			activitySubTypeForm.setActivitySubTypes(activitySubTypes);
			activitySubTypeForm.setDisplayActSubTypes("YES");

			PrintWriter pw = response.getWriter();
			if (activitySubTypes.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(activitySubTypes));
			}
			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			ActivitySubType activitySubType = adminAgent.getActivitySubType(StringUtils.s2i(request.getParameter("id")));

			activitySubTypeForm.setActivitySubTypeId(activitySubType.getId());
			activitySubTypeForm.setActivityType(activitySubType.getActType());
			activitySubTypeForm.setActivitySubType(activitySubType.getDescription());
			activitySubTypeForm.setDisplayActSubTypes("YES");

			PrintWriter pw = response.getWriter();
			pw.write(activitySubTypeForm.getActivityType().toString() + ',' + activitySubTypeForm.getActivitySubType().toString());
			return null;

		} else if (request.getParameter("save") != null) {

			ActivitySubTypeForm actSubTypeForm = (ActivitySubTypeForm) session.getAttribute("oldActSubTypeForm");

			activitySubTypeForm.setActivityType(StringUtils.nullReplaceWithEmpty(request.getParameter("activityTypeStr")));
			activitySubTypeForm.setActivitySubType(StringUtils.nullReplaceWithEmpty(request.getParameter("activitySubType")));
			activitySubTypeForm.setActivitySubTypeId(StringUtils.s2i(request.getParameter("subTypeId")));

			result = adminAgent.saveActivitySubType(activitySubTypeForm.getActivityType(), activitySubTypeForm.getActivitySubType(), StringUtils.i2s(activitySubTypeForm.getActivitySubTypeId()));

			activitySubTypeForm.setDisplayActSubTypes("NO");
			PrintWriter pw = response.getWriter();

			if (result == 1) {
				pw.write("Activity Sub Type Saved Successfully");
			} else if (result == 2) {
				activitySubTypes = adminAgent.getActivitySubTypes(actSubTypeForm);

				pw.write(innerTable(activitySubTypes));
			} else {
				pw.write("");
			}
			return null;

		} else if (request.getParameter("delete") != null) {

			activitySubTypeForm.setActivityType(StringUtils.nullReplaceWithEmpty(request.getParameter("activityTypeStr")));
			activitySubTypeForm.setActivitySubType(StringUtils.nullReplaceWithEmpty(request.getParameter("activitySubType")));

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deleteActivitySubType(idList);

			ActivitySubTypeForm actSubTypeDelForm = (ActivitySubTypeForm) session.getAttribute("oldActSubTypeForm");
			// session.removeAttribute("oldActTypeForm");

			activitySubTypes = adminAgent.getActivitySubTypes(actSubTypeDelForm);
			activitySubTypeForm.setDisplayActSubTypes("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(activitySubTypes));
			return null;
		} else {
			activitySubTypeForm = new ActivitySubTypeForm();
			List activityTypes = new ArrayList();
			activitySubTypeForm.setActivitySubTypes(activityTypes);

			activitySubTypeForm.setDisplayActSubTypes("NO");
			session.setAttribute("activitySubTypeForm", activitySubTypeForm);
		}
		List activityTypes;
		try {
			activityTypes = LookupAgent.getActivityTypes();
		} catch (Exception e2) {
			activityTypes = new ArrayList();
			logger.warn("activityTypes is not available, initializing it to blank arraylist");
		}
		request.setAttribute("activityTypes", activityTypes);
		logger.debug("The Activity Type Maping Activity Types set to request with size " + activityTypes.size());

		return (mapping.findForward("success"));
	}

	public String innerTable(List actSubTypes) {
		String actSubTypesHtml = "";
		if (actSubTypes != null && actSubTypes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem activitySubTypes = null;

			if (actSubTypes != null && actSubTypes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">Activity Sub Type List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Activity Sub Type</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Activity Type</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Activity Sub Type</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < actSubTypes.size(); i++) {
					activitySubTypes = (DisplayItem) actSubTypes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (activitySubTypes.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + activitySubTypes.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + activitySubTypes.getFieldOne() + "," + StringUtils.checkString(activitySubTypes.getFieldSix()) + ")\" >");
					sb.append(activitySubTypes.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(activitySubTypes.getFieldThree());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actSubTypesHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return actSubTypesHtml;
	}
}

package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.admin.AssessorExportForm;

public class AssessorExportAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AssessorExportAction.class.getName());

	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		String type = (String) request.getParameter("type");

		try {
			AssessorExportForm hef = (AssessorExportForm) form;
			String date = hef.getDate();

			String month = "";
			String year = "";

			if (date != null) {
				month = date.substring(0, 2);
				year = date.substring(3);
				logger.debug(month + " -" + year);
			} else {
				request.setAttribute("message", "Please enter correct month and year");
			}

			String getRealPath = this.getServlet().getServletContext().getRealPath("/");
			logger.debug("Real path is " + getRealPath);
			String pdfPath = getRealPath + "pdf";

			String loc = AdminAgent.assessorReport(month, year, pdfPath);
			request.setAttribute("path", loc);
			request.setAttribute("message", "Your request had been sucessfully Completed. Please click on view file");
		} catch (Exception e) {
			logger.debug("Exception occured in Assesor  Export Function" + e.getMessage());
			request.setAttribute("message", "Some Error has occured.Please check Logs");
		}

		return (mapping.findForward("success"));
	}

}
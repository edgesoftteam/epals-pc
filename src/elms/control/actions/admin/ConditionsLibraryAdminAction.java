package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.control.beans.admin.ConditionsLibraryForm;
import elms.security.User;
import elms.util.StringUtils;

public class ConditionsLibraryAdminAction extends Action {

	static Logger logger = Logger.getLogger(ConditionsLibraryAdminAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into UserAdminAction .. ");

		// getting user id from session
		HttpSession session = request.getSession();
		User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		int userId = user.getUserId();
		logger.debug("user id is " + userId);

		// Action errors object
		ActionErrors errors = new ActionErrors();

		AdminAgent adminAgent = new AdminAgent();
		ConditionsLibraryForm conditionsLibraryForm = (ConditionsLibraryForm) form;
		if (request.getParameter("lookup") != null) {

			conditionsLibraryForm.setDisplayConditions("YES");
			List recs = adminAgent.getLibraryConditions(conditionsLibraryForm);
			logger.debug("obtained records before sorting");

			conditionsLibraryForm.setConditions(recs);
			logger.debug("set records to the form after sorting");
		} else if (request.getParameter("ctype") != null) {

			conditionsLibraryForm.setDisplayConditions("NO");
		} else if (request.getParameter("id") != null) {

			conditionsLibraryForm = adminAgent.getLibraryCondition(StringUtils.s2i(request.getParameter("id")));
			conditionsLibraryForm.setDisplayConditions("NO");
		} else if (request.getParameter("save") != null) {

			String conditionId = conditionsLibraryForm.getConditionId();
			String conditionCode = conditionsLibraryForm.getConditionCode();

			if ((conditionId == null) || conditionId.equals("")) {

				if (new CommonAgent().conditionCodeExists(conditionCode)) {

					errors.add("codeExists", new ActionError("conditionCodeExists", conditionsLibraryForm.getConditionCode() + " Condition Code already exists"));
				} else {

					try {
						conditionsLibraryForm = adminAgent.saveLibraryCondition(conditionsLibraryForm, userId);
						errors.add("success", new ActionError("success", "Condition Library record added successfully"));
					} catch (Exception e2) {
						throw new ServletException("Unable to save library condition " + e2.getMessage());
					}
				}
			} else {

				try {
					conditionsLibraryForm = adminAgent.saveLibraryCondition(conditionsLibraryForm, userId);
					errors.add("success", new ActionError("success", "Condition Library record updated successfully"));
				} catch (Exception e2) {
					throw new ServletException("Unable to save library condition " + e2.getMessage());
				}
			}

			conditionsLibraryForm.setDisplayConditions("NO");
			saveErrors(request, errors);
		} else if (request.getParameter("delete") != null) {

			// geting items to delete
			String[] mapIds = conditionsLibraryForm.getSelectedItems();
			for (int i = 0; i < mapIds.length; i++) {

				adminAgent.deleteLibraryCondition(mapIds[i]);
			}

			conditionsLibraryForm = new ConditionsLibraryForm();
			conditionsLibraryForm.setConditions(adminAgent.getLibraryConditions(conditionsLibraryForm));
			conditionsLibraryForm.setDisplayConditions("YES");
		} else {

			conditionsLibraryForm = new ConditionsLibraryForm();
			conditionsLibraryForm.setDisplayConditions("NO");
		}

		session.setAttribute("conditionsLibraryForm", conditionsLibraryForm);

		// setting lso use based on L , S , O
		List lsoUses = null;
		if ((conditionsLibraryForm.getLevelType() == null) || conditionsLibraryForm.getLevelType().equals("")) {

			conditionsLibraryForm.setLevelType("");
			lsoUses = new ArrayList();
		} else {

			try {

				lsoUses = LookupAgent.getLsoUses(conditionsLibraryForm.getLevelType());
			} catch (Exception e) {

				lsoUses = new ArrayList();
				logger.warn("could not get list of lsoUses, initializing it to a blank arrayList");
			}
		}

		request.setAttribute("lsoUses", lsoUses);
		logger.debug("The Conditions Library Admin set lsouse with size .. " + lsoUses.size());

		List projectTypes;
		try {

			projectTypes = LookupAgent.getProjectNames();
		} catch (Exception e) {

			projectTypes = new ArrayList();
			logger.warn("could not get list of projectTypes, initializing it to a blank arrayList");
		}

		request.setAttribute("projectTypes", projectTypes);
		logger.debug("The Conditions Library Admin set Project Types with size " + projectTypes.size());

		List subProjectTypes;
		try {

			subProjectTypes = LookupAgent.getSubProjectTypes();
		} catch (Exception e) {

			subProjectTypes = new ArrayList();
			logger.warn("could not get list of subProjectTypes, initializing it to a blank arrayList");
		}

		request.setAttribute("subProjectTypes", subProjectTypes);
		logger.debug("The Conditions Library Admin set Sub Project Types with size " + subProjectTypes.size());

		List activityTypes;
		try {

			activityTypes = LookupAgent.getActivityTypes();
		} catch (Exception e1) {

			activityTypes = new ArrayList();
			logger.warn("could not get list of activityTypes, initializing it to a blank arrayList");
		}

		request.setAttribute("activityTypes", activityTypes);
		logger.debug("The Conditions Library Admin set activity types with size " + activityTypes.size());

		return (mapping.findForward("success"));
	}
}
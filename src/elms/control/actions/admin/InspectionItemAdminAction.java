/**
 * 
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.InspectionItemForm;
import elms.util.StringUtils;
import elms.util.Util;

/**
 * @author Arjun
 *
 */
public class InspectionItemAdminAction extends Action{
	static Logger logger = Logger.getLogger(InspectionItemAdminAction.class.getName());
	int result = -1;
	public ActionForward perform(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
		
		logger.debug("in InspectionItemAdminAction");
		String nextpage = "success";
		AdminAgent adminAgent  = new AdminAgent(); 
		HttpSession session = request.getSession();
		InspectionItemForm inspectionItemForm = (InspectionItemForm) form;
		 ActionErrors errors = new ActionErrors();
		if(request.getParameter("firstTime") != null && request.getParameter("firstTime").equalsIgnoreCase("yes")){
			inspectionItemForm = new InspectionItemForm();
		}
		List inspItems = new ArrayList();
		inspectionItemForm.setDisplayStatus("NO");
		
		if (request.getParameter("lookup") != null) {
			result = 0;
			// getting and setting all the values to the form
			inspectionItemForm.setId(StringUtils.s2i(request.getParameter("id")));
			inspectionItemForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			inspectionItemForm.setActType(request.getParameter("actType"));
			inspectionItemForm.setCode(StringUtils.s2i(request.getParameter("code")));


			inspItems = adminAgent.getInspItems(inspectionItemForm);

			session.removeAttribute("inspectionItemForm");
			session.setAttribute("oldInspectionItemForm", inspectionItemForm);

			inspectionItemForm.setDisplayStatus("YES");

			PrintWriter pw = response.getWriter();
			if (inspItems.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(inspItems));
			}
			return null;
		}else if(request.getParameter("inspItemId")!=null){// condition to populate values to jsp page
			result = 0;
			inspectionItemForm = adminAgent.getInspectionItem(StringUtils.s2i(request.getParameter("inspItemId")), inspectionItemForm);
			inspectionItemForm.setDisplayStatus("NO");

			PrintWriter pw = response.getWriter();
			pw.write(inspectionItemForm.getDescription().toString() + ',' + inspectionItemForm.getCode() + ',' + inspectionItemForm.getActType().toString() + ',' + inspectionItemForm.getId());
			return null;		
		}else if (request.getParameter("save") != null) { // condition to save

			InspectionItemForm inspItemForm = (InspectionItemForm) session.getAttribute("oldInspectionItemForm");

			inspectionItemForm.setId(StringUtils.s2i(request.getParameter("itemId")));
			inspectionItemForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			inspectionItemForm.setActType(request.getParameter("actType"));
			inspectionItemForm.setCode(StringUtils.s2i(request.getParameter("code")));
			
			// save function called from Adminagent
			result = adminAgent.saveInspItem(inspectionItemForm.getId(), inspectionItemForm.getDescription(), inspectionItemForm.getActType(), inspectionItemForm.getCode());
			inspectionItemForm.setDisplayStatus("NO");

			if (result == 1) {
				PrintWriter pw = response.getWriter();
				pw.write("Inspection Item Saved Successfully");
			} else if (result == 2) {
				inspItems = adminAgent.getInspItems(inspectionItemForm);

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(inspItems));
			} else {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Inspection Item is not added"));
				saveErrors(request, errors);
			}
			return null;
		}else if (request.getParameter("delete") != null) {// condition to delete

			// getting and setting all the values to the form

			String idList = (String) request.getParameter("checkboxArray");

			inspectionItemForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("description")));
			inspectionItemForm.setActType(request.getParameter("actType"));
			inspectionItemForm.setCode(StringUtils.s2i(request.getParameter("code")));

			result = adminAgent.deleteInspItems(idList);

			InspectionItemForm oldInspItemForm = (InspectionItemForm) session.getAttribute("oldInspectionItemForm");

			inspectionItemForm.setDisplayStatus("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(adminAgent.getInspItems(oldInspItemForm)));
			return null;
		} else{
		
		try{
		setDropDownValues(request);
		}catch (Exception e) {
			e.printStackTrace();
		}
		inspectionItemForm.setDisplayStatus("NO");
		session.setAttribute("inspectionItemForm", inspectionItemForm);
		}
		return (mapping.findForward(nextpage));
	}
	
	private void setDropDownValues(HttpServletRequest request) throws Exception{
		List activityTypeList = LookupAgent.getActivityTypes();
		request.setAttribute("activityTypeList", activityTypeList);
	}
	public String innerTable(List inspCodes) {
		String actStatusHtml = "";
		if (inspCodes != null && inspCodes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem inspCode = null;

			if (inspCodes != null && inspCodes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"100%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><font class=\"con_hdr_3b\">Inspection Item List<br><br></font></td></tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"98%\" height=\"25\" bgcolor=\"#B7C1CB\"><font class=\"con_hdr_2b\">Inspection Item</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Insp Item\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\">");
				sb.append("<tr bgcolor=\"#FFFFFF\">");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Inspection Item</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Item Code</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\"><nobr>Activity Type</nobr></font></td>");
				/*sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Active</font></td>");*/
				sb.append("</tr>");

				for (int i = 0; i < inspCodes.size(); i++) {
					inspCode = (DisplayItem) inspCodes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (inspCode.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + inspCode.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + inspCode.getFieldOne() + "," + StringUtils.checkString(inspCode.getFieldSix()) + ")\" >");
					sb.append(inspCode.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(inspCode.getFieldFour() );
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(StringUtils.nullReplaceWithEmpty(inspCode.getFieldThree()));
					sb.append("</font></td>");
					/*sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(inspCode.getFieldFive());
					sb.append("</font></td>");*/
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actStatusHtml = sb.toString();
			// logger.debug(actStatusHtml);
		}
		return actStatusHtml;
	}

}
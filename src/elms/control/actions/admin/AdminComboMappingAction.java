package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.app.lso.LsoUse;
import elms.control.beans.admin.AdminComboMappingForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class AdminComboMappingAction extends Action {

	static Logger logger = Logger.getLogger(AdminComboMappingAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into AdminComboMappingAction ..  ");
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		AdminComboMappingForm adminComboMappingForm = (AdminComboMappingForm) form;
		ActivityAgent ActivityAgent = new ActivityAgent();
		String newMap = "";
		List useIdMaps = new ArrayList();
		LsoUse lsoUseMap = null;
		newMap = StringUtils.nullReplaceWithEmpty(request.getParameter("newMap"));

		// to reset the session variable plan check when new record is to be added -added by Manjuprasad
		if (newMap.equalsIgnoreCase("new")) {
			session.setAttribute("pcReq", "");
		}
		// end - added by Manjuprasad
		try {

			String mapId = request.getParameter("mapId");
			String subPrjName = request.getParameter("subProjName");

			String selectedActType = "";
			String selectedActTypeCode = "";
			String deptCode = "";

			/**
			 * Block to add plan check to individual activity -added by Manjuprasad
			 */
			String pCheck = "";
			String pcReq = "";

			pCheck = StringUtils.nullReplaceWithEmpty(request.getParameter("pCheck"));

			// comes to this if block wen we clicked on add Plan check button - added by Manjuprasad
			if (pCheck.equalsIgnoreCase("Yes")) {

				String actname = request.getParameter("strDesc");
				String actcode = request.getParameter("strCode");

				pcReq = StringUtils.nullReplaceWithEmpty((String) session.getAttribute("pcReq"));
				logger.debug("getting pcReq fron session " + pcReq);

				if (pcReq.equals("")) {
					pcReq = StringUtils.nullReplaceWithEmpty(request.getParameter("pcReq"));
					logger.debug("Set of PcReq when window opens " + adminComboMappingForm.getPcReq());
				}
				request.setAttribute("actname", actname);

				if (!(pcReq.equalsIgnoreCase("")) || (pcReq != null)) {
					session.setAttribute("pcReq", pcReq);
				}
				adminComboMappingForm.setPcReq(pcReq);
				logger.debug("hai");
				return (mapping.findForward("planCheck"));

			}

			// comes to this part when adding plan check for particular activities is finished - added by Manjuprasad
			if (pCheck.equalsIgnoreCase("No")) {

				pcReq = StringUtils.nullReplaceWithEmpty(request.getParameter("pcReq"));
				logger.debug("pcReq pcReq pcReq" + pcReq);
				adminComboMappingForm.setPcReq(pcReq);
				session.setAttribute("pcReq", pcReq);

			}

			if (request.getParameter("deptCode") != null)
				deptCode = LookupAgent.getDepartmentCode(request.getParameter("deptCode"));

			if (request.getParameter("lookup") != null) {

				List comboList = ActivityAgent.getComboMappingList(adminComboMappingForm);
				adminComboMappingForm = new AdminComboMappingForm();
				adminComboMappingForm.setComboList(comboList);
				adminComboMappingForm.setDisplayComboList("YES");

			} else if (request.getParameter("save") != null) {
				Wrapper db = new Wrapper();
				/*
				 * Getting the list of activities to which plan check is added
				 */
				pcReq = StringUtils.nullReplaceWithEmpty((String) session.getAttribute("pcReq"));
				logger.debug("The pc Req Value is " + pcReq);
				if (!(pcReq.equalsIgnoreCase(""))) {

					int lastIndx = pcReq.lastIndexOf(",");
					pcReq = pcReq.substring(0, lastIndx);
				}
				try {
					db.beginTransaction();
					if (Integer.parseInt(mapId) != 0) {// deletes all the row on Edit Later inserts.
						ActivityAgent.deleteCombo(adminComboMappingForm);
						logger.debug("deleted the values ");
					}
					ActivityAgent.saveCombo(adminComboMappingForm, pcReq);
					adminComboMappingForm = new AdminComboMappingForm();
					adminComboMappingForm.setDisplayComboList("NO");
					String message = "Combo Details saved successfully.";
					request.setAttribute("message", message);
					db.commitTransaction();
				} catch (Exception e) {
					db.rollbackTransaction();
				}

			} else if (subPrjName != "" && subPrjName != null && deptCode != "" && deptCode != null) { // Edit mode on click of Hyperlink

				adminComboMappingForm = new AdminComboMappingForm();
				adminComboMappingForm = ActivityAgent.getComboForm(Integer.parseInt(mapId), subPrjName, deptCode);
				selectedActType = ActivityAgent.getSelectedActivityType(Integer.parseInt(mapId), subPrjName, deptCode);
				adminComboMappingForm.setLsoUse(ActivityAgent.setlsoUse(subPrjName, deptCode));

				// This part to display and set the list of LSO Land use
				try {
					int s = adminComboMappingForm.getLsoUse().getLsoUseId().size();
					logger.debug("the value of s is " + s);
					String useIdArray = "";

					if (!(s == 0)) {

						for (int j = 0; j < s; j++) {
							logger.debug("Getting lsoUSeID and setting in useId array");
							useIdArray = useIdArray + StringUtils.nullReplaceWithEmpty((String) adminComboMappingForm.getLsoUse().getLsoUseId().get(j)) + ",";
							useIdMaps.add(adminComboMappingForm.getLsoUse().getLsoUseId().get(j));
						}
					}
					adminComboMappingForm.setUseIdArray(StringUtils.nullReplaceWithEmpty(useIdArray));
					request.setAttribute("useIdMaps", useIdMaps);

					selectedActTypeCode = ActivityAgent.getSelectedActivityCode(Integer.parseInt(mapId), subPrjName, deptCode);

					session.setAttribute("pcReq", adminComboMappingForm.getPcReq());
					logger.debug("Look up part  " + adminComboMappingForm.getPcReq());
					adminComboMappingForm.setDisplayComboList("NO");
				} catch (Exception e) {
					e.printStackTrace();
				}

			} else if (request.getParameter("active") != null) { // Logical Deleting of Combo Mapping

				String[] mapIds = adminComboMappingForm.getSelectedId();
				AdminComboMappingForm deladminComboMappingForm = new AdminComboMappingForm();
				boolean isMapped = false;
				if (mapIds != null) {
					for (int i = 0; i < mapIds.length; i++) {
						deladminComboMappingForm = ActivityAgent.getComboValues(mapIds[i]);
						ActivityAgent.updateActiveDeActiveCombo(deladminComboMappingForm);
					}
					adminComboMappingForm.setSelectedId(null);
				} else {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.combo.selectdelete"));
					saveErrors(request, errors);
				}
				List comboList = ActivityAgent.getComboMappingList(adminComboMappingForm);
				adminComboMappingForm = new AdminComboMappingForm();
				adminComboMappingForm.setComboList(comboList);
				adminComboMappingForm.setDisplayComboList("YES");

			} else {

				adminComboMappingForm = new AdminComboMappingForm();
				adminComboMappingForm.setDisplayComboList("NO");

			}
			session.setAttribute("comboMappingForm", adminComboMappingForm);
			session.setAttribute("AdminComboMappingForm", adminComboMappingForm);
			List atList = LookupAgent.getActivityTypes();
			request.setAttribute("atList", atList);
			request.setAttribute("selectedActType", selectedActType);
			request.setAttribute("selectedActTypeCode", selectedActTypeCode);
			List departments = LookupAgent.getDepartmentList();
			request.setAttribute("departments", departments);
			lsoUseMap = ActivityAgent.getlsoUse();
			request.setAttribute("lsoUseMap", lsoUseMap);
			request.setAttribute("useIdMaps", useIdMaps);
			AddressAgent useAgent = new AddressAgent();
			request.setAttribute("useAgent", useAgent);

			List subProjectTypes = LookupAgent.getSubProjectTypes();
			request.setAttribute("subProjectTypes", subProjectTypes);
			List subProjectSubTypes = LookupAgent.getSubProjectSubTypes();
			request.setAttribute("subProjectSubTypes", subProjectSubTypes);
			logger.debug("The User Admin departments set with size .. " + departments.size());

			// setting up the session variable plan check when finished with selecting of add plan check for indivudual activity - added by Manjuprasad
			adminComboMappingForm.setPcReq((String) session.getAttribute("pcReq"));
			logger.debug("in action file  " + (String) session.getAttribute("pcReq"));
			session.setAttribute("pcReq", (String) session.getAttribute("pcReq"));

			request.setAttribute("adminComboMappingForm", adminComboMappingForm);

			return (mapping.findForward("success"));
		} catch (Exception e) {

			logger.error("Exception thrown " + e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}

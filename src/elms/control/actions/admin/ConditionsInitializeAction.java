package elms.control.actions.admin;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.CommonAgent;
import elms.control.beans.ConditionsInitializeForm;
import elms.util.StringUtils;

public class ConditionsInitializeAction extends Action {
	static Logger logger = Logger.getLogger(ConditionsInitializeAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		boolean success = false;
		Calendar today = Calendar.getInstance();
		String moduleIdString = null;
		String[] moduleId = null;
		try {

			String conditionDate = request.getParameter("conditionDate");
			moduleIdString = request.getParameter("moduleIdString");

			if (conditionDate != null) {

				logger.debug("moduleIdString" + moduleIdString);
				moduleId = moduleIdString.split(",");
				for (int i = 0; i < moduleId.length; i++) {
					logger.debug("moduleIdString(Split)" + moduleId.length);

					// function called to initialize conditions
					success = new CommonAgent().initializeConditions((StringUtils.str2cal(conditionDate)), StringUtils.s2i(moduleId[i]));

				}
			} else {
				conditionDate = StringUtils.cal2str(today);
			}

			if (mapping.getAttribute() != null) {

				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			ConditionsInitializeForm frmInit = new ConditionsInitializeForm();
			frmInit.setconditionDate(conditionDate);

			if (success) {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Conditions Initialized successfully"));
				saveErrors(request, errors);
			}
			request.setAttribute("conditionsInitializeForm", frmInit);
			return (mapping.findForward("success"));
		} catch (Exception e) {
			throw new ServletException("condition did not initialize...error is " + e.getMessage());
		}
	}

}

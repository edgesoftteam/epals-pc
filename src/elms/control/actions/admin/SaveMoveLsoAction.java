package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.MoveVerifyForm;

public class SaveMoveLsoAction extends Action {

	static Logger logger = Logger.getLogger(SaveMoveLsoAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			logger.debug("entered the save move lso action");

			MoveVerifyForm frm = (MoveVerifyForm) form;
			logger.debug("The form is " + frm);

			String toLsoId = frm.getToId();
			String moveLsoId = frm.getMoveId();
			String moveType = frm.getMoveType();

			logger.debug("Move :" + moveType + ": " + moveLsoId + " to " + toLsoId);

			AdminAgent moveLsoAgent = new AdminAgent();
			boolean updno = moveLsoAgent.moveLso(moveType, moveLsoId, toLsoId);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));

	}

}
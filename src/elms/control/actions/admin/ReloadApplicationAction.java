/*
 * Created on Oct 27, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionServlet;

import elms.agent.AddressAgent;

public class ReloadApplicationAction extends Action {

	static Logger logger = Logger.getLogger(ReloadApplicationAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		try {
			ActionServlet servlet = getServlet();
			// setting the street list to application scope
			List streets = AddressAgent.getStreetArrayList();
			servlet.getServletContext().setAttribute("streets", streets);
			logger.debug("Loaded Streets List(" + streets.size() + ")");

		} catch (Exception e) {
			logger.error("Error while loading application scope lists");
			logger.error(e.getLocalizedMessage());
		}

		return (mapping.findForward("success"));
	}
}

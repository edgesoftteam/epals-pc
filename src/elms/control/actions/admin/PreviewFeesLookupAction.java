package elms.control.actions.admin;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.control.beans.LookupFeeEdit;
import elms.control.beans.LookupFeesForm;
import elms.util.StringUtils;

public class PreviewFeesLookupAction extends Action {

	static Logger logger = Logger.getLogger(PreviewFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			String lookupId = (String) session.getAttribute("lookupId");
			List lookupFeeList = new ArrayList();
			BigDecimal bdChange = new BigDecimal("0.00");

			LookupFeesForm subForm = (LookupFeesForm) session.getAttribute("lookupFeesForm");
			LookupFeeEdit[] lst = subForm.getLkupFeeEditList();

			double percentChange = (100.0d + StringUtils.s2d(subForm.getPercent())) / 100.0d;
			bdChange = new BigDecimal(percentChange);

			logger.info("percentChange is 100.00 + " + subForm.getPercent() + " = " + percentChange + "(" + bdChange + ")");

			BigDecimal bdResult = new BigDecimal("0.00");
			BigDecimal bdPlus = new BigDecimal("0.00");

			for (int i = 0; i < lst.length; i++) {
				logger.debug(i + "-" + lst[i].getCheck() + "-");
				if (lst[i].getUp().equals("on")) {
					bdResult = StringUtils.s2bd(lst[i].getResult()).multiply(bdChange).setScale(1, BigDecimal.ROUND_HALF_UP);
					bdPlus = StringUtils.s2bd(lst[i].getPlus()).multiply(bdChange).setScale(1, BigDecimal.ROUND_HALF_UP);
					lst[i].setResult(bdResult.toString());
					lst[i].setPlus(bdPlus.toString());
					lst[i].setUp("");
				}
				lookupFeeList.add((LookupFeeEdit) lst[i]);
			}

			LookupFeeEdit[] aryLookupFee = new LookupFeeEdit[lookupFeeList.size()];
			for (int i = 0; i < lookupFeeList.size(); i++) {
				aryLookupFee[i] = (LookupFeeEdit) lookupFeeList.get(i);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LookupFeesForm();
			LookupFeesForm frmFee = (LookupFeesForm) frm;

			try {
				frmFee.setLkupFeeEditList(aryLookupFee);
			} catch (Exception ex) {
				logger.info(ex.getMessage());
			}

			session.setAttribute("lookupFeesForm", frmFee);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

} // End class
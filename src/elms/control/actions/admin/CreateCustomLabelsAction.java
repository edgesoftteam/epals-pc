package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivitySubType;
import elms.app.admin.ActivityType;
import elms.app.admin.PeopleType;
import elms.app.common.DropdownValue;
import elms.control.beans.admin.CustomLabelsForm;
import elms.exception.AgentException;

/**
 * 
 * @author sunil
 * 
 */

public class CreateCustomLabelsAction extends Action {

	static Logger logger = Logger.getLogger(CreateCustomLabelsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into CreateCustomLabelsAction ..  ");
		HttpSession session = request.getSession();
		CustomLabelsForm customLabelsForm = (CustomLabelsForm) form;
		ActivityAgent customLabelsAgent = new ActivityAgent();
		List comboList = new ArrayList();
		String display = "No";
		try {
			List atList = new ArrayList();

			
			if(request.getParameter("action") != null){
				if(request.getParameter("action").equals("lookup")){// lookup mode
					logger.debug("lookup");

					logger.debug("Level Type::"+customLabelsForm.getLevelType());
					logger.debug("levelTypeId" + customLabelsForm.getLevelTypeId());
					 
					comboList = customLabelsAgent.getCustomLabelsMappingList(customLabelsForm);
					logger.debug("comboList into CreateAppsAction ..  "	+ comboList.size());
					customLabelsForm.setComboList(comboList);
					atList = ActivityAgent.getActivityTypes();
					display = "YES";
					request.setAttribute("generalTypeList", getLevelTypeDropDownList(customLabelsForm)); 
					
				}else if(request.getParameter("action").equals("getLevelSubType")){
					logger.debug("getLevelSubType");
		            PrintWriter pw = response.getWriter();
		            pw.write(getlevelSubType(Integer.parseInt(request.getParameter("levelTypeId"))));
		            return null;
				}
			}  else if (request.getParameter("save") != null) {
				customLabelsAgent.saveCustomLabels(customLabelsForm);

				// Prompt Msg
				if (customLabelsForm.getMessage() != null && customLabelsForm.getMessage().startsWith("COMP")) {
					request.setAttribute("message", "CustomLabels Details saved successfully.");
				} else if (customLabelsForm.getMessage() != null && customLabelsForm.getMessage().startsWith("INCOMP")) {
					request.setAttribute("error", "Duplicate Field Label Insert Failed");
				} else if (customLabelsForm.getMessage() != null && customLabelsForm.getMessage().startsWith("ERR")) {
					request.setAttribute("error", customLabelsForm.getMessage());
				}
				customLabelsForm = new CustomLabelsForm();

			} else if (request.getParameter("active") != null) {
				String[] mapIds = customLabelsForm.getSelectedActItem();
				if (mapIds != null) {
					for (int i = 0; i < mapIds.length; i++) {
						customLabelsAgent.deleteCustomLabels(mapIds[i]);
					}
					customLabelsForm.setSelectedActItem(null);
				}

			} else if (request.getParameter("edit") != null) { // Edit mode on Hyperlink
				customLabelsForm = customLabelsAgent.getCustomLabelForm((String) request.getParameter("fieldId"));
				request.setAttribute("edit", "edit");
				
				request.setAttribute("generalTypeList", getLevelTypeDropDownList(customLabelsForm));
				
				List levelSubTypeList = null;
				levelSubTypeList = LookupAgent.getActivitySubTypesByActTypeId(customLabelsForm.getLevelTypeId());
				request.setAttribute("levelSubTypeList", levelSubTypeList);
				

			} else if (request.getParameter("getList") != null) { // getList mode
				if (request.getParameter("levelType").equalsIgnoreCase("ACTIVITY")) {
					atList = ActivityAgent.getActivityTypes();
				}

			} else if (request.getParameter("editSave") != null) { // Edit mode
				logger.debug("editSave into editSave ..  " + customLabelsForm.getFieldId());
				customLabelsAgent.updateCustomLabels(customLabelsForm);
				// Prompt Msg
				if (customLabelsForm.getMessage() != null && customLabelsForm.getMessage().startsWith("COMP")) {
					request.setAttribute("message", "CustomLabels Details Updated successfully.");
				} else if (customLabelsForm.getMessage() != null && customLabelsForm.getMessage().startsWith("ERR")) {
					request.setAttribute("error", customLabelsForm.getMessage());
				}
				customLabelsForm = new CustomLabelsForm();

			} else if (request.getParameter("levelTypeDesc") != null) {
				String changeList = "";
				String levelType = (String) request.getParameter("levelType");
				
				logger.debug("levelType::" + levelType);
				
				if(levelType != null){
				if(levelType.equalsIgnoreCase("Activity")){
					changeList = ActivityAgent.getLevelTypeAjaxList(levelType);
				}else if(levelType.equalsIgnoreCase("People")){
					changeList = getPeopleTypeAjaxList();
				}
				}
				 logger.debug("Ajax in action ::" + changeList);

				PrintWriter out = response.getWriter();
				out.println(changeList);
				// Close the writer
				out.close();

				return (mapping.findForward(null));
			}

			customLabelsForm.setComboList(comboList);
			customLabelsForm.setDisplayComboList(display);
			request.setAttribute("atList", atList);
			session.setAttribute("customLabelsForm", customLabelsForm);
			request.setAttribute("customLabelsForm", customLabelsForm);

		} catch (Exception e) {
			logger.error("error while getting department list " + e.getMessage());
		}
		return (mapping.findForward("success"));
	}
	
private List getLevelTypeDropDownList(CustomLabelsForm customLabelsForm) throws Exception{
		
		if(customLabelsForm.getLevelType() == null){
			return null;
		}
		List generalTypeList = null;

			List actTypeList  = new LookupAgent().getActivityTypes();
			if(actTypeList != null && actTypeList.size()>0){
				generalTypeList = new ArrayList<DropdownValue>();
				for(int i=0;i<actTypeList.size();i++){
					DropdownValue dropdownValue = new DropdownValue();
					ActivityType actType = (ActivityType) actTypeList.get(i);
					dropdownValue.setId(actType.getTypeId());
					dropdownValue.setLabel(actType.getDescription());
					generalTypeList.add(dropdownValue);
				}
			}
		

		return generalTypeList;
	}

private String getlevelSubType(int levelTypeId) throws ServletException{
	String  levelSubTyprString = "";
	try {
		List subTypeList = LookupAgent.getActivitySubTypesByActTypeId(levelTypeId);
		
		if(subTypeList != null && subTypeList.size()>0){
			for(int i=0;i<subTypeList.size();i++){
				ActivitySubType actSubType = (ActivitySubType) subTypeList.get(i);
				levelSubTyprString = levelSubTyprString + actSubType.getId() + "#@#" + actSubType.getDescription() + "$@$";
			}
		}

	} catch (AgentException e) {
		throw new ServletException("",e);
	}
	return levelSubTyprString;
}

private String getPeopleTypeAjaxList() throws Exception {
	
	String stringList = "";
	
	List PeopleTypes = LookupAgent.getPeopleTypes();
	
	if(PeopleTypes != null && PeopleTypes.size()>0){
		
		for(int i=0;i<PeopleTypes.size();i++){
			
			PeopleType peopleType = (PeopleType) PeopleTypes.get(i);
			
			stringList = stringList + peopleType.getTypeId() + ";" + peopleType.getType() + "||";
		}
		
		stringList = stringList.substring(0, stringList.lastIndexOf("||"));
		
	}
	
	return stringList;
}
}

package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.Street;
import elms.control.beans.admin.AdvancedMoveLsoForm;
import elms.util.StringUtils;

public class VerifyAdvancedMoveLsoAction extends Action {

	static Logger logger = Logger.getLogger(VerifyAdvancedMoveLsoAction.class.getName());
	String nextPage = "";
	String contextRoot = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering VerifyAdvancedMoveLsoAction");

		try {
			HttpSession session = request.getSession();
			ActionErrors errors = new ActionErrors();
			contextRoot = request.getContextPath();

			AdvancedMoveLsoForm advMoveLsoForm = (AdvancedMoveLsoForm) form;

			String streetNumber = advMoveLsoForm.getMoveNumber();
			String streetId = advMoveLsoForm.getMoveName();

			String tostreetNumber = advMoveLsoForm.getToNumber();
			String tostreetId = advMoveLsoForm.getToName();

			logger.debug("from street number is " + streetNumber);
			logger.debug("from street id is " + streetId);

			logger.debug("to street number is " + tostreetNumber);
			logger.debug("to street id is " + tostreetId);

			boolean active = false;
			AddressAgent lsoTreeAgent = new AddressAgent();

			/**
			 * First try to find the address in the Address Range Table. If found use that to display the tree. If not found then try to look thru the lso_address table for a matching entry. There are some lands being found in the address range table which don't have any entry in the lso_address table. This is a data issue
			 */
			List fromLsoIdList = new ArrayList();

			AddressAgent streetAgent = new AddressAgent();
			if (tostreetId == null || streetId == null) {
				if (errors.empty()) {
					logger.debug("Please enter valid address ");
					errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter valid addresss"));
					saveErrors(request, errors);
				}
				return (mapping.findForward("failure"));
			}
			Street street = streetAgent.getStreet(StringUtils.s2i(streetId));
			Address address = new Address(StringUtils.s2i(streetNumber), "", street, "");
			fromLsoIdList = lsoTreeAgent.getLandId(address, active);

			if (fromLsoIdList.size() == 0) {
				fromLsoIdList = lsoTreeAgent.getLsoIdList(streetNumber, StringUtils.s2i(streetId));
			}

			List fromLsoTreeList = lsoTreeAgent.getLsoTreeList(fromLsoIdList, active, contextRoot);

			logger.debug("fromLsoIdList size is " + fromLsoIdList.size());
			logger.debug("fromLsoTreeList size is " + fromLsoTreeList.size());

			List toLsoIdList = new ArrayList();

			street = streetAgent.getStreet(StringUtils.s2i(tostreetId));
			address = new Address(StringUtils.s2i(tostreetNumber), "", street, "");
			toLsoIdList = lsoTreeAgent.getLandId(address, active);

			if (toLsoIdList.size() == 0) {
				toLsoIdList = lsoTreeAgent.getLsoIdList(tostreetNumber, StringUtils.s2i(tostreetId));
			}

			List toLsoTreeList = lsoTreeAgent.getLsoTreeList(toLsoIdList, active, contextRoot);
			logger.debug("toLsoIdList size is " + toLsoIdList.size());
			logger.debug("toLsoTreeList size is " + toLsoTreeList.size());

			request.setAttribute("fromLsoTreeList", fromLsoTreeList);
			request.setAttribute("toLsoTreeList", toLsoTreeList);
			request.setAttribute("advancedMoveLsoForm", advMoveLsoForm);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Exiting VerifyAdvancedMoveLsoAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));

	}
}
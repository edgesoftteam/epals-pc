/**
 * 
 */
package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.ActivityStatusAdminForm;
import elms.control.beans.admin.AddressTypeAdminForm;
import elms.util.StringUtils;

/**
 * @author Arjun
 *
 */
public class AddressTypeAdminAction extends Action {

	static Logger logger = Logger.getLogger(AddressTypeAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into AddressTypeAdminAction .. ");

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		AdminAgent adminAgent = new AdminAgent();
		AddressTypeAdminForm addressTypeAdminForm = (AddressTypeAdminForm) form;
		List addressTypes = new ArrayList();
		addressTypeAdminForm.setDisplayAddressType("NO");

		// condition to lookup
		if (request.getParameter("lookup") != null) {
			result = 0;
			// getting and setting all the values to the form
		
			addressTypeAdminForm.setAddressType(request.getParameter("addressType"));
			addressTypeAdminForm.setBtFlag(StringUtils.s2b(request.getParameter("btFlag")));
			addressTypeAdminForm.setBlFlag(StringUtils.s2b(request.getParameter("blFlag")));
			
			addressTypes = adminAgent.getAddressType(addressTypeAdminForm);

			session.removeAttribute("addressTypeAdminForm");
			session.setAttribute("oldAddressTypeForm", addressTypeAdminForm);
			addressTypeAdminForm.setAddressTypes(addressTypes);
			addressTypeAdminForm.setDisplayAddressType("NO");

			PrintWriter pw = response.getWriter();
			if (addressTypes.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(addressTypes));
			}
			return null;
		} else if (request.getParameter("typeId") != null) { // condition to populate values to jsp page
			result = 0;
			addressTypeAdminForm = adminAgent.getAddressType(StringUtils.s2i(request.getParameter("typeId")), addressTypeAdminForm);
			addressTypeAdminForm.setDisplayAddressType("NO");

			PrintWriter pw = response.getWriter();
			pw.write(addressTypeAdminForm.getAddressType() + ',' +addressTypeAdminForm.isBtFlag() + ',' + addressTypeAdminForm.isBlFlag());
			return null;
		} else if (request.getParameter("save") != null) { // condition to save

			AddressTypeAdminForm addTypeForm = (AddressTypeAdminForm) session.getAttribute("oldAddressTypeForm");
			
			addressTypeAdminForm.setId(request.getParameter("id"));
			addressTypeAdminForm.setAddressType(request.getParameter("addressType"));
			addressTypeAdminForm.setBtFlag(StringUtils.s2b(request.getParameter("btFlag")));
			addressTypeAdminForm.setBlFlag(StringUtils.s2b(request.getParameter("blFlag")));
			logger.debug(" Id : "+addressTypeAdminForm.getId());
			if(addressTypeAdminForm.getId() ==null || addressTypeAdminForm.getId().equalsIgnoreCase("0") || addressTypeAdminForm.getId().equalsIgnoreCase("-1")){
			result= adminAgent.checkAddressType(addressTypeAdminForm.getAddressType());
			}
			// save function called from Adminagent
			if(result!=3){
			result = adminAgent.saveAddressType(addressTypeAdminForm.getId(), addressTypeAdminForm.getAddressType(), addressTypeAdminForm.isBtFlag(), addressTypeAdminForm.isBlFlag());
			}
			addressTypeAdminForm.setDisplayAddressType("NO");
			logger.debug("result :"+result);
			if (result == 1) {
				PrintWriter pw = response.getWriter();
				pw.write("Address Type Saved Successfully");
			} else if (result == 2) {
				addressTypes = adminAgent.getAddressType(addressTypeAdminForm);

				PrintWriter pw = response.getWriter();
				pw.write(innerTable(addressTypes));
			}else if (result == 3) {
				PrintWriter pw = response.getWriter();
				pw.write("Address Type Already Available");
			} else {
				errors.add("statusCodeExists", new ActionError("statusCodeExists", " Address Type is not added"));
				saveErrors(request, errors);
			}
			return null;
		} else if (request.getParameter("delete") != null) {// condition to delete

			// getting and setting all the values to the form
			
			String idList = (String) request.getParameter("checkboxArray");

			addressTypeAdminForm.setId(request.getParameter("id"));
			addressTypeAdminForm.setAddressType(request.getParameter("addressType"));
			addressTypeAdminForm.setBtFlag(StringUtils.s2b(request.getParameter("btFlag")));
			addressTypeAdminForm.setBlFlag(StringUtils.s2b(request.getParameter("blFlag")));
			
			result = adminAgent.deleteAddressType(idList);

			AddressTypeAdminForm oldAddrTypeForm = (AddressTypeAdminForm) session.getAttribute("oldAddressTypeForm");

			addressTypeAdminForm.setAddressTypes(adminAgent.getAddressType(oldAddrTypeForm));
			addressTypeAdminForm.setDisplayAddressType("YES");

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(addressTypeAdminForm.getAddressTypes()));
			return null;
		} else {
			addressTypeAdminForm = new AddressTypeAdminForm();
			addressTypeAdminForm.setDisplayAddressType("NO");
			session.setAttribute("addressTypeAdminForm", addressTypeAdminForm);
		}// end of if
		return (mapping.findForward("success"));
	}

	public String innerTable(List addressTypes) {
		String actStatusHtml = "";
		if (addressTypes != null && addressTypes.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem addressType = null;

			if (addressTypes != null && addressTypes.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"100%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><font class=\"con_hdr_3b\">Address Type List<br><br></font></td></tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"98%\" height=\"25\" bgcolor=\"#B7C1CB\"><font class=\"con_hdr_2b\">Address Type</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Address Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"1\">");
				sb.append("<tr bgcolor=\"#FFFFFF\">");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Address Type</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">BT Active</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">BL Active</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < addressTypes.size(); i++) {
					addressType = (DisplayItem) addressTypes.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (addressType.getFieldFive().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + addressType.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + addressType.getFieldOne() + ")\" >");
					sb.append(addressType.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(addressType.getFieldThree());
					sb.append("</font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(StringUtils.nullReplaceWithEmpty(addressType.getFieldFour()));
					sb.append("</font></td>");
					/*sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(activityStatus.getFieldFive());
					sb.append("</font></td>");*/
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			actStatusHtml = sb.toString();
			// logger.debug(actStatusHtml);
		}
		return actStatusHtml;
	}
}
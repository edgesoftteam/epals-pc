package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.OnlineAgent;
import elms.common.Constants;
import elms.control.beans.admin.OnlinePermitForm;
import elms.util.StringUtils;

public class OnlineLoadQAAction extends Action {
	static Logger logger = Logger.getLogger(OnlineLoadQAAction.class.getName());
	String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered OnlineLoadQAAction ...");

		OnlinePermitForm onlinePermitForm = (OnlinePermitForm) form;
		OnlineAgent opma = new OnlineAgent();
		String action = request.getParameter("action") != null ? (String) request.getParameter("action") : "";
		List questionaireList = new ArrayList();
		List lkupQuestionaireList = new ArrayList();

		String message = "";

		try {

			
			
			if (action.equals("edit")) {
				logger.info("get info"+onlinePermitForm.getStypeId());
				//actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
			} else if (action.equals("save")) {
				opma.saveLkupQA(onlinePermitForm.getActType(),onlinePermitForm.getDescription());
				message = "Saved successfully";
			}else if (action.equals("del")) {
				
				String lkId[] = onlinePermitForm.getDeleteSelectedId();
				for (int i = 0; i < lkId.length; i++) {
					opma.deleteLoadQAId(onlinePermitForm.getActType(),StringUtils.s2i(lkId[i]));
				}
					
				//actSubFeeMap = opma.getActSubFeeMapping(onlinePermitForm.getStypeId() + "");
				message = "Deleted successfully";
			}
			
			if(!StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()).equals("")){
				if(onlinePermitForm.getActType().equalsIgnoreCase("LKUP_QUESTIONS")){
					questionaireList = opma.getQuestionaireList();
					
				}else {
					questionaireList = opma.getAcknowledgementList();
				}
				
				lkupQuestionaireList = opma.getLkupLoadQAList(onlinePermitForm.getActType()); 
			}
			

			request.setAttribute("message", message);
			
			request.setAttribute("questionaireList", questionaireList);
			request.setAttribute("lkupQuestionaireList", lkupQuestionaireList);
			
			onlinePermitForm.setActType(StringUtils.nullReplaceWithEmpty(onlinePermitForm.getActType()));
			request.setAttribute("onlinePermitForm", onlinePermitForm);
			
			
		} catch (Exception e) {
			logger.error("error in OnlineLoadQAAction " + e.getMessage());
		}
		logger.debug("Exiting OnlineAcknowledgementAction ...");
		return (mapping.findForward(nextPage));
	}
}
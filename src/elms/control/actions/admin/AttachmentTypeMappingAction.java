package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.app.admin.ActivityType;
import elms.control.beans.admin.AttachmentTypeAdmin;
import elms.control.beans.admin.AttachmentTypeForm;
import elms.control.beans.admin.AttachmentTypeMappingForm;
import elms.security.User;
import elms.util.StringUtils;

public class AttachmentTypeMappingAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AttachmentTypeMappingAction.class.getName());
	String nextPage = "";

	@SuppressWarnings("rawtypes")
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		ActionErrors errors = new ActionErrors();
		AttachmentTypeMappingForm attachmentTypeMappingForm = (AttachmentTypeMappingForm) form;
		AdminAgent adminAgent = new AdminAgent();
		List attachmentList = new ArrayList();
		HttpSession session = request.getSession();
		List attachmentTypeList = new ArrayList();
		List activityTypesList = null;
		
		try {
			String action = request.getParameter("action") != null ? request.getParameter("action") : "display";
			User user = (User) session.getAttribute("user");
			if (action.equalsIgnoreCase("display")) {
				attachmentList = adminAgent.getAttachmentTypes();
				request.setAttribute("attachmentList", attachmentList);
				nextPage = "success";
			}else if (action.equalsIgnoreCase("new")) {
				request.setAttribute("addFlag", "Y");
				attachmentTypeMappingForm.setAttachmentTypeId(0);
				attachmentTypeList = adminAgent.getAttachmentTypesForAdd();
				activityTypesList = adminAgent.getActivityTypes();
				nextPage = "template";
			}else if (action.equalsIgnoreCase("edit")) {
				String id = request.getParameter("id") != null ? request.getParameter("id") : "0";
				String actTypeId = request.getParameter("actTypeId") != null ? request.getParameter("actTypeId") : "0";				
				attachmentTypeList = adminAgent.getAttachment(StringUtils.s2i(id));
				activityTypesList = adminAgent.getActivityTypes();			
				  
				for(int i=0; i< activityTypesList.size(); i++) {
					AttachmentTypeAdmin adminData = (AttachmentTypeAdmin)activityTypesList.get(i);
					if(adminData.getActTypeId() == Integer.parseInt(actTypeId)) {
			        	 activityTypesList.remove(i);
			        	 activityTypesList.add(0,adminData);
			         }
				}
				
				nextPage = "template";				
			}else if (action.equalsIgnoreCase("delete")) {
				String[] mapIds = attachmentTypeMappingForm.getSelectedItems();
				for (int i = 0; i < mapIds.length; i++) {
					adminAgent.deleteAttachment(StringUtils.s2i(mapIds[i]));
					logger.debug("DELETE "+mapIds[i]);
				}
				attachmentTypeMappingForm.setSelectedItems(null);
				attachmentList = adminAgent.getAttachmentTypes();
				request.setAttribute("attachmentList", attachmentList);
				nextPage = "success";				
			}else if (action.equalsIgnoreCase("save")) {				
				//adminAgent.updateAttachment(attachmentTypeMappingForm);
				
				int attachmentTypeId = Integer.parseInt(attachmentTypeMappingForm.getDescription());
				if(attachmentTypeId > 0) {
					boolean flag = adminAgent.checkIdExistOrNot(attachmentTypeId);
					if(flag == true) {
						adminAgent.updateAttachment(attachmentTypeMappingForm);
					}else {
						adminAgent.saveAttachment(attachmentTypeMappingForm);
					}
				}		
				
				attachmentList = adminAgent.getAttachmentTypes();
				request.setAttribute("attachmentList", attachmentList);
				nextPage = "success";
			}

			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the login page");
				saveErrors(request, errors);

				return (new ActionForward(mapping.getInput()));
			}
			request.setAttribute("attachmentTypeList", attachmentTypeList);	
			request.setAttribute("activityTypesList", activityTypesList);	
			
			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			logger.error("Exception occured while online logon " + e.getMessage());
			return (mapping.findForward("error"));
		}

	}
}

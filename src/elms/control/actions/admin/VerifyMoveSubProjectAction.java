package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.control.beans.MoveProjectForm;
import elms.control.beans.MoveVerifyForm;

public class VerifyMoveSubProjectAction extends Action {

	static Logger logger = Logger.getLogger(VerifyMoveProjectAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		MoveProjectForm moveProjectForm = (MoveProjectForm) form;

		try {
			AdminAgent moveProjectAgent = new AdminAgent();

			String projectNbr = moveProjectForm.getSprojectno();
			logger.debug("Got Project No from form " + projectNbr);
			String subProjectNbr = moveProjectForm.getSsubprojectno();
			logger.debug("Got sub Project no from form " + subProjectNbr);

			MoveVerifyForm verifyForm = new MoveVerifyForm();
			verifyForm = moveProjectAgent.getSubProject(subProjectNbr, verifyForm);
			verifyForm = moveProjectAgent.getProject(projectNbr, verifyForm);

			session.setAttribute("moveVerifyForm", verifyForm);
			session.setAttribute("subProjectNbr", subProjectNbr);

			nextPage = "success";
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));

	}

}
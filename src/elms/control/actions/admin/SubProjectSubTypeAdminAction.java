package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.app.admin.SubProjectSubType;
import elms.app.common.DisplayItem;
import elms.control.beans.admin.SubProjectLookupsForm;
import elms.util.StringUtils;

public class SubProjectSubTypeAdminAction extends Action {

	static Logger logger = Logger.getLogger(SubProjectSubTypeAdminAction.class.getName());
	int result = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered into SubProjectNameAdminAction .. ");

		HttpSession session = request.getSession();
		AdminAgent adminAgent = new AdminAgent();
		SubProjectLookupsForm sprojLookupsForm = (SubProjectLookupsForm) form;

		List subProjectSubTypeList = new ArrayList();

		if (request.getParameter("lookup") != null) {
			result = 0;
			sprojLookupsForm.setType(StringUtils.nullReplaceWithEmpty(request.getParameter("type")));
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));

			subProjectSubTypeList = adminAgent.getSubProjectSubTypes(sprojLookupsForm);

			session.removeAttribute("sprojLookupsForm");
			session.setAttribute("oldActSubTypeForm", sprojLookupsForm);

			sprojLookupsForm.setSubProjectSubTypeList(subProjectSubTypeList);

			PrintWriter pw = response.getWriter();
			if (subProjectSubTypeList.size() == 0) {
				pw.write("No Records Found");
			} else {
				pw.write(innerTable(subProjectSubTypeList));
			}
			return null;
		} else if (request.getParameter("id") != null) {
			result = 0;
			SubProjectSubType subProjectSubType = adminAgent.getSubProjectSubType(StringUtils.s2i(request.getParameter("id")));

			sprojLookupsForm.setSubProjectTypeId(subProjectSubType.getSubProjectSubTypeId());
			sprojLookupsForm.setType(subProjectSubType.getType());
			sprojLookupsForm.setDescription(subProjectSubType.getDescription());

			PrintWriter pw = response.getWriter();
			pw.write(sprojLookupsForm.getType().toString() + ',' + sprojLookupsForm.getDescription().toString());
			return null;

		} else if (request.getParameter("save") != null) {

			SubProjectLookupsForm subProjForm = (SubProjectLookupsForm) session.getAttribute("oldActSubTypeForm");

			sprojLookupsForm.setType(StringUtils.nullReplaceWithEmpty(request.getParameter("type")));
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));

			int spId = StringUtils.s2i(request.getParameter("subProjectTypeId"));
			if (spId <= 0)
				spId = 0;
			sprojLookupsForm.setSubProjectTypeId(spId);

			result = adminAgent.saveSubProjectSubType(sprojLookupsForm.getType(), sprojLookupsForm.getDescription(), sprojLookupsForm.getSubProjectTypeId(),sprojLookupsForm.getActivityType(),sprojLookupsForm.getSubProjectType());

			PrintWriter pw = response.getWriter();
			if (result == 1) {
				pw.write("Sub Project Sub Type Saved Successfully");
			} else if (result == 2) {
				subProjectSubTypeList = adminAgent.getSubProjectSubTypes(subProjForm);

				pw.write(innerTable(subProjectSubTypeList));
			} else {
				pw.write("");
			}
			return null;

		} else if (request.getParameter("delete") != null) {

			sprojLookupsForm.setType(StringUtils.nullReplaceWithEmpty(request.getParameter("type")));
			sprojLookupsForm.setDescription(StringUtils.nullReplaceWithEmpty(request.getParameter("desc")));

			String idList = (String) request.getParameter("checkboxArray");

			result = adminAgent.deleteSubProjectSubType(idList);

			SubProjectLookupsForm subProjDelForm = (SubProjectLookupsForm) session.getAttribute("oldActSubTypeForm");
			// session.removeAttribute("oldActTypeForm");

			subProjectSubTypeList = adminAgent.getSubProjectSubTypes(subProjDelForm);

			PrintWriter pw = response.getWriter();
			pw.write(innerTable(subProjectSubTypeList));
			return null;
		} else {
			sprojLookupsForm = new SubProjectLookupsForm();
			subProjectSubTypeList = new ArrayList();
			sprojLookupsForm.setSubProjectSubTypes(subProjectSubTypeList);
		}
		session.setAttribute("sprojLookupsForm", sprojLookupsForm);
		
		
		List activityTypes;
		List subProjectTypes;
		try {
			subProjectTypes=LookupAgent.getSubProjectTypes();
			activityTypes = LookupAgent.getActivityTypes();
		} catch (Exception e2) {
			activityTypes = new ArrayList();
			subProjectTypes = new ArrayList();
			logger.warn("activityTypes is not available, initializing it to blank arraylist");
		}
		request.setAttribute("activityTypes", activityTypes);
		request.setAttribute("subProjectTypes", subProjectTypes);
		logger.debug("The Activity Type Maping Activity Types set to request with size " + activityTypes.size());

		return (mapping.findForward("success"));
	}

	public String innerTable(List spSubType) {
		String subProjSubTypeHtml = "";
		if (spSubType != null && spSubType.size() > 0) {
			StringBuffer sb = new StringBuffer("");
			DisplayItem sprojNames = null;

			if (spSubType != null && spSubType.size() > 0) {
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"10\" cellpadding=\"0\" >");
				sb.append("<tr valign=\"top\">");
				sb.append("<td width=\"99%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr>");
				sb.append("<td><font class=\"con_hdr_3b\">Sub Project Sub Types List<br>");
				sb.append("<br>");
				sb.append("</font></td></tr><tr>");
				if (result == 2) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Saved Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				} else if (result == 3) {
					sb.append("<tr>");
					sb.append("<td><font class=\"con_hdr_3b\" style=\"color:#088A08\">Record Deleted Successfully<br>");
					sb.append("<br>");
					sb.append("</font></td></tr><tr>");
				}
				sb.append("<td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">");
				sb.append("<tr><td><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr>");
				sb.append("<td width=\"95%\" height=\"25\" background=\"../images/site_bg_B7C1CB.jpg\"><font class=\"con_hdr_2b\">Sub Project Sub Types</font></td>");
				sb.append("<td width=\"1%\"><img src=\"../images/site_hdr_split_1_tall.jpg\" width=\"26\" height=\"25\"></td>");
				sb.append("<td width=\"1%\" background=\"../images/site_bg_D7DCE2.jpg\"><nobr>");
				sb.append("<input type=\"button\" property=\"delete\" value=\"Delete\" styleClass=\"button\" onclick=\"removeTable()\"/>");
				sb.append("<input type=\"button\" property=\"add\" value=\"Add Type\" styleClass=\"button\" onclick=\"refresh()\"/>");
				sb.append("&nbsp;</nobr></td></tr></table></td></tr>");
				sb.append("<tr>");
				sb.append("<td background=\"../images/site_bg_B7C1CB.jpg\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"1\" cellpadding=\"2\">");
				sb.append("<tr>");
				sb.append("<td width=\"10%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Delete</font></td>");
				sb.append("<td width=\"15%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Sub Project Sub Type</font></td>");
				sb.append("<td width=\"30%\" background=\"../images/site_bg_D7DCE2.jpg\"><font class=\"con_hdr_1\">Description</font></td>");
				sb.append("</tr>");

				for (int i = 0; i < spSubType.size(); i++) {
					sprojNames = (DisplayItem) spSubType.get(i);

					sb.append("<tr bgcolor=\"#FFFFFF\">");
					if (sprojNames.getFieldSix().equalsIgnoreCase("Y")) {
						sb.append("<td>");
						sb.append("<input type=\"checkbox\" name=\"selectedMaping\" value=\"" + sprojNames.getFieldOne() + "\"  onclick=\"checkIds(this)\" >");
						sb.append("</td>");
					} else {
						sb.append("<td>&nbsp;</td>");
					}
					sb.append("<td bgcolor=\"#FFFFFF\">");
					sb.append("<font class=\"con_text_1\"><a href=\"javascript:void(0)\" onclick=\"CreateXmlHttp(" + sprojNames.getFieldOne() + "," + StringUtils.checkString(sprojNames.getFieldSix()) + ")\" >");
					sb.append(sprojNames.getFieldTwo());
					sb.append("</a></font></td>");
					sb.append("<td bgcolor=\"#FFFFFF\"><font class=\"con_text_1\">");
					sb.append(sprojNames.getFieldThree());
					sb.append("</font></td>");
					sb.append("</tr>");
				}
				sb.append("</table></td></tr></table></td></tr><tr><td>&nbsp;</td></tr></table></td><td width=\"1%\">");
				sb.append("<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"><tr><td height=\"32\">&nbsp;</td>");
				sb.append("</tr><tr><td>&nbsp;</td></tr></table></td></tr></table>");
			}
			subProjSubTypeHtml = sb.toString();
			// logger.debug(actTypesHtml);
		}
		return subProjSubTypeHtml;
	}

}
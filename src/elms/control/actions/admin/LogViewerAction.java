package elms.control.actions.admin;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.common.Constants;
import elms.security.User;

public class LogViewerAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(LogViewerAction.class.getName());
	String nextPage = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession(true);
		User user = (User) session.getAttribute(Constants.USER_KEY);
		String username = user.getUsername();
		if (username.equalsIgnoreCase("superadmin")) {
			logger.debug("super admin role");
			return (mapping.findForward("success"));

		} else {
			return (mapping.findForward("error"));
		}

	}
}

package elms.control.actions.admin;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.control.beans.admin.AttachmentTypeForm;
import elms.security.Department;
import elms.util.StringUtils;

public class AttachmentTypeAction extends Action {

    static Logger logger = Logger.getLogger(AttachmentTypeAction.class.getName());

    public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        logger.debug("Entered into AttachmentTypeAction .. ");
        HttpSession session = request.getSession();
        ActionErrors errors = new ActionErrors();
        int deleteCount = 0;
        
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        PrintWriter pw = response.getWriter(); 

        String action = request.getParameter("action")!=null?request.getParameter("action"):"";
        AdminAgent adminAgent = new AdminAgent();
        AttachmentTypeForm attachmentTypeForm = (AttachmentTypeForm) form;
        
        /*elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
        attachmentTypeForm.setCreatedBy(user.getUserId());
        attachmentTypeForm.setUpdatedBy(user.getUserId());*/
        @SuppressWarnings("rawtypes")
		List attachmentTypeList;
                
        try {
        	List departments = LookupAgent.getDepartmentList();
	        if (action.equalsIgnoreCase("lookup")) {
	        	 attachmentTypeForm.setDisplayAttachmentType("YES");
	        	 attachmentTypeList = adminAgent.getAttachmentTypes(attachmentTypeForm);
	        	 attachmentTypeForm.setAttachmentTypeList(attachmentTypeList);
	        }else if (action.equalsIgnoreCase("edit")) {
	        	 attachmentTypeForm.setAttachmentTypeId(StringUtils.s2i(request.getParameter("attachmentTypeId")));	        	
	        	 attachmentTypeForm.setDisplayAttachmentType("NO");
	        }else if (action.equalsIgnoreCase("Save")) {
	        	 attachmentTypeForm.setDisplayAttachmentType("NO");
	        	 int attachmentTypeId = adminAgent.saveAttachmentType(attachmentTypeForm);
	        	 attachmentTypeForm.setAttachmentTypeId(attachmentTypeId);
	        	// int attachmentTypeId = attachmentTypeForm.getAttachmentTypeId();
					if(attachmentTypeId > 0) {
						boolean flag = adminAgent.checkIdExistOrNot(attachmentTypeId);
						if(flag == true) {
							adminAgent.updateAttachment(attachmentTypeForm);
						}else {
							adminAgent.saveAttachment(attachmentTypeForm);
						}
					}
					
					if(attachmentTypeForm.getModuleId() != null && !" ".equals(attachmentTypeForm.getModuleId())) {						
						request.setAttribute("activityTypes", adminAgent.getActivityTypes(Integer.parseInt(attachmentTypeForm.getModuleId())));
					}else {
						request.setAttribute("activityTypes", adminAgent.getActivityTypesData());
					}
		        	 
		        	int actTypeId = Integer.parseInt(attachmentTypeForm.getActivityType());
		        	attachmentTypeForm.setAttachmentTypeDesc(String.valueOf(attachmentTypeForm.getAttachmentTypeId()));
		        	request.setAttribute("attachmentTypes", adminAgent.getAttachmentTypes(actTypeId));
					
	        	 errors.add("error", new ActionError("error.attachment.type.add.success"));		        	 
	        }else if (request.getParameter("id") != null) {
	        	 String depts = request.getParameter("department").replace("[","").replace("]", "");
	        	 String levels = request.getParameter("level").replace("[","").replace("]", "");
	        	 attachmentTypeForm = adminAgent.getAttachmentType(StringUtils.s2i(request.getParameter("id")),depts,levels);
	        	 attachmentTypeForm.setDisplayAttachmentType("NO");
	        	 request.setAttribute("activityTypes", adminAgent.getActivityTypesData());
	        	 
	        	 int actTypeId = Integer.parseInt(attachmentTypeForm.getActivityType());
	        	 attachmentTypeForm.setAttachmentTypeDesc(String.valueOf(attachmentTypeForm.getAttachmentTypeId()));
	        	 request.setAttribute("attachmentTypes", adminAgent.getAttachmentTypes(actTypeId));
	        	 request.setAttribute("update", "Y");
	        }else if (action.equalsIgnoreCase("delete")) {
	            //geting items to delete
	            String[] mapIds = attachmentTypeForm.getSelectedItems();
	            for (int i = 0; i < mapIds.length; i++) {
	            	deleteCount = adminAgent.deleteAttachmentType(mapIds[i]);
	            	deleteCount ++;
	            }

	            if(deleteCount > 0) {
	            	errors.add("error", new ActionError("error.attachment.type.delete.success"));	            	
	            }
	            attachmentTypeForm.setDisplayAttachmentType("YES");
	            List attachmentTypeListData = adminAgent.getAttachmentTypes(attachmentTypeForm);
	            attachmentTypeForm.setAttachmentTypeList(attachmentTypeListData);
	        }else if(action.equalsIgnoreCase("actTypes")) {        	          
	        		
	    		try{	
	    			
	    			int moduleId = Integer.parseInt(request.getParameter("moduleId"));
	    			pw.write(adminAgent.getActivityTypes(moduleId));
	    			
	    		}catch(Exception e){
	    			logger.error("Error while getting activitytypes "+e.getMessage());			
	    		}		
	    		return null;
	        }else if(action.equalsIgnoreCase("attachmentTypes")) {
	        		
	    		try{	
	    			
	    			int actTypeId = Integer.parseInt(request.getParameter("actTypeId"));
	    			pw.write(adminAgent.getAttachmentTypes(actTypeId));
	    			
	    		}catch(Exception e){
	    			logger.error("Error while getting attachment types "+e.getMessage());			
	    		}		
	    		return null;
	        }else {

	        	attachmentTypeForm = new AttachmentTypeForm();	        		        	
	        	attachmentTypeForm.setDisplayAttachmentType("NO");
	        	//attachmentTypeForm.setDepartmentsList(departments);
	        	//request.setAttribute("departments", departments);
	        }
	        
	        List modules;
			try {
				modules = LookupAgent.getModules();
			} catch (Exception e) {
				modules = new ArrayList();
				logger.warn("no module list obtained, so initializing it to blank arraylist");
			}
			request.setAttribute("modules", modules);
			logger.debug("The modules set with size .. " + departments.size());

	        saveErrors(request, errors);
	        session.setAttribute("attachmentTypeForm", attachmentTypeForm);
	        request.setAttribute("attachmentTypeForm", attachmentTypeForm);	
	        request.setAttribute("departments", departments);
        }catch (Exception e) {
        	e.printStackTrace();
        	logger.error(e.getMessage());
        }
        return (mapping.findForward("success"));
    }
}

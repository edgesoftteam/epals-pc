package elms.control.actions.admin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.app.finance.LookupFee;
import elms.control.beans.LookupFeeEdit;
import elms.control.beans.LookupFeesForm;
import elms.util.StringUtils;

public class SaveFeesLookupAction extends Action {

	static Logger logger = Logger.getLogger(SaveFeesLookupAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		// Extract attributes and parameters we will need

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			int lookupId = StringUtils.s2i((String) session.getAttribute("lookupId"));
			List lookupFeeList = new ArrayList();

			LookupFeesForm subForm = (LookupFeesForm) session.getAttribute("lookupFeesForm");
			LookupFeeEdit[] lst = subForm.getLkupFeeEditList();

			for (int i = 0; i < lst.length; i++) {
				LookupFee lkupFee = new LookupFee();
				lkupFee.setLookupFeeId(lookupId);
				lkupFee.setLookupFee(lst[i].getLookupFee());
				lkupFee.setCreationDate(StringUtils.str2cal(lst[i].getCreationDate()));
				lkupFee.setExpirationDate(0);
				lkupFee.setLowRange(StringUtils.s2bd(lst[i].getLowRange()));
				lkupFee.setHighRange(StringUtils.s2bd(lst[i].getHighRange()));
				lkupFee.setOver(StringUtils.s2bd(lst[i].getOver()));
				lkupFee.setPlus(StringUtils.s2bd(lst[i].getPlus()));
				lkupFee.setResult(StringUtils.s2bd(lst[i].getResult()));
				logger.info(lkupFee);
				lookupFeeList.add(lkupFee);
			}

			// Save the fees to the database by calling the Agent
			FinanceAgent fa = new FinanceAgent();
			fa.setLookupFee(lookupId, lookupFeeList);
			List lookupList = fa.getLookupFeeList();

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			ActionForm frm = new LookupFeesForm();
			LookupFeesForm frmFee = (LookupFeesForm) frm;

			session.setAttribute("lookupFeesForm", frmFee);
			session.setAttribute("lookupList", lookupList);
			session.setAttribute("lookupId", "0");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

} // End class
package elms.control.actions.sitedata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.app.common.PickList;
import elms.app.sitedata.SiteData;
import elms.app.sitedata.SiteStructureData;
import elms.control.beans.sitedata.SiteStructureForm;
import elms.util.StringUtils;

public class ViewSiteStructureAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ViewSiteStructureAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewSiteStructureAction()");

		HttpSession session = request.getSession();
		SiteStructureForm siteStructureForm = (SiteStructureForm) form;
		String nextPage = "";

		String action = (String) request.getParameter("action") != null ? request.getParameter("action") : "list";
		String activeSite = (String) request.getParameter("activeSite") != null ? request.getParameter("activeSite") : "Y";

		long structureId = siteStructureForm.getStructureId();
		logger.debug("structure id of this form is " + siteStructureForm.getStructureId());

		long lsoId = siteStructureForm.getLsoId();
		logger.debug("lso id of this site structure form obtained is " + siteStructureForm.getLsoId());

		List structureList = new ArrayList();

		logger.debug("level type from form is " + siteStructureForm.getLevelType());

		logger.debug("level id from form is " + siteStructureForm.getLevelId());

		AddressAgent siteDataAgent = new AddressAgent();

		try {

			if (action.equalsIgnoreCase("list")) {
				// list site structures
				lsoId = siteStructureForm.getLevelId();//siteDataAgent.getLandId(siteStructureForm.getLevelType(), siteStructureForm.getLevelId());
				logger.debug("lso id obtained from site data agent is " + lsoId);
				siteStructureForm.setLsoId(lsoId);
				structureList = siteDataAgent.getSiteStructures(lsoId, activeSite);
				siteStructureForm.setStructureList(structureList);
				siteStructureForm.setActiveSite(activeSite);

				nextPage = "gotoStructureList";
			} else if (action.equalsIgnoreCase("add")) {
				// add new site structure
				SiteStructureData structure = new SiteStructureData(lsoId);
				String zoning = structure.getZoning();
				logger.debug("Zoning initialized as " + zoning);
				if (zoning == null) {
					// get the land zone and populate to this one
					zoning = StringUtils.i2s(LookupAgent.getLandZoneId(lsoId));
					logger.debug("obtained zoning is " + zoning);
					structure.setZoning(zoning);
				}

				// set the building type to the land use
				String buildingType = structure.getBuildingType();
				logger.debug("building type id is  " + buildingType);

				if (buildingType == null) {
					// get the land use and populate it to building type
					buildingType = StringUtils.i2s(LookupAgent.getLandUseId(lsoId));
					logger.debug("obtained building type id is " + buildingType);
					structure.setBuildingType(buildingType);
				}
				structure.setActive("on");
				siteStructureForm.setSiteStructure(structure);
				nextPage = "gotoStructure";

			} else if (action.equalsIgnoreCase("edit")) {
				// view site structure
				SiteStructureData structure = siteDataAgent.getSiteStructure(structureId);
				logger.debug("Building Type " + structure.getBuildingType());

				siteStructureForm.setStructureId(structureId);
				siteStructureForm.setSiteStructure(structure);
				SiteData siteData = siteDataAgent.getSetbackData(lsoId);
				siteStructureForm.setSetback(siteData.getSetback());

				// set the list of site data history to the form
				List structureHistory = siteDataAgent.getStructureHistory(structureId);
				siteStructureForm.setStructureHistory(structureHistory);
				nextPage = "gotoStructure";

			} else if (action.equalsIgnoreCase("version")) {
				// view site structure
				String strVersion = request.getParameter("version");
				SiteStructureData structure = siteDataAgent.getSiteStructureHistory(structureId,StringUtils.s2i(strVersion));
				logger.debug("Building Type " + structure.getBuildingType());

				siteStructureForm.setStructureId(structureId);
				siteStructureForm.setSiteStructure(structure);
				SiteData siteData = siteDataAgent.getSetbackData(lsoId);
				siteStructureForm.setSetback(siteData.getSetback());

				nextPage = "gotoStructureHistory";

			}

			List sprinklerList = new ArrayList();
			sprinklerList.add(new PickList(1, "Y"));
			sprinklerList.add(new PickList(2, "N"));
			request.setAttribute("sprinklerList", sprinklerList);

			List buildingTypeList = new ArrayList();
			buildingTypeList = new AddressAgent().getUseList("L");
			request.setAttribute("buildingTypeList", buildingTypeList);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		request.setAttribute("flag", action);
		session.setAttribute("siteStructureForm", siteStructureForm);
		logger.info("Exiting ViewSiteStructureAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}

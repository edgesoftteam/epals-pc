package elms.control.actions.sitedata;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.common.PickList;
import elms.app.sitedata.SiteStructureData;
import elms.control.actions.lso.ViewOccupancyAction;
import elms.control.actions.lso.ViewStructureAction;
import elms.control.beans.sitedata.SiteStructureForm;
import elms.security.User;

public class SaveSiteStructureAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SaveSiteStructureAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		SiteStructureForm siteStructureForm = (SiteStructureForm) session.getAttribute("siteStructureForm");
		User user = (User) session.getAttribute("user");
		String nextPage = "success";
		String flag = "add";

		try {
			logger.debug("lso id of site structure form is " + siteStructureForm.getLsoId());
			logger.debug("SaveSiteStructureAction(" + siteStructureForm.getAction() + ")");

			if (siteStructureForm.getAction().equalsIgnoreCase("Cancel")) {
				long levelId = siteStructureForm.getLevelId();
				String levelType = siteStructureForm.getLevelType();
				logger.debug("Level Id :" + levelId + " : Level Type : " + levelType);

				session.removeAttribute("siteStructureForm");

				switch (levelType.charAt(0)) {
				case 'L':
					nextPage = "cancelLand";

					break;

				case 'S':
					new ViewStructureAction().processStructure(request, "" + levelId, "");
					nextPage = "cancelStructure";

					break;

				case 'O':
					new ViewOccupancyAction().processOccupancy(request, (int) levelId, "");
					nextPage = "cancelOccupancy";

					break;

				default:
					nextPage = "cancelPSA";
				}

				return (mapping.findForward(nextPage));
			} else if (siteStructureForm.getAction().equalsIgnoreCase("Save")) {
				flag = "edit";
				AddressAgent siteDataAgent = new AddressAgent();
				SiteStructureData structure = siteStructureForm.getSiteStructure();
				logger.debug(structure);
				String active = (String) request.getParameter("active") != null ? request.getParameter("active") : "N";
				structure.setActive(active);
				structure.setUpdatedBy("" + user.getUserId());
				siteDataAgent.saveSiteStructure(structure);

				if (active.equalsIgnoreCase("Y")) {
					structure.setActive("on");
				} else {
					structure.setActive("off");
				}
				siteStructureForm.setSiteStructure(structure);
				siteDataAgent.addSetbackData(siteStructureForm.getLsoId(), siteStructureForm.getSetback(), siteStructureForm.getStructureId());
				List structureHistory = siteDataAgent.getStructureHistory(structure.getStructureId());
				siteStructureForm.setStructureHistory(structureHistory);

				session.setAttribute("siteStructureForm", siteStructureForm);
			}

			List sprinklerList = new ArrayList();
			sprinklerList.add(new PickList(1, "Y"));
			sprinklerList.add(new PickList(2, "N"));
			request.setAttribute("sprinklerList", sprinklerList);

			List buildingTypeList = new ArrayList();
			buildingTypeList = new AddressAgent().getUseList("L");
			request.setAttribute("buildingTypeList", buildingTypeList);
			request.setAttribute("flag", flag);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}

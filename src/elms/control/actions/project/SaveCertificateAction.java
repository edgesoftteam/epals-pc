package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.app.project.Certificate;
import elms.control.beans.CertificateForm;

public class SaveCertificateAction extends Action {
	static Logger logger = Logger.getLogger(SaveCertificateAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		CertificateForm certificateForm = (CertificateForm) form;
		String nextPage = "success";
		try {

			ActionErrors errors = new ActionErrors();
			Certificate certificate = null;
			logger.debug("This is before getting certificate");
			certificate = certificateForm.getCertificate();
			logger.debug("certificate got from form");

			ActivityAgent certificateAgent = new ActivityAgent();
			String actNbr = certificate.getACT_NBR();

			logger.debug("Got ActNbr" + actNbr);
			System.out.println(" Activity number" +actNbr);

			if ((actNbr == null) || (actNbr.equalsIgnoreCase(""))) {
				actNbr = "0";
				logger.debug("No ActNbr, New Ceritificate");
				// certificate = certificateAgent.getCertificateOthers(certificate, actNbr);
			}

			if (!errors.empty()) {
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}

			certificateAgent.setCertificate(certificate,actNbr);
			logger.debug("saved certificate successfully");
			request.setAttribute("certificate", certificate);
			request.setAttribute("actNBR", certificate.getACT_NBR());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}

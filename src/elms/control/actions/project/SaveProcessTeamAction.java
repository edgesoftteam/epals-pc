package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.control.beans.ProcessTeamForm;
import elms.security.User;

public class SaveProcessTeamAction extends Action {
	static Logger logger = Logger.getLogger(SaveProcessTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering SaveProcessTeamAction");

		HttpSession session = request.getSession();
		ProcessTeamForm frm = (ProcessTeamForm) session.getAttribute("processTeamForm");

		CommonAgent agent = new CommonAgent();

		User user = (User) session.getAttribute("user");
		agent.saveTeam(frm, user.getUserId());

		session.removeAttribute("processTeamForm");

		logger.info("Exiting SaveProcessTeamAction");

		// Forward control to the specified success URI
		return (mapping.findForward("success"));
	}
}

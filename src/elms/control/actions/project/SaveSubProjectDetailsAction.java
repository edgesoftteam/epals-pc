package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.Epals311Agent;
import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.finance.SubProjectFinance;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.common.Constants;
import elms.control.beans.SubProjectForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveSubProjectDetailsAction extends Action {
	static Logger logger = Logger.getLogger(SaveSubProjectDetailsAction.class.getName());
	protected SubProject subProject = null;
	protected SubProjectDetail subProjectDetail = null;
	protected SubProjectFinance subProjectFinance = null;
	protected ProjectType projectType = null;
	protected SubProjectType subProjectType = null;
	protected SubProjectSubType subProjectSubType = null;
	protected int projectId = -1;
	protected int subProjectId = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into the SaveSubProjectDetailAction");

		HttpSession session = request.getSession();

		// need to refresh the PSA tree so onloadAction is setting to
		String onloadAction = "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "'";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		SubProjectForm subProjectForm = (SubProjectForm) form;

		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);

			return (new ActionForward(mapping.getInput()));
		} else {
			this.doRequestProcess(request, subProjectForm);
		}

		return (mapping.findForward("success"));
	}

	public void doRequestProcess(HttpServletRequest request, SubProjectForm subProjectForm) {
		try {
			HttpSession session = request.getSession();

			// need to refresh the PSA tree so onloadAction is setting to
			String onloadAction = "parent.f_psa.location.href='/elms/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "'";
			logger.debug("Setting the refresh tree flag to " + onloadAction);
			session.setAttribute("onloadAction", onloadAction);

			subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
			logger.debug("SaveSubProjectDetailAction -- got parameter subProjectId as " + subProjectId);

			// setting the subProject Details
			subProjectDetail = new SubProjectDetail();
			projectType = new ProjectType();
			projectType.setProjectTypeId(StringUtils.s2i(subProjectForm.getSubProject()));
			logger.debug("SaveSubProjectDetailAction -- set   projectType -- projectTypeId   as   " + projectType.getProjectTypeId());
			subProjectType = new SubProjectType();
			subProjectType.setSubProjectTypeId(StringUtils.s2i(subProjectForm.getType()));
			logger.debug("SaveSubProjectDetailAction -- set  subProjectType -- subProjectTypeId   as   " + subProjectType.getSubProjectTypeId());
			subProjectSubType = new SubProjectSubType();
			subProjectSubType.setSubProjectSubTypeId(StringUtils.s2i(subProjectForm.getSubType()));
			logger.debug("SaveSubProjectDetailAction -- set  SubProjectSubType -- subProjectSubTypeId   as   " + subProjectSubType.getSubProjectSubTypeId());

			subProjectDetail.setProjectType(projectType);
			subProjectDetail.setSubProjectType(subProjectType);
			subProjectDetail.setSubProjectSubType(subProjectSubType);
			subProjectDetail.setDescription(subProjectForm.getDescription());
			subProjectDetail.setLabel(subProjectForm.getLabel());
			subProjectDetail.setCaseLogId(StringUtils.s2i(subProjectForm.getCaseLogId()));

			subProjectDetail.setSubProjectStatus(LookupAgent.getSubProjectStatus(StringUtils.s2i(subProjectForm.getStatus())));
			logger.debug("SaveSubprojectDetails --  set the status for sub project detail object as " + subProjectForm.getStatus());

			logger.debug("SaveSubProjectDetailAction -- set  SubProjectDetail -- Description  as " + subProjectDetail.getDescription());
			subProjectDetail.setUpdatedBy((User) session.getAttribute("user"));
			logger.debug("SaveSubProjectDetailAction -- set  SubProjectDetail -- Updated user set   as " + subProjectDetail.getUpdatedBy().toString());
			logger.debug("SaveSubProjectDetailAction -- set  SubProjectDetail  Object  " + subProjectDetail.toString());

			// Not really using the below objects...but required
			subProjectFinance = new SubProjectFinance();

			List subProjectActivityList = new ArrayList();
			List subProjectHoldList = new ArrayList();
			List subProjectConditionList = new ArrayList();
			List subProjectAttachmentList = new ArrayList();
			List subProjectCommentList = new ArrayList();

			subProject = new SubProject(subProjectId, projectId, subProjectDetail, subProjectFinance, subProjectActivityList, subProjectHoldList, subProjectConditionList, subProjectAttachmentList, subProjectCommentList);

			ProjectAgent projectAgent = new ProjectAgent();
			
			// adding the sub project to database
			subProjectId = projectAgent.saveSubProjectDetail(subProject);
			logger.debug("editSubProjectAction -- got subProject from database as  " + subProject.toString());

			String projectName = projectAgent.getProjectTypeOfSubProject(subProjectId);
			logger.debug("projectName : "+projectName);
			
			if(projectName.equalsIgnoreCase(Constants.PROJECT_NAME_LICENSE_AND_CODE)){
				String description311 = projectAgent.get311SubProjectTypeName(subProject.getSubProjectDetail().getSubProjectType().getSubProjectTypeId());
				Epals311Agent.updateStructureTypeIn311System(description311,subProjectId);
			}
			
			ViewSubProjectAction viewSubProjectAction = new ViewSubProjectAction();
			String active = "Y";
			viewSubProjectAction.doRequestProcess(request, subProjectId,active);
		} catch (Exception e) {
			logger.error("Exception in SaveSubProjectDetailAction " + e.getMessage());
		}
	}
}

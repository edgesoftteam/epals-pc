package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.project.SubProject;
import elms.util.StringUtils;

public class DeleteActivityAction extends Action {

	static Logger logger = Logger.getLogger(DeleteActivityAction.class.getName());
	protected SubProject subProject;
	protected int subProjectId = -1;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		try {
			// Getting required values from the request Object
			int activityId = StringUtils.s2i(request.getParameter("activityId"));
			logger.debug("Obtained Activty id from request as " + activityId);
			subProjectId = new ActivityAgent().deleteActivity(activityId);
			logger.info("optained sub project Id back as " + subProjectId);
			// Cleaning the request Object
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			// refresh PSA Tree
			session.setAttribute("refreshPSATree", "Y");

			ViewSubProjectAction viewSubProjectAction = new ViewSubProjectAction();
			String active = "Y";
			viewSubProjectAction.doRequestProcess(request, subProjectId,active);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		logger.info("Forwarding the delete Sub Project Action to view project jsp");
		return (mapping.findForward("success"));

	}

}
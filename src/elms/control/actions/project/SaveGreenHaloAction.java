package elms.control.actions.project;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import elms.agent.ActivityAgent;
import elms.app.project.GreenHalo;
import elms.app.project.GreenHaloNewProjectResponse;
import elms.app.project.GreenHaloPermitResponse;
import elms.common.Constants;
import elms.control.beans.GreenHaloForm;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class SaveGreenHaloAction extends Action {
	static Logger logger = Logger.getLogger(SaveGreenHaloAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		GreenHaloForm greenHaloForm = (GreenHaloForm) form;
		String nextPage = "success";
		ActionErrors errors = new ActionErrors();
		try {

			GreenHalo greenHalo = null;
			logger.debug("This is before getting greenHalo");
			greenHalo = greenHaloForm.getGreenHalo();
			logger.debug("GreenHalo got from form");

			ActivityAgent activityAgent = new ActivityAgent();
			Integer actId = greenHalo.getActId();
			
			if(actId == 0 || actId == null){
				String actID = (String) request.getParameter("actId");
				// String actNBR = (String) request.getParameter("actNBR");

				if (actID == null)
					actID = (String) session.getAttribute(Constants.PSA_ID);
				logger.debug("Got actId from request " + actId);
				actId = Integer.parseInt(actID);
			}

			logger.debug("Got actId" + actId);
			System.out.println(" Activity ID: " +actId);

			if ((actId == null) || (actId==0)) {
				actId = 0;
				logger.debug("No ActID, Nothing to do here.");
			}

			if (!errors.empty()) {
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}
			
			greenHalo.setActId(actId);
			activityAgent.setGreenHalo(greenHalo);
			logger.debug("saved GreenHalo successfully");
			request.setAttribute("greenHalo", greenHalo);
			request.setAttribute("id", greenHalo.getId());
			request.setAttribute("act_id", greenHalo.getActId());
			request.setAttribute("greenHaloSaveStatus", "GreenHalo save/updated successfully");
		} catch (Exception e) {
			logger.error(e.getMessage());
			nextPage="error";
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.green.halo.save/update", "Unable to save/update Green Halo :: exception "+ e.getMessage()   ));
		}

		return (mapping.findForward(nextPage));
	}
}

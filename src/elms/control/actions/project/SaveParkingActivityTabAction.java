package elms.control.actions.project;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AddressAgent;
import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.CustomField;
import elms.app.admin.SubProjectType;
import elms.common.Constants;
import elms.control.beans.ActivityForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveParkingActivityTabAction extends Action {

	static Logger logger = Logger.getLogger(SaveParkingActivityTabAction.class.getName());
	
	public static final int PW_CITYWIDE_STREET_ID = 333;
	public static String LNCV_ID = "255002";//hard coded per db changes.txt

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		ActivityForm activityForm = (ActivityForm) form;
		ActionErrors errors = new ActionErrors();
		int subProjectTypeId = -1;
		
	  

		try {

			User user = (User) session.getAttribute("user");
			logger.debug("Logged in user details: id = " + user.getUserId() + " Name = " + user.getFullName());
			
			String lncvYN = (String) (request.getParameter("lncv")!=null? request.getParameter("lncv"):"N");
			String actId = (String) (request.getParameter("actId")!=null? request.getParameter("actId"):"");
				
				
			

			String streetNumber = null;
			String streetNameId = null;

			String subProjectName = activityForm.getSubProjectName().trim();
			logger.debug("subProjectName=" + subProjectName);
			if ((subProjectName == null) || subProjectName.equalsIgnoreCase("-1")) {
				logger.debug("project type required");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.subProjectName.required"));
			}

			String activityType = activityForm.getActivityType();
			logger.debug("Activity type is " + activityType);
			if ((activityType == null || activityType.equalsIgnoreCase("-1"))) {
				logger.debug("activityType required");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activityType.required"));
			}

			// validate address
			int lsoId = -1;
			int addressId = -1;
			
			try {

				if (!(subProjectName.equalsIgnoreCase(LNCV_ID))) {// LNCV

					// check for validation errors(address, sub project name, sub project type, applied date, description)
					streetNumber = activityForm.getStreetNumber();
					if ((streetNumber == null) || streetNumber.equalsIgnoreCase("")) {
						logger.debug("street number required");
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetNumber.required"));
					}
					streetNameId = activityForm.getStreetName();
					if ((streetNameId == null) || streetNameId.equalsIgnoreCase("-1")) {
						logger.debug("street name required");
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.streetName.required"));
					}

					int streetNumberInt = Integer.parseInt(streetNumber.trim());
					int streetNameIdInt = Integer.parseInt(streetNameId);
					lsoId = new AddressAgent().getLsoId(streetNumberInt, streetNameIdInt);
					logger.debug("LSO Id = " + lsoId);
					addressId = AddressAgent.getLandAddressId(streetNumberInt, streetNameIdInt);

					logger.debug("Address Id = " + addressId);
					if (lsoId == -1 || addressId == -1) {
						if (errors.empty()) {
							errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.mismatch"));
						}
					}

				}

			} catch (Exception e) {
				logger.debug("Address not found " + e.getMessage());
				if (errors.empty()) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.mismatch"));
				}
			}

			// if errors exist, then forward to the input page.
			if (!errors.empty()) {
				logger.debug("Errors exist, going back to the input page");
				saveErrors(request, errors);
				request.setAttribute("hasErrors", "hasErrors");
				return (new ActionForward(mapping.getInput()));
			}

			logger.debug("no errors, inserting data..");

			// get non location id for PW city wide address (hard coded)
			int nonLocationalLsoId = new AddressAgent().getLsoId(1, PW_CITYWIDE_STREET_ID);
			if (subProjectName.equalsIgnoreCase(LNCV_ID)) {
				lsoId = nonLocationalLsoId;
				addressId = AddressAgent.getLandAddressId(1, PW_CITYWIDE_STREET_ID);
				logger.debug("Address Id = " + addressId);
				logger.debug("Non locational address ");
			}
			int activityId =0;
			// check lncv activityUpdate else normal process 
			if(actId.equals("")){
			// check and create project
			ProjectAgent projectAgent = new ProjectAgent();
			int projectId = projectAgent.getProjectId(lsoId, Constants.PROJECT_NAME_PARKING);
			if (projectId == -1) {
				// project does not exist, create a new building project and proceed
				projectId = projectAgent.createProject(Constants.PROJECT_NAME_PARKING, Constants.DEPARTMENT_PARKING, StringUtils.i2s(lsoId), user.getUserId());
				logger.debug("New project created with project Id = " + projectId);
			}

			List<SubProjectType> subProjectTypes = LookupAgent.getSubProjectTypes(Integer.parseInt(subProjectName));
			if (subProjectTypes.size() == 1) {
				logger.debug("Only 1 Sub-Project type was obtained, automatically populating Activity Types");
				subProjectTypeId = ((SubProjectType) subProjectTypes.get(0)).getSubProjectTypeId();
				logger.debug("Sub Project Type Id = " + subProjectTypeId);

			}

			// check and create sub-project
			ProjectAgent subProjectAgent = new ProjectAgent();
			int subProjectId = subProjectAgent.getSubProjectId(projectId, StringUtils.s2i(subProjectName));
			if (subProjectId == -1) {
				// sub-project does not exist, create a new sub-project and proceed
				subProjectId = subProjectAgent.createSubProject(projectId, subProjectName, "" + subProjectTypeId, user.getUserId());
				logger.debug("New sub-project created with sub-project Id = " + subProjectId);
			}

			// create activity
			ActivityAgent activityAgent = new ActivityAgent();

			Calendar cal = Calendar.getInstance();
			activityForm.setIssueDate(StringUtils.cal2str(cal));
			activityForm.setStartDate(StringUtils.cal2str(cal));

			String description = " ";

			activityId = activityAgent.createParkingActivity(subProjectId, addressId, activityForm.getActivityType(), description, StringUtils.$2dbl(activityForm.getValuation()), StringUtils.b2s(activityForm.getPlanCheckRequired()), activityForm.getActivityStatus(), activityForm.getStartDate(), activityForm.getExpirationDate(), user.getUserId(), activityForm.getPlanNo(), activityForm.getSubDivision(), activityForm.getIssueDate(), "1",activityForm.getParkingZone(),activityForm.getLabel());
			logger.debug("New Parking Activity created with activity id = " + activityId);
			
			// insert lncv 
			if(lncvYN.equals("Y")){
				String email = (String) (request.getParameter("email")!=null? request.getParameter("email"):"N");
				String dlno = (String) (request.getParameter("dlno")!=null? request.getParameter("dlno"):"N");
				String phno = (String) (request.getParameter("phno")!=null? request.getParameter("phno"):"N");
				String fname = (String) (request.getParameter("fname")!=null? request.getParameter("fname"):"N");
				String lname = (String) (request.getParameter("email")!=null? request.getParameter("lname"):"N");
				int spcount = Integer.parseInt((String) (request.getParameter("spcount")!=null? request.getParameter("spcount"):"0"));
				int peopleId =0;
				if(!email.equalsIgnoreCase("")){
					peopleId= activityAgent.insertPeopleForParkingActivity(activityId,email,fname,lname,dlno,phno,activityForm.getLabel());
				}
				
				if(spcount!=0){
					int pblock = new ActivityAgent().getPBlock(activityId);
					int lncvCount = 0;
					for(int i=1;i<= spcount;i++){
						logger.debug((String) (request.getParameter("lncvDate"+i+"")));
						String lncvdt= (String) (request.getParameter("lncvDate"+i+""))!=null? (request.getParameter("lncvDate"+i+"")):"";
						if(!lncvdt.equalsIgnoreCase("")){
							activityAgent.insertPkActivity(activityId,lncvdt,pblock);
							lncvCount++;
						}
					}
					
					int paymentAmt = 6;
					if(lncvCount >0){
						int k = lncvCount % 3 ;
						if(k==1){
							lncvCount = lncvCount + 2;
							k = lncvCount % 3;
						}
						if(k==2){
							lncvCount = lncvCount + 1;
							k = lncvCount % 3;
						}
						if(k==0){
							lncvCount = lncvCount / 3;
							paymentAmt = paymentAmt * lncvCount;
						
						}
						int feeId = new ActivityAgent().getFeeIdForLncv(activityForm.getActivityType());
						new ActivityAgent().insertActivity_fee(activityId,peopleId,feeId,lncvCount,"$"+StringUtils.i2s(paymentAmt),"N");
					}	
					

				}
			
			
			}
			
			}// end normal process
			else { //begin updte lncv panel
				activityId = StringUtils.s2i(actId);
				int spcount = Integer.parseInt((String) (request.getParameter("spcount")!=null? request.getParameter("spcount"):"0"));
				if(spcount!=0){
					int pblock = new ActivityAgent().getPBlock(activityId);
					int lncvCount =0;
					for(int i=1;i<= spcount;i++){
						logger.debug((String) (request.getParameter("lncvDate"+i+"")));
						String lncvdt= (String) (request.getParameter("lncvDate"+i+""))!=null? (request.getParameter("lncvDate"+i+"")):"";
						if(!lncvdt.equalsIgnoreCase("")){
							new ActivityAgent().insertPkActivity(activityId,lncvdt,pblock);
							lncvCount++;
						}
					}
					
					int feeUnit = new ActivityAgent().getFeeUnits(activityId);
					logger.debug("****feeUnit*****"+feeUnit);
					
					
					int unit = lncvCount;
					
					if(feeUnit == 96){
						//don't add payment has it is already paid
					}else {
						int paymentAmt = 6;
						if(lncvCount >0){
							int k = lncvCount % 3 ;
							if(k==1){
								lncvCount = lncvCount + 2;
								k = lncvCount % 3;
							}
							if(k==2){
								lncvCount = lncvCount + 1;
								k = lncvCount % 3;
							}
							if(k==0){
								lncvCount = lncvCount / 3;
								paymentAmt = paymentAmt * lncvCount;
							
							}
							new ActivityAgent().updateActivity_fee(StringUtils.s2i(actId),unit,"$"+StringUtils.i2s(paymentAmt));
						}	
					
					}
					
				}
			} //end update lncv panel
			
			
			// insert Custom fields
			// Custom fields start

			if (activityForm.getCustomFieldList().length > 0) {
				logger.debug("-----------------Entered Custom fields************************** ");
				CustomField[] customFieldList = activityForm.getCustomFieldList();
				new ActivityAgent().saveCustomActvityFields(activityId, customFieldList);
			}
			// end custom fields

			// redirect back to the view Activity page
			session.setAttribute("refreshTree", "Y");
			session.removeAttribute("siteStructureForm");
			session.removeAttribute("siteUnitForm");

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			session.setAttribute(Constants.PSA_ID, StringUtils.i2s(activityId));
			session.setAttribute(Constants.PSA_TYPE, "A");
			session.setAttribute(Constants.LSO_ID, StringUtils.i2s(lsoId));

			request.setAttribute("activityId", StringUtils.i2s(activityId));

			ActivityAgent actAgent = new ActivityAgent();
			String lsoAddress = actAgent.getActivityAddressForId(activityId);
			String psaInfo = "";
			session.setAttribute("psaInfo", psaInfo);
			session.setAttribute("lsoAddress", lsoAddress);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				}
			} catch (Exception e) {
				e.getMessage();
			}
			
			return (mapping.findForward("success"));

		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException(e.getMessage());
		}

	}

}

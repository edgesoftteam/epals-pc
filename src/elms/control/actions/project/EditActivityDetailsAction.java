package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.PlanCheckAgent;
import elms.app.admin.ActivityType;
import elms.app.admin.CustomField;
import elms.app.admin.ParkingZone;
import elms.app.common.DropdownValue;
import elms.app.finance.ActivityFinance;
import elms.app.project.Activity;
import elms.app.project.ActivityAttributes;
import elms.app.project.ActivityDetail;
import elms.control.beans.ActivityForm;
import elms.util.StringUtils;

public class EditActivityDetailsAction extends Action {
	static Logger logger = Logger.getLogger(EditActivityDetailsAction.class.getName());
	protected int activityId;
	protected String aType;
	protected String nextPage = "common";
	protected Activity activity;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		activityId = StringUtils.s2i(request.getParameter("activityId"));
		logger.debug("Entered Into EditActivityDetailsAction with activityId .. " + activityId);
		aType = request.getParameter("type");
		logger.debug("type   ::::  "+aType);

		if (aType == null) {
			aType = "";
		}

		ActivityAgent activityAgent = new ActivityAgent();

		ActivityForm activityForm = null;

		try {
			activity = activityAgent.getActivity(activityId);
			activityForm = requestProcess(activity, request);

			if (activity == null) {
				activity = new Activity();
			}

			nextPage = "common";
			//Setting activity attributes counts
            ActivityAttributes attribs = new PlanCheckAgent().getPlanCheckCounts(activity.getActivityId());
            request.setAttribute("activityAttributes", attribs);
            
			activityForm.setDisplayContent("EDIT");
		} catch (Exception e) {
			logger.error("NoSuchMethodException " + e.getMessage());
		}

		request.setAttribute("activityForm", activityForm);
		session.setAttribute("activityForm", activityForm);
		logger.debug("The next page : " + nextPage);

		return (mapping.findForward(nextPage));
	}

	public ActivityForm requestProcess(Activity activity, HttpServletRequest request) {
		logger.debug("Entered Into EditActivityDetailsAction request process");

		int subProjectId = activity.getSubProjectId();
		logger.debug("..Sub ProjectId " + activity.getSubProjectId());

		String sprojName = "";

		int projectNameId = -1;

		try {
			projectNameId = LookupAgent.getProjectNameId("Q", subProjectId);
		} catch (Exception e5) {
			logger.error("Exception occured while getting project name id " + e5.getMessage());
		}

		logger.debug("Obtained project name id is" + projectNameId);

		request.setAttribute("projectNameId", "" + projectNameId);

		ActivityDetail activityDetail = activity.getActivityDetail();
		ActivityFinance activityFinance = activity.getActivityFinance();
		ActivityForm activityForm = new ActivityForm();
		activityForm.setActivityId(StringUtils.i2s(activity.getActivityId()));
		activityForm.setSubProjectId(StringUtils.i2s(subProjectId));

		if (activityDetail == null) {
			activityDetail = new ActivityDetail();
		} else {
			request.setAttribute("levelId", StringUtils.i2s(activity.getActivityId()));
			request.setAttribute("level", "A");
			logger.debug("EditActivityDetailsAction -- Parameter activityId is set to Request as  " + request.getAttribute("activityId"));
			activityForm.setActivityNumber(activity.getActivityDetail().getActivityNumber());
			logger.debug("EditActivityDetailsAction -- Parameter activityNumber is set to Request as  " + activityForm.getActivityNumber());
			activityForm.setAddress(StringUtils.i2s(activityDetail.getAddressId()));
			logger.debug("EditActivityDetailsAction -- Issue Date is set to activityForm as  " + activityForm.getIssueDate());
			activityForm.setIssueDate(StringUtils.cal2str(activityDetail.getIssueDate()));
			logger.debug("EditActivityDetailsAction -- Issue Date is set to activityForm as  " + activityForm.getIssueDate());

			if (activityDetail.getActivityType() != null) {
				activityForm.setActivityTypeDesc(activityDetail.getActivityType().getDescription());
				logger.debug("EditActivityDetailsAction -- activityTypeDesc is set to request  as  " + activityForm.getActivityTypeDesc());
				activityDetail.setFinalDateLabel(aType);
				activityForm.setCompletionDateLabel(activityDetail.getFinalDateLabel());
				logger.debug("Completion Date Label is set to request  as  " + activityForm.getCompletionDateLabel());
				activityDetail.setExpirationDateLabel(aType);
				activityForm.setExpirationDateLabel(activityDetail.getExpirationDateLabel());
				logger.debug("Expiration Date Label is set to request  as  " + activityForm.getExpirationDateLabel());
			}

			if (activityDetail.getActivityType() != null) {
				activityForm.setActivityType(activityDetail.getActivityType().getType());
				
			}

			List activitySubTypes;

			try {
				activitySubTypes = LookupAgent.getActivitySubTypes(aType);

			} catch (Exception e1) {
				activitySubTypes = new ArrayList();
				logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
			}

			request.setAttribute("activitySubTypesList", activitySubTypes);
			
			logger.debug("EditActivityDetailsAction -- activityType is set to activity Form   as  " + request.getAttribute("activityType"));

			if (activityDetail.getActivitySubTypes() != null) {
				
				//Get act sub type id's
				String ids = "";
				try {
					ids = new ActivityAgent().getMultipleActivitySubTypes(activityId);
				} catch (Exception e) {}
				
				List activitySubTypesValues = new ArrayList();

				String idsArr [] = ids.replace("(","").replace(")","").split(",");
				
				for(int id=0;id<idsArr.length;id++)
					activitySubTypesValues.add(idsArr[id]);

				activityDetail.setActivitySubTypes(activitySubTypesValues);
				
				activityForm.setActivitySubTypes((String[]) activityDetail.getActivitySubTypes().toArray(new String[activityDetail.getActivitySubTypes().size()]));
				request.setAttribute("activitySubTypes", activityDetail.getActivitySubTypes());
			}

			if (activityDetail.getStatus() != null) {
				activityForm.setActivityStatus(StringUtils.i2s(activityDetail.getStatus().getStatus()));
				activityForm.setOldActivityStatus(StringUtils.i2s(activityDetail.getStatus().getStatus()));
			}

			logger.debug("EditActivityDetailsAction -- activity Status is set to activityForm as  " + activityForm.getActivityStatus());

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, 6);
			activityForm.setSixMonthsLaterDate(StringUtils.cal2str(cal));
			logger.debug("EditActivityDetailsAction -- SixMonthsLaterDate is set to activityForm as  " + activityForm.getSixMonthsLaterDate());
			activityForm.setCompletionDate(StringUtils.cal2str(activityDetail.getFinaledDate()));
			logger.debug("EditActivityDetailsAction -- Completion Date is set to activityForm as  " + activityForm.getCompletionDate());
			activityForm.setExpirationDate(StringUtils.cal2str(activityDetail.getExpirationDate()));
			logger.debug("EditActivityDetailsAction -- Expiration Date is set to activityForm as  " + activityForm.getExpirationDate());

			activityForm.setOldIssueDate(StringUtils.cal2str(activityDetail.getIssueDate()));
			logger.debug("EditActivityDetailsAction -- Old Issue Date is set to activityForm as " + activityForm.getOldIssueDate());
			logger.debug("after adding oldissuedate");

			Calendar issue6Months = activityDetail.getIssueDate();

			if (issue6Months == null) {
				issue6Months = Calendar.getInstance();
			}

			issue6Months.add(Calendar.MONTH, 6);

			String strIssue6Months = StringUtils.cal2str(issue6Months);

			if (strIssue6Months == null) {
				strIssue6Months = "";
			}

			activityForm.setSixMonthsOfIssueDate(strIssue6Months);
			logger.debug("EditActivityDetailsAction -- Old Issue Date is set to activityForm as " + activityForm.getSixMonthsOfIssueDate());
			activityForm.setOldExpirationDate(StringUtils.cal2str(activityDetail.getExpirationDate()));
			logger.debug("EditActivityDetailsAction -- Old Expiration Date is set to activityForm as  " + activityForm.getOldExpirationDate());
			activityForm.setStartDate(StringUtils.cal2str(activityDetail.getStartDate()));
			logger.debug("EditActivityDetailsAction -- Start Date is set to activityForm as  " + activityForm.getStartDate());
			activityForm.setValuation(StringUtils.d2s(activityDetail.getValuation()));
			logger.debug("EditActivityDetailsAction -- Valuation is set to activityForm as  " + activityForm.getValuation());
			activityForm.setDescription(activityDetail.getDescription());
			logger.debug("EditActivityDetailsAction -- Description  is set to activityForm as  " + activityForm.getDescription());
			activityForm.setLabel(activityDetail.getLabel());
			logger.debug("EditActivityDetailsAction -- Label  is set to activityForm as  " + activityForm.getLabel());
			activityForm.setParkingZone(activityDetail.getParkingZone() != null ? activityDetail.getParkingZone().getPzoneId() : "");
			logger.debug("EditActivityDetailsAction -- Parking zone  is set to activityForm as  " + activityDetail.getParkingZone() != null ? activityDetail.getParkingZone().getPzoneId() : "");
			activityForm.setPlanCheckRequired(StringUtils.s2b(activityDetail.getPlanCheckRequired()));
			logger.debug("EditActivityDetailsAction -- PlanCheckRequired  is set to activityForm as  " + activityForm.getPlanCheckRequired());
			activityForm.setDevelopmentFeeRequired(StringUtils.s2b(activityDetail.getDevelopmentFeeRequired()));
			logger.debug("EditActivityDetailsAction -- dev fee reqd  is set to activityForm as  " + activityForm.getDevelopmentFeeRequired());
			activityForm.setMicrofilm(activityDetail.getMicrofilm());
			logger.debug("EditActivityDetailsAction -- Micrifilm  is set to activityForm as  " + activityForm.getMicrofilm());

			activityForm.setNewUnits(activityDetail.getNewUnits());
			logger.debug("EditActivityDetailsAction -- newUnits: " + activityDetail.getNewUnits());
			activityForm.setExistingsUnits(activityDetail.getExistingUnits());
			logger.debug("EditActivityDetailsAction -- existingUnits: " + activityDetail.getExistingUnits());
			activityForm.setDemolishedUnits(activityDetail.getDemolishedUnits());
			logger.debug("EditActivityDetailsAction -- DemolishedUnits: " + activityDetail.getDemolishedUnits());
			activityForm.setGreenHalo(activityDetail.getGreenHalo());
			logger.debug("EditActivityDetailsAction -- GreenHalo  is set to activityForm as  " + activityForm.getGreenHalo());

			activityForm.setExtRefNumber(activityDetail.getExtRefNumber());
			logger.debug("EditActivityDetailsAction -- ExtRefNumber  is set to activityForm as  " + activityForm.getExtRefNumber());
			
			List<ParkingZone> parkingZones = new ArrayList<ParkingZone>();

			try {
				logger.debug("***********EDIT CUSTOM FIELDS*************************  ");
				

				int actId = activity.getActivityId();
				ActivityAgent customLabelsAgent = new ActivityAgent();
				ActivityType activityType=new ActivityType();
				
				logger.debug("getTypeId    :::::::     "+ activityDetail.getActivityType().getTypeId()+"  leveltypeid  ::::   "+activityType.getTypeId());
				

				CustomField[] customFieldsArray = customLabelsAgent.getEditCustomFieldArray(actId,"ACTIVITY",activityDetail.getActivityType().getTypeId());
				
				if(customFieldsArray != null && customFieldsArray.length >0){
					
					for(int i=0; i< customFieldsArray.length; i++){
						CustomField cf = customFieldsArray[i];
						logger.debug("field Type::"+cf.getFieldType());
						if(cf.getFieldType() != null && cf.getFieldType().equalsIgnoreCase("DROPDOWN")){
							List<DropdownValue> optionList = customLabelsAgent.getOptionsAsList(cf.getFieldId());
							request.setAttribute("dropdownOptionList"+cf.getFieldId(), optionList);
						}else if(cf.getFieldType() != null && cf.getFieldType().equalsIgnoreCase("CHECKBOX")){
							if(cf.getFieldValue()!= null && cf.getFieldValue().equalsIgnoreCase("Y")){
								cf.setFieldValue("on");
							}							
						}
					}
				}
				activityForm.setCustomFieldList(customFieldsArray);// added for customFields
			

				RowSet rs = new ActivityAgent().getEditCustomFieldList(activity.getActivityId(), new ActivityAgent().getActivityTypeId(activityDetail.getActivityType().getType()));
				activityForm.setCustomFieldList(rs, "edit");// added for customFields
				

				logger.debug("***********************************************  " + activityForm.getCustomFieldList().length);
				parkingZones = LookupAgent.getParkingZones();
				request.setAttribute("parkingZones", parkingZones);

			} catch (Exception e) {
				logger.error("Exception in EditActivityDetailsAction::",e);
			}
		}

		if (activityFinance == null) {
			activityFinance = new ActivityFinance();
		} else {
			if (activityFinance.getAmountDue() <= 0) {
				activityForm.setOutStandingFees("N");
			} else {
				activityForm.setOutStandingFees("Y");
			}

			logger.debug("OutStanding fees is set to activityForm as  " + activityForm.getOutStandingFees());
		}

		List addresses;

		try {
			addresses = new ActivityAgent().getActivityAddresses(StringUtils.i2s(subProjectId));
		} catch (Exception e3) {
			addresses = new ArrayList();
			logger.warn("Could not get list of getActivityAddresses, initializing it to blank arraylist");
		}

		request.setAttribute("addresses", addresses);

		List activityStatuses;

		try {

			if (subProjectId > 0) {
				sprojName = LookupAgent.getSprojNameForSprojId(StringUtils.i2s(subProjectId));
				logger.debug("Got SubprojectName " + sprojName);
			}
			activityStatuses = LookupAgent.getActivityStatuses(LookupAgent.getProjectNameId("A", activityId), sprojName);
		} catch (Exception e) {
			activityStatuses = new ArrayList();
			logger.warn("Could not get list of activityStatuses, initializing it to blank arraylist");
		}

		request.setAttribute("activityStatuses", activityStatuses);

		List planCheckStatuses;

		try {
			planCheckStatuses = LookupAgent.getPlanCheckStatuses();
		} catch (Exception e1) {
			planCheckStatuses = new ArrayList();
			logger.warn("Could not get list of planCheckStatuses, initializing it to blank arraylist");
		}

		request.setAttribute("planCheckStatuses", planCheckStatuses);

		// setting the list of micro film satauses
		List microfilmStatuses;

		try {
			microfilmStatuses = LookupAgent.getMicrofilmStatuses();
		} catch (Exception e2) {
			microfilmStatuses = new ArrayList();
			logger.warn("Could not get list of microfilmStatuses, initializing it to blank arraylist");
		}

		request.setAttribute("microfilmStatuses", microfilmStatuses);

		return activityForm;
	}
}

package elms.control.actions.project;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import com.fasterxml.jackson.databind.ObjectMapper;

import elms.agent.ActivityAgent;
import elms.app.project.GreenHalo;
import elms.app.project.GreenHaloNewProjectResponse;
import elms.common.Constants;
import elms.util.HTTPClientUtils;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class SubmitGreenHaloAction extends Action {
	
	static Logger logger = Logger.getLogger(SubmitGreenHaloAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		//GreenHaloForm greenHaloForm = (GreenHaloForm) form;
		String nextPage = "";
		try {
			
			Integer actId = 0;
			if(actId == 0 || actId == null){
				String actID = (String) request.getParameter("actId");

				if (actID == null)
					actID = (String) session.getAttribute(Constants.PSA_ID);
				logger.debug("Got actId from request " + actId);
				actId = Integer.parseInt(actID);
				request.setAttribute("activityId", actID);
			}
			logger.debug("Got actId" + actId);
			System.out.println(" Activity ID: " +actId);
			
			ActionErrors errors = new ActionErrors();
			
			if ((actId == null) || (actId==0)) {
				actId = 0;
				logger.debug("No ActID, Nothing to do here.");
			}

			if (!errors.empty()) {
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}
			ActivityAgent activityAgent = new ActivityAgent();
			GreenHaloNewProjectResponse   projectResponse = activityAgent.getGreenHaloProjectResponse(actId);
			if(projectResponse == null || projectResponse.getStatus()== null || projectResponse.getStatus().isEmpty() || projectResponse.getStatus().equalsIgnoreCase("fail") ){
				session.setAttribute("greenHaloButton", "Create Green Halo Project");
				//Calling create New Project API
				createNewProject(actId, errors);
				if(errors.empty()){
					session.setAttribute("greenHaloButton", "");
					nextPage = "success";
				}
			}else{
				session.setAttribute("greenHaloButton", "");
			}
			
			if(errors.get(ActionErrors.GLOBAL_ERROR) != null){
				nextPage = "fail";
				if (!errors.empty()) {
					saveErrors(request, errors);
					session.setAttribute("error", errors);
					return (new ActionForward(mapping.getInput()));
				}
			}
			nextPage = "success";
			logger.debug("GreenHalo send to API successfully");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
	private void createNewProject(int actId, ActionErrors errors) throws Exception{
		ActivityAgent activityAgent = new ActivityAgent();
		GreenHalo greenHalo = activityAgent.getGreenHalo(actId);
		 ResourceBundle properties = HTTPClientUtils.getResourceBundle();
		greenHalo.setApi_key(properties.getString("green.halo.api.key"));
		logger.debug("Green Halo toString() ===================== > "+ greenHalo.addProject());
		MediaType mediaType = MediaType.parse("application/json");
		RequestBody requestBody = RequestBody.create(mediaType,greenHalo.addProject());
		String url = "https://dev.greenhalosystems.com/?func=api/contact";
		Map<String, String> headers = new HashMap<String, String>();
		String projectJsonString= HTTPClientUtils.post(url, headers, requestBody);
		logger.debug("API response ========================> "+   projectJsonString);
		ObjectMapper mapper = new ObjectMapper();
		GreenHaloNewProjectResponse project = mapper.readValue(projectJsonString, GreenHaloNewProjectResponse.class);
		activityAgent.setGreenHaloProject(project, actId);
		if(project.getStatus().equalsIgnoreCase("fail")){
			logger.debug("GreenHalo create project API :: issue ::" + project.getNote());
			logger.debug("GreenHalo :: skiping call to permit creation API.");
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.green.halo.api", "createNewProject API :: "+project.getNote()));
		}
	}
}

package elms.control.actions.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import elms.agent.LookupAgent;
import elms.agent.PlanCheckAgent;
import elms.app.admin.EngineerUser;
import elms.app.common.Module;
import elms.app.project.PlanCheck;
import elms.app.project.PlanCheckEngineer;
import elms.common.Constants;
import elms.control.beans.online.EmailTemplateAdminForm;
import elms.control.beans.project.PlanCheckReassignForm;
import elms.exception.AgentException;
import elms.gsearch.GlobalSearch;
import elms.security.Group;
import elms.security.User;
import elms.util.ExchangeEmail;
import elms.util.StringUtils;
import elms.util.Util;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ViewAllPlanCheckAction extends Action{
	
    /**
     * The Logger
     */
    static Logger logger = Logger.getLogger(ViewAllPlanCheckAction.class.getName());
    
    public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
    	String nextPage = "success";
    	PlanCheckReassignForm planCheckReassignForm = (PlanCheckReassignForm) form;
    	HttpSession session = request.getSession();
    	User user =(User) session.getAttribute(Constants.USER_KEY);
    	logger.debug("action... "+request.getParameter("action") );
    	String action= request.getParameter("action");
    	try {
    		if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("getUsers")){
    			String deptDesc = request.getParameter("deptDesc");
    			logger.debug("deptDesc.. "+deptDesc);
    			String engineerUsers = getEngineerUsers(deptDesc);
    			PrintWriter pw = response.getWriter();
                pw.write(engineerUsers);
                return null;
    		}
    		else if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("reAssign")){
    			   
    			   PlanCheckEngineer[] pcEngineerArray = planCheckReassignForm.getPcEngineerArray();
    			   List<PlanCheck> pcListToUpdate = new ArrayList<PlanCheck>();
    			   
    			   if(pcEngineerArray != null && pcEngineerArray.length>0){
    				   
    				   for(int i=0;i<pcEngineerArray.length;i++){
    					   PlanCheckEngineer pcEngineer = pcEngineerArray[i];
    					   PlanCheck[] pcArray = pcEngineer.getPlanCheckArray();
    					   
    					   if(pcArray != null && pcArray.length>0){
    						   for(int j=0;j<pcArray.length;j++){
    							   PlanCheck pc = pcArray[j];
    							   logger.debug(pc.getPlanCheckId() +"::"+pc.getCheck());
    							   if(pc.getCheck() != null && pc.getCheck().equalsIgnoreCase("true")){
    								   pcListToUpdate.add(pc);
    								   
    							   }
    						   }
    					   }
    				   
    				   
    				   }
    			   }
    			   
    			   logger.debug("dept::"+planCheckReassignForm.getDeptDesc());
    			   //getdepartmentid based on desc
    			   
    			   if(pcListToUpdate.size()>0 && planCheckReassignForm.getEngineerId() != -1){
    				   new PlanCheckAgent().reAssignPLanCheck(pcListToUpdate, planCheckReassignForm.getEngineerId(),LookupAgent.getDepartment(planCheckReassignForm.getDeptDesc()).getDepartmentId());
    				   for (int i = 0; i < pcListToUpdate.size(); i++) {

    						PlanCheck pc = pcListToUpdate.get(i);
    						EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
    						emailTemplateAdminForm = new LookupAgent().getEmailTempByTempTypeId(Constants.PLANCHECK_REASSIGNED);
    						
    						ExchangeEmail msg = new ExchangeEmail();
    						if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")){
    					    	msg.setRecipient(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
    					    	msg.setSubject(Constants.CONTEXT_PATH+emailTemplateAdminForm.getEmailSubject());
    					    }
    						else{
    						msg.setRecipient(pc.getEngineerMailId());
    						msg.setSubject(emailTemplateAdminForm.getEmailSubject());
    						}
    						msg.setContent(getEmailBody(emailTemplateAdminForm.getEmailMessage(),pc));
    						msg.deliver();
    			   }
    				   }
    		   }else if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("print")){
    			   nextPage = "print";
    		   }
    		try {
    			String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PLANCHECK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Plan check
				}
			} catch (Exception e) {
				e.getMessage();
			}
    	        String fromDate = planCheckReassignForm.getFromDate()!=null?planCheckReassignForm.getFromDate():request.getParameter("fromDate");
    	        String toDate = planCheckReassignForm.getToDate()!=null?planCheckReassignForm.getToDate():request.getParameter("toDate");
    	        
    	        if(request.getParameter("action") != null && request.getParameter("action").equalsIgnoreCase("reset")){
    	        	SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  
    	            Date date = new Date();  
    	            System.out.println(formatter.format(date));  
    	            
    	        	fromDate = formatter.format(date);
    	        	toDate = formatter.format(date);
    	        } 
    	        
    			if(action != null && action.equalsIgnoreCase("initialOne")) {
    			logger.debug("in if.. ");
    				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    				 
    				 DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
    				 Calendar cal = Calendar.getInstance();


    				 //Substract 60 days to current date.
    				 cal = Calendar.getInstance();
    				 cal.add(Calendar.DATE, -60);
    				 System.out.println(dateFormat.format(cal.getTime()));

    				 fromDate = dateFormat.format(cal.getTime());
    			}
    	        logger.debug("fromDate is " + fromDate);
    	        if(toDate==null || toDate.equals("")) {
    				 SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    				 toDate = sdf.format(new Date());
    			}
    	        
    	        logger.debug("toDate is " + toDate);
    			request.setAttribute("fromDate", fromDate);
    			request.setAttribute("toDate", toDate);

    			session.setAttribute("fromDate", fromDate);
    			session.setAttribute("toDate", toDate);
    			planCheckReassignForm.setFromDate(fromDate);
    			planCheckReassignForm.setToDate(toDate);
    			
    		   planCheckReassignForm.setPcEngineerArray(getAllPLanchecks(request));
    		   planCheckReassignForm.setEngineerId(-1);

    		   List engineerUsers = null;
			  /* if(request.getParameter("deptDesc") != null){
				   engineerUsers = PlanCheckAgent.getEngineerUsers(LookupAgent.getDepartment(request.getParameter("deptDesc")).getDepartmentId());
			   }else{*/
    		   logger.debug("user... "+user.getDepartment().getDepartmentId());
				   engineerUsers = PlanCheckAgent.getEngineerUsers(user.getDepartment().getDepartmentId());
				   planCheckReassignForm.setDeptDesc(user.getDepartment().getDescription());
			  /* }*/
    		   session.setAttribute("engineerUsers", engineerUsers);
    		   List deptList = LookupAgent.getDepartmentListForPC();
    	    	
    	       session.setAttribute("deptList", deptList);
   
               List groups = user.getGroups();
               planCheckReassignForm.setManager("N");
               Group group;
               for (int i = 0; i < groups.size(); i++) {
                   group = (Group) groups.get(i);
                   if (group.getGroupId() == Constants.GROUPS_PLAN_CHECK_MANAGER) {
                	   planCheckReassignForm.setManager("Y");
                       break;
                   }
               }
    	       
		} catch (AgentException e) {
			throw new ServletException("",e);
		} catch (Exception e) {
			logger.error("",e);
			throw new ServletException("",e);
		}
    	request.setAttribute("planCheckReassignForm", planCheckReassignForm);
    	
    	
    	return (mapping.findForward(nextPage));
    }
    

    public PlanCheckEngineer[] getAllPLanchecks(HttpServletRequest request) throws AgentException{
    	PlanCheckAgent pcAgent = new PlanCheckAgent();
    	PlanCheckEngineer[] planCheckEngineerArray = new PlanCheckEngineer[0];
    	
    	HttpSession session = request.getSession();
    	
    	String engIdFromRequest = request.getParameter("engineerId");
    	/*
    	 * This List has all the plan checks for all Engineers
    	 * */

    	String action = request.getParameter("action");
    	if(action != null && (action.equalsIgnoreCase("reAssign") || action.equalsIgnoreCase("deptChanged") || action.equalsIgnoreCase("fromEditPC") || action.equalsIgnoreCase("reset") || action.equalsIgnoreCase("refresh"))){
    		engIdFromRequest = null;
    	}
    	
    	User user = (User) session.getAttribute(Constants.USER_KEY);
    	int deptId = 0;
        int moduleId = 0;
        List moduleList = user.getModules();
        if(moduleList != null && moduleList.size()>0){
        	Module module = (Module) moduleList.get(0);
        	moduleId = module.getId();
        }
        List<PlanCheck> pcList = null;
        String deptDesc = "";        
        /*if(request.getParameter("deptDesc") != null){
        	deptDesc = request.getParameter("deptDesc");
        }else{*/
        	deptDesc = user.getDepartment().getDescription();
       /* }*/
        
        request.setAttribute("deptDesc", deptDesc);
        
        String fromDate = (String) request.getAttribute("fromDate");
        String toDate = (String) request.getAttribute("toDate");
        
        List groups = user.getGroups();
        
        Group group;
        boolean isPlanCheckManager = false;
        boolean isPlanCheckReviewEngineer = false;
        for (int i = 0; i < groups.size(); i++) {
            group = (Group) groups.get(i);
            if (group.getGroupId() == Constants.GROUPS_PLAN_REVIEW_ENGINEER) {
            	isPlanCheckReviewEngineer = true;
            	
            }
            if (group.getGroupId() == Constants.GROUPS_PLAN_CHECK_MANAGER) {
            	isPlanCheckManager = true;
            	
            }
        }
        deptId = user.getDepartment().getDepartmentId();
        if(isPlanCheckReviewEngineer && !(isPlanCheckManager)){
        	engIdFromRequest = StringUtils.i2s(user.getUserId());
        	deptId=0;
        }
        	
        
        pcList = pcAgent.getAllPlanChecks(engIdFromRequest,moduleId,deptDesc, fromDate,toDate,deptId);
        
    	/*
    	 * This List has all the plan checks for an Engineers
    	 * */
    	List<PlanCheck> pcListTemp = null;
    	
    	List engineerList = new ArrayList<Map>();
    	
    	/*
    	 * This list has all the Engineer who has plan check assigned to them
    	 * */
    	List<PlanCheckEngineer> pcEngineerList = new ArrayList<PlanCheckEngineer>();
    	
    	PlanCheck[] pcListArrayTemp = null;
    	
    	PlanCheck planCheckObj = null;
    	
    	/*
    	 * engineer id need to be initialize to a value which is not 
    	 * there in db and is not used by any one in workspace.
    	 * */
    	int engineerId = -12456;
    	PlanCheckEngineer pcEngineer = null;
    	if(pcList != null){
    		int i=0;
    		for(;i<pcList.size();i++){
    			planCheckObj = pcList.get(i);
    			if(planCheckObj.getEngineer() ==  engineerId){
    				pcListTemp.add(planCheckObj);
    			}else{
    				if(pcEngineer != null){
    					if(pcListTemp != null && pcListTemp.size()>0){
    						pcListArrayTemp = new PlanCheck[pcListTemp.size()];
    						
    						for(int j=0;j<pcListTemp.size();j++){
    							pcListArrayTemp[j] = pcListTemp.get(j);
    						}
    					}
    					pcEngineer.setPlanCheckArray(pcListArrayTemp);
    					logger.debug(pcEngineer.getUserName() +" has " + pcListTemp.size() +" planChecks.");
    					pcEngineerList.add(pcEngineer);
    					
    					Map engineerMap = new HashMap<String, String>();
    					engineerMap.put("name", pcEngineer.getUserName());
    					engineerMap.put("id", pcEngineer.getUserId());
    					engineerMap.put("count", pcListTemp.size());
    					engineerList.add(engineerMap);
    				}
    				pcListTemp = new ArrayList<PlanCheck>();
    				pcEngineer = new PlanCheckEngineer();
    				engineerId = planCheckObj.getEngineer();
    				pcEngineer.setUserId(engineerId);
    				pcEngineer.setUserName(planCheckObj.getEngineerName());
    				pcListTemp.add(planCheckObj);
    			}
    		}
    		if(i == pcList.size()){
				if(pcEngineer != null){
					if(pcListTemp != null && pcListTemp.size()>0){
						pcListArrayTemp = new PlanCheck[pcListTemp.size()];
						
						for(int j=0;j<pcListTemp.size();j++){
							pcListArrayTemp[j] = pcListTemp.get(j);
						}
					}
					pcEngineer.setPlanCheckArray(pcListArrayTemp);
					logger.debug(pcEngineer.getUserName() +" has " + pcListTemp.size() +" planChecks.");
					pcEngineerList.add(pcEngineer);
					
					Map engineerMap = new HashMap<String, String>();
					engineerMap.put("name", pcEngineer.getUserName());
					engineerMap.put("id", pcEngineer.getUserId());
					engineerMap.put("count", pcListTemp.size());
					engineerList.add(engineerMap);
				}
    		}
    		
    		logger.debug(pcEngineerList.size());
    		
    		
    		if(pcEngineerList != null && pcEngineerList.size()>0){
    			planCheckEngineerArray = new PlanCheckEngineer[pcEngineerList.size()];
    			for(i=0;i<pcEngineerList.size();i++){
    				planCheckEngineerArray[i] = pcEngineerList.get(i);
    			}
    			
    		}
    		
    	}
    	request.getSession().setAttribute("engineerList", engineerList);
    	 return planCheckEngineerArray;
    }
   

private String getEngineerUsers(String deptDesc) throws Exception{
	String engineerUsersStr = "";
	String separator1 = "#@#";
	String separator2 = "$@$";
	StringBuffer sb = new StringBuffer("");
	List pcEngineers = new PlanCheckAgent().getEngineerUsers(new LookupAgent().getDepartment(deptDesc).getDepartmentId());
	EngineerUser engineerUser = null;
	if(pcEngineers != null && pcEngineers.size()>0){
		for(int i=0;i<pcEngineers.size();i++){
			engineerUser =(EngineerUser) pcEngineers.get(i);
			sb.append(engineerUser.getUserId());
			sb.append(separator1);
			sb.append(engineerUser.getName());
			sb.append(separator2);
		}
	}
	engineerUsersStr = sb.toString();
	if(engineerUsersStr.endsWith(separator2)){
		engineerUsersStr = engineerUsersStr.substring(0,engineerUsersStr.lastIndexOf(separator2));
	}
	return engineerUsersStr;
} 
private String getEmailBody(String emailBody, PlanCheck pc) {
	logger.debug("getEmailBody :: " + emailBody);
	
	if(emailBody!=null) {
		if(emailBody.contains("##ADDRESS##")) emailBody = emailBody.replaceAll("##ADDRESS##", pc.getAddress());
		if(emailBody.contains("##ACTTYPE##")) emailBody = emailBody.replaceAll("##ACTTYPE##", pc.getActType());
		if(emailBody.contains("##PERMITNO##")) emailBody = emailBody.replaceAll("##PERMITNO##", pc.getActNbr());
	}
	emailBody = emailBody.replace("\\\"", "\"");
	logger.debug("getEmailBody :: " + emailBody);
	return emailBody;
}
}

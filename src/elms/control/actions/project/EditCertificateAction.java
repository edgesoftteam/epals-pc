package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.InspectionAgent;
import elms.app.project.Activity;
import elms.app.project.Certificate;
import elms.common.Constants;
import elms.control.beans.CertificateForm;
import elms.util.StringUtils;

public class EditCertificateAction extends Action {

	static Logger logger = Logger.getLogger(EditCertificateAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {
			// String strActId = (String) session.getAttribute(Constants.LSO_ID);
			// logger.debug(" got act Id from session "+strActId);
			String actId = (String) request.getParameter("actId");
			// String actNBR = (String) request.getParameter("actNBR");

			if (actId == null)
				actId = (String) session.getAttribute(Constants.PSA_ID);
			logger.debug("Got actId from request " + actId);

			// if (mapping.getAttribute() != null) {
			// if ("request".equals(mapping.getScope()))
			// request.removeAttribute(mapping.getAttribute());
			// else
			// session.removeAttribute(mapping.getAttribute());
			// }

			// ActionForm frm = new CertificateForm();

			String actNBR = ((Activity) new ActivityAgent().getActivity(StringUtils.s2i(actId))).getActivityDetail().getActivityNumber();

			CertificateForm certificateForm = (CertificateForm) form;

			logger.debug("Before  agent ");

			Certificate certificate = new ActivityAgent().getCertificate(actNBR, actId);

			// Certificate certificate1 = new CertificateAgent().
			logger.debug("certificate obtained from agent ");

			if (certificate == null) {
				certificate = new Certificate();
				certificate.setACT_ID(actId);
				certificate.setACT_NBR(actNBR);
			}

			certificateForm.setCertificate(certificate);
			request.setAttribute("certificateForm", form);
			logger.debug("act nbr after set form is " + certificate.getACT_NBR());
			request.setAttribute("actNBR", certificate.getACT_NBR());

			request.setAttribute("actid", actId);

			logger.debug("set certificate to form ");

			java.util.ArrayList iList = (java.util.ArrayList) InspectionAgent.getInspectorUsers(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getDepartment().getDepartmentId());

			request.setAttribute("inspectorUsers", iList);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

} // End class
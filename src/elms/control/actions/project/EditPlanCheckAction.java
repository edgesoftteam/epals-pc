package elms.control.actions.project;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.agent.PlanCheckAgent;
import elms.app.common.Module;
import elms.app.project.PlanCheck;
import elms.common.Constants;
import elms.common.Dropdown;
import elms.control.beans.PlanCheckForm;
import elms.security.Department;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.Util;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;


public class EditPlanCheckAction extends Action {
    static Logger logger = Logger.getLogger(EditPlanCheckAction.class.getName());
    protected PlanCheckForm planCheckForm;
    protected List pcStatuses;
    String strPlanCheckId = "";
    int activityId = -1;
    Calendar calendar = Calendar.getInstance();

    public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
        logger.debug("Entered Into EditPlanCheckAction ....");

        HttpSession session = request.getSession();

        try {
        	 String psaId = StringUtils.nullReplaceWithEmpty((String) request.getParameter("activityId"));

     		if (psaId.trim().equals("")) {
     			psaId = (String) session.getAttribute(Constants.PSA_ID);
     		}
     		
     		String fromWhere = request.getParameter("fromWhere");
     		request.setAttribute("fromWhere", fromWhere);
     		logger.debug("obtained psa id from request as " + psaId);

     		String psaType = StringUtils.nullReplaceWithEmpty((String) request.getParameter("type"));
     		
     		logger.debug("psaType"+psaType);

     		if (psaType.trim().equals("")) {
     			psaType = (String) session.getAttribute(Constants.PSA_TYPE);
     		}


     		String fromDate = request.getParameter("fromDate");
     		logger.debug("fromDate... "+fromDate);
     		String toDate = request.getParameter("toDate");
     		logger.debug("toDate... "+toDate);
     		request.setAttribute("fromDate", fromDate);
     		request.setAttribute("toDate", toDate);
     		logger.debug("obtained psa type from request as " + psaType);
            PlanCheckForm planCheckForm = new PlanCheckForm();
            PlanCheckAgent planCheckAgent = new PlanCheckAgent();
            
           /* if(request.getParameter("fromWhere") != null){
            	request.setAttribute("fromWhere", request.getParameter("fromWhere"));
            }
            if(request.getParameter("deptDesc") != null){
            	request.setAttribute("deptDesc", request.getParameter("deptDesc"));
            }*/
            strPlanCheckId = request.getParameter("planCheckId");
            
            session.removeAttribute("planCheckForm");

            if (strPlanCheckId == null) {
                strPlanCheckId = "-1";
            }

            planCheckForm.setPlanCheckId(strPlanCheckId);
            logger.debug("plancheck id" + strPlanCheckId);

            PlanCheck plancheck = new PlanCheck();
            plancheck = planCheckAgent.getPlanCheck(StringUtils.s2i(strPlanCheckId));

            planCheckForm.setEngineerId(StringUtils.i2s(plancheck.getEngineer()));
            logger.debug("engineer Id " + plancheck.getEngineer());

            planCheckForm.setPlanCheckStatus(StringUtils.i2s(plancheck.getStatusCode()));

            planCheckForm.setActivityId(StringUtils.i2s(plancheck.getActivityId()));
            logger.debug("activity Id is set to plancheck form as " + plancheck.getActivityId());
            
            ActivityAgent activityAgent = new ActivityAgent();
           String lsoAddress = activityAgent.getActivityAddressForId(plancheck.getActivityId());
            session.setAttribute("lsoAddress", lsoAddress);
            String psaInfo = " -- " + plancheck.getActNbr();
            psaInfo = psaInfo + " - " + plancheck.getActType();
            session.setAttribute("psaInfo", psaInfo);
			
//            String psaInfo = activityAgent.getPsaInfo(plancheck.getActivityId());
//            session.setAttribute("psaInfo", psaInfo);

            planCheckForm.setPlanCheckDate(plancheck.getDate());
            
            /*Defaut plan check will be the current date*/
//            planCheckForm.setPlanCheckDate(StringUtils.cal2str(Calendar.getInstance()));
            logger.debug("Plan Check date is set to Plan check form as " + planCheckForm.getPlanCheckDate());
            planCheckForm.setTodaysDate(StringUtils.cal2str(Calendar.getInstance()));
            
            planCheckForm.setTargetDate(plancheck.getTrgdate());
            logger.debug("Target Completion date is set to Plan check form as " + planCheckForm.getTargetDate());
            
            planCheckForm.setCompletionDate(plancheck.getCompdate());
            logger.debug("Completion date is set to Plan check form as " + planCheckForm.getCompletionDate());
            
        /*    planCheckForm.setUnit(plancheck.getUnit());
            logger.debug("Time unit is set to Plan check form as " + planCheckForm.getUnit());*/
            
            int totalMinutes=StringUtils.s2i(plancheck.getUnitMinutes());
            
            logger.debug("totalMinutes::"+totalMinutes);
            int hours=totalMinutes/60;
            logger.debug("hours"+hours);
            planCheckForm.setUnitHours(hours+"");
            
            totalMinutes=totalMinutes%60;
            logger.debug("Minutes"+totalMinutes);
            planCheckForm.setUnitMinutes(totalMinutes+"");
            
            planCheckForm.setComments(plancheck.getComments());
            logger.debug("plan check comment " + planCheckForm.getComments());
            
            planCheckForm.setProcessType(plancheck.getProcessType());
            
            planCheckForm.setCreated(StringUtils.cal2str(plancheck.getCreated()));
            
            planCheckForm.setDepartment(StringUtils.nullReplaceWithEmpty(plancheck.getDeptDesc()));
            
            planCheckForm.setPlanChecksHistory(planCheckAgent.getPlanChecksHistory(StringUtils.s2i(strPlanCheckId)));
            logger.debug("Plan Check History is set to Plan check form");
            
            LookupAgent lookupAgent = new LookupAgent();
            
            Department dept = lookupAgent.getDepartment(plancheck.getDeptDesc());
            request.setAttribute("deptDesc", plancheck.getDeptDesc());
            request.setAttribute("deptId", dept.getDepartmentId());


            List planCheckStatuses = null;
            int moduleId = LookupAgent.getModuleId(LookupAgent.getActivityTypeForActId(""+planCheckForm.getActivityId()));
            request.setAttribute("moduleId", moduleId);

            int deptId = dept.getDepartmentId();
            
            planCheckStatuses = LookupAgent.getPCStatuses();	//Constants.MODULEID_BUILDING
            
            
            request.setAttribute("planCheckStatuses", planCheckStatuses);
            
            
            session.setAttribute("planCheckForm", planCheckForm);
            request.setAttribute("planCheckForm", planCheckForm);
            request.setAttribute("planCheckId", strPlanCheckId);
            request.setAttribute("activityId", StringUtils.i2s(plancheck.getActivityId()));
            request.setAttribute("planCheckProcessTypeList",new LookupAgent().getPlanCheckProcessTypes());
            request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
            
            // list of attachments for this activity.
 			List attachmentList;
 			try {
 				int planCheckId=StringUtils.s2i(strPlanCheckId);
 				 logger.debug("planCheckId  ::  "+ strPlanCheckId);
 				attachmentList = new CommonAgent().getAttachments(planCheckId, "F");
 				if (!attachmentList.isEmpty() && attachmentList.size() > 3) {

     				attachmentList = attachmentList.subList(0, 3);
     			}
 				request.setAttribute("attachmentList", attachmentList);
 			} catch (Exception e) { 
 				logger.error("Error in Attachments ",e);
 			}

            
            
            logger.debug("Exit of  EditPlanCheckAction ....");

            return (mapping.findForward("success"));
        }
        catch (Exception e) {
            logger.error(e.getMessage()+e);

            return (mapping.findForward("error"));
        }
    }
}

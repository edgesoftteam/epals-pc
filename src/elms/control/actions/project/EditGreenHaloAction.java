package elms.control.actions.project;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AddressAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.app.admin.CustomField;
import elms.app.project.Activity;
import elms.app.project.GreenHalo;
import elms.app.project.GreenHaloNewProjectResponse;
import elms.common.Constants;
import elms.common.Dropdown;
import elms.control.beans.GreenHaloForm;
import elms.util.StringUtils;

public class EditGreenHaloAction extends Action {
	static Logger logger = Logger.getLogger(EditGreenHaloAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();

		try {
			 String lsoId = (String) session.getAttribute(Constants.LSO_ID);
			 String actId = (String) request.getParameter("actId");
			if (actId == null)
					actId = (String) session.getAttribute(Constants.PSA_ID);
				logger.debug("Got actId from request " + actId);
			 if(lsoId == null) {
				 lsoId = ""+ LookupAgent.getLsoIdForPsaId(actId, "A");
			 }
			 logger.debug("lsoid"+ lsoId);	

			GreenHaloForm greenHaloForm = (GreenHaloForm) form;

			logger.debug("Before  agent ");
			ActivityAgent activityAgent = new ActivityAgent();
			
			GreenHalo greenHalo = activityAgent.getGreenHalo(StringUtils.s2i(actId));
			
			greenHalo.setPermit(activityAgent.getActivityNumber(StringUtils.s2i(actId)));
			
			if(greenHalo.getStreet() == null || greenHalo.getStreet().equalsIgnoreCase("")) {
				greenHalo = populateAddress(lsoId, greenHalo);
			}
			
			greenHaloForm.setGreenHalo(greenHalo);
			request.setAttribute("greenHaloForm", greenHaloForm);
			//logger.debug("act nbr after set form is " + greenHalo.getACT_NBR());
			//request.setAttribute("actNBR", greenHalo.getACT_NBR());

			request.setAttribute("actid", actId);
			
			logger.debug("set GreenHalo to form ");
			
			Dropdown dropdown=new Dropdown();
			
			request.setAttribute("projectTypes",dropdown.getGreenHaloProjectTypes());
			request.setAttribute("buildingTypes",dropdown.getGreenHaloBuildingTypes());
			
			/*Activity activity = activityAgent.getActivity(StringUtils.s2i(actId));
			session.setAttribute("greenHaloFlag", greenHaloForm.getGreenHalo());
			GreenHaloNewProjectResponse   projectResponse = activityAgent.getGreenHaloProjectResponse(StringUtils.s2i(actId));
			if(projectResponse == null || projectResponse.getStatus()== null || projectResponse.getStatus().isEmpty() || projectResponse.getStatus().equalsIgnoreCase("fail") ){
				session.setAttribute("greenHaloButton", "Create Green Halo Project");
			}else{
				session.setAttribute("greenHaloButton", "");
			}
			logger.info("ViewFeesMgrAction  :: greenHalo ============> " + activity.getGreenHalo());*/
			
			
			if(((String)request.getAttribute("greenHaloSaveStatus"))== null){
				request.setAttribute("greenHaloSaveStatus", "");
			}

			request.setAttribute("gHaloForm", greenHaloForm);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}
	
	private GreenHalo populateAddress(String lsoId, GreenHalo greenHalo) throws SQLException {
		
		AddressAgent addressAgent = new AddressAgent();
		
		RowSet rs = addressAgent.getLsoAddressList(lsoId);
		
		if(rs != null && rs.next()) {
			if(rs.getString("UNIT") != null) {
			greenHalo.setAptSuite(rs.getString(rs.getString("UNIT")));
			}
			String address = rs.getString("STR_NO");  
			if(rs.getString("STR_MOD") != null) {
				address = address + " " + rs.getString("STR_MOD");
			}
			address = address + " "+rs.getString("STREET_NAME");
			
			address = address.replaceAll("  ", " ");
			greenHalo.setStreet(address);
			if(rs.getString("ZIP") != null) {
				greenHalo.setZipcode(rs.getString("ZIP"));
			}
		}
		return greenHalo;
	}
}

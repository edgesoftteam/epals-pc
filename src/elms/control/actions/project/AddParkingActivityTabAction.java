package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AddressAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivitySubType;
import elms.app.admin.ActivityType;
import elms.app.admin.ParkingZone;
import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectType;
import elms.app.lso.Street;
import elms.app.people.People;
import elms.common.Constants;
import elms.control.beans.ActivityForm;
import elms.util.StringUtils;

public class AddParkingActivityTabAction extends Action {

	static Logger logger = Logger.getLogger(AddParkingActivityTabAction.class.getName());

	protected String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<SubProjectType> subProjectTypes = new ArrayList<SubProjectType>();
		List<ActivityType> activityTypes = new ArrayList<ActivityType>();
		List<ParkingZone> parkingZones = new ArrayList<ParkingZone>();

		int subProjectTypeId = -1;
		String from = request.getParameter("from") != null ? request.getParameter("from") : "";
		try {

			Calendar now = Calendar.getInstance();
			ActivityForm activityForm = (ActivityForm) form;

			logger.debug("Activity Type is :::" + activityForm.getActivityType());
			if (from.equalsIgnoreCase("activityTab")) {
				// This is to prevent the load of date from previous session when user clicks on the tab.
				activityForm = new ActivityForm();
				activityForm.setSubmitted("Y");
			}
			if (from.equalsIgnoreCase("activityType") || from.equalsIgnoreCase("activityTab")) {
				activityForm.setSubmitted("Y");
			} else {
				activityForm.setSubmitted("N");
			}

			// test submitted
			logger.debug("Submitted = " + activityForm.getSubmitted());

			// Get applied date from the form, if does not exist, set today's date
			String appliedDate = activityForm.getStartDate() != null ? activityForm.getStartDate() : StringUtils.cal2str(now);
			activityForm.setStartDate(appliedDate);
			activityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_PARKING_ISSUED));

			// populate sub-project types and activity types
			int subProjectNameId = Integer.parseInt(activityForm.getSubProjectName() != null ? activityForm.getSubProjectName() : "-1");
			logger.debug("Sub-Project Name ID is " + subProjectNameId);
			if (subProjectNameId != -1) {
				subProjectTypes = LookupAgent.getSubProjectTypes(subProjectNameId);
				if (subProjectTypes.size() == 1) {
					logger.debug("Only 1 Sub-Project type was obtained, automatically populating Activity Types");
					subProjectTypeId = ((SubProjectType) subProjectTypes.get(0)).getSubProjectTypeId();
					logger.debug("Sub Project Type Id = " + subProjectTypeId);

					activityTypes = LookupAgent.getActivityTypes(subProjectTypeId);
				}
			}

			// get the correct zone for this address
			String streetNumber = activityForm.getStreetNumber() != null ? activityForm.getStreetNumber() : "";
			String streetName = activityForm.getStreetName() != null ? activityForm.getStreetName() : "";
			if ((streetNumber.equalsIgnoreCase("")) || (streetName.equalsIgnoreCase(""))) {
				// either the street number or name is null, do nothing
			} else {
				int lsoId = new AddressAgent().getLsoId(Integer.parseInt(streetNumber), Integer.parseInt(streetName));
				ParkingZone parkingZone = AddressAgent.getParkingZone("" + lsoId);
				activityForm.setParkingZone(parkingZone.getPzoneId());
			}

			String onloadAction = "";
			if (subProjectNameId == 255002) {
				onloadAction = "disableAddress()";
			}
			request.setAttribute("onloadAction", onloadAction);

			parkingZones = LookupAgent.getParkingZones();
			request.setAttribute("parkingZones", parkingZones);

			request.setAttribute("subProjectTypes", subProjectTypes);
			request.setAttribute("activityTypes", activityTypes);

			List<ActivitySubType> activitySubTypes = new ArrayList<ActivitySubType>();
			request.setAttribute("activitySubTypes", activitySubTypes);

			List<ActivityStatus> activityStatuses = LookupAgent.getActivityStatusList(Constants.MODULE_NAME_PARKING);
			request.setAttribute("activityStatuses", activityStatuses);

			List<ProjectType> subProjectNames = LookupAgent.getSubProjectNames(elms.common.Constants.DEPARTMENT_PARKING_CODE);
			request.setAttribute("subProjectNames", subProjectNames);

			List<Street> streetList = AddressAgent.getStreetArrayList();
			request.setAttribute("streetList", streetList);

			// set default activity status after checking from form
			activityForm.setActivityStatus(activityForm.getActivityStatus() != null ? activityForm.getActivityStatus() : "" + Constants.ACTIVITY_STATUS_PARKING_ISSUED);

			// Custom Fields - Start
			int count = 0;
			String activityType = activityForm.getActivityType() != null ? activityForm.getActivityType() : "-1";
			logger.debug("Activity Type is " + activityForm.getActivityType());

			String submitted = activityForm.getSubmitted();
			logger.debug("submitted = " + submitted);

			if (activityType.equalsIgnoreCase("-1") || submitted.equalsIgnoreCase("Y")) {
				// reload the form with new empty custom fields, otherwise, just get the existing custom fields from request
				RowSet rs = new ActivityAgent().getCustomFieldList(new ActivityAgent().getActivityTypeId(activityForm.getActivityType()));
				activityForm.setCustomFieldList(rs, "add");// added for customFields
			}

			if (activityForm.getActivityType() != null && activityForm.getActivityType().equalsIgnoreCase("PKNCOM")) {
				logger.debug("********************************");
				request.setAttribute("lncv", "Y");
			}
			String exisitingDt = "";
			List lncvPeopleList = new ArrayList();
			Map m = new HashMap();
			if (from.equalsIgnoreCase("lncvSearch")) {
				String email = (String) (request.getParameter("email") != null ? request.getParameter("email") : "");
				String dlno = (String) (request.getParameter("dlno") != null ? request.getParameter("dlno") : "");
				String fname = (String) (request.getParameter("fname") != null ? request.getParameter("fname") : "");
				String lname = (String) (request.getParameter("lname") != null ? request.getParameter("lname") : "");
				String phno = (String) (request.getParameter("phno") != null ? request.getParameter("phno") : "");
				if (!email.equalsIgnoreCase("") || dlno.equalsIgnoreCase("") || fname.equalsIgnoreCase("") || lname.equalsIgnoreCase("")) {
					lncvPeopleList = new ActivityAgent().searchPeopleForParkingActivity(email.toLowerCase(), fname, lname, dlno);
				}

				if (lncvPeopleList.size() == 1) {
					m = new ActivityAgent().searchedPeopleForParkingActivity(((People) lncvPeopleList.get(0)).getActivityId(), ((People) lncvPeopleList.get(0)).getPeopleId());
					request.setAttribute("email", m.get("EMAIL"));
					request.setAttribute("fname", m.get("NAME"));
					request.setAttribute("lname", m.get("LNAME"));
					request.setAttribute("peopleId", m.get("PEOPLE_ID"));
					request.setAttribute("actId", m.get("ACT_ID"));
					request.setAttribute("dtcount", m.get("COUNT"));
					request.setAttribute("dlno", m.get("LIC_NO"));
					request.setAttribute("phno", m.get("PHONE"));
					request.setAttribute("label", m.get("LABEL"));
					lncvPeopleList = new ArrayList();
				} else {
					request.setAttribute("email", email);
					request.setAttribute("fname", fname);
					request.setAttribute("lname", lname);
					request.setAttribute("dlno", dlno);
					request.setAttribute("phno", phno);
				}

				String actId = m.get("ACT_ID") + "";
				if (StringUtils.s2i(actId) > 0) {
					exisitingDt = new ActivityAgent().checkExisitingDates(StringUtils.s2i(actId));
				}

			}

			if (from.equalsIgnoreCase("lncvSearched")) {
				String email = (String) (request.getParameter("email") != null ? request.getParameter("email") : "N");
				String dlno = (String) (request.getParameter("dlno") != null ? request.getParameter("dlno") : "N");
				String fname = (String) (request.getParameter("fname") != null ? request.getParameter("fname") : "N");
				String lname = (String) (request.getParameter("email") != null ? request.getParameter("lname") : "N");
				String actId = (String) (request.getParameter("psaId") != null ? request.getParameter("psaId") : "0");
				logger.debug("**********************************" + actId);
				String peopleId = (String) (request.getParameter("peopleId") != null ? request.getParameter("peopleId") : "0");

				m = new ActivityAgent().searchedPeopleForParkingActivity(StringUtils.s2i(actId), StringUtils.s2i(peopleId));
				request.setAttribute("email", m.get("EMAIL"));
				request.setAttribute("fname", m.get("NAME"));
				request.setAttribute("peopleId", m.get("PEOPLE_ID"));
				request.setAttribute("actId", m.get("ACT_ID"));
				request.setAttribute("dtcount", m.get("COUNT"));
				request.setAttribute("dlno", m.get("LIC_NO"));
				request.setAttribute("phno", m.get("PHONE"));
				request.setAttribute("lname", m.get("LNAME"));
				request.setAttribute("label", m.get("LABEL"));

				if (StringUtils.s2i(actId) > 0) {
					exisitingDt = new ActivityAgent().checkExisitingDates(StringUtils.s2i(actId));
				}

			}

			request.setAttribute("exisitingDt", exisitingDt);
			request.setAttribute("peopleList", lncvPeopleList);
			count = activityForm.getCustomFieldList() != null ? activityForm.getCustomFieldList().length : 0;
			request.setAttribute("count", count);
			// Custom Fields - End

			session.setAttribute("activityForm", activityForm);

			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServletException("Exception occured " + e.getMessage());
		}
	}
}

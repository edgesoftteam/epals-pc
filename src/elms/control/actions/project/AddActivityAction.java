package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.CustomField;
import elms.app.common.DropdownValue;
import elms.app.lso.LsoAddress;
import elms.app.project.ActivityDetail;
import elms.app.project.SubProject;
import elms.common.Constants;
import elms.control.beans.ActivityForm;
import elms.exception.AgentException;
import elms.security.Group;
import elms.security.User;
import elms.util.StringUtils;

public class AddActivityAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AddActivityAction.class.getName());

	/**
	 * The nextpage
	 */
	protected String nextPage = "common";

	/**
	 * The perform method
	 */
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// the web form
		ActivityForm activityForm = null;

		// the http session
		HttpSession session = request.getSession();
		SubProject subProject;

		// get the sub project id from the request as parameter.
		String subProjectId = request.getParameter("subProjectId");
		logger.debug("AddActivityAction With Sub Project : " + subProjectId);
		String sprojName = "";

		// get the user and user groups.
		User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
		boolean businessLicenseUser = false, businessLicenseApproval = false;
		List groups = user.getGroups();
		Iterator itr = groups.iterator();
		while (itr.hasNext()) {
			Group group = (elms.security.Group) itr.next();
			if (group == null)
				group = new elms.security.Group();
			if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER)
				businessLicenseUser = true;
			if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL)
				businessLicenseApproval = true;
		}
		logger.debug("business License user " + businessLicenseUser);
		logger.debug("business License approval " + businessLicenseApproval);

		// using the sub project id, we get the project name id (bldg or
		// planning or ce)
		int projectNameId = -1;

		try {
			projectNameId = LookupAgent.getProjectNameId("Q", StringUtils.s2i(subProjectId));
		} catch (Exception e5) {
			logger.error("Exception occured while getting project name id " + e5.getMessage());
		}

		logger.debug("Obtained project name id is" + projectNameId);

		// from the location
		String from = request.getParameter("from");

		if (from == null) {
			from = "";
		}

		if (from.equals("P") || from.equals("Q")) {
			activityForm = new ActivityForm();
		} else {
			logger.debug("Got the form from session");
			activityForm = (ActivityForm) session.getAttribute("activityForm");
		}

		String aType = activityForm.getActivityType();
		
		String wmFlag = "N";
		if (aType != null) {
			wmFlag = new ActivityAgent().getWMRequiredFlag(aType);
		}
		
		logger.debug("aType  :"+aType);

		request.setAttribute("projectNameId", "" + projectNameId);
		request.setAttribute("wmFlag", "" + wmFlag);

		if (aType == null) {
			aType = "";
		}

		logger.debug("Activity Type is " + aType);

		if (aType.equals("")) {
			ActionForm frm = new ActivityForm();
			session.setAttribute("activityForm", frm);
			activityForm = (ActivityForm) frm;
			activityForm.setSubProjectId(subProjectId);
			activityForm.setActivityId("0");
			activityForm.setOrigin(from);
			logger.debug("subProject Id " + subProjectId);

			LsoAddress address = new ProjectAgent().getPrimaryAddress(subProjectId);
			activityForm.setAddress(StringUtils.i2s(address.getAddressId()));
			logger.debug("address Id for activity :" + address.getAddressId());

			// set start date of the activity as today's date.
			Calendar calendar = Calendar.getInstance();
			activityForm.setStartDate(StringUtils.cal2str(calendar));

			int landuseId = new ProjectAgent().getLandUseIdForSubProject(StringUtils.s2i(subProjectId));
			activityForm.setLandUseId(StringUtils.i2s(landuseId));

			// set the activity status per the project type.
			if (projectNameId == Constants.PROJECT_NAME_BUILDING_ID) {
				activityForm.setActivityStatus("" + Constants.ACTIVITY_STATUS_BUILDING_PERMIT_READY);
			} else if (projectNameId == Constants.PROJECT_NAME_PLANNING_ID) {
				activityForm.setActivityStatus("" + Constants.ACTIVITY_STATUS_PLANNING_SUBMITTED);
			} else if (projectNameId == Constants.PROJECT_NAME_PUBLIC_WORKS_ID) {
				activityForm.setActivityStatus("" + Constants.ACTIVITY_STATUS_PUBLIC_WORKS_ISSUED);
			} else if (projectNameId == Constants.PROJECT_NAME_LICENSE_AND_CODE_ID) {
				activityForm.setActivityStatus("" + Constants.ACTIVITY_STATUS_CE_ACTIVE);
			} else if (projectNameId == Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES_ID) {
				activityForm.setActivityStatus("" + Constants.ACTIVITY_STATUS_LC_PERMIT_PENDING);
			} else {
				activityForm.setActivityStatus("" + Constants.ACTIVITY_STATUS_CE_ACTIVE);
			}

			try {
				activityForm.setOwnerFromPeople(new ActivityAgent().getOwnerByActivityHistory(subProjectId));
			} catch (Exception e3) {
				logger.error("Error getting People (Owner) from Co activities" + e3.getMessage());
			}

			try {
				activityForm.setOwnerFromAssessor(new ProjectAgent().getOwnerByAssessorData(subProjectId));
			} catch (Exception e4) {
				logger.error("Error in getting Owner from Assessor" + e4.getMessage());
			}

			String peopleName = "";

			if (activityForm.getOwnerFromPeople() != null) {
				peopleName = activityForm.getOwnerFromPeople().getName();
			}

			if (peopleName == null) {
				peopleName = "";
			}

			logger.debug("People owner name : " + peopleName);

			String assessorName = "";

			if (activityForm.getOwnerFromAssessor() != null) {
				assessorName = activityForm.getOwnerFromAssessor().getName();
			}

			if (assessorName == null) {
				assessorName = "";
			}

			logger.debug("Assessor owner name : " + assessorName);

			if ((peopleName.length() > 0) && (assessorName.length() > 0)) {
				activityForm.setDisplayPeople("YES");
				activityForm.setDisplayAssessor("YES");

				if (peopleName.trim().toUpperCase().equals(assessorName.trim().toUpperCase())) {
					logger.debug("Both are Equal");
	
					activityForm.setDisplayAssessor("NO");
				} else {
					logger.debug("Both are Not Equal");
				}
			} else {
				if (peopleName.length() > 0) {
					activityForm.setDisplayPeople("YES");
				} else {
					activityForm.setDisplayPeople("NO");
				}

				if (assessorName.length() > 0) {
					activityForm.setDisplayAssessor("YES");
				} else {
					activityForm.setDisplayAssessor("NO");
				}
			}

			nextPage = "common";

		} else {
			nextPage = "common";
		}

		activityForm.setDisplayContent("add");

		ActivityDetail activityDetail = new ActivityDetail();
		activityDetail.setFinalDateLabel(aType);
		activityForm.setCompletionDateLabel(activityDetail.getFinalDateLabel());
		logger.debug("Completion Date Label is set to request  as  " + activityForm.getCompletionDateLabel());
		activityDetail.setExpirationDateLabel(aType);
		activityForm.setExpirationDateLabel(activityDetail.getExpirationDateLabel());
		logger.debug("Expiration Date Label is set to request  as  " + activityForm.getExpirationDateLabel());

		// To find the Planning Activities
		try {
			subProject = new ProjectAgent().getSubProject(StringUtils.s2i(subProjectId), "Y");

		} catch (Exception e) {
			subProjectId = "";
			subProject = new SubProject();
		}
		
		if (projectNameId == Constants.PROJECT_NAME_BUILDING_ID) {
			LookupAgent lookupAgent = new LookupAgent();
			String actType="";
			try {
			actType= lookupAgent.getActTypeId(aType);
			} catch (AgentException e) {
				logger.error(""+e);
				e.printStackTrace();
			}
			logger.debug("actType Id :"+actType);
			
			// as per Chris email on 05/17/2023 to Gaurav, Exp date should be 180 days for all activities
			
//			if( actType != null && (actType.equalsIgnoreCase(StringUtils.i2s(Constants.SINGLE_FAMILY_RESIDENTIAL_ACT_tYPE)) || actType.equals(StringUtils.i2s(Constants.MULTI_FAMILY_ACCESSORY_BUILDING_ACT_TYPE)) || actType.equals(StringUtils.i2s(Constants.Multi_Family_Addition_and_Alteration_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Multi_Family_Repair_and_Maintenance_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.New_Multi_Family_Dwelling_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.New_Single_Family_Dwelling_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Building_Combo_ACT_tYPE)) || actType.equals(StringUtils.i2s(Constants.Single_Family_Accessory_Building_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Single_Family_Addition_Alteration_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Single_Family_Repair_Maintenance_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Duplex_Apartment_Condominium_Townhouse_ACT_tYPE)) )){
//				logger.debug("actType Id :"+actType);
//			Calendar cal = Calendar.getInstance();
//			cal.add(Calendar.DATE, 365);
//			activityForm.setExpirationDate(StringUtils.cal2str(cal));
//			}else{
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 180);
			activityForm.setExpirationDate(StringUtils.cal2str(cal));	
//			}
		}
		logger.debug("Expiration Date :"+activityForm.getExpirationDate());
		session.setAttribute("activityForm", activityForm);
		
		
		//Custom Fields
		int count = 0;
		try{
			RowSet rs = null;
        if (aType != null && !aType.equals("")) {
			 logger.debug("****************************"+aType);
			 ActivityAgent customLabelsAgent = new ActivityAgent();
	       	 CustomField[] customFieldArray =customLabelsAgent.getCustomFieldArray(new LookupAgent().getActivityTypeId(aType)); 
				if(customFieldArray != null && customFieldArray.length >0){
					
					for(int i=0; i< customFieldArray.length; i++){
						CustomField cf = customFieldArray[i];
						logger.debug("field Type::"+cf.getFieldType());
						if(cf.getFieldType() != null && cf.getFieldType().equalsIgnoreCase("DROPDOWN")){
							List<DropdownValue> optionList = customLabelsAgent.getOptionsAsList(cf.getFieldId());
							request.setAttribute("dropdownOptionList"+cf.getFieldId(), optionList);
						}else if(cf.getFieldType() != null && cf.getFieldType().equalsIgnoreCase("CHECKBOX")){
							if(cf.getFieldValue()!= null && cf.getFieldValue().equalsIgnoreCase("Y")){
								cf.setFieldValue("on");
							}							
						}
					}
				}
				
	       	 
             activityForm.setCustomFieldList(customFieldArray);// added for customFields
             count= activityForm.getCustomFieldList().length;
		}else{
       	 CustomField[] customFieldArray = new CustomField[0];
         activityForm.setCustomFieldList(customFieldArray);// added for customFields
		}
        request.setAttribute("count", count);
		}catch(Exception ex){
			logger.error("Error while getting custol fields",ex);
			throw new ServletException("",ex);
		}
        // end customfields	
		
		
		List activityStatuses;

		try {
			if (subProjectId != null || !subProjectId.equalsIgnoreCase("")) {
				sprojName = LookupAgent.getSprojNameForSprojId(subProjectId);
				logger.debug("Got SubprojectName " + sprojName);
			}
			activityStatuses = LookupAgent.getActivityStatuses(projectNameId, sprojName);
		} catch (Exception e) {
			activityStatuses = new ArrayList();
			logger.warn("Could not get list of activityStatuses, initializing it to blank arraylist");
		}
		
		logger.debug("activityForm.getPlanCheckRequired()"+ activityForm.getPlanCheckRequired());

		request.setAttribute("activityStatuses", activityStatuses);

		List activityTypes;

		try {
			activityTypes = LookupAgent.getActivityTypes(subProjectId);

		} catch (Exception e) {
			activityTypes = new ArrayList();
			logger.warn("Could not find list of activityTypes, initializing it to blank arraylist");
		}

		request.setAttribute("activityTypes", activityTypes);

		List activitySubTypes;

		try {
			activitySubTypes = LookupAgent.getActivitySubTypes(aType);

		} catch (Exception e1) {
			activitySubTypes = new ArrayList();
			logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
		}

		request.setAttribute("activitySubTypes", activitySubTypes);

		List addresses;

		try {
			addresses = new ActivityAgent().getActivityAddresses(subProjectId);
		} catch (Exception e2) {
			addresses = new ArrayList();
			logger.warn("Could not find list of getActivityAddresses, initializing it to blank arraylist");
		}

		request.setAttribute("addresses", addresses);

		logger.info("Exit of  AddActivityAction .... with next Page " + nextPage);

		return (mapping.findForward(nextPage));
	}
}
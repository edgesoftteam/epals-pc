package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.common.Constants;
import elms.control.beans.PlanCheckForm;
import elms.util.StringUtils;

public class DeletePlanCheckAction extends Action {

	static Logger logger = Logger.getLogger(DeletePlanCheckAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		PlanCheckForm planCheckForm = (PlanCheckForm) form;
		String psaType = "";

		try {

			String[] selectedPlanCheck = planCheckForm.getSelectedPlanCheck();
			logger.debug("obtained array of selected people elememts" + selectedPlanCheck);

			String strPsaId = (String) session.getAttribute(Constants.PSA_ID);
			// String strPsaId = (String) request.getParameter("psaId");
			logger.debug("obtained psa id from session as " + strPsaId);
			int psaId = StringUtils.s2i(strPsaId);

			ActivityAgent planCheckAgent = new ActivityAgent();
			planCheckAgent.deletePlanCheck(selectedPlanCheck, strPsaId);

			logger.debug("after plan check agent");

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			session.setAttribute(Constants.PSA_ID, StringUtils.i2s(psaId));

			// List peopleList = new PeopleAgent().getPeoples(psaId, psaType,0);
			// request.setAttribute("peopleList", peopleList);
			// request.setAttribute("psaId",strPsaId);
			// request.setAttribute("psaType",psaType);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));

	}

}
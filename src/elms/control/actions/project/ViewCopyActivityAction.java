package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.project.Activity;
import elms.control.beans.project.CopyActivityForm;
import elms.util.StringUtils;

public class ViewCopyActivityAction extends Action {

	static Logger logger = Logger.getLogger(ViewCopyActivityAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering ViewCopyActivityAction");

		try {
			HttpSession session = request.getSession();
			CopyActivityForm copyForm = new CopyActivityForm();

			int activityId = StringUtils.s2i(request.getParameter("activityId"));
			logger.debug("Activity Id :" + activityId);

			String tmpActId = request.getParameter("activityId");
			request.setAttribute("activityId", tmpActId);

			Activity activity = new ActivityAgent().getActivityLite(activityId);
			copyForm.getCopyActivity().setActivity(activity);

			request.setAttribute("copyActivityForm", copyForm);

			logger.info("Exiting ViewCopyActivityAction");

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}
}

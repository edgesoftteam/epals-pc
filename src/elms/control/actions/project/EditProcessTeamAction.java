package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.common.Constants;
import elms.control.beans.ProcessTeamForm;
import elms.security.User;

public class EditProcessTeamAction extends Action {
	static Logger logger = Logger.getLogger(EditProcessTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		String psaId = (String) session.getAttribute(Constants.PSA_ID);
		String psaType = (String) session.getAttribute(Constants.PSA_TYPE);
		User user = (User) session.getAttribute(Constants.USER_KEY);

		try {
			String backUrl = "";

			if (psaType == null) {
				psaType = "";
			}

			if (psaType.equals("P")) {
				backUrl = request.getContextPath() + "/viewProject.do?projectId=" + psaId;
			} else if (psaType.equals("A")) {
				logger.debug("obtained the PSA Type as Activity A");
				try {
					String activityType = LookupAgent.getActivityTypeForActId(psaId);

					if (activityType == null) {
						activityType = "";
					}

					if (activityType.equals("PWFAC")) {
						backUrl = request.getContextPath() + "/pwViewFacilities.do?activityId=" + psaId;
					} else if (activityType.equals("PWINFO")) {
						backUrl = request.getContextPath() + "/pwViewFacilities.do?activityId=" + psaId;
					} else if (activityType.equals("PWSAN")) {
						backUrl = request.getContextPath() + "/pwViewSanitation.do.do?activityId=" + psaId;
					} else if (activityType.equals("PWSEW")) {
						backUrl = request.getContextPath() + "/pwViewSewer.do?activityId=" + psaId;
					} else if (activityType.equals("PWSTR")) {
						backUrl = request.getContextPath() + "/pwViewStreets.do?activityId=" + psaId;
					} else if (activityType.equals("PWWAT")) {
						backUrl = request.getContextPath() + "/pwViewWater.do?activityId=" + psaId;
					} else if (activityType.equals("PWSANC")) {
						backUrl = request.getContextPath() + "/pwViewSanitation.do?activityId=" + psaId;
					} else if (activityType.equals("PWTSSL")) {
						backUrl = request.getContextPath() + "/pwViewTrafficSignals.do?activityId=" + psaId;
					} else if (activityType.equals("PWELEC")) {
						backUrl = request.getContextPath() + "/pwViewElectrical.do?activityId=" + psaId;
					} else {
						backUrl = request.getContextPath() + "/viewActivity.do?activityId=" + psaId;
					}

					logger.debug("Back URL is " + backUrl);
				} catch (Exception e) {
					logger.error("Error when creating backUrl " + e.getMessage());
				}
			}

			CommonAgent agent = new CommonAgent();
			ProcessTeamForm frm = (ProcessTeamForm) form;

			frm.setSupervisor(user.getIsSupervisor());
			frm.setPsaId(psaId);
			frm.setPsaType(psaType);
			frm.setBackUrl(backUrl);

			frm = agent.populateTeam(frm);
			session.setAttribute("processTeamForm", frm);

			// Forward control to the specified success URI
			return (mapping.findForward("success"));

		} catch (Exception e) {
			logger.error("Exception occured when modifying process team. Message is  " + e.getMessage());
			return (mapping.findForward("error"));
		}
	}
}

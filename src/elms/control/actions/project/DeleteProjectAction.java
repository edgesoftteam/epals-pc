package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.project.Project;
import elms.common.Constants;
import elms.control.actions.lso.ViewLandAction;
import elms.control.actions.lso.ViewOccupancyAction;
import elms.control.actions.lso.ViewStructureAction;
import elms.util.StringUtils;

public class DeleteProjectAction extends Action {

	static Logger logger = Logger.getLogger(DeleteProjectAction.class.getName());
	protected Project project;
	protected int projectId;
	protected int lsoId;
	String lsoType;
	String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		try {

			// Getting required values from the request Object
			projectId = StringUtils.s2i(request.getParameter("projectId"));
			logger.debug("Obtained Project id from request as " + projectId);
			lsoId = StringUtils.s2i((String) session.getAttribute(Constants.LSO_ID));
			logger.debug("Obtained lsoId from session as " + lsoId);
			lsoType = new AddressAgent().getLsoType(lsoId);
			ProjectAgent projectAgent = new ProjectAgent();
			if (lsoType.equals("L") || lsoType.equals("S") || lsoType.equals("O")) {
				logger.debug("In the Loop and LSO_TYPE is " + lsoType);
				int result = projectAgent.deleteProject(projectId);
				logger.debug("delete result is " + result);
				if (result != -1) {
					if (lsoType.equals("L")) {
						nextPage = "land";
						ViewLandAction viewLandAction = new ViewLandAction();
						viewLandAction.processLand(request, lsoId, "");
					} else if (lsoType.equals("S")) {
						nextPage = "structure";
						ViewStructureAction viewStructureAction = new ViewStructureAction();
						viewStructureAction.processStructure(request, StringUtils.i2s(lsoId), "0");
					} else {
						nextPage = "occupancy";
						ViewOccupancyAction viewOccupancyAction = new ViewOccupancyAction();
						viewOccupancyAction.processOccupancy(request, lsoId, "");
					}
					// need to refresh the PSA tree so onloadAction is setting
					// to
					String onloadAction = "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "'";
					logger.debug("Setting the refresh tree flag to " + onloadAction);
					session.setAttribute("onloadAction", onloadAction);
				} else {
					nextPage = "failed";
					ViewProjectAction viewProjectAction = new ViewProjectAction();
					viewProjectAction.getProject(projectId, request);
				}
			} else {
				nextPage = "failed";
				ViewProjectAction viewProjectAction = new ViewProjectAction();
				viewProjectAction.getProject(projectId, request);
			}
			if (nextPage.equals("failed")) {
				// need not refresh the PSA tree so onloadAction is setting to
				// ""
				String onloadAction = "";
				logger.debug("Setting the refresh tree flag to " + onloadAction);
				session.setAttribute("onloadAction", onloadAction);
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		logger.info("Forwarding the delete Project Action with next page is " + nextPage);
		return (mapping.findForward(nextPage));

	}

}
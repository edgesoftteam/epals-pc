package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.RowSet;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.app.admin.SubProjectType;
import elms.common.Constants;
import elms.control.beans.ActivityForm;
import elms.util.StringUtils;

public class AddActivityTabAction extends Action {

	static Logger logger = Logger.getLogger(AddActivityTabAction.class.getName());

	protected String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		// default lists
		List subProjectTypes = new ArrayList();
		List activityTypes = new ArrayList();
		int subProjectTypeId = -1;
		String from = request.getParameter("from") != null ? request.getParameter("from") : "";
		try {

			Calendar now = Calendar.getInstance();
			ActivityForm activityForm = (ActivityForm) form;

			logger.debug("Activity Type is :::" + activityForm.getActivityType());
			if (from.equalsIgnoreCase("activityTab")) {
				// This is to prevent the load of date from previous session when user clicks on the tab.
				activityForm = new ActivityForm();
				activityForm.setSubmitted("Y");
			}
			if (from.equalsIgnoreCase("activityType") || from.equalsIgnoreCase("activityTab")) {
				activityForm.setSubmitted("Y");
			} else {
				activityForm.setSubmitted("N");
			}

			// test submitted
			logger.debug("Submitted = " + activityForm.getSubmitted());

			// Get applied date from the form, if does not exist, set today's date
			String appliedDate = activityForm.getStartDate() != null ? activityForm.getStartDate() : StringUtils.cal2str(now);
			activityForm.setStartDate(appliedDate);
			activityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_PUBLIC_WORKS_PENDING));

			// make expiration date as issue date + 180 days
			if (!(GenericValidator.isBlankOrNull(appliedDate))) {
				Calendar expirationDateCal = StringUtils.str2cal(appliedDate);
				expirationDateCal.add(Calendar.DATE, 180);
				String expirationDate = activityForm.getExpirationDate() != null ? activityForm.getExpirationDate() : StringUtils.cal2str(expirationDateCal);
				logger.debug("Expiration date set to the form is " + expirationDate);
				activityForm.setExpirationDate(expirationDate);
			}

			// populate sub-project types and activity types
			int subProjectNameId = Integer.parseInt(activityForm.getSubProjectName() != null ? activityForm.getSubProjectName() : "-1");
			logger.debug("Sub-Project Name ID is " + subProjectNameId);
			if (subProjectNameId != -1) {
				subProjectTypes = LookupAgent.getSubProjectTypes(subProjectNameId);
				if (subProjectTypes.size() == 1) {
					logger.debug("Only 1 Sub-Project type was obtained, automatically populating Activity Types");
					subProjectTypeId = ((SubProjectType) subProjectTypes.get(0)).getSubProjectTypeId();
					logger.debug("Sub Project Type Id = " + subProjectTypeId);

					activityTypes = LookupAgent.getActivityTypes(subProjectTypeId);
				}
			}
			request.setAttribute("subProjectTypes", subProjectTypes);
			request.setAttribute("activityTypes", activityTypes);

			List activitySubTypes = new ArrayList();
			request.setAttribute("activitySubTypes", activitySubTypes);

			List activityStatuses = LookupAgent.getActivityStatusList(Constants.MODULE_NAME_PUBLIC_WORKS);
			request.setAttribute("activityStatuses", activityStatuses);

			List landmarks = LookupAgent.getLandmarks();
			request.setAttribute("landmarks", landmarks);

			List subProjectNames = LookupAgent.getSubProjectNames(elms.common.Constants.DEPARTMENT_PUBLIC_WORKS_CODE);
			request.setAttribute("subProjectNames", subProjectNames);

			List crossStreets = new ArrayList();
			String crossStreetName1 = activityForm.getCrossStreetName1() != null ? activityForm.getCrossStreetName1() : "-1";
			logger.debug("Cross Street 1 selected is " + crossStreetName1);
			if (!crossStreetName1.equalsIgnoreCase("-1")) {
				logger.debug("Cross Street 1 is " + crossStreetName1);
				crossStreets = new AddressAgent().getCrossStreets(crossStreetName1);
			}

			request.setAttribute("crossStreets", crossStreets);

			// set default activity status after checking from form
			activityForm.setActivityStatus(activityForm.getActivityStatus() != null ? activityForm.getActivityStatus() : "" + Constants.ACTIVITY_STATUS_BUILDING_PERMIT_READY);

			// Custom Fields - Start
			int count = 0;
			String activityType = activityForm.getActivityType() != null ? activityForm.getActivityType() : "-1";
			logger.debug("Activity Type is " + activityForm.getActivityType());

			String submitted = activityForm.getSubmitted();
			logger.debug("submitted = " + submitted);

			if (activityType.equalsIgnoreCase("-1") || submitted.equalsIgnoreCase("Y")) {
				// reload the form with new empty custom fields, otherwise, just get the existing custom fields from request
				RowSet rs = new ActivityAgent().getCustomFieldList(new ActivityAgent().getActivityTypeId(activityForm.getActivityType()));
				activityForm.setCustomFieldList(rs, "add");// added for customFields
			}

			count = activityForm.getCustomFieldList() != null ? activityForm.getCustomFieldList().length : 0;
			request.setAttribute("count", count);
			// Custom Fields - End

			session.setAttribute("activityForm", activityForm);

			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			throw new ServletException("Exception occured " + e.getMessage());
		}
	}
}

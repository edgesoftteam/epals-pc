package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.common.Hold;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.Owner;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.Use;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.control.beans.ProjectForm;
import elms.util.StringUtils;

public class EditProjectDetailsAction extends Action {
	static Logger logger = Logger.getLogger(EditProjectDetailsAction.class.getName());
	protected Project project;
	protected ProjectDetail projectDetail;
	protected int projectId = -1;
	protected String ownerName;
	protected String streetNumber;
	protected String streetName;
	protected List useList;
	protected List projectStatuses;
	protected String level;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			projectId = StringUtils.s2i(request.getParameter("levelId"));
			logger.debug("got projectId from the request " + projectId);
			level = request.getParameter("level");
			project = new ProjectAgent().getProject(projectId);
			projectDetail = project.getProjectDetail();

			ActionForm frm = new ProjectForm();
			request.setAttribute("projectForm", frm);

			ProjectForm projectForm = (ProjectForm) frm;

			// setting the pasId to request object
			request.setAttribute("levelId", Integer.toString(project.getProjectId()));
			request.setAttribute("level", level);
			logger.debug("set  projectId in the request " + project.getProjectId());

			if (projectDetail != null) {
				String projectNumber = projectDetail.getProjectNumber();
				logger.debug("setting projectNumber in the request " + projectNumber);
				request.setAttribute("projectNumber", projectNumber);

				String projectName = projectDetail.getName();
				logger.debug("setting projectName to projectForm " + projectName);
				projectForm.setProjectName(projectName);

				String projectDescription = projectDetail.getDescription();
				logger.debug("setting projectDescription to projectForm " + projectDescription);
				projectForm.setDescription(projectDescription);

				String startDate = StringUtils.cal2str(projectDetail.getStartDate());
				logger.debug("setting startDate to projectForm " + startDate);
				projectForm.setStartDate(startDate);

				String completionDate = StringUtils.cal2str(projectDetail.getCompletionDate());
				logger.debug("setting completionDate to projectForm " + completionDate);
				projectForm.setCompletionDate(completionDate);

				String expitationDate = StringUtils.cal2str(projectDetail.getExpriationDate());
				logger.debug("setting expirationDate to projectForm " + expitationDate);
				projectForm.setExpirationDate(expitationDate);

				String status = "";

				if (projectDetail.getProjectStatus() != null) {
					status = StringUtils.i2s(projectDetail.getProjectStatus().getStatusId());
				}

				logger.debug("setting status to projectForm " + status);
				projectForm.setStatus(status);
				projectForm.setOpenActivitiesCount(StringUtils.i2s(projectDetail.getOpenActivitiesCount()));
				logger.debug("setting open Activtiy Count to projectForm " + projectForm.getOpenActivitiesCount());
				projectForm.setMicrofilm(projectDetail.getMicrofilm());
				logger.debug("setting Microfilm to projectForm is set to activityForm as  " + projectForm.getMicrofilm());
				projectForm.setLabel(projectDetail.getLabel());
				logger.debug("setting Label to projectForm is set to activityForm as  " + projectForm.getLabel());
			}

			String valuation = "no data";
			logger.debug("View project action -- setting valuation to projectForm " + valuation);
			projectForm.setValuation(valuation);

			// get the list of project names and set it to the attribute
			List projectNames = new ArrayList();
			projectNames = LookupAgent.getProjectNames();
			logger.debug("project names list obtained of size " + projectNames.size());
			request.setAttribute("projectNames", projectNames);

			String use = Integer.toString(project.getUse().getId());
			logger.debug("View project action -- setting use id to projectForm " + use);
			projectForm.setUse(use);
			request.setAttribute("useId", use);

			String useDesc = "";

			try {
				
				if(project.getUse()!=null)
				{
				   Use useObj=  new AddressAgent().getUse(project.getUse().getId());
				   if(useObj!=null)
				   {
				     useDesc =useObj.getDescription();
				   }
				}

				if (useDesc == null) {
					useDesc = "";
				}
			} catch (Exception useEx) {
				useEx.printStackTrace();
				logger.error("Use Error:" + useEx.getMessage());
			} finally {
				request.setAttribute("useDesc", useDesc);
			}

			if (project.getLso().getLsoType().equals("L")) {
				Land land = new AddressAgent().getLand(project.getLso().getLsoId());

				// getting the Owner Name of the Land
				List landApnList = land.getLandApn();

				if (landApnList.size() > 0) {
					LandApn landApn = (LandApn) landApnList.get(0);
					Owner owner = landApn.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of the Land
				List landAddressList = land.getLandAddress();

				if (landAddressList.size() > 0) {
					LandAddress landAddressObj = (LandAddress) landAddressList.get(0);
					streetNumber = StringUtils.i2s(landAddressObj.getStreetNbr());
					streetName = landAddressObj.getStreet().getStreetName();
				}
			} else if (project.getLso().getLsoType().equals("S")) {
				Structure structure = new AddressAgent().getStructure(project.getLso().getLsoId());

				// Getting the Owner Name of Land for this Structure
				Land land = new AddressAgent().getLand(structure.getLandId());
				List landApnList = land.getLandApn();

				if (landApnList.size() > 0) {
					LandApn landApnObj = (LandApn) landApnList.get(0);
					Owner owner = landApnObj.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of structure
				List structureAddressList = structure.getStructureAddress();

				if (structureAddressList.size() > 0) {
					StructureAddress structureAddressObj = (StructureAddress) structureAddressList.get(0);
					streetNumber = StringUtils.i2s(structureAddressObj.getStreetNbr());
					streetName = structureAddressObj.getStreet().getStreetName();
				}
			} else if (project.getLso().getLsoType().equals("O")) {
				Occupancy occupancy = new AddressAgent().getOccupancy(project.getLso().getLsoId());

				// Getting the Owner of Occupancy
				List occupancyApnList = occupancy.getOccupancyApn();

				if (occupancyApnList.size() > 0) {
					OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApnList.get(0);
					Owner owner = occupancyApnObj.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of Occupancy
				List occupancyAddressList = occupancy.getOccupancyAddress();

				if (occupancyAddressList.size() > 0) {
					OccupancyAddress occupancyAddressObj = (OccupancyAddress) occupancyAddressList.get(0);
					streetNumber = StringUtils.i2s(occupancyAddressObj.getStreetNbr());
					streetName = occupancyAddressObj.getStreet().getStreetName();
				}
			}

			request.setAttribute("ownerName", ownerName);
			logger.debug("set  owner name in the request " + ownerName);
			request.setAttribute("streetNumber", streetNumber);
			logger.debug("set  street number in the request " + streetNumber);
			request.setAttribute("streetName", streetName);
			logger.debug("setting streetName in the request " + streetName);

			// useList = new UseAgent().getUseList(project.getLso().getLsoType());
			// logger.debug("setting useList in the request " + useList.size());
			// request.setAttribute("useList", useList);
			try {
				projectStatuses = LookupAgent.getProjectStatuses();
			} catch (Exception e) {
				projectStatuses = new ArrayList();
				logger.warn("Could not find list of projectStatuses, initializing it to blank arraylist");
			}

			request.setAttribute("projectStatuses", projectStatuses);
			logger.debug("setting projectStatuses in the request with size" + projectStatuses.size());

			// setting the list of micro film satauses
			List microfilmStatuses;

			try {
				microfilmStatuses = LookupAgent.getMicrofilmStatuses();
			} catch (Exception e1) {
				microfilmStatuses = new ArrayList();
				logger.warn("Could not find list of microfilmStatuses, initializing it to blank arraylist");
			}

			request.setAttribute("microfilmStatuses", microfilmStatuses);

			// / Done By Gayathri -- For not Editing Project name LC which contains BL/BT
			AdminAgent moveProjectAgent = new AdminAgent();

			List subProjecNamesList = new ArrayList();
			String SubProjName = "";

			subProjecNamesList = moveProjectAgent.getsubProjectName(projectDetail.getProjectNumber());

			for (int i = 0; i < subProjecNamesList.size(); i++) {
				/* get sub project Name */
				SubProjName = subProjecNamesList.get(i).toString();

				/* check whether sub project name starts with BT,BL */

				if (SubProjName.startsWith("BL") || SubProjName.startsWith("BT")) {
					request.setAttribute("editableProjName", "false");
				} else {
					request.setAttribute("editableProjName", "true");
				}
			}

			// Setting the final staus Allowed field
			projectForm.setFinalStatusAllowed("Y");

			List holds = project.getPsaHold();

			if (holds == null) {
				holds = new ArrayList();
			}

			logger.debug("Size of holds is " + holds.size());

			if ((holds != null) && (holds.size() > 0)) {
				Hold hold = (Hold) holds.get(0);

				if (hold != null) {
					if (hold.getType() != null) {
						if (hold.getType().equals("Hard")) {
							projectForm.setFinalStatusAllowed("N");
						}
					}

					logger.debug("projectForm final Status Allowed is set to " + projectForm.getFinalStatusAllowed() + "Because of Hard Holds");
				}

				logger.debug("projectForm final Status Allowed is set to " + projectForm.getFinalStatusAllowed());
			}

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Excetion occured with message of " + e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}

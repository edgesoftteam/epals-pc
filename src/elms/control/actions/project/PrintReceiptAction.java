package elms.control.actions.project;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom.Document;
import org.jdom.output.XMLOutputter;

import elms.agent.LookupAgent;
import elms.agent.ReportAgent;
import elms.app.admin.ActivityType;
import elms.util.Doc2Pdf;
import elms.util.StringUtils;

public class PrintReceiptAction extends Action {
	static Logger logger = Logger.getLogger(PrintReceiptAction.class.getName());
	protected String actNbr = "";
	protected String pdfFileName = "";
	protected String xslFileName = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into the PrintPermitAction");

		ActionErrors errors = new ActionErrors();

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			logger.info("Entered PrintPermitAction with errors");

			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				actNbr = (String) request.getParameter("actNbr");

				String actType = (String) request.getParameter("actType");

				if (actType == null) {
					actType = "";
				}

				logger.debug("actNbr is " + actNbr);
				logger.debug("actType is " + actType);

				ReportAgent ra = new ReportAgent();

				Document document = null;

				ActivityType type = LookupAgent.getActivityType(actType);

				if (type != null) {
					if (type.getDepartmentCode().equals("PL") && (type.getSubProjectType() == 1)) {
						logger.debug("Control is in Planning");
						xslFileName = "planning.xsl";
						document = ra.getGeneralPermitJDOM(actNbr, "PL", actType);
					} else {
						logger.debug("Control is in Building");

						if (type.getSubProjectType() == 2) {
							// Code Enforcement
							xslFileName = "ce.xsl";
						} else if (actNbr.trim().startsWith("BL")) {
							xslFileName = "printreceiptBL.xsl";
						} else if (actNbr.trim().startsWith("BT")) {
							xslFileName = "printreceiptBT.xsl";
						} else {
							xslFileName = "printreceipt.xsl";
						}

						document = ra.getGeneralPermitJDOM(actNbr, "", actType);
					}
				}

				logger.debug("The xsl file name is " + xslFileName);

				// for debugging start
				XMLOutputter outputter = new XMLOutputter();

				try {
					outputter.setIndent("  "); // use two space indent
					outputter.setNewlines(true);
					outputter.output(document, System.out);
				} catch (IOException e) {
					logger.error(e);
				}

				// for debugging end

				pdfFileName = StringUtils.getUniqueFileName() + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);

				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					xslFileName = getRealPath + File.separator+ "xsl" + File.separator + xslFileName;
					pdfFileName = getRealPath + File.separator+ "pdf" + File.separator + pdfFileName;
					Doc2Pdf.start(document, xslFileName, pdfFileName);
				}
				return (mapping.findForward("success"));
			} catch (Exception e) {
				logger.error("Unable to print permit " + e.getMessage());
				throw new ServletException(e.getMessage());
			}
		}

	}
}

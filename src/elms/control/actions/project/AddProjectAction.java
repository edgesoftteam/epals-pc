package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.LandDetails;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.Owner;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.Use;
import elms.control.beans.ProjectForm;
import elms.util.StringUtils;

public class AddProjectAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AddProjectAction.class.getName());

	protected ProjectForm projectForm;
	protected String ownerName = "";
	protected String streetNumber = "";
	protected String streetName = "";
	protected String unit = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		String lsoId = request.getParameter("lsoId");
		int intLsoId = StringUtils.s2i(lsoId);
		logger.debug("Got the int value of lsoId from request" + intLsoId);

		String lsoType = new AddressAgent().getLsoType(intLsoId);

		try {
			ActionForm frm = new ProjectForm();
			request.setAttribute("projectForm", frm);
			projectForm = (ProjectForm) frm;

			Calendar calendar = Calendar.getInstance();
			projectForm.setStartDate(StringUtils.cal2str(calendar));
			logger.debug("Setting the Start date to current date " + projectForm.getStartDate());
			logger.debug("setting lsoType in the request " + lsoType);
			request.setAttribute("lsoType", lsoType);
			logger.debug("setting lsoId in the request " + lsoId);
			request.setAttribute("lsoId", lsoId);

			String useDesc = "";
			int useId = -1;
			List landUse;
			Use lUse = new Use();
			Land useLand = new Land();
			LandDetails landDetail = new LandDetails();
			useLand = new AddressAgent().getLand(intLsoId);
			landDetail = useLand.getLandDetails();
			landUse = landDetail.getUse();

			// get the list of project names and set it to the attribute
			List projectNames = new ArrayList();
			projectNames = LookupAgent.getProjectNames();
			logger.debug("project names list obtained of size " + projectNames.size());
			request.setAttribute("projectNames", projectNames);

			if (landUse.size() > 0) {
				lUse = (Use) landUse.get(0);

				if (lUse == null) {
					logger.debug("land Use is empty");
					request.setAttribute("useDesc", "");
					request.setAttribute("useId", "");
					projectForm.setUse("-1");
				} else {
					logger.debug("land Use : " + landUse);
					useDesc = lUse.getDescription();
					logger.debug("land desc " + useDesc);

					if (useDesc == null)
						useDesc = "";

					request.setAttribute("useDesc", useDesc);
					useId = lUse.getId();
					logger.debug("land Use Id " + useId);
					request.setAttribute("useId", StringUtils.i2s(useId));
					projectForm.setUse(StringUtils.i2s(useId));
				}
			}

			if (lsoType.equals("L")) {
				Land land = new AddressAgent().getLand(intLsoId);

				// getting the Owner Name of the Land
				List landApnList = land.getLandApn();

				if (landApnList.size() > 0) {
					LandApn landApn = (LandApn) landApnList.get(0);
					Owner owner = landApn.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of the Land
				List landAddressList = land.getLandAddress();

				if (landAddressList.size() > 0) {
					LandAddress landAddressObj = (LandAddress) landAddressList.get(0);
					streetNumber = StringUtils.i2s(landAddressObj.getStreetNbr());
					streetName = landAddressObj.getStreet().getStreetName();
				}

				logger.debug("obtained the land object for lsoId =" + lsoId);
			} else if (lsoType.equals("S")) {
				Structure structure = new AddressAgent().getStructure(intLsoId);

				// Getting the Address of structure
				List structureAddressList = structure.getStructureAddress();

				if (structureAddressList.size() > 0) {
					StructureAddress structureAddressObj = (StructureAddress) structureAddressList.get(0);
					streetNumber = StringUtils.i2s(structureAddressObj.getStreetNbr());
					streetName = structureAddressObj.getStreet().getStreetName();
				}
				// The below is required for the fact that the prevous owner name stays in the request
				// and as there is no owner for structure, we need to clear it to blank.
				ownerName = "";
			} else if (lsoType.equals("O")) {
				Occupancy occupancy = new AddressAgent().getOccupancy(intLsoId);

				// Getting the Owner of Occupancy
				List occupancyApnList = occupancy.getOccupancyApn();

				if (occupancyApnList.size() > 0) {
					OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApnList.get(0);
					Owner owner = occupancyApnObj.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of Occupancy
				List occupancyAddressList = occupancy.getOccupancyAddress();

				if (occupancyAddressList.size() > 0) {
					OccupancyAddress occupancyAddressObj = (OccupancyAddress) occupancyAddressList.get(0);
					streetNumber = StringUtils.i2s(occupancyAddressObj.getStreetNbr());
					streetName = occupancyAddressObj.getStreet().getStreetName();
					unit = occupancyAddressObj.getUnit();

				}
			}
			if (unit == null) {
				unit = "";
			}
			request.setAttribute("ownerName", ownerName);
			logger.debug("set  owner name in the request " + ownerName);
			request.setAttribute("streetNumber", streetNumber);
			logger.debug("set  street number in the request " + streetNumber);
			request.setAttribute("streetName", streetName);
			logger.debug("setting streetName in the request " + streetName);
			request.setAttribute("unit", unit);
			logger.debug("setting unit in the request " + unit);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

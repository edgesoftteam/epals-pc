package elms.control.actions.project;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.PlanCheckAgent;
import elms.app.admin.ActivityType;
import elms.app.project.ActivityAttributes;
import elms.app.project.PlanCheck;
import elms.common.Constants;
import elms.control.beans.PlanCheckForm;
import elms.control.beans.online.EmailTemplateAdminForm;
import elms.exception.AgentException;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.ExchangeEmail;
import elms.util.StringUtils;

public class SavePlanCheckAction extends Action {
	static Logger logger = Logger.getLogger(SavePlanCheckAction.class.getName());
	int activityId = -1;
	int planCheckId = -1;
	String strPlanCheckId = "";
	protected PlanCheck planCheck = null;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into SavePlanCheckAction ....");

		HttpSession session = request.getSession();

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		PlanCheckForm planCheckForm = (PlanCheckForm) form;
		ActionErrors errors = new ActionErrors();

		String pcStatusCode = planCheckForm.getPlanCheckStatus();
		
		 String action = request.getParameter("action");
	        logger.debug("action.. "+action);
	        if(action != null){
	        	if(action.equalsIgnoreCase("delete")){
	        		List<PlanCheck> pcToDelete = new ArrayList<PlanCheck>();
	        		if(planCheckForm.getPlanChecks() != null && planCheckForm.getPlanChecks().length >0){
	        			PlanCheck[] planchecks = planCheckForm.getPlanChecks();
	        			for(int i=0;i<planchecks.length;i++ ){
	            			if(planchecks[i].getCheck() != null && planchecks[i].getCheck().equalsIgnoreCase("true")){
	            				pcToDelete.add(planchecks[i]);
	            				try{
	            					String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
	            					if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
	            				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PLANCHECK"), "id", StringUtils.i2s(planchecks[i].getPlanCheckId()));
	            					}
	            				}catch (Exception e) {
									e.getMessage();
								}
	            			}
	        			}
	        		}
	        		if(pcToDelete.size() >0){
	        			try {
							new PlanCheckAgent().deletePlanChecks(pcToDelete);
							if(planCheckId > 0 ){
								errors.add("plancheckMessage", new ActionError("plancheckMessage", " Plan Check details deleted successfully"));
					            saveErrors(request, errors); 
							}
						
						} catch (AgentException e) {
								throw new ServletException("",e);
						}
	        		}
	        		logger.debug("testData:::" + request.getParameter("planChecks[0].check"));
	        		return (mapping.findForward("success"));
	        	}
	        }


		try {
			if ((pcStatusCode.equalsIgnoreCase("-1")) || (pcStatusCode == null)) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.planCheck.noStatusCode"));
			    List sorting=null;
				planCheckForm.setPlanChecks(new PlanCheckAgent().getPlanChecks(StringUtils.s2i(planCheckForm.getActivityId()),null));
				request.setAttribute("planCheckForm", planCheckForm);
				request.setAttribute("activityId", planCheckForm.getActivityId());

				logger.debug("no Plan Check Status");
			}
		} catch (Exception e1) {
			logger.error("Exception occured while getting plan check statuses " + e1.getMessage());
			throw new ServletException("Exception occured while getting plan check status, Error is " + e1.getMessage());
		}

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);

			return (new ActionForward(mapping.getInput()));
		} else {
			try {
				 

	     		String fromWhere = "";
	     		if(request.getParameter("fromWhere") != null) {
	     			fromWhere = request.getParameter("fromWhere");
	     		}
	     		 String fromDate="";
     		   String toDate="";
     		   if(request.getAttribute("fromDate")!= null){
     			   fromDate=(String)request.getAttribute("fromDate");
     		   }
     		   if(request.getAttribute("toDate")!= null){
     			   toDate=(String)request.getAttribute("toDate");
     		   }
	     		logger.debug("fromDate... "+fromDate);
	     		logger.debug("toDate... "+toDate);
	     		request.setAttribute("fromDate", fromDate);
	     		request.setAttribute("toDate", toDate);	     		
	     		request.setAttribute("fromWhere", fromWhere);
	     		
	     		PlanCheck planCheck = new PlanCheck();
	                strPlanCheckId = planCheckForm.getPlanCheckId();
	                

	                if (strPlanCheckId == null)
	                    strPlanCheckId = request.getParameter("planCheckId");

	                if (strPlanCheckId == null)
	                    strPlanCheckId = "0";

	                logger.debug("plan check id is " + strPlanCheckId);
	                planCheck.setPlanCheckId(StringUtils.s2i(strPlanCheckId));
	                
	                activityId = StringUtils.s2i(planCheckForm.getActivityId());
	                logger.debug("Got parameter activityId as " + activityId);
	                planCheck.setActivityId(activityId);
	                logger.debug("set activityId  as " + planCheck.getActivityId());
	                
	                planCheck.setStatusCode(StringUtils.s2i(planCheckForm.getPlanCheckStatus()));
	                logger.debug("set PlanCheckStatus as " + planCheck.getStatusCode());
	                
	                planCheck.setPlanCheckDate(StringUtils.str2cal(planCheckForm.getPlanCheckDate()));
	                logger.debug("set  PlanCheckDate  as " + StringUtils.cal2Ts(planCheck.getPlanCheckDate()));
	                
	                planCheck.setTargetDate(StringUtils.str2cal(planCheckForm.getTargetDate()));
	                logger.debug("set  TargetDate as " + StringUtils.cal2Ts(planCheck.getTargetDate()));
	                
	                planCheck.setCompletionDate(StringUtils.str2cal(planCheckForm.getCompletionDate()));
	                logger.debug("set  CompletionDate  as " + StringUtils.cal2Ts(planCheck.getCompletionDate()));
	                
	                /*planCheck.setUnit(planCheckForm.getUnit());
	                logger.debug("set  Unit  as " + planCheck.getUnit());*/
	                
	                planCheck.setUnitHours(planCheckForm.getUnitHours());
	                logger.debug("Set Unit Hours as "+planCheck.getUnitHours());
	                
	                planCheck.setUnitMinutes(planCheckForm.getUnitMinutes());
	                logger.debug("Set Unit Minutes as "+planCheck.getUnitMinutes());
	                int totalminutes=0;
	                if(planCheck.getUnitMinutes() != ""&&planCheck.getUnitHours() != "")
	                 totalminutes=StringUtils.s2i(planCheck.getUnitMinutes())+StringUtils.s2i(planCheck.getUnitHours())*60;
	                
	                logger.debug("totalminutes::"+totalminutes);
	                planCheck.setUnitMinutes(StringUtils.i2s(totalminutes));
	                
	                planCheck.setEngineer(StringUtils.s2i(planCheckForm.getEngineerId()));
	                logger.debug("getEngineerId "+StringUtils.s2i(planCheckForm.getEngineerId()));
	                logger.debug("set  Engineer  as " + planCheck.getEngineer());
	                
	                planCheck.setDeptDesc(planCheckForm.getDepartment());                
	                logger.debug("deptName"+planCheck.getDeptDesc());
	                
	                planCheck.setComments(planCheckForm.getComments());
	                logger.debug("set  Comment  as " + planCheck.getComments());

	                User user = (User) session.getAttribute("user");
	                planCheck.setCreatedBy(user);
	                logger.debug("set  Created By as " + planCheck.getCreatedBy());
	                
	                planCheck.setUpdatedBy(user);
	                logger.debug("set  updated By as " + planCheck.getUpdatedBy());
	                
	                planCheck.setProcessType(planCheckForm.getProcessType());
	                logger.debug("selt  Process type as " + planCheck.getProcessType());
	                
	                planCheck.setEmailflag(planCheckForm.getEmailflag());
	                logger.debug("set  Email flag as " + planCheck.getEmailflag());

				if (strPlanCheckId.equalsIgnoreCase("0")){
					planCheckId = new PlanCheckAgent().addPlanCheck(planCheck);
					if(planCheck.getEmailflag() != null && planCheck.getEmailflag().equalsIgnoreCase("on")){
						PlanCheck pc = new PlanCheckAgent().getPlanCheck(planCheckId);
						EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
						emailTemplateAdminForm = new LookupAgent().getEmailTempByTempTypeId(Constants.PLANCHECK_CREATED);
						
						ExchangeEmail msg = new ExchangeEmail();
						if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")){
					    	msg.setRecipient(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
					    	msg.setSubject(Constants.CONTEXT_PATH+emailTemplateAdminForm.getEmailSubject());
					    }
						else{
						msg.setRecipient(pc.getEngineerMailId());
						msg.setSubject(emailTemplateAdminForm.getEmailSubject());
						}
						msg.setContent(getEmailBody(emailTemplateAdminForm.getEmailMessage(),pc));
						msg.deliver();
					}
					if(planCheckId > 0 ){
						errors.add("plancheckMessage", new ActionError("plancheckMessage", " Plan Check details added successfully"));
			            saveErrors(request, errors); 
					}
					
				}
				else{
					planCheckId = new PlanCheckAgent().editPlanCheck(planCheck);
					if(planCheck.getEmailflag() != null && planCheck.getEmailflag().equalsIgnoreCase("on")){
						PlanCheck pc = new PlanCheckAgent().getPlanCheck(planCheckId);
						EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
						emailTemplateAdminForm = new LookupAgent().getEmailTempByTempTypeId(Constants.PLANCHECK_STATUS_CHANGE);
						
						ExchangeEmail msg = new ExchangeEmail();
						if(LookupAgent.getKeyValue(Constants.PROD_SERVER_FLAG).equalsIgnoreCase("N")){
					    	msg.setRecipient(LookupAgent.getKeyValue(Constants.EDGESOFT_SUPPORT_EMAIL));
					    	msg.setSubject(Constants.CONTEXT_PATH+emailTemplateAdminForm.getEmailSubject());
					    }
						else{
						msg.setRecipient(pc.getEngineerMailId());
						msg.setSubject(emailTemplateAdminForm.getEmailSubject());
						}
						msg.setContent(getEmailBody(emailTemplateAdminForm.getEmailMessage(),pc));
						msg.deliver();
					}
					
					if(planCheckId > 0 ){
						errors.add("plancheckMessage", new ActionError("plancheckMessage", " Plan Check details saved successfully"));
			            saveErrors(request, errors); 
					}
					request.setAttribute("planCheckId", planCheckId);
					return (new ActionForward("/editPlanCheck.do"));
				
				}
				try {
					String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
					if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
					GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PLANCHECK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Plan check
					}
				} catch (Exception e) {
					e.getMessage();
				}
				 ActivityAttributes plancheckCounts = new PlanCheckAgent().getPlanCheckCounts(planCheck.getActivityId());
	                
	             ActivityType actType = LookupAgent.getActivityTypeForActId(planCheck.getActivityId());

				   if(actType.getModuleId()!= Constants.MODULEID_BUILDING && plancheckCounts.getTotalPlanCheck() >0 && plancheckCounts.getOpenPlanCheck() == 0){
	                	SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
	                	//new ActivityAgent().updateCompletionDate(planCheck.getActivityId(), sdf.format(new Date()));
	                }
					logger.debug("Exit SaveActivityAction ...  ");  
				
				return (mapping.findForward("success"));

			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				return (mapping.findForward("error"));
			}
		}

	}
	private String getEmailBody(String emailBody, PlanCheck pc) {
		logger.debug("getEmailBody :: " + emailBody);
		logger.debug("act no... "+pc.getActNbr());
		if(emailBody!=null) {
			if(emailBody.contains("##ADDRESS##")) emailBody = emailBody.replaceAll("##ADDRESS##", pc.getAddress());
			if(emailBody.contains("##ACTTYPE##")) emailBody = emailBody.replaceAll("##ACTTYPE##", pc.getActType());
			if(emailBody.contains("##PERMITNO##")) emailBody = emailBody.replaceAll("##PERMITNO##", pc.getActNbr());
		}
		emailBody = emailBody.replace("\\\"", "\"");
		logger.debug("getEmailBody :: " + emailBody);
		return emailBody;
	}
}

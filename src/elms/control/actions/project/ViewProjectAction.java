package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.common.Condition;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.Owner;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.common.Constants;
import elms.control.beans.ConditionForm;
import elms.util.StringUtils;

public class ViewProjectAction extends Action {
	static Logger logger = Logger.getLogger(ViewProjectAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("ViewProjectAction perform()");

		int projectId;

		HttpSession session = request.getSession();

		try {
			String refreshTree = (String) session.getAttribute("refreshTree");
			if ((refreshTree == null) || refreshTree.equals("")) {
				refreshTree = "N";
			}
			session.setAttribute("refreshTree", refreshTree);
			String projectInactiveEditPermission = "false";
			List groups = new ArrayList();
			elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {
				groups = (List) user.getGroups();
			}

			java.util.Iterator itr = groups.iterator();

			while (itr.hasNext()) {
				elms.security.Group group = (elms.security.Group) itr.next();

				if (group == null) {
					group = new elms.security.Group();
				}

				if (group.groupId == 5) {
					projectInactiveEditPermission = "true";
				}

			}

			request.setAttribute("projectInactiveEditPermission", projectInactiveEditPermission);

			// --------------------------------------------------
			// Getting required values from the request Object
			projectId = StringUtils.s2i(request.getParameter("projectId"));

			if (projectId < 0) {
				projectId = StringUtils.s2i((String) request.getAttribute("projectId"));
			}

			logger.debug("obtained project id from request as " + projectId);
			getProject(projectId, request);
			logger.info("Forwarding the view project action to view project jsp");

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException("Exception thrown in View  project action " + e.getMessage());
		}
	}

	public void getProject(int projectId, HttpServletRequest request) {
		logger.info("getProject(" + projectId + ", " + request + ")");

		Project project = null;
		ProjectDetail projectDetail = null;
		List subProjects = null;
		String ownerName = "";
		String streetNumber = "";
		String streetName = "";
		String streetModifier = "";
		String unit = "";

		HttpSession session = request.getSession();

		try {
			String refreshTree = "";
			String refreshPSATree = "";

			try {
				refreshTree = (String) session.getAttribute("refreshTree");

				if (refreshTree == null) {
					refreshTree = "";
				}

				refreshPSATree = (String) session.getAttribute("refreshPSATree");

				if (refreshPSATree == null) {
					refreshPSATree = "";
				}
			} catch (Exception nothing) {
			}

			session.setAttribute(Constants.PSA_ID, StringUtils.i2s(projectId));
			logger.debug("set the psa type into session under Constants.PSA_ID as " + (String) session.getAttribute(Constants.PSA_ID));
			session.setAttribute(Constants.PSA_TYPE, "P");
			logger.debug("set the psa type into session under Constants.PSA_TYPE as " + (String) session.getAttribute(Constants.PSA_TYPE));
			session.removeAttribute("siteStructureForm");
			session.removeAttribute("siteUnitForm");

			// get the lso id for the given psa id and psa type.
			int lsoId = LookupAgent.getLsoIdForPsaId(StringUtils.i2s(projectId), "P");
			logger.debug("obtained lso id is " + lsoId);

			session.setAttribute(Constants.LSO_ID, StringUtils.i2s(lsoId));
			logger.debug("Set lso id to session as " + lsoId);

			// Getting the Project
			project = new ProjectAgent().getProject(projectId);
			logger.info("optained project" + project.toString());

			// setting the pasId to request object
			request.setAttribute("projectId", Integer.toString(project.getProjectId()));
			logger.debug(" ********** set  projectId in the request " + project.getProjectId());

			// need to refresh the PSA tree so onloadAction is setting to
			String onloadAction = "";

			if (refreshTree.equals("Y")) {
				onloadAction = "parent.f_lso.location.href='" + request.getContextPath() + "/lsoSearchWithLso.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "';parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "&psaNodeId=" + projectId + "'";
			}

			if (refreshPSATree.equals("Y")) {
				onloadAction = "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "&psaNodeId=" + projectId + "'";
			}

			logger.debug("Setting the refresh tree flag to " + onloadAction);
			session.setAttribute("onloadAction", onloadAction);
			session.setAttribute("refreshTree", "N");
			session.removeAttribute("refreshPSATree");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		try {
			// Setting the request Object
			projectDetail = project.getProjectDetail();

			if (projectDetail != null) {
				String projectNumber = projectDetail.getProjectNumber();
				logger.debug("projectNumber :" + projectNumber);
				request.setAttribute("projectNumber", projectNumber);

				String psaInfo = " -- " + projectNumber;
				String projectName = projectDetail.getName();
				logger.debug("projectName :" + projectName);
				request.setAttribute("projectName", projectName);
				psaInfo = psaInfo + " - " + projectName;
				session.setAttribute("psaInfo", psaInfo);

				String projectDescription = projectDetail.getDescription();
				logger.debug("View project action -- setting projectDescription in the request " + projectDescription);
				request.setAttribute("projectDescription", projectDescription);

				String microfilm = "";

				if (projectDetail.getMicrofilm() != null) {
					microfilm = LookupAgent.getMicrofilmStatus(projectDetail.getMicrofilm()).getDescription();
				}

				request.setAttribute("microfilm", microfilm);
				logger.debug("microFilm:" + request.getAttribute("microFilm"));

				String startDate = StringUtils.cal2str(projectDetail.getStartDate());
				logger.debug("View project action -- setting startDate in the request " + startDate);
				request.setAttribute("startDate", startDate);

				String completionDate = StringUtils.cal2str(projectDetail.getCompletionDate());
				logger.debug("View project action -- setting completionDate in the request " + completionDate);
				request.setAttribute("completionDate", completionDate);

				String expirationDate = StringUtils.cal2str(projectDetail.getExpriationDate());
				logger.debug("View project action -- setting expirationDate in the request " + completionDate);
				request.setAttribute("expirationDate", expirationDate);

				String status = "";

				if (projectDetail.getProjectStatus() != null) {
					status = projectDetail.getProjectStatus().getDescription();
				}

				logger.debug("View project action -- setting status in the request " + status);
				request.setAttribute("status", status);

				String valuation = StringUtils.dbl2$(projectDetail.getValuation());
				logger.debug("View project action -- setting valuation in the request " + valuation);
				request.setAttribute("valuation", valuation);

				String createdBy = "";

				if (projectDetail.getCreatedBy() != null) {
					String firstName = "";
					String lastName = "";

					if (projectDetail.getCreatedBy().getFirstName() != null) {
						firstName = projectDetail.getCreatedBy().getFirstName();
					}

					if (projectDetail.getCreatedBy().getLastName() != null) {
						lastName = projectDetail.getCreatedBy().getLastName();
					}

					createdBy = firstName + " " + lastName;
				}

				request.setAttribute("createdBy", createdBy);

				String label = (projectDetail.getLabel()) != null ? projectDetail.getLabel() : "";
				request.setAttribute("label", label);

				logger.debug("ViewActivityAction -- Parameter createdBy is set to Request as" + request.getAttribute("createdBy"));
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		try {
			String use = project.getUse().getDescription();
			logger.debug("View project action -- setting use description in  the request " + use);
			request.setAttribute("use", use);

			if (project.getLso().getLsoType().equals("L")) {
				Land land = new AddressAgent().getLand(project.getLso().getLsoId());

				// getting the Owner Name of the Land
				List landApnList = land.getLandApn();

				if (landApnList.size() > 0) {
					LandApn landApn = (LandApn) landApnList.get(0);
					Owner owner = landApn.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of the Land
				List landAddressList = land.getLandAddress();

				if (landAddressList.size() > 0) {
					LandAddress landAddressObj = (LandAddress) landAddressList.get(0);
					streetNumber = StringUtils.i2s(landAddressObj.getStreetNbr());
					streetName = landAddressObj.getStreet().getStreetName();
				}
			} else if (project.getLso().getLsoType().equals("S")) {
				Structure structure = new AddressAgent().getStructure(project.getLso().getLsoId());

				// Getting the Owner Name of Land for this Structure
				Land land = new AddressAgent().getLand(structure.getLandId());
				List landApnList = land.getLandApn();

				if (landApnList.size() > 0) {
					LandApn landApnObj = (LandApn) landApnList.get(0);
					Owner owner = landApnObj.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of structure
				List structureAddressList = structure.getStructureAddress();

				if (structureAddressList.size() > 0) {
					StructureAddress structureAddressObj = (StructureAddress) structureAddressList.get(0);
					streetNumber = StringUtils.i2s(structureAddressObj.getStreetNbr());
					streetName = structureAddressObj.getStreet().getStreetName();
				}
			} else if (project.getLso().getLsoType().equals("O")) {
				Occupancy occupancy = new AddressAgent().getOccupancy(project.getLso().getLsoId());

				// Getting the Owner of Occupancy
				List occupancyApnList = occupancy.getOccupancyApn();

				if (occupancyApnList.size() > 0) {
					OccupancyApn occupancyApnObj = (OccupancyApn) occupancyApnList.get(0);
					Owner owner = occupancyApnObj.getOwner();

					if (owner != null) {
						ownerName = owner.getName();
					}
				}

				// Getting the Address of Occupancy
				List occupancyAddressList = occupancy.getOccupancyAddress();

				if (occupancyAddressList.size() > 0) {
					OccupancyAddress occupancyAddressObj = (OccupancyAddress) occupancyAddressList.get(0);
					streetNumber = StringUtils.i2s(occupancyAddressObj.getStreetNbr());
					streetModifier = StringUtils.nullReplaceWithEmpty(occupancyAddressObj.getStreetModifier());
					streetName = occupancyAddressObj.getStreet().getStreetName();
					unit = occupancyAddressObj.getUnit();
				}
			}
			if (unit == null) {
				unit = "";
			}
			request.setAttribute("ownerName", ownerName);
			logger.debug("set  owner name in the request " + ownerName);
			request.setAttribute("streetNumber", streetNumber);
			logger.debug("set  street number in the request " + streetNumber);
			request.setAttribute("streetName", streetName);
			logger.debug("setting streetName in the request " + streetName);
			request.setAttribute("unit", unit);
			logger.debug("setting unit in the request " + unit);

			// setting the lsoAddress in session for display purpos on the screens.
			String lsoAddress = streetNumber + " " + streetModifier + " " + streetName + " " + unit;
			session.setAttribute("lsoAddress", lsoAddress);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		try {
			// Setting SubProject List

			String active = (String) request.getParameter("active") != null ? request.getParameter("active") : "Y";
			subProjects = new ArrayList();
			subProjects = new ProjectAgent().getSubProjects(projectId, active);
			request.setAttribute("subProjects", subProjects);
			request.setAttribute("active", active);
			logger.debug("set List of subProjects to request " + subProjects.size());

			// setting Holdlist
			List holds = new ArrayList();
			holds = project.getPsaHold();
			request.setAttribute("holds", holds);
			logger.debug("set Holds to request with size " + holds.size());

			if (new CommonAgent().editable(projectId, "P")) {
				request.setAttribute("editable", "true");
			} else {
				request.setAttribute("editable", "false");
			}

			// **** Condition List
			ConditionForm condForm = new ConditionForm();
			CommonAgent condAgent = new CommonAgent();
			condForm.setLevelId(projectId);
			condForm.setConditionLevel("P");
			condForm = condAgent.populateConditionForm(condForm);

			Condition[] conditions = condForm.getCondition();
			List condList = Arrays.asList(conditions);

			if (condList.size() > 3) {
				condList = condList.subList(0, 3);
			}

			request.setAttribute("condList", condList);

			// list of comments for this project.
			List commentList = new CommonAgent().getCommentList(projectId, 3, "P");
			request.setAttribute("commentList", commentList);

			// list of attachments for this project.
			List attachmentList = new CommonAgent().getAttachments(projectId, "P");
			request.setAttribute("attachmentList", attachmentList);

			logger.info("Exiting ViewProjectAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}

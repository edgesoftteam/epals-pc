package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.LookupAgent;
import elms.agent.ProjectAgent;
import elms.control.beans.SubProjectForm;
import elms.util.StringUtils;

public class AddSubProjectAction extends Action {
	static Logger logger = Logger.getLogger(AddSubProjectAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			String projectId = request.getParameter("projectId");
			String projectName = (request.getParameter("projectName") != null) ? request.getParameter("projectName") : "";
			session.setAttribute("projectName", projectName);

			logger.debug("Entered Into AddSubProjectAction with projectId as " + projectId);

			SubProjectForm subProjectForm = (SubProjectForm) form;
			String projectType = subProjectForm.getSubProject();
			logger.debug("Get  Sub Project as  .." + projectType);

			projectName = LookupAgent.getProjectNameCode(projectName);

			List ptList;

			try {
				ptList = LookupAgent.getSubProjectNames(projectName);
			} catch (Exception e) {
				ptList = new ArrayList();
				logger.warn("Could not get list of projectTypes, initializing it to blank arraylist");
			}

			request.setAttribute("ptList", ptList);
			logger.debug("The sub Project List is set to request  with size " + ptList.size());

			if ((projectType == null) || projectType.equals("")) {
				List sptList = new ArrayList();
				request.setAttribute("sptList", sptList);
				logger.debug("The subProject Type List is set to request  with size " + sptList.size());

				List spstList = new ArrayList();
				request.setAttribute("spstList", spstList);
				logger.debug("The subProject Sub Type List is set to request  with size " + sptList.size());
				session.setAttribute("subProjectForm", subProjectForm);
			} else {
				ProjectAgent projectAgent = new ProjectAgent();

				List sptList;

				try {
					sptList = LookupAgent.getSubProjectTypes(StringUtils.s2i(projectType));
				} catch (Exception e1) {
					sptList = new ArrayList();
					logger.debug("Could not get list of sptList, initializing it to blank arraylist");
				}

				request.setAttribute("sptList", sptList);
				logger.debug("The subProject Type List is set to request  with size " + sptList.size());

				String from = request.getParameter("from");

				if (from == null) {
					from = "";
				}

				if (from.equals("ptList")) {
					List spstList = new ArrayList();
					request.setAttribute("spstList", spstList);
					logger.debug("The subProject Sub Type List is set to request  with size " + sptList.size());
				} else {
					subProjectForm = (SubProjectForm) session.getAttribute("subProjectForm");

					String subProjectType = subProjectForm.getType();

					if ((subProjectType == null) || subProjectType.equals("")) {
						List spstList = new ArrayList();
						request.setAttribute("spstList", spstList);
						logger.debug("The subProject Sub Type List is set to request  with size " + sptList.size());
					} else {
						List spstList;

						try {
							spstList = LookupAgent.getSubProjectSubTypes(StringUtils.s2i(subProjectType));
						} catch (Exception e2) {
							spstList = new ArrayList();
							logger.debug("Could not get list of spstList, initializing it to blank arraylist");
						}

						request.setAttribute("spstList", spstList);
						logger.debug("The subProject Sub Type List is set to request  with size " + spstList.size());
					}
				}
			}

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error("Exception occured while adding sub project " + e.getMessage());

			return (mapping.findForward("error"));
		}
	}
}

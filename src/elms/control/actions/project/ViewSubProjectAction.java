package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.CommonAgent;
import elms.agent.FinanceAgent;
import elms.agent.PeopleAgent;
import elms.agent.ActivityAgent;
import elms.agent.ProjectAgent;
import elms.agent.ProjectAgent;
import elms.app.planning.CalendarFeature;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.control.beans.ConditionForm;
import elms.security.Group;
import elms.security.User;
import elms.util.StringUtils;

public class ViewSubProjectAction extends Action {
	static Logger logger;

	static {
		logger = Logger.getLogger(elms.control.actions.project.ViewSubProjectAction.class.getName());
	}

	public ViewSubProjectAction() {
	}

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into the ViewSubProjectAction");

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		String active = null;

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);

			return new ActionForward(mapping.getInput());
		}

		try {
			int subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
			active = request.getParameter("active");
			
			if(active == null){
				active = ProjectAgent.getSubProjectActiveOrNot(subProjectId);
			}
			
			if (subProjectId < 0) {
				subProjectId = StringUtils.s2i((String) request.getAttribute("subProjectId"));
			}

			logger.debug("Print address id: " + request.getParameter("address"));
			String addressFromPlanner = request.getParameter("address");

			if (addressFromPlanner != null) {
				session.setAttribute("lsoAddress", addressFromPlanner);
			}
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			logger.debug("viewSubProjectAction -- got parameter subProjectId as " + subProjectId);
			doRequestProcess(request, subProjectId, active);
		} catch (Exception e) {
			logger.error("Exception in ViewSubProjectAction " + e.getMessage());
		}

		return mapping.findForward("success");
	}

	public void doRequestProcess(HttpServletRequest request, int subProjectId, String active) {
		try {
			HttpSession session = request.getSession();
			boolean subProjectInactiveEditPermission = false;
			List groups = new ArrayList();
			User user = (User) session.getAttribute("user");

			if (user != null) {
				groups = user.getGroups();
			}

			for (Iterator itr = groups.iterator(); itr.hasNext();) {
				Group group = (Group) itr.next();

				if (group == null) {
					group = new Group();
				}

				if (group.groupId == 5) {
					subProjectInactiveEditPermission = true;
				}
			}

			request.setAttribute("subProjectInactiveEditPermission", new Boolean(subProjectInactiveEditPermission));

			String refreshTree = "";
			String refreshPSATree = "";

			try {
				refreshTree = (String) session.getAttribute("refreshTree");

				if (refreshTree == null) {
					refreshTree = "";
				}

				refreshPSATree = (String) session.getAttribute("refreshPSATree");

				if (refreshPSATree == null) {
					refreshPSATree = "";
				}
			} catch (Exception exception) {
			}

			session.setAttribute("id", StringUtils.i2s(subProjectId));
			logger.debug("set the psa type into session under Constants.PSA_ID as " + (String) session.getAttribute("id"));
			session.setAttribute("type", "Q");
			logger.debug("set the psa type into session under Constants.PSA_TYPE as " + (String) session.getAttribute("type"));

			String onloadAction = "";

			if (refreshTree.equals("Y")) {
				onloadAction = "parent.f_lso.location.href='" + request.getContextPath() + "/lsoSearchWithLso.do?lsoId=" + (String) session.getAttribute("lsoId") + "';parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute("lsoId") + "&psaNodeId=" + subProjectId + "'";
			}

			if (refreshPSATree.equals("Y")) {
				onloadAction = "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute("lsoId") + "&psaNodeId=" + subProjectId + "'";
			}

			logger.debug("Setting the refresh tree flag to " + onloadAction);
			session.setAttribute("onloadAction", onloadAction);
			session.setAttribute("refreshTree", "N");
			session.removeAttribute("refreshPSATree");
			session.removeAttribute("subProjectForm");
			

			SubProject subProject = (new ProjectAgent()).getSubProject(subProjectId, active);
			request.setAttribute("subProject", subProject);
			logger.debug("Got subProject from database as  " + subProject.toString());
			request.setAttribute("projectId", StringUtils.i2s(subProject.getProjectId()));
			request.setAttribute("subProjectId", StringUtils.i2s(subProject.getSubProjectId()));

			SubProjectDetail subProjectDetail = subProject.getSubProjectDetail();

			if (subProjectDetail != null) {
				String subProjectName = "";

				if (subProjectDetail.getProjectType() != null) {
					subProjectName = subProjectDetail.getProjectType().getDescription();
				}

				String psaInfo = " -- " + subProjectDetail.getSubProjectNumber() + " - " + subProjectName;
				session.setAttribute("psaInfo", psaInfo);
			}

			List holds = new ArrayList();
			holds = subProject.getPsaHold();
			request.setAttribute("holds", holds);
			logger.debug("set Holds to request with size " + holds.size());

			if ((new CommonAgent()).editable(subProjectId, "Q")) {
				request.setAttribute("editable", "true");
			} else {
				request.setAttribute("editable", "false");
			}

			ConditionForm condForm = new ConditionForm();
			CommonAgent condAgent = new CommonAgent();
			condForm.setLevelId(subProject.getSubProjectId());
			condForm.setConditionLevel("Q");
			condForm = condAgent.populateConditionForm(condForm);

			elms.app.common.Condition[] conditions = condForm.getCondition();
			List condList = Arrays.asList(conditions);

			if (condList.size() > 3) {
				condList = condList.subList(0, 3);
			}

			request.setAttribute("condList", condList);

			FinanceAgent financeAgent = new FinanceAgent();
			elms.app.finance.FinanceSummary financeSummary = financeAgent.getSubProjectFinance(subProject.getSubProjectId());
			request.setAttribute("financeSummary", financeSummary);
			request.setAttribute("activities", subProject.getActivity());

			List commentList = (new CommonAgent()).getCommentList(subProjectId, 3, "Q");
			request.setAttribute("commentList", commentList);

			List allPeopleBySubProjectId = (new PeopleAgent()).getAllPeopleBySubProjectId(subProjectId);

			if (allPeopleBySubProjectId.size() > 3) {
				allPeopleBySubProjectId = allPeopleBySubProjectId.subList(0, 3);
			}

			request.setAttribute("allPeopleBySubProjectId", allPeopleBySubProjectId);

			List activityTeamList = (new CommonAgent()).getActivityTeamList(subProjectId);

			if (activityTeamList.size() > 3) {
				activityTeamList = activityTeamList.subList(0, 3);
			}

			request.setAttribute("activityTeamList", activityTeamList);

			// list of CalendarFeatures for this activity
			List calendarFeaturesList = new ActivityAgent().getCalendarFeaturesListForMini(subProjectId, 3);
			if (calendarFeaturesList.size() > 3) {
				calendarFeaturesList = calendarFeaturesList.subList(0, 3);

			}

			Iterator iter = calendarFeaturesList.iterator();
			while (iter.hasNext()) {
				CalendarFeature calendarFeature = (CalendarFeature) iter.next();
				if (GenericValidator.isBlankOrNull(calendarFeature.getMeetingDate())) {
					iter.remove();
				}

			}
			request.setAttribute("calendarFeaturesList", calendarFeaturesList);

			logger.debug("calendarFeaturesList size: " + calendarFeaturesList.size());

			List attachmentList = (new CommonAgent()).getAttachments(subProjectId, "Q");
			request.setAttribute("attachmentList", attachmentList);

			logger.debug("set List of Activities to request " + subProject.getActivity().size());
		} catch (Exception e) {
			logger.error("Exception in ViewSubProjectAction " + e.getMessage());
		}
	}
}

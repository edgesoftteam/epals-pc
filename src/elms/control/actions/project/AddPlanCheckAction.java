package elms.control.actions.project;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.PlanCheckAgent;
import elms.app.admin.EngineerUser;
import elms.app.admin.PlanCheckStatus;
import elms.app.project.PlanCheck;
import elms.common.Constants;
import elms.common.Dropdown;
import elms.control.beans.PlanCheckForm;
import elms.security.Group;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.Util;


public class AddPlanCheckAction extends Action {
    static Logger logger = Logger.getLogger(AddPlanCheckAction.class.getName());

    public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
        logger.debug("Entered Into AddPlanCheckAction ....");

        String strActivityId = "";
        int activityId;
        List sorting = null;

        HttpSession session = request.getSession();
        User user =(User) session.getAttribute(Constants.USER_KEY);
        String psaId = StringUtils.nullReplaceWithEmpty((String) request.getParameter("activityId"));

		if (psaId.trim().equals("")) {
			psaId = (String) session.getAttribute(Constants.PSA_ID);
		}

		logger.debug("obtained psa id from request as " + psaId);

		String psaType = StringUtils.nullReplaceWithEmpty((String) request.getParameter("type"));
		
		logger.debug("psaType"+psaType);

		if (psaType.trim().equals("")) {
			psaType = (String) session.getAttribute(Constants.PSA_TYPE);
		}

		logger.debug("obtained psa type from request as " + psaType);

        try {
        	
			 List groups = user.getGroups();
		     Group group;
		        boolean isPlanCheckManager = false;
		        boolean isPlanCheckReviewEngineer = false;
		        int engineerId = -1;
		        List<Integer> pcIdBasedOnEngineer = null;
		        List<Integer> pcIdBasedOnDept = null;
		        for (int i = 0; i < groups.size(); i++) {
		            group = (Group) groups.get(i);
		            if (group.getGroupId() == Constants.GROUPS_PLAN_REVIEW_ENGINEER) {
		            	isPlanCheckReviewEngineer = true;
		            	
		            }
		            if (group.getGroupId() == Constants.GROUPS_PLAN_CHECK_MANAGER) {
		            	isPlanCheckManager = true;
		            	
		            }
		        }
		    String showAddButton = "no";
        	if(user.getRole().description == Constants.ROLES_ADMINISTRATOR || isPlanCheckManager) {
        		showAddButton = "yes";
        	 }
        	logger.debug("showAddButton :: "+showAddButton);
        	request.setAttribute("showAddButton", showAddButton);
        	request.setAttribute("showAdd", request.getParameter("showAdd"));
        	logger.debug("showAdd :: "+request.getParameter("showAdd"));
        	if(request.getParameter("action") != null){
        		if(request.getParameter("action").equalsIgnoreCase("getUsers")){
        			String deptDesc = request.getParameter("deptDesc");
        			String engineerUsers = getEngineerUsers(deptDesc);
        			PrintWriter pw = response.getWriter();
                    pw.write(engineerUsers);
                    return null;
        		}else if(request.getParameter("action").equalsIgnoreCase("getStatusList")){
            			String deptDesc = request.getParameter("deptDesc");
            			String pcStatus = getPCStatus(deptDesc);
            			PrintWriter pw = response.getWriter();
                        pw.write(pcStatus);
                        return null;
        		}
        		else if(request.getParameter("action").equalsIgnoreCase("sorting"))
        		{
        			PlanCheckForm planCheckForm =(PlanCheckForm) session.getAttribute("planCheckForm");
					 sorting = planCheckForm.getSorting();
					if(sorting == null){
						sorting = new ArrayList();
						planCheckForm.setSorting(sorting);
					}
					String newsorting = request.getParameter("sortingField");
					boolean flag = false;
					if(sorting.size() <=0 ){
						sorting.add(newsorting);
					}else{
	        			for(int i=0; i<sorting.size(); i++){
	        				String srt = (String) sorting.get(i);
	        				if(srt.equals(newsorting)){
	        					sorting.set(i, newsorting +" desc");
	        					flag = true;
	        				}else if(srt.contains(newsorting)){
	        					sorting.set(i, newsorting );
	        					flag = true;
	        				}
	        			}
	        			if(!flag)
	        				sorting.add(newsorting);
					}
					
        		}
        	}
        	
        	PlanCheckForm planCheckForm = new PlanCheckForm();
            PlanCheckAgent planCheckAgent = new PlanCheckAgent();

            strActivityId = request.getParameter("activityId");

            if ((strActivityId == null) || strActivityId.equals("")) {
                strActivityId = (String) session.getAttribute(Constants.PSA_ID);
            }

            activityId = StringUtils.s2i(strActivityId);

            ActivityAgent activityAgent = new ActivityAgent();
            String projectId = activityAgent.getProjectId(strActivityId);

            String engineer = planCheckAgent.getEngFromProj(projectId);

            planCheckForm.setEngineerId(engineer);
            logger.debug("engineer Id " + engineer);

            planCheckForm.setActivityId(strActivityId);
            logger.debug("activity Id is set to plancheck form as " + activityId);
            if(sorting!=null){
            planCheckForm.setSorting(sorting);
            logger.debug("sorting columns"+sorting.toString());
            }
            //It is to display the list of Plan Checks

            PlanCheck[] planCheck = planCheckAgent.getPlanChecks(StringUtils.s2i(strActivityId),sorting);
           //planCheckForm.setPlanChecks(planCheckAgent.getPlanChecks(StringUtils.s2i(strActivityId),sorting));
          
            List<Integer> plancheckId =planCheckAgent.getPCHistoryId(strActivityId);
          	List<Integer> pcId = planCheckAgent.getPCId(strActivityId,sorting);
			if (user.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_BUILDING_SAFETY_CODE)
					|| user.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_PUBLIC_WORKS_CODE)) {
				if (user.getDepartment().getDepartmentId() == new ActivityAgent()
						.getDepartmentIdForActivityId(strActivityId)) {

				} else {
					pcIdBasedOnEngineer = planCheckAgent.getPCIdBasedOnDept(user.getDepartment().getDepartmentId(),
							strActivityId, sorting);
					if(!(pcIdBasedOnEngineer != null && pcIdBasedOnEngineer.size()>0)){
						pcIdBasedOnEngineer.add(0, 0);
					}
				}
			} else if (isPlanCheckReviewEngineer && !(isPlanCheckManager)) {
				engineerId = user.getUserId();
				pcIdBasedOnEngineer = planCheckAgent.getPCIdBasedOnEngineerID(engineerId, strActivityId, sorting);
				if(!(pcIdBasedOnEngineer != null && pcIdBasedOnEngineer.size()>0)){
					pcIdBasedOnEngineer.add(0, 0);
				}
			} else {
				pcIdBasedOnEngineer = planCheckAgent.getPCIdBasedOnDept(user.getDepartment().getDepartmentId(),
						strActivityId, sorting);
				if(!(pcIdBasedOnEngineer != null && pcIdBasedOnEngineer.size()>0)){
					pcIdBasedOnEngineer.add(0, 0);
				}
			}
          	 
          	request.setAttribute("plancheckId", plancheckId);
          	request.setAttribute("pcId", pcId);
          	request.setAttribute("pcIdBasedOnEngineer", pcIdBasedOnEngineer);
            logger.debug("Plan Checks are set to Plan check form");

            //It is to display the list of Plan Checks
           /* planCheckForm.setPlanChecksHistory(planCheckAgent.getPlanChecksHistory(StringUtils.s2i(strActivityId)));
            logger.debug("Plan Check History is set to Plan check form");*/
            
            
            planCheckForm.setPlanCheckDate(StringUtils.cal2str(Calendar.getInstance()));
            logger.debug("Plan Check Date: " + planCheckForm.getPlanCheckDate());
            planCheckForm.setTodaysDate(StringUtils.cal2str(Calendar.getInstance()));
            
            Calendar cal = Calendar.getInstance();           
            cal.add(Calendar.DAY_OF_YEAR, Constants.PC_DAYS_TO_ADD_REGULAR);
            planCheckForm.setTargetDate(StringUtils.cal2str(cal));
            
            planCheckForm.setCreated(StringUtils.cal2str(Calendar.getInstance()));
            
            planCheckForm.setEngineerId(StringUtils.i2s(planCheckAgent.getPlanReviewEngineer(strActivityId)));
            logger.debug("Plan Check Engineer: " + planCheckForm.getEngineerId());
            
           // planCheckForm.setPlanCheckStatus("");//+Constants.BUILDING_PLAN_CHECK_STATUS_PC_SUBMITTED);
            
            int moduleId = LookupAgent.getModuleId(LookupAgent.getActivityTypeForActId(strActivityId));
            request.setAttribute("moduleId", moduleId);

            List planCheckStatuses = null;
           // int deptId = LookupAgent.getDepartment(planCheckForm.getDepartment()).getDepartmentId();
           
            planCheckStatuses = LookupAgent.getPCStatuses();	//Constants.MODULEID_BUILDING
            

            request.setAttribute("planCheckStatuses", planCheckStatuses);
            
            List deptList = LookupAgent.getDepartmentListForPC();
            request.setAttribute("deptList", deptList);

            logger.debug("Plan check lenght:"+planCheck.length);
            planCheckForm.setPlanChecks(planCheck);
            
            request.setAttribute("planCheckForm", planCheckForm);
            session.setAttribute("planCheckForm", planCheckForm);
            request.setAttribute("activityId", strActivityId);
            request.setAttribute("psaId", psaId);
			request.setAttribute("psaType", psaType);
            
            request.setAttribute("planCheckProcessTypeList",new LookupAgent().getPlanCheckProcessTypes());
            
            logger.debug("Exit of  AddPlanCheckAction ....");
            return (mapping.findForward("success"));
        }
        catch (Exception e) {
            logger.error(e.getMessage());
            return (mapping.findForward("error"));
        }
       

    }
    
    private String getEngineerUsers(String deptDesc) throws Exception{
    	String engineerUsersStr = "";
    	String separator1 = "#@#";
    	String separator2 = "$@$";
    	StringBuffer sb = new StringBuffer("");
    	List pcEngineers = new PlanCheckAgent().getEngineerUsers(new LookupAgent().getDepartmentbasedOnId(deptDesc).getDepartmentId());
    	EngineerUser engineerUser = null;
    	if(pcEngineers != null && pcEngineers.size()>0){
    		for(int i=0;i<pcEngineers.size();i++){
    			engineerUser =(EngineerUser) pcEngineers.get(i);
    			sb.append(engineerUser.getUserId());
    			sb.append(separator1);
    			sb.append(engineerUser.getName());
    			sb.append(separator2);
    		}
    	}
    	engineerUsersStr = sb.toString();
    	if(engineerUsersStr.endsWith(separator2)){
    		engineerUsersStr = engineerUsersStr.substring(0,engineerUsersStr.lastIndexOf(separator2));
    	}
    	return engineerUsersStr;
    } 
    
    private String getPCStatus(String deptDesc) throws Exception{
    	String statusStr = "";
    	String separator1 = "#@#";
    	String separator2 = "$@$";
    	StringBuffer sb = new StringBuffer("");
        List planCheckStatuses = null;
        int deptId = LookupAgent.getDepartment(deptDesc).getDepartmentId();
        /*if(deptId == Constants.DEPARTMENT_CITY_CLERK){
        	planCheckStatuses = LookupAgent.getPlanCheckStatuses(Constants.MODULEID_CITY_CLERK);
        }else{
        	planCheckStatuses = LookupAgent.getPlanCheckStatuses(Constants.MODULEID_BUILDING);	
        }*/
        planCheckStatuses = LookupAgent.getPCStatuses();
        PlanCheckStatus planCheckStatus = null;
        
        if(planCheckStatuses != null && planCheckStatuses.size() > 0){
        	for(int i=0;i<planCheckStatuses.size();i++){
        	planCheckStatus = (PlanCheckStatus) planCheckStatuses.get(i);
        	sb.append(planCheckStatus.getCode());
        	sb.append(separator1);
        	sb.append(planCheckStatus.getDescription());
        	sb.append(separator2);
        	}
        }
        statusStr = sb.toString();
        if(statusStr.endsWith(separator2)){
        	statusStr = statusStr.substring(0,statusStr.lastIndexOf(separator2));
        }
    	return statusStr;
    }
}

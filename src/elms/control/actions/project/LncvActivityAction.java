package elms.control.actions.project;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom.Document;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.agent.ReportAgent;
import elms.gsearch.GlobalSearch;
import elms.util.Doc2Pdf;
import elms.util.StringUtils;

public class LncvActivityAction extends Action {
	static Logger logger = Logger.getLogger(LncvActivityAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
	
		int lsoId = -1;
		
		String forward ="success";

		HttpSession session = request.getSession();
		try {
			
			int activityId = Integer.parseInt((String) (request.getParameter("activityId")!=null? request.getParameter("activityId"):"0"));
			
			String action = (String) (request.getParameter("action")!=null? request.getParameter("action"):"");
			
			if(action.equalsIgnoreCase("update")){
				String lncvDate = (String) (request.getParameter("lncvDate")!=null? request.getParameter("lncvDate"):"");
				String oldlncvDate = (String) (request.getParameter("oldlncvDate")!=null? request.getParameter("oldlncvDate"):"");
				logger.info("**lncvDate*"+lncvDate);
				logger.info("**oldlncvDate*"+oldlncvDate);
				String msg= new ActivityAgent().updateParkingActivity(activityId,oldlncvDate,lncvDate);
				if(msg.startsWith("Success")){
					request.setAttribute("message", msg);
				}else {
					request.setAttribute("error", msg);
				}
				
			}
			
			if(action.equalsIgnoreCase("print")){
				
				String printval = (String) (request.getParameter("printval")!=null? request.getParameter("printval"):"");
				logger.info("**printval*"+printval);
				ReportAgent ra = new ReportAgent();
				Document document = null;
				String xslFileName = "";
				String actNbr = "";
				String pdfFileName = "";
				xslFileName = "lncv.xsl";
				actNbr = LookupAgent.getActivityNumberForActivityId(StringUtils.i2s(activityId));
				document = ra.getLNCVPermitJDOM(actNbr,printval);
				
				
				pdfFileName = actNbr + "-" + "LNCV" + "-" + StringUtils.cal2Ts(Calendar.getInstance()) + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);

				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					xslFileName = getRealPath + "xsl" + File.separator + xslFileName;
					pdfFileName = getRealPath + "pdf" + File.separator + pdfFileName;
					Doc2Pdf.start(document, xslFileName, pdfFileName);
				}
				forward ="print";
				
			}
			List lncvList = new ActivityAgent().getLNCVList(activityId);
			request.setAttribute("lncvList", lncvList);
			
			request.setAttribute("activityId", StringUtils.i2s(activityId));
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				}
			} catch (Exception e) {
				e.getMessage();
			}
			logger.info("Exiting LncvActivityAction");
		
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward(forward));
	}
}
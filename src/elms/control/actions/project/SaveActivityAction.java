package elms.control.actions.project;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.agent.PeopleAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.ActivityStatus;
import elms.app.admin.CustomField;
import elms.app.common.ProcessTeamRecord;
import elms.app.enforcement.CodeEnforcement;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.actions.inspection.InspectionValidationAction;
import elms.control.beans.ActivityForm;
import elms.control.beans.InspectionForm;
import elms.control.beans.ProcessTeamForm;
import elms.exception.AgentException;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class SaveActivityAction extends Action {
	/**
	 * The Logger.
	 */
	static Logger logger = Logger.getLogger(SaveActivityAction.class.getName());

	/**
	 * Protected variables.
	 */
	protected int activityId;
	protected int subProjectId;
	protected int projectId;
	protected Activity activity;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");

		if (user == null) {
			user = new User();
			System.out.println("user null");
		}

		subProjectId = StringUtils.s2i(request.getParameter("subProjectId"));
		logger.debug("Got parameter subProjectId  as " + subProjectId);
		projectId = new ProjectAgent().getProjectId(subProjectId);
		logger.debug("Got Project Id : " + projectId);

		ActivityForm activityForm = (ActivityForm) form;
		String aType = activityForm.getActivityType();
		logger.debug(aType);
		activityForm.setPlanCheckRequired(activityForm.isTempPlanCheckRequired());
		if (aType == null) {
			aType = "";
		}

		try {
			ActivityAgent activityAgent = new ActivityAgent();
			Wrapper db = new Wrapper();
			db.beginTransaction();
			activity = processRequest(subProjectId, activityForm, request);
			activityId = activityAgent.addActivity(activity);
			logger.debug("Added activity with Id : " + activityId);
			request.setAttribute("a", activity.getActivityDetail().getActivitySubTypes());
			
			 activityAgent.getUpdateStatus( activityId,activity,user.getUserId());
	            logger.debug("update status");

			/*
			 * Begin Adding owner to Activity
			 */
			if (activityId > 0) {
				// add owner to Activity
				if (activityForm.getOwnerFromCoActivitiesSelected()) {
					String[] people = { request.getParameter("ownerFromPeople.peopleId") };
					new PeopleAgent().assignPeople(people, StringUtils.i2s(activityId), "A");
				} else if (activityForm.getOwnerFromAssessorDataSelected()) {
					String ownerFromPeople = request.getParameter("ownerFromPeople.name");

					if (ownerFromPeople == null) {
						ownerFromPeople = "";
					}

					ownerFromPeople.trim();
					logger.debug("People name " + ownerFromPeople);

					String ownerFromAssessor = request.getParameter("ownerFromAssessor.name");

					if (ownerFromAssessor == null) {
						ownerFromAssessor = "";
					}

					logger.debug("Owner name " + ownerFromAssessor);
					ownerFromAssessor.trim();

					if (ownerFromPeople.toUpperCase().equals(ownerFromAssessor.toUpperCase())) {
						logger.debug("Both are equal : " + request.getParameter("ownerFromPeople.peopleId"));

						String[] people = { request.getParameter("ownerFromPeople.peopleId") };
						new PeopleAgent().assignPeople(people, StringUtils.i2s(activityId), "A");
					} else {
						PeopleAgent agent = new PeopleAgent();
						int peopleId = agent.checkAssessorExistsAsOwnerInPeople(ownerFromAssessor);
						String[] peopleIds = new String[1];

						if (peopleId > 0) {
							logger.debug("Assessor Owner Exists in People Data.");
							peopleIds[0] = StringUtils.i2s(peopleId);
						} else {
							People people = agent.addPeople(agent.getOwnerFromAssessorAsPeople(request.getParameter("ownerFromAssessor.ownerId")),user.getUserId());
							peopleIds[0] = StringUtils.i2s(peopleId);
						}

						agent.assignPeople(peopleIds, StringUtils.i2s(activityId), "A");
					}
				}
			}

			/*
			 * End adding owner to Activity
			 */

			// Custom fields start
			// To Be used later
			
			if (activityForm.getCustomFieldList().length > 0) {
				logger.debug("-----------------Entered Custom fields**************************8 ");
				CustomField[] customFieldList = activityForm.getCustomFieldList();
				new ActivityAgent().saveCustomActvityFields(activityId, customFieldList);
			}
			 
			// end custom fields

			activityForm.setActivityId(StringUtils.i2s(activityId));

			List sqls = new ArrayList();

			Iterator specItr = sqls.iterator();

			while (specItr.hasNext()) {
				String sql = (String) specItr.next();
				db.addBatch(sql);
			}

			// execute the batch;
			db.executeBatch();

			// Add Required condition for Activity
			String typeId = "";
			CommonAgent condAgent = new CommonAgent();
			typeId = condAgent.getActTypeId(aType);
			boolean added = condAgent.copyRequiredCondition(typeId, activityId, user);
			logger.debug("added new conditions" + added);

			// Add Process Team from project
			ProcessTeamForm processTeamForm = new ProcessTeamForm();
			processTeamForm.setPsaId(StringUtils.i2s(projectId));
			processTeamForm.setPsaType("P");
			processTeamForm = new CommonAgent().populateTeam(processTeamForm);
			logger.debug("after populateTeam : " + processTeamForm.getPsaId() + " and " + processTeamForm.getPsaType());
			processTeamForm.setPsaId(StringUtils.i2s(activityId));
			processTeamForm.setPsaType("A");

			ProcessTeamRecord tmRec = new ProcessTeamRecord();
			tmRec.setIsNew(true);
			tmRec.setLead("of");

			int projectNameId = -1;

			try {
				projectNameId = LookupAgent.getProjectNameId("A", activityId);
			} catch (Exception e5) {
				logger.error("Exception occured while getting project name id " + e5.getMessage());
			}

			logger.debug("Obtained project name id is" + projectNameId);

			if (projectNameId == Constants.PROJECT_NAME_PLANNING_ID) {
				User unassignedUser;

				try {
					unassignedUser = new AdminAgent().getUser("unassigned");
					tmRec.setNameId(StringUtils.i2s(unassignedUser.getUserId()));
					tmRec.setName(unassignedUser.getFirstName() + "  " + unassignedUser.getLastName());
					tmRec.setTitleId(StringUtils.i2s(Constants.GROUPS_PLANNER));
					new CommonAgent().saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);
				} catch (Exception e1) {
					throw new Exception("Unassigned user not found, please create this user");
				}
			}
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Inspection
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				}
			} catch (Exception e) {
				e.getMessage();
			}
			// view Activity
			ViewActivityAction viewActivityAction = new ViewActivityAction();
			viewActivityAction.getActivity(activityId, request);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());

			return (mapping.findForward("error"));
		}
	}

	public Activity processRequest(int subProjectId, ActivityForm activityForm, HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();

		// removing the activityForm from session which was kept in addActivity
		session.removeAttribute("activityForm");

		ActivityDetail activityDetail = new ActivityDetail();
		activityDetail.setActivityType(LookupAgent.getActivityType(activityForm.getActivityType()));
		logger.debug("activitytype is set to " + activityDetail.getActivityType().getDescription());
		logger.debug("ActivityType  :"+activityForm.getActivityType());
		activityDetail.setActivitySubTypes(StringUtils.arrayToList(activityForm.getActivitySubTypes()));
		logger.debug("setActivitySubTypes : " + StringUtils.arrayToList(activityForm.getActivitySubTypes()));

		activityDetail.setAddressId(StringUtils.s2i(activityForm.getAddress()));
		logger.debug("address is set to " + activityDetail.getAddressId());

		activityDetail.setDescription(activityForm.getDescription());
		logger.debug("description is set to " + activityDetail.getDescription());
		activityDetail.setStartDate(StringUtils.str2cal(activityForm.getStartDate()));
		logger.debug("Start Date is set to " + StringUtils.cal2str(activityDetail.getStartDate()));
		activityDetail.setIssueDate(StringUtils.str2cal(activityForm.getIssueDate()));
		logger.debug("Issue Date is set to " + StringUtils.cal2str(activityDetail.getIssueDate()));

		String tmpNewUnits = null;
		String tmpExistingUnits = null;
		String tmpDemolishedUnits = null;

		if (!GenericValidator.isBlankOrNull(activityForm.getNewUnits())) {
			tmpNewUnits = activityForm.getNewUnits();
		}

		if (!GenericValidator.isBlankOrNull(activityForm.getExistingsUnits())) {
			tmpExistingUnits = activityForm.getExistingsUnits();
		}

		if (!GenericValidator.isBlankOrNull(activityForm.getDemolishedUnits())) {
			tmpDemolishedUnits = activityForm.getDemolishedUnits();
		}

		activityDetail.setNewUnits(tmpNewUnits);
		logger.debug("newUnits is set to: " + activityDetail.getNewUnits());

		activityDetail.setExistingUnits(tmpExistingUnits);
		logger.debug("existingUnits set to: " + activityDetail.getExistingUnits());

		activityDetail.setDemolishedUnits(tmpDemolishedUnits);
		logger.debug("demolishUnits is set to : " + activityDetail.getDemolishedUnits());

		activityDetail.setExpirationDate(StringUtils.str2cal(activityForm.getExpirationDate()));
		logger.debug("Expiration Date is set to " + StringUtils.cal2str(activityDetail.getExpirationDate()));

		// override the expiration date if the activity status is PC Submitted (7) or Permit Issued (6)
		// this is a customization for burbank - May 12,2006 - Anand belaguly (anand@edgesoftinc.com)
		/*Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_YEAR, 365); // add 365 days to today's date.
		*/
		LookupAgent lookupAgent = new LookupAgent();
		String actType="";
		try {  
			System.out.println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&  "+activityDetail.getActivityType().getType());
		actType=lookupAgent.getActTypeId(activityDetail.getActivityType().getType());
		} catch (AgentException e) {
			logger.error(""+e);
			e.printStackTrace();
		}
		logger.debug("actType Id :"+actType);
	
		
		Calendar cal = StringUtils.str2cal(activityForm.getIssueDate());//Calendar.getInstance();
		if(cal!=null)
		{
			if( actType != null && (actType.equalsIgnoreCase(StringUtils.i2s(Constants.SINGLE_FAMILY_RESIDENTIAL_ACT_tYPE)) || actType.equals(StringUtils.i2s(Constants.MULTI_FAMILY_ACCESSORY_BUILDING_ACT_TYPE)) || actType.equals(StringUtils.i2s(Constants.Multi_Family_Addition_and_Alteration_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Multi_Family_Repair_and_Maintenance_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.New_Multi_Family_Dwelling_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.New_Single_Family_Dwelling_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Building_Combo_ACT_tYPE)) || actType.equals(StringUtils.i2s(Constants.Single_Family_Accessory_Building_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Single_Family_Addition_Alteration_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Single_Family_Repair_Maintenance_ACT_tYPE))|| actType.equals(StringUtils.i2s(Constants.Duplex_Apartment_Condominium_Townhouse_ACT_tYPE))  )){
	
				cal.add(Calendar.DATE, 365);
				
			
			}else{
				
				cal.add(Calendar.DATE, 180);   
			}
			
			if ((activityForm.getActivityStatus().equals("7")) || (activityForm.getActivityStatus().equals("6"))) {
				activityDetail.setExpirationDate(cal);
				logger.debug("Activity expiration overidden by business logic to " + StringUtils.cal2str(cal));
			}
			
		}

		activityDetail.setFinaledDate(StringUtils.str2cal(activityForm.getCompletionDate()));
		logger.debug("Completion Date is set to " + activityDetail.getFinaledDate());

		activityDetail.setStatus(new ActivityStatus(StringUtils.s2i(activityForm.getActivityStatus()), "", ""));
		logger.debug("status is  set to Pending is set to " + activityDetail.getStatus().getDescription());
		activityDetail.setValuation(StringUtils.$2dbl(activityForm.getValuation()));
		logger.debug("Valuation is set to " + activityDetail.getValuation());
		activityDetail.setPlanCheckRequired(StringUtils.b2s(activityForm.getPlanCheckRequired()));
		logger.debug("Plan Check Required is set to " + activityDetail.getPlanCheckRequired());
		activityDetail.setDevelopmentFeeRequired(StringUtils.b2s(activityForm.getDevelopmentFeeRequired()));
		logger.debug("dev fee Required is set to " + activityDetail.getDevelopmentFeeRequired());

		User user = (User) session.getAttribute("user");
		activityDetail.setCreatedBy(user);
		logger.debug("created user is set to " + activityDetail.getCreatedBy().getUserId());
		activityDetail.setUpdatedBy(user);
		logger.debug("updated user is set to " + activityDetail.getCreatedBy().getUserId());

		Activity activity = new Activity(0, activityDetail);
		logger.debug("activity Object is created with activityId and activity Details Object" + activity.toString());
		activity.setSubProjectId(subProjectId);
		logger.debug("subProjectId " + subProjectId);

		List selectedFees = new ArrayList();

		session.setAttribute("refreshTree", "Y");

		return activity;
	}
	
    public void addInspection(int activityId, int userId) throws Exception{
        int projectNameId = -1;
            projectNameId = LookupAgent.getProjectNameId("A", activityId);
            String actType = LookupAgent.getActivityTypeForActId(""+activityId);
            int moduleId = LookupAgent.getModuleId(actType);
            logger.debug("moduleId::" + moduleId);
         
          if(moduleId == Constants.MODULEID_NEIGHBORHOOD_SERVICES){
          if(projectNameId == Constants.NSD_PNAME_ID){
          InspectionForm frm = new InspectionForm();
          InspectionAgent inspectionAgent = new InspectionAgent();
          if(projectNameId==Constants.NSD_PNAME_ID)
          {
          frm.setActionCode(Constants.SYSTEMATIC_INSP_STAT_REQ_FOR_INSP);
          }else
          {
          	frm.setActionCode(StringUtils.i2s(Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION_RFS));
          }
          frm.setActivityId(activityId+"");
          frm.setInspectorId(Constants.INSPECTOR_UNASSIGNED);
          frm.setInspectionItem(Constants.INSPECTION_INITIAL_ITEM_CODE);
          /*
           * Default Inspection should be added for next available date and not for today's date
           * */
          String date = new InspectionValidationAction().addDaysToCurrentDate(1);
          frm.setInspectionDate(date);
          frm.setComments("Inspection Requested by systematic inspection");//Description
          frm.setUpdatedBy(userId);
          inspectionAgent.saveNewInspection(frm, "");
          
          CodeEnforcement frms = new CodeEnforcement();
          ActivityAgent activityAgent = new ActivityAgent();
          frms.setCeTypeId(Constants.CE_INSP_TYPE_ID_NEW);
          frms.setId(activityId); 
          frms.setOfficerId(Constants.INSPECTOR_UNASSIGNED);
          //frm.setInspectionItem(Constants.INSPECTION_INITIAL_ITEM_CODE);
          frms.setCeDate(StringUtils.cal2str(Calendar.getInstance()));
          //frm.setComments("Inspection Requested by systematic inspection");//Description
          activityAgent.saveNewCENotices(frms);
          }
        }else if(moduleId == Constants.MODULEID_FIRE){
        	
        	
        	/*
        	 * This is for CSIPROD-553
        	 * As per Irma, Fire Department. Doesn't want an inspection while creating an activity
        	 * 
        	 * But we need initial inspection for VMP
        	 * */
        	if(projectNameId != Constants.PROJECT_NAME_SYSTEMATIC_ID){
        		return;
        	}
            InspectionForm frm = new InspectionForm();
            InspectionAgent inspectionAgent = new InspectionAgent();
            
            if(projectNameId == Constants.PROJECT_NAME_SYSTEMATIC_ID){
            	if(actType != null && actType.equalsIgnoreCase(Constants.VMP_ACTIVITY_TYPE)){
            		frm.setActionCode(""+Constants.INSPECTION_CODES_VMP_REQUEST_FOR_INSPECTION);
            		frm.setComments("");//Description
            	}else{
            		frm.setActionCode(""+Constants.INSPECTION_CODES_FIRE_SYSTEMATIC_REQUEST_FOR_INSPECTION);
            		frm.setComments("Inspection Requested by systematic inspection");//Description
            	}
            }else if(projectNameId == Constants.PROJECT_NAME_PERMITS_ID){
            	frm.setActionCode(""+Constants.INSPECTION_CODES_FIRE_PERMIT_REQUEST_FOR_INSPECTION);	
            }else{
            	frm.setActionCode(""+Constants.INSPECTION_CODES_FIRE_CASES_REQUEST_FOR_INSPECTION);
            }
            
                       
            frm.setActivityId(activityId+"");
            frm.setInspectorId(Constants.INSPECTOR_UNASSIGNED);
            frm.setInspectionItem(""+Constants.INSPECTION_ITEM_VMP_INITIAL_INSPECTION);

            /*
             * Default Inspection should be added for next available date and not for today's date
             * */
            
            /*
             * For VMP Default Inspection should be added for May 1st of next year
             * */
            
            if(actType != null && actType.equalsIgnoreCase(Constants.VMP_ACTIVITY_TYPE)){
            	
            	SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
                String datestr = "";
                Calendar cl = Calendar.getInstance();
            	Date date = new Date();
                /*
                if(cl.get(Calendar.MONTH) >= Calendar.MAY){
                	cl.set(cl.get(Calendar.YEAR)+1, Calendar.MAY, 01);
                	date.setTime(cl.getTimeInMillis());
                	datestr = sdf.format(date);
                }
                */
               // if(cl.get(Calendar.MONTH) < Calendar.MAY){
                	cl.set(cl.get(Calendar.YEAR),Calendar.MAY, 01);
                	date.setTime(cl.getTimeInMillis());
                	datestr = sdf.format(date);
                //}
                frm.setInspectionDate(datestr);
        	}else{
                String date = new InspectionValidationAction().addDaysToCurrentDate(1);
                frm.setInspectionDate(date);
        	}
            
            
            frm.setUpdatedBy(userId);
            inspectionAgent.saveNewInspection(frm, "");
        }
    }
}

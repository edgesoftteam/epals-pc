package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.ProjectAgent;
import elms.app.project.Activity;
import elms.app.project.Project;
import elms.app.project.SubProject;
import elms.common.Constants;
import elms.util.StringUtils;
import elms.util.Util;

public class ViewPSAAction extends Action {
	static Logger logger = Logger.getLogger(ViewPSAAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if(Util.reloginRequired(request)){
			return (mapping.findForward("logon"));
		}
		HttpSession session = request.getSession();
		String nextPage = "activity";
		int lsoId = 0;
		int levelId = 0;
		String levelType = "";
		String lsoAddress = "";
		String psaInfo = "";

		logger.info("Entering ViewPSAAction");

		try {
			// Refresh the lso tree when view page is called only the second time.
			// --------------------------------------------------
			session.setAttribute("refreshTree", "Y");
			session.removeAttribute("siteStructureForm");
			session.removeAttribute("siteUnitForm");

			// --------------------------------------------------
			// Getting required values from the request Object
			if (request.getParameter("levelId") == null) {
				levelId = StringUtils.s2i((String) request.getAttribute("levelId"));
				levelType = (String) request.getAttribute("levelType");
				lsoId = StringUtils.s2i((String) request.getAttribute("lsoId"));
			} else {
				levelId = StringUtils.s2i(request.getParameter("levelId"));
				levelType = request.getParameter("levelType");
				lsoId = StringUtils.s2i(request.getParameter("lsoId"));
			}

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			session.setAttribute(Constants.PSA_ID, StringUtils.i2s(levelId));
			session.setAttribute(Constants.PSA_TYPE, levelType);
			session.setAttribute(Constants.LSO_ID, StringUtils.i2s(lsoId));

			if (levelType.equals("A")) {
				request.setAttribute("activityId", "" + levelId);

				ActivityAgent actAgent = new ActivityAgent();
				Activity activity = actAgent.getActivityLite(levelId);
				lsoAddress = activity.getActivityDetail().getAddress();
				psaInfo = ""; // activity.getActivityDetail().getDescription();
				nextPage = "activity";
			} else if (levelType.equals("Q")) {
				request.setAttribute("subProjectId", "" + levelId);

				ProjectAgent subProjectAgent = new ProjectAgent();
				SubProject subProject = subProjectAgent.getSubProject(levelId, "Y");
				psaInfo = ""; // subProject.getSubProjectDetail().getDescription();
				lsoAddress = subProject.getSubProjectDetail().getAddress();
				nextPage = "subProject";
								
			} else {
				request.setAttribute("projectId", "" + levelId);

				ProjectAgent projectAgent = new ProjectAgent();
				Project project = projectAgent.getProject(levelId);
				psaInfo = ""; // project.getProjectDetail().getDescription();
				lsoAddress = project.getProjectDetail().getAddress();
				nextPage = "project";
			}

			session.setAttribute("psaInfo", psaInfo);
			session.setAttribute("lsoAddress", lsoAddress);

			logger.debug("lsoAddress is" + lsoAddress);
			logger.debug("psaInfo is " + psaInfo);
			logger.debug("nextpage is " + nextPage);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException("Exception occured " + e.getMessage());
		}

		logger.debug(levelType + ":" + levelId + ":" + lsoId + ":" + lsoAddress + ":" + nextPage);
		logger.debug("Exiting ViewPSAAction");

		return (mapping.findForward(nextPage));
	}
}
 

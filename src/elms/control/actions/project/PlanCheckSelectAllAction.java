package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.app.project.PlanCheck;
import elms.app.project.PlanCheckEngineer;
import elms.control.beans.project.PlanCheckReassignForm;

public class PlanCheckSelectAllAction extends Action {

	static Logger logger = Logger.getLogger(PlanCheckSelectAllAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		org.apache.struts.util.MessageResources messages = getResources();
		HttpSession session = request.getSession();
		logger.info("Entering PlanCheckSelectAllAction");
		PlanCheckReassignForm planCheckReassignForm = (PlanCheckReassignForm) form;
		try {
			PlanCheckEngineer pcEngineerArray[] = planCheckReassignForm.getPcEngineerArray();
			for (int i = 0; i < pcEngineerArray.length; i++)
				if (pcEngineerArray[i].getSelectAllCheckBox().equals("true")) {
					PlanCheck planCheck[] = pcEngineerArray[i].getPlanCheckArray();
					for (int j = 0; j < planCheck.length; j++)
						planCheck[j].setCheck("on");

				} else {
					PlanCheck planCheck[] = pcEngineerArray[i].getPlanCheckArray();
					for (int j = 0; j < planCheck.length; j++)
						planCheck[j].setCheck("false");

				}

			planCheckReassignForm.setPcEngineerArray(pcEngineerArray);
			session.setAttribute("plannerUpdateForm", planCheckReassignForm);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return mapping.findForward("success");
	}

}
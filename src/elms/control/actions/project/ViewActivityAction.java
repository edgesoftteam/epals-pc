package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.ActivityAgent;
import elms.agent.FinanceAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.agent.PeopleAgent;
								 
import elms.agent.ProjectAgent;
import elms.app.admin.ActivityStatus;
import elms.app.admin.MicrofilmStatus;
import elms.app.bl.BusinessLicenseActivity;
import elms.app.bt.BusinessTaxActivity;
import elms.app.common.Condition;
import elms.app.common.ProcessTeamRecord;
import elms.app.finance.FinanceSummary;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.app.project.PlanCheck;
import elms.common.Constants;
import elms.control.beans.ConditionForm;
										
import elms.control.beans.ProcessTeamForm;
import elms.security.Group;
import elms.security.User;
import elms.util.StringUtils;

public class ViewActivityAction extends Action {
	static Logger logger = Logger.getLogger(ViewActivityAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		int activityId = -1;
		int lsoId = -1;
		List activityTeamList = null;

		HttpSession session = request.getSession();
		try {

			// get the user and user groups.
			User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
			boolean businessLicenseUser = false, businessLicenseApproval = false;
			List groups = user.getGroups();
			Iterator itr = groups.iterator();
			while (itr.hasNext()) {
				Group group = (elms.security.Group) itr.next();
				if (group == null)
					group = new elms.security.Group();
				if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER)
					businessLicenseUser = true;
				if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL)
					businessLicenseApproval = true;
			}
			logger.debug("business License user " + businessLicenseUser);
			logger.debug("business License approval " + businessLicenseApproval);

			// get activity id from request as parameter or as
			activityId = Integer.parseInt((request.getParameter("activityId") != null) ? request.getParameter("activityId") : (String) request.getAttribute("activityId"));
			logger.debug("Entered Into the ViewActivityAction With activity Id : " + activityId);

			String activityNumber = new ActivityAgent().getActivtiyNumber(activityId);
			logger.debug("Activity number is " + activityNumber);
			ActivityAgent activityAgent = new ActivityAgent();

			if (activityNumber.trim().startsWith("BL")) {
				activityTeamList = activityAgent.getActivityTeamList(activityId, 3);
			} else if (activityNumber.trim().startsWith("BT")) {
				// added for Business Tax
				activityTeamList = activityAgent.getBTActivityTeamList(activityId, 3);
			}

			request.setAttribute("activityTeamList", activityTeamList);

			String projectName = LookupAgent.getProjectNameForActivityNumber(activityNumber);
			lsoId = LookupAgent.getLsoIdForPsaId("" + activityId, "A");
			logger.debug("lsoId is " + lsoId);

			BusinessLicenseActivity businessLicenseActivity = new ActivityAgent().getBusinessLicenseActivity(activityId, lsoId);
			BusinessTaxActivity businessTaxActivity = new ActivityAgent().getBusinessTaxActivity(activityId, lsoId);
			session.removeAttribute("sTypeId");// for online
			session.removeAttribute("online");
			request.setAttribute("businessLicenseActivity", businessLicenseActivity);
			request.setAttribute("businessTaxActivity", businessTaxActivity);
			request.setAttribute("activityId", StringUtils.i2s(activityId));
			request.setAttribute("lsoId", StringUtils.i2s(lsoId));
			getActivity(activityId, request);

			String levelType = LookupAgent.getLSOType(lsoId);
			if (levelType.equalsIgnoreCase("O")) {
				request.setAttribute("levelType", levelType);
				logger.debug("levelType is " + levelType);

			}

			return (mapping.findForward("success"));

		} catch (Exception e) {
			logger.error(e.getMessage());

			return (mapping.findForward("error"));
		}
	}

	/**
	 * Gets the activity details from the request for a given activity id.
	 * 
	 * @param activityId
	 * @param request
	 * @throws Exception
	 */
	public void getActivity(int activityId, HttpServletRequest request) throws Exception {
		Activity activity = null;
		ActivityDetail activityDetail = null;

		try {
			// The http session
			HttpSession session = request.getSession();
			boolean editable = true;
			boolean activityInactiveEditPermission = false;

			logger.debug("LSO_ID is set inthe session as  " + (String) session.getAttribute(Constants.LSO_ID));

			List groups = new ArrayList();
			elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);

			if (user != null) {
				groups = (List) user.getGroups();
			}

			java.util.Iterator itr = groups.iterator();

			while (itr.hasNext()) {
				elms.security.Group group = (elms.security.Group) itr.next();

				if (group == null) {
					group = new elms.security.Group();
				}

				if (group.groupId == 5) {
					activityInactiveEditPermission = true;
				}

				logger.debug("The Group iDs are" + group.groupId);
			}

			request.setAttribute("activityInactiveEditPermission", new Boolean(activityInactiveEditPermission));

			// check for the session lso address
			String lsoAddress = (String) session.getAttribute("lsoAddress");
			logger.debug("The lso address from session is " + lsoAddress);

			ActivityAgent activityAgent = new ActivityAgent();
			lsoAddress = activityAgent.getActivityAddressForId(activityId);
			session.setAttribute("lsoAddress", lsoAddress);
			logger.debug("Set the lso address to session as " + lsoAddress);

			session.removeAttribute("siteStructureForm");
			session.removeAttribute("siteUnitForm");

			// Setting the PSA_ID,PSA_TYPE in the session;
			String refreshTree = ((String) session.getAttribute("refreshTree") != null) ? (String) session.getAttribute("refreshTree") : "";
			session.setAttribute(Constants.PSA_ID, StringUtils.i2s(activityId));
			logger.debug("set the psa type into session under Constants.PSA_ID as " + (String) session.getAttribute(Constants.PSA_ID));
			session.setAttribute(Constants.PSA_TYPE, "A");
			logger.debug("set the psa type into session under Constants.PSA_TYPE as " + (String) session.getAttribute(Constants.PSA_TYPE));

			// need to refresh the PSA tree so onloadAction is setting to
			String onloadAction = "";

			// lso tree refresh
			if (refreshTree.equals("Y")) {
				onloadAction = "parent.f_lso.location.href='" + request.getContextPath() + "/lsoSearchWithLso.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "';" + "parent.f_psa.location.href='" + request.getContextPath() + "/viewPsaTree.do?lsoId=" + (String) session.getAttribute(Constants.LSO_ID) + "&psaNodeId=" + activityId + "'";
				logger.debug("Setting the refresh tree flag to " + onloadAction);

				session.setAttribute("onloadAction", onloadAction);
				session.setAttribute("refreshTree", "N");
			}

			activity = new ActivityAgent().getActivity(activityId);

			if (activity == null) {
				logger.debug("Null Activity Object is created with activityId .." + activityId);
			} else {
				activityDetail = activity.getActivityDetail();
			}

			if (activityDetail == null) {
				logger.debug("Null ActivityDetail  Object is created from activity  Object ..");
			} else {
				request.setAttribute("activityId", StringUtils.i2s(activity.getActivityId()));
				logger.debug("activityId:" + request.getAttribute("activityId"));
				request.setAttribute("subProjectId", StringUtils.i2s(activity.getSubProjectId()));
				logger.debug("subprojectId:" + request.getAttribute("subProjectId"));
				request.setAttribute("activityNumber", activityDetail.getActivityNumber());

				String psaInfo = " -- " + activityDetail.getActivityNumber();
				String actNumber = activityDetail.getActivityNumber();
				logger.debug("activityNumber:" + request.getAttribute("activityNumber"));

				if (activityDetail.getActivityType() != null) {
					request.setAttribute("activityType", activityDetail.getActivityType().getType());
					request.setAttribute("activityTypeDesc", activityDetail.getActivityType().getDescription());
					psaInfo = psaInfo + " - " + activityDetail.getActivityType().getDescription();
					logger.debug("activityType:" + request.getAttribute("activityType"));
					request.setAttribute("completionDateLabel", activityDetail.getFinalDateLabel());
					logger.debug("Completion Date Label is set to request  as  " + activityDetail.getFinalDateLabel());
					request.setAttribute("expirationDateLabel", activityDetail.getExpirationDateLabel());
					logger.debug("Expiration Date Label is set to request  as  " + activityDetail.getFinalDateLabel());
				}

				logger.debug("entering activityDetail.getActivitySubTypes");

				if (activityDetail.getActivitySubTypes() != null) {
					logger.debug("entering activityDetail.getActivitySubType" + activityDetail.getActivitySubTypes());

					request.setAttribute("activitySubTypes", activityDetail.getActivitySubTypes());

				}
				List customFieldsList;
				// custom Fields
				customFieldsList = new ActivityAgent().getCustomFieldActivityList(activityId, new ActivityAgent().getActivityTypeId(activityDetail.getActivityType().getType()));
				request.setAttribute("customFieldsList", customFieldsList);

				session.setAttribute("psaInfo", psaInfo);
				session.setAttribute("actNumber", actNumber);
				request.setAttribute("description", activityDetail.getDescription());
				logger.debug("description:" + request.getAttribute("description"));

				if (activityDetail.getAddress() == null) {
					request.setAttribute("address", "");
				} else {
					request.setAttribute("address", activityDetail.getAddress());
				}

				logger.debug("address:" + request.getAttribute("address"));

				// setting addressId in constants
				session.setAttribute(Constants.ADDR_ID, StringUtils.i2s(activityDetail.getAddressId()));
				request.setAttribute("valuation", StringUtils.dbl2$(activityDetail.getValuation()));
				logger.debug("Valuation:" + request.getAttribute("valuation"));
				request.setAttribute("planCheckRequired", activityDetail.getPlanCheckRequired());
				logger.debug("planCheckRequired:" + request.getAttribute("planCheckRequired"));

				request.setAttribute("developmentFeeRequired", activityDetail.getDevelopmentFeeRequired());
				logger.debug("developmentFeeRequired:" + request.getAttribute("developmentFeeRequired"));

				request.setAttribute("inspectionRequired", activityDetail.getInspectionRequired());
				logger.debug("inspectionRequired:" + request.getAttribute("inspectionRequired"));
				
				request.setAttribute("greenHalo", activityDetail.getGreenHalo());
				logger.debug("greenHalo:" + request.getAttribute("greenHalo"));

				request.setAttribute("extRefNumber", activityDetail.getExtRefNumber());
				logger.debug("extRefNumber:" + request.getAttribute("extRefNumber"));

				ActivityStatus activityStatus = activityDetail.getStatus();

				if (activityStatus != null) {
					request.setAttribute("status", activityStatus.getDescription());
				}

				logger.debug("status is set to Request as " + request.getAttribute("status"));
				request.setAttribute("startDate", StringUtils.cal2str(activityDetail.getStartDate()));
				logger.debug("startDate:" + request.getAttribute("startDate"));
				request.setAttribute("appliedDate", StringUtils.cal2str(activityDetail.getAppliedDate()));
				logger.debug("appliedDate:" + request.getAttribute("appliedDate"));
				request.setAttribute("issueDate", StringUtils.cal2str(activityDetail.getIssueDate()));
				logger.debug("issue:" + request.getAttribute("issueDate"));
				request.setAttribute("expirationDate", StringUtils.cal2str(activityDetail.getExpirationDate()));
				logger.debug("expirationDate:" + request.getAttribute("expirationDate"));
				request.setAttribute("completionDate", StringUtils.cal2str(activityDetail.getFinaledDate()));
				logger.debug("completionDate:" + request.getAttribute("completionDate"));
				request.setAttribute("planCheckFeeDate", StringUtils.cal2str(activityDetail.getPlanCheckFeeDate()));
				logger.debug("planCheckFeeDate:" + request.getAttribute("planCheckFeeDate"));
				request.setAttribute("permitFeeDate", StringUtils.cal2str(activityDetail.getPermitFeeDate()));
				logger.debug("permitFeeDate:" + request.getAttribute("permitFeeDate"));

				request.setAttribute("newUnits", activityDetail.getNewUnits());
				logger.debug("newUnits:" + request.getAttribute("newUnits"));

				request.setAttribute("existingUnits", activityDetail.getExistingUnits());
				logger.debug("existingUnits:" + request.getAttribute("existingUnits"));

				request.setAttribute("demolishedUnits", activityDetail.getDemolishedUnits());
				logger.debug("demolishedUnits:" + request.getAttribute("demolishedUnits"));

				MicrofilmStatus status = LookupAgent.getMicrofilmStatus(activityDetail.getMicrofilm());

				request.setAttribute("microfilm", status.getDescription());
				logger.debug("microFilm:" + request.getAttribute("microFilm"));

				String createdBy = "";

				if (activityDetail.getCreatedBy() != null) {
					String firstName = "";
					String lastName = "";

					if (activityDetail.getCreatedBy().getFirstName() != null) {
						firstName = activityDetail.getCreatedBy().getFirstName();
					}

					if (activityDetail.getCreatedBy().getLastName() != null) {
						lastName = activityDetail.getCreatedBy().getLastName();
					}

					createdBy = firstName + " " + lastName;
				}

				request.setAttribute("createdBy", createdBy);
				
				request.setAttribute("onlineApplication", activityDetail.getOnlineApplication());

				String label = activityDetail.getLabel();
				logger.debug("label =" + label);
				request.setAttribute("label", label);

				String parkingZoneDescription = activityDetail.getParkingZone() != null ? activityDetail.getParkingZone().getName() : "";
				logger.debug("parkingZoneDescription =" + parkingZoneDescription);
				request.setAttribute("parkingZoneDescription", parkingZoneDescription);

				logger.debug("createdBy:" + request.getAttribute("createdBy"));
			}

			// setting the People List
			List peopleList = new PeopleAgent().getPeoples(activityId, "A", 3);
			request.setAttribute("peopleList", peopleList);
			logger.debug("set people List to request with size " + peopleList.size());

			// Getting Latest Plan check status and date
													 
			PlanCheck latestPlanCheck = new ActivityAgent().getLatestPlanCheck(activityId);
																				
													   
												 

			String disableFees = "N";

			if ((latestPlanCheck == null) || (latestPlanCheck.getPlanCheckId() == 0)) {
				latestPlanCheck = new PlanCheck();

				if ((activityDetail.getPlanCheckRequired() == null) || activityDetail.getPlanCheckRequired().equals("Y")) {
					disableFees = "Y";
				}
			}

			String projectName = LookupAgent.getProjectNameForActivityId("" + activityId);

			if ((projectName.equalsIgnoreCase(Constants.PROJECT_NAME_BUILDING)) || (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_PUBLIC_WORKS))) {
				request.setAttribute("disableFees", disableFees);
				logger.debug("disableFees:" + disableFees);
			} else {
				disableFees = "N";
				request.setAttribute("disableFees", disableFees);
			}

			request.setAttribute("latestPlanCheck", latestPlanCheck);

			// setting Holdlist
			List holds = new ArrayList();
			holds = activity.getPsaHold();
			request.setAttribute("holds", holds);
			logger.debug("set Holds to request with size " + holds.size());

			if (!(new CommonAgent().editable(activityId, "A"))) {
				editable = false;
			}

			// setting Notices list
			List notices = new ArrayList();
			notices = activity.getPsaNotice();
			request.setAttribute("notices", notices);
			logger.debug("set notices to request with size " + notices.size());

			// setting Tenant List
			List tenants = new ArrayList();

			tenants = activity.getPsaTenant();
			request.setAttribute("tenants", tenants);
			logger.debug("set tenants to request with size " + tenants.size());

			// list of attachments for this activity.
			List attachmentList = new CommonAgent().getAttachments(activityId, "A");
			request.setAttribute("attachmentList", attachmentList);

			List lncvList = new ActivityAgent().getLNCVInternalList(activityId);
			request.setAttribute("lncvList", lncvList);

			// list of comments for this activity.
			List commentList = new CommonAgent().getCommentList(activityId, 3, "A");
			request.setAttribute("commentList", commentList);

			// list of inspections for this activity
			List inspectionList = new InspectionAgent().getInspectionList(new Integer(activityId).toString(), 3);
			request.setAttribute("inspectionList", inspectionList);

			// finance for this activity
			FinanceSummary financeSummary = new FinanceAgent().getActivityFinance(activityId);
			request.setAttribute("financeSummary", financeSummary);

			// ProcessTeam List
			ProcessTeamForm processForm = new ProcessTeamForm();
			CommonAgent teamAgent = new CommonAgent();
			processForm.setPsaId(StringUtils.i2s(activityId));
			processForm.setPsaType("A");
			processForm = teamAgent.populateTeam(processForm);

			ProcessTeamRecord[] processTeamRecord = processForm.getProcessTeamRecord();
			List processList = Arrays.asList(processTeamRecord);

			if (processList.size() > 3) {
				processList = processList.subList(0, 3);
			}

			request.setAttribute("processList", processList);

			List linkedActivityList = ProjectAgent.getLinkedActivityList(activityId);
			request.setAttribute("linkedActivityList", linkedActivityList);
			logger.debug("size of Linked Activity List : " + linkedActivityList.size());

			// Custom Fileds
			logger.debug("         :::::::::         Custom Fields        ::::::::::::::         ");

			List customFieldsList;
			// custom Fields
			customFieldsList = new ActivityAgent().getCustomFieldActivityList(activityId, "ACTIVITY", activity.getActivityDetail().getActivityType().getTypeId());
			request.setAttribute("customFieldsList", customFieldsList);

			// Condition List
			ConditionForm condForm = new ConditionForm();
			CommonAgent condAgent = new CommonAgent();
			condForm.setLevelId(activityId);
			condForm.setConditionLevel("A");
			condForm = condAgent.populateConditionForm(condForm);

			Condition[] conditions = condForm.getCondition();
			List condList = Arrays.asList(conditions);

			if (condList.size() > 3) {
				condList = condList.subList(0, 3);
			}

			request.setAttribute("condList", condList);

			logger.debug("The size of the cond List is " + condList.size());
			request.setAttribute("totalConditionCount", StringUtils.i2s(condAgent.getConditionCount(activityId, "A")));
			logger.debug("The Total Condition Count is set as  " + condAgent.getConditionCount(activityId, "A"));
			request.setAttribute("warnConditionCount", StringUtils.i2s(condAgent.getWarningConditionCount(activityId, "A")));
			logger.debug("The Warning Condition Count is set as  " + condAgent.getWarningConditionCount(activityId, "A"));

			if (editable) {
				request.setAttribute("editable", "true");
			} else {
				request.setAttribute("editable", "false");
			}

			request.setAttribute("activity", activity);
			logger.info("Exiting ViewActivityAction");
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw e;
		}
	}
}

package elms.control.actions.project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.beans.project.CopyActivityForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveCopyActivityAction extends Action {

	static Logger logger = Logger.getLogger(SaveCopyActivityAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering SaveCopyActivityAction");

		try {

			HttpSession session = request.getSession();
			CopyActivityForm frm = (CopyActivityForm) form;
			int activityId = StringUtils.s2i(request.getParameter("oldActivityId"));
			int userId = ((User) session.getAttribute("user")).getUserId();
			frm.getCopyActivity().setActivity(new Activity(activityId, new ActivityDetail()));
			frm.getCopyActivity().setUserId(userId);
			ActivityAgent agent = new ActivityAgent();
			activityId = agent.performCopyActivity(frm.getCopyActivity());

			logger.debug("New Activity Created: " + activityId);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for inspection
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PLANCHECK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for plan check
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				}
			} catch (Exception e) {
				e.getMessage();
			}
			request.setAttribute("lsoId", session.getAttribute(Constants.LSO_ID));
			request.setAttribute("levelId", "" + activityId);
			request.setAttribute("levelType", "A");

			logger.info("Exiting SaveCopyActivityAction");

			return (mapping.findForward("success"));
		} catch (Exception e) {

			return (mapping.findForward("error"));
		}
	}
}

package elms.control.actions.project;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AddressAgent;
import elms.agent.Epals311Agent;
import elms.agent.FinanceAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivityType;
import elms.app.admin.CustomField;
import elms.app.admin.ParkingZone;
import elms.app.finance.ActivityFinance;
import elms.app.finance.FinanceSummary;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.beans.ActivityForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class SaveActivityDetailsAction extends Action {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(SaveActivityDetailsAction.class.getName());
	protected int activityId = -1;
	protected int subProjectId = -1;
	protected Activity activity;
	protected ActivityDetail activityDetail;
	protected ActivityType activityType;
	protected People people;
	protected ActivityFinance activityFinance;
	protected List activityTeam;
	protected List planChecks;
	protected List inspections;
	protected String sActivityType;
	protected String nextPage = "success";
	String outstandingFees = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered Into SaveActivityDetailsAction ....");

		ActivityForm activityForm = (ActivityForm) form;
		ActionErrors errors = new ActionErrors();

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		// Checking for outstanding fees for the Activity before 'final'
		FinanceSummary financeSummary;

		try {
			financeSummary = new FinanceAgent().getActivityFinance(activityId);
		} catch (Exception e1) {
			throw new ServletException(e1.getMessage());
		}

		if (financeSummary == null) {
			financeSummary = new FinanceSummary();
		}

		String amountDue = financeSummary.getTotalAmountDue();
		float fAmount = StringUtils.s2bd(amountDue).floatValue();
		logger.debug("amount due ### :" + fAmount);

		if (fAmount <= 0) {
			outstandingFees = "N";
		} else {
			outstandingFees = "Y";
		}

		if ((activityForm.getActivityStatus().equalsIgnoreCase("4")) && (outstandingFees.equals("Y"))) {
			errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activity.finalWithPaymentDue"));
			logger.debug("do final error");
		}

		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);

			return (new ActionForward(mapping.getInput()));
		} else {
			activityId = StringUtils.s2i(request.getParameter("activityId"));

			String aType = request.getParameter("activityType");

			if (aType == null) {
				aType = "";
			}

			try {
				ActivityAgent activityAgent = new ActivityAgent();
				Wrapper db = new Wrapper();
				db.beginTransaction();

				activity = processRequest(activityForm, request);

				String sql = activityAgent.getSaveActivityDetailsSql(activity);
				

				logger.debug("old Status::::" + activityForm.getOldActivityStatus());
				logger.debug("new Status::::" + activityForm.getActivityStatus());
				if (!activityForm.getOldActivityStatus().equalsIgnoreCase(activityForm.getActivityStatus())) {
					/*
					 * 
					 * We need to make an entry in status history only if there is a change in activity status
					 */
					activityAgent.getUpdateStatusPlancheck(activityId, activity, user.getUserId());
				}

				logger.debug("update status");

				// update sql for activity
				db.addBatch(sql);
				
				if(activityForm.getActivitySubTypes().length > 0){
					sql = "DELETE FROM ACT_SUBTYPE WHERE ACT_ID = "+activityId;
					db.addBatch(sql);
					String subTypeArr[] = activityForm.getActivitySubTypes();
					for(int subTypeIndex = 0 ; subTypeIndex < subTypeArr.length ; subTypeIndex++){
						if(!subTypeArr[subTypeIndex].equalsIgnoreCase("0")){
							sql = "INSERT INTO ACT_SUBTYPE VALUES ("+activityId +" , "+subTypeArr[subTypeIndex]+")";
							db.addBatch(sql);
						}
					}
				}
								
				// getting sql for special permits
				request.setAttribute("actionType", "Update ");
				request.setAttribute("activityNumber", new ActivityAgent().getActivtiyNumber(activityId));

				List sqls = new ArrayList();

				Iterator itr = sqls.iterator();

				while (itr.hasNext()) {
					sql = (String) itr.next();
					db.addBatch(sql);
				}

				db.executeBatch();

				// Custom fields start

				if (activityForm.getCustomFieldList().length > 0) {
					logger.debug("-----------------Entered EDIT Custom fields**************************");
					CustomField[] customFieldList = activityForm.getCustomFieldList();
					new ActivityAgent().saveEditedCustomActvityFields(activityId, customFieldList);
				}
				// end custom fields
				try {
					String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
					if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
					GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
					GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
					}
				} catch (Exception e) {
					e.getMessage();
				}
				ViewActivityAction viewActivityAction = new ViewActivityAction();
				viewActivityAction.getActivity(activityId, request);
				
				
				logger.debug("Activity type : "+activity.getActivityDetail().getActivityType().getType());
				if(activity.getActivityDetail().getActivityType().getType().equalsIgnoreCase(Constants.THREE11_SERVICE_ACT_TYPE))
					Epals311Agent.updateCaseIn311System(user,activity);

				return (mapping.findForward("success"));
			} catch (Exception e) {
				logger.error("Error in Save Activity Detail .." + e.getMessage());

				return (mapping.findForward("error"));
			}
		}
	}

	public Activity processRequest(ActivityForm activityForm, HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		activityId = StringUtils.s2i(activityForm.getActivityId());
		logger.debug("SaveActivityDetailsAction -- got parameter activityId  as " + activityId);

		// session.setAttribute("onloadAction",onloadAction);
		session.setAttribute("refreshTree", "Y");

		activityDetail = new ActivityDetail();
		activityDetail.setValuation(StringUtils.s2d(activityForm.getValuation()));
		logger.debug("SaveActivityDetailsAction -- activityDetail -- Valuation is set to " + activityDetail.getValuation());
		activityDetail.setActivityType(LookupAgent.getActivityType(activityForm.getActivityType()));
		logger.debug("SaveActivityDetailsAction -- activityDetail -- Activity Type is set to " + activityDetail.getActivityType().getDescription());
		
		System.out.println("&&&&&&&&&&&&&&&&&&&&& " +activityDetail.getActivityType().getType());
		sActivityType = activityForm.getActivityType();
		LookupAgent la=new LookupAgent();   
		int actTypeId = Integer.parseInt(la.getActTypeId(activityDetail.getActivityType().getType()));      
		logger.debug("SaveActivityDetailsAction -- activityType String  --  is set to " + sActivityType);
		activityDetail.setAddressId(StringUtils.s2i(activityForm.getAddress()));
		logger.debug("SaveActivityDetailsAction -- activityDetail -- addressId is set to " + activityDetail.getAddressId());

		// Activity status
		String actStatus = activityForm.getActivityStatus();

		if (actStatus.equals("")) {
			activityDetail.setStatus(new ActivityStatus());
		} else {
			activityDetail.setStatus(LookupAgent.getActivityStatus(StringUtils.s2i(actStatus)));
		}

		logger.debug("SaveActivityDetailsAction -- activityDetail -- Activity Type is set to " + activityDetail.getStatus());

		activityDetail.setIssueDate(StringUtils.str2cal(activityForm.getIssueDate()));
		logger.debug("SaveActivityDetailsAction -- activityDetail -- Issue Date is set to " + activityDetail.getIssueDate());

		activityDetail.setStartDate(StringUtils.str2cal(activityForm.getStartDate()));
		logger.debug("SaveActivityDetailsAction -- activityDetail --Start Date is set to " + activityDetail.getStartDate());

		activityDetail.setExpirationDate(StringUtils.str2cal(activityForm.getExpirationDate()));
		logger.debug("SaveActivityDetailsAction -- activityDetail --Expiration Date is set to " + StringUtils.cal2str(activityDetail.getExpirationDate()));

		
		
		Calendar cal = StringUtils.str2cal(activityForm.getIssueDate());//Calendar.getInstance();
		
		cal = Calendar.getInstance();
		
		
		// This is as per Taj's email on 03/02/2023
		if ((activityForm.getActivityStatus().equals(StringUtils.i2s(Constants.ACTIVITY_STATUS_BUILDING_PC_SUBMITTED)))) {
			cal.add(Calendar.DATE, 180);
			activityDetail.setExpirationDate(cal);
			logger.debug("Activity expiration overidden by business logic to " + StringUtils.cal2str(cal));
		}else if(activityForm.getActivityStatus().equals(StringUtils.i2s(Constants.ACTIVITY_STATUS_BUILDING_PERMIT_ISSUED))){
			cal.add(Calendar.DATE, 365);
			activityDetail.setExpirationDate(cal);
			logger.debug("Activity expiration overidden by business logic to " + StringUtils.cal2str(cal));
		}
		
		
		
//		if(cal!=null) {
//		if( actTypeId != 0 && ((actTypeId==Constants.SINGLE_FAMILY_RESIDENTIAL_ACT_tYPE) || (actTypeId==Constants.MULTI_FAMILY_ACCESSORY_BUILDING_ACT_TYPE) || (actTypeId==Constants.Multi_Family_Addition_and_Alteration_ACT_tYPE)|| (actTypeId==Constants.Multi_Family_Repair_and_Maintenance_ACT_tYPE)|| (actTypeId==Constants.New_Multi_Family_Dwelling_ACT_tYPE)|| (actTypeId==Constants.New_Single_Family_Dwelling_ACT_tYPE)|| (actTypeId==Constants.Building_Combo_ACT_tYPE) || (actTypeId==Constants.Single_Family_Accessory_Building_ACT_tYPE)|| (actTypeId==Constants.Single_Family_Addition_Alteration_ACT_tYPE)|| (actTypeId==Constants.Single_Family_Repair_Maintenance_ACT_tYPE)|| (actTypeId==Constants.Duplex_Apartment_Condominium_Townhouse_ACT_tYPE))  ){
//			cal.add(Calendar.DATE, 365);   
//		    
//    	}else{
//			cal.add(Calendar.DATE, 180);
//			
//		}
//		}
		
		//cal.add(Calendar.DAY_OF_YEAR, 180); // add 180 days to today's date.
		
		// Disable the exp date logis as per Mario's request 02/15/2023
//		if ((activityForm.getActivityStatus().equals(StringUtils.i2s(Constants.ACTIVITY_STATUS_BUILDING_PC_SUBMITTED))) || (activityForm.getActivityStatus().equals(StringUtils.i2s(Constants.ACTIVITY_STATUS_BUILDING_PERMIT_ISSUED)))) {
//			activityDetail.setExpirationDate(cal);
//			logger.debug("Activity expiration overidden by business logic to " + StringUtils.cal2str(cal));
//		}

		activityDetail.setFinaledDate(StringUtils.str2cal(activityForm.getCompletionDate()));
		logger.debug("SaveActivityDetailsAction -- activityDetail --Completion Date is set to " + activityDetail.getFinaledDate());

		activityDetail.setDescription(activityForm.getDescription());
		logger.debug("SaveActivityDetailsAction -- activityDetail -- description is set to " + activityDetail.getDescription());
		activityDetail.setLabel(activityForm.getLabel());
		logger.debug("SaveActivityDetailsAction -- activityDetail -- Label is set to " + activityDetail.getLabel());
		
		if(activityForm.getGreenHalo().equalsIgnoreCase("on")){
			activityDetail.setGreenHalo("Y");
			logger.debug("SaveActivityDetailsAction -- activityDetail -- GreenHalo is set to Y");
		}else{
			activityDetail.setGreenHalo("N");
		}
		
		String parkingZoneId = activityForm.getParkingZone()!=null ? activityForm.getParkingZone():"";
		logger.debug("Parking zone id is "+parkingZoneId);
		ParkingZone parkingZone = new ParkingZone();
		if(!(parkingZoneId.equalsIgnoreCase(""))){
			parkingZone = AddressAgent.getParkingZoneForZoneId(parkingZoneId);
		}
		activityDetail.setParkingZone(parkingZone);

		logger.debug("SaveActivityDetailsAction -- activityDetail -- Label is set to " + activityDetail.getLabel());
		activityDetail.setPlanCheckRequired(StringUtils.b2s(activityForm.getPlanCheckRequired()));
		logger.debug("SaveActivityDetailsAction -- activityDetail -- Plan Check Required is set to " + activityDetail.getPlanCheckRequired());
		activityDetail.setDevelopmentFeeRequired(StringUtils.b2s(activityForm.getDevelopmentFeeRequired()));
		logger.debug("SaveActivityDetailsAction -- activityDetail -- dev fee Required is set to " + activityDetail.getDevelopmentFeeRequired());
		activityDetail.setMicrofilm(activityForm.getMicrofilm());
		logger.debug("SaveActivityDetailsAction -- activityDetail -- Microfilm is set to " + activityDetail.getMicrofilm());

		activityDetail.setNewUnits(activityForm.getNewUnits());
		logger.debug("SaveActivityDetailsAction -- activityDetail -- NewUnits is set to " + activityDetail.getNewUnits());

		activityDetail.setExistingUnits(activityForm.getExistingsUnits());
		logger.debug("SaveActivityDetailsAction -- activityDetail -- ExistingsUnits is set to " + activityDetail.getExistingUnits());

		activityDetail.setDemolishedUnits(activityForm.getDemolishedUnits());
		logger.debug("SaveActivityDetailsAction -- activityDetail -- DemolishedUnits is set to " + activityDetail.getDemolishedUnits());

		User user = (User) session.getAttribute("user");
		activityDetail.setUpdatedBy(user);
		logger.debug("SaveActivityDetailsAction -- activityDetail -- updatedby is set to " + activityDetail.getUpdatedBy().toString());
		
		activityDetail.setExtRefNumber(activityForm.getExtRefNumber());
		activity = new Activity();
		activity.setActivityDetail(activityDetail);
		activity.setActivityId(activityId);

		if ((actStatus.equalsIgnoreCase(StringUtils.i2s(Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL))) && (outstandingFees.equals("N"))) {
			int finalUpdate = new ActivityAgent().finalActivity(StringUtils.i2s(activityId));
			logger.debug("do final" + finalUpdate);
		}

		return activity;
	}
}
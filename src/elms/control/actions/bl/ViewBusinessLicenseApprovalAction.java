package elms.control.actions.bl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.control.beans.BusinessLicenseApprovalForm;

public class ViewBusinessLicenseApprovalAction extends Action {
	static Logger logger = Logger.getLogger(ViewBusinessLicenseApprovalAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		BusinessLicenseApprovalForm businessLicenseApprovalForm = new BusinessLicenseApprovalForm();

		String actNbr = "";
		List activityTeamList = null;

		try {

			String activityId = request.getParameter("activityId");

			actNbr = LookupAgent.getActivityNumberForActivityId(activityId);

			if (actNbr.trim().startsWith("BL"))

				activityTeamList = new ActivityAgent().getActivityTeamList(Integer.parseInt(activityId), 0);

			else

				activityTeamList = new ActivityAgent().getBTActivityTeamList(Integer.parseInt(activityId), 0);

			request.setAttribute("activityTeamList", activityTeamList);

		} catch (Exception e) {
		}
		return (mapping.findForward("success"));
	}
}
package elms.control.actions.bl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.common.Constants;
import elms.control.beans.BusinessLicenseApprovalForm;
import elms.security.User;
import elms.util.StringUtils;

public class CheckForDuplicatesAction extends Action {
	static Logger logger = Logger.getLogger(CheckForDuplicatesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		boolean duplicateRecord = false;
		String duplicate = null;
		try {
			ActivityAgent activityAgent = new ActivityAgent();
			BusinessLicenseApprovalForm businessLicenseApprovalForm = (BusinessLicenseApprovalForm) form;
			User user = (User) session.getAttribute(Constants.USER_KEY);

			logger.info("In Duplicate action" + duplicateRecord);

			String departmentId = request.getParameter("departmentId") != null ? request.getParameter("departmentId") : "-1";
			String activityId = request.getParameter("activityId") != null ? request.getParameter("activityId") : "";

			//            
			// //get the list of users for one department
			// String departmentId = request.getParameter("departmentId")!=null?request.getParameter("departmentId"): "-1";
			// logger.debug("department id is "+departmentId);
			// businessLicenseApprovalForm.setDepartmentId(Integer.parseInt(departmentId));
			//           
			// String activityId = request.getParameter("psaId")!=null?request.getParameter("psaId"): businessLicenseApprovalForm.getActivityId();
			// logger.debug("activity id is "+activityId);
			// businessLicenseApprovalForm.setActivityId(activityId);

			request.setAttribute("businessLicenseApprovalForm", businessLicenseApprovalForm);
			request.setAttribute("activityId", activityId);

			duplicateRecord = activityAgent.checkForDuplicates(businessLicenseApprovalForm, businessLicenseApprovalForm.getUserId(), activityId, departmentId);
			logger.debug("$$$ Duplicate value $$$" + duplicateRecord);

			request.setAttribute("duplicateRecord", StringUtils.nullReplaceWithEmpty("" + duplicateRecord));
			logger.debug("@@@ Forwarding back to the jsp @@@");
			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}
}

package elms.control.actions.bl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class BatchPrintAction extends Action {
	/*
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(BatchPrintAction.class.getName());
	boolean editable = true;
	ActionErrors errors = null;

	/**
	 * The nextpage
	 */
	protected String nextPage = "success";

	/**
	 * The perform method
	 * 
	 */
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		return (mapping.findForward("success"));

	}
}
package elms.control.actions.bl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;

public class DeleteActivityTeamAction extends Action {
	static Logger logger = Logger.getLogger(DeleteActivityTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		try {
			List activityTeamList = new ArrayList();
			String activityId = request.getParameter("activityId");
			String approvalId = request.getParameter("approvalId");
			ActivityAgent activityAgent = new ActivityAgent();
			LookupAgent lookupAgent = new LookupAgent();

			String actNbr = lookupAgent.getActivityNumberForActivityId(activityId);
			activityAgent.deleteActivityTeam(Integer.parseInt(approvalId));
			/**** Check for bt or bl activty and based on that get the activity team list */
			if (actNbr.startsWith("BL")) {
				activityTeamList = activityAgent.getActivityTeamList(Integer.parseInt(activityId), 0);
			} else {
				activityTeamList = activityAgent.getBTActivityTeamList(Integer.parseInt(activityId), 0);
			}
			logger.debug("activityTeamList.size ::" + activityTeamList.size());
			request.setAttribute("activityTeamList", activityTeamList);
			request.setAttribute("activityId", activityId);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}
}

package elms.control.actions.bl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.control.beans.BusinessLicenseApprovalForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveBusinessLicenseApprovalAction extends Action {
	static Logger logger = Logger.getLogger(SaveBusinessLicenseApprovalAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		int approvalId = -1;
		String nextPage = "";
		String actNbr = "";

		try {
			HttpSession session = request.getSession();
			User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
			int userId = user.getUserId();

			logger.debug("user id is " + userId);
			actNbr = LookupAgent.getActivityNumberForActivityId(request.getParameter("activityId"));
			BusinessLicenseApprovalForm businessLicenseApprovalForm = (BusinessLicenseApprovalForm) form;
			businessLicenseApprovalForm.setUpdatedBy(userId);
			logger.info("Entering BusinessLicenseApprovalAction");

			approvalId = StringUtils.s2i(request.getParameter("approvalId"));
			ActivityAgent businessLicenseAgent = new ActivityAgent();

			businessLicenseAgent.updateBusinessLicenceApproval(businessLicenseApprovalForm, approvalId);
			request.setAttribute("businessLicenseApprovalForm", businessLicenseApprovalForm);

			String applicationDateString = businessLicenseApprovalForm.getApplicationDateString();
			request.setAttribute("applicationDateString", applicationDateString);

			// String activityId= request.getParameter("activityId");
			request.setAttribute("activityId", request.getParameter("activityId"));
			logger.info("activityId" + businessLicenseApprovalForm.getActivityId());

			request.setAttribute("approvalId", request.getParameter("approvalId"));

			String departmentName = request.getParameter("departmentName");
			request.setAttribute("departmentName", departmentName);

			String userName = request.getParameter("userName");
			request.setAttribute("userName", userName);

			if (actNbr.trim().startsWith("BL")) {
				nextPage = "success";
			} else if (actNbr.trim().startsWith("BT")) {
				// added for Business Tax
				nextPage = "showBTApproval";
			}
			logger.debug("nextPage is " + nextPage);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}
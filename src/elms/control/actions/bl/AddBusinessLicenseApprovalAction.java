package elms.control.actions.bl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.control.beans.BusinessLicenseApprovalForm;

public class AddBusinessLicenseApprovalAction extends Action {
	static Logger logger = Logger.getLogger(AddBusinessLicenseApprovalAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		BusinessLicenseApprovalForm businessLicenseApprovalForm = new BusinessLicenseApprovalForm();
		List<BusinessLicenseApprovalForm> activityTeamList = new ArrayList<BusinessLicenseApprovalForm>();
		try {
			String applicationDateString = request.getParameter("applicationDateString");
			request.setAttribute("applicationDateString", applicationDateString);

			String approvalId = request.getParameter("approvalId");
			request.setAttribute("approvalId", approvalId);
 
			String departmentName = request.getParameter("departmentName");
			request.setAttribute("departmentName", departmentName);

			String userName = request.getParameter("userName");
			request.setAttribute("userName", userName);

			String address = request.getParameter("address");
			request.setAttribute("address", address);

			String activityNumber = request.getParameter("activityNumber");
			request.setAttribute("activityNumber", activityNumber);

			String activityType = request.getParameter("activityType");
			request.setAttribute("activityType", activityType);
			logger.info("ActivityType()" + activityType);

			String activityId = request.getParameter("activityId");

			request.setAttribute("businessLicenseApprovalForm", businessLicenseApprovalForm);
			
			activityNumber =LookupAgent.getActivityNumberForActivityId(activityId);
			request.setAttribute("activityNumber", activityNumber);   

			if (activityNumber.trim().startsWith("BL")) {
				activityTeamList = new ActivityAgent().getActivityTeamList(Integer.parseInt(activityId), 0);
			} else if (activityNumber.trim().startsWith("BT")) {
				// added for Business Tax
				activityTeamList = new ActivityAgent().getBTActivityTeamList(Integer.parseInt(activityId), 0);
			}
			
			if(activityTeamList==null)
			{
				activityTeamList = new ArrayList<BusinessLicenseApprovalForm>();   	
			}
			request.setAttribute("activityTeamList", activityTeamList);

		} catch (Exception e) {
		}
		return (mapping.findForward("success"));
	}
}
/**
 * Created on Sep 26, 2007
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.actions.bl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.FinanceAgent;
import elms.app.bl.BusinessLicenseActivity;
import elms.app.common.MultiAddress;
import elms.app.finance.FinanceSummary;
import elms.control.beans.BusinessLicenseActivityForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class EditBusinessLicenseActivityAction extends Action {
	/*
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(EditBusinessLicenseActivityAction.class.getName());
	boolean editable = true;
	int activityId;
	int lsoId;
	String outstandingFees = "";
	String updateAct = "success";
	String businessName = "";
	String bName = "";

	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			BusinessLicenseActivity businessLicenseActivity = null;
			BusinessLicenseActivity businessLicenseActivityForAddr = null;
			HttpSession session = request.getSession();
			ActionErrors errors = new ActionErrors();
			User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
			int userId = user.getUserId();
			logger.debug("user id is " + userId);
			updateAct = (String) request.getParameter("updateAct");

			if (updateAct == null) {
				updateAct = "";
			}

			if (updateAct.equals("update")) {
				BusinessLicenseActivityForm businessLicenseActivityForm = (BusinessLicenseActivityForm) form;
				businessLicenseActivityForm.setUpdatedBy(userId);

				ActivityAgent businessLicenseAgent = new ActivityAgent();
				activityId = StringUtils.s2i((String) request.getParameter("actId"));
				logger.debug("Activity Id is " + activityId);

				lsoId = StringUtils.s2i((String) request.getParameter("lsId"));
				logger.debug("lso Id is " + lsoId);
				businessLicenseActivityForm.setAddressStreetName(null);
				businessLicenseActivity = businessLicenseActivityForm.getBusinessLicenseActivity();
				businessLicenseActivity.setUpdatedBy(userId);

				businessName = businessLicenseActivity.getBusinessName();
				logger.debug("businessName is " + businessName);

				businessLicenseActivityForAddr = businessLicenseAgent.getBusinessLicenseActivity(activityId, lsoId);

				bName = businessLicenseActivityForAddr.getBusinessName();
				businessLicenseActivity.setAddressStreetNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressStreetNumber()));
				businessLicenseActivity.setAddressStreetFraction(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressStreetFraction()));
				businessLicenseActivity.setAddressStreetName(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressStreetName()));
				businessLicenseActivity.setAddressUnitNumber(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressUnitNumber()));
				businessLicenseActivity.setAddressCity(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressCity()));
				businessLicenseActivity.setAddressState(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressState()));
				businessLicenseActivity.setAddressZip(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressZip()));
				businessLicenseActivity.setAddressZip4(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForAddr.getAddressZip4()));
				bName = bName.trim();
				logger.debug("bname is " + bName);

				if (bName.equalsIgnoreCase(businessName)) {
					
					MultiAddress[] multiAddessList = businessLicenseActivityForm.getMultiAddress();
		            logger.debug("multiAddessList :"+multiAddessList);
		            MultiAddress rec = (MultiAddress) multiAddessList[0];

					logger.debug("number 1 :"+rec.getStreetNumber());
										
					activityId = businessLicenseAgent.updateBusinessLicenseActivity(businessLicenseActivity, activityId);
					businessLicenseAgent.updateMultiAddress(multiAddessList, activityId);
					businessLicenseActivity = businessLicenseAgent.getBusinessLicenseActivity(activityId, lsoId);
					businessLicenseActivityForm = businessLicenseActivityForm.setBusinessLicenseActivity(businessLicenseActivity);

					session.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
					session.setAttribute("activityId", StringUtils.i2s(activityId));
					session.setAttribute("lsoId", StringUtils.i2s(lsoId));
					logger.debug("All the data is updated except for the business name which is already existing.");

				} else {
					// Update activity with BUSINESS NAME
					activityId = businessLicenseAgent.updateBusinessLicenseActivityWithBusinessName(businessLicenseActivity, activityId, bName, lsoId);
				}
				businessLicenseActivity = businessLicenseAgent.getBusinessLicenseActivity(activityId, lsoId);

				request.setAttribute("businessLicenseActivity", businessLicenseActivity);
				request.setAttribute("activityId", StringUtils.i2s(activityId));
				request.setAttribute("lsoId", StringUtils.i2s(lsoId));
				nextPage = "viewBL";

				// Checking for outstanding fees for the Activity before 'final'
				FinanceSummary financeSummary;

				try {
					financeSummary = new FinanceAgent().getActivityFinance(activityId);
					logger.debug("Got finance summary" + financeSummary);
				} catch (Exception e1) {
					throw new ServletException(e1.getMessage());
				}

				if (financeSummary == null) {
					financeSummary = new FinanceSummary();
				}

				String amountDue = financeSummary.getTotalAmountDue();
				float fAmount = StringUtils.s2bd(amountDue).floatValue();
				logger.debug("amount due ### :" + fAmount);

				if (fAmount <= 0) {
					outstandingFees = "N";
					logger.debug("outstandingFees" + outstandingFees);
					request.setAttribute("outstandingFees", outstandingFees);

				} else {
					outstandingFees = "Y";
					logger.debug("outstandingFees" + outstandingFees);
					request.setAttribute("outstandingFees in else part : ", outstandingFees);
				}

				if ((businessLicenseActivityForm.getActivityStatus().equalsIgnoreCase("4")) && (outstandingFees.equals("Y"))) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activity.finalWithPaymentDue"));
					logger.debug("do final error");
				}

				logger.debug("outstandingFees" + outstandingFees);

				if (!errors.empty()) {
					saveErrors(request, errors);
					saveToken(request);

					return (new ActionForward(mapping.getInput()));
				}
			} else {
				BusinessLicenseActivityForm businessLicenseActivityForm = new BusinessLicenseActivityForm();
				ActivityAgent businessLicenseAgent = new ActivityAgent();
				lsoId = StringUtils.s2i(request.getParameter("lsoId"));
				logger.debug("lsoId is " + lsoId);
				activityId = StringUtils.s2i((String) request.getParameter("activityId"));
				logger.debug("Activity Id is " + activityId);
				businessLicenseActivity = businessLicenseAgent.getBusinessLicenseActivity(activityId, lsoId);
				businessLicenseActivityForm = businessLicenseActivityForm.setBusinessLicenseActivity(businessLicenseActivity);
				
				session.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
				request.setAttribute("activityId", StringUtils.i2s(activityId));
				request.setAttribute("lsoId", StringUtils.i2s(lsoId));
				nextPage = "editBL";

				// Checking for outstanding fees for the Activity before 'final'
				FinanceSummary financeSummary;

				try {
					financeSummary = new FinanceAgent().getActivityFinance(activityId);
					logger.debug("Got finance summary");
				} catch (Exception e1) {
					throw new ServletException(e1.getMessage());
				}

				if (financeSummary == null) {
					financeSummary = new FinanceSummary();
				}

				String amountDue = financeSummary.getTotalAmountDue();
				float fAmount = StringUtils.s2bd(amountDue).floatValue();
				logger.debug("amount due ### :" + fAmount);

				if (fAmount <= 0) {
					outstandingFees = "N";
					logger.debug("outstandingFees" + outstandingFees);
					request.setAttribute("outstandingFees", outstandingFees);
				} else {
					outstandingFees = "Y";
					logger.debug("outstandingFees in else part " + outstandingFees);
					request.setAttribute("outstandingFees", outstandingFees);
				}

				if ((businessLicenseActivityForm.getActivityStatus().equalsIgnoreCase("4")) && (outstandingFees.equals("Y"))) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activity.finalWithPaymentDue"));
					logger.debug("do final error");
				}

				if (!errors.empty()) {
					saveErrors(request, errors);
					saveToken(request);

					return (new ActionForward(mapping.getInput()));
				}
			}
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				}
				} catch (Exception e) {
				e.getMessage();
			}
			session.setAttribute("refreshTree", "Y");
		} catch (Exception e) {
			logger.error("Exception occured while saving business license activity " + e.getMessage());
			throw new ServletException("Exception occured while saving business license activity " + e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}

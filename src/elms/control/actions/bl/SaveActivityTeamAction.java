package elms.control.actions.bl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.common.Constants;
import elms.control.beans.BusinessLicenseApprovalForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveActivityTeamAction extends Action {
	static Logger logger = Logger.getLogger(SaveActivityTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		String duplicate = "false";
		String dupl = "true";
		boolean duplicateRecord = false;
		String activityId = "";
		String departmentId = "";
		String actNbr = "";
		List activityTeamList = null;
		ActionErrors errors = new ActionErrors();

		int userId = 0;
		BusinessLicenseApprovalForm businessLicenseApprovalForm = (BusinessLicenseApprovalForm) form;
		try {
			User user = (User) session.getAttribute(Constants.USER_KEY);
			duplicate = (String) request.getParameter("duplicate") != null ? request.getParameter("duplicate") : "";
			dupl = (String) request.getParameter("dupl") != null ? request.getParameter("dupl") : "";
			logger.debug("dupl   " + dupl);

			ActivityAgent activityAgent = new ActivityAgent();
			userId = businessLicenseApprovalForm.getUserId();

			departmentId = request.getParameter("departmentId") != null ? request.getParameter("departmentId") : "-1";

			activityId = request.getParameter("activityId") != null ? request.getParameter("activityId") : "";
			logger.debug("&& departmentId && " + departmentId);

			actNbr = LookupAgent.getActivityNumberForActivityId(activityId);

			List users = LookupAgent.getUsersByDepartment(StringUtils.s2i(departmentId));
			logger.debug("obtained users of size " + users.size());
			request.setAttribute("users", users);

			request.setAttribute("activityId", activityId);

			if (dupl.equalsIgnoreCase("false")) {

				duplicateRecord = activityAgent.checkForDuplicates(businessLicenseApprovalForm, businessLicenseApprovalForm.getUserId(), activityId, departmentId);
				logger.debug("$$$ Duplicate value $$$" + duplicateRecord);
			}

			if (duplicateRecord == true) {
				List departments = LookupAgent.getDepartmentList();
				request.setAttribute("departments", departments);
				request.setAttribute("departmentId", departmentId);
				request.setAttribute("userId", "" + userId);
				request.setAttribute("duplicate", "true");

				// if errors exist, then forward to the input page.
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.empty.mismatch"));
				saveErrors(request, errors);

				return (new ActionForward(mapping.getInput()));
			}

			departmentId = request.getParameter("departmentId") != null ? request.getParameter("departmentId") : "-1";
			businessLicenseApprovalForm.setDepartmentId(StringUtils.s2i(departmentId));

			if (duplicate.equals("true")) {
				logger.debug("userId is" + businessLicenseApprovalForm.getUpdatedBy());
				businessLicenseApprovalForm.setDepartmentId(StringUtils.s2i(departmentId));
				activityAgent.updateBLApproval(businessLicenseApprovalForm);
				request.setAttribute("dup", "true");
			}
			if (duplicate.equalsIgnoreCase("false")) {
				activityId = (String) request.getAttribute("activityId");
				logger.debug("in duplicate true condition" + activityId);
				activityAgent.saveActivityTeam(businessLicenseApprovalForm, user.getUserId());
				request.setAttribute("dup", "false");
			}
			logger.debug("######### " + actNbr);
			if (actNbr.trim().startsWith("BL")) {
				activityTeamList = activityAgent.getActivityTeamList(Integer.parseInt(activityId), 0);
			} else if (actNbr.trim().startsWith("BT")) {
				// added for Business Tax
				activityTeamList = activityAgent.getBTActivityTeamList(Integer.parseInt(activityId), 0);
			}

			request.setAttribute("activityTeamList", activityTeamList);
			request.setAttribute("activityId", activityId);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}
}

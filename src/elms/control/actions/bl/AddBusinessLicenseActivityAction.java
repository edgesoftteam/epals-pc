package elms.control.actions.bl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.app.project.SubProject;
import elms.common.Constants;
import elms.control.beans.BusinessLicenseActivityForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * @author Manjuprasad and Gayathri Business License Activity Action
 */

public class AddBusinessLicenseActivityAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AddBusinessLicenseActivityAction.class.getName());

	/**
	 * The nextpage
	 */
	protected String nextPage = "success";

	SubProject subProject;

	/**
	 * The perform method
	 */
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// the web form
		// resetToken(request);
		BusinessLicenseActivityForm businessLicenseActivityForm = (BusinessLicenseActivityForm) form;
		ActionErrors errors = new ActionErrors();
		Wrapper db = new Wrapper();
		logger.debug("address street name is " + businessLicenseActivityForm.getAddressStreetName());
		Map codes = null;
		String MuncipalCode = "";
		String SicCode = "";
		String setClassCode = "";
		List activitySubTypes = new ArrayList();
		;
		List activityTypes;
		response.setContentType("text");

		try {
			activityTypes = LookupAgent.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_LICENSE);
		} catch (Exception e) {
			activityTypes = new ArrayList();
			logger.warn("Could not find list of activityTypes, initializing it to blank arraylist");
		}
		request.setAttribute("activityTypes", activityTypes);

		String addressStreetName = businessLicenseActivityForm.getAddressStreetName();

		request.setAttribute("addressStreetName", addressStreetName);

		String resetValue = (StringUtils.nullReplaceWithEmpty(request.getParameter("resetValue")));
		logger.debug("resetVaue::" + resetValue);

		String clCode = (StringUtils.nullReplaceWithEmpty((String) request.getParameter("clCode")));
		clCode = clCode.toUpperCase();

		String activityType = request.getParameter("activityType") != null ? request.getParameter("activityType") : "";
		activityType = activityType.trim();
		boolean flag = StringUtils.s2b(request.getParameter("flag"));
		logger.debug("activity type got from the jsp is " + activityType);

		String businessLoc = StringUtils.nullReplaceWithEmpty(request.getParameter("businessLoc"));
		logger.debug("To set the busines location defaulted to :: " + businessLoc);

		if (!activityType.equals("") && (businessLicenseActivityForm.getMuncipalCode() != "") && (businessLicenseActivityForm.getSicCode() != "") && (businessLicenseActivityForm.getClassCode() != "")) {
			try {
				activitySubTypes = LookupAgent.getActivitySubTypes(businessLicenseActivityForm.getActivityType());
				businessLicenseActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));

				codes = LookupAgent.getCodesForClassCode(clCode);
				if (codes.size() <= 0) {
					businessLicenseActivityForm.setActivityType("");
					businessLicenseActivityForm.setMuncipalCode("");
					businessLicenseActivityForm.setSicCode("");
					businessLicenseActivityForm.setClassCode("");
				} else {
					businessLicenseActivityForm.setActivityType(codes.get("TYPE").toString());
					businessLicenseActivityForm.setMuncipalCode(codes.get("MUNI_CODE").toString());
					businessLicenseActivityForm.setSicCode(codes.get("SIC_CODE").toString());
					businessLicenseActivityForm.setClassCode(codes.get("CLASS_CODE").toString());
				}
				/*********** check for valid class code *********************/
				if (!clCode.trim().equals("") && codes.size() <= 0) {
					if (errors.empty()) {
						errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter a valid Class Code"));
						saveErrors(request, errors);
					}
				}
			} catch (Exception e) {
				activitySubTypes = new ArrayList();
				logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
			}
		}
		if (activityType != null) {
			try {

				if (flag == true) {
					logger.debug("Im here");
					businessLicenseActivityForm.setMuncipalCode("");
					businessLicenseActivityForm.setSicCode("");
					businessLicenseActivityForm.setClassCode("");
					businessLicenseActivityForm.reset();
					businessLicenseActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BL_PENDING));
					businessLicenseActivityForm.setBusinessLocation(true);
					activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
				} else {
					businessLicenseActivityForm.setActivityType(activityType);
					activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
					businessLicenseActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));
					logger.debug("size of the list of activity sub types" + activitySubTypes.size());

					codes = LookupAgent.getCodesForClassCode(clCode);
					if (codes.size() <= 0) {
						businessLicenseActivityForm.setActivityType("");
						businessLicenseActivityForm.setMuncipalCode("");
						businessLicenseActivityForm.setSicCode("");
						businessLicenseActivityForm.setClassCode("");
					} else {
						businessLicenseActivityForm.setActivityType(codes.get("TYPE").toString());
						businessLicenseActivityForm.setMuncipalCode(codes.get("MUNI_CODE").toString());
						businessLicenseActivityForm.setSicCode(codes.get("SIC_CODE").toString());
						businessLicenseActivityForm.setClassCode(codes.get("CLASS_CODE").toString());
					}
					/*********** check for valid class code *********************/
					if (!clCode.trim().equals("") && codes.size() <= 0) {
						if (errors.empty()) {
							errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter a valid Class Code"));
							saveErrors(request, errors);
						}
					}

					logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
				}
			} catch (Exception e1) {
				activityTypes = new ArrayList();
				logger.warn("Could not find list of activityTypes, initializing it to blank arraylist");
			}
			request.setAttribute("activitySubTypes", activitySubTypes);
			flag = false;
		}

		if (activityType.trim().equals("")) {
			ActivityAgent activityAgent  = new ActivityAgent();
			businessLicenseActivityForm.setApplicationType(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSLICENSE));
			businessLicenseActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BL_PENDING));
			try {
				businessLicenseActivityForm.setMultiAddress(activityAgent.getMultiAddress(0, "BL"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			if (!(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetNumber()).equalsIgnoreCase("")) && !(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetName()).equalsIgnoreCase(""))) {
				businessLicenseActivityForm.setBusinessLocation(false);
			} else if (businessLoc.equalsIgnoreCase("true") || (businessLicenseActivityForm.isBusinessLocation() == true)) {
				businessLicenseActivityForm.setBusinessLocation(true);
			} else {
				businessLicenseActivityForm.setBusinessLocation(false);
			}

			// Start of Class Code Refresh using Ajax
			if ((clCode != null) || (clCode != "") && (businessLicenseActivityForm.getMuncipalCode() == null) && (businessLicenseActivityForm.getSicCode() == null)) {
				try {
					codes = LookupAgent.getCodesForClassCode(clCode);
					if (codes.size() > 0) {
						PrintWriter pw = response.getWriter();
						pw.write(codes.get("TYPE").toString() + ',' + codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString());
						return null;
					} else {
						businessLicenseActivityForm.setMuncipalCode("");
						businessLicenseActivityForm.setSicCode("");
						businessLicenseActivityForm.setClassCode("");
						businessLicenseActivityForm.setActivityType("");
					}
					/*********** check for valid class code *********************/
					if (!clCode.trim().equals("") && codes.size() <= 0) {
						if (errors.empty()) {
							errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter a valid Class Code"));
							saveErrors(request, errors);
						}
					}
				} catch (Exception e) {
					activitySubTypes = new ArrayList();
					logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
				}
			}// End of Class Code Refresh using Ajax
		}

		// Start of Activity Type Refresh using Ajax
		if (!activityType.equals("") && (businessLicenseActivityForm.getMuncipalCode() == "") && (businessLicenseActivityForm.getSicCode() == "") && (businessLicenseActivityForm.getClassCode() == "")) {
			businessLicenseActivityForm.setActivityType(activityType);
			try {
				activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
				businessLicenseActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));

				codes = LookupAgent.getCodes(activityType);
				if (resetValue == "") {
					PrintWriter pw = response.getWriter();
					pw.write(codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString() + ',' + codes.get("CLASS_CODE").toString());
					return null;
				}
				businessLicenseActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
				businessLicenseActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
				businessLicenseActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));
			} catch (Exception e1) {
				activitySubTypes = new ArrayList();
				logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
			}
		}// End of Activity Type Refresh using Ajax

		if (activityType == "" && (StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getMuncipalCode()) == "") && (StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getSicCode()) == "") && (StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getClassCode()) == "")) {
			businessLicenseActivityForm.setMuncipalCode("");
			businessLicenseActivityForm.setSicCode("");
			businessLicenseActivityForm.setClassCode("");
			businessLicenseActivityForm.setActivityType("");
		}
		saveToken(request);
		request.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
		businessLicenseActivityForm.setActivitySubType("-1");
		flag = false;
		return (mapping.findForward(nextPage));
	}
}
package elms.control.actions.bl;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom.Document;

import elms.agent.ReportAgent;
import elms.util.Doc2Pdf;
import elms.util.StringUtils;

public class PrintBusinessLicenseAction extends Action {
	/*
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(PrintBusinessLicenseAction.class.getName());

	protected String activityNumber = "";

	ActionErrors errors = new ActionErrors();

	ReportAgent reportAgent = new ReportAgent();

	Document document = null;

	protected String pdfFileName = "";

	protected String xslFileName = "";

	protected String btXslFileName = "";

	/**
	 * The nextpage
	 */

	protected String nextPage;

	/**
	 * The perform method
	 * 
	 */
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		activityNumber = (String) request.getParameter("activityNumber");
		logger.debug("Activity number got from request is " + activityNumber);
		try {
			xslFileName = "businessLicensel.xsl";
			btXslFileName = "businessTax.xsl";

			if (activityNumber.startsWith("BT")) {
				document = reportAgent.getBusinessTaxJDOM(activityNumber);
				pdfFileName = StringUtils.getUniqueFileName() + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);

				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					btXslFileName = getRealPath + "xsl" + File.separator + btXslFileName;
					pdfFileName = getRealPath + "pdf" + File.separator + pdfFileName;
					Doc2Pdf.start(document, btXslFileName, pdfFileName);
				}
			} else {
				document = reportAgent.getBusinessLicenseJDOM(activityNumber);
				pdfFileName = StringUtils.getUniqueFileName() + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);
				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					xslFileName = getRealPath + "xsl" + File.separator + xslFileName;
					pdfFileName = getRealPath + "pdf" + File.separator + pdfFileName;
					Doc2Pdf.start(document, xslFileName, pdfFileName);
				}
			}

			// for debugging start
			/*
			 * XMLOutputter outputter = new XMLOutputter();
			 * 
			 * try { outputter.setIndent("  "); // use two space indent outputter.setNewlines(true); outputter.output(document, System.out); } catch (IOException e) { logger.error(e); }
			 */

			// for debugging end
		} catch (Exception e) {
			logger.error("Exception occured while printing business license " + e.getMessage());
		}
		return (mapping.findForward("success"));

	}
}
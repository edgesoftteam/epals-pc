package elms.control.actions.bl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;

public class ListActivityTeamAction extends Action {
	static Logger logger = Logger.getLogger(ListActivityTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		String actNbr = "";
		List activityTeamList = null;
		try {
			String activityId = request.getParameter("psaId");

			actNbr = LookupAgent.getActivityNumberForActivityId(activityId);

			request.setAttribute("activityId", activityId);
			logger.debug("activity Id obtained is " + activityId);
			ActivityAgent activityAgent = new ActivityAgent();

			if (actNbr.trim().startsWith("BL")) {
				activityTeamList = activityAgent.getActivityTeamList(Integer.parseInt(activityId), 0);
			} else if (actNbr.trim().startsWith("BT")) {
				// added for Business Tax
				logger.debug("Entered into Business Tax Activity team fetch");
				activityTeamList = activityAgent.getBTActivityTeamList(Integer.parseInt(activityId), 0);
			}
			request.setAttribute("activityTeamList", activityTeamList);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}
}

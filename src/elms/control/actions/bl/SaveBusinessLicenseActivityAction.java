package elms.control.actions.bl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.app.admin.ActivityType;
import elms.app.bl.BusinessLicenseActivity;
import elms.app.common.MultiAddress;
import elms.control.beans.BusinessLicenseActivityForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveBusinessLicenseActivityAction extends Action {

	static Logger logger = Logger.getLogger(SaveBusinessLicenseActivityAction.class.getName());
	boolean editable = true;

	String typeId = "";

	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered into SaveBusinessLicenseActivityAction ");
		try {
			BusinessLicenseActivityForm businessLicenseActivityForm = (BusinessLicenseActivityForm) form;
			if (!(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetNumber()).equalsIgnoreCase("")) && !(StringUtils.nullReplaceWithEmpty(businessLicenseActivityForm.getOutOfTownStreetName()).equalsIgnoreCase(""))) {
				businessLicenseActivityForm.setBusinessLocation(false);
				businessLicenseActivityForm.setAddressStreetName(null);
				}
			BusinessLicenseActivity businessLicenseActivity = businessLicenseActivityForm.getBusinessLicenseActivity();
			HttpSession session = request.getSession();
			ActionErrors errors = new ActionErrors();
			Map classcode = null;
			Map codes = null;
			int activityId = -1;
			int lsoId = -1;
			String lsoAddress = "";
			List activitySubTypes;
			String activityType = "";
			String clCode = (StringUtils.nullReplaceWithEmpty((String) request.getParameter("clCode")));
			clCode = clCode.toUpperCase();
			MultiAddress[] multiAddessList = businessLicenseActivity.getMultiAddress();
            logger.debug("multiAddessList :"+multiAddessList);
          
			if (!isTokenValid(request)) {
				try {
					activityType = (request.getParameter("activityType") != null) ? request.getParameter("activityType") : "";
					activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
					classcode = LookupAgent.getCodesForClassCode(clCode);

					codes = LookupAgent.getCodes(activityType);
					logger.debug("obtained codes of size " + codes.size());
					businessLicenseActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
					businessLicenseActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
					businessLicenseActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));
					if (classcode.size() > 0) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.btclasscode.mismatch"));
						if (!errors.empty()) {
							saveErrors(request, errors);
							session.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
							request.setAttribute("activitySubTypes", activitySubTypes);
							return (new ActionForward(mapping.getInput()));
						}

					}
					if (!clCode.trim().equals("") && classcode.size() <= 0) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.btclasscode.mismatch"));
						if (!errors.empty()) {
							saveErrors(request, errors);
							session.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
							request.setAttribute("activitySubTypes", activitySubTypes);
							return (new ActionForward(mapping.getInput()));
						}
					}
				} catch (Exception e1) {
					logger.debug("Form resubmitted by the user...i am not processing it again..");
				}
			} else {

				try {
					User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
					int userId = user.getUserId();
					logger.debug("user id is " + userId);

					ActivityAgent businessLicenseAgent = new ActivityAgent();
					
					
					boolean businessLocation = businessLicenseActivityForm.isBusinessLocation();
					logger.debug("Business Location Is " + businessLocation);

					if (!businessLocation) {
						businessLicenseActivity.setAddressStreetNumber("1");
						businessLicenseActivity.setAddressStreetName("CITYWIDE");
						businessLicenseActivity.setStreet(LookupAgent.getStreet("53"));
						logger.debug("Address Street Id is " + businessLicenseActivity.getStreet().getStreetId());
					}

					lsoId = businessLicenseAgent.checkAddress(businessLicenseActivity.getAddressStreetNumber(), businessLicenseActivity.getAddressStreetFraction(), businessLicenseActivity.getStreet().getStreetId(), businessLicenseActivity.getAddressUnitNumber(), businessLicenseActivity.getAddressZip4(), businessLicenseActivity);
					logger.debug("Lso Id Returned Is " + lsoId);

					if (lsoId == 0) {
						String addressStreetName = (String) request.getAttribute("addressStreetName");
						// if errors exist, then forward to the input page.
						logger.debug("Address did not match");
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.address.mismatch"));
						saveErrors(request, errors);
						request.setAttribute("addressStreetName", addressStreetName);
					}
					classcode = LookupAgent.getCodesForClassCode(clCode);
					if (!clCode.trim().equals("") && classcode.size() <= 0) {
						if (!errors.empty()) {
							errors.add("statusCodeExists", new ActionError("statusCodeExists", " Please enter a valid Class Code"));
							saveErrors(request, errors);
						}

					}
					if (!errors.empty()) {
						activityType = (request.getParameter("activityType") != null) ? request.getParameter("activityType") : "";
						logger.debug("activity type got from the jsp is " + activityType);

						if (activityType != null) {
							businessLicenseActivityForm.setActivityType(activityType);

							try {
								activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
								logger.debug("size of the list of activity sub types" + activitySubTypes.size());

								codes = LookupAgent.getCodes(activityType);
								logger.debug("obtained codes of size " + codes.size());
								businessLicenseActivityForm.setMuncipalCode(codes.get("MUNI_CODE").toString());
								businessLicenseActivityForm.setSicCode(codes.get("SIC_CODE").toString());
								businessLicenseActivityForm.setClassCode(codes.get("CLASS_CODE").toString());
							} catch (Exception e1) {
								activitySubTypes = new ArrayList();
								logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
							}

							request.setAttribute("activitySubTypes", activitySubTypes);
						}

						logger.debug("Errors exist, going back to the Business License Activity page");
						logger.debug("Input Is " + mapping.getInput());
						request.setAttribute("businessLicenseActivityForm", businessLicenseActivityForm);
						
						return (new ActionForward(mapping.getInput()));
					} else {

						ActivityType actType = new ActivityType();
						classcode = LookupAgent.getCodesForClassCode(clCode);
						if (classcode.size() > 0) {
							businessLicenseActivity.getActivityType().setType(classcode.get("TYPE").toString());
							businessLicenseActivity.setMuncipalCode(StringUtils.nullReplaceWithEmpty(classcode.get("MUNI_CODE").toString()));
							businessLicenseActivity.setSicCode(StringUtils.nullReplaceWithEmpty(classcode.get("SIC_CODE").toString()));
							businessLicenseActivity.setClassCode(clCode);
						}
						businessLicenseActivity.setCreatedBy(userId);
						activityId = businessLicenseAgent.saveBusinessLicenseActivity(businessLicenseActivity, lsoId);
						logger.debug("Activity created, the new ID is " + activityId);
						businessLicenseAgent.updateMultiAddress(multiAddessList, activityId);

						businessLicenseActivity = businessLicenseAgent.getBusinessLicenseActivity(activityId, lsoId);

						request.setAttribute("businessLicenseActivity", businessLicenseActivity);
						request.setAttribute("activityId", StringUtils.i2s(activityId));

						lsoAddress = businessLicenseAgent.getActivityAddressForId(activityId);
						session.setAttribute("lsoAddress", lsoAddress);
						logger.debug("Set the lso address to session as " + lsoAddress);

						request.setAttribute("lsoId", StringUtils.i2s(lsoId));
						session.setAttribute("lsoId", StringUtils.i2s(lsoId));
						session.setAttribute("refreshTree", "Y");

						CommonAgent condAgent = new CommonAgent();
						typeId = businessLicenseActivity.getActivityType().getTypeId();

						boolean added = condAgent.copyRequiredCondition(typeId, activityId, user);
						logger.debug("added new conditions" + added);

						nextPage = "view";
					}
					try {
						String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
						if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
						GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
						GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
						}
						} catch (Exception e) {
						e.getMessage();
					}
				} catch (Exception e) {
					logger.error("Exception occured while saving business license activity " + e.getMessage());
					throw new ServletException("Exception occured while saving business license activity " + e.getMessage());
				}

				resetToken(request);
				logger.debug("Token reset successfully");
			}
		} catch (Exception e) {
			logger.error("Exception occured while saving business license activity " + e.getMessage());
			throw new ServletException("Exception occured while saving business license activity " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}

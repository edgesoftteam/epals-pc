/*
 * Created on Jun 5, 2009
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.actions.bl;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;

/**
 * @author Manjuprasad
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style - Code Templates
 */
public class ViewApprovalHistoryAction extends Action {
	static Logger logger = Logger.getLogger(ViewApprovalHistoryAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		List activityTeamHistoryList = null;
		try {
			HttpSession session = request.getSession();
			ActivityAgent businessLicenseAgent = new ActivityAgent();

			String approvalId = request.getParameter("approvalId");
			request.setAttribute("approvalId", approvalId);

			String activityId = request.getParameter("activityId");
			String activityNumber = request.getParameter("activityNumber");

			activityTeamHistoryList = new ActivityAgent().getApprovalHistoryList(Integer.parseInt(activityId), Integer.parseInt(approvalId), activityNumber);

			request.setAttribute("activityTeamHistoryList", activityTeamHistoryList);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}

package elms.control.actions.bt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AdminAgent;
import elms.agent.CommonAgent;
import elms.agent.LookupAgent;
import elms.app.bt.BusinessTaxActivity;
import elms.app.common.MultiAddress;
import elms.app.common.ProcessTeamRecord;
import elms.common.Constants;
import elms.control.beans.BusinessTaxActivityForm;
import elms.control.beans.ProcessTeamForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveBusinessTaxActivityAction extends Action {

	static Logger logger = Logger.getLogger(SaveBusinessTaxActivityAction.class.getName());
	boolean editable = true;

	String typeId = "";
	protected int projectId;

	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.debug("Entered into SaveBusinessTaxActivityAction ");
		try {
			BusinessTaxActivityForm businessTaxActivityForm = (BusinessTaxActivityForm) form;
			HttpSession session = request.getSession();
			ActionErrors errors = new ActionErrors();
			Map classcode = null;
			int activityId = -1;
			int lsoId = -1;
			Map codes = null;
			String lsoAddress = "";
			List activitySubTypes;
			String activityType = "";
			String clCode = (StringUtils.nullReplaceWithEmpty((String) request.getParameter("clCode")));
			clCode = clCode.toUpperCase();
			logger.debug("businessLocation :"+businessTaxActivityForm.isBusinessLocation());
			
			if (!(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownStreetNumber()).equalsIgnoreCase("")) && !(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownStreetName()).equalsIgnoreCase(""))) {
				businessTaxActivityForm.setBusinessLocation(false);
				businessTaxActivityForm.setBusinessAddressStreetName(null);
			}
			
			BusinessTaxActivity businessTaxActivity = businessTaxActivityForm.getBusinessTaxActivity();
			MultiAddress[] multiAddessList = businessTaxActivity.getMultiAddress();
            logger.debug("multiAddessList :"+multiAddessList);
           
			if (!isTokenValid(request)) {

				try {
					activityType = (request.getParameter("activityType") != null) ? request.getParameter("activityType") : "";
					activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
					classcode = LookupAgent.getCodesForClassCode(clCode);
					codes = LookupAgent.getCodes(activityType);
					logger.debug("obtained codes of size " + codes.size());
					businessTaxActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
					businessTaxActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
					businessTaxActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));

					if (classcode.size() > 0) {

						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.btclasscode.mismatch"));
						if (!errors.empty()) {
							saveErrors(request, errors);
							session.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
							request.setAttribute("activitySubTypes", activitySubTypes);
							return (new ActionForward(mapping.getInput()));
						}

					}
					if (!clCode.trim().equals("") && classcode.size() <= 0) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.btclasscode.mismatch"));
						if (!errors.empty()) {
							saveErrors(request, errors);
							session.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
							request.setAttribute("activitySubTypes", activitySubTypes);
							return (new ActionForward(mapping.getInput()));
						}
					}
				} catch (Exception e1) {
					logger.debug("Form resubmitted by the user...i am not processing it again..");
				}

			} else {

				String apnNoStr = "";
				int inspetorId = 0;
				String inspetor = "";

				try {

					User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
					int userId = user.getUserId();
					logger.debug("user id is " + userId);

					logger.debug("businessTaxActivity object created ");
					ActivityAgent businessTaxAgent = new ActivityAgent();
					
					boolean businessLocation = businessTaxActivityForm.isBusinessLocation();
					logger.debug("Business Location Is " + businessLocation);
					if (!businessLocation) {
						businessTaxActivity.setBusinessAddressStreetNumber("1");
						businessTaxActivity.setBusinessAddressStreetName("CITYWIDE");
						businessTaxActivity.setStreet(LookupAgent.getStreet("53"));
						logger.debug("Address Street Id is " + businessTaxActivity.getStreet().getStreetId());
					}

					lsoId = businessTaxAgent.checkAddress(businessTaxActivity.getBusinessAddressStreetNumber(), businessTaxActivity.getBusinessAddressStreetFraction(), businessTaxActivity.getStreet().getStreetId(), businessTaxActivity.getBusinessAddressUnitNumber(), businessTaxActivity.getBusinessAddressZip(), businessTaxActivity.getBusinessAddressZip4(), businessTaxActivity);
					logger.debug("Lso Id Returned Is " + lsoId);

					if (lsoId == 0) {
						String businessAddressStreetName = businessTaxActivityForm.getBusinessAddressStreetName();
						// if errors exist, then forward to the input page.
						logger.debug("Address did not match");
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.btaddress.mismatch"));
						saveErrors(request, errors);

						request.setAttribute("businessAddressStreetName", businessAddressStreetName);
					}
					classcode = LookupAgent.getCodesForClassCode(clCode);
					if (!clCode.trim().equals("") && classcode.size() <= 0) {
						if (!errors.empty()) {
							errors.add("statusCodeExists", new ActionError("statusCodeExists", " Please enter a valid Class Code"));
							saveErrors(request, errors);
						}

					}

					if (!errors.empty()) {
						activityType = (request.getParameter("activityType") != null) ? request.getParameter("activityType") : "";
						logger.debug("activity type got from the jsp is " + activityType);

						if (activityType != null) {

							businessTaxActivityForm.setActivityType(activityType);

							try {
								activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
								logger.debug("size of the list of activity sub types" + activitySubTypes.size());

								codes = LookupAgent.getCodes(activityType);
								logger.debug("obtained codes of size " + codes.size());
								businessTaxActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
								businessTaxActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
								businessTaxActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));
								logger.debug("Got Codes");
							} catch (Exception e1) {
								activitySubTypes = new ArrayList();
								logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
							}
							request.setAttribute("activitySubTypes", activitySubTypes);
						}
						logger.debug("Errors exist, going back to the Business Tax Activity page");
						logger.debug("Input Is " + mapping.getInput());
						request.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
						return (new ActionForward(mapping.getInput()));
					} else {

						classcode = LookupAgent.getCodesForClassCode(clCode);
						if (classcode.size() > 0) {
							businessTaxActivity.getActivityType().setType(classcode.get("TYPE").toString());
							businessTaxActivity.setMuncipalCode(StringUtils.nullReplaceWithEmpty(classcode.get("MUNI_CODE").toString()));
							businessTaxActivity.setSicCode(StringUtils.nullReplaceWithEmpty(classcode.get("SIC_CODE").toString()));
							businessTaxActivity.setClassCode(clCode);
						}

						businessTaxActivity.setCreatedBy(userId);
						activityId = businessTaxAgent.saveBusinessTaxActivity(businessTaxActivity, lsoId);
						businessTaxAgent.updateMultiAddress(multiAddessList, activityId);
						logger.debug("Activity created, the new ID is " + activityId);

						int projectNameId = -1;
						// Add Process Team from project
						ProcessTeamForm processTeamForm = new ProcessTeamForm();
						processTeamForm.setPsaId(StringUtils.i2s(projectId));
						processTeamForm.setPsaType("P");
						processTeamForm = new CommonAgent().populateTeam(processTeamForm);
						logger.debug("after populateTeam : " + processTeamForm.getPsaId() + " and " + processTeamForm.getPsaType());
						processTeamForm.setPsaId(StringUtils.i2s(activityId));
						processTeamForm.setPsaType("A");

						ProcessTeamRecord tmRec = new ProcessTeamRecord();
						tmRec.setIsNew(true);
						tmRec.setLead("of");

						try {
							projectNameId = LookupAgent.getProjectNameId("A", activityId);
						} catch (Exception e5) {
							logger.error("Exception occured while getting project name id " + e5.getMessage());
						}

						logger.debug("Obtained project name id is" + projectNameId);

						if (projectNameId == Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES_ID) {
							User unassignedUser;

							try {
								codes = LookupAgent.getCodes(businessTaxActivityForm.getActivityType());
								logger.debug("obtained codes of size " + codes.size());
								businessTaxActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));

								if ((!businessTaxActivity.getBusinessAddressStreetName().trim().endsWith("CITYWIDE")) && (!businessTaxActivity.getClassCode().trim().startsWith("K01") && !businessTaxActivity.getClassCode().trim().startsWith("K03"))) {

									unassignedUser = new AdminAgent().getUser(Constants.USER_UNASSIGNED);
									tmRec.setNameId(StringUtils.i2s(unassignedUser.getUserId()));
									tmRec.setName(unassignedUser.getFirstName() + "  " + unassignedUser.getLastName());

									new CommonAgent().saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);

									// added for BT done Gayathri on 10thDec
									// For Dept - Planning Division & Approved by - Unassigned Planner in Approval List Screen
									int deptId = CommonAgent.getDepartmentId(unassignedUser.getUserId());
									// added for BT done by Gayathri on 10thDec
									// For Dept - Planning Division & Approved by - Unassigned Planner in Approval List Screen
									new CommonAgent().insertIntoBTapproval(activityId, deptId, unassignedUser.getUserId());
								} else if (businessTaxActivity.getBusinessAddressStreetName().trim().endsWith("CITYWIDE")) {

									User edUser = new AdminAgent().getUser(Constants.USER_ED_KHOURDADJIAN);

									tmRec.setNameId(StringUtils.i2s(edUser.getUserId()));
									tmRec.setName(edUser.getFirstName() + "  " + edUser.getLastName());

									new CommonAgent().saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);

									new CommonAgent();
									// added for BT done by Gayathri
									int deptId = CommonAgent.getDepartmentId(edUser.getUserId());
									// added for BT done by Gayathri
									new CommonAgent().insertIntoBTapproval(activityId, deptId, edUser.getUserId());

								}

								unassignedUser = new AdminAgent().getUser(Constants.USER_EULA_EWARREN);

								tmRec.setNameId(StringUtils.i2s(unassignedUser.getUserId()));
								tmRec.setName(unassignedUser.getFirstName() + "  " + unassignedUser.getLastName());

								new CommonAgent().saveProcessTeam(processTeamForm.getPsaId(), processTeamForm.getPsaType(), user.getUserId(), tmRec);

								new CommonAgent();
								int deptId = CommonAgent.getDepartmentId(unassignedUser.getUserId());
								// added for BT done by Gayathri on 15thJan For Dept - License and Code Services, Approved by - Inspector depending on the APN from LSO_ID in Approval List Screen
								new CommonAgent().insertIntoBTapproval(activityId, deptId, unassignedUser.getUserId());

								// added for BT done by Gayathri on 15thJan For Dept - License and Code Services, Approved by - Inspector depending on the APN from LSO_ID in Approval List Screen
								apnNoStr = StringUtils.apnWithoutHyphens(LookupAgent.getApnForLsoId(StringUtils.i2s(lsoId)));
								logger.debug("Got APN # :: " + apnNoStr);
								if (apnNoStr != "0" || apnNoStr.equals("")) {
									inspetorId = LookupAgent.getInspectorForApn(apnNoStr);
									if (inspetorId != 0) {
										inspetor = LookupAgent.getInspectorUser(inspetorId);
									}
									if (!inspetor.equalsIgnoreCase("")) {
										new CommonAgent().insertIntoBTapproval(activityId, Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES, inspetorId);
									}
								}
							} catch (Exception e1) {
								throw new Exception("Unassigned user not found, please create this user");
							}
						}

						businessTaxActivity = businessTaxAgent.getBusinessTaxActivity(activityId, lsoId);

						request.setAttribute("businessTaxActivity", businessTaxActivity);

						lsoAddress = businessTaxAgent.getActivityAddressForId(activityId);

						logger.debug("Set the lso address to session as " + lsoAddress);
						CommonAgent condAgent = new CommonAgent();
						typeId = businessTaxActivity.getActivityType().getTypeId();

						boolean added = condAgent.copyRequiredCondition(typeId, activityId, user);
						logger.debug("added new conditions" + added);

						nextPage = "view";
					}
				} catch (Exception e) {
					logger.error("Exception occured while saving business license activity " + e.getMessage());
					throw new ServletException("Exception occured while saving business Tax activity " + e.getMessage());
				}

				resetToken(request);
				logger.debug("Token reset successfully");
			}
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				}
				} catch (Exception e) {
				e.getMessage();
			}
			request.setAttribute("activityId", StringUtils.i2s(activityId));
			request.setAttribute("lsoId", StringUtils.i2s(lsoId));
			session.setAttribute("lsoId", StringUtils.i2s(lsoId));
			session.setAttribute("refreshTree", "Y");
			session.setAttribute("lsoAddress", lsoAddress);
		} catch (Exception e) {
			logger.error("Exception occured while saving business license activity " + e.getMessage());
			throw new ServletException("Exception occured while saving business Tax activity " + e.getMessage());
		}
		return (mapping.findForward(nextPage));
	}
}

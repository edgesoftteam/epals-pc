/*
 * Created on Nov 25, 2008
 */
package elms.control.actions.bt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.app.project.SubProject;
import elms.common.Constants;
import elms.control.beans.BusinessTaxActivityForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * @author Gayathri Business Tax Activity Action
 */
public class AddBusinessTaxActivityAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AddBusinessTaxActivityAction.class.getName());

	/**
	 * The nextpage
	 */
	protected String nextPage = "success";

	SubProject subProject;

	/**
	 * The perform method
	 */
	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		BusinessTaxActivityForm businessTaxActivityForm = (BusinessTaxActivityForm) form;

		Wrapper db = new Wrapper();
		String MuncipalCode = "";
		String SicCode = "";
		String setClassCode = "";
		List activitySubTypes = new ArrayList();
		Map codes = null;
		ActionErrors errors = new ActionErrors();

		response.setContentType("text");

		List activityTypes;
		try {
			// Getting all Activity Types by passing Business Tax module id
			activityTypes = LookupAgent.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_TAX);
		} catch (Exception e) {
			activityTypes = new ArrayList();
			logger.warn("Could not find list of activityTypes, initializing it to blank arraylist");
		}
		request.setAttribute("activityTypes", activityTypes);

		String businessAddressStreetName = businessTaxActivityForm.getBusinessAddressStreetName();
		request.setAttribute("businessAddressStreetName", businessAddressStreetName);

		String activityType = request.getParameter("activityType") != null ? request.getParameter("activityType") : "";
		activityType = activityType.trim();
		boolean flag = StringUtils.s2b(request.getParameter("flag"));

		String resetValue = (StringUtils.nullReplaceWithEmpty(request.getParameter("resetValue")));

		String businessLoc = StringUtils.nullReplaceWithEmpty(request.getParameter("businessLoc"));
		logger.debug("To set the busines location defaulted to :: " + businessLoc);

		logger.debug("activity type got from the jsp is " + activityType);

		String clCode = (StringUtils.nullReplaceWithEmpty((String) request.getParameter("clCode")));
		clCode = clCode.toUpperCase();
		businessTaxActivityForm.setApplicationDate(StringUtils.cal2str(Calendar.getInstance()));

		if (!activityType.equals("") && (businessTaxActivityForm.getMuncipalCode() != "") && (businessTaxActivityForm.getSicCode() != "") && (businessTaxActivityForm.getClassCode() != "")) {
			try {

				activitySubTypes = LookupAgent.getActivitySubTypes(businessTaxActivityForm.getActivityType());
				businessTaxActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));
				codes = LookupAgent.getCodesForClassCode(clCode);
				if (codes.size() <= 0) {
					businessTaxActivityForm.setActivityType("");
					businessTaxActivityForm.setMuncipalCode("");
					businessTaxActivityForm.setSicCode("");
					businessTaxActivityForm.setClassCode("");
				} else {
					businessTaxActivityForm.setActivityType(codes.get("TYPE").toString());
					businessTaxActivityForm.setMuncipalCode(codes.get("MUNI_CODE").toString());
					businessTaxActivityForm.setSicCode(codes.get("SIC_CODE").toString());
					businessTaxActivityForm.setClassCode(codes.get("CLASS_CODE").toString());
				}
				/*********** check for valid class code *********************/
				if (!clCode.trim().equals("") && codes.size() <= 0) {
					if (errors.empty()) {
						errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter a valid Class Code"));
						saveErrors(request, errors);
					}
				}
			} catch (Exception e) {
				activitySubTypes = new ArrayList();
				logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
			}
		}

		if (!activityType.equals("") || activityType != null) {
			try {

				if (flag == true) {
					businessTaxActivityForm.setMuncipalCode("");
					businessTaxActivityForm.setSicCode("");
					businessTaxActivityForm.setClassCode("");
					businessTaxActivityForm.setActivityType("");
					businessTaxActivityForm.reset();
					activitySubTypes = LookupAgent.getActivitySubTypes(businessTaxActivityForm.getActivityType());
					businessTaxActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BT_PENDING_APPROVAL));
					businessTaxActivityForm.setBusinessLocation(true);
				} else {
					activitySubTypes = LookupAgent.getActivitySubTypes(businessTaxActivityForm.getActivityType());
					businessTaxActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));

					codes = LookupAgent.getCodesForClassCode(clCode);
					if (codes.size() <= 0) {
						businessTaxActivityForm.setActivityType("");
						businessTaxActivityForm.setMuncipalCode("");
						businessTaxActivityForm.setSicCode("");
						businessTaxActivityForm.setClassCode("");
					} else {

						businessTaxActivityForm.setActivityType(codes.get("TYPE").toString());
						businessTaxActivityForm.setMuncipalCode(codes.get("MUNI_CODE").toString());
						businessTaxActivityForm.setSicCode(codes.get("SIC_CODE").toString());
						businessTaxActivityForm.setClassCode(codes.get("CLASS_CODE").toString());

					}
					if (!clCode.trim().equals("") && codes.size() <= 0) {
						if (errors.empty()) {
							errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter a valid Class Code"));
							saveErrors(request, errors);
						}
					}
					flag = false;
				}
			} catch (Exception e) {
				activitySubTypes = new ArrayList();
				logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
			}
		}

		if (activityType.trim().equals("")) {
			
			ActivityAgent activityAgent  = new ActivityAgent();
			
			businessTaxActivityForm.setApplicationType(StringUtils.i2s(Constants.APPLICATION_TYPE_BUSINESSTAX));
			businessTaxActivityForm.setActivityStatus(StringUtils.i2s(Constants.ACTIVITY_STATUS_BT_PENDING_APPROVAL));
			try {
				businessTaxActivityForm.setMultiAddress(activityAgent.getMultiAddress(0, "BT"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			if (!(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownStreetNumber()).equalsIgnoreCase("")) && !(StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getOutOfTownStreetName()).equalsIgnoreCase(""))) {
				businessTaxActivityForm.setBusinessLocation(false);
			} else if (businessLoc.equalsIgnoreCase("true") || (businessTaxActivityForm.isBusinessLocation() == true)) {
				businessTaxActivityForm.setBusinessLocation(true);
			} else {
				businessTaxActivityForm.setBusinessLocation(false);
			}
			// Start of Class code Refresh using Ajax
			if ((clCode != null) || (clCode != "") && (businessTaxActivityForm.getMuncipalCode() == null) && (businessTaxActivityForm.getSicCode() == null)) {
				try {
					codes = LookupAgent.getCodesForClassCode(clCode);
					if (codes.size() > 0) {
						PrintWriter pw = response.getWriter();
						pw.write(codes.get("TYPE").toString() + ',' + codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString());
						return null;
					} else {
						businessTaxActivityForm.setMuncipalCode("");
						businessTaxActivityForm.setSicCode("");
						businessTaxActivityForm.setClassCode("");
						businessTaxActivityForm.setActivityType("");
					}

					if (!clCode.trim().equals("") && codes.size() <= 0) {
						if (errors.empty()) {
							errors.add("statusCodeExists", new ActionError("statusCodeExists", "Enter a valid Class Code"));
							saveErrors(request, errors);
						}
					}
				} catch (Exception e) {
					activitySubTypes = new ArrayList();
					logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
				}
			} // End of Class code Refresh using Ajax
		}

		// Start of Activity Type Refresh using Ajax
		if (!activityType.equals("") && (businessTaxActivityForm.getMuncipalCode() == "") && (businessTaxActivityForm.getSicCode() == "") && (businessTaxActivityForm.getClassCode() == "")) {
			businessTaxActivityForm.setActivityType(activityType);
			try {

				activitySubTypes = LookupAgent.getActivitySubTypes(activityType);
				businessTaxActivityForm.setActivitySubType(StringUtils.i2s(Constants.ACTIVITY_SUB_TYPE));

				codes = LookupAgent.getCodes(activityType);
				if (resetValue == "") {
					PrintWriter pw = response.getWriter();
					pw.write(codes.get("MUNI_CODE").toString() + ',' + codes.get("SIC_CODE").toString() + ',' + codes.get("CLASS_CODE").toString());

					return null;
				}

				businessTaxActivityForm.setMuncipalCode(StringUtils.nullReplaceWithEmpty(codes.get("MUNI_CODE").toString()));
				businessTaxActivityForm.setSicCode(StringUtils.nullReplaceWithEmpty(codes.get("SIC_CODE").toString()));
				businessTaxActivityForm.setClassCode(StringUtils.nullReplaceWithEmpty(codes.get("CLASS_CODE").toString()));

			} catch (Exception e1) {
				activitySubTypes = new ArrayList();
				logger.warn("Could not find list of activitySubTypes, initializing it to blank arraylist");
			}
		}// End of Activity Type Refresh using Ajax

		if (activityType == "" && (StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getMuncipalCode()) == "") && (StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getSicCode()) == "") && (StringUtils.nullReplaceWithEmpty(businessTaxActivityForm.getClassCode()) == "")) {
			logger.debug("activityType:: " + activityType);
			businessTaxActivityForm.setMuncipalCode("");
			businessTaxActivityForm.setSicCode("");
			businessTaxActivityForm.setClassCode("");
			businessTaxActivityForm.setActivityType("");
			businessTaxActivityForm.reset();
		}
		saveToken(request);
		request.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
		request.setAttribute("activitySubTypes", activitySubTypes);
		logger.debug("Exiting AddBusinessTaxAction");
		flag = false;

		return (mapping.findForward(nextPage));
	}
}

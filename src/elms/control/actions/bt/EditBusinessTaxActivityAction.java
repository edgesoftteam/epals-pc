package elms.control.actions.bt;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.FinanceAgent;
import elms.app.bt.BusinessTaxActivity;
import elms.app.common.MultiAddress;
import elms.app.finance.FinanceSummary;
import elms.control.beans.BusinessTaxActivityForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class EditBusinessTaxActivityAction extends Action {

	static Logger logger = Logger.getLogger(EditBusinessTaxActivityAction.class.getName());
	boolean editable = true;
	int activityId;
	int lsoId;
	String outstandingFees = "";
	protected String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {
			BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
			HttpSession session = request.getSession();
			String otherBusinessOccupancy =((String) request.getParameter("otherBusinessOccupancy")!=null) ?(String) request.getParameter("otherBusinessOccupancy"):"";
			String homeOccupation =((String) request.getParameter("homeOccupation")!=null) ?(String) request.getParameter("homeOccupation"):"";
			String decalCode =((String) request.getParameter("decalCode")!=null) ?(String) request.getParameter("decalCode"):"";


			ActionErrors errors = new ActionErrors();
			User user = (elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY);
			int userId = user.getUserId();
			logger.debug("user id is " + userId);
			String updateAct = (String) request.getParameter("updateAct")!=null ? (String) request.getParameter("updateAct") : "";
			logger.debug("updateAct is " + updateAct);

			if (updateAct.equals("update")) {

				BusinessTaxActivityForm businessTaxActivityForm = (BusinessTaxActivityForm) form;
				businessTaxActivityForm.setUpdatedBy(userId);
			
				if(otherBusinessOccupancy.equalsIgnoreCase("Y"))
				{
					businessTaxActivityForm.setOtherBusinessOccupancy(true);
					businessTaxActivity.setOtherBusinessOccupancy(true);
					
					System.out.println("true Location");
				}else {
					businessTaxActivityForm.setOtherBusinessOccupancy(false);
					businessTaxActivity.setOtherBusinessOccupancy(false);
					System.out.println("Location false");
				}
				       
				if(homeOccupation.equalsIgnoreCase("Y"))
				{
					businessTaxActivityForm.setHomeOccupation(true);
					businessTaxActivity.setHomeOccupation(true);
					System.out.println(" true HomeOccupation");
				}else {
					businessTaxActivityForm.setHomeOccupation(false);
					businessTaxActivity.setHomeOccupation(false);
					System.out.println("false HomeOccupation");
				}
				
				if(decalCode.equalsIgnoreCase("Y"))
				{
					businessTaxActivityForm.setDecalCode(true);
					businessTaxActivity.setDecalCode(true);
					System.out.println("true Decal Code");      
				}else {
					businessTaxActivityForm.setDecalCode(false);
					businessTaxActivity.setDecalCode(false);     
					System.out.println("false Decal Code");
				}
				
				businessTaxActivityForm.setBusinessAddressStreetName(null);
				businessTaxActivity = businessTaxActivityForm.getBusinessTaxActivity();
				businessTaxActivity.setUpdatedBy(userId);
				String businessName = businessTaxActivity.getBusinessName();
				logger.debug("businessName is " + businessName);

				ActivityAgent businessTaxAgent = new ActivityAgent();
				activityId = StringUtils.s2i((String) request.getParameter("actId"));
				logger.debug("Activity Id is " + activityId);

				lsoId = StringUtils.s2i((String) request.getParameter("lsId"));
				logger.debug("lso Id is " + lsoId);

				BusinessTaxActivity businessTaxActivityForAddr = businessTaxAgent.getBusinessTaxActivity(activityId, lsoId);
				logger.debug("********Home occupation******* " + businessTaxActivityForm.isHomeOccupation());
				String bName = businessTaxActivityForAddr.getBusinessName();
				businessTaxActivity.setBusinessAddressStreetNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressStreetNumber()));
				businessTaxActivity.setBusinessAddressStreetFraction(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressStreetFraction()));
				businessTaxActivity.setBusinessAddressStreetName(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressStreetName()));
				businessTaxActivity.setBusinessAddressUnitNumber(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressUnitNumber()));
				businessTaxActivity.setBusinessAddressCity(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressCity()));
				businessTaxActivity.setBusinessAddressState(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressState()));
				businessTaxActivity.setBusinessAddressZip(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressZip()));
				businessTaxActivity.setBusinessAddressZip4(StringUtils.nullReplaceWithEmpty(businessTaxActivityForAddr.getBusinessAddressZip4()));
				bName = bName.trim();
				logger.debug("bname is " + bName);

				if (bName.equalsIgnoreCase(businessName)) {
					MultiAddress[] multiAddessList = businessTaxActivityForm.getMultiAddress();
		            logger.debug("multiAddessList :"+multiAddessList);
		           /* MultiAddress rec = (MultiAddress) multiAddessList[0];

					logger.debug("number 1 :"+rec.getStreetNumber());
					*/					
		           	activityId = businessTaxAgent.updateBusinessTaxActivity(businessTaxActivity, activityId);
		           	businessTaxAgent.updateMultiAddress(multiAddessList, activityId);
					//businessTaxAgent.updateMultiAddress(businessTaxActivity, activityId);
					businessTaxActivity = businessTaxAgent.getBusinessTaxActivity(activityId, lsoId);
					businessTaxActivityForm = businessTaxActivityForm.setBusinessTaxActivity(businessTaxActivity);

					session.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
					request.setAttribute("activityId", StringUtils.i2s(activityId));
					request.setAttribute("lsoId", StringUtils.i2s(lsoId));
					logger.debug("All the data is updated except for the business name which is already existing.");

				} else {
					//update activity with business name
					activityId = businessTaxAgent.updateBusinessTaxActivityWithBusinessName(businessTaxActivity, activityId, bName, lsoId);
				}

				businessTaxActivity = businessTaxAgent.getBusinessTaxActivity(activityId, lsoId);

				request.setAttribute("businessTaxActivity", businessTaxActivity);
				request.setAttribute("activityId", StringUtils.i2s(activityId));
				request.setAttribute("lsoId", StringUtils.i2s(lsoId));
				nextPage = "viewBT";

				// Checking for outstanding fees for the Activity before 'final'
				FinanceSummary financeSummary;

				try {
					financeSummary = new FinanceAgent().getActivityFinance(activityId);
					logger.debug("Got finance summary" + financeSummary);
				} catch (Exception e1) {
					throw new ServletException(e1.getMessage());
				}

				if (financeSummary == null) {
					financeSummary = new FinanceSummary();
				}

				String amountDue = financeSummary.getTotalAmountDue();
				float fAmount = StringUtils.s2bd(amountDue).floatValue();
				logger.debug("amount due ### :" + fAmount);

				if (fAmount <= 0) {
					outstandingFees = "N";
					request.setAttribute("outstandingFees", outstandingFees);
				} else {
					outstandingFees = "Y";
					request.setAttribute("outstandingFees", outstandingFees);
				}

				if ((businessTaxActivityForm.getActivityStatus().equalsIgnoreCase("4")) && (outstandingFees.equals("Y"))) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activity.finalWithPaymentDue"));
					logger.debug("do final error");
				}

				logger.debug("outstandingFees" + outstandingFees);

				if (!errors.empty()) {
					saveErrors(request, errors);
					saveToken(request);

					return (new ActionForward(mapping.getInput()));
				}
			} else {
				BusinessTaxActivityForm businessTaxActivityForm = new BusinessTaxActivityForm();
				ActivityAgent businessTaxAgent = new ActivityAgent();
				lsoId = StringUtils.s2i(request.getParameter("lsoId"));
				logger.debug("lsoId is " + lsoId);
				activityId = StringUtils.s2i((String) request.getParameter("activityId"));
				logger.debug("Activity Id is " + activityId);
				businessTaxActivity = businessTaxAgent.getBusinessTaxActivity(activityId, lsoId);			
				businessTaxActivityForm = businessTaxActivityForm.setBusinessTaxActivity(businessTaxActivity);
				logger.debug("multi address :"+businessTaxActivityForm.getMultiAddress());
				logger.debug("multi address :"+businessTaxActivityForm.getMultiAddress().length);
				session.setAttribute("businessTaxActivityForm", businessTaxActivityForm);
				request.setAttribute("activityId", StringUtils.i2s(activityId));
				request.setAttribute("lsoId", StringUtils.i2s(lsoId));
				nextPage = "editBT";

				// Checking for outstanding fees for the Activity before 'final'
				FinanceSummary financeSummary;

				try {
					financeSummary = new FinanceAgent().getActivityFinance(activityId);
					logger.debug("Got finance summary");
				} catch (Exception e1) {
					throw new ServletException(e1.getMessage());
				}

				if (financeSummary == null) {
					financeSummary = new FinanceSummary();
				}

				String amountDue = financeSummary.getTotalAmountDue();
				float fAmount = StringUtils.s2bd(amountDue).floatValue();
				logger.debug("amount due ### :" + fAmount);

				if (fAmount <= 0) {
					outstandingFees = "N";
					logger.debug("outstandingFees" + outstandingFees);
					request.setAttribute("outstandingFees", outstandingFees);
				} else {
					outstandingFees = "Y";
					logger.debug("outstandingFees in else part " + outstandingFees);
					request.setAttribute("outstandingFees", outstandingFees);
				}

				if ((businessTaxActivityForm.getActivityStatus().equalsIgnoreCase("4")) && (outstandingFees.equals("Y"))) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.activity.finalWithPaymentDue"));
					logger.debug("do final error");
				}

				if (!errors.empty()) {
					saveErrors(request, errors);
					saveToken(request);

					return (new ActionForward(mapping.getInput()));
				}
			}
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for people
				}
				} catch (Exception e) {
				e.getMessage();
			}
			session.setAttribute("refreshTree", "Y");
			

		} catch (Exception e) {
			logger.error("Exception occured while saving business Tax activity " + e.getMessage());
			throw new ServletException("Exception occured while saving business Tax activity " + e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}

}

package elms.control.actions.bt;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;

/**
 * @author Gayathri
 * 
 */
public class DeleteBTActivityTeamAction extends Action {
	static Logger logger = Logger.getLogger(DeleteBTActivityTeamAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		try {

			String activityId = request.getParameter("activityId");
			String approvalId = request.getParameter("approvalId");
			ActivityAgent activityAgent = new ActivityAgent();
			activityAgent.deleteActivityTeam(Integer.parseInt(approvalId));
			List activityTeamList = activityAgent.getBTActivityTeamList(Integer.parseInt(activityId), 0);
			request.setAttribute("activityTeamList", activityTeamList);
			request.setAttribute("activityId", activityId);

			return (mapping.findForward("success"));
		} catch (Exception e) {
			return (mapping.findForward("error"));
		}
	}

}

package elms.control.actions.bt;

/**
 * @author Gayathri Created on Dec 9, 2008
 *
 */
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.control.beans.BusinessLicenseApprovalForm;

public class AddBusinessTaxApprovalAction extends Action {

	static Logger logger = Logger.getLogger(AddBusinessTaxApprovalAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		BusinessLicenseApprovalForm businessLicenseApprovalForm = new BusinessLicenseApprovalForm();

		try {
			String applicationDateString = request.getParameter("applicationDateString");
			request.setAttribute("applicationDateString", applicationDateString);

			String approvalId = request.getParameter("approvalId");
			request.setAttribute("approvalId", approvalId);

			String departmentName = request.getParameter("departmentName");
			request.setAttribute("departmentName", departmentName);

			String userName = request.getParameter("userName");
			request.setAttribute("userName", userName);

			String address = request.getParameter("address");
			request.setAttribute("address", address);

			String activityNumber = request.getParameter("activityNumber");
			request.setAttribute("activityNumber", activityNumber);

			String activityType = request.getParameter("activityType");
			request.setAttribute("activityType", activityType);
			logger.info("ActivityType()" + activityType);

			String activityId = request.getParameter("activityId");

			request.setAttribute("businessLicenseApprovalForm", businessLicenseApprovalForm);

			List activityTeamList = new ActivityAgent().getActivityTeamList(Integer.parseInt(activityId), 0);
			request.setAttribute("activityTeamList", activityTeamList);

		} catch (Exception e) {
		}
		return (mapping.findForward("success"));
	}
}

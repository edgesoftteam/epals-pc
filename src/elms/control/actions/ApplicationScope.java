package elms.control.actions;

import java.util.Calendar;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Timer;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.apache.struts.action.ActionServlet;

import elms.gsearch.AttachmentSolrScheduler;
import elms.gsearch.GlobalSearch;
import elms.gsearch.SolrScheduler;
import elms.util.GreenHaloPermitCheckScheduler;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class ApplicationScope extends ActionServlet {
	static Logger logger = Logger.getLogger(ApplicationScope.class.getName());
	
	public static ResourceBundle obcProperties = null;
	public static HashMap<String, String> map = new HashMap<String, String>();
	public void init() throws ServletException {
		
		super.init();
		try {
			deltaIndexing();
			GreenHaloPermitCheck();
		} catch (Exception e) {
			logger.error("Error while deltaIndexing " + e.getMessage(),e);
		}
		
	}
	
	
	public void deltaIndexing() throws Exception{
		String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
		String attachmentsDeltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_ATTACHMENTS_DELTA_INDEX"));
		int deltaIndexTime= StringUtils.s2i(StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX_TIME")));
		int attachmentsDeltaIndexTime= StringUtils.s2i(StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_ATTACHMENTS_DELTA_INDEX_TIME")));
		Timer timer = new Timer();
		Timer timerAttachments = new Timer();
	    Calendar date = Calendar.getInstance();
		if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
		    //Schedule every one minute
		    date.set(Calendar.HOUR_OF_DAY, 4);
		    date.set(Calendar.MINUTE, 0);
		    date.set(Calendar.SECOND, 0);
		    date.set(Calendar.MILLISECOND, 0);
		    //Repeat every one minute
		    timer.schedule(new SolrScheduler(),date.getTime(),1000 * 60* deltaIndexTime);
		}else{
			timer.cancel();
            timer.purge();
           
		}
		if(!attachmentsDeltaIndex.equals("") && attachmentsDeltaIndex.equalsIgnoreCase("YES")){
		    //Schedule every 5 minutes
		    date.set(Calendar.HOUR_OF_DAY, 4);
		    date.set(Calendar.MINUTE, 0);
		    date.set(Calendar.SECOND, 0);
		    date.set(Calendar.MILLISECOND, 0);
		    //Repeat every 5 minute
		    timerAttachments.schedule(new AttachmentSolrScheduler(),date.getTime(),1000 * 60 * attachmentsDeltaIndexTime);
		}else{
			timerAttachments.cancel();
			timerAttachments.purge();
		}
		map.put("deltaIndex", deltaIndex);
		map.put("attachmentsDeltaIndex", attachmentsDeltaIndex);
	}
	public void GreenHaloPermitCheck() throws Exception{
		 Calendar date = Calendar.getInstance();
		 Timer timer = new Timer();
		date.set(Calendar.HOUR_OF_DAY, 4);
	    date.set(Calendar.MINUTE, 0);
	    date.set(Calendar.SECOND, 0);
	    date.set(Calendar.MILLISECOND, 0);
	    timer.schedule(new GreenHaloPermitCheckScheduler(),date.getTime(),1000 * 60 * 60 * 24);
	}
}

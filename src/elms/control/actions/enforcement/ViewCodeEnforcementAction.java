package elms.control.actions.enforcement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.app.enforcement.CodeEnforcement;
import elms.common.Constants;
import elms.control.beans.enforcement.CodeEnforcementForm;
import elms.util.StringUtils;

public class ViewCodeEnforcementAction extends Action {
	static Logger logger = Logger.getLogger(ViewCodeEnforcementAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewSiteStructureAction()");

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		CodeEnforcementForm codeEnforcementForm = (CodeEnforcementForm) form;
		String nextPage = "success";
		String codeInspector = "false";


		String action=request.getParameter("action");
		logger.debug("action"+action);

		List groups = new ArrayList();
		elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);

		if (user != null) {
			groups = (List) user.getGroups();
		}

		java.util.Iterator itr = groups.iterator();

		while (itr.hasNext()) {
			elms.security.Group group = (elms.security.Group) itr.next();

			if (group == null) {
				group = new elms.security.Group();
			}

			if (group.groupId == 12) {
				codeInspector = "true";
			}

			logger.debug("The Group iDs are" + group.groupId);
		}

		session.setAttribute("codeInspector", codeInspector);

		String userFullName = user.getFirstName() + " " + user.getLastName();
		logger.debug("The Username is:" + userFullName);
		session.setAttribute("userFullName", userFullName);

		try {
			long activityId = codeEnforcementForm.getActivityId();

			
			ActivityAgent ceAgent = new ActivityAgent();
			logger.debug("Save check :" + activityId);
			
			List<CodeEnforcement> actionList = new ArrayList<CodeEnforcement>();
			List<CodeEnforcement> typeList = new ArrayList<CodeEnforcement>();
			int count=0;
			
			count=LookupAgent.getActivityTypeForActIdLong(activityId);
			 
			if(user.getDepartment().getDepartmentId()==Constants.DEPARTMENT_HOUSING && count >=1) 
			{
				
			actionList = ceAgent.getNoticetList(activityId);
			logger.debug("SAve heck size :" + actionList.size());
			codeEnforcementForm.reset();

			for (int i = 0; i < actionList.size(); i++) {
				codeEnforcementForm.addList(actionList.get(i));
			}  

			 typeList = ceAgent.getNoticeTypes();
			logger.debug("Type List Size :" + typeList.size());

				
			}else{
			 actionList = ceAgent.getCodeEnforcementList(activityId);
			 logger.debug("SAve heck size :" + actionList.size());
			codeEnforcementForm.reset();

			for (int i = 0; i < actionList.size(); i++) {
				codeEnforcementForm.addList(actionList.get(i));
			}

			typeList = ceAgent.getCodeEnforcementTypes();
			logger.debug("Type List Size :" + typeList.size());
			}

			for (int i = 0; i < typeList.size(); i++) {
				codeEnforcementForm.addTypes(typeList.get(i));
			}
			
			logger.debug("Complaint List Size :" + codeEnforcementForm.getComplaintTypeList().size());
			logger.debug("Fees List Size :" + codeEnforcementForm.getFeeTypeList().size());
			logger.debug("Inspection List Size :" + codeEnforcementForm.getInspectionTypeList().size());
			//logger.debug("Notice List Size :" + codeEnforcementForm.getNoticeTypeList().size());
			logger.debug("People List Size :" + codeEnforcementForm.getPeopleTypeList().size());
			logger.debug("Resolution List Size :" + codeEnforcementForm.getResolutionTypeList().size());
			
			if(user.getDepartment().getDepartmentId()==Constants.DEPARTMENT_HOUSING) 
			{
				codeEnforcementForm.setNoticeTypeList(typeList);
			}
			//codeEnforcementForm.setNoticeList(actionList);
			logger.debug("Notice List Size :" + codeEnforcementForm.getNoticeList().size());
			logger.debug("Notice Type List Size :" + codeEnforcementForm.getNoticeTypeList().size());
			codeEnforcementForm.setUserList(ceAgent.getUserList());
			codeEnforcementForm.setSubTypeList(ceAgent.getCodeEnforcementSubTypes(activityId));
			
			CodeEnforcement ceItem = new CodeEnforcement();
			
			if (action!=null&& action.equals("addNotice")){
            	ceItem.setOfficerId("" + Constants.INSPECTOR_UNASSIGNED);
                ceItem.setCeDate(StringUtils.cal2str(Calendar.getInstance()));
                codeEnforcementForm.addNoticeList(ceItem);
             }
		    
		    int moduleId = LookupAgent.getModuleId(LookupAgent.getActivityTypeForActId(""+activityId));
		    request.setAttribute("moduleId", moduleId);
		    
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}

		session.setAttribute("codeEnforcementForm", codeEnforcementForm);
		logger.info("Exiting ViewSiteStructureAction(" + nextPage + ")");

		return (mapping.findForward(nextPage));
	}
}

//    String systematicInsp ="";
//  
//    ActivityAgent activityAgent = new ActivityAgent();
//    logger.debug("systematicInsp :" + request.getParameter("activityId"));    
//    systematicInsp= activityAgent.getProjectNameforLevelId(StringUtils.s2i(request.getParameter("activityId")));    
//    String actType = LookupAgent.getActivityTypeForActId(request.getParameter("activityId"));    
//    int deptId = new ActivityAgent().getDepartmentIdForActivityId(""+activityId);    
//    logger.debug("deptId::"+deptId);    
//    logger.debug("systematicInsp :" + systematicInsp);
//    List typeList ;
//    if(actType != null && actType.equalsIgnoreCase("VMP")){
//    	typeList = ceAgent.getCodeEnforcementTypes("VMP",deptId);
//    }else if(systematicInsp.equalsIgnoreCase(Constants.PROJECT_NAME_SYSTEMATIC))
//    {
//    	systematicInsp="y";
//    	logger.debug("systematicInsp yyyy :" + systematicInsp);
//    	typeList = ceAgent.getCodeEnforcementTypes("systematic",deptId);
//        logger.debug("Type List Size :" + typeList.size());
//    }else{
//    	systematicInsp="n";
//    	logger.debug("systematicInsp nnnn :" + systematicInsp);
//    	 typeList = ceAgent.getCodeEnforcementTypes("other",deptId);
//        logger.debug("Type List Size :" + typeList.size());
//    }
//    request.setAttribute("deptId", ""+deptId);
//    CodeEnforcement ceItem = new CodeEnforcement();
//    if (action!=null&& action.equals("addResolution")){
//    	ceItem.setOfficerId("" + user.getUserId());
//        ceItem.setCeDate(StringUtils.cal2str(Calendar.getInstance()));
//        codeEnforcementForm.addResolutionList(ceItem);}
//    if (action!=null&& action.equals("addNotice")){
//    	ceItem.setOfficerId("" + Constants.INSPECTOR_UNASSIGNED);
//        ceItem.setCeDate(StringUtils.cal2str(Calendar.getInstance()));
//        codeEnforcementForm.addNoticeList(ceItem);
//     }
package elms.control.actions.enforcement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.ActivityAgent;
import elms.agent.LookupAgent;
import elms.app.enforcement.CodeEnforcement;
import elms.common.Constants;
import elms.control.beans.enforcement.CodeEnforcementForm;
import elms.security.User;
import elms.util.StringUtils;

public class SaveCodeEnforcementAction extends Action {

	static Logger logger = Logger.getLogger(SaveCodeEnforcementAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.info("Entering SaveSiteStructureAction()");

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		CodeEnforcementForm codeEnforcementForm = (CodeEnforcementForm) session.getAttribute("codeEnforcementForm");
		String action = codeEnforcementForm.getAction();
		String nextPage = "";

		User user = (User) session.getAttribute(Constants.USER_KEY);
		ActivityAgent ceAgent = new ActivityAgent();
		try {
			logger.debug("Action :" + action);

			if (action.startsWith("add")) { 
				addBlankItem(action, codeEnforcementForm, user);
				codeEnforcementForm.setScrollTo(action);
				if(user.getDepartment().getDepartmentId()==Constants.DEPARTMENT_HOUSING)
				{
				codeEnforcementForm.setNoticeTypeList(ceAgent.getNoticeTypes());
				}
				session.setAttribute("codeEnforcementForm", codeEnforcementForm);
				nextPage = "success";
			} else if (action.startsWith("Save")) {
				long activityId = codeEnforcementForm.getActivityId();
				List actionList = new ArrayList();
				actionList.addAll(codeEnforcementForm.getComplaintList());
				actionList.addAll(codeEnforcementForm.getFeeList());
				actionList.addAll(codeEnforcementForm.getInspectionList());
				actionList.addAll(codeEnforcementForm.getNoticeList());
				actionList.addAll(codeEnforcementForm.getPeopleList());
				actionList.addAll(codeEnforcementForm.getResolutionList());
				actionList.addAll(codeEnforcementForm.getComplianceList());
				
				int count=0;
				
				count=LookupAgent.getActivityTypeForActIdLong(activityId);
				 
				if(user.getDepartment().getDepartmentId()==Constants.DEPARTMENT_HOUSING && count >=1) 
				{
					ceAgent.saveNoticesList(activityId, actionList);	  
				}else{
					ceAgent.saveCodeEnforcementList(activityId, actionList);	
				}
				
				nextPage = "refresh";
			} else {
				request.setAttribute("levelId", "" + codeEnforcementForm.getActivityId());
				request.setAttribute("levelType", "A");
				request.setAttribute(Constants.LSO_ID, session.getAttribute(Constants.LSO_ID));
				nextPage = "cancel";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.info("Exiting SaveSiteStructureAction(" + nextPage + ")");
		return (mapping.findForward(nextPage));
	}

	private void addBlankItem(String action, CodeEnforcementForm form, User user) {
		CodeEnforcement ceItem = new CodeEnforcement();
		ceItem.setOfficerId("" + user.getUserId());
		ceItem.setCeDate(StringUtils.cal2str(Calendar.getInstance()));

		if (action.equals("addComplaint"))
			form.addComplaintList(ceItem);
		else if (action.equals("addFee"))
			form.addFeeList(ceItem);
		else if (action.equals("addInspection"))
			form.addInspectionList(ceItem);
		else if (action.equals("addNotice"))
			form.addNoticeList(ceItem);
		else if (action.equals("addPerson"))
			form.addPeopleList(ceItem);
		else if (action.equals("addResolution"))
			form.addResolutionList(ceItem);
		else if (action.equals("addCompliance"))
			form.addComplianceList(ceItem);
	}
}
package elms.control.actions.finance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.control.actions.project.ViewActivityAction;
import elms.control.beans.FeesMgrForm;
import elms.gsearch.GlobalSearch;
import elms.util.StringUtils;

public class SaveBusTaxFeesAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SaveBusTaxFeesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		logger.info("Entering SaveBusTaxFeesAction");

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		session.setAttribute("onloadAction", onloadAction);

		try {
			int activityId = elms.util.StringUtils.s2i(request.getParameter("id"));
			FeesMgrForm feesMgrForm = (FeesMgrForm) session.getAttribute("feesMgrForm");

			FinanceAgent financeAgent = new FinanceAgent();

			ActivityFeeEdit[] aFee = feesMgrForm.getBusinessTaxList();

			ActivityFee activityFee = null;
			List businessTaxList = new ArrayList();

			for (int i = 0; i < aFee.length; i++) {
				activityFee = new ActivityFee();
				activityFee.setActivityId(activityId);
				activityFee.setFeeId(0);
				activityFee.setPeopleId(StringUtils.s2i(aFee[i].getPeopleId()));
				activityFee.setFeeValuation(StringUtils.s2i(aFee[i].getFeeValuation()));
				activityFee.setComments(aFee[i].getComments());
				businessTaxList.add(activityFee);
			}

			financeAgent.saveBusinessTaxList(businessTaxList);

			session.removeAttribute("feesMgrForm");
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				}
			} catch (Exception e) {
				e.getMessage();
			}
			new ViewActivityAction().getActivity(activityId, request);

			logger.info("Exiting SaveBusTaxFeesAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

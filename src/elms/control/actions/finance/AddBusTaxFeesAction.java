package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.control.beans.FeesMgrForm;

public class AddBusTaxFeesAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(AddBusTaxFeesAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			logger.info("Entering AddBusTaxFeesMgrAction");

			int activityId = elms.util.StringUtils.s2i(request.getParameter("id"));
			FeesMgrForm feesMgrForm = (FeesMgrForm) session.getAttribute("feesMgrForm");

			FinanceAgent financeAgent = new FinanceAgent();
			FinanceSummary financeSummary = financeAgent.getActivityFinance(activityId);
			session.setAttribute("financeSummary", financeSummary);
			feesMgrForm.setFinanceSummary(financeSummary);

			ActivityFeeEdit[] bFee = feesMgrForm.getBusinessTaxList();
			int i = bFee.length;
			ActivityFeeEdit[] aFee = new ActivityFeeEdit[i + 1];

			for (int j = 0; j < i; j++) {
				aFee[j] = bFee[j];
			}

			aFee[i] = new ActivityFeeEdit();
			feesMgrForm.setBusinessTaxList(aFee);

			session.setAttribute("feesMgrForm", feesMgrForm);

			logger.info("Exiting AddBusTaxFeesMgrAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

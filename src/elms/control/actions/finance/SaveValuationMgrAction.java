package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.control.beans.FeesMgrForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class SaveValuationMgrAction extends Action {

	static Logger logger = Logger.getLogger(SaveValuationMgrAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);

		int activityId = 0;

		try {
			request.setAttribute("fromPage", "SaveValuationManager");
			activityId = elms.util.StringUtils.s2i(request.getParameter("id"));
			FeesMgrForm feesForm = (FeesMgrForm) form;

			FinanceAgent financeAgent = new FinanceAgent();
			feesForm.setActivityFeeList(financeAgent.getFeeList(activityId));
			feesForm.setPlanCheckFeeList(financeAgent.getFeeList(activityId));

			String valuation = feesForm.getValuation();
			logger.debug("Valuation is " + valuation);
			Wrapper db = new Wrapper();
			db.update("update activity set UPDATED=CURRENT_TIMESTAMP,valuation = " + StringUtils.$2dbl(valuation) + " where act_id= " + activityId);

			String planCheckFeeDate = request.getParameter("planCheckFeeDate");
			String permitFeeDate = request.getParameter("permitFeeDate");
			// author Gayathri For Development Fee Date -- Dec'08
			String developmentFeeDate = request.getParameter("developmentFeeDate");

			logger.debug("FeeDates are " + planCheckFeeDate + "," + developmentFeeDate + "," + permitFeeDate + " for Activity " + activityId);

			financeAgent.setActivityDates(activityId, planCheckFeeDate, developmentFeeDate, permitFeeDate);

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope()))
					request.removeAttribute(mapping.getAttribute());
				else
					session.removeAttribute(mapping.getAttribute());
			}

			session.setAttribute("feesMgrForm", feesForm);
			logger.info("Exiting SaveFeesMgrAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		return (mapping.findForward("success"));
	}

}
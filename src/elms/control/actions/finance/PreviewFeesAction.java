package elms.control.actions.finance;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.control.beans.FeesMgrForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class PreviewFeesAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(PreviewFeesAction.class.getName());
	private String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();
		try {
			String activityId = request.getParameter("id");
			String view = request.getParameter("view");

			if (view == null) {
				view = "";
			}

			if (view.equals("pw")) {
				nextPage = "publicworks";
			}

			String sTypeId = (session.getAttribute("sTypeId") != null) ? (String) (session.getAttribute("sTypeId")) : "";

			if (!sTypeId.equals("")) {
				nextPage = "online";
			}

			FeesMgrForm feesMgrForm = (FeesMgrForm) form;
			FinanceSummary financeSummary = (FinanceSummary) session.getAttribute("financeSummary");

			logger.info("Entering PreviewFeesAction with ActivityId of " + activityId);

			Wrapper db = new Wrapper();
			FinanceAgent financeAgent = new FinanceAgent();

			ActivityFee activityFee = null;
			double permitFeeAmount = 0.0;

			// Processing the activity Fees
			int count = 0;
			boolean adminFeeCalcuated = false;
			StringBuilder errorDisplaymessage = new StringBuilder();
			List<Integer> adminFeeIds = financeAgent.getReadyToPayFlagEnabledFeeList(StringUtils.s2i(activityId));
			activityFee = null;
			double pfTotal = 0;

			double[] subTotals = new double[6];
			subTotals[0] = 0.0;
			subTotals[1] = 0.0;
			subTotals[2] = 0.0;
			subTotals[3] = 0.0;
			subTotals[4] = 0.0;
			subTotals[5] = 0.0;
			double permitLicenseFeeTotal = 0;// added for penalty
			int aFeeSTFlg;
			
			// Processing the Plan Check Fees
						ActivityFeeEdit[] aPlFee = feesMgrForm.getPlanCheckFeeList();
						logger.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ length::" + aPlFee.length);
						double plTotal = 0;
						count = 0;
						adminFeeCalcuated = false;
						
						for (int i = 0; i < aPlFee.length; i++) {
							try {
								String checked = aPlFee[i].getChecked();
								logger.debug("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ values::" + aPlFee[i].getFeeId() + "::" + aPlFee[i].getFeeUnits() + "::" + aPlFee[i].getFeeAmount());
								if (!checked.equals("")) {
									activityFee = new ActivityFee(activityId, aPlFee[i].getFeeId(), aPlFee[i].getFeeUnits(), aPlFee[i].getFeeAmount());

									logger.debug("Fee [" + i + "]" + aPlFee[i].getFeeDescription());

									activityFee.setFeeInit(StringUtils.s2i(aPlFee[i].getFeeInit()));
									activityFee.setFeeFactor(StringUtils.s2bd(aPlFee[i].getFeeFactor()));
									activityFee.setFactor(StringUtils.s2bd(aPlFee[i].getFactor()));
									activityFee.setSubtotalLevel(StringUtils.s2i(aPlFee[i].getSubtotalLevel()));

									// add factor too
									// setting subTotal array to ActivityFees
									activityFee.setSubTotals(subTotals);
									logger.debug("$$$ calculateFeeAmount $$$ else");
									// Calculate the Fee Amount
									financeAgent.calculateFeeAmount(db, activityFee);
									//Calculating activity fee is paid fee or not (Ready To Pay flag)
									if(financeAgent.getActivityFeeBalanceAmount(db, activityFee))
									{
										count++;
									}
									// for PlanCheck fees, checking for Aministration fee if ready to pay flag enable 
									if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
										for(int j= 0;j<adminFeeIds.size();j++){
											if(adminFeeIds.get(j) == activityFee.getFeeId()){
												adminFeeCalcuated = true; 
												logger.debug("admin fee calculating for PlanCheck fee");
												
											}
										}
									}
									// setting array for subtotals
									if ((!(aPlFee[i].getSubtotalLevel() == null))) {
										aFeeSTFlg = StringUtils.s2i(aPlFee[i].getSubtotalLevel());
										subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
										subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
										logger.debug("sub total value is " + aFeeSTFlg);
										logger.debug("sub total flag is " + subTotals[aFeeSTFlg]);

									}

									aPlFee[i].setFeeAmount(StringUtils.dbl2$(activityFee.getFeeAmount()));
									plTotal += activityFee.getFeeAmount();
									plTotal = Double.parseDouble(new DecimalFormat("0.00").format(plTotal));
								} else {
									aPlFee[i].setFeeAmount("$0.00");
									aPlFee[i].setFeeUnits("");
									aPlFee[i].setChecked("");
								}
							} catch (Exception e) {
								aPlFee[i].setFeeAmount("$0.00");
								aPlFee[i].setFeeUnits("");
								aPlFee[i].setChecked("");
							} finally {
							}
						}
						if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
							if(!adminFeeCalcuated)
								errorDisplaymessage.append("Plan Check Administration Fee");
						}
						logger.debug("Plan Check total : " + plTotal);
						financeSummary.setPlanCheckFeeAmt(StringUtils.dbl2$(plTotal));

						double plpaid = StringUtils.s2bd(financeSummary.getPlanCheckPaidAmt()).doubleValue();
						double plDue = plTotal - plpaid;
						plDue = Double.parseDouble(new DecimalFormat("0.00").format(plDue));
						financeSummary.setPlanCheckAmountDue(StringUtils.dbl2$(plDue));
						logger.debug("plancheck due : " + plDue);

						// ///////////
						logger.debug("#######################################");
						// Processing the Development Fees
						ActivityFeeEdit[] aDevFee = feesMgrForm.getDevelopmentFeeList();
						logger.debug("####################################### length::" + aDevFee.length);
						double devTotal = 0;
						count = 0;
						adminFeeCalcuated = false;
						for (int i = 0; i < aDevFee.length; i++) {
							try {
								String checked = aDevFee[i].getChecked();
								logger.debug("####################################### values::" + aDevFee[i].getFeeId() + "::" + aDevFee[i].getFeeUnits() + "::" + aDevFee[i].getFeeAmount());
								if (!checked.equals("")) {
									activityFee = new ActivityFee(activityId, aDevFee[i].getFeeId(), aDevFee[i].getFeeUnits(), aDevFee[i].getFeeAmount());

									logger.debug("Fee [" + i + "]" + aDevFee[i].getFeeDescription());

									activityFee.setFeeInit(StringUtils.s2i(aDevFee[i].getFeeInit()));
									activityFee.setFeeFactor(StringUtils.s2bd(aDevFee[i].getFeeFactor()));
									activityFee.setFactor(StringUtils.s2bd(aDevFee[i].getFactor()));
									activityFee.setSubtotalLevel(StringUtils.s2i(aDevFee[i].getSubtotalLevel()));

									// add factor too
									// setting subTotal array to ActivityFees
									activityFee.setSubTotals(subTotals);
									logger.debug("$$$ calculateFeeAmount $$$ else");
									// Calculate the Fee Amount
									financeAgent.calculateFeeAmount(db, activityFee);
									
									//Calculating activity fee is paid fee or not (Ready To Pay flag)
									if(financeAgent.getActivityFeeBalanceAmount(db, activityFee))
									{
										count++;
									}
									if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
										for(int j= 0;j<adminFeeIds.size();j++){
											if(adminFeeIds.get(j) == activityFee.getFeeId()){
												adminFeeCalcuated = true; 
												logger.debug("admin fee calculating for PlanCheck fee");
												
											}
										}
									}
									
									// setting array for subtotals
									if ((!(aDevFee[i].getSubtotalLevel() == null))) {
										aFeeSTFlg = StringUtils.s2i(aDevFee[i].getSubtotalLevel());
										subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
										subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
										logger.debug("sub total value is " + aFeeSTFlg);
										logger.debug("sub total flag is " + subTotals[aFeeSTFlg]);

									}

									aDevFee[i].setFeeAmount(StringUtils.dbl2$(activityFee.getFeeAmount()));
									devTotal += activityFee.getFeeAmount();
									devTotal = Double.parseDouble(new DecimalFormat("0.00").format(devTotal));
								} else {
									aDevFee[i].setFeeAmount("$0.00");
									aDevFee[i].setFeeUnits("");
									aDevFee[i].setChecked("");
								}
							} catch (Exception e) {
								aDevFee[i].setFeeAmount("$0.00");
								aDevFee[i].setFeeUnits("");
								aDevFee[i].setChecked("");
							} finally {
							}
						}
						if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
							if(!adminFeeCalcuated){
								if(errorDisplaymessage != null && errorDisplaymessage.length()>0)
									errorDisplaymessage.append(", ");
								errorDisplaymessage.append("Development Administration Fee");
							}
						}
						logger.debug("devTotal : " + devTotal);
						financeSummary.setDevelopmentFeeAmt(StringUtils.dbl2$(devTotal));

						double devpaid = StringUtils.s2bd(financeSummary.getDevelopmentFeePaidAmt()).doubleValue();
						double devDue = devTotal - devpaid;
						devDue = Double.parseDouble(new DecimalFormat("0.00").format(devDue));
						financeSummary.setDevelopmentFeeAmountDue(StringUtils.dbl2$(devDue));
						logger.debug("development due : " + devDue);
				
			//Processing permit fee		
			ActivityFeeEdit[] aFee = feesMgrForm.getActivityFeeList();
			count = 0;
			adminFeeCalcuated = false;
			for (int i = 0; i < aFee.length; i++) {
				try {
					String checked = aFee[i].getChecked();
					logger.debug("checked is " + checked);
					logger.debug("$$$$$$$$$ " + aFee[i].getFeePc());
					if (aFee[i].getFeePc().equals("X") || aFee[i].getInput().equals("X") || !checked.equals("")) {
						activityFee = new ActivityFee(activityId, aFee[i].getFeeId(), aFee[i].getFeeUnits(),
								aFee[i].getFeeAmount());

						logger.debug("Fee [" + i + "]" + aFee[i].getFeeDescription());
						activityFee.setFeePaid((double) StringUtils.s2d(aFee[i].getFeePaid()));
						activityFee.setFeeInit(StringUtils.s2i(aFee[i].getFeeInit()));
						activityFee.setFeeFactor(StringUtils.s2bd(aFee[i].getFeeFactor()));
						activityFee.setFactor(StringUtils.s2bd(aFee[i].getFactor()));
						activityFee.setSubtotalLevel(StringUtils.s2i(aFee[i].getSubtotalLevel()));
						logger.debug("fee sub total level is " + aFee[i].getSubtotalLevel());

						// setting subTotal array to ActivityFees
						logger.debug("Fee id is " + activityFee.getFeeId());

						// set the fee subtotals only if the fee is not a
						// calculated fee.
						activityFee.setSubTotals(subTotals);

						logger.debug("$$$ calculateFeeAmount $$$ if");

						// Calculate the Fee Amount
						financeAgent.calculateFeeAmountPftotal(db, activityFee, pfTotal);// added
																							// for
																							// penalty

						// setting array for subtotals
						if ((!(aFee[i].getSubtotalLevel() == null)) && (!(aFee[i].getFeeCalcOne().equals("J")))) {
							aFeeSTFlg = StringUtils.s2i(aFee[i].getSubtotalLevel());
							subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
							subTotals[aFeeSTFlg] = Double
									.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
							logger.debug("sub total value is " + aFeeSTFlg);
							logger.debug("sub total flag is " + subTotals[aFeeSTFlg]);
						}

						aFee[i].setFeeAmount(StringUtils.dbl2$(activityFee.getFeeAmount()));

						if ((aFee[i].getFeeFlagFour().equals("1") || aFee[i].getFeeFlagFour().equals("2"))) {
							pfTotal += activityFee.getFeeAmount();
							pfTotal = Double.parseDouble(new DecimalFormat("0.00").format(pfTotal));
						}

						logger.debug("Permit Fee total : " + pfTotal);
						permitLicenseFeeTotal = pfTotal;
						activityFee.setPermitLicenseFeeTotal(permitLicenseFeeTotal);
						////Calculating activity fee is paid fee or not (Ready To Pay flag)
						if (financeAgent.getActivityFeeBalanceAmount(db, activityFee)) {
							count++;
						}
						logger.debug("paidActivityFee:: " + financeAgent.getActivityFeeBalanceAmount(db, activityFee));
						if (feesMgrForm.getReadyToPay() != null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0) {
							for (int j = 0; j < adminFeeIds.size(); j++) {
								if (adminFeeIds.get(j) == activityFee.getFeeId()) {
									adminFeeCalcuated = true;
									logger.debug("admin fee calculating for Permit fee");

								}
							}
						}
					} else {
						aFee[i].setFeeAmount("$0.00");
						aFee[i].setFeeUnits("");
						aFee[i].setChecked("");
					}

				} catch (Exception e) {
					logger.warn("warning message..defaulting the fee amount, units to 0 - reason:" + e.getMessage());
					aFee[i].setFeeAmount("$0.00");
					aFee[i].setFeeUnits("");
					aFee[i].setChecked("");
				}

			}
			if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
				if(!adminFeeCalcuated){
					if(errorDisplaymessage != null && errorDisplaymessage.length()>0)
						errorDisplaymessage.append(", ");
					errorDisplaymessage.append("Permit Administration Fee");
				}
			}
			logger.debug("Permit Fee total : " + pfTotal);
			financeSummary.setPermitFeeAmt(StringUtils.dbl2$(pfTotal));

			double pfpaid = StringUtils.s2bd(financeSummary.getPermitPaidAmt()).doubleValue();
			double pfDue = pfTotal - pfpaid;
			pfDue = Double.parseDouble(new DecimalFormat("0.00").format(pfDue));
			financeSummary.setPermitAmountDue(StringUtils.dbl2$(pfDue));
			logger.debug("Permit fee due : " + pfDue);

			/********************************** Processing the Penalty Fees ***************************/

			ActivityFeeEdit[] aPenaltyFee = feesMgrForm.getPenaltyFeeList();
			logger.debug("feesMgrForm.getPenaltyFeeList()" + feesMgrForm.getPenaltyFeeList().length);
			double penaltyFeeTotal = 0;
			count = 0;
			adminFeeCalcuated = false;
			for (int i = 0; i < aPenaltyFee.length; i++) {
				try {
					String checked = aPenaltyFee[i].getChecked();

					if (!checked.equals("")) {
						activityFee = new ActivityFee(activityId, aPenaltyFee[i].getFeeId(), aPenaltyFee[i].getFeeUnits(), aPenaltyFee[i].getFeeAmount());

						logger.debug("Fee [" + i + "]" + aPenaltyFee[i].getFeeDescription());

						activityFee.setFeeInit(StringUtils.s2i(aPenaltyFee[i].getFeeInit()));
						activityFee.setFeeFactor(StringUtils.s2bd(aPenaltyFee[i].getFeeFactor()));
						activityFee.setFactor(StringUtils.s2bd(aPenaltyFee[i].getFactor()));
						activityFee.setSubtotalLevel(StringUtils.s2i(aPenaltyFee[i].getSubtotalLevel()));

						// add factor too
						// setting subTotal array to ActivityFees
						activityFee.setSubTotals(subTotals);

						// Calculate the Fee Amount
						financeAgent.calculateFeeAmountPftotal(db, activityFee, pfTotal);
						//Calculating activity fee is paid fee or not (Ready To Pay flag)
						if(financeAgent.getActivityFeeBalanceAmount(db, activityFee)){
							count++;
						}
						logger.debug("paidActivityFee:: "+financeAgent.getActivityFeeBalanceAmount(db, activityFee));
						if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
							for(int j= 0;j<adminFeeIds.size();j++){
								if(adminFeeIds.get(j) == activityFee.getFeeId()){
									adminFeeCalcuated = true; 
									logger.debug("admin fee calculating for Permit fee");
									
								}
							}
						}
						// setting array for subtotals
						if ((!(aPenaltyFee[i].getSubtotalLevel() == null))) {
							logger.debug("subtotal inside penalty" + aPenaltyFee[i].getSubtotalLevel());
							aFeeSTFlg = StringUtils.s2i(aPenaltyFee[i].getSubtotalLevel());
							subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
							subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
							logger.debug("sub total value is " + aFeeSTFlg);
							logger.debug("sub total flag is " + subTotals[aFeeSTFlg]);

						}

						aPenaltyFee[i].setFeeAmount(StringUtils.dbl2$(activityFee.getFeeAmount()));
						penaltyFeeTotal += activityFee.getFeeAmount();
						penaltyFeeTotal = Double.parseDouble(new DecimalFormat("0.00").format(penaltyFeeTotal));
					} else {
						aPenaltyFee[i].setFeeAmount("$0.00");
						aPenaltyFee[i].setFeeUnits("");
						aPenaltyFee[i].setChecked("");
					}
				} catch (Exception e) {
					aPenaltyFee[i].setFeeAmount("$0.00");
					aPenaltyFee[i].setFeeUnits("");
					aPenaltyFee[i].setChecked("");
				} finally {
				}
			}
			if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
				if(!adminFeeCalcuated){
					if(errorDisplaymessage != null && errorDisplaymessage.length()>0)
						errorDisplaymessage.append(", ");
						errorDisplaymessage.append("Penalty Administration Fee");
				}
			}
			financeSummary.setPenaltyFeeAmt(StringUtils.dbl2$(penaltyFeeTotal));
			double penaltyFeespaid = StringUtils.s2bd(financeSummary.getPenaltyPaidAmt()).doubleValue();
			double penaltyFeesDue = penaltyFeeTotal - penaltyFeespaid;
			penaltyFeesDue = Double.parseDouble(new DecimalFormat("0.00").format(penaltyFeesDue));
			financeSummary.setPenaltyAmountDue(StringUtils.dbl2$(penaltyFeesDue));
			logger.debug("Permit fee due : " + pfDue);

			/********************************** End of Penalty Fees ***************************/
			logger.debug(errorDisplaymessage.toString());
			if(errorDisplaymessage != null && errorDisplaymessage.length()>0){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.adminFeeRequired","Please Calculate "+errorDisplaymessage.toString()));
			}
			
			// ///////////

			double btamt = StringUtils.s2bd(financeSummary.getBusinessTaxFeeAmt()).doubleValue();
			double btDue = StringUtils.s2bd(financeSummary.getBusinessTaxAmountDue()).doubleValue();
			double feeAmt = plTotal + pfTotal + btamt + devTotal + penaltyFeeTotal;
			logger.debug("feeAmt : " + feeAmt);
			feeAmt = Double.parseDouble(new DecimalFormat("0.00").format(feeAmt));
			financeSummary.setTotalFeeAmt(StringUtils.dbl2$(feeAmt));

			double totalAmtDue = plDue + pfDue + btDue + devDue + penaltyFeesDue;
			totalAmtDue = Double.parseDouble(new DecimalFormat("0.00").format(totalAmtDue));
			logger.debug("totalAmtDue : " + totalAmtDue);
			financeSummary.setTotalAmountDue(StringUtils.dbl2$(totalAmtDue));
			if (!errors.empty()) {
				logger.debug("Errors Encountered during Fee Save");
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}
			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			feesMgrForm.setActivityFeeList(aFee);
			session.setAttribute("feesMgrForm", feesMgrForm);
			session.setAttribute("financeSummary", financeSummary);

			logger.info("Exiting PreviewFeesAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return (mapping.findForward(nextPage));
	}
}

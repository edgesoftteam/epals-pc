package elms.control.actions.finance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.FinanceAgent;
import elms.agent.ProjectAgent;
import elms.app.admin.PayType;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.Payment;
import elms.app.finance.PaymentDetail;
import elms.common.Constants;
import elms.control.beans.PaymentMgrForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SavePaymentMgrAction extends Action {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SavePaymentMgrAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering SavePaymentMgrAction");

		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		session.setAttribute("onloadAction", onloadAction);

		PaymentMgrForm paymentMgrForm = (PaymentMgrForm) session.getAttribute("paymentMgrForm");
		String success = "";

		try {
			int levelId = StringUtils.s2i(paymentMgrForm.getLevelId());
			User user = (User) session.getAttribute(Constants.USER_KEY);
			logger.debug("Level ID : " + levelId);

			String level = paymentMgrForm.getLevelType();
			logger.debug("level : " + level);

			session.setAttribute("levelId", request.getParameter("levelId"));
			session.setAttribute("level", level);

			char voided = 'N';
			String planCheck = "";
			String developmentFees = "";
			String permitFees = "";
			String penaltyFees = "";
			String businessTax = "";
			String other = "";
			String otherText = "";
			double amount = 0;
			String feeType = "";
			String method = "";
			String checkNo = "";
			String comments = "";
			int paidBy = 0;
			String departmentCode;
			double pcCredit = 0;
			double pmtCredit = 0;
			double developmentFeeCredit = 0;
			double penaltyCredit = 0;// added for penalty
			boolean isCheckNo = false;

			PayType payType = new PayType();
			String authorizedBy = "";

			FinanceAgent financeAgent = new FinanceAgent();

			planCheck = paymentMgrForm.getPlanCheck();

			if (planCheck == null) {
				planCheck = "off";
			}

			developmentFees = paymentMgrForm.getDevelopmentFees();

			if (developmentFees == null) {
				developmentFees = "off";
			}

			permitFees = paymentMgrForm.getPermitFees();

			if (permitFees == null) {
				permitFees = "off";
			}

			penaltyFees = paymentMgrForm.getPenaltyFees();

			if (penaltyFees == null) {
				penaltyFees = "off";
			}

			businessTax = paymentMgrForm.getBusinessTax();

			if (businessTax == null) {
				businessTax = "off";
			}

			if (planCheck.equals("on")) {
				feeType = "'1',";
			}

			if (permitFees.equals("on")) {
				feeType += "'2',";
			}

			if (developmentFees.equals("on")) {
				feeType += "'3',";
			}

			if (penaltyFees.equals("on")) {
				feeType += "'4',";
			}

			if (businessTax.equals("on")) {
				feeType += "'5',";
			}

			feeType += "'X'";

			logger.debug("plancheck= " + planCheck);
			logger.debug("permitfees= " + permitFees);
			logger.debug("developmentFees= " + developmentFees);
			logger.debug("penaltyFees= " + penaltyFees);
			logger.debug("businessTax= " + businessTax);
			logger.debug("feeType = " + feeType);

			method = paymentMgrForm.getMethod();
			logger.debug("method = " + method);

			payType.setPayTypeId(StringUtils.s2i(paymentMgrForm.getTransactionType()));
			logger.debug("Transaction Type = " + payType);

			if (payType.getPayTypeId() == 3) {
				voided = 'Y';
			}

			paidBy = elms.util.StringUtils.s2i(paymentMgrForm.getPaidBy());
			logger.debug("Paid By " + paidBy);
			other = paymentMgrForm.getOther();
			logger.debug("Other = " + other);

			if (other == null) {
				other = "off";
			}

			if (other.equals("on")) {
				otherText = paymentMgrForm.getOtherText();
				logger.debug("othertext = " + otherText);
				paidBy = -1;
			}

			amount = StringUtils.s2bd(paymentMgrForm.getAmount()).doubleValue();
			logger.debug("Amount =  " + amount);

			checkNo = paymentMgrForm.getCheckNoConfirmation();
			logger.debug("checkNo= " + checkNo);

			comments = paymentMgrForm.getComments();
			logger.debug("comment= " + comments);

			authorizedBy = paymentMgrForm.getAuthorizedBy();
			logger.debug("authorizedby= " + authorizedBy);

			pcCredit = StringUtils.s2bd(financeAgent.pcpCreditForActivity('1', levelId)).doubleValue();
			logger.debug("plan check credit= " + pcCredit);

			pmtCredit = StringUtils.s2bd(financeAgent.pcpCreditForActivity('2', levelId)).doubleValue();
			logger.debug("permit credit= " + pmtCredit);

			developmentFeeCredit = StringUtils.s2bd(financeAgent.pcpCreditForActivity('3', levelId)).doubleValue();
			logger.debug("development Fee credit= " + developmentFeeCredit);

			penaltyCredit = StringUtils.s2bd(financeAgent.pcpCreditForActivity('4', levelId)).doubleValue();
			logger.debug("penalty credit= " + penaltyCredit);

			Payment payment = new Payment(method, payType, paidBy, otherText, amount, checkNo, comments, user, authorizedBy, voided);
			payment.setLevelId(levelId);
			if(payType.getPayTypeId() == 4 && amount<0.00) {
				payment.setVoided('Y');
				payment.setVoidBy(user.getUserId());
			}
			
			logger.debug("voided:: "+voided);
			logger.debug("pc :" + planCheck + " dev_Fee : " + developmentFees + " pf :" + permitFees + " bt : " + businessTax + "act_id : " + levelId + " feeType :" + feeType);

			logger.debug("level :" + level + ": payType :" + payType.getPayTypeId());

			if (level.equals("A")) {
				departmentCode = (new ActivityAgent()).getDepartmentCode(levelId);
				payment.setDepartmentCode(departmentCode);

				switch (payType.getPayTypeId()) {
				case 1:
					logger.debug("option 1:payment of balance due");
					if ((method.equals("refund")) || (method.equals("transferOut"))) {
						payment.setAmount(0 - pcCredit - pmtCredit - developmentFeeCredit - penaltyCredit);
					}

					financeAgent.payPartialAmount(levelId, payment, feeType);

					break;

				case 3:
					logger.debug("option 3:unpay all fees");
					financeAgent.voidAllPayments(levelId, payment);

					break;

				case 4:
					logger.debug("option 4:specific fee payment");

					// *** for Specific Fee Payment
					int transactionId = 0;
					int paymentId = 0;
					int feeId = 0;
					String feeAccount = "0";// hard coded for business tax
					double paymentAmount = 0;
					double bouncedAmount = 0;

					List paymentDetailList = new ArrayList();

					ActivityFeeEdit[] fees = paymentMgrForm.getActivityFeeList();
					logger.debug(" Size of fees " + fees.length + ", : " + fees);

					for (int i = 0; i < fees.length; i++) {
						feeId = StringUtils.s2i(fees[i].getFeeId());
						logger.debug(" feeId " + feeId);
						feeAccount = fees[i].getFeeAccount();
						logger.debug(" feeAccount " + feeAccount);
						paymentAmount = Double.valueOf(fees[i].getPaymentAmount()).doubleValue();
						logger.debug(" paymentAmount " + paymentAmount);
						bouncedAmount = Double.valueOf(fees[i].getBouncedAmount()).doubleValue();
						logger.debug(" bouncedAmount " + bouncedAmount);
						logger.debug(i + ":" + feeAccount + ":" + paymentAmount + ":" + bouncedAmount);

						if (paymentAmount > 0.0) {
							if (paymentAmount > bouncedAmount) {
								bouncedAmount *= -1;
								paymentAmount += bouncedAmount;
							} else {
								bouncedAmount -= paymentAmount;
								paymentAmount = 0.0;
							}
						}

						if (paymentAmount != 0.0) {
							PaymentDetail paymentDetail = new PaymentDetail(transactionId, paymentId, levelId, feeId, paymentAmount, bouncedAmount, feeAccount);
							logger.debug("Payment Detail Obj ######### : " + paymentDetail.toString());
							paymentDetailList.add(paymentDetail);
						}
					}

					ActivityFeeEdit[] busfees = paymentMgrForm.getBusinessTaxList();
					logger.debug(" Size of busfees " + busfees.length + ", : " + busfees);
					if (busfees.length > 0) {
						for(int i=0; i<busfees.length;i++){
						feeId = StringUtils.s2i(busfees[i].getFeeId());
						logger.debug("bus feeId " + feeId);
						feeAccount = busfees[i].getFeeAccount();
						logger.debug("bus feeAccount " + feeAccount);
						paymentAmount = Double.valueOf(busfees[i].getPaymentAmount()).doubleValue();
						logger.debug("bus paymentAmount " + paymentAmount);
						logger.debug("bounced amount : " + busfees[i].getBouncedAmount());

						if (!GenericValidator.isBlankOrNull(busfees[i].getBouncedAmount())) {
							bouncedAmount = Double.valueOf(busfees[i].getBouncedAmount()).doubleValue();
						}

						logger.debug("bus bouncedAmount " + bouncedAmount);
						if (paymentAmount > 0.0) {
							if (paymentAmount > bouncedAmount) {
								bouncedAmount *= -1;
								paymentAmount += bouncedAmount;
							} else {
								bouncedAmount -= paymentAmount;
								paymentAmount = 0.0;
							}
						}
						if (paymentAmount != 0.0) {
							PaymentDetail paymentDetail = new PaymentDetail(transactionId, paymentId, levelId, feeId, paymentAmount, bouncedAmount, feeAccount);
							paymentDetail.setPeopleId(StringUtils.s2i(busfees[i].getPeopleId()));
							logger.debug("People Id ::"+paymentDetail.getPeopleId());
							logger.debug("Business t Detail Obj ######### : " + paymentDetail.toString());
							paymentDetailList.add(paymentDetail);
						}

					}
					}
					
					financeAgent.paySpecificAmount(levelId, payment, paymentDetailList);
					
					/*if(payType.getPayTypeId() == 4 && amount<0.00) {
						financeAgent.updateVoidDetails(paymentId);
					}*/
					logger.debug("voided:: "+voided);

					if (method.equals("pcCredit") && (pcCredit > 0)) {
						logger.debug("updating permit fees, development fees and plan check fees");
						financeAgent.reverseCredit(levelId, payment, "'1'");
					}

					if (method.equals("actCredit") && (pmtCredit > 0)) {
						logger.debug("updating permit fees, development fees and plan check fees");
						financeAgent.reverseCredit(levelId, payment, "'2'");
					}

					if (method.equals("developmentFeeCredit") && (developmentFeeCredit > 0)) {
						logger.debug("updating permit fees, development fees and plan check fees");
						financeAgent.reverseCredit(levelId, payment, "'3'");
					}
					/** added for penalty **/
					if (method.equals("penaltyCredit") && (penaltyCredit > 0)) {
						logger.debug("updating permit fees,penalty fees and plan check fees");
						financeAgent.reverseCredit(levelId, payment, "'4'");
					}

					break;

				}
			} else {
				departmentCode = (new ProjectAgent().getSubProjectLite(levelId).getSubProjectDetail().getSubProjectType().getDepartmentCode());
				payment.setDepartmentCode(departmentCode);

				financeAgent.paySubProjectBalance(levelId, payment);
			}
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				}
			} catch (Exception e) {
				e.getMessage();
			}
			logger.info("Exiting SavePaymentMgrAction");
			return (mapping.findForward("viewPaymentMgr"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException("unable to pay, error is " + e.getMessage());
		}

	}
}

package elms.control.actions.finance;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jdom.Document;

import elms.agent.ReportAgent;
import elms.util.Doc2Pdf;
import elms.util.StringUtils;

public class PrintBusinessTaxReportAction extends Action {
	static Logger logger = Logger.getLogger(PrintBusinessTaxReportAction.class.getName());

	protected String pdfFileName = "";
	protected String xslFileName = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		logger.debug("Entered Into the PrintBusinessTaxReportAction");
		ActionErrors errors = new ActionErrors();
		if (!errors.empty()) {
			saveErrors(request, errors);
			saveToken(request);
			logger.info("Entered PrintBusinessTaxReportAction with errors");
			return (new ActionForward(mapping.getInput()));
		} else {
			try {

				// if (actNbr.equalsIgnoreCase("")) {
				String actNbr = request.getParameter("actNbr");
				if (actNbr == null)
					actNbr = "";
				String peopleId = request.getParameter("pId");
				if (peopleId == null)
					peopleId = "";
				String report = request.getParameter("report");
				if (report == null)
					report = "";
				logger.debug("Activity Number : " + actNbr);
				logger.debug("People id : " + peopleId);
				logger.debug("Report : " + report);

				ReportAgent ra = new ReportAgent();

				xslFileName = "businesstaxReceipt.xsl";
				logger.debug("The xsl file name is " + xslFileName);

				if (report.equals("yes"))
					xslFileName = "businesstaxReport.xsl";
				else
					xslFileName = "businesstaxReceipt.xsl";

				Document document = ra.getBusinessTaxJDOM(actNbr, peopleId);

				// //for debugging start
				// XMLOutputter outputter = new XMLOutputter();
				// try {
				// outputter.setIndent("  "); // use two space indent
				// outputter.setNewlines(true);
				// outputter.output(document, System.out);
				// }catch (IOException e) {
				// logger.error(e);
				// }
				// //for debugging end

				pdfFileName = StringUtils.getUniqueFileName() + ".pdf";
				request.setAttribute("pdfFileName", pdfFileName);
				logger.debug("pdf file name is set in request " + pdfFileName);

				if (document != null) {
					String getRealPath = this.getServlet().getServletContext().getRealPath("/");
					logger.debug("Real path is " + getRealPath);
					xslFileName = getRealPath + "xsl" + File.separator + xslFileName;
					pdfFileName = getRealPath + "pdf" + File.separator + pdfFileName;
					Doc2Pdf.start(document, xslFileName, pdfFileName);
				}

			} catch (Exception e) {
				logger.warn(e.getMessage());
			}
		}
		return (mapping.findForward("success"));
	}
}
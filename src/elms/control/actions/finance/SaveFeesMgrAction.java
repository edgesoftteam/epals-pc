package elms.control.actions.finance;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.common.Constants;
import elms.control.beans.FeesMgrForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class SaveFeesMgrAction extends Action {
	static Logger logger = Logger.getLogger(SaveFeesMgrAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		ActionErrors errors = new ActionErrors();

		// setting the onloadAction of next page not to refresh PSA tree
		String onloadAction = "";
		logger.debug("Setting the refresh tree flag to " + onloadAction);
		session.setAttribute("onloadAction", onloadAction);
		String sTypeId = (session.getAttribute("sTypeId") != null) ? (String) (session.getAttribute("sTypeId")) : "";
		if (!sTypeId.equals("")) {
			session.setAttribute("online", "Y");
		}
		try {
			int activityId = elms.util.StringUtils.s2i((String) session.getAttribute(Constants.PSA_ID));
			if (activityId == -1)
				activityId = elms.util.StringUtils.s2i(request.getParameter("id"));
			FeesMgrForm feesMgrForm = (FeesMgrForm) session.getAttribute("feesMgrForm");

			logger.info("Entering SaveFeesMgrAction with ActivityId of " + activityId);

			FinanceAgent financeAgent = new FinanceAgent();
			ActivityFee activityFee = null;
			List activityFeeList = new ArrayList();
			Wrapper db = new Wrapper();

			// create an array of subtotals and initialise all to zero
			double[] subTotals = new double[6];
			double permitLicenseFeeTotal = 0;
			double total = 0.0;
			subTotals[0] = 0.0;
			subTotals[1] = 0.0;
			subTotals[2] = 0.0;
			subTotals[3] = 0.0;
			subTotals[4] = 0.0;
			subTotals[5] = 0.0;

			int aFeeSTFlg;

			
			double permitFeeAmount = 0.0;
			List<Integer> adminFeeIds = financeAgent.getReadyToPayFlagEnabledFeeList(activityId);
			int count = 0;
			boolean adminFeeCalcuated = false;
			//StringBuilder[] errorDisplaymessage= new StringBuilder[]{};
			//errorDisplaymessage[0]=new StringBuilder("Please Calculate");
			StringBuilder errorDisplaymessage = new StringBuilder();
			
			// processing the Plan Check fees
			ActivityFeeEdit[] aPcFee = feesMgrForm.getPlanCheckFeeList();
			if (aPcFee != null) {
				for (int i = 0; i < aPcFee.length; i++) {
					try {
						String checked = aPcFee[i].getChecked();
						if (!checked.equals("")) {
							activityFee = new ActivityFee();
							activityFee.setActivityId(activityId);
							activityFee.setFeeId(StringUtils.s2i(aPcFee[i].getFeeId()));
							activityFee.setFeeUnits(StringUtils.s2i(aPcFee[i].getFeeUnits()));
							activityFee.setFeeAmount(StringUtils.s2d(aPcFee[i].getFeeAmount()));
							activityFee.setFeePaid(StringUtils.s2d(aPcFee[i].getFeePaid()));
							activityFee.setAdjustmentAmount(StringUtils.s2d(aPcFee[i].getAdjustmentAmount()));
							activityFee.setCreditAmount(StringUtils.s2d(aPcFee[i].getCreditAmount()));

							activityFee.setFeeInit(StringUtils.s2i(aPcFee[i].getFeeInit()));
							activityFee.setFeeFactor(StringUtils.s2bd(aPcFee[i].getFeeFactor()));
							activityFee.setFactor(StringUtils.s2bd(aPcFee[i].getFactor()));
							activityFee.setSubtotalLevel(StringUtils.s2i(aPcFee[i].getSubtotalLevel()));

							// setting subTotal array to ActivityFees
							activityFee.setSubTotals(subTotals);

							// Calculate the Fee Amount
							financeAgent.calculateFeeAmount(db, activityFee);
							
							//Calculating activity fee is paid fee or not (Ready To Pay flag)
							if(financeAgent.getActivityFeeBalanceAmount(db, activityFee))
							{
								count++;
							}
							

							if (activityFee.getFeeAmount() == 0.00d) {
								logger.info("Calculated Fee Amount is 0.00 for fee : " + activityFee.getFeeId());
								if (errors.empty()) {
									errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.feeiszero"));
								}
							}

							// setting array for subtotals
							if (!(aPcFee[i].getSubtotalLevel() == null)) {
								aFeeSTFlg = StringUtils.s2i(aPcFee[i].getSubtotalLevel());
								subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
								subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
							}
							total = total + activityFee.getFeeAmount();
							
							// for PlanCheck fees, checking for Aministration fee if ready to pay flag enable 
							if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
								for(int j= 0;j<adminFeeIds.size();j++){
									if(adminFeeIds.get(j) == activityFee.getFeeId()){
										adminFeeCalcuated = true; 
										logger.debug("admin fee calculating for PlanCheck fee");
										
									}
								}
							}
							activityFeeList.add(activityFee);
						}
					} catch (Exception e) {
						logger.warn("Error in Processing Plan Check Fees");
					} finally {
					}
				}
				// adding error message if administration fee not calculated (ReadyToPay flag)
				if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
					if(!adminFeeCalcuated){
						errorDisplaymessage.append("Plan Check Administration Fee");
					
					}
				}
				logger.debug("Plan Check Fee List Size " + activityFeeList.size());
			}

			
			
			// processing the development fees
			// code added for development fees by Gayathri on 31stDec'08
			count = 0;
			adminFeeCalcuated = false;
			ActivityFeeEdit[] adevFee = feesMgrForm.getDevelopmentFeeList();
			logger.debug("$$$$$$ ::::: $$$$$$ " + feesMgrForm.getDevelopmentFeeList().length);

			if (adevFee != null) {
				for (int i = 0; i < adevFee.length; i++) {
					try {
						String checked = adevFee[i].getChecked();

						if (!checked.equals("")) {
							activityFee = new ActivityFee();
							activityFee.setActivityId(activityId);
							activityFee.setFeeId(StringUtils.s2i(adevFee[i].getFeeId()));
							activityFee.setFeeUnits(StringUtils.s2i(adevFee[i].getFeeUnits()));
							if (StringUtils.s2i(adevFee[i].getFeeUnits()) == -1.0 || StringUtils.s2i(adevFee[i].getFeeUnits()) < 0) {
								adevFee[i].setFeeUnits("0");
								activityFee.setFeeUnits(StringUtils.s2i(adevFee[i].getFeeUnits()));
							} else {
								activityFee.setFeeUnits(StringUtils.s2i(adevFee[i].getFeeUnits()));
							}
							logger.debug("$$$$$$ ::::: $$$$$$ " + adevFee[i].getFeeUnits());
							activityFee.setFeeAmount(StringUtils.s2d(adevFee[i].getFeeAmount()));
							activityFee.setFeePaid(StringUtils.s2d(adevFee[i].getFeePaid()));
							activityFee.setAdjustmentAmount(StringUtils.s2d(adevFee[i].getAdjustmentAmount()));
							activityFee.setCreditAmount(StringUtils.s2d(adevFee[i].getCreditAmount()));

							activityFee.setFeeInit(StringUtils.s2i(adevFee[i].getFeeInit()));
							activityFee.setFeeFactor(StringUtils.s2bd(adevFee[i].getFeeFactor()));
							activityFee.setFactor(StringUtils.s2bd(adevFee[i].getFactor()));
							activityFee.setSubtotalLevel(StringUtils.s2i(adevFee[i].getSubtotalLevel()));

							// setting subTotal array to ActivityFees
							activityFee.setSubTotals(subTotals);

							// Calculate the Fee Amount
							financeAgent.calculateFeeAmount(db, activityFee);
							
							//Calculating activity fee is paid fee or not (Ready To Pay flag)
							if(financeAgent.getActivityFeeBalanceAmount(db, activityFee))
							{
								count++;
							}

							if (activityFee.getFeeAmount() == 0.00d) {
								logger.info("Calculated Fee Amount is 0.00 for fee : " + activityFee.getFeeId());
								if (errors.empty()) {
									errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.feeiszero"));
								}
							}

							// setting array for subtotals
							if (!(adevFee[i].getSubtotalLevel() == null)) {
								aFeeSTFlg = StringUtils.s2i(adevFee[i].getSubtotalLevel());
								subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
								subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
							}
							// for Development fees, checking for Aministration fee if ready to pay flag enable
							if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count >0){
								for(int j= 0;j<adminFeeIds.size();j++){
									if(adminFeeIds.get(j) == activityFee.getFeeId()){
										adminFeeCalcuated = true; 
										logger.debug("admin fee calculating for Permit fee");
										
									}
								}
							}
							activityFeeList.add(activityFee);
						}
					} catch (Exception e) {
						logger.warn("Error in Processing Plan Check Fees");
					} finally {
					}
				}
				// adding error message if administration fee not calculated (ReadyToPay flag)
				if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
					if(!adminFeeCalcuated){
						if(errorDisplaymessage != null && errorDisplaymessage.length()>0)
							errorDisplaymessage.append(", ");
						errorDisplaymessage.append("Development Administration Fee");
					}
				}
				logger.debug("Development Fee List Size " + activityFeeList.size());
			}

			// end of the code
			
			//processing pemit fee
			count = 0;
			adminFeeCalcuated = false;
			ActivityFeeEdit[] aFee = feesMgrForm.getActivityFeeList();
			if (aFee != null) {
				for (int i = 0; i < aFee.length; i++) {
					try {
						String checked = aFee[i].getChecked();

						if (aFee[i].getFeePc().equals("X") || aFee[i].getInput().equals("X") || !checked.equals("")) {
							activityFee = new ActivityFee();
							activityFee.setActivityId(activityId);
							activityFee.setFeeId(StringUtils.s2i(aFee[i].getFeeId()));
							activityFee.setFeeUnits(StringUtils.s2d(aFee[i].getFeeUnits()));
							activityFee.setFeeAmount(StringUtils.s2d(aFee[i].getFeeAmount()));
							permitLicenseFeeTotal = permitLicenseFeeTotal + activityFee.getFeeAmount();
							activityFee.setFeeAccount(StringUtils.s2i(aFee[i].getFeeAccount()));
							activityFee.setFeePaid(StringUtils.s2d(aFee[i].getFeePaid()));
							activityFee.setAdjustmentAmount(StringUtils.s2d(aFee[i].getAdjustmentAmount()));
							activityFee.setCreditAmount(StringUtils.s2d(aFee[i].getCreditAmount()));

							activityFee.setFeeInit(StringUtils.s2i(aFee[i].getFeeInit()));
							activityFee.setFeeFactor(StringUtils.s2bd(aFee[i].getFeeFactor()));
							activityFee.setFactor(StringUtils.s2bd(aFee[i].getFactor()));
							activityFee.setSubtotalLevel(StringUtils.s2i(aFee[i].getSubtotalLevel()));
							logger.debug("activity sub total::" + aFee[i].getSubtotalLevel());
							// setting subTotal array to ActivityFees
							activityFee.setSubTotals(subTotals);
							
							// Calculate the Fee Amount
							financeAgent.calculateFeeAmount(db, activityFee);
							logger.debug("(((((( " + activityFee.getFeeAmount());
							
							//Calculating activity fee is paid fee or not (Ready To Pay flag)
							if(financeAgent.getActivityFeeBalanceAmount(db, activityFee)){
								count++;
							}
							logger.debug("paidActivityFee:: "+financeAgent.getActivityFeeBalanceAmount(db, activityFee));
							

							if ((activityFee.getFeeAmount() == 0.00d) && !checked.equals("")) {
								logger.info("Calculated Fee Amount is 0.00 for fee : " + activityFee.getFeeId());
								if (errors.empty()) {
									errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.feeiszero"));
								}
							}

							// setting array for subtotals
							if ((!(aFee[i].getSubtotalLevel() == null)) && (!(aFee[i].getFeeCalcOne().equals("J")))) {
								aFeeSTFlg = StringUtils.s2i(aFee[i].getSubtotalLevel());
								subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
								subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
							}
							if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
								for(int j= 0;j<adminFeeIds.size();j++){
									if(adminFeeIds.get(j) == activityFee.getFeeId()){
										adminFeeCalcuated = true; 
										logger.debug("admin fee calculating for Permit fee");
										
									}
								}
							}
							activityFeeList.add(activityFee);
						}
					} catch (Exception e) {
						logger.error("Error in processing Activity Fees " + e.getMessage());
					}

				}
				// adding error message if administration fee not calculated (ReadyToPay flag)
				if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
					if(!adminFeeCalcuated){
						if(errorDisplaymessage != null && errorDisplaymessage.length()>0)
							errorDisplaymessage.append(", ");
						errorDisplaymessage.append("Permit Administration Fee");
					}
				}
				logger.debug("Activity Fee List Size " + activityFeeList.size());
			}

			/****************** processing the Penalty fees ****************************/

			ActivityFeeEdit[] adFee = feesMgrForm.getPenaltyFeeList();
			count = 0;
			adminFeeCalcuated = false;
			if (adFee != null) {
				for (int i = 0; i < adFee.length; i++) {
					try {
						String checked = adFee[i].getChecked();

						if (!checked.equals("")) {
							activityFee = new ActivityFee();
							activityFee.setActivityId(activityId);
							activityFee.setFeeId(StringUtils.s2i(adFee[i].getFeeId()));
							activityFee.setFeeUnits(StringUtils.s2i(adFee[i].getFeeUnits()));
							activityFee.setFeeAmount(StringUtils.s2d(adFee[i].getFeeAmount()));
							logger.debug("Penalty Fee Amount" + adFee[i].getFeeAmount());
							activityFee.setFeePaid(StringUtils.s2d(adFee[i].getFeePaid()));
							activityFee.setAdjustmentAmount(StringUtils.s2d(adFee[i].getAdjustmentAmount()));
							activityFee.setCreditAmount(StringUtils.s2d(adFee[i].getCreditAmount()));

							activityFee.setFeeInit(StringUtils.s2i(adFee[i].getFeeInit()));
							activityFee.setFeeFactor(StringUtils.s2bd(adFee[i].getFeeFactor()));
							activityFee.setFactor(StringUtils.s2bd(adFee[i].getFactor()));
							activityFee.setSubtotalLevel(StringUtils.s2i(adFee[i].getSubtotalLevel()));

							activityFee.setPermitLicenseFeeTotal(permitLicenseFeeTotal);
							// setting subTotal array to ActivityFees
							activityFee.setSubTotals(subTotals);
							// Calculate the Fee Amount
							financeAgent.calculateFeeAmount(db, activityFee);
							
							//Calculating activity fee is paid fee or not (Ready To Pay flag)
							if(financeAgent.getActivityFeeBalanceAmount(db, activityFee))
							{
								count++;
							}

							if (activityFee.getFeeAmount() == 0.00d) {
								logger.info("Calculated Fee Amount is 0.00 for fee : " + activityFee.getFeeId());
								if (errors.empty()) {
									errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.feeiszero"));
								}
							}

							// setting array for subtotals
							if (!(adFee[i].getSubtotalLevel() == null)) {
								logger.debug("inside subtotals");
								aFeeSTFlg = StringUtils.s2i(adFee[i].getSubtotalLevel());
								subTotals[aFeeSTFlg] += activityFee.getFeeAmount();
								subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
							}
							// for Penalty fees, checking for Aministration fee if ready to pay flag enable
							if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count >0){
								for(int j= 0;j<adminFeeIds.size();j++){
									if(adminFeeIds.get(j) == activityFee.getFeeId()){
										adminFeeCalcuated = true; 
										logger.debug("admin fee calculating for Permit fee");
										
									}
								}
							}
							activityFeeList.add(activityFee);
						}
					} catch (Exception e) {
						logger.warn("Error in Processing Plan Check Fees");
					} finally {
					}
				}
				// adding error message if administration fee not calculated (ReadyToPay flag)
				if(feesMgrForm.getReadyToPay()!=null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on") && count > 0){
					if(!adminFeeCalcuated){
						if(errorDisplaymessage != null && errorDisplaymessage.length()>0)
						errorDisplaymessage.append(", ");
						errorDisplaymessage.append("Penalty Administration Fee");
					}
				}
				logger.debug("Activity Fee List Size " + activityFeeList.size());
			}

			/****************** End of Penalty fees *****************************************/
			logger.debug(errorDisplaymessage.toString());
			if(errorDisplaymessage != null && errorDisplaymessage.length()>0){
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.adminFeeRequired","Please Calculate "+errorDisplaymessage.toString()));
			}
			if (!errors.empty()) {
				logger.debug("Errors Encountered during Fee Save");
				saveErrors(request, errors);
				return (new ActionForward(mapping.getInput()));
			}
			financeAgent.saveFeesList(activityId, activityFeeList);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for finance
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for ledger
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for activity
				}
			} catch (Exception e) {
				e.getMessage();
			}
			if (!errors.empty()) {
				logger.debug("Errors Encountered during Fee Save");
				saveErrors(request, errors);

				// return (new ActionForward(mapping.getInput()));
			}
			User user = (User) session.getAttribute(Constants.USER_KEY);
			logger.debug("readyToPay ::"+feesMgrForm.getReadyToPay()+":: activityId ::"+activityId);
        	if(feesMgrForm.getReadyToPay()!= null && feesMgrForm.getReadyToPay().equalsIgnoreCase("on")){
        		feesMgrForm.setReadyToPay("Y");
        	}else{
        		feesMgrForm.setReadyToPay("N");
        		if(errors.empty())
        		errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.finance.readyToPayFlagUncheckMessage"));
        	}
        	//Updating ReadyToPay flag in ONLINE_PAYMENT_FLAG table
			financeAgent.updateReadyToPayFlag(activityId,feesMgrForm.getReadyToPay(),user.getUserId());
			if (!errors.empty()) {
				logger.debug("Please remove Administration Fee if payment type is not credit card");
				saveErrors(request, errors);
			}
			
			logger.info("Exiting SaveFeesMgrAction");
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new ServletException("Exception occured while saving fees " + e.getMessage());
		}

	}
}

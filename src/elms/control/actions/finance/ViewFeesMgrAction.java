package elms.control.actions.finance;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.FinanceAgent;
import elms.agent.LookupAgent;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.FinanceSummary;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.app.project.GreenHaloNewProjectResponse;
import elms.control.beans.FeesMgrForm;
import elms.util.StringUtils;

public class ViewFeesMgrAction extends Action {

	static Logger logger = Logger.getLogger(ViewFeesMgrAction.class.getName());
	private String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		try {
			logger.info("Entering ViewFeesMgrAction");
			FinanceAgent financeAgent = new FinanceAgent();
			int activityId = 0;
            
			if (!(request.getParameter("id") == null)) {
				activityId = Integer.parseInt(request.getParameter("id"));
			} else {
				activityId = Integer.parseInt((String) request.getAttribute("id"));
			}

			String actType = new ActivityAgent().getActivityType(activityId);
			String sTypeId = "";
			if (request.getParameter("sTypeId") != null) {
				sTypeId = (String) (request.getParameter("sTypeId"));
				session.setAttribute("sTypeId", sTypeId);
				session.setAttribute("online", "Y");
			} else if (session.getAttribute("sTypeId") != null) {
				sTypeId = (String) (session.getAttribute("sTypeId"));
			} else {
				sTypeId = "";
			}

			logger.debug("Activity Id :" + activityId);

			String view = request.getParameter("view");
			String backUrl = "";

			if (view == null) {
				view = "";
			}

			if (view.equals("pw")) {
				nextPage = "publicworks";

				String activityType = LookupAgent.getActivityTypeForActId(StringUtils.i2s(activityId));

				if (activityType == null) {
					activityType = "";
				}

				backUrl = request.getContextPath() + "/viewActivity.do?activityId=" + activityId;

				logger.debug("Back URL is " + backUrl);
			} else {
				nextPage = "success";
			}
			String online = session.getAttribute("online") != null ? (String) session.getAttribute("online") : "";
			if (online.equals("Y")) {
				nextPage = "online";
			}
			logger.debug("next page " + nextPage);

			// This Code is for smart navigation to bus tax area
			if (request.getParameter("smrt") == null) {
				request.setAttribute("onloadScript", "frmLoad()");
			} else {
				request.setAttribute("onloadScript", "frmLoad();window.scrollTo(0,20000)");
			}

			logger.debug("On Load Script : " + request.getAttribute("onloadScript"));

			FeesMgrForm feesMgrForm = new FeesMgrForm();
			feesMgrForm.setActivityId(activityId);
			feesMgrForm.setBackUrl(backUrl);

			String planCheckFeesRequired = "";
			String developmentFeesRequired = "";
			

			if (nextPage.equals("success") || nextPage.equals("online")) { // for Building and Safety

				ActivityDetail activityDetail = new FinanceAgent().getActivityDetail(activityId);
				String planCheckFeeDate = StringUtils.cal2str(activityDetail.getPlanCheckFeeDate());
				String developmentFeeDate = StringUtils.cal2str(activityDetail.getDevelopmentFeeDate());
				String activityFeeDate = StringUtils.cal2str(activityDetail.getActivityFeeDate());
				String valuation = StringUtils.dbl2$(activityDetail.getValuation());
				planCheckFeesRequired = (activityDetail.getPlanCheckRequired());

				developmentFeesRequired = (activityDetail.getDevelopmentFeeRequired());

				feesMgrForm.setPermitFeeDate(activityFeeDate);
				feesMgrForm.setPlanCheckFeeDate(planCheckFeeDate);
				feesMgrForm.setDevelopmentFeeDate(developmentFeeDate);
				logger.debug("^^^^ " + feesMgrForm.getDevelopmentFeeDate());
				feesMgrForm.setValuation(valuation);
				feesMgrForm.setPlanCheckFeesRequired(planCheckFeesRequired);
				if (sTypeId.equals(""))
					feesMgrForm.setPlanCheckFeeList(financeAgent.getFeeList(activityId));
				else
					feesMgrForm.setPlanCheckFeeList(financeAgent.getOnlineFeeList(activityId, sTypeId));

				// code added for development fees by Gayathri on 17thDec'08
				feesMgrForm.setDevelopmentFeeDate(developmentFeeDate);
				feesMgrForm.setDevelopmentFeesRequired(developmentFeesRequired);
				feesMgrForm.setDevelopmentFeeList(financeAgent.getDevelopmentFeeList(activityId));

				feesMgrForm.setBusinessTaxList(financeAgent.getBusinessTaxList(activityId));
			}

			FinanceSummary financeSummary = financeAgent.getActivityFinance(activityId);
			logger.debug("++++++ " + financeSummary.getDevelopmentFeeAmt());
			feesMgrForm.setFinanceSummary(financeSummary);
			
			if(actType.equalsIgnoreCase("RONPER") || actType.equalsIgnoreCase("CONPER") || actType.equalsIgnoreCase("MONPER")){
				//String feesubtype = new ActivityAgent().getMultipleActivitySubTypes(activityId);
				feesMgrForm.setActivityFeeList(financeAgent.getNewOnlineFeeList(activityId));
			}else {
			
			if (sTypeId.equals(""))
				feesMgrForm.setActivityFeeList(financeAgent.getFeeList(activityId));
			else
				feesMgrForm.setActivityFeeList(financeAgent.getOnlineFeeList(activityId, sTypeId));
			if (sTypeId.equals(""))
				feesMgrForm.setPenaltyFeeList(financeAgent.getFeeList(activityId));// added for penalty
			else
				feesMgrForm.setPenaltyFeeList(financeAgent.getOnlineFeeList(activityId, sTypeId));// added for penalty

			if (planCheckFeesRequired.equals("N")) {
				ActivityFeeEdit[] fees = feesMgrForm.getActivityFeeList();

				for (int i = 0; i < fees.length; i++) {
					if (fees[i].getPlanCheckFees().equals("Y")) {
						fees[i].setRequired("");
					}

					feesMgrForm.setActivityFeeList(fees);
				}
			}
			feesMgrForm.setReadyToPay(StringUtils.YNtoOnOff(financeAgent.getReadyToPayFlag(activityId)));
			}
			
			Map busTaxList = new HashMap();
			busTaxList = new FinanceAgent().getBusTaxList();

			session.setAttribute("busTaxMin", busTaxList.get("busTaxMin"));
			session.setAttribute("busTaxMax", busTaxList.get("busTaxMax"));
			session.setAttribute("busTax", busTaxList.get("busTax"));
			ActivityAgent activityAgent = new ActivityAgent();
			Activity activity = activityAgent.getActivity(activityId);
			feesMgrForm.setGreenHalo(activity.getGreenHalo());
			session.setAttribute("greenHalo", activity.getGreenHalo());
			GreenHaloNewProjectResponse   projectResponse = activityAgent.getGreenHaloProjectResponse(activityId);
			if(projectResponse == null || projectResponse.getStatus()== null || projectResponse.getStatus().isEmpty() || projectResponse.getStatus().equalsIgnoreCase("fail") ){
				session.setAttribute("greenHaloButton", "Create Green Halo Project");
			}else{
				session.setAttribute("greenHaloButton", "");
			}
			logger.info("ViewFeesMgrAction  :: greenHalo ============> " + activity.getGreenHalo());

			session.setAttribute("feesMgrForm", feesMgrForm);
			session.setAttribute("financeSummary", financeSummary);
			logger.info("Exiting ViewFeesMgrAction" + nextPage);
			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException(e);
		}

	}
}

package elms.control.actions.finance;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger; 
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.security.User;
public class ReversePaymentAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ReversePaymentAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		int activityId =  Integer.parseInt(request.getParameter("activityId"));
		int paymentId = Integer.parseInt(request.getParameter("paymentId"));
		int userId = user.getUserId();
		FinanceAgent financeAgent = new FinanceAgent();
		logger.debug("activityId:: "+activityId);
		logger.debug("paymentId:: "+paymentId);
		logger.debug("Reversing specific transaction");

		financeAgent.reverseSpecificTransaction(activityId, paymentId,user);
		
		return (mapping.findForward("success"));
	}
}
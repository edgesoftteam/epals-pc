package elms.control.actions.finance;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.FinanceAgent;
import elms.agent.LookupAgent;
import elms.app.finance.FinanceSummary;
import elms.control.beans.PaymentMgrForm;
import elms.util.StringUtils;

public class ViewPaymentMgrAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ViewPaymentMgrAction.class.getName());
	private String nextPage = "success";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		int levelId = Integer.parseInt((String) session.getAttribute("levelId"));
		String level = (String) session.getAttribute("level");
		logger.info("Entering ViewPaymentMgrAction with levelId of " + levelId);
		FinanceAgent financeAgent = new FinanceAgent();
		try {
			String action = request.getParameter("action");
            if(action!=null && action.equalsIgnoreCase("getReadyToPayFlag")){
            	int activityId = StringUtils.s2i(request.getParameter("actId"));
            	logger.debug(":: activityId ::"+activityId);
            	
            	PrintWriter pw = response.getWriter();
            	pw.write(financeAgent.getReadyToPayFlag(activityId));
            	return null;
            }
         
			String backUrl = "";
			String view = request.getParameter("view");

			if (view == null) {
				view = "";
			}

			if (view.equals("pw")) {
				nextPage = "publicworks";

				if (level.equals("A")) {
					String activityType = LookupAgent.getActivityTypeForActId(StringUtils.i2s(levelId));

					if (activityType == null) {
						activityType = "";
					}

					request.setAttribute("activityType", activityType);
					logger.debug("ActivityType: " + activityType);

					backUrl = request.getContextPath() + "/viewActivity.do?activityId=" + levelId;

					logger.debug("Back URL is " + backUrl);
				}
			} else {
				nextPage = "success";
			}

			
			FinanceSummary financeSummary = new FinanceSummary();

			if (level.equals("A")) {
				financeSummary = financeAgent.getActivityFinance(levelId);

				String activityType = LookupAgent.getActivityTypeForActId(StringUtils.i2s(levelId));

				if (activityType == null) {
					activityType = "";
				}

				request.setAttribute("activityType", activityType);
				logger.debug("Print ActivityType: " + activityType);

			}

			if (level.equals("Q")) {
				financeSummary = financeAgent.getSubProjectFinance(levelId);
			}

			request.setAttribute("levelId", "" + levelId);
			request.setAttribute("level", level);

			session.removeAttribute("paymentMgrForm");

			ActionForm frm = new PaymentMgrForm();
			PaymentMgrForm paymentMgrForm = (PaymentMgrForm) frm;

			request.setAttribute("financeSummary", financeSummary);
			paymentMgrForm.setFinanceSummary(financeSummary);
			paymentMgrForm.setPlanCheck("false");
			paymentMgrForm.setPermitFees("false");
			paymentMgrForm.setDevelopmentFees("false");
			paymentMgrForm.setBusinessTax("false");
			paymentMgrForm.setLevelId("" + levelId);
			paymentMgrForm.setLevelType(level);
			paymentMgrForm.setAmount(financeSummary.getTotalAmountDue());
			paymentMgrForm.setActivityFeeList(financeAgent.getActivityFeeList(levelId));
			paymentMgrForm.setBusinessTaxList(financeAgent.getBusinessTaxList(levelId));
			paymentMgrForm.setBackUrl(backUrl);

			session.setAttribute("paymentMgrForm", paymentMgrForm);

			logger.info("Exiting ViewPaymentMgrAction");
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		logger.debug("next page in view payment " + nextPage);

		return (mapping.findForward(nextPage));
	}
}

package elms.control.actions.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.SearchAgent;
import elms.app.project.Activity;
import elms.control.beans.BasicSearchForm;

public class ViewQuickSearchAction extends Action {

	static Logger logger = Logger.getLogger(ViewQuickSearchAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		SearchAgent agent = new SearchAgent();
		String nextPage = "";
		try {

			String strVal = (String) request.getParameter("searchVal") != null ? (String) request.getParameter("searchVal") : "";
			logger.debug("got : " + strVal);

			Pattern act = Pattern.compile("[A-Za-z][A-Za-z][0-9]*");

			if (act.matcher(strVal).matches()) {
				logger.info("inside activity search...");
				BasicSearchForm basicSearchForm = new BasicSearchForm();
				List actList = new ArrayList();
				if (!strVal.equals(""))
					actList = agent.searchActivities(strVal);
				int count = 0;
				boolean check = true;
				for (int i = 0; i < actList.size(); i++) {
					String number = ((Activity) (actList.get(i))).getActivityDetail().getActivityNumber();
					for (int j = i + 1; j < actList.size(); j++) {
						if (number.equalsIgnoreCase(((Activity) (actList.get(j))).getActivityDetail().getActivityNumber())) {
							check = false;
							break;
						}
					}
					if (check)
						count++;
					check = true;
				}
				if (actList.size() == 1) {
					Activity activity = (Activity) actList.get(0);
					basicSearchForm.setActivityId(activity.getActivityId());
					basicSearchForm.setLsoId(activity.getActivityDetail().getLsoId());
					request.setAttribute("activityId", "" + activity.getActivityId());
					request.setAttribute("lsoId", "" + activity.getActivityDetail().getLsoId());
				}
				request.setAttribute("actList", actList);
				request.setAttribute("count", "" + count);
				basicSearchForm.setActivityList(actList);
				basicSearchForm.setActivityListSize("" + actList.size());
				request.setAttribute("basicSearchForm", basicSearchForm);
				nextPage = "successActivity";
			} else{
				logger.debug("No known patterns matched...showing error message");
				nextPage = "noResults";
			}
		} catch (Exception e) {
			logger.error("Exception thrown in the basic search action " + e.getMessage());
			throw new ServletException(e.getMessage());
		}
		return (mapping.findForward(nextPage));

	}
}

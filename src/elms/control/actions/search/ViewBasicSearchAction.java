package elms.control.actions.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.LookupAgent;
import elms.agent.AddressAgent;
import elms.common.Constants;
import elms.control.beans.BasicSearchForm;
import elms.security.User;

public class ViewBasicSearchAction extends Action {

	static Logger logger = Logger.getLogger(ViewBasicSearchAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		logger.info("Entering ViewBasicSearchAction");
		String from = request.getParameter("from") != null ? request.getParameter("from") : "";
		try {

			BasicSearchForm basicSearchForm = (BasicSearchForm) form;
			if (from.equalsIgnoreCase("searchTab")) {
				// This is to prevent the load of date from previous session when user clicks on the tab.
				logger.debug("From Search Tab");
				basicSearchForm = new BasicSearchForm();
			}

			User user = (User) session.getAttribute(Constants.USER_KEY);
			String userType = (String) session.getAttribute(Constants.USER_TYPE);
			session.setAttribute(Constants.USER_KEY, user);
			session.setAttribute(Constants.USER_TYPE, userType);

			AdminAgent userAgent = new AdminAgent();
			basicSearchForm.setUserList(userAgent.getNameList());

			List multiStreetList = new AddressAgent().getOnlyStreetNameArrayList();
			request.setAttribute("multiStreetList", multiStreetList);

			List sptList = LookupAgent.getSubProjectTypes();
			request.setAttribute("sptList", sptList);

			List spstList = LookupAgent.getSubProjectSubTypes();
			request.setAttribute("spstList", spstList);

			List ptList = LookupAgent.getSubProjectNames("ALL");
			request.setAttribute("ptList", ptList);

			List atList = LookupAgent.getActivityTypesForFees();
			request.setAttribute("atList", atList);

			List atsList = LookupAgent.getActivitySubTypes();
			request.setAttribute("atsList", atsList);

			List userList = new AdminAgent().getNameList();
			request.setAttribute("userList", userList);

			List atStatus = LookupAgent.getActivityStatusesForFees();
			request.setAttribute("atStatus", atStatus);

			List crossStreets = new ArrayList();
			String crossStreetName1 = GenericValidator.isBlankOrNull(basicSearchForm.getCrossStreetName1()) ? "-1" : basicSearchForm.getCrossStreetName1();
			logger.debug("Cross Street 1 selected is " + crossStreetName1);
			if (!crossStreetName1.equalsIgnoreCase("-1")) {
				logger.debug("Cross Street 1 is " + crossStreetName1);
				basicSearchForm.setCrossStreetName1(crossStreetName1);
				request.setAttribute("crossStreetName1", crossStreetName1);
				crossStreets = new AddressAgent().getCrossStreets(crossStreetName1);
			}

			request.setAttribute("crossStreets", crossStreets);

			List landmarks = LookupAgent.getLandmarks();
			request.setAttribute("landmarks", landmarks);

			request.setAttribute("basicSearchForm", basicSearchForm);
			request.setAttribute("userList", basicSearchForm.getUserList());
			session.removeAttribute(Constants.LSO_ID);
			session.setAttribute("refreshTree", "N");
			session.setAttribute("onLoadAction", "N");
			logger.info("Exiting ViewBasicSearchAction");

			return (mapping.findForward("success"));

		} catch (Exception e) {
			logger.error("Exception thrown in the basic search action " + e.getMessage());
			throw new ServletException(e.getMessage());
		}

	}
}

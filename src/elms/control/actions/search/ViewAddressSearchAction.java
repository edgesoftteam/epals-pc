package elms.control.actions.search;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AddressAgent;
import elms.app.lso.Address;
import elms.app.lso.Street;
import elms.control.beans.AddressSummaryForm;
import elms.control.beans.BasicSearchForm;
import elms.util.StringUtils;

/**
 * @author shekhar
 * 
 */
public class ViewAddressSearchAction extends Action {
	static Logger logger = Logger.getLogger(ViewAddressSearchAction.class.getName());
	protected String nextPage;

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering ViewAddressSearchAction");

		HttpSession session = request.getSession();
		BasicSearchForm addrForm = (BasicSearchForm) form;

		if (mapping.getAttribute() != null) {
			if ("request".equals(mapping.getScope())) {
				request.removeAttribute(mapping.getAttribute());
			} else {
				session.removeAttribute(mapping.getAttribute());
			}
		}

		// Determine if this is a searvh for an address
		if ((addrForm.getStreetId() == null) || addrForm.getStreetId().equals("-1")) {
			request.setAttribute("basicSearchForm", addrForm);
			nextPage = "basicSearch";
		} else {
			AddressAgent treeAgent = new AddressAgent();

			// Get the list of lands that match the specified Street Address
			AddressAgent streetAgent = new AddressAgent();
			Street street = streetAgent.getStreet(StringUtils.s2i(addrForm.getStreetId()));
			Address address = new Address(StringUtils.s2i(addrForm.getStreetNumber()), addrForm.getStreetFraction(), street, addrForm.getUnitNbr());
			List landList;
			try {
				landList = treeAgent.getLandId(address, true);
			} catch (Exception e1) {
				throw new ServletException("couldn not get land id " + e1.getMessage());
			}
			if (landList.size() == 1) {
				// Since there is only one match we can jump straight to the
				// AddressSummary Page
				Long landId = (Long) landList.get(0);
				Long lsoMatch;
				try {
					lsoMatch = treeAgent.getLsoId(address, true);
				} catch (Exception e1) {
					throw new ServletException("couldn not get lso id " + e1.getMessage());
				}

				AddressSummaryForm frm = new AddressSummaryForm();
				frm.setAddress(address.toString());
				logger.debug("Address is " + frm.getAddress());
				frm.setLsoId(landId.toString());
				frm.setLsoMatch(lsoMatch.toString());
				session.setAttribute("addressSummaryForm", frm);
				nextPage = "successOne";
			} else {
				// Get the list of addresses for the Land List
				try {
					landList = treeAgent.getLsoIdList(addrForm.getStreetNumber(), addrForm.getStreetId());
				} catch (Exception e1) {
					throw new ServletException("couldn not get getLsoIdList " + e1.getMessage());
				}
				List lsoList;
				try {
					lsoList = treeAgent.getLsoTreeList(landList, request.getContextPath());
				} catch (Exception e1) {
					throw new ServletException("couldn not get getLsoTreeList " + e1.getMessage());
				}

				request.setAttribute("lsoList", lsoList);

				String highlight = "search";
				request.setAttribute("hl", highlight);
				request.setAttribute("addressSearchForm", addrForm);
				request.setAttribute("streetId", addrForm.getStreetId());
				nextPage = "successMany";
			}
		}

		logger.info("Exiting ViewAddressSearchAction :" + nextPage);

		return (mapping.findForward(nextPage));
	}
}

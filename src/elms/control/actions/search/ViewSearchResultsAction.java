package elms.control.actions.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.ActivityAgent;
import elms.agent.AddressAgent;
import elms.agent.ProjectAgent;
import elms.app.bl.BusinessOwner;
import elms.app.project.Activity;
import elms.control.beans.BasicSearchForm;
import elms.util.StringUtils;

/**
 * This class searches the OBC database based on the query strings and displays results
 */
public class ViewSearchResultsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ViewSearchResultsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		try {

			HttpSession session = request.getSession(true);

			BasicSearchForm basicSearchForm = (BasicSearchForm) form;

			String nextPage = "search";

			boolean searchAddress = false;
			boolean searchProject = false;
			boolean searchActivity = false;
			boolean searchSubProject = false;
			boolean searchOwner = false;
			boolean searchForeignAddress = false;
			boolean searchBusinessOwner = false;
			boolean searchOutofTownAddress = false;

			ActionErrors errors = new ActionErrors();

			String streetNumber = (basicSearchForm.getStreetNumber() == null) ? "" : basicSearchForm.getStreetNumber();
			logger.debug("got streetNumber: " + streetNumber);

			String streetFraction = (basicSearchForm.getStreetFraction() == null) ? "" : basicSearchForm.getStreetFraction();
			logger.debug("got streetFraction: " + streetFraction);

			String streetName = (basicSearchForm.getStreetName() == null) ? "" : basicSearchForm.getStreetName();
			logger.debug("got streetName: " + streetName);

			String crossStreetName1 = (basicSearchForm.getCrossStreetName1() == null) ? "" : basicSearchForm.getCrossStreetName1();
			logger.debug("got crossStreetName1: " + streetName);

			String crossStreetName2 = (basicSearchForm.getCrossStreetName2() == null) ? "" : basicSearchForm.getCrossStreetName2();
			logger.debug("got crossStreetName2: " + streetName);

			String apn = (basicSearchForm.getApn() == null) ? "" : basicSearchForm.getApn();
			logger.debug("got apn: " + apn);

			String streetName2 = (basicSearchForm.getStreetName2() == null) ? "" : basicSearchForm.getStreetName2();
			logger.debug("got streetName2: " + streetName2);

			String ownerName = (basicSearchForm.getOwnerName() == null) ? "" : basicSearchForm.getOwnerName().toUpperCase();
			logger.debug("got OwnerName: " + ownerName);

			String projectTeam = (basicSearchForm.getProjectTeam() == null) ? "" : basicSearchForm.getProjectTeam();
			logger.debug("got projectTeam: " + projectTeam);

			String personType = (basicSearchForm.getPersonType() == null) ? "" : basicSearchForm.getPersonType();
			logger.debug("got personType: " + personType);

			String personInfoType = (basicSearchForm.getPersonInfoType() == null) ? "" : basicSearchForm.getPersonInfoType();
			logger.debug("got personInfoType: " + personInfoType);

			String projectName = (basicSearchForm.getProjectName() == null) ? "" : basicSearchForm.getProjectName();
			String name = (basicSearchForm.getName() == null) ? "" : basicSearchForm.getName();
			logger.debug("got name: " + name);

			String peopleName = (basicSearchForm.getPeopleName() == null) ? "" : basicSearchForm.getPeopleName();
			logger.debug("got peopleName: " + peopleName);

			String peopleLic = (basicSearchForm.getPeopleLic() == null) ? "" : basicSearchForm.getPeopleLic();
			logger.debug("got peopleLic: " + peopleLic);

			String projectNumber = (basicSearchForm.getProjectNumber() == null) ? "" : basicSearchForm.getProjectNumber();
			logger.debug("got projectNumber: " + projectNumber);

			String subProjectNumber = (basicSearchForm.getSubProjectNumber() == null) ? "" : basicSearchForm.getSubProjectNumber();
			logger.debug("got Sub Project Number: " + subProjectNumber);

			String projectType = (basicSearchForm.getProjectType() == null) ? "" : basicSearchForm.getProjectType();
			logger.debug("got projectType: " + projectType);

			String subProjectType = (basicSearchForm.getSubProjectType() == null) ? "" : basicSearchForm.getSubProjectType();
			logger.debug("got subProjectType: " + subProjectType);

			String subProjectSubType = (basicSearchForm.getSubProjectSubType() == null) ? "" : basicSearchForm.getSubProjectSubType();
			logger.debug("got subProjectSubType: " + subProjectSubType);

			String activityNumber = (basicSearchForm.getActivityNumber() == null) ? "" : basicSearchForm.getActivityNumber();
			logger.debug("got activityNumber: " + activityNumber);

			// added for foreign address on 08/29/05
			String foreignAddressLine1 = (basicSearchForm.getForeignAddressLine1() == null) ? "" : basicSearchForm.getForeignAddressLine1().toUpperCase();
			logger.debug("got ForeignAddressLine1: " + foreignAddressLine1);

			String foreignAddressLine2 = (basicSearchForm.getForeignAddressLine2() == null) ? "" : basicSearchForm.getForeignAddressLine2().toUpperCase();
			logger.debug("got ForeignAddressLine2: " + foreignAddressLine2);

			String foreignAddressLine3 = (basicSearchForm.getForeignAddressLine3() == null) ? "" : basicSearchForm.getForeignAddressLine3().toUpperCase();
			logger.debug("got ForeignAddressLine3: " + foreignAddressLine3);

			String foreignAddressLine4 = (basicSearchForm.getForeignAddressLine4() == null) ? "" : basicSearchForm.getForeignAddressLine4().toUpperCase();
			logger.debug("got ForeignAddressLine4: " + foreignAddressLine4);

			String foreignAddressState = (basicSearchForm.getForeignAddressState() == null) ? "" : basicSearchForm.getForeignAddressState().toUpperCase();
			logger.debug("got ForeignAddressState: " + foreignAddressState);

			String foreignAddressCountry = (basicSearchForm.getForeignAddressCountry() == null) ? "" : basicSearchForm.getForeignAddressCountry().toUpperCase();
			logger.debug("got ForeignAddressCountry: " + foreignAddressCountry);

			String foreignAddressZip = (basicSearchForm.getForeignAddressZip() == null) ? "" : basicSearchForm.getForeignAddressZip().toUpperCase();
			logger.debug("got ForeignAddressZip: " + foreignAddressZip);

			String businessAccountNumber = (basicSearchForm.getBusinessAcNumber() == null) ? "" : basicSearchForm.getBusinessAcNumber().toUpperCase();
			logger.debug("got BusinessAcountNumber:" + businessAccountNumber);

			String businessName = (basicSearchForm.getBusinessName() == null) ? "" : basicSearchForm.getBusinessName().toUpperCase();
			logger.debug("got BusinessName:" + businessName);

			String businessOwnerName = (basicSearchForm.getBusinessOwnerName() == null) ? "" : basicSearchForm.getBusinessOwnerName().toUpperCase();
			logger.debug("got BusinessOwnerName:" + businessOwnerName);

			String owner1 = (basicSearchForm.getOwner1() == null) ? "" : basicSearchForm.getOwner1().toUpperCase();
			logger.debug("got Owner1:" + owner1);

			String owner2 = (basicSearchForm.getOwner2() == null) ? "" : basicSearchForm.getOwner2().toUpperCase();
			logger.debug("got Owner2:" + owner2);

			String outofTownAddress1 = (basicSearchForm.getOutofTownAddress1() == null) ? "" : basicSearchForm.getOutofTownAddress1().toUpperCase();
			logger.debug("got OutofTownaddress1:" + outofTownAddress1);

			String outofTownAddress2 = (basicSearchForm.getOutofTownAddress2() == null) ? "" : basicSearchForm.getOutofTownAddress2().toUpperCase();
			logger.debug("got OutofTownAddress2:" + outofTownAddress2);

			String outofTownAddress3 = (basicSearchForm.getOutofTownAddress3() == null) ? "" : basicSearchForm.getOutofTownAddress3().toUpperCase();
			logger.debug("got OutofTownAddress3:" + outofTownAddress3);

			String outofTownAddress4 = (basicSearchForm.getOutofTownAddress4() == null) ? "" : basicSearchForm.getOutofTownAddress4().toUpperCase();
			logger.debug("got OutofTownAddress4:" + outofTownAddress4);

			String outofTownAddressState = (basicSearchForm.getOutofTownAddressState() == null) ? "" : basicSearchForm.getOutofTownAddressState().toUpperCase();
			logger.debug("got OutofTownAddressState:" + outofTownAddressState);

			String outofTownAddressCountry = (basicSearchForm.getOutofTownAddressCountry() == null) ? "" : basicSearchForm.getOutofTownAddressCountry().toUpperCase();
			logger.debug("got OutofTownAddressCountry:" + outofTownAddressCountry);

			String outofTownAddressZip = (basicSearchForm.getOutofTownAddressZip() == null) ? "" : basicSearchForm.getOutofTownAddressZip().toUpperCase();
			logger.debug("got OutofTownAddressZip:" + outofTownAddressZip);

			String landmark = (basicSearchForm.getLandmark() == null) ? "" : basicSearchForm.getLandmark();
			logger.debug("got Landmark:" + landmark);

			String[] activityType = basicSearchForm.getActivityType();

			if (activityType == null) {
				activityType = new String[0];
			}

			String[] activityStatus = basicSearchForm.getActivityStatus();

			if (activityStatus == null) {
				activityStatus = new String[0];
			}

			String[] activitySubType = basicSearchForm.getActivitySubType();

			if (activitySubType == null) {
				activitySubType = new String[0];
			}

			String streetName1 = (basicSearchForm.getStreetName1() == null) ? "" : basicSearchForm.getStreetName1();
			logger.debug("got streetName1: " + streetName1);

			if ((foreignAddressLine1.length() > 0) || (foreignAddressLine2.length() > 0) || (foreignAddressLine3.length() > 0) || (foreignAddressLine4.length() > 0) || (foreignAddressCountry.length() > 0) || (foreignAddressState.length() > 0) || (foreignAddressZip.length() > 0)) {
				searchForeignAddress = true;
				logger.debug("searching Foreign Address");

			}

			if ((streetNumber.length() > 0) || (streetFraction.length() > 0) || ((streetName != null) && !streetName.equals("") && !streetName.equals("-1")) || (apn.length() > 0) || ((streetName != null) && !streetName2.equals("") && !streetName2.equals("-1"))) {
				searchAddress = true;
				logger.debug("searching Address");

			}

			if ((ownerName.length() > 0) && (searchForeignAddress == false)) {
				searchAddress = false;
				searchOwner = true;
				logger.debug("searching owner");

			}

			if ((outofTownAddress1.length() > 0) || (outofTownAddress2.length() > 0) || (outofTownAddress3.length() > 0) || (outofTownAddress4.length() > 0) || (outofTownAddressCountry.length() > 0) || (outofTownAddressState.length() > 0) || (outofTownAddressZip.length() > 0)) {
				searchOutofTownAddress = true;
				logger.debug("searching OOT Address");

			}

			logger.debug("searchOutofTownAddress is" + searchOutofTownAddress);

			if ((businessOwnerName.length() > 0) || (businessAccountNumber.length() > 0) || (businessName.length() > 0) && (searchOutofTownAddress == false)) {
				searchBusinessOwner = true;
				searchOutofTownAddress = false;
				logger.debug("searching business owner");

			}

			logger.debug("searchOutofTownAddress is" + searchOutofTownAddress);
			logger.debug("searchBusinessOwner is" + searchBusinessOwner);

			if ((projectNumber.length() > 0) || (projectName.length() > 0)) {
				searchProject = true;
				searchAddress = false;
				searchOwner = false;
				searchBusinessOwner = false;
				searchOutofTownAddress = false;
				logger.debug("searching project");
			}

			if ((subProjectNumber.length() > 0) || (projectType.length() > 0) || (subProjectType.length() > 0) || (subProjectSubType.length() > 0)) {
				searchProject = false;
				searchAddress = false;
				searchSubProject = true;
				searchOwner = false;
				searchBusinessOwner = false;
				searchOutofTownAddress = false;
				logger.debug("searching sub-project");
			}

			if ((activityNumber.length() > 0) || (activityStatus.length > 0) || (activityType.length > 0) || (activitySubType.length > 0) || (name.length() > 0) || (peopleName.length() > 0) || (peopleLic.length() > 0) || (!streetName1.equals("-1") && !streetName1.equals("")) || (owner1.length() > 0) || (owner2.length() > 0) || (!(crossStreetName1.equals("-1")) || (!(landmark.equals("-1"))))) {
				searchProject = false;
				searchAddress = false;
				searchSubProject = false;
				searchOwner = false;
				searchActivity = true;
				searchBusinessOwner = false;
				searchOutofTownAddress = false;
				logger.debug("searching activity");

			}

			// define the next page controls this control means that the end user searched for the address.
			if (searchAddress) {
				logger.debug("Searching for address...");
				nextPage = "successAddress";
				try {
					AddressAgent lsoAgent = new AddressAgent();
					if ((!(streetName.equalsIgnoreCase("")) || !(streetName.equalsIgnoreCase(null))) || (!(streetName2.equalsIgnoreCase("")) || !(streetName2.equalsIgnoreCase(null)))) {
						List addressResults = lsoAgent.searchAddresses(StringUtils.nullReplaceWithEmpty(streetNumber), StringUtils.nullReplaceWithEmpty(streetName), StringUtils.nullReplaceWithEmpty(apn), StringUtils.nullReplaceWithEmpty(streetName2));
						request.setAttribute("addressResults", addressResults);
					}
				} catch (Exception e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.search.fewActivities"));
					if (!errors.empty()) {
						saveToken(request);

						request.setAttribute("error", e);
						logger.debug("Set the error to attribute");

						saveErrors(request, errors);
						return (new ActionForward(mapping.getInput()));  
					}

				}
			} else if (searchOwner) {
				logger.debug("Searching for owner...");
				nextPage = "successOwner";

				AddressAgent lsoAgent = new AddressAgent();
				List ownerResults = lsoAgent.searchOwner(streetNumber, streetName, apn, ownerName);
				request.setAttribute("ownerResults", ownerResults);
			} else if (searchForeignAddress) {
				logger.debug("Searching for Foreign Address");
				logger.debug("owner name" + ownerName);
				nextPage = "successForeignAddress";

				AddressAgent lsoAgent = new AddressAgent();
				List foreignAddressResults = lsoAgent.searchForeignAddress(ownerName, foreignAddressLine1, foreignAddressLine2, foreignAddressLine3, foreignAddressLine4, foreignAddressState, foreignAddressCountry, foreignAddressZip);
				request.setAttribute("foreignAddressResults", foreignAddressResults);
			} else if (searchBusinessOwner) {
				logger.debug("Searching for business owner");
				logger.debug("business owner name" + businessOwnerName);
				nextPage = "successBusinessOwner";
				List businessOwnerResults = new ArrayList();
				ActivityAgent businessLicenseAgent = new ActivityAgent();
				businessOwnerResults = businessLicenseAgent.searchBusinessOwner(StringUtils.nullReplaceWithEmpty(businessAccountNumber), StringUtils.nullReplaceWithEmpty(businessName), StringUtils.nullReplaceWithEmpty(businessOwnerName));
				List businessOwnerResults1 = businessLicenseAgent.searchBusinessBTOwner(StringUtils.nullReplaceWithEmpty(businessAccountNumber), StringUtils.nullReplaceWithEmpty(businessName), StringUtils.nullReplaceWithEmpty(businessOwnerName));

				for (int i = 0; i < businessOwnerResults1.size(); i++) {
					businessOwnerResults.add(businessOwnerResults1.get(i));

				}

				if (businessOwnerResults.size() == 1) {
					BusinessOwner businessOwner = (BusinessOwner) businessOwnerResults.get(0);
					request.setAttribute("businessOwner1", businessOwner);
				}

				request.setAttribute("businessOwnerResults", businessOwnerResults);
			} else if (searchOutofTownAddress) {
				logger.debug("Searching for out of town address");
				nextPage = "successOutOfTownAddress";

				ActivityAgent businessLicenseSearchAgent = new ActivityAgent();
				List outOfTownAddressResults = businessLicenseSearchAgent.searchOutofTownAddress(outofTownAddress1, outofTownAddress2, outofTownAddress3, outofTownAddress4, outofTownAddressState, outofTownAddressCountry, outofTownAddressZip);
				List outOfTownAddressResults1 = businessLicenseSearchAgent.searchBTOutofTownAddress(outofTownAddress1, outofTownAddress2, outofTownAddress3, outofTownAddress4, outofTownAddressState, outofTownAddressCountry, outofTownAddressZip);
				for (int i = 0; i < outOfTownAddressResults1.size(); i++) {
					outOfTownAddressResults.add(outOfTownAddressResults1.get(i));
				}
				request.setAttribute("outOfTownAddressResults", outOfTownAddressResults);

			} else if (searchProject) {
				logger.debug("Searching for project...");
				nextPage = "successProject";

				ProjectAgent proj = new ProjectAgent();
				List projList = proj.searchProjects(projectNumber, projectName, name, peopleName, peopleLic, personType, personInfoType, streetNumber, streetName, apn);
				request.setAttribute("projList", projList);

				String onloadAction = "";
				session.setAttribute("onloadAction", onloadAction);
			} else if (searchSubProject) {
				logger.debug("Searching for sub-project...");
				nextPage = "successSubProject";

				ActivityAgent subProj = new ActivityAgent();
				List subProjList = null;

				try {
					subProjList = subProj.searchSubProjects(projectNumber, projectName, name, peopleName, peopleLic, personType, personInfoType, streetNumber, streetName, apn, subProjectNumber, projectType, subProjectType, subProjectSubType);
				} catch (Exception e) {
					logger.warn("Exception generated while searching for sub projects " + e.getMessage());
				}

				request.setAttribute("subProjList", subProjList);
			} else if (searchActivity) {
				logger.debug("Searching for activities");
				nextPage = "successActivity";

				ActivityAgent act = new ActivityAgent();
				List actList = null;

				try {

					int count = act.searchActivitiesCount(activityNumber, activityType, activitySubType, projectNumber, projectName, streetNumber, streetName, apn, name, peopleName, peopleLic, personType, personInfoType, projectType, subProjectType, subProjectSubType, streetName1, activityStatus, owner1, owner2, crossStreetName1, crossStreetName2, landmark);
					logger.debug(":: Count :: " + count);
					if (count > 2000) {
						errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.search.limtOfSearchResults"));
						logger.debug("error :: Your search returned a large number of results. Narrow your search and try again.");
						if (!errors.empty()) {
							saveErrors(request, errors);
							saveToken(request);
							return (new ActionForward(mapping.getInput()));
						}
					} else {
						actList = act.searchActivities(activityNumber, activityType, activitySubType, projectNumber, projectName, streetNumber, streetName, apn, name, peopleName, peopleLic, personType, personInfoType, projectType, subProjectType, subProjectSubType, streetName1, activityStatus, owner1, owner2, crossStreetName1, crossStreetName2, landmark);
						Activity activity = (Activity) actList.get(0);
						logger.debug("activity.getActivityDetail().getActivityNumber()" + activity.getActivityDetail().getActivityNumber());
						if (activity.getActivityDetail().getActivityNumber().startsWith("BT"))
							nextPage = "successActivityOwner";
					}
				} catch (Exception e) {
					errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.search.fewActivities"));
					logger.debug("do final search error");
					if (!errors.empty()) {
						saveErrors(request, errors);
						saveToken(request);
						request.setAttribute("error", e);
						logger.debug("Set the error to attribute");
						saveErrors(request, errors);
						return (new ActionForward(mapping.getInput()));
					}

				}

				request.setAttribute("actList", actList);
				request.setAttribute("activityListSize", "" + actList.size());
				basicSearchForm.setActivityList(actList);
				basicSearchForm.setActivityListSize("" + actList.size());

				if (actList.size() == 1) {
					Activity activity = (Activity) actList.get(0);
					logger.debug("actList.get(0)" + (Activity) actList.get(0));
					basicSearchForm.setActivityId(activity.getActivityId());
					basicSearchForm.setLsoId(activity.getActivityDetail().getLsoId());
					request.setAttribute("activityId", "" + activity.getActivityId());
					request.setAttribute("lsoId", "" + activity.getActivityDetail().getLsoId());
					request.setAttribute("actDetail", activity.getActivityDetail());
					logger.debug("Activity Id is " + activity.getActivityId());
					logger.debug("Lso ID is " + activity.getActivityDetail().getLsoId());
				}
			}

			request.setAttribute("basicSearchForm", basicSearchForm);
			logger.debug("forwarding to next page " + nextPage);
			session.setAttribute("nextPage", nextPage);
			if (nextPage.equalsIgnoreCase("search")) {
				request.setAttribute("result", "NoResult");
			}

			return (mapping.findForward(nextPage));
		} catch (Exception e) {
			logger.error("exception occured " + e.getMessage());
			throw new ServletException("Unknown exception occured " + e.getMessage());
		}

	}
}

package elms.control.actions.search;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.SearchAgent;
import elms.control.beans.DocumentSearchForm;

/**
 * This class searches the OBC database for documents based on the query strings and displays results
 * 
 * @author Anand Belaguly (anand@edgesoftinc.com - Jan 2, 2004)
 */
public class ViewDocumentSearchResultsAction extends Action {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(ViewDocumentSearchResultsAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		// the flow control that holds the pages.
		String nextPage = "";

		// make an instance of the document search form
		DocumentSearchForm documentSearchForm = (DocumentSearchForm) form;

		String contextRoot = request.getContextPath();

		try {
			SearchAgent searchAgent = new SearchAgent();
			List documentSearchResults = searchAgent.getDocumentSearchResults(documentSearchForm, contextRoot);
			logger.debug("obtained document search results of size " + documentSearchResults.size());
			request.setAttribute("documentSearchResults", documentSearchResults);
			request.setAttribute("numberOfResults", "" + documentSearchResults.size());
			request.setAttribute("documentUrl", "file:///c:/data/msg.txt");
			logger.debug("set the document search results to the attribute with size " + documentSearchResults.size());
			nextPage = "success";
		} catch (Exception e) {
			logger.error(e);
			request.setAttribute("error", e);
			logger.debug("Set the error to attribute");
			nextPage = "error";
		}

		logger.debug("forwarding to next page " + nextPage);

		return (mapping.findForward(nextPage));
	}
}

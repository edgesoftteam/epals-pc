package elms.control.actions.inspection;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.InspectionAgent;
import elms.common.Constants;
import elms.control.beans.ViewAllInspectionForm;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

public class SaveRoutingAction extends Action {

	static Logger logger = Logger.getLogger(SaveRoutingAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		int deptId = 0;
		User user = (User) session.getAttribute(Constants.USER_KEY);
		deptId = user.getDepartment().getDepartmentId();

		logger.info("Entering SaveRoutingAction");
		try {
			ViewAllInspectionForm frm = (ViewAllInspectionForm) session.getAttribute("viewAllInspectionForm");

			InspectionAgent inspectionAgent = new InspectionAgent();

			// cal the method to change routing sequence
			String inspectorId = inspectionAgent.changeRouting(frm);

			ViewAllInspectionForm viewAllInspectionForm = inspectionAgent.getAllInspections(inspectorId, frm.getInspectionDate(), deptId);

			session.setAttribute("viewAllInspectionForm", viewAllInspectionForm);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Inspection
				}
			} catch (Exception e) {
				e.getMessage();
			}
			logger.info("Exiting SaveRoutingAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}
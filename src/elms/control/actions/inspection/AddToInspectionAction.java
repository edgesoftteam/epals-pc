package elms.control.actions.inspection;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.InspectionAgent;
import elms.app.inspection.InspectionLibraryRecord;
import elms.control.beans.InspectionForm;
import elms.control.beans.SearchLibForm;

public class AddToInspectionAction extends Action {

	static Logger logger = Logger.getLogger(AddToInspectionAction.class.getName());
	String forward = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			logger.info("Entering AddToInspectionAction ");
			SearchLibForm searchForm = (SearchLibForm) session.getAttribute("searchLibForm");
			InspectionForm inspctForm = (InspectionForm) session.getAttribute("inspectionForm");
			InspectionLibraryRecord[] recList = searchForm.getLibRecordList();
			String comments = "";
			List inspectionItemCodeList = InspectionAgent.getInspectionItemCodes(inspctForm.getActivityType());
			request.setAttribute("inspectionItemCodeList", inspectionItemCodeList);
			logger.debug("Inspection Item List is set to request with size  " + inspectionItemCodeList.size());

			for (int i = 0; i < recList.length; i++) {
				InspectionLibraryRecord rec = (InspectionLibraryRecord) recList[i];
				String check = rec.getCheck();
				String comment = rec.getComment();
				if (check.equals("on"))
					comments = comments + rec.getComment() + "\n\n     	";
			}
			if (!comments.equals(""))
				inspctForm.setComments(inspctForm.getComments() + "\n\n   	" + comments);
			session.setAttribute("inspectionForm", inspctForm);
			logger.info("Exiting AddToInspectionAction. forward is: " + inspctForm.getAddOrEdit());

			if (inspctForm.getAddOrEdit().equals("add")) {
				forward = "addPage";
			} else {
				forward = "editPage";
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return (mapping.findForward(forward));
	}

}
package elms.control.actions.inspection;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.InspectionAgent;
import elms.common.Constants;
import elms.control.beans.ViewAllInspectionForm;
import elms.gsearch.GlobalSearch;
import elms.security.Group;
import elms.security.User;
import elms.util.StringUtils;

public class AssignInspectionAction extends Action {

	static Logger logger = Logger.getLogger(AssignInspectionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();
		logger.info("Entering AssignInspectionAction");
		int deptId = 0;

		try {
			ViewAllInspectionForm frm = (ViewAllInspectionForm) form;
			User user = (User) session.getAttribute(Constants.USER_KEY);
			deptId = user.getDepartment().getDepartmentId();

			InspectionAgent inspectionAgent = new InspectionAgent();
			inspectionAgent.assignInspections(frm);
			ViewAllInspectionForm viewAllInspectionForm = inspectionAgent.getAllInspections(null, frm.getInspectionDate(), deptId);

			List inspectorUsers = InspectionAgent.getInspectorUsers(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getDepartment().getDepartmentId());

			// Check if the user is a manager
			List groups = user.getGroups();
			Group group;
			String manager = "N";

			for (int i = 0; i < groups.size(); i++) {
				group = (Group) groups.get(i);
				logger.debug("Group Name : " + group.getDescription().toUpperCase() + ":");
				if ((group.getDescription() != null) && (group.getDescription().toUpperCase().equals("INSPECTION MANAGER"))) {
					manager = "Y";
					break;
				}
			}
			viewAllInspectionForm.setManager(manager);

			session.setAttribute("inspectorUsers", inspectorUsers);
			session.setAttribute("viewAllInspectionForm", viewAllInspectionForm);
			// request.setAttribute("viewAllInspectionForm", viewAllInspectionForm);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Inspection
				}
			} catch (Exception e) {
				e.getMessage();
			}
			logger.info("Exiting AssignInspectionAction");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}

}
package elms.control.actions.inspection;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.common.Constants;
import elms.control.beans.ViewAllInspectionForm;
import elms.security.Group;
import elms.security.User;
import elms.util.StringUtils;

public class ViewAllInspectionAction extends Action {
	static Logger logger = Logger.getLogger(ViewAllInspectionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();
		session.removeAttribute(Constants.APN);
		int deptId = 0;

		logger.info("Entering ViewAllInspectionAction");
		try {
			InspectionAgent inspectionAgent = new InspectionAgent();
			String date = StringUtils.cal2str(Calendar.getInstance());

			ViewAllInspectionForm frm = (ViewAllInspectionForm) form;

			if (frm == null) {
				logger.debug("frm is null");
			} else {
				if ((frm.getInspectionDate() != null) && !(frm.getInspectionDate()).equals("")) {
					date = frm.getInspectionDate();
				}
			}

			// Check if the user is a manager
			User user = (User) session.getAttribute(Constants.USER_KEY);
			List groups = user.getGroups();
			Group group;
			String manager = "N";
			deptId = user.getDepartment().getDepartmentId();

			for (int i = 0; i < groups.size(); i++) {
				group = (Group) groups.get(i);
				logger.debug("Group Name : " + group.getDescription().toUpperCase() + ":");
				if (group.getGroupId() == Constants.GROUPS_INSPECTION_MANAGER) {
					manager = "Y";
					break;
				}
			}

			logger.debug("user is " + user.getDepartment().getDepartmentId());
			logger.debug("date is " + date);

			ViewAllInspectionForm viewAllInspectionForm = inspectionAgent.getAllInspections(null, date, deptId);

			viewAllInspectionForm.setManager(manager);

			List inspectorUsers = InspectionAgent.getInspectorUsers(((elms.security.User) session.getAttribute(elms.common.Constants.USER_KEY)).getDepartment().getDepartmentId());

			session.setAttribute("inspectorUsers", inspectorUsers);
			session.setAttribute("viewAllInspectionForm", viewAllInspectionForm);
			logger.info("Exiting ViewAllInspectionAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

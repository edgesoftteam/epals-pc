package elms.control.actions.inspection;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.ActivityAgent;
import elms.agent.FinanceAgent;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.app.finance.ActivityFinance;
import elms.app.finance.FinanceSummary;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.control.actions.project.ViewActivityAction;
import elms.control.beans.InspectionForm;
import elms.control.beans.SearchLibForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class SaveNewInspectionAction extends Action {

	static Logger logger = Logger.getLogger(SaveNewInspectionAction.class.getName());
	protected Activity activity;
	protected ActivityFinance activityFinance;
	String outstandingFees = "";
	protected String fromEmail = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		String libOrSave = "";
		String fromOnline = "";

		try {

			ResourceBundle elmsProperties = Wrapper.getResourceBundle();
			fromEmail = elmsProperties.getString("EMAIL_FROM");

			ActionErrors errors = new ActionErrors();
			InspectionForm frm = (InspectionForm) session.getAttribute("inspectionForm");

			String activityId = frm.getActivityId();
			String activityType = frm.getActivityType();

			fromOnline = (String) request.getAttribute("fromOnline");

			if (fromOnline == null) {
				fromOnline = "";
			}

			logger.info("Entering SaveNewInspectionAction with activityId of " + activityId);
			logger.info("Entering SaveNewInspectionAction with fromOnline of " + fromOnline);
			libOrSave = frm.getLibOrSave();
			logger.info("libOrSave is: " + libOrSave);

			InspectionAgent inspectionAgent = new InspectionAgent();

			// get the comments and validate for the same.
			String comments = frm.getComments();
			int characterCount = comments.length();
			logger.debug("The character count of the comments string is " + characterCount);
			// if the character count is more than 7200, throw an error
			if (characterCount > 7200) {
				logger.debug("Comments more than permissible column length");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.inspection.commentsMoreThanAllowed"));
			}

			FinanceSummary financeSummary = new FinanceAgent().getActivityFinance(StringUtils.s2i(activityId));
			if (financeSummary == null)
				financeSummary = new FinanceSummary();
		
			String amountDue = financeSummary.getTotalAmountDue();
			logger.debug("amount due ### :" + amountDue);
			float fAmount = 0.0f;
			if(amountDue.startsWith("("))
				fAmount = 0;
			else
			fAmount = StringUtils.s2bd(amountDue).floatValue();
			logger.debug("amount due ### :" + fAmount);

			String actionCode = frm.getActionCode();
			if (fAmount <= 0)
				outstandingFees = "N";
			else
				outstandingFees = "Y";

			if (outstandingFees.equalsIgnoreCase("Y") & actionCode.equals("16")) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.inspection.finalOnOutstandingFees"));
				logger.debug("Can not final Inspection, while outstanding fees on activity");
			}
			if (!errors.empty()) {
				saveErrors(request, errors);
				List inspectionItemCodeList = InspectionAgent.getInspectionItemCodes(activityType);
				int inspectionItemCode = inspectionAgent.getInspectionItemCode(activityId); // to set in drop-down
				request.setAttribute("inspectionItemCodeList", inspectionItemCodeList);
				logger.debug("Inspection Item List is set to request with size  " + inspectionItemCodeList.size());

				request.setAttribute("inspectionForm", frm);
				session.setAttribute("inspectionForm", frm);
				request.setAttribute("activityId", activityId);

				return (new ActionForward(mapping.getInput()));
			}

			if (libOrSave.equals("save")) {
				inspectionAgent.saveNewInspection(frm, outstandingFees, "MAN");
				CachedRowSet inspectionRowSet = (CachedRowSet) inspectionAgent.getInspectionList(activityId);
				request.setAttribute("inspectionList", inspectionRowSet);
				request.setAttribute("activityId", activityId);
				ViewActivityAction viewActivityAction = new ViewActivityAction();
				viewActivityAction.getActivity(StringUtils.s2i(activityId), request);
			} else {
				// String strCategoryId = request.getParameter("strCategoryId");
				String strCategoryId = "";
				if (activityType.equals("BLDG"))
					strCategoryId = "5";
				else if (activityType.equals("ELEC"))
					strCategoryId = "3";
				else if (activityType.equals("MECH"))
					strCategoryId = "2";
				else if (activityType.equals("PLUM"))
					strCategoryId = "4";
				logger.debug("category Id is " + strCategoryId);
				SearchLibForm searchLibForm = new SearchLibForm();
				List inspectionSubCategoryList = new ArrayList();
				if (strCategoryId == null) {
					searchLibForm.setCategory("-1");
				} else {
					searchLibForm.setCategory(strCategoryId);
					inspectionSubCategoryList = InspectionAgent.getInspectionSubCategories(StringUtils.s2i(strCategoryId));
					logger.debug("sub Category List size  is " + inspectionSubCategoryList.size());
				}
				request.setAttribute("inspectionSubCategoryList", inspectionSubCategoryList);
				session.setAttribute("searchLibForm", searchLibForm);
			}
			logger.info("Exiting SaveNewInspectionAction");

			if (fromOnline.equalsIgnoreCase("fromOnline")) {
				String inspDt = (String) request.getAttribute("inspectionDate");
				frm.setInspectionDate(StringUtils.cal2str(StringUtils.dbDate2cal(inspDt)));
				logger.debug("Date is " + frm.getInspectionDate());
				inspectionAgent.saveNewInspection(frm, outstandingFees, "ONL");
				logger.debug(" History is" + frm.getActionCode());
				logger.debug("Farwording to request diaptcher");

				StringBuffer emailTo = new StringBuffer();
				StringBuffer nullEmail = new StringBuffer();
				boolean nullable = false;

				List emailList = new ActivityAgent().getEmailBlastforActivityStatusChange(StringUtils.s2i(activityId));

				if (!emailList.isEmpty()) {

					for (int i = 0; i < emailList.size(); i++) {

						People people = (People) emailList.get(i);

						// Check if email is null means send mail to webmaster saying people does not have email address
						// else send email to the concertned person

						if (people.getEmailAddress().equalsIgnoreCase(fromEmail)) {

							nullEmail.append("People ID: " + people.getPeopleId() + " \n Name: " + people.getName() + "  \n  Address:  " + people.getAddress() + "\n\n");

							nullable = true;
						} else {
							emailTo.append(people.getEmailAddress() + ";");
						}
					}
				}

				// date type status permit
				// Sending Email

				StringUtils.sendEmail(emailTo + "", "request", LookupAgent.getActivityType(new InspectionAgent().getActivityId(new ActivityAgent().getActivtiyNumber(StringUtils.s2i(frm.getActivityId())))[1]).getDescription(), LookupAgent.getActivityAddress(StringUtils.s2i(frm.getActivityId()))[0], frm.getInspectionDate(), frm.getInspectionItemDesc(), "Request For Inspection", new ActivityAgent().getActivtiyNumber(StringUtils.s2i(frm.getActivityId())), new InspectionAgent().getInspectionCommentsForRequest(frm.getActivityId()));

				request.setAttribute("message", "Inspection request is sucessfully submitted.");

				request.setAttribute("DatefromReq", frm.getInspectionDate());

				// request.setAttribute("codefromReq",
				// frm.getInspectionDate());
				//                
				RequestDispatcher rd = this.getServlet().getServletContext().getRequestDispatcher("/inspectionResults.do?action=view");

				// RequestDispatcher rd = this.getServlet().getServletContext()
				// .getRequestDispatcher("/inspectionRequest.do?action=req");
				rd.forward(request, response);

				return null;
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		if (libOrSave.equals("save")) {
			return (mapping.findForward("save"));
		} else {
			return (mapping.findForward("lib"));
		}
	}

}
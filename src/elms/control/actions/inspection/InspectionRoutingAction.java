package elms.control.actions.inspection;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.common.Constants;
import elms.control.beans.ViewAllInspectionForm;
import elms.security.User;
import elms.util.StringUtils;

public class InspectionRoutingAction extends Action {
	/**
	 * The logger.
	 */
	static Logger logger = Logger.getLogger(InspectionRoutingAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering InspectionRoutingAction");

		HttpSession session = request.getSession();
		String inspectorId = (request.getParameter("inspectorId") != null) ? request.getParameter("inspectorId") : "";
		logger.debug("The inspector id obtained is " + inspectorId);

		ViewAllInspectionForm frm = (ViewAllInspectionForm) form;
		int deptId = 0;
		User user = (User) session.getAttribute(Constants.USER_KEY);
		deptId = user.getDepartment().getDepartmentId();

		if (frm == null) {
			throw new ServletException("The action form is null");
		}

		try {
			InspectionAgent inspectionAgent = new InspectionAgent();

			String date = StringUtils.cal2str(Calendar.getInstance());
			logger.debug("Today's date is " + date);

			if ((frm.getInspectionDate() != null) && !(frm.getInspectionDate()).equals("")) {
				date = frm.getInspectionDate();
			}

			logger.debug("The inspection date obtained from the form is " + date);

			// get the all inspectons for the inspector id.
			ViewAllInspectionForm viewAllInspectionForm = inspectionAgent.getAllInspections(inspectorId, date, deptId);
			viewAllInspectionForm.setRoutingId(inspectorId);

			session.setAttribute("viewAllInspectionForm", viewAllInspectionForm);

			logger.info("Exiting InspectionRoutingAction");

			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException(e.getMessage());
		}
	}
}

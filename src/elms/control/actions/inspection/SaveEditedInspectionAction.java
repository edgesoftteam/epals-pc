package elms.control.actions.inspection;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import elms.agent.FinanceAgent;
import elms.agent.InspectionAgent;
import elms.app.finance.ActivityFinance;
import elms.app.finance.FinanceSummary;
import elms.app.project.Activity;
import elms.control.actions.project.ViewActivityAction;
import elms.control.beans.InspectionForm;
import elms.gsearch.GlobalSearch;
import elms.util.StringUtils;

public class SaveEditedInspectionAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(SaveEditedInspectionAction.class.getName());
	protected Activity activity;
	protected ActivityFinance activityFinance;
	String outstandingFees = "";

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		HttpSession session = request.getSession();

		logger.debug("Entered SaveEditedInspectionAction");
		try {
			ActionErrors errors = new ActionErrors();
			InspectionForm frm = (InspectionForm) form;
			String inspectId = frm.getInspectionId();
			String activityId = frm.getActivityId();

			FinanceSummary financeSummary = new FinanceAgent().getActivityFinance(StringUtils.s2i(activityId));

			if (financeSummary == null) {
				financeSummary = new FinanceSummary();
			}

			String amountDue = financeSummary.getTotalAmountDue();
			logger.debug("amount due ### :" + amountDue);
			float fAmount = 0.0f;
			if(amountDue.startsWith("("))
				fAmount = 0;
			else
			fAmount = StringUtils.s2bd(amountDue).floatValue();
			logger.debug("amount due ### :" + fAmount);

			// get the comments and validate for the same.
			String comments = frm.getComments();
			int characterCount = comments.length();
			logger.debug("The character count of the comments string is " + characterCount);

			// if the character count is more than 7200, throw an error
			if (characterCount > 7200) {
				logger.debug("Comments more than permissible column length");
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.inspection.commentsMoreThanAllowed"));
			}

			String actionCode = frm.getActionCode();

			if (fAmount <= 0) {
				outstandingFees = "N";
			} else {
				outstandingFees = "Y";
			}

			if (outstandingFees.equalsIgnoreCase("Y") & actionCode.equals("16")) {
				errors.add(ActionErrors.GLOBAL_ERROR, new ActionError("error.inspection.finalOnOutstandingFees"));
				logger.debug("Can not final Inspection, while outstanding fees on activity");
			}

			if (!errors.empty()) {
				saveErrors(request, errors);
				request.setAttribute("inspectionForm", frm);
				session.setAttribute("inspectionForm", frm);

				return (new ActionForward(mapping.getInput()));
			}

			logger.info("Entering SaveEditedInspectionAction with inspectId of " + inspectId);

			InspectionAgent inspectionAgent = new InspectionAgent();

			inspectionAgent.saveEditedInspection(frm, outstandingFees);

			request.setAttribute("activityId", activityId);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Inspection
				}
			} catch (Exception e) {
				e.getMessage();
			}
			ViewActivityAction viewActivityAction = new ViewActivityAction();
			viewActivityAction.getActivity(StringUtils.s2i(activityId), request);
			logger.info("Exiting SaveEditedInspectionAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

package elms.control.actions.inspection;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.InspectionAgent;
import elms.common.Constants;
import elms.control.beans.ViewAllInspectionForm;
import elms.exception.DuplicateInspectionRouteException;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;

/**
 * The inspecton routing refresh action.
 * 
 * @author Pradeep Singh
 * @author Anand Belaguly updated Jul 24, 2004 11:42:58 AM
 */
public class RefreshRoutingAction extends Action {
	/**
	 * The logger.
	 */
	static Logger logger = Logger.getLogger(RefreshRoutingAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		logger.info("Entering RefreshRoutingAction");

		// the session.
		HttpSession session = request.getSession();
		String inspectorId = null;
		int deptId = 0;
		User user = (User) session.getAttribute(Constants.USER_KEY);
		deptId = user.getDepartment().getDepartmentId();

		// the form that was submitted.
		ViewAllInspectionForm frm = (ViewAllInspectionForm) form;
		if (frm == null) {
			throw new ServletException("The action form is null");
		}

		try {
			String date = StringUtils.cal2str(Calendar.getInstance());
			logger.debug("Today's date is " + date);

			if ((frm.getInspectionDate() != null) && !(frm.getInspectionDate()).equals("")) {
				date = frm.getInspectionDate();
			}

			logger.debug("the date obtained from the form is " + date);

			InspectionAgent inspectionAgent = new InspectionAgent();

			ViewAllInspectionForm viewAllInspectionForm;

			if (frm.getChanged().equals("yes")) {
				inspectorId = inspectionAgent.changeRouting(frm);
				frm.setChanged("no");

				viewAllInspectionForm = inspectionAgent.getAllInspections(inspectorId, date, deptId);
			} else {
				if ((frm.getRoutingId() != null) && !(frm.getRoutingId()).equals("")) {
					inspectorId = frm.getRoutingId();
				}

				viewAllInspectionForm = inspectionAgent.getAllInspections(inspectorId, date, deptId);
			}

			// set the form to the session.
			session.setAttribute("viewAllInspectionForm", viewAllInspectionForm);
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for Inspection
				}
			} catch (Exception e) {
				e.getMessage();
			}
			logger.info("Exiting RefreshRoutingAction");

			return (mapping.findForward("success"));
		} catch (DuplicateInspectionRouteException e) {
			logger.debug(e.getMessage());
			request.setAttribute("error", "Duplicate/Empty route entered, Please correct it and save again");
			session.setAttribute("viewAllInspectionForm", frm);
			return (mapping.findForward("success"));
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ServletException(e.getMessage());
		}
	}
}

/*
 * Created on Jul 14, 2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.control.actions.inspection;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import elms.agent.AdminAgent;
import elms.agent.InspectionAgent;
import elms.common.Constants;
import elms.security.Group;
import elms.security.User;
import elms.util.Util;

/**
 * @author gaurav
 *
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style - Code Templates
 */
public class InspectionValidationAction extends Action {
	static Logger logger = Logger.getLogger(InspectionValidationAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if (Util.reloginRequired(request)) {
			return (mapping.findForward("logon"));
		}

		logger.debug(" in InspectionValidationAction");
		PrintWriter pw = response.getWriter();

		User user = (User) request.getSession().getAttribute(Constants.USER_KEY);
		if (user.getDepartment().getDepartmentId() == Constants.DEPARTMENT_BUILDING_SAFETY) {
			pw.write("");
			return null;
		}

		try {
			if (request.getParameter("validationType") != null) {
				if (request.getParameter("validationType").equalsIgnoreCase("inspectionType")) {
					pw.write(addDaysForInspectionType(request.getParameter("inspctItemId")));
				}
				if (request.getParameter("validationType").equalsIgnoreCase("dateValidation")) {
					String inspectorId = request.getParameter("inspectorId");
					String date = request.getParameter("date");
					String timeUnit = request.getParameter("timeUnit");
					String inspectionId = "";
					if (request.getParameter("inspectionId") != null) {
						inspectionId = request.getParameter("inspectionId");
					}
					pw.write(checkInspectorAvailabeDate(inspectorId, date, timeUnit, inspectionId));
				}
				if (request.getParameter("validationType").equalsIgnoreCase("actionCode")) {

				} else if (request.getParameter("validationType").equalsIgnoreCase("canHaveOpenViolations")) {
					InspectionAgent agent = new InspectionAgent();
					String actionCodeId = request.getParameter("actnCode");
					boolean canHaveOpenViolation = agent.canHaveOpenViolation(actionCodeId);

					logger.debug("canHaveOpenViolation::" + canHaveOpenViolation);
					if (canHaveOpenViolation) {
						pw.write("Y");
					} else {
						pw.write("N");
					}
				}

				if (request.getParameter("validationType").equalsIgnoreCase("getTimeUnits")) {
					String inspectorId = request.getParameter("inspectorId");
					logger.debug("inspectorId::" + inspectorId);
					if (inspectorId != null && !inspectorId.equalsIgnoreCase("")) {

						int userId = Integer.parseInt(inspectorId);

						User inspector = new AdminAgent().getUser(userId);
						String timeUnit = "";
						List userGroups = inspector.groups;

						for (int i = 0; i < userGroups.size(); i++) {
							Group group = (Group) userGroups.get(i);
							if (group.getGroupId() == Constants.GROUPS_FIELD_REPRESENTATIVE) {
								timeUnit = "" + Constants.INSPECTION_TIME_FOR_FIELD_REPRESENTATIVE;
								break;
							} else if (group.getGroupId() == Constants.GROUPS_INSPECTOR) {
								timeUnit = "" + Constants.INSPECTION_TIME_FOR_INSPECTOR;
							}
						}
						pw.write(timeUnit);
					}

				}
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new ServletException("", e);
		}
		return null;
	}

	private String addDaysForInspectionType(String inspctItemId) throws Exception {
		InspectionAgent agent = new InspectionAgent();
		int days = 0;
		String day = agent.getdayAdd(inspctItemId);
		if (day != null) {
			days = Integer.parseInt(day);
		} else {
			return "";
		}
		String dateString = addDaysToCurrentDate(days);
		return dateString;
	}

	private String checkInspectorAvailabeDate(String inspectorId, String date, String timeUnit, String inspectionId) throws Exception {
		InspectionAgent agent = new InspectionAgent();
		AdminAgent userAgent = new AdminAgent();
		int inspId = 0;
		if (inspectorId != null) {
			inspId = Integer.parseInt(inspectorId);
		}

		User user = userAgent.getUser(inspId);
		List userGroups = user.groups;
		int maxInspectionCount = 0;
		for (int i = 0; i < userGroups.size(); i++) {
			Group group = (Group) userGroups.get(i);
			if (group.getGroupId() == Constants.GROUPS_FIELD_REPRESENTATIVE) {
				maxInspectionCount = Constants.MAX_INSPECTION_FOR_FIELD_REPRESENTATIVE;
			} else if (group.getGroupId() == Constants.GROUPS_INSPECTOR) {
				maxInspectionCount = Constants.MAX_INSPECTION_FOR_INSPECTOR;
				break;
			}
		}

		int timeUnitFromUI = Integer.parseInt(timeUnit);

		String dateStr = agent.isInspectorAvailable(inspectorId, date, timeUnitFromUI, inspectionId, maxInspectionCount);
		return dateStr;
	}

	public String addDaysToCurrentDate(int days) {
		Calendar cl = Calendar.getInstance();
		Date date = null;
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");

		if (days != 0 && days % 365 == 0) {
			int year = days / 365;
			cl.add(Calendar.YEAR, year);
		} else {
			cl.add(Calendar.DATE, days);
		}
		List holidays = null;
		while (true) {
			int day = cl.get(Calendar.DAY_OF_WEEK);
			if (day == Calendar.SUNDAY) {
				cl.add(Calendar.DATE, 1);
			} else if (day == Calendar.SATURDAY) {
				cl.add(Calendar.DATE, 2);
			}

			date = new Date();
			date.setTime(cl.getTimeInMillis());
			if (holidays != null && holidays.contains(sdf.format(date))) {
				cl.add(Calendar.DATE, 1);
			} else {
				break;
			}
		}
		return sdf.format(date);
	}

}

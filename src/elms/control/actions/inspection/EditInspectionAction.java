package elms.control.actions.inspection;

import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.InspectionAgent;
import elms.agent.LookupAgent;
import elms.control.beans.InspectionForm;
import elms.util.StringUtils;

public class EditInspectionAction extends Action {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(EditInspectionAction.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			// get the inspector id from the request
			String inspectId = request.getParameter("inspectId");
			logger.info("Entering EditInspectionAction with inspectId of " + inspectId);

			// create an instance of inspection agent
			InspectionAgent inspectionAgent = new InspectionAgent();

			CachedRowSet inspectionRowSet = (CachedRowSet) inspectionAgent.getInspectionData(inspectId);

			if (inspectionRowSet != null) {
				inspectionRowSet.beforeFirst();
			} else {
				logger.debug("inspectionRowSet is null");
			}

			request.setAttribute("inspectionRowSet", inspectionRowSet);

			InspectionForm inspectionForm = new InspectionForm();
			inspectionForm.setAddOrEdit("edit");

			String inspector_id = "";
			String inspDate = "";
			Calendar c;

			if (inspectionRowSet.next()) {
				inspectionForm.setActionCode(inspectionRowSet.getString("ACTN_CODE"));
				inspectionForm.setInspectionItemDesc(inspectionRowSet.getString("DESCRIPTION"));
				inspectionForm.setComments(inspectionRowSet.getString("COMMENTS"));
				inspDate = StringUtils.cal2str(StringUtils.dbDate2cal(inspectionRowSet.getString("INSPECTION_DT")));
				inspectionForm.setInspectionDate(inspDate);
				inspectionForm.setInspectionItem(inspectionRowSet.getString("INSPCT_ITEM_ID"));
				inspectionForm.setInspectorId(inspectionRowSet.getString("INSPECTOR_ID"));
				inspectionForm.setActivityId(inspectionRowSet.getString("ACT_ID"));
				inspectionForm.setActivityType(LookupAgent.getActivityTypeForActId(inspectionRowSet.getString("ACT_ID")));
				inspectionForm.setInspectionId(inspectionRowSet.getString("INSPECTION_ID"));

				String inspectorName = inspectionAgent.getInspectorUser(inspectionRowSet.getString("INSPECTOR_ID"));
				inspectionForm.setInspectorName(inspectorName);
			}

			session.setAttribute("inspectionForm", inspectionForm);

			logger.info("Exiting EditInspectionAction");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

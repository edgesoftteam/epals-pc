package elms.control.actions.inspection;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.MessageResources;

import sun.jdbc.rowset.CachedRowSet;
import elms.agent.InspectionAgent;
import elms.app.inspection.InspectionList;
import elms.util.StringUtils;

public class ListInspection extends Action {
	static Logger logger = Logger.getLogger(ListInspection.class.getName());

	public ActionForward perform(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		MessageResources messages = getResources();
		HttpSession session = request.getSession();

		try {
			String activityId = request.getParameter("activityId");
			String activityType = request.getParameter("type");
			logger.info("Entering ListInspection with activityId of " + activityId);

			InspectionAgent inspectionAgent = new InspectionAgent();

			if (mapping.getAttribute() != null) {
				if ("request".equals(mapping.getScope())) {
					request.removeAttribute(mapping.getAttribute());
				} else {
					session.removeAttribute(mapping.getAttribute());
				}
			}

			CachedRowSet rsInspct = inspectionAgent.getInspectionList(activityId);

			ArrayList inspectionList = new ArrayList();

			while (rsInspct.next()) {
				InspectionList il = new InspectionList();
				il.setInspectionDate(StringUtils.date2str(rsInspct.getDate("INSPECTION_DT")));
				il.setInspectionId(rsInspct.getString("ID"));
				il.setInspectionItem(rsInspct.getString("ITEM"));
				il.setActionCode(rsInspct.getString("ACTN_CODE"));
				il.setRequestSource(rsInspct.getString("REQUEST_SOURCE"));
				inspectionList.add(il);
			}

			request.setAttribute("inspectionList", inspectionList);
			request.setAttribute("activityId", activityId);
			request.setAttribute("activityType", activityType);
			logger.info("Exiting ListInspection");
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return (mapping.findForward("success"));
	}
}

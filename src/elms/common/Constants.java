package elms.common;

public final class Constants {
	// General
	public static final String Package = "elms";
	public static final String USER_KEY = "user";
	public static final String USER_TYPE = "userType";
	public static final String USER_UNASSIGNED = "unassigned";
	public static final String USER_ED_KHOURDADJIAN = "ekhourdadjian";
	public static final String USER_EULA_EWARREN = "ewarren";

	public static String NEXTPAGE = "success";
	public static String ERRORPAGE = "error";

	// People profile codes
	public static final String ACTOR = "O";
	public static final String OWNER = "W";
	public static final String APPLICANT = "A";
	public static final String ARCHITECT = "R";
	public static final String CONTRACTOR = "C";
	public static final String DESIGNER = "D";
	public static final String ENGINEER = "E";
	public static final String OWNERBUILDER = "B";
	public static final String AGENT = "G";
	public static final String TENANT = "T";

	// People Types
	public static final String SACTOR = "Other";
	public static final String SOWNER = "Owner";
	public static final String SAPPLICANT = "Applicant";
	public static final String SARCHITECT = "Architect";
	public static final String SCONTRACTOR = "Contractor";
	public static final String SDESIGNER = "Designer";
	public static final String SENGINEER = "Engineer";
	public static final String SOWNERBUILDER = "Owner/Builder";
	public static final String SAGENT = "Agent";
	public static final String STENANT = "Tenant";

	// Level Types
	public static final String LAND = "L";
	public static final String STRUCTURE = "S";
	public static final String OCCUPANCY = "O";
	public static final String ACTIVITY_TYPE = "T";
	public static final String PROJECT = "P";
	public static final String SUB_PROJECT = "Q";
	public static final String ACTIVITY = "A";
	public static final String INSPECTION = "I";
	public static final String APN = "APN";
	public static final String LSO_ID = "lsoId";
	public static final String LSO_TYPE = "lsoType";
	public static final String PEOPLE_ID = "-1";
	public static final String PSA_TYPE = "type";
	public static final String PSA_ID = "id";
	public static final String ADDR_ID = "addr_id";
	public static final String ADD = "Add";
	public static final String EDIT = "Edit";

	// CRCT Module specifics
	public static final String PWCALL = "PWCALL";
	public static final String PWFAC = "PWFAC";
	public static final String PWELEC = "PWELEC";
	public static final String PWINFO = "PWINFO";
	public static final String PWROLL = "PWROLL";
	public static final String PWSAN = "PWSAN";
	public static final String PWSEW = "PWSEW";
	public static final String PWSTR = "PWSTR";
	public static final String PWVEH = "PWVEH";
	public static final String PWWAT = "PWWAT";
	public static final String PWTSSL = "PWTSSL";
	public static final String CALL_FORM = "callForm";
	public static final String WATER_FORM = "waterForm";
	public static final String SANITATION_FORM = "sanitationForm";
	public static final String STREETS_FORM = "streetsForm";
	public static final String TRAFFICSIGNALS_FORM = "trafficSignalsForm";
	public static final String FACILITIES_FORM = "facilitiesForm";
	public static final String ELECTRICAL_FORM = "electricalForm";
	public static final String PWINFORMATION_FORM = "pwInformationForm";
	public static final String SEWER_FORM = "sewerForm";

	// Planning specific
	public static String statusList = "statusList";
	public static String priorityList = "priorityList";
	public static String subTypeList = "subTypeList";
	public static String activitySubTypeList = "activitySubTypeList";
	public static String locationList = "locationList";
	public static String divisionList = "divisionList";
	public static String repairClassList = "repairClassList";
	public static String responsibilityList = "responsibilityList";
	public static String streetNameList = "streetNameList";
	public static String streetFractionList = "streetFractionList";
	public static String lineTypeList = "lineTypeList";
	public static String lineSizeList = "lineSizeList";
	public static String maintenanceCycleList = "maintenanceCycleList";
	public static String causeOfRestrictionList = "causeOfRestrictionList";
	public static String trafficSignalsLocationList = "trafficSignalsLocationList";
	public static String LookupValues = "LookupValues";

	// Departments
	public static final int DEPARTMENT_POLICY_AND_MANAGEMENT = 1;
	public static final int DEPARTMENT_FIRE = 2;
	public static final int DEPARTMENT_CITY_CLERK = 3;
	public static final int DEPARTMENT_FINANCE_ADMINISTRATION = 4;
	public static final int DEPARTMENT_COMMUNICATIONS_MARKETING = 6;
	public static final int DEPARTMENT_DEPARTMENT_BUILDING_SAFETY = 8;
	public static final int DEPARTMENT_TRANSPORTATION_SERVICES = 9;
	public static final int DEPARTMENT_GENERAL_SERVICES = 10;
	public static final int DEPARTMENT_PUBLIC_WORKS = 11;
	public static final int DEPARTMENT_RECREATION_PARKS = 12;
	public static final int DEPARTMENT_LIBRARY_AND_COMMUNITY_SERVICES = 13;
	public static final int DEPARTMENT_INFORMATION_TECHNOLOGY = 14;
	public static final int DEPARTMENT_HUMAN_SERVICES = 15;
	public static final int DEPARTMENT_POLICE_DEPARTMENT = 16;
	public static final int DEPARTMENT_PLANNING_AND_COMMUNITY_DEVELOPMENT = 17;
	public static final int DEPARTMENT_POLICE_DEPARTMENT_DCS = 18;
	public static final int DEPARTMENT_TRANSPORTATION_SERVICES2 = 19;
	public static final int DEPARTMENT_LICENSE_AND_CODE_SERVICES = 22;
	public static final int DEPARTMENT_PARKING = 24;
	public static final int DEPARTMENT_BUILDING_SAFETY = 8;
	public static final int DEPARTMENT_NEIGHBOURHOOD_SERVICES = 12;
	public static final int DEPARTMENT_HOUSING = 25;
	

	// Department Codes
	public static final String DEPARTMENT_BUILDING_SAFETY_CODE = "BS";
	public static final String DEPARTMENT_CODE_ENFORCEMENT_CODE = "CE";
	public static final String DEPARTMENT_PUBLIC_WORKS_CODE ="PW";
	public static final String DEPARTMENT_PLANNING_CODE = "PL";
	public static final String DEPARTMENT_PARKING_CODE = "PK";
	public static final String DEPARTMENT_LICENSE_CODE_SERVICES_CODE = "LC";
	public static final String DEPARTMENT_HOUSING_CODE = "HD";

	// Project Names
	public static final String PROJECT_NAME_BUILDING = "Building";
	public static final int PROJECT_NAME_BUILDING_ID = 1;
	public static final String PROJECT_NAME_PLANNING = "Planning";
	public static final int PROJECT_NAME_PLANNING_ID = 2;
	public static final String PROJECT_NAME_LICENSE_AND_CODE = "Code Enforcement";
	public static final int PROJECT_NAME_LICENSE_AND_CODE_ID = 3;
	public static final String PROJECT_NAME_LICENSE_AND_CODE_SERVICES = "License and Code Services";
	public static final int PROJECT_NAME_LICENSE_AND_CODE_SERVICES_ID = 4;
	public static final String SUB_PROJECT_NAME_REGULATORY_PERMIT = "Regulatory Permit";
	public static final String SUB_PROJECT_NAME_STARTS_WITH = "BL - ";
	public static final String BT_SUB_PROJECT_NAME_STARTS_WITH = "BT - ";
	public static final String PROJECT_NAME_PUBLIC_WORKS = "Public Works";
	public static final int PROJECT_NAME_PUBLIC_WORKS_ID = 5;
	public static final String PROJECT_NAME_PARKING = "Parking";
	public static final int PROJECT_NAME_PARKING_ID = 6;
	public static final String PROJECT_NAME_HOUSING = "Housing";
	public static final int PROJECT_NAME_HOUSING_ID = 7;

	// Activity Statuses
	public static final int ACTIVITY_STATUS_UNKNOWN_INVALID = -1;
	public static final int ACTIVITY_SUB_TYPE = -1;
	public static final int ACTIVITY_STATUS_MODULE_ID = 4;
	public static final int APPLICATION_TYPE_BUSINESSLICENSE = 101;
	public static final int APPLICATION_TYPE_BUSINESSTAX = 102;

	// Activity Statuses for code enforcement
	public static final int ACTIVITY_STATUS_CE_ACTIVE = 1;
	public static final int ACTIVITY_STATUS_CE_INACTIVE = 2;
	public static final int ACTIVITY_STATUS_CE_SUSPENSE = 3;

	// Activity Statuses for building module
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_FINAL = 4;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_ON_HOLD = 5;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_ISSUED = 6;
	public static final int ACTIVITY_STATUS_BUILDING_PC_SUBMITTED = 7;
	public static final int ACTIVITY_STATUS_BUILDING_PC_ASSIGNED = 8;
	public static final int ACTIVITY_STATUS_BUILDING_PC_WITH_CORRECTIONS = 10;
	public static final int ACTIVITY_STATUS_BUILDING_PC_PICKED_UP = 12;
	public static final int ACTIVITY_STATUS_BUILDING_PC_RESUBMITTED = 13;
	public static final int ACTIVITY_STATUS_BUILDING_PC_APPROVED = 15;
	public static final int ACTIVITY_STATUS_BUILDING_PC_APPROVED_WITH_SIGNOFFS = 16;
	public static final int ACTIVITY_STATUS_BUILDING_PC_EXPIRED = 21;
	public static final int ACTIVITY_STATUS_BUILDING_PC_EXTENDED = 22;
	public static final int ACTIVITY_STATUS_BUILDING_PC_ON_HOLD = 26;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_READY = 28;
	public static final int ACTIVITY_STATUS_BUILDING_TCO = 32;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_REISSUED = 33;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_EXTENDED = 34;
	public static final int ACTIVITY_STATUS_BUILDING_PC_CANCELLED = 35;
	public static final int ACTIVITY_STATUS_BUILDING_ADMIN_COMPLETED = 36;
	public static final int ACTIVITY_STATUS_BUILDING_ADMIN_PENDING = 37;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_CANCELLED = 38;
	public static final int ACTIVITY_STATUS_BUILDING_CERTIFICATE_OF_OCCUPANCY = 39;
	public static final int ACTIVITY_STATUS_BUILDING_PERMIT_EXPIRED = 40;

	// Activity Statuses for planning module
	public static final int ACTIVITY_STATUS_PLANNING_SUBMITTED = 1000;
	public static final int ACTIVITY_STATUS_PLANNING_INCOMPLETE = 1001;
	public static final int ACTIVITY_STATUS_PLANNING_INPROCESS = 1002;
	public static final int ACTIVITY_STATUS_PLANNING_ONHOLD = 1003;
	public static final int ACTIVITY_STATUS_PLANNING_APPROVED = 1004;
	public static final int ACTIVITY_STATUS_PLANNING_DENIED = 1005;
	public static final int ACTIVITY_STATUS_PLANNING_DENIED_WITHOUT_PREJUDICE = 1006;
	public static final int ACTIVITY_STATUS_PLANNING_APPEALED = 1007;
	public static final int ACTIVITY_STATUS_PLANNING_WITHDRAWN = 1008;
	public static final int ACTIVITY_STATUS_PLANNING_EXPIRED = 1009;

	// Activity Statuses for license and code module
	public static final int ACTIVITY_STATUS_LC_PERMIT_EXPIRED = 1010;
	public static final int ACTIVITY_STATUS_LC_ADMIN_PENDING = 1011;
	public static final int ACTIVITY_STATUS_LC_ADMIN_COMPLETED = 1012;
	public static final int ACTIVITY_STATUS_LC_PERMIT_CANCELLED = 1013;
	public static final int ACTIVITY_STATUS_LC_PERMIT_EXTENDED = 1014;
	public static final int ACTIVITY_STATUS_LC_PERMIT_FINAL = 1015;
	public static final int ACTIVITY_STATUS_LC_PERMIT_ISSUED = 1016;
	public static final int ACTIVITY_STATUS_LC_PERMIT_PENDING = 1017;
	public static final int ACTIVITY_STATUS_LC_PERMIT_DENIED = 1018;

	// Activity Statuses for business license module
	public static final int ACTIVITY_STATUS_BL_PENDING = 1050;
	public static final int ACTIVITY_STATUS_BL_PENDING_RENEWAL = 1051;
	public static final int ACTIVITY_STATUS_BL_DISAPPROVED = 1052;
	public static final int ACTIVITY_STATUS_BL_ISSUED = 1053;
	public static final int ACTIVITY_STATUS_BL_OUT_OF_BUSINESS = 1054;
	public static final int ACTIVITY_STATUS_BL_SEE_COMMENTS = 1055;
	public static final int ACTIVITY_STATUS_BL_CONDITIONAL = 1056;
	public static final int ACTIVITY_STATUS_BL_APPEALED = 1057;
	public static final int ACTIVITY_STATUS_BL_DENIED = 1058;
	public static final int ACTIVITY_STATUS_BL_PROBATIONARY = 1059;
	public static final int ACTIVITY_STATUS_BL_SUSPENSE = 1060;
	public static final int ACTIVITY_STATUS_BL_REVOKED = 1061;
	public static final int ACTIVITY_STATUS_BL_RETURNED_CHECK = 1062;
	public static final int ACTIVITY_STATUS_BL_DELINQUENT = 1063;
	public static final int ACTIVITY_STATUS_BL_FISCAL_YEAR_EXPIRED = 1064;

	// Activity Statuses for business Tax module
	public static final int ACTIVITY_STATUS_BT_DELINQUENT = 1065;
	public static final int ACTIVITY_STATUS_BT_DISAPPROVED = 1066;
	public static final int ACTIVITY_STATUS_BT_OUT_OF_BUSINESS = 1067;
	public static final int ACTIVITY_STATUS_BT_PAID_OR_CURRENT = 1068;
	public static final int ACTIVITY_STATUS_BT_PENDING_APPROVAL = 1069;
	public static final int ACTIVITY_STATUS_BT_PENDING_RENEWAL = 1070;
	public static final int ACTIVITY_STATUS_BT_RETURNED_CHECK = 1071;
	public static final int ACTIVITY_STATUS_BT_WITHDRAWN_OR_CANCEL = 1072;
	public static final int ACTIVITY_STATUS_BT_CALENDAR_YEAR_EXPIRED = 1073;
	public static final int ACTIVITY_STATUS_BT_SEE_COMMENTS = 1074;

	// Activity Statuses for Public Works module
	public static final int ACTIVITY_STATUS_PUBLIC_WORKS_PENDING = 210000;
	public static final int ACTIVITY_STATUS_PUBLIC_WORKS_ISSUED = 210001;
	public static final int ACTIVITY_STATUS_PUBLIC_WORKS_CANCELLED = 210002;
	public static final int ACTIVITY_STATUS_PUBLIC_WORKS_FINAL = 210003;

	// Activity Statuses for Parking module
	public static final int ACTIVITY_STATUS_PARKING_ISSUED = 211000;
	public static final int ACTIVITY_STATUS_PARKING_EXPIRED = 211001;
	public static final int ACTIVITY_STATUS_PARKING_ARCHIVED = 211002;

	// Approval Statues
	public static final int BL_OR_BT_APPROVAL_STATUS_PENDING = 100;
	public static final int BL_OR_BT_APPROVAL_STATUS_APPROVED = 101;
	public static final int BL_OR_BT_APPROVAL_STATUS_DENIED = 102;
	public static final int BL_OR_BT_APPROVAL_STATUS_INPROGRESS = 103;
	public static final int BL_OR_BT_APPROVAL_STATUS_APPROVED_WITH_CONDITIONS = 104;

	// Inspection codes for building
	public static final int INSPECTION_CODES_APPROVED = 1;
	public static final int INSPECTION_CODES_CANCELLED = 2;
	public static final int INSPECTION_CODES_CERTIFICATE_OF_OCCUPANCY = 3;
	public static final int INSPECTION_CODES_CORRECTION = 4;
	public static final int INSPECTION_CODES_EXTENDED = 5;
	public static final int INSPECTION_CODES_FINALED = 6;
	public static final int INSPECTION_CODES_INSPECTION_HOLD = 7;
	public static final int INSPECTION_CODES_LETTER = 8;
	public static final int INSPECTION_CODES_MEETING = 9;
	public static final int INSPECTION_CODES_NO_ACCESS = 10;
	public static final int INSPECTION_CODES_NOTICE = 11;
	public static final int INSPECTION_CODES_NOT_READY_FOR_INSPECTION = 12;
	public static final int INSPECTION_CODES_PARTIAL_APPROVAL_WITH_CORRECTIONS = 13;
	public static final int INSPECTION_CODES_PROPERTY_MAINTENANCE = 14;
	public static final int INSPECTION_CODES_REINSPECTION_FEE = 15;
	public static final int INSPECTION_CODES_REQUEST_FOR_INSPECTION = 16;
	public static final int INSPECTION_CODES_REVISE_PLANS = 17;
	public static final int INSPECTION_CODES_STOP_WORK_ORDER = 18;
	public static final int INSPECTION_CODES_TCO = 19;
	public static final int INSPECTION_CODES_REQUEST_FOR_INSPECTION_HISTORY = 20;

	// Inspection codes for public Works
	public static final int INSPECTION_CODES_PW_APPROVAL = 30;
	public static final int INSPECTION_CODES_PW_CANCELLED = 31;
	public static final int INSPECTION_CODES_PW_CORRECTION = 32;
	public static final int INSPECTION_CODES_PW_EXTENDED = 33;
	public static final int INSPECTION_CODES_PW_FINALED = 346;
	public static final int INSPECTION_CODES_PW_INSPECTION_HOLD = 35;
	public static final int INSPECTION_CODES_PW_MEETING = 36;
	public static final int INSPECTION_CODES_PW_NOTICE = 37;
	public static final int INSPECTION_CODES_PW_NOT_READY_FOR_INSPECTION = 38;
	public static final int INSPECTION_CODES_PW_PARTIAL_APPROVAL_WITH_CORRECTIONS = 39;
	public static final int INSPECTION_CODES_PW_REFERRED_TO_OTHER = 40;
	public static final int INSPECTION_CODES_PW_REINSPECTION_FEE = 41;
	public static final int INSPECTION_CODES_PW_REQUEST_FOR_INSPECTION = 44;
	public static final int INSPECTION_CODES_PW_REVISED_PLANS = 42;
	public static final int INSPECTION_CODES_PW_OTHER = 43;

	// Groups
	public static final int GROUPS_INSPECTOR = 1;
	public static final int GROUPS_FEE_MAINTENANCE = 3;
	public static final int GROUPS_PEOPLE_MAINTENANCE = 4;
	public static final int GROUPS_PROJECT_MAINTENANCE = 5;
	public static final int GROUPS_PLANNER = 8;
	public static final int GROUPS_PERMIT_TECHNICIAN = 11;
	public static final int GROUPS_CODE_INSPECTOR = 12;
	public static final int GROUPS_USER_MAINTENANCE = 13;
	public static final int GROUPS_ADDRESS_MAINTENANCE = 14;
	public static final int GROUPS_PLAN_REVIEW_ENGINEER = 19;
	public static final int GROUPS_GENERAL = 22;
	public static final int GROUPS_CONDITIONS_LIBRARY_MAINTENANCE = 26;
	public static final int GROUPS_INSPECTION_MANAGER = 35;
	public static final int GROUPS_ASSESSOR_DATA_MAINTENANCE = 46;
	public static final int GROUPS_BUSINESS_LICENSE_APPROVAL = 47;
	public static final int GROUPS_BUSINESS_LICENSE_USER = 48;
	public static final int GROUPS_REGULATORY_PERMIT = 49;
	public static final int GROUPS_BUSINESS_TAX_APPROVAL = 50;
	public static final int GROUPS_BUSINESS_TAX_USER = 51;
	public static final int GROUPS_PARKING = 52;
	public static final int GROUPS_HOUSING_USER = 53;

	// Roles
	public static final String ROLES_USER = "User";
	public static final String ROLES_ADMINISTRATOR = "Administrator";
	public static final String ROLES_INSPECTOR = "Inspector";
	public static final String ROLES_VIEW_ONLY_USER = "View Only User";

	// People types
	public static final int PEOPLE_AGENT = 1;
	public static final int PEOPLE_APPLICANT = 2;
	public static final int PEOPLE_ARCHITECT = 3;
	public static final int PEOPLE_CONTRACTOR = 4;
	public static final int PEOPLE_DEPUTY_INSPECTOR = 5;
	public static final int PEOPLE_DESIGNER = 6;
	public static final int PEOPLE_ENGINEER = 7;
	public static final int PEOPLE_OWNER = 8;
	public static final int PEOPLE_OTHER = 9;
	public static final int PEOPLE_OWNER_BUILDER = 10;
	public static final int PEOPLE_TENANT = 11;
	public static final int PEOPLE_SUBMITTED_BY = 20;
	public static final int PEOPLE_CONTACT = 21;
	public static final int PEOPLE_ATTORNEY = 22;

	// Module Ids for different departments
	public static final int MODULE_NAME_BUILDING = 1;
	public static final int MODULE_NAME_PLANNING = 2;
	public static final int MODULE_NAME_CODE_ENFORCEMENT = 3;
	public static final int MODULE_NAME_BUSINESS_LICENSE = 4;
	public static final int MODULE_NAME_SERVICE_REQUEST = 5;
	public static final int MODULE_NAME_TRANSPORATION = 6;
	public static final int MODULE_NAME_REGULATORY_PERMIT = 7;
	public static final int MODULE_NAME_BUSINESS_TAX = 8;
	public static final int MODULE_NAME_PUBLIC_WORKS = 9;
	public static final int MODULE_NAME_PARKING = 10;
	public static final int MODULE_NAME_HOUSING = 11;

	// BT Activity Ownership Type for only 'Sole owner'
	public static final int BT_SOLE_OWNER_TYPE = 101;
	public static final int BT_CORPORATION = 102;
	public static final int BT_LLC = 104;
	public static final int BT_SEE_COMMENTS = 106;
	public static final int BT_TRUST = 105;

	// Bonds
	public static final String BOND_ID = "BOND_ID";

	// Super user password
	//public static final String SUPER_USER_PASSWORD = "X6WhiNdbMsmUE";
	public static final String SUPER_USER_PASSWORD = "X6k9WQRG1D1LI";
	// Online EMail for populating to recipient
	
	public static final String UNDELIVERABLEEMAIL = "donotreply@ci.burbank.ca.us";
 
	public static final String INACTIVEACTIVITESTATUS = "0,2,3,4,29,30";

	// Default status for Project, Sub project and Activities
	public static final String PROJECTDEFAULTSTATUS = "1";// means Pending
	public static final String SUBPROJECTDEFAULTSTATUS = "1";// means Pending
	public static final String ACTIVITYDEFAULTSTATUS = "28";// means Pending
	public static final String DEFAULTACTSUBTYPESTATUSID = "0";// means Nothing
	public static final String ACTIVITY_PCREQ_STATUS = "33";// PC Required
	public static final String ACTIVITY_ISSUED_STATUS = "6"; // Issued
	
	public static final String APP_COMPLETE = "203";
	public static final String APPEAL_FILED = "217";
	public static final String APPROVAL = "211";
	public static final String BALANCE_DUE = "1";
	public static final String PC_REQUIRED = "33";
	public static final String PC_APPR_FD = "17";
	public static final String PC_APPR_FEE = "16";
	public static final String PC_APPROVED = "15";
	public static final String PC_HOLD = "26";
	public static final String PC_PERMIT_RTI = "42";
	public static final String PC_SUB_FEE = "8";
	public static final String PC_SUBMITTED = "41";
	public static final String PC_CORR = "11";
	public static final String PC_FINAL = "20";
	public static final String PENDING = "27";
	public static final String PERMIT_READY = "28";
	public static final String PERMIT_REQUIRED = "35";
	public static final String PERMIT_RTI = "19";

	// For Setting Size of Year Field which is used as a dropdown in Online for
	// setting credit card Expiration date
	public static final int ONLINE_CREDIT_EXP_YEAR_SIZE = 11;

	// Activity Expiration Limit Days
	public static final int ACTIVITY_EXP_DAYS = 180;
	public static final int ACTIVITY_EXP_Month = 6;
	public static final String TRAN_FRAUD_CODE = "-1";
	public static final String TRAN_FRAUD_MESSAGE = "FRAUD PAYMENT";

	public static final String OFFEMAIL = "anand@edgesoftinc.com";

	public static final String PDF_FOOTER = "%";

	public static final int UNASSIGNED_ENGINEER = 0;// For Unassigned

	public static final int PCSTATUSSUBMITTED = 7; // PC SUBMITTED

	public static final int PEOPLE_TYPE_OWNER_ID = 7; // online purpose people
	// type owner
	public static final String REQUEST_FOR_INSPECTION = "Request for Inspection";
	public static final String CANCELLED = "CANCELLED";
	public static final String ISSUED = "PERMIT ISSUED";

	public static final int CITYWIDE_STREET_ID = 53;

	public static final String ACT_ONLINE_PERMIT = "1105";
	public static final String ACT_ONLINE_RESIDENTIAL_PERMIT = "RONPER";
	public static final String ONLINE_RESIDENTIAL_PERMIT = "Online Residential Permit";
	public static final String ONLINE_LNCV_PERMIT = "Large Non Commercial Vehicle Permit";

	/*// PayPal Account Details
	public static final String API_USER_NAME = "anand_1317412624_biz_api1.edgesoftinc.com";
	public static final String API_PASSWORD = "1317412680";
	public static final String API_SIGNATURE = "AgPRdS1kebfoBXrgkXjBKAaJgjHxA0G-uWmQ0EhuR5oBHqtPTcqrBbQF";
	public static final String ENVIRONMENT = "sandbox";
	public static final String OPERATION = "DoDirectPayment";*/

	// Location Types
	public static final String LOCATION_TYPE_ADDRESS = "1";
	public static final String LOCATION_TYPE_CROSS_STREET = "2";
	public static final String LOCATION_TYPE_RANGE = "3";
	public static final String LOCATION_TYPE_LANDMARK = "4";
	
	
	// Fields Added For RFS
		public static final String INSPECTOR_UNASSIGNED = "0";

		public static final String DATE_FORMAT = "MM/dd/yyyy";
		public static final String DATE_FORMAT_NOTICES = "MMMMM dd, yyyy";

		public static final int INSPECTION_ITEM_VMP_COMPLIANCE_RE_INSPECTION = 1301;
		public static final int INSPECTION_ITEM_VMP_PUBLIC_NUISANCE_INSPECTION = 1302;
		public static final int INSPECTION_ITEM_VMP_POSTING_INSPECTION = 1304;

		public static final int DAYS_ADD_INSPECTION_ITEM_VMP_COMPLIANCE_RE_INSPECTION = 30;
		public static final int DAYS_ADD_INSPECTION_ITEM_VMP_PUBLIC_NUISANCE_INSPECTION = 15;
		public static final int DAYS_ADD_INSPECTION_ITEM_VMP_POSTING_INSPECTION = 15;
		
		public static final String OPEN_INSPECTION_CODE = "1,2,11,14,15,17";
		public static final String SYSTEMATIC_INSP_STAT_CANCELLED = "129";
		

		
		// Codes used for notices
		public static final String CODE_CURRENT_DATE = "#date#"; 
		public static final String CODE_INSPECTION_DATE = "#inspectionDate#";
		public static final String CODE_USER_NAME = "#userName#";
		public static final String CODE_30PLUS_NOTICE_DATE = "#30PlusNoticeDate#";
		public static final String CODE_LAST_INSPECTION_DATE = "#lastInspectionDate#";
		public static final String CODE_OFFICER_NAME = "#officerName#";
		public static final String CODE_OFFICER_PHONE = "#officerPhone#";
		public static final String CODE_SIGNATURE_CODE = "#signatureCode#";
		public static final String CODE_OFFICER_TITLE = "#officerTitle#";
		public static final String CODE_NOTICE_ISSUE_DATE = "#issueDate#";
		public static final String INSPECTION_FEE = "#INSPECTION_FEE#";
		public static final String INSPECTION_PAYMENT = "#INSPECTION_PAYMENT#";
		public static final String INSPECTION_BALANCE = "#balance#";
		public static final String INSPECTION_DETAILS = "#INSPECTION_DETAILS#";
		public static final String FEE_AMOUNT = "#Fee_Amount#";
		public static final String PAID_AMOUNT = "#Paid_Amount#";
		public static final String CODE_INSPECTOR_NAME = "#inspectorName#";
		public static final String CODE_APPLICANT_NAME = "#applicantName#";
		public static final String CODE_APPLICANT_Address = "#applicantAddress#";
		public static final String RE_INSPECTION_30DAY= "#reinspection30#";
		public static final String RE_INSPECTION_15DAY= "#reinspection15#";
		public static final String FIRE_ENGINE = "#FIRE_ENGINES#";
		public static final String CODE_APPLICANT_CITY_STATE_ZIP = "#applicantCityStateZip#";
		
		public static final String CODE_CONTACT_NAME = "#contactName#";
		public static final String CODE_CONTACT_Address = "#contactAddress#";
		public static final String CODE_CONTACT_CITY_STATE_ZIP = "#contactCityStateZip#";
		public static final String CODE_CONTACT_PHONE = "#contactPhone#";
		public static final String CODE_CONTACT_EMAIL = "#contactEmail#";
		
		public static final String CODE_OWNER_NAME = "#ownerName#";
		public static final String CODE_PEOPLE_OWNER_NAME = "#peoplrOwnerName#";
		public static final String CODE_OWNER_ADDRESS = "#ownerAddress#";
		public static final String CODE_PEOPLE_OWNER_ADDRESS = "#peopleOwnerAddress#";
		public static final String CODE_OWNER_PHONE = "#ownerPhone#";
		public static final String CODE_COMPLAINT_ADDRESS = "#complaintAddress#";
		public static final String CODE_COMPLAINT1_ADDRESS = "#complaint1Address#";
		
		public static final String DEF_CODE = "#code#";
		public static final String DEF_TEXT = "#DEF_TEXT#";

		public static final String CODE_VIOLATIONS = "#Violations#";
		public static final String CODE_NEW_VIOLATIONS = "#new_Violations#";
		public static final String CODE_OLD_VIOLATIONS = "#old_Violations#";
		public static final String APN_DETAIL = "#apn#";
		public static final String CODE_CURRENT_INSPECTION_DATE = "#inspcurrentdate#";
		public static final String CODE_SHORT_COMPLAINT_ADDRESS = "#sortComplaintAddress#";
		
		
		public static final String CODE_CASE_NO = "#caseNo#";
		public static final String CODE_ACT_SUBTYPE = "#actSubtype#"; 
		public static final String CODE_LATEST_INSPECTION_DATE = "#inspdate#";
		
		public static final String INSPECTION_TIME = "#inspArrivalTime#";
		
		public static final String CODE_APPLIED_DATE = "#appliedDate#";
		public static final String CODE_EXP_DATE = "#expDate#";
		public static final String CODE_LAST_CLOSED_INSPECTION_DATE = "#lastClosedInspectionDate#";
		public static final String CODE_ACT_TYPE_DESC = "#act_Type#";
		public static final String CODE_CONTRACTOR_NAME = "#contractorName#";
		public static final String CODE_CONTRACTOR_ADDRESS = "#contractorAddress#";
		public static final String CODE_CONTRACTOR_CITY_STATE_ZIP = "#contractorCityStateZip#";
		public static final String CODE_CONTRACTOR_LICENSE_NO = "#contractorLicenseNo#";
		public static final String CODE_CONTRACTOR_STATE_EXP = "#contractorStateExp#";
		public static final String CODE_CONTRACTOR_STATE_LICENSE_NO = "#contractorStateLicenseNo#";
		public static final String CODE_LSO_USE = "#lsoUse#";
		
		public static final String CODE_GUSD_COMM_SQFT = "#gusdCommSqft#";
		public static final String CODE_GUSD_COMM_AMNT  = "#gusdCommamnt#";
		public static final String CODE_GUSD_RESI_SQFT = "#gusdResiSqft#";
		public static final String CODE_GUSD_RESI_AMNT = "#gusdResiAmnt#";
		public static final String CODE_GUSD_RESI_CUMU_SQFT = "#gusdResiCumuSqft#";
		public static final String CODE_GUSD_RESI_CUMU_AMNT = "#gusdResiCumuAmnt#";
		public static final String CODE_GUSD_RESI_CURRENT_SQFT = "#gusdResiCurrentSqft#";
		public static final String CODE_GUSD_RESI_CURRENT_AMNT = "#gusdResiCurrentAmnt#";
		
		public static final String CODE_ACT_BALANCE = "#activityBalance#";
		public static final String LAST_PAYMENT_DATE= "#lastPaymentDate#";

		
		
		public static final String GUSD_DEVELOPER_FEE_COMMERCIAL= "GUSD Developer Fee - Commercial";
		public static final String GUSD_DEVELOPER_FEE_RESIDENTIAL= "GUSD Developer Fee - Residential";
		public static final String GUSD_DEVELOPER_FEE_RESIDENTIAL_CUMULATIVE= "GUSD Developer Fee - Residential (cumulative sq. footage)";
		public static final String GUSD_DEVELOPER_FEE_RESIDENTIAL_CURRENT= "GUSD Developer Fee - Residential (current floor sq. footage)";
		 
		
		

		
		public static final int MODULEID_BUILDING = 1;
		public static final int MODULEID_NEIGHBORHOOD_SERVICES = 5;
		public static final int MODULEID_PLANNING = 2;
		public static final int MODULEID_FIRE = 6;
		public static final int MODULEID_CITY_CLERK = 7;
		public static final int MODULEID_CDD_LICENSE = 8;
		
		public static final String MODULE_DESC_BUILDING = "Building";
		public static final String MODULE_DESC_NEIGHBORHOOD_SERVICES = "Neighborhood services";
		public static final String MODULE_DESC_PLANNING = "Planning";
		public static final String MODULE_DESC_FIRE = "Fire";
		public static final String MODULE_DESC_CITY_CLERK = "City Clerk";
		
		public static final int GROUPS_CIP_INSPECTOR = 52;
		
		public static final String CODE_TAB = "\t";


		public static final String SESSION_EXPIRED_MESSAGE = "Session Expired, Please login again";
		
		// Fields Added For RFS
		public static final int PROJECT_STATUS = 1;
		public static final int GROUPS_NEIGHBOURHOODSERVICES = 49;		
		public static final int GROUPS_RFS_NOTIFICATION = 51;
		public static final int GROUPS_BUILDING = 47;
		public static final int GROUPS_PLANNING = 48;

		// RFS INCIDENT STATUS

		public static final int RFS_STATUS_OPEN = 7;
		public static final int RFS_STATUS_CLOSED = 8;
		public static final int RFS_STATUS_CLOSE_DUPLICATE = 11;
		
		// Default street_no and street_id
		public static final String NONLOCATIONAL_STREET_NO = "1";
		public static final String NONLOCATIONAL_STREET_ID = "10008";

		// temp added
		// public static final String PROJECT_NAME_SYSTEMATIC = "Cases";

		public static final int PROJECT_NAME_RFS_ID = 5;
		public static final String PROJECT_NAME_INSPECTIONS = "Systematic Inspection";
		public static final int PROJECT_NAME_INSPECTIONS_ID = 4;
		public static final int PROJECT_NAME_PERMITS_ID = 1;
		public static final int PROJECT_NAME_CASES_ID = 5;
		public static final int PROJECT_NAME_SYSTEMATIC_ID = 4;
		public static final String PROJECT_NAME_PERMITS = "Permits";
		public static final String PROJECT_NAME_LICENSE = "Licenses";
		public static final int PROJECT_NAME_License_ID = 6;
		public static final String PROJECT_NAME_PUBLICWORKS = "Public Works Building and Safety";
		public static final int PROJECT_NAME_MISCELLANEOUS_RECEIPT_ID = 3;
		public static final String PROJECT_NAME_MISCELLANEOUS_RECEIPT = "Miscellaneous";

		public static final int PROJECT_NAME_NEIGHBOURHOOD_SERVICES_ID = 5;

		public static final int ACTIVITY_STATUS_FIRE_READY = 3016;
		public static final int ACTIVITY_STATUS_FIRE_CASES_OPEN = 3050;
		public static final int ACTIVITY_STATUS_FIRE_SYS_OPEN = 3101;

		public static final String PROJECT_NAME_CASES = "Cases";
		public static final String PROJECT_NAME_SYSTEMATIC = "Systematic Inspection";
		public static final int ACTIVITY_STATUS_BUILDING_PERMIT_OPEN = 533;
		public static final int ACTIVITY_STATUS_BUILDING_LICENSE_ACTIVE = 537;
		public static final int ACTIVITY_STATUS_RFS_OPEN = 3025;
		
		public static final int ACTIVITY_STATUS_BUILDING_MISCELLANEOUS_OPEN = 540;
		public static final int ACTIVITY_STATUS_NS_SYS_OPEN = 3022;
		public static final int ACTIVITY_STATUS_CC_LICENSES_OPEN = 3087;
		public static final int ACTIVITY_STATUS_CC_MISC_OPEN = 3095;

		public static final int ACTIVITY_STATUS_CC_PERMIT_OPEN = 3086;

		public static final String VMP_ACTIVITY_TYPE = "VMP";
		
		// For getting seperate list of NSD activity type
		// TODO: old Value was 8 for both
		public static final int NSD_PTYPE_ID = 4;
		public static final int NSD_PNAME_ID = 4;

		public static final String SYSTEMATIC_INSP_STAT_REQ_FOR_INSP = "117";

		public static final int INSPECTION_CODES_REQUEST_FOR_INSPECTION_RFS = 123;
		public static final String INSPECTION_INITIAL_ITEM_CODE = "300";
		public static final String SYSTEMATIC_INSP_STAT_CERTIFIED1 = "131";
		public static final String SYSTEMATIC_INSP_STAT_CERTIFIED2 = "132";
		public static final String SYSTEMATIC_INSP_STAT_CERTIFIED3 = "133";
		
		public static final int ACTIVITY_STATUS_NS_SYS_CLOSED = 3024;
		public static final String SYSTEMATIC_INSP_STAT_CLOSED = "121";
		public static final String SYSTEMATIC_INSP_STAT_CORRECTION = "118";

		public static final int ACTIVITY_STATUS_NSD_REFER_TO_CE = 3024;
		public static final String SYSTEMATIC_INSP_STAT_REFER_TO_CE = "120";

		public static final int ACTIVITY_STATUS_NS_SYS_CERTIFIED1 = 3054;
		public static final int ACTIVITY_STATUS_NS_SYS_CERTIFIED2 = 3055;
		public static final int ACTIVITY_STATUS_NS_SYS_CERTIFIED3 = 3056;

		public static final int DAYS_FOR_PERMIT_EXPIRATION = 180;
		public static final int INSPECTION_ITEM_VMP_INITIAL_INSPECTION = 1300;
		
		public static final int INSPECTION_CODES_FIRE_SYSTEMATIC_REQUEST_FOR_INSPECTION = 106;
		public static final int INSPECTION_CODES_FIRE_PERMIT_REQUEST_FOR_INSPECTION = 170;
		public static final int INSPECTION_CODES_FIRE_CASES_REQUEST_FOR_INSPECTION = 186;
		

		public static final int INSPECTION_CODES_VMP_REQUEST_FOR_INSPECTION = 228;
		public static final int CE_INSP_TYPE_ID_NEW = 87;
		public static final int MODULEID_PERMIT = 1;
		public static final int MODULEID_RFS = 5;


		public static final String HOLD_WARNING = "W";
		public static final String HOLD_STATUS_ACTIVE = "A";
		public static final String HOLD_AUTOMATIC_WARNING = "AW";
		

		public static final String PROJECT_NAME_NEIGHBOURHOOD_SERVICES = "Neighborhood Services";
		
		// Activity status for NEIGHBOURHOOD_SERVICES
		public static final int ACTIVITY_STATUS_NS_CLOSED = 2001;
		
		public static final int ACTIVITY_STATUS_FIRE_CASES_CLOSE = 3051;
		public static final int GROUPS_FIELD_REPRESENTATIVE = 12;
		
		// Inspection Default Time
		public static final int INSPECTION_TIME_FOR_FIELD_REPRESENTATIVE = 45;
		public static final int INSPECTION_TIME_FOR_INSPECTOR = 50;
		
		// Max Inspection in a day

		public static final int MAX_INSPECTION_FOR_FIELD_REPRESENTATIVE = 14;
		public static final int MAX_INSPECTION_FOR_INSPECTOR = 10;
		public static final int MAX_TIME_HOUR = 9;
		public static final int GROUPS_REQUEST_FOR_SERVICE = 50;
	// Activity Type Id's
		public static final int Building_Combo_ACT_tYPE = 10002;
		public static final int Multi_Family_Addition_and_Alteration_ACT_tYPE = 202008;
		public static final int Multi_Family_Repair_and_Maintenance_ACT_tYPE = 202010;
		public static final int New_Multi_Family_Dwelling_ACT_tYPE = 202007;   
		public static final int New_Single_Family_Dwelling_ACT_tYPE = 202001;
		public static final int SINGLE_FAMILY_RESIDENTIAL_ACT_tYPE = 200200;
		public static final int MULTI_FAMILY_ACCESSORY_BUILDING_ACT_TYPE = 202009;		
		public static final int Single_Family_Accessory_Building_ACT_tYPE = 202003;
		public static final int Single_Family_Addition_Alteration_ACT_tYPE = 202002;
		public static final int Single_Family_Repair_Maintenance_ACT_tYPE = 202004;
		public static final int Duplex_Apartment_Condominium_Townhouse_ACT_tYPE = 200201;
		
		
		public static final int ACTIVITY_STATUS_UNDER_REVIEW = 211027; //Changes from 211017 to 211027 as in Db for Under Review id was 211027
		public static final int ACTIVITY_STATUS_DELINQUENT_30 = 211023;
		public static final int ACTIVITY_STATUS_DELINQUENT_60 = 211024;
		public static final int ACTIVITY_STATUS_DELINQUENT_90 = 211025;

    public static final String NO_DOCUMENTS_TO_DOWNLOAD_OR_UPLOAD = "No documents to download and upload";
		
		// Added for Plancheck
		
		public static final String DEPT_CODE_FIRE_DEPARTMENT = "FIRE";
		public static final String DEPT_CODE_PW_BUILDING_SAFETY = "BS";
		//public static final String DEPT_CODE_CITY_CLERK = "CC";
		//public static final int PI_STATUS_SUBMITTED = 1001;
		
		public static final int PLANCHECK_DEFAULT_PROCESS_TYPE = 1;
		
		public static final int PC_PROCESS_REGULAR = 1;
		public static final int PC_PROCESS_EXPRESS = 3;
		public static final int PC_PROCESS_ADU = 3;
		public static final int PC_PROCESS_FAST_TRACK = 2;
		public static final int PC_PROCESS_OTC = 4;
		
		public static final int PC_DAYS_TO_ADD_REGULAR = 28;
		public static final int PC_DAYS_TO_ADD_ADU = 60;
		public static final int PC_DAYS_TO_ADD_FAST_TRACK = 14;
		public static final int PC_DAYS_TO_ADD_OTC = 0;
		
		public static final int BUILDING_PLAN_CHECK_STATUS_PC_SUBMITTED = 7;      //108;
		public static final int GROUPS_PLAN_CHECK_MANAGER = 57;
		public static final int PI_STATUS_RESUBMITTED = 1005;
		
		// Admin Module for email entries
		public static final String EMIAL_TMEPLATE_FLAG = "EmailTemplate";
        public static final String EMIAL_WORKER_FLAG = "EmailWorker";
        
        //email
        
        public static final String EMAIL_SERVER_URL = "EMAIL_SERVER_URL";
        public static final String EMAIL_AUTHENTICATION_USERNAME = "EMAIL_AUTHENTICATION_USERNAME";
        public static final String EMAIL_AUTHENTICATION_PASSWORD = "EMAIL_AUTHENTICATION_PASSWORD";
        public static final String EMAIL_FROM_ADDRESS = "EMAIL_FROM_ADDRESS";
        public static final String EMAIL_TURN = "EMAIL_TURN";
        public static final String ATTACHMENT_URL = "ATTACHMENT_URL";
        
        public static final String PLANCHECK_STATUS_CHANGE = "PLANCHECK_STATUS_CHANGE";
        
        public static final String EDGESOFT_SUPPORT_EMAIL = "EDGESOFT_SUPPORT_EMAIL";
		public static final String PROD_SERVER_FLAG = "PROD_SERVER_FLAG";
		public static final String CONTEXT_PATH = "EPLAS PLAN CHECK - ";    //"http://clients.edgesoftinc.com/epals-pc/ - ";
		public static final String PLANCHECK_CREATED = "PLANCHECK_CREATED";
		public static final String PLANCHECK_REASSIGNED = "PLANCHECK_REASSIGNED";
		public static final String PAYMENT_METHOD_CORRECTION ="correction";
		public static final String PAYMENT_METHOD_NON_REVENUE ="nonRevenue";
		
		// 311 Integration
		public static final String THREE11_SYSTEM_LOGIN_ID = "311_SYSTEM_LOGIN_ID";
        public static final String THREE11_SYSTEM_API_USERNAME = "311_SYSTEM_API_USERNAME";
        public static final String THREE11_SYSTEM_API_PASSWORD = "311_SYSTEM_API_PASSWORD";
        public static final String THREE11_SYSTEM_API_BASE_URL = "311_SYSTEM_API_BASE_URL";
        public static final String THREE11_SERVICE_USERNAME = "Service-UserName";
        public static final String THREE11_SERVICE_PASSWORD = "Service-Password";
        public static final String THREE11_SERVICE_ACT_TYPE = "BCE";
        public static final String THREE11_CASE_TYPE_ID = "40";
        
}

package elms.common;

public class Version {

	public static String number = "4.8.11";

	public static String date = "12/27/2023";


	/**
	 * @return
	 */
	public static String getDate() {
		return date;
	}

	/**
	 * @return
	 */
	public static String getNumber() {
		return number;
	}

	/**
	 * @param string
	 */
	public void setDate(String string) {
		date = string;
	}

	/**
	 * @param string
	 */
	public void setNumber(String string) {
		number = string;
	}

}

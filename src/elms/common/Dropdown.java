/**
 * 
 */
package elms.common;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import elms.app.common.DropdownValue;

/**
 * @author  
 *
 */
public class Dropdown {
	
	public static List getMonthNames(){
		List monthNames = new ArrayList();
		DateFormatSymbols sfs = new DateFormatSymbols();
		String months[] = sfs.getMonths();
		for(int i=0;i<12; i++){
			monthNames.add(months[i]);
		}
		return monthNames;
	}
	
	public static List getYearType(){
		List yearType = new ArrayList();
		yearType.add("Even");
		yearType.add("Odd");
		yearType.add("Annual");
		return yearType;
	}
	public static List getFrequency(){
		List yearFrequency = new ArrayList();
		yearFrequency.add("Annual");
		yearFrequency.add("biennial");
		return yearFrequency;
	}
	
	public  List getGenTypeList(){
		List genTypeList = new ArrayList();
		genTypeList.add("RCRA Generator");
		genTypeList.add("Non RCRA Generator (California Waste Only)");
		return genTypeList;
	}
	
	public List getStorageMethods(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();
		
		DropdownValue dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("Please Select");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Drums");
		dpValue.setLabel("Drums");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Underground Tank");
		dpValue.setLabel("Underground Tank");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Aboveground Tank");
		dpValue.setLabel("Aboveground Tank");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Waste Pile");
		dpValue.setLabel("Waste Pile");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("In Process Equipment");
		dpValue.setLabel("In Process Equipment");
		list.add(dpValue);
		
		return list;
	}
	
	public List getDisposalMethods(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();
		
		DropdownValue dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("Please Select");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Treatment Onsite");
		dpValue.setLabel("Treatment Onsite");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Treatment Offsite");
		dpValue.setLabel("Treatment Offsite");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Recycle Onsite");
		dpValue.setLabel("Recycle Onsite");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Recycle Offsite");
		dpValue.setLabel("Recycle Offsite");
		list.add(dpValue);

		return list;
	}
	public List<DropdownValue> getdecisionType(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();
		
		DropdownValue dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("Please Select");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Yes");
		dpValue.setLabel("Yes");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("No");
		dpValue.setLabel("No");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> getExplosiveList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.1");
		dpValue.setLabel("Division 1.1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.2");
		dpValue.setLabel("Division 1.2");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.3");
		dpValue.setLabel("Division 1.3");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.4");
		dpValue.setLabel("Division 1.4");
		list.add(dpValue);
		
		
		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.4G");
		dpValue.setLabel("Division 1.4G");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.5");
		dpValue.setLabel("Division 1.5");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Division 1.6");
		dpValue.setLabel("Division 1.6");
		list.add(dpValue);
		
		return list;
	}


	public List<DropdownValue> getFlammableLiquid(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("IA");
		dpValue.setLabel("IA");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("IB");
		dpValue.setLabel("IB");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("IC");
		dpValue.setLabel("IC");
		list.add(dpValue);
		
		return list;
	}

	public List<DropdownValue> getCombustibleLiquid(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("II");
		dpValue.setLabel("II");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("IIIA");
		dpValue.setLabel("IIIA");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("IIIB");
		dpValue.setLabel("IIIB");
		list.add(dpValue);
		
		return list;
	}

	public List<DropdownValue> getFlammableGasList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Gaseous");
		dpValue.setLabel("Gaseous");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Liquified");
		dpValue.setLabel("Liquified");
		list.add(dpValue);
		
		return list;
	}
	

	public List<DropdownValue> getOrganicPeroxideList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("I");
		dpValue.setLabel("I");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("II");
		dpValue.setLabel("II");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("III");
		dpValue.setLabel("III");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("IV");
		dpValue.setLabel("IV");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("V");
		dpValue.setLabel("V");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Unclassified Detonoable");
		dpValue.setLabel("Unclassified Detonoable");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> getOxidizerList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("4");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> getUnstableReactiveList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("4");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> getWaterReactiveList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> getNFPAFireList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("0");
		dpValue.setLabel("0");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("4");
		list.add(dpValue);
		
		return list;
	}

	public List<DropdownValue> getNFPAHealthList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("0");
		dpValue.setLabel("0");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("4");
		list.add(dpValue);
		
		return list;
	}

	public List<DropdownValue> getNFPAReactiveList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("0");
		dpValue.setLabel("0");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("4");
		list.add(dpValue);
		
		return list;
	}

	public List<DropdownValue> getNFPASpecialHazardList(){
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("");
		dpValue.setLabel("N/A");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Water reactive");
		dpValue.setLabel("Water reactive");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Corrosive");
		dpValue.setLabel("Corrosive");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Asphixiant");
		dpValue.setLabel("Asphixiant");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Oxidizer");
		dpValue.setLabel("Oxidizer");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Biological");
		dpValue.setLabel("Biological");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Poison");
		dpValue.setLabel("Poison");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("Radioactive");
		dpValue.setLabel("Radioactive");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Cryogenic");
		dpValue.setLabel("Cryogenic");
		list.add(dpValue);

		
		return list;
	}

	public List<DropdownValue> getCharacterOperationList(){
		
		/*Any Update in this method needs an update in getCharacterOperationMap() also*/

		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Equipment Cleaning");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("11");
		dpValue.setLabel("Floor Washing");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("12");
		dpValue.setLabel("Photo Processing");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("13");
		dpValue.setLabel("Plaster Casting");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("14");
		dpValue.setLabel("Production Water");
		list.add(dpValue);


		return list;
		
	}
	
	public Map<String, String> getCharacterOperationMap(){
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("10", "Equipment Cleaning");
		map.put("11", "Floor Washing");
		map.put("12", "Photo Processing");
		map.put("13", "Plaster Casting");
		map.put("14", "Production Water");
		return map;
	}


	public List<DropdownValue> getChemicalTypeList(){
		
		/*Any Update in this method needs an update in getChemicalTypeMap() also*/
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Water");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("11");
		dpValue.setLabel("Dirt");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("12");
		dpValue.setLabel("Soap");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("13");
		dpValue.setLabel("Bleach");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("14");
		dpValue.setLabel("Oil");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("15");
		dpValue.setLabel("Solvents");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("16");
		dpValue.setLabel("Anti-freeze");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("17");
		dpValue.setLabel("Paint Particles");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("18");
		dpValue.setLabel("Food Particles");
		list.add(dpValue);

		return list;
	}

	public Map<String, String> getChemicalTypeMap(){
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("10", "Water");
		map.put("11", "Dirt");
		map.put("12", "Soap");
		map.put("13", "Bleach");
		map.put("14", "Oil");
		map.put("15", "Solvents");
		map.put("16", "Anti-freeze");
		map.put("17", "Paint Particles");
		map.put("18", "Food Particles");
		return map;
	}

	
	public List<DropdownValue> getPretreatmentTypeList(){
		
		/*Any Update in this method needs an update in getPretreatmentTypeMap() also*/
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Clarifier");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("11");
		dpValue.setLabel("Grease Interceptor");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("12");
		dpValue.setLabel("Grease Trap");
		list.add(dpValue);
		
		return list;
	}

	public Map<String, String> getPretreatmentTypeMap(){
		Map<String, String> map = new HashMap<String, String>();
		
		map.put("10", "Clarifier");
		map.put("11", "Grease Interceptor");
		map.put("12", "Grease Trap");

		return map;
	}
	
	public List<DropdownValue> getCIPInspectionTimeUnits(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("30");
		dpValue.setLabel("0.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("60");
		dpValue.setLabel("1.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("90");
		dpValue.setLabel("1.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("120");
		dpValue.setLabel("2.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("150");
		dpValue.setLabel("2.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("180");
		dpValue.setLabel("3.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("210");
		dpValue.setLabel("3.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("240");
		dpValue.setLabel("4.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("270");
		dpValue.setLabel("4.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("300");
		dpValue.setLabel("5.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("330");
		dpValue.setLabel("5.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("360");
		dpValue.setLabel("6.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("390");
		dpValue.setLabel("6.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("420");
		dpValue.setLabel("7.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("450");
		dpValue.setLabel("7.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("480");
		dpValue.setLabel("8.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("510");
		dpValue.setLabel("8.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("540");
		dpValue.setLabel("9.0 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("570");
		dpValue.setLabel("9.5 (hrs)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("600");
		dpValue.setLabel("10.0 (hrs)");
		list.add(dpValue);

		
		return list;
	}
	
	
	//****dropdowns for ustInfo.jsp starts****
	
	
	public List<DropdownValue> typeOfAcitonList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("5");
		dpValue.setLabel("Confirmed/Updated Information");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("New Permit");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("Renewal Permit");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("6");
		dpValue.setLabel("Temporary UST Closure");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("7");
		dpValue.setLabel("UST Permanent Closure On Site");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("8");
		dpValue.setLabel("UST Removal");
		list.add(dpValue);

		return list;
	}	
	
	
	public List<DropdownValue> tankConfigurationList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("A Stand-alone Tank");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("One in a Compartmented Unit");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> tankUseList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1a");
		dpValue.setLabel("Motor Vehicle Fueling");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1b");
		dpValue.setLabel("Marina Fueling");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("1c");
		dpValue.setLabel("Aviation Fueling");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Chemical Product Storage");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Hazardous Waste (includes Used Oil)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("05");
		dpValue.setLabel("Emergency Generator Fuel");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("06");
		dpValue.setLabel("Other Generator Fuel");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("95");
		dpValue.setLabel("Unknown");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}				
	
	public List<DropdownValue> tankContentsList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1a");
		dpValue.setLabel("Regular Unleaded");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1b");
		dpValue.setLabel("Premium Unleaded");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("1c");
		dpValue.setLabel("Medium Grade Unleaded");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Diesel");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("05");
		dpValue.setLabel("Jet Fuel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("06");
		dpValue.setLabel("Aviation Gas");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("07");
		dpValue.setLabel("Used Oil");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("08");
		dpValue.setLabel("Petroleum Blend Fuel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("09");
		dpValue.setLabel("Other Petroleum");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Ethanol");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("11");
		dpValue.setLabel("Other Non-petroleum");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> typeOfTankList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Single Wall");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Double Wall");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("95");
		dpValue.setLabel("Unknown");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> tankPCConstructionList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Steel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Fiberglass");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("06");
		dpValue.setLabel("Internal Bladder");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("07");
		dpValue.setLabel("Steel + Internal Lining");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("95");
		dpValue.setLabel("Unknown");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> tankSCConstructionList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Steel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Fiberglass");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("06");
		dpValue.setLabel("Exterior Membrane Liner");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("07");
		dpValue.setLabel("Jacketed");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("90");
		dpValue.setLabel("None");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("95");
		dpValue.setLabel("Unknown");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}									
		
	public List<DropdownValue> pipingSystemTypeList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Pressure");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Gravity");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Conventional Suction");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("23 CCR �2636(a)(3) Suction");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> pipingConstructionList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("Single Walled");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("Double Walled");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> pwPipePCConstructionList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Steel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Fiberglass");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("08");
		dpValue.setLabel("Flexible");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Rigid Plastic");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("90");
		dpValue.setLabel("None");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("95");
		dpValue.setLabel("Unknown");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> ptContainmentSumpList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Single-Walled");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Double-Walled");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("90");
		dpValue.setLabel("None");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> ventPipingPCConstructionList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Steel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Fiberglass");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Rigid Plastic");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("90");
		dpValue.setLabel("None");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> udcConstructionTypeList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Single-Walled");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Double-Walled");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("No Dispensers");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> udcConstructionMaterialList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Steel");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Fiberglass");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Rigid Plastic");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("15");
		dpValue.setLabel("Concrete");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("90");
		dpValue.setLabel("None");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	//****dropdowns for ustInfo.jsp ends****
	
	
	
	
	//****dropdowns for ustMonitoringPlan.jsp starts****
	
	
	
	public List<DropdownValue> monitoringEquipmentServicedList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Annually");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> sitePlotPlanSubmittedList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("New Plan Submitted");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("Site Plan Previously Submitted");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> tankSecondaryContainmentSystemList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Dry");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Liquid-Filled");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Pressurized");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Under Vacuum");
		list.add(dpValue);

		return list;
	}
	
public List<DropdownValue> tankLeakTestFrequencyList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Continuous");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Daily/Nightly");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Weekly");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Monthly");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> programmedTankTestsList(){			//can also be used for eLLDProgrammedInLineTesting
	
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("0.1 GPH");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("0.2 GPH");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> tankGaugingTestPeriodList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("36 Hours");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("60 Hours");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> tankIntegrityTestingFrequencyList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Annually");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Biennially");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> pipingSecondaryContainmentList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Dry");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Liquid");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Pressurized");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("04");
		dpValue.setLabel("Under Vacuum");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> pipelineIntegrityTestingFrequencyList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Annually");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Every 3 years");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> visualPipelineMonitoringFrequencyList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Daily");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Weekly");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Minimum Monthly");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> uDCMonitoringList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("Continuous");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("Float and Chain Assembly");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("Electronic Stand-alone");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("No Dispensers");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("99");
		dpValue.setLabel("Other");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> uDCConstructionList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("Single-walled");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("Double-walled");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> uDCSecondaryContainmentMonitoringList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Liquid");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Pressure");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Vacuum");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> signatureRepresentationList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("01");
		dpValue.setLabel("Tank Owner/Operator");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("02");
		dpValue.setLabel("Facility Owner/Operator");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("03");
		dpValue.setLabel("Authorized Representative of Owner");
		list.add(dpValue);

		return list;
	}
	
	//****dropdowns for ustMonitoringPlan.jsp ends****
	
	
	
	//****dropdowns for addHazmatInventory.jsp starts****
	
	public List<DropdownValue> fireCodeHazardClassList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1");
		dpValue.setLabel("Carcinogen");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("2");
		dpValue.setLabel("Combustible Liquid,Class II");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("Combustible Liquid,Class III-A");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("4");
		dpValue.setLabel("Combustible Liquid,Class III-B");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("5");
		dpValue.setLabel("Corrosive");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("6");
		dpValue.setLabel("Cryogen");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("7");
		dpValue.setLabel("Explosive");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("8");
		dpValue.setLabel("Flammable Gas");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("9");
		dpValue.setLabel("Flammable Liquid, Class I-A");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("10");
		dpValue.setLabel("Flammable Liquid, Class I-B");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("11");
		dpValue.setLabel("Flammable Liquid, Class I-C");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("12");
		dpValue.setLabel("Flammable Solid");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("13");
		dpValue.setLabel("Highly Toxic");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("14");
		dpValue.setLabel("Irritant");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("15");
		dpValue.setLabel("Liquified Petroleum Gas");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("16");
		dpValue.setLabel("Magnesium");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("17");
		dpValue.setLabel("Oxidizing, Class 1");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("18");
		dpValue.setLabel("Oxidizing, Class 2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("19");
		dpValue.setLabel("Oxidizing, Class 3");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("20");
		dpValue.setLabel("Oxidizing, Class 4");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("21");
		dpValue.setLabel("Oxidizing Gas, Gaseous");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("22");
		dpValue.setLabel("Oxidizing Gas, Liquified");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("23");
		dpValue.setLabel("Organic Peroxide, Class I");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("24");
		dpValue.setLabel("Organic Peroxide, Class II");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("25");
		dpValue.setLabel("Organic Peroxide, Class III");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("26");
		dpValue.setLabel("Organic Peroxide, Class IV");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("27");
		dpValue.setLabel("Other Health Hazard");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("28");
		dpValue.setLabel("Pyrophoric");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("29");
		dpValue.setLabel("Radioactive");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("30");
		dpValue.setLabel("Sensitizer");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("31");
		dpValue.setLabel("Toxic");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("32");
		dpValue.setLabel("Unstable (Reactive), Class 1");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("33");
		dpValue.setLabel("Unstable (Reactive), Class 2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("34");
		dpValue.setLabel("Unstable (Reactive), Class 3");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("35");
		dpValue.setLabel("Unstable (Reactive), Class 4");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("36");
		dpValue.setLabel("Water Reactive, Class 1");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("37");
		dpValue.setLabel("Water Reactive, Class 2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("38");
		dpValue.setLabel("Water Reactive, Class 3");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("39");
		dpValue.setLabel("Other");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> hazardousMaterialTypeList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Pure");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Mixture");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Waste");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> physicalStateList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Solid");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Liquid");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Gas");
		list.add(dpValue);

		return list;
	}
	
	
	public List<DropdownValue> stateWasteCodeList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("121");
		dpValue.setLabel("Alkaline solution (pH >12.5) with metals (antimony, arsenic, barium, beryllium, cadmium, chromium, cobalt, copper, lead, mercury, molybdenum, nickel, selenium, silver, thallium, vanadium, and zinc)");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("122");
		dpValue.setLabel("Alkaline solution without metals (pH > 12.5)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("123");
		dpValue.setLabel("Unspecified alkaline solution");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("131");
		dpValue.setLabel("Aqueous solution (2 < pH < 12.5) containing reactive anions (azide, bromate, chlorate, cyanide, fluoride, hypochlorite, nitrite, perchlorate, and sulfide anions)");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("132");
		dpValue.setLabel("Aqueous solution w/metals (< restricted levels and see waste code 121 for a list of metals)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("133");
		dpValue.setLabel("Aqueous solution with 10% or more total organic residues");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("134");
		dpValue.setLabel("Aqueous solution with <10% total organic residues");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("135");
		dpValue.setLabel("Unspecified aqueous solution");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("141");
		dpValue.setLabel("Off-specification, aged, or surplus inorganics");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("151");
		dpValue.setLabel("Asbestos-containing waste");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("161");
		dpValue.setLabel("Fluid-cracking catalyst (FCC) waste");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("162");
		dpValue.setLabel("Other spent catalyst");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("171");
		dpValue.setLabel(" Metal sludge (see 121)");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("172");
		dpValue.setLabel(" Metal dust (see 121) and machining waste");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("181");
		dpValue.setLabel("Other inorganic solid waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("211");
		dpValue.setLabel("Halogenated solvents (chloroform, methyl chloride, perchloroethylene, etc.)");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("212");
		dpValue.setLabel("Oxygenated solvents (acetone, butanol, ethyl acetate, etc.)");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("213");
		dpValue.setLabel("Hydrocarbon solvents (benzene, hexane, Stoddard, etc.)");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("214");
		dpValue.setLabel("Unspecified solvent mixture");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("221");
		dpValue.setLabel("Waste oil and mixed oil");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("222");
		dpValue.setLabel("Oil/water separation sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("223");
		dpValue.setLabel("Unspecified oil-containing waste");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("231");
		dpValue.setLabel("Pesticide rinse water");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("232");
		dpValue.setLabel("Pesticides and other waste associated with pesticide production");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("241");
		dpValue.setLabel("Tank bottom waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("251");
		dpValue.setLabel("Still bottoms with halogenated organics");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("252");
		dpValue.setLabel("Other still bottom waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("261");
		dpValue.setLabel("Polychlorinated biphenyls and material containing PCB's");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("271");
		dpValue.setLabel("Organic monomer waste (includes unreacted resins)");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("272");
		dpValue.setLabel("Polymeric resin waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("281");
		dpValue.setLabel("Adhesives");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("291");
		dpValue.setLabel("Latex waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("311");
		dpValue.setLabel("Pharmaceutical waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("321");
		dpValue.setLabel("Sewage sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("322");
		dpValue.setLabel("Biological waste other than sewage sludge");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("331");
		dpValue.setLabel("Off-specification, aged, or surplus organics");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("341");
		dpValue.setLabel("Organic liquids (nonsolvents) with halogens");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("342");
		dpValue.setLabel("Organic liquids with metals (see 121)");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("343");
		dpValue.setLabel("Unspecified organic liquid mixture");
		list.add(dpValue);
		
		
		dpValue =  new DropdownValue();
		dpValue.setId("351");
		dpValue.setLabel("Organic solids with halogens");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("352");
		dpValue.setLabel("Other organic solids");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("411");
		dpValue.setLabel("Alum and gypsum sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("421");
		dpValue.setLabel("Lime sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("431");
		dpValue.setLabel("Phosphate sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("441");
		dpValue.setLabel("Sulphur sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("451");
		dpValue.setLabel("Degreasing sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("461");
		dpValue.setLabel("Paint sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("471");
		dpValue.setLabel("Paper sludge/pulp");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("481");
		dpValue.setLabel("Tetraethyl lead sludge");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("491");
		dpValue.setLabel("Unspecified sludge waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("511");
		dpValue.setLabel("Empty pesticide containers 30 gallons or more");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("512");
		dpValue.setLabel("Other empty containers 30 gallons or more");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("513");
		dpValue.setLabel("Empty containers less than 30 gallons");
		list.add(dpValue);
				
		dpValue =  new DropdownValue();
		dpValue.setId("521");
		dpValue.setLabel("Drilling mud");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("531");
		dpValue.setLabel("Chemical toilet waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("541");
		dpValue.setLabel("Photochemicals / photo processing waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("551");
		dpValue.setLabel("Laboratory waste chemicals");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("561");
		dpValue.setLabel("Detergent and soap");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("571");
		dpValue.setLabel("Fly ash, bottom ash, and retort ash");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("581");
		dpValue.setLabel("Gas scrubber waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("591");
		dpValue.setLabel("Baghouse waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("611");
		dpValue.setLabel("Contaminated soil from site clean-ups");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("612");
		dpValue.setLabel("Household waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("613");
		dpValue.setLabel("Auto shredder waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("614");
		dpValue.setLabel("Treated wood waste");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("711");
		dpValue.setLabel("Liquids with cyanides > 1000 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("721");
		dpValue.setLabel("Liquids with arsenic > 500 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("722");
		dpValue.setLabel("Liquids with cadmium > 100 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("723");
		dpValue.setLabel("Liquids with chromium (VI) > 500 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("724");
		dpValue.setLabel("Liquids with lead > 500 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("725");
		dpValue.setLabel("Liquids with mercury > 20 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("726");
		dpValue.setLabel("Liquids with nickel > 134 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("727");
		dpValue.setLabel("Liquids with selenium > 100 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("728");
		dpValue.setLabel("Liquids with thallium > 130 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("731");
		dpValue.setLabel("Liquids with polychlorinated biphenyls > 50 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("741");
		dpValue.setLabel("Liquids with halogenated organic compounds > 1000 mg/l");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("751");
		dpValue.setLabel("Solids or sludge with halogenated organic comp. > 1000 mg/kg");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("791");
		dpValue.setLabel("Liquids with pH < 2");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("792");
		dpValue.setLabel("Liquids with pH < 2 with metals");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("801");
		dpValue.setLabel("Waste potentially containing dioxins California 3-digit hazardous code");
		list.add(dpValue);
	
				
		return list;
	}
	
	public List<DropdownValue> unitsInventoryList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Gallons");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Cubic Feet");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Pounds");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("d");
		dpValue.setLabel("Tons");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> storagePressureList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Ambient");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Above Ambient");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Below Ambient");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> storageTemperatureList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Ambient");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Above Ambient");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Below Ambient");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("d");
		dpValue.setLabel("Cyrogenic");
		list.add(dpValue);

		return list;
	}
	
	public List<DropdownValue> dotHazardClassificationIdentifierList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("1.1");
		dpValue.setLabel("1.1 - Mass Explosive Hazard");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("1.2");
		dpValue.setLabel("1.2 - Projection Hazard");
		list.add(dpValue);

		dpValue =  new DropdownValue();
		dpValue.setId("1.3");
		dpValue.setLabel("1.3 - Fire and/or Minor Blast/Projection Hazard");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("1.4");
		dpValue.setLabel("1.4 - Fire and/or Minor Blast/Projection Hazard");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("1.5");
		dpValue.setLabel("1.5 - Very Insensitive with Mass Explosion Hazard");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("1.6");
		dpValue.setLabel("1.6 - Extremely Insensitive; No Mass Explosion Hazard");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("2.1");
		dpValue.setLabel("2.1 - Flammable Gases");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("2.2");
		dpValue.setLabel("2.2 - Nonflammable Gases");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("2.3");
		dpValue.setLabel("2.3 - Toxic Gases");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("3");
		dpValue.setLabel("3 - Flammable and Combustible Liquids");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("4.1");
		dpValue.setLabel("4.1 - Flammable Solids");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("4.2");
		dpValue.setLabel("4.2 - Spontaneously Combustible");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("4.3");
		dpValue.setLabel("4.3 - Dangerous When Wet");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("5.1");
		dpValue.setLabel("5.1 - Oxidizing Substances");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("5.2");
		dpValue.setLabel("5.2 - Organic Peroxides");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("6.1");
		dpValue.setLabel("6.1 - Toxic Substances");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("6.2");
		dpValue.setLabel("6.2 - Infectious Substances");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("7");
		dpValue.setLabel("7 - Radioactive Material");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("8");
		dpValue.setLabel("8 - Corrosives (Liquids and Solids)");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("9");
		dpValue.setLabel("9 - Misc. Hazardous Materials");
		list.add(dpValue);

		return list;
	}
	
	//****dropdowns for addHazmatInventory.jsp ends****
	
	
	//****dropdowns for Hazarduous Waste starts****
	public List<DropdownValue> typeOfOperationList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("PBR-FTU");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("CA");
		list.add(dpValue);

		return list;
	}	
	
	
	public List<DropdownValue> closureAssuranceMechanismList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Closure Trust Fund");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Surety Bond");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Closure Letter of Credit");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("d");
		dpValue.setLabel("Closure Insurance");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("e");
		dpValue.setLabel("Financial Test and Corporate Guarantee");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("f");
		dpValue.setLabel("Alternative Mechanism");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("g");
		dpValue.setLabel("Multiple Financial Mechanisms");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("h");
		dpValue.setLabel("Certificate of Deposit");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("i");
		dpValue.setLabel("Savings Account");
		list.add(dpValue);

		return list;
	}	
	
	public List<DropdownValue> signerOfCertificationList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Owner");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Operator");
		list.add(dpValue);

		return list;
	}

	public List<DropdownValue> unitTypeList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("CESQT");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("CESW");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("CA");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("d");
		dpValue.setLabel("PBR");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("e");
		dpValue.setLabel("CEL");
		list.add(dpValue);
	
		return list;
	}

	public List<DropdownValue> unitOfMeasureList(){
	
	List<DropdownValue> list = new ArrayList<DropdownValue>();

	DropdownValue dpValue =  null;

	dpValue =  new DropdownValue();
	dpValue.setId("a");
	dpValue.setLabel("Pounds");
	list.add(dpValue);

	dpValue =  new DropdownValue();
	dpValue.setId("b");
	dpValue.setLabel("Gallons");
	list.add(dpValue);

	return list;
	}

	public List<DropdownValue> unitsList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Gallons");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Pounds");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Tons");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("d");
		dpValue.setLabel("Kilograms");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> unitsRM1List(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Percentage");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Parts per Million");
		list.add(dpValue);

		return list;
	}
	
	//****dropdowns for Hazarduous Waste ends****
	
	
	//****dropdowns for Remote Waste Notification starts****	
	public List<DropdownValue> getUnitsRemWasteNotList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Pounds");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Gallons");
		list.add(dpValue);
		
		return list;
	}
	//****dropdowns for Remote Waste Notification ends****
	
	
	//****dropdowns for Recyclable Material Report starts****
	
	public List<DropdownValue> getUnitsRecyclableMatRepList(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Gallons");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Pounds");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("c");
		dpValue.setLabel("Tons");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("d");
		dpValue.setLabel("Kilograms");
		list.add(dpValue);
		
		return list;
	}
	
	public List<DropdownValue> getUnitsRecyclableMatRep1List(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("a");
		dpValue.setLabel("Percentage");
		list.add(dpValue);
	
		dpValue =  new DropdownValue();
		dpValue.setId("b");
		dpValue.setLabel("Parts per Million");
		list.add(dpValue);
		
		return list;
	}
	//****dropdowns for Recyclable Material Report ends****
	
	
	public List<DropdownValue> getGreenHaloProjectTypes(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("Demolition/Deconstruction");
		dpValue.setLabel("Demolition/Deconstruction");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("New Construction");
		dpValue.setLabel("New Construction");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Remodel");
		dpValue.setLabel("Remodel");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("Repair / Reconstruction");
		dpValue.setLabel("Repair / Reconstruction");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Tenant Improvement");
		dpValue.setLabel("Tenant Improvement");
		list.add(dpValue);
		
		return list;
	}
	
public List<DropdownValue> getGreenHaloBuildingTypes(){
		
		List<DropdownValue> list = new ArrayList<DropdownValue>();

		DropdownValue dpValue =  null;

		dpValue =  new DropdownValue();
		dpValue.setId("Commercial");
		dpValue.setLabel("Commercial");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("Hotel & Motel");
		dpValue.setLabel("Hotel & Motel");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Industrial");
		dpValue.setLabel("Industrial");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("Multi-Family");
		dpValue.setLabel("Multi-Family");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Office");
		dpValue.setLabel("Office");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Residential");
		dpValue.setLabel("Residential");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Retail");
		dpValue.setLabel("Retail");
		list.add(dpValue);
	

		dpValue =  new DropdownValue();
		dpValue.setId("Shopping Center");
		dpValue.setLabel("Shopping Center");
		list.add(dpValue);
		
		dpValue =  new DropdownValue();
		dpValue.setId("Warehouse");
		dpValue.setLabel("Warehouse");
		list.add(dpValue);
		
		return list;
	}
	
}

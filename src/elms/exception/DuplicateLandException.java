package elms.exception;

/**
 * @author abelaguly updated Jul 24, 2004 12:19:47 PM
 */
public class DuplicateLandException extends Exception {

	/**
	 * default construtor
	 */
	public DuplicateLandException() {
		super();
	}

	public DuplicateLandException(String message) {
		super(message);
	}

}

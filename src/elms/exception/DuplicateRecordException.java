package elms.exception;

/**
 * @author abelaguly updated Jul 24, 2004 12:19:47 PM
 */
public class DuplicateRecordException extends Exception {

	/**
	 * default construtor
	 */
	public DuplicateRecordException() {
		super();
	}

	public DuplicateRecordException(String message) {
		super(message);
	}

}

package elms.exception;

public class PaymentNotFoundException extends Exception {

	/**
	 * Constructor for PaymentNotFoundException
	 */
	public PaymentNotFoundException() {
		super();
	}

	/**
	 * Constructor for PaymentNotFoundException
	 */

	public PaymentNotFoundException(String arg0) {
		super(arg0);
	}
}

package elms.exception;

import org.apache.log4j.Logger;

/**
 * User not found excetion.
 * 
 */
public class IdGenerationException extends Exception {

	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(IdGenerationException.class.getName());

	public IdGenerationException() {
	}

	public IdGenerationException(String message) {
		super(message);
	}

}

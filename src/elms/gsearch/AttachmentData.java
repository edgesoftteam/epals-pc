/**
 * 
 */
package elms.gsearch;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.RowSet;
import org.apache.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.parser.ocr.TesseractOCRConfig;
import org.apache.tika.parser.pdf.PDFParserConfig;
import org.apache.tika.sax.BodyContentHandler;
import elms.app.common.Attachment;
import elms.app.common.ObcFile;
import elms.util.Operator;
import elms.util.StringUtils;
import elms.util.db.Wrapper;
import jdk.internal.org.xml.sax.SAXNotRecognizedException;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

/**
 * @author arjun
 *
 */
public class AttachmentData {
	static Logger logger = Logger.getLogger(AttachmentData.class.getName());
	
	public static String scanLocalFIles(){
		scanFiles();
		
		return "<h1 style='color:green;'>Successfully Loaded</h1>";
	}
	
	public static void scanFiles() {
		List<Attachment> attachments= getAttachments();
		logger.debug("attachments size : "+attachments.size());
		//Iterator it = FileUtils.iterateFiles(new File(PATH), null, true); //true to iterate Sub Folder
		for (int i=0; i<attachments.size(); i++) {
			logger.debug("FileLocation : "+attachments.get(i).getFile().getFileLocation());
			
			try {
				extractAndSave(attachments.get(i));
			} catch (Exception e) {
				logger.error("Problem while exctracting text of file '" + attachments.get(i).getFile().getFileName() + "'", e);
			}
		}
	}
	public static void extractAndSave(Attachment attachment) throws Exception{
		File file = new File(attachment.getFile().getFileLocation()) ;
		//File file = new File("D:\\EdgeSoft Files\\Fremont\\Fremont_NovEnhancements_Dev.docx") ;
		Boolean dataValue =	getAttachmentData(attachment.getAttachmentId());
		String fileType ="";
		try{
		fileType =Files.probeContentType(file.toPath());
		if(fileType!=null){
			fileType=fileType.trim();
		}
		
		String content = "";
		content = extract(file, fileType);
		if(content!=null && content.equals("FNFE")){
			
		}else{
		if(content!=null && !content.equals("")){
			content=content.trim().replace("'", "");
		}
		fileType=getExt(file.getName());
		if (dataValue) {
			insertFiletoDB(attachment, file.getName(), content,fileType);
		} else if (!dataValue) {
			updateFiletoDB(attachment, file.getName(), content,fileType);
		}
		}
		}catch (Exception e) {
			e.getMessage();
		}
	}

	public static String extract(File file, String fileType){
		String content;
		String fileName = getExt(file.getName());
		try {
			if (fileName != null && !(fileName.contains("pdf") || fileName.contains("xml"))) {
				content = extractBasic(file);
				if (content.equals(""))
					content = extractAutoDetect(file);
			} else {
				content = extractAutoDetect(file);
			}
			if (content.equals(""))
				content = extract(file);
		} catch (FileNotFoundException e) {
		//	logger.error("TIKA was not able to find '" + file.getName() + "'", e);
			content = "FNFE";
		} catch (Exception e) {
		//	logger.error("TIKA was not able to exctract text of file '" + file.getName() + "'", e);
			content = extract(file);
		}
		return content;
	}
	private static String extractBasic(File file)  {
		String tika ="";
		 Tika tikaparse =new Tika();
		try{
		 tika = tikaparse.parseToString(file);
		}catch (TikaException e) {
		e.getMessage();
		}catch (IOException e) {
			e.getMessage();
		}catch (Exception e) {
			e.getMessage();
		}
		return tika;
	}
	public static String extract(File file) {
       String result = null;
       
       try {
    	 /*  ITesseract instance = new Tesseract();
    	 //In case you don't have your own tessdata, let it also be extracted for you
    	   File tessDataFolder = LoadLibs.extractTessResources("tessdata");
    	   //Set the tessdata path
    	   instance.setDatapath(tessDataFolder.getAbsolutePath());
    	   result= instance.doOCR(file);
    	   */
    	   Tesseract tessInst = new Tesseract();
           tessInst.setDatapath(GlobalSearch.getKeyValue("TESSERACT_PATH"));
           result = tessInst.doOCR(file);
       } catch (TesseractException e) {
			logger.error("Tessaract was not able to OCR text of file '" + file.getName() + "'", e);
       }catch (Exception e) {
    	   e.getMessage();
		}catch (Throwable e) {
			logger.error("error : "+e.getMessage());
		}
       return result;
	}

	private static String extractAutoDetect(File file) throws Exception {
		String content = null;
		InputStream fileStream = new FileInputStream(file);
		Parser parser = new AutoDetectParser();
		Metadata metadata = new Metadata();
		BodyContentHandler handler = new BodyContentHandler(-1);

		TesseractOCRConfig config = new TesseractOCRConfig();
		PDFParserConfig pdfConfig = new PDFParserConfig();
		pdfConfig.setExtractInlineImages(true);

		// To parse images in files those lines are needed
		ParseContext parseContext = new ParseContext();
		parseContext.set(TesseractOCRConfig.class, config);
		parseContext.set(PDFParserConfig.class, pdfConfig);
		parseContext.set(Parser.class, parser); // need to add this to make sure recursive parsing happens!
		try {
			parser.parse(fileStream, handler, metadata, parseContext);
			content = handler.toString();

		} catch (IOException  | TikaException e) {
			throw new Exception();
		} finally {
			try {
				fileStream.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		return content;
	}

public static void insertFiletoDB(Attachment attachment,String name,String content,String type) {
	Wrapper db = new Wrapper();
	PreparedStatement psmt = null;
	Connection con = null;
	try{
		String sql = "INSERT INTO ATTACHMENTS_DATA (ID, ATTACH_ID, EXTENSION, FILE_NAME, CONTENT, CREATED_BY, CREATED, UPDATED_BY, UPDATED, ACTIVE) VALUES (SEQUENCE_ATTACHMENTS_DATA_ID.NEXTVAL,?,?,?,?,?,CURRENT_TIMESTAMP,?,CURRENT_TIMESTAMP,?)";
		con = db.getConnectionForPreparedStatementOnly();
		psmt = con.prepareStatement(sql);
		psmt.setInt(1, attachment.getAttachmentId());
		psmt.setString(2,type);
		psmt.setString(3, name);
		psmt.setString(4, content);
		psmt.setInt(5, attachment.getEnteredBy());
		psmt.setInt(6,  attachment.getEnteredBy());
		String active ="Y";
		if(attachment.getDeleted()!=null && attachment.getDeleted().equalsIgnoreCase("N")){
			active ="Y";
		}else if(attachment.getDeleted()!=null && attachment.getDeleted().equalsIgnoreCase("Y")){
			active ="N";
		}
		psmt.setString(7,  active);
		psmt.execute();
		logger.debug("insert attachments data : "+sql);
	}catch (Exception e) {
		e.printStackTrace();
	}finally {
		try{
			if(con!=null){
				con.close();
			}
			if(psmt!=null){
				psmt.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
public static void updateFiletoDB(Attachment attachment,String name,String content,String type) {
	Wrapper db = new Wrapper();
	PreparedStatement psmt = null;
	Connection con = null;
	try{
		String active ="Y";
		if(attachment.getDeleted()!=null && attachment.getDeleted().equalsIgnoreCase("N")){
			active ="Y";
		}else if(attachment.getDeleted()!=null && attachment.getDeleted().equalsIgnoreCase("Y")){
			active ="N";
		}
		StringBuilder	sb = new StringBuilder("UPDATE ATTACHMENTS_DATA SET EXTENSION = ");
			sb.append(Operator.checkString(type)).append(", ");
			sb.append(" FILE_NAME = ");
			sb.append(Operator.checkString(name.trim())).append(", ");
			sb.append(" CONTENT = ");
			sb.append("?").append(", ");
			sb.append(" UPDATED_BY = ");
			sb.append(attachment.getEnteredBy()).append(", ");
			sb.append(" UPDATED = ");
			sb.append("CURRENT_TIMESTAMP ").append(", ");
			sb.append(" ACTIVE = ");
			sb.append(Operator.checkString(active));
			
			sb.append(" WHERE ATTACH_ID = ");
			sb.append(attachment.getAttachmentId());
			logger.debug("update sql : "+sb.toString());
			con = db.getConnectionForPreparedStatementOnly();
			psmt = con.prepareStatement(sb.toString());
			psmt.setString(1, content);
			psmt.execute();
			logger.debug("update attachments data : "+sb.toString());
		//	db.update(sb.toString());
	}catch (Exception e) {
		e.printStackTrace();
	}finally {
		try{
			if(con!=null){
				con.close();
			}
			if(psmt!=null){
				psmt.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}
public static String getExt(String filename) {
	if (!Operator.hasValue(filename)) { return ""; }
	int dot = filename.lastIndexOf(".");
	String e = filename.substring(dot + 1);
	return e;
}

public static String fileType(String filename) {
	if (!Operator.hasValue(filename)) { return ""; }
	String ext = getExt(filename);
	if (!Operator.hasValue(ext)) { return ""; }
	else if (ext.equalsIgnoreCase("tif"))   { return "image"; }
	else if (ext.equalsIgnoreCase("tiff"))   { return "image"; }
	else if (ext.equalsIgnoreCase("gif"))   { return "image"; }
	else if (ext.equalsIgnoreCase("jpg"))   { return "image"; }
	else if (ext.equalsIgnoreCase("jpeg"))  { return "image"; }
	else if (ext.equalsIgnoreCase("png"))   { return "image"; }
	else if (ext.equalsIgnoreCase("bmp"))   { return "image"; }
	else if (ext.equalsIgnoreCase("css"))   { return "style"; }
	else if (ext.equalsIgnoreCase("js"))    { return "script"; }
	else if (ext.equalsIgnoreCase("aiff"))  { return "audio"; }
	else if (ext.equalsIgnoreCase("au"))    { return "audio"; }
	else if (ext.equalsIgnoreCase("mp3"))   { return "audio"; }
	else if (ext.equalsIgnoreCase("avi"))   { return "video"; }
	else if (ext.equalsIgnoreCase("mov"))   { return "video"; }
	else if (ext.equalsIgnoreCase("mpg"))   { return "video"; }
	else if (ext.equalsIgnoreCase("mpeg"))  { return "video"; }
	else if (ext.equalsIgnoreCase("wma"))   { return "video"; }
	else if (ext.equalsIgnoreCase("wmv"))   { return "video"; }
	else if (ext.equalsIgnoreCase("mp4"))   { return "video"; }
	else if (ext.equalsIgnoreCase("m4v"))   { return "video"; }
	else if (ext.equalsIgnoreCase("ra"))   { return "video"; }
	else if (ext.equalsIgnoreCase("ram"))   { return "video"; }
	
	else if (ext.equalsIgnoreCase("html"))  { return "webdoc"; }
	else if (ext.equalsIgnoreCase("xml"))   { return "webdoc"; }
	else if (ext.equalsIgnoreCase("rss"))   { return "webdoc"; }
	else if (ext.equalsIgnoreCase("jsp"))   { return "webdoc"; }
	
	else if (ext.equalsIgnoreCase("pdf"))   { return "document"; }
	else if (ext.equalsIgnoreCase("ppt"))   { return "document"; }
	else if (ext.equalsIgnoreCase("pptx"))   { return "document"; }
	else if (ext.equalsIgnoreCase("doc"))   { return "document"; }
	else if (ext.equalsIgnoreCase("docx"))   { return "document"; }
	else if (ext.equalsIgnoreCase("vdx"))   { return "document"; }
	else if (ext.equalsIgnoreCase("vsd"))   { return "document"; }
	else if (ext.equalsIgnoreCase("xls"))   { return "document"; }
	else if (ext.equalsIgnoreCase("xlsx"))   { return "document"; }
	else if (ext.equalsIgnoreCase("csv"))   { return "document"; }
	else if (ext.equalsIgnoreCase("txt"))   { return "document"; }
	
	else if (ext.equalsIgnoreCase("ai"))   { return "document"; }
	else if (ext.equalsIgnoreCase("mpp"))   { return "document"; }
	else if (ext.equalsIgnoreCase("wks"))   { return "document"; }
	
	else { return "webdoc"; }
}
public static List<Attachment> getAttachments() {
	logger.debug("getAttachments()");
	List<Attachment> attachmentList = new ArrayList<Attachment>();
	Attachment attachment = null;
	RowSet rs =null;
	try {
		Wrapper db = new Wrapper();
		String sql = "select * from ATTACHMENTS WHERE  ATTACH_ID NOT IN (select ATTACH_ID from ATTACHMENTS_DATA WHERE ACTIVE='Y') AND DELETED='N' order by ATTACH_ID desc ";
		logger.debug(sql);

		 rs = db.select(sql);

		while (rs.next()) {
			 attachment = new Attachment();
			 ObcFile obcFile = new ObcFile();
				attachment.setAttachmentId(rs.getInt("attach_id"));
				attachment.setAttachmentLevel(rs.getString("attach_level"));
				attachment.setLevelId(rs.getInt("level_id"));
				attachment.setDescription(rs.getString("description"));
				attachment.setEnteredBy(rs.getInt("created_by"));
				attachment.setDeleted(rs.getString("DELETED"));
				obcFile.setFileName(rs.getString("FILE_NAME"));
				obcFile.setFileLocation(rs.getString("LOCATION"));
				obcFile.setFileSize(StringUtils.i2nbr(rs.getInt("attach_size")));
				obcFile.setCreateDate(rs.getDate("created"));
				obcFile.setStrCreateDate(StringUtils.cal2str(obcFile.getCreateDate()));
				attachment.setFile(obcFile);
			attachmentList.add(attachment);
		}
	} catch (Exception e) {
		logger.error(attachment);
		logger.error(e);
		e.printStackTrace();
	}finally {
		try{
			if(rs!=null){
				rs.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	return attachmentList;
}
public static Boolean getAttachmentData(int attachId) throws Exception {
	logger.info("getAttachmentData(" + attachId + ")");
	Boolean flag = true;
	Wrapper db = new Wrapper();
	RowSet rs =null;
	try {
		String sql = "select ID from ATTACHMENTS_DATA where  ATTACH_ID="+attachId+"";
		logger.debug("sql : "+sql);
		 rs = db.select(sql);
		if (rs!=null && rs.next()) {
			flag= false;
		}
		return flag;
	} catch (Exception e) {
		logger.error(e.getMessage());
		throw e;
	}finally {
		try{
			if(rs!=null){
				rs.close();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
}

	
	
}

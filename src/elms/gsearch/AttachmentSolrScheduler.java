/**
 * 
 */
package elms.gsearch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.common.Attachment;
import elms.app.common.ObcFile;
import elms.control.actions.ApplicationScope;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * @author arjun
 *
 */
public class AttachmentSolrScheduler extends TimerTask{
	static Logger logger = Logger.getLogger(AttachmentSolrScheduler.class.getName()); 

	public AttachmentSolrScheduler(){
		logger.debug("Running the attachment delta scheduler. Time now is "+Calendar.getInstance().getTime());
		run();
		logger.debug("Schdueler completed");
	}
	
	public void run() {
		try{
			
			deltaForAttachmentsLoadInitials();
		}catch (Exception e) {
			e.getMessage();
		}
		
	}
	public static void deltaForAttachmentsLoadInitials(){
		try {
			String attachmentsDeltaIndex= ApplicationScope.map.get("attachmentsDeltaIndex");
			logger.debug("attachmentsDeltaIndex : "+attachmentsDeltaIndex);
			if(attachmentsDeltaIndex!=null && !attachmentsDeltaIndex.equals("") && attachmentsDeltaIndex.equalsIgnoreCase("YES")){
			List<Attachment> attachments= getDeltaAttachments();
			logger.debug("attachments size :"+attachments.size());
			for (int i=0; i<attachments.size(); i++) {
				logger.debug("FileLocation : "+attachments.get(i).getFile().getFileLocation());
			AttachmentData.extractAndSave(attachments.get(i));
			}
			
			GlobalSearch.index(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_ATTACHMENT").replace("query","dataimport?command=delta-import&wt=json&indent=on")); //Delta indexing for attachments
			}
		} catch (Exception e) {
			logger.error("error occured in deltaForAttachmentsLoadInitials : "+e.getMessage());
		}
	}
	
	public static List<Attachment> getDeltaAttachments() {
		logger.debug("getDeltaAttachments()");
		List<Attachment> attachmentList = new ArrayList<Attachment>();
		Attachment attachment = null;
		RowSet rs =null;
		try {
			Wrapper db = new Wrapper();
			String sql = "select * from attachments where DELETED='N' AND UPDATED>=to_date(to_char((select max(UPDATED) from ATTACHMENTS_DATA),'yyyy-mm-dd hh24:mi:ss'),'yyyy-mm-dd hh24:mi:ss')";
			logger.debug(sql);

			 rs = db.select(sql);

			while (rs.next()) {
				 attachment = new Attachment();
				 ObcFile obcFile = new ObcFile();
					attachment.setAttachmentId(rs.getInt("attach_id"));
					attachment.setAttachmentLevel(rs.getString("attach_level"));
					attachment.setLevelId(rs.getInt("level_id"));
					attachment.setDescription(rs.getString("description"));
					attachment.setEnteredBy(rs.getInt("created_by"));
					attachment.setDeleted(rs.getString("DELETED"));
					obcFile.setFileName(rs.getString("FILE_NAME"));
					obcFile.setFileLocation(rs.getString("LOCATION"));
					obcFile.setFileSize(StringUtils.i2nbr(rs.getInt("attach_size")));
					obcFile.setCreateDate(rs.getDate("created"));
					obcFile.setStrCreateDate(StringUtils.cal2str(obcFile.getCreateDate()));
					attachment.setFile(obcFile);
				attachmentList.add(attachment);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				if(rs!=null){
					rs.close();
				}
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		return attachmentList;
	}
	
}

/**
 * 
 */
package elms.gsearch;

import java.util.Calendar;
import java.util.TimerTask;
import org.apache.log4j.Logger;
import elms.control.actions.ApplicationScope;


/**
 * @author arjun
 *
 */
public class SolrScheduler extends TimerTask{
	static Logger logger = Logger.getLogger(SolrScheduler.class.getName()); 

	public final static String SOLR_LOAD_INITIAL_BURBANK =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String SOLR_LOAD_INITIAL_BURBANK_PEOPLE =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PEOPLE").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String SOLR_LOAD_INITIAL_BURBANK_INSPECTION =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String SOLR_LOAD_INITIAL_BURBANK_PLANCHECK =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PLANCHECK").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String SOLR_LOAD_INITIAL_BURBANK_FINANCE =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String SOLR_LOAD_INITIAL_BURBANK_LEDGER =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String SOLR_DELETE_LOAD_INITIAL_BURBANK_INSPECTION =GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION");
	
	public final static String login_username =GlobalSearch.getKeyValue("SOLR_LOGIN_USERNAME");//"admin";
	public final static String login_pass =GlobalSearch.getKeyValue("SOLR_LOGIN_PASSWORD");//"$01r";
	public SolrScheduler(){
		logger.debug("Running the scheduler. Time now is "+Calendar.getInstance().getTime());
		run();
		logger.debug("Schdueler completed");
	}
	public void run() {
		try{

		deltaLoadInitials();
		Thread.sleep(10000);
		
		}catch (Exception e) {
			e.getMessage();
		}
		
	}
	public void deltaLoadInitials(){
		try {
			String deltaIndex= ApplicationScope.map.get("deltaIndex");
			logger.debug("deltaIndex : "+deltaIndex);
			if(deltaIndex!=null && !deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
			logger.debug("Running the scheduler. Time now is "+Calendar.getInstance().getTime());
			GlobalSearch.index(SOLR_LOAD_INITIAL_BURBANK); //Delta indexing for activity
			logger.debug("Running the SOLR_LOAD_INITIAL_BURBANK_PEOPLE. Time now is "+Calendar.getInstance().getTime());
			GlobalSearch.index(SOLR_LOAD_INITIAL_BURBANK_PEOPLE); //Delta indexing for people
			logger.debug("Running the SOLR_LOAD_INITIAL_BURBANK_INSPECTION. Time now is "+Calendar.getInstance().getTime());
			GlobalSearch.index(SOLR_LOAD_INITIAL_BURBANK_INSPECTION); //Delta indexing for inspection
			logger.debug("Running the SOLR_LOAD_INITIAL_BURBANK_PLANCHECK. Time now is "+Calendar.getInstance().getTime());
			GlobalSearch.index(SOLR_LOAD_INITIAL_BURBANK_PLANCHECK); //Delta indexing for plan check
			logger.debug("Running the SOLR_LOAD_INITIAL_BURBANK_FINANCE. Time now is "+Calendar.getInstance().getTime());
			GlobalSearch.index(SOLR_LOAD_INITIAL_BURBANK_FINANCE); //Delta indexing for finance
			logger.debug("Running the SOLR_LOAD_INITIAL_BURBANK_LEDGER. Time now is "+Calendar.getInstance().getTime());
			GlobalSearch.index(SOLR_LOAD_INITIAL_BURBANK_LEDGER); //Delta indexing for ledger
			}
		} catch (Exception e) {
			e.getMessage();
		}
	}

}

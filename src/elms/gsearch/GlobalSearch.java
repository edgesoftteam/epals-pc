package elms.gsearch;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import elms.exception.AgentException;
import elms.util.OBCTimekeeper;
import elms.util.Operator;
import elms.util.StringUtils;
import elms.util.db.Wrapper;


public class GlobalSearch  {
	
	public final static String LOAD_INITIAL_DELTA =getKeyValue("SOLR_LOAD_INITIAL_BURBANK").replace("query","dataimport?command=delta-import&wt=json&indent=on");
	public final static String login_username =getKeyValue("SOLR_LOGIN_USERNAME");//"admin";
	public final static String login_pass =getKeyValue("SOLR_LOGIN_PASSWORD");//"$01r";
	static Logger logger = Logger.getLogger(GlobalSearch.class.getName());

	public static String search(HttpServletRequest map){
		return search(map, false,false);
	}
	
	public static String search(HttpServletRequest map,boolean trends,boolean stats){
		
		String resp ="";
		try{
			
			URIBuilder  o = new URIBuilder(map.getParameter("_url"));
			ArrayList<NameValuePair> oparams = new ArrayList<NameValuePair>();
			String q = (String)map.getParameter("q");
			if(!Operator.hasValue(q)){
				q = "*";
			}
			
			oparams.add(new BasicNameValuePair("q",URLEncoder.encode(q, "UTF-8")));
			oparams.add(new BasicNameValuePair("start",map.getParameter("start")));
			oparams.add(new BasicNameValuePair("rows",reqString(map, "rows")));
			if(stats){
				oparams.add(new BasicNameValuePair("stats","true"));
				oparams.add(new BasicNameValuePair("stats.field",map.getParameter("statsfield")));
			}
		
			oparams.add(new BasicNameValuePair("json.facet", map.getParameter("_facet")));
			oparams.add(new BasicNameValuePair("defType","edismax"));
			oparams.add(new BasicNameValuePair("mm","100"));
			oparams.add(new BasicNameValuePair("indent","on"));
			oparams.add(new BasicNameValuePair("wt",map.getParameter("wt")));
			oparams.add(new BasicNameValuePair("_fq",reqString(map, "_fq")));
			oparams.add(new BasicNameValuePair("sort",Operator.replace(map.getParameter("_sort"), " ", "%20")));
			
			
			oparams.add(new BasicNameValuePair("_filters",reqString(map, "_filters")));
			
			oparams.add(new BasicNameValuePair("_dt",reqString(map, "_dt")));
			oparams.add(new BasicNameValuePair("_customdt",reqString(map, "_customdt")));
			//oparams.add(new BasicNameValuePair("_userId",map.getParameter("_userId")));
			oparams.add(new BasicNameValuePair("_price",reqString(map, "_price")));
			oparams.add(new BasicNameValuePair("fl",map.getParameter("fl")));
			//oparams.add(new BasicNameValuePair("_bookmarktitle",map.getParameter("_bookmarktitle")));
			if(map.getParameter("_view")!=null && map.getParameter("_view").equalsIgnoreCase("viewrow")){
				oparams.add(new BasicNameValuePair("hl.fl","_text_"));
				oparams.add(new BasicNameValuePair("hl","on"));
				oparams.add(new BasicNameValuePair("hl.simple.pre",URLEncoder.encode("<mark>", "UTF-8")));
				oparams.add(new BasicNameValuePair("hl.simple.post",URLEncoder.encode("</mark>", "UTF-8")));
			}
			
			String u = o.toString();
			if(Operator.equalsIgnoreCase(map.getParameter("_bookmark"),"Y")){
				//System.out.println(map.getParameter("_bookmarktitle") + " Title ");
				//System.out.println( "User id " + map.getParameter("_userId"));
					saveBookmark(Integer.parseInt(map.getParameter("_userId")),map.getParameter("_bookmarktitle"),map);
			}
			resp = searchSolr(u, oparams,map.getParameter("method"));
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return resp;
	}
	
	
	public static String reqString(HttpServletRequest map,String param){
		String s = "";
		try{
			if(Operator.hasValue(map.getParameter(param))){
				s = map.getParameter(param);
			}
			
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return s;
	}
	
	public static boolean saveBookmark(int userId,String title,HttpServletRequest map){
		boolean r = false;
		 JSONObject o = new JSONObject();
		 try{
		 
		 
		  
		  
		 // System.out.println("save form called...");
			String sql = null;
			//StringTokenizer st = new StringTokenizer(form.getIds(), ",");
			//StringTokenizer st1 = new StringTokenizer(form.getUserTypes(), ",");
			//while (st.hasMoreElements()) {
			//	st1.hasMoreElements();

		 	o.put("q",map.getParameter("q"));
			o.put("start",map.getParameter("start"));
			o.put("rows","50"); //map.getParameter("rows","50")
		
			o.put("_fq",map.getParameter("_fq"));
			o.put("sort",Operator.replace(map.getParameter("_sort"), " ", "%20"));
			o.put("_filters",map.getParameter("_filters"));
			o.put("_dt",map.getParameter("_dt"));
			o.put("_customdt",map.getParameter("_customdt"));
			o.put("_facetvalues",map.getParameter("_facetvalues"));
			o.put("_price",map.getParameter("_price"));
			o.put("fl",map.getParameter("fl"));
			//}
			sql = "INSERT INTO BOOKMARK (ID, USERS_ID, TITLE, DESCRIPTION, LOCATION, BOOKMARK, SCHEDULE, RECURRENCE_PATTERN, SHARE_ID, EMAIL_TO, EMAIL_ON, ACTIVE, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE) VALUES(SEQUENCE_BOOKMARK_ID.NEXTVAL, "+userId+", '"+title+"','"+title+"', 'Glendale', '"+o.toString()+"', null, null, -1, null, 'Y', 'Y', "+userId+", CURRENT_TIMESTAMP, "+userId+", CURRENT_TIMESTAMP)" ;
			
			//System.out.println("sql..." + sql);
			try {   
				new Wrapper().insert(sql);
			} catch (Exception exp) {
				exp.printStackTrace();
				logger.error("error while saveSubContractor method...:" + exp);
				
			} 
				 
		  
		 }catch(Exception e){
			 logger.error(e.getMessage());
		 }
		 if(Operator.hasValue(o.toString())){
			 	//r =addBookmark(userId, title,map.getString("_location"), o);
		 }
		 return r;
	 } 
	
	
	public static String spell(HttpServletRequest map){
		
		String resp ="";
		try{
			String url = map.getParameter("_url");
			url = Operator.replace(url, "/query", "/spell");
			URIBuilder  o = new URIBuilder(url);
			ArrayList<NameValuePair> oparams = new ArrayList<NameValuePair>();
			oparams.add(new BasicNameValuePair("spellcheck.q",URLEncoder.encode(map.getParameter("q"), "UTF-8")));
			oparams.add(new BasicNameValuePair("indent","on"));
			oparams.add(new BasicNameValuePair("wt",map.getParameter("wt")));
			oparams.add(new BasicNameValuePair("spellcheck.maxCollations","1"));
			oparams.add(new BasicNameValuePair("spellcheck.collateParam.q.op","AND"));
			String u = o.toString();
			resp = searchSolr(u, oparams,map.getParameter("method"));
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return resp;
	}
	
	
	 public static String searchSolr(String url,String format)  {
		 StringBuilder out = new StringBuilder();
		  try {
		  HttpClient httpclient = new DefaultHttpClient();
		  HttpGet httpget = new HttpGet();
		
		  String encoding =  login_username+":"+ login_pass; 
		  byte[] encodedBytes = Base64.encodeBase64(encoding.getBytes());
		  httpget.setHeader("Authorization", "Basic " + new String(encodedBytes));
		  
		
	      String s = url;
	    
	      logger.info(s);
	      URI website = new URI(s);
	   
	      httpget.setURI(website);
		  HttpResponse response = httpclient.execute(httpget);
		  HttpEntity entity = response.getEntity();
	
			  if (entity != null) {
				  	InputStream instream = entity.getContent();
			          	BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
			    	    String newLine = System.getProperty("line.separator");
			    	    String line;
			    	    while ((line = reader.readLine()) != null) {
			    	        out.append(line);
			    	        out.append(newLine);
			    	    }
			    	instream.close();    
			      }
			      
			  }
			  catch(Exception ex){
		    	  out.append("Error while getting response "+ex.getMessage());  
		      } 
			  return out.toString();
	 }
	
	 public static String searchSolr(String url,ArrayList<NameValuePair> params,String format)  {
		  StringBuilder out = new StringBuilder();
		  try {
		  HttpClient httpclient = new DefaultHttpClient();
		  HttpGet httpget = new HttpGet();
		
		  String encoding =  login_username+":"+ login_pass; 
		  byte[] encodedBytes = Base64.encodeBase64(encoding.getBytes());
		  httpget.setHeader("Authorization", "Basic " + new String(encodedBytes));
		  
		 // httpget.setParams(arg0);
		  StringBuilder sb = new StringBuilder();
		  sb.append(url);
	      if(params.size()>0){
	    	  sb.append("?");
	      }
	      String pr = "";
	      String dt = "";
	      String fq = "";
	      String filters="";
	      String customdt = "";
	      
	      for(int i=0;i<params.size();i++){
	    	 logger.info(params.get(i)+"");
	    	 if(params.get(i).getName().equals("_fq")){
	    		 fq = params.get(i).getValue();
	    	 }else  if(params.get(i).getName().equals("_filters")){
	    		 filters = params.get(i).getValue();
	    	 }else  if(params.get(i).getName().equals("_dt")){
	    		 dt = params.get(i).getValue();
	    	 }else  if(params.get(i).getName().equals("_customdt")){
	    		 customdt = params.get(i).getValue();
	    	 }else  if(params.get(i).getName().equals("_price")){
	    		 pr = params.get(i).getValue();
	    	 }else  if(params.get(i).getName().equals("q")){
	    		 
	    		 String q = params.get(i).getValue();
	    		
	    		 if(Operator.hasValue(q) && q.indexOf("@")==-1){
	    			 q = Operator.replace(q,"@","&#64;");
	    		 }else {
	    			 q = URLEncoder.encode(q, "UTF-8");
	    		 }
	    		 
	    		 sb.append(params.get(i).getName()).append("=").append(q);
	    		 sb.append("&");
	    	 }
	    	 else
	    		 if(params.get(i).getName().equals("json.facet")){
	    		// sb.append(params.get(i).getName()).append("=").append(URLEncoder.encode(params.get(i).getValue(), "UTF-8"));
	    		 
	    		 //211{"type" :{ "type":"terms","field":"type","domain": {"excludeTags": "type" } },"status" :{ "type":"terms","field":"status","domain": {"excludeTags": "status" } }}
	    		// %7B%22type%22+%3A%7B+%22type%22%3A%22terms%22%2C%22field%22%3A%22type%22%2C%22domain%22%3A+%7B%22excludeTags%22%3A+%22type%22+%7D+%7D%2C%22status%22+%3A%7B+%22type%22%3A%22terms%22%2C%22field%22%3A%22status%22%2C%22domain%22%3A+%7B%22excludeTags%22%3A+%22status%22+%7D+%7D%7D
	    		/* String j = params.get(i).getValue();
	    		 j = Operator.replace(j, "{", "%7b");
	    		 j = Operator.replace(j, "}", "%7d");
	    		 j = Operator.replace(j,"\"","%22");
	    		 j = Operator.replace(j," ","%20");
	    		
	    		 sb.append(params.get(i).getName()).append("=").append(j);
	    		 sb.append("&");*/
	    			 String j = params.get(i).getValue();
						if(j !=null && !j.equals("")) {
							JSONArray fqarray = new JSONArray(j);
					    	String fqjson = "{";
							for(int k = 0; k < fqarray.length(); k++) {
							      JSONObject objects = fqarray.getJSONObject(k);
							      if(objects != null){
							    	  fqjson += "\""+ objects.getString("field") + "\":" +objects.toString();
							      }
							      if(k < fqarray.length()-1)
							    	  fqjson +=",";
							}
					    	fqjson +="}";
					    	
							fqjson = Operator.replace(fqjson, "{", "%7b");
					    	fqjson = Operator.replace(fqjson, "}", "%7d");
					    	fqjson = Operator.replace(fqjson, "\"", "%22");
					    	fqjson = Operator.replace(fqjson, " ", "%20");
					    	sb.append(params.get(i).getName()).append("=").append(fqjson).append("&");
						}
	    		
	    	 }  	 
	    	 else {
	    	  sb.append(params.get(i));
	    	  sb.append("&");
	    	 }
	      }
	      
	   
	      
	  	ArrayList<String> a = solrescapeFilter(fq, filters);
	    for(String qfp : a){
	    	
	    	String k = qfp;
	    	
	    	if(k.indexOf("display_type")<0){
	    		sb.append("&fq").append("=").append(k).append("&");
	    	}
	    }
	    
	    String []dts = Operator.split(dt,"&");
	    for(String qfp : dts){
	    	sb.append("&fq").append("=").append(Operator.replace(qfp, " ", "%20")).append("&");
	    }
	    String []prs = Operator.split(pr,"&");
	    for(String qfp : prs){
	    	sb.append("&fq").append("=").append(qfp).append("&");
	    }
	    
	   
	    sb.append(doCustomDates(customdt));
	    
	      
	   
	      String s = sb.toString();
	    
	     // System.out.println(s);
	      URI website = new URI(s);
	   
	      httpget.setURI(website);
		  HttpResponse response = httpclient.execute(httpget);
		  HttpEntity entity = response.getEntity();
	
			  if (entity != null) {
				  	InputStream instream = entity.getContent();
			          	BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
			    	    String newLine = System.getProperty("line.separator");
			    	    String line;
			    	    while ((line = reader.readLine()) != null) {
			    	        out.append(line);
			    	        out.append(newLine);
			    	    }
			    	instream.close();    
			      }
			      
			  }
			  catch(Exception ex){
		    	  out.append("Error while getting response "+ex.getMessage());  
		      } 
			  return out.toString();
		  }
	 
	 
	 
	 
	 
	 public static ArrayList<String> solrescapeFilter(String fq, String filter){
			ArrayList<String> a = new ArrayList<String>();
			try{
				String t = filter;
				String type[] = Operator.split(t,",");
				StringBuilder sb = new StringBuilder();
				Matcher m = Pattern.compile("\\[(.*?)\\]").matcher(fq);
				int i =0; 
				
				
				while(m.find()) {
				    sb = new StringBuilder();
				    String g = m.group(1);
				  
				    String s = Operator.replace(g," ","%5C%20");
				    s = Operator.replace(s, "(", "%5C%28");
					
					s = Operator.replace(s, ")", "%5C%29");
					s = Operator.replace(s, "," ,"%20");
					s = Operator.replace(s,"&","%26");
				    
				    sb.append("%7B!tag=").append(type[i]).append("%7D").append(type[i]).append(":(").append(s).append(")");
				    a.add(sb.toString());
				    i = i+1;
				}
				
				
				
			}catch(Exception e){
				logger.error(e.getMessage());
				e.printStackTrace();
			}
			return a;
		}
	 
	 public static String doCustomDates(String customdt){
			
		 StringBuilder sb =new StringBuilder();
		 String []cdts = Operator.split(customdt,",");
		 	for(String qfp : cdts){
		    	logger.info(qfp);
		    	String qf[] = Operator.split(qfp, "-");
		    	sb.append("&fq").append("=").append(qf[0]).append(":").append(getDates(qf[1])).append("&");
		    }
		return sb.toString();   
	 }
	 
	 
	 public static String getDates(String pattern){
		 StringBuilder sb = new StringBuilder();
		 OBCTimekeeper P24 = new OBCTimekeeper();
		 P24.addDay(-1);
		 
		 OBCTimekeeper C1M = new OBCTimekeeper();
		 C1M.setDay(1);
		 
		 OBCTimekeeper C1Y = new OBCTimekeeper();
		 C1Y.setDay(1);
		 C1Y.setMonth(1);
		 
		 OBCTimekeeper F1Y = new OBCTimekeeper();
		 if(F1Y.MONTH()>6){
			 F1Y.setDay(1);
			 F1Y.setMonth(7);
		 }else {
			 F1Y.addYear(-1);
			 F1Y.setDay(1);
			 F1Y.setMonth(7);
		 }
		 
		 
		 if(pattern.equalsIgnoreCase("P24")){
			 sb.append("[").append(P24.getString("YYYY-MM-DD")).append("T00:00:00Z%20TO%20*]");
		 }
		 
		 if(pattern.equalsIgnoreCase("C1M")){
			 sb.append("[").append(C1M.getString("YYYY-MM-DD")).append("T00:00:00Z%20TO%20*]");
		 }
		 if(pattern.equalsIgnoreCase("C1Y")){
			 sb.append("[").append(C1Y.getString("YYYY-MM-DD")).append("T00:00:00Z%20TO%20*]");
		 }
		 
		 if(pattern.equalsIgnoreCase("F1Y")){
			 sb.append("[").append(F1Y.getString("YYYY-MM-DD")).append("T00:00:00Z%20TO%20*]");
		 }
		 
		 return sb.toString();
		 
	 }
	 
	 
	 public static JSONObject viewBookmark(int bookmarkId) throws JSONException{
		 JSONObject t  = new JSONObject();
		 Wrapper db = new Wrapper();
		 ResultSet rs=null;
		 
		 String command = "select * from bookmark where ID="+bookmarkId;
		//System.out.println(command);
		 try {
			 rs=db.select(command);
			while(rs.next()){
				t = new JSONObject(rs.getString("BOOKMARK"));
				t.put("_BTITLE", rs.getString("TITLE"));
				t.put("_BDESCRIPTION", rs.getString("DESCRIPTION"));
			 }
		} catch (Exception e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 //db.clear();
		 
		 return t;
		 
		
		 
	 }
	
	 
	/* public static int userId(String token,String ip) throws JSONException{
		 RequestToken r = new RequestToken();
		 r.setToken(token);
		 r.setIp(ip);
		 String s  = post("lso", r);
		 JSONObject t  = new JSONObject(s);
		 System.out.println(t.getInt("id"));
		 return t.getInt("id");
		 
		
		 
	 }
	 */
	/* public static String post(String entity, RequestToken vo) {
			String r = "";
			try {
				StringBuilder sb = new StringBuilder();
				sb.append(Operator.removeTrailingSlash("lso"));
				sb.append("/");
				sb.append(Operator.removeOpeningAndTrailingSlash(""));
				sb.append("/auth/login");
				String url = sb.toString();
				sb = new StringBuilder();
				sb = null;
				r = post(url, vo.toString());
			}
			catch (Exception e) {}
			return r;
		}
	 
	 
	 public static String post(String url, String json) {
			logger.info("POST URL", url);
			StringBuilder sb = new StringBuilder();
			try {
				HttpClient c = new DefaultHttpClient();
				
				HttpPost p = new HttpPost(url);
				
				p.addHeader("token", "123");
				
				StringEntity input = new StringEntity(json);
				input.setContentType("application/json");
				p.setEntity(input);

				HttpResponse r = c.execute(p);
				HttpEntity entity = r.getEntity();

				if (entity != null) {
					InputStream is = entity.getContent();

					BufferedReader br = new BufferedReader(new InputStreamReader(is));
					String l = System.getProperty("line.separator");
					String line;
					
					while ((line = br.readLine()) != null) {
						sb.append(line);
						sb.append(l);
					}
					is.close();
				}
			}
			catch (Exception e) { logger.error(e); }
			return sb.toString();
		}
	 

	 
	 
	 
	 
	 public static boolean addBookmark(int userId,String title,String location,JSONObject o){
		 boolean r = false;
		 Wrapper db = new Wrapper();
		 
		 StringBuilder sb = new StringBuilder();
		 sb.append("insert into BOOKMARK (USERS_ID, TITLE, LOCATION,BOOKMARK,CREATED_BY,UPDATED_BY) VALUES (");
		 sb.append(userId);
		 sb.append(",");
		 sb.append("'").append(Operator.sqlEscape(title)).append("'");
		 sb.append(",");
		 sb.append("'").append(Operator.sqlEscape(location)).append("'");
		 sb.append(",");
		 sb.append("'").append(Operator.sqlEscape(o.toString())).append("'");
		 sb.append(",");
		 sb.append(userId);
		 sb.append(",");
		 sb.append(userId);
		 sb.append(")");
		
		 r =db.update(sb.toString());
			 
		 
		 
		 db.clear();
		 
		 return r;
	 }
	 
	 
	 
	 public static JSONArray getBookmarks(int userId) throws JSONException{
		 JSONArray a = new JSONArray();
		 Wrapper db = new Wrapper();
		 
		 //String command = "select * from bookmark left outer join  where users_id="+userId;
		 StringBuilder sb = new StringBuilder();
		 sb.append("  select   ");
		 sb.append(" A.* ");
		 sb.append(",");	
		 sb.append(" CONVERT(VARCHAR(10),A.CREATED_DATE,101) as C_CREATED_DATE ");
		 sb.append(",");
		 sb.append(" CONVERT(VARCHAR(10),A.UPDATED_DATE,101) as C_UPDATED_DATE ");
		 sb.append(",");
		 sb.append("   CU.USERNAME AS CREATED ");
		 sb.append(",");
		 sb.append("   UP.USERNAME as UPDATED from BOOKMARK A "); 
		 sb.append(" LEFT OUTER JOIN USERS CU on A.CREATED_BY = CU.ID ");
		 sb.append(" LEFT OUTER JOIN USERS UP on A.UPDATED_BY = UP.ID "); 
		 sb.append(" WHERE A.ACTIVE ='Y' AND A.USERS_ID = ").append(userId).append(" ORDER BY A.TITLE "); 
		 db.query(sb.toString());
		 while(db.next()){
			 JSONObject t  = new JSONObject();
			 t.put("ID", db.getInt("ID"));
			 t.put("TITLE", db.getString("TITLE"));
			 t.put("DESCRIPTION", db.getString("DESCRIPTION"));
			 t.put("LOCATION", db.getString("LOCATION"));
			 t.put("SHARE_ID", db.getInt("SHARE_ID"));
			 t.put("C_CREATED_DATE", db.getString("C_CREATED_DATE"));
			 t.put("C_UPDATED_DATE", db.getString("C_UPDATED_DATE"));
			 t.put("EMAIL_ON", db.getString("EMAIL_ON"));
			 t.put("CREATED", db.getString("CREATED"));
			 t.put("UPDATED", db.getString("UPDATED"));
			 a.put(t);
		 }
		 db.clear();
		 
		 return a;
		 
		
		 
	 }
	 
	
	 
	 
	 public static JSONArray getStaff() throws JSONException{
		 JSONArray a = new JSONArray();
		 Wrapper db = new Wrapper();
		 //String command = "select * from bookmark left outer join  where users_id="+userId;
		 StringBuilder sb = new StringBuilder();
		 sb.append("  select   ");
		 sb.append(" U.ID,A.TITLE,U.FIRST_NAME,U.LAST_NAME,U.USERNAME,U.EMAIL ");
		 sb.append("    from STAFF A "); 
		 sb.append(" LEFT OUTER JOIN USERS U on A.USERS_ID = U.ID ");

		 sb.append(" WHERE A.ACTIVE ='Y' AND U.ID >0 ORDER BY FIRST_NAME "); 
		 db.query(sb.toString());
		 while(db.next()){
			 JSONObject t  = new JSONObject();
			 t.put("ID", db.getInt("ID"));
			 t.put("TITLE", db.getString("TITLE"));
			 t.put("FIRST_NAME", db.getString("FIRST_NAME"));
			 t.put("LAST_NAME", db.getString("LAST_NAME"));
			 t.put("USERNAME", db.getString("USERNAME"));
			 t.put("EMAIL", db.getString("EMAIL"));
			 a.put(t);
		 }
		 db.clear();
		 
		 return a;
	 }
	 public static JSONArray getStaff(int bookmarkId,int shareId) throws JSONException{
		 JSONArray a = new JSONArray();
		 Wrapper db = new Wrapper();
		 
		
		 if(shareId<0){
			 shareId = bookmarkId;
		 }
		 
		 //String command = "select * from bookmark left outer join  where users_id="+userId;
		 StringBuilder sb = new StringBuilder();
		 sb.append("  select   ");
		 sb.append(" U.ID,A.TITLE,U.FIRST_NAME,U.LAST_NAME,U.USERNAME,U.EMAIL,B.ID as BOOKMARK_ID ");
		 sb.append("    from STAFF A "); 
		 sb.append(" LEFT OUTER JOIN USERS U on A.USERS_ID = U.ID ");
		 sb.append(" LEFT OUTER JOIN BOOKMARK B on A.USERS_ID = B.USERS_ID and (B.ID =").append(bookmarkId).append(" OR B.SHARE_ID= ").append(shareId).append(") ");

		 sb.append(" WHERE A.ACTIVE ='Y' AND U.ID >0 ORDER BY FIRST_NAME "); 
		 db.query(sb.toString());
		 while(db.next()){
			 JSONObject t  = new JSONObject();
			 t.put("ID", db.getInt("ID"));
			 t.put("TITLE", db.getString("TITLE"));
			 t.put("FIRST_NAME", db.getString("FIRST_NAME"));
			 t.put("LAST_NAME", db.getString("LAST_NAME"));
			 t.put("USERNAME", db.getString("USERNAME"));
			 t.put("EMAIL", db.getString("EMAIL"));
			 t.put("BOOKMARK_ID", db.getInt("BOOKMARK_ID"));
			 a.put(t);
		 }
		 db.clear();
		 
		 return a;
		 
		
		 
	 }
	 
	 public static boolean shareControl(Cartographer map){
		 boolean result = false;
		 	
		 
		 int bookmarkId = map.getInt("bookmarkId",0);
		 int shareId = map.getInt("shareId",-1);
		 if(bookmarkId>0){
			 if(shareId<0){
				 shareId = bookmarkId;
			 }
			 Wrapper db = new Wrapper();
			 String share = map.getString("share","");
			 String recurrence = map.getString("recurrence_pattern","");
			
			 if(Operator.hasValue(share)){
				 String a[] = Operator.split(share,"|");
				 for(String s: a){
					 String ids[] = Operator.split(s,"$"); 
					 StringBuilder sb = new StringBuilder();
					 
					 sb.append(" insert into bookmark (USERS_ID,TITLE,DESCRIPTION,LOCATION,BOOKMARK");
					 if(Operator.hasValue(recurrence)){
						 sb.append(" ,SCHEDULE,RECURRENCE_PATTERN,EMAIL_TO,EMAIL_ON");
					 }
					 sb.append(" ,SHARE_ID,CREATED_BY,UPDATED_BY) ");
					
					 
					 sb.append(" select ").append(ids[0]).append(",B.TITLE,B.DESCRIPTION,B.LOCATION,B.BOOKMARK");
					 if(Operator.hasValue(recurrence)){
						 sb.append(",getdate(),'").append(Operator.sqlEscape(recurrence)).append("'");	
						 sb.append(",'").append(Operator.sqlEscape(ids[1].trim())).append("@beverlyhills.org','Y'");	 
					 }
					 sb.append(",").append(shareId).append(",B.CREATED_BY,B.UPDATED_BY from bookmark B");
					 sb.append(" left outer join BOOKMARK D on D.USERS_ID= ").append(ids[0]).append(" AND (B.ID=D.SHARE_ID OR B.SHARE_ID= D.SHARE_ID) AND D.ACTIVE='Y' ");
					 sb.append(" where B.id =").append(bookmarkId).append(" and D.ID is null ");
					 //sb.append(" select ").append(ids[0]).append(",TITLE,DESCRIPTION,LOCATION,BOOKMARK,getdate(),'monthly',8,'re','Y',CREATED_BY,UPDATED_BY from bookmark where id =").append(bookmarkId);
					 logger.info(sb.toString());
					 
					 StringBuilder sb2 = new StringBuilder();
					 sb2.append(" UPDATE BOOKMARK SET ACTIVE='N' , UPDATED_DATE = getdate(),UPDATED_BY=").append(map.getInt("userId",0)).append(" WHERE USERS_ID= ").append(ids[0]).append(" AND  SHARE_ID=").append(shareId);
					 result= db.update(sb2.toString());
					 result= db.update(sb.toString());
				 }
			 }
			 
			 db.clear();
		 }
		 
		 return result;
	 }
	 
	 public static boolean deleteBookmark(int bookmarkId){
		 boolean result = false;
		 StringBuilder sb = new StringBuilder();
		 sb.append(" UPDATE BOOKMARK SET ACTIVE='N'  , UPDATED_DATE = getdate() WHERE ID  = ").append(bookmarkId);
		 Wrapper db = new Wrapper();
		 result = db.update(sb.toString());
		 db.clear();
		 return result;
	 }
	  */
	 public static JSONObject emailControl(HttpServletRequest map){
		 int result;
		 	
		 
		 int bookmarkId = Operator.toInt(map.getParameter("bookmarkId"));
		 int shareId = Operator.toInt(map.getParameter("shareId"));
		 int userId = Operator.toInt(map.getParameter("userId"));
		 String emailon = map.getParameter("EMAIL_ON");
		 if(Operator.equalsIgnoreCase(emailon, "on")){	emailon = "Y";}
		 
		 String emailstaff = map.getParameter("emailstaff");
		 emailstaff = Operator.replace(emailstaff, "|", ",");
		 emailstaff=emailstaff!=null?emailstaff:"";
		 
		 String recurrence = map.getParameter("recurrence_pattern");
		 String emailselected = map.getParameter("emailselected");
		 String email ="";
		 if(Operator.hasValue(emailselected)){
			 email = emailselected;//+","+emailstaff;
		 }else {
			 email = emailstaff;
		 }
		 
		 email = Operator.replace(email, ",,", ",");
		 if(email.endsWith(",")){
			 email = email.substring(0,email.length()-1);
		 }
		 
		 StringBuilder sb = new StringBuilder();
		 sb.append(" UPDATE BOOKMARK SET EMAIL_ON= '").append(sqlEscape(emailon)).append("'");
		 sb.append(",");
		 sb.append(" EMAIL_TO = '").append(sqlEscape(email)).append("'");
		 if(Operator.hasValue(recurrence)){
			 sb.append(",");
			 sb.append(" SCHEDULE = CURRENT_TIMESTAMP ");
			 sb.append(",");
			 sb.append(" RECURRENCE_PATTERN = '").append(sqlEscape(recurrence)).append("'");
		 }else {
			 sb.append(",");
			 sb.append(" SCHEDULE = null ");
			 sb.append(",");
			 sb.append(" RECURRENCE_PATTERN = null");
		 }
		 sb.append(",");
		 sb.append(" UPDATED_DATE = CURRENT_TIMESTAMP ");
		 sb.append(",");
		 sb.append(" UPDATED_BY =").append(userId);
		 sb.append(" WHERE ID  =").append(bookmarkId);
		 
		 Wrapper db = new Wrapper();
		 try {
			 //System.out.println("sb : "+sb.toString());
			result = db.update(sb.toString());
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		 JSONObject t = null;
		 try {
			t = viewBookmark(bookmarkId);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		 
		 return t;
	 }
	 
	 
	
	
	 
	 public static JSONObject getBookmark(int bookmarkId) throws Exception{
		 JSONObject t  = new JSONObject();
		 Wrapper db = new Wrapper();
		 ResultSet rs = null;
		 //String command = "select * from bookmark left outer join  where users_id="+userId;
		 StringBuilder sb = new StringBuilder();
		 sb.append("  select   ");
		 sb.append(" A.* ");
		 sb.append(",");	
		 sb.append(" TO_DATE(A.CREATED_DATE,'YYYY/MM/DD') as C_CREATED_DATE ");
		 sb.append(",");
		 sb.append(" TO_DATE(A.UPDATED_DATE,'YYYY/MM/DD') as C_UPDATED_DATE ");
		 sb.append(",");
		 sb.append("   CU.USERNAME AS CREATED ");
		 sb.append(",");
		 sb.append("   UP.USERNAME as UPDATED from BOOKMARK A "); 
		 sb.append(" LEFT OUTER JOIN USERS CU on A.CREATED_BY = CU.USERID ");
		 sb.append(" LEFT OUTER JOIN USERS UP on A.UPDATED_BY = UP.USERID "); 
		 sb.append(" WHERE A.ACTIVE ='Y' AND A.ID = ").append(bookmarkId).append("  "); 
		// System.out.println(sb.toString());
		 rs=db.select(sb.toString());
		 while(rs.next()){
			 t.put("ID", rs.getInt("ID"));
			 t.put("TITLE", StringUtils.nullReplaceWithEmpty(rs.getString("TITLE")));
			 t.put("DESCRIPTION", rs.getString("DESCRIPTION"));
			 t.put("LOCATION", rs.getString("LOCATION"));
			 t.put("SHARE_ID", rs.getInt("SHARE_ID"));
			 t.put("RECURRENCE_PATTERN", StringUtils.nullReplaceWithEmpty(rs.getString("RECURRENCE_PATTERN")));
			 t.put("C_CREATED_DATE", rs.getString("C_CREATED_DATE"));
			 t.put("C_UPDATED_DATE", rs.getString("C_UPDATED_DATE"));
			 t.put("EMAIL_TO", StringUtils.nullReplaceWithEmpty(rs.getString("EMAIL_TO")));
			 t.put("EMAIL_ON", rs.getString("EMAIL_ON"));
			 t.put("CREATED", rs.getString("CREATED"));
			 t.put("UPDATED", rs.getString("UPDATED"));
			
		 }
		rs.close();
		 
		 return t;
		 
		
		 
	 }
	 
	 
	 public static JSONArray getStaff() throws Exception{
		 JSONArray a = new JSONArray();
		 Wrapper db = new Wrapper();
		 //String command = "select * from bookmark left outer join  where users_id="+userId;
		 StringBuilder sb = new StringBuilder();
		 sb.append("  select   ");
		 sb.append(" U.USERID,U.TITLE,U.FIRST_NAME,U.LAST_NAME,U.USERNAME,U.EMAIL_ID ");
		 sb.append("    from USERS U "); 

		 ResultSet rs =db.select(sb.toString());
		 while(rs.next()){
			 JSONObject t  = new JSONObject();
			 t.put("ID", rs.getInt("USERID"));
			 t.put("TITLE", StringUtils.nullReplaceWithEmpty(rs.getString("TITLE")));
			 t.put("FIRST_NAME", StringUtils.nullReplaceWithEmpty(rs.getString("FIRST_NAME")));
			 t.put("LAST_NAME", StringUtils.nullReplaceWithEmpty(rs.getString("LAST_NAME")));
			 t.put("USERNAME", StringUtils.nullReplaceWithEmpty(rs.getString("USERNAME")));
			 t.put("EMAIL", StringUtils.nullReplaceWithEmpty(rs.getString("EMAIL_ID")));
			 a.put(t);
		 }
		 rs.close();
		 
		 return a;
	 }
	 
	 
	 
/*	 public static JSONArray getStaff(int bookmarkId,int shareId) throws JSONException{
		 JSONArray a = new JSONArray();
		 Sage db = new Sage();
		 
		
		 if(shareId<0){
			 shareId = bookmarkId;
		 }
		 
		 //String command = "select * from bookmark left outer join  where users_id="+userId;
		 StringBuilder sb = new StringBuilder();
		 sb.append("  select   ");
		 sb.append(" U.ID,A.TITLE,U.FIRST_NAME,U.LAST_NAME,U.USERNAME,U.EMAIL,B.ID as BOOKMARK_ID ");
		 sb.append("    from STAFF A "); 
		 sb.append(" LEFT OUTER JOIN USERS U on A.USERS_ID = U.ID ");
		 sb.append(" LEFT OUTER JOIN BOOKMARK B on A.USERS_ID = B.USERS_ID and (B.ID =").append(bookmarkId).append(" OR B.SHARE_ID= ").append(shareId).append(") ");

		 sb.append(" WHERE A.ACTIVE ='Y' AND U.ID >0 ORDER BY FIRST_NAME "); 
		 db.query(sb.toString());
		 while(db.next()){
			 JSONObject t  = new JSONObject();
			 t.put("ID", db.getInt("ID"));
			 t.put("TITLE", db.getString("TITLE"));
			 t.put("FIRST_NAME", db.getString("FIRST_NAME"));
			 t.put("LAST_NAME", db.getString("LAST_NAME"));
			 t.put("USERNAME", db.getString("USERNAME"));
			 t.put("EMAIL", db.getString("EMAIL"));
			 t.put("BOOKMARK_ID", db.getInt("BOOKMARK_ID"));
			 a.put(t);
		 }
		 db.clear();
		 
		 return a;
		 
		
		 
	 }
*/	 

	 public static int shareControl(int bookmarkId,int shareId , int userId) throws Exception{
		 int result = 0;  
		
		 if(bookmarkId>0){
			 if(shareId<=0){
				 shareId = bookmarkId;
			 }
			 Wrapper db = new Wrapper();
			 //String share = map.getString("share","");
			 //String recurrence = map.getString("recurrence_pattern","");
			
/*			 if(Operator.hasValue(share)){
				 String a[] = Operator.split(share,"|");
				 for(String s: a){
					 String ids[] = Operator.split(s,"$"); */
					 StringBuilder sb = new StringBuilder();
					 
					 sb.append(" insert into bookmark (ID,USERS_ID,TITLE,DESCRIPTION,LOCATION,BOOKMARK");
					// if(Operator.hasValue(recurrence)){
						 sb.append(" ,SCHEDULE,RECURRENCE_PATTERN,EMAIL_TO,EMAIL_ON");
					 //}   
					 sb.append(" ,SHARE_ID,CREATED_BY,UPDATED_BY) ");
					
					 
					 sb.append(" select SEQUENCE_BOOKMARK_ID.NEXTVAL,").append(userId).append(",B.TITLE,B.DESCRIPTION,B.LOCATION,B.BOOKMARK");
					// if(Operator.hasValue(recurrence)){
						 sb.append(",CURRENT_TIMESTAMP");
						 //.append(Operator.sqlEscape(recurrence)).append("'");	
						 sb.append(",RECURRENCE_PATTERN,EMAIL_TO").append(",'Y'");	 
					 //}     
					 sb.append(",").append(shareId).append(",CREATED_BY,UPDATED_BY FROM BOOKMARK B ");
					 sb.append(" WHERE ID= ").append(bookmarkId);
					 //.append(" AND (B.ID=D.SHARE_ID OR B.SHARE_ID= D.SHARE_ID) AND D.ACTIVE='Y' ");
					 //sb.append(" where B.id =").append(bookmarkId).append(" and D.ID is null ");
					 //sb.append(" select ").append(ids[0]).append(",TITLE,DESCRIPTION,LOCATION,BOOKMARK,getdate(),'monthly',8,'re','Y',CREATED_BY,UPDATED_BY from bookmark where id =").append(bookmarkId);
					 //System.out.println(sb.toString());
					 
					/* StringBuilder sb2 = new StringBuilder();
					 sb2.append(" UPDATE BOOKMARK SET ACTIVE='N' , UPDATED_DATE = getdate(),UPDATED_BY=").append(map.getInt("userId",0)).append(" WHERE USERS_ID= ").append(ids[0]).append(" AND  SHARE_ID=").append(shareId);
					 result= db.update(sb2.toString());*/
					 result= db.insert(sb.toString());
				 }
			// }
			 
			
		// }
		 
		 return result;
	 }
	 
	 public static int bookmarkEdit(int bookmarkId ,int shareId,int userId,String title,String desc) throws Exception{
		 int result = 0;
		 	
		 StringBuilder sb = new StringBuilder();
		 sb.append(" UPDATE BOOKMARK SET TITLE= '").append(sqlEscape(title)).append("'");
		 sb.append(",");
		 sb.append(" DESCRIPTION = '").append(sqlEscape(desc)).append("'");
		 sb.append(",");
		 sb.append(" UPDATED_DATE = CURRENT_TIMESTAMP ");
		 sb.append(",");
		 sb.append(" UPDATED_BY =").append(userId);
		 sb.append(" WHERE ID  =").append(bookmarkId);
		// System.out.println(sb.toString());
		 Wrapper db = new Wrapper();
		 result = db.update(sb.toString());

		 
		 return result;
	 }
	
	 
		
		/**
		 * Gets the list of sub project sub types
		 * 
		 * @return
		 */
		public static List getBookmarkList(int userId) throws Exception {
			logger.debug("getBookmarkList()");
            String sql="";
			List<Bookmark> bList = new ArrayList<Bookmark>();
			Bookmark bookmark = null;

			try {
				sql = "SELECT * FROM BOOKMARK B JOIN USERS U on B.CREATED_BY=U.USERID  where B.USERS_ID= "+userId+" ORDER BY description";
				logger.debug(sql);   
	    
				RowSet rs = new Wrapper().select(sql);

				while (rs.next()) {
					bookmark= new Bookmark();
					bookmark.setId(rs.getInt("ID"));
					bookmark.setTitle(StringUtils.nullReplaceWithBlank(rs.getString("TITLE")));
					bookmark.setDescription(StringUtils.nullReplaceWithBlank(rs.getString("DESCRIPTION")));
					bookmark.setBookmark(StringUtils.nullReplaceWithBlank(rs.getString("BOOKMARK")));
					bookmark.setCreated(StringUtils.nullReplaceWithBlank(rs.getString("FIRST_NAME"))+ " "+ StringUtils.nullReplaceWithBlank(rs.getString("LAST_NAME")));
					bookmark.setCreatedDate(StringUtils.nullReplaceWithBlank(rs.getString("CREATED_DATE")));
				    bList.add(bookmark);
				}

				rs.close();

				return bList;
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
		}
		
		
		// Delete bookmark

		public static void deleteBookmark(int bookmarkId) throws AgentException {

			
				Wrapper db = new Wrapper();
				String deleteSql = "";
				logger.debug("In deleteBookmark");

				try {
					deleteSql = "delete from BOOKMARK where Id=" + bookmarkId ;
					db.update(deleteSql);
					
				} catch (Exception e) {

					logger.error("Error in deleting fire Block record", e);
					throw new AgentException("", e);
				}

			
		}
		public static boolean index(String url) {
			updateGlobalSearch(url);
			return true;
		}
		public static void updateGlobalSearch(String url) {
			logger.info(url);
			final String u = url;
			// start process in new thread
			try {
				new Thread(new Runnable() {
					public void run() {
						try {
							// GlobalSearch.index(GlobalSearch.INSPECTIONS_DELTA);
							indexThread(u);
						} catch (Exception e) {
							logger.error(e.getMessage());
						}
					}
				}).start();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}
		public static boolean indexThread(String url) {
			boolean res = true;
			boolean r = true;
			logger.info(url);
			int i = 0;
			while (r) {
				String s = getIndexStatus(url);
				if (!Operator.hasValue(s)) {
					r = false;
				} else if (!Operator.equalsIgnoreCase(s, "busy") && i > 1) {
					r = false;
				}
				i++;

				if (i == 900) {
					break;
				}
				logger.info(i);
			}
			return res;
		}
		public static String getIndexStatus(String url) {
			String r = "";
			try {
				logger.info(url);
				HttpClient httpclient = new DefaultHttpClient();
				HttpGet httpget = new HttpGet();
				
				String encoding = login_username + ":" + login_pass; // administrator / solr
				byte[] encodedBytes = Base64.encodeBase64(encoding.getBytes());
				httpget.setHeader("Authorization", "Basic " + new String(encodedBytes));

				httpget.addHeader("content-type", "application/json");
				URI website = new URI(url);
				httpget.setURI(website);
				HttpResponse response = httpclient.execute(httpget);
				HttpEntity entity = response.getEntity();
				StringBuilder out = new StringBuilder();
				if (entity != null) {
					InputStream instream = entity.getContent();
					BufferedReader reader = new BufferedReader(new InputStreamReader(instream));
					String newLine = System.getProperty("line.separator");
					String line;
					while ((line = reader.readLine()) != null) {
						out.append(line);
						out.append(newLine);
					}
					instream.close();
				}

				String content = out.toString();
				JSONObject o = new JSONObject(content);
				r = o.getString("status");
				logger.info(r);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
			return r;
		}
		public static String getKeyValue(String keyName) {
			String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString(keyName);
			String value="";
			RowSet rs = null;
			try{
				rs = new Wrapper().select(sql);
			if (rs!=null && rs.next()) {
				value = rs.getString(1);
			}
			}catch (Exception e) {
				e.getMessage();
			}finally {
				try{
					if(rs!=null){
						rs.close();
					}
				}catch (Exception e) {
					e.getMessage();
				}
			}
			return value;
		}
		 /**
		 * Escape single quotes in specified string to remove possibility of sql injections.
		 *
		 * @param str  string to search and replace, may be null
		 * @return the string with any replacements processed
		 */
		public static String sqlEscape(String str) {
			if (str == null) { return ""; }
			String result = Operator.replaceSmartChars(str);
			result = Operator.replace(result,"'","''");
			return result;
		}
 
		public static void deleteLoadInitials(String url,String key,String id){
			try {
				logger.debug("deleteLoadInitials. Time now is "+Calendar.getInstance().getTime());
				String myUrl = url.replace("query","update?stream.body=" + URLEncoder.encode("<delete><query>"+key+":"+id+"</query></delete>", "UTF-8")+"&commit=true");
				GlobalSearch.index(myUrl);
			
			} catch (Exception e) {
				e.getMessage();
			}
		}
}

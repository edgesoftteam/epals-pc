package elms.extract;


import org.apache.log4j.Logger;

import javax.sql.RowSet;
import java.sql.*;
import java.util.ResourceBundle;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.File;

/**
 * This file extracts EPALS payment information.
 * version 2.17
 * Created: October 22,2006
 * 10/2006: Added cash/check/credit card for the reverse payments
 * 07/2007: Added the 'reverse' payment capabilities to the extract and added the 'Activity number' to the extract.
 * 08/2008 : Enabled payments from planning and License and code to be extracted.
 * 07/2010: Added PW department to the query so that PW payments will also be pulled.
 * @author Edgesoft Inc
 */
public class PayPalExtract {

    /**
     * The logger
     */
    static Logger logger = Logger.getLogger(PayPalExtract.class.getName());

    public static String DATABASE_DRIVER = "";
    public static String DATABASE_URL = "";
    public static String DATABASE_USER = "";
    public static String DATABASE_PASSWORD = "";
    public static String FILENAME_PREFIX = "";
    public static String FILE_LOCATION = "";
    public static ResourceBundle obcOracleExtract = null;


    /**
     * The Main program for the extract
     */
    public static void main(String args[]) {
        try {
            logger.debug("Starting the year end maintenance program");

            //read properties
            readProperties();

            //initialize the year end maintenance program
            PayPalExtract obcOracleExtract = new PayPalExtract();
            obcOracleExtract.createFile();

            logger.debug("Completed the year end maintenance program");

        } catch (Exception e) {
            logger.error("Error occured during import program " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * The create file execute program
     * @throws Exception
     * file format is as follows: Online Trans ID | GL Account | Amount | Mode of Payment | Reverse Flag | Transaction ID | Date | Permit Number | Description | Address | City | State| Zip | Phone | Check #
     * example file name : epalsext05082006.txt
     * example contents : 001.CD000.41001.0000.000000|121.0|check||340844|09/02/2009|BL0905419|MIAMI FITNESS |1611 W, VERDUGO AVE.|BURBANK|CA|91506|8185667547|1587
     */
    public void createFile() throws Exception {
        logger.debug("execute()");
        Connection connection = null;
        Statement selectStatement = null;
        ResultSet rs = null;
        String sql = "";
        String today = "";
        String todayFileName = "";
        String fileName = "";
        FileWriter oracleExportFile = null;
        PrintWriter fileOutput = null;
        try {
            //========get connection
            connection = getConnection();
            selectStatement = connection.createStatement();
            //get today's date
            Calendar cal = Calendar.getInstance();
            todayFileName = cal2strFileName(cal);
            logger.debug("Today's filename date is " + todayFileName);
            today = cal2str(cal);
            logger.debug("Today's date is " + today);
            //create the file with the filename.
            fileName = FILE_LOCATION + FILENAME_PREFIX + todayFileName + ".txt";
            File file = new File(fileName);
            oracleExportFile = new FileWriter(file);
            fileOutput = new PrintWriter(oracleExportFile);
            sql = "select rownum *10 as RW, pd.fee_account, pd.amnt as AMNT, p.pymnt_method, p.ACCNT_NO, pd.fee_id, p.pymnt_dt, a.act_nbr, (xu.firstname || ' ' || xu.lastname) as name , xu.address, xu.city, xu.state, xu.zip, xu.phone, p.onlinetxnid from payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id join activity a on pd.act_id=a.act_id left join ext_user xu ON  p.people_id = xu.ext_user_id Where ((to_char(sysdate, 'D') = '2' and to_char(p.pymnt_dt, 'yyyy/mm/dd') between TO_CHAR(sysdate -5, 'yyyy/mm/dd') AND TO_CHAR(sysdate -3, 'yyyy/mm/dd')) Or (to_char(sysdate, 'D') in ('3','4','5','6')and to_char(p.pymnt_dt, 'yyyy/mm/dd') = TO_CHAR(sysdate -3, 'yyyy/mm/dd'))) And p.dept_code in ('BS','PL','LC','PK','PW') and p.pymnt_method in('cash','check','creditcard','reverse') and p.onlinetxnid is not null UNION ALL select (rownum *10)+1 as RW, '001.PW22A.62830.1001' as fee_account, -(ROUND((pd.amnt * .029),2)+.3) as AMNT, p.pymnt_method,  p.ACCNT_NO, 123456 as fee_id, p.pymnt_dt, '000000000' as act_nbr, 'PayPal' as name , 'NA' as address, 'NA' as city, 'NA' as state, 'NA' as zip, 'NA' as phone, p.onlinetxnid  from payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id join activity a on pd.act_id=a.act_id left join ext_user xu ON  p.people_id = xu.ext_user_id Where ((to_char(sysdate, 'D') = '2' and to_char(p.pymnt_dt, 'yyyy/mm/dd') between TO_CHAR(sysdate -5, 'yyyy/mm/dd') AND TO_CHAR(sysdate -3, 'yyyy/mm/dd')) Or (to_char(sysdate, 'D') in ('3','4','5','6')and to_char(p.pymnt_dt, 'yyyy/mm/dd') = TO_CHAR(sysdate -3, 'yyyy/mm/dd'))) And p.dept_code in ('BS','PL','LC','PK','PW') and p.pymnt_method in('cash','check','creditcard','reverse') and p.onlinetxnid is not null order by rw";
            logger.debug(sql);
            rs = selectStatement.executeQuery(sql);
            while (rs.next()) {
                String onlinetxnid = rs.getString("onlinetxnid");
                String accountNumber = rs.getString("fee_account");
                double amount = rs.getDouble("amnt");
		String paymentMethod = "";
		if (onlinetxnid != null) {
                        paymentMethod = "paypal";
                } else {
                        paymentMethod = rs.getString("pymnt_method");
                }
                int feeId = rs.getInt("fee_id");
                String paymentDate = date2str(rs.getDate("pymnt_dt"));
                String act_nbr = rs.getString("act_nbr");
                String name             = rs.getString("name");
		String address = rs.getString("address");
				String city             = rs.getString("city");
				String state            = rs.getString("state");
				String zip              = rs.getString("zip");
				String phone            = rs.getString("phone");
                String checkNo          = rs.getString("ACCNT_NO");

                String row = "";
                row = ""+ accountNumber + "|" //account number 
                        + amount + "|" // amount
                        + paymentMethod + "|" // payment method
                        + getOriginalPaymentMethod(feeId, act_nbr, today, paymentMethod,amount) + "|" // original payment method for reverse payments
                        + feeId + "|" // fee id
                        + paymentDate + "|" // Date
                        + act_nbr + "|" //activity number
                        + name + "|"    //payee name
						+ address + "|"    //payee address
						+ city + "|"    //city
						+ state + "|"   //state
						+ zip  + "|" //zip
                        + phone + "|"
                        + checkNo + "|" //Check 
                        + onlinetxnid; // onlinetxnid #
                logger.debug(row);
                fileOutput.println(row);
            }


        } catch (Exception e) {
            logger.error("Error occured during execute of oracle extract file " + e.getMessage());
            throw new Exception(e.getMessage());
        } finally {
            try {
                if (fileOutput != null) fileOutput.close();
                if (rs != null) rs.close();
                if (selectStatement != null) selectStatement.close();
                if (connection != null) connection.close();
            } catch (Exception ignored) {
                logger.error("exception occured while closing handles to database " + ignored.getMessage());
            }
        }
    }

    /**
     * gets the database connection
     * @return
     * @throws Exception
     */
    public Connection getConnection() throws Exception {
        logger.info("getConnection()");
        Connection connection = null;

        try {
            logger.debug("Trying to load the db driver ...");
            logger.debug("Database Driver is " + DATABASE_DRIVER);
            Class.forName(DATABASE_DRIVER).newInstance();
            logger.debug("Loaded driver successfully...trying to get a connection");
            connection = DriverManager.getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD);
            logger.debug("Obtained database connection successfully");

            return connection;
        } catch (Exception e) {
            logger.fatal("getConnection: Exception thrown while getting database connection " + e.getMessage());
            throw new Exception(e.getMessage());
        }
    }

    /**
     * get original payment -- for the reverse transaction.
     * @param feeId
     * @param activityNumber
     * @param today
     * @return
     * @throws Exception
     */
    public String getOriginalPaymentMethod(int feeId, String activityNumber, String today, String paymentMethod,double amount) throws Exception {
        logger.info("getOriginalPaymentMethod(" + feeId + "," + activityNumber + "," + today + "," + paymentMethod + ","+amount+")");

        String originalPaymentMethod = "";
        if (paymentMethod.equals("reverse")) {
            try {
                String sql = "select p.onlinetxnid, p.pymnt_method from payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id join activity a on pd.act_id=a.act_id where to_date(p.pymnt_dt)=to_date('" + today + "','MM/DD/YYYY') and p.dept_code in ('BS','PK','PL','LC','PW') and p.onlinetxnid is not null and pd.fee_id=" + feeId + " and a.act_nbr='" + activityNumber + "' and pd.amnt="+Math.abs(amount)+" and p.pymnt_method!='reverse'";
                logger.info(sql);
                Connection con = getConnection();
                Statement stmt = con.createStatement();
                ResultSet rs = stmt.executeQuery(sql);
                if (rs.next()) {
                    originalPaymentMethod = rs.getString("pymnt_method");
                }

            } catch (Exception e) {
                logger.error("exception occured while getOriginalPaymentMethod " + e.getMessage());
                throw new Exception("Problem occured while getting the original payment " + e.getMessage());
            }
        }
        return originalPaymentMethod;

    }


    /**
     * Utility method: converts date to java string
     */

    public static String date2str(Date d) {

        String strDate = null;
        if (d != null) {

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                strDate = formatter.format(d);
            } catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strDate;
    }


    /**
     * Utility method: converts java calendar to formatted java string used for the file name.
     */
    public static String cal2strFileName(Calendar c) {

        String strCalendar = "";
        if (c != null) {

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("MMddyy");
                strCalendar = formatter.format(c.getTime());
            } catch (Exception e) {

                logger.error(e.getMessage());
            }
        }
        return strCalendar;
    }

    /**
     * Utility method: converts java calendar to formatted java string used as an entry in the file.
     */
    public static String cal2str(Calendar c) {

        String strCalendar = "";
        if (c != null) {

            try {

                SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
                strCalendar = formatter.format(c.getTime());
            } catch (Exception e) {

                logger.error(e.getMessage());
            }
        }

        return strCalendar;
    }


    /**
     * Reads the properties from the obc properties file.
     *
     */
    static void readProperties() throws Exception {
        logger.info("readProperties");

        ResourceBundle obcOracleExtractProperties = ResourceBundle.getBundle("PayPalExtract");
        DATABASE_DRIVER = obcOracleExtractProperties.getString("DATABASE_DRIVER");
        DATABASE_URL = obcOracleExtractProperties.getString("DATABASE_URL");
        DATABASE_USER = obcOracleExtractProperties.getString("DATABASE_USER");
        DATABASE_PASSWORD = obcOracleExtractProperties.getString("DATABASE_PASSWORD");

        FILENAME_PREFIX = obcOracleExtractProperties.getString("FILENAME_PREFIX");
        FILE_LOCATION = obcOracleExtractProperties.getString("FILE_LOCATION");

    }

}

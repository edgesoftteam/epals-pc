package elms.security;

import java.io.Serializable;

public class Role implements Serializable {

	public int roleId;
	public String description;

	public Role() {
	}

	public Role(int roleId, String description) {
		this.roleId = roleId;
		this.description = description;
	}

	/**
	 * Access method for the description property.
	 * 
	 * @return the current value of the password property
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the desc property.
	 * 
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the roleId
	 * 
	 * @return Returns a int
	 */
	public int getRoleId() {
		return roleId;
	}

	/**
	 * Sets the roleid
	 */
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

}
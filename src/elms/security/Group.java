package elms.security;

import java.io.Serializable;

public class Group implements Serializable {

	public int groupId;
	public String name;
	public String description;
	private int moduleId;

	

	public Group() {
	}

	public Group(int groupId, String name) {
		this.groupId = groupId;
		this.name = name;
		this.description = "";
	}

	public Group(int groupId, String name, String description) {
		this.groupId = groupId;
		this.name = name;
		this.description = description;
	}

	/**
	 * Access method for the description property.
	 * 
	 * @return the current value of the password property
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the desc property.
	 * 
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the groupId.
	 * 
	 * @return int
	 */
	public int getGroupId() {
		return groupId;
	}

	/**
	 * Sets the groupId.
	 * 
	 * @param groupId
	 *            The groupId to set
	 */
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	/**
	 * Returns the name.
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
}
package elms.util.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.naming.InitialContext;
import javax.sql.DataSource;
import javax.sql.RowSet;

import org.apache.log4j.Logger;

import sun.jdbc.rowset.CachedRowSet;
import elms.exception.IdGenerationException;
import elms.util.StringUtils;

public class Wrapper implements java.io.Serializable {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(Wrapper.class.getName());

	public static ResourceBundle elmsProperties;

	public static String DATASOURCE = "";
	public static String CITY_NAME = "";
	public static String PRIVATE_LABEL = "";
	public static String REPORTS_SERVER = "";
	//values from lkup_system table
	public static String KEY_VALUE = "UNDEFINED";

	protected Connection batchCon;
	protected Statement stmt;
	int[] recordseffected;
	int results;

	public Wrapper() {
		try {
			if (elmsProperties == null) {
				readProperties();
			}
		} catch (Exception e) {
			logger.fatal("Exception occured while reading properties " + e.getMessage());
		}
	}

	/**
	 * Reads the properties from the elms properties file.
	 * 
	 */
	public static void readProperties() throws Exception {
		logger.info("readProperties");

		elmsProperties = getResourceBundle();

		DATASOURCE = elmsProperties.getString("DATASOURCE").trim();
		REPORTS_SERVER = elmsProperties.getString("REPORTS_SERVER").trim();

	}

	public static String getCityName() throws Exception {
		String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString("CITY_NAME");
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			CITY_NAME = rs.getString(1);
		}
		return CITY_NAME;
	}

	public static String getPrivateLabel() throws Exception {
		String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString("PRIVATE_LABEL");
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			PRIVATE_LABEL = rs.getString(1);
		}
		return PRIVATE_LABEL;
	}

	public static String getReportsServer() throws Exception {
		return REPORTS_SERVER;
	}

	/**
	 * Get the resource bundle
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ResourceBundle getResourceBundle() throws Exception {
		logger.info("getResourceBundle()");

		try {
			elmsProperties = ResourceBundle.getBundle("elms-config");

			return elmsProperties;
		} catch (MissingResourceException e) {
			throw new Exception("unable to read property files " + e.getMessage());
		}
	}

	/**
	 * Please use this only for getting connections for prepared statement only. for all other sql query execution use the select(), insert() and update() public methods
	 * 
	 * @return
	 * @throws Exception
	 */
	public Connection getConnectionForPreparedStatementOnly() throws Exception {
		return getConnection();
	}

	/**
	 * Obtain a connection from the Oracle data source
	 * 
	 * @return
	 * @throws Exception
	 */
	public Connection getConnection() throws Exception {

		Connection localCon = null;
		javax.sql.DataSource ds = null;

		try {
			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup(DATASOURCE);
			localCon = ds.getConnection();

			return localCon;
		} catch (Exception e) {
			logger.error("Unable to get a datasource connection:" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Return the batch connection.
	 * 
	 * @throws Exception
	 */
	public void returnBatchConnection() throws Exception {
		logger.info("returnBatchConnection()");

		try {
			if (batchCon != null) {
				batchCon.close();
			}
		} catch (Exception e) {
			logger.error("Wrapper.java:Not able to close connection" + e.getMessage());
		}
	}

	public RowSet select(String sql) throws Exception {
		Connection localCon = null;
		Statement statement = null;
		ResultSet results = null;
		if ((sql == null) || (sql.length() <= 0)) {
			logger.error("Invalid sql statement");

			return null;
		}

		CachedRowSet cachedRowSet = null;

		try {
			localCon = getConnection();
			statement = localCon.createStatement();
			results = statement.executeQuery(sql);
			cachedRowSet = new sun.jdbc.rowset.CachedRowSet();
			cachedRowSet.populate(results);
			return cachedRowSet;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			try {
				if (results != null) {
					results.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (localCon != null) {
					localCon.close();
				}
			} catch (Exception e) {
				logger.error("Exception occured while closing results and connections");
				throw e;
			}
		}

	}

	/**
	 * Insert data to the database using the SQL statement
	 * 
	 * @param sql
	 * @return int
	 * @throws Exception
	 */
	public int insert(String sql) throws Exception {
		results = 0;
		Connection localCon = null;
		PreparedStatement statement = null;
		if ((sql == null) || (sql.length() <= 0)) {
			logger.warn("invalid sql statement");
		}

		try {
			localCon = getConnection();
			statement = localCon.prepareStatement(sql);
			results = statement.executeUpdate();

			return results;
		} catch (Exception e) {
			logger.error("Error:" + sql + ":" + e.getMessage());
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}
				if (localCon != null) {
					localCon.close();
				}

			} catch (Exception e) {
				// ignored
			}
		}
	}

	/**
	 * Insert data to the database using the SQL statement
	 * 
	 * @param sql
	 * @return int
	 * @throws Exception
	 */
	public int update(String sql) throws Exception {
		return this.insert(sql);
	}

	/**
	 * Get the next id from the database for the corresponding id name.
	 * 
	 * @param idName
	 * @return
	 */
	public synchronized int getNextId(String idName) throws Exception {
		logger.info("getNextId(" + idName + ")");

		int idValue = -1;
		RowSet rs = null;

		try {
			String sql = "select idvalue from nextid where upper(idname)=" + StringUtils.checkString(idName.toUpperCase());
			logger.debug("select ID Value is " + sql);
			rs = select(sql);

			if (rs.next()) {
				idValue = rs.getInt(1) + 1;
				sql = "update nextid set idvalue = " + idValue + " where upper(idname) =" + StringUtils.checkString(idName.toUpperCase());
				logger.debug("select ID Value is " + sql);
				insert(sql);
			}

			return idValue;
		} catch (Exception ex) {
			logger.error("Unable to generate new ID " + ex.getMessage());
			throw new IdGenerationException("Unable to create new ID " + ex.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Begins the transaction for the batch connection statement
	 */
	public void beginTransaction() {
		try {
			batchCon = null;
			batchCon = getConnection();
			stmt = batchCon.createStatement();
			batchCon.setAutoCommit(false);
		} catch (Exception e) {
			logger.error("Failed beginTransaction .." + e.getMessage());
		}
	}

	/**
	 * Commits the transaction for the batch connection statement
	 */
	public void commitTransaction() {
		try {
			batchCon.commit();
		} catch (Exception e) {
			logger.error("commit Failed " + e.getMessage());
		}
	}

	/**
	 * Rollbacks the transaction for the batch connection statement
	 */
	public void rollbackTransaction() {
		try {
			batchCon.rollback();
		} catch (Exception e) {
			logger.error("Rollback Failed " + e.getMessage());
		}
	}

	/**
	 * Add the batch for transaction for the batch connection statement
	 */
	public void addBatch(String sql) {
		try {
			stmt.addBatch(sql);
			logger.info(sql);
		} catch (Exception e) {
			logger.error("rollback Failed");
		}
	}

	/**
	 * Clears the batch
	 */
	public void clearBatch() {
		try {
			stmt.clearBatch();
			logger.info("The batch of Sqls which are added were cleared");
		} catch (Exception e) {
			logger.error("Exception in clearBatch method");
		}
	}

	/**
	 * Executes the batch for
	 */
	public int[] executeBatch() throws Exception {
		try {
			recordseffected = stmt.executeBatch();
			batchCon.commit();
		} catch (SQLException ex) {
			try {
				batchCon.rollback();
				logger.error("RollBack performed");
				throw new Exception("Rollback performed " + ex.getMessage());
			} catch (Exception e1) {
				logger.warn("RollBack failed");
				throw new Exception("Rollback failed " + ex.getMessage());
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Exception("Exception occured while executing batch " + e.getMessage());
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}

				returnBatchConnection();
			} catch (Exception e1) {
				logger.error("Exception occured while closing handles " + e1.getMessage());
			}
		}

		return recordseffected;
	}
	
	/**
	 * These are keys and values from lookup system table
	 * @param keyName
	 * @return
	 * @throws Exception
	 */
	public static String getKeyValue(String keyName) throws Exception {
		String sql = "SELECT VALUE FROM LKUP_SYSTEM WHERE NAME=" + StringUtils.checkString(keyName);
		logger.info(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			KEY_VALUE = rs.getString(1);
		}
		return KEY_VALUE;
	}

}

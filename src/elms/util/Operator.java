package elms.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.servlet.http.Cookie;

/**
 * Operator is a utility class that perform <code>String</code> operations which are <code>null</code> safe.
 */
public class Operator {

	public static String NOVALUE = "NONE";

	public Operator() {
	}

	/**
	 * Checks if specified <code>String</code> is a number.
	 * 
	 * @param str
	 *            The String to parse.
	 * @return <code>true</code> if <code>String</code> is a number
	 */
	public static boolean isNumber(String str) {
		try {
			int num = Integer.parseInt(str);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	/**
	 * <p>
	 * Checks if a String is any type of address.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> string input will return <code>false</code>.
	 * 
	 * <pre>
	 * Operator.isAddress("me@myurl.com") = true
	 * Operator.isAddress("http://www.url.com") = true
	 * Operator.isAddress("hello") = false
	 * </pre>
	 * 
	 * @param str
	 *            the String to parse, may be null
	 * @return <code>true</code> if is an address, and is non-null
	 * @since 1.0
	 */
	public static boolean isAddress(String str) {
		if (str == null || str.length() == 0) {
			return false;
		} else if (addressType(str) > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <p>
	 * Checks if a String is an email address.
	 * </p>
	 * 
	 * <pre>
	 * Operator.isEmail("me@myurl.com") = true
	 * Operator.isEmail("hello") = false
	 * </pre>
	 * 
	 * @param str
	 *            the String to check, may be null
	 * @return <code>true</code> if the String is an email address
	 */
	public static boolean isEmail(String str) {

		if (str == null || str.length() == 0) {
			return false;
		}

		String invalidChars = "/:,;!| ";
		for (int i = 0; i < invalidChars.length(); i++) {
			if (str.indexOf(invalidChars.charAt(i)) != -1) {
				return false;
			}
		}

		if (str.indexOf("@", 1) == -1) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * <p>
	 * Checks if a String is a url address.
	 * </p>
	 * 
	 * <pre>
	 * Operator.isURL("www.url.com") = true
	 * Operator.isURL("http://www.url.com") = true
	 * Operator.isURL("https://www.url.com") = true
	 * Operator.isURL("ftp://www.url.com") = true
	 * Operator.isURL("hello") = false
	 * </pre>
	 * 
	 * @param str
	 *            the String to check, may be null
	 * @return <code>true</code> if the String is a url address
	 */
	public static boolean isURL(String str) {
		if (str == null || str.length() == 0) {
			return false;
		} else if (addressType(str) > 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <p>
	 * Checks if a String is a properly formatted url address.
	 * </p>
	 * 
	 * <pre>
	 * Operator.isProperURL("www.url.com") = false
	 * Operator.isProperURL("http://www.url.com") = true
	 * Operator.isProperURL("https://www.url.com") = true
	 * Operator.isProperURL("ftp://www.url.com") = true
	 * Operator.isProperURL("hello") = false
	 * </pre>
	 * 
	 * @param str
	 *            the String to check, may be null
	 * @return <code>true</code> if the String is a url address
	 */
	public static boolean isProperURL(String str) {
		if (str == null || str.length() == 0) {
			return false;
		} else if (addressType(str) > 2) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * <p>
	 * Checks String and determine the type of address.
	 * </p>
	 * 
	 * <pre>
	 * Operator.addressType("hello") = 0
	 * Operator.addressType("me@myurl.com") = 1
	 * Operator.addressType("www.url.com") = 2
	 * Operator.addressType("http://www.url.com") = 3
	 * Operator.addressType("https://www.url.com") = 4
	 * Operator.addressType("ftp://www.url.com") = 5
	 * </pre>
	 * 
	 * @param str
	 *            the String to check, may be null
	 * @return value of address
	 */
	public static int addressType(String str) {

		if (str == null || str.length() == 0) {
			return 0;
		}

		if (isEmail(str)) {
			return 1;
		} else if (str.indexOf("www.") == 0) {
			return 2;
		} else {
			try {
				URL URLADDRESS = new URL(str);
				String protocol = URLADDRESS.getProtocol();
				if (protocol.equalsIgnoreCase("http")) {
					return 3;
				} else if (protocol.equalsIgnoreCase("https")) {
					return 4;
				} else if (protocol.equalsIgnoreCase("ftp")) {
					return 5;
				}
			} catch (MalformedURLException E) {
				return 0;
			}
		}
		return 0;

	}

	// Check if empty
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Checks if a String contains value, and is not null.
	 * </p>
	 * 
	 * <pre>
	 * Operator.hasValue("hello world") = true
	 * Operator.hasValue(null) = false
	 * Operator.hasValue("") = false
	 * </pre>
	 * 
	 * @param str
	 *            the String to analyze, may be null
	 * @return <code>true</code> if is string is not null and has value.
	 * @since 1.0
	 */
	public static boolean hasValue(String str) {
		if (str == null || str.length() == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * <p>
	 * Checks if an object contains value, and is not null.
	 * </p>
	 * 
	 * <pre>
	 * Operator.hasValue(["a", "b", "c"]) = true
	 * Operator.hasValue(null) = false
	 * Operator.hasValue("[]") = false
	 * </pre>
	 * 
	 * @param array
	 *            the object to analyze, may be null
	 * @return <code>true</code> if the object is not null and has value.
	 * @since 1.0
	 */
	public static boolean hasValue(Object[] array) {
		if (array == null || array.length == 0) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * <p>
	 * Checks if an ArrayList contains value, and is not null.
	 * </p>
	 * 
	 * <pre>
	 * Operator.hasValue(["a", "b", "c"]) = true
	 * Operator.hasValue(null) = false
	 * Operator.hasValue("[]") = false
	 * </pre>
	 * 
	 * @param list
	 *            the object to analyze, may be null
	 * @return <code>true</code> if the object is not null and has value.
	 * @since 1.0
	 */
	public static boolean hasValue(ArrayList list) {
		if (list == null || list.size() == 0) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean equalsIgnoreCase(String one, String two) {
		if (one == null && two == null) {
			return true;
		}
		if (one == null) {
			return false;
		}
		if (two == null) {
			return false;
		}
		return one.equalsIgnoreCase(two);
	}

	// Convert String
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Convert a string to int.
	 * </p>
	 * 
	 * <pre>
	 * Operator.toInt("5") = 5
	 * Operator.toInt(null) = 0
	 * Operator.toInt("") = 0
	 * Operator.toInt("Hello World") = 0
	 * </pre>
	 * 
	 * @param str
	 *            the String to convert, may be null
	 * @return the int represented by the string, or 0 if conversion fails
	 * @since 1.0
	 */
	public static int toInt(String str) {
		return toInt(str, 0);
	}

	/**
	 * <p>
	 * Convert a string to int.
	 * </p>
	 * 
	 * <pre>
	 * Operator.toInt("5",-1) = 5
	 * Operator.toInt(null,-1) = -1
	 * Operator.toInt("",-1) = -1
	 * Operator.toInt("Hello World",-1) = -1
	 * </pre>
	 * 
	 * @param str
	 *            the String to convert, may be null
	 * @param defaultValue
	 *            the default value
	 * @return the int represented by the string, or the default if conversion fails
	 * @since 1.0
	 */
	public static int toInt(String str, int defaultValue) {
		try {
			return Integer.parseInt(str);
		} catch (NumberFormatException nfe) {
			return defaultValue;
		}
	}

	/**
	 * <p>
	 * Convert a int to String.
	 * </p>
	 * 
	 * <pre>
	 * Operator.toString(5) = &quot;5&quot;
	 * </pre>
	 * 
	 * @param num
	 *            the int to convert.
	 * @return the String represented by the int.
	 * @since 1.0
	 */
	public static String toString(int num) {
		return Integer.toString(num);
	}

	/**
	 * <p>
	 * Convert a long to String.
	 * </p>
	 * 
	 * <pre>
	 * Operator.toString(5555555555555L) = &quot;5555555555555&quot;
	 * </pre>
	 * 
	 * @param num
	 *            the long to convert.
	 * @return the String represented by the long.
	 * @since 1.0
	 */
	public static String toString(long num) {
		return Long.toString(num);
	}

	/**
	 * <p>
	 * Convert an ArrayList to String[].
	 * </p>
	 * 
	 * @param list
	 *            the ArrayList to convert.
	 * @return the converted String[] array.
	 */
	public static String[] toArray(ArrayList list) {
		list.trimToSize();
		return (String[]) list.toArray(new String[list.size()]);
	}

	/**
	 * <p>
	 * Convert a String[] to ArrayList.
	 * </p>
	 * 
	 * @param list
	 *            the String[] to convert.
	 * @return the converted ArrayList.
	 */
	public static ArrayList toArrayList(String[] str) {
		ArrayList al = new ArrayList();
		al.add(str);
		return al;
	}

	// Splitting
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Splits the provided text into an array, using whitespace as the separator. Whitespace is defined by {@link Character#isWhitespace(char)}.
	 * </p>
	 * 
	 * <p>
	 * The separator is not included in the returned String array. Adjacent separators are treated as one separator.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> input String returns <code>null</code>.
	 * </p>
	 * 
	 * <pre>
	 * Operator.split(null)       = null
	 * Operator.split("")         = []
	 * Operator.split("abc def")  = ["abc", "def"]
	 * Operator.split("abc  def") = ["abc", "def"]
	 * Operator.split(" abc ")    = ["abc"]
	 * </pre>
	 * 
	 * @param str
	 *            the String to parse, may be null
	 * @return an array of parsed Strings, <code>null</code> if null String input
	 */
	public static String[] split(String str) {
		return split(str, null, -1);
	}

	/**
	 * <p>
	 * Splits the provided text into an array, separator specified. This is an alternative to using StringTokenizer.
	 * </p>
	 * 
	 * <p>
	 * The separator is not included in the returned String array. Adjacent separators are treated as one separator.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> input String returns <code>null</code>.
	 * </p>
	 * 
	 * <pre>
	 * Operator.split(null, *)         = null
	 * Operator.split("", *)           = []
	 * Operator.split("a.b.c", '.')    = ["a", "b", "c"]
	 * Operator.split("a..b.c", '.')   = ["a", "b", "c"]
	 * Operator.split("a:b:c", '.')    = ["a:b:c"]
	 * Operator.split("a\tb\nc", null) = ["a", "b", "c"]
	 * Operator.split("a b c", ' ')    = ["a", "b", "c"]
	 * </pre>
	 * 
	 * @param str
	 *            the String to parse, may be null
	 * @param separatorChar
	 *            the character used as the delimiter, <code>null</code> splits on whitespace
	 * @return an array of parsed Strings, <code>null</code> if null String input
	 * @since 2.0
	 */
	public static String[] split(String str, char separatorChar) {
		// Performance tuned for 2.0 (JDK1.4)

		if (str == null) {
			return null;
		}
		int len = str.length();
		if (len == 0) {
			return new String[0];
		}
		List list = new ArrayList();
		int i = 0, start = 0;
		boolean match = false;
		while (i < len) {
			if (str.charAt(i) == separatorChar) {
				if (match) {
					list.add(str.substring(start, i).trim());
					match = false;
				}
				start = ++i;
				continue;
			}
			match = true;
			i++;
		}
		if (match) {
			list.add(str.substring(start, i).trim());
		}
		return (String[]) list.toArray(new String[list.size()]);
	}

	/**
	 * <p>
	 * Splits the provided text into an array, separators specified. This is an alternative to using StringTokenizer.
	 * </p>
	 * 
	 * <p>
	 * The separator is not included in the returned String array. Adjacent separators are treated as one separator.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> input String returns <code>null</code>. A <code>null</code> separatorChars splits on whitespace.
	 * </p>
	 * 
	 * <pre>
	 * Operator.split(null, *)         = null
	 * Operator.split("", *)           = []
	 * Operator.split("abc def", null) = ["abc", "def"]
	 * Operator.split("abc def", " ")  = ["abc", "def"]
	 * Operator.split("abc  def", " ") = ["abc", "def"]
	 * Operator.split("ab:cd:ef", ":") = ["ab", "cd", "ef"]
	 * </pre>
	 * 
	 * @param str
	 *            the String to parse, may be null
	 * @param separatorChars
	 *            the characters used as the delimiters, <code>null</code> splits on whitespace
	 * @return an array of parsed Strings, <code>null</code> if null String input
	 */
	public static String[] split(String str, String separatorChars) {
		return split(str, separatorChars, -1);
	}

	/**
	 * <p>
	 * Splits the provided text into an array, separators specified. This is an alternative to using StringTokenizer.
	 * </p>
	 * 
	 * <p>
	 * The separator is not included in the returned String array. Adjacent separators are treated as one separator.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> input String returns <code>null</code>. A <code>null</code> separatorChars splits on whitespace.
	 * </p>
	 * 
	 * <pre>
	 * Operator.split(null, *, *)            = null
	 * Operator.split("", *, *)              = []
	 * Operator.split("ab de fg", null, 0)   = ["ab", "cd", "ef"]
	 * Operator.split("ab   de fg", null, 0) = ["ab", "cd", "ef"]
	 * Operator.split("ab:cd:ef", ":", 0)    = ["ab", "cd", "ef"]
	 * Operator.split("ab:cd:ef", ":", 2)    = ["ab", "cdef"]
	 * </pre>
	 * 
	 * @param str
	 *            the String to parse, may be null
	 * @param separatorChars
	 *            the characters used as the delimiters, <code>null</code> splits on whitespace
	 * @param max
	 *            the maximum number of elements to include in the array. A zero or negative value implies no limit
	 * @return an array of parsed Strings, <code>null</code> if null String input
	 */
	public static String[] split(String str, String separatorChars, int max) {
		// Performance tuned for 2.0 (JDK1.4)
		// Direct code is quicker than StringTokenizer.
		// Also, StringTokenizer uses isSpace() not isWhitespace()

		if (str == null) {
			return null;
		}
		int len = str.length();
		if (len == 0) {
			return new String[0];
		}
		List list = new ArrayList();
		int sizePlus1 = 1;
		int i = 0, start = 0;
		boolean match = false;
		if (separatorChars == null) {
			// Null separator means use whitespace
			while (i < len) {
				if (Character.isWhitespace(str.charAt(i))) {
					if (match) {
						if (sizePlus1++ == max) {
							i = len;
						}
						list.add(str.substring(start, i).trim());
						match = false;
					}
					start = ++i;
					continue;
				}
				match = true;
				i++;
			}
		} else if (separatorChars.length() == 1) {
			// Optimise 1 character case
			char sep = separatorChars.charAt(0);
			while (i < len) {
				if (str.charAt(i) == sep) {
					if (match) {
						if (sizePlus1++ == max) {
							i = len;
						}
						list.add(str.substring(start, i).trim());
						match = false;
					}
					start = ++i;
					continue;
				}
				match = true;
				i++;
			}
		} else {
			// standard case
			while (i < len) {
				if (separatorChars.indexOf(str.charAt(i)) >= 0) {
					if (match) {
						if (sizePlus1++ == max) {
							i = len;
						}
						list.add(str.substring(start, i).trim());
						match = false;
					}
					start = ++i;
					continue;
				}
				match = true;
				i++;
			}
		}
		if (match) {
			list.add(str.substring(start, i).trim());
		}
		return (String[]) list.toArray(new String[list.size()]);
	}

	public static boolean contains(String[] array, String value) {
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equals(value)) {
				return true;
			}
		}
		return false;
	}

	public static boolean containsIgnoreCase(String[] array, String value) {
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static boolean contains(String delimited, String value) {
		String[] array = split(delimited);
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equals(value)) {
				return true;
			}
		}
		return false;
	}

	public static boolean contains(String delimited, String delimiter, String value) {
		String[] array = split(delimited, delimiter);
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equals(value)) {
				return true;
			}
		}
		return false;
	}

	public static boolean containsIgnoreCase(String delimited, String value) {
		String[] array = split(delimited);
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	public static boolean containsIgnoreCase(String delimited, String delimiter, String value) {
		String[] array = split(delimited, delimiter);
		int arraySize = array.length;
		for (int i = 0; i < arraySize; i++) {
			if (array[i].equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}

	// Joining
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Joins the elements of the provided array into a single String containing the provided list of elements.
	 * </p>
	 * 
	 * <p>
	 * No separator is added to the joined String. Null objects or empty strings within the array are represented by empty strings.
	 * </p>
	 * 
	 * <pre>
	 * Operator.join(null)            = null
	 * Operator.join([])              = ""
	 * Operator.join([null])          = ""
	 * Operator.join(["a", "b", "c"]) = "abc"
	 * Operator.join([null, "", "a"]) = "a"
	 * </pre>
	 * 
	 * @param array
	 *            the array of values to join together, may be null
	 * @return the joined String, <code>null</code> if null array input
	 * @since 2.0
	 */
	public static String join(Object[] array) {
		return join(array, null);
	}

	/**
	 * <p>
	 * Joins the elements of the provided array into a single String containing the provided list of elements.
	 * </p>
	 * 
	 * <p>
	 * No delimiter is added before or after the list. Null objects or empty strings within the array are represented by empty strings.
	 * </p>
	 * 
	 * <pre>
	 * Operator.join(null, *)               = null
	 * Operator.join([], *)                 = ""
	 * Operator.join([null], *)             = ""
	 * Operator.join(["a", "b", "c"], ';')  = "a;b;c"
	 * Operator.join(["a", "b", "c"], null) = "abc"
	 * Operator.join([null, "", "a"], ';')  = ";;a"
	 * </pre>
	 * 
	 * @param array
	 *            the array of values to join together, may be null
	 * @param separator
	 *            the separator character to use
	 * @return the joined String, <code>null</code> if null array input
	 * @since 2.0
	 */
	public static String join(Object[] array, char separator) {
		if (array == null) {
			return null;
		}
		int arraySize = array.length;
		int bufSize = (arraySize == 0 ? 0 : ((array[0] == null ? 16 : array[0].toString().length()) + 1) * arraySize);
		StringBuffer buf = new StringBuffer(bufSize);

		for (int i = 0; i < arraySize; i++) {
			if (i > 0) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}

	/**
	 * <p>
	 * Joins the elements of the provided array into a single String containing the provided list of elements.
	 * </p>
	 * 
	 * <p>
	 * No delimiter is added before or after the list. A <code>null</code> separator is the same as an empty String (""). Null objects or empty strings within the array are represented by empty strings.
	 * </p>
	 * 
	 * <pre>
	 * Operator.join(null, *)                = null
	 * Operator.join([], *)                  = ""
	 * Operator.join([null], *)              = ""
	 * Operator.join(["a", "b", "c"], "--")  = "a--b--c"
	 * Operator.join(["a", "b", "c"], null)  = "abc"
	 * Operator.join(["a", "b", "c"], "")    = "abc"
	 * Operator.join([null, "", "a"], ',')   = ",,a"
	 * </pre>
	 * 
	 * @param array
	 *            the array of values to join together, may be null
	 * @param separator
	 *            the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null array input
	 */
	public static String join(Object[] array, String separator) {
		if (array == null) {
			return null;
		}
		if (separator == null) {
			separator = "";
		}
		int arraySize = array.length;

		// ArraySize == 0: Len = 0
		// ArraySize > 0: Len = NofStrings *(len(firstString) + len(separator))
		// (Assuming that all Strings are roughly equally long)
		int bufSize = ((arraySize == 0) ? 0 : arraySize * ((array[0] == null ? 16 : array[0].toString().length()) + ((separator != null) ? separator.length() : 0)));

		StringBuffer buf = new StringBuffer(bufSize);

		for (int i = 0; i < arraySize; i++) {
			if ((separator != null) && (i > 0)) {
				buf.append(separator);
			}
			if (array[i] != null) {
				buf.append(array[i]);
			}
		}
		return buf.toString();
	}

	/**
	 * <p>
	 * Joins the elements of the provided <code>Iterator</code> into a single String containing the provided elements.
	 * </p>
	 * 
	 * <p>
	 * No delimiter is added before or after the list. Null objects or empty strings within the iteration are represented by empty strings.
	 * </p>
	 * 
	 * <p>
	 * See the examples here: {@link #join(Object[],char)}.
	 * </p>
	 * 
	 * @param iterator
	 *            the <code>Iterator</code> of values to join together, may be null
	 * @param separator
	 *            the separator character to use
	 * @return the joined String, <code>null</code> if null iterator input
	 * @since 2.0
	 */
	public static String join(Iterator iterator, char separator) {
		if (iterator == null) {
			return null;
		}
		StringBuffer buf = new StringBuffer(256); // Java default is 16, probably too small
		while (iterator.hasNext()) {
			Object obj = iterator.next();
			if (obj != null) {
				buf.append(obj);
			}
			if (iterator.hasNext()) {
				buf.append(separator);
			}
		}
		return buf.toString();
	}

	/**
	 * <p>
	 * Joins the elements of the provided <code>Iterator</code> into a single String containing the provided elements.
	 * </p>
	 * 
	 * <p>
	 * No delimiter is added before or after the list. A <code>null</code> separator is the same as an empty String ("").
	 * </p>
	 * 
	 * <p>
	 * See the examples here: {@link #join(Object[],String)}.
	 * </p>
	 * 
	 * @param iterator
	 *            the <code>Iterator</code> of values to join together, may be null
	 * @param separator
	 *            the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null iterator input
	 */
	public static String join(Iterator iterator, String separator) {
		if (iterator == null) {
			return null;
		}
		StringBuffer buf = new StringBuffer(256); // Java default is 16, probably too small
		while (iterator.hasNext()) {
			Object obj = iterator.next();
			if (obj != null) {
				buf.append(obj);
			}
			if ((separator != null) && iterator.hasNext()) {
				buf.append(separator);
			}
		}
		return buf.toString();
	}

	// Replacing
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Replaces a String with another String inside a larger String, once.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method is a no-op.
	 * </p>
	 * 
	 * <pre>
	 * Operator.replaceOnce(null, *, *)        = null
	 * Operator.replaceOnce("", *, *)          = ""
	 * Operator.replaceOnce("aba", null, null) = "aba"
	 * Operator.replaceOnce("aba", null, null) = "aba"
	 * Operator.replaceOnce("aba", "a", null)  = "aba"
	 * Operator.replaceOnce("aba", "a", "")    = "aba"
	 * Operator.replaceOnce("aba", "a", "z")   = "zba"
	 * </pre>
	 * 
	 * @see #replace(String text, String repl, String with, int max)
	 * @param text
	 *            text to search and replace in, may be null
	 * @param repl
	 *            the String to search for, may be null
	 * @param with
	 *            the String to replace with, may be null
	 * @return the text with any replacements processed, <code>null</code> if null String input
	 */
	public static String replaceOnce(String text, String repl, String with) {
		return replace(text, repl, with, 1);
	}

	/**
	 * <p>
	 * Replaces all occurances of a String within another String.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method is a no-op.
	 * </p>
	 * 
	 * <pre>
	 * Operator.replace(null, *, *)        = null
	 * Operator.replace("", *, *)          = ""
	 * Operator.replace("aba", null, null) = "aba"
	 * Operator.replace("aba", null, null) = "aba"
	 * Operator.replace("aba", "a", null)  = "aba"
	 * Operator.replace("aba", "a", "")    = "aba"
	 * Operator.replace("aba", "a", "z")   = "zbz"
	 * </pre>
	 * 
	 * @see #replace(String text, String repl, String with, int max)
	 * @param text
	 *            text to search and replace in, may be null
	 * @param repl
	 *            the String to search for, may be null
	 * @param with
	 *            the String to replace with, may be null
	 * @return the text with any replacements processed, <code>null</code> if null String input
	 */
	public static String replace(String text, String repl, String with) {
		return replace(text, repl, with, -1);
	}

	/**
	 * <p>
	 * Replaces a String with another String inside a larger String, for the first <code>max</code> values of the search String.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method is a no-op.
	 * </p>
	 * 
	 * <pre>
	 * Operator.replace(null, *, *, *)         = null
	 * Operator.replace("", *, *, *)           = ""
	 * Operator.replace("abaa", null, null, 1) = "abaa"
	 * Operator.replace("abaa", null, null, 1) = "abaa"
	 * Operator.replace("abaa", "a", null, 1)  = "abaa"
	 * Operator.replace("abaa", "a", "", 1)    = "abaa"
	 * Operator.replace("abaa", "a", "z", 0)   = "abaa"
	 * Operator.replace("abaa", "a", "z", 1)   = "zbaa"
	 * Operator.replace("abaa", "a", "z", 2)   = "zbza"
	 * Operator.replace("abaa", "a", "z", -1)  = "zbzz"
	 * </pre>
	 * 
	 * @param text
	 *            text to search and replace in, may be null
	 * @param repl
	 *            the String to search for, may be null
	 * @param with
	 *            the String to replace with, may be null
	 * @param max
	 *            maximum number of values to replace, or <code>-1</code> if no maximum
	 * @return the text with any replacements processed, <code>null</code> if null String input
	 */
	public static String replace(String text, String repl, String with, int max) {
		if (text == null || repl == null || with == null || repl.length() == 0 || max == 0) {
			return text;
		}

		StringBuffer buf = new StringBuffer(text.length());
		int start = 0, end = 0;
		while ((end = text.indexOf(repl, start)) != -1) {
			buf.append(text.substring(start, end)).append(with);
			start = end + repl.length();

			if (--max == 0) {
				break;
			}
		}
		buf.append(text.substring(start));
		return buf.toString();
	}

	public static String replace(String text, char repl, String with) {
		String r = new Character(repl).toString();
		return replace(text, r, with);
	}

	public static String replace(String text, String repl, char with) {
		String w = new Character(with).toString();
		return replace(text, repl, w);
	}

	public static String replace(String text, char repl, char with) {
		return text.replace(repl, with);
	}

	public static String replaceSmartQuotes(String text) {

		String TEXT = "";

		// smart quotes
		TEXT = replace(text, (char) 0x93, (char) 0x22);
		text = replace(TEXT, (char) 0x94, (char) 0x22);
		TEXT = replace(text, (char) 0x201C, (char) 0x22);
		text = replace(TEXT, (char) 0x201D, (char) 0x22);

		// smart single quotes
		TEXT = replace(text, (char) 0x91, (char) 0x27);
		text = replace(TEXT, (char) 0x92, (char) 0x27);
		TEXT = replace(text, (char) 0x2018, (char) 0x27);
		text = replace(TEXT, (char) 0x2019, (char) 0x27);

		return text;

	}

	public static String replaceSmartChars(String text) {

		String TEXT = replaceSmartQuotes(text);

		// em/en dash
		text = replace(TEXT, (char) 0x96, (char) 0x2D);
		TEXT = replace(text, (char) 0x97, (char) 0x2D);
		text = replace(TEXT, (char) 0x2013, (char) 0x2D);
		TEXT = replace(text, (char) 0x2014, (char) 0x2D);

		// elipse
		text = replace(TEXT, (char) 0x85, "...");

		TEXT = replace(text, (char) 0x1A, (char) 0x27);

		return TEXT;

	}

	/**
	 * <p>
	 * Capitalizes all the whitespace separated words in a String. All letters are changed, so the resulting string will be fully changed.
	 * </p>
	 * 
	 * <p>
	 * Whitespace is defined by {@link Character#isWhitespace(char)}. A <code>null</code> input String returns <code>null</code>. Capitalization uses the unicode title case, normally equivalent to upper case.
	 * </p>
	 * 
	 * @param str
	 *            the String to capitalize, may be null
	 * @return capitalized String, <code>null</code> if null String input
	 */
	public static String toTitleCase(String str) {
		int strLen;
		if (str == null || (strLen = str.length()) == 0) {
			return "";
		}
		StringBuffer buffer = new StringBuffer(strLen);
		boolean whitespace = true;
		for (int i = 0; i < strLen; i++) {
			char ch = str.charAt(i);
			if (Character.isWhitespace(ch)) {
				buffer.append(ch);
				whitespace = true;
			} else if (whitespace) {
				buffer.append(Character.toTitleCase(ch));
				whitespace = false;
			} else {
				buffer.append(Character.toLowerCase(ch));
			}
		}
		return buffer.toString();
	}

	// Special Characters
	// -----------------------------------------------------------------------
	/**
	 * <p>
	 * Replaces occurences of special characters within a String with proper HTML code.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method returns "".
	 * </p>
	 * 
	 * <pre>
	 * Operator.toHTML(null)    = ""
	 * Operator.toHTML("")      = ""
	 * Operator.toHTML("10%") = "10&#37;"
	 * </pre>
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String toHTML(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = str;
		String result = replaceSmartChars(RESULT);

		RESULT = replace(result, "'", "&#39;");
		result = replace(RESULT, "%", "&#37;");
		RESULT = replace(result, "\"", "&#34;");
		result = replace(RESULT, "(", "&#40;");
		RESULT = replace(result, ")", "&#41;");
		result = replace(RESULT, "\\", "&#92;");
		RESULT = replace(result, "^", "&#94;");
		result = replace(RESULT, "_", "&#95;");
		RESULT = replace(result, "<", "&#60;");
		result = replace(RESULT, ">", "&#62;");
		RESULT = replace(result, ",", "&#44;");

		result = replace(RESULT, "\r\n", "<BR>");
		RESULT = replace(result, "\n", "<BR>");

		return RESULT;

	}

	/**
	 * <p>
	 * Replaces occurences of HTML codes within a String with corresponding special character.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method returns "".
	 * </p>
	 * 
	 * <pre>
	 * Operator.fromHTML(null)    = ""
	 * Operator.fromHTML("")      = ""
	 * Operator.fromHTML("10&#37;") = "10%"
	 * </pre>
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String fromHTML(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = str;
		String result = RESULT;

		RESULT = replace(result, "&#39;", "'");
		result = replace(RESULT, "&#37;", "%");
		RESULT = replace(result, "&#34;", "\"");
		result = replace(RESULT, "&#40;", "(");
		RESULT = replace(result, "&#41;", ")");
		result = replace(RESULT, "&#92;", "\\");
		RESULT = replace(result, "&#94;", "^");
		result = replace(RESULT, "&#95;", "_");
		RESULT = replace(result, "&#60;", "<");
		result = replace(RESULT, "&#62;", ">");
		RESULT = replace(result, "&#44;", ",");

		result = replace(RESULT, "<BR>", "\n");

		return result;

	}

	/**
	 * <p>
	 * Replaces occurences of special characters within a String with corresponding ASCII codes.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method returns "".
	 * </p>
	 * 
	 * <pre>
	 * Operator.toASCII(null)    = ""
	 * Operator.toASCII("")      = ""
	 * Operator.toASCII("a b c") = "a%20b%20c"
	 * </pre>
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String toASCII(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = replaceSmartChars(str);
		String result = "";

		result = replace(RESULT, " ", "%20");
		RESULT = replace(result, "\"", "%22");
		result = replace(RESULT, "&", "%26");
		RESULT = replace(result, "=", "%3D");
		result = replace(RESULT, "?", "%3F");
		RESULT = replace(result, "<", "%3C");
		result = replace(RESULT, ">", "%3E");

		RESULT = replace(result, "\r\n", "%0A");
		result = replace(RESULT, "\n", "%0A");

		return result;

	}

	/**
	 * <p>
	 * Replaces occurences of ASCII codes within a String with proper ASCII code.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method returns "".
	 * </p>
	 * 
	 * <pre>
	 * Operator.fromASCII(null)    = ""
	 * Operator.fromASCII("")      = ""
	 * Operator.fromASCII("a%20b%20a") = "a b a"
	 * </pre>
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String fromASCII(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = str;
		String result = "";

		result = replace(RESULT, "%20", " ");
		RESULT = replace(result, "%22", "\"");
		result = replace(RESULT, "%26", "&");
		RESULT = replace(result, "%3D", "=");
		result = replace(RESULT, "%3F", "?");
		RESULT = replace(result, "%3C", "<");
		result = replace(RESULT, "%3E", ">");

		RESULT = replace(result, "%0A", "\n");

		return RESULT;

	}

	/**
	 * <p>
	 * Replaces occurences of ASCII codes and HTML codes within a String with corresponding special character.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method returns "".
	 * </p>
	 * 
	 * <pre>
	 * Operator.toText(null)    = ""
	 * Operator.toText("")      = ""
	 * Operator.toText("a%20b%20a<BR>") = "a b a\n"
	 * </pre>
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String toText(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = fromASCII(str);
		String result = fromHTML(RESULT);

		return result;

	}

	/**
	 * Make string friendly to sql statements.
	 * 
	 * @param str
	 *            string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String sqlFriendly(String str) {
		String result = toHTML(str);
		return replace(result, "&#44;", ",");
	}

	public static String rtToSQL(String str) {
		if (str == null) {
			return "";
		}
		String RESULT = replaceSmartChars(str);
		String result = replace(RESULT, "|", "&#124;");
		RESULT = replace(result, "'", "|Q");
		result = replace(RESULT, "%", "|P");

		RESULT = result;

		result = replace(RESULT, "\\", "&#92;");
		RESULT = replace(result, "^", "&#94;");
		result = replace(RESULT, "_", "&#95;");
		RESULT = replace(result, "\r\n", " ");
		result = replace(RESULT, "\n", " ");

		RESULT = replace(result, "<TABLE", "|T");
		result = replace(RESULT, "</TABLE>", "|/T");
		RESULT = replace(result, "<table", "|T");
		result = replace(RESULT, "</table>", "|/T");

		RESULT = replace(result, "<TBODY>", "");
		result = replace(RESULT, "</TBODY>", "");
		RESULT = replace(result, "<tbody>", "");
		result = replace(RESULT, "</tbody>", "");

		RESULT = replace(result, "<STRONG>", "|S");
		result = replace(RESULT, "</STRONG>", "|/S");
		RESULT = replace(result, "<strong>", "|S");
		result = replace(RESULT, "</strong>", "|/S");

		RESULT = replace(result, "<EM>", "|E");
		result = replace(RESULT, "</EM>", "|/E");
		RESULT = replace(result, "<em>", "|E");
		result = replace(RESULT, "</em>", "|/E");

		RESULT = replace(result, "<BLOCKQUOTE", "|B");
		result = replace(RESULT, "</BLOCKQUOTE>", "|/B");
		RESULT = replace(result, "<blockquote", "|B");
		result = replace(RESULT, "</blockquote>", "|/B");

		RESULT = replace(result, "<FONT", "|F");
		result = replace(RESULT, "</FONT>", "|/F");
		RESULT = replace(result, "<font", "|F");
		result = replace(RESULT, "</font>", "|/F");

		RESULT = replace(result, " cellspacing=", "|CS");
		result = replace(RESULT, " cellpadding=", "|CP");
		RESULT = replace(result, " cellSpacing=", "|CS");
		result = replace(RESULT, " cellPadding=", "|CP");

		RESULT = replace(result, "&nbsp;", " ");
		return RESULT;
	}

	public static String sqlToRT(String str) {
		if (str == null) {
			return "";
		}
		String RESULT = str;
		String result = RESULT;
		RESULT = replace(result, "&#39;", "'");
		result = replace(RESULT, "&#37;", "%");
		RESULT = replace(result, "|Q", "'");
		result = replace(RESULT, "|P", "%");
		RESULT = replace(result, "&#34;", "\"");
		result = replace(RESULT, "&#40;", "(");
		RESULT = replace(result, "&#41;", ")");
		result = replace(RESULT, "&#92;", "\\");
		RESULT = replace(result, "&#94;", "^");
		result = replace(RESULT, "&#95;", "_");
		RESULT = replace(result, "&#60;", "<");
		result = replace(RESULT, "&#62;", ">");
		RESULT = replace(result, "&#44;", ",");

		result = replace(RESULT, "|T", "<table");
		RESULT = replace(result, "|/T", "</table>");

		result = replace(RESULT, "|S", "<strong>");
		RESULT = replace(result, "|/S", "</strong>");

		result = replace(RESULT, "|E", "<em>");
		RESULT = replace(result, "|/E", "</em>");

		result = replace(RESULT, "|B", "<blockquote");
		RESULT = replace(result, "|/B", "</blockquote>");

		result = replace(RESULT, "|F", "<font");
		RESULT = replace(result, "|/F", "</font>");

		result = replace(RESULT, "|CS", " cellspacing=");
		RESULT = replace(result, "|CP", " cellpadding=");

		result = replace(RESULT, "&#124;", "|");
		return result;
	}

	/**
	 * Return string from sql friendly result to original state.
	 * 
	 * @param str
	 *            string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String sqlUnfriendly(String str) {
		return fromHTML(str);
	}

	/**
	 * Make string query friendly.
	 * 
	 * @param str
	 *            string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String queryFriendly(String str) {

		if (str == null) {
			return "";
		}

		return toASCII(str);

	}

	/**
	 * Make string cookie friendly.
	 * 
	 * @param str
	 *            string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String cookieFriendly(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = replaceSmartChars(str);
		String result = "";

		result = replace(RESULT, " ", "|W|");
		RESULT = replace(result, ";", "|S|");
		result = replace(RESULT, ",", "|C|");
		RESULT = replace(result, "\n", "|N|");
		result = replace(RESULT, "\t", "|T|");
		RESULT = replace(result, "@", "|A|");
		result = replace(RESULT, "=", "|E|");
		RESULT = replace(result, "\"", "|Q|");
		result = replace(RESULT, "/", "|SL|");
		RESULT = replace(result, "?", "|QU|");
		result = replace(RESULT, "(", "|OP|");
		RESULT = replace(result, ")", "|CP|");
		result = replace(RESULT, "[", "|OB|");
		RESULT = replace(result, "]", "|CB|");

		return RESULT;

	}

	/**
	 * Make string cookie unfriendly.
	 * 
	 * @param str
	 *            string to search and replace, may be null
	 * @return the string with any replacements processed
	 */
	public static String cookieUnfriendly(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = str;
		String result = "";

		result = replace(RESULT, "|W|", " ");
		RESULT = replace(result, "|S|", ";");
		result = replace(RESULT, "|C|", ",");
		RESULT = replace(result, "|N|", "\n");
		result = replace(RESULT, "|T|", "\t");
		RESULT = replace(result, "|A|", "@");
		result = replace(RESULT, "|E|", "=");
		RESULT = replace(result, "|Q|", "\"");
		result = replace(RESULT, "|SL|", "/");
		RESULT = replace(result, "|QU|", "?");
		result = replace(RESULT, "|OP|", "(");
		RESULT = replace(result, "|CP|", ")");
		result = replace(RESULT, "|OB|", "[");
		RESULT = replace(result, "|CB|", "]");

		return RESULT;

	}

	/**
	 * Escape all occurences of quotes and new lines.
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String escape(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = replaceSmartChars(str);
		String result = "";

		result = replace(RESULT, "\"", "\\\"");
		RESULT = replace(result, "\n", "\\n");

		return RESULT;

	}

	/**
	 * Make string excel friendly.
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String excelFriendly(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = replaceSmartChars(str);
		String result = "";

		result = replace(RESULT, "\"", "\"\"");
		RESULT = replace(result, "\r\n", "\n");

		return RESULT;

	}

	/**
	 * Make string javascript friendly.
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String javascriptFriendly(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = replaceSmartChars(str);
		String result = "";

		result = replace(RESULT, "\"", "\\\"");
		RESULT = replace(result, "\r\n", "\n");
		result = replace(RESULT, "\n", "\\n");

		return result;

	}

	/**
	 * Make string csv friendly.
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String csvFriendly(String str) {
		return excelFriendly(str);
	}

	/**
	 * Make string tsv friendly.
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String tsvFriendly(String str) {
		return excelFriendly(str);
	}

	/**
	 * Make string form input field friendly.
	 * 
	 * @param str
	 *            string to make friendly, may be null
	 * @return the string with any replacements processed
	 */
	public static String formFriendly(String str) {

		if (str == null) {
			return "";
		}

		String RESULT = replaceSmartChars(str);
		String result = "";

		result = replace(RESULT, "\"", "&quot;");
		RESULT = replace(result, "\r\n", "\n");

		return RESULT;

	}

	/**
	 * Make string xml friendly. This method will not replace the ampersand symbols to html format(&#38;).</p>
	 * 
	 * @param str
	 *            string to make friendly, may be null
	 * @return the string with any replacements processed
	 */
	public static String xmlFriendly(String str) {
		return xmlFriendly(str, true);
	}

	/**
	 * Make string xml friendly.
	 * 
	 * @param str
	 *            string to make friendly, may be null
	 * @param and
	 *            If <code>true</code> the method will replace the ampersand symbols to html format(&#38;).
	 * @return the string with any replacements processed
	 */
	public static String xmlFriendly(String str, boolean and) {

		if (str == null) {
			return "";
		}

		String RESULT;
		String result;

		if (and) {
			RESULT = Operator.replace(str, "&", "&#38;");
		} else {
			RESULT = str;
		}

		result = Operator.replace(RESULT, "<", "&#60;");
		RESULT = Operator.replace(result, ">", "&#62;");
		result = Operator.replace(RESULT, "'", "&#39;");
		RESULT = Operator.replace(result, "\"", "&#34;");

		return RESULT;

	}

	/**
	 * Make string text file database friendly.
	 * 
	 * @param str
	 *            string to make friendly, may be null
	 * @return the string with any replacements processed
	 */
	public static String textDBFriendly(String str) {

		if (str == null) {
			return "";
		}

		String RESULT;
		String result;

		RESULT = Operator.replace(str, "\r\n", "<br>");
		result = Operator.replace(RESULT, "\n", "<br>");
		RESULT = Operator.replace(result, "|", " ");

		return RESULT;

	}

	/**
	 * <p>
	 * Add HTML hyperlinks to all found addresses in a string.
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method returns "".
	 * </p>
	 * 
	 * <pre>
	 * Operator.hyperlink(null)    = ""
	 * Operator.hyperlink("")      = ""
	 * Operator.hyperlink("the address is http://url.com") = "the address is <a href=http://url.com>http://url.com</a>"
	 * </pre>
	 * 
	 * @param str
	 *            string to search and replace in, may be null
	 * @return the string with any replacements processed
	 */
	public static String hyperlink(String str) {

		String[] ARRAY = split(str, " ");
		String STRING = "";
		StringBuffer ARRAYBUFFER = new StringBuffer(0);
		int ARRAYLENGTH = ARRAY.length;

		for (int i = 0; i < ARRAYLENGTH; i++) {
			STRING = ARRAY[i];
			if (Operator.isURL(STRING)) {
				ARRAYBUFFER = new StringBuffer(5000);
				ARRAYBUFFER.append("<a href=\"").append(STRING).append("\">").append(STRING).append("</a>");
				ARRAY[i] = ARRAYBUFFER.toString();
			} else if (Operator.isEmail(STRING)) {
				ARRAYBUFFER = new StringBuffer(5000);
				ARRAYBUFFER.append("<a href=\"mailto:").append(STRING).append("\">").append(STRING).append("</a>");
				ARRAY[i] = ARRAYBUFFER.toString();
			}
		}

		return (join(ARRAY, " "));
	}

	/**
	 * Create a random string.
	 * 
	 * @param length
	 *            Length of string.
	 * @return Returns random <code>String</code>.
	 */
	public static String randomString(int length) {

		char[] chars = new char[52];
		for (int i = 0; i < 26; i++) {
			chars[i] = (char) (97 + i);
			chars[i + 26] = (char) (65 + i);
		}

		Random random = new Random();

		char[] array = new char[length];
		for (int i = 0; i < length; i++) {
			array[i] = chars[random.nextInt(chars.length)];
		}
		return new String(array);

	}

	/**
	 * Create a random 10 character string.
	 * 
	 * @return Returns random <code>String</code>.
	 */
	public static String randomString() {
		return randomString(10);
	}

	/**
	 * Create a random string.
	 * 
	 * @param length
	 *            Length of string.
	 * @return Returns random <code>String</code>.
	 */
	public static String randomString(String length) {
		if (!Operator.hasValue(length)) {
			return randomString(10);
		} else if (isNumber(length)) {
			return randomString(toInt(length));
		} else {
			return length;
		}
	}

	public static String sqlWhere(String field, String values) {
		return sqlWhere("", field, Operator.split(values, " "));
	}

	public static String sqlWhere(String table, String field, String values) {
		return sqlWhere(table, field, Operator.split(values, " "));
	}

	public static String sqlWhere(String field, String[] values) {
		return sqlWhere("", field, values);
	}

	public static String sqlWhere(String table, String field, String[] values) {
		if (!Operator.hasValue(field)) {
			return "";
		}
		if (!Operator.hasValue(values)) {
			return "";
		}
		if (!Operator.hasValue(table)) {
			table = "";
		}
		StringBuffer sb = new StringBuffer(500);
		sb.append("(");
		for (int i = 0; i < values.length; i++) {
			values[i] = Operator.replace(values[i], "'", "");
			values[i] = Operator.replace(values[i], "%", " percent ");
			values[i] = Operator.replace(values[i], "_", " ");
			values[i] = Operator.replace(values[i], "\r\n", " ");
			values[i] = Operator.replace(values[i], "\n", " ");
			if (i > 0) {
				sb.append(" AND ");
			}
			if (hasValue(table)) {
				sb.append(table).append(".");
			}
			sb.append(field).append(" LIKE '%").append(values[i]).append("%'");
		}
		sb.append(")");
		return sb.toString();
	}

	public static Cookie cookie(String field, String value) {
		if (Operator.hasValue(value)) {
			Cookie cookie = new Cookie(field, cookieFriendly(value));
			cookie.setPath("/");
			return cookie;
		} else {
			Cookie cookie = new Cookie(field, NOVALUE);
			cookie.setPath("/");
			return cookie;
		}
	}

	public static Cookie persistentCookie(String field, String value) {
		if (Operator.hasValue(value)) {
			Cookie cookie = new Cookie(field, cookieFriendly(value));
			cookie.setPath("/");
			cookie.setMaxAge(60 * 60 * 24 * 365);
			return cookie;
		} else {
			Cookie cookie = new Cookie(field, NOVALUE);
			cookie.setPath("/");
			cookie.setMaxAge(60 * 60 * 24 * 365);
			return cookie;
		}
	}

	public static Cookie persistentCookie(String field, String value, int duration) {
		if (Operator.hasValue(value)) {
			Cookie cookie = new Cookie(field, cookieFriendly(value));
			cookie.setPath("/");
			cookie.setMaxAge(duration);
			return cookie;
		} else {
			Cookie cookie = new Cookie(field, NOVALUE);
			cookie.setPath("/");
			cookie.setMaxAge(duration);
			return cookie;
		}
	}

	public static String sqlSearchFriendly(String table, String field, String val) {

		StringBuffer SEARCHBUFFER = new StringBuffer(5000);
		StringBuffer SEARCHOPERATOR = new StringBuffer(0);
		String value = rtToSQL(val);

		if (!Operator.hasValue(value)) {
			return "";
		} else {

			SEARCHOPERATOR = new StringBuffer(500);
			SEARCHOPERATOR.append("%') AND LCASE(").append(table).append(".");
			SEARCHOPERATOR.append(field);
			SEARCHOPERATOR.append(") LIKE LCASE('%");
			value = Operator.replace(value, " AND ", SEARCHOPERATOR.toString());

			SEARCHOPERATOR = new StringBuffer(500);
			SEARCHOPERATOR.append("%') OR LCASE(").append(table).append(".");
			SEARCHOPERATOR.append(field);
			SEARCHOPERATOR.append(") LIKE LCASE('%");
			value = Operator.replace(value, " OR ", SEARCHOPERATOR.toString());

			SEARCHBUFFER.append("(LCASE(").append(table).append(".");
			SEARCHBUFFER.append(field);
			SEARCHBUFFER.append(") LIKE LCASE('%");
			SEARCHBUFFER.append(value);
			SEARCHBUFFER.append("%'))");

			return SEARCHBUFFER.toString();

		}
	}
	
	public static String getEndString(String str, int chars) {
		int l = str.length();
		int start = l - chars;
		if (start < 0) { start = 0; } 
		return subString(str, start, l);
	}
	
	public static String subString(String str, int start, int end) {
		if (str.length() < start) { return ""; }
		if (str.length() < end) { end = str.length(); }
		return str.substring(start, end);
	}
	 public static String checkString(String aValue) {
	        if (aValue == null) {
	            return "null";
	        } else {
	            if (aValue.equals("")) {
	                return "null";
	            }

	            // preparing string for database insert
	            String tempStr = "";
	            char quot = '\'';

	            if ((aValue != null) && (aValue.length() > 0)) {
	                for (int i = 0; i < aValue.length(); i++) {
	                    char c = aValue.charAt(i);

	                    if (c != quot) {
	                        tempStr = tempStr + aValue.charAt(i);
	                    } else {
	                        tempStr = tempStr + aValue.charAt(i) + aValue.charAt(i);
	                    }
	                }

	                aValue = tempStr;
	            }

	            //else {
	            aValue = "'" + aValue + "'";

	            return aValue;

	            //}
	        }
	    }
	    
	 
}
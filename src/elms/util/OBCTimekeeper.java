package elms.util;

import java.util.*;
import java.text.*;




/**
 * OBCTimekeeper is a date and time interpreter which accepts and returns practical values.<br><br>
 * For example: The integer value of January is 1 (opposed to 0, in most java date interpreters).<br><br>
 *
 * Below is a proper use example.<br><br>
 *
 * <pre>
 * OBCTimekeeper DATE = new Timekepper(1969, 11, 4);
 * out.print(DATE.fullDate());
 * </pre>
 *
 * @author Alain Romero
 *
 */
 public class OBCTimekeeper {

    /**
     * Create an instance of Calendar
     */
    public Calendar CALENDAR;

	/**
	 * Array of Months (January = 0)
	 */
	public static String MONTHS[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};

	/**
	 * Array of Months (January = 1)
	 */
	public static String PMONTHS[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};

	/**
	 * Array of Abbreviated Months (Jan = 0)
	 */
	public static String ABMONTHS[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

	/**
	 * Array of Abbreviated Months (Jan = 1)
	 */
	public static String PABMONTHS[] = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    /**
     * Array of Days (Sunday = 0)
     */
    public static String DAYS[]   = {"Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"};

	/**
	 * Number of seconds in a year.
	 */
    public static int SECONDS_PER_YEAR = 60*60*24*365;

	/**
	 * Number of seconds in a day.
	 */
    public static int SECONDS_PER_DAY = 60*60*24;

	/**
	 * Number of seconds in an hour.
	 */
    public static int SECONDS_PER_HOUR = 60*60;

	public static int FISCAL_MONTH = 7;

	public boolean HASVALUE = false;

/* #################################################################################################
##  CONSTRUCTORS                                                                                  ##
################################################################################################# */

    /**
     * Constructs an instance of this class using today's date.
     */
    public OBCTimekeeper() {
        CALENDAR = Calendar.getInstance();
    }

    /**
     * Constructs an instance of this class using a datecode (yyyymmdd).
     *
     * @param datecode yyyymmdd formatted date (use 1 - 12 for month: January = 1).
     */
    public OBCTimekeeper(int datecode) {
		if (datecode > 0) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(DATEDECODE(datecode,"YEAR"), DATEDECODE(datecode,"MONTH")-1, DATEDECODE(datecode,"DAY"));
    }

    /**
     * Constructs an instance of this class using a <code>String</code> representation of a datecode ("yyyymmdd").
     *
     * @param datecode yyyymmdd, yyyy-mm-dd, yyyy/mm/dd formatted date (use 1 - 12 for month: January = 1).
     */
    public OBCTimekeeper(String datecode) {
		if (Operator.hasValue(datecode)) { HASVALUE = true; }
		datecode = Operator.replace(datecode, "-", "");
		datecode = Operator.replace(datecode, "/", "");
		datecode = Operator.replace(datecode, " ", "");
        int DATECODE = Operator.toInt(datecode);
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(DATEDECODE(DATECODE,"YEAR"), DATEDECODE(DATECODE,"MONTH")-1, DATEDECODE(DATECODE,"DAY"));
    }

    /**
     * Constructs an instance of this class using a <code>Date</code> object.
     *
     * @param date Date object.
     */
    public OBCTimekeeper(Date date) {
        CALENDAR = Calendar.getInstance();
        CALENDAR.setTime(date);
		HASVALUE = true;
    }

    /**
     * Constructs an instance of this class using a the number of milliseconds since January 1, 1970, 00:00:00 GMT.
     *
     * @param ms number of milliseconds since January 1, 1970, 00:00:00 GMT.
     */
    public OBCTimekeeper(long ms) {
		if (ms > 0) { HASVALUE = true; }
        Date DATE = new Date(ms);
        CALENDAR = Calendar.getInstance();
        CALENDAR.setTime(DATE);
    }

    /**
     * Constructs an instance of this class using a datecode (yyyymmdd) and a timecode (hhmm).
     *
     * @param datecode yyyymmdd formatted date (use 1 - 12 for month: January = 1).
     * @param timecode hhmm formatted time.
     */
    public OBCTimekeeper(int datecode, int timecode) {
		if (datecode > 0) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(DATEDECODE(datecode,"YEAR"), DATEDECODE(datecode,"MONTH")-1, DATEDECODE(datecode,"DAY"), TIMEDECODE(timecode,"HOUR_OF_DAY"), TIMEDECODE(timecode,"MINUTE"));
    }

    /**
     * Constructs an instance of this class using specified year, month and day.
     *
     * @param year the year to set current instance.
     * @param month the month to set current instance (use 1 - 12: January = 1).
     * @param day the day to set current instance.
     */
    public OBCTimekeeper(int year, int month, int day) {
		if (year > 0 || month > 0 || day > 0) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(year, month-1, day);
    }

    /**
     * Constructs an instance of this class using specified year, month, day, hour and minute.
     *
     * @param year the year to set current instance.
     * @param month the month to set current instance (use 1 - 12: January = 1).
     * @param day the day to set current instance.
     * @param hour the hour to set current instance.
     * @param minute the minute to set current instance.
     */
    public OBCTimekeeper(int year, int month, int day, int hour, int minute) {
		if (year > 0 || month > 0 || day > 0 || hour > 0 || minute > 0) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(year, month-1, day, hour, minute);
    }

    /**
     * Constructs an instance of this class using specified year, month, day, hour, minute, and seconds.
     *
     * @param year the year to set current instance.
     * @param month the month to set current instance (use 1 - 12: January = 1).
     * @param day the day to set current instance.
     * @param hour the hour to set current instance.
     * @param minute the minute to set current instance.
     * @param second the seconds to set current instance.
     */
    public OBCTimekeeper(int year, int month, int day, int hour, int minute, int second) {
		if (year > 0 || month > 0 || day > 0 || hour > 0 || minute > 0 || second > 0) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(year, month-1, day, hour, minute, second);
    }

    /**
     * Constructs an instance of this class using a <code>String</code> representation of a specified year, month and day.
     *
     * @param year the year to set current instance.
     * @param month the month to set current instance. You may use abbreviated month (Nov), full month (November), or numerical value of month (11).
     * @param day the day to set current instance.
     */
    public OBCTimekeeper(String year, String month, String day) {
		if (Operator.hasValue(year) || Operator.hasValue(month) || Operator.hasValue(day)) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        setYear(year);
        setMonth(month);
        setDay(day);
    }

    /**
     * Constructs an instance of this class using a <code>String</code> representation of a specified year, month, day and minute.
     *
     * @param year the year to set current instance.
     * @param month the month to set current instance. You may use abbreviated month (Nov), full month (November), or numerical value of month (11).
     * @param day the day to set current instance.
     * @param hour the hour to set current instance.
     * @param minute the minute to set current instance.
     */
    public OBCTimekeeper(String year, String month, String day, String hour, String minute) {
		if (Operator.hasValue(year) || Operator.hasValue(month) || Operator.hasValue(day) || Operator.hasValue(hour) || Operator.hasValue(minute)) { HASVALUE = true; }
        CALENDAR = Calendar.getInstance();
        setYear(year);
        setMonth(month);
        setDay(day);
        setHour(hour);
        setMinute(minute);
    }

/* #################################################################################################
##  SET DATECODE                                                                                  ##
################################################################################################# */

    /**
     * Set the date to reflect the specified datecode (yyyymmdd).
     *
     * @param datecode yyyymmdd formatted date.
     */
    public void setDate(int datecode) {
		if (datecode > 0) { HASVALUE = true; }
        CALENDAR.set(DATEDECODE(datecode,"YEAR"), DATEDECODE(datecode,"MONTH")-1, DATEDECODE(datecode,"DAY"));
    }

    /**
     * Set the date to reflect the specified <code>String</code> representation of a datecode ("yyyymmdd").
     *
     * @param datecode yyyymmdd formatted date.
     */
    public void setDate(String datecode) {
        if (Operator.hasValue(datecode)) {
        	
        	if (datecode.length() >= 19) {
        		setTimestamp(datecode);
        	}
        	else {
				datecode = Operator.replace(datecode, "-", "");
				datecode = Operator.replace(datecode, "/", "");
				datecode = Operator.replace(datecode, " ", "");
				int DATECODE = Operator.toInt(datecode, 10000101);
				
				CALENDAR.set(DATEDECODE(DATECODE,"YEAR"), DATEDECODE(DATECODE,"MONTH")-1, DATEDECODE(DATECODE,"DAY"));
        	}
        	HASVALUE = true;
        }
    }

    /**
     * Sets the instance of this class to a <code>Date</code> object.
     *
     * @param date Date object.
     */
    public void setDate(Date date) {
        CALENDAR = Calendar.getInstance();
        CALENDAR.setTime(date);
		HASVALUE = true;
    }

    /**
     * Set the date to reflect the same date in a specified <code>OBCTimekeeper</code>.
     *
     * @param t - another instance of OBCTimekeeper();
     */
	public void setDate(OBCTimekeeper t) {
		setDate(t.date());
	}

    /**
     * Set the date to reflect the specified <code>String</code> representation of a timestamp ("YYYY-MM-DD HH:MM:SS.MSMSMS").
     *
     * @param timestamp YYYY-MM-DD HH:MM:SS.MSMSMS formatted date.
     */
	public void setTimestamp(String timestamp) {
		try {
			if (Operator.hasValue(timestamp)) {
				String tr = Operator.replace(timestamp, " ","-");
				tr = Operator.replace(tr, "/","-");
				String[] datetime = Operator.split(tr, "-");
				if (Operator.hasValue(datetime) && datetime.length > 2) {
					setYear(datetime[0]);
					setMonth(datetime[1]);
					setDay(datetime[2]);
					
				}
				if (Operator.hasValue(datetime) && datetime.length > 3) {
					String[] t = Operator.split(datetime[3], ":");
					if (Operator.hasValue(t) && t.length > 2) {
						setHour(t[0]);
						setMinute(t[1]);
						String[] s = Operator.split(t[2], ".");
						if (Operator.hasValue(s) && s.length > 0) {
							setSecond(s[0]);
						}
					}
					else {
						String[] ts = Operator.split(datetime[3], ".");
						setHour(ts[0]);
						setMinute(ts[1]);
						setSecond(ts[2]);
					}
				}
				HASVALUE = true;
			}
		}
		catch (Exception e) { }
	}

/* #################################################################################################
##  SET/ADD MONTH                                                                                 ##
################################################################################################# */

    /**
     * Set the month to specified value.
     *
     * @param month the month to set current instance (use 1 - 12: January = 1)..
     */
    public void setMonth(int month) {
		if (month > 0) { HASVALUE = true; }
        if (month < 1) { month = 1; } else if (month > 12) { month = 12; }
        CALENDAR.set(Calendar.MONTH, month-1);
        if (MONTH() > month) {
        	setDay(1);
			CALENDAR.set(Calendar.MONTH, month-1);
			setDay(DAYS_IN_MONTH());
        }
    }

    /**
     * Set the month to specified value.
     *
     * @param month the month to set current instance. You may use abbreviated month (Nov), full month (November), or numerical value of month (11).
     */
    public void setMonth(String month) {
        if (!Operator.hasValue(month)) { }
        else if (Operator.isNumber(month)) {
            setMonth(Operator.toInt(month));
			HASVALUE = true;
        }
        else {
            for (int i = 0; i < 12; i++ ) {
                if (month.equalsIgnoreCase(MONTHS[i]) || month.equalsIgnoreCase(ABMONTHS[i])) {
                    setMonth(i+1);
					HASVALUE = true;
                    break;
                }
            }
        }
    }

    /**
     * Add specified amount of months to current instance.
     *
     * @param amount number of months to add.
     */
    public void addMonth(int amount) {
		if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.MONTH, amount);
    }

    /**
     * Add specified <code>String</code> representation of amount of months to current instance.
     *
     * @param amount number of months to add.
     */
    public void addMonth(String amount) {
		if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.MONTH, Operator.toInt(amount));
    }

/* #################################################################################################
##  SET/ADD DAY                                                                                   ##
################################################################################################# */

    /**
     * Set the day of month to specified value.
     *
     * @param day the day to set current instance (use 1 - 31).
     */
    public void setDay(int day) {
        if (day < 1) { day = 1; } else if (day > 31) { day = 31; } else { HASVALUE = true; }
        CALENDAR.set(Calendar.DAY_OF_MONTH, day);
    }

    /**
     * Set the day of month to specified value.
     *
     * @param day the day to set current instance (use 1 - 31).
     */
    public void setDay(String day) {
        if (Operator.hasValue(day) && Operator.isNumber(day)) {
            setDay(Operator.toInt(day));
            HASVALUE = true;
        }
    }

    public void setToLastDayOfMonth() {
        CALENDAR.set(Calendar.DAY_OF_MONTH, DAYS_IN_MONTH());
    }

    /**
     * Add specified amount of days to current instance.
     *
     * @param amount the amount of days to add.
     */
    public void addDay(int amount) {
		if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.DAY_OF_MONTH, amount);
    }

    /**
     * Add specified amount of days to current instance.
     *
     * @param amount the amount of days to add.
     */
    public void addDay(String amount) {
		if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.DAY_OF_MONTH, Operator.toInt(amount));
    }

/* #################################################################################################
##  SET/ADD WEEK                                                                                   ##
################################################################################################# */

    public void setWeekday(String weekday) {
    	if (weekday.equalsIgnoreCase("SUNDAY") || weekday.equalsIgnoreCase("SUN")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY); }
    	if (weekday.equalsIgnoreCase("MONDAY") || weekday.equalsIgnoreCase("MON")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY); }
    	if (weekday.equalsIgnoreCase("TUESDAY") || weekday.equalsIgnoreCase("TUE")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY); }
    	if (weekday.equalsIgnoreCase("WEDNESDAY") || weekday.equalsIgnoreCase("WED")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY); }
    	if (weekday.equalsIgnoreCase("THURSDAY") || weekday.equalsIgnoreCase("THU")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY); }
    	if (weekday.equalsIgnoreCase("FRIDAY") || weekday.equalsIgnoreCase("FRI")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY); }
    	if (weekday.equalsIgnoreCase("SATURDAY") || weekday.equalsIgnoreCase("SAT")) { CALENDAR.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY); }
    }

    public void setDayOfWeek(int dow) {
    	CALENDAR.set(Calendar.DAY_OF_WEEK, dow);
    }

    public void setWeekOfMonth(int week, String day) {
    	if (week > 5) { week = 5; }
    	int month = MONTH();
    	setWeekOfMonth(1);
    	setWeekday(day);
    	int add = 0;
    	if (MONTH() != month) {
    		add = 1;
    	}
    	setMonth(month);
    	setWeekday(day);
    	setWeekOfMonth(week+add);
    }

    public void setWeekOfMonth(int week) {
        CALENDAR.set(Calendar.WEEK_OF_MONTH, week);
    }

    public void setWeekOfMonth(String week) {
    	setWeekOfMonth(Operator.toInt(week));
    }

    public void setLastWeekOfMonth() {
    	String wday = weekday();
    	int month = MONTH();
    	int day = DAYS_IN_MONTH();
    	setDay(day);
    	OBCTimekeeper r = new OBCTimekeeper();
    	r.setDate(this);
    	r.setWeekday(wday);
    	if (r.MONTH() != month) {
    		r.addWeek(-1);
    	}
    	this.setDate(r);
    }

    public void addWeek(int week) {
    	addDay(7*week);
    }

    public void addWeek(String week) {
    	addWeek(Operator.toInt(week));
    }

/* #################################################################################################
##  SET/ADD YEAR                                                                                   ##
################################################################################################# */

    /**
     * Set the year to specified value.
     *
     * @param year the year to set current instance.
     */
    public void setYear(int year) {
		if (year > 0) { HASVALUE = true; }
        CALENDAR.set(Calendar.YEAR, year);
    }

    /**
     * Set the year to specified value.
     *
     * @param year the year to set current instance.
     */
    public void setYear(String year) {
        if (Operator.hasValue(year) && Operator.isNumber(year)) {
            setYear(Operator.toInt(year));
            HASVALUE = true;
        }
    }

    /**
     * Add specified amount of years to current instance.
     *
     * @param amount the amount of years to add.
     */
    public void addYear(int amount) {
		if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.YEAR, amount);
    }

    /**
     * Add specified amount of years to current instance.
     *
     * @param amount the amount of years to add.
     */
    public void addYear(String amount) {
		if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.YEAR, Operator.toInt(amount));
    }


/* #################################################################################################
##  SET TIME                                                                                      ##
################################################################################################# */

    /**
     * Set the time to specified value.
     *
     * @param time hhmm formatted time.
     */
    public void setTime(int time) {
		if (time > 0) { HASVALUE = true; }
        int hour = TIMEDECODE(time,"HOUR_OF_DAY");
        int minute = TIMEDECODE(time,"MINUTE");
        setHour(hour);
        setMinute(minute);
    }

    /**
     * Set the time to specified value.
     *
     * @param time hhmm formatted time.
     */
    public void setTime(String time) {
        if (Operator.hasValue(time) && Operator.isNumber(time)) {
            setTime(Operator.toInt(time));
            HASVALUE = true;
        }
    }

/* #################################################################################################
##  SET/ADD HOUR                                                                                  ##
################################################################################################# */

    /**
     * Set the hour to specified value.
     *
     * @param hour the hour to set current instance.
     */
    public void setHour(int hour) {
        if (hour < 0) { hour = 0; } else if (hour > 24) { hour = 24; } else { HASVALUE = true; }
        CALENDAR.set(Calendar.AM_PM,0);
        CALENDAR.set(Calendar.HOUR, hour);
    }

    /**
     * Set the hour to specified value.
     *
     * @param hour the hour to set current instance.
     */
    public void setHour(String hour) {
        if (Operator.hasValue(hour) && Operator.isNumber(hour)) {
            setHour(Operator.toInt(hour));
            HASVALUE = true;
        }
    }

    /**
     * Add specified amount of hours to current instance.
     *
     * @param amount the amount of hours to add.
     */
    public void addHour(int amount) {
    	if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.HOUR, amount);
    }

    /**
     * Add specified amount of hours to current instance.
     *
     * @param amount the amount of hours to add.
     */
    public void addHour(String amount) {
    	if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.HOUR, Operator.toInt(amount));
    }

/* #################################################################################################
##  SET/ADD MINUTE                                                                                ##
################################################################################################# */

    /**
     * Set the minute to specified value.
     *
     * @param minute the minute to set current instance.
     */
    public void setMinute(int minute) {
        if (minute < 0) { minute = 0; } else if (minute > 59) { minute = 0; } else { HASVALUE = true; }
        CALENDAR.set(Calendar.MINUTE, minute);
    }

    /**
     * Set the minute to specified value.
     *
     * @param minute the minute to set current instance.
     */
    public void setMinute(String minute) {
        if (Operator.hasValue(minute) && Operator.isNumber(minute)) {
            setMinute(Operator.toInt(minute));
            HASVALUE = true;
        }
    }

    /**
     * Add specified amount of minutes to current instance.
     *
     * @param amount the amount of minutes to add.
     */
    public void addMinute(int amount) {
    	if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.MINUTE, amount);
    }

    /**
     * Add specified amount of minutes to current instance.
     *
     * @param amount the amount of minutes to add.
     */
    public void addMinute(String amount) {
    	if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.MINUTE, Operator.toInt(amount));
    }

/* #################################################################################################
##  SET/ADD SECONDS                                                                                ##
################################################################################################# */

    /**
     * Set the second to specified value.
     *
     * @param second the second to set current instance.
     */
    public void setSecond(int second) {
        if (second < 0) { second = 0; } else if (second > 59) { second = 0; } else { HASVALUE = true; }
        CALENDAR.set(Calendar.SECOND, second);
    }

    /**
     * Set the second to specified value.
     *
     * @param second the second to set current instance.
     */
    public void setSecond(String second) {
        if (Operator.hasValue(second) && Operator.isNumber(second)) {
            setSecond(Operator.toInt(second));
            HASVALUE = true;
        }
    }

    /**
     * Add specified amount of seconds to current instance.
     *
     * @param amount the amount of seconds to add.
     */
    public void addSecond(int amount) {
    	if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.SECOND, amount);
    }

    /**
     * Add specified amount of seconds to current instance.
     *
     * @param amount the amount of seconds to add.
     */
    public void addSecond(String amount) {
    	if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.SECOND, Operator.toInt(amount));
    }

/* #################################################################################################
##  SET/ADD MILLISECONDS                                                                                ##
################################################################################################# */

    /**
     * Set the millisecond to specified value.
     *
     * @param milli the millisecond to set current instance.
     */
    public void setMilliSecond(int milli) {
        if (milli < 0) { milli = 0; } else { HASVALUE = true; }
        CALENDAR.set(Calendar.MILLISECOND, milli);
    }

    /**
     * Set the millisecond to specified value.
     *
     * @param milli the millisecond to set current instance.
     */
    public void setMilliSecond(String milli) {
        if (Operator.hasValue(milli) && Operator.isNumber(milli)) {
            setSecond(Operator.toInt(milli));
            HASVALUE = true;
        }
    }

    /**
     * Add specified amount of seconds to current instance.
     *
     * @param amount the amount of seconds to add.
     */
    public void addMilliSecond(int amount) {
    	if (amount > 0) { HASVALUE = true; }
        CALENDAR.add(Calendar.MILLISECOND, amount);
    }

    /**
     * Add specified amount of seconds to current instance.
     *
     * @param amount the amount of seconds to add.
     */
    public void addMilliSecond(String amount) {
    	if (Operator.hasValue(amount)) { HASVALUE = true; }
        CALENDAR.add(Calendar.MILLISECOND, Operator.toInt(amount));
    }

/* #################################################################################################
##  SET AMPM                                                                                      ##
################################################################################################# */

    /**
     * Set the ampm to specified value.
     *
     * @param ampm the am or pm to set current instance (am=0, pm=1).
     */
    public void setAmpm(int ampm) {
        if (ampm < 0) { ampm = 0; } else if (ampm > 1) { ampm = 1; }
        CALENDAR.set(Calendar.AM_PM,ampm);
    }

    /**
     * Set the ampm to specified value.
     *
     * @param ampm the am or pm to set current instance (use am/pm or am=0, pm=1).
     */
    public void setAmpm(String ampm) {
        if (Operator.hasValue(ampm)) { }
        else if (Operator.isNumber(ampm)) {
            setAmpm(Operator.toInt(ampm));
        }
        else if (ampm.equalsIgnoreCase("AM")) {
            setAmpm(0);
        }
        else if (ampm.equalsIgnoreCase("PM")) {
            setAmpm(1);
        }
    }

	/**
	 * Go to next upcomming date based on month.
	 *
	 * @param months the number of months to add to current date.
	 */
	public boolean nextByMonth(int months) {
		if (months == 0) { return false; }
		OBCTimekeeper today = new OBCTimekeeper();
		int todaint = today.YYYYMMDD();
		boolean x = true;
		while (x) {
			int currint = YYYYMMDD();
			if (months > 0) {
				if (currint >= todaint) {
					x = false;
					break;
				}
				else {
					addMonth(months);
				}
			}
			else {
				if (currint <= todaint) {
					x = false;
					break;
				}
				else {
					addMonth(months);
				}
			}
		}
		return true;
	}

	/**
	 * Go to next upcomming date based on year.
	 *
	 * @param years the number of years to add to current date.
	 */
	public boolean nextByYear(int years) {
		if (years == 0) { return false; }
		OBCTimekeeper today = new OBCTimekeeper();
		int todaint = today.YYYYMMDD();
		boolean x = true;
		while (x) {
			int currint = YYYYMMDD();
			if (years > 0) {
				if (currint >= todaint) {
					x = false;
					break;
				}
				else {
					addYear(years);
				}
			}
			else {
				if (currint <= todaint) {
					x = false;
					break;
				}
				else {
					addYear(years);
				}
			}
		}
		return true;
	}

/* #################################################################################################
##  STRINGS                                                                                       ##
################################################################################################# */

    /**
     * Returns the day of the week (Monday, Tuesday...) representing the current instance.
     */
    public String weekday()      { return DAYS[DAY_OF_WEEK() - 1]; }

    /**
     * Returns the month (January, February...) representing the current instance.
     */
    public String month()        { return MONTHS[MONTH() - 1]; }

    /**
     * Returns the abbreviated month (Jan, Feb...) representing the current instance.
     */
    public String abMonth()      { return ABMONTHS[MONTH() - 1]; }

	/**
	 * Returns the formal date representing the current instance.
	 *
	 * @return November 4, 1969
	 */
	public String formalDate()   { return month() + " " + DAY() + ", " + YEAR(); }

	/**
	 * Returns the month and day representing the current instance.
	 *
	 * @return November 4
	 */
	public String monthDay()   { return month() + " " + DAY(); }

	/**
	 * Returns the informal date representing the current instance.
	 *
	 * @return 11/4/1969
	 */
	public String informalDate() { return MONTH() + "/" + DAY() + "/" + YEAR(); }

	/**
	 * Returns the informal date representing the current instance.
	 *
	 * @return 11/4/1969
	 */
	public String informalDateTime() { return MONTH() + "/" + DAY() + "/" + YEAR() + " @ " + standardTime(); }

	/**
	 * Returns the full date representing the current instance.
	 *
	 * @return Tuesday, November 4, 1969
	 */
	public String fullDate()     { return weekday() + ", " + month() + " " + DAY() + ", " + YEAR(); }

	/**
	 * Returns the full date and time representing the current instance.
	 *
	 * @return Tuesday, November 4, 1969 @ 2:00PM
	 */
	public String fullDateTime()     { return weekday() + ", " + month() + " " + DAY() + ", " + YEAR() + " @ " + standardTime(); }

	/**
	 * Returns the full date and military time representing the current instance.
	 *
	 * @return Tuesday, November 4, 1969 @ 14:00
	 */
	public String fullDateMilitaryTime()     { return weekday() + ", " + month() + " " + DAY() + ", " + YEAR() + " @ " + militaryTime(); }

    /**
     * Returns the short date representing the current instance.
     *
     * @return Nov 4, 1969
     */
    public String shortDate()    { return abMonth() + " " + DAY() + ", " + YEAR(); }

    /**
     * Returns the standard time representing the current instance.
     *
     * @return 2:00PM
     */
    public String standardTime() { return HOUR() + ":" + minute() + amPm(); }

    /**
     * Returns the full standard time representing the current instance.
     *
     * @return 2:00:00PM
     */
    public String fullStandardTime() { return HOUR() + ":" + minute() + ":" + second() + amPm(); }

    /**
     * Returns the military time representing the current instance.
     *
     * @return 14:00
     */
    public String militaryTime() { return HOUR_OF_DAY() + ":" + minute(); }

    /**
     * Returns the full military time representing the current instance.
     *
     * @return 14:00:00
     */
    public String fullMilitaryTime() { return HOUR_OF_DAY() + ":" + minute() + ":" + second(); }

    /**
     * Returns the hour representing the current instance.
     *
     * @return 00
     */
    public String hour() {
        return Operator.toString(HOUR());
    }

	public String hh() {
		if (HOUR() < 10) { return "0" + HOUR(); }
		else { return Operator.toString(HOUR()); }
	}

	/**
	 * Returns the minute representing the current instance.
	 *
	 * @return 00
	 */
	public String minute() {
		if (MINUTE() < 10) { return "0" + MINUTE(); }
		else { return Operator.toString(MINUTE()); }
	}

	/**
	 * Returns the month representing the current instance.
	 *
	 * @return 00
	 */
	public String mm() {
		if (MONTH() < 10) { return "0" + MONTH(); }
		else { return Operator.toString(MONTH()); }
	}

	/**
	 * Returns the day representing the current instance.
	 *
	 * @return 00
	 */
	public String dd() {
		if (DAY() < 10) { return "0" + DAY(); }
		else { return Operator.toString(DAY()); }
	}

	/**
	 * Returns the day representing the current instance.
	 *
	 * @return 00
	 */
	public String yy() {
		if (YY() < 10) { return "0" + YY(); }
		else { return Operator.toString(YY()); }
	}

	/**
	 * Returns the day representing the current instance.
	 *
	 * @return 00
	 */
	public String yyyy() {
		int y = YEAR();
		StringBuffer sb = new StringBuffer();
		if (y < 10) { sb.append("200"); }
		else if (y < 100) { sb.append("20"); }
		else if (y < 1000) { sb.append("2"); }
		sb.append(y);
		return sb.toString();
	}

    /**
     * Returns the second representing the current instance.
     *
     * @return 00
     */
    public String second() {
        if (SECOND() < 10) { return "0" + SECOND(); }
        else { return Operator.toString(SECOND()); }
    }

    /**
     * Returns the am/pm representing the current instance.
     *
     * @return AM or PM
     */
    public String amPm() {
        if (AM_PM() == 0) { return "AM"; }
        else { return "PM"; }
    }

    /**
     * Returns a Date object representing the current instance.
     *
     * @return Date object
     */
    public Date date() {
    	return CALENDAR.getTime();
    }

    /**
	 * Return an sql statement formatted String representation of the current instance's date
	 * @return an sql statement formatted String representation of the current instance's date
	 */
	public String sql() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("DATE('");
		sb.append(mm());
		sb.append("/");
		sb.append(dd());
		sb.append("/");
		sb.append(yyyy());
		sb.append("')");
		return sb.toString();
	}

	/**
	 * Return an sql statement formatted String representation of the current instance's date to be used in Microsoft SQL Server
	 * @return an sql statement formatted String representation of the current instance's date to be used in Microsoft SQL Server
	 */
	public String sqlServer() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("('");
		sb.append(informalDate());
		sb.append("')");
		return sb.toString();
	}

	/**
	 * Return an sql statement formatted String representation of the current instance's time
	 * @return an sql statement formatted String representation of the current instance's time
	 */
	public String sqlTime() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("TIME('");
		sb.append(HOUR_OF_DAY());
		sb.append(":");
		sb.append(minute());
		sb.append("')");
		return sb.toString();
	}

	/**
	 * Return an sql statement formatted String representation of the current instance's time to be used in Microsoft SQL Server
	 * @return an sql statement formatted String representation of the current instance's time to be used in Microsoft SQL Server
	 */
	public String sqlServerTime() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("('");
		sb.append(HOUR_OF_DAY());
		sb.append(":");
		sb.append(minute());
		sb.append("')");
		return sb.toString();
	}


    /**
     * Returns the DT (vCal formatted Date Time) representing the current instance.
     *
     * @return vCal(19691104T140000) formatted String
     */
    public String DT() {

        StringBuffer DT = new StringBuffer(15);
        DT.append(YYYYMMDD());
        DT.append("T");

        int hour = HOUR_OF_DAY();
        if (hour < 10) { DT.append("0"); }
        DT.append(hour);

        int minute = MINUTE();
        if (minute < 10) { DT.append("0"); }
        DT.append(minute);

        DT.append("00");

        return DT.toString();

    }

    /**
     * Returns the fiscal year of the current instance based on the FISCAL_MONTH variable.
     *
     * @return fiscal year of the current instance
     */
	public int FISCAL() {
		if (MONTH() < FISCAL_MONTH) {
			return YEAR() - 1;
		}
		else {
			return YEAR();
		}
	}

	/**
	 * Get a timestamp formatted representation of the current instance
	 * @return timestamp formatted representation of the current instance
	 */
	public String timestamp() {

		StringBuffer TS = new StringBuffer(20);
		TS.append(YEAR());
		TS.append("-");

		int month = MONTH();
		if (month < 10) { TS.append("0"); }
		TS.append(month);
		TS.append("-");

		int day = DAY();
		if (day < 10) { TS.append("0"); }
		TS.append(day);
		TS.append("-");

		int hour = HOUR_OF_DAY();
		if (hour < 10) { TS.append("0"); }
		TS.append(hour);
		TS.append(".");

		int minute = MINUTE();
		if (minute < 10) { TS.append("0"); }
		TS.append(minute);
		TS.append(".");

		int second = SECOND();
		if (second < 10) { TS.append("0"); }
		TS.append(second);

		return TS.toString();

	}

	/**
	 * Return an sql statement formatted String representation of the current instance's timestamp
	 * @return an sql statement formatted String representation of the current instance's timestamp
	 */
	public String sqlTimestamp() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("TIMESTAMP('");
		sb.append(timestamp());
		sb.append("')");
		return sb.toString();
	}

	/**
	 * Return an sql statement formatted String representation of the current instance's datetime
	 * @return an sql statement formatted String representation of the current instance's datetime
	 */
	public String sqlDatetime() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("('");
		sb.append(DATECODE());
		sb.append(" ");
		sb.append(fullMilitaryTime());
		sb.append("')");
		return sb.toString();
	}
	
	public String oracleTimestamp() {
		StringBuffer sb = new StringBuffer(20);
		sb.append("TIMESTAMP '");
		sb.append(getString("YYYY-MM-DD"));
		sb.append(" ");
		sb.append(fullMilitaryTime());
		sb.append("'");
		return sb.toString();
	}


/* #################################################################################################
##  INT                                                                                           ##
################################################################################################# */

    /**
     * Returns the maximum number of days for current month.
     */
    public int DAYS_IN_MONTH()  { return CALENDAR.getActualMaximum(Calendar.DAY_OF_MONTH); }

	/**
	 * Return the index of the first weekday of the month
	 * @return the index of the first weekday of the month (1 - Sunday, 7 - Saturday)
	 */
	public int FIRST_DAY_OF_MONTH() {
		OBCTimekeeper date = new OBCTimekeeper(YEAR(), MONTH(), 1);
		return date.DAY_OF_WEEK();
	}

	/**
	 * Return the index of the last weekday of the month
	 * @return the index of the last weekday of the month (1 - Sunday, 7 - Saturday)
	 */
	public int LAST_DAY_OF_MONTH() {
		OBCTimekeeper date = new OBCTimekeeper();
		date.setYear(YEAR());
		date.setMonth(MONTH());
		date.setDay(DAYS_IN_MONTH());
		return date.DAY_OF_WEEK();
	}

	/**
	 * Get the number of calendar rows
	 * @return the number of calendar rows
	 */
	public int NUMBER_OF_ROWS() {
		OBCTimekeeper date = new OBCTimekeeper(YEAR(), MONTH(), 1);
		int f = date.FIRST_DAY_OF_MONTH();
		int t = date.DAYS_IN_MONTH() + f - 1;
		double r = Math.ceil((double) t / 7);
		return (int) r;
	}

    /**
     * Returns the day of current instance.
     */
    public int DAY()            { return CALENDAR.get(Calendar.DAY_OF_MONTH); }

    /**
     * Returns the day of the week of current instance.
     */
    public int DAY_OF_WEEK()    { return CALENDAR.get(Calendar.DAY_OF_WEEK); }

    /**
     * Returns the week in the month of current instance.
     */
    public int WEEK_OF_MONTH()  { return CALENDAR.get(Calendar.WEEK_OF_MONTH); }

    /**
     * Returns the month of current instance (1 - 12: January = 1).
     */
    public int MONTH()          { return CALENDAR.get(Calendar.MONTH) + 1; }

    /**
     * Returns the year of current instance.
     */
    public int YEAR()           { return CALENDAR.get(Calendar.YEAR); }

    /**
     * Returns the minute of current instance.
     */
    public int MINUTE()         { return CALENDAR.get(Calendar.MINUTE); }

    /**
     * Returns the hour of day of current instance (military hour: 0 - 23).
     */
    public int HOUR_OF_DAY()    { return CALENDAR.get(Calendar.HOUR_OF_DAY); }

    /**
     * Returns the second of current instance.
     */
    public int SECOND()         { return CALENDAR.get(Calendar.SECOND); }

    /**
     * Returns the am (0) or pm (1) of current instance.
     */
    public int AM_PM()          { return CALENDAR.get(Calendar.AM_PM); }

    /**
     * Returns the datecode (yyyymmdd) of current instance.
     */
    public int DATECODE()       {
//    	if (!hasValue()) { return -1; }
    	return (YEAR() * 10000) + (MONTH() * 100) + DAY();
    }

	public long FULLDATECODE()       {
//		if (!hasValue()) { return -1; }
		return ((long) YEAR() * 100000000) + ((long) MONTH() * 1000000) + ((long) DAY() * 10000) + ((long) HOUR_OF_DAY() * 100) + ((long) MINUTE());
	}

	/**
	 * Returns the formatted time of current instance.
	 */
	public int HHMM()           { return (HOUR_OF_DAY() * 100) + MINUTE(); }

	/**
	 * Returns the formatted time of current instance.
	 */
	public int HHMMSS()          { return (HOUR_OF_DAY() * 10000) + (MINUTE() * 100) + SECOND(); }

    /**
     * Returns the formatted date of current instance.
     */
    public int MMYYYY()         { return (MONTH() * 10000) + YEAR(); }

    /**
     * Returns the formatted date of current instance.
     */
    public int YYYYMM()         { return (YEAR() * 10000) + (MONTH() * 100); }
    /**
     * Returns the formatted date of current instance.
     */
    public int MMDDYY()         { return (MONTH() * 10000) + (DAY() * 100) + YY(); }

    /**
     * Returns the formatted date of current instance.
     */
    public int MMDDYYYY()       { return (MONTH() * 1000000) + (DAY() * 10000) + YEAR(); }

    /**
     * Returns the formatted date of current instance.
     */
    public int YYYYMMDD()       { return (YEAR() * 10000) + (MONTH() * 100) + DAY(); }

    /**
     * Returns the formatted year of current instance.
     */
    public int YY() {
        String year = Operator.toString(YEAR());
        String yy = year.substring(year.length()-2,year.length());
        return Operator.toInt(yy);
    }

    /**
     * Returns the hour of current instance (standard time: 1 -12).
     */
    public int HOUR() {
        int hour = CALENDAR.get(Calendar.HOUR);
        if (hour == 0) { hour = 12; }
        return hour;
    }

/* #################################################################################################
##  LONG                                                                                          ##
################################################################################################# */

    /**
     * Returns the timestamp of current instance.
     */
    public long MILLISECONDS()   {
        Date DATE = CALENDAR.getTime();
        return DATE.getTime();
    }

    /**
     * Returns the age of specified value based on current instance.
     */
    public long AGE(long before) {

        Date BEFOREDATE = CALENDAR.getTime();
        long AFTER = BEFOREDATE.getTime();
        return AFTER - before;

    }

    /**
     * Returns the age of specified value based on current instance.
     */
    public long AGE(OBCTimekeeper before) {

        long BEFORE = before.MILLISECONDS();
        long AFTER = MILLISECONDS();
        return AFTER - BEFORE;

    }

	/**
	 * Returns the age of current instance.
	 */
	public long SECONDS_SINCE()       { return SECONDS_SINCE(new OBCTimekeeper()); }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long SECONDS_SINCE(long date)       { return AGE(date)/1000; }

    /**
     * Returns the age of specified value based on current instance.
     */
    public long SECONDS_SINCE(OBCTimekeeper date) { return AGE(date)/1000; }

	/**
	 * Returns the age of current instance.
	 */
	public long MINUTES_SINCE()       { return MINUTES_SINCE(new OBCTimekeeper()); }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long MINUTES_SINCE(long date)       { return AGE(date)/60000; }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long MINUTES_SINCE(OBCTimekeeper date) { return AGE(date)/60000; }

	/**
	 * Returns the age of current instance.
	 */
	public long HOURS_SINCE()         { return HOURS_SINCE(new OBCTimekeeper()); }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long HOURS_SINCE(long date)         { return AGE(date)/3600000; }

    /**
     * Returns the age of specified value based on current instance.
     */
    public long HOURS_SINCE(OBCTimekeeper date)   { return AGE(date)/3600000; }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long DAYS_SINCE()          { return DAYS_SINCE(new OBCTimekeeper()); }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long DAYS_SINCE(long date)          { return AGE(date)/86400000; }

    /**
     * Returns the age of specified value based on current instance.
     */
    public long DAYS_SINCE(OBCTimekeeper date)    { return AGE(date)/86400000; }

	public int DAYS_TILL() { return DAYS_TILL(new OBCTimekeeper()); }

	public int DAYS_TILL(OBCTimekeeper date) {
		date.setHour(0);
		date.setMinute(0);
		date.setMilliSecond(0);
		OBCTimekeeper t = new OBCTimekeeper();
		t.setDate(DATECODE());
		t.setHour(0);
		t.setMinute(0);
		t.setMilliSecond(0);
//		return t.FULLDATECODE();
		return (int) t.DAYS_SINCE(date);
	}

	/**
	 * Returns the age of current instance.
	 */
	public long WEEKS_SINCE()         { return WEEKS_SINCE(new OBCTimekeeper()); }

	/**
	 * Returns the age of specified value based on current instance.
	 */
	public long WEEKS_SINCE(long date)         { return AGE(date)/1036800000; }

    /**
     * Returns the age of specified value based on current instance.
     */
    public long WEEKS_SINCE(OBCTimekeeper date)   { return AGE(date)/1036800000; }

	/**
	 * Check if current instance is greater than specified OBCTimekeeper
	 * @param date - the object to compare
	 * @return true if current instance is greater than specified OBCTimekeeper
	 */
	public boolean greaterThan(OBCTimekeeper date)          { return FULLDATECODE() > date.FULLDATECODE(); }

	/**
	 * Check if current instance is greater than or equal to specified OBCTimekeeper
	 * @param date - the object to compare
	 * @return true if current instance is greater than or equal to specified OBCTimekeeper
	 */
	public boolean greaterThanOrEqualTo(OBCTimekeeper date) { return FULLDATECODE() >= date.FULLDATECODE(); }

	/**
	 * Check if current instance is less than specified OBCTimekeeper
	 * @param date - the object to compare
	 * @return true if current instance is less than specified OBCTimekeeper
	 */
	public boolean lessThan(OBCTimekeeper date)             { return FULLDATECODE() < date.FULLDATECODE(); }

	/**
	 * Check if current instance is less than or equal to specified OBCTimekeeper
	 * @param date - the object to compare
	 * @return true if current instance is less than or equal to specified OBCTimekeeper
	 */
	public boolean lessThanOrEqualTo(OBCTimekeeper date)    { return FULLDATECODE() <= date.FULLDATECODE(); }

	/**
	 * Check if current instance is equal to specified OBCTimekeeper
	 * @param date - the object to compare
	 * @return true if current instance is equal to specified OBCTimekeeper
	 */
	public boolean equalTo(OBCTimekeeper date)              { return FULLDATECODE() == date.FULLDATECODE(); }

	/**
	 * Check if current instance is the same day as specified OBCTimekeeper
	 * @param date - the object to compare
	 * @return true if current instance is the same day as specified OBCTimekeeper
	 */
	public boolean isDay(OBCTimekeeper date) {
		return DATECODE() == date.DATECODE();
	}

	/**
	 * Check if current instance occurs in the future
	 * @return true if current instance occurs in the future
	 */
	public boolean upcomming() {
		OBCTimekeeper today = new OBCTimekeeper();
		return greaterThan(today);
	}

	/**
	 * Check if current instance occurs in the past
	 * @return true if current instance occurs in the past
	 */
	public boolean past() {
		OBCTimekeeper today = new OBCTimekeeper();
		return lessThan(today);
	}

	public static int daysBetween(Date d1, Date d2){
		return (int)((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
	}


/* #################################################################################################
##  OBJECTS                                                                                       ##
################################################################################################# */

	public ArrayList<OBCTimekeeper> daysInWeek(String[] weekdays) {
		return daysInWeek(weekdays, null, null);
	}

	public ArrayList<OBCTimekeeper> daysInWeek(String[] weekdays, OBCTimekeeper excludebefore, OBCTimekeeper excludeafter) {
		ArrayList<OBCTimekeeper> r = new ArrayList<OBCTimekeeper>();
		OBCTimekeeper c = new OBCTimekeeper();
		c.setDate(this);
		c.setDayOfWeek(1);
    	for (int i=0; i < weekdays.length; i++) {
    		c.setWeekday(weekdays[i]);
    		if ((excludebefore == null || c.greaterThanOrEqualTo(excludebefore)) && (excludeafter == null || c.lessThanOrEqualTo(excludeafter))) {
        		OBCTimekeeper w = new OBCTimekeeper();
        		w.setDate(c);
        		r.add(w);
    		}
    	}
    	return r;
	}


/* #################################################################################################
##  GET                                                                                           ##
################################################################################################# */

    /**
     * Returns formatted value of current instance. Available formats include (using Tuesday, November
     * 4, 1969 at 2:00PM):<br><br>
     *
     * getString("WEEKDAY"): Tuesday<br>
     * getString("DAY"): 4<br>
     * getString("MONTH"): November<br>
     * getString("ABMONTH"): Nov<br>
     * getString("INTMONTH"): 11 (January = 1)<br>
     * getString("YEAR"): 1969<br><br>
     *
     * getString("TIME"): 2:00PM<br>
     * getString("MILITARYTIME"): 14:00<br>
     * getString("HOUR"): 2<br>
     * getString("HOUR_OF_DAY"): 14<br>
     * getString("MINUTE"): 00<br>
     * getString("SECOND"): 00<br>
     * getString("AMPM"): PM<br>
     * getString("_AMPM"): 1<br><br>
     *
     * getString("DATE"): November 4, 1969<br>
     * getString("FULLDATE"): Tuesday, November 4, 1969<br>
     * getString("SHORTDATE"): Nov 4, 1969<br>
     * getString("DATECODE"): 19691104<br>
     * getString("DT"): 19691104T140000<br><br>
     *
     * getString("MMDDYYYY"): 11/4/1969<br>
     * getString("MM/DD/YYYY"): 11/4/1969<br>
     * getString("MM-DD-YYYY"): 11-4-1969<br>
     * getString("MM-DD-YYYY @ HH:MM"): 11-4-1969 @ 12:00<br>
     * getString("MMDDYY"): 11/4/69<br>
     * getString("MM/DD/YY"): 11/4/69<br>
     * getString("MM-DD-YY"): 11-4-69<br>
     * getString("MM"): 11 (January = 01)<br>
     * getString("DD"): 04<br>
     * getString("YY"): 69<br>
     * getString("YYYY"): 1969<br>
     * getString("YYYY/MM/DD"): 1969/11/4<br>
     * getString("YYYY-MM-DD"): 1969-11-4<br>
     * getString("YYYY/NN"): 1969/70<br>
     */
    public String getString(String format) {

		if (format.equalsIgnoreCase("WEEKDAY"))           { return weekday(); }
		else if (format.equalsIgnoreCase("UCWEEKDAY"))    { return weekday().toUpperCase(); }
        else if (format.equalsIgnoreCase("DAY"))          { return Operator.toString(DAY()); }

		else if (format.equalsIgnoreCase("MONTH"))        { return month(); }
		else if (format.equalsIgnoreCase("MONTH_YEAR"))   { return month() + ", " + Operator.toString(YEAR()); }
		else if (format.equalsIgnoreCase("UCMONTH"))      { return month().toUpperCase(); }
        else if (format.equalsIgnoreCase("INTMONTH"))     { return Operator.toString(MONTH()); }
        else if (format.equalsIgnoreCase("ABMONTH"))      { return abMonth(); }
        else if (format.equalsIgnoreCase("YEAR"))         { return Operator.toString(YEAR()); }

        else if (format.equalsIgnoreCase("TIME"))         { return standardTime(); }
        else if (format.equalsIgnoreCase("MILITARYTIME")) { return militaryTime(); }
        else if (format.equalsIgnoreCase("HOUR"))         { return Operator.toString(HOUR()); }
        else if (format.equalsIgnoreCase("HOUR_OF_DAY"))  { return Operator.toString(HOUR_OF_DAY()); }
        else if (format.equalsIgnoreCase("MINUTE"))       { return minute(); }
        else if (format.equalsIgnoreCase("SECOND"))       { return second(); }
        else if (format.equalsIgnoreCase("AMPM"))         { return amPm(); }
        else if (format.equalsIgnoreCase("_AMPM"))        { return Operator.toString(AM_PM()); }

		else if (format.equalsIgnoreCase("DATE"))         { return formalDate(); }
		else if (format.equalsIgnoreCase("MONTHDAY"))     { return monthDay(); }
		else if (format.equalsIgnoreCase("FULLDATE"))     { return fullDate(); }
		else if (format.equalsIgnoreCase("FULLDATETIME")) { return fullDateTime(); }
		else if (format.equalsIgnoreCase("FULLDATEMILITARYTIME")) { return fullDateMilitaryTime(); }
        else if (format.equalsIgnoreCase("SHORTDATE"))    { return shortDate(); }

		else if (format.equalsIgnoreCase("DATECODE"))     { return Operator.toString(DATECODE()); }
        else if (format.equalsIgnoreCase("DT"))           { return DT(); }

		else if (format.equalsIgnoreCase("MMDDYYYY"))     { return mm() + "/" + dd() + "/" + YEAR(); }
		else if (format.equalsIgnoreCase("MM/DD/YYYY"))   { return mm() + "/" + dd() + "/" + YEAR(); }
		else if (format.equalsIgnoreCase("MM-DD-YYYY"))   { return mm() + "-" + dd() + "-" + YEAR(); }
		else if (format.equalsIgnoreCase("MM-DD-YYYY @ HH:MM"))   { return mm() + "-" + dd() + "-" + YEAR() + " @ "+ standardTime(); }
		else if (format.equalsIgnoreCase("MM/DD/YYYY @ HH:MM"))   { return mm() + "/" + dd() + "/" + YEAR() + " @ "+ standardTime(); }
		else if (format.equalsIgnoreCase("MM-DD-YYYY @ HH:MM:SS"))   { return mm() + "-" + dd() + "-" + YEAR() + " @ "+ fullStandardTime(); }
		else if (format.equalsIgnoreCase("MM/DD/YYYY @ HH:MM:SS"))   { return mm() + "/" + dd() + "/" + YEAR() + " @ "+ fullStandardTime(); }
		else if (format.equalsIgnoreCase("MM-DD-YYYY HH:MM:SS"))   { return mm() + "-" + dd() + "-" + YEAR() + " "+ fullMilitaryTime(); }
		else if (format.equalsIgnoreCase("MM/DD/YYYY HH:MM:SS"))   { return mm() + "/" + dd() + "/" + YEAR() + " "+ fullMilitaryTime(); }
		else if (format.equalsIgnoreCase("MMDDYY"))       { return mm() + "/" + dd() + "/" + yy(); }
		else if (format.equalsIgnoreCase("MM/DD/YY"))     { return mm() + "/" + dd() + "/" + yy(); }
		else if (format.equalsIgnoreCase("MM-DD-YY"))     { return mm() + "-" + dd() + "-" + yy(); }
        else if (format.equalsIgnoreCase("YYYY"))         { return Operator.toString(YEAR()); }
		else if (format.equalsIgnoreCase("YYYY-MM-DD"))   { return YEAR() + "-" + mm() + "-" + dd(); }
		else if (format.equalsIgnoreCase("YYYYMMDD"))     { return YEAR() + mm() + dd(); }
		else if (format.equalsIgnoreCase("YYYYMM"))       { return YEAR() + mm(); }
		else if (format.equalsIgnoreCase("YYYY/MM/DD"))   { return YEAR() + "/" + mm() + "/" + dd(); }
		else if (format.equalsIgnoreCase("YYYY/NN"))      {
			int y = YEAR();
			StringBuffer sb = new StringBuffer();
			sb.append(y);
			sb.append("/");
			y = y + 1;
			String n = Operator.getEndString(Operator.toString(y), 2);
			sb.append(n);
			return sb.toString();		
		}
		else if (format.equalsIgnoreCase("FULLDATECODE")) {
			StringBuffer h = new StringBuffer();
			h.append(Operator.toString(DATECODE()));
			int hh = HOUR_OF_DAY();
			if (hh < 10) { h.append("0"); }
			h.append(Operator.toString(hh));
			h.append(minute());
			return h.toString();
		}
		else if (format.equalsIgnoreCase("TIMECODE")) {
			StringBuffer h = new StringBuffer();
			int hh = HOUR_OF_DAY();
			if (hh < 10) { h.append("0"); }
			h.append(Operator.toString(hh));
			h.append(minute());
			return h.toString();
		}
		else if (format.equalsIgnoreCase("HH")) {
			int hh = HOUR_OF_DAY();
			if (hh < 10) { return "0" + hh; }
			else { return Operator.toString(hh); }
		}

		else if (format.equalsIgnoreCase("FISCAL")) {
			int m = MONTH();
			if (m < 7) {
				return (YEAR()-1) + " - " + YEAR();
			}
			else {
				return YEAR() + " - " + (YEAR()+1);
			}
		}

        else if (format.equalsIgnoreCase("MM")) {
            int MM = MONTH();
            if (MM < 10) { return "0" + MM; }
            else { return Operator.toString(MM); }
        }
        else if (format.equalsIgnoreCase("YY")) {
            int YY = YY();
            if (YY < 10) { return "0" + YY; }
            else { return Operator.toString(YY); }
        }
        else if (format.equalsIgnoreCase("DD")) {
            int DD = DAY();
            if (DD < 10) { return "0" + DD; }
            else { return Operator.toString(DD); }
        }

        else {
        	SimpleDateFormat f = new SimpleDateFormat(format);
        	return f.format(CALENDAR.getTime());
        }

    }

/* #################################################################################################
##  DECODE                                                                                        ##
################################################################################################# */

    /**
     * Convert timecode to specified type.
     *
     * @param code hhmm formatted time
     * @param type type of request (HOUR, HOUR_OF_DAY, MINUTE)
     */
    public static int TIMEDECODE(int code, String type) {
        String codestring = Operator.toString(code);
        if (codestring.length() == 1) {
            StringBuffer CODESTRING = new StringBuffer(4);
            CODESTRING.append("000").append(codestring);
            codestring = CODESTRING.toString();
        }
        else if (codestring.length() == 2) {
            StringBuffer CODESTRING = new StringBuffer(4);
            CODESTRING.append("00").append(codestring);
            codestring = CODESTRING.toString();
        }
        else if (codestring.length() == 3) {
            StringBuffer CODESTRING = new StringBuffer(4);
            CODESTRING.append("0").append(codestring);
            codestring = CODESTRING.toString();
        }
        String decoded = "";
        if (type.equals("HOUR_OF_DAY")) {
            if (codestring.length() > 1) {
                decoded = codestring.substring(0,2);
                return Operator.toInt(decoded);
            }
        }
        else if (type.equals("HOUR")) {
            if (codestring.length() > 1) {
                decoded = codestring.substring(0,2);
                int DECODED = Operator.toInt(decoded);
                if (DECODED > 12) { return DECODED - 12; }
                else { return DECODED; }
            }
        }
        else if (type.equals("MINUTE")) {
            if (codestring.length() > 3) {
                decoded = codestring.substring(2,4);
                return Operator.toInt(decoded);
            }
        }
        return 0;
    }

    /**
     * Convert datecode to specified type.
     *
     * @param code yyyymmdd formatted time
     * @param type type of request (YEAR, YY, MONTH, DAY)
     */
    public static int DATEDECODE(int code, String type) {
        String codestring = Operator.toString(code);
        String decoded = "";
        if (type.equals("YEAR")) {
            if (codestring.length() > 3) {
                decoded = codestring.substring(0,4);
                return Operator.toInt(decoded);
            }
        }
        else if (type.equals("YY")) {
            if (codestring.length() > 3) {
                decoded = codestring.substring(2,4);
                return Operator.toInt(decoded);
            }
        }
        else if (type.equals("MONTH")) {
            if (codestring.length() > 5) {
                decoded = codestring.substring(4,6);
				if (Operator.toInt(decoded) == 0) {
					decoded = "1";
				}
                return Operator.toInt(decoded);
            }
        }
        else if (type.equals("DAY")) {
            if (codestring.length() > 7) {
                decoded = codestring.substring(6,8);
                if (Operator.toInt(decoded) == 0) {
                	decoded = "1";
                }
                return Operator.toInt(decoded);
            }
        }
        return 0;
    }

	/* #################################################################################################
	##  HOUSEKEEPING                                                                                  ##
	################################################################################################# */

	/**
	 * Determine if a value has been given to this instance
	 * @return true if setxxx or addxxx has been called
	 */
	public boolean hasValue() {
		return HASVALUE;
	}

	/* #################################################################################################
	##  STATIC                                                                                        ##
	################################################################################################# */

	/**
	 * Return the number of days in specified year and month
	 * @param year
	 * @param month
	 * @return the number of days in specified year and month
	 */
	public static int DAYS_IN_MONTH(int year, int month) {
		OBCTimekeeper date = new OBCTimekeeper(year, month, 1);
		return date.DAYS_IN_MONTH();
	}

	/**
	 * Return the index of the first weekday of the month for the specified year and month
	 * @param year
	 * @param month
	 * @return the index of the first weekday of the month for the specified year and month (1 - Sunday, 7 - Saturday)
	 */
	public static int FIRST_DAY_OF_MONTH(int year, int month) {
		OBCTimekeeper date = new OBCTimekeeper();
		date.setYear(year);
		date.setMonth(month);
		date.setDay(1);
		return date.DAY_OF_WEEK();
	}

	/**
	 * Return the index of the last weekday of the month for the specified year and month
	 * @param year
	 * @param month
	 * @return the index of the last weekday of the month for the specified year and month (1 - Sunday, 7 - Saturday)
	 */
	public static int LAST_DAY_OF_MONTH(int year, int month) {
		OBCTimekeeper date = new OBCTimekeeper();
		date.setYear(year);
		date.setMonth(month);
		date.setDay(DAYS_IN_MONTH(year, month));
		return date.DAY_OF_WEEK();
	}

	/**
	 * Return the weekday that the first day of the specified year and month falls
	 * @param year
	 * @param month
	 * @return the weekday that the first day of the specified year and month falls
	 */
	public static String firstDayOfMonth(int year, int month) {
		return OBCTimekeeper.DAYS[OBCTimekeeper.FIRST_DAY_OF_MONTH(year, month) - 1];
	}

	/**
	 * Return the weekday that the last day of the specified year and month falls
	 * @param year
	 * @param month
	 * @return the weekday that the last day of the specified year and month falls
	 */
	public static String lastDayOfMonth(int year, int month) {
		return OBCTimekeeper.DAYS[OBCTimekeeper.LAST_DAY_OF_MONTH(year, month) - 1];
	}

	/**
	 * Return the current year
	 * @return the current year
	 */
	public static int CURRENTYEAR() {
		OBCTimekeeper date = new OBCTimekeeper();
		return date.YEAR();
	}

	/**
	 * Return the current day
	 * @return the current dat
	 */
	public static int CURRENTDAY() {
		OBCTimekeeper date = new OBCTimekeeper();
		return date.DAY();
	}

	/**
	 * Return the current month
	 * @return the current month
	 */
	public static int CURRENTMONTH() {
		OBCTimekeeper date = new OBCTimekeeper();
		return date.MONTH();
	}

	/**
	 * Return the current date in the specified format. Check documentation for the getString() method for available formats.
	 * @param format - the format to return. Check documentation for the getString() method for available formats.
	 * @return the current date in the specified format.
	 */
	public static String getCurrent(String format) {
		OBCTimekeeper date = new OBCTimekeeper();
		return date.getString(format);
	}

	/**
	 * Get the number of calendar rows for the specified year and month
	 * @param year
	 * @param month
	 * @return the number of calendar rows for the specified year and month
	 */
	public static int NUMBER_OF_ROWS(int year, int month) {
		int f = OBCTimekeeper.FIRST_DAY_OF_MONTH(year, month);
		int t = OBCTimekeeper.DAYS_IN_MONTH(year, month) + f - 1;
		double r = Math.ceil((double) t / 7);
		return (int) r;
	}

	/**
	 * Get input field for html form
	 * @param form - Name of form
	 * @param field - Name of field
	 * @param showtime - Show time
	 * @param required - if true, field is a required field
	 * @param classname - class name for style sheet
	 * @return html format form input field
	 */
	/*public String form(String form, String field, boolean showtime, boolean required, String classname) {
		String f = DBFormJs.dateChooser(form, field, this, showtime, required, classname);
		return f;
	}
*/






}


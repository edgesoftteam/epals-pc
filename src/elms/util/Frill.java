package elms.util;

import java.io.*;
import java.util.*;
import java.net.*;

/**
 * Frill parses a <code>String</code> so that it may be used as a template for a java application.
 * It works by iterating through the specified <code>String</code>, pausing when it encounters
 * a known tag allowing the application developer to create code in the place of the tag.<br><br>
 *
 * Known tags can be either a simple tag:<br><br>
 *
 * <pre>
 * &lt;APP [FIELD] = [VALUE] /&gt;
 * </pre>
 *
 * <p>or a complex tag:</p>
 *
 * <pre>
 * &lt;APP [FIELD] = [VALUE] &gt;
 *   Hello World
 * &lt;/APP [FIELD]&gt;
 * </pre>
 *
 * <hr>
 *
 * Below is a proper use example.<br><br>
 *
 * <pre>
 * Cloak CLOAK = new Cloak("http://www.url.com/template.html");
 * while (CLOAK.next()) {
 *     out.print(CLOAK.LINE);
 *     if (CLOAK.equals("FIELD")) {
 *         out.print("The value is " + CLOAK.VALUE);
 *     }
 * }
 * CLOAK.clear();
 * </pre>
 *
 * @author Alain Romero
 *
 */
public class Frill {

    /**
     * Original <code>String</code> specified during construction of the class.
     */
    public String ORIGINAL = "";

    /**
     * Next line.
     */
    public String NEXT = "";

    /**
     * Current tag field.
     */
    public String FIELD = "";

    /**
     * Current tag value.
     */
    public String VALUE = "";

    /**
     * Current tag frill.
     */
    public String FRILL = "";

    /**
     * Current line.
     */
    public String LINE  = null;


/* #################################################################################################
##  CONSTRUCTORS                                                                                  ##
################################################################################################# */

    /**
     * Constructs an instance of this class with specified <code>String</code>.
     *
     * <p>A <code>null</code> will empty string.
     *
     * @param The <code>String</code> to use as template.
     */
    public Frill(String frill) {
        if (frill == null) { frill = ""; }
        this.ORIGINAL = frill;
        this.NEXT = frill;
    }


/* #################################################################################################
##  ITERATE                                                                                       ##
################################################################################################# */

    /**
     * Iterate through the template and process the next line.
     *
     * @return Return <code>false</code> at end of template.
     */
    public boolean next() {

        if (!Operator.hasValue(NEXT)) {
            this.NEXT = ORIGINAL;
            return false;
        }
        containsTag(NEXT);
        return true;

    }

    /**
     * Get the current line.
     *
     * @return current line.
     */
    public String getString() {
        if (Operator.hasValue(LINE)) { return LINE; }
        else { return ""; }
    }

    /**
     * Get the current tag field.
     *
     * @return current field.
     */
    public String getField()  {
        if (Operator.hasValue(FIELD)) { return FIELD; }
        else { return ""; }
    }

    /**
     * Get the current tag value.
     *
     * @return current value.
     */
    public String getValue()  {
        if (Operator.hasValue(VALUE)) { return VALUE; }
        else { return ""; }
    }

    /**
     * Check if current tag field is equal to specified string.
     *
     * @param field The string to compare to the current tag field.
     * @return Return <code>true</code> if current tag field is equal to specified string.
     */
    public boolean equals(String field) {
        return fieldEquals(field);
    }

    /**
     * Check if current tag field and tag value are equal to specified strings.
     *
     * @param field The string to compare to the current tag field.
     * @param value The string to compare to the current tag value.
     * @return Return <code>true</code> if current tag field and tag value are equal to specified strings.
     */
    public boolean equals(String field, String value) {
        if (fieldEquals(field) && valueEquals(value)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag field and tag value are equal to specified strings.
     *
     * @param field The string to compare to the current tag field.
     * @param value The string to compare to the current tag value.
     * @return Return <code>true</code> if current tag field and tag value are equal to specified strings.
     */
    public boolean tagEquals(String field, String value) {
        if (fieldEquals(field) && valueEquals(value)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag field is equal to specified string.
     *
     * @param field The string to compare to the current tag field.
     * @return Return <code>true</code> if current tag field is equal to specified string.
     */
    public boolean fieldEquals(String field) {
        if (FIELD.equalsIgnoreCase(field)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag value is equal to specified string.
     *
     * @param value The string to compare to the current tag value.
     * @return Return <code>true</code> if current tag value is equal to specified string.
     */
    public boolean valueEquals(String value) {
        if (VALUE.equalsIgnoreCase(value)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag field exists.
     *
     * @return Return <code>true</code> if current tag field exists.
     */
    public boolean hasField()  {
        if (Operator.hasValue(FIELD)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag value exists.
     *
     * @return Return <code>true</code> if current tag value exists.
     */
    public boolean hasValue()  {
        if (Operator.hasValue(VALUE)) { return true; }
        else { return false; }
    }

    /**
     * Check if current tag frill exists.
     *
     * @return Return <code>true</code> if current tag frill exists.
     */
    public boolean hasFrill()  {
        if (Operator.hasValue(FRILL)) { return true; }
        else { return false; }
    }


/* #################################################################################################
##  MANAGEMENT                                                                                    ##
################################################################################################# */

    /**
     * Clear memory.
     */
    public void clear() {
        NEXT = "";
        FIELD = "";
        VALUE = "";
        FRILL = "";
        LINE = null;
    }

    /**
     * Clear current fields.
     */
    private void clearCurrent() {
        this.LINE = "";
        this.FIELD = "";
        this.VALUE = "";
        this.NEXT = "";
        this.FRILL = "";
    }

    /**
     * Create instance of Frill().
     *
     * @return a Frill() of current tag frill.
     */
    public Frill frill() { return new Frill(FRILL); }


/* #################################################################################################
##  TAG TRANSLATION                                                                               ##
################################################################################################# */

    /**
     * Read through next line and determine if it contains a tag.
     *
     * @param str next line.
     * @return Return <code>true</code> if line contains tag.
     */
    private boolean containsTag(String str) {

        clearCurrent();

        if (str == null || str.length() == 0) { return false; }

        String[] open_array = Cloak.openTagArray(str);

        if (open_array[Cloak._exists].equals("true")) {

            this.LINE  = open_array[Cloak._before];
            this.FIELD = open_array[Cloak._field];
            this.VALUE = open_array[Cloak._value];

            if (open_array[Cloak._type].equals("multiline")) {

                String[] close_array = Cloak.closeTagArray(open_array[Cloak._after], open_array[Cloak._field]);

                if (close_array[Cloak._exists].equals("true")) {
                    this.FRILL = close_array[Cloak._before];
                    this.NEXT  = close_array[Cloak._after];
                }
                else {
                    this.FRILL = open_array[Cloak._after];
                    this.NEXT = "";
                }

            }
            else { this.NEXT = open_array[Cloak._after]; }

            return true;

        }
        else {
            this.LINE = str;
            return false;
        }

    }

}
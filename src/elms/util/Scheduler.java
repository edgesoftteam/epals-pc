package elms.util;

import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import elms.agent.ActivityAgent;



public class Scheduler extends TimerTask {
	static Logger logger = Logger.getLogger(Scheduler.class.getName()); 
	
	public Scheduler(){
		logger.debug("Running the scheduler. Time now is "+Calendar.getInstance().getTime());
		run();
		logger.debug("Schdueler completed");
	}
	
	public void run() {
		    logger.debug("Sending Emails");
		    List actList = ActivityAgent.getReviewActivities();
		    
		    if(actList.size() > 0) {		    			   
			    try {
					new EmailSender().sendEmails(actList);
				} catch (Exception e) {
					logger.error("",e);
	
				}
		    }
	  }
	
}

	
	


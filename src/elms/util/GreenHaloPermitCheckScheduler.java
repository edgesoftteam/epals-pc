package elms.util;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import elms.agent.ActivityAgent;
import elms.app.project.GreenHaloFinalResponse;

public class GreenHaloPermitCheckScheduler extends TimerTask{
	static Logger logger = Logger.getLogger(GreenHaloPermitCheckScheduler.class.getName());

	public GreenHaloPermitCheckScheduler() {
		logger.debug("Running the GreenHaloPermitCheckScheduler. Time now is " + Calendar.getInstance().getTime());
		run();
		logger.debug("GreenHaloPermitCheckScheduler completed");
	}

	public void run() {
		logger.debug("GreenHaloPermitCheckScheduler");
		ActivityAgent agent = new ActivityAgent();
		try {
			Map<Integer,Integer> list = agent.getGreenHaloProjectIds();
			if (list.size() > 0) {

				Map<String, String> headers = new HashMap<String, String>();
				ResourceBundle properties = HTTPClientUtils.getResourceBundle();
				String apiKey = properties.getString("green.halo.api.key");
				for (Entry<Integer, Integer> entry : list.entrySet()) {
					Integer id =  entry.getKey();
					Integer projectId = entry.getValue();
					String url = "https://dev.greenhalosystems.com/?func=api/project/div_rate&api_key=" + apiKey
							+ "&project_id=" + projectId;
					String response = HTTPClientUtils.getandReturnString(url, headers);
					logger.debug("For Project_ID : "+projectId+",Response is : "+response);
					ObjectMapper mapper = new ObjectMapper();
					GreenHaloFinalResponse green = mapper.readValue(response, GreenHaloFinalResponse.class);
					green.setProjectId(projectId);
					green.setGhrId(id);
					agent.setUpdateFinalResponse(green);
				}
			}

		} catch (Exception e) {
			logger.error("Error in GreenHaloPermitCheckScheduler : ", e);

		}
	}
}

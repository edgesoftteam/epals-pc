package elms.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

//import com.alain.Guise.Frill;

import elms.agent.AdminAgent;
import elms.common.Constants;
import elms.control.beans.online.EmailAdminForm;
import elms.util.db.Wrapper;

public class StringUtils {
	static Logger logger = Logger.getLogger(StringUtils.class.getName());

	public StringUtils() {
	}

	// Code to replace all special characters in the incoming file with '_' symbol.
	public static String replaceSpecialCharWithUnderscore(String originalFileName) {
		final String[] splChars = { "#", "+", "$", "," };
		String newString = originalFileName;
		for (int i = 0; i < splChars.length; i++)
			newString = StringUtils.replace(newString, splChars[i], "_");
		return newString;
	}

	public static Calendar str2cal(String s) {
		Calendar calendar = null;

		if ((s != null) && (!s.equals(""))) {
			try {
				SimpleDateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy");
				Date date = inputFormat.parse(s);
				calendar = getCalendar(date);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return calendar;
	}

	public static Calendar dbDate2cal(String s) {
		Calendar calendar = null;

		if (s != null) {
			try {
				SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date date = inputFormat.parse(s);
				calendar = getCalendar(date);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return calendar;
	}

	public static String getUniqueFileName() {
		String strTimeStamp = null;
		Calendar cal = Calendar.getInstance();

		if (cal != null) {
			try {
				SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
				SimpleDateFormat formatter2 = new SimpleDateFormat("hhmmssSSS");
				strTimeStamp = formatter1.format(cal.getTime());
				strTimeStamp = strTimeStamp + "_" + formatter2.format(cal.getTime());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strTimeStamp;
	}

	public static String cal2Ts(Calendar c) {
		String strTimeStamp = null;
		Calendar cal = Calendar.getInstance();

		if (c != null) {
			try {
				SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat formatter2 = new SimpleDateFormat("hh.mm.ss");
				strTimeStamp = formatter1.format(c.getTime());
				strTimeStamp = strTimeStamp + "-" + formatter2.format(cal.getTime());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strTimeStamp;
	}

	public static String cal2datetime(Calendar c) {
		String strTimeStamp = null;
		Calendar cal = Calendar.getInstance();

		if (c != null) {
			try {
				SimpleDateFormat formatter1 = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat formatter2 = new SimpleDateFormat("hh:mm:ss");
				strTimeStamp = formatter1.format(c.getTime());
				strTimeStamp = strTimeStamp + " " + formatter2.format(cal.getTime());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strTimeStamp;
	}

	public static String datetime2dbTs(String s) {
		if (s != null) {
			try {
				s = s.substring(6, 10) + "-" + s.substring(0, 2) + "-" + s.substring(3, 5) + " " + s.substring(11, 19);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return s;
	}

	public static String cal2str(Calendar c) {
		String strCalendar = "";

		if (c != null) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				strCalendar = formatter.format(c.getTime());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strCalendar;
	}

	public static java.sql.Date cal2SQLDate(Calendar c) {
		java.sql.Date sqlDate = null;

		if (c != null) {
			try {
				sqlDate = new java.sql.Date(c.getTime().getTime());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return sqlDate;
	}

	public static String date2str(Date d) {
		String strDate = null;

		if (d != null) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				strDate = formatter.format(d);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strDate;
	}

	public static String date2strTS(Date d) {
		String strDate = null;

		if (d != null) {
			try {
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
				strDate = formatter.format(d);
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strDate;
	}

	public static String toOracleDate(String s) {
		if (s == null) {
			return s;
		} else if (s.equals("")) {
			return null;
		} else {
			return "to_date(" + checkString(s) + ",'MM/DD/YYYY')";
		}
	}

	public static String toOracleDateTime(String s) {
		if (s == null) {
			return s;
		} else if (s.equals("")) {
			return null;
		} else {
			return "to_date('" +s+ " 23:59:59','MM/DD/YYYY hh24:mi:ss')";
		}
	}

	public static int s2i(String s) {
		try {
			StringBuffer b = new StringBuffer();

			for (int j = 0; j < s.length(); j++) {
				char c = s.charAt(j);

				if ((c >= '0') && (c <= '9')) {
					b.append(c);
				}

				if (c == '.') {
					break;
				}
			}

			Integer i = new Integer(b.toString());

			return i.intValue();
		} catch (Exception _ex) {
			return -1;
		}
	}

	public static double s2d(String s) {
		try {
			StringBuffer b = new StringBuffer();

			for (int j = 0; j < s.length(); j++) {
				char c = s.charAt(j);

				if (((c >= '0') && (c <= '9')) || (c == '.') || (c == '-')) {
					b.append(c);
				}
			}

			double d = (new Double(b.toString())).doubleValue();

			return d;
		} catch (Exception _ex) {
			return 0.0D;
		}
	}

	public static java.math.BigDecimal s2bd(String s) {
		try {
			StringBuffer b = new StringBuffer();

			for (int j = 0; j < s.length(); j++) {
				char c = s.charAt(j);

				if (((c >= '0') && (c <= '9')) || (c == '.') || (c == '-')) {
					b.append(c);
				}
			}

			java.math.BigDecimal bd = (new java.math.BigDecimal(b.toString()));

			return bd;
		} catch (Exception _ex) {
			return new java.math.BigDecimal("0.00");
		}
	}

	public static String i2s(int val) {
		try {
			Integer i = new Integer(val);

			return i.toString();
		} catch (Exception _ex) {
			return "";
		}
	}

	// double to string converter
	public static String dbl2str(double val) {
		try {
			Double i = new Double(val);

			return i.toString();
		} catch (Exception _ex) {
			return "";
		}
	}

	public static String b2s(boolean b) {
		return (!b) ? "N" : "Y";
	}

	public static String yn(String b) {
		String b1 = b;

		if (b1 == null) {
			b1 = "";
		}

		if (b1.equalsIgnoreCase("Yes")) {
			b1 = "Y";
		}

		return (b1.equalsIgnoreCase("Y")) ? "Yes" : "No";
	}

	public static String OnOfftoYN(String b) {
		String b1 = b;

		if (b1 == null) {
			b1 = "N";
		}

		if (b1.equalsIgnoreCase("on")) {
			b1 = "Y";
		} else {
			b1 = "N";
		}

		return b1;
	}

	public static String YNtoOnOff(String b) {
		String b1 = b;

		if (b1 == null) {
			b1 = "";
		}

		if (b1.equalsIgnoreCase("Y")) {
			b1 = "on";
		} else {
			b1 = "off";
		}

		return b1; // (b1.equalsIgnoreCase("Y")) ? "on" : "off";
	}

	public static boolean s2b(String s) {
		boolean result = false;

		if (s != null) {
			if (s.equalsIgnoreCase("Y")) {
				result = true;
			}
		}

		return result;
	}

	// converts any string to proper case
	public static String properCase(String s) {
		StringBuffer b = new StringBuffer(s.toLowerCase());
		boolean icap = false;

		for (int i = 0; i < b.length(); i++) {
			char c = b.charAt(i);

			if (!icap && Character.isLetter(c)) {
				b.setCharAt(i, Character.toUpperCase(c));
				icap = true;
			}

			if (!Character.isLetter(c)) {
				icap = false;
			}
		}

		return b.toString();
	}

	public static String checkString(String aValue) {
		if (aValue == null) {
			return "null";
		} else {
			if (aValue.equals("")) {
				return "null";
			}

			// preparing string for database insert
			String tempStr = "";
			char quot = '\'';

			if ((aValue != null) && (aValue.length() > 0)) {
				for (int i = 0; i < aValue.length(); i++) {
					char c = aValue.charAt(i);

					if (c != quot) {
						tempStr = tempStr + aValue.charAt(i);
					} else {
						tempStr = tempStr + aValue.charAt(i) + aValue.charAt(i);
					}
				}

				aValue = tempStr;
			}

			// else {
			aValue = "'" + aValue + "'";

			return aValue;

			// }
		}
	}

	public static String handleSingleQuot(String aValue) {
		if (aValue == null) {
			return "null";
		} else {
			if (aValue.equals("")) {
				return "null";
			}

			// preparing string for database insert
			String tempStr = "";
			char quot = '\'';

			if ((aValue != null) && (aValue.length() > 0)) {
				for (int i = 0; i < aValue.length(); i++) {
					char c = aValue.charAt(i);

					if (c != quot) {
						tempStr = tempStr + aValue.charAt(i);
					} else {
						tempStr = tempStr + aValue.charAt(i) + aValue.charAt(i);
					}
				}

				aValue = tempStr;
			}

			return aValue;
		}
	}

	public static String checkString(Calendar aValue) {
		return (aValue != null) ? ("'" + cal2str(aValue) + "'") : "null";
	}

	public static String checkInteger(String aValue) {
		return ((aValue == null) || aValue.equals("")) ? "null" : aValue;
	}

	public static String checkNumber(String aValue) {
		if ((aValue == null) || aValue.trim().equals("")) {
			return "null";
		} else {
			return aValue.trim();
		}
	}

	public static String nullReplaceWithEmpty(String aValue) {
		if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {
			return "";
		} else {
			return aValue.trim();
		}
	}

	
	public static boolean isBlankOrNull(String aValue) {
		if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null")) || (aValue.trim().equalsIgnoreCase(""))) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public static String zeroReplaceWithEmpty(int aValue) {
		if ((aValue == 0) || (aValue < 0)) {
			return "";
		} else {
			return i2s(aValue);
		}
	}

	public static String checkNumber(int aValue) {
		if (aValue < 1) {
			return "null";
		} else {
			return i2s(aValue);
		}
	}

	/**
	 * Function to format a String to make it suitable for SQL query Allows Letters,Digits and Whitespaces
	 * 
	 * @return A String of the form string%
	 */
	public static String formatQueryForString(String aString) {
		String query = "";

		if (aString.equals("") || (aString.length() == 0)) {
			aString = "%";
		}

		for (int i = 0; i < aString.length(); i++) {
			char quot = '\'';

			if (Character.isLetterOrDigit(aString.charAt(i)) || Character.isWhitespace(aString.charAt(i))) {
				query = query + aString.charAt(i);
			} else if ((aString.charAt(i) == quot)) {
				query = query + quot + quot;
			} else if ((aString.charAt(i) == '%') || (aString.charAt(i) == '*')) {
				query = query + '%';
			} else {
				query = query + aString.charAt(i);
			}
		}

		return query;
	}

	/**
	 * Function to format a Number to make it suitable for SQL query Allows Digits only
	 * 
	 * @return A number of the form number%
	 */
	public static String formatQueryForNumber(String aString) {
		String query = "";

		if (aString.equals("") || (aString.length() == 0)) {
			aString = "%";
		}

		for (int i = 0; i < aString.length(); i++) {
			if (Character.isDigit(aString.charAt(i))) {
				query = query + aString.charAt(i);
			} else {
				query = query + '%';

				break;
			}
		}

		return query;
	}

	public static String f2s(float floatValue) {
		String flt = "";

		try {
			DecimalFormat df = new DecimalFormat();
			df.setGroupingUsed(false);
			flt = df.format(floatValue);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flt;
	}

	public static String f2$(float floatValue) {
		String flt = "0.00";

		try {
			NumberFormat nf = NumberFormat.getCurrencyInstance();
			flt = nf.format(floatValue);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flt;
	}

	public static String d2s(double doubleValue) {
		String flt = "";
		try {
			DecimalFormat df = new DecimalFormat();
			df.setGroupingUsed(false);
			df.applyPattern("##,###,##0.00");
			// df.applyPattern("#0.00");
			flt = df.format(doubleValue);

			if (flt.endsWith(".00")) {
				flt = flt.substring(0, flt.length() - 3);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flt;
	}

	public static String d2str(double doubleValue) {
		String flt = "";
		try {
			DecimalFormat df = new DecimalFormat();
			df.setGroupingUsed(false);
			df.applyPattern("#0.00");
			flt = df.format(doubleValue);

			if (flt.endsWith(".00")) {
				flt = flt.substring(0, flt.length() - 3);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flt;
	}

	public static String dbl2prct(double doubleValue) {
		String flt = "";

		try {
			DecimalFormat df = new DecimalFormat();
			df.setGroupingUsed(false);
			df.applyPattern("#,##0.00%");
			flt = df.format(doubleValue);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flt;
	}


	public static String dbl2$(double doubleValue) {
		String flt = "0.00";

		try {

			NumberFormat nf = NumberFormat.getCurrencyInstance();
			flt = nf.format(doubleValue);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return flt;
	}

	public static String str2$(String value) {

		NumberFormat nf = NumberFormat.getCurrencyInstance();
		if ((value == null) || value.equals("") || value.equals("0")) {
			value = "0.00";
			return nf.format(0);
		}

		try {
			int intValue = Integer.parseInt(value);

			String flt = nf.format(intValue);

			return flt;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());

			return nf.format(0.00);
		}
	}

	public static String i2nbr(int i) {
		try {
			NumberFormat nf = NumberFormat.getNumberInstance();

			return nf.format(i);
		} catch (Exception e) {
			logger.warn(e.getMessage());

			return "0";
		}
	}

	public static double $2dbl(String dollorvalue) {
		String tempStr = "";
		double retVal = 0.00;

		if (dollorvalue == null) {
			dollorvalue = "";
		}

		try {
			for (int i = 0; i < dollorvalue.length(); i++) {
				if ((dollorvalue.charAt(i) != ',') && (dollorvalue.charAt(i) != '$') && (dollorvalue.charAt(i) != ')')) {
					if (dollorvalue.charAt(i) == '(') {
						tempStr += "-";
					} else {
						tempStr += dollorvalue.charAt(i);
					}
				}
			}

			retVal = Double.parseDouble(tempStr);
		} catch (Exception e) {
			logger.warn("Error on " + dollorvalue + ":" + tempStr + ":" + e.getMessage());
			tempStr = "0";
		}

		if (("").equalsIgnoreCase(tempStr) || ("0.00").equalsIgnoreCase(tempStr)) {
			return 0.00;
		} else {
			return retVal;
		}
	}

	/**
	 * Function to format a String to make it suitable for SQL query Allows Letters,Digits and Whitespaces Replace * with % in the String
	 * 
	 * @return A String
	 */
	public static String replaceStarWithPcnt(String aString) {
		if (aString.equals("") || (aString.length() == 0)) {
			aString = "%";
		}

		return aString.replace('*', '%');
	}

	public static String n2b(String b) {
		return (b == null) ? "" : b;
	}

	public static String replaceQuot(String s) {
		String rtnStr = "";
		char quot = '\'';

		if ((s != null) && (s.length() > 0)) {
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);

				if (c != quot) {
					rtnStr = rtnStr + s.charAt(i);
				}
			}
		}

		return rtnStr;
	}

	public static String encodeQuot(String s) {
		String rtnStr = "";
		char quot = '\'';

		if ((s != null) && (s.length() > 0)) {
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);

				if (c == quot) {
					rtnStr = rtnStr + "\\" + s.charAt(i);
				} else {
					rtnStr = rtnStr + s.charAt(i);
				}
			}
		}

		return rtnStr;
	}

	public static String replace(String source, String from, String to) {
		StringBuffer result = new StringBuffer();
		int i = source.indexOf(from);

		if (i >= 0) {
			result.append(source.substring(0, i));
			result.append(to);
			i += from.length();
		} else {
			i = 0;
		}

		result.append(source.substring(i));

		return new String(result);
	}

	/* Handling double Quotes replace it with empty spaece */
	public static String replaceDoubleQuot(String s) {
		if (s == null) {
			return "";
		}

		String find = "\"";
		String replace = "\\";

		int index = -1;

		while ((index = s.indexOf(find)) > -1) {
			s = s.substring(0, index) + replace + s.substring(index + find.length(), s.length());
		}

		return s;
	}
	

	/* Handling double Quotes replace it with empty space */
	public static String replaceDoubleQuotWithSplChar(String s) {
		if (s == null) {
			return "";
		}

		String find = "\"";
		String replace = "\'\'";

		int index = -1;

		while ((index = s.indexOf(find)) > -1) {
			s = s.substring(0, index) + replace + s.substring(index + find.length(), s.length());
		}

		return s;
	}

	/* Handling double Quotes replace it with empty spaece */
	public static String removeDoubleQuot(String s) {
		if (s == null) {
			return "";
		}

		String find = "\"";
		String replace = " ";

		int index = -1;

		while ((index = s.indexOf(find)) > -1) {
			s = s.substring(0, index) + replace + s.substring(index + find.length(), s.length());
		}

		return s;
	}

	public static String fixedLengthString(String str, int length) {
		String tempStr = "";

		if (str == null) {
			str = "";
		} else if ((str.length() > 0) || (str.length() < length)) {
			length = length - str.length();
		}

		for (int i = 0; i < length; i++) {
			tempStr = tempStr + "0";
		}

		tempStr = tempStr + str;

		return tempStr;
	}

	public static List arrayToList(Object[] objects) {
		List objList = new ArrayList();

		if (objects != null) {
			for (int i = 0; i < objects.length; i++) {
				objList.add(objects[i]);
			}
		}

		return objList;
	}

	public static String stringArrayToCommaSeperatedString(String[] stringArray) {

		/**
		 * Was not working ...fixed it...edgesoft india 9/17/07 (AB)
		 */

		/*
		 * if (stringArray == null) { return ""; } else { String commanSeperatedString = ""; for (int i = 0; i < stringArray.length; i++) { commanSeperatedString += stringArray[i] + ","; } return commanSeperatedString.substring(0, commanSeperatedString.length() - 1); }
		 */
		String csvString = "";

		for (int i = 0; i < stringArray.length; i++) {
			if (i == 0) {
				csvString = csvString + stringArray[i];
			} else {
				csvString = csvString + "," + stringArray[i];
			}
		}

		return csvString;
	}

	/* Handling phone format */
	public static String phoneFormat(String phoneIn) {
		String phoneOut = "";

		if ((phoneIn != null) && (!phoneIn.trim().equalsIgnoreCase(""))) {

			try {
				if (phoneIn.charAt(3) == '-') {

					if ((phoneIn != null) && (phoneIn.length() > 0)) {
						for (int i = 0; i < phoneIn.length(); i++) {
							if (!((i == 3) || (i == 7))) {
								phoneOut = phoneOut + phoneIn.charAt(i);
							}
						}
					}
				} else {

					if ((phoneIn != null) && (phoneIn.length() > 0)) {
						for (int i = 0; i < phoneIn.length(); i++) {
							phoneOut = phoneOut + phoneIn.charAt(i);

							if ((i == 2) || (i == 5)) {
								phoneOut = phoneOut + "-";
							}
						}
					}
				}
			} catch (RuntimeException e) {
				phoneOut = phoneIn;
			}
		}

		if (phoneOut.equalsIgnoreCase("")) {
			phoneOut = null;
		}

		return phoneOut;
	}

	/* Handling phone format */
	public static String stripPhone(String phoneIn) {

		String phoneOut = "";

		if ((phoneIn != null) && (!phoneIn.trim().equalsIgnoreCase(""))) {

			if ((phoneIn != null) && (phoneIn.length() > 0)) {
				for (int i = 0; i < phoneIn.length(); i++) {
					if (!((i == 3) || (i == 7))) {
						phoneOut = phoneOut + phoneIn.charAt(i);
					}
				}
			}

		}

		if (phoneOut.equalsIgnoreCase("")) {
			phoneOut = null;
		}


		return phoneOut;
	}

	public static int divideCeiling(double dividend, double divisor) {

		int value = new Double((dividend >= 0) ? (((dividend + divisor) - 1) / divisor) : (dividend / divisor)).intValue();

		return value;
	}

	public static int divideBase(double dividend, double divisor) {
		return new Double((dividend >= 0) ? (dividend / divisor) : ((dividend - divisor + 1) / divisor)).intValue();
	}

	public static String yymmdd2date(String yymmdd) {
		try {
			int yy = s2i(yymmdd.substring(0, 2));

			if (yy < 20) {
				yy = 2000 + yy;
			} else {
				yy = 1900 + yy;
			}

			return yymmdd.substring(2, 4) + "/" + yymmdd.substring(4, 6) + "/" + yy;
		} catch (Exception e) {
			return "";
		}
	}

	public static String mmddyyyy2yyyymmdd(String date) {
		return date.substring(6) + "/" + date.substring(0, 5);
	}

	public static String lpad(String s, int len) {
		return lpad(s, len, " ");
	}

	public static String lpad(String s, int len, String padder) {
		StringBuffer buf = new StringBuffer();
		int slen = 0;

		if (s != null) {
			slen = s.length();
		}

		if (len > slen) {
			// int diff = len - s.length();
			int diff = len - slen;

			for (int i = 0; i < diff; i++) {
				buf.append(padder);
			}
		}

		if (s != null) {
			buf.append(s);
		}

		return buf.toString();
	}

	public static String rpad(String s, int len) {
		return rpad(s, len, " ");
	}

	public static String rpad(String s, int len, String padder) {
		StringBuffer buf = new StringBuffer();
		int slen = 0;

		if (s != null) {
			buf.append(s);
			slen = s.length();
		}

		// int diff = len - s.length();
		int diff = len - slen;

		for (int i = 0; i < diff; i++) {
			buf.append(padder);
		}

		return buf.toString();
	}

	/**
	 * This method changes date formate YYYY-MM-DD to DD/MM/YYYY java.lang.String str.
	 * 
	 * @param dateStr
	 * @return String
	 * @throws Exception
	 *             if the entered date is not of the format MM-DD-YYYY
	 */
	public static String changeDateFormate(String dateStr) throws Exception {

		if ((dateStr == null) || (dateStr.trim().length() <= 0)) {
			return null;
		}

		StringTokenizer st = new StringTokenizer(dateStr, "-");

		String year = st.nextToken();
		String day = st.nextToken();

		if (day.length() == 1) {
			day = "0" + day;
		}

		String month = st.nextToken();

		if (month.length() == 1) {
			month = "0" + month;
		}


		return day + "/" + month + "/" + year;
	}

	/**
	 * This method changes date formate YYYY-MM-DD to DD/MM/YYYY java.lang.String str.
	 * 
	 * @param dateStr
	 * @return String
	 * @throws Exception
	 *             if the entered date is not of the format MM-DD-YYYY
	 */
	public static String changeDateToSimpleDate(String dateStr) throws Exception {

		if ((dateStr == null) || (dateStr.trim().length() <= 0)) {
			return null;
		}

		StringTokenizer st = new StringTokenizer(dateStr, "/");

		String day = st.nextToken();

		if (day.length() == 1) {
			day = "0" + day;
		}

		String month = st.nextToken();

		if (month.length() == 1) {
			month = "0" + month;
		}

		String year = st.nextToken();

		return day + "/" + month + "/" + year;
	}

	/**
	 * <p>
	 * Replaces all occurances of a String within another String.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method is a no-op.
	 * </p>
	 * 
	 * <pre>
	 * 
	 * 
	 *   Operator.replace(null, *, *)        = null
	 *   Operator.replace(&quot;&quot;, *, *)          = &quot;&quot;
	 *   Operator.replace(&quot;aba&quot;, null, null) = &quot;aba&quot;
	 *   Operator.replace(&quot;aba&quot;, null, null) = &quot;aba&quot;
	 *   Operator.replace(&quot;aba&quot;, &quot;a&quot;, null)  = &quot;aba&quot;
	 *   Operator.replace(&quot;aba&quot;, &quot;a&quot;, &quot;&quot;)    = &quot;aba&quot;
	 *   Operator.replace(&quot;aba&quot;, &quot;a&quot;, &quot;z&quot;)   = &quot;zbz&quot;
	 * 
	 * 
	 * </pre>
	 * 
	 * @see #replace(String text, String repl, String with, int max)
	 * @param text
	 *            text to search and replace in, may be null
	 * @param repl
	 *            the String to search for, may be null
	 * @param with
	 *            the String to replace with, may be null
	 * @return the text with any replacements processed, <code>null</code> if null String input
	 */
	public static String replaceString(String text, String repl, String with) {
		return replaceString(text, repl, with, -1);
	}

	/**
	 * <p>
	 * Replaces a String with another String inside a larger String, for the first <code>max</code> values of the search String.
	 * </p>
	 * 
	 * <p>
	 * A <code>null</code> reference passed to this method is a no-op.
	 * </p>
	 * 
	 * <pre>
	 * 
	 * 
	 *   Operator.replace(null, *, *, *)         = null
	 *   Operator.replace(&quot;&quot;, *, *, *)           = &quot;&quot;
	 *   Operator.replace(&quot;abaa&quot;, null, null, 1) = &quot;abaa&quot;
	 *   Operator.replace(&quot;abaa&quot;, null, null, 1) = &quot;abaa&quot;
	 *   Operator.replace(&quot;abaa&quot;, &quot;a&quot;, null, 1)  = &quot;abaa&quot;
	 *   Operator.replace(&quot;abaa&quot;, &quot;a&quot;, &quot;&quot;, 1)    = &quot;abaa&quot;
	 *   Operator.replace(&quot;abaa&quot;, &quot;a&quot;, &quot;z&quot;, 0)   = &quot;abaa&quot;
	 *   Operator.replace(&quot;abaa&quot;, &quot;a&quot;, &quot;z&quot;, 1)   = &quot;zbaa&quot;
	 *   Operator.replace(&quot;abaa&quot;, &quot;a&quot;, &quot;z&quot;, 2)   = &quot;zbza&quot;
	 *   Operator.replace(&quot;abaa&quot;, &quot;a&quot;, &quot;z&quot;, -1)  = &quot;zbzz&quot;
	 * 
	 * 
	 * </pre>
	 * 
	 * @param text
	 *            text to search and replace in, may be null
	 * @param repl
	 *            the String to search for, may be null
	 * @param with
	 *            the String to replace with, may be null
	 * @param max
	 *            maximum number of values to replace, or <code>-1</code> if no maximum
	 * @return the text with any replacements processed, <code>null</code> if null String input
	 */
	public static String replaceString(String text, String repl, String with, int max) {
		if ((text == null) || (repl == null) || (with == null) || (repl.length() == 0) || (max == 0)) {
			return text;
		}

		StringBuffer buf = new StringBuffer(text.length());
		int start = 0;
		int end = 0;

		while ((end = text.indexOf(repl, start)) != -1) {
			buf.append(text.substring(start, end)).append(with);
			start = end + repl.length();

			if (--max == 0) {
				break;
			}
		}

		buf.append(text.substring(start));

		return buf.toString();
	}

	public static List stringToList(String s, String delim) {
		List l = new ArrayList();
		StringTokenizer st = new StringTokenizer(s, delim);

		while (st.hasMoreElements()) {
			l.add((String) st.nextElement());
		}

		return l;
	}
	 
	public static String clobToString(Clob data) {
	    StringBuilder sb = new StringBuilder();
	    try {
	        Reader reader = data.getCharacterStream();
	        BufferedReader br = new BufferedReader(reader);

	        String line;
	        while(null != (line = br.readLine())) {
	            sb.append(line);
	        }
	        br.close();
	    } catch (SQLException e) {
	        // handle this exception
	    } catch (IOException e) {
	        // handle this exception
	    }
	    return sb.toString();
	}

	public static String listToString(List list) {
		String result = "";

		for (int i = 0; i < list.size(); i++) {
			result += (list.get(i) + " ");
		}

		return result;
	}
	
	public static String replaceSmartChars(String input) {
		String result = "";

		result = replace(input,"'","&#39;");
		result = replace(result,"%","&#37;");
		result = replace(result,"\"","&#34;");
        result = replace(result,"(","&#40;");
        result = replace(result,")","&#41;");
        result = replace(result,"\\","&#92;");
        result = replace(result,"^","&#94;");
        result = replace(result,"_","&#95;");
        result = replace(result,",","&#44;");
        result = replace(result,"\r\n","<BR>");
        result = replace(result,"\n","<BR>");

		return result;
	}

	/**
	 * Utility function that truncates millisecond portion of a timestamp formatted string
	 * 
	 * @param timestamp
	 * @return
	 */
	public static String truncateMilliseconds(String timestamp) {
		return timestamp.substring(0, timestamp.lastIndexOf("."));
	}

	public static String yyyymmdd2date(String yymmdd) {
		try {
			int yy = s2i(yymmdd.substring(0, 4));

			return yymmdd.substring(5, 7) + "/" + yymmdd.substring(8, 10) + "/" + yy;
		} catch (Exception e) {
			return "";
		}
	}

	public static long countLineBreaks(String s) throws IOException {
		Reader reader = (Reader) new StringReader(s);
		long nbLines = 0L;
		BufferedReader bfr = new BufferedReader(reader);
		char previousChar = '\0';
		int charCode = 0;
		long index = 0L;
		boolean newLine = false;

		while (charCode != -1) {
			charCode = bfr.read();

			char character = (char) charCode;

			if ((character == '\n') || (previousChar == '\r')) {
				newLine = true;
			} else {
				newLine = false;
			}

			if (newLine) {
				nbLines++;
			}

			previousChar = character;
			index++;
		}

		bfr.close();
		reader.close();

		return nbLines;
	}

	// Method added by Gayathri
	public static String formatString(String theString) {
		String mainString = theString;

		int startOTag = -1;
		int endOTag = -1;
		int startCTag = -1;
		int endCTag = -1;
		int lastTag = -1;
		String newString = "";

		startOTag = mainString.indexOf("<");

		if (startOTag > 0) {
			lastTag = mainString.lastIndexOf(">");

			endOTag = mainString.indexOf(">");
			startCTag = mainString.indexOf("</", endOTag);
			endCTag = mainString.indexOf(">", startCTag);

			newString = mainString.substring(0, startOTag);
			newString = newString + "<" + mainString.substring(startOTag + 3, endOTag) + ">";
			newString = newString + mainString.substring(endOTag + 3, startCTag);
			newString = newString + "</" + mainString.substring(startCTag + 4, endCTag) + ">";

			while (endCTag < lastTag) {
				startOTag = mainString.indexOf("<", endCTag);
				endOTag = mainString.indexOf(">", startOTag);

				newString = newString + mainString.substring(endCTag + 3, startOTag);
				newString = newString + "<" + mainString.substring(startOTag + 3, endOTag) + ">";

				startCTag = mainString.indexOf("</", endOTag);
				endCTag = mainString.indexOf(">", startCTag);

				newString = newString + mainString.substring(endOTag + 3, startCTag);
				newString = newString + "</" + mainString.substring(startCTag + 4, endCTag) + ">";
			}

			newString = newString + mainString.substring(endCTag + 3);

			return newString;
		} else {
			return mainString;
		}
	}

	// Method Added by Aman
	public static String formatBusinessLicenceCertificateData(String s) throws IOException {
		Reader reader = (Reader) new StringReader(s);
		BufferedReader bfr = new BufferedReader(reader);
		ArrayList paragraphs = new ArrayList();
		String paragraph = "";
		String currentParagraph = "";
		String line = "";
		int lineLength = 0;
		int charCode = 0;
		int indexOfPreviousSpace = 0;
		int noOfCharacters = 0;
		int noOfLines = 0;
		String resultString = "";

		StringTokenizer paragraphWords = null;

		while ((paragraph = bfr.readLine()) != null) {
			paragraphs.add(paragraph);
		}


		for (int i = 0; i < paragraphs.size(); i++) {
			// for (int i=0; i<1; i++) {
			currentParagraph = (String) paragraphs.get(i);
			paragraphWords = new StringTokenizer(currentParagraph, " ");

			while (paragraphWords.hasMoreTokens()) {
				String token = paragraphWords.nextToken() + " ";
				String temp = line + token;

				if (temp.length() < 120) {
					line = line + token;
				} else {
					resultString = resultString + line + '\n';
					line = token;
				}
			}

			resultString = resultString + line + '\n' + '\n';
			line = "";


		}

		bfr.close();
		reader.close();

		return resultString;
	}

	// Method to create new line
	public static String getPageData(String s, int noOfLines) throws IOException {
		Reader reader = (Reader) new StringReader(s);
		BufferedReader bfr = new BufferedReader(reader);
		String page = "";
		String line = "";
		int index = 0;

		while ((line = bfr.readLine()) != null) {
			index++;

			if (index <= noOfLines) {
				page = page + line + "\n";
			} else {
			}
		}

		// pages.add(page);
		bfr.close();
		reader.close();

		return page;
	}

	public static String replaceNewLineByEmpty(String s) throws IOException {
		Reader reader = (Reader) new StringReader(s);
		BufferedReader bfr = new BufferedReader(reader);
		String page = "";
		String line;

		while ((line = bfr.readLine()) != null) {
			page = page + line;
		}

		bfr.close();
		reader.close();

		return page;
	}

	/**
	 * Gets the current fiscal year
	 * 
	 * @return
	 */
	public String getCurrentFiscalYear() {
		String fiscalYear = "";

		// calculate the fiscal year
		Calendar now = Calendar.getInstance();
		int currentMonth = now.get(Calendar.MONTH) + 1;
		String currentYear = i2s(now.get(Calendar.YEAR));
		String nextYear = i2s(now.get(Calendar.YEAR) + 1);

		// logic to computer the current fiscal year
		if (currentMonth < 7) { // if less than the month of july
			fiscalYear = currentYear;
		} else {
			fiscalYear = nextYear;
		}

		return fiscalYear;
	}

	/* Putting Hyphen in APN number */
	public static String apnWithHyphens(String apn) {
		String apnOut = "";

		if ((apn != null) && (!apn.trim().equalsIgnoreCase(""))) {

			try {
				if ((apn != null) && (apn.length() > 0)) {
					for (int i = 0; i < apn.length(); i++) {
						apnOut = apnOut + apn.charAt(i);

						if ((i == 3) || (i == 6)) {
							apnOut = apnOut + "-";
						}
					}
				}
			} catch (RuntimeException e) {
				logger.error("Error in apn formatting");
				apnOut = apn;
			}
		}

		if (apnOut.equalsIgnoreCase("")) {
			apnOut = null;
		}

		return apnOut;
	}

	/* Removing Hyphen in APN number */
	public static String apnWithoutHyphens(String apn) {

		if ((apn != null) && (!apn.trim().equalsIgnoreCase(""))) {

			try {
				apn = apn.replaceAll("-", "");

			} catch (RuntimeException e) {
				logger.error("Error in apn formatting");
			}
		}
		if (apn.equalsIgnoreCase("")) {
			apn = null;
		}

		return apn;
	}

	public static String[] stringtoArray(String s, String sep) {
		// convert a String s to an Array, the elements
		// are delimited by sep
		StringBuffer buf = new StringBuffer(s);
		int arraysize = 1;

		for (int i = 0; i < buf.length(); i++) {
			if (sep.indexOf(buf.charAt(i)) != -1) {
				arraysize++;
			}
		}

		String[] elements = new String[arraysize];
		int y;
		int z = 0;

		if (buf.toString().indexOf(sep) != -1) {
			while (buf.length() > 0) {
				if (buf.toString().indexOf(sep) != -1) {
					y = buf.toString().indexOf(sep);

					if (y != buf.toString().lastIndexOf(sep)) {
						elements[z] = buf.toString().substring(0, y);
						z++;
						buf.delete(0, y + 1);
					} else if (buf.toString().lastIndexOf(sep) == y) {
						elements[z] = buf.toString().substring(0, buf.toString().indexOf(sep));
						z++;
						buf.delete(0, buf.toString().indexOf(sep) + 1);
						elements[z] = buf.toString();
						z++;
						buf.delete(0, buf.length());
					}
				}
			}
		} else {
			elements[0] = buf.toString();
		}

		buf = null;

		return elements;
	}

	public static void sendEmail(String toEmail, String decide, String password, String Address, String inspectionDate, String InspectionType, String Status, String permit, String comments) throws Exception {
		try {
			String subject = null;
			String message = null;
			EmailAdminForm emailAdminForm = new AdminAgent().getEmailTemplates();

			if (decide.equalsIgnoreCase("request")) {
				if (emailAdminForm.getInspectionRequestedSubject() != null) {
					// subject = emailAdminForm.getInspectionRequestedSubject();
					subject = new AdminAgent().getEmailSubject("INSPECTION_REQUESTED");
				} else {
					subject = "Inspection Request";
				}

				if (emailAdminForm.getInspectionRequestedMessage() != null) {
					HashMap hm = new HashMap();
					hm.put("PERMITNUMBER", permit);
					hm.put("PERMITTYPE", password);
					hm.put("ADDRESS", Address);
					hm.put("DATE", inspectionDate);
					hm.put("TYPE", InspectionType);
					hm.put("STATUS", Status);
					hm.put("COMMENTS", comments);

					message = parseString(emailAdminForm.getInspectionRequestedMessage(), hm);
				} else {
					message = "Message from OBC Online Registration System - Inspection Request Sucessful";
				}

				// composeMessage = StringUtils.composeEmailMessageRequest(message,
				// toEmail, password,Address, "",
				// "");;
			} else if (decide.equalsIgnoreCase("register")) {
				subject = new AdminAgent().getEmailSubject("REGISTRATION_SUCCESS");

				// if(emailAdminForm.getRegistrationSuccessSubject()!=null){
				// subject =emailAdminForm.getRegistrationSuccessSubject();
				// }
				// else subject="Registration  Succesful";
				if (emailAdminForm.getRegistrationSuccessMessage() != null) {
					HashMap hm = new HashMap();
					hm.put("USERNAME", toEmail);
					hm.put("PASSWORD", password);
					message = parseString(emailAdminForm.getRegistrationSuccessMessage(), hm);
				} else {
					message = "Message from OBC Online Registration System - Registration for Inspections online ";
				}

				// composeMessage = StringUtils.composeEmailMessageRegister(message,
				// toEmail, password,Address, "",
				// "");;
			} else if (decide.equalsIgnoreCase("cancel")) {
				subject = new AdminAgent().getEmailSubject("INSPECTION_CANCELLED");

				// if (emailAdminForm.getInspectionCancelledSubject() != null) {
				// subject = emailAdminForm.getInspectionCancelledSubject();
				// } else {
				// subject = "Inspection Cancel";
				// }
				if (emailAdminForm.getInspectionCancelledMessage() != null) {
					HashMap hm = new HashMap();
					hm.put("PERMITNUMBER", permit);
					hm.put("PERMITTYPE", password);
					hm.put("ADDRESS", Address);
					hm.put("DATE", inspectionDate);
					hm.put("TYPE", InspectionType);
					hm.put("STATUS", Status);

					message = parseString(emailAdminForm.getInspectionCancelledMessage(), hm);

					// message = ;
				} else {
					message = "Message from OBC Online Registration System - Inspection Cancel Sucessful";
				}

				// composeMessage = StringUtils.composeEmailMessageCancel(message,
				//
				// toEmail, password,Address, "",
				// "");;
			} else if (decide.equalsIgnoreCase("forgot")) {
				// if(emailAdminForm.getForgotPasswordSubject()!=null){
				// subject =emailAdminForm.getForgotPasswordSubject();
				subject = new AdminAgent().getEmailSubject("FORGOT_PASSWORD");

				// }
				// else subject="Forgot Password";
				if (emailAdminForm.getForgotPasswordMessage() != null) {
					HashMap hm = new HashMap();
					hm.put("USERNAME", toEmail);
					hm.put("PASSWORD", password);
					message = parseString(emailAdminForm.getForgotPasswordMessage(), hm);
				} else {
					message = "Forgot Password ";
				}
			}

			ResourceBundle obcProperties = Wrapper.getResourceBundle();

			String emailFlag = obcProperties.getString("EMAIL_FLAG");

			if (emailFlag.equalsIgnoreCase("off")) {
				toEmail = "rgovind@edgesoftinc.com;";
			}

			new MessageUtils().sendEmail(toEmail, subject, message);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static String parseString(String str, HashMap a) {
		str = Operator.replace(str, "\r\n", "<br>");
		str = Operator.replace(str, "\n", "<br>");

		StringBuffer sb = new StringBuffer();

		Frill f = new Frill(str);

		while (f.next()) {
			sb.append(f.LINE);

			if (f.equals("FIELD") && f.hasValue()) {
				String value = (String) a.get(f.VALUE);

				if (value == null) {
					value = "";
				}

				value = Operator.toHTML(value);

				// value = replace(value,"\n","<br>");
				sb.append(value);
			}
		}

		f.clear();

		return sb.toString();
	}

	public static String cal2TimeStamp(Calendar c) {
		String strTimeStamp = null;
		Calendar cal = Calendar.getInstance();

		if (c != null) {
			try {
				SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
				strTimeStamp = formatter1.format(c.getTime());
			} catch (Exception e) {
				logger.error(e.getMessage());
			}
		}

		return strTimeStamp;
	}

	public static Calendar getCalendar(java.util.Date aDate) throws Exception {

		Calendar cal = Calendar.getInstance();
		cal.setTime(aDate);
		return cal;
	}

	public static Date getDate(java.util.Calendar aCalendar) throws Exception {

		Date date = aCalendar.getTime();
		return date;
	}

	// checks for connection to the internet through dummy request
	public static boolean isInternetReachable() {
		try {
			// make a URL to a known source
			URL url = new URL("http://www.google.com");

			// open a connection to that source
			HttpURLConnection urlConnect = (HttpURLConnection) url.openConnection();

			// trying to retrieve data from the source. If there
			// is no connection, this line will fail
			Object objData = urlConnect.getContent();

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	 public static String nullReplaceWithZero(String aValue){
	        if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {
	            return "0";
	        }
	        else {
	            return aValue.trim();
	        }
	    }
	 public static String sqlDateToString(java.sql.Date date){
			if(date == null){
			return null;	
			}
			Date jDate = new Date(date.getTime());
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
			String dateValue = sdf.format(jDate);
			return dateValue;
			
		}
	 public static String toNoticeDateFormat(String date){
			String noticeDate = null;
			if(date == null || date.equalsIgnoreCase("")){
				return "";
			}
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
			SimpleDateFormat sdfNotice = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
			
			try {
				noticeDate = sdfNotice.format(sdf.parse(date));
			} catch (ParseException e) {
				logger.warn("date is not in correct format");
				return "";
			}
			return noticeDate;
		}
	 
	 public static String cal2NoticeDateFormat(Calendar cal){
			String noticeDate = null;
			if(cal == null){
				return "";
			}
			SimpleDateFormat sdfNotice = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
			noticeDate = sdfNotice.format(cal.getTime());
			return noticeDate;
		}
	 public static String nullReplaceWithBlank(String aValue) {

	        if ((aValue == null) || (aValue.trim().equalsIgnoreCase("null"))) {

	            return " ";
	        }
	        else {

	            return aValue.trim();
	        }
	    }
	 
}

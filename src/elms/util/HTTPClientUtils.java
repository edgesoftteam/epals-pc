/**
 * 
 */
package elms.util;

import java.io.IOException;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import elms.control.actions.project.SubmitGreenHaloAction;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * @author ankur agarwal
 *
 */
public class HTTPClientUtils {
	
	static Logger logger = Logger.getLogger(SubmitGreenHaloAction.class.getName());
	/**
	 * Get the resource bundle
	 * 
	 * @return
	 * @throws Exception
	 */
	public static ResourceBundle getResourceBundle() throws Exception {
		logger.info("getResourceBundle()");

		try {
			ResourceBundle properties = ResourceBundle.getBundle("ApplicationResources");

			return properties;
		} catch (MissingResourceException e) {
			throw new Exception("unable to read property files " + e.getMessage());
		}
	}
	
	public static String post(String url, Map<String, String> headers, RequestBody requestBody){
		Request request = new Request.Builder()
				.url(url)
				.headers(createHeaders(headers))
				.post(requestBody)
				.build();
		try{
			return execute(request).string();
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	private static Headers createHeaders(Map<String, String> headers){
		if(headers.size()==0){
			headers.put("Content-Type", "application/json");
		}
		Headers.Builder headersBuilder = new Headers.Builder();
		for(Map.Entry<String, String> entry : headers.entrySet() ){
			headersBuilder.add(entry.getKey(), entry.getValue());
		}
		return headersBuilder.build();
	}
	
	private static ResponseBody execute(Request request){
		try{
			OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
			OkHttpClient okHttpClient = builder.connectTimeout(60, TimeUnit.SECONDS).build();
			Response response = okHttpClient.newCall(request).execute();
			ResponseBody  body = response.body();
			if(body==null){
				return null;
			}
			if(response.code() >= 300 || response.code()<200){
				throw new RuntimeException("Failed http call(" + response.code()+"):"+body.string());
			}
			return body;
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
	public static String getandReturnString(String url,Map<String, String> headers){
		Request request = new Request.Builder()
				.url(url)
				.headers(createHeaders(headers))
				.build();
		try{
			return execute(request).string();
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}
}
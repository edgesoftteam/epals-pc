package elms.util;

import java.util.Calendar;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

import elms.common.Constants;
import elms.util.db.Wrapper;

public class MessageUtils {
	/**
	 * The logger.
	 */
	static Logger logger = Logger.getLogger(MessageUtils.class.getName());

	/**
	 * the default constructor.
	 * 
	 */
	public MessageUtils() {
	}

	/**
	 * Send emails
	 * 
	 * @param email
	 * @throws Exception
	 */
	public static void sendEmail(Email email) throws Exception {
		logger.info("sendEmail()");

		ResourceBundle bundle = Wrapper.getResourceBundle();

		String host = bundle.getString("EMAIL_SERVER");
		logger.debug("EMAIL_SERVER:" + host);
		String username = bundle.getString("EMAIL_USERNAME");
		logger.debug("EMAIL_USERNAME:" + username);
		String password = bundle.getString("EMAIL_PASSWORD");
		logger.debug("EMAIL_PASSWORD:" + password);
		String from = bundle.getString("EMAIL_FROM");
		logger.debug("EMAIL_FROM:" + from);

		// the to address
		String to = email.getToAddress();
		logger.debug("Email To: " + to);

		// Create properties, get Session
		Properties props = new Properties();

		// need to specify which host to send it to
		props.put("mail.smtp.host", host);

		// To see what is going on behind the scene
		props.put("mail.debug", "true");
		props.put("mail.smtp.auth", "true");
		
		Authenticator auth = new EmailAuthenticator(username, password);
		if (bundle.getString("GMAIL_SERVER") != null && bundle.getString("GMAIL_SERVER").equalsIgnoreCase("on")) {

			String d_host = "smtp.gmail.com", d_port = "465";
			username = "mail.edgesoft@gmail.com";
			password = "edge2000";
			from = "mail.edgesoft@gmail.com";

			props.put("mail.smtp.user", username);
			props.put("mail.smtp.host", d_host);
			props.put("mail.smtp.port", d_port);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.socketFactory.port", d_port);
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.fallback", "false");
			
			auth = new EmailAuthenticator(username, password);
		}

		
		Session session = Session.getInstance(props, auth);

		try {
			// Instantiate a message
			Message msg = new MimeMessage(session);

			// Set message attributes
			msg.setFrom(new InternetAddress(from));

			String toSplit1 = null;
			String toSplit2 = null;
			String toSplit3 = null;
			String toSplit4 = null;
			String toSplit5 = null;
			String toSplit6 = null;
			if (to != null) {
				int semi = to.indexOf(';');
				if (semi != -1) {
					toSplit1 = to.substring(0, to.indexOf(';'));
					to = to.substring(to.indexOf(';') + 1, to.length());
					logger.debug("First EMail is " + toSplit1);
					logger.debug("TO is " + to);
				}
				if (to != null && to.indexOf(';') != -1) {
					toSplit2 = to.substring(0, to.indexOf(';'));
					to = to.substring(to.indexOf(';') + 1, to.length());
					semi = to.indexOf(';');
					logger.debug("Second EMail is " + toSplit2);
				}
				if (to != null && to.indexOf(';') != -1) {
					toSplit3 = to.substring(0, to.indexOf(';'));
					to = to.substring(to.indexOf(';') + 1, to.length());
					semi = to.indexOf(';');
					logger.debug("Third EMail is " + toSplit3);
				}
				if (to != null && to.indexOf(';') != -1) {
					toSplit4 = to.substring(0, to.indexOf(';'));
					to = to.substring(to.indexOf(';') + 1, to.length());
					semi = to.indexOf(';');
				}
				if (to != null && to.indexOf(';') != -1) {
					toSplit5 = to.substring(0, to.indexOf(';'));
					to = to.substring(to.indexOf(';') + 1, to.length());
					semi = to.indexOf(';');
				}
			}
			int length = 0;
			if (toSplit1 != null)
				length++;
			if (toSplit2 != null)
				length++;
			if (toSplit3 != null)
				length++;
			if (toSplit4 != null)
				length++;
			if (toSplit5 != null)
				length++;

			InternetAddress[] address = new InternetAddress[length];
			if (toSplit1 != null)
				address[0] = new InternetAddress(toSplit1);
			if (toSplit2 != null)
				address[1] = new InternetAddress(toSplit2);
			if (toSplit3 != null)
				address[2] = new InternetAddress(toSplit3);
			if (toSplit4 != null)
				address[3] = new InternetAddress(toSplit4);
			if (toSplit5 != null)
				address[4] = new InternetAddress(toSplit5);

			if (bundle.getString("GMAIL_SERVER") != null && bundle.getString("GMAIL_SERVER").equalsIgnoreCase("on")) {
				Address[] toAddress  = InternetAddress.parse(to);
				msg.setRecipients(Message.RecipientType.TO, toAddress);
			
			}else {
				int a = address.length;
				msg.setRecipient(Message.RecipientType.TO, new InternetAddress(Constants.UNDELIVERABLEEMAIL));
				for (int i = 0; i < a; i++) {
					msg.setRecipients(Message.RecipientType.BCC, address);
				}
			}

			// msg.setRecipients(Message.RecipientType.BCC, addressBcc);
			msg.setSubject(email.getSubject());
			Calendar cal = Calendar.getInstance();

			// SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
			msg.setSentDate(cal.getTime());

			// Set message content
			logger.debug("Message is " + email.getMessage());
			msg.setContent(email.getMessage(), "text/html");

			// Send the message
			Transport.send(msg);
			logger.debug("Email message sent successfully");
		} catch (Exception e) {
			logger.error("Error in sending emails " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Send email message with parameters.
	 * 
	 * @param to
	 * @param subject
	 * @param message
	 * @throws Exception
	 */
	public void sendEmail(String to, String subject, String message) throws Exception {
		logger.info("sendEmail(" + to + "," + subject + "," + message + ")");
		try {
			ResourceBundle elmsProperties = Wrapper.getResourceBundle();
			String from = elmsProperties.getString("EMAIL_FROM");

			sendEmailAsNewThread(subject, from, to, message);
		} catch (Exception e) {
			logger.error("Unable to send email message" + e.getMessage());
			throw e;
		}
	}

	/**
	 * @param fileName
	 * @throws Exception
	 */
	public void sendEmailAsNewThread(String subject, String from, String to, String message) throws Exception {
		logger.debug("sendEmailAsNewThread(" + subject + "," + from + "," + to + "," + message + ")");

		final Email email = new Email(subject, from, to, message);

		// start process in new thread
		new Thread(new Runnable() {
			public void run() {
				try {
					sendEmail(email);
				} catch (Exception e) {
					logger.error("run: Exception thrown while attempting to send email by a new thread", e);
				}
			}
		}).start();
	}
}

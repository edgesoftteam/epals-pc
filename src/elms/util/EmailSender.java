package elms.util;

import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpSession;

import elms.agent.ActivityAgent;
import elms.app.project.Activity;
import elms.common.Constants;
import elms.security.User;
import elms.util.db.Wrapper;

import org.apache.log4j.Logger;


public class EmailSender {
	
	static Logger logger = Logger.getLogger(EmailSender.class.getName()); 
	
	public void sendEmails(List<Activity> emailRuleList) throws Exception {
		ResourceBundle prop = Wrapper.getResourceBundle();
		String temp = null;
		String password = prop.getString("EMAIL_PASSWORD");
		String username = prop.getString("EMAIL_USERNAME");
		String server = prop.getString("EMAIL_SERVER");
		String fromAddress = prop.getString("EMAIL_FROM");
		String emailBody = prop.getString("EMAIL_BODY");
		String port = prop.getString("EMAIL_PORT");
			
		logger.info("sendEmails(List<EmailRule> emailRuleList)");

		// Create properties, get Session
		Properties props = new Properties();

		// need to specify which host to send it to
		props.put("mail.smtp.host", server);

		// To see what is going on behind the scene
		props.put("mail.debug", "true");
		props.put("mail.smtp.auth", "true");

		 props.put("mail.smtp.port",port);

		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.starttls.enable", "true");
		
		// props.put("mail.smtp.socketFactory.class",
		// "javax.net.ssl.SSLSocketFactory");
		// props.put("mail.smtp.socketFactory.fallback", "false");

		// the auth
		Authenticator auth = new EmailAuthenticator(username, password);

		// the session
		Session session = Session.getInstance(props, auth);
	

		if (emailRuleList != null) {

			for (int i = 0; i < emailRuleList.size(); i++) {
				Activity emailRule = emailRuleList.get(i);
				try {
					//Updating activity status
					ActivityAgent.updateStatus(emailRule.getActivityId(), getNextStatusId(emailRule.getActivityDetail().getStatus().getStatus()));
					
					// Instantiate a message
					Message msg = new MimeMessage(session);
					// Set message attributes
					msg.setFrom(new InternetAddress(fromAddress));
					logger.debug("email to:"+emailRule.getPeople().getEmailAddress());
					msg.setRecipient(Message.RecipientType.TO,new InternetAddress(emailRule.getPeople().getEmailAddress()));
					msg.setSubject(getSubject(emailRule.getActivityId()));
					temp = getEmailBody(emailBody, emailRule);
					logger.debug("Message is " + temp);
					msg.setContent(temp, "text/html");
					// Send the message
					Transport.send(msg);
					logger.debug("Email message sent successfully");
				} catch (Exception e) {
					logger.error("Error in sending emails "	+ e.getMessage());
					//throw e;
				}
			}
		}
	}
	
	private String getEmailBody(String emailBody, Activity a) {
		emailBody = emailBody.replace("##NAME##", a.getPeople().getName());
		emailBody = emailBody.replace("##ACT_DESC##", a.getActivityDetail().getDescription());
		emailBody = emailBody.replace("##STATUS##", a.getActivityDetail().getStatus().getDescription());
		emailBody = emailBody.replace("##STATUS1##", getNextStatus(a.getActivityDetail().getStatus().getStatus()));
		return emailBody.toString();
	}
	
	private String getSubject(int actId) {
		return "Permit is under you review "+actId;
	}
	
	private String getNextStatus(int statusId) {
		if(statusId == Constants.ACTIVITY_STATUS_UNDER_REVIEW) {
			return "Delinquent (30)";
		} else if (statusId == Constants.ACTIVITY_STATUS_DELINQUENT_30) {
			return "Delinquent (60)";
		} else {
			return "Delinquent (90)";
		}
	}
	private int getNextStatusId(int statusId) {
		if(statusId == Constants.ACTIVITY_STATUS_UNDER_REVIEW) {
			return Constants.ACTIVITY_STATUS_DELINQUENT_30; //"Delinquent (30)"
		} else if (statusId == Constants.ACTIVITY_STATUS_DELINQUENT_30) {
			return Constants.ACTIVITY_STATUS_DELINQUENT_60; //"Delinquent (60)"
		} else {
			return Constants.ACTIVITY_STATUS_DELINQUENT_90; //"Delinquent (90)"
		}
	}
}

/**
 * 
 */
package elms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import elms.common.Constants;

/**
 * @author Gaurav Garg
 *
 */
public class Util {
	
	public void copyFile(String fromFile, String toFile) throws IOException {
		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			from = new FileInputStream(fromFile);
			to = new FileOutputStream(toFile);
			byte[] buffer = new byte[4096];
			int bytesRead;

			while ((bytesRead = from.read(buffer)) != -1)
				to.write(buffer, 0, bytesRead); // write
		} finally {
			if (from != null)
				try {
					from.close();
				} catch (IOException e) {
					;
				}
			if (to != null)
				try {
					to.close();
				} catch (IOException e) {
					;
				}

		}
	}
	
	public void deleteFile(String fileName) throws IOException {
		FileInputStream from = null;
		FileOutputStream to = null;
		try {
			File file = new File(fileName); 
			if(file.exists()){
				file.delete();
			}
		} finally {
			if (from != null)
				try {
					from.close();
				} catch (IOException e) {
					;
				}
			if (to != null)
				try {
					to.close();
				} catch (IOException e) {
					;
				}
		}
	}
	
    public static String addMonthsToDate(String dateString,int months){

    	if(dateString == null || dateString.equalsIgnoreCase("")){
    		return "";
    	}
    	
    	Calendar cl = Calendar.getInstance();

    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    	
    	Date date;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			return "";
		}
    	cl.setTime(date);
    	if(months != 0){
    		cl.add(Calendar.MONTH, months);
    	}
    	date.setTime(cl.getTimeInMillis());
		return sdf.format(date);	
    }
    
    public static String addDaysToCurrentDate(int days){
    	
    	Calendar cl = Calendar.getInstance();

    	SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    	
    	Date date = new Date();

    	if(days != 0){
    		cl.add(Calendar.DATE, days);
    	}
    	date.setTime(cl.getTimeInMillis());
		return sdf.format(date);	
    }
    
    public static String addDaysToDate(String dateString ,int days){
    	
    	if(dateString == null || dateString.equalsIgnoreCase("")){
    		return "";
    	}
    	
    	Calendar cl = Calendar.getInstance();

    	SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
    	
    	Date date;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			return "";
		}
    	cl.setTime(date);
    	if(days != 0){
    		cl.add(Calendar.DATE, days);
    	}
    	date.setTime(cl.getTimeInMillis());
		return sdf.format(date);
    }

    
    public static String addDaysToNoticeDate(String dateString ,int days){
    	
    	if(dateString == null || dateString.equalsIgnoreCase("")){
    		return "";
    	}
    	
    	Calendar cl = Calendar.getInstance();

    	SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
    	
    	Date date;
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			return "";
		}
    	cl.setTime(date);
    	if(days != 0){
    		cl.add(Calendar.DATE, days);
    	}
    	date.setTime(cl.getTimeInMillis());
		return sdf.format(date);
    }
    
    public static boolean reloginRequired(HttpServletRequest request){
    	boolean reloginRequired = false;
		HttpSession session = request.getSession();
        if(session.getAttribute(Constants.USER_KEY) == null){
			request.setAttribute("message", Constants.SESSION_EXPIRED_MESSAGE);
			reloginRequired = true;
        }
    	return reloginRequired;
    }
}

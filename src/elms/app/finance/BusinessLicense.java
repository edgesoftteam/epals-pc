//Source file: C:\\datafile\\source\\elms\\app\\finance\\Finance.java

package elms.app.finance;

import org.apache.log4j.Logger;

/**
 * @author Shekhar Jain
 */
public class BusinessLicense {
	static Logger logger = Logger.getLogger(BusinessLicense.class.getName());

	protected String licenseNumber;
	protected String name;
	protected String active;
	protected String streetNumber;
	protected String streetName;
	protected String address2;
	protected String service;
	protected String title;
	protected String reference;
	protected String date;
	protected String billed;
	protected String paid;

	/**
	 * Returns the licenseNumber.
	 * 
	 * @return String
	 */
	public String getLicenseNumber() {
		return licenseNumber;
	}

	/**
	 * Returns the name.
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the streetName.
	 * 
	 * @return String
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * Returns the streetNumber.
	 * 
	 * @return String
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * Sets the licenseNumber.
	 * 
	 * @param licenseNumber
	 *            The licenseNumber to set
	 */
	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the streetName.
	 * 
	 * @param streetName
	 *            The streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * Sets the streetNumber.
	 * 
	 * @param streetNumber
	 *            The streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * Returns the billed.
	 * 
	 * @return String
	 */
	public String getBilled() {
		return billed;
	}

	/**
	 * Returns the date.
	 * 
	 * @return String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Returns the paid.
	 * 
	 * @return String
	 */
	public String getPaid() {
		return paid;
	}

	/**
	 * Returns the reference.
	 * 
	 * @return String
	 */
	public String getReference() {
		return reference;
	}

	/**
	 * Returns the service.
	 * 
	 * @return String
	 */
	public String getService() {
		return service;
	}

	/**
	 * Returns the title.
	 * 
	 * @return String
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the billed.
	 * 
	 * @param billed
	 *            The billed to set
	 */
	public void setBilled(String billed) {
		this.billed = billed;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Sets the paid.
	 * 
	 * @param paid
	 *            The paid to set
	 */
	public void setPaid(String paid) {
		this.paid = paid;
	}

	/**
	 * Sets the reference.
	 * 
	 * @param reference
	 *            The reference to set
	 */
	public void setReference(String reference) {
		this.reference = reference;
	}

	/**
	 * Sets the service.
	 * 
	 * @param service
	 *            The service to set
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * Sets the title.
	 * 
	 * @param title
	 *            The title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return
	 */
	public String getActive() {
		return active;
	}

	/**
	 * @param string
	 */
	public void setActive(String string) {
		active = string;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

}
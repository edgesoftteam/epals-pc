package elms.app.finance;

import java.io.Serializable;

import org.apache.log4j.Logger;

import elms.util.StringUtils;

public class FinanceSummary implements Serializable {
	static Logger logger = Logger.getLogger(FinanceSummary.class.getName());
	protected String permitFeeAmt = "$0.00";
	protected String permitPaidAmt = "$0.00";
	protected String permitCreditAmt = "$0.00";
	protected String permitAdjustmentAmt = "$0.00";
	protected String permitBouncedAmt = "$0.00";
	protected String permitAmountDue = "$0.00";

	/********* added for penalty ********/
	protected String penaltyFeeAmt = "$0.00";
	protected String penaltyPaidAmt = "$0.00";
	protected String penaltyCreditAmt = "$0.00";
	protected String penaltyAdjustmentAmt = "$0.00";
	protected String penaltyBouncedAmt = "$0.00";
	protected String penaltyAmountDue = "$0.00";

	protected String planCheckFeeAmt = "$0.00";
	protected String planCheckPaidAmt = "$0.00";
	protected String planCheckCreditAmt = "$0.00";
	protected String planCheckAdjustmentAmt = "$0.00";
	protected String planCheckBouncedAmt = "$0.00";
	protected String planCheckAmountDue = "$0.00";

	protected String developmentFeeAmt = "$0.00";
	protected String developmentFeePaidAmt = "$0.00";
	protected String developmentFeeCreditAmt = "$0.00";
	protected String developmentFeeAdjustmentAmt = "$0.00";
	protected String developmentFeeBouncedAmt = "$0.00";
	protected String developmentFeeAmountDue = "$0.00";

	protected String businessTaxFeeAmt = "$0.00";
	protected String businessTaxPaidAmt = "$0.00";
	protected String businessTaxCreditAmt = "$0.00";
	protected String businessTaxAdjustmentAmt = "$0.00";
	protected String businessTaxBouncedAmt = "$0.00";
	protected String businessTaxAmountDue = "$0.00";

	protected String otherFeeAmt = "$0.00";
	protected String otherPaidAmt = "$0.00";
	protected String otherCreditAmt = "$0.00";
	protected String otherAdjustmentAmt = "$0.00";
	protected String otherBouncedAmt = "$0.00";

	protected String otherAmountDue = "$0.00";

	protected String totalFeeAmt = "$0.00";
	protected String totalPaidAmt = "$0.00";
	protected String totalCreditAmt = "$0.00";
	protected String totalAdjustmentAmt = "$0.00";
	protected String totalBouncedAmt = "$0.00";
	protected String totalAmountDue = "$0.00";
	protected double amountDue = 0.00;

	public void add(Finance finance) {
		switch (finance.getType()) {
		case 'P':
			this.permitFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.permitPaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.permitCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.permitAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.permitBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.permitAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			this.amountDue = finance.getAmountDue();
			break;
		case 'B':
			this.planCheckFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.planCheckPaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.planCheckCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.planCheckAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.planCheckBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.planCheckAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			break;

		case 'D':
			this.developmentFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.developmentFeePaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.developmentFeeCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.developmentFeeAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.developmentFeeBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.developmentFeeAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			break;
		case 'X':
			this.businessTaxFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.businessTaxPaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.businessTaxCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.businessTaxAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.businessTaxBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.businessTaxAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			break;
		case 'O':
			this.otherFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.otherPaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.otherCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.otherAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.otherBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.otherAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			break;
		case 'T':
			this.totalFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.totalPaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.totalCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.totalAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.totalBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.totalAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			break;
		case 'F':
			this.penaltyFeeAmt = StringUtils.dbl2$(finance.getFeeAmt());
			this.penaltyPaidAmt = StringUtils.dbl2$(finance.getPaidAmt());
			this.penaltyCreditAmt = StringUtils.dbl2$(finance.getCreditAmt());
			this.penaltyAdjustmentAmt = StringUtils.dbl2$(finance.getAdjustmentAmt());
			this.permitBouncedAmt = StringUtils.dbl2$(finance.getBouncedAmt());
			this.penaltyAmountDue = StringUtils.dbl2$(finance.getAmountDue());
			break;

		}
		return;
	}

	/**
	 * @return Returns the developmentFeeAdjustmentAmt.
	 */
	public String getDevelopmentFeeAdjustmentAmt() {
		return developmentFeeAdjustmentAmt;
	}

	/**
	 * @param developmentFeeAdjustmentAmt
	 *            The developmentFeeAdjustmentAmt to set.
	 */
	public void setDevelopmentFeeAdjustmentAmt(String developmentFeeAdjustmentAmt) {
		this.developmentFeeAdjustmentAmt = developmentFeeAdjustmentAmt;
	}

	/**
	 * @return Returns the developmentFeeAmountDue.
	 */
	public String getDevelopmentFeeAmountDue() {
		return developmentFeeAmountDue;
	}

	/**
	 * @param developmentFeeAmountDue
	 *            The developmentFeeAmountDue to set.
	 */
	public void setDevelopmentFeeAmountDue(String developmentFeeAmountDue) {
		this.developmentFeeAmountDue = developmentFeeAmountDue;
	}

	/**
	 * @return Returns the developmentFeeBouncedAmt.
	 */
	public String getDevelopmentFeeBouncedAmt() {
		return developmentFeeBouncedAmt;
	}

	/**
	 * @param developmentFeeBouncedAmt
	 *            The developmentFeeBouncedAmt to set.
	 */
	public void setDevelopmentFeeBouncedAmt(String developmentFeeBouncedAmt) {
		this.developmentFeeBouncedAmt = developmentFeeBouncedAmt;
	}

	/**
	 * @return Returns the developmentFeeCreditAmt.
	 */
	public String getDevelopmentFeeCreditAmt() {
		return developmentFeeCreditAmt;
	}

	/**
	 * @param developmentFeeCreditAmt
	 *            The developmentFeeCreditAmt to set.
	 */
	public void setDevelopmentFeeCreditAmt(String developmentFeeCreditAmt) {
		this.developmentFeeCreditAmt = developmentFeeCreditAmt;
	}

	/**
	 * @return Returns the developmentFeePaidAmt.
	 */
	public String getDevelopmentFeePaidAmt() {
		return developmentFeePaidAmt;
	}

	/**
	 * @param developmentFeePaidAmt
	 *            The developmentFeePaidAmt to set.
	 */
	public void setDevelopmentFeePaidAmt(String developmentFeePaidAmt) {
		this.developmentFeePaidAmt = developmentFeePaidAmt;
	}

	/**
	 * @return Returns the developmentFeeAmt.
	 */
	public String getDevelopmentFeeAmt() {
		return developmentFeeAmt;
	}

	/**
	 * @param developmentFeeAmt
	 *            The developmentFeeAmt to set.
	 */
	public void setDevelopmentFeeAmt(String developmentFeeAmt) {
		this.developmentFeeAmt = developmentFeeAmt;
	}

	/**
	 * Gets the permitFeeAmt
	 * 
	 * @return Returns a String
	 */
	public String getPermitFeeAmt() {
		return permitFeeAmt;
	}

	/**
	 * Sets the permitFeeAmt
	 * 
	 * @param permitFeeAmt
	 *            The permitFeeAmt to set
	 */
	public void setPermitFeeAmt(String permitFeeAmt) {
		this.permitFeeAmt = permitFeeAmt;
	}

	/**
	 * Gets the permitPaidAmt
	 * 
	 * @return Returns a String
	 */
	public String getPermitPaidAmt() {
		return permitPaidAmt;
	}

	/**
	 * Sets the permitPaidAmt
	 * 
	 * @param permitPaidAmt
	 *            The permitPaidAmt to set
	 */
	public void setPermitPaidAmt(String permitPaidAmt) {
		this.permitPaidAmt = permitPaidAmt;
	}

	/**
	 * Gets the permitCreditAmt
	 * 
	 * @return Returns a String
	 */
	public String getPermitCreditAmt() {
		return permitCreditAmt;
	}

	/**
	 * Sets the permitCreditAmt
	 * 
	 * @param permitCreditAmt
	 *            The permitCreditAmt to set
	 */
	public void setPermitCreditAmt(String permitCreditAmt) {
		this.permitCreditAmt = permitCreditAmt;
	}

	/**
	 * Gets the permitAdjustmentAmt
	 * 
	 * @return Returns a String
	 */
	public String getPermitAdjustmentAmt() {
		return permitAdjustmentAmt;
	}

	/**
	 * Sets the permitAdjustmentAmt
	 * 
	 * @param permitAdjustmentAmt
	 *            The permitAdjustmentAmt to set
	 */
	public void setPermitAdjustmentAmt(String permitAdjustmentAmt) {
		this.permitAdjustmentAmt = permitAdjustmentAmt;
	}

	/**
	 * Gets the permitAmountDue
	 * 
	 * @return Returns a String
	 */
	public String getPermitAmountDue() {
		return permitAmountDue;
	}

	/**
	 * Sets the permitAmountDue
	 * 
	 * @param permitAmountDue
	 *            The permitAmountDue to set
	 */
	public void setPermitAmountDue(String permitAmountDue) {
		this.permitAmountDue = permitAmountDue;
	}

	/**
	 * Gets the planCheckFeeAmt
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckFeeAmt() {
		return planCheckFeeAmt;
	}

	/**
	 * Sets the planCheckFeeAmt
	 * 
	 * @param planCheckFeeAmt
	 *            The planCheckFeeAmt to set
	 */
	public void setPlanCheckFeeAmt(String planCheckFeeAmt) {
		this.planCheckFeeAmt = planCheckFeeAmt;
	}

	/**
	 * Gets the planCheckPaidAmt
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckPaidAmt() {
		return planCheckPaidAmt;
	}

	/**
	 * Sets the planCheckPaidAmt
	 * 
	 * @param planCheckPaidAmt
	 *            The planCheckPaidAmt to set
	 */
	public void setPlanCheckPaidAmt(String planCheckPaidAmt) {
		this.planCheckPaidAmt = planCheckPaidAmt;
	}

	/**
	 * Gets the planCheckCreditAmt
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckCreditAmt() {
		return planCheckCreditAmt;
	}

	/**
	 * Sets the planCheckCreditAmt
	 * 
	 * @param planCheckCreditAmt
	 *            The planCheckCreditAmt to set
	 */
	public void setPlanCheckCreditAmt(String planCheckCreditAmt) {
		this.planCheckCreditAmt = planCheckCreditAmt;
	}

	/**
	 * Gets the planCheckAdjustmentAmt
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckAdjustmentAmt() {
		return planCheckAdjustmentAmt;
	}

	/**
	 * Sets the planCheckAdjustmentAmt
	 * 
	 * @param planCheckAdjustmentAmt
	 *            The planCheckAdjustmentAmt to set
	 */
	public void setPlanCheckAdjustmentAmt(String planCheckAdjustmentAmt) {
		this.planCheckAdjustmentAmt = planCheckAdjustmentAmt;
	}

	/**
	 * Gets the planCheckAmountDue
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckAmountDue() {
		return planCheckAmountDue;
	}

	/**
	 * Sets the planCheckAmountDue
	 * 
	 * @param planCheckAmountDue
	 *            The planCheckAmountDue to set
	 */
	public void setPlanCheckAmountDue(String planCheckAmountDue) {
		this.planCheckAmountDue = planCheckAmountDue;
	}

	/**
	 * Gets the businessTaxFeeAmt
	 * 
	 * @return Returns a String
	 */
	public String getBusinessTaxFeeAmt() {
		return businessTaxFeeAmt;
	}

	/**
	 * Sets the businessTaxFeeAmt
	 * 
	 * @param businessTaxFeeAmt
	 *            The businessTaxFeeAmt to set
	 */
	public void setBusinessTaxFeeAmt(String businessTaxFeeAmt) {
		this.businessTaxFeeAmt = businessTaxFeeAmt;
	}

	/**
	 * Gets the businessTaxPaidAmt
	 * 
	 * @return Returns a String
	 */
	public String getBusinessTaxPaidAmt() {
		return businessTaxPaidAmt;
	}

	/**
	 * Sets the businessTaxPaidAmt
	 * 
	 * @param businessTaxPaidAmt
	 *            The businessTaxPaidAmt to set
	 */
	public void setBusinessTaxPaidAmt(String businessTaxPaidAmt) {
		this.businessTaxPaidAmt = businessTaxPaidAmt;
	}

	/**
	 * Gets the businessTaxCreditAmt
	 * 
	 * @return Returns a String
	 */
	public String getBusinessTaxCreditAmt() {
		return businessTaxCreditAmt;
	}

	/**
	 * Sets the businessTaxCreditAmt
	 * 
	 * @param businessTaxCreditAmt
	 *            The businessTaxCreditAmt to set
	 */
	public void setBusinessTaxCreditAmt(String businessTaxCreditAmt) {
		this.businessTaxCreditAmt = businessTaxCreditAmt;
	}

	/**
	 * Gets the businessTaxAdjustmentAmt
	 * 
	 * @return Returns a String
	 */
	public String getBusinessTaxAdjustmentAmt() {
		return businessTaxAdjustmentAmt;
	}

	/**
	 * Sets the businessTaxAdjustmentAmt
	 * 
	 * @param businessTaxAdjustmentAmt
	 *            The businessTaxAdjustmentAmt to set
	 */
	public void setBusinessTaxAdjustmentAmt(String businessTaxAdjustmentAmt) {
		this.businessTaxAdjustmentAmt = businessTaxAdjustmentAmt;
	}

	/**
	 * Gets the businessTaxAmountDue
	 * 
	 * @return Returns a String
	 */
	public String getBusinessTaxAmountDue() {
		return businessTaxAmountDue;
	}

	/**
	 * Sets the businessTaxAmountDue
	 * 
	 * @param businessTaxAmountDue
	 *            The businessTaxAmountDue to set
	 */
	public void setBusinessTaxAmountDue(String businessTaxAmountDue) {
		this.businessTaxAmountDue = businessTaxAmountDue;
	}

	/**
	 * Gets the otherFeeAmt
	 * 
	 * @return Returns a String
	 */
	public String getOtherFeeAmt() {
		return otherFeeAmt;
	}

	/**
	 * Sets the otherFeeAmt
	 * 
	 * @param otherFeeAmt
	 *            The otherFeeAmt to set
	 */
	public void setOtherFeeAmt(String otherFeeAmt) {
		this.otherFeeAmt = otherFeeAmt;
	}

	/**
	 * Gets the otherPaidAmt
	 * 
	 * @return Returns a String
	 */
	public String getOtherPaidAmt() {
		return otherPaidAmt;
	}

	/**
	 * Sets the otherPaidAmt
	 * 
	 * @param otherPaidAmt
	 *            The otherPaidAmt to set
	 */
	public void setOtherPaidAmt(String otherPaidAmt) {
		this.otherPaidAmt = otherPaidAmt;
	}

	/**
	 * Gets the otherCreditAmt
	 * 
	 * @return Returns a String
	 */
	public String getOtherCreditAmt() {
		return otherCreditAmt;
	}

	/**
	 * Sets the otherCreditAmt
	 * 
	 * @param otherCreditAmt
	 *            The otherCreditAmt to set
	 */
	public void setOtherCreditAmt(String otherCreditAmt) {
		this.otherCreditAmt = otherCreditAmt;
	}

	/**
	 * Gets the otherAdjustmentAmt
	 * 
	 * @return Returns a String
	 */
	public String getOtherAdjustmentAmt() {
		return otherAdjustmentAmt;
	}

	/**
	 * Sets the otherAdjustmentAmt
	 * 
	 * @param otherAdjustmentAmt
	 *            The otherAdjustmentAmt to set
	 */
	public void setOtherAdjustmentAmt(String otherAdjustmentAmt) {
		this.otherAdjustmentAmt = otherAdjustmentAmt;
	}

	/**
	 * Gets the otherAmountDue
	 * 
	 * @return Returns a String
	 */
	public String getOtherAmountDue() {
		return otherAmountDue;
	}

	/**
	 * Sets the otherAmountDue
	 * 
	 * @param otherAmountDue
	 *            The otherAmountDue to set
	 */
	public void setOtherAmountDue(String otherAmountDue) {
		this.otherAmountDue = otherAmountDue;
	}

	/**
	 * Gets the totalFeeAmt
	 * 
	 * @return Returns a String
	 */
	public String getTotalFeeAmt() {
		return totalFeeAmt;
	}

	/**
	 * Sets the totalFeeAmt
	 * 
	 * @param totalFeeAmt
	 *            The totalFeeAmt to set
	 */
	public void setTotalFeeAmt(String totalFeeAmt) {
		this.totalFeeAmt = totalFeeAmt;
	}

	/**
	 * Gets the totalPaidAmt
	 * 
	 * @return Returns a String
	 */
	public String getTotalPaidAmt() {
		return totalPaidAmt;
	}

	/**
	 * Sets the totalPaidAmt
	 * 
	 * @param totalPaidAmt
	 *            The totalPaidAmt to set
	 */
	public void setTotalPaidAmt(String totalPaidAmt) {
		this.totalPaidAmt = totalPaidAmt;
	}

	/**
	 * Gets the totalCreditAmt
	 * 
	 * @return Returns a String
	 */
	public String getTotalCreditAmt() {
		return totalCreditAmt;
	}

	/**
	 * Sets the totalCreditAmt
	 * 
	 * @param totalCreditAmt
	 *            The totalCreditAmt to set
	 */
	public void setTotalCreditAmt(String totalCreditAmt) {
		this.totalCreditAmt = totalCreditAmt;
	}

	/**
	 * Gets the totalAdjustmentAmt
	 * 
	 * @return Returns a String
	 */
	public String getTotalAdjustmentAmt() {
		return totalAdjustmentAmt;
	}

	/**
	 * Sets the totalAdjustmentAmt
	 * 
	 * @param totalAdjustmentAmt
	 *            The totalAdjustmentAmt to set
	 */
	public void setTotalAdjustmentAmt(String totalAdjustmentAmt) {
		this.totalAdjustmentAmt = totalAdjustmentAmt;
	}

	/**
	 * Gets the totalAmountDue
	 * 
	 * @return Returns a String
	 */
	public String getTotalAmountDue() {
		return totalAmountDue;
	}

	/**
	 * Sets the totalAmountDue
	 * 
	 * @param totalAmountDue
	 *            The totalAmountDue to set
	 */
	public void setTotalAmountDue(String totalAmountDue) {
		this.totalAmountDue = totalAmountDue;
	}

	/**
	 * Returns the businessTaxBounced.
	 * 
	 * @return String
	 */
	public String getBusinessTaxBouncedAmt() {
		return businessTaxBouncedAmt;
	}

	/**
	 * Returns the permitBounced.
	 * 
	 * @return String
	 */
	public String getPermitBouncedAmt() {
		return permitBouncedAmt;
	}

	/**
	 * Returns the planCheckBounced.
	 * 
	 * @return String
	 */
	public String getPlanCheckBouncedAmt() {
		return planCheckBouncedAmt;
	}

	/**
	 * Returns the totalBounced.
	 * 
	 * @return String
	 */
	public String getTotalBouncedAmt() {
		return totalBouncedAmt;
	}

	/**
	 * Sets the businessTaxBounced.
	 * 
	 * @param businessTaxBounced
	 *            The businessTaxBounced to set
	 */
	public void setBusinessTaxBouncedAmt(String businessTaxBounced) {
		this.businessTaxBouncedAmt = businessTaxBounced;
	}

	/**
	 * Sets the permitBounced.
	 * 
	 * @param permitBounced
	 *            The permitBounced to set
	 */
	public void setPermitBouncedAmt(String permitBounced) {
		this.permitBouncedAmt = permitBounced;
	}

	/**
	 * Sets the planCheckBounced.
	 * 
	 * @param planCheckBounced
	 *            The planCheckBounced to set
	 */
	public void setPlanCheckBouncedAmt(String planCheckBounced) {
		this.planCheckBouncedAmt = planCheckBounced;
	}

	/**
	 * Sets the totalBounced.
	 * 
	 * @param totalBounced
	 *            The totalBounced to set
	 */
	public void setTotalBouncedAmt(String totalBounced) {
		this.totalBouncedAmt = totalBounced;
	}

	/**
	 * @return
	 */
	public double getAmountDue() {
		return amountDue;
	}

	/**
	 * @return Returns the penaltyFeeAmt.
	 */
	public String getPenaltyFeeAmt() {
		return penaltyFeeAmt;
	}

	/**
	 * @param penaltyFeeAmt
	 *            The penaltyFeeAmt to set.
	 */
	public void setPenaltyFeeAmt(String penaltyFeeAmt1) {
		this.penaltyFeeAmt = penaltyFeeAmt1;
	}

	/**
	 * @return Returns the penaltyAdjustmentAmt.
	 */
	public String getPenaltyAdjustmentAmt() {
		return penaltyAdjustmentAmt;
	}

	/**
	 * @param penaltyAdjustmentAmt
	 *            The penaltyAdjustmentAmt to set.
	 */
	public void setPenaltyAdjustmentAmt(String penaltyAdjustmentAmt) {
		this.penaltyAdjustmentAmt = penaltyAdjustmentAmt;
	}

	/**
	 * @return Returns the penaltyAmountDue.
	 */
	public String getPenaltyAmountDue() {
		return penaltyAmountDue;

	}

	/**
	 * @param penaltyAmountDue
	 *            The penaltyAmountDue to set.
	 */
	public void setPenaltyAmountDue(String penaltyAmountDue) {
		this.penaltyAmountDue = penaltyAmountDue;
	}

	/**
	 * @return Returns the penaltyBouncedAmt.
	 */
	public String getPenaltyBouncedAmt() {
		return penaltyBouncedAmt;
	}

	/**
	 * @param penaltyBouncedAmt
	 *            The penaltyBouncedAmt to set.
	 */
	public void setPenaltyBouncedAmt(String penaltyBouncedAmt) {
		this.penaltyBouncedAmt = penaltyBouncedAmt;
	}

	/**
	 * @return Returns the penaltyCreditAmt.
	 */
	public String getPenaltyCreditAmt() {
		return penaltyCreditAmt;
	}

	/**
	 * @param penaltyCreditAmt
	 *            The penaltyCreditAmt to set.
	 */
	public void setPenaltyCreditAmt(String penaltyCreditAmt) {
		this.penaltyCreditAmt = penaltyCreditAmt;
	}

	/**
	 * @return Returns the penaltyPaidAmt.
	 */
	public String getPenaltyPaidAmt() {
		return penaltyPaidAmt;
	}

	/**
	 * @param penaltyPaidAmt
	 *            The penaltyPaidAmt to set.
	 */
	public void setPenaltyPaidAmt(String penaltyPaidAmt) {
		this.penaltyPaidAmt = penaltyPaidAmt;
	}
}
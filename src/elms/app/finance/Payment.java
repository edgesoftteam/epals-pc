//Source file: C:\\datafile\\source\\elms\\app\\finance\\Payment.java

package elms.app.finance;

import java.util.Calendar;
import java.util.Date;

import elms.app.admin.PayType;
import elms.security.User;

public class Payment {
	protected int paymentId;
	protected int levelId;
	protected String method = "";
	// protected int type;
	protected PayType type;
	protected Calendar paymentDate;
	protected String strPaymentDate;
	protected int peopleId;
	protected String payee = "";
	protected double amount;
	protected String accountNbr = "";
	protected String comment = "";
	protected User enteredBy;
	protected String authorizedBy;
	protected char voided = 'N';
	protected int voidBy;
	protected Calendar voidDate;
	protected Calendar postDate;
	protected String strPostDate;
	protected String departmentCode = "";
	protected String depositBalance = "";
	protected String strAmnt = "";
	protected String actNbr = "";
	protected String strDate = "";
	
	protected String onlineTxnId = "";

	/**
	 * @roseuid 3CAA028201F2
	 */
	public Payment() {

	}

	public Payment(String method,
	// int type,
			PayType type, int peopleId, String payee, double amount, String accountNbr, String comment, User enteredBy, String authorizedBy, char voided) {

		this.method = method;
		this.type = type;
		this.peopleId = peopleId;
		this.payee = payee;
		this.amount = amount;
		this.accountNbr = accountNbr;
		this.comment = comment;
		this.authorizedBy = authorizedBy;
		this.voided = voided;
		this.enteredBy = enteredBy;
	}

	/**
	 * Access method for the paymentId property.
	 * 
	 * @return the current value of the paymentId property
	 */
	public int getPaymentId() {
		return paymentId;
	}

	/**
	 * Sets the value of the paymentId property.
	 * 
	 * @param aPaymentId
	 *            the new value of the paymentId property
	 */
	public void setPaymentId(int aPaymentId) {
		paymentId = aPaymentId;
	}

	/**
	 * Access method for the method property.
	 * 
	 * @return the current value of the method property
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Sets the value of the method property.
	 * 
	 * @param aMethod
	 *            the new value of the method property
	 */
	public void setMethod(String aMethod) {
		method = aMethod;
	}

	/**
	 * Access method for the type property.
	 * 
	 * @return the current value of the type property
	 */
	// public int getType() {
	public PayType getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param aType
	 *            the new value of the type property
	 */
	// public void setType(int aType) {
	public void setType(PayType aType) {
		type = aType;
	}

	/**
	 * Access method for the peopleId property.
	 * 
	 * @return the current value of the peopleId property
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the value of the peopleId property.
	 * 
	 * @param aPeopleId
	 *            the new value of the peopleId property
	 */
	public void setPeopleId(int aPeopleId) {
		peopleId = aPeopleId;
	}

	/**
	 * Access method for the accountNbr property.
	 * 
	 * @return the current value of the accountNbr property
	 */
	public String getAccountNbr() {
		return accountNbr;
	}

	/**
	 * Sets the value of the accountNbr property.
	 * 
	 * @param aAccountNbr
	 *            the new value of the accountNbr property
	 */
	public void setAccountNbr(String aAccountNbr) {
		accountNbr = aAccountNbr;
	}

	/**
	 * Gets the paymentDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getPaymentDate() {
		return paymentDate;
	}

	/**
	 * Sets the paymentDate
	 * 
	 * @param paymentDate
	 *            The paymentDate to set
	 */
	public void setPaymentDate(Calendar paymentDate) {
		this.paymentDate = paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		if (paymentDate == null) {
			this.paymentDate = null;
		} else {
			this.paymentDate.setTime(paymentDate);
		}
	}

	/**
	 * Gets the comment
	 * 
	 * @return Returns a String
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment
	 * 
	 * @param comment
	 *            The comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the voided
	 * 
	 * @return Returns a char
	 */
	public char getVoided() {
		return voided;
	}

	/**
	 * Sets the voided
	 * 
	 * @param voided
	 *            The voided to set
	 */
	public void setVoided(char voided) {
		this.voided = voided;
	}

	/**
	 * Gets the voidBy
	 * 
	 * @return Returns a int
	 */
	public int getVoidBy() {
		return voidBy;
	}

	/**
	 * Sets the voidBy
	 * 
	 * @param voidBy
	 *            The voidBy to set
	 */
	public void setVoidBy(int voidBy) {
		this.voidBy = voidBy;
	}

	/**
	 * Gets the voidDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getVoidDate() {
		return voidDate;
	}

	/**
	 * Sets the voidDate
	 * 
	 * @param voidDate
	 *            The voidDate to set
	 */
	public void setVoidDate(Calendar voidDate) {
		this.voidDate = voidDate;
	}

	/**
	 * Gets the departmentCode
	 * 
	 * @return Returns a String
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}

	/**
	 * Sets the departmentCode
	 * 
	 * @param departmentCode
	 *            The departmentCode to set
	 */
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	/**
	 * Gets the payee
	 * 
	 * @return Returns a String
	 */
	public String getPayee() {
		return payee;
	}

	/**
	 * Sets the payee
	 * 
	 * @param payee
	 *            The payee to set
	 */
	public void setPayee(String payee) {
		this.payee = payee;
	}

	public String toString() {
		return ("Payment[" + getPaymentId() + ":" + getType() + ":" + getMethod() + ":" + getAmount() + "]");
	}

	/**
	 * Gets the postDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getPostDate() {
		return postDate;
	}

	/**
	 * Sets the postDate
	 * 
	 * @param postDate
	 *            The postDate to set
	 */
	public void setPostDate(Calendar postDate) {
		this.postDate = postDate;
	}

	/**
	 * Sets the paymentDate as String
	 * 
	 * @param paymentDate
	 *            The paymentDate to set
	 */
	public void setStrPaymentDate(String paymentDate) {
		this.strPaymentDate = paymentDate;
	}

	public String getStrPaymentDate() {
		return strPaymentDate;
	}

	/**
	 * Sets the paymentDate as String
	 * 
	 * @param paymentDate
	 *            The paymentDate to set
	 */
	public void setStrPostDate(String postDate) {
		this.strPostDate = postDate;
	}

	public String getStrPostDate() {
		return strPostDate;
	}

	/**
	 * Gets the enteredBy
	 * 
	 * @return Returns a User
	 */
	public User getEnteredBy() {
		return enteredBy;
	}

	/**
	 * Sets the enteredBy
	 * 
	 * @param enteredBy
	 *            The enteredBy to set
	 */
	public void setEnteredBy(User enteredBy) {
		this.enteredBy = enteredBy;
	}

	/**
	 * Gets the amount
	 * 
	 * @return Returns a double
	 */
	public double getAmount() {
		return amount;
	}

	/**
	 * Sets the amount
	 * 
	 * @param amount
	 *            The amount to set
	 */
	public void setAmount(double amount) {
		this.amount = amount;
	}

	/**
	 * Returns the actNbr.
	 * 
	 * @return String
	 */
	public String getActNbr() {
		return actNbr;
	}

	/**
	 * Sets the actNbr.
	 * 
	 * @param actNbr
	 *            The actNbr to set
	 */
	public void setActNbr(String actNbr) {
		this.actNbr = actNbr;
	}

	/**
	 * Returns the depositBalance.
	 * 
	 * @return String
	 */
	public String getDepositBalance() {
		return depositBalance;
	}

	/**
	 * Sets the depositBalance.
	 * 
	 * @param depositBalance
	 *            The depositBalance to set
	 */
	public void setDepositBalance(String depositBalance) {
		this.depositBalance = depositBalance;
	}

	/**
	 * Returns the strAmnt.
	 * 
	 * @return String
	 */
	public String getStrAmnt() {
		return strAmnt;
	}

	/**
	 * Returns the strDate.
	 * 
	 * @return String
	 */
	public String getStrDate() {
		return strDate;
	}

	/**
	 * Sets the strAmnt.
	 * 
	 * @param strAmnt
	 *            The strAmnt to set
	 */
	public void setStrAmnt(String strAmnt) {
		this.strAmnt = strAmnt;
	}

	/**
	 * Sets the strDate.
	 * 
	 * @param strDate
	 *            The strDate to set
	 */
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}

	/**
	 * @return
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * @param i
	 */
	public void setLevelId(int i) {
		levelId = i;
	}

	/**
	 * @return
	 */
	public String getAuthorizedBy() {
		return authorizedBy;
	}

	/**
	 * @param string
	 */
	public void setAuthorizedBy(String string) {
		authorizedBy = string;
	}

	public String getOnlineTxnId() {
		return onlineTxnId;
	}

	public void setOnlineTxnId(String onlineTxnId) {
		this.onlineTxnId = onlineTxnId;
	}


}
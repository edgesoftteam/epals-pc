//Source file: C:\\datafile\\source\\elms\\app\\finance\\Finance.java

package elms.app.finance;

import java.text.DecimalFormat;

import org.apache.log4j.Logger;

/**
 * @author Shekhar Jain
 */
public class Finance {
	static Logger logger = Logger.getLogger(Finance.class.getName());

	protected char type;
	protected double feeAmt = 0;
	protected double paidAmt = 0;
	protected double creditAmt = 0;
	protected double adjustmentAmt = 0;
	protected double bouncedAmt = 0;
	protected double amountDue = 0;

	/**
	 * @roseuid 3CF6C1C0037B
	 */
	public Finance() {
		this.setType(' ');
	}

	public Finance(char aType) {
		this.type = aType;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public char getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(char type) {
		this.type = type;
	}

	/**
	 * Calculates the amountDue from the fee amount and payments
	 */
	public void setAmountDue() {
		this.amountDue = this.feeAmt - this.paidAmt - this.creditAmt - this.adjustmentAmt + this.bouncedAmt;
		this.amountDue = Double.parseDouble(new DecimalFormat("0.00").format(this.amountDue));
	}

	/**
	 * Adds the amounts from the passed object to the current object
	 */

	public void add(Finance finance) {
		this.feeAmt += finance.getFeeAmt();
		this.paidAmt += finance.getPaidAmt();
		this.creditAmt += finance.getCreditAmt();
		this.adjustmentAmt += finance.getAdjustmentAmt();
		this.bouncedAmt += finance.getBouncedAmt();
		setAmountDue();
	}

	public String toString() {
		return this.type + ":" + feeAmt + ":";
	}

	/**
	 * Gets the feeAmt
	 * 
	 * @return Returns a double
	 */
	public double getFeeAmt() {
		return feeAmt;
	}

	/**
	 * Sets the feeAmt
	 * 
	 * @param feeAmt
	 *            The feeAmt to set
	 */
	public void setFeeAmt(double feeAmt) {
		this.feeAmt = feeAmt;
	}

	/**
	 * Gets the paidAmt
	 * 
	 * @return Returns a double
	 */
	public double getPaidAmt() {
		return paidAmt;
	}

	/**
	 * Sets the paidAmt
	 * 
	 * @param paidAmt
	 *            The paidAmt to set
	 */
	public void setPaidAmt(double paidAmt) {
		this.paidAmt = paidAmt;
	}

	/**
	 * Gets the creditAmt
	 * 
	 * @return Returns a double
	 */
	public double getCreditAmt() {
		return creditAmt;
	}

	/**
	 * Sets the creditAmt
	 * 
	 * @param creditAmt
	 *            The creditAmt to set
	 */
	public void setCreditAmt(double creditAmt) {
		this.creditAmt = creditAmt;
	}

	/**
	 * Gets the adjustmentAmt
	 * 
	 * @return Returns a double
	 */
	public double getAdjustmentAmt() {
		return adjustmentAmt;
	}

	/**
	 * Sets the adjustmentAmt
	 * 
	 * @param adjustmentAmt
	 *            The adjustmentAmt to set
	 */
	public void setAdjustmentAmt(double adjustmentAmt) {
		this.adjustmentAmt = adjustmentAmt;
	}

	/**
	 * Gets the amountDue
	 * 
	 * @return Returns a double
	 */
	public double getAmountDue() {
		return amountDue;
	}

	/**
	 * Sets the amountDue
	 * 
	 * @param amountDue
	 *            The amountDue to set
	 */
	public void setAmountDue(double amountDue) {
		this.amountDue = amountDue;
	}

	/**
	 * Returns the bouncedAmt.
	 * 
	 * @return double
	 */
	public double getBouncedAmt() {
		return bouncedAmt;
	}

	/**
	 * Sets the bouncedAmt.
	 * 
	 * @param bouncedAmt
	 *            The bouncedAmt to set
	 */
	public void setBouncedAmt(double bouncedAmt) {
		this.bouncedAmt = bouncedAmt;
	}

}
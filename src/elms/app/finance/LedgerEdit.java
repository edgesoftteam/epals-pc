package elms.app.finance;

import java.io.Serializable;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class LedgerEdit implements Serializable {

	private int activityId;
	private String hide;
	private String transactionId;
	private String date;
	private String postDate;
	private String activity;
	private String type;
	private String amount;
	private String method;
	private String comments;
	private String checkNbr;
	private String depositId;
	private String otherPayee;
	private String enteredBy;
	private String authorizedBy;
	private String voidBy;
	private String voidPayment;
	private String voidDate;
	
	/**
	 * Constructor for LedgerEdit.
	 */
	public LedgerEdit() {
		super();
	}

	/**
	 * Returns the activity.
	 * 
	 * @return String
	 */
	public String getActivity() {
		return activity;
	}

	/**
	 * Returns the activityId.
	 * 
	 * @return int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Returns the amount.
	 * 
	 * @return String
	 */
	public String getAmount() {
		return amount;
	}

	/**
	 * Returns the authorizedBy.
	 * 
	 * @return String
	 */
	public String getAuthorizedBy() {
		return authorizedBy;
	}

	/**
	 * Returns the checkNbr.
	 * 
	 * @return String
	 */
	public String getCheckNbr() {
		return checkNbr;
	}

	/**
	 * Returns the comments.
	 * 
	 * @return String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Returns the date.
	 * 
	 * @return String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Returns the depositid.
	 * 
	 * @return String
	 */
	public String getDepositId() {
		return depositId;
	}

	/**
	 * Returns the enteredBy.
	 * 
	 * @return String
	 */
	public String getEnteredBy() {
		return enteredBy;
	}

	/**
	 * Returns the hide.
	 * 
	 * @return String
	 */
	public String getHide() {
		return hide;
	}

	/**
	 * Returns the method.
	 * 
	 * @return String
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Returns the postDate.
	 * 
	 * @return String
	 */
	public String getPostDate() {
		return postDate;
	}

	/**
	 * Returns the transactionId.
	 * 
	 * @return String
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * Returns the type.
	 * 
	 * @return String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Returns the voidBy.
	 * 
	 * @return String
	 */
	public String getVoidBy() {
		return voidBy;
	}

	/**
	 * Sets the activity.
	 * 
	 * @param activity
	 *            The activity to set
	 */
	public void setActivity(String activity) {
		this.activity = activity;
	}

	/**
	 * Sets the activityId.
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * Sets the amount.
	 * 
	 * @param amount
	 *            The amount to set
	 */
	public void setAmount(String amount) {
		this.amount = amount;
	}

	/**
	 * Sets the authorizedBy.
	 * 
	 * @param authorizedBy
	 *            The authorizedBy to set
	 */
	public void setAuthorizedBy(String authorizedBy) {
		this.authorizedBy = authorizedBy;
	}

	/**
	 * Sets the checkNbr.
	 * 
	 * @param checkNbr
	 *            The checkNbr to set
	 */
	public void setCheckNbr(String checkNbr) {
		this.checkNbr = checkNbr;
	}

	/**
	 * Sets the comments.
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Sets the depositid.
	 * 
	 * @param depositid
	 *            The depositid to set
	 */
	public void setDepositId(String depositId) {
		this.depositId = depositId;
	}

	/**
	 * Sets the enteredBy.
	 * 
	 * @param enteredBy
	 *            The enteredBy to set
	 */
	public void setEnteredBy(String enteredBy) {
		this.enteredBy = enteredBy;
	}

	/**
	 * Sets the hide.
	 * 
	 * @param hide
	 *            The hide to set
	 */
	public void setHide(String hide) {
		this.hide = hide;
	}

	/**
	 * Sets the method.
	 * 
	 * @param method
	 *            The method to set
	 */
	public void setMethod(String method) {
		this.method = method;
	}

	/**
	 * Sets the postDate.
	 * 
	 * @param postDate
	 *            The postDate to set
	 */
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	/**
	 * Sets the transactionId.
	 * 
	 * @param transactionId
	 *            The transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Sets the voidBy.
	 * 
	 * @param voidBy
	 *            The voidBy to set
	 */
	public void setVoidBy(String voidBy) {
		this.voidBy = voidBy;
	}

	/**
	 * @return
	 */
	public String getOtherPayee() {
		return otherPayee;
	}

	/**
	 * @param string
	 */
	public void setOtherPayee(String string) {
		otherPayee = string;
	}

	public String getVoidPayment() {
		return voidPayment;
	}

	public void setVoidPayment(String voidPayment) {
		this.voidPayment = voidPayment;
	}

	public String getVoidDate() {
		return voidDate;
	}

	public void setVoidDate(String voidDate) {
		this.voidDate = voidDate;
	}

}

package elms.app.finance;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * @author Shekhar Jain
 */
public class Fee {

	protected int feeId;
	protected String actType;
	protected String feeType;
	public Calendar creationDate;
	public Calendar expirationDate;
	protected int sequence;
	protected String inputFlag;
	protected String flagOne;
	protected String feeCalcOne;
	protected String flagTwo;
	protected String feeCalcTwo;
	protected String flagThree;
	protected String feeCalcThree;
	protected String flagFour;
	protected String feeCalcFour;
	protected String description;
	protected int initial;
	protected int subtotalLevel;
	public BigDecimal factor;
	public BigDecimal feeFactor;
	protected int account;
	protected String miscellaneous;
	protected String type;
	protected int taxFlag;
	protected boolean renewable = false;

	
	protected String accountCode;
	protected String feeClass;
	protected String activitySubtypeId;
	protected String onlineInput;
	
	static Logger logger = Logger.getLogger(LookupFee.class.getName());

	/**
	 * @roseuid 3CF6C1C001F4
	 */
	public Fee() {

	}

	/**
	 * @return Returns the renewable.
	 */
	public boolean isRenewable() {
		return renewable;
	}

	/**
	 * @param renewable
	 *            The renewable to set.
	 */
	public void setRenewable(boolean renewable) {
		this.renewable = renewable;
	}

	/**
	 * Gets the feeId
	 * 
	 * @return Returns a int
	 */
	public int getFeeId() {
		return feeId;
	}

	/**
	 * Sets the feeId
	 * 
	 * @param feeId
	 *            The feeId to set
	 */
	public void setFeeId(int feeId) {
		this.feeId = feeId;
	}

	/**
	 * Gets the permitType
	 * 
	 * @return Returns a String
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * Sets the permitType
	 * 
	 * @param permitType
	 *            The permitType to set
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}

	/**
	 * Gets the feeType
	 * 
	 * @return Returns a String
	 */
	public String getFeeType() {
		return feeType;
	}

	/**
	 * Sets the feeType
	 * 
	 * @param feeType
	 *            The feeType to set
	 */
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		if (creationDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(creationDate);
			this.creationDate = cal;
		}
	}

	/**
	 * Gets the expirationDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(Calendar expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		try {
			if (expirationDate == null) {
				this.expirationDate = null;
			} else {
				this.expirationDate.setTime(expirationDate);
			}
		} catch (Exception e) {
			logger.info("Error Date : " + expirationDate.toString());
		}
	}

	/**
	 * Gets the sequence
	 * 
	 * @return Returns a int
	 */
	public int getSequence() {
		return sequence;
	}

	/**
	 * Sets the sequence
	 * 
	 * @param sequence
	 *            The sequence to set
	 */
	public void setSequence(int sequence) {
		this.sequence = sequence;
	}

	/**
	 * Gets the inputFlag
	 * 
	 * @return Returns a String
	 */
	public String getInputFlag() {
		return inputFlag;
	}

	/**
	 * Sets the inputFlag
	 * 
	 * @param inputFlag
	 *            The inputFlag to set
	 */
	public void setInputFlag(String inputFlag) {
		this.inputFlag = inputFlag;
	}

	/**
	 * Gets the flagOne
	 * 
	 * @return Returns a String
	 */
	public String getFlagOne() {
		return flagOne;
	}

	/**
	 * Sets the flagOne
	 * 
	 * @param flagOne
	 *            The flagOne to set
	 */
	public void setFlagOne(String flagOne) {
		this.flagOne = flagOne;
	}

	/**
	 * Gets the feeCalcOne
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcOne() {
		return feeCalcOne;
	}

	/**
	 * Sets the feeCalcOne
	 * 
	 * @param feeCalcOne
	 *            The feeCalcOne to set
	 */
	public void setFeeCalcOne(String feeCalcOne) {
		this.feeCalcOne = feeCalcOne;
	}

	/**
	 * Gets the flagTwo
	 * 
	 * @return Returns a String
	 */
	public String getFlagTwo() {
		return flagTwo;
	}

	/**
	 * Sets the flagTwo
	 * 
	 * @param flagTwo
	 *            The flagTwo to set
	 */
	public void setFlagTwo(String flagTwo) {
		this.flagTwo = flagTwo;
	}

	/**
	 * Gets the feeCalcTwo
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcTwo() {
		return feeCalcTwo;
	}

	/**
	 * Sets the feeCalcTwo
	 * 
	 * @param feeCalcTwo
	 *            The feeCalcTwo to set
	 */
	public void setFeeCalcTwo(String feeCalcTwo) {
		this.feeCalcTwo = feeCalcTwo;
	}

	/**
	 * Gets the flagThree
	 * 
	 * @return Returns a String
	 */
	public String getFlagThree() {
		return flagThree;
	}

	/**
	 * Sets the flagThree
	 * 
	 * @param flagThree
	 *            The flagThree to set
	 */
	public void setFlagThree(String flagThree) {
		this.flagThree = flagThree;
	}

	/**
	 * Gets the feeCalcThree
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcThree() {
		return feeCalcThree;
	}

	/**
	 * Sets the feeCalcThree
	 * 
	 * @param feeCalcThree
	 *            The feeCalcThree to set
	 */
	public void setFeeCalcThree(String feeCalcThree) {
		this.feeCalcThree = feeCalcThree;
	}

	/**
	 * Gets the flagFour
	 * 
	 * @return Returns a String
	 */
	public String getFlagFour() {
		return flagFour;
	}

	/**
	 * Sets the flagFour
	 * 
	 * @param flagFour
	 *            The flagFour to set
	 */
	public void setFlagFour(String flagFour) {
		this.flagFour = flagFour;
	}

	/**
	 * Gets the feeCalcFour
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcFour() {
		return feeCalcFour;
	}

	/**
	 * Sets the feeCalcFour
	 * 
	 * @param feeCalcFour
	 *            The feeCalcFour to set
	 */
	public void setFeeCalcFour(String feeCalcFour) {
		this.feeCalcFour = feeCalcFour;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the initial
	 * 
	 * @return Returns a int
	 */
	public int getInitial() {
		return initial;
	}

	/**
	 * Sets the initial
	 * 
	 * @param initial
	 *            The initial to set
	 */
	public void setInitial(int initial) {
		this.initial = initial;
	}

	/**
	 * Gets the subtotalLevel
	 * 
	 * @return Returns a int
	 */
	public int getSubtotalLevel() {
		return subtotalLevel;
	}

	/**
	 * Sets the subtotalLevel
	 * 
	 * @param subtotalLevel
	 *            The subtotalLevel to set
	 */
	public void setSubtotalLevel(int subtotalLevel) {
		this.subtotalLevel = subtotalLevel;
	}

	/**
	 * Gets the factor
	 * 
	 * @return Returns a float
	 */
	public BigDecimal getFactor() {
		return factor;
	}

	/**
	 * Sets the factor
	 * 
	 * @param factor
	 *            The factor to set
	 */
	public void setFactor(BigDecimal factor) {
		this.factor = factor;
	}

	/**
	 * Gets the feeFactor
	 * 
	 * @return Returns a BigDecimal
	 */
	public BigDecimal getFeeFactor() {
		return feeFactor;
	}

	/**
	 * Sets the feeFactor
	 * 
	 * @param feeFactor
	 *            The feeFactor to set
	 */
	public void setFeeFactor(BigDecimal feeFactor) {
		this.feeFactor = feeFactor;
	}

	/**
	 * Gets the account
	 * 
	 * @return Returns a int
	 */
	public int getAccount() {
		return account;
	}

	/**
	 * Sets the account
	 * 
	 * @param account
	 *            The account to set
	 */
	public void setAccount(int account) {
		this.account = account;
	}

	/**
	 * Gets the miscellaneous
	 * 
	 * @return Returns a String
	 */
	public String getMiscellaneous() {
		return miscellaneous;
	}

	/**
	 * Sets the miscellaneous
	 * 
	 * @param miscellaneous
	 *            The miscellaneous to set
	 */
	public void setMiscellaneous(String miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Returns the taxFlag.
	 * 
	 * @return int
	 */
	public int getTaxFlag() {
		return taxFlag;
	}

	/**
	 * Sets the taxFlag.
	 * 
	 * @param taxFlag
	 *            The taxFlag to set
	 */
	public void setTaxFlag(int taxFlag) {
		this.taxFlag = taxFlag;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getFeeClass() {
		return feeClass;
	}

	public void setFeeClass(String feeClass) {
		this.feeClass = feeClass;
	}

	public String getActivitySubtypeId() {
		return activitySubtypeId;
	}

	public void setActivitySubtypeId(String activitySubtypeId) {
		this.activitySubtypeId = activitySubtypeId;
	}

	public String getOnlineInput() {
		return onlineInput;
	}

	public void setOnlineInput(String onlineInput) {
		this.onlineInput = onlineInput;
	}

	
	
}
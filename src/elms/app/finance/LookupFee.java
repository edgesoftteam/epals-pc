package elms.app.finance;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import elms.util.StringUtils;

public class LookupFee {
	/**
	 * Constructor for FeeReference
	 */

	static Logger logger = Logger.getLogger(LookupFee.class.getName());

	protected int lookupFeeId;
	protected String lookupFee;
	protected BigDecimal lowRange = new BigDecimal(0.0);
	protected BigDecimal highRange = new BigDecimal(0.0);
	protected Calendar creationDate = Calendar.getInstance();
	protected String strCreationDate = "";
	protected Calendar expirationDate = Calendar.getInstance();
	protected String strExpirationDate = "";
	protected BigDecimal result = new BigDecimal(0.0);
	protected BigDecimal plus = new BigDecimal(0.0);
	protected BigDecimal over = new BigDecimal(0.0);
	protected String updateFlag = "Y";

	public LookupFee() {
	}

	public LookupFee(int lookupFeeId, String lookupFee) {
		this.lookupFeeId = lookupFeeId;
		this.lookupFee = lookupFee;
	}

	/**
	 * Gets the lookupFeeId
	 * 
	 * @return Returns a int
	 */
	public int getLookupFeeId() {
		return lookupFeeId;
	}

	/**
	 * Sets the lookupFeeId
	 * 
	 * @param lookupFeeId
	 *            The lookupFeeId to set
	 */
	public void setLookupFeeId(int lookupFeeId) {
		this.lookupFeeId = lookupFeeId;
	}

	/**
	 * Gets the lookupFee
	 * 
	 * @return Returns a String
	 */
	public String getLookupFee() {
		return lookupFee;
	}

	/**
	 * Sets the lookupFee
	 * 
	 * @param lookupFee
	 *            The lookupFee to set
	 */
	public void setLookupFee(String lookupFee) {
		this.lookupFee = lookupFee;
	}

	/**
	 * Gets the lowRange
	 * 
	 * @return Returns a BigDecimal
	 */
	public BigDecimal getLowRange() {
		return lowRange;
	}

	/**
	 * Sets the lowRange
	 * 
	 * @param lowRange
	 *            The lowRange to set
	 */
	public void setLowRange(BigDecimal lowRange) {
		this.lowRange = lowRange;
	}

	/**
	 * Gets the highRange
	 * 
	 * @return Returns a BigDecimal
	 */
	public BigDecimal getHighRange() {
		return highRange;
	}

	/**
	 * Sets the highRange
	 * 
	 * @param highRange
	 *            The highRange to set
	 */
	public void setHighRange(BigDecimal highRange) {
		this.highRange = highRange;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
		this.strCreationDate = StringUtils.cal2str(creationDate);
	}

	public void setCreationDate(Date creationDate) {
		try {
			if (creationDate == null) {
				this.creationDate = null;
				this.strCreationDate = "";
			} else {
				this.creationDate.setTime(creationDate);
				this.strCreationDate = StringUtils.cal2str(this.creationDate);
			}
		} catch (Exception e) {
			logger.info("Error Date : " + creationDate.toString());
		}
	}

	/**
	 * Gets the expirationDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(Calendar expirationDate) {
		this.expirationDate = expirationDate;

	}

	public void setExpirationDate(int i) {
		this.expirationDate = null;

	}

	public void setExpirationDate(Date expirationDate) {
		try {
			if (expirationDate == null) {
				this.expirationDate = null;
			} else {
				this.expirationDate.setTime(expirationDate);
				this.strExpirationDate = StringUtils.cal2str(this.expirationDate);
			}
		} catch (Exception e) {
			logger.info("Error Date : " + expirationDate.toString());
		}
	}

	/**
	 * Gets the result
	 * 
	 * @return Returns a BigDecimal
	 */
	public BigDecimal getResult() {
		return result;
	}

	/**
	 * Sets the result
	 * 
	 * @param result
	 *            The result to set
	 */
	public void setResult(BigDecimal result) {
		this.result = result;
	}

	/**
	 * Gets the plus
	 * 
	 * @return Returns a BigDecimal
	 */
	public BigDecimal getPlus() {
		return plus;
	}

	/**
	 * Sets the plus
	 * 
	 * @param plus
	 *            The plus to set
	 */
	public void setPlus(BigDecimal plus) {
		this.plus = plus;
	}

	/**
	 * Gets the over
	 * 
	 * @return Returns a BigDecimal
	 */
	public BigDecimal getOver() {
		return over;
	}

	/**
	 * Sets the over
	 * 
	 * @param over
	 *            The over to set
	 */
	public void setOver(BigDecimal over) {
		this.over = over;
	}

	/**
	 * Gets the updateFlag
	 * 
	 * @return Returns a String
	 */
	public String getUpdateFlag() {
		return updateFlag;
	}

	/**
	 * Sets the updateFlag
	 * 
	 * @param updateFlag
	 *            The updateFlag to set
	 */
	public void setUpdateFlag(String updateFlag) {
		this.updateFlag = updateFlag;
	}

	public String toString() {
		String sb = "LookupFee [ " + lookupFeeId + " : " + lookupFee + " : " + lowRange + " : " + highRange + " : " + creationDate.getTime();

		if (expirationDate == null) {
			sb += " : null";
		} else {
			sb += " : " + expirationDate.getTime();
		}

		sb += " : " + result + " : " + plus + " : " + over + " : " + updateFlag + " ]";
		return sb;
	}

	/**
	 * Gets the strExpirationDate
	 * 
	 * @return Returns a String
	 */
	public String getStrExpirationDate() {
		return strExpirationDate;
	}

	/**
	 * Gets the strCreationDate
	 * 
	 * @return Returns a String
	 */
	public String getStrCreationDate() {
		return strCreationDate;
	}

}

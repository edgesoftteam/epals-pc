/*
 * Created on Aug 12, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.sitedata;

/**
 * @author Shekhar
 * 
 *         To change the template for this generated type comment go to Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SiteSetback {

	protected boolean changed;

	protected int setbackId;
	protected int lsoId;

	protected String frontFt;
	protected String frontIn;
	protected String frontComment;

	protected String rearFt;
	protected String rearIn;
	protected String rearComment;

	protected String side1Ft;
	protected String side1In;
	protected String side1Dir;
	protected String side1Comment;

	protected String side2Ft;
	protected String side2In;
	protected String side2Dir;
	protected String side2Comment;

	protected String comment;
	protected String updated;

	public SiteSetback() {
		this.changed = false;
	}

	/**
	 * @return
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @return
	 */
	public String getFrontComment() {
		return frontComment;
	}

	/**
	 * @return
	 */
	public String getFrontFt() {
		return frontFt;
	}

	/**
	 * @return
	 */
	public String getFrontIn() {
		return frontIn;
	}

	/**
	 * @return
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * @return
	 */
	public String getRearComment() {
		return rearComment;
	}

	/**
	 * @return
	 */
	public String getRearFt() {
		return rearFt;
	}

	/**
	 * @return
	 */
	public String getRearIn() {
		return rearIn;
	}

	/**
	 * @return
	 */
	public int getSetbackId() {
		return setbackId;
	}

	/**
	 * @return
	 */
	public String getSide1Comment() {
		return side1Comment;
	}

	/**
	 * @return
	 */
	public String getSide1Dir() {
		return side1Dir;
	}

	/**
	 * @return
	 */
	public String getSide1Ft() {
		return side1Ft;
	}

	/**
	 * @return
	 */
	public String getSide1In() {
		return side1In;
	}

	/**
	 * @return
	 */
	public String getSide2Comment() {
		return side2Comment;
	}

	/**
	 * @return
	 */
	public String getSide2Dir() {
		return side2Dir;
	}

	/**
	 * @return
	 */
	public String getSide2Ft() {
		return side2Ft;
	}

	/**
	 * @return
	 */
	public String getSide2In() {
		return side2In;
	}

	/**
	 * @return
	 */
	public String getUpdated() {
		return updated;
	}

	/**
	 * @param string
	 */
	public void setComment(String string) {
		comment = string;
	}

	/**
	 * @param string
	 */
	public void setFrontComment(String string) {
		frontComment = string;
	}

	/**
	 * @param string
	 */
	public void setFrontFt(String string) {
		frontFt = string;
	}

	/**
	 * @param string
	 */
	public void setFrontIn(String string) {
		frontIn = string;
	}

	/**
	 * @param i
	 */
	public void setLsoId(int i) {
		lsoId = i;
	}

	/**
	 * @param string
	 */
	public void setRearComment(String string) {
		rearComment = string;
	}

	/**
	 * @param string
	 */
	public void setRearFt(String string) {
		rearFt = string;
	}

	/**
	 * @param string
	 */
	public void setRearIn(String string) {
		rearIn = string;
	}

	/**
	 * @param i
	 */
	public void setSetbackId(int i) {
		setbackId = i;
	}

	/**
	 * @param string
	 */
	public void setSide1Comment(String string) {
		side1Comment = string;
	}

	/**
	 * @param string
	 */
	public void setSide1Dir(String string) {
		side1Dir = string;
	}

	/**
	 * @param string
	 */
	public void setSide1Ft(String string) {
		side1Ft = string;
	}

	/**
	 * @param string
	 */
	public void setSide1In(String string) {
		side1In = string;
	}

	/**
	 * @param string
	 */
	public void setSide2Comment(String string) {
		side2Comment = string;
	}

	/**
	 * @param string
	 */
	public void setSide2Dir(String string) {
		side2Dir = string;
	}

	/**
	 * @param string
	 */
	public void setSide2Ft(String string) {
		side2Ft = string;
	}

	/**
	 * @param string
	 */
	public void setSide2In(String string) {
		side2In = string;
	}

	/**
	 * @param string
	 */
	public void setUpdated(String string) {
		updated = string;
	}

	/**
	 * @return
	 */
	public boolean isChanged() {
		return changed;
	}

	/**
	 * @param b
	 */
	public void setChanged(boolean b) {
		changed = b;
	}

}

package elms.app.sitedata;

import java.util.Calendar;
import java.util.Date;

import elms.util.StringUtils;

public class SiteStructureData {

	protected int version;
	protected long structureId;
	protected long lsoId;
	protected String buildingName;
	protected String buildingType;
	protected String buildingUse;
	protected String occupancyGroup;
	protected String constructionType;
	protected String stories;
	protected String actualStories;
	protected String basementLevels;
	protected String actualBasementLevels;
	protected String heightFeet;
	protected String addHeightFeet;
	protected String heightInches;
	protected String sprinklered;
	protected String lotArea;
	protected String padArea;
	protected String garageArea;
	protected String actualGarageArea;
	protected String slopeArea;
	protected String buildingArea;
	protected String basementArea;
	protected String actualBasementArea;
	protected String newBuildingArea;
	protected String actualNewBuildingArea;
	protected String dwellingUnits;
	protected String actualDwellingUnits;
	protected String bedrooms;
	protected String actualBedrooms;
	protected String roofing;
	protected String zoning;
	protected String location;
	protected String allowedFar;
	protected String actualFar;
	protected String floorAreaAdded;
	protected String requiredParking;
	protected String providedParking;
	protected String comment;
	protected String active;
	protected String updatedBy;
	protected Date updated;

	protected String moduleNumber;
	protected String kwSize;
	protected String confNumber;
	protected String meterSpotDate;
	protected String batteryIncluded;

	public SiteStructureData() {
	}

	public SiteStructureData(long aLsoId) {
		this();
		this.lsoId = aLsoId;
		this.structureId = 0;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getBuildingType() {
		return buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}

	public String getBuildingUse() {
		return buildingUse;
	}

	public void setBuildingUse(String buildingUse) {
		this.buildingUse = buildingUse;
	}

	public String getOccupancyGroup() {
		return occupancyGroup;
	}

	public void setOccupancyGroup(String occupancyGroup) {
		this.occupancyGroup = occupancyGroup;
	}

	public String getConstructionType() {
		return constructionType;
	}

	public void setConstructionType(String constructionType) {
		this.constructionType = constructionType;
	}

	public String getStories() {
		return stories;
	}

	public void setStories(String stories) {
		this.stories = stories;
	}

	public String getBasementLevels() {
		return basementLevels;
	}

	public void setBasementLevels(String basementLevels) {
		this.basementLevels = basementLevels;
	}

	public String getHeightFeet() {
		return heightFeet;
	}

	public void setHeightFeet(String heightFeet) {
		this.heightFeet = heightFeet;
	}

	public String getHeightInches() {
		return heightInches;
	}

	public void setHeightInches(String heightInches) {
		this.heightInches = heightInches;
	}

	public String getSprinklered() {
		return sprinklered;
	}

	public void setSprinklered(String sprinklered) {
		this.sprinklered = sprinklered;
	}

	public String getLotArea() {
		return lotArea;
	}

	public void setLotArea(String lotArea) {
		this.lotArea = lotArea;
	}

	public String getPadArea() {
		return padArea;
	}

	public void setPadArea(String padArea) {
		this.padArea = padArea;
	}

	public String getGarageArea() {
		return garageArea;
	}

	public void setGarageArea(String garageArea) {
		this.garageArea = garageArea;
	}

	public String getSlopeArea() {
		return slopeArea;
	}

	public void setSlopeArea(String slopeArea) {
		this.slopeArea = slopeArea;
	}

	public String getBuildingArea() {
		return buildingArea;
	}

	public void setBuildingArea(String buildingArea) {
		this.buildingArea = buildingArea;
	}

	public String getBasementArea() {
		return basementArea;
	}

	public void setBasementArea(String basementArea) {
		this.basementArea = basementArea;
	}

	public String getNewBuildingArea() {
		return newBuildingArea;
	}

	public void setNewBuildingArea(String newBuildingArea) {
		this.newBuildingArea = newBuildingArea;
	}

	public String getDwellingUnits() {
		return dwellingUnits;
	}

	public void setDwellingUnits(String dwellingUnits) {
		this.dwellingUnits = dwellingUnits;
	}

	public String getBedrooms() {
		return bedrooms;
	}

	public void setBedrooms(String bedrooms) {
		this.bedrooms = bedrooms;
	}

	public String getRoofing() {
		return roofing;
	}

	public void setRoofing(String roofing) {
		this.roofing = roofing;
	}

	public String getZoning() {
		return zoning;
	}

	public void setZoning(String zoning) {
		this.zoning = zoning;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAllowedFar() {
		return allowedFar;
	}

	public void setAllowedFar(String allowedFar) {
		this.allowedFar = allowedFar;
	}

	public String getActualFar() {
		return actualFar;
	}

	public void setActualFar(String actualFar) {
		this.actualFar = actualFar;
	}

	public String getFloorAreaAdded() {
		return floorAreaAdded;
	}

	public void setFloorAreaAdded(String floorAreaAdded) {
		this.floorAreaAdded = floorAreaAdded;
	}

	public String getRequiredParking() {
		return requiredParking;
	}

	public void setRequiredParking(String requiredParking) {
		this.requiredParking = requiredParking;
	}

	public String getProvidedParking() {
		return providedParking;
	}

	public void setProvidedParking(String providedParking) {
		this.providedParking = providedParking;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return
	 */
	public long getLsoId() {
		return lsoId;
	}

	/**
	 * @return
	 */
	public long getStructureId() {
		return structureId;
	}

	/**
	 * @param l
	 */
	public void setLsoId(long l) {
		lsoId = l;
	}

	/**
	 * @param l
	 */
	public void setStructureId(long l) {
		structureId = l;
	}

	public String toString() {
		return "SiteStructureData(" + this.lsoId + "," + this.structureId + ")";
	}

	/**
	 * @return
	 */
	public String getAddHeightFeet() {
		return addHeightFeet;
	}

	/**
	 * @param string
	 */
	public void setAddHeightFeet(String string) {
		addHeightFeet = string;
	}

	/**
	 * @return
	 */
	public String getActualGarageArea() {
		return actualGarageArea;
	}

	/**
	 * @param string
	 */
	public void setActualGarageArea(String string) {
		actualGarageArea = string;
	}

	/**
	 * @return
	 */
	public String getActualBasementArea() {
		return actualBasementArea;
	}

	/**
	 * @return
	 */
	public String getActualBasementLevels() {
		return actualBasementLevels;
	}

	/**
	 * @return
	 */
	public String getActualBedrooms() {
		return actualBedrooms;
	}

	/**
	 * @return
	 */
	public String getActualDwellingUnits() {
		return actualDwellingUnits;
	}

	/**
	 * @return
	 */
	public String getActualNewBuildingArea() {
		return actualNewBuildingArea;
	}

	/**
	 * @return
	 */
	public String getActualStories() {
		return actualStories;
	}

	/**
	 * @param string
	 */
	public void setActualBasementArea(String string) {
		actualBasementArea = string;
	}

	/**
	 * @param string
	 */
	public void setActualBasementLevels(String string) {
		actualBasementLevels = string;
	}

	/**
	 * @param string
	 */
	public void setActualBedrooms(String string) {
		actualBedrooms = string;
	}

	/**
	 * @param string
	 */
	public void setActualDwellingUnits(String string) {
		actualDwellingUnits = string;
	}

	/**
	 * @param string
	 */
	public void setActualNewBuildingArea(String string) {
		actualNewBuildingArea = string;
	}

	/**
	 * @param string
	 */
	public void setActualStories(String string) {
		actualStories = string;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public String getModuleNumber() {
		return moduleNumber;
	}

	public void setModuleNumber(String moduleNumber) {
		this.moduleNumber = moduleNumber;
	}

	public String getKwSize() {
		return kwSize;
	}

	public void setKwSize(String kwSize) {
		this.kwSize = kwSize;
	}

	public String getConfNumber() {
		return confNumber;
	}

	public void setConfNumber(String confNumber) {
		this.confNumber = confNumber;
	}

	public String getMeterSpotDate() {
		return meterSpotDate;
	}

	public void setMeterSpotDate(String meterSpotDate) {
		this.meterSpotDate = meterSpotDate;
	}

	public void setMeterSpotDate(Date meterSpotDate) {
		if (meterSpotDate == null)
			this.meterSpotDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(meterSpotDate);
			this.meterSpotDate = StringUtils.cal2str(calendar);
		}
	}

	public String getBatteryIncluded() {
		return batteryIncluded;
	}

	public void setBatteryIncluded(String batteryIncluded) {
		this.batteryIncluded = batteryIncluded;
	}
}
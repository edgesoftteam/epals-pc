package elms.app.lso;

import java.util.List;

/**
 * @author Shekhar Jain
 */
public class Occupancy extends Lso {
	protected int structureId;
	protected OccupancyDetails occupancyDetails;
	protected List occupancyAddress;
	protected List occupancyApn;
	protected OccupancySiteData occupancySiteData;

	public Occupancy(int occupancyId, int structureId, OccupancyDetails occupancyDetails, List occupancyAddress, List occupancyApn, List occupancyHold, List occupancyCondition, List occupancyAttachment, OccupancySiteData occupancySiteData, List occupancyComment) {
		super(occupancyId, occupancyHold, occupancyCondition, occupancyAttachment, occupancyComment);
		this.occupancyDetails = occupancyDetails;
		this.occupancyAddress = occupancyAddress;
		this.occupancyApn = occupancyApn;
		this.occupancySiteData = occupancySiteData;
		this.structureId = structureId;
	}

	// temp constructure for development

	public Occupancy(int occupancyId, OccupancyDetails occupancyDetails, List occupancyAddress, List occupancyApn) {
		super(occupancyId);
		this.occupancyDetails = occupancyDetails;
		this.occupancyAddress = occupancyAddress;
		this.occupancyApn = occupancyApn;

	}

	public Occupancy() {
		super();
	}

	public OccupancyDetails getOccupancyDetails() {
		return occupancyDetails;
	}

	/**
	 * Sets the occupancyDetails
	 * 
	 * @param occupancyDetails
	 *            The occupancyDetails to set
	 */
	public void setOccupancyDetails(OccupancyDetails occupancyDetails) {
		this.occupancyDetails = occupancyDetails;
	}

	/**
	 * Gets the occupancyAddress
	 * 
	 * @return Returns a List
	 */
	public List getOccupancyAddress() {
		return occupancyAddress;
	}

	/**
	 * Sets the occupancyAddress
	 * 
	 * @param occupancyAddress
	 *            The occupancyAddress to set
	 */
	public void setOccupancyAddress(List occupancyAddress) {
		this.occupancyAddress = occupancyAddress;
	}

	/**
	 * Gets the occupancyApn
	 * 
	 * @return Returns a List
	 */
	public List getOccupancyApn() {
		return occupancyApn;
	}

	/**
	 * Sets the occupancyApn
	 * 
	 * @param occupancyApn
	 *            The occupancyApn to set
	 */
	public void setOccupancyApn(List occupancyApn) {
		this.occupancyApn = occupancyApn;
	}

	/**
	 * Gets the occupancySiteData
	 * 
	 * @return Returns a OccupancySiteData
	 */
	public OccupancySiteData getOccupancySiteData() {
		return occupancySiteData;
	}

	/**
	 * Sets the occupancySiteData
	 * 
	 * @param occupancySiteData
	 *            The occupancySiteData to set
	 */
	public void setOccupancySiteData(OccupancySiteData occupancySiteData) {
		this.occupancySiteData = occupancySiteData;
	}

	/**
	 * Gets the structureId
	 * 
	 * @return Returns a int
	 */
	public int getStructureId() {
		return structureId;
	}

	/**
	 * Sets the structureId
	 * 
	 * @param structureId
	 *            The structureId to set
	 */
	public void setStructureId(int structureId) {
		this.structureId = structureId;
	}

}
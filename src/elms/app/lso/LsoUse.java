//Source file: C:\\datafile\\source\\elms\\app\\lso\\Lso.java

package elms.app.lso;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

public class LsoUse {

	protected List lsoUseId = new ArrayList();
	protected List lsoUseType = new ArrayList();
	protected List lsoUseDescription = new ArrayList();

	/**
	 * 
	 */
	public LsoUse() {

		// TODO Auto-generated constructor stub
	}

	public LsoUse(List lsoUseId, List lsoUseType, List lsoUseDescription) {
		this.lsoUseId = lsoUseId;
		this.lsoUseType = lsoUseType;
		this.lsoUseDescription = lsoUseDescription;
	}

	/**
	 * @return
	 */
	public List getLsoUseDescription() {
		return lsoUseDescription;
	}

	/**
	 * @return
	 */
	public List getLsoUseId() {
		return lsoUseId;
	}

	/**
	 * @return
	 */
	public List getLsoUseType() {
		return lsoUseType;
	}

	/**
	 * @param list
	 */
	public void setLsoUseDescription(List list) {
		lsoUseDescription = list;
	}

	/**
	 * @param list
	 */
	public void setLsoUseId(List list) {
		lsoUseId = list;
	}

	/**
	 * @param list
	 */
	public void setLsoUseType(List list) {
		lsoUseType = list;
	}

}
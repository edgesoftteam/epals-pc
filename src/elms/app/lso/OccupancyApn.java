package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class OccupancyApn extends Apn {
	public OccupancyApn(String apn, String ownerName, int ownerId, int streetNbr, String streetMod, String streetName, String unit, String city, String state, String zip, String zip4, String foreignAddress, String line1, String line2, String line3, String line4, String country, String email, String phone, String fax) {
		super(apn, ownerName, ownerId, streetNbr, streetMod, streetName, unit, city, state, zip, zip4, foreignAddress, line1, line2, line3, line4, country, email, phone, fax);
	}

	public OccupancyApn(String parcelNumber, Owner owner, String active) {
		super(parcelNumber, owner, active);
	}

	public OccupancyApn() {

	}
}

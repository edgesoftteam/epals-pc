package elms.app.lso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import elms.app.admin.ParkingZone;

/**
 * @author Shekhar Jain
 */
public class LandDetails extends Details {
	protected List zone;
	protected String xCordinate;
	protected String yCordinate;
	protected List addressRangeList;
	protected ParkingZone parkingZone;
	protected String rZone;
	protected String highFireArea;
	
	/**
	 * @param zone
	 * @param xCordinate
	 * @param yCordinate
	 *            3CCDBC1400BF
	 */
	public LandDetails(String alias, String description, List use, String active, List zone, String xCordinate, String yCordinate, String label) {
		super(alias, description, use, active, label);
		this.zone = zone;
		this.xCordinate = xCordinate;
		this.yCordinate = yCordinate;

	}

	/**
	 * 3CCDBC0C01EA
	 */
	public LandDetails() {
		zone = new ArrayList();
		Zone defaultZone = new Zone();
		zone.add(defaultZone);

	}

	/**
	 * Access method for the zone property.
	 * 
	 * @return the current value of the zone property
	 */
	public List getZone() {
		return zone;
	}

	public String getZoneAsString() {
		List zoneList = (List) this.getZone();
		int i = zoneList.size();
		int j = 0;
		String zoneString = "";
		Iterator iter = zoneList.iterator();
		while (iter.hasNext()) {
			j++;
			Zone zoneObj = (Zone) iter.next();
			if (j < i)
				zoneString = zoneString + zoneObj.getDescription().trim() + ", ";
			else
				zoneString = zoneString + zoneObj.getDescription().trim();
		}
		return zoneString;
	}

	public String[] getZoneAsArray() {

		List zoneList = (List) this.getZone();
		String[] zoneArray = new String[zoneList.size()];
		for (int i = 0; i < zoneList.size(); i++) {
			Zone zone = (Zone) zoneList.get(i);
			zoneArray[i] = zone.getDescription().trim();
		}
		return zoneArray;
	}

	/**
	 * Sets the value of the zone property.
	 * 
	 * @param aZone
	 *            the new value of the zone property
	 */
	public void setZone(List aZone) {
		zone = aZone;
	}

	/**
	 * Access method for the xCordinate property.
	 * 
	 * @return the current value of the xCordinate property
	 */
	public String getXCordinate() {
		return xCordinate;
	}

	/**
	 * Sets the value of the xCordinate property.
	 * 
	 * @param aXCordinate
	 *            the new value of the xCordinate property
	 */
	public void setXCordinate(String aXCordinate) {
		xCordinate = aXCordinate;
	}

	/**
	 * Access method for the yCordinate property.
	 * 
	 * @return the current value of the yCordinate property
	 */
	public String getYCordinate() {
		return yCordinate;
	}

	/**
	 * Sets the value of the yCordinate property.
	 * 
	 * @param aYCordinate
	 *            the new value of the yCordinate property
	 */
	public void setYCordinate(String aYCordinate) {
		yCordinate = aYCordinate;
	}

	/**
	 * Returns the addressRangeList.
	 * 
	 * @return List
	 */
	public List getAddressRangeList() {
		return addressRangeList;
	}

	/**
	 * Sets the addressRangeList.
	 * 
	 * @param addressRangeList
	 *            The addressRangeList to set
	 */
	public void setAddressRangeList(List addressRangeList) {
		this.addressRangeList = addressRangeList;
	}

	/**
	 * Returns the parkingZone.
	 * 
	 * @return ParkingZone
	 */
	public ParkingZone getParkingZone() {
		return parkingZone;
	}

	/**
	 * Returns the rZone.
	 * 
	 * @return String
	 */
	public String getRZone() {
		return rZone;
	}

	/**
	 * Sets the parkingZone.
	 * 
	 * @param parkingZone
	 *            The parkingZone to set
	 */
	public void setParkingZone(ParkingZone parkingZone) {
		this.parkingZone = parkingZone;
	}

	/**
	 * Sets the rZone.
	 * 
	 * @param rZone
	 *            The rZone to set
	 */
	public void setRZone(String rZone) {
		this.rZone = rZone;
	}

	public String getHighFireArea() {
		return highFireArea;
	}

	public void setHighFireArea(String highFireArea) {
		this.highFireArea = highFireArea;
	}

}
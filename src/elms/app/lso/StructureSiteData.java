//Source file: C:\\datafile\\source\\elms\\app\\lso\\StructureSiteData.java

package elms.app.lso;

import java.util.Date;

import elms.app.sitedata.SiteData;

public class StructureSiteData extends SiteData {
	public Integer structureId;
	public String STRUCTURE_NO;
	public String OCCUPANT_NAME;
	public String PRIMARY_USE;
	public String OCCUPANCY_GRP;
	public float OCCUPANCY_AREA;
	public float REQ_PARKING;
	public float STD_PARKING;
	public float ON_SITE_PARKING;
	public float IN_LIEU_PARKING;
	public float ENCUMBERED_PARKING;
	public float HANDICAP_PARKING;
	public float COMPACT_PARKING;
	public float TANDEM_PARKING;
	public float VALET_PARKING;
	public float TOT_PARKING;
	public float ACTUAL_BLDG_HEIGHT;
	public float ALLOW_BLDG_HEIGHT;
	public float NO_OF_FLOORS;
	public String MEZZANINE;
	public float STORIES_ABOVE_GRADE;
	public float LEVELS_BELOW_GRADE;
	public float FLOOR_AREA;
	public float FLOOR_SETBACK;
	public float ALLOW_AREA;
	public float ACTUAL_AREA;
	public float REQ_PARKING_AREA;
	public float RUBBISH_AREA;
	public String RUBBISH;
	public String CONSTRUCTION_TYPE;
	public Date BLDG_CODE_ADD_DATE;
	public String DRIVEWAY_SLOPE;
	public float NO_OF_ELEVATORS;
	public float NO_OF_STAIRWAYS;
	public String ALARM_TYPES;
	public String FIRE_SPRINKLERS;
	public String EMERG_POWER_SOURCE;
	public String SHAFT_STAIR_PRESSURE;
	public String FINE_ART;
	public String FINE_ART_DESC;

	/**
	 * 3CD02F960161
	 */
	public StructureSiteData() {

	}

	/**
	 * Access method for the structureId property.
	 * 
	 * @return the current value of the structureId property
	 */
	public Integer getStructureId() {
		return structureId;
	}

	/**
	 * Sets the value of the structureId property.
	 * 
	 * @param aStructureId
	 *            the new value of the structureId property
	 */
	public void setStructureId(Integer aStructureId) {
		structureId = aStructureId;
	}

	/**
	 * Access method for the STRUCTURE_NO property.
	 * 
	 * @return the current value of the STRUCTURE_NO property
	 */
	public String getSTRUCTURE_NO() {
		return STRUCTURE_NO;
	}

	/**
	 * Sets the value of the STRUCTURE_NO property.
	 * 
	 * @param aSTRUCTURE_NO
	 *            the new value of the STRUCTURE_NO property
	 */
	public void setSTRUCTURE_NO(String aSTRUCTURE_NO) {
		STRUCTURE_NO = aSTRUCTURE_NO;
	}

	/**
	 * Access method for the OCCUPANT_NAME property.
	 * 
	 * @return the current value of the OCCUPANT_NAME property
	 */
	public String getOCCUPANT_NAME() {
		return OCCUPANT_NAME;
	}

	/**
	 * Sets the value of the OCCUPANT_NAME property.
	 * 
	 * @param aOCCUPANT_NAME
	 *            the new value of the OCCUPANT_NAME property
	 */
	public void setOCCUPANT_NAME(String aOCCUPANT_NAME) {
		OCCUPANT_NAME = aOCCUPANT_NAME;
	}

	/**
	 * Access method for the PRIMARY_USE property.
	 * 
	 * @return the current value of the PRIMARY_USE property
	 */
	public String getPRIMARY_USE() {
		return PRIMARY_USE;
	}

	/**
	 * Sets the value of the PRIMARY_USE property.
	 * 
	 * @param aPRIMARY_USE
	 *            the new value of the PRIMARY_USE property
	 */
	public void setPRIMARY_USE(String aPRIMARY_USE) {
		PRIMARY_USE = aPRIMARY_USE;
	}

	/**
	 * Access method for the OCCUPANCY_GRP property.
	 * 
	 * @return the current value of the OCCUPANCY_GRP property
	 */
	public String getOCCUPANCY_GRP() {
		return OCCUPANCY_GRP;
	}

	/**
	 * Sets the value of the OCCUPANCY_GRP property.
	 * 
	 * @param aOCCUPANCY_GRP
	 *            the new value of the OCCUPANCY_GRP property
	 */
	public void setOCCUPANCY_GRP(String aOCCUPANCY_GRP) {
		OCCUPANCY_GRP = aOCCUPANCY_GRP;
	}

	/**
	 * Access method for the OCCUPANCY_AREA property.
	 * 
	 * @return the current value of the OCCUPANCY_AREA property
	 */
	public float getOCCUPANCY_AREA() {
		return OCCUPANCY_AREA;
	}

	/**
	 * Sets the value of the OCCUPANCY_AREA property.
	 * 
	 * @param aOCCUPANCY_AREA
	 *            the new value of the OCCUPANCY_AREA property
	 */
	public void setOCCUPANCY_AREA(float aOCCUPANCY_AREA) {
		OCCUPANCY_AREA = aOCCUPANCY_AREA;
	}

	/**
	 * Access method for the REQ_PARKING property.
	 * 
	 * @return the current value of the REQ_PARKING property
	 */
	public float getREQ_PARKING() {
		return REQ_PARKING;
	}

	/**
	 * Sets the value of the REQ_PARKING property.
	 * 
	 * @param aREQ_PARKING
	 *            the new value of the REQ_PARKING property
	 */
	public void setREQ_PARKING(float aREQ_PARKING) {
		REQ_PARKING = aREQ_PARKING;
	}

	/**
	 * Access method for the STD_PARKING property.
	 * 
	 * @return the current value of the STD_PARKING property
	 */
	public float getSTD_PARKING() {
		return STD_PARKING;
	}

	/**
	 * Sets the value of the STD_PARKING property.
	 * 
	 * @param aSTD_PARKING
	 *            the new value of the STD_PARKING property
	 */
	public void setSTD_PARKING(float aSTD_PARKING) {
		STD_PARKING = aSTD_PARKING;
	}

	/**
	 * Access method for the ON_SITE_PARKING property.
	 * 
	 * @return the current value of the ON_SITE_PARKING property
	 */
	public float getON_SITE_PARKING() {
		return ON_SITE_PARKING;
	}

	/**
	 * Sets the value of the ON_SITE_PARKING property.
	 * 
	 * @param aON_SITE_PARKING
	 *            the new value of the ON_SITE_PARKING property
	 */
	public void setON_SITE_PARKING(float aON_SITE_PARKING) {
		ON_SITE_PARKING = aON_SITE_PARKING;
	}

	/**
	 * Access method for the IN_LIEU_PARKING property.
	 * 
	 * @return the current value of the IN_LIEU_PARKING property
	 */
	public float getIN_LIEU_PARKING() {
		return IN_LIEU_PARKING;
	}

	/**
	 * Sets the value of the IN_LIEU_PARKING property.
	 * 
	 * @param aIN_LIEU_PARKING
	 *            the new value of the IN_LIEU_PARKING property
	 */
	public void setIN_LIEU_PARKING(float aIN_LIEU_PARKING) {
		IN_LIEU_PARKING = aIN_LIEU_PARKING;
	}

	/**
	 * Access method for the ENCUMBERED_PARKING property.
	 * 
	 * @return the current value of the ENCUMBERED_PARKING property
	 */
	public float getENCUMBERED_PARKING() {
		return ENCUMBERED_PARKING;
	}

	/**
	 * Sets the value of the ENCUMBERED_PARKING property.
	 * 
	 * @param aENCUMBERED_PARKING
	 *            the new value of the ENCUMBERED_PARKING property
	 */
	public void setENCUMBERED_PARKING(float aENCUMBERED_PARKING) {
		ENCUMBERED_PARKING = aENCUMBERED_PARKING;
	}

	/**
	 * Access method for the HANDICAP_PARKING property.
	 * 
	 * @return the current value of the HANDICAP_PARKING property
	 */
	public float getHANDICAP_PARKING() {
		return HANDICAP_PARKING;
	}

	/**
	 * Sets the value of the HANDICAP_PARKING property.
	 * 
	 * @param aHANDICAP_PARKING
	 *            the new value of the HANDICAP_PARKING property
	 */
	public void setHANDICAP_PARKING(float aHANDICAP_PARKING) {
		HANDICAP_PARKING = aHANDICAP_PARKING;
	}

	/**
	 * Access method for the COMPACT_PARKING property.
	 * 
	 * @return the current value of the COMPACT_PARKING property
	 */
	public float getCOMPACT_PARKING() {
		return COMPACT_PARKING;
	}

	/**
	 * Sets the value of the COMPACT_PARKING property.
	 * 
	 * @param aCOMPACT_PARKING
	 *            the new value of the COMPACT_PARKING property
	 */
	public void setCOMPACT_PARKING(float aCOMPACT_PARKING) {
		COMPACT_PARKING = aCOMPACT_PARKING;
	}

	/**
	 * Access method for the TANDEM_PARKING property.
	 * 
	 * @return the current value of the TANDEM_PARKING property
	 */
	public float getTANDEM_PARKING() {
		return TANDEM_PARKING;
	}

	/**
	 * Sets the value of the TANDEM_PARKING property.
	 * 
	 * @param aTANDEM_PARKING
	 *            the new value of the TANDEM_PARKING property
	 */
	public void setTANDEM_PARKING(float aTANDEM_PARKING) {
		TANDEM_PARKING = aTANDEM_PARKING;
	}

	/**
	 * Access method for the VALET_PARKING property.
	 * 
	 * @return the current value of the VALET_PARKING property
	 */
	public float getVALET_PARKING() {
		return VALET_PARKING;
	}

	/**
	 * Sets the value of the VALET_PARKING property.
	 * 
	 * @param aVALET_PARKING
	 *            the new value of the VALET_PARKING property
	 */
	public void setVALET_PARKING(float aVALET_PARKING) {
		VALET_PARKING = aVALET_PARKING;
	}

	/**
	 * Access method for the TOT_PARKING property.
	 * 
	 * @return the current value of the TOT_PARKING property
	 */
	public float getTOT_PARKING() {
		return TOT_PARKING;
	}

	/**
	 * Sets the value of the TOT_PARKING property.
	 * 
	 * @param aTOT_PARKING
	 *            the new value of the TOT_PARKING property
	 */
	public void setTOT_PARKING(float aTOT_PARKING) {
		TOT_PARKING = aTOT_PARKING;
	}

	/**
	 * Access method for the ACTUAL_BLDG_HEIGHT property.
	 * 
	 * @return the current value of the ACTUAL_BLDG_HEIGHT property
	 */
	public float getACTUAL_BLDG_HEIGHT() {
		return ACTUAL_BLDG_HEIGHT;
	}

	/**
	 * Sets the value of the ACTUAL_BLDG_HEIGHT property.
	 * 
	 * @param aACTUAL_BLDG_HEIGHT
	 *            the new value of the ACTUAL_BLDG_HEIGHT property
	 */
	public void setACTUAL_BLDG_HEIGHT(float aACTUAL_BLDG_HEIGHT) {
		ACTUAL_BLDG_HEIGHT = aACTUAL_BLDG_HEIGHT;
	}

	/**
	 * Access method for the ALLOW_BLDG_HEIGHT property.
	 * 
	 * @return the current value of the ALLOW_BLDG_HEIGHT property
	 */
	public float getALLOW_BLDG_HEIGHT() {
		return ALLOW_BLDG_HEIGHT;
	}

	/**
	 * Sets the value of the ALLOW_BLDG_HEIGHT property.
	 * 
	 * @param aALLOW_BLDG_HEIGHT
	 *            the new value of the ALLOW_BLDG_HEIGHT property
	 */
	public void setALLOW_BLDG_HEIGHT(float aALLOW_BLDG_HEIGHT) {
		ALLOW_BLDG_HEIGHT = aALLOW_BLDG_HEIGHT;
	}

	/**
	 * Access method for the NO_OF_FLOORS property.
	 * 
	 * @return the current value of the NO_OF_FLOORS property
	 */
	public float getNO_OF_FLOORS() {
		return NO_OF_FLOORS;
	}

	/**
	 * Sets the value of the NO_OF_FLOORS property.
	 * 
	 * @param aNO_OF_FLOORS
	 *            the new value of the NO_OF_FLOORS property
	 */
	public void setNO_OF_FLOORS(float aNO_OF_FLOORS) {
		NO_OF_FLOORS = aNO_OF_FLOORS;
	}

	/**
	 * Access method for the MEZZANINE property.
	 * 
	 * @return the current value of the MEZZANINE property
	 */
	public String getMEZZANINE() {
		return MEZZANINE;
	}

	/**
	 * Sets the value of the MEZZANINE property.
	 * 
	 * @param aMEZZANINE
	 *            the new value of the MEZZANINE property
	 */
	public void setMEZZANINE(String aMEZZANINE) {
		MEZZANINE = aMEZZANINE;
	}

	/**
	 * Access method for the STORIES_ABOVE_GRADE property.
	 * 
	 * @return the current value of the STORIES_ABOVE_GRADE property
	 */
	public float getSTORIES_ABOVE_GRADE() {
		return STORIES_ABOVE_GRADE;
	}

	/**
	 * Sets the value of the STORIES_ABOVE_GRADE property.
	 * 
	 * @param aSTORIES_ABOVE_GRADE
	 *            the new value of the STORIES_ABOVE_GRADE property
	 */
	public void setSTORIES_ABOVE_GRADE(float aSTORIES_ABOVE_GRADE) {
		STORIES_ABOVE_GRADE = aSTORIES_ABOVE_GRADE;
	}

	/**
	 * Access method for the LEVELS_BELOW_GRADE property.
	 * 
	 * @return the current value of the LEVELS_BELOW_GRADE property
	 */
	public float getLEVELS_BELOW_GRADE() {
		return LEVELS_BELOW_GRADE;
	}

	/**
	 * Sets the value of the LEVELS_BELOW_GRADE property.
	 * 
	 * @param aLEVELS_BELOW_GRADE
	 *            the new value of the LEVELS_BELOW_GRADE property
	 */
	public void setLEVELS_BELOW_GRADE(float aLEVELS_BELOW_GRADE) {
		LEVELS_BELOW_GRADE = aLEVELS_BELOW_GRADE;
	}

	/**
	 * Access method for the FLOOR_AREA property.
	 * 
	 * @return the current value of the FLOOR_AREA property
	 */
	public float getFLOOR_AREA() {
		return FLOOR_AREA;
	}

	/**
	 * Sets the value of the FLOOR_AREA property.
	 * 
	 * @param aFLOOR_AREA
	 *            the new value of the FLOOR_AREA property
	 */
	public void setFLOOR_AREA(float aFLOOR_AREA) {
		FLOOR_AREA = aFLOOR_AREA;
	}

	/**
	 * Access method for the FLOOR_SETBACK property.
	 * 
	 * @return the current value of the FLOOR_SETBACK property
	 */
	public float getFLOOR_SETBACK() {
		return FLOOR_SETBACK;
	}

	/**
	 * Sets the value of the FLOOR_SETBACK property.
	 * 
	 * @param aFLOOR_SETBACK
	 *            the new value of the FLOOR_SETBACK property
	 */
	public void setFLOOR_SETBACK(float aFLOOR_SETBACK) {
		FLOOR_SETBACK = aFLOOR_SETBACK;
	}

	/**
	 * Access method for the ALLOW_AREA property.
	 * 
	 * @return the current value of the ALLOW_AREA property
	 */
	public float getALLOW_AREA() {
		return ALLOW_AREA;
	}

	/**
	 * Sets the value of the ALLOW_AREA property.
	 * 
	 * @param aALLOW_AREA
	 *            the new value of the ALLOW_AREA property
	 */
	public void setALLOW_AREA(float aALLOW_AREA) {
		ALLOW_AREA = aALLOW_AREA;
	}

	/**
	 * Access method for the ACTUAL_AREA property.
	 * 
	 * @return the current value of the ACTUAL_AREA property
	 */
	public float getACTUAL_AREA() {
		return ACTUAL_AREA;
	}

	/**
	 * Sets the value of the ACTUAL_AREA property.
	 * 
	 * @param aACTUAL_AREA
	 *            the new value of the ACTUAL_AREA property
	 */
	public void setACTUAL_AREA(float aACTUAL_AREA) {
		ACTUAL_AREA = aACTUAL_AREA;
	}

	/**
	 * Access method for the REQ_PARKING_AREA property.
	 * 
	 * @return the current value of the REQ_PARKING_AREA property
	 */
	public float getREQ_PARKING_AREA() {
		return REQ_PARKING_AREA;
	}

	/**
	 * Sets the value of the REQ_PARKING_AREA property.
	 * 
	 * @param aREQ_PARKING_AREA
	 *            the new value of the REQ_PARKING_AREA property
	 */
	public void setREQ_PARKING_AREA(float aREQ_PARKING_AREA) {
		REQ_PARKING_AREA = aREQ_PARKING_AREA;
	}

	/**
	 * Access method for the RUBBISH_AREA property.
	 * 
	 * @return the current value of the RUBBISH_AREA property
	 */
	public float getRUBBISH_AREA() {
		return RUBBISH_AREA;
	}

	/**
	 * Sets the value of the RUBBISH_AREA property.
	 * 
	 * @param aRUBBISH_AREA
	 *            the new value of the RUBBISH_AREA property
	 */
	public void setRUBBISH_AREA(float aRUBBISH_AREA) {
		RUBBISH_AREA = aRUBBISH_AREA;
	}

	/**
	 * Access method for the RUBBISH property.
	 * 
	 * @return the current value of the RUBBISH property
	 */
	public String getRUBBISH() {
		return RUBBISH;
	}

	/**
	 * Sets the value of the RUBBISH property.
	 * 
	 * @param aRUBBISH
	 *            the new value of the RUBBISH property
	 */
	public void setRUBBISH(String aRUBBISH) {
		RUBBISH = aRUBBISH;
	}

	/**
	 * Access method for the CONSTRUCTION_TYPE property.
	 * 
	 * @return the current value of the CONSTRUCTION_TYPE property
	 */
	public String getCONSTRUCTION_TYPE() {
		return CONSTRUCTION_TYPE;
	}

	/**
	 * Sets the value of the CONSTRUCTION_TYPE property.
	 * 
	 * @param aCONSTRUCTION_TYPE
	 *            the new value of the CONSTRUCTION_TYPE property
	 */
	public void setCONSTRUCTION_TYPE(String aCONSTRUCTION_TYPE) {
		CONSTRUCTION_TYPE = aCONSTRUCTION_TYPE;
	}

	/**
	 * Access method for the BLDG_CODE_ADD_DATE property.
	 * 
	 * @return the current value of the BLDG_CODE_ADD_DATE property
	 */
	public Date getBLDG_CODE_ADD_DATE() {
		return BLDG_CODE_ADD_DATE;
	}

	/**
	 * Sets the value of the BLDG_CODE_ADD_DATE property.
	 * 
	 * @param aBLDG_CODE_ADD_DATE
	 *            the new value of the BLDG_CODE_ADD_DATE property
	 */
	public void setBLDG_CODE_ADD_DATE(Date aBLDG_CODE_ADD_DATE) {
		BLDG_CODE_ADD_DATE = aBLDG_CODE_ADD_DATE;
	}

	/**
	 * Access method for the DRIVEWAY_SLOPE property.
	 * 
	 * @return the current value of the DRIVEWAY_SLOPE property
	 */
	public String getDRIVEWAY_SLOPE() {
		return DRIVEWAY_SLOPE;
	}

	/**
	 * Sets the value of the DRIVEWAY_SLOPE property.
	 * 
	 * @param aDRIVEWAY_SLOPE
	 *            the new value of the DRIVEWAY_SLOPE property
	 */
	public void setDRIVEWAY_SLOPE(String aDRIVEWAY_SLOPE) {
		DRIVEWAY_SLOPE = aDRIVEWAY_SLOPE;
	}

	/**
	 * Access method for the NO_OF_ELEVATORS property.
	 * 
	 * @return the current value of the NO_OF_ELEVATORS property
	 */
	public float getNO_OF_ELEVATORS() {
		return NO_OF_ELEVATORS;
	}

	/**
	 * Sets the value of the NO_OF_ELEVATORS property.
	 * 
	 * @param aNO_OF_ELEVATORS
	 *            the new value of the NO_OF_ELEVATORS property
	 */
	public void setNO_OF_ELEVATORS(float aNO_OF_ELEVATORS) {
		NO_OF_ELEVATORS = aNO_OF_ELEVATORS;
	}

	/**
	 * Access method for the NO_OF_STAIRWAYS property.
	 * 
	 * @return the current value of the NO_OF_STAIRWAYS property
	 */
	public float getNO_OF_STAIRWAYS() {
		return NO_OF_STAIRWAYS;
	}

	/**
	 * Sets the value of the NO_OF_STAIRWAYS property.
	 * 
	 * @param aNO_OF_STAIRWAYS
	 *            the new value of the NO_OF_STAIRWAYS property
	 */
	public void setNO_OF_STAIRWAYS(float aNO_OF_STAIRWAYS) {
		NO_OF_STAIRWAYS = aNO_OF_STAIRWAYS;
	}

	/**
	 * Access method for the ALARM_TYPES property.
	 * 
	 * @return the current value of the ALARM_TYPES property
	 */
	public String getALARM_TYPES() {
		return ALARM_TYPES;
	}

	/**
	 * Sets the value of the ALARM_TYPES property.
	 * 
	 * @param aALARM_TYPES
	 *            the new value of the ALARM_TYPES property
	 */
	public void setALARM_TYPES(String aALARM_TYPES) {
		ALARM_TYPES = aALARM_TYPES;
	}

	/**
	 * Access method for the FIRE_SPRINKLERS property.
	 * 
	 * @return the current value of the FIRE_SPRINKLERS property
	 */
	public String getFIRE_SPRINKLERS() {
		return FIRE_SPRINKLERS;
	}

	/**
	 * Sets the value of the FIRE_SPRINKLERS property.
	 * 
	 * @param aFIRE_SPRINKLERS
	 *            the new value of the FIRE_SPRINKLERS property
	 */
	public void setFIRE_SPRINKLERS(String aFIRE_SPRINKLERS) {
		FIRE_SPRINKLERS = aFIRE_SPRINKLERS;
	}

	/**
	 * Access method for the EMERG_POWER_SOURCE property.
	 * 
	 * @return the current value of the EMERG_POWER_SOURCE property
	 */
	public String getEMERG_POWER_SOURCE() {
		return EMERG_POWER_SOURCE;
	}

	/**
	 * Sets the value of the EMERG_POWER_SOURCE property.
	 * 
	 * @param aEMERG_POWER_SOURCE
	 *            the new value of the EMERG_POWER_SOURCE property
	 */
	public void setEMERG_POWER_SOURCE(String aEMERG_POWER_SOURCE) {
		EMERG_POWER_SOURCE = aEMERG_POWER_SOURCE;
	}

	/**
	 * Access method for the SHAFT_STAIR_PRESSURE property.
	 * 
	 * @return the current value of the SHAFT_STAIR_PRESSURE property
	 */
	public String getSHAFT_STAIR_PRESSURE() {
		return SHAFT_STAIR_PRESSURE;
	}

	/**
	 * Sets the value of the SHAFT_STAIR_PRESSURE property.
	 * 
	 * @param aSHAFT_STAIR_PRESSURE
	 *            the new value of the SHAFT_STAIR_PRESSURE property
	 */
	public void setSHAFT_STAIR_PRESSURE(String aSHAFT_STAIR_PRESSURE) {
		SHAFT_STAIR_PRESSURE = aSHAFT_STAIR_PRESSURE;
	}

	/**
	 * Access method for the FINE_ART property.
	 * 
	 * @return the current value of the FINE_ART property
	 */
	public String getFINE_ART() {
		return FINE_ART;
	}

	/**
	 * Sets the value of the FINE_ART property.
	 * 
	 * @param aFINE_ART
	 *            the new value of the FINE_ART property
	 */
	public void setFINE_ART(String aFINE_ART) {
		FINE_ART = aFINE_ART;
	}

	/**
	 * Access method for the FINE_ART_DESC property.
	 * 
	 * @return the current value of the FINE_ART_DESC property
	 */
	public String getFINE_ART_DESC() {
		return FINE_ART_DESC;
	}

	/**
	 * Sets the value of the FINE_ART_DESC property.
	 * 
	 * @param aFINE_ART_DESC
	 *            the new value of the FINE_ART_DESC property
	 */
	public void setFINE_ART_DESC(String aFINE_ART_DESC) {
		FINE_ART_DESC = aFINE_ART_DESC;
	}
}

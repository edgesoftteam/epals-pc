package elms.app.lso;

import org.apache.log4j.Logger;

/**
 * The address range for a certain land
 * 
 * @author Shekhar Jain (shekhar@edgesoftinc.com)
 * @author Anand Belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - formatted the source and added comments
 */

public class AddressRange {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AddressRange.class.getName());
	/**
	 * The start range of the address
	 */
	protected String startRange;
	/**
	 * The end range of the address
	 */
	protected String endRange;
	/**
	 * The street id
	 */
	protected String streetId;

	/**
	 * The address range default constructor
	 */
	public AddressRange() {
		this.startRange = "";
		this.endRange = "";
		this.streetId = "";
	}

	/**
	 * loaded constructor
	 * 
	 * @param aStartRange
	 * @param aEndRange
	 * @param aStreetId
	 */
	public AddressRange(String aStartRange, String aEndRange, String aStreetId) {
		this.startRange = aStartRange;
		this.endRange = aEndRange;
		this.streetId = aStreetId;
	}

	/**
	 * Returns the endRange.
	 * 
	 * @return String
	 */
	public String getEndRange() {
		return endRange;
	}

	/**
	 * Returns the startRange.
	 * 
	 * @return String
	 */
	public String getStartRange() {
		return startRange;
	}

	/**
	 * Returns the streetId.
	 * 
	 * @return Sttring
	 */
	public String getStreetId() {
		return streetId;
	}

	/**
	 * Sets the endRange.
	 * 
	 * @param endRange
	 *            The endRange to set
	 */
	public void setEndRange(String endRange) {
		this.endRange = endRange;
	}

	/**
	 * Sets the startRange.
	 * 
	 * @param startRange
	 *            The startRange to set
	 */
	public void setStartRange(String startRange) {
		this.startRange = startRange;
	}

	/**
	 * Sets the streetId.
	 * 
	 * @param streetId
	 *            The streetId to set
	 */
	public void setStreetId(String streetId) {
		this.streetId = streetId;
	}

}
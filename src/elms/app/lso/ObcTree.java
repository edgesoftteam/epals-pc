package elms.app.lso;

import java.io.Serializable;

import org.apache.log4j.Logger;

/**
 * @author Anand Belaguly
 */
public class ObcTree implements Serializable {

	static Logger logger = Logger.getLogger(ObcTree.class.getName());

	protected String nodeId;
	protected String nodeLevel;
	protected String nodeText;
	protected String nodeLink;
	protected String nodeAlt;
	protected boolean onHold = false;
	protected String holdType;
	protected String holdColor;

	protected long id;
	protected String type;
	protected String apn;
	protected String owner;

	/**
	 * 3CD0322702D9
	 */
	public ObcTree() {

	}

	public ObcTree(String nodeId, String nodeLevel, String nodeText, String nodeLink, String nodeAlt) {
		this.nodeId = nodeId;
		this.nodeLevel = nodeLevel;
		this.nodeText = nodeText;
		this.nodeLink = nodeLink;
		this.nodeAlt = nodeAlt;
	}

	public ObcTree(String nodeId, String nodeLevel, String nodeText, String nodeLink, String nodeAlt, long id, String type) {

		this.nodeId = nodeId;
		this.nodeLevel = nodeLevel;
		this.nodeText = nodeText;
		this.nodeLink = nodeLink;
		this.nodeAlt = nodeAlt;
		this.id = id;
		this.type = type;
	}

	/**
	 * @return Returns the apn.
	 */
	public String getApn() {
		return apn;
	}

	/**
	 * @param apn
	 *            The apn to set.
	 */
	public void setApn(String apn) {
		this.apn = apn;
	}

	/**
	 * @return Returns the owner.
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * @param owner
	 *            The owner to set.
	 */
	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * Gets the nodeId
	 * 
	 * @return Returns a String
	 */
	public String getNodeId() {
		return nodeId;
	}

	/**
	 * Sets the nodeId
	 * 
	 * @param nodeId
	 *            The nodeId to set
	 */
	public void setNodeId(String nodeId) {
		this.nodeId = nodeId;
	}

	/**
	 * Gets the nodeLevel
	 * 
	 * @return Returns a String
	 */
	public String getNodeLevel() {
		return nodeLevel;
	}

	/**
	 * Sets the nodeLevel
	 * 
	 * @param nodeLevel
	 *            The nodeLevel to set
	 */
	public void setNodeLevel(String nodeLevel) {
		this.nodeLevel = nodeLevel;
	}

	/**
	 * Gets the nodeText
	 * 
	 * @return Returns a String
	 */
	public String getNodeText() {
		return nodeText;
	}

	/**
	 * Sets the nodeText
	 * 
	 * @param nodeText
	 *            The nodeText to set
	 */
	public void setNodeText(String nodeText) {
		this.nodeText = nodeText;
	}

	/**
	 * Gets the nodeLink
	 * 
	 * @return Returns a String
	 */
	public String getNodeLink() {
		return nodeLink;
	}

	/**
	 * Sets the nodeLink
	 * 
	 * @param nodeLink
	 *            The nodeLink to set
	 */
	public void setNodeLink(String nodeLink) {
		this.nodeLink = nodeLink;
	}

	/**
	 * Gets the nodeAlt
	 * 
	 * @return Returns a String
	 */
	public String getNodeAlt() {
		return nodeAlt != null ? nodeAlt.toUpperCase() : "";
	}

	/**
	 * Sets the nodeAlt
	 * 
	 * @param nodeAlt
	 *            The nodeAlt to set
	 */
	public void setNodeAlt(String nodeAlt) {
		this.nodeAlt = nodeAlt;
	}

	/**
	 * @return
	 */
	public String getHoldType() {
		return holdType;
	}

	/**
	 * @return
	 */
	public boolean isOnHold() {
		return onHold;
	}

	/**
	 * @param string
	 */
	public void setHoldType(String string) {
		holdType = string;
	}

	/**
	 * @param b
	 */
	public void setOnHold(boolean b) {
		onHold = b;
	}

	/**
	 * @return
	 */
	public String getHoldColor() {
		if (holdType != null) {
			if (holdType.equals("H")) {
				holdColor = "red";
			} else if (holdType.equals("S")) {
				holdColor = "orange";
			} else if (holdType.equals("W")) {
				holdColor = "blue";
			}
		}
		return holdColor;
	}

	/**
	 * @param string
	 */
	public void setHoldColor(String string) {
		holdColor = string;
	}

	/**
	 * @return
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param string
	 */
	public void setType(String string) {
		type = string;
	}

	/**
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param l
	 */
	public void setId(long l) {
		id = l;
	}

}
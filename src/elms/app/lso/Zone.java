package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class Zone {
	protected int id;
	protected String description;

	public Zone() {
	}

	public Zone(int id, String description) {
		this.id = id;
		this.description = description;
	}

	/**
	 * Gets the id
	 * 
	 * @return Returns a String
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public String toString() {
		return ("Zone Object " + id + "--" + description);
	}

}

//Source file: C:\\datafile\\source\\elms\\app\\lso\\Owner.java

package elms.app.lso;

import java.util.Date;

import elms.security.User;

/**
 * @author Shekhar Jain
 * @author Anand Belaguly
 */
public class Owner {
	protected int ownerId;
	protected String name;
	protected Address localAddress;
	protected String foreignFlag = "N";
	protected ForeignAddress foreignAddress;
	protected String phone;
	protected String fax;
	protected String email;
	protected Date lastUpdated;
	protected User updatedBy;

	public Owner(int ownerId, String name, Address localAddress, String foreignFlag, ForeignAddress foreignAddress, String phone, String fax, String email) {
		this.ownerId = ownerId;
		this.name = name;
		this.localAddress = localAddress;
		this.foreignFlag = foreignFlag;
		this.foreignAddress = foreignAddress;
		this.phone = phone;
		this.fax = fax;
		this.email = email;
	}

	/**
	 * 3CCDDD02022D
	 */
	public Owner() {
		localAddress = new Address();
		foreignAddress = new ForeignAddress();
		lastUpdated = new Date();
		updatedBy = new User();
	}

	/**
	 * Gets the ownerId
	 * 
	 * @return Returns a int
	 */
	public int getOwnerId() {
		return ownerId;
	}

	/**
	 * Sets the ownerId
	 * 
	 * @param ownerId
	 *            The ownerId to set
	 */
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the localAddress
	 * 
	 * @return Returns a Address
	 */
	public Address getLocalAddress() {
		return localAddress;
	}

	/**
	 * Sets the localAddress
	 * 
	 * @param localAddress
	 *            The localAddress to set
	 */
	public void setLocalAddress(Address localAddress) {
		this.localAddress = localAddress;
	}

	/**
	 * Gets the foreignAddress
	 * 
	 * @return Returns a ForeignAddress
	 */
	public ForeignAddress getForeignAddress() {
		return foreignAddress;
	}

	/**
	 * Sets the foreignAddress
	 * 
	 * @param foreignAddress
	 *            The foreignAddress to set
	 */
	public void setForeignAddress(ForeignAddress foreignAddress) {
		this.foreignAddress = foreignAddress;
	}

	/**
	 * Gets the phone
	 * 
	 * @return Returns a String
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * Sets the phone
	 * 
	 * @param phone
	 *            The phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * Gets the fax
	 * 
	 * @return Returns a String
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * Sets the fax
	 * 
	 * @param fax
	 *            The fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * Gets the email
	 * 
	 * @return Returns a String
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email
	 * 
	 * @param email
	 *            The email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the lastUpdated
	 * 
	 * @return Returns a Date
	 */
	public Date getLastUpdated() {
		return lastUpdated;
	}

	/**
	 * Sets the lastUpdated
	 * 
	 * @param lastUpdated
	 *            The lastUpdated to set
	 */
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the foreignFlag
	 * 
	 * @return Returns a String
	 */
	public String getForeignFlag() {
		return foreignFlag;
	}

	/**
	 * Sets the foreignFlag
	 * 
	 * @param foreignFlag
	 *            The foreignFlag to set
	 */
	public void setForeignFlag(String foreignFlag) {
		this.foreignFlag = foreignFlag;
	}

}
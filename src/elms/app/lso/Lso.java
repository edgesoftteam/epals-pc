//Source file: C:\\datafile\\source\\elms\\app\\lso\\Lso.java

package elms.app.lso;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shekhar Jain
 * @author Anand Belaguly (05/09/2002)
 */
public class Lso {
	protected int lsoId;
	protected String lsoType;
	List lsoHold;
	List lsoTenant;
	List lsoCondition;
	List lsoAttachment;
	List lsoComment;

	
	public Lso(int lsoId, List lsoHold, List lsoTenant, List lsoCondition, List lsoAttachment, List lsoComment) {
		this.lsoId = lsoId;
		this.lsoHold = lsoHold;
		this.lsoCondition = lsoCondition;
		this.lsoAttachment = lsoAttachment;
		this.lsoComment = lsoComment;
		this.lsoTenant = lsoTenant;
	}

	public Lso(int lsoId, List lsoHold, List lsoCondition, List lsoAttachment, List lsoComment) {
		this.lsoId = lsoId;
		this.lsoHold = lsoHold;
		this.lsoCondition = lsoCondition;
		this.lsoAttachment = lsoAttachment;
		this.lsoComment = lsoComment;
	}

	public Lso(int lsoId) {
		this.lsoId = lsoId;
	}

	public Lso() {
		lsoHold = new ArrayList();
		lsoCondition = new ArrayList();
		lsoAttachment = new ArrayList();
		lsoComment = new ArrayList();

	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the lsoHold
	 * 
	 * @return Returns a List
	 */
	public List getLsoHold() {
		return lsoHold;
	}

	/**
	 * Sets the lsoHold
	 * 
	 * @param lsoHold
	 *            The lsoHold to set
	 */
	public void setLsoHold(List lsoHold) {
		this.lsoHold = lsoHold;
	}

	/**
	 * Gets the lsoCondition
	 * 
	 * @return Returns a List
	 */
	public List getLsoCondition() {
		return lsoCondition;
	}

	/**
	 * Sets the lsoCondition
	 * 
	 * @param lsoCondition
	 *            The lsoCondition to set
	 */
	public void setLsoCondition(List lsoCondition) {
		this.lsoCondition = lsoCondition;
	}

	/**
	 * Gets the lsoAttachment
	 * 
	 * @return Returns a List
	 */
	public List getLsoAttachment() {
		return lsoAttachment;
	}

	/**
	 * Sets the lsoAttachment
	 * 
	 * @param lsoAttachment
	 *            The lsoAttachment to set
	 */
	public void setLsoAttachment(List lsoAttachment) {
		this.lsoAttachment = lsoAttachment;
	}

	/**
	 * Gets the lsoComment
	 * 
	 * @return Returns a List
	 */
	public List getLsoComment() {
		return lsoComment;
	}

	/**
	 * Sets the lsoComment
	 * 
	 * @param lsoComment
	 *            The lsoComment to set
	 */
	public void setLsoComment(List lsoComment) {
		this.lsoComment = lsoComment;
	}

	/**
	 * Gets the lsoType
	 * 
	 * @return Returns a String
	 */
	public String getLsoType() {
		return lsoType;
	}

	/**
	 * Sets the lsoType
	 * 
	 * @param lsoType
	 *            The lsoType to set
	 */
	public void setLsoType(String lsoType) {
		this.lsoType = lsoType;
	}

	/**
	 * @return the lsoTenant
	 */
	public List getLsoTenant() {
		return lsoTenant;
	}

	/**
	 * @param lsoTenant the lsoTenant to set
	 */
	public void setLsoTenant(List lsoTenant) {
		this.lsoTenant = lsoTenant;
	}

}
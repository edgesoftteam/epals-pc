//Source file: C:\\datafile\\source\\elms\\app\\lso\\OccupancyAddress.java

package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class OccupancyAddress extends LsoAddress {

	protected String unit;

	/**
	 * 3CCDCC7E027E
	 */
	public OccupancyAddress() {
		super();
		this.lsoType = "O";

	}

	public OccupancyAddress(int addressId, int lsoId, String lsoType, int streetNbr, String streetModifier, Street street, String unit, String city, String state, String zip, String zip4, String description, String primary, String active) {

		super(addressId, lsoId, lsoType, streetNbr, streetModifier, street, city, state, zip, zip4, description, primary, active);

		this.unit = unit;

	}

	/**
	 * Gets the unit
	 * 
	 * @return Returns a int
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * Sets the addressId
	 * 
	 * @param addressId
	 *            The addressId to set
	 */
	public void setUnit(String aUnit) {
		this.unit = aUnit;
	}

}
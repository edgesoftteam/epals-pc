//Source file: C:\\datafile\\source\\elms\\app\\lso\\OccupancySiteData.java

package elms.app.lso;

import elms.app.sitedata.SiteData;

public class OccupancySiteData extends SiteData {
	public Integer occupancyId;
	public String UNIT_ADDRESS;
	public String USE_DESC;
	public float UNIT_AREA;
	public String OCCUPANT_NAME;
	public String OCCUPANCY_GRP;
	public float OCCUPANCY_AREA;
	public float OCCUPANCY_LOAD;
	public String OCCUPANCY_SEPARATION;
	public String OCCUPANCY_SEPARATION_DESC;
	public float NO_OF_BEDROOMS;
	public float BAR_DINING_AREA;
	public float NO_OF_EXITS;
	public String CORRIDOR;
	public String SMOKE_DETECTOR;
	public String SMOKE_DETECTOR_DESC;
	public String HAZ_MAT;
	public String HAZ_MAT_DESC;
	public String EMERG_LITES;
	public String EMERG_LITES_DESC;
	public String EXIT_SIGNS;
	public String EXIT_SIGNS_DESC;
	public String CERT_OF_OCCUPANCY;
	public String CERT_OF_OCCUPANCY_DESC;
	public String MODIFICATIONS;
	public float ASSEMBLY_SEATING;
	public float STAGE;
	public String FINE_ART;
	public String MED_GAS;
	public String XRAY_EQUIP;

	/**
	 * 3CD02FA401BC
	 */
	public OccupancySiteData() {

	}

	/**
	 * Access method for the occupancyId property.
	 * 
	 * @return the current value of the occupancyId property
	 */
	public Integer getOccupancyId() {
		return occupancyId;
	}

	/**
	 * Sets the value of the occupancyId property.
	 * 
	 * @param aOccupancyId
	 *            the new value of the occupancyId property
	 */
	public void setOccupancyId(Integer aOccupancyId) {
		occupancyId = aOccupancyId;
	}

	/**
	 * Access method for the UNIT_ADDRESS property.
	 * 
	 * @return the current value of the UNIT_ADDRESS property
	 */
	public String getUNIT_ADDRESS() {
		return UNIT_ADDRESS;
	}

	/**
	 * Sets the value of the UNIT_ADDRESS property.
	 * 
	 * @param aUNIT_ADDRESS
	 *            the new value of the UNIT_ADDRESS property
	 */
	public void setUNIT_ADDRESS(String aUNIT_ADDRESS) {
		UNIT_ADDRESS = aUNIT_ADDRESS;
	}

	/**
	 * Access method for the USE_DESC property.
	 * 
	 * @return the current value of the USE_DESC property
	 */
	public String getUSE_DESC() {
		return USE_DESC;
	}

	/**
	 * Sets the value of the USE_DESC property.
	 * 
	 * @param aUSE_DESC
	 *            the new value of the USE_DESC property
	 */
	public void setUSE_DESC(String aUSE_DESC) {
		USE_DESC = aUSE_DESC;
	}

	/**
	 * Access method for the UNIT_AREA property.
	 * 
	 * @return the current value of the UNIT_AREA property
	 */
	public float getUNIT_AREA() {
		return UNIT_AREA;
	}

	/**
	 * Sets the value of the UNIT_AREA property.
	 * 
	 * @param aUNIT_AREA
	 *            the new value of the UNIT_AREA property
	 */
	public void setUNIT_AREA(float aUNIT_AREA) {
		UNIT_AREA = aUNIT_AREA;
	}

	/**
	 * Access method for the OCCUPANT_NAME property.
	 * 
	 * @return the current value of the OCCUPANT_NAME property
	 */
	public String getOCCUPANT_NAME() {
		return OCCUPANT_NAME;
	}

	/**
	 * Sets the value of the OCCUPANT_NAME property.
	 * 
	 * @param aOCCUPANT_NAME
	 *            the new value of the OCCUPANT_NAME property
	 */
	public void setOCCUPANT_NAME(String aOCCUPANT_NAME) {
		OCCUPANT_NAME = aOCCUPANT_NAME;
	}

	/**
	 * Access method for the OCCUPANCY_GRP property.
	 * 
	 * @return the current value of the OCCUPANCY_GRP property
	 */
	public String getOCCUPANCY_GRP() {
		return OCCUPANCY_GRP;
	}

	/**
	 * Sets the value of the OCCUPANCY_GRP property.
	 * 
	 * @param aOCCUPANCY_GRP
	 *            the new value of the OCCUPANCY_GRP property
	 */
	public void setOCCUPANCY_GRP(String aOCCUPANCY_GRP) {
		OCCUPANCY_GRP = aOCCUPANCY_GRP;
	}

	/**
	 * Access method for the OCCUPANCY_AREA property.
	 * 
	 * @return the current value of the OCCUPANCY_AREA property
	 */
	public float getOCCUPANCY_AREA() {
		return OCCUPANCY_AREA;
	}

	/**
	 * Sets the value of the OCCUPANCY_AREA property.
	 * 
	 * @param aOCCUPANCY_AREA
	 *            the new value of the OCCUPANCY_AREA property
	 */
	public void setOCCUPANCY_AREA(float aOCCUPANCY_AREA) {
		OCCUPANCY_AREA = aOCCUPANCY_AREA;
	}

	/**
	 * Access method for the OCCUPANCY_LOAD property.
	 * 
	 * @return the current value of the OCCUPANCY_LOAD property
	 */
	public float getOCCUPANCY_LOAD() {
		return OCCUPANCY_LOAD;
	}

	/**
	 * Sets the value of the OCCUPANCY_LOAD property.
	 * 
	 * @param aOCCUPANCY_LOAD
	 *            the new value of the OCCUPANCY_LOAD property
	 */
	public void setOCCUPANCY_LOAD(float aOCCUPANCY_LOAD) {
		OCCUPANCY_LOAD = aOCCUPANCY_LOAD;
	}

	/**
	 * Access method for the OCCUPANCY_SEPARATION property.
	 * 
	 * @return the current value of the OCCUPANCY_SEPARATION property
	 */
	public String getOCCUPANCY_SEPARATION() {
		return OCCUPANCY_SEPARATION;
	}

	/**
	 * Sets the value of the OCCUPANCY_SEPARATION property.
	 * 
	 * @param aOCCUPANCY_SEPARATION
	 *            the new value of the OCCUPANCY_SEPARATION property
	 */
	public void setOCCUPANCY_SEPARATION(String aOCCUPANCY_SEPARATION) {
		OCCUPANCY_SEPARATION = aOCCUPANCY_SEPARATION;
	}

	/**
	 * Access method for the OCCUPANCY_SEPARATION_DESC property.
	 * 
	 * @return the current value of the OCCUPANCY_SEPARATION_DESC property
	 */
	public String getOCCUPANCY_SEPARATION_DESC() {
		return OCCUPANCY_SEPARATION_DESC;
	}

	/**
	 * Sets the value of the OCCUPANCY_SEPARATION_DESC property.
	 * 
	 * @param aOCCUPANCY_SEPARATION_DESC
	 *            the new value of the OCCUPANCY_SEPARATION_DESC property
	 */
	public void setOCCUPANCY_SEPARATION_DESC(String aOCCUPANCY_SEPARATION_DESC) {
		OCCUPANCY_SEPARATION_DESC = aOCCUPANCY_SEPARATION_DESC;
	}

	/**
	 * Access method for the NO_OF_BEDROOMS property.
	 * 
	 * @return the current value of the NO_OF_BEDROOMS property
	 */
	public float getNO_OF_BEDROOMS() {
		return NO_OF_BEDROOMS;
	}

	/**
	 * Sets the value of the NO_OF_BEDROOMS property.
	 * 
	 * @param aNO_OF_BEDROOMS
	 *            the new value of the NO_OF_BEDROOMS property
	 */
	public void setNO_OF_BEDROOMS(float aNO_OF_BEDROOMS) {
		NO_OF_BEDROOMS = aNO_OF_BEDROOMS;
	}

	/**
	 * Access method for the BAR_DINING_AREA property.
	 * 
	 * @return the current value of the BAR_DINING_AREA property
	 */
	public float getBAR_DINING_AREA() {
		return BAR_DINING_AREA;
	}

	/**
	 * Sets the value of the BAR_DINING_AREA property.
	 * 
	 * @param aBAR_DINING_AREA
	 *            the new value of the BAR_DINING_AREA property
	 */
	public void setBAR_DINING_AREA(float aBAR_DINING_AREA) {
		BAR_DINING_AREA = aBAR_DINING_AREA;
	}

	/**
	 * Access method for the NO_OF_EXITS property.
	 * 
	 * @return the current value of the NO_OF_EXITS property
	 */
	public float getNO_OF_EXITS() {
		return NO_OF_EXITS;
	}

	/**
	 * Sets the value of the NO_OF_EXITS property.
	 * 
	 * @param aNO_OF_EXITS
	 *            the new value of the NO_OF_EXITS property
	 */
	public void setNO_OF_EXITS(float aNO_OF_EXITS) {
		NO_OF_EXITS = aNO_OF_EXITS;
	}

	/**
	 * Access method for the CORRIDOR property.
	 * 
	 * @return the current value of the CORRIDOR property
	 */
	public String getCORRIDOR() {
		return CORRIDOR;
	}

	/**
	 * Sets the value of the CORRIDOR property.
	 * 
	 * @param aCORRIDOR
	 *            the new value of the CORRIDOR property
	 */
	public void setCORRIDOR(String aCORRIDOR) {
		CORRIDOR = aCORRIDOR;
	}

	/**
	 * Access method for the SMOKE_DETECTOR property.
	 * 
	 * @return the current value of the SMOKE_DETECTOR property
	 */
	public String getSMOKE_DETECTOR() {
		return SMOKE_DETECTOR;
	}

	/**
	 * Sets the value of the SMOKE_DETECTOR property.
	 * 
	 * @param aSMOKE_DETECTOR
	 *            the new value of the SMOKE_DETECTOR property
	 */
	public void setSMOKE_DETECTOR(String aSMOKE_DETECTOR) {
		SMOKE_DETECTOR = aSMOKE_DETECTOR;
	}

	/**
	 * Access method for the SMOKE_DETECTOR_DESC property.
	 * 
	 * @return the current value of the SMOKE_DETECTOR_DESC property
	 */
	public String getSMOKE_DETECTOR_DESC() {
		return SMOKE_DETECTOR_DESC;
	}

	/**
	 * Sets the value of the SMOKE_DETECTOR_DESC property.
	 * 
	 * @param aSMOKE_DETECTOR_DESC
	 *            the new value of the SMOKE_DETECTOR_DESC property
	 */
	public void setSMOKE_DETECTOR_DESC(String aSMOKE_DETECTOR_DESC) {
		SMOKE_DETECTOR_DESC = aSMOKE_DETECTOR_DESC;
	}

	/**
	 * Access method for the HAZ_MAT property.
	 * 
	 * @return the current value of the HAZ_MAT property
	 */
	public String getHAZ_MAT() {
		return HAZ_MAT;
	}

	/**
	 * Sets the value of the HAZ_MAT property.
	 * 
	 * @param aHAZ_MAT
	 *            the new value of the HAZ_MAT property
	 */
	public void setHAZ_MAT(String aHAZ_MAT) {
		HAZ_MAT = aHAZ_MAT;
	}

	/**
	 * Access method for the HAZ_MAT_DESC property.
	 * 
	 * @return the current value of the HAZ_MAT_DESC property
	 */
	public String getHAZ_MAT_DESC() {
		return HAZ_MAT_DESC;
	}

	/**
	 * Sets the value of the HAZ_MAT_DESC property.
	 * 
	 * @param aHAZ_MAT_DESC
	 *            the new value of the HAZ_MAT_DESC property
	 */
	public void setHAZ_MAT_DESC(String aHAZ_MAT_DESC) {
		HAZ_MAT_DESC = aHAZ_MAT_DESC;
	}

	/**
	 * Access method for the EMERG_LITES property.
	 * 
	 * @return the current value of the EMERG_LITES property
	 */
	public String getEMERG_LITES() {
		return EMERG_LITES;
	}

	/**
	 * Sets the value of the EMERG_LITES property.
	 * 
	 * @param aEMERG_LITES
	 *            the new value of the EMERG_LITES property
	 */
	public void setEMERG_LITES(String aEMERG_LITES) {
		EMERG_LITES = aEMERG_LITES;
	}

	/**
	 * Access method for the EMERG_LITES_DESC property.
	 * 
	 * @return the current value of the EMERG_LITES_DESC property
	 */
	public String getEMERG_LITES_DESC() {
		return EMERG_LITES_DESC;
	}

	/**
	 * Sets the value of the EMERG_LITES_DESC property.
	 * 
	 * @param aEMERG_LITES_DESC
	 *            the new value of the EMERG_LITES_DESC property
	 */
	public void setEMERG_LITES_DESC(String aEMERG_LITES_DESC) {
		EMERG_LITES_DESC = aEMERG_LITES_DESC;
	}

	/**
	 * Access method for the EXIT_SIGNS property.
	 * 
	 * @return the current value of the EXIT_SIGNS property
	 */
	public String getEXIT_SIGNS() {
		return EXIT_SIGNS;
	}

	/**
	 * Sets the value of the EXIT_SIGNS property.
	 * 
	 * @param aEXIT_SIGNS
	 *            the new value of the EXIT_SIGNS property
	 */
	public void setEXIT_SIGNS(String aEXIT_SIGNS) {
		EXIT_SIGNS = aEXIT_SIGNS;
	}

	/**
	 * Access method for the EXIT_SIGNS_DESC property.
	 * 
	 * @return the current value of the EXIT_SIGNS_DESC property
	 */
	public String getEXIT_SIGNS_DESC() {
		return EXIT_SIGNS_DESC;
	}

	/**
	 * Sets the value of the EXIT_SIGNS_DESC property.
	 * 
	 * @param aEXIT_SIGNS_DESC
	 *            the new value of the EXIT_SIGNS_DESC property
	 */
	public void setEXIT_SIGNS_DESC(String aEXIT_SIGNS_DESC) {
		EXIT_SIGNS_DESC = aEXIT_SIGNS_DESC;
	}

	/**
	 * Access method for the CERT_OF_OCCUPANCY property.
	 * 
	 * @return the current value of the CERT_OF_OCCUPANCY property
	 */
	public String getCERT_OF_OCCUPANCY() {
		return CERT_OF_OCCUPANCY;
	}

	/**
	 * Sets the value of the CERT_OF_OCCUPANCY property.
	 * 
	 * @param aCERT_OF_OCCUPANCY
	 *            the new value of the CERT_OF_OCCUPANCY property
	 */
	public void setCERT_OF_OCCUPANCY(String aCERT_OF_OCCUPANCY) {
		CERT_OF_OCCUPANCY = aCERT_OF_OCCUPANCY;
	}

	/**
	 * Access method for the CERT_OF_OCCUPANCY_DESC property.
	 * 
	 * @return the current value of the CERT_OF_OCCUPANCY_DESC property
	 */
	public String getCERT_OF_OCCUPANCY_DESC() {
		return CERT_OF_OCCUPANCY_DESC;
	}

	/**
	 * Sets the value of the CERT_OF_OCCUPANCY_DESC property.
	 * 
	 * @param aCERT_OF_OCCUPANCY_DESC
	 *            the new value of the CERT_OF_OCCUPANCY_DESC property
	 */
	public void setCERT_OF_OCCUPANCY_DESC(String aCERT_OF_OCCUPANCY_DESC) {
		CERT_OF_OCCUPANCY_DESC = aCERT_OF_OCCUPANCY_DESC;
	}

	/**
	 * Access method for the MODIFICATIONS property.
	 * 
	 * @return the current value of the MODIFICATIONS property
	 */
	public String getMODIFICATIONS() {
		return MODIFICATIONS;
	}

	/**
	 * Sets the value of the MODIFICATIONS property.
	 * 
	 * @param aMODIFICATIONS
	 *            the new value of the MODIFICATIONS property
	 */
	public void setMODIFICATIONS(String aMODIFICATIONS) {
		MODIFICATIONS = aMODIFICATIONS;
	}

	/**
	 * Access method for the ASSEMBLY_SEATING property.
	 * 
	 * @return the current value of the ASSEMBLY_SEATING property
	 */
	public float getASSEMBLY_SEATING() {
		return ASSEMBLY_SEATING;
	}

	/**
	 * Sets the value of the ASSEMBLY_SEATING property.
	 * 
	 * @param aASSEMBLY_SEATING
	 *            the new value of the ASSEMBLY_SEATING property
	 */
	public void setASSEMBLY_SEATING(float aASSEMBLY_SEATING) {
		ASSEMBLY_SEATING = aASSEMBLY_SEATING;
	}

	/**
	 * Access method for the STAGE property.
	 * 
	 * @return the current value of the STAGE property
	 */
	public float getSTAGE() {
		return STAGE;
	}

	/**
	 * Sets the value of the STAGE property.
	 * 
	 * @param aSTAGE
	 *            the new value of the STAGE property
	 */
	public void setSTAGE(float aSTAGE) {
		STAGE = aSTAGE;
	}

	/**
	 * Access method for the FINE_ART property.
	 * 
	 * @return the current value of the FINE_ART property
	 */
	public String getFINE_ART() {
		return FINE_ART;
	}

	/**
	 * Sets the value of the FINE_ART property.
	 * 
	 * @param aFINE_ART
	 *            the new value of the FINE_ART property
	 */
	public void setFINE_ART(String aFINE_ART) {
		FINE_ART = aFINE_ART;
	}

	/**
	 * Access method for the MED_GAS property.
	 * 
	 * @return the current value of the MED_GAS property
	 */
	public String getMED_GAS() {
		return MED_GAS;
	}

	/**
	 * Sets the value of the MED_GAS property.
	 * 
	 * @param aMED_GAS
	 *            the new value of the MED_GAS property
	 */
	public void setMED_GAS(String aMED_GAS) {
		MED_GAS = aMED_GAS;
	}

	/**
	 * Access method for the XRAY_EQUIP property.
	 * 
	 * @return the current value of the XRAY_EQUIP property
	 */
	public String getXRAY_EQUIP() {
		return XRAY_EQUIP;
	}

	/**
	 * Sets the value of the XRAY_EQUIP property.
	 * 
	 * @param aXRAY_EQUIP
	 *            the new value of the XRAY_EQUIP property
	 */
	public void setXRAY_EQUIP(String aXRAY_EQUIP) {
		XRAY_EQUIP = aXRAY_EQUIP;
	}
}

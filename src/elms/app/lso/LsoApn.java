package elms.app.lso;

import org.apache.log4j.Logger;

public class LsoApn extends Apn {

	static Logger logger = Logger.getLogger(LsoApn.class.getName());

	public LsoApn() {
	}

	public LsoApn(String parcelNumber, Owner owner, String active) {
		super(parcelNumber, owner, active);
	}

}
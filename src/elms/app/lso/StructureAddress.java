//Source file: C:\\datafile\\source\\elms\\app\\lso\\StructureAddress.java

package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class StructureAddress extends LsoAddress {

	/**
	 * @param street
	 * @param primary
	 * @param active
	 * @param city
	 * @param state
	 * @param zip
	 *            3CCDCC58007B
	 */

	/**
	 * 3CCDCC50034A
	 */
	public StructureAddress() {
		super();
		this.lsoType = "S";
	}

	public StructureAddress(int addressId, int lsoId, int streetNbr, String streetModifier, Street street, String city, String state, String zip, String zip4, String description, String primary, String active) {
		this();
		this.addressId = addressId;
		this.lsoId = lsoId;
		this.streetNbr = streetNbr;
		this.streetModifier = streetModifier;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.zip4 = zip4;
		this.description = description;
		this.primary = primary;
		this.active = active;
	}
}

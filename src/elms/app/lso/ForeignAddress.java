package elms.app.lso;

/**
 * The foreign address
 * 
 * @author Shekhar jain (shekhar@edgesoftinc.com)
 * @author Anand Belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - formatted and commented the file.
 */
public class ForeignAddress {
	/**
	 * The line one
	 */
	protected String lineOne;
	/**
	 * The line two of the address
	 */
	protected String lineTwo;
	/**
	 * The third line of the address
	 */
	protected String lineThree;
	/**
	 * The fourth line of the address
	 */
	protected String lineFour;
	/**
	 * The country of the address
	 */
	protected String country;

	/**
	 * default constructor
	 */
	public ForeignAddress() {
	}

	/**
	 * The loaded constructor
	 * 
	 * @param aLineOne
	 * @param aLineTwo
	 * @param aLineThree
	 * @param aLineFour
	 * @param aCountry
	 */
	public ForeignAddress(String aLineOne, String aLineTwo, String aLineThree, String aLineFour, String aCountry) {
		this.lineOne = aLineOne;
		this.lineTwo = aLineTwo;
		this.lineThree = aLineThree;
		this.lineFour = aLineFour;
		this.country = aCountry;

	}

	/**
	 * Gets the lineOne
	 * 
	 * @return Returns a String
	 */
	public String getLineOne() {
		return lineOne;
	}

	/**
	 * Sets the lineOne
	 * 
	 * @param lineOne
	 *            The lineOne to set
	 */
	public void setLineOne(String lineOne) {
		this.lineOne = lineOne;
	}

	/**
	 * Gets the lineTwo
	 * 
	 * @return Returns a Street
	 */
	public String getLineTwo() {
		return lineTwo;
	}

	/**
	 * Sets the lineTwo
	 * 
	 * @param lineTwo
	 *            The lineTwo to set
	 */
	public void setLineTwo(String lineTwo) {
		this.lineTwo = lineTwo;
	}

	/**
	 * Gets the lineThree
	 * 
	 * @return Returns a String
	 */
	public String getLineThree() {
		return lineThree;
	}

	/**
	 * Sets the lineThree
	 * 
	 * @param lineThree
	 *            The lineThree to set
	 */
	public void setLineThree(String lineThree) {
		this.lineThree = lineThree;
	}

	/**
	 * Gets the lineFour
	 * 
	 * @return Returns a String
	 */
	public String getLineFour() {
		return lineFour;
	}

	/**
	 * Sets the lineFour
	 * 
	 * @param lineFour
	 *            The lineFour to set
	 */
	public void setLineFour(String lineFour) {
		this.lineFour = lineFour;
	}

	/**
	 * Gets the country
	 * 
	 * @return Returns a String
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country
	 * 
	 * @param country
	 *            The country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

}

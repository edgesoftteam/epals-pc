//Source file: C:\\datafile\\source\\elms\\app\\lso\\OccupancyDetails.java

package elms.app.lso;

import java.util.List;

import elms.app.admin.ParkingZone;

/**
 * @author Shekhar Jain
 */
public class OccupancyDetails extends Details {
	// protected String name;
	protected String unitNumber;
	protected String beginFloor;
	protected String endFloor;
	protected String condo;
	protected ParkingZone parkingZone;
	protected String rZone;

	/**
	 * @param unitNumber
	 * @param beginFloor
	 * @param endFloor
	 *            3CCDBD3B0177
	 */
	public OccupancyDetails(String alias, String description, List use, String active, String unitNumber, String beginFloor, String endFloor, String label) {
		super(alias, description, use, active, label);
		this.unitNumber = unitNumber;
		this.beginFloor = beginFloor;
		this.endFloor = endFloor;
	}

	/**
	 * 3CCDBD32037D
	 */
	public OccupancyDetails() {

	}

	/**
	 * Access method for the name property.
	 * 
	 * @return the current value of the name property
	 */
	/**
	 * public String getName() { return name; }
	 * 
	 * 
	 * Sets the value of the name property.
	 * 
	 * @param aName
	 *            the new value of the name property
	 */
	/**
	 * public void setName(String aName) { name = aName; }
	 * 
	 * 
	 * Access method for the unitNumber property.
	 * 
	 * @return the current value of the unitNumber property
	 */
	public String getUnitNumber() {
		return unitNumber;
	}

	/**
	 * Sets the value of the unitNumber property.
	 * 
	 * @param aUnitNumber
	 *            the new value of the unitNumber property
	 */
	public void setUnitNumber(String aUnitNumber) {
		unitNumber = aUnitNumber;
	}

	/**
	 * Access method for the beginFloor property.
	 * 
	 * @return the current value of the beginFloor property
	 */
	public String getBeginFloor() {
		return beginFloor;
	}

	/**
	 * Sets the value of the beginFloor property.
	 * 
	 * @param aBeginFloor
	 *            the new value of the beginFloor property
	 */
	public void setBeginFloor(String aBeginFloor) {
		beginFloor = aBeginFloor;
	}

	/**
	 * Access method for the endFloor property.
	 * 
	 * @return the current value of the endFloor property
	 */
	public String getEndFloor() {
		return endFloor;
	}

	/**
	 * Sets the value of the endFloor property.
	 * 
	 * @param aEndFloor
	 *            the new value of the endFloor property
	 */
	public void setEndFloor(String aEndFloor) {
		endFloor = aEndFloor;
	}

	/**
	 * Gets the condo
	 * 
	 * @return Returns a String
	 */
	public String getCondo() {
		return condo;
	}

	/**
	 * Sets the condo
	 * 
	 * @param condo
	 *            The condo to set
	 */
	public void setCondo(String condo) {
		this.condo = condo;
	}

	/**
	 * Returns the parkingZone.
	 * 
	 * @return ParkingZone
	 */
	public ParkingZone getParkingZone() {
		return parkingZone;
	}

	/**
	 * Returns the rZone.
	 * 
	 * @return String
	 */
	public String getRZone() {
		return rZone;
	}

	/**
	 * Sets the parkingZone.
	 * 
	 * @param parkingZone
	 *            The parkingZone to set
	 */
	public void setParkingZone(ParkingZone parkingZone) {
		this.parkingZone = parkingZone;
	}

	/**
	 * Sets the rZone.
	 * 
	 * @param rZone
	 *            The rZone to set
	 */
	public void setRZone(String rZone) {
		this.rZone = rZone;
	}

}

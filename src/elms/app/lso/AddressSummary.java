package elms.app.lso;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import elms.app.admin.lso.StructureEdit;

/**
 * The Address summary
 * 
 * @author Shekhar Jain (shekhar@edgesoftinc.com)
 * @author Anand Belaguly (anand@edgesoftinc.com) last updated - Dec 11, 2003 - Anand Belaguly
 * 
 */
public class AddressSummary implements Serializable {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(AddressSummary.class.getName());

	/**
	 * The LSO ID
	 */
	protected int lsoId;
	/**
	 * The description
	 */
	protected String description;
	/**
	 * The list of zones to which this land belongs
	 */
	protected List zoneList;
	/**
	 * The list of uses of the land
	 */
	protected List useList;
	/**
	 * The list of address ranges surrounding this address
	 */
	protected List addressRangeList;
	/**
	 * The list of projects happenning on this land
	 */
	protected List projectList;
	/**
	 * The owners for this address
	 */
	protected List ownerList;
	/**
	 * The APNs for this address
	 */
	protected List apnList;
	/**
	 * The list of holds for this address
	 */
	protected List holdList;
	/**
	 * The count of hard holds on this address
	 */
	protected int hardHoldCount;
	/**
	 * The count of soft holds on this address
	 */
	protected int softHoldCount;
	/**
	 * The count of warns on this address
	 */
	protected int warnHoldCount;
	/**
	 * The count of conditions on this address
	 */
	protected int conditionCount;

	/**
	 * The list of structures on this address
	 */
	protected StructureEdit[] structureList;

	/**
	 * Constructor for AddressSummary.
	 */
	public AddressSummary() {

	}

	/**
	 * Returns the addressRangeList.
	 * 
	 * @return List
	 */
	public List getAddressRangeList() {
		return addressRangeList;
	}

	/**
	 * Returns the apnList.
	 * 
	 * @return List
	 */
	public List getApnList() {
		return apnList;
	}

	/**
	 * Returns the conditionCount.
	 * 
	 * @return int
	 */
	public int getConditionCount() {
		return conditionCount;
	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the holdList.
	 * 
	 * @return List
	 */
	public List getHoldList() {
		return holdList;
	}

	/**
	 * Returns the lsoId.
	 * 
	 * @return int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Returns the projectList.
	 * 
	 * @return List
	 */
	public List getProjectList() {
		return projectList;
	}

	/**
	 * Returns the useList.
	 * 
	 * @return List
	 */
	public List getUseList() {
		return useList;
	}

	/**
	 * Returns the zoneList.
	 * 
	 * @return List
	 */
	public List getZoneList() {
		return zoneList;
	}

	/**
	 * Sets the addressRangeList.
	 * 
	 * @param addressRangeList
	 *            The addressRangeList to set
	 */
	public void setAddressRangeList(List addressRangeList) {
		this.addressRangeList = addressRangeList;
	}

	/**
	 * Sets the apnList.
	 * 
	 * @param apnList
	 *            The apnList to set
	 */
	public void setApnList(List apnList) {
		this.apnList = apnList;
	}

	/**
	 * Sets the conditionCount.
	 * 
	 * @param conditionCount
	 *            The conditionCount to set
	 */
	public void setConditionCount(int conditionCount) {
		this.conditionCount = conditionCount;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the holdList.
	 * 
	 * @param holdList
	 *            The holdList to set
	 */
	public void setHoldList(List holdList) {
		this.holdList = holdList;
	}

	/**
	 * Sets the lsoId.
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Sets the projectList.
	 * 
	 * @param projectList
	 *            The projectList to set
	 */
	public void setProjectList(List projectList) {
		this.projectList = projectList;
	}

	/**
	 * Sets the useList.
	 * 
	 * @param useList
	 *            The useList to set
	 */
	public void setUseList(List useList) {
		this.useList = useList;
	}

	/**
	 * Sets the zoneList.
	 * 
	 * @param zoneList
	 *            The zoneList to set
	 */
	public void setZoneList(List zoneList) {
		this.zoneList = zoneList;
	}

	/**
	 * Returns the hardHoldCount.
	 * 
	 * @return int
	 */
	public int getHardHoldCount() {
		return hardHoldCount;
	}

	/**
	 * Returns the softHoldCount.
	 * 
	 * @return int
	 */
	public int getSoftHoldCount() {
		return softHoldCount;
	}

	/**
	 * Returns the warnHoldCount.
	 * 
	 * @return int
	 */
	public int getWarnHoldCount() {
		return warnHoldCount;
	}

	/**
	 * Sets the hardHoldCount.
	 * 
	 * @param hardHoldCount
	 *            The hardHoldCount to set
	 */
	public void setHardHoldCount(int hardHoldCount) {
		this.hardHoldCount = hardHoldCount;
	}

	/**
	 * Sets the softHoldCount.
	 * 
	 * @param softHoldCount
	 *            The softHoldCount to set
	 */
	public void setSoftHoldCount(int softHoldCount) {
		this.softHoldCount = softHoldCount;
	}

	/**
	 * Sets the warnHoldCount.
	 * 
	 * @param warnHoldCount
	 *            The warnHoldCount to set
	 */
	public void setWarnHoldCount(int warnHoldCount) {
		this.warnHoldCount = warnHoldCount;
	}

	/**
	 * Returns the ownerList.
	 * 
	 * @return List
	 */
	public List getOwnerList() {
		return ownerList;
	}

	/**
	 * Sets the ownerList.
	 * 
	 * @param ownerList
	 *            The ownerList to set
	 */
	public void setOwnerList(List ownerList) {
		this.ownerList = ownerList;
	}

	/**
	 * Returns the structureList.
	 * 
	 * @return StructureEdit[]
	 */
	public StructureEdit[] getStructureList() {
		return structureList;
	}

	/**
	 * Sets the structureList.
	 * 
	 * @param structureList
	 *            The structureList to set
	 */
	public void setStructureList(StructureEdit[] structureList) {
		this.structureList = structureList;
	}

	/**
	 * sets the structure list
	 * 
	 * @param structureList
	 */
	public void setStructureList(List structureList) {

		if (structureList == null)
			this.structureList = new StructureEdit[0];
		else {
			int count = structureList.size();
			StructureEdit[] structureEdit = new StructureEdit[count];
			Iterator iter = structureList.iterator();
			count = 0;
			while (iter.hasNext()) {
				structureEdit[count] = (StructureEdit) iter.next();
				count++;
			}
			this.structureList = structureEdit;
		}

	}
}

package elms.app.lso;

import java.util.List;

/**
 * The land object that represents a piece of land in the actual system
 * 
 * @author Anand Belaguly (anand@edgesoftinc.com) last updated : Dec 11, 2003 - Anand Belaguly - updated the file with comments and formatting
 */
public class Land extends Lso {

	/**
	 * The land details
	 */
	public LandDetails landDetails;
	/**
	 * The land address
	 */
	public List landAddress;
	/**
	 * The land APN
	 */
	public List landApn;
	/**
	 * The land site data.
	 */
	public LandSiteData landSiteData;

	public Land(int landId, LandDetails landDetails, List landAddress, List landApn, List landHold,List landTenant, List landCondition, List landAttachment, LandSiteData landSiteData, List landComment) {
		super(landId, landHold, landTenant, landCondition, landAttachment, landComment);
		this.landDetails = landDetails;
		this.landAddress = landAddress;
		this.landApn = landApn;
		this.landSiteData = landSiteData;
	}
	
	/**
	 * The loaded constructor
	 * 
	 * @param landId
	 * @param landDetails
	 * @param landAddress
	 * @param landApn
	 * @param landHold
	 * @param landCondition
	 * @param landAttachment
	 * @param landSiteData
	 * @param landComment
	 */
	public Land(int landId, LandDetails landDetails, List landAddress, List landApn, List landHold, List landCondition, List landAttachment, LandSiteData landSiteData, List landComment) {
		super(landId, landHold, landCondition, landAttachment, landComment);
		this.landDetails = landDetails;
		this.landAddress = landAddress;
		this.landApn = landApn;
		this.landSiteData = landSiteData;
	}

	/**
	 * The mini land constructor
	 * 
	 * @param landId
	 * @param landDetails
	 * @param landAddress
	 * @param landApn
	 */
	public Land(int landId, LandDetails landDetails, List landAddress, List landApn) {
		super(landId);
		this.landDetails = landDetails;
		this.landAddress = landAddress;
		this.landApn = landApn;
	}

	/**
	 * The default constructor
	 */
	public Land() {
		super();
	}

	/**
	 * Gets the landDetails
	 * 
	 * @return Returns a LandDetails
	 */
	public LandDetails getLandDetails() {
		return landDetails;
	}

	/**
	 * Sets the landDetails
	 * 
	 * @param landDetails
	 *            The landDetails to set
	 */
	public void setLandDetails(LandDetails landDetails) {
		this.landDetails = landDetails;
	}

	/**
	 * Gets the landAddress
	 * 
	 * @return Returns a List
	 */
	public List getLandAddress() {
		return landAddress;
	}

	/**
	 * Sets the landAddress
	 * 
	 * @param landAddress
	 *            The landAddress to set
	 */
	public void setLandAddress(List landAddress) {
		this.landAddress = landAddress;
	}

	/**
	 * Gets the landApn
	 * 
	 * @return Returns a List
	 */
	public List getLandApn() {
		return landApn;
	}

	/**
	 * Sets the landApn
	 * 
	 * @param landApn
	 *            The landApn to set
	 */
	public void setLandApn(List landApn) {
		this.landApn = landApn;
	}

	/**
	 * Gets the landSiteData
	 * 
	 * @return Returns a LandSiteData
	 */
	public LandSiteData getLandSiteData() {
		return landSiteData;
	}

	/**
	 * Sets the landSiteData
	 * 
	 * @param landSiteData
	 *            The landSiteData to set
	 */
	public void setLandSiteData(LandSiteData landSiteData) {
		this.landSiteData = landSiteData;
	}
}

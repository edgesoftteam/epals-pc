//Source file: C:\\datafile\\source\\elms\\app\\lso\\Use.java

package elms.app.lso;

/**
 * @author Shekhar Jain
 */
public class Use {
	protected int id;
	protected String type;
	protected String description;

	/**
	 * @param type
	 * @param code
	 * @param description
	 *            3CD0308A02A3
	 */
	public Use(int id, String type, String description) {
		this.id = id;
		this.type = type;
		this.description = description;
	}

	public Use(int id, String description) {
		this.id = id;
		this.description = description;
	}

	/**
	 * 3CD0308301D0
	 */
	public Use() {

	}

	/**
	 * Gets the id
	 * 
	 * @return Returns a String
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id
	 * 
	 * @param id
	 *            The id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Access method for the type property.
	 * 
	 * @return the current value of the type property
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param aType
	 *            the new value of the type property
	 */
	public void setType(String aType) {
		type = aType;
	}

	/**
	 * Access method for the description property.
	 * 
	 * @return the current value of the description property
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the value of the description property.
	 * 
	 * @param aDescription
	 *            the new value of the description property
	 */
	public void setDescription(String aDescription) {
		description = aDescription;
	}

	public String toString() {
		return ("Use Object " + id + "--" + type + "--" + description);
	}

}

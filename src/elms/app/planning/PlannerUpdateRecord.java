package elms.app.planning;

import org.apache.log4j.Logger;

public class PlannerUpdateRecord {

	static Logger logger;
	public String subProjectNumber;
	public String activityNumber;
	public String activityId;
	public String subProjectType;
	public String subProjectStatus;
	public String subProjectDescription;
	public String address;
	public String appliedDate;
	public String appliedDateLabel;
	public String userName;
	public String check;
	public String subProjectName;
	public String subProjectId;
	public String fromUserId;
	public String active;

	static {
		logger = Logger.getLogger(elms.app.planning.PlannerUpdateRecord.class.getName());
	}

	public PlannerUpdateRecord() {
	}

	public PlannerUpdateRecord(String subProjectNumber, String activityNumber, String activityId, String subProjectType, String subProjectStatus, String subProjectDescription, String address, String appliedDate, String check, String subProjectName, String subProjectId, String fromUserId, String active) {
		this.subProjectNumber = subProjectNumber;
		this.activityNumber = activityNumber;
		this.activityId = activityId;
		this.subProjectType = subProjectType;
		this.subProjectStatus = subProjectStatus;
		this.subProjectDescription = subProjectDescription;
		this.address = address;
		this.appliedDate = appliedDate;
		this.check = check;
		this.subProjectName = subProjectName;
		this.subProjectId = subProjectId;
		this.fromUserId = fromUserId;
		this.active = active;
	}

	public String getActivityNumber() {
		return activityNumber;
	}

	public String getAddress() {
		return address;
	}

	public String getSubProjectDescription() {
		return subProjectDescription;
	}

	public String getSubProjectNumber() {
		return subProjectNumber;
	}

	public String getSubProjectStatus() {
		return subProjectStatus;
	}

	public String getSubProjectType() {
		return subProjectType;
	}

	public void setActivityNumber(String string) {
		activityNumber = string;
	}

	public void setAddress(String string) {
		address = string;
	}

	public void setSubProjectDescription(String string) {
		subProjectDescription = string;
	}

	public void setSubProjectNumber(String string) {
		subProjectNumber = string;
	}

	public void setSubProjectStatus(String string) {
		subProjectStatus = string;
	}

	public void setSubProjectType(String string) {
		subProjectType = string;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String string) {
		userName = string;
	}

	public String getCheck() {
		return check;
	}

	public void setCheck(String string) {
		check = string;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String string) {
		activityId = string;
	}

	public void setAppliedDateLabel(String string) {
		appliedDateLabel = string;
	}

	public String getAppliedDate() {
		return appliedDate;
	}

	public void setAppliedDate(String string) {
		appliedDate = string;
	}

	public String getSubProjectId() {
		return subProjectId;
	}

	public String getSubProjectName() {
		return subProjectName;
	}

	public void setSubProjectId(String string) {
		subProjectId = string;
	}

	public void setSubProjectName(String string) {
		subProjectName = string;
	}

	public String getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(String string) {
		fromUserId = string;
	}

	/**
	 * @return the active
	 */
	public String getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(String active) {
		this.active = active;
	}

}

package elms.app.planning;

/**
 * @author Shekhar
 */
public class StatusEdit {
	private String update; // To track whether this record has been updated
	private String deleteFlag; // If checked this record has to be deleted
	private String updateActivities;
	private String updateSubProject;
	private String statusId; // Primary key to the status table, for update and delete
	private String planStatusId;
	private String statusDate;
	private String status; // Status Description
	private String comment;
	private String added;

	public StatusEdit() {
		this.statusId = "0";
		this.update = "";
		this.deleteFlag = "";
		this.added = "";
	}

	/**
	 * Returns the comment.
	 * 
	 * @return String
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Returns the deleteFlag.
	 * 
	 * @return String
	 */
	public String getDeleteFlag() {
		return deleteFlag;
	}

	/**
	 * Returns the status.
	 * 
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Returns the statusDate.
	 * 
	 * @return String
	 */
	public String getStatusDate() {
		return statusDate;
	}

	/**
	 * Returns the statusId.
	 * 
	 * @return String
	 */
	public String getStatusId() {
		return statusId;
	}

	/**
	 * Returns the update.
	 * 
	 * @return String
	 */
	public String getUpdate() {
		return update;
	}

	/**
	 * Sets the comment.
	 * 
	 * @param comment
	 *            The comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Sets the deleteFlag.
	 * 
	 * @param deleteFlag
	 *            The deleteFlag to set
	 */
	public void setDeleteFlag(String deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the statusDate.
	 * 
	 * @param statusDate
	 *            The statusDate to set
	 */
	public void setStatusDate(String statusDate) {
		this.statusDate = statusDate;
	}

	/**
	 * Sets the statusId.
	 * 
	 * @param statusId
	 *            The statusId to set
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	/**
	 * Sets the update.
	 * 
	 * @param update
	 *            The update to set
	 */
	public void setUpdate(String update) {
		this.update = update;
	}

	/**
	 * Returns the updateActivities.
	 * 
	 * @return String
	 */
	public String getUpdateActivities() {
		return updateActivities;
	}

	/**
	 * Returns the updateSubProject.
	 * 
	 * @return String
	 */
	public String getUpdateSubProject() {
		return updateSubProject;
	}

	/**
	 * Sets the updateActivities.
	 * 
	 * @param updateActivities
	 *            The updateActivities to set
	 */
	public void setUpdateActivities(String updateActivities) {
		this.updateActivities = updateActivities;
	}

	/**
	 * Sets the updateSubProject.
	 * 
	 * @param updateSubProject
	 *            The updateSubProject to set
	 */
	public void setUpdateSubProject(String updateSubProject) {
		this.updateSubProject = updateSubProject;
	}

	/**
	 * @param string
	 */
	public void setAdded(String added) {
		if (added != null)
			this.added = added;
	}

	/**
	 * @return
	 */
	public String getAdded() {
		return added;
	}

	/**
	 * @return
	 */
	public String getPlanStatusId() {
		return planStatusId;
	}

	/**
	 * @param string
	 */
	public void setPlanStatusId(String string) {
		planStatusId = string;
	}

}

package elms.app.planning;

import java.util.ArrayList;
import java.util.List;

public class PlanningDetails {
	protected List statusList;
	protected List resolutionList;
	protected List envReviewList;
	protected List linkedSubProjects;

	public PlanningDetails() {
		this.statusList = new ArrayList();
		this.resolutionList = new ArrayList();
		this.envReviewList = new ArrayList();

	}

	/**
	 * @return
	 */
	public List getEnvReviewList() {
		return envReviewList;
	}

	/**
	 * @return
	 */
	public List getResolutionList() {
		return resolutionList;
	}

	/**
	 * @return
	 */
	public List getStatusList() {
		return statusList;
	}

	/**
	 * @param list
	 */
	public void setEnvReviewList(List list) {
		envReviewList = list;
	}

	/**
	 * @param list
	 */
	public void setResolutionList(List list) {
		resolutionList = list;
	}

	/**
	 * @param list
	 */
	public void setStatusList(List list) {
		statusList = list;
	}

	/**
	 * @return
	 */
	public List getLinkedSubProjects() {
		return linkedSubProjects;
	}

	/**
	 * @param list
	 */
	public void setLinkedSubProjects(List list) {
		linkedSubProjects = list;
	}

}
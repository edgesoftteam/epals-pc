package elms.app.planning;

import java.util.ArrayList;
import java.util.List;

// Referenced classes of package elms.app.planning:
//            PlannerUpdateRecord

public class Planner {

	public Planner() {
		activityDetailsList = new ArrayList();
		plannerUpdateRecord = new PlannerUpdateRecord[0];
	}

	public List getActivityDetailsList() {
		return activityDetailsList;
	}

	public String getUserName() {
		return userName;
	}

	public void setActivityDetailsList(List list) {
		activityDetailsList = list;
	}

	public void setUserName(String string) {
		userName = string;
	}

	public PlannerUpdateRecord[] getPlannerUpdateRecord() {
		return plannerUpdateRecord;
	}

	public void setPlannerUpdateRecord(PlannerUpdateRecord records[]) {
		plannerUpdateRecord = records;
	}

	public String getSelectAllCheckBox() {
		return selectAllCheckBox;
	}

	public void setSelectAllCheckBox(String string) {
		selectAllCheckBox = string;
	}

	public String userName;
	public List activityDetailsList;
	public PlannerUpdateRecord plannerUpdateRecord[];
	public String selectAllCheckBox;
}

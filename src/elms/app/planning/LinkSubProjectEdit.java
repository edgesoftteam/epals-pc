package elms.app.planning;

/**
 * @author Shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */
public class LinkSubProjectEdit {

	private String linked;
	private String subProjectId;
	private String subProjectNbr;
	private String description;
	private String status;
	private String subProjectName;
	private String projectName;

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the linked.
	 * 
	 * @return String
	 */
	public String getLinked() {
		return linked;
	}

	/**
	 * Returns the status.
	 * 
	 * @return String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Returns the subProjectId.
	 * 
	 * @return String
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * Returns the subProjectNbr.
	 * 
	 * @return String
	 */
	public String getSubProjectNbr() {
		return subProjectNbr;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the linked.
	 * 
	 * @param linked
	 *            The linked to set
	 */
	public void setLinked(String linked) {
		this.linked = linked;
	}

	/**
	 * Sets the status.
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the subProjectId.
	 * 
	 * @param subProjectId
	 *            The subProjectId to set
	 */
	public void setSubProjectId(String subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * Sets the subProjectNbr.
	 * 
	 * @param subProjectNbr
	 *            The subProjectNbr to set
	 */
	public void setSubProjectNbr(String subProjectNbr) {
		this.subProjectNbr = subProjectNbr;
	}

	/**
	 * @return
	 */
	public String getSubProjectName() {
		return subProjectName;
	}

	/**
	 * @param string
	 */
	public void setSubProjectName(String string) {
		subProjectName = string;
	}

	/**
	 * @return
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param string
	 */
	public void setProjectName(String string) {
		projectName = string;
	}

}

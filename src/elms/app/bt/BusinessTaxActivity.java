/*
 * Created on Nov 24, 2008
 *
 */
package elms.app.bt;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;

import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivitySubType;
import elms.app.admin.ActivityType;
import elms.app.bl.ApplicationType;
import elms.app.bl.OwnershipType;
import elms.app.bl.QuantityType;
import elms.app.common.MultiAddress;
import elms.app.lso.Street;
import elms.util.StringUtils;

/**
 * @author Gayathri
 * 
 */
public class BusinessTaxActivity {
	static Logger logger = Logger.getLogger(BusinessTaxActivity.class.getName());

	protected String activityId;
	protected boolean businessLocation;
	protected ApplicationType applicationType;
	protected Street street;
	protected String address;
	protected String businessAddressStreetNumber;
	protected String businessAddressStreetFraction;
	protected String businessAddressStreetName;
	protected String businessAddressUnitNumber;
	protected String businessAddressCity;
	protected String businessAddressState;
	protected String businessAddressZip;
	protected String businessAddressZip4;
	protected String outOfTownStreetNumber;
	protected String outOfTownStreetName;
	protected String outOfTownUnitNumber;
	protected String outOfTownCity;
	protected String outOfTownState;
	protected String outOfTownZip;
	protected String outOfTownZip4;
	protected String activityNumber;
	protected ActivityStatus activityStatus;
	protected Calendar creationDate;
	protected String businessName;
	protected String businessAccountNumber;
	protected String corporateName;
	protected String attn;
	protected String businessPhone;
	protected String businessExtension;
	protected String businessFax;
	protected ActivityType activityType;
	protected ActivitySubType activitySubType;
	protected String sicCode;
	protected String muncipalCode;
	protected String descOfBusiness;
	protected String classCode;
	protected boolean homeOccupation;
	protected boolean decalCode;
	protected boolean otherBusinessOccupancy;
	protected Calendar applicationDate;
	protected String applicationDateString;
	protected Calendar issueDate;
	protected Calendar startingDate;
	protected Calendar outOfBusinessDate;
	protected OwnershipType ownershipType;
	protected String federalIdNumber;
	protected String emailAddress;
	protected String socialSecurityNumber;
	protected String mailStreetNumber;
	protected String mailStreetName;
	protected String mailUnitNumber;
	protected String mailCity;
	protected String mailState;
	protected String mailZip;
	protected String mailZip4;
	protected String prevStreetNumber;
	protected String prevStreetName;
	protected String prevUnitNumber;
	protected String prevCity;
	protected String prevState;
	protected String prevZip;
	protected String prevZip4;
	protected String name1;
	protected String title1;
	protected String owner1StreetNumber;
	protected String owner1StreetName;
	protected String owner1UnitNumber;
	protected String owner1City;
	protected String owner1State;
	protected String owner1Zip;
	protected String owner1Zip4;
	protected String name2;
	protected String title2;
	protected String owner2StreetNumber;
	protected String owner2StreetName;
	protected String owner2UnitNumber;
	protected String owner2City;
	protected String owner2State;
	protected String owner2Zip;
	protected String owner2Zip4;
	protected String squareFootage;
	protected QuantityType quantity;
	protected String quantityNum;
	protected String driverLicense;
	protected int subProjectId;
	protected String copyBusinessTaxActivity = "N";
	protected String displayContent;
	protected int createdBy;
	protected int updatedBy;
	protected int departmentId;// to be used only for BL approval
	protected int userId;// approver id , to be used only for BL approval
	protected String departmentName;
	protected String userName;
	protected String approvalStatus;
	protected String activityStatusDescription;
	protected int approvalId;
	protected List businessLicenseApprovals;
	protected String departmentCheck;
	protected String mailAttn;
	protected String preAttn;
	protected String owner1Attn;
	protected String owner2Attn;
	protected MultiAddress[] multiAddress;
	protected String renewalOnline;
	
	public String getMailingAddress() {
		return StringUtils.nullReplaceWithEmpty(mailStreetNumber) + " " + StringUtils.nullReplaceWithEmpty(mailStreetName) + " " + StringUtils.nullReplaceWithEmpty(mailUnitNumber);
	}

	/**
	 * @return Returns the businessLicenseApprovals.
	 */
	public List getBusinessLicenseApprovals() {
		return businessLicenseApprovals;
	}

	/**
	 * @param businessLicenseApprovals
	 *            The businessLicenseApprovals to set.
	 */
	public void setBusinessLicenseApprovals(List businessLicenseApprovals) {
		this.businessLicenseApprovals = businessLicenseApprovals;
	}

	/**
	 * @return Returns the approvalId.
	 */
	public int getApprovalId() {
		return approvalId;
	}

	/**
	 * @param approvalId
	 *            The approvalId to set.
	 */
	public void setApprovalId(int approvalId) {
		this.approvalId = approvalId;
	}

	/**
	 * @return Returns the departmentName.
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * @param departmentName
	 *            The departmentName to set.
	 */
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	/**
	 * @return Returns the userName.
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            The userName to set.
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address
	 *            The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return Returns the activityId.
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            The activityId to set.
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return Returns the createdBy.
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy
	 *            The createdBy to set.
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the updatedBy.
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy
	 *            The updatedBy to set.
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return Returns the departmentCheck.
	 */
	public String getDepartmentCheck() {
		return departmentCheck;
	}

	/**
	 * @param departmentCheck
	 *            The departmentCheck to set.
	 */
	public void setDepartmentCheck(String departmentCheck) {
		this.departmentCheck = departmentCheck;
	}

	/**
	 * @return Returns the businessAddressCity.
	 */
	public String getBusinessAddressCity() {
		return businessAddressCity;
	}

	/**
	 * @param businessAddressCity
	 *            The businessAddressCity to set.
	 */
	public void setBusinessAddressCity(String businessAddressCity) {
		this.businessAddressCity = businessAddressCity;
	}

	/**
	 * @return Returns the businessAddressState.
	 */
	public String getBusinessAddressState() {
		return businessAddressState;
	}

	/**
	 * @param businessAddressState
	 *            The businessAddressState to set.
	 */
	public void setBusinessAddressState(String businessAddressState) {
		this.businessAddressState = businessAddressState;
	}

	/**
	 * @return Returns the businessAddressStreetFraction.
	 */
	public String getBusinessAddressStreetFraction() {
		return businessAddressStreetFraction;
	}

	/**
	 * @param businessAddressStreetFraction
	 *            The businessAddressStreetFraction to set.
	 */
	public void setBusinessAddressStreetFraction(String businessAddressStreetFraction) {
		this.businessAddressStreetFraction = businessAddressStreetFraction;
	}

	/**
	 * @return Returns the businessAddressStreetName.
	 */
	public String getBusinessAddressStreetName() {
		return businessAddressStreetName;
	}

	/**
	 * @param businessAddressStreetName
	 *            The businessAddressStreetName to set.
	 */
	public void setBusinessAddressStreetName(String businessAddressStreetName) {
		this.businessAddressStreetName = businessAddressStreetName;
	}

	/**
	 * @return Returns the businessAddressStreetNumber.
	 */
	public String getBusinessAddressStreetNumber() {
		return businessAddressStreetNumber;
	}

	/**
	 * @param businessAddressStreetNumber
	 *            The businessAddressStreetNumber to set.
	 */
	public void setBusinessAddressStreetNumber(String businessAddressStreetNumber) {
		this.businessAddressStreetNumber = businessAddressStreetNumber;
	}

	/**
	 * @return Returns the businessAddressUnitNumber.
	 */
	public String getBusinessAddressUnitNumber() {
		return businessAddressUnitNumber;
	}

	/**
	 * @param businessAddressUnitNumber
	 *            The businessAddressUnitNumber to set.
	 */
	public void setBusinessAddressUnitNumber(String businessAddressUnitNumber) {
		this.businessAddressUnitNumber = businessAddressUnitNumber;
	}

	/**
	 * @return Returns the businessAddressZip.
	 */
	public String getBusinessAddressZip() {
		return businessAddressZip;
	}

	/**
	 * @param businessAddressZip
	 *            The businessAddressZip to set.
	 */
	public void setBusinessAddressZip(String businessAddressZip) {
		this.businessAddressZip = businessAddressZip;
	}

	/**
	 * @return Returns the businessAddressZip4.
	 */
	public String getBusinessAddressZip4() {
		return businessAddressZip4;
	}

	/**
	 * @param businessAddressZip4
	 *            The businessAddressZip4 to set.
	 */
	public void setBusinessAddressZip4(String businessAddressZip4) {
		this.businessAddressZip4 = businessAddressZip4;
	}

	/**
	 * @return Returns the logger.
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger
	 *            The logger to set.
	 */
	public static void setLogger(Logger logger) {
		BusinessTaxActivity.logger = logger;
	}

	/**
	 * @return Returns the activityNumber.
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * @param activityNumber
	 *            The activityNumber to set.
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * @return Returns the activityStatus.
	 */
	public ActivityStatus getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            The activityStatus to set.
	 */
	public void setActivityStatus(ActivityStatus activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return Returns the activitySubType.
	 */
	public ActivitySubType getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param activitySubType
	 *            The activitySubType to set.
	 */
	public void setActivitySubType(ActivitySubType activitySubType) {
		this.activitySubType = activitySubType;
	}

	/**
	 * @return Returns the activityType.
	 */
	public ActivityType getActivityType() {

		return activityType;
	}

	/**
	 * @param activityType
	 *            The activityType to set.
	 */
	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;

	}

	/**
	 * @return Returns the departmentId.
	 */
	public int getDepartmentId() {
		return departmentId;
	}

	/**
	 * @param departmentId
	 *            The departmentId to set.
	 */
	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	/**
	 * @return Returns the userId.
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @param applicationDateString
	 *            The applicationDateString to set.
	 */
	public void setApplicationDateString(String applicationDateString) {
		this.applicationDateString = applicationDateString;
	}

	/**
	 * @return Returns the applicationDate.
	 */
	public Calendar getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate
	 *            The applicationDate to set.
	 */
	public void setApplicationDate(Calendar applicationDate) {
		this.applicationDate = applicationDate;
	}

	/**
	 * @return Returns the applicationType.
	 */
	public ApplicationType getApplicationType() {
		return applicationType;
	}

	/**
	 * @param applicationType
	 *            The applicationType to set.
	 */
	public void setApplicationType(ApplicationType applicationType) {
		this.applicationType = applicationType;
	}

	/**
	 * @return Returns the businessExtension.
	 */
	public String getBusinessExtension() {
		return businessExtension;
	}

	/**
	 * @param businessExtension
	 *            The businessExtension to set.
	 */
	public void setBusinessExtension(String businessExtension) {
		this.businessExtension = businessExtension;
	}

	/**
	 * @return Returns the businessFax.
	 */
	public String getBusinessFax() {
		return businessFax;
	}

	/**
	 * @param businessFax
	 *            The businessFax to set.
	 */
	public void setBusinessFax(String businessFax) {
		this.businessFax = businessFax;
	}

	/**
	 * @return Returns the businessLocation.
	 */
	public boolean isBusinessLocation() {
		return businessLocation;
	}

	/**
	 * @param businessLocation
	 *            The businessLocation to set.
	 */
	public void setBusinessLocation(boolean businessLocation) {
		this.businessLocation = businessLocation;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the businessAccountNumber.
	 */
	public String getBusinessAccountNumber() {
		return businessAccountNumber;
	}

	/**
	 * @param businessAccountNumber
	 *            The businessAccountNumber to set.
	 */
	public void setBusinessAccountNumber(String businessAccountNumber) {
		this.businessAccountNumber = businessAccountNumber;
	}

	/**
	 * @return Returns the businessPhone.
	 */
	public String getBusinessPhone() {
		return businessPhone;
	}

	/**
	 * @param businessPhone
	 *            The businessPhone to set.
	 */
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	/**
	 * @return Returns the classCode.
	 */
	public String getClassCode() {
		return classCode;
	}

	/**
	 * @param classCode
	 *            The classCode to set.
	 */
	public void setClassCode(String classCode) {
		this.classCode = classCode;
	}

	/**
	 * @return Returns the corporateName.
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * @param corporateName
	 *            The corporateName to set.
	 */
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	/**
	 * @return Returns the attn.
	 */
	public String getAttn() {
		return attn;
	}

	/**
	 * @param attn
	 *            The attn to set.
	 */
	public void setAttn(String attn) {
		this.attn = attn;
	}

	/**
	 * @return Returns the creationDate.
	 */
	public Calendar getCreationDate() {
		return creationDate;
	}

	/**
	 * @param creationDate
	 *            The creationDate to set.
	 */
	public void setCreationDate(Calendar creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * @return Returns the decalCode.
	 */
	public boolean isDecalCode() {
		return decalCode;
	}

	/**
	 * @param decalCode
	 *            The decalCode to set.
	 */
	public void setDecalCode(boolean decalCode) {
		this.decalCode = decalCode;
	}

	/**
	 * @return Returns the descOfBusiness.
	 */
	public String getDescOfBusiness() {
		return descOfBusiness;
	}

	/**
	 * @param descOfBusiness
	 *            The descOfBusiness to set.
	 */
	public void setDescOfBusiness(String descOfBusiness) {
		this.descOfBusiness = descOfBusiness;
	}

	/**
	 * @return Returns the emailAddress.
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            The emailAddress to set.
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * @return Returns the federalIdNumber.
	 */
	public String getFederalIdNumber() {
		return federalIdNumber;
	}

	/**
	 * @param federalIdNumber
	 *            The federalIdNumber to set.
	 */
	public void setFederalIdNumber(String federalIdNumber) {
		this.federalIdNumber = federalIdNumber;
	}

	/**
	 * @return Returns the homeOccupation.
	 */
	public boolean isHomeOccupation() {
		return homeOccupation;
	}

	/**
	 * @param homeOccupation
	 *            The homeOccupation to set.
	 */
	public void setHomeOccupation(boolean homeOccupation) {
		this.homeOccupation = homeOccupation;
	}

	/**
	 * @return Returns the issueDate.
	 */
	public Calendar getIssueDate() {
		return issueDate;
	}

	/**
	 * @param issueDate
	 *            The issueDate to set.
	 */
	public void setIssueDate(Calendar issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * @return Returns the mailCity.
	 */
	public String getMailCity() {
		return mailCity;
	}

	/**
	 * @param mailCity
	 *            The mailCity to set.
	 */
	public void setMailCity(String mailCity) {
		this.mailCity = mailCity;
	}

	/**
	 * @return Returns the mailState.
	 */
	public String getMailState() {
		return mailState;
	}

	/**
	 * @param mailState
	 *            The mailState to set.
	 */
	public void setMailState(String mailState) {
		this.mailState = mailState;
	}

	/**
	 * @return Returns the mailStreetName.
	 */
	public String getMailStreetName() {
		return mailStreetName;
	}

	/**
	 * @param mailStreetName
	 *            The mailStreetName to set.
	 */
	public void setMailStreetName(String mailStreetName) {
		this.mailStreetName = mailStreetName;
	}

	/**
	 * @return Returns the name1.
	 */
	public String getName1() {
		return name1;
	}

	/**
	 * @param name1
	 *            The name1 to set.
	 */
	public void setName1(String name1) {
		this.name1 = name1;
	}

	/**
	 * @return Returns the name2.
	 */
	public String getName2() {
		return name2;
	}

	/**
	 * @param name2
	 *            The name2 to set.
	 */
	public void setName2(String name2) {
		this.name2 = name2;
	}

	/**
	 * @return Returns the owner1City.
	 */
	public String getOwner1City() {
		return owner1City;
	}

	/**
	 * @param owner1City
	 *            The owner1City to set.
	 */
	public void setOwner1City(String owner1City) {
		this.owner1City = owner1City;
	}

	/**
	 * @return Returns the owner1State.
	 */
	public String getOwner1State() {
		return owner1State;
	}

	/**
	 * @param owner1State
	 *            The owner1State to set.
	 */
	public void setOwner1State(String owner1State) {
		this.owner1State = owner1State;
	}

	/**
	 * @return Returns the owner1StreetName.
	 */
	public String getOwner1StreetName() {
		return owner1StreetName;
	}

	/**
	 * @param owner1StreetName
	 *            The owner1StreetName to set.
	 */
	public void setOwner1StreetName(String owner1StreetName) {
		this.owner1StreetName = owner1StreetName;
	}

	/**
	 * @return Returns the owner1StreetNumber.
	 */
	public String getOwner1StreetNumber() {
		return owner1StreetNumber;
	}

	/**
	 * @param owner1StreetNumber
	 *            The owner1StreetNumber to set.
	 */
	public void setOwner1StreetNumber(String owner1StreetNumber) {
		this.owner1StreetNumber = owner1StreetNumber;
	}

	/**
	 * @return Returns the owner1UnitNumber.
	 */
	public String getOwner1UnitNumber() {
		return owner1UnitNumber;
	}

	/**
	 * @param owner1UnitNumber
	 *            The owner1UnitNumber to set.
	 */
	public void setOwner1UnitNumber(String owner1UnitNumber) {
		this.owner1UnitNumber = owner1UnitNumber;
	}

	/**
	 * @return Returns the owner1Zip.
	 */
	public String getOwner1Zip() {
		return owner1Zip;
	}

	/**
	 * @param owner1Zip
	 *            The owner1Zip to set.
	 */
	public void setOwner1Zip(String owner1Zip) {
		this.owner1Zip = owner1Zip;
	}

	/**
	 * @return Returns the owner1Zip4.
	 */
	public String getOwner1Zip4() {
		return owner1Zip4;
	}

	/**
	 * @param owner1Zip4
	 *            The owner1Zip4 to set.
	 */
	public void setOwner1Zip4(String owner1Zip4) {
		this.owner1Zip4 = owner1Zip4;
	}

	/**
	 * @return Returns the owner2City.
	 */
	public String getOwner2City() {
		return owner2City;
	}

	/**
	 * @param owner2City
	 *            The owner2City to set.
	 */
	public void setOwner2City(String owner2City) {
		this.owner2City = owner2City;
	}

	/**
	 * @return Returns the owner2State.
	 */
	public String getOwner2State() {
		return owner2State;
	}

	/**
	 * @param owner2State
	 *            The owner2State to set.
	 */
	public void setOwner2State(String owner2State) {
		this.owner2State = owner2State;
	}

	/**
	 * @return Returns the owner2StreetName.
	 */
	public String getOwner2StreetName() {
		return owner2StreetName;
	}

	/**
	 * @param owner2StreetName
	 *            The owner2StreetName to set.
	 */
	public void setOwner2StreetName(String owner2StreetName) {
		this.owner2StreetName = owner2StreetName;
	}

	/**
	 * @return Returns the owner2StreetNumber.
	 */
	public String getOwner2StreetNumber() {
		return owner2StreetNumber;
	}

	/**
	 * @param owner2StreetNumber
	 *            The owner2StreetNumber to set.
	 */
	public void setOwner2StreetNumber(String owner2StreetNumber) {
		this.owner2StreetNumber = owner2StreetNumber;
	}

	/**
	 * @return Returns the owner2UnitNumber.
	 */
	public String getOwner2UnitNumber() {
		return owner2UnitNumber;
	}

	/**
	 * @param owner2UnitNumber
	 *            The owner2UnitNumber to set.
	 */
	public void setOwner2UnitNumber(String owner2UnitNumber) {
		this.owner2UnitNumber = owner2UnitNumber;
	}

	/**
	 * @return Returns the owner2Zip.
	 */
	public String getOwner2Zip() {
		return owner2Zip;
	}

	/**
	 * @param owner2Zip
	 *            The owner2Zip to set.
	 */
	public void setOwner2Zip(String owner2Zip) {
		this.owner2Zip = owner2Zip;
	}

	/**
	 * @return Returns the owner2Zip4.
	 */
	public String getOwner2Zip4() {
		return owner2Zip4;
	}

	/**
	 * @param owner2Zip4
	 *            The owner2Zip4 to set.
	 */
	public void setOwner2Zip4(String owner2Zip4) {
		this.owner2Zip4 = owner2Zip4;
	}

	/**
	 * @return Returns the title1.
	 */
	public String getTitle1() {
		return title1;
	}

	/**
	 * @param title1
	 *            The title1 to set.
	 */
	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	/**
	 * @return Returns the title2.
	 */
	public String getTitle2() {
		return title2;
	}

	/**
	 * @param title2
	 *            The title2 to set.
	 */
	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	/**
	 * @return Returns the mailStreetNumber.
	 */
	public String getMailStreetNumber() {
		return mailStreetNumber;
	}

	/**
	 * @param mailStreetNumber
	 *            The mailStreetNumber to set.
	 */
	public void setMailStreetNumber(String mailStreetNumber) {
		this.mailStreetNumber = mailStreetNumber;
	}

	/**
	 * @return Returns the mailUnitNumber.
	 */
	public String getMailUnitNumber() {
		return mailUnitNumber;
	}

	/**
	 * @param mailUnitNumber
	 *            The mailUnitNumber to set.
	 */
	public void setMailUnitNumber(String mailUnitNumber) {
		this.mailUnitNumber = mailUnitNumber;
	}

	/**
	 * @return Returns the mailZip.
	 */
	public String getMailZip() {
		return mailZip;
	}

	/**
	 * @param mailZip
	 *            The mailZip to set.
	 */
	public void setMailZip(String mailZip) {
		this.mailZip = mailZip;
	}

	/**
	 * @return Returns the mailZip4.
	 */
	public String getMailZip4() {
		return mailZip4;
	}

	/**
	 * @param mailZip4
	 *            The mailZip4 to set.
	 */
	public void setMailZip4(String mailZip4) {
		this.mailZip4 = mailZip4;
	}

	/**
	 * @return Returns the muncipalCode.
	 */
	public String getMuncipalCode() {
		return muncipalCode;
	}

	/**
	 * @param muncipalCode
	 *            The muncipalCode to set.
	 */
	public void setMuncipalCode(String muncipalCode) {
		this.muncipalCode = muncipalCode;
	}

	/**
	 * @return Returns the outOfBusinessDate.
	 */
	public Calendar getOutOfBusinessDate() {
		return outOfBusinessDate;
	}

	/**
	 * @param outOfBusinessDate
	 *            The outOfBusinessDate to set.
	 */
	public void setOutOfBusinessDate(Calendar outOfBusinessDate) {
		this.outOfBusinessDate = outOfBusinessDate;
	}

	/**
	 * @return Returns the outOfTownCity.
	 */
	public String getOutOfTownCity() {
		return outOfTownCity;
	}

	/**
	 * @param outOfTownCity
	 *            The outOfTownCity to set.
	 */
	public void setOutOfTownCity(String outOfTownCity) {
		this.outOfTownCity = outOfTownCity;
	}

	/**
	 * @return Returns the outOfTownState.
	 */
	public String getOutOfTownState() {
		return outOfTownState;
	}

	/**
	 * @param outOfTownState
	 *            The outOfTownState to set.
	 */
	public void setOutOfTownState(String outOfTownState) {
		this.outOfTownState = outOfTownState;
	}

	/**
	 * @return Returns the outOfTownStreetName.
	 */
	public String getOutOfTownStreetName() {
		return outOfTownStreetName;
	}

	/**
	 * @param outOfTownStreetName
	 *            The outOfTownStreetName to set.
	 */
	public void setOutOfTownStreetName(String outOfTownStreetName) {
		this.outOfTownStreetName = outOfTownStreetName;
	}

	/**
	 * @return Returns the outOfTownStreetNumber.
	 */
	public String getOutOfTownStreetNumber() {
		return outOfTownStreetNumber;
	}

	/**
	 * @param outOfTownStreetNumber
	 *            The outOfTownStreetNumber to set.
	 */
	public void setOutOfTownStreetNumber(String outOfTownStreetNumber) {
		this.outOfTownStreetNumber = outOfTownStreetNumber;
	}

	/**
	 * @return Returns the outOfTownUnitNumber.
	 */
	public String getOutOfTownUnitNumber() {
		return outOfTownUnitNumber;
	}

	/**
	 * @param outOfTownUnitNumber
	 *            The outOfTownUnitNumber to set.
	 */
	public void setOutOfTownUnitNumber(String outOfTownUnitNumber) {
		this.outOfTownUnitNumber = outOfTownUnitNumber;
	}

	/**
	 * @return Returns the outOfTownZip.
	 */
	public String getOutOfTownZip() {
		return outOfTownZip;
	}

	/**
	 * @param outOfTownZip
	 *            The outOfTownZip to set.
	 */
	public void setOutOfTownZip(String outOfTownZip) {
		this.outOfTownZip = outOfTownZip;
	}

	/**
	 * @return Returns the outOfTownZip4.
	 */
	public String getOutOfTownZip4() {
		return outOfTownZip4;
	}

	/**
	 * @param outOfTownZip4
	 *            The outOfTownZip4 to set.
	 */
	public void setOutOfTownZip4(String outOfTownZip4) {
		this.outOfTownZip4 = outOfTownZip4;
	}

	/**
	 * @return Returns the ownershipType.
	 */
	public OwnershipType getOwnershipType() {
		return ownershipType;
	}

	/**
	 * @param ownershipType
	 *            The ownershipType to set.
	 */
	public void setOwnershipType(OwnershipType ownershipType) {
		this.ownershipType = ownershipType;
	}

	/**
	 * @return Returns the prevCity.
	 */
	public String getPrevCity() {
		return prevCity;
	}

	/**
	 * @param prevCity
	 *            The prevCity to set.
	 */
	public void setPrevCity(String prevCity) {
		this.prevCity = prevCity;
	}

	/**
	 * @return Returns the prevState.
	 */
	public String getPrevState() {
		return prevState;
	}

	/**
	 * @param prevState
	 *            The prevState to set.
	 */
	public void setPrevState(String prevState) {
		this.prevState = prevState;
	}

	/**
	 * @return Returns the prevStreetName.
	 */
	public String getPrevStreetName() {
		return prevStreetName;
	}

	/**
	 * @param prevStreetName
	 *            The prevStreetName to set.
	 */
	public void setPrevStreetName(String prevStreetName) {
		this.prevStreetName = prevStreetName;
	}

	/**
	 * @return Returns the prevStreetNumber.
	 */
	public String getPrevStreetNumber() {
		return prevStreetNumber;
	}

	/**
	 * @param prevStreetNumber
	 *            The prevStreetNumber to set.
	 */
	public void setPrevStreetNumber(String prevStreetNumber) {
		this.prevStreetNumber = prevStreetNumber;
	}

	/**
	 * @return Returns the prevUnitNumber.
	 */
	public String getPrevUnitNumber() {
		return prevUnitNumber;
	}

	/**
	 * @param prevUnitNumber
	 *            The prevUnitNumber to set.
	 */
	public void setPrevUnitNumber(String prevUnitNumber) {
		this.prevUnitNumber = prevUnitNumber;
	}

	/**
	 * @return Returns the prevZip.
	 */
	public String getPrevZip() {
		return prevZip;
	}

	/**
	 * @param prevZip
	 *            The prevZip to set.
	 */
	public void setPrevZip(String prevZip) {
		this.prevZip = prevZip;
	}

	/**
	 * @return Returns the prevZip4.
	 */
	public String getPrevZip4() {
		return prevZip4;
	}

	/**
	 * @param prevZip4
	 *            The prevZip4 to set.
	 */
	public void setPrevZip4(String prevZip4) {
		this.prevZip4 = prevZip4;
	}

	/**
	 * @return Returns the quantity.
	 */
	public QuantityType getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity
	 *            The quantity to set.
	 */
	public void setQuantity(QuantityType quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return Returns the quantityNum.
	 */
	public String getQuantityNum() {
		return quantityNum;
	}

	/**
	 * @param quantityNum
	 *            The quantityNum to set.
	 */
	public void setQuantityNum(String quantityNum) {
		this.quantityNum = quantityNum;
	}

	/**
	 * @return Returns the driverLicense.
	 */
	public String getDriverLicense() {
		return driverLicense;
	}

	/**
	 * @param driverLicense
	 *            The driverLicense to set.
	 */
	public void setDriverLicense(String driverLicense) {
		this.driverLicense = driverLicense;
	}

	/**
	 * @return Returns the sicCode.
	 */
	public String getSicCode() {
		return sicCode;
	}

	/**
	 * @param sicCode
	 *            The sicCode to set.
	 */
	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

	/**
	 * @return Returns the socialSecurityNumber.
	 */
	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	/**
	 * @param socialSecurityNumber
	 *            The socialSecurityNumber to set.
	 */
	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	/**
	 * @return Returns the squareFootage.
	 */
	public String getSquareFootage() {
		return squareFootage;
	}

	/**
	 * @param squareFootage
	 *            The squareFootage to set.
	 */
	public void setSquareFootage(String squareFootage) {
		this.squareFootage = squareFootage;
	}

	/**
	 * @return Returns the startingDate.
	 */
	public Calendar getStartingDate() {
		return startingDate;
	}

	/**
	 * @param startingDate
	 *            The startingDate to set.
	 */
	public void setStartingDate(Calendar startingDate) {
		this.startingDate = startingDate;
	}

	/**
	 * @return Returns the subProjectId.
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @param subProjectId
	 *            The subProjectId to set.
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return Returns the street.
	 */
	public Street getStreet() {
		return street;
	}

	/**
	 * @param street
	 *            The street to set.
	 */
	public void setStreet(Street street) {
		this.street = street;
	}

	/**
	 * @return Returns the displayContent.
	 */
	public String getDisplayContent() {
		return displayContent;
	}

	/**
	 * @param displayContent
	 *            The displayContent to set.
	 */
	public void setDisplayContent(String displayContent) {
		this.displayContent = displayContent;
	}

	/**
	 * @return Returns the applicationDateString.
	 */
	public String getApplicationDateString() {
		return StringUtils.cal2str(applicationDate);
	}

	/**
	 * @return Returns the approvalStatus.
	 */
	public String getApprovalStatus() {
		return approvalStatus;
	}

	/**
	 * @param approvalStatus
	 *            The approvalStatus to set.
	 */
	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	/**
	 * @return Returns the activityStatusDescription.
	 */
	public String getActivityStatusDescription() {
		return activityStatusDescription;
	}

	/**
	 * @param activityStatusDescription
	 *            The activityStatusDescription to set.
	 */
	public void setActivityStatusDescription(String activityStatusDescription) {
		this.activityStatusDescription = activityStatusDescription;
	}

	/**
	 * @return Returns the copyBusinessTaxActivity.
	 */
	public String getCopyBusinessTaxActivity() {
		return copyBusinessTaxActivity;
	}

	/**
	 * @param copyBusinessTaxActivity
	 *            The copyBusinessTaxActivity to set.
	 */
	public void setCopyBusinessTaxActivity(String copyBusinessTaxActivity) {
		this.copyBusinessTaxActivity = copyBusinessTaxActivity;
	}

	/**
	 * @return Returns the otherBusinessOccupancy.
	 */
	public boolean isOtherBusinessOccupancy() {
		return otherBusinessOccupancy;
	}

	/**
	 * @param otherBusinessOccupancy
	 *            The otherBusinessOccupancy to set.
	 */
	public void setOtherBusinessOccupancy(boolean otherBusinessOccupancy) {
		this.otherBusinessOccupancy = otherBusinessOccupancy;
	}

	public String getMailAttn() {
		return mailAttn;
	}

	public void setMailAttn(String mailAttn) {
		this.mailAttn = mailAttn;
	}

	public String getPreAttn() {
		return preAttn;
	}

	public void setPreAttn(String preAttn) {
		this.preAttn = preAttn;
	}

	public String getOwner1Attn() {
		return owner1Attn;
	}

	public void setOwner1Attn(String owner1Attn) {
		this.owner1Attn = owner1Attn;
	}

	public String getOwner2Attn() {
		return owner2Attn;
	}

	public void setOwner2Attn(String owner2Attn) {
		this.owner2Attn = owner2Attn;
	}

	public MultiAddress[] getMultiAddress() {
		return multiAddress;
	}

	public void setMultiAddress(MultiAddress[] multiAddress) {
		this.multiAddress = multiAddress;
	}
	/**
	 * @return the renewalOnline
	 */
	public String getRenewalOnline() {
		return renewalOnline;
	}

	/**
	 * @param renewalOnline the renewalOnline to set
	 */
	public void setRenewalOnline(String renewalOnline) {
		this.renewalOnline = renewalOnline;
	}
}

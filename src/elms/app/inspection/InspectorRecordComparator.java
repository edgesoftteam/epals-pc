package elms.app.inspection;

import java.util.Comparator;

import org.apache.log4j.Logger;

public class InspectorRecordComparator implements Comparator {
	static Logger logger = Logger.getLogger(InspectorRecordComparator.class.getName());

	InspectorRecordComparator() {
	}

	public int compare(Object a, Object b) {
		InspectorRecord recA = (InspectorRecord) a;
		InspectorRecord recB = (InspectorRecord) b;
		try {
			if (recA.getRoute() == null)
				return -1;
			if (recB.getRoute() == null)
				return 1;
			int aRoute = (new Integer(recA.getRoute())).intValue();
			int bRoute = (new Integer(recB.getRoute())).intValue();
			if (aRoute < bRoute)
				return -1;
			return aRoute != bRoute ? 1 : 0;
		} catch (NumberFormatException num) {
			logger.error("Number format exception in InspectorRecordComparator");
		}
		return 99;
	}

	public boolean equals(Object c) {
		return super.equals(c);
	}
}

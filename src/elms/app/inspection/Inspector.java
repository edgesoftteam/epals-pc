package elms.app.inspection;

import java.io.Serializable;
import java.util.List;

public class Inspector implements Serializable {

	protected String inspectorId;
	protected String name;
	protected int count;
	protected String selectAll;
	protected String selectValue;
	protected InspectorRecord[] inspectorRecord;

	public Inspector() {
		name = "";
		inspectorId = "";
		selectAll = "of";
		selectValue = "off";
		inspectorRecord = new InspectorRecord[0];
	}

	/**
	 * Gets the inspectorId
	 * 
	 * @return Returns a String
	 */
	public String getInspectorId() {
		return inspectorId;
	}

	/**
	 * Sets the inspectorId
	 * 
	 * @param inspectorId
	 *            The inspectorId to set
	 */
	public void setInspectorId(String inspectorId) {
		this.inspectorId = inspectorId;
	}

	/**
	 * Gets the inspectorRecord
	 * 
	 * @return Returns a InspectorRecord[]
	 */
	public InspectorRecord[] getInspectorRecord() {
		return inspectorRecord;
	}

	/**
	 * Sets the inspectorRecord
	 * 
	 * @param inspectorRecord
	 *            The inspectorRecord to set
	 */
	public void setInspectorRecord(InspectorRecord[] inspectorRecord) {
		this.inspectorRecord = inspectorRecord;
	}

	public void setInspectorRecord(List inspectionList) {
		InspectorRecord[] recArray = new InspectorRecord[inspectionList.size()];
		inspectionList.toArray(recArray);
		this.setInspectorRecord(recArray);
		this.setCount(recArray.length);
	}

	/**
	 * Gets the count
	 * 
	 * @return Returns a int
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Sets the count
	 * 
	 * @param count
	 *            The count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the selectAll
	 * 
	 * @return Returns a String
	 */
	public String getSelectAll() {
		return selectAll;
	}

	/**
	 * Sets the selectAll
	 * 
	 * @param selectAll
	 *            The selectAll to set
	 */
	public void setSelectAll(String selectAll) {
		this.selectAll = selectAll;
	}

	/**
	 * Gets the selectValue
	 * 
	 * @return Returns a String
	 */
	public String getSelectValue() {
		return selectValue;
	}

	/**
	 * Sets the selectValue
	 * 
	 * @param selectValue
	 *            The selectValue to set
	 */
	public void setSelectValue(String selectValue) {
		this.selectValue = selectValue;
	}

}
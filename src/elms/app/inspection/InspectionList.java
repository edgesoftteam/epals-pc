package elms.app.inspection;

import java.io.Serializable;

/**
 * @author shekhar
 * 
 *         To change this generated comment edit the template variable "typecomment": Window>Preferences>Java>Templates. To enable and disable the creation of type comments go to Window>Preferences>Java>Code Generation.
 */

public class InspectionList implements Serializable {

	protected String inspectionDate;
	protected String inspectionItem;
	protected String inspectionId;
	protected String actionCode;
	protected String requestSource;

	/**
	 * Constructor for InspectionList.
	 */
	public InspectionList() {
		super();
	}

	public InspectionList(String inspectionDate, String inspectionItem, String inspectionId, String actionCode, String requestSource) {
		this.inspectionDate = inspectionDate;
		this.inspectionItem = inspectionItem;
		this.inspectionId = inspectionId;
		this.actionCode = actionCode;
		this.requestSource = requestSource;
	}

	/**
	 * Returns the actionCode.
	 * 
	 * @return String
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * Returns the inspectionDate.
	 * 
	 * @return String
	 */
	public String getInspectionDate() {
		return inspectionDate;
	}

	/**
	 * Returns the inspectionId.
	 * 
	 * @return String
	 */
	public String getInspectionId() {
		return inspectionId;
	}

	/**
	 * Returns the inspectionItem.
	 * 
	 * @return String
	 */
	public String getInspectionItem() {
		return inspectionItem;
	}

	/**
	 * Returns the requestSource.
	 * 
	 * @return String
	 */
	public String getRequestSource() {
		return requestSource;
	}

	/**
	 * Sets the actionCode.
	 * 
	 * @param actionCode
	 *            The actionCode to set
	 */
	public void setActionCode(String actionCode) {
		if (actionCode == null)
			actionCode = "";
		this.actionCode = actionCode;
	}

	/**
	 * Sets the inspectionDate.
	 * 
	 * @param inspectionDate
	 *            The inspectionDate to set
	 */
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	/**
	 * Sets the inspectionId.
	 * 
	 * @param inspectionId
	 *            The inspectionId to set
	 */
	public void setInspectionId(String inspectionId) {
		this.inspectionId = inspectionId;
	}

	/**
	 * Sets the inspectionItem.
	 * 
	 * @param inspectionItem
	 *            The inspectionItem to set
	 */
	public void setInspectionItem(String inspectionItem) {
		if (inspectionItem == null)
			inspectionItem = "";
		this.inspectionItem = inspectionItem;
	}

	/**
	 * Sets the requestSource.
	 * 
	 * @param requestSource
	 *            The requestSource to set
	 */
	public void setRequestSource(String requestSource) {
		this.requestSource = requestSource;
	}

}

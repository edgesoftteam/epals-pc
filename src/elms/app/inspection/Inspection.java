package elms.app.inspection;

import java.io.Serializable;

public class Inspection implements Serializable {

	protected String inspectionId;
	protected String activityId;
	protected String inspectionType;
	protected String date;
	protected String source;
	protected String actnCode;
	protected String actionCodeDescription;
	protected String inspectorId;
	protected String comments;
	protected String inspectionItem;

	public Inspection() {

		inspectionId = "";
		activityId = "";
		inspectionType = "";
		date = "";
		source = "";
		actnCode = "";
	}

	public Inspection(String inspectionId, String activityId, String inspectionType, String date, String source, String actnCode) {
		this.inspectionId = inspectionId;
		this.activityId = activityId;
		this.inspectionType = inspectionType;
		this.date = date;
		this.source = source;
		this.actnCode = actnCode;
	}

	/**
	 * Gets the inspectionId
	 * 
	 * @return Returns a String
	 */
	public String getInspectionId() {
		return inspectionId;
	}

	/**
	 * Sets the inspectionId
	 * 
	 * @param inspectionId
	 *            The inspectionId to set
	 */
	public void setInspectionId(String inspectionId) {
		this.inspectionId = inspectionId;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the inspectionType
	 * 
	 * @return Returns a String
	 */
	public String getInspectionType() {
		return inspectionType;
	}

	/**
	 * Sets the inspectionType
	 * 
	 * @param inspectionType
	 *            The inspectionType to set
	 */
	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}

	/**
	 * Gets the date
	 * 
	 * @return Returns a String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Sets the date
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Gets the source
	 * 
	 * @return Returns a String
	 */
	public String getSource() {
		return source;
	}

	/**
	 * Sets the source
	 * 
	 * @param source
	 *            The source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}

	/**
	 * Gets the actnCode
	 * 
	 * @return Returns a String
	 */
	public String getActnCode() {
		return actnCode;
	}

	/**
	 * Sets the actnCode
	 * 
	 * @param actnCode
	 *            The actnCode to set
	 */
	public void setActnCode(String actnCode) {
		this.actnCode = actnCode;
	}

	/**
	 * @return
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @return
	 */
	public String getInspectionItem() {
		return inspectionItem;
	}

	/**
	 * @return
	 */
	public String getInspectorId() {
		return inspectorId;
	}

	/**
	 * @param string
	 */
	public void setComments(String string) {
		comments = string;
	}

	/**
	 * @param string
	 */
	public void setInspectionItem(String string) {
		inspectionItem = string;
	}

	/**
	 * @param string
	 */
	public void setInspectorId(String string) {
		inspectorId = string;
	}

	/**
	 * @return
	 */
	public String getActionCodeDescription() {
		return actionCodeDescription;
	}

	/**
	 * @param string
	 */
	public void setActionCodeDescription(String string) {
		actionCodeDescription = string;
	}

}
package elms.app.inspection;

import java.io.Serializable;

public class InspectorRecord implements Serializable {

	protected String check;
	protected String projectId;
	protected String subProjectId;
	protected String address;
	protected String activityId;
	protected String inspectionType;
	protected String inspectionId;
	protected String inspectionDate;
	protected String actionCode;
	protected String route;
	protected String projectNbr;
	protected String subProjectNbr;
	protected String activityNbr;

	public InspectorRecord() {

		check = "of";
		projectId = "";
		address = "";
		activityId = "";
		inspectionType = "";
		inspectionId = "";
		route = "";
		projectNbr = "";
		activityNbr = "";
	}

	public InspectorRecord(String check, String projectId, String projectNbr, String subProjectId, String subProjectNbr, String address, String activityId, String inspectionType, String inspectionId, String actionCode, String activityNbr, String route) {
		this.check = check;
		this.projectId = projectId;
		this.subProjectId = subProjectId;
		this.address = address;
		this.activityId = activityId;
		this.inspectionType = inspectionType;
		this.inspectionId = inspectionId;
		this.route = route;
		this.projectNbr = projectNbr;
		this.subProjectNbr = subProjectNbr;
		this.activityNbr = activityNbr;
		this.actionCode = actionCode;
	}

	/**
	 * Gets the check
	 * 
	 * @return Returns a String
	 */
	public String getCheck() {
		return check;
	}

	/**
	 * Sets the check
	 * 
	 * @param check
	 *            The check to set
	 */
	public void setCheck(String check) {
		this.check = check;
	}

	/**
	 * Gets the projectId
	 * 
	 * @return Returns a String
	 */
	public String getProjectId() {
		return projectId;
	}

	/**
	 * Sets the projectId
	 * 
	 * @param projectId
	 *            The projectId to set
	 */
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the address
	 * 
	 * @return Returns a String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a String
	 */
	public String getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the inspectionType
	 * 
	 * @return Returns a String
	 */
	public String getInspectionType() {
		return inspectionType;
	}

	/**
	 * Sets the inspectionType
	 * 
	 * @param inspectionType
	 *            The inspectionType to set
	 */
	public void setInspectionType(String inspectionType) {
		this.inspectionType = inspectionType;
	}

	/**
	 * Gets the inspectionId
	 * 
	 * @return Returns a String
	 */
	public String getInspectionId() {
		return inspectionId;
	}

	/**
	 * Sets the inspectionId
	 * 
	 * @param inspectionId
	 *            The inspectionId to set
	 */
	public void setInspectionId(String inspectionId) {
		this.inspectionId = inspectionId;
	}

	/**
	 * Gets the route
	 * 
	 * @return Returns a String
	 */
	public String getRoute() {
		return route;
	}

	/**
	 * Sets the route
	 * 
	 * @param route
	 *            The route to set
	 */
	public void setRoute(String route) {
		this.route = route;
	}

	/**
	 * Gets the projectNbr
	 * 
	 * @return Returns a String
	 */
	public String getProjectNbr() {
		return projectNbr;
	}

	/**
	 * Sets the projectNbr
	 * 
	 * @param projectNbr
	 *            The projectNbr to set
	 */
	public void setProjectNbr(String projectNbr) {
		this.projectNbr = projectNbr;
	}

	/**
	 * Gets the activityNbr
	 * 
	 * @return Returns a String
	 */
	public String getActivityNbr() {
		return activityNbr;
	}

	/**
	 * Sets the activityNbr
	 * 
	 * @param activityNbr
	 *            The activityNbr to set
	 */
	public void setActivityNbr(String activityNbr) {
		this.activityNbr = activityNbr;
	}

	/**
	 * Returns the actionCode.
	 * 
	 * @return String
	 */
	public String getActionCode() {
		return actionCode;
	}

	/**
	 * Sets the actionCode.
	 * 
	 * @param actionCode
	 *            The actionCode to set
	 */
	public void setActionCode(String actionCode) {
		this.actionCode = actionCode;
	}

	/**
	 * @return
	 */
	public String getInspectionDate() {
		return inspectionDate;
	}

	/**
	 * @param string
	 */
	public void setInspectionDate(String string) {
		inspectionDate = string;
	}

	/**
	 * @return
	 */
	public String getSubProjectId() {
		return subProjectId;
	}

	/**
	 * @return
	 */
	public String getSubProjectNbr() {
		return subProjectNbr;
	}

	/**
	 * @param string
	 */
	public void setSubProjectId(String string) {
		subProjectId = string;
	}

	/**
	 * @param string
	 */
	public void setSubProjectNbr(String string) {
		subProjectNbr = string;
	}

}
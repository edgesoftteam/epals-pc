package elms.app.admin;

public class InspectionSubCategory {

	private int subCategoryId;
	private int categoryId;
	private String description;

	/**
	 * Constructor for InspectionSubCategory
	 */
	public InspectionSubCategory() {
		super();
	}

	public InspectionSubCategory(int subCategoryId, String description) {
		this.subCategoryId = subCategoryId;
		this.description = description;
	}

	/**
	 * Gets the subCategoryId
	 * 
	 * @return Returns a int
	 */
	public int getSubCategoryId() {
		return subCategoryId;
	}

	/**
	 * Sets the subCategoryId
	 * 
	 * @param subCategoryId
	 *            The subCategoryId to set
	 */
	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	/**
	 * Gets the categoryId
	 * 
	 * @return Returns a int
	 */
	public int getCategoryId() {
		return categoryId;
	}

	/**
	 * Sets the categoryId
	 * 
	 * @param categoryId
	 *            The categoryId to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}

package elms.app.admin;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import elms.util.StringUtils;

public class FeeEdit implements Serializable {

	protected String feeId = "0";
	protected String actType = "";
	protected String feeType = "";
	protected String description = "";
	protected String creationDate = "";
	protected String expirationDate = "";
	protected String inputFlag = "";
	protected String flagOne = "";
	protected String flagTwo = "";
	protected String flagThree = "";
	protected String flagFour = "";
	protected String feeCalcOne = "";
	protected String feeCalcTwo = "";
	protected String feeCalcThree = "";
	protected String feeCalcFour = "";
	protected String initial;
	protected String subtotalLevel = "";
	protected String feeFactor = "";
	protected String factor = "";
	protected String account = "0";
	protected String miscellaneous = "";
	protected String sequence = "";
	protected String type = "P";
	protected String feeLookup = "";
	protected String taxFlag = "0";
	protected boolean renewable = false;
	protected String check = "";
	protected String up = "";
	protected String edited = "";
	protected String renewableFlag = "false";
	protected boolean onlineRenewalbleFlag = false;
	
	protected String accountCode = "";
	protected String feeClass = "";
	protected String activitySubtypeId = "0";
	protected String onlineInput = "";

	/**
	 * @return Returns the renewable.
	 */
	public boolean isRenewable() {
		return renewable;
	}

	/**
	 * @param renewable
	 *            The renewable to set.
	 */
	public void setRenewable(boolean renewable) {
		this.renewable = renewable;
	}

	/**
	 * @return the onlineRenewalbleFlag
	 */
	public boolean isOnlineRenewalbleFlag() {
		return onlineRenewalbleFlag;
	}

	/**
	 * @param onlineRenewalbleFlag the onlineRenewalbleFlag to set
	 */
	public void setOnlineRenewalbleFlag(boolean onlineRenewalbleFlag) {
		this.onlineRenewalbleFlag = onlineRenewalbleFlag;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(Date creationDate) {
		try {
			if (creationDate == null) {
				this.creationDate = "";
			} else {
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(creationDate);
				this.creationDate = StringUtils.cal2str(tmpCalendar);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		try {
			if (expirationDate == null) {
				this.expirationDate = "";
			} else {
				Calendar tmpCalendar = Calendar.getInstance();
				tmpCalendar.setTime(expirationDate);
				this.expirationDate = StringUtils.cal2str(tmpCalendar);
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Gets the feeId
	 * 
	 * @return Returns a String
	 */
	public String getFeeId() {
		return feeId;
	}

	/**
	 * Sets the feeId
	 * 
	 * @param feeId
	 *            The feeId to set
	 */
	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}

	/**
	 * Gets the actType
	 * 
	 * @return Returns a String
	 */
	public String getActType() {
		return actType;
	}

	/**
	 * Sets the actType
	 * 
	 * @param actType
	 *            The actType to set
	 */
	public void setActType(String actType) {
		this.actType = actType;
	}

	/**
	 * Gets the feeType
	 * 
	 * @return Returns a String
	 */
	public String getFeeType() {
		return feeType;
	}

	/**
	 * Sets the feeType
	 * 
	 * @param feeType
	 *            The feeType to set
	 */
	public void setFeeType(String feeType) {
		this.feeType = feeType;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Gets the expirationDate
	 * 
	 * @return Returns a String
	 */
	public String getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Gets the inputFlag
	 * 
	 * @return Returns a String
	 */
	public String getInputFlag() {
		return inputFlag;
	}

	/**
	 * Sets the inputFlag
	 * 
	 * @param inputFlag
	 *            The inputFlag to set
	 */
	public void setInputFlag(String inputFlag) {
		this.inputFlag = inputFlag;
	}

	/**
	 * Gets the flagOne
	 * 
	 * @return Returns a String
	 */
	public String getFlagOne() {
		return flagOne;
	}

	/**
	 * Sets the flagOne
	 * 
	 * @param flagOne
	 *            The flagOne to set
	 */
	public void setFlagOne(String flagOne) {
		this.flagOne = flagOne;
	}

	/**
	 * Gets the flagTwo
	 * 
	 * @return Returns a String
	 */
	public String getFlagTwo() {
		return flagTwo;
	}

	/**
	 * Sets the flagTwo
	 * 
	 * @param flagTwo
	 *            The flagTwo to set
	 */
	public void setFlagTwo(String flagTwo) {
		this.flagTwo = flagTwo;
	}

	/**
	 * Gets the flagThree
	 * 
	 * @return Returns a String
	 */
	public String getFlagThree() {
		return flagThree;
	}

	/**
	 * Sets the flagThree
	 * 
	 * @param flagThree
	 *            The flagThree to set
	 */
	public void setFlagThree(String flagThree) {
		this.flagThree = flagThree;
	}

	/**
	 * Gets the flagFour
	 * 
	 * @return Returns a String
	 */
	public String getFlagFour() {
		return flagFour;
	}

	/**
	 * Sets the flagFour
	 * 
	 * @param flagFour
	 *            The flagFour to set
	 */
	public void setFlagFour(String flagFour) {
		this.flagFour = flagFour;
	}

	/**
	 * Gets the feeCalcOne
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcOne() {
		return feeCalcOne;
	}

	/**
	 * Sets the feeCalcOne
	 * 
	 * @param feeCalcOne
	 *            The feeCalcOne to set
	 */
	public void setFeeCalcOne(String feeCalcOne) {
		this.feeCalcOne = feeCalcOne;
	}

	/**
	 * Gets the feeCalcTwo
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcTwo() {
		return feeCalcTwo;
	}

	/**
	 * Sets the feeCalcTwo
	 * 
	 * @param feeCalcTwo
	 *            The feeCalcTwo to set
	 */
	public void setFeeCalcTwo(String feeCalcTwo) {
		this.feeCalcTwo = feeCalcTwo;
	}

	/**
	 * Gets the feeCalcThree
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcThree() {
		return feeCalcThree;
	}

	/**
	 * Sets the feeCalcThree
	 * 
	 * @param feeCalcThree
	 *            The feeCalcThree to set
	 */
	public void setFeeCalcThree(String feeCalcThree) {
		this.feeCalcThree = feeCalcThree;
	}

	/**
	 * Gets the feeCalcFour
	 * 
	 * @return Returns a String
	 */
	public String getFeeCalcFour() {
		return feeCalcFour;
	}

	/**
	 * Sets the feeCalcFour
	 * 
	 * @param feeCalcFour
	 *            The feeCalcFour to set
	 */
	public void setFeeCalcFour(String feeCalcFour) {
		this.feeCalcFour = feeCalcFour;
	}

	/**
	 * Gets the initial
	 * 
	 * @return Returns a String
	 */
	public String getInitial() {
		return initial;
	}

	/**
	 * Sets the initial
	 * 
	 * @param initial
	 *            The initial to set
	 */
	public void setInitial(String initial) {
		this.initial = initial;
	}

	/**
	 * Gets the subtotalLevel
	 * 
	 * @return Returns a String
	 */
	public String getSubtotalLevel() {
		return subtotalLevel;
	}

	/**
	 * Sets the subtotalLevel
	 * 
	 * @param subtotalLevel
	 *            The subtotalLevel to set
	 */
	public void setSubtotalLevel(String subtotalLevel) {
		this.subtotalLevel = subtotalLevel;
	}

	/**
	 * Gets the feeFactor
	 * 
	 * @return Returns a String
	 */
	public String getFeeFactor() {
		return feeFactor;
	}

	/**
	 * Sets the feeFactor
	 * 
	 * @param feeFactor
	 *            The feeFactor to set
	 */
	public void setFeeFactor(String feeFactor) {
		this.feeFactor = feeFactor;
	}

	/**
	 * Gets the factor
	 * 
	 * @return Returns a String
	 */
	public String getFactor() {
		return factor;
	}

	/**
	 * Sets the factor
	 * 
	 * @param factor
	 *            The factor to set
	 */
	public void setFactor(String factor) {
		this.factor = factor;
	}

	/**
	 * Gets the account
	 * 
	 * @return Returns a String
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * Sets the account
	 * 
	 * @param account
	 *            The account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * Gets the miscellaneous
	 * 
	 * @return Returns a String
	 */
	public String getMiscellaneous() {
		return miscellaneous;
	}

	/**
	 * Sets the miscellaneous
	 * 
	 * @param miscellaneous
	 *            The miscellaneous to set
	 */
	public void setMiscellaneous(String miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	/**
	 * Gets the sequence
	 * 
	 * @return Returns a String
	 */
	public String getSequence() {
		return sequence;
	}

	/**
	 * Sets the sequence
	 * 
	 * @param sequence
	 *            The sequence to set
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the check
	 * 
	 * @return Returns a String
	 */
	public String getCheck() {
		return check;
	}

	/**
	 * Sets the check
	 * 
	 * @param check
	 *            The check to set
	 */
	public void setCheck(String check) {
		this.check = check;
	}

	/**
	 * Gets the up
	 * 
	 * @return Returns a String
	 */
	public String getUp() {
		return up;
	}

	/**
	 * Sets the up
	 * 
	 * @param up
	 *            The up to set
	 */
	public void setUp(String up) {
		this.up = up;
	}

	/**
	 * Gets the edited
	 * 
	 * @return Returns a String
	 */
	public String getEdited() {
		return edited;
	}

	/**
	 * Sets the edited
	 * 
	 * @param edited
	 *            The edited to set
	 */
	public void setEdited(String edited) {
		this.edited = edited;
	}

	/**
	 * Gets the feeLookup
	 * 
	 * @return Returns a String
	 */
	public String getFeeLookup() {
		return feeLookup;
	}

	/**
	 * Sets the feeLookup
	 * 
	 * @param feeLookup
	 *            The feeLookup to set
	 */
	public void setFeeLookup(String feeLookup) {
		this.feeLookup = feeLookup;
	}

	/**
	 * Returns the taxFlag.
	 * 
	 * @return String
	 */
	public String getTaxFlag() {
		return taxFlag;
	}

	/**
	 * Sets the taxFlag.
	 * 
	 * @param taxFlag
	 *            The taxFlag to set
	 */
	public void setTaxFlag(String taxFlag) {
		this.taxFlag = taxFlag;
	}

	/**
	 * @return Returns the renewableFlag.
	 */
	public String getRenewableFlag() {
		return renewableFlag;
	}

	/**
	 * @param renewableFlag
	 *            The renewableFlag to set.
	 */
	public void setRenewableFlag(String renewableFlag) {
		this.renewableFlag = renewableFlag;
	}

	public String getAccountCode() {
		return accountCode;
	}

	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}

	public String getFeeClass() {
		return feeClass;
	}

	public void setFeeClass(String feeClass) {
		this.feeClass = feeClass;
	}

	public String getActivitySubtypeId() {
		return activitySubtypeId;
	}

	public void setActivitySubtypeId(String activitySubtypeId) {
		this.activitySubtypeId = activitySubtypeId;
	}

	public String getOnlineInput() {
		return onlineInput;
	}

	public void setOnlineInput(String onlineInput) {
		this.onlineInput = onlineInput;
	}
	
	

}

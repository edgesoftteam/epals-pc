package elms.app.admin;

import java.io.Serializable;

public class ActivitySubType311 implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int mappingId;
	private int subTypeId;
	private String violationType;
	private String violationType311;
	
	public int getMappingId() {
		return mappingId;
	}
	public void setMappingId(int mappingId) {
		this.mappingId = mappingId;
	}
	public int getSubTypeId() {
		return subTypeId;
	}
	public void setSubTypeId(int subTypeId) {
		this.subTypeId = subTypeId;
	}
	public String getViolationType() {
		return violationType;
	}
	public void setViolationType(String violationType) {
		this.violationType = violationType;
	}
	public String getViolationType311() {
		return violationType311;
	}
	public void setViolationType311(String violationType311) {
		this.violationType311 = violationType311;
	}
}

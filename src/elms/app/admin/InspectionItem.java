package elms.app.admin;

public class InspectionItem {

	private int code;
	private String description;

	public InspectionItem() {
	}

	public InspectionItem(int code, String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a int
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}

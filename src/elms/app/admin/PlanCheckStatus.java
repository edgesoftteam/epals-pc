package elms.app.admin;

public class PlanCheckStatus {

	private int code;
	private int actStatusCode;
	private String description;
	private String active;
	//private int moduleId;
	private String visible;

	/**
	 * Constructor for PlanCheckStatus
	 */
	public PlanCheckStatus() {

	}

	/**
	 * Constructor for PlanCheckStatus
	 */
	public PlanCheckStatus(int code, String description, int activityStatusCode) {
		this.code = code;
		this.description = description;
		this.actStatusCode = activityStatusCode;
	}

	/**
	 * Constructor for PlanCheckStatus
	 */
	public PlanCheckStatus(int code, String description, int activityStatusCode, int moduleId, String active) {
		this.code = code;
		this.description = description;
		this.actStatusCode = activityStatusCode;
		//this.moduleId = moduleId;
		this.active = active;
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a int
	 */
	public int getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(int code) {
		this.code = code;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the actStatusCode
	 * 
	 * @return Returns a int
	 */
	public int getActStatusCode() {
		return actStatusCode;
	}

	/**
	 * Sets the actStatusCode
	 * 
	 * @param actStatusCode
	 *            The actStatusCode to set
	 */
	public void setActStatusCode(int actStatusCode) {
		this.actStatusCode = actStatusCode;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	/*public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}*/

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

}

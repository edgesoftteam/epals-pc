package elms.app.admin;

public class CountByHoldType {

	private int count;
	private String holdType;

	public CountByHoldType() {
	}

	public CountByHoldType(int count, String holdType) {
		this.count = count;
		this.holdType = holdType;
	}

	/**
	 * Gets the count
	 * 
	 * @return Returns a int
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Sets the count
	 * 
	 * @param count
	 *            The count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Gets the holdType
	 * 
	 * @return Returns a String
	 */
	public String getHoldType() {
		return holdType;
	}

	/**
	 * Sets the holdType
	 * 
	 * @param holdType
	 *            The holdType to set
	 */
	public void setHoldType(String holdType) {
		this.holdType = holdType;
	}

}
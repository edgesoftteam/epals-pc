package elms.app.admin;

public class Landmark {

	private int addressId;
	private String name;
	private String description;
	private String nearbyAddress1;
	private String nearbyAddress2;
	private String nearbyAddress3;
	private String nearbyAddress4;

	public Landmark(int addressId, String name, String description) {
		super();
		this.addressId = addressId;
		this.name = name;
		this.description = description;
	}

	public int getAddressId() {
		return addressId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getNearbyAddress1() {
		return nearbyAddress1;
	}

	public void setNearbyAddress1(String nearbyAddress1) {
		this.nearbyAddress1 = nearbyAddress1;
	}

	public String getNearbyAddress2() {
		return nearbyAddress2;
	}

	public void setNearbyAddress2(String nearbyAddress2) {
		this.nearbyAddress2 = nearbyAddress2;
	}

	public String getNearbyAddress3() {
		return nearbyAddress3;
	}

	public void setNearbyAddress3(String nearbyAddress3) {
		this.nearbyAddress3 = nearbyAddress3;
	}

	public String getNearbyAddress4() {
		return nearbyAddress4;
	}

	public void setNearbyAddress4(String nearbyAddress4) {
		this.nearbyAddress4 = nearbyAddress4;
	}

}

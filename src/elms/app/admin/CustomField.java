package elms.app.admin;

import java.io.Serializable;

public class CustomField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	protected int fieldId;
	protected String fieldValue;
	protected String fieldLabel;
	protected String fieldDesc;
	protected String fieldType;
	protected String levelType;
	protected int levelTypeId;
	protected String fieldRequired;

	public CustomField() {
		super();
	}

	public int getFieldId() {
		return fieldId;
	}

	public void setFieldId(int fieldId) {
		this.fieldId = fieldId;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldLabel() {
		return fieldLabel;
	}

	public void setFieldLabel(String fieldLabel) {
		this.fieldLabel = fieldLabel;
	}

	public String getFieldDesc() {
		return fieldDesc;
	}

	public void setFieldDesc(String fieldDesc) {
		this.fieldDesc = fieldDesc;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String levelType) {
		this.levelType = levelType;
	}

	public int getLevelTypeId() {
		return levelTypeId;
	}

	public void setLevelTypeId(int levelTypeId) {
		this.levelTypeId = levelTypeId;
	}

	public String getFieldRequired() {
		return fieldRequired;
	}

	public void setFieldRequired(String fieldRequired) {
		this.fieldRequired = fieldRequired;
	}

}

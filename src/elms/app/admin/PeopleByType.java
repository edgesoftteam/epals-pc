package elms.app.admin;

public class PeopleByType {
	private int peopleId;
	private String name;

	public PeopleByType(int peopleId, String name) {
		this.peopleId = peopleId;
		this.name = name;
	}

	public PeopleByType() {
	}

	/**
	 * Gets the peopleId
	 * 
	 * @return Returns a int
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the peopleId
	 * 
	 * @param peopleId
	 *            The peopleId to set
	 */
	public void setPeopleId(int peopleId) {
		this.peopleId = peopleId;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}

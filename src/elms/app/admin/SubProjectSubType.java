package elms.app.admin;

public class SubProjectSubType {

	private int subProjectSubTypeId;
	private String type;
	private String description;

	private String delete;
	private String deletable;

	/**
	 * Constructor for SubActivitySubType
	 */
	public SubProjectSubType() {
	}

	public SubProjectSubType(int subProjectSubTypeId, String type, String description) {
		this.subProjectSubTypeId = subProjectSubTypeId;
		this.type = type;
		this.description = description;
	}

	/**
	 * Gets the subProjectSubTypeId
	 * 
	 * @return Returns a int
	 */
	public int getSubProjectSubTypeId() {
		return subProjectSubTypeId;
	}

	/**
	 * Sets the subProjectSubTypeId
	 * 
	 * @param subProjectSubTypeId
	 *            The subProjectSubTypeId to set
	 */
	public void setSubProjectSubTypeId(int subProjectSubTypeId) {
		this.subProjectSubTypeId = subProjectSubTypeId;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the delatable.
	 * 
	 * @return String
	 */
	public String getDeletable() {
		return deletable;
	}

	/**
	 * Returns the delete.
	 * 
	 * @return String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Sets the delatable.
	 * 
	 * @param delatable
	 *            The delatable to set
	 */
	public void setDeletable(String deletable) {
		this.deletable = deletable;
	}

	/**
	 * Sets the delete.
	 * 
	 * @param delete
	 *            The delete to set
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}

}
//Source file: C:\\datafile\\source\\elms\\app\\lso\\Street.java

package elms.app.admin.lso;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import elms.security.User;


/**
 * @author Shekhar Jain
 */
public class StructureEdit implements Serializable {
    protected String structureId;
    protected String description;
    protected int projectCount;
    protected String holdsExists;
    protected String structureFunction;
    protected String match = "N";
    protected OccupancyEdit[] occupancyList;

    protected String streetNumber;
    protected String streetModifier;
    protected String streetId;
    protected String streetName;
    protected String isLot;
    protected User createdBy;
    protected Calendar created;
    protected User updatedBy;
    protected Calendar updated;

    /**
     * Constructor for StructureSummary.
     */

    public StructureEdit() {

    }

    public StructureEdit(String structureId, String description, int projectCount, String holdsExists, OccupancyEdit[] occupancyList) {
        this.structureId = structureId;
        this.description = description;
        this.projectCount = projectCount;
        this.holdsExists = holdsExists;
        this.occupancyList = occupancyList;
    }

    /**
     * Returns the description.
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the projectCount.
     * @return int
     */
    public int getProjectCount() {
        return projectCount;
    }

    /**
     * Sets the description.
     * @param description The description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Sets the projectCount.
     * @param projectCount The projectCount to set
     */
    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }


    /**
     * Returns the holdsExists.
     * @return String
     */
    public String getHoldsExists() {
        return holdsExists;
    }

    /**
     * Sets the holdsExists.
     * @param holdsExists The holdsExists to set
     */
    public void setHoldsExists(String holdsExists) {
        this.holdsExists = holdsExists;
    }

    /**
     * Returns the structureFunction.
     * @return String
     */
    public String getStructureFunction() {
        return structureFunction;
    }

    /**
     * Sets the structureFunction.
     * @param structureFunction The structureFunction to set
     */
    public void setStructureFunction(String structureFunction) {
        this.structureFunction = structureFunction;
    }

    /**
     * Returns the occupancyList.
     * @return OccupancyEdit[]
     */
    public OccupancyEdit[] getOccupancyList() {
        return occupancyList;
    }

    /**
     * Sets the occupancyList.
     * @param occupancyList The occupancyList to set
     */
    public void setOccupancyList(OccupancyEdit[] occupancyList) {
        this.occupancyList = occupancyList;
    }

    public void setOccupancyList(List occupancyList) {
        if (occupancyList == null)
            this.occupancyList = new OccupancyEdit[0];
        else {
            int count = occupancyList.size();
            OccupancyEdit[] occupancyEdit = new OccupancyEdit[count];
            Iterator iter = occupancyList.iterator();
            count = 0;
            while (iter.hasNext()) {
                occupancyEdit[count] = (OccupancyEdit) iter.next();
                count++;
            }
            this.occupancyList = occupancyEdit;
        }
    }

    /**
     * Returns the streetName.
     * @return String
     */

    public String getStreetName() {
        return streetName;
    }

    /**
     * Returns the streetNumber.
     * @return String
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * Sets the streetName.
     * @param streetName The streetName to set
     */

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * Sets the streetNumber.
     * @param streetNumber The streetNumber to set
     */
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    /**
     * Returns the createdBy.
     * @return User
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * Returns the updatedBy.
     * @return User
     */
    public User getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the createdBy.
     * @param createdBy The createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Sets the updatedBy.
     * @param updatedBy The updatedBy to set
     */
    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Returns the created.
     * @return Calendar
     */
    public Calendar getCreated() {
        return created;
    }

    /**
     * Returns the updated.
     * @return Calendar
     */
    public Calendar getUpdated() {
        return updated;
    }

    /**
     * Sets the created.
     * @param created The created to set
     */
    public void setCreated(Calendar created) {
        this.created = created;
    }

    /**
     * Sets the created
     * @param created The created to set
     */
    public void setCreated(Date created) {
        if (created == null)
            this.created = null;
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(created);
            this.created = calendar;
        }
    }

    /**
     * Sets the updated.
     * @param updated The updated to set
     */
    public void setUpdated(Calendar updated) {
        this.updated = updated;
    }

    /**
     * Sets the updated
     * @param updated The updated to set
     */
    public void setUpdated(Date updated) {
        if (updated == null)
            this.updated = null;
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(updated);
            this.updated = calendar;
        }
    }

    /**
     * Returns the structureId.
     * @return String
     */
    public String getStructureId() {
        return structureId;
    }

    /**
     * Sets the structureId.
     * @param structureId The structureId to set
     */
    public void setStructureId(String structureId) {
        this.structureId = structureId;
    }

    /**
     * Returns the isLot.
     * @return String
     */
    public String getIsLot() {
        return isLot;
    }

    /**
     * Sets the isLot.
     * @param isLot The isLot to set
     */
    public void setIsLot(String isLot) {
        this.isLot = isLot;
    }

    /**
     * Returns the streetModifier.
     * @return String
     */
    public String getStreetModifier() {
        return streetModifier;
    }

    /**
     * Sets the streetModifier.
     * @param streetModifier The streetModifier to set
     */
    public void setStreetModifier(String streetModifier) {
        this.streetModifier = streetModifier;
    }

    /**
     * Returns the match.
     * @return String
     */
    public String getMatch() {
        return match;
    }

    /**
     * Sets the match.
     * @param match The match to set
     */
    public void setMatch(String match) {
        this.match = match;
    }

    /**
     * Returns the streetId.
     * @return String
     */
    public String getStreetId() {
        return streetId;
    }

    /**
     * Sets the streetId.
     * @param streetId The streetId to set
     */
    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

}

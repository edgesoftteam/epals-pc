//Source file: C:\\datafile\\source\\elms\\app\\lso\\Street.java

package elms.app.admin.lso;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import elms.security.User;


/**
 * @author Shekhar Jain
 */
public class OccupancyEdit implements Serializable {
    protected String occupancyId;
    protected String description;

    protected String delete;
    protected String active;
    protected String streetNumber = "";
    protected String streetModifier = "";
    protected String streetId = "";
    protected String streetName = "";
    protected String unitNumber = "";
    protected String apnNumber = "";
    protected String oldApnNumber = "";
    protected String isLot;
    protected String match = "N";
    protected User createdBy;
    protected Calendar created;
    protected User updatedBy;
    protected Calendar updated;

    protected int projectCount;
    protected String holdsExists;

    /**
     * Constructor for OccupancySummary.
     */
    public OccupancyEdit() {

    }

    public OccupancyEdit(String occupancyId, String description, int projectCount, String holdsExists) {
        this.occupancyId = occupancyId;
        this.description = description;
        this.projectCount = projectCount;
        this.holdsExists = holdsExists;
    }

    /**
     * Returns the description.
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Returns the projectCount.
     * @return int
     */
    public int getProjectCount() {
        return projectCount;
    }

    /**
     * Sets the description.
     * @param description The description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets the projectCount.
     * @param projectCount The projectCount to set
     */
    public void setProjectCount(int projectCount) {
        this.projectCount = projectCount;
    }

    /**
     * Returns the holdsExists.
     * @return String
     */
    public String getHoldsExists() {
        return holdsExists;
    }

    /**
     * Sets the holdsExists.
     * @param holdsExists The holdsExists to set
     */
    public void setHoldsExists(String holdsExists) {
        this.holdsExists = holdsExists;
    }

    /**
     * Returns the active.
     * @return String
     */
    public String getActive() {
        return active;
    }

    /**
     * Returns the apnNumber.
     * @return String
     */
    public String getApnNumber() {
        return apnNumber;
    }

    /**
     * Returns the deleted.
     * @return String
     */
    public String getDelete() {
        return delete;
    }

    /**
     * Returns the unitNumber.
     * @return String
     */
    public String getUnitNumber() {
        return unitNumber;
    }

    /**
     * Sets the active.
     * @param active The active to set
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * Sets the apnNumber.
     * @param apnNumber The apnNumber to set
     */
    public void setApnNumber(String apnNumber) {
        this.apnNumber = apnNumber;
    }

    /**
     * Sets the deleted.
     * @param deleted The deleted to set
     */
    public void setDeleted(String delete) {
        this.delete = delete;
    }

    /**
     * Sets the unitNumber.
     * @param unitNumber The unitNumber to set
     */
    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    /**
     * Returns the updatedBy.
     * @return User
     */
    public User getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Sets the delete.
     * @param delete The delete to set
     */
    public void setDelete(String delete) {
        this.delete = delete;
    }

    /**
     * Sets the updatedBy.
     * @param updatedBy The updatedBy to set
     */
    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * Returns the created.
     * @return Calendar
     */
    public Calendar getCreated() {
        return created;
    }

    /**
     * Returns the createdBy.
     * @return User
     */
    public User getCreatedBy() {
        return createdBy;
    }

    /**
     * Returns the updated.
     * @return Calendar
     */
    public Calendar getUpdated() {
        return updated;
    }

    /**
     * Sets the created.
     * @param created The created to set
     */
    public void setCreated(Calendar created) {
        this.created = created;
    }

    /**
     * Sets the created
     * @param created The created to set
     */
    public void setCreated(Date created) {
        if (created == null)
            this.created = null;
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(created);
            this.created = calendar;
        }
    }

    /**
     * Sets the createdBy.
     * @param createdBy The createdBy to set
     */
    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * Sets the updated.
     * @param updated The updated to set
     */
    public void setUpdated(Calendar updated) {
        this.updated = updated;
    }

    /**
     * Sets the updated
     * @param updated The updated to set
     */
    public void setUpdated(Date updated) {
        if (updated == null)
            this.updated = null;
        else {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(updated);
            this.updated = calendar;
        }
    }

    /**
     * Returns the occupancyId.
     * @return String
     */
    public String getOccupancyId() {
        return occupancyId;
    }

    /**
     * Sets the occupancyId.
     * @param occupancyId The occupancyId to set
     */
    public void setOccupancyId(String occupancyId) {
        this.occupancyId = occupancyId;
    }

    /**
     * Returns the oldApnNumber.
     * @return String
     */
    public String getOldApnNumber() {
        return oldApnNumber;
    }

    /**
     * Sets the oldApnNumber.
     * @param oldApnNumber The oldApnNumber to set
     */
    public void setOldApnNumber(String oldApnNumber) {
        this.oldApnNumber = oldApnNumber;
    }

    /**
     * Returns the streetNumber.
     * @return String
     */
    public String getStreetNumber() {
        return streetNumber;
    }

    /**
     * Sets the streetNumber.
     * @param streetNumber The streetNumber to set
     */
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    /**
     * Returns the isLot.
     * @return String
     */
    public String getIsLot() {
        return isLot;
    }

    /**
     * Sets the isLot.
     * @param isLot The isLot to set
     */
    public void setIsLot(String isLot) {
        this.isLot = isLot;
    }

    /**
     * Returns the streetModifier.
     * @return String
     */
    public String getStreetModifier() {
        return streetModifier;
    }

    /**
     * Sets the streetModifier.
     * @param streetModifier The streetModifier to set
     */
    public void setStreetModifier(String streetModifier) {
        this.streetModifier = streetModifier;
    }

    /**
     * Returns the match.
     * @return String
     */
    public String getMatch() {
        return match;
    }

    /**
     * Sets the match.
     * @param match The match to set
     */
    public void setMatch(String match) {
        this.match = match;
    }

    /**
     * Returns the streetId.
     * @return String
     */
    public String getStreetId() {
        return streetId;
    }

    /**
     * Sets the streetId.
     * @param streetId The streetId to set
     */
    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    /**
     * Returns the streetName.
     * @return String
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the streetName.
     * @param streetName The streetName to set
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

}

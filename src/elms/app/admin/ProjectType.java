package elms.app.admin;

public class ProjectType {

	protected int projectTypeId;
	protected String description;
	protected String department;
	protected String type;
	protected String subProjectName;
	protected String departmentDescription;
	protected String delete;
	protected String deletable;

	public ProjectType(int projectTypeId, String description) {
		this.projectTypeId = projectTypeId;
		this.description = description;
	}

	public ProjectType(int projectTypeId, String description, String departmentDescription) {
		this.projectTypeId = projectTypeId;
		this.description = description;
		this.departmentDescription = departmentDescription;
	}

	public ProjectType() {
		// default constr
	}

	public String getDepartmentDescription() {
		return departmentDescription;
	}

	public void setDepartmentDescription(String departmentDescription) {
		this.departmentDescription = departmentDescription;
	}

	/**
	 * Gets the projectTypeId
	 * 
	 * @return Returns a int
	 */
	public int getProjectTypeId() {
		return projectTypeId;
	}

	/**
	 * Sets the projectTypeId
	 * 
	 * @param projectTypeId
	 *            The projectTypeId to set
	 */
	public void setProjectTypeId(int projectTypeId) {
		this.projectTypeId = projectTypeId;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the deletable.
	 * 
	 * @return String
	 */
	public String getDeletable() {
		return deletable;
	}

	/**
	 * Returns the delete.
	 * 
	 * @return String
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * Sets the deletable.
	 * 
	 * @param deletable
	 *            The deletable to set
	 */
	public void setDeletable(String deletable) {
		this.deletable = deletable;
	}

	/**
	 * Sets the delete.
	 * 
	 * @param delete
	 *            The delete to set
	 */
	public void setDelete(String delete) {
		this.delete = delete;
	}

	/**
	 * @return Returns the department.
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department
	 *            The department to set.
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return Returns the subProjectName.
	 */
	public String getSubProjectName() {
		return subProjectName;
	}

	/**
	 * @param subProjectName
	 *            The subProjectName to set.
	 */
	public void setSubProjectName(String subProjectName) {
		this.subProjectName = subProjectName;
	}

	/**
	 * @return Returns the type.
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            The type to set.
	 */
	public void setType(String type) {
		this.type = type;
	}
}
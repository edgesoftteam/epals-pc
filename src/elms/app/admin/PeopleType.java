package elms.app.admin;

/**
 * @author Shekhar Jain
 */
public class PeopleType {

	protected int typeId;
	protected String type;
	protected String code;
	protected String projectName;
	protected String adminType;

	public PeopleType() {
	}

	public PeopleType(int typeId, String type, String code) {

		this.typeId = typeId;
		this.type = type;
		this.code = code;
	}

	/**
	 * Gets the type
	 * 
	 * @return Returns a String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the code
	 * 
	 * @return Returns a String
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Sets the code
	 * 
	 * @param code
	 *            The code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Gets the typeId
	 * 
	 * @return Returns a int
	 */
	public int getTypeId() {
		return typeId;
	}

	/**
	 * Sets the typeId
	 * 
	 * @param typeId
	 *            The typeId to set
	 */
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param string
	 */
	public void setProjectName(String string) {
		projectName = string;
	}

	/**
	 * @return
	 */
	public String getAdminType() {
		return type + " (" + projectName + ")";
	}

	/**
	 * @param string
	 */
	public void setAdminType(String string) {
		adminType = string;
	}

}
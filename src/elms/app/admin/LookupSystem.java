package elms.app.admin;

/**
 * @author Gayathri Turlapati
 */
public class LookupSystem {

	public int systemId;
	public String name;
	public String value;

	public LookupSystem() {

	}

	public LookupSystem(int systemId, String name,String value) {
		this.systemId = systemId;
		this.name = "";
		this.value = "";
	}

	// Getters and Setters

	public int getSystemId() {
		return systemId;
	}

	public void setSystemId(int systemId) {
		this.systemId = systemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
package elms.app.admin;

import java.io.Serializable;

public class EngineerUser implements Serializable {

	private int userId;
	private String engineerId;
	 private String name;
	public EngineerUser() {
		userId = 0;
		engineerId = "";
	}

	  public EngineerUser(int userId, String name) {
	        this.userId = userId;
	        this.name = name;
	    }
	/**
	 * Gets the userId
	 * 
	 * @return Returns a int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the userId
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Returns the engineerId.
	 * 
	 * @return String
	 */
	public String getEngineerId() {
		return engineerId;
	}

	/**
	 * Sets the engineerId.
	 * 
	 * @param engineerId
	 *            The engineerId to set
	 */
	public void setEngineerId(String engineerId) {
		this.engineerId = engineerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

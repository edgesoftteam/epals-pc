package elms.app.admin;

public class PayType {

	private int payTypeId;
	private String description;

	public PayType() {
	}

	public PayType(int paytypeId, String desc) {
		this.payTypeId = paytypeId;
		this.description = desc;
	}

	/**
	 * Gets the pay_type_id
	 * 
	 * @return Returns a String
	 */
	public int getPayTypeId() {
		return payTypeId;
	}

	/**
	 * Sets the pay_type_id
	 * 
	 * @param pay_type_id
	 *            The pay_type_id to set
	 */
	public void setPayTypeId(int pay_type_id) {
		this.payTypeId = pay_type_id;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}

package elms.app.admin;

public class InspectionCategory {

	private int categoryId;
	private String description;

	/**
	 * Constructor for InspectionCategory
	 */
	public InspectionCategory() {
		super();
	}

	public InspectionCategory(int categoryId, String description) {
		this.categoryId = categoryId;
		this.description = description;
	}

	/**
	 * Gets the categoryId
	 * 
	 * @return Returns a int
	 */
	public int getCategoryId() {
		return categoryId;
	}

	/**
	 * Sets the categoryId
	 * 
	 * @param categoryId
	 *            The categoryId to set
	 */
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}

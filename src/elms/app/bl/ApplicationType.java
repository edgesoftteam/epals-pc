package elms.app.bl;

/**
 * @author Gayathri & Manjuprasad
 * 
 *         Lookup object for business license application type
 */
public class ApplicationType {

	public int id;
	public String description;

	/**
	 * 
	 */
	public ApplicationType() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public ApplicationType(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
}

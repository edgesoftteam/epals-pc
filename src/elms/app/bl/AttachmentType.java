package elms.app.bl;

/*
 * @Krishna
 * This object is for attachment types to show in add attachments
 */
public class AttachmentType {
	public int id;
	public String description;
	
	
	public AttachmentType() {
		super();
	}
	/**
	 * @param id
	 * @param description
	 */
	public AttachmentType(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}	
	
}

package elms.app.bl;

/**
 * @author Aman Puri
 * 
 *         Business Owner object
 */
public class BusinessOwner {

	protected int businessAcNumber;
	protected String businessName;
	protected String businessOwnerName;
	protected String owner1Name;
	protected String owner2Name;
	protected String businessAddress;
	protected String activityType;
	protected String activitySubType;
	protected String activityStatus;
	protected String outofTownAddress1;
	protected String outofTownAddress2;
	protected String outofTownAddress3;
	protected String outofTownAddress4;
	protected String outofTownAddressState;
	protected String outofTownAddressCountry;
	protected String outofTownAddressZip;
	protected String outofTownAddressZip4;
	protected int lso_id;
	protected int activity_id;
	protected String businessOwnerResults;

	/**
	 * @return Returns the activitySubType.
	 */
	public String getActivitySubType() {
		return activitySubType;
	}

	/**
	 * @param activitySubType
	 *            The activitySubType to set.
	 */
	public void setActivitySubType(String activitySubType) {
		this.activitySubType = activitySubType;
	}

	/**
	 * @return Returns the businessAddress.
	 */
	public String getBusinessAddress() {
		return businessAddress;
	}

	/**
	 * @param businessAddress
	 *            The businessAddress to set.
	 */
	public void setBusinessAddress(String businessAddress) {
		this.businessAddress = businessAddress;
	}

	/**
	 * @return Returns the activityStatus.
	 */
	public String getActivityStatus() {
		return activityStatus;
	}

	/**
	 * @param activityStatus
	 *            The activityStatus to set.
	 */
	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	/**
	 * @return Returns the activityType.
	 */
	public String getActivityType() {
		return activityType;
	}

	/**
	 * @param activityType
	 *            The activityType to set.
	 */
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	/**
	 * @return Returns the businessAcNumber.
	 */
	public int getBusinessAcNumber() {
		return businessAcNumber;
	}

	/**
	 * @param businessAcNumber
	 *            The businessAcNumber to set.
	 */
	public void setBusinessAcNumber(int businessAcNumber) {
		this.businessAcNumber = businessAcNumber;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the businessOwnerName.
	 */
	public String getBusinessOwnerName() {
		return businessOwnerName;
	}

	/**
	 * @param businessOwnerName
	 *            The businessOwnerName to set.
	 */
	public void setBusinessOwnerName(String businessOwnerName) {
		this.businessOwnerName = businessOwnerName;
	}

	/**
	 * @return Returns the owner1Name.
	 */
	public String getOwner1Name() {
		return owner1Name;
	}

	/**
	 * @param owner1Name
	 *            The owner1Name to set.
	 */
	public void setOwner1Name(String owner1Name) {
		this.owner1Name = owner1Name;
	}

	/**
	 * @return Returns the owner2Name.
	 */
	public String getOwner2Name() {
		return owner2Name;
	}

	/**
	 * @param owner2Name
	 *            The owner2Name to set.
	 */
	public void setOwner2Name(String owner2Name) {
		this.owner2Name = owner2Name;
	}

	/**
	 * @return Returns the outofTownAddress1.
	 */
	public String getOutofTownAddress1() {
		return outofTownAddress1;
	}

	/**
	 * @param outofTownAddress1
	 *            The outofTownAddress1 to set.
	 */
	public void setOutofTownAddress1(String outofTownAddress1) {
		this.outofTownAddress1 = outofTownAddress1;
	}

	/**
	 * @return Returns the outofTownAddress2.
	 */
	public String getOutofTownAddress2() {
		return outofTownAddress2;
	}

	/**
	 * @param outofTownAddress2
	 *            The outofTownAddress2 to set.
	 */
	public void setOutofTownAddress2(String outofTownAddress2) {
		this.outofTownAddress2 = outofTownAddress2;
	}

	/**
	 * @return Returns the outofTownAddress3.
	 */
	public String getOutofTownAddress3() {
		return outofTownAddress3;
	}

	/**
	 * @param outofTownAddress3
	 *            The outofTownAddress3 to set.
	 */
	public void setOutofTownAddress3(String outofTownAddress3) {
		this.outofTownAddress3 = outofTownAddress3;
	}

	/**
	 * @return Returns the outofTownAddress4.
	 */
	public String getOutofTownAddress4() {
		return outofTownAddress4;
	}

	/**
	 * @param outofTownAddress4
	 *            The outofTownAddress4 to set.
	 */
	public void setOutofTownAddress4(String outofTownAddress4) {
		this.outofTownAddress4 = outofTownAddress4;
	}

	/**
	 * @return Returns the outofTownAddressCountry.
	 */
	public String getOutofTownAddressCountry() {
		return outofTownAddressCountry;
	}

	/**
	 * @param outofTownAddressCountry
	 *            The outofTownAddressCountry to set.
	 */
	public void setOutofTownAddressCountry(String outofTownAddressCountry) {
		this.outofTownAddressCountry = outofTownAddressCountry;
	}

	/**
	 * @return Returns the outofTownAddressState.
	 */
	public String getOutofTownAddressState() {
		return outofTownAddressState;
	}

	/**
	 * @param outofTownAddressState
	 *            The outofTownAddressState to set.
	 */
	public void setOutofTownAddressState(String outofTownAddressState) {
		this.outofTownAddressState = outofTownAddressState;
	}

	/**
	 * @return Returns the outofTownAddressZip.
	 */
	public String getOutofTownAddressZip() {
		return outofTownAddressZip;
	}

	/**
	 * @param outofTownAddressZip
	 *            The outofTownAddressZip to set.
	 */
	public void setOutofTownAddressZip(String outofTownAddressZip) {
		this.outofTownAddressZip = outofTownAddressZip;
	}

	/**
	 * @return Returns the outofTownAddressZip4.
	 */
	public String getOutofTownAddressZip4() {
		return outofTownAddressZip4;
	}

	/**
	 * @param outofTownAddressZip4
	 *            The outofTownAddressZip4 to set.
	 */
	public void setOutofTownAddressZip4(String outofTownAddressZip4) {
		this.outofTownAddressZip4 = outofTownAddressZip4;

	}

	/**
	 * @return Returns the lso_id.
	 */
	public int getLso_id() {
		return lso_id;
	}

	/**
	 * @param lso_id
	 *            The lso_id to set.
	 */
	public void setLso_id(int lso_id) {
		this.lso_id = lso_id;
	}

	/**
	 * @return Returns the activity_id.
	 */
	public int getActivity_id() {
		return activity_id;
	}

	/**
	 * @param activity_id
	 *            The activity_id to set.
	 */
	public void setActivity_id(int activity_id) {
		this.activity_id = activity_id;
	}

	/**
	 * @return Returns the businessOwnerResults.
	 */
	public String getBusinessOwnerResults() {
		return businessOwnerResults;
	}

	/**
	 * @param businessOwnerResults
	 *            The businessOwnerResults to set.
	 */
	public void setBusinessOwnerResults(String businessOwnerResults) {
		this.businessOwnerResults = businessOwnerResults;
	}
}

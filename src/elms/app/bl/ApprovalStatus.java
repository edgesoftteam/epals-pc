package elms.app.bl;

public class ApprovalStatus {

	private int id;

	private String description;

	/**
	 * Constructor for ActivityStatus
	 */
	public ApprovalStatus() {

	}

	/**
	 * Constructor for ActivityStatus
	 */
	public ApprovalStatus(int id, String description) {
		this.id = id;

		this.description = description;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
}

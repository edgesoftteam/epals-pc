/*
 * Created on Jan 7, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package elms.app.common;

/**
 * @author Anand Belaguly (anand@edgesoftinc.com) updated - Jan 7, 2004,10:44:50 AM - Anand Belaguly
 */
public class AttachmentResult {

	public String attachmentId;
	public String levelId;
	public String attachmentLevel;
	public String fileName;
	public String description;
	public String location;
	public String status;
	public String fileUrl;

	/**
	 * @return
	 */
	public String getAttachmentId() {
		return attachmentId;
	}

	/**
	 * @return
	 */
	public String getAttachmentLevel() {
		return attachmentLevel;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return
	 */
	public String getLevelId() {
		return levelId;
	}

	/**
	 * @return
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param string
	 */
	public void setAttachmentId(String string) {
		attachmentId = string;
	}

	/**
	 * @param string
	 */
	public void setAttachmentLevel(String string) {
		attachmentLevel = string;
	}

	/**
	 * @param string
	 */
	public void setDescription(String string) {
		description = string;
	}

	/**
	 * @param string
	 */
	public void setFileName(String string) {
		fileName = string;
	}

	/**
	 * @param string
	 */
	public void setLevelId(String string) {
		levelId = string;
	}

	/**
	 * @param string
	 */
	public void setLocation(String string) {
		location = string;
	}

	/**
	 * @param string
	 */
	public void setStatus(String string) {
		status = string;
	}

	/**
	 * @return
	 */
	public String getFileUrl() {
		return fileUrl;
	}

	/**
	 * @param string
	 */
	public void setFileUrl(String string) {
		fileUrl = string;
	}

}

//Source file: C:\\datafile\\source\\elms\\app\\common\\Condition.java
package elms.app.common;

import java.util.Calendar;
import java.util.Date;

import elms.security.User;

/**
 * @author Shekhar Jain
 */
public class Condition {
	protected int conditionId;
	protected int levelId;
	protected int libraryId;
	protected String conditionCode;
	protected String conditionLevel;
	protected String shortText;
	protected String text;
	protected String inspectable;
	protected String warning;
	protected String dpa;
	protected String complete;
	protected String required;
	protected int createdBy;
	protected Calendar createdDate;
	protected int updatedBy;
	protected Calendar updatedDate;
	protected int approvedBy;
	protected Calendar approvedDate;
	
	private String citation1;
    private String citation2;
    private String citation3;
	private String citation4;
	
	private String citation1Comment;
    private String citation2Comment;
    private String citation3Comment;
    private String citation4Comment;

    private String createDateStr;
    private String progType;
    private String conditionGroup;

	/**
	 * @param conditionGroup the conditionGroup to set
	 */
	public void setConditionGroup(String conditionGroup) {
		this.conditionGroup = conditionGroup;
	}

	/**
	 * @roseuid 3CD214F80391
	 */
	public Condition() {
		User user = new User();
	}

	// public Condition(int conditionId, int levelId, String conditionLevel, String shortText)

	// {
	// this.conditionId = conditionId;
	// this.levelId = levelId;
	// this.conditionLevel = conditionLevel;
	// this.shortText = shortText;
	// }

	public Condition(int conditionId, int levelId, int libraryId, String conditionLevel, String shortText, String text, String inspectable, String warning, String dpa, String complete, int createdBy, Calendar createdDate, int updatedBy, Calendar updatedDate, int approvedBy, Calendar approvedDate, String conditionCode) {
		this.conditionId = conditionId;
		this.levelId = levelId;
		this.libraryId = libraryId;
		this.conditionLevel = conditionLevel;
		this.shortText = shortText;
		this.text = text;
		this.inspectable = inspectable;
		this.warning = warning;
		this.dpa = dpa;
		this.complete = complete;
		this.createdBy = createdBy;
		this.createdDate = Calendar.getInstance();
		this.updatedBy = updatedBy;
		this.updatedDate = updatedDate;
		this.approvedBy = approvedBy;
		this.approvedDate = approvedDate;
		this.conditionCode = conditionCode;
	}

	/**
	 * Gets the conditionId
	 * 
	 * @return Returns a int
	 */
	public int getConditionId() {
		return conditionId;
	}

	/**
	 * Sets the conditionId
	 * 
	 * @param conditionId
	 *            The conditionId to set
	 */
	public void setConditionId(int conditionId) {
		this.conditionId = conditionId;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the conditionLevel
	 * 
	 * @return Returns a String
	 */
	public String getConditionLevel() {
		return conditionLevel;
	}

	/**
	 * Sets the conditionLevel
	 * 
	 * @param conditionLevel
	 *            The conditionLevel to set
	 */
	public void setConditionLevel(String conditionLevel) {
		this.conditionLevel = conditionLevel;
	}

	/**
	 * Gets the shortText
	 * 
	 * @return Returns a String
	 */
	public String getShortText() {
		return shortText;
	}

	/**
	 * Sets the shortText
	 * 
	 * @param shortText
	 *            The shortText to set
	 */
	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	/**
	 * Gets the text
	 * 
	 * @return Returns a String
	 */
	public String getText() {
		return text;
	}

	/**
	 * Sets the text
	 * 
	 * @param text
	 *            The text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Gets the inspectable
	 * 
	 * @return Returns a String
	 */
	public String getInspectable() {
		return inspectable;
	}

	/**
	 * Sets the inspectable
	 * 
	 * @param inspectable
	 *            The inspectable to set
	 */
	public void setInspectable(String inspectable) {
		this.inspectable = inspectable;
	}

	/**
	 * Gets the warning
	 * 
	 * @return Returns a String
	 */
	public String getWarning() {
		return warning;
	}

	/**
	 * Sets the warning
	 * 
	 * @param warning
	 *            The warning to set
	 */
	public void setWarning(String warning) {
		this.warning = warning;
	}

	/**
	 * Gets the dpa
	 * 
	 * @return Returns a String
	 */
	public String getDpa() {
		return dpa;
	}

	/**
	 * Sets the dpa
	 * 
	 * @param dpa
	 *            The dpa to set
	 */
	public void setDpa(String dpa) {
		this.dpa = dpa;
	}

	/**
	 * Gets the complete
	 * 
	 * @return Returns a String
	 */
	public String getComplete() {
		return complete;
	}

	/**
	 * Sets the complete
	 * 
	 * @param complete
	 *            The complete to set
	 */
	public void setComplete(String complete) {
		this.complete = complete;
	}

	/**
	 * Gets the createdDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreatedDate() {
		return createdDate;
	}

	/**
	 * Sets the createdDate
	 * 
	 * @param createdDate
	 *            The createdDate to set
	 */
	public void setCreatedDate(Calendar createdDate) {
		this.createdDate = createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		if (createdDate == null)
			this.createdDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(createdDate);
			this.createdDate = calendar;
		}
	}

	/**
	 * Gets the updatedDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdatedDate() {
		return updatedDate;
	}

	/**
	 * Sets the updatedDate
	 * 
	 * @param updatedDate
	 *            The updatedDate to set
	 */
	public void setUpdatedDate(Calendar updatedDate) {
		this.updatedDate = updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		if (updatedDate == null)
			this.updatedDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updatedDate);
			this.updatedDate = calendar;
		}
	}

	/**
	 * Gets the approvedDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getApprovedDate() {
		return approvedDate;
	}

	/**
	 * Sets the approvedDate
	 * 
	 * @param approvedDate
	 *            The approvedDate to set
	 */
	public void setApprovedDate(Calendar approvedDate) {
		this.approvedDate = approvedDate;
	}

	public void setApprovedDate(Date approvedDate) {
		if (approvedDate == null)
			this.approvedDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(approvedDate);
			this.approvedDate = calendar;
		}
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a int
	 */
	public int getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the approvedBy
	 * 
	 * @return Returns a int
	 */
	public int getApprovedBy() {
		return approvedBy;
	}

	/**
	 * Sets the approvedBy
	 * 
	 * @param approvedBy
	 *            The approvedBy to set
	 */
	public void setApprovedBy(int approvedBy) {
		this.approvedBy = approvedBy;
	}

	public String toString() {
		return ("Condition Object " + conditionId + "--" + levelId + "--" + conditionLevel + "--" + shortText + "--" + text + "--" + inspectable + "--" + warning + "--" + dpa + "--" + complete + "--" + createdBy + "--" + createdDate + "--" + updatedBy + "--" + updatedDate + "--" + approvedBy + "--" + approvedDate);
	}

	/**
	 * Gets the libraryId
	 * 
	 * @return Returns a int
	 */
	public int getLibraryId() {
		return libraryId;
	}

	/**
	 * Sets the libraryId
	 * 
	 * @param libraryId
	 *            The libraryId to set
	 */
	public void setLibraryId(int libraryId) {
		this.libraryId = libraryId;
	}

	/**
	 * Gets the conditionCode
	 * 
	 * @return Returns a String
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * Sets the conditionCode
	 * 
	 * @param conditionCode
	 *            The conditionCode to set
	 */
	public void setConditionCode(String conditionCode) {
		this.conditionCode = conditionCode;
	}

	/**
	 * Returns the required.
	 * 
	 * @return String
	 */
	public String getRequired() {
		return required;
	}

	/**
	 * Sets the required.
	 * 
	 * @param required
	 *            The required to set
	 */
	public void setRequired(String required) {
		this.required = required;
	}

	 /**
		 * @return the citation1
		 */
		public String getCitation1() {
			return citation1;
		}

		/**
		 * @param citation1 the citation1 to set
		 */
		public void setCitation1(String citation1) {
			this.citation1 = citation1;
		}

		/**
		 * @return the citation2
		 */
		public String getCitation2() {
			return citation2;
		}

		/**
		 * @param citation2 the citation2 to set
		 */
		public void setCitation2(String citation2) {
			this.citation2 = citation2;
		}

		/**
		 * @return the citation3
		 */
		public String getCitation3() {
			return citation3;
		}

		/**
		 * @param citation3 the citation3 to set
		 */
		public void setCitation3(String citation3) {
			this.citation3 = citation3;
		}

		/**
		 * @return the citation4
		 */
		public String getCitation4() {
			return citation4;
		}

		/**
		 * @param citation4 the citation4 to set
		 */
		public void setCitation4(String citation4) {
			this.citation4 = citation4;
		}

		/**
		 * @return the citation1Comment
		 */
		public String getCitation1Comment() {
			return citation1Comment;
		}

		/**
		 * @param citation1Comment the citation1Comment to set
		 */
		public void setCitation1Comment(String citation1Comment) {
			this.citation1Comment = citation1Comment;
		}

		/**
		 * @return the citation2Comment
		 */
		public String getCitation2Comment() {
			return citation2Comment;
		}

		/**
		 * @param citation2Comment the citation2Comment to set
		 */
		public void setCitation2Comment(String citation2Comment) {
			this.citation2Comment = citation2Comment;
		}

		/**
		 * @return the citation3Comment
		 */
		public String getCitation3Comment() {
			return citation3Comment;
		}

		/**
		 * @param citation3Comment the citation3Comment to set
		 */
		public void setCitation3Comment(String citation3Comment) {
			this.citation3Comment = citation3Comment;
		}

		/**
		 * @return the citation4Comment
		 */
		public String getCitation4Comment() {
			return citation4Comment;
		}

		/**
		 * @param citation4Comment the citation4Comment to set
		 */
		public void setCitation4Comment(String citation4Comment) {
			this.citation4Comment = citation4Comment;
		}
		/**
		 * @return the createDateStr
		 */
		public String getCreateDateStr() {
			return createDateStr;
		}

		/**
		 * @param createDateStr the createDateStr to set
		 */
		public void setCreateDateStr(String createDateStr) {
			this.createDateStr = createDateStr;
		}

		/**
		 * @return the progType
		 */
		public String getProgType() {
			return progType;
		}

		/**
		 * @param progType the progType to set
		 */
		public void setProgType(String progType) {
			this.progType = progType;
		}

		/**
		 * @return the conditionGroup
		 */
		public String getConditionGroup() {
			return conditionGroup;
		}

}

package elms.app.common;

import java.util.Calendar;

/**
 * @author siva kumari
 */
public class Tenant {
	private String tenantId;
	private String name;
	private Calendar updateDate;
	private int updatedBy;

	public Tenant() {

	}

	public Tenant(String tenantId, String name, int enteredBy, Calendar updateDate) {
		this.tenantId = tenantId;
		this.name = name;
		this.updateDate = this.updateDate;
		this.updatedBy = enteredBy;
	}

	
	public String toString() {
		return ("Hold Object " + tenantId + "--" + name + "--" + updateDate + "--" + updatedBy);
	}

	/**
	 * @return the tenantId
	 */
	public String getTenantId() {
		return tenantId;
	}

	/**
	 * @param tenantId the tenantId to set
	 */
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the updateDate
	 */
	public Calendar getUpdateDate() {
		return updateDate;
	}

	/**
	 * @param updateDate the updateDate to set
	 */
	public void setUpdateDate(Calendar updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * @return the updatedBy
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	
}

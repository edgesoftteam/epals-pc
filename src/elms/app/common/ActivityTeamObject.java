package elms.app.common;

public class ActivityTeamObject {

	public ActivityTeamObject() {
	}

	public ActivityTeamObject(String checkRemove, String name, String title, String lead) {
		this.checkRemove = checkRemove;
		this.name = name;
		this.title = title;
		this.lead = lead;
	}

	public String getCheckRemove() {
		return checkRemove;
	}

	public String getLead() {
		return lead;
	}

	public String getName() {
		return name;
	}

	public String getTitle() {
		return title;
	}

	public void setCheckRemove(String string) {
		checkRemove = string;
	}

	public void setLead(String string) {
		lead = string;
	}

	public void setName(String string) {
		name = string;
	}

	public void setTitle(String string) {
		title = string;
	}

	protected String checkRemove;
	protected String name;
	protected String title;
	protected String lead;
}

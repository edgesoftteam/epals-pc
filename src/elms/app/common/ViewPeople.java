package elms.app.common;

import elms.app.admin.PeopleType;

public class ViewPeople {

	public ViewPeople() {
		name = "";
		licenseNbr = "";
		phoneNbr = "";
		onHold = "N";
	}

	public ViewPeople(PeopleType peopleType, String name, String licenseNbr, String phoneNbr, String onHold, String levelType) {
		this.name = "";
		this.licenseNbr = "";
		this.phoneNbr = "";
		this.onHold = "N";
		this.peopleType = peopleType;
		this.name = name;
		this.licenseNbr = licenseNbr;
		this.onHold = onHold;
		this.phoneNbr = phoneNbr;
		this.levelType = levelType;
	}

	public String getLicenseNbr() {
		return licenseNbr;
	}

	public String getName() {
		return name;
	}

	public String getOnHold() {
		return onHold;
	}

	public PeopleType getPeopleType() {
		return peopleType;
	}

	public String getPhoneNbr() {
		return phoneNbr;
	}

	public void setLicenseNbr(String string) {
		licenseNbr = string;
	}

	public void setName(String string) {
		name = string;
	}

	public void setOnHold(String string) {
		onHold = string;
	}

	public void setPeopleType(PeopleType type) {
		peopleType = type;
	}

	public void setPhoneNbr(String string) {
		phoneNbr = string;
	}

	public String getLevelType() {
		return levelType;
	}

	public void setLevelType(String string) {
		levelType = string;
	}

	protected PeopleType peopleType;
	protected String name;
	protected String licenseNbr;
	protected String phoneNbr;
	protected String onHold;
	protected String levelType;
}

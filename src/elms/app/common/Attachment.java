package elms.app.common;

import java.io.InputStream;
import java.util.Calendar;

/**
 * This class is a value object for Attachment.
 */
public class Attachment {

	public int attachmentId;
	public String attachmentLevel;
	public int levelId;
	public ObcFile file;
	public String description;
	public int enteredBy;
	public String deleted;
	public Calendar enterDate;
	public String keyword1;
	public String keyword2;
	public String keyword3;
	public String keyword4;
	private InputStream stream;
    private String attachmentType;
	private int attachmentTypeId;

    
	public Attachment() {

	}

	public Attachment(int attachmentId, String attachmentLevel, int levelId, ObcFile file, String description, Calendar enterDate, int enteredBy, String keyword1, String keyword2, String keyword3, String keyword4,String attachmentType) {

		this.attachmentId = attachmentId;
		this.attachmentLevel = attachmentLevel;
		this.levelId = levelId;
		this.file = file;
		this.description = description;
		this.enteredBy = enteredBy;
		this.enterDate = enterDate;
		this.keyword1 = keyword1;
		this.keyword2 = keyword2;
		this.keyword3 = keyword3;
		this.keyword4 = keyword4;
		this.attachmentType = attachmentType;
	}

	public Attachment(int attachmentId, String attachmentLevel, int levelId, ObcFile file, String description, Calendar enterDate, int enteredBy, String keyword1, String keyword2, String keyword3, String keyword4) {
		this.attachmentId = attachmentId;
		this.attachmentLevel = attachmentLevel;
		this.levelId = levelId;
		this.file = file;
		this.description = description;
		this.enteredBy = enteredBy;
		this.enterDate = enterDate;
		this.keyword1 = keyword1;
		this.keyword2 = keyword2;
		this.keyword3 = keyword3;
		this.keyword4 = keyword4;		
	}

	/**
	 * Gets the attachmentId
	 * 
	 * @return Returns a int
	 */
	public int getAttachmentId() {

		return attachmentId;
	}

	/**
	 * Sets the attachmentId
	 * 
	 * @param attachmentId
	 *            The attachmentId to set
	 */
	public void setAttachmentId(int attachmentId) {

		this.attachmentId = attachmentId;
	}

	/**
	 * Gets the attachmentLevel
	 * 
	 * @return Returns a String
	 */
	public String getAttachmentLevel() {

		return attachmentLevel;
	}

	/**
	 * Sets the attachmentLevel
	 * 
	 * @param attachmentLevel
	 *            The attachmentLevel to set
	 */
	public void setAttachmentLevel(String attachmentLevel) {

		this.attachmentLevel = attachmentLevel;
	}

	/**
	 * Gets the file
	 * 
	 * @return Returns a ObcFile
	 */
	public ObcFile getFile() {

		return file;
	}

	/**
	 * Sets the file
	 * 
	 * @param file
	 *            The file to set
	 */
	public void setFile(ObcFile file) {

		this.file = file;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {

		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {

		this.description = description;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {

		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {

		this.levelId = levelId;
	}

	/**
	 * Gets the enteredBy
	 * 
	 * @return Returns a int
	 */
	public int getEnteredBy() {

		return enteredBy;
	}

	/**
	 * Sets the enteredBy
	 * 
	 * @param enteredBy
	 *            The enteredBy to set
	 */
	public void setEnteredBy(int enteredBy) {

		this.enteredBy = enteredBy;
	}

	/**
	 * Gets the deleted
	 * 
	 * @return Returns a String
	 */
	public String getDeleted() {

		return deleted;
	}

	/**
	 * Sets the deleted
	 * 
	 * @param deleted
	 *            The deleted to set
	 */
	public void setDeleted(String deleted) {

		this.deleted = deleted;
	}

	/**
	 * Gets the enterDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getEnterDate() {

		return enterDate;
	}

	/**
	 * Sets the enterDate
	 * 
	 * @param enterDate
	 *            The enterDate to set
	 */
	public void setEnterDate(Calendar enterDate) {

		this.enterDate = enterDate;
	}

	/**
	 * @return
	 */
	public String getKeyword1() {

		return keyword1;
	}

	/**
	 * @return
	 */
	public String getKeyword2() {

		return keyword2;
	}

	/**
	 * @return
	 */
	public String getKeyword3() {

		return keyword3;
	}

	/**
	 * @return
	 */
	public String getKeyword4() {

		return keyword4;
	}

	/**
	 * @param string
	 */
	public void setKeyword1(String string) {

		keyword1 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword2(String string) {

		keyword2 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword3(String string) {

		keyword3 = string;
	}

	/**
	 * @param string
	 */
	public void setKeyword4(String string) {

		keyword4 = string;
	}
	public InputStream getStream() {
		return stream;
	}

	public void setStream(InputStream stream) {
		this.stream = stream;
	}

 /**
	 * @return the attachmentType
	 */
	public String getAttachmentType() {
		return attachmentType;
	}

	/**
	 * @param attachmentType the attachmentType to set
	 */
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}

	/**
	 * @return the attachmentTypeId
	 */
	public int getAttachmentTypeId() {
		return attachmentTypeId;
	}

	/**
	 * @param attachmentTypeId the attachmentTypeId to set
	 */
	public void setAttachmentTypeId(int attachmentTypeId) {
		this.attachmentTypeId = attachmentTypeId;
	}

}

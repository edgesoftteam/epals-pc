package elms.app.common;

import java.util.Calendar;

import elms.util.StringUtils;

/**
 * @author Shekhar Jain
 */
public class Comment {

	protected int commentId;
	protected String commentLevel;
	protected int levelId;
	protected String comment;
	protected String internal;
	private Calendar enterDate;
	private int enteredBy;
	private Calendar updateDate;
	private int updatedBy;
	private String displayEnterDate;
	private String enteredUserName;
	private String updateSubProject;
	private String updateActivities;

	/**
	 * @param user
	 * @param internal
	 * @param description
	 * @param date
	 * @roseuid 3CCF00B702A0
	 */
	public Comment(int aCommentId, String aCommentLevel, int aLevelId, String aComment, String aInternal, Calendar aEnterDate, int aEnteredBy, Calendar aUpdateDate, int aUpdatedBy) {
		this.commentId = aCommentId;
		this.commentLevel = aCommentLevel;
		this.levelId = aLevelId;
		this.comment = aComment;
		this.internal = aInternal;
		this.enterDate = aEnterDate;
		this.enteredBy = aEnteredBy;
		this.updateDate = aUpdateDate;
		this.updatedBy = aUpdatedBy;
	}

	/**
	 * @roseuid 3CCF00B20393
	 */
	public Comment() {

	}

	/**
	 * Gets the commentId
	 * 
	 * @return Returns a int
	 */
	public int getCommentId() {
		return commentId;
	}

	/**
	 * Sets the commentId
	 * 
	 * @param commentId
	 *            The commentId to set
	 */
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}

	/**
	 * Gets the commentLevel
	 * 
	 * @return Returns a String
	 */
	public String getCommentLevel() {
		return commentLevel;
	}

	/**
	 * Sets the commentLevel
	 * 
	 * @param commentLevel
	 *            The commentLevel to set
	 */
	public void setCommentLevel(String commentLevel) {
		this.commentLevel = commentLevel;
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the comment
	 * 
	 * @return Returns a String
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the comment
	 * 
	 * @param comment
	 *            The comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * Gets the internal
	 * 
	 * @return Returns a String
	 */
	public String getInternal() {
		return internal;
	}

	/**
	 * Sets the internal
	 * 
	 * @param internal
	 *            The internal to set
	 */
	public void setInternal(String internal) {
		this.internal = internal;
	}

	/**
	 * Gets the enterDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getEnterDate() {
		return enterDate;
	}

	/**
	 * Sets the enterDate
	 * 
	 * @param enterDate
	 *            The enterDate to set
	 */
	public void setEnterDate(Calendar enterDate) {
		this.enterDate = enterDate;
	}

	/**
	 * Gets the enteredBy
	 * 
	 * @return Returns a int
	 */
	public int getEnteredBy() {
		return enteredBy;
	}

	/**
	 * Sets the enteredBy
	 * 
	 * @param enteredBy
	 *            The enteredBy to set
	 */
	public void setEnteredBy(int enteredBy) {
		this.enteredBy = enteredBy;
	}

	/**
	 * Gets the updateDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the updateDate
	 * 
	 * @param updateDate
	 *            The updateDate to set
	 */
	public void setUpdateDate(Calendar updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a int
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(int updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return
	 */
	public String getDisplayEnterDate() {
		return StringUtils.cal2str(enterDate);
	}

	/**
	 * @param string
	 */
	public void setDisplayEnterDate(String string) {
		displayEnterDate = string;
	}

	/**
	 * @return
	 */
	public String getEnteredUserName() {
		return enteredUserName;
	}

	/**
	 * @param string
	 */
	public void setEnteredUserName(String string) {
		enteredUserName = string;
	}

	/**
	 * @return
	 */
	public String getUpdateActivities() {
		return updateActivities;
	}

	/**
	 * @return
	 */
	public String getUpdateSubProject() {
		return updateSubProject;
	}

	/**
	 * @param string
	 */
	public void setUpdateActivities(String string) {
		updateActivities = string;
	}

	/**
	 * @param string
	 */
	public void setUpdateSubProject(String string) {
		updateSubProject = string;
	}

}

//Source file: C:\\datafile\\source\\elms\\app\\common\\Agent.java

package elms.app.common;

import org.apache.log4j.Logger;

/**
 * @author Anand Belaguly
 * 
 *         This is the database update class of the land object, The corresponding action calls this agent with the object (land, structure or occupancy), based on the contents of that object, the database updations are carried out.
 */
public class LookupType {
	static Logger logger = Logger.getLogger(LookupType.class.getName());
	String type;
	String description;

	/**
	 * @roseuid 3CD975A70036
	 */
	public LookupType() {

	}

	/**
	 * Returns the description.
	 * 
	 * @return String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the type.
	 * 
	 * @return String
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the description.
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the type.
	 * 
	 * @param type
	 *            The type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

}
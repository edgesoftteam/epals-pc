package elms.app.common;

/**
 * @author Shekhar Jain
 */
public class AttachmentEdit {

	protected String description;
	protected String fileName;
	protected String location;
	protected int fileSize;
	protected String creationDate;
	protected String checked = "";

	public AttachmentEdit(String fileName, String description, int fileSize, String creationDate) {
		this.fileName = fileName;
		this.description = description;
		this.fileSize = fileSize;
		this.creationDate = creationDate;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the fileName
	 * 
	 * @return Returns a String
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the fileName
	 * 
	 * @param fileName
	 *            The fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * Gets the location
	 * 
	 * @return Returns a String
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Sets the location
	 * 
	 * @param location
	 *            The location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Gets the fileSize
	 * 
	 * @return Returns a int
	 */
	public int getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the fileSize
	 * 
	 * @param fileSize
	 *            The fileSize to set
	 */
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * Gets the creationDate
	 * 
	 * @return Returns a String
	 */
	public String getCreationDate() {
		return creationDate;
	}

	/**
	 * Sets the creationDate
	 * 
	 * @param creationDate
	 *            The creationDate to set
	 */
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * Gets the checked
	 * 
	 * @return Returns a String
	 */
	public String getChecked() {
		return checked;
	}

	/**
	 * Sets the checked
	 * 
	 * @param checked
	 *            The checked to set
	 */
	public void setChecked(String checked) {
		this.checked = checked;
	}

}
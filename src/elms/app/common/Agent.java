package elms.app.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import sun.jdbc.rowset.CachedRowSet;
import elms.app.lso.ObcTree;
import elms.util.db.Wrapper;

public class Agent {
	static Logger logger = Logger.getLogger(Agent.class.getName());
	public static final String GET_ACTIVE_HOLDS = "SELECT * FROM HOLDS H WHERE H.STAT ='A' AND H.UPDATE_DT IN (SELECT MAX(UPDATE_DT) FROM HOLDS WHERE LEVEL_ID=? AND HOLD_LEVEL=? GROUP BY HOLD_ID) ORDER BY HOLD_TYPE";

	public Agent() {
	}

	public CachedRowSet getLsoList(String lsoId) {
		CachedRowSet rs = null;
		Statement stmt = null;
		Connection con = null;
		try {
			Wrapper db = new Wrapper();
			con = db.getConnectionForPreparedStatementOnly();
			stmt = con.createStatement();
			rs = new CachedRowSet();

			String sql = "SELECT ADDR_ID,LSO_ID, STR_NO, CITY, STATE, ZIP, PRIMARY, STREET_NAME " + " FROM LSO_ADDRESS, V_STREET_LIST " + " WHERE LSO_ADDRESS.LSO_ID =" + lsoId + " AND " + " V_STREET_LIST.STREET_ID = LSO_ADDRESS.STREET_ID";
			rs.populate(stmt.executeQuery(sql));
			con.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			try {
				if (rs != null)
					rs.close();
				if (stmt != null)
					stmt.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				// ignored
			}
		}

		return rs;
	}

	/**
	 * updates the hold status.
	 * 
	 * @param lsoTree
	 * @param level
	 * @param lsoId
	 * @return
	 */

	public void getHoldInfo(List psaTreeList) {
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;
		try {

			connection = new Wrapper().getConnectionForPreparedStatementOnly();
			statement = connection.prepareStatement(GET_ACTIVE_HOLDS);
			logger.debug(GET_ACTIVE_HOLDS);
			
			Iterator itr = psaTreeList.iterator();
			while (itr.hasNext()) {
				ObcTree tree = (ObcTree) itr.next();
				statement.setLong(1, tree.getId());
				statement.setString(2, tree.getType());
				result = statement.executeQuery();
				if (result.next()) {
					logger.debug("found hold on Psa Id " + tree.getId());
					tree.setOnHold(true);
					tree.setHoldType(result.getString("HOLD_TYPE"));
				} else {
					tree.setOnHold(false);
				}
				if (result != null)
					result.close();
			}
			if (statement != null)
				statement.close();
			if (connection != null)
				connection.close();

		} catch (Exception e) {
			logger.error("Error in updateHoldStatus() " + e.getMessage());
		}
		finally {
			try {
				if (result != null)
					result.close();
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (Exception e) {
				// ignored
			}
		}
		
	}
}

//Source file: C:\\datafile\\source\\elms\\app\\lso\\Street.java

package elms.app.common;

/**
 * @author Shekhar Jain
 */
public class Names {
	protected int userId;
	protected String firstName;
	protected String lastName;
	protected String name;

	public Names(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public Names(int aUserId, String aName) {
		this.userId = aUserId;
		this.name = aName;
	}

	/**
	 * @roseuid 3CCDCBA7036F
	 */
	public Names() {

	}

	/**
	 * Gets the firstName
	 * 
	 * @return Returns a String
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the firstName
	 * 
	 * @param firstName
	 *            The firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the lastName
	 * 
	 * @return Returns a String
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName
	 * 
	 * @param lastName
	 *            The lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Returns the name.
	 * 
	 * @return String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the userId.
	 * 
	 * @return int
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the userId.
	 * 
	 * @param userId
	 *            The userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

}

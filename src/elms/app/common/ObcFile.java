package elms.app.common;

import java.util.Calendar;
import java.util.Date;

import org.apache.struts.upload.FormFile;

/**
 * @author Shekhar Jain
 */

public class ObcFile implements java.io.Serializable {
	protected String fileName;
	protected String fileLocation;
	protected String fileSize;
	protected Calendar createDate;
	protected String strCreateDate;
	protected FormFile theFile;

	public ObcFile(String aFileName, String aFileLocation, String aFileSize, Calendar aCreateDate) {
		this.fileName = aFileName;
		this.fileLocation = aFileLocation;
		this.fileSize = aFileSize;
		this.createDate = aCreateDate;
	}

	/**
     */
	public ObcFile() {

	}

	/**
	 * Access method for the fileName property.
	 * 
	 * @return the current value of the fileName property
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * Sets the value of the fileName property.
	 * 
	 * @param aFileName
	 *            the new value of the fileName property
	 */
	public void setFileName(String aFileName) {
		fileName = aFileName;
	}

	/**
	 * Access method for the fileLocation property.
	 * 
	 * @return the current value of the fileLocation property
	 */
	public String getFileLocation() {
		return fileLocation;
	}

	/**
	 * Sets the value of the fileLocation property.
	 * 
	 * @param aFileLocation
	 *            the new value of the fileLocation property
	 */
	public void setFileLocation(String aFileLocation) {
		fileLocation = aFileLocation;
	}

	/**
	 * Access method for the fileSize property.
	 * 
	 * @return the current value of the fileSize property
	 */
	public String getFileSize() {
		return fileSize;
	}

	/**
	 * Sets the value of the fileSize property.
	 * 
	 * @param aFileSize
	 *            the new value of the fileSize property
	 */
	public void setFileSize(String aFileSize) {
		fileSize = aFileSize;
	}

	/**
	 * Gets the createDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreateDate() {
		return createDate;
	}

	/**
	 * Sets the createDate
	 * 
	 * @param createDate
	 *            The createDate to set
	 */
	public void setCreateDate(Calendar createDate) {
		this.createDate = createDate;
	}

	public void setCreateDate(Date createDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(createDate);
		this.createDate = calendar;
	}

	/**
	 * Gets the theFile
	 * 
	 * @return Returns a FormFile
	 */
	public FormFile getTheFile() {
		return theFile;
	}

	/**
	 * Sets the theFile
	 * 
	 * @param theFile
	 *            The theFile to set
	 */
	public void setTheFile(FormFile theFile) {
		this.theFile = theFile;
	}

	/**
	 * Gets the strCreateDate
	 * 
	 * @return Returns a String
	 */
	public String getStrCreateDate() {
		return strCreateDate;
	}

	/**
	 * Sets the strCreateDate
	 * 
	 * @param strCreateDate
	 *            The strCreateDate to set
	 */
	public void setStrCreateDate(String strCreateDate) {
		this.strCreateDate = strCreateDate;
	}

}

//Source file: C:\\Datafile\\OBC\\war\\src\\main\\classes\\elms\\app\\common\\Hold.java

package elms.app.common;

import java.util.Calendar;

/**
 * @author Shekhar Jain
 */
public class Hold {
	private int holdId;
	private String holdLevel;
	private int levelId;
	private String type;
	private String title;
	private String comment;
	private String status;
	private Calendar enterDate;
	private int enteredBy;
	private Calendar updateDate;
	private int updatedBy;
	private Calendar statusDate;

	/**
	 * @roseuid 3CD972510146
	 */
	public Hold() {

	}

	public Hold(int holdId, String holdLevel, int levelId, String holdType, int enteredBy, String status, String title, String comment) {
		this.holdId = holdId;
		this.holdLevel = holdLevel;
		this.levelId = levelId;
		this.type = holdType;
		this.status = status;
		this.title = title;
		this.comment = comment;
		this.enterDate = Calendar.getInstance();
		this.enteredBy = enteredBy;
		this.updateDate = this.enterDate;
		this.updatedBy = enteredBy;
	}

	/**
	 * Access method for the holdId property.
	 * 
	 * @return the current value of the holdId property
	 */
	public int getHoldId() {
		return holdId;
	}

	/**
	 * Sets the value of the holdId property.
	 * 
	 * @param aHoldId
	 *            the new value of the holdId property
	 */
	public void setHoldId(int aHoldId) {
		holdId = aHoldId;
	}

	/**
	 * Access method for the holdLevel property.
	 * 
	 * @return the current value of the holdLevel property
	 */
	public String getHoldLevel() {
		return holdLevel;
	}

	/**
	 * Sets the value of the holdLevel property.
	 * 
	 * @param aHoldLevel
	 *            the new value of the holdLevel property
	 */
	public void setHoldLevel(String aHoldLevel) {
		holdLevel = aHoldLevel;
	}

	/**
	 * Access method for the type property.
	 * 
	 * @return the current value of the type property
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the value of the type property.
	 * 
	 * @param aType
	 *            the new value of the type property
	 */
	public void setType(String aType) {
		type = aType;
	}

	/**
	 * Access method for the title property.
	 * 
	 * @return the current value of the title property
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the value of the title property.
	 * 
	 * @param aTitle
	 *            the new value of the title property
	 */
	public void setTitle(String aTitle) {
		title = aTitle;
	}

	/**
	 * Access method for the comment property.
	 * 
	 * @return the current value of the comment property
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * Sets the value of the comment property.
	 * 
	 * @param aComment
	 *            the new value of the comment property
	 */
	public void setComment(String aComment) {
		comment = aComment;
	}

	/**
	 * Access method for the status property.
	 * 
	 * @return the current value of the status property
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the value of the status property.
	 * 
	 * @param aStatus
	 *            the new value of the status property
	 */
	public void setStatus(String aStatus) {
		status = aStatus;
	}

	/**
	 * Access method for the enterDate property.
	 * 
	 * @return the current value of the enterDate property
	 */
	public Calendar getEnterDate() {
		return enterDate;
	}

	/**
	 * Sets the value of the enterDate property.
	 * 
	 * @param aEnterDate
	 *            the new value of the enterDate property
	 */
	public void setEnterDate(Calendar aEnterDate) {
		enterDate = aEnterDate;
	}

	/**
	 * Access method for the enteredBy property.
	 * 
	 * @return the current value of the enteredBy property
	 */
	public int getEnteredBy() {
		return enteredBy;
	}

	/**
	 * Sets the value of the enteredBy property.
	 * 
	 * @param aEnteredBy
	 *            the new value of the enteredBy property
	 */
	public void setEnteredBy(int aEnteredBy) {
		enteredBy = aEnteredBy;
	}

	/**
	 * Access method for the updateDate property.
	 * 
	 * @return the current value of the updateDate property
	 */
	public Calendar getUpdateDate() {
		return updateDate;
	}

	/**
	 * Sets the value of the updateDate property.
	 * 
	 * @param aUpdateDate
	 *            the new value of the updateDate property
	 */
	public void setUpdateDate(Calendar aUpdateDate) {
		updateDate = aUpdateDate;
	}

	/**
	 * Access method for the updatedBy property.
	 * 
	 * @return the current value of the updatedBy property
	 */
	public int getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the value of the updatedBy property.
	 * 
	 * @param aUpdatedBy
	 *            the new value of the updatedBy property
	 */
	public void setUpdatedBy(int aUpdatedBy) {
		updatedBy = aUpdatedBy;
	}

	public String toString() {
		return ("Hold Object " + holdId + "--" + holdLevel + "--" + levelId + "--" + type + "--" + title + "--" + comment + "--" + status + "--" + enterDate + "--" + enteredBy + "--" + updateDate + "--" + updatedBy);
	}

	/**
	 * Gets the levelId
	 * 
	 * @return Returns a int
	 */
	public int getLevelId() {
		return levelId;
	}

	/**
	 * Sets the levelId
	 * 
	 * @param levelId
	 *            The levelId to set
	 */
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}

	/**
	 * Gets the statusDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getStatusDate() {
		return statusDate;
	}

	/**
	 * Sets the statusDate
	 * 
	 * @param statusDate
	 *            The statusDate to set
	 */
	public void setStatusDate(Calendar statusDate) {
		this.statusDate = statusDate;
	}

}

package elms.app.project;

public class PlanCheckProcessType {
	
	private int processTypeId;
	private String processType;
	
	public PlanCheckProcessType(){
		
	}
	public PlanCheckProcessType(int processTypeId, String processType) {
		this.processTypeId = processTypeId;
		this.processType = processType;
	}
	public int getProcessTypeId() {
		return processTypeId;
	}
	public void setProcessTypeId(int processTypeId) {
		this.processTypeId = processTypeId;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	
	

}
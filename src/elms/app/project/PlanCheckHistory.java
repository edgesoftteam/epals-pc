package elms.app.project;

import java.util.Calendar;

import elms.security.User;

public class PlanCheckHistory {
	private int pcHistoryId;
	private int planCheckId;
	private String statusDesc;
	private String deptDesc;
	private Calendar planCheckDate;
	private String date;
	private Calendar targetDate;
    private String trgdate;
    private String compdate;
    private Calendar completionDate;
    private String engineerName;
    private String comments;
	public int getPcHistoryId() {
		return pcHistoryId;
	}
	public void setPcHistoryId(int pcHistoryId) {
		this.pcHistoryId = pcHistoryId;
	}
	public int getPlanCheckId() {
		return planCheckId;
	}
	public void setPlanCheckId(int planCheckId) {
		this.planCheckId = planCheckId;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getDeptDesc() {
		return deptDesc;
	}
	public void setDeptDesc(String deptDesc) {
		this.deptDesc = deptDesc;
	}
	public Calendar getPlanCheckDate() {
		return planCheckDate;
	}
	public void setPlanCheckDate(Calendar planCheckDate) {
		this.planCheckDate = planCheckDate;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Calendar getTargetDate() {
		return targetDate;
	}
	public void setTargetDate(Calendar targetDate) {
		this.targetDate = targetDate;
	}
	public String getTrgdate() {
		return trgdate;
	}
	public void setTrgdate(String trgdate) {
		this.trgdate = trgdate;
	}
	public String getCompdate() {
		return compdate;
	}
	public void setCompdate(String compdate) {
		this.compdate = compdate;
	}
	public Calendar getCompletionDate() {
		return completionDate;
	}
	public void setCompletionDate(Calendar completionDate) {
		this.completionDate = completionDate;
	}
	public String getEngineerName() {
		return engineerName;
	}
	public void setEngineerName(String engineerName) {
		this.engineerName = engineerName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
    
    
	
    
    
}

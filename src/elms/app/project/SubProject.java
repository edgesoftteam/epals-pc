//Source file: C:\\datafile\\source\\elms\\app\\project\\SubProject.java

package elms.app.project;

import java.util.List;

import elms.app.finance.SubProjectFinance;
import elms.app.planning.PlanningDetails;

public class SubProject extends Psa {
	protected int subProjectId;
	protected int projectId;
	protected SubProjectDetail subProjectDetail;
	protected SubProjectFinance subProjectFinance;
	protected PlanningDetails planningDetails;

	protected List activity;

	/**
	 * 3CC746950139
	 */
	public SubProject() {

	}

	public SubProject(int subProjectId, SubProjectDetail subProjectDetail) {
		this.subProjectId = subProjectId;
		this.subProjectDetail = subProjectDetail;
	}

	public SubProject(int aSubProjectId, int aProjectId, SubProjectDetail aSubProjectDetail, SubProjectFinance aSubProjectFinance, List aActivity, List aSubProjectHold, List aSubProjectCondition, List aSubProjectAttachment, List aSubProjectComment) {
		super(aSubProjectHold, aSubProjectCondition, aSubProjectAttachment, aSubProjectComment);
		this.subProjectId = aSubProjectId;
		this.projectId = aProjectId;
		this.subProjectDetail = aSubProjectDetail;
		this.activity = aActivity;
		this.subProjectFinance = aSubProjectFinance;
	}

	/**
	 * Gets the subProjectDetail
	 * 
	 * @return Returns a SubProjectDetail
	 */
	public SubProjectDetail getSubProjectDetail() {
		return subProjectDetail;
	}

	/**
	 * Sets the subProjectDetail
	 * 
	 * @param subProjectDetail
	 *            The subProjectDetail to set
	 */
	public void setSubProjectDetail(SubProjectDetail subProjectDetail) {
		this.subProjectDetail = subProjectDetail;
	}

	/**
	 * Gets the subProjectFinance
	 * 
	 * @return Returns a SubProjectFinance
	 */
	public SubProjectFinance getSubProjectFinance() {
		return subProjectFinance;
	}

	/**
	 * Sets the subProjectFinance
	 * 
	 * @param subProjectFinance
	 *            The subProjectFinance to set
	 */
	public void setSubProjectFinance(SubProjectFinance subProjectFinance) {
		this.subProjectFinance = subProjectFinance;
	}

	/**
	 * Gets the activity
	 * 
	 * @return Returns a List
	 */
	public List getActivity() {
		return activity;
	}

	/**
	 * Sets the activity
	 * 
	 * @param activity
	 *            The activity to set
	 */
	public void setActivity(List activity) {
		this.activity = activity;
	}

	/**
	 * Gets the projectId
	 * 
	 * @return Returns a int
	 */
	public int getProjectId() {
		return projectId;
	}

	/**
	 * Sets the projectId
	 * 
	 * @param projectId
	 *            The projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	/**
	 * Gets the subProjectId
	 * 
	 * @return Returns a int
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * Sets the subProjectId
	 * 
	 * @param subProjectId
	 *            The subProjectId to set
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * @return
	 */
	public PlanningDetails getPlanningDetails() {
		return planningDetails;
	}

	/**
	 * @param details
	 */
	public void setPlanningDetails(PlanningDetails details) {
		planningDetails = details;
	}

}
package elms.app.project;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivitySubType;
import elms.app.admin.ActivityType;
import elms.app.admin.ParkingZone;
import elms.security.User;
import elms.util.StringUtils;

public class ActivityDetail {
	static Logger logger = Logger.getLogger(ActivityDetail.class.getName());
	private String activityNumber;
	private ActivityType activityType;
	private ActivitySubType activitySubType;
	private String description;
	private double valuation;
	private ActivityStatus status;
	private Calendar startDate;
	private Calendar completionDate;
	private Calendar activityFeeDate;
	private Calendar planCheckFeeDate;
	private Calendar permitFeeDate;
	private Calendar developmentFeeDate;
	private Calendar appliedDate;
	private Calendar issueDate;
	private Calendar expirationDate;
	private Calendar finaledDate;
	private String planCheckRequired;
	private String developmentFeeRequired;
	private String inspectionRequired;
	private String microfilm;
	private User createdBy;
	private Calendar created;
	private User updatedBy;
	private Calendar updated;
	private int lsoId;
	private String finalDateLabel;
	private String expirationDateLabel;
	private int addressId;
	private String address;
	private String ownerName;
	private List activitySubTypes;
	private String newUnits;
	private String existingUnits;
	private String demolishedUnits;
	private String multiAddress;
	protected String label;
	protected ParkingZone parkingZone;
	private String greenHalo;
	private String onlineApplication;
	protected String extRefNumber;
	
	public ParkingZone getParkingZone() {
		return parkingZone;
	}

	public void setParkingZone(ParkingZone parkingZone) {
		this.parkingZone = parkingZone;
	}

	public ActivityDetail() {
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getMultiAddress() {
		return multiAddress;
	}

	public void setMultiAddress(String multiAddress) {
		this.multiAddress = multiAddress;
	}

	/**
	 * Gets the activityNumber
	 * 
	 * @return Returns a String
	 */
	public String getActivityNumber() {
		return activityNumber;
	}

	/**
	 * Sets the activityNumber
	 * 
	 * @param activityNumber
	 *            The activityNumber to set
	 */
	public void setActivityNumber(String activityNumber) {
		this.activityNumber = activityNumber;
	}

	/**
	 * Gets the planCheckRequired
	 * 
	 * @return Returns a String
	 */
	public String getPlanCheckRequired() {
		return planCheckRequired;
	}

	/**
	 * Sets the planCheckRequired
	 * 
	 * @param planCheckRequired
	 *            The planCheckRequired to set
	 */
	public void setPlanCheckRequired(String planCheckRequired) {
		this.planCheckRequired = planCheckRequired;
	}

	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the valuation
	 * 
	 * @return Returns a float
	 */
	public double getValuation() {
		return valuation;
	}

	/**
	 * Sets the valuation
	 * 
	 * @param valuation
	 *            The valuation to set
	 */
	public void setValuation(double valuation) {
		this.valuation = valuation;
	}

	/**
	 * Gets the activityFeeDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getActivityFeeDate() {
		return activityFeeDate;
	}

	/**
	 * Sets the activityFeeDate
	 * 
	 * @param activityFeeDate
	 *            The activityFeeDate to set
	 */
	public void setActivityFeeDate(Calendar activityFeeDate) {
		this.activityFeeDate = activityFeeDate;
	}

	/**
	 * Sets the activityFeeDate
	 * 
	 * @param activityFeeDate
	 *            The activityFeeDate to set
	 */
	public void setActivityFeeDate(Date activityFeeDate) {
		if (activityFeeDate == null)
			this.activityFeeDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(activityFeeDate);
			this.activityFeeDate = calendar;
		}
	}

	/**
	 * Gets the planCheckFeeDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getPlanCheckFeeDate() {
		return planCheckFeeDate;
	}

	/**
	 * Sets the planCheckFeeDate
	 * 
	 * @param planCheckFeeDate
	 *            The planCheckFeeDate to set
	 */
	public void setPlanCheckFeeDate(Calendar planCheckFeeDate) {
		this.planCheckFeeDate = planCheckFeeDate;
	}

	/**
	 * Sets the planCheckFeeDate
	 * 
	 * @param planCheckFeeDate
	 *            The planCheckFeeDate to set
	 */
	public void setPlanCheckFeeDate(Date planCheckFeeDate) {
		if (planCheckFeeDate == null)
			this.planCheckFeeDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(planCheckFeeDate);
			this.planCheckFeeDate = calendar;
		}
	}

	/**
	 * Gets the appliedDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getAppliedDate() {
		return appliedDate;
	}

	/**
	 * Gets the appliedDate
	 * 
	 * @return Returns a Calendar
	 */
	public String getAppliedDateString() {
		return StringUtils.cal2str(appliedDate);
	}

	/**
	 * Sets the appliedDate
	 * 
	 * @param appliedDate
	 *            The appliedDate to set
	 */
	public void setAppliedDate(Calendar appliedDate) {
		this.appliedDate = appliedDate;
	}

	/**
	 * Sets the appliedDate
	 * 
	 * @param appliedDate
	 *            The appliedDate to set
	 */
	public void setAppliedDate(Date appliedDate) {
		if (appliedDate == null)
			this.appliedDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(appliedDate);
			this.appliedDate = calendar;
		}
	}

	/**
	 * Gets the issueDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getIssueDate() {
		return issueDate;
	}

	/**
	 * Gets the issueDate
	 * 
	 * @return Returns a Calendar
	 */
	public String getIssueDateString() {
		return StringUtils.cal2str(issueDate);
	}

	/**
	 * Sets the issueDate
	 * 
	 * @param issueDate
	 *            The issueDate to set
	 */
	public void setIssueDate(Calendar issueDate) {
		this.issueDate = issueDate;
	}

	/**
	 * Sets the issueDate
	 * 
	 * @param issueDate
	 *            The issueDate to set
	 */
	public void setIssueDate(Date issueDate) {
		if (issueDate == null)
			this.issueDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(issueDate);
			this.issueDate = calendar;
		}
	}

	/**
	 * Gets the expirationDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getExpirationDate() {
		return expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(Calendar expirationDate) {
		this.expirationDate = expirationDate;
	}

	/**
	 * Sets the expirationDate
	 * 
	 * @param expirationDate
	 *            The expirationDate to set
	 */
	public void setExpirationDate(Date expirationDate) {
		if (expirationDate == null)
			this.expirationDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(expirationDate);
			this.expirationDate = calendar;
		}
	}

	/**
	 * Gets the finaledDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getFinaledDate() {
		return finaledDate;
	}

	/**
	 * Sets the finaledDate
	 * 
	 * @param finaledDate
	 *            The finaledDate to set
	 */
	public void setFinaledDate(Calendar finaledDate) {
		this.finaledDate = finaledDate;
	}

	/**
	 * Sets the finaledDate
	 * 
	 * @param finaledDate
	 *            The finaledDate to set
	 */
	public void setFinaledDate(Date finaledDate) {
		if (finaledDate == null)
			this.finaledDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(finaledDate);
			this.finaledDate = calendar;
		}
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a User
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Date created) {
		if (created == null)
			this.created = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(created);
			this.created = calendar;
		}
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a User
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Date updated) {
		if (updated == null)
			this.updated = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updated);
			this.updated = calendar;
		}
	}

	/**
	 * Gets the activityType
	 * 
	 * @return Returns a ActivityType
	 */
	public ActivityType getActivityType() {
		return activityType;
	}

	/**
	 * Sets the activityType
	 * 
	 * @param activityType
	 *            The activityType to set
	 */
	public void setActivityType(ActivityType activityType) {
		this.activityType = activityType;
	}

	/**
	 * Gets the startDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getStartDate() {
		return startDate;
	}

	/**
	 * Sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set
	 */
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	/**
	 * Sets the startDate
	 * 
	 * @param startDate
	 *            The startDate to set
	 */
	public void setStartDate(Date startDate) {
		if (startDate == null)
			this.startDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			this.startDate = calendar;
		}
	}

	/**
	 * Gets the permitFeeDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getPermitFeeDate() {
		return permitFeeDate;
	}

	/**
	 * Gets the DevelopmentFeeDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getDevelopmentFeeDate() {
		return developmentFeeDate;
	}

	/**
	 * Sets the developmentFeeDate
	 * 
	 * @param developmentFeeDate
	 *            The developmentFeeDate to set
	 */
	public void setDevelopmentFeeDate(Calendar developmentFeeDate) {
		this.developmentFeeDate = developmentFeeDate;
	}

	/**
	 * Sets the developmentFeeDate
	 * 
	 * @param developmentFeeDate
	 *            The developmentFeeDate to set
	 */
	public void setDevelopmentFeeDate(Date developmentFeeDate) {
		if (developmentFeeDate == null)
			this.developmentFeeDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(developmentFeeDate);
			this.developmentFeeDate = calendar;
		}
	}

	/**
	 * Sets the permitFeeDate
	 * 
	 * @param permitFeeDate
	 *            The permitFeeDate to set
	 */
	public void setPermitFeeDate(Calendar permitFeeDate) {
		this.permitFeeDate = permitFeeDate;
	}

	/**
	 * Sets the permitFeeDate
	 * 
	 * @param permitFeeDate
	 *            The permitFeeDate to set
	 */
	public void setPermitFeeDate(Date permitFeeDate) {
		if (permitFeeDate == null)
			this.permitFeeDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(permitFeeDate);
			this.permitFeeDate = calendar;
		}
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a ActivityStatus
	 */
	public ActivityStatus getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(ActivityStatus status) {
		this.status = status;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the finalDateLabel
	 * 
	 * @return Returns a String
	 */
	public String getFinalDateLabel() {
		return finalDateLabel;
	}

	/**
	 * Sets the finalDateLabel
	 * 
	 * @param finalDateLabel
	 *            The finalDateLabel to set
	 */
	public void setFinalDateLabel(String activtiyTypeCode) {
		if (activtiyTypeCode == null)
			activtiyTypeCode = "";
		if (activtiyTypeCode.equals("CLSA") || activtiyTypeCode.equals("CLSB") || activtiyTypeCode.equals("CLSC") || activtiyTypeCode.equals("CLSD") || activtiyTypeCode.equals("CLSE") || activtiyTypeCode.equals("AFTH"))
			this.finalDateLabel = "Job Start";
		else if (activtiyTypeCode.equals("CARFAI") || activtiyTypeCode.equals("CENTP") || activtiyTypeCode.equals("SPYPNT") || activtiyTypeCode.equals("SPLFX") || activtiyTypeCode.equals("DRYCLN") || activtiyTypeCode.equals("TENTFD") || activtiyTypeCode.equals("PUBAS"))
			this.finalDateLabel = "Granted";
		else if (activtiyTypeCode.equals("GARA") || activtiyTypeCode.equals("PHOTO") || activtiyTypeCode.equals("SPEV") || activtiyTypeCode.equals("FILM"))
			this.finalDateLabel = "Start";
		else
			this.finalDateLabel = "Completion Date";
	}

	/**
	 * Gets the expirationDateLabel
	 * 
	 * @return Returns a String
	 */
	public String getExpirationDateLabel() {
		return expirationDateLabel;
	}

	/**
	 * Sets the expirationDateLabel
	 * 
	 * @param expirationDateLabel
	 *            The expirationDateLabel to set
	 */
	public void setExpirationDateLabel(String activtiyTypeCode) {
		if (activtiyTypeCode == null)
			activtiyTypeCode = "";
		// else activtiyTypeCode = activtiyTypeCode.trim();
		if (activtiyTypeCode.equals("CLSA") || activtiyTypeCode.equals("CLSB") || activtiyTypeCode.equals("CLSC") || activtiyTypeCode.equals("CLSD") || activtiyTypeCode.equals("CLSE") || activtiyTypeCode.equals("AFTH"))
			this.expirationDateLabel = "Job End";
		else if (activtiyTypeCode.equals("GARA") || activtiyTypeCode.equals("PHOTO") || activtiyTypeCode.equals("SPEV") || activtiyTypeCode.equals("FILM"))
			this.expirationDateLabel = "End";
		else if (activtiyTypeCode.equals("TENT"))
			this.expirationDateLabel = "Removed by";
		else
			this.expirationDateLabel = "Expiration Date";

		logger.debug("expirationDateLabel  is  set to " + this.expirationDateLabel);

	}

	/**
	 * Gets the inspectionRequired
	 * 
	 * @return Returns a String
	 */
	public String getInspectionRequired() {
		return inspectionRequired;
	}

	/**
	 * Sets the inspectionRequired
	 * 
	 * @param inspectionRequired
	 *            The inspectionRequired to set
	 */
	public void setInspectionRequired(String inspectionRequired) {
		this.inspectionRequired = inspectionRequired;
	}

	/**
	 * Returns the completionDate.
	 * 
	 * @return Calendar
	 */
	public Calendar getCompletionDate() {
		return completionDate;
	}

	/**
	 * Sets the completionDate.
	 * 
	 * @param completionDate
	 *            The completionDate to set
	 */
	public void setCompletionDate(Calendar completionDate) {
		this.completionDate = completionDate;
	}

	/**
	 * Returns the microfilm.
	 * 
	 * @return String
	 */
	public String getMicrofilm() {
		return microfilm;
	}

	/**
	 * Sets the microfilm.
	 * 
	 * @param microfilm
	 *            The microfilm to set
	 */
	public void setMicrofilm(String microfilm) {
		this.microfilm = microfilm;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Returns the addressId.
	 * 
	 * @return int
	 */
	public int getAddressId() {
		return addressId;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Sets the addressId.
	 * 
	 * @param addressId
	 *            The addressId to set
	 */
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	/**
	 * Returns the ownerName.
	 * 
	 * @return String
	 */
	public String getOwnerName() {
		return ownerName;
	}

	/**
	 * Sets the ownerName.
	 * 
	 * @param ownerName
	 *            The ownerName to set
	 */
	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	/**
	 * Returns the activitySubType.
	 * 
	 * @return ActivitySubType
	 */
	public ActivitySubType getActivitySubType() {
		return activitySubType;
	}

	/**
	 * Sets the activitySubType.
	 * 
	 * @param activitySubType
	 *            The activitySubType to set
	 */
	public void setActivitySubType(ActivitySubType activitySubType) {
		this.activitySubType = activitySubType;
	}

	// // @return
	// }
	public List getActivitySubTypes() {
		return activitySubTypes;
	}

	/**
	 * @param list
	 */
	public void setActivitySubTypes(List list) {
		logger.debug("setting activity sub types in the activity detail object");
		activitySubTypes = list;
		logger.debug("set activity sub types in the activity detail object");
	}

	/**
	 * @return
	 */
	public String getDevelopmentFeeRequired() {
		return developmentFeeRequired;
	}

	/**
	 * @param string
	 */
	public void setDevelopmentFeeRequired(String string) {
		developmentFeeRequired = string;
	}

	/**
	 * @return Returns the demolishedUnits.
	 */
	public String getDemolishedUnits() {
		return demolishedUnits;
	}

	/**
	 * @param demolishedUnits
	 *            The demolishedUnits to set.
	 */
	public void setDemolishedUnits(String demolishedUnits) {
		this.demolishedUnits = demolishedUnits;
	}

	/**
	 * @return Returns the existingUnits.
	 */
	public String getExistingUnits() {
		return existingUnits;
	}

	/**
	 * @param existingUnits
	 *            The existingUnits to set.
	 */
	public void setExistingUnits(String existingUnits) {
		this.existingUnits = existingUnits;
	}

	/**
	 * @return Returns the newUnits.
	 */
	public String getNewUnits() {
		return newUnits;
	}

	/**
	 * @param newUnits
	 *            The newUnits to set.
	 */
	public void setNewUnits(String newUnits) {
		this.newUnits = newUnits;
	}

	@Override
	public String toString() {
		return "ActivityDetail [activityNumber=" + activityNumber + ", activityType=" + activityType + ", activitySubType=" + activitySubType + ", description=" + description + ", valuation=" + valuation + ", status=" + status + ", startDate=" + startDate + ", completionDate=" + completionDate + ", activityFeeDate=" + activityFeeDate + ", planCheckFeeDate=" + planCheckFeeDate + ", permitFeeDate=" + permitFeeDate + ", developmentFeeDate=" + developmentFeeDate + ", appliedDate=" + appliedDate + ", issueDate=" + issueDate + ", expirationDate=" + expirationDate + ", finaledDate=" + finaledDate + ", planCheckRequired=" + planCheckRequired + ", developmentFeeRequired=" + developmentFeeRequired + ", inspectionRequired=" + inspectionRequired + ", microfilm=" + microfilm + ", createdBy=" + createdBy + ", created=" + created + ", updatedBy=" + updatedBy + ", updated=" + updated + ", lsoId=" + lsoId + ", finalDateLabel=" + finalDateLabel + ", expirationDateLabel=" + expirationDateLabel + ", addressId=" + addressId + ", address=" + address + ", ownerName=" + ownerName + ", activitySubTypes=" + activitySubTypes + ", newUnits=" + newUnits + ", existingUnits=" + existingUnits + ", demolishedUnits=" + demolishedUnits + ", multiAddress=" + multiAddress + ", label=" + label + ", parkingZone=" + parkingZone + "]";
	}

	/**
	 * @return Returns the GreenHalo.
	 */
	public String getGreenHalo() {
		return greenHalo;
	}

	/**
	 * @param greenHalo
	 */
	public void setGreenHalo(String greenHalo) {
		this.greenHalo = greenHalo;
	}

	public String getOnlineApplication() {
		return onlineApplication;
	}

	public void setOnlineApplication(String onlineApplication) {
		this.onlineApplication = onlineApplication;
	}

	public String getExtRefNumber() {
		return extRefNumber;
	}

	public void setExtRefNumber(String extRefNumber) {
		this.extRefNumber = extRefNumber;
	}
	
}
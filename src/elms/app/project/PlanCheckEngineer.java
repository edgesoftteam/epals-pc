package elms.app.project;

import java.util.List;

import elms.app.planning.PlannerUpdateRecord;

public class PlanCheckEngineer {
	
    public String userName;
    private int userId;
    public List activityDetailsList;
    public PlanCheck planCheckArray[];
    public String selectAllCheckBox;
    
	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @return the activityDetailsList
	 */
	public List getActivityDetailsList() {
		return activityDetailsList;
	}
	/**
	 * @param activityDetailsList the activityDetailsList to set
	 */
	public void setActivityDetailsList(List activityDetailsList) {
		this.activityDetailsList = activityDetailsList;
	}
	/**
	 * @return the planCheckArray
	 */
	public PlanCheck[] getPlanCheckArray() {
		return planCheckArray;
	}
	/**
	 * @param planCheckArray the planCheckArray to set
	 */
	public void setPlanCheckArray(PlanCheck[] planCheckArray) {
		this.planCheckArray = planCheckArray;
	}
	/**
	 * @return the selectAllCheckBox
	 */
	public String getSelectAllCheckBox() {
		return selectAllCheckBox;
	}
	/**
	 * @param selectAllCheckBox the selectAllCheckBox to set
	 */
	public void setSelectAllCheckBox(String selectAllCheckBox) {
		this.selectAllCheckBox = selectAllCheckBox;
	}

}

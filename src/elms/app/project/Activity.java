package elms.app.project;

import java.util.ArrayList;
import java.util.List;


import elms.app.bl.BusinessLicenseActivity;
import elms.app.bt.BusinessTaxActivity;
import elms.app.finance.ActivityFinance;
import elms.app.lso.Owner;
import elms.app.people.People;
import elms.app.planning.PlanningDetails;
import elms.util.StringUtils;

public class Activity extends Psa {
	protected int activityId;
	protected int subProjectId;
	protected ActivityDetail activityDetail;
	protected People people;
	protected ActivityFinance activityFinance;
	protected PlanningDetails planningDetails;
	protected List activityTeam;
	protected List planCheck;
	protected List inspection;
	protected List fees;
	protected boolean inspectable;
	protected boolean deletable;
	protected BusinessLicenseActivity businessLicenseActivity;
	protected BusinessTaxActivity businessTaxActivity;
	protected Owner owner;
	protected String greenHalo;
	protected String extRefNumber;

	public Activity() {
	}

	public Activity(int aActivityId, ActivityDetail aActivityDetail) {
		this.activityId = aActivityId;
		this.activityDetail = aActivityDetail;
		this.fees = new ArrayList();
		this.inspection = new ArrayList();
		this.planCheck = new ArrayList();
		this.activityTeam = new ArrayList();

	}

	public Activity(int aActivityId, int aSubProjectId, ActivityDetail aActivityDetail, People aPeople, ActivityFinance aActivityFinance, List aActivityTeam, List aPlanCheck, List aInspection, List aActivityHold, List aActivityCondition, List aActivityAttachment, List aActivityComment, List aFees) {
		super(aActivityHold, aActivityCondition, aActivityAttachment, aActivityComment);
		this.activityId = aActivityId;
		this.subProjectId = aSubProjectId;
		this.activityDetail = aActivityDetail;
		this.people = aPeople;
		this.activityFinance = aActivityFinance;
		this.activityTeam = aActivityTeam;
		this.planCheck = aPlanCheck;
		this.inspection = aInspection;
		this.fees = aFees;
	}

	/**
	 * @return Returns the owner.
	 */
	public Owner getOwner() {
		return owner;
	}

	/**
	 * @param owner
	 *            The owner to set.
	 */
	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	/**
	 * Gets the activityDetail
	 * 
	 * @return Returns a ActivityDetail
	 */
	public ActivityDetail getActivityDetail() {
		return activityDetail;
	}

	/**
	 * Sets the activityDetail
	 * 
	 * @param activityDetail
	 *            The activityDetail to set
	 */
	public void setActivityDetail(ActivityDetail activityDetail) {
		this.activityDetail = activityDetail;
	}

	/**
	 * Gets the people
	 * 
	 * @return Returns a People
	 */
	public People getPeople() {
		return people;
	}

	/**
	 * Sets the people
	 * 
	 * @param people
	 *            The people to set
	 */
	public void setPeople(People people) {
		this.people = people;
	}

	/**
	 * Gets the activityFinance
	 * 
	 * @return Returns a ActivityFinance
	 */
	public ActivityFinance getActivityFinance() {
		return activityFinance;
	}

	/**
	 * Sets the activityFinance
	 * 
	 * @param activityFinance
	 *            The activityFinance to set
	 */
	public void setActivityFinance(ActivityFinance activityFinance) {
		this.activityFinance = activityFinance;
	}

	/**
	 * Gets the activityTeam
	 * 
	 * @return Returns a List
	 */
	public List getActivityTeam() {
		return activityTeam;
	}

	/**
	 * Sets the activityTeam
	 * 
	 * @param activityTeam
	 *            The activityTeam to set
	 */
	public void setActivityTeam(List activityTeam) {
		this.activityTeam = activityTeam;
	}

	/**
	 * Gets the planCheck
	 * 
	 * @return Returns a List
	 */
	public List getPlanCheck() {
		return planCheck;
	}

	/**
	 * Sets the planCheck
	 * 
	 * @param planCheck
	 *            The planCheck to set
	 */
	public void setPlanCheck(List planCheck) {
		this.planCheck = planCheck;
	}

	/**
	 * Gets the inspection
	 * 
	 * @return Returns a List
	 */
	public List getInspection() {
		return inspection;
	}

	/**
	 * Sets the inspection
	 * 
	 * @param inspection
	 *            The inspection to set
	 */
	public void setInspection(List inspection) {
		this.inspection = inspection;
	}

	/**
	 * Gets the activityId
	 * 
	 * @return Returns a int
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * Sets the activityId
	 * 
	 * @param activityId
	 *            The activityId to set
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * Gets the subProjectId
	 * 
	 * @return Returns a int
	 */
	public int getSubProjectId() {
		return subProjectId;
	}

	/**
	 * Sets the subProjectId
	 * 
	 * @param subProjectId
	 *            The subProjectId to set
	 */
	public void setSubProjectId(int subProjectId) {
		this.subProjectId = subProjectId;
	}

	/**
	 * Gets the fees
	 * 
	 * @return Returns a List
	 */
	public List getFees() {
		return fees;
	}

	/**
	 * Sets the fees
	 * 
	 * @param fees
	 *            The fees to set
	 */
	public void setFees(List fees) {
		this.fees = fees;
	}

	/**
	 * @return
	 */
	public PlanningDetails getPlanningDetails() {
		return planningDetails;
	}

	/**
	 * @param details
	 */
	public void setPlanningDetails(PlanningDetails details) {
		planningDetails = details;
	}

	/**
	 * @return
	 */
	public boolean isDeletable() {
		return deletable;
	}

	/**
	 * @return
	 */
	public boolean isInspectable() {
		return inspectable;
	}

	/**
	 * @param b
	 */
	public void setDeletable(boolean b) {
		deletable = b;
	}

	/**
	 * @param b
	 */
	public void setInspectable(boolean b) {
		inspectable = b;
	}

	/**
	 * @return Returns the businessLicenseActivity.
	 */
	public BusinessLicenseActivity getBusinessLicenseActivity() {
		return businessLicenseActivity;
	}

	/**
	 * @param businessLicenseActivity
	 *            The businessLicenseActivity to set.
	 */
	public void setBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity) {
		this.businessLicenseActivity = businessLicenseActivity;
	}

	/**
	 * @return Returns the businessTaxActivity.
	 */
	public BusinessTaxActivity getBusinessTaxActivity() {
		return businessTaxActivity;
	}

	/**
	 * @param businessTaxActivity
	 *            The businessTaxActivity to set.
	 */
	public void setBusinessTaxActivity(BusinessTaxActivity businessTaxActivity) {
		this.businessTaxActivity = businessTaxActivity;
	}

	public String getGreenHalo() {
		return greenHalo;
	}

	public void setGreenHalo(String greenHalo) {
			this.greenHalo = greenHalo;
	}

	public String getExtRefNumber() {
		return extRefNumber;
	}

	public void setExtRefNumber(String extRefNumber) {
		this.extRefNumber = extRefNumber;
	}
}
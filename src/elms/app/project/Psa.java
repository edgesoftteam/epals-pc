//Source file: C:\\datafile\\source\\elms\\app\\project\\Psa.java

package elms.app.project;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Shekhar Jain
 */
public class Psa {
	protected List psaHold;
	protected List psaTenant;
	protected List psaNotice;
	protected List psaCondition;
	protected List psaAttachment;
	protected List psaComment;

	public Psa(List psaHold, List psaCondition, List psaAttachment, List psaComment) {
		this.psaHold = psaHold;
		this.psaCondition = psaCondition;
		this.psaAttachment = psaAttachment;
		this.psaComment = psaComment;
	}

	public Psa() {
		psaHold = new ArrayList();
		psaCondition = new ArrayList();
		psaAttachment = new ArrayList();
		psaComment = new ArrayList();

	}

	/**
	 * Gets the psaHold
	 * 
	 * @return Returns a List
	 */
	public List getPsaHold() {
		return psaHold;
	}

	/**
	 * Sets the psaHold
	 * 
	 * @param psaHold
	 *            The psaHold to set
	 */
	public void setPsaHold(List psaHold) {
		this.psaHold = psaHold;
	}

	/**
	 * Gets the psaCondition
	 * 
	 * @return Returns a List
	 */
	public List getPsaCondition() {
		return psaCondition;
	}

	/**
	 * Sets the psaCondition
	 * 
	 * @param psaCondition
	 *            The psaCondition to set
	 */
	public void setPsaCondition(List psaCondition) {
		this.psaCondition = psaCondition;
	}

	/**
	 * Gets the psaAttachment
	 * 
	 * @return Returns a List
	 */
	public List getPsaAttachment() {
		return psaAttachment;
	}

	/**
	 * Sets the psaAttachment
	 * 
	 * @param psaAttachment
	 *            The psaAttachment to set
	 */
	public void setPsaAttachment(List psaAttachment) {
		this.psaAttachment = psaAttachment;
	}

	/**
	 * Gets the psaComment
	 * 
	 * @return Returns a List
	 */
	public List getPsaComment() {
		return psaComment;
	}

	/**
	 * Sets the psaComment
	 * 
	 * @param psaComment
	 *            The psaComment to set
	 */
	public void setPsaComment(List psaComment) {
		this.psaComment = psaComment;
	}

	/**
	 * @return the psaTenant
	 */
	public List getPsaTenant() {
		return psaTenant;
	}

	/**
	 * @param psaTenant the psaTenant to set
	 */
	public void setPsaTenant(List psaTenant) {
		this.psaTenant = psaTenant;
	}

	/**
	 * @return the psaNotice
	 */
	public List getPsaNotice() {
		return psaNotice;
	}

	/**
	 * @param psaNotice the psaNotice to set
	 */
	public void setPsaNotice(List psaNotice) {
		this.psaNotice = psaNotice;
	}

}
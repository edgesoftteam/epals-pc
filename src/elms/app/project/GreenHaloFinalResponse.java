package elms.app.project;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GreenHaloFinalResponse {
	private int id;
	private int projectId;
	private int ghrId;
	@JsonProperty("div_rate")
	private int divRate;
	@JsonProperty("status")
	private String status;
	@JsonProperty("note")
	private String note;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}
	/**
	 * @return the divRate
	 */
	public int getDivRate() {
		return divRate;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	/**
	 * @param divRate the divRate to set
	 */
	public void setDivRate(int divRate) {
		this.divRate = divRate;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the ghrId
	 */
	public int getGhrId() {
		return ghrId;
	}
	/**
	 * @param ghrId the ghrId to set
	 */
	public void setGhrId(int ghrId) {
		this.ghrId = ghrId;
	}
	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}
	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
}

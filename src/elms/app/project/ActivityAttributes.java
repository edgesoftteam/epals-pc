package elms.app.project;

public class ActivityAttributes {

    private int totalPeople;
    private int totalInspection;
    private int openInspection;
    private int totalCVD;
    private int openCVD;
    private int totalComment;
    private int totalAttachment;
    private int totalNotice;
    private int totalTeam;
    private int totalMeeting;
    private int totalResolution;
    private int totalEnvReview;
    private int totalStatusList;
    private int totalPlanCheck;
    private int openPlanCheck;
    private int totalHolds;
    private int totalBlTeam;
    
    
	/**
	 * @return the totalPlanCheck
	 */
	public int getTotalPlanCheck() {
		return totalPlanCheck;
	}
	/**
	 * @param totalPlanCheck the totalPlanCheck to set
	 */
	public void setTotalPlanCheck(int totalPlanCheck) {
		this.totalPlanCheck = totalPlanCheck;
	}
	/**
	 * @return the openPlanCheck
	 */
	public int getOpenPlanCheck() {
		return openPlanCheck;
	}
	/**
	 * @param openPlanCheck the openPlanCheck to set
	 */
	public void setOpenPlanCheck(int openPlanCheck) {
		this.openPlanCheck = openPlanCheck;
	}
	/**
	 * @return the totalStatusList
	 */
	public int getTotalStatusList() {
		return totalStatusList;
	}
	/**
	 * @param totalStatusList the totalStatusList to set
	 */
	public void setTotalStatusList(int totalStatusList) {
		this.totalStatusList = totalStatusList;
	}
	/**
	 * @return the totalMeetings
	 */
	public int getTotalMeeting() {
		return totalMeeting;
	}
	/**
	 * @param totalMeetings the totalMeetings to set
	 */
	public void setTotalMeeting(int totalMeeting) {
		this.totalMeeting = totalMeeting;
	}
	/**
	 * @return the totalResolutions
	 */
	public int getTotalResolution() {
		return totalResolution;
	}
	/**
	 * @param totalResolutions the totalResolutions to set
	 */
	public void setTotalResolution(int totalResolution) {
		this.totalResolution = totalResolution;
	}
	/**
	 * @return the totalEnvReview
	 */
	public int getTotalEnvReview() {
		return totalEnvReview;
	}
	/**
	 * @param totalEnvReview the totalEnvReview to set
	 */
	public void setTotalEnvReview(int totalEnvReview) {
		this.totalEnvReview = totalEnvReview;
	}
	/**
	 * @return the totalTeam
	 */
	public int getTotalTeam() {
		return totalTeam;
	}
	/**
	 * @param totalTeam the totalTeam to set
	 */
	public void setTotalTeam(int totalTeam) {
		this.totalTeam = totalTeam;
	}
	/**
	 * @return the totalPeople
	 */
	public int getTotalPeople() {
		return totalPeople;
	}
	/**
	 * @param totalPeople the totalPeople to set
	 */
	public void setTotalPeople(int totalPeople) {
		this.totalPeople = totalPeople;
	}
	/**
	 * @return the totalInspection
	 */
	public int getTotalInspection() {
		return totalInspection;
	}
	/**
	 * @param totalInspection the totalInspection to set
	 */
	public void setTotalInspection(int totalInspection) {
		this.totalInspection = totalInspection;
	}
	/**
	 * @return the openInspection
	 */
	public int getOpenInspection() {
		return openInspection;
	}
	/**
	 * @param openInspection the openInspection to set
	 */
	public void setOpenInspection(int openInspection) {
		this.openInspection = openInspection;
	}
	
	/**
	 * @return the totalCVD
	 */
	public int getTotalCVD() {
		return totalCVD;
	}
	/**
	 * @param totalCVD the totalCVD to set
	 */
	public void setTotalCVD(int totalCVD) {
		this.totalCVD = totalCVD;
	}
	/**
	 * @return the openCVD
	 */
	public int getOpenCVD() {
		return openCVD;
	}
	/**
	 * @param openCVD the openCVD to set
	 */
	public void setOpenCVD(int openCVD) {
		this.openCVD = openCVD;
	}
	/**
	 * @return the totalComment
	 */
	public int getTotalComment() {
		return totalComment;
	}
	/**
	 * @param totalComment the totalComment to set
	 */
	public void setTotalComment(int totalComment) {
		this.totalComment = totalComment;
	}
	/**
	 * @return the totalAttachment
	 */
	public int getTotalAttachment() {
		return totalAttachment;
	}
	/**
	 * @param totalAttachment the totalAttachment to set
	 */
	public void setTotalAttachment(int totalAttachment) {
		this.totalAttachment = totalAttachment;
	}
	/**
	 * @return the totalNotice
	 */
	public int getTotalNotice() {
		return totalNotice;
	}
	/**
	 * @param totalNotice the totalNotice to set
	 */
	public void setTotalNotice(int totalNotice) {
		this.totalNotice = totalNotice;
	}
	/**
	 * @return the totalHolds
	 */
	public int getTotalHolds() {
		return totalHolds;
	}
	/**
	 * @param totalHolds the totalHolds to set
	 */
	public void setTotalHolds(int totalHolds) {
		this.totalHolds = totalHolds;
	}
	public int getTotalBlTeam() {
		return totalBlTeam;
	}
	public void setTotalBlTeam(int totalBlTeam) {
		this.totalBlTeam = totalBlTeam;
	}
}

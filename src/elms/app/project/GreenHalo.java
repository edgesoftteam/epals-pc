package elms.app.project;

public class GreenHalo {
	
	private int id;
	private int actId;
	private String companyName;
	private String phone;
	private String projectName;
	private String email;
	private String street;
	private String aptSuite;
	private String zipcode;
	private String projectStartDate; 
	private String projectEndDate; 
	private int value;
	private int squareFootage; 
	private String description; 
	private String projectType;
	private String buildingType;
	private String permit;
	private int permitSqFootage; 
	private String permitNote;
	private int permitValue;
	private String permitProjectType;
	private String apiKey;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return companyName;
	}
	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}
	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}
	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}
	/**
	 * @return the aptSuite
	 */
	public String getAptSuite() {
		return aptSuite;
	}
	/**
	 * @param aptSuite the aptSuite to set
	 */
	public void setAptSuite(String aptSuite) {
		this.aptSuite = aptSuite;
	}
	/**
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}
	/**
	 * @param zipcode the zipcode to set
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	/**
	 * @return the projectStartDate
	 */
	public String getProjectStartDate() {
		return projectStartDate;
	}
	/**
	 * @param projectStartDate the projectStartDate to set
	 */
	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}
	/**
	 * @return the projectEndDate
	 */
	public String getProjectEndDate() {
		return projectEndDate;
	}
	/**
	 * @param projectEndDate the projectEndDate to set
	 */
	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}
	/**
	 * @return the fsquare_footage
	 */
	public int getSquareFootage() {
		return squareFootage;
	}
	/**
	 * @param fsquare_footage the fsquare_footage to set
	 */
	public void setSquareFootage(int squareFootage) {
		this.squareFootage = squareFootage;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the projectType
	 */
	public String getProjectType() {
		return projectType;
	}
	/**
	 * @param projectType the projectType to set
	 */
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	/**
	 * @return the buildingType
	 */
	public String getBuildingType() {
		return buildingType;
	}
	/**
	 * @param buildingType the buildingType to set
	 */
	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}
	/**
	 * @return the permit
	 */
	public String getPermit() {
		return permit;
	}
	/**
	 * @param permit the permit to set
	 */
	public void setPermit(String permit) {
		this.permit = permit;
	}
	/**
	 * @return the permitSqFootage
	 */
	public int getPermitSqFootage() {
		return permitSqFootage;
	}
	/**
	 * @param permitSqFootage the permitSqFootage to set
	 */
	public void setPermitSqFootage(int permitSqFootage) {
		this.permitSqFootage = permitSqFootage;
	}
	/**
	 * @return the permitNote
	 */
	public String getPermitNote() {
		return permitNote;
	}
	/**
	 * @param permitNote the permitNote to set
	 */
	public void setPermitNote(String permitNote) {
		this.permitNote = permitNote;
	}
	/**
	 * @return the permitValue
	 */
	public int getPermitValue() {
		return permitValue;
	}
	/**
	 * @param permitValue the permitValue to set
	 */
	public void setPermitValue(int permitValue) {
		this.permitValue = permitValue;
	}
	/**
	 * @return the permitProjectType
	 */
	public String getPermitProjectType() {
		return permitProjectType;
	}
	/**
	 * @param permitProjectType the permitProject_type to set
	 */
	public void setPermitProjectType(String permitProjectType) {
		this.permitProjectType = permitProjectType;
	}
	/**
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}
	/**
	 * @param apiKey the apiKey to set
	 */
	public void setApi_key(String apiKey) {
		this.apiKey = apiKey;
	}
	/**
	 * @return the actId
	 */
	public int getActId() {
		return actId;
	}
	/**
	 * @param actId the actId to set
	 */
	public void setActId(int actId) {
		this.actId = actId;
	}
	public String addProject() {
		String json = "{"+
				  "\"company_name\":\""+getCompanyName()+"\", "+
				  "\"phone\":\""+getPhone()+"\", "+
				  "\"project_name\":\""+getProjectName()+"\", "+
				  "\"email\": \""+getEmail()+"\", "+
				  "\"street\":\""+getStreet()+"\", "+
				  "\"apt_suite\":\""+getAptSuite()+"\", "+
				  "\"zipcode\":"+getZipcode()+", "+
				  " \"project_start_date\":\""+getProjectStartDate()+"\", "+
				  " \"project_end_date\":\""+getProjectEndDate()+"\", "+
				  "\"value\":"+getValue()+", "+
				  "\"square_footage\":"+getSquareFootage()+", "+
				  "\"description\":\""+getDescription()+"\", "+
				  "\"project_type\":\""+getProjectType()+"\", "+
				  "\"building_type\":\""+getBuildingType()+"\", "+
				  "\"permit\":\""+getPermit()+"\", "+
				  "\"permit_sq_footage\":\""+getPermitSqFootage()+"\", "+
				  "\"permit_note\":\""+getPermitNote()+"\", "+
				  "\"permit_value\":\""+getPermitValue()+"\", "+
				  "\"permit_project_type\":\""+getPermitProjectType()+"\", "+
				  "\"api_key\":\""+getApiKey()+"\" "+
				"}";
		return json;
	}
	public String addPermit(String wmpNumber) {
		String json = "{"+
				  "\"square_footage\":\""+getSquareFootage()+"\", "+
				  "\"description\":\""+getDescription()+"\", "+
				  "\"api_key\":\"9e710b89840852b490ab5d47228de65f\", "+
				  "\"permit\":\""+getPermit()+"\", "+
				  "\"value\":"+getValue()+", "+
				  "\"project_type\":\""+getProjectType()+"\", "+
				  "\"wmp_number\":\""+wmpNumber+"\" "+
				"}";
		return json;
	}

}

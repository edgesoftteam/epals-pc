package elms.app.project;

public class Certificate {

	private String ACT_ID;

	private String ACT_APN;

	private String ACT_NBR;

	private String ACT_ADDR;

	private String ISSUED_DATE;

	private String OCC_GRP;

	private String CONSTR_TYPE;

	private String SPRINKLE;

	private String GARAGE_AREA;

	private String STORIES;

	private String BLDG_AREA;

	private String BSMT_AREA;

	private String DWELLING;

	private String PARKING;

	private String INSPECTOR;

	private String BLDG_OFFICIAL;

	private String COND_CODE;

	private String ACT_DESCRIPTION;

	private String SPEC_CONDITIONS;

	private String ACT_PEOPLE;

	/**
	 * @return
	 */
	public String getACT_ADDR() {
		return ACT_ADDR;
	}

	/**
	 * @return
	 */
	public String getACT_APN() {
		return ACT_APN;
	}

	/**
	 * @return
	 */
	public String getACT_DESCRIPTION() {
		return ACT_DESCRIPTION;
	}

	/**
	 * @return
	 */
	public String getBLDG_AREA() {
		return BLDG_AREA;
	}

	/**
	 * @return
	 */
	public String getBLDG_OFFICIAL() {
		return BLDG_OFFICIAL;
	}

	/**
	 * @return
	 */
	public String getBSMT_AREA() {
		return BSMT_AREA;
	}

	/**
	 * @return
	 */
	public String getCOND_CODE() {
		return COND_CODE;
	}

	/**
	 * @return
	 */
	public String getCONSTR_TYPE() {
		return CONSTR_TYPE;
	}

	/**
	 * @return
	 */
	public String getDWELLING() {
		return DWELLING;
	}

	/**
	 * @return
	 */
	public String getGARAGE_AREA() {
		return GARAGE_AREA;
	}

	/**
	 * @return
	 */
	public String getINSPECTOR() {
		return INSPECTOR;
	}

	/**
	 * @return
	 */
	public String getOCC_GRP() {
		return OCC_GRP;
	}

	/**
	 * @return
	 */
	public String getPARKING() {
		return PARKING;
	}

	/**
	 * @return
	 */
	public String getSPEC_CONDITIONS() {
		return SPEC_CONDITIONS;
	}

	/**
	 * @return
	 */
	public String getSPRINKLE() {
		return SPRINKLE;
	}

	/**
	 * @return
	 */
	public String getSTORIES() {
		return STORIES;
	}

	/**
	 * @return
	 */

	/**
	 * @param string
	 */
	public void setACT_ADDR(String string) {
		ACT_ADDR = string;
	}

	/**
	 * @param string
	 */
	public void setACT_APN(String string) {
		ACT_APN = string;
	}

	/**
	 * @param string
	 */
	public void setACT_DESCRIPTION(String string) {
		ACT_DESCRIPTION = string;
	}

	/**
	 * @param string
	 */
	public void setBLDG_AREA(String string) {
		BLDG_AREA = string;
	}

	/**
	 * @param string
	 */
	public void setBLDG_OFFICIAL(String string) {
		BLDG_OFFICIAL = string;
	}

	/**
	 * @param string
	 */
	public void setBSMT_AREA(String string) {
		BSMT_AREA = string;
	}

	/**
	 * @param string
	 */
	public void setCOND_CODE(String string) {
		COND_CODE = string;
	}

	/**
	 * @param string
	 */
	public void setCONSTR_TYPE(String string) {
		CONSTR_TYPE = string;
	}

	/**
	 * @param string
	 */
	public void setDWELLING(String string) {
		DWELLING = string;
	}

	/**
	 * @param string
	 */
	public void setGARAGE_AREA(String string) {
		GARAGE_AREA = string;
	}

	/**
	 * @param string
	 */
	public void setINSPECTOR(String string) {
		INSPECTOR = string;
	}

	/**
	 * @param string
	 */
	public void setOCC_GRP(String string) {
		OCC_GRP = string;
	}

	/**
	 * @param string
	 */
	public void setPARKING(String string) {
		PARKING = string;
	}

	/**
	 * @param string
	 */
	public void setSPEC_CONDITIONS(String string) {
		SPEC_CONDITIONS = string;
	}

	/**
	 * @param string
	 */
	public void setSPRINKLE(String string) {
		SPRINKLE = string;
	}

	/**
	 * @param string
	 */
	public void setSTORIES(String string) {
		STORIES = string;
	}

	/**
	 * @return
	 */
	public String getACT_ID() {
		return ACT_ID;
	}

	/**
	 * @param string
	 */
	public void setACT_ID(String string) {
		ACT_ID = string;
	}

	/**
	 * @return
	 */
	public String getISSUED_DATE() {
		return ISSUED_DATE;
	}

	/**
	 * @param string
	 */
	public void setISSUED_DATE(String string) {
		ISSUED_DATE = string;
	}

	/**
	 * @return
	 */
	public String getACT_NBR() {
		return ACT_NBR;
	}

	/**
	 * @param string
	 */
	public void setACT_NBR(String string) {
		ACT_NBR = string;
	}

	/**
	 * @return
	 */

	/**
	 * @return
	 */
	public String getACT_PEOPLE() {
		return ACT_PEOPLE;
	}

	/**
	 * @param string
	 */
	public void setACT_PEOPLE(String string) {
		ACT_PEOPLE = string;
	}

}
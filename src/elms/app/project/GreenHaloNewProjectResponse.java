package elms.app.project;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GreenHaloNewProjectResponse {
	private int id;
	private int ghId;
	@JsonProperty("user_id")
	private int userId;
	@JsonProperty("contractor_id")
	private int contractorId;
	@JsonProperty("project_id")
	private int projectId;
	@JsonProperty("wmp_number")
	private String wmpNumber;
	@JsonProperty("status")
	private String status;
	@JsonProperty("note")
	private String note;
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @return the ghId
	 */
	public int getGhId() {
		return ghId;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @param ghId the ghId to set
	 */
	public void setGhId(int ghId) {
		this.ghId = ghId;
	}
	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}
	/**
	 * @return the contractorId
	 */
	public int getContractorId() {
		return contractorId;
	}
	/**
	 * @return the projectId
	 */
	public int getProjectId() {
		return projectId;
	}
	/**
	 * @return the wmpNnumber
	 */
	public String getWmpNumber() {
		return wmpNumber;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/**
	 * @param contractorId the contractorId to set
	 */
	public void setContractorId(int contractorId) {
		this.contractorId = contractorId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	/**
	 * @param wmpNnumber the wmpNnumber to set
	 */
	public void setWmpNumber(String wmpNumber) {
		this.wmpNumber = wmpNumber;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}
}

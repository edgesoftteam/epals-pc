//Source file: C:\\datafile\\source\\elms\\app\\project\\SubProjectDetail.java

package elms.app.project;

import java.util.Calendar;
import java.util.Date;

import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectStatus;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.security.User;

/**
 * @author Anand Belaguly
 */
public class SubProjectDetail {
	protected ProjectType projectType;
	protected SubProjectType subProjectType;
	protected SubProjectSubType subProjectSubType;
	protected String subProjectNumber;
	protected String description;
	protected SubProjectStatus subProjectStatus;
	protected String status;
	protected User createdBy;
	protected Calendar created;
	protected User updatedBy;
	protected Calendar updated;
	protected int lsoId;
	protected int hardHoldCount;
	protected int softHoldCount;
	protected int warnHoldCount;
	protected int attachmentCount;
	protected int conditionCount;

	protected String address;

	protected int caseLogId;
	protected String caseLogDesc;
	protected String label;

	/**
	 * 3CD178B6010C
	 */
	public SubProjectDetail() {

	}

	public SubProjectDetail(ProjectType aProjectType, SubProjectType aSubProjectType, SubProjectSubType aSubProjectSubType, String aDescription, String aStatus) {
		this.projectType = aProjectType;
		this.subProjectType = aSubProjectType;
		this.subProjectSubType = aSubProjectSubType;
		this.description = aDescription;
		this.status = aStatus;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * Access method for the subProjectType property.
	 * 
	 * @return the current value of the subProjectType property
	 */
	/**
	 * Gets the description
	 * 
	 * @return Returns a String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 * 
	 * @param description
	 *            The description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the status
	 * 
	 * @return Returns a String
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status
	 * 
	 * @param status
	 *            The status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the createdBy
	 * 
	 * @return Returns a User
	 */
	public User getCreatedBy() {
		return createdBy;
	}

	/**
	 * Sets the createdBy
	 * 
	 * @param createdBy
	 *            The createdBy to set
	 */
	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * Gets the created
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getCreated() {
		return created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Calendar created) {
		this.created = created;
	}

	/**
	 * Sets the created
	 * 
	 * @param created
	 *            The created to set
	 */
	public void setCreated(Date created) {
		if (created == null)
			this.created = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(created);
			this.created = calendar;
		}
	}

	/**
	 * Gets the updatedBy
	 * 
	 * @return Returns a User
	 */
	public User getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * Sets the updatedBy
	 * 
	 * @param updatedBy
	 *            The updatedBy to set
	 */
	public void setUpdatedBy(User updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * Gets the updated
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getUpdated() {
		return updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Calendar updated) {
		this.updated = updated;
	}

	/**
	 * Sets the updated
	 * 
	 * @param updated
	 *            The updated to set
	 */
	public void setUpdated(Date updated) {
		if (updated == null)
			this.updated = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(updated);
			this.updated = calendar;
		}

	}

	/**
	 * Gets the projectType
	 * 
	 * @return Returns a ProjectType
	 */
	public ProjectType getProjectType() {
		return projectType;
	}

	/**
	 * Sets the projectType
	 * 
	 * @param projectType
	 *            The projectType to set
	 */
	public void setProjectType(ProjectType projectType) {
		this.projectType = projectType;
	}

	/**
	 * Gets the subProjectType
	 * 
	 * @return Returns a SubProjectType
	 */
	public SubProjectType getSubProjectType() {
		return subProjectType;
	}

	/**
	 * Sets the subProjectType
	 * 
	 * @param subProjectType
	 *            The subProjectType to set
	 */
	public void setSubProjectType(SubProjectType subProjectType) {
		this.subProjectType = subProjectType;
	}

	/**
	 * Gets the subProjectSubType
	 * 
	 * @return Returns a SubProjectSubType
	 */
	public SubProjectSubType getSubProjectSubType() {
		return subProjectSubType;
	}

	/**
	 * Sets the subProjectSubType
	 * 
	 * @param subProjectSubType
	 *            The subProjectSubType to set
	 */
	public void setSubProjectSubType(SubProjectSubType subProjectSubType) {
		this.subProjectSubType = subProjectSubType;
	}

	/**
	 * Gets the lsoId
	 * 
	 * @return Returns a int
	 */
	public int getLsoId() {
		return lsoId;
	}

	/**
	 * Sets the lsoId
	 * 
	 * @param lsoId
	 *            The lsoId to set
	 */
	public void setLsoId(int lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * Gets the hardHoldCount
	 * 
	 * @return Returns a int
	 */
	public int getHardHoldCount() {
		return hardHoldCount;
	}

	/**
	 * Sets the hardHoldCount
	 * 
	 * @param hardHoldCount
	 *            The hardHoldCount to set
	 */
	public void setHardHoldCount(int hardHoldCount) {
		this.hardHoldCount = hardHoldCount;
	}

	/**
	 * Gets the softHoldCount
	 * 
	 * @return Returns a int
	 */
	public int getSoftHoldCount() {
		return softHoldCount;
	}

	/**
	 * Sets the softHoldCount
	 * 
	 * @param softHoldCount
	 *            The softHoldCount to set
	 */
	public void setSoftHoldCount(int softHoldCount) {
		this.softHoldCount = softHoldCount;
	}

	/**
	 * Gets the warnHoldCount
	 * 
	 * @return Returns a int
	 */
	public int getWarnHoldCount() {
		return warnHoldCount;
	}

	/**
	 * Sets the warnHoldCount
	 * 
	 * @param warnHoldCount
	 *            The warnHoldCount to set
	 */
	public void setWarnHoldCount(int warnHoldCount) {
		this.warnHoldCount = warnHoldCount;
	}

	/**
	 * Gets the attachmentCount
	 * 
	 * @return Returns a int
	 */
	public int getAttachmentCount() {
		return attachmentCount;
	}

	/**
	 * Sets the attachmentCount
	 * 
	 * @param attachmentCount
	 *            The attachmentCount to set
	 */
	public void setAttachmentCount(int attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

	/**
	 * Gets the conditionCount
	 * 
	 * @return Returns a int
	 */
	public int getConditionCount() {
		return conditionCount;
	}

	/**
	 * Sets the conditionCount
	 * 
	 * @param conditionCount
	 *            The conditionCount to set
	 */
	public void setConditionCount(int conditionCount) {
		this.conditionCount = conditionCount;
	}

	/**
	 * Gets the subProjectStatus
	 * 
	 * @return Returns a SubProjectStatus
	 */
	public SubProjectStatus getSubProjectStatus() {
		return subProjectStatus;
	}

	/**
	 * Sets the subProjectStatus
	 * 
	 * @param subProjectStatus
	 *            The subProjectStatus to set
	 */
	public void setSubProjectStatus(SubProjectStatus subProjectStatus) {
		this.subProjectStatus = subProjectStatus;
	}

	/**
	 * Gets the subProjectNumber
	 * 
	 * @return Returns a String
	 */
	public String getSubProjectNumber() {
		return subProjectNumber;
	}

	/**
	 * Sets the subProjectNumber
	 * 
	 * @param subProjectNumber
	 *            The subProjectNumber to set
	 */
	public void setSubProjectNumber(String subProjectNumber) {
		this.subProjectNumber = subProjectNumber;
	}

	/**
	 * Returns the address.
	 * 
	 * @return String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return
	 */
	public int getCaseLogId() {
		return caseLogId;
	}

	/**
	 * @param i
	 */
	public void setCaseLogId(int i) {
		caseLogId = i;
	}

	/**
	 * @return
	 */
	public String getCaseLogDesc() {
		return caseLogDesc;
	}

	/**
	 * @param string
	 */
	public void setCaseLogDesc(String string) {
		caseLogDesc = string;
	}

}

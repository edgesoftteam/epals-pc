package elms.app.people;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import elms.app.admin.PeopleType;
import elms.util.StringUtils;

public class People implements Serializable, Cloneable {

	static Logger logger = Logger.getLogger(People.class.getName());

	public int peopleId;
	public int activityId;
	public PeopleType peopleType;
	public String name = "";
	public String address = "";
	public String city = "";
	public String state = "";
	public String zipCode = "";
	public String phoneNbr = "";
	public String ext = "";
	public String faxNbr = "";
	public String emailAddress = "";
	public String comments = "";
	public String licenseNbr = "";
	public Calendar licenseExpires = Calendar.getInstance();
	public Calendar dlExpiryDate;
	public String confirmDlNumber;
	public String confirmStrDlExpiryDate;
	public String confirmSsn;
	public String onHold = "N";
	public Calendar holdDate;
	public String holdComments;
	public String hasHold;
	public float depositAmount;
	public String strDepositAmount;
	public String copyApplicant = "N";
	public String hicFlag = "N";
	public boolean pcPeople;
	public String levelType;
	public String isExpired = "";
	public String dlExpiryDateString = "N";
	public String BLExpirationDate = "Y";
	public String GLDate = "Y";
	public String ALDate = "Y";
	public String WCEDate = "Y";
	protected int versionNumber;
	public String date = StringUtils.cal2str(Calendar.getInstance());
	public Calendar today = Calendar.getInstance();
	public String agentName = "";
	public String agentPhone = "";
	public int caId;
	public Calendar generalLiabilityDate;
	public Calendar autoLiabilityDate;
	public String businessLicenseNbr;
	public Calendar businessLicenseExpires;
	public Calendar workersCompExpires;
	public String workersCompensationWaive = "N";
	public String title = "";
	public String firstName = "";
	public String businessName = "";
	public String corporateName = "";
	public String homeAddress = "";
	public String homeCity = "";
	public String homeState = "";
	public String homeZipCode = "";
	public String homeZipCode4 = "";
	public String mailingAddress = "";
	public String mailingCity = "";
	public String mailingState = "";
	public String mailingZipCode = "";
	public String mailingZipCode4 = "";
	public String businessPhone = "";
	public String businessPhoneExt = "";
	public String homePhone = "";
	public String dlNumberState = "";
	public String dlNumber = "";
	public String lastName = "";
	public String ssn = "";
	public Calendar dateOfBirth;
	public String height = "";
	public String weight = "";
	public String hairColor = "";
	public String eyeColor = "";
	public String gender = "";
	public String pType = "";
	public String psaType = "";
	public String currentVersionNumber;
	public String insuranceType = "";
	public double insuranceAmount;
	public String insuranceProvider = "";
	public Calendar insuranceExpirationDate;
	protected String anyDateExpired = "N";
	protected String householdIncome="$0.00";
	protected String netRent="$0.00";
	public String moveInDate;
	public String moveOutDate;
	protected String isFromOccupancy;
	protected String lsoId;
	protected String unitDesignation;
	protected int activityCount;
	protected Calendar startDate;
	protected Calendar expirationDate;
	protected String status;
	
	protected String ul = "";
    public String getWorkersCompPolicy() {
		return workersCompPolicy;
	}

	public void setWorkersCompPolicy(String workersCompPolicy) {
		this.workersCompPolicy = workersCompPolicy;
	}

	protected String workersCompPolicy;

	public String getUl() {
		return ul;
	}

	public void setUl(String ul) {
		this.ul = ul;
	}

	/**
	 * This is for showing the red border on the activity manager screen where 4 dates are past today the dates are generalLia bilityDate,autoLiabilityDate,businessLicenseExpires,workersCompExpires, license expires
	 */

	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return "flase";
		}
	}

	public People() {
		peopleType = new PeopleType();
	}

	public People(int aPeopleId, String aName) {
		this.peopleId = aPeopleId;
		this.name = aName;
	}

	public People(int aPeopleId, String aName, String licenseNbr) {
		this.peopleId = aPeopleId;
		this.name = aName;
		setLicenseNbr(licenseNbr);
	}

	public People(int aPeopleId, PeopleType peopleType, String licenseNbr, String aName, String phoneNbr) {
		this.peopleId = aPeopleId;
		this.peopleType = peopleType;
		this.licenseNbr = licenseNbr;
		this.name = aName;
		this.phoneNbr = phoneNbr;
	}

	public void setInsuranceExpirationDate(Date aInsuranceExpirationDate) {
		if (aInsuranceExpirationDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aInsuranceExpirationDate);
			this.insuranceExpirationDate = cal;
		}

	}

	public String getInsuranceProvider() {
		return insuranceProvider;
	}

	public void setInsuranceProvider(String insuranceProvider) {
		this.insuranceProvider = insuranceProvider;
	}

	public Calendar getInsuranceExpirationDate() {
		return insuranceExpirationDate;
	}

	public void setInsuranceExpirationDate(Calendar insuranceExpirationDate) {
		this.insuranceExpirationDate = insuranceExpirationDate;
	}

	public String getInsuranceType() {
		return insuranceType;
	}

	public void setInsuranceType(String insuranceType) {
		this.insuranceType = insuranceType;
	}

	public double getInsuranceAmount() {
		return insuranceAmount;
	}

	public void setInsuranceAmount(double insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}

	/**
	 * Gets the peopleId
	 * 
	 * @return Returns a int
	 */
	public int getPeopleId() {
		return peopleId;
	}

	/**
	 * Sets the peopleId
	 * 
	 * @param peopleId
	 *            The peopleId to set
	 */
	public void setPeopleId(int peopleId) {
		this.peopleId = peopleId;
	}

	/**
	 * Gets the name
	 * 
	 * @return Returns a String
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name
	 * 
	 * @param name
	 *            The name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the address
	 * 
	 * @return Returns a String
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address
	 * 
	 * @param address
	 *            The address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Gets the city
	 * 
	 * @return Returns a String
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city
	 * 
	 * @param city
	 *            The city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the state
	 * 
	 * @return Returns a String
	 */
	public String getState() {
		return state;
	}

	/**
	 * Sets the state
	 * 
	 * @param state
	 *            The state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Gets the zipCode
	 * 
	 * @return Returns a String
	 */
	public String getZipCode() {
		return zipCode;
	}

	/**
	 * Sets the zipCode
	 * 
	 * @param zipCode
	 *            The zipCode to set
	 */
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	/**
	 * Gets the phoneNbr
	 * 
	 * @return Returns a String
	 */
	public String getPhoneNbr() {
		return phoneNbr;
	}

	/**
	 * Sets the phoneNbr
	 * 
	 * @param phoneNbr
	 *            The phoneNbr to set
	 */
	public void setPhoneNbr(String phoneNbr) {
		this.phoneNbr = phoneNbr;
	}

	/**
	 * Gets the faxNbr
	 * 
	 * @return Returns a String
	 */
	public String getFaxNbr() {
		return faxNbr;
	}

	/**
	 * Sets the faxNbr
	 * 
	 * @param faxNbr
	 *            The faxNbr to set
	 */
	public void setFaxNbr(String faxNbr) {
		this.faxNbr = faxNbr;
	}

	/**
	 * Gets the emailAddress
	 * 
	 * @return Returns a String
	 */
	public String getEmailAddress() {
		return emailAddress;
	}

	/**
	 * Sets the emailAddress
	 * 
	 * @param emailAddress
	 *            The emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * Gets the comments
	 * 
	 * @return Returns a String
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * Sets the comments
	 * 
	 * @param comments
	 *            The comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Gets the licenseNbr
	 * 
	 * @return Returns a String
	 */
	public String getLicenseNbr() {
		return licenseNbr;
	}

	/**
	 * Sets the licenseNbr
	 * 
	 * @param licenseNbr
	 *            The licenseNbr to set
	 */
	public void setLicenseNbr(String licenseNbr) {
		this.licenseNbr = (licenseNbr == null) ? null : licenseNbr.trim().toUpperCase();
	}

	/**
	 * Gets the licenseExpires
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getLicenseExpires() {
		return licenseExpires;
	}

	/**
	 * Sets the licenseExpires
	 * 
	 * @param licenseExpires
	 *            The licenseExpires to set
	 */
	public void setLicenseExpires(Calendar licenseExpires) {
		this.licenseExpires = licenseExpires;

		if (this.licenseExpires.after(date)) {
			this.isExpired = "N";
		} else {
			this.isExpired = "Y";
		}
	}

	/**
	 * @return Returns the dlExpiryDate.
	 */
	public Calendar getDlExpiryDate() {
		return dlExpiryDate;
	}

	/**
	 * @param dlExpiryDate
	 *            The dlExpiryDate to set.
	 */
	public void setDlExpiryDate(Calendar dlExpiryDate) {

		this.dlExpiryDate = dlExpiryDate;

	}

	/**
	 * @param dlExpiryDate
	 *            The dlExpiryDate to set.
	 */
	public void setDlExpiryDate(Date aDlExpiryDate) {
		if (aDlExpiryDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aDlExpiryDate);
			this.dlExpiryDate = cal;
		}
	}

	/**
	 * Gets the onHold
	 * 
	 * @return Returns a boolean
	 */
	public String getOnHold() {
		return onHold;
	}

	/**
	 * Sets the onHold
	 * 
	 * @param onHold
	 *            The onHold to set
	 */
	public void setOnHold(String onHold) {
		this.onHold = onHold;
	}

	/**
	 * Gets the holdDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getHoldDate() {
		return holdDate;
	}

	/**
	 * Sets the holdDate
	 * 
	 * @param holdDate
	 *            The holdDate to set
	 */
	public void setHoldDate(Calendar holdDate) {
		this.holdDate = holdDate;
	}

	/**
	 * Gets the holdComments
	 * 
	 * @return Returns a String
	 */
	public String getHoldComments() {
		return holdComments;
	}

	/**
	 * Sets the holdComments
	 * 
	 * @param holdComments
	 *            The holdComments to set
	 */
	public void setHoldComments(String holdComments) {
		this.holdComments = holdComments;
	}

	/**
	 * Gets the depositAmount
	 * 
	 * @return Returns a float
	 */
	public float getDepositAmount() {
		return depositAmount;
	}

	/**
	 * Sets the depositAmount
	 * 
	 * @param depositAmount
	 *            The depositAmount to set
	 */
	public void setDepositAmount(float depositAmount) {
		this.depositAmount = depositAmount;
	}

	/**
	 * Gets the agentName
	 * 
	 * @return Returns a String
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * Sets the agentName
	 * 
	 * @param agentName
	 *            The agentName to set
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * Gets the agentPhone
	 * 
	 * @return Returns a String
	 */
	public String getAgentPhone() {
		return agentPhone;
	}

	/**
	 * Sets the agentPhone
	 * 
	 * @param agentPhone
	 *            The agentPhone to set
	 */
	public void setAgentPhone(String agentPhone) {
		this.agentPhone = agentPhone;
	}

	/**
	 * Gets the generalLiabilityDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getGeneralLiabilityDate() {
		return generalLiabilityDate;
	}

	/**
	 * Sets the generalLiabilityDate
	 * 
	 * @param generalLiabilityDate
	 *            The generalLiabilityDate to set
	 */
	public void setGeneralLiabilityDate(Calendar generalLiabilityDate) {
		this.generalLiabilityDate = generalLiabilityDate;
	}

	/**
	 * Gets the autoLiabilityDate
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getAutoLiabilityDate() {
		return autoLiabilityDate;
	}

	/**
	 * Sets the autoLiabilityDate
	 * 
	 * @param autoLiabilityDate
	 *            The autoLiabilityDate to set
	 */
	public void setAutoLiabilityDate(Calendar autoLiabilityDate) {
		this.autoLiabilityDate = autoLiabilityDate;
	}

	/**
	 * Gets the businessLicenseNbr
	 * 
	 * @return Returns a String
	 */
	public String getBusinessLicenseNbr() {
		return businessLicenseNbr;
	}

	/**
	 * Sets the businessLicenseNbr
	 * 
	 * @param businessLicenseNbr
	 *            The businessLicenseNbr to set
	 */
	public void setBusinessLicenseNbr(String businessLicenseNbr) {
		this.businessLicenseNbr = businessLicenseNbr;
	}

	/**
	 * Gets the businessLicenseExpires
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getBusinessLicenseExpires() {
		return businessLicenseExpires;
	}

	/**
	 * Sets the businessLicenseExpires
	 * 
	 * @param businessLicenseExpires
	 *            The businessLicenseExpires to set
	 */
	public void setBusinessLicenseExpires(Calendar businessLicenseExpires) {
		this.businessLicenseExpires = businessLicenseExpires;
	}

	/**
	 * Gets the workersCompExpires
	 * 
	 * @return Returns a Calendar
	 */
	public Calendar getWorkersCompExpires() {
		return workersCompExpires;
	}

	/**
	 * Sets the workersCompExpires
	 * 
	 * @param workersCompExpires
	 *            The workersCompExpires to set
	 */
	public void setWorkersCompExpires(Calendar workersCompExpires) {
		this.workersCompExpires = workersCompExpires;
	}

	public void setBusinessLicenseExpires(Date businessLicenseExpires) {
		if (businessLicenseExpires != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(businessLicenseExpires);
			this.businessLicenseExpires = cal;
		}
	}

	public void setAutoLiabilityDate(Date aAutoLiabilityDate) {
		if (aAutoLiabilityDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aAutoLiabilityDate);
			this.autoLiabilityDate = cal;
		}
	}

	public void setGeneralLiabilityDate(Date aGeneralLiabilityDate) {
		if (aGeneralLiabilityDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aGeneralLiabilityDate);
			this.generalLiabilityDate = cal;
		}
	}

	public void setWorkersCompExpires(Date aWorkersCompExpires) {
		if (aWorkersCompExpires != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aWorkersCompExpires);
			this.workersCompExpires = cal;
		}
	}

	public void setLicenseExpires(Date aLicenseExpires) {
		if (aLicenseExpires != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aLicenseExpires);
			this.licenseExpires = cal;
		}
	}

	public void setHoldDate(Date holdDate) {
		if (holdDate != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(holdDate);
			this.holdDate = cal;
		}
	}

	/**
	 * Gets the workersCompensationWaive
	 * 
	 * @return Returns a String
	 */
	public String getWorkersCompensationWaive() {
		return workersCompensationWaive;
	}

	/**
	 * Sets the workersCompensationWaive
	 * 
	 * @param workersCompensationWaive
	 *            The workersCompensationWaive to set
	 */
	public void setWorkersCompensationWaive(String workersCompensationWaive) {
		this.workersCompensationWaive = workersCompensationWaive;
	}

	/**
	 * Gets the copyApplicant
	 * 
	 * @return Returns a String
	 */
	public String getCopyApplicant() {
		return copyApplicant;
	}

	/**
	 * Sets the copyApplicant
	 * 
	 * @param copyApplicant
	 *            The copyApplicant to set
	 */
	public void setCopyApplicant(String copyApplicant) {
		this.copyApplicant = copyApplicant;
	}

	/**
	 * Gets the ext
	 * 
	 * @return Returns a String
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * Sets the ext
	 * 
	 * @param ext
	 *            The ext to set
	 */
	public void setExt(String ext) {
		this.ext = ext;
	}

	/**
	 * @return the peopleType
	 */
	public PeopleType getPeopleType() {
		return peopleType;
	}

	/**
	 * @param peopleType the peopleType to set
	 */
	public void setPeopleType(PeopleType peopleType) {
		this.peopleType = peopleType;
	}

	/**
	 * Returns the hicFlag.
	 * 
	 * @return String
	 */
	public String getHicFlag() {
		return hicFlag;
	}

	/**
	 * Sets the hicFlag.
	 * 
	 * @param hicFlag
	 *            The hicFlag to set
	 */
	public void setHicFlag(String hicFlag) {
		this.hicFlag = hicFlag;
	}

	/**
	 * Returns the date.
	 * 
	 * @return String
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Returns the isExpired.
	 * 
	 * @return String
	 */
	public String getIsExpired() {
		return isExpired;
	}

	/**
	 * Sets the date.
	 * 
	 * @param date
	 *            The date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Sets the isExpired.
	 * 
	 * @param isExpired
	 *            The isExpired to set
	 */
	public void setIsExpired(String isExpired) {
		this.isExpired = isExpired;
	}

	/**
	 * Returns the aLDate.
	 * 
	 * @return String
	 */
	public String getALDate() {
		return ALDate;
	}

	/**
	 * Returns the bLExpirationDate.
	 * 
	 * @return String
	 */
	public String getBLExpirationDate() {
		return BLExpirationDate;
	}

	/**
	 * Returns the gLDate.
	 * 
	 * @return String
	 */
	public String getGLDate() {
		return GLDate;
	}

	/**
	 * Returns the wCEDate.
	 * 
	 * @return String
	 */
	public String getWCEDate() {
		return WCEDate;
	}

	/**
	 * Sets the aLDate.
	 * 
	 * @param aLDate
	 *            The aLDate to set
	 */
	public void setALDate(String aLDate) {
		ALDate = aLDate;
	}

	/**
	 * Sets the bLExpirationDate.
	 * 
	 * @param bLExpirationDate
	 *            The bLExpirationDate to set
	 */
	public void setBLExpirationDate(String bLExpirationDate) {
		BLExpirationDate = bLExpirationDate;
	}

	/**
	 * Sets the gLDate.
	 * 
	 * @param gLDate
	 *            The gLDate to set
	 */
	public void setGLDate(String gLDate) {
		GLDate = gLDate;
	}

	/**
	 * Sets the wCEDate.
	 * 
	 * @param wCEDate
	 *            The wCEDate to set
	 */
	public void setWCEDate(String wCEDate) {
		WCEDate = wCEDate;
	}

	/**
	 * @return
	 */
	public boolean getPcPeople() {
		return pcPeople;
	}

	/**
	 * @param b
	 */
	public void setPcPeople(boolean b) {
		pcPeople = b;
	}

	/**
	 * @return
	 */
	public String getLevelType() {
		return levelType;
	}

	/**
	 * @param string
	 */
	public void setLevelType(String string) {
		levelType = string;
	}

	/**
	 * @return
	 */
	public Calendar getToday() {
		return today;
	}

	/**
	 * @param calendar
	 */
	public void setToday(Calendar calendar) {
		today = calendar;
	}

	/**
	 * @return - anand belaguly
	 */
	public String getAnyDateExpired() {
		logger.info("getting anyDateExpired");
		logger.debug("businessLicenseExpires :" + businessLicenseExpires);
		logger.debug("autoLiabilityDate :" + autoLiabilityDate);
		logger.debug("generalLiabilityDate :" + generalLiabilityDate);
		logger.debug("workersCompExpires :" + workersCompExpires);
		logger.debug("licenseExpires :" + workersCompExpires);
		logger.debug("people type is " + peopleType.getCode());
		if (peopleType.getCode().equals("C")) {
			logger.debug("people type is contractor");
			if ((businessLicenseExpires != null ? (businessLicenseExpires.before(today) ? "Y" : "N") : "N").equals("Y") || (autoLiabilityDate != null ? (autoLiabilityDate.before(today) ? "Y" : "N") : "N").equals("Y") || (generalLiabilityDate != null ? (generalLiabilityDate.before(today) ? "Y" : "N") : "N").equals("Y") || (workersCompExpires != null ? (workersCompExpires.before(today) ? "Y" : "N") : "N").equals("Y") || (licenseExpires != null ? (licenseExpires.before(today) ? "Y" : "N") : "N").equals("Y")) {
				anyDateExpired = "Y";
				logger.debug("any of the 5 dates are expired and people type is contractor");
			}
		} else if (peopleType.getCode().equals("A")) {
			logger.debug("people type is Applicant");
			if ((autoLiabilityDate != null ? (autoLiabilityDate.before(today) ? "Y" : "N") : "N").equals("Y") || (generalLiabilityDate != null ? (generalLiabilityDate.before(today) ? "Y" : "N") : "N").equals("Y")) {
				anyDateExpired = "Y";
				logger.debug("if autoLiabilityDate & generalLiabilityDate are expired, then this contractor should be hightlighted red");
			}
		} else {
			anyDateExpired = "N";

			logger.debug("contrator and applicant licenses are not expired.");
		}

		return StringUtils.nullReplaceWithEmpty(anyDateExpired);
	}

	/**
	 * @param string
	 */
	public void setAnyDateExpired(String string) {
		anyDateExpired = StringUtils.nullReplaceWithEmpty(string);
	}

	/**
	 * @return
	 */
	public String getStrDepositAmount() {
		return strDepositAmount;
	}

	/**
	 * @param string
	 */
	public void setStrDepositAmount(String string) {
		strDepositAmount = string;
	}

	/**
	 * @return Returns the logger.
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * @param logger
	 *            The logger to set.
	 */
	public static void setLogger(Logger logger) {
		People.logger = logger;
	}

	/**
	 * @return Returns the businessName.
	 */
	public String getBusinessName() {
		return businessName;
	}

	/**
	 * @param businessName
	 *            The businessName to set.
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	/**
	 * @return Returns the corporateName.
	 */
	public String getCorporateName() {
		return corporateName;
	}

	/**
	 * @param corporateName
	 *            The corporateName to set.
	 */
	public void setCorporateName(String corporateName) {
		this.corporateName = corporateName;
	}

	/**
	 * @return Returns the dateOfBirth.
	 */
	public Calendar getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth
	 *            The dateOfBirth to set.
	 */
	public void setDateOfBirth(Calendar dateOfBirth) {

		this.dateOfBirth = dateOfBirth;

	}

	/**
	 * @param DateofBirth
	 *            The dlExpiryDate to set.
	 */
	public void setDateOfBirth(Date aDateOfBirth) {
		if (aDateOfBirth != null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(aDateOfBirth);
			this.dateOfBirth = cal;
		}
	}

	/**
	 * @return Returns the dlNumber.
	 */
	public String getDlNumber() {
		return dlNumber;
	}

	/**
	 * @param dlNumber
	 *            The dlNumber to set.
	 */
	public void setDlNumber(String dlNumber) {
		this.dlNumber = dlNumber;
	}

	/**
	 * @return Returns the dlNumberState.
	 */
	public String getDlNumberState() {
		return dlNumberState;
	}

	/**
	 * @param dlNumberState
	 *            The dlNumberState to set.
	 */
	public void setDlNumberState(String dlNumberState) {
		this.dlNumberState = dlNumberState;
	}

	/**
	 * @return Returns the eyeColor.
	 */
	public String getEyeColor() {
		return eyeColor;
	}

	/**
	 * @param eyeColor
	 *            The eyeColor to set.
	 */
	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}

	/**
	 * @return Returns the firstName.
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 *            The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return Returns the gender.
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            The gender to set.
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return Returns the hairColor.
	 */
	public String getHairColor() {
		return hairColor;
	}

	/**
	 * @param hairColor
	 *            The hairColor to set.
	 */
	public void setHairColor(String hairColor) {
		this.hairColor = hairColor;
	}

	/**
	 * @return Returns the height.
	 */
	public String getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            The height to set.
	 */
	public void setHeight(String height) {
		this.height = height;
	}

	/**
	 * @return Returns the homeAddress.
	 */
	public String getHomeAddress() {
		return homeAddress;
	}

	/**
	 * @param homeAddress
	 *            The homeAddress to set.
	 */
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}

	/**
	 * @return Returns the homeCity.
	 */
	public String getHomeCity() {
		return homeCity;
	}

	/**
	 * @param homeCity
	 *            The homeCity to set.
	 */
	public void setHomeCity(String homeCity) {
		this.homeCity = homeCity;
	}

	/**
	 * @return Returns the homePhone.
	 */
	public String getHomePhone() {
		return homePhone;
	}

	/**
	 * @param homePhone
	 *            The homePhone to set.
	 */
	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return Returns the homeState.
	 */
	public String getHomeState() {
		return homeState;
	}

	/**
	 * @param homeState
	 *            The homeState to set.
	 */
	public void setHomeState(String homeState) {
		this.homeState = homeState;
	}

	/**
	 * @return Returns the mailingAddress.
	 */
	public String getMailingAddress() {
		return mailingAddress;
	}

	/**
	 * @param mailingAddress
	 *            The mailingAddress to set.
	 */
	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}

	/**
	 * @return Returns the mailingCity.
	 */
	public String getMailingCity() {
		return mailingCity;
	}

	/**
	 * @param mailingCity
	 *            The mailingCity to set.
	 */
	public void setMailingCity(String mailingCity) {
		this.mailingCity = mailingCity;
	}

	/**
	 * @return Returns the mailingState.
	 */
	public String getMailingState() {
		return mailingState;
	}

	/**
	 * @param mailingState
	 *            The mailingState to set.
	 */
	public void setMailingState(String mailingState) {
		this.mailingState = mailingState;
	}

	/**
	 * @return Returns the ssn.
	 */
	public String getSsn() {
		return ssn;
	}

	/**
	 * @param ssn
	 *            The ssn to set.
	 */
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the weight.
	 */
	public String getWeight() {
		return weight;
	}

	/**
	 * @param weight
	 *            The weight to set.
	 */
	public void setWeight(String weight) {
		this.weight = weight;
	}

	/**
	 * @return Returns the homeZipCode.
	 */
	public String getHomeZipCode() {
		return homeZipCode;
	}

	/**
	 * @param homeZipCode
	 *            The homeZipCode to set.
	 */
	public void setHomeZipCode(String homeZipCode) {
		this.homeZipCode = homeZipCode;
	}

	/**
	 * @return Returns the homeZipCode4.
	 */
	public String getHomeZipCode4() {
		return homeZipCode4;
	}

	/**
	 * @param homeZipCode4
	 *            The homeZipCode4 to set.
	 */
	public void setHomeZipCode4(String homeZipCode4) {
		this.homeZipCode4 = homeZipCode4;
	}

	/**
	 * @return Returns the mailingZipCode.
	 */
	public String getMailingZipCode() {
		return mailingZipCode;
	}

	/**
	 * @param mailingZipCode
	 *            The mailingZipCode to set.
	 */
	public void setMailingZipCode(String mailingZipCode) {
		this.mailingZipCode = mailingZipCode;
	}

	/**
	 * @return Returns the mailingZipCode4.
	 */
	public String getMailingZipCode4() {
		return mailingZipCode4;
	}

	/**
	 * @param mailingZipCode4
	 *            The mailingZipCode4 to set.
	 */
	public void setMailingZipCode4(String mailingZipCode4) {
		this.mailingZipCode4 = mailingZipCode4;
	}

	/**
	 * @return Returns the businessPhone.
	 */
	public String getBusinessPhone() {
		return businessPhone;
	}

	/**
	 * @param businessPhone
	 *            The businessPhone to set.
	 */
	public void setBusinessPhone(String businessPhone) {
		this.businessPhone = businessPhone;
	}

	/**
	 * @return Returns the businessPhoneExt.
	 */
	public String getBusinessPhoneExt() {
		return businessPhoneExt;
	}

	/**
	 * @param businessPhoneExt
	 *            The businessPhoneExt to set.
	 */
	public void setBusinessPhoneExt(String businessPhoneExt) {
		this.businessPhoneExt = businessPhoneExt;
	}

	/**
	 * @return Returns the lastName.
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            The lastName to set.
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return Returns the versionNumber.
	 */
	public int getVersionNumber() {
		return versionNumber;
	}

	/**
	 * @param versionNumber
	 *            The versionNumber to set.
	 */
	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}

	/**
	 * @return Returns the hasHold.
	 */
	public String getHasHold() {
		return hasHold;
	}

	/**
	 * @param hasHold
	 *            The hasHold to set.
	 */
	public void setHasHold(String hasHold) {
		this.hasHold = hasHold;
	}

	/**
	 * @return Returns the pType.
	 */
	public String getPType() {
		return pType;
	}

	/**
	 * @param type
	 *            The pType to set.
	 */
	public void setPType(String type) {
		pType = type;
	}

	/**
	 * @return Returns the activityId.
	 */
	public int getActivityId() {
		return activityId;
	}

	/**
	 * @param activityId
	 *            The activityId to set.
	 */
	public void setActivityId(int activityId) {
		this.activityId = activityId;
	}

	/**
	 * @return Returns the psaType.
	 */
	public String getPsaType() {
		return psaType;
	}

	/**
	 * @param psaType
	 *            The psaType to set.
	 */
	public void setPsaType(String psaType) {
		this.psaType = psaType;
	}

	/**
	 * @return Returns the dlExpiryDateString.
	 */
	public String getDlExpiryDateString() {
		return dlExpiryDateString;
	}

	/**
	 * @param dlExpiryDateString
	 *            The dlExpiryDateString to set.
	 */
	public void setDlExpiryDateString(String dLExpiryDateString) {
		if (dLExpiryDateString != null) {
			this.dlExpiryDateString = dLExpiryDateString;
		} else {
			dLExpiryDateString = "Y";
			this.dlExpiryDateString = dLExpiryDateString;
		}
	}

	/**
	 * @return Returns the currentVersionNumber.
	 */
	public String getCurrentVersionNumber() {
		return currentVersionNumber;
	}

	/**
	 * @param currentVersionNumber
	 *            The currentVersionNumber to set.
	 */
	public void setCurrentVersionNumber(String currentVersionNumber) {
		this.currentVersionNumber = currentVersionNumber;
	}

	/**
	 * @return Returns the confirmDlNumber.
	 */
	public String getConfirmDlNumber() {
		return confirmDlNumber;
	}

	/**
	 * @param confirmDlNumber
	 *            The confirmDlNumber to set.
	 */
	public void setConfirmDlNumber(String confirmDlNumber) {
		this.confirmDlNumber = confirmDlNumber;
	}

	/**
	 * @return Returns the confirmStrDlExpiryDate.
	 */
	public String getConfirmStrDlExpiryDate() {
		return confirmStrDlExpiryDate;
	}

	/**
	 * @param confirmStrDlExpiryDate
	 *            The confirmStrDlExpiryDate to set.
	 */
	public void setConfirmStrDlExpiryDate(String confirmStrDlExpiryDate) {
		this.confirmStrDlExpiryDate = confirmStrDlExpiryDate;

	}

	/**
	 * @return Returns the confirmSsn.
	 */
	public String getConfirmSsn() {
		return confirmSsn;
	}

	/**
	 * @param confirmSsn
	 *            The confirmSsn to set.
	 */
	public void setConfirmSsn(String confirmSsn) {
		this.confirmSsn = confirmSsn;
	}

	/**
	 * @return the pType
	 */
	public String getpType() {
		return pType;
	}

	/**
	 * @param pType the pType to set
	 */
	public void setpType(String pType) {
		this.pType = pType;
	}

	/**
	 * @return the householdIncome
	 */
	public String getHouseholdIncome() {
		return householdIncome;
	}

	/**
	 * @param householdIncome the householdIncome to set
	 */
	public void setHouseholdIncome(String householdIncome) {
		this.householdIncome = householdIncome;
	}

	/**
	 * @return the netRent
	 */
	public String getNetRent() {
		return netRent;
	}

	/**
	 * @param netRent the netRent to set
	 */
	public void setNetRent(String netRent) {
		this.netRent = netRent;
	}

	/**
	 * @return the isFromOccupancy
	 */
	public String getIsFromOccupancy() {
		return isFromOccupancy;
	}

	/**
	 * @param isFromOccupancy the isFromOccupancy to set
	 */
	public void setIsFromOccupancy(String isFromOccupancy) {
		this.isFromOccupancy = isFromOccupancy;
	}

	/**
	 * @return the lsoId
	 */
	public String getLsoId() {
		return lsoId;
	}

	/**
	 * @param lsoId the lsoId to set
	 */
	public void setLsoId(String lsoId) {
		this.lsoId = lsoId;
	}

	/**
	 * @return the unitDesignation
	 */
	public String getUnitDesignation() {
		return unitDesignation;
	}

	/**
	 * @param unitDesignation the unitDesignation to set
	 */
	public void setUnitDesignation(String unitDesignation) {
		this.unitDesignation = unitDesignation;
	}

	/**
	 * @return the moveInDate
	 */
	public String getMoveInDate() {
		return moveInDate;
	}

	/**
	 * @param moveInDate the moveInDate to set
	 */
	public void setMoveInDate(String moveInDate) {
		this.moveInDate = moveInDate;
	}

	/**
	 * @return the moveOutDate
	 */
	public String getMoveOutDate() {
		return moveOutDate;
	}

	/**
	 * @param moveOutDate the moveOutDate to set
	 */
	public void setMoveOutDate(String moveOutDate) {
		this.moveOutDate = moveOutDate;
	}

	public int getActivityCount() {
		return activityCount;
	}

	public void setActivityCount(int activityCount) {
		this.activityCount = activityCount;
	}
	
	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		if (startDate == null)
			this.startDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(startDate);
			this.startDate = calendar;
		}
		
	}

	public Calendar getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		if (expirationDate == null)
			this.expirationDate = null;
		else {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(expirationDate);
			this.expirationDate = calendar;
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCaId() {
		return caId;
	}

	public void setCaId(int caId) {
		this.caId = caId;
	}
}

/*
 * Created on 25-Sep-2008
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package elms.app.people;

/**
 * @author Gayathri
 * 
 *         TODO To change the template for this generated type comment go to Window - Preferences - Java - Code Style - Code Templates
 */
public class EyeColorType {
	public int id;
	public String description;

	/**
	 * 
	 */
	public EyeColorType() {
		super();
	}

	/**
	 * @param id
	 * @param description
	 */
	public EyeColorType(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return Returns the id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
}

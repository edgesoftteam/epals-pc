package elms.agent;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.sql.RowSet;

import org.apache.log4j.Logger;

import sun.jdbc.rowset.CachedRowSet;
import elms.app.admin.CountByHoldType;
import elms.app.common.ActivityTeamObject;
import elms.app.common.Attachment;
import elms.app.common.Comment;
import elms.app.common.Condition;
import elms.app.common.ConditionLibraryRecord;
import elms.app.common.Hold;
import elms.app.common.Module;
import elms.app.common.ObcFile;
import elms.app.common.ProcessTeamCollectionRecord;
import elms.app.common.ProcessTeamNameRecord;
import elms.app.common.ProcessTeamRecord;
import elms.app.common.ProcessTeamTitleRecord;
import elms.app.common.Tenant;
import elms.app.enforcement.CodeEnforcement;
import elms.common.Constants;
import elms.control.beans.ConditionForm;
import elms.control.beans.ConditionLibraryForm;
import elms.control.beans.ProcessTeamForm;
import elms.exception.DuplicateRecordException;
import elms.gsearch.GlobalSearch;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class CommonAgent implements Serializable {
	static Logger logger = Logger.getLogger(CommonAgent.class.getName());

	public static final String ADD_UPDATE_HOLD = "INSERT INTO HOLDS (HOLD_ID,LEVEL_ID,HOLD_LEVEL,TITLE,DESCRIPTION,HOLD_TYPE,HOLD_STAT_DT,STAT,CREATED_BY,CREATION_DT,UPDATED_BY,UPDATE_DT) VALUES(?,?,?,?,?,?,?,?,?,sysdate,?,sysdate)";

	/**
	 * populates attachments
	 * 
	 * @param rs
	 * @return
	 */
	private Attachment populateAttachment(RowSet rs) {
		Attachment attachment = new Attachment();
		String fileNameStr = "";

		try {
			attachment.setAttachmentId(rs.getInt("attach_id"));
			logger.debug("populateAttachment() -- attachmentId is set to attachement as " + attachment.getAttachmentId());
			attachment.setAttachmentLevel(rs.getString("attach_level"));
			logger.debug("populateAttachment() -- AttachmentLevel is set to attachement as " + attachment.getAttachmentLevel());
			attachment.setLevelId(rs.getInt("level_id"));
			logger.debug("populateAttachment() -- levelid is set to attachement as " + attachment.getAttachmentLevel());
			attachment.setDescription(rs.getString("description"));
			logger.debug("populateAttachment() -- description is set to attachement as " + attachment.getAttachmentLevel());
			attachment.setEnteredBy(rs.getInt("created_by"));
			logger.debug("populateAttachment() -- created by is set to attachement as " + attachment.getAttachmentLevel());
			attachment.setDeleted(rs.getString("deleted"));
			logger.debug("populateAttachment() -- deleted is set to attachement as " + attachment.getAttachmentLevel());

			if (rs.getString("keyword1") == null) {
				attachment.setKeyword1("");
			} else {
				attachment.setKeyword1(rs.getString("keyword1"));
			}

			logger.debug("populateAttachment() -- keyword1 is set to attachement as " + attachment.getKeyword1());

			if (rs.getString("keyword2") == null) {
				attachment.setKeyword2("");
			} else {
				attachment.setKeyword2(rs.getString("keyword2"));
			}

			logger.debug("populateAttachment() -- keyword2 is set to attachement as " + attachment.getKeyword2());

			if (rs.getString("keyword3") == null) {
				attachment.setKeyword3("");
			} else {
				attachment.setKeyword3(rs.getString("keyword3"));
			}

			logger.debug("populateAttachment() -- keyword3 is set to attachement as " + attachment.getKeyword3());

			if (rs.getString("keyword4") == null) {
				attachment.setKeyword4("");
			} else {
				attachment.setKeyword4(rs.getString("keyword4"));
			}

			logger.debug("populateAttachment() -- keyword4 is set to attachement as " + attachment.getKeyword4());

			ObcFile obcFile = new ObcFile();
			fileNameStr = rs.getString("file_name");
			if (fileNameStr == null || ("").equalsIgnoreCase(fileNameStr))
				fileNameStr = "";

			obcFile.setFileName(fileNameStr);
			logger.debug("populateAttachment() -- file  -- file_name is set as " + obcFile.getFileName());
			obcFile.setFileLocation(rs.getString("location"));
			logger.debug("populateAttachment() -- file  -- file_location is set as " + obcFile.getFileLocation());
			obcFile.setFileSize(StringUtils.i2nbr(rs.getInt("attach_size")));
			logger.debug("populateAttachment() -- file  -- file_size is set as " + obcFile.getFileSize());
			obcFile.setCreateDate(rs.getDate("created"));
			logger.debug("populateAttachment() -- file  -- created  is set as " + StringUtils.cal2str(obcFile.getCreateDate()));
			obcFile.setStrCreateDate(StringUtils.cal2str(obcFile.getCreateDate()));
			logger.debug("populateAttachment() -- file  --  String created  is set as " + obcFile.getStrCreateDate());
			attachment.setFile(obcFile);
			logger.debug("after populaging attachment obj");
			logger.debug(attachment);
			int lkupAttachTypeId = rs.getInt("LKUP_ATTACHMENT_TYPE_ID");
			CommonAgent commonAgent = new CommonAgent();
			String attachmentType = commonAgent.getAttachmentTypeDesc(lkupAttachTypeId);		
			logger.debug("attachmentType = "+attachmentType);
			attachment.setAttachmentType(attachmentType);
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return attachment;
	}

	/**
	 * gets the attachment using the attachment id
	 * 
	 * @param attachmentId
	 * @return
	 */
	public Attachment getAttachment(int attachmentId) {
		logger.debug("getAttachment(" + attachmentId + ")");

		Attachment attachment = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from attachments where attach_id=" + attachmentId;
			logger.debug(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				attachment = populateAttachment(rs);
			} else {
				attachment = new Attachment();
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in AttachmentAgent getAttachment() ");
			logger.error(attachment);
			logger.error(e);
			e.printStackTrace();
		}

		logger.info("Exiting AttachmentAgent getAttachment()");

		return attachment;
	}

	/**
	 * gets the list of attachments using the level id and level
	 * 
	 * @param levelId
	 * @param level
	 * @return
	 */
	public List getAttachments(int levelId, String level) throws Exception {
		logger.info("getAttachments(" + levelId + "," + level + ")");

		List attachments = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "select attach_id from attachments where level_id=" + levelId + " and attach_level='" + level + "' and deleted='N' order by created desc";
		logger.debug(sql);

		RowSet rs = db.select(sql);

		while (rs.next()) {
			Attachment attachment = getAttachment(rs.getInt("attach_id"));
			attachments.add(attachment);
		}

		if (rs != null) {
			rs.close();
		}
		logger.debug("Attachments list is created with  size .." + attachments.size());

		logger.info("Exiting  AttachmentAgent getAttachment()");

		return attachments;
	}

	/**
	 * gets the list of attachments for the list of attachments ids
	 * 
	 * @param attachmentIds
	 * @return
	 */
	public List getAttachments(List attachmentIds) {
		if (attachmentIds == null) {
			attachmentIds = new ArrayList();
		}

		Iterator itr = attachmentIds.iterator();
		List attachments = new ArrayList();

		while (itr.hasNext()) {
			attachments.add(getAttachment(StringUtils.s2i((String) itr.next())));
		}

		return attachments;
	}

	/**
	 * deletes the attachment for a given attachment id.
	 * 
	 * @param attachmentId
	 */
	public void deleteAttachment(int attachmentId, String level) throws Exception {
		logger.debug("deleteAttachment(" + attachmentId + "," + level + ")");

		Wrapper db = new Wrapper();

		// get the file name of the attachment table
		String sql = "select file_name from attachments where attach_id=" + attachmentId;
		logger.info(sql);

		String fileName = "";
		RowSet rs = db.select(sql);

		if (rs.next()) {
			fileName = rs.getString(1);
			logger.debug("obtained file name is " + fileName);
		}

		if (rs != null) {
			rs.close();
		}

		// delete attachment from physical storage also
		if (!(fileName.equals(""))) {
			deleteFtpFile(fileName);

			// logger.debug("physical file deleted successfully");
		} else {
			throw new Exception("File name does not exist");
		}

		//sql = "delete from attachments where attach_id=" + attachmentId;
		sql = "update attachments set DELETED='Y',UPDATED=CURRENT_TIMESTAMP where attach_id=" + attachmentId;
		logger.info(sql);
		db.update(sql);
		try {
			String attachmentsDeltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_ATTACHMENTS_DELTA_INDEX"));
			if(!attachmentsDeltaIndex.equals("") && attachmentsDeltaIndex.equalsIgnoreCase("YES")){
			GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_ATTACHMENT"), "id",StringUtils.i2s(attachmentId));
			}
		} catch (Exception e) {
			e.getMessage();
		}
		logger.debug("Attachment deleted successfully from database");
	}

	/**
	 * gets the attachment list as a cached rowset.
	 * 
	 * @param id
	 * @param count
	 * @param level
	 * @return
	 */
	public CachedRowSet getAttachmentList(int id, int count, String level) {
		String sql = "";
		CachedRowSet rs;

		try {
			rs = new CachedRowSet();

			logger.debug("rs value is : " + rs);

			Wrapper db = new Wrapper();
			sql = "select * from attachments where level_id=" + id + " and  attach_level = '" + level + "' and deleted='N' ";

			if (count > 0) {
				sql += (" and rownum <= " + count);
			}

			sql += (" order by created desc fetch first " + count + " rows only");

			logger.debug("rowset sql is " + sql);
			rs.populate(db.select(sql));
			logger.debug("rs value is : " + rs);

			return rs;
		} catch (java.sql.SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
			e.printStackTrace();
		}

		return (null);
	}

	/**
	 * Saves a new attachment and updated attachment to the database.
	 * 
	 * @param aAttachment
	 * @param aContentType
	 * @param aData
	 * @return
	 */
	public int saveAttachment(Attachment aAttachment, String aContentType, InputStream stream) throws Exception {
		int attachmentId = -1;
		if(stream == null)
			stream = aAttachment.getStream();
		if (aAttachment.getAttachmentId() < 1) {
			attachmentId = addAttachmentNew(aAttachment, aContentType, stream);
		} else {
			attachmentId = updateAttachment(aAttachment);
		}
		return attachmentId;
	}

	/**
	 * Adds a new attachment to the database.
	 * 
	 * @param aAttachment
	 * @param aContentType
	 * @param aData
	 * @return
	 */
	private int addAttachmentNew(Attachment aAttachment, String aContentType, InputStream aData) {
		logger.info("addAttachmentNew(Attachment, " + aContentType + ")");
		int attachmentId = -1;

		try {
			java.util.ResourceBundle obcProperties = elms.util.db.Wrapper.getResourceBundle();
			String path = obcProperties.getString("ATTACHMENT_FOLDER");
			logger.debug("The path read from elms.properties for ATTACHMENT_FOLDER is " + path);

			String fileName = aAttachment.getFile().getFileName();
			logger.debug("The obtained filename of the attached file is " + fileName);

			// parse the filename and see if the file has been smart named already.
			if (isFileSmartNamed(aAttachment.getLevelId(), aAttachment.getAttachmentLevel(), fileName)) {
				logger.debug("The file has a smart name already, so using the same name");
			} else {
				logger.debug("The file has not been smart-named, so renaming the file to smart-name");
				fileName = renameTheFile(aAttachment.getLevelId(), aAttachment.getAttachmentLevel(), fileName);
			}

			logger.debug("filename after smart-naming " + fileName);

			String location = path + fileName;

			boolean newFile = saveStringToFile(aData, location, path);
			logger.debug("File created in server " + newFile);

			Wrapper db = new Wrapper();
			attachmentId = db.getNextId("ATTACHMENT_ID");
			logger.debug("Got the Attchment ID" + attachmentId);
			String sql = "";
			if(aAttachment.getAttachmentType() != null && !"".equals(aAttachment.getAttachmentType())) {
				sql = "INSERT INTO ATTACHMENTS(ATTACH_ID,LEVEL_ID,ATTACH_LEVEL,FILE_NAME,DESCRIPTION,LOCATION,ATTACH_SIZE,CREATED_BY,CREATED,DELETED,STATUS,KEYWORD1,KEYWORD2,KEYWORD3,KEYWORD4,LKUP_ATTACHMENT_TYPE_ID) VALUES (" + attachmentId + "," + aAttachment.getLevelId() + "," + StringUtils.checkString(aAttachment.getAttachmentLevel()) + "," + StringUtils.checkString(fileName) + "," + StringUtils.checkString(aAttachment.getDescription()) + "," + StringUtils.checkString(location) + "," + aAttachment.getFile().getFileSize() + "," + aAttachment.getEnteredBy() + "," + "CURRENT_DATE" + "," + "'N','A'" + "," + StringUtils.checkString(aAttachment.getKeyword1()) + "," + StringUtils.checkString(aAttachment.getKeyword2()) + "," + StringUtils.checkString(aAttachment.getKeyword3()) + "," + StringUtils.checkString(aAttachment.getKeyword4()) + ","+aAttachment.getAttachmentType()+")";
			}else {
				sql = "INSERT INTO ATTACHMENTS(ATTACH_ID,LEVEL_ID,ATTACH_LEVEL,FILE_NAME,DESCRIPTION,LOCATION,ATTACH_SIZE,CREATED_BY,CREATED,DELETED,STATUS,KEYWORD1,KEYWORD2,KEYWORD3,KEYWORD4) VALUES (" + attachmentId + "," + aAttachment.getLevelId() + "," + StringUtils.checkString(aAttachment.getAttachmentLevel()) + "," + StringUtils.checkString(fileName) + "," + StringUtils.checkString(aAttachment.getDescription()) + "," + StringUtils.checkString(location) + "," + aAttachment.getFile().getFileSize() + "," + aAttachment.getEnteredBy() + "," + "CURRENT_DATE" + "," + "'N','A'" + "," + StringUtils.checkString(aAttachment.getKeyword1()) + "," + StringUtils.checkString(aAttachment.getKeyword2()) + "," + StringUtils.checkString(aAttachment.getKeyword3()) + "," + StringUtils.checkString(aAttachment.getKeyword4()) + ")";
			}
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Error in Add Attachment");
			attachmentId = -1;
		}

		return attachmentId;
	}

	/**
	 * Updates the attachment details as per the information in attachment object.
	 * 
	 * @param attachment
	 * @return
	 * @throws Exception
	 */
	private int updateAttachment(Attachment attachment) throws Exception {
		try {
			Wrapper db = new Wrapper();
			String sql = "UPDATE ATTACHMENTS SET description=" + StringUtils.checkString(attachment.getDescription()) + "," + "KEYWORD1=" + StringUtils.checkString(attachment.getKeyword1()) + "," + "KEYWORD2=" + StringUtils.checkString(attachment.getKeyword2()) + "," + "KEYWORD3=" + StringUtils.checkString(attachment.getKeyword3()) + "," + "KEYWORD4=" + StringUtils.checkString(attachment.getKeyword4()) + " ,UPDATED=CURRENT_TIMESTAMP " + "WHERE ATTACH_ID=" + attachment.getAttachmentId();
			logger.info(sql);
			db.update(sql);

			return attachment.getAttachmentId();
		} catch (Exception e) {
			logger.error("Exception occured while updating attachment data " + e.getMessage());
			throw new Exception("Occured while updating attachments " + e.getMessage());
		}
	}

	/**
	 * Replaces a new attachment onto the database .
	 * 
	 * @param aAttachment
	 * @param aContentType
	 * @param aData
	 * @return
	 */
	public boolean updateAttachmentNew(Attachment aAttachment, String aContentType, InputStream aData) {
		boolean newFile = false;
		FileOutputStream outputStream = null;
		try {
			java.util.ResourceBundle obcProperties = elms.util.db.Wrapper.getResourceBundle();
			String path = obcProperties.getString("ATTACHMENT_FOLDER");
			logger.debug("The path read from elms.properties for ATTACHMENT_FOLDER is " + path);

			String fileName = aAttachment.getFile().getFileName();
			logger.debug("The obtained filename of the attached file is " + fileName);

			// parse the filename and see if the file has been smart named already.
			if (isFileSmartNamed(aAttachment.getLevelId(), aAttachment.getAttachmentLevel(), fileName)) {
				logger.debug("The file has a smart name already, so using the same name");
			} else {
				logger.debug("The file has not been smart-named, so renaming the file to smart-name");
				fileName = renameTheFile(aAttachment.getLevelId(), aAttachment.getAttachmentLevel(), fileName);
			}

			logger.debug("filename after smart-naming " + fileName);

			String location = path + fileName;

			newFile = saveStringToFile(aData, location, path);
			logger.debug("File created in server " + newFile);
		} catch (Exception e) {
			logger.error("Error in Add Attachment");
			newFile = false;
		}

		return newFile;
	}

	/**
	 * saves the string to file.
	 * 
	 * @param location
	 * @param path
	 * @return
	 * @throws Exception
	 */

	private boolean saveStringToFile(InputStream stream, String location, String path) throws Exception {
		logger.info("saveStringToFile( file, " + location + ", " + path + ")");

		boolean didConvert = false;
		OutputStream bos = null;

		try {

			File dir = new File(path);
			logger.debug("file created");
			if (!dir.exists()) {
				logger.debug("directory does not exist, creating...");
				dir.mkdirs();
			}

			logger.debug("trying to create the input stream");
			logger.debug("obtained the input stream from the file");

			bos = new FileOutputStream(location);
			logger.debug("created new output stream for the file");

			int bytesRead = 0;
			final byte[] buffer = new byte[1024];

			while ((bytesRead = stream.read(buffer)) != -1) {
				bos.write(buffer, 0, bytesRead);
			}

			logger.debug("completed writing the file");
			return didConvert;
		} catch (Exception e) {
			logger.error("problem while saving file " + e.getMessage());
			didConvert = false;
			throw new Exception("Problem while saving file " + e.getMessage());
		} finally {
			if (bos != null) {
				bos.close();
			}
		}
	}

	/**
	 * gets the attachment count for the attachment level and level id
	 * 
	 * @param levelId
	 * @param attachmentLevel
	 * @return
	 */
	public int getAttachmentCount(int levelId, String attachmentLevel) {
		int attachmentCount = 0;
		String sql = "";

		try {
			Wrapper db = new Wrapper();

			if (attachmentLevel.equals("Q")) {
				sql = "select count(*) as count from attachments where level_Id=" + levelId + " and attach_level='Q' and deleted='N'";
				logger.debug("The sub project Attachment count sql" + sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					attachmentCount = rs.getInt("count");
				}

				rs.close();

				// getting attachments for all activities in the sub project
				sql = "select count(*) as count from attachments where  level_id in (select act_id from activity where sproj_id=" + levelId + ")  and attach_level='A' and deleted='N'";
				logger.debug("The Sub projects activities  Attachment count  sql" + sql);
				rs = db.select(sql);

				if (rs.next()) {
					attachmentCount = attachmentCount + rs.getInt("count");
				}

				if (rs != null) {
					rs.close();
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}

		return attachmentCount;
	}

	/**
	 * Rename the filename to the smart-name of the file
	 * 
	 * @param levelId
	 * @param levelType
	 * @param fileName
	 * @return
	 */
	public String renameTheFile(int levelId, String levelType, String fileName) {
		logger.info("renameTheFile(" + levelId + ", " + levelType + ", " + fileName + ")");
		if (levelType == null) {
			levelType = "";
		}

		if (fileName == null) {
			fileName = "";
		}

		String newFileName = "";
		String sql = "";
		try {
		if (levelType.equals("L") || levelType.equals("S") || levelType.equals("O")) {
			sql = "select dl_address as name from v_address_list where lso_id=" + levelId;
		} else if (levelType.equals("P")) {
			sql = "select project_nbr as name from project where proj_id=" + levelId;
		} else if (levelType.equals("Q")) {
			sql = "select sproj_nbr as name from sub_project where sproj_id=" + levelId;
		} else if (levelType.equals("A")) {
			sql = "select act_nbr as name from activity where act_id=" + levelId;
		} else{
			int sequence = new Wrapper().getNextId("IMAGE_ID");
			newFileName = levelType + "-" + levelId;
			logger.debug("newFileName :"+newFileName);
			newFileName = newFileName.trim().replace(' ', '_');
			newFileName = newFileName + "-" + sequence+ "-" + fileName.trim() ;
			newFileName = newFileName.replace("(", "_");
			newFileName = newFileName.replace(")", "_");
			newFileName = newFileName.replaceAll("%28", "_");
			newFileName = newFileName.replaceAll("%29", "_");
			
			newFileName = java.net.URLEncoder.encode(newFileName, "UTF-8");
			return newFileName;
		}

		
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				newFileName = levelType + "-" + rs.getString("name");
			}

			if (rs != null) {
				rs.close();
			}

			newFileName = newFileName + "-" + fileName.trim();
		} catch (Exception e) {
			logger.error("issue with renaming the file " + e.getMessage());
		}

		return newFileName;
	}

	/**
	 * Rename the filename to the smart-name of the file if it is not named smart already
	 * 
	 * @param levelId
	 * @param levelType
	 * @param fileName
	 * @return
	 */
	private boolean isFileSmartNamed(int levelId, String levelType, String fileName) {
		logger.debug("isFileSmartNamed(" + levelId + "," + levelType + "," + fileName + ")");

		try {
			// find the level to where the document belongs to.
			String firstTwoChars = fileName.substring(0, 2);

			StringTokenizer st = new StringTokenizer(fileName, "-");
			int numberOfTokens = st.countTokens();
			logger.debug("The number of tokens in this filename is " + numberOfTokens);

			// first check if there are expected number of tokens.
			if (numberOfTokens < 3) {
				return false;
			}

			// check if the level and level type are valid.
			String fileLevelType = st.nextToken();

			if (!(fileLevelType.equals(levelType))) {
				logger.debug("The file level type is not equal to the level type, returning false");

				return false;
			}

			char fileLevel = ' ';

			if (firstTwoChars.equalsIgnoreCase("L-")) {
				logger.debug("Document Level : Land");
				fileLevel = 'L';
			} else if (firstTwoChars.equalsIgnoreCase("S-")) {
				logger.debug("Document Level : Structure");
				fileLevel = 'S';
			} else if (firstTwoChars.equalsIgnoreCase("O-")) {
				logger.debug("Document Level : Occupancy");
				fileLevel = 'O';
			} else if (firstTwoChars.equalsIgnoreCase("P-")) {
				logger.debug("Document Level : Project");
				fileLevel = 'P';
			} else if (firstTwoChars.equalsIgnoreCase("SP")) {
				logger.debug("Document Level : Sub-Project");
				fileLevel = 'Q';
			} else if (firstTwoChars.equalsIgnoreCase("A-")) {
				logger.debug("Document Level : Activity");
				fileLevel = 'A';
			}

			logger.debug("The obtained file level is " + fileLevel);

			switch (fileLevel) {
			case 'L':
			case 'S':
			case 'O':
				logger.debug("Starting parsing for LSO document ");

				String address = st.nextToken();
				address = address.trim().replace('_', ' ');
				logger.debug("The lso address is " + address);

				int fileLevelId = getLsoId(fileLevelType, address);
				logger.debug("The file level obtained is " + fileLevelId);

				if (fileLevelId != levelId) {
					logger.debug("The lso id is not equal to the level id, returning false.");

					return false;
				}

				logger.debug("Ending parsing for LSO document");

				break;

			case 'P':
			case 'Q':
			case 'A':
				logger.debug("Starting parsing for PSA document ");

				String psaNumber = st.nextToken();
				logger.debug("The psa number obtained is " + psaNumber);

				int psaId = getPsaId(fileLevelType, psaNumber);
				logger.debug("The psa id obtained is " + psaId);

				if (psaId != levelId) {
					logger.debug("The pas id is not equal to the level id, returning false.");

					return false;
				}

				logger.debug("Ending parsing for PSA document");

				break;
			}

			return true;
		} catch (Exception e) {
			logger.error("Exception occured while parsing filename for smart-name-check " + e.getMessage());

			return false;
		}
	}

	/**
	 * gets the LSO id based on the data provided.
	 * 
	 * @return
	 * @throws Exception
	 */
	private int getLsoId(String level, String address) throws Exception {
		RowSet result = null;
		int lsoId = -1;
		String sql = "";

		try {
			sql = "select lso_id from v_address_list where lso_type=" + StringUtils.checkString(level) + " and dl_address =" + StringUtils.checkString(address);

			logger.debug(sql);
			result = new Wrapper().select(sql);

			if (result.next()) {
				lsoId = result.getInt(1);
			}

			
			
			if (result != null) {
				result.close();
			}

			if (lsoId == -1) {
				logger.debug("ERROR: while getting lso id from  database, it is -1 ");
				throw new Exception("Error: unable to get lso id for the address " + address);
			} else {
				return lsoId;
			}
		} catch (Exception e) {
			logger.error("ERROR: while getting lso id from  database " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * gets the PSA ID based on the data provided
	 * 
	 * @param psaType
	 * @param psaNumber
	 * @return
	 * @throws Exception
	 */
	public int getPsaId(String psaType, String psaNumber) throws Exception {
		RowSet result = null;
		int id = -1;
		String sql = "";

		try {

			if (psaType.equals("P")) {
				sql = "select proj_id from project where project_nbr=" + StringUtils.checkString(psaNumber);
			}

			if (psaType.equals("SP")) {
				sql = "select sproj_id from sub_project where sproj_nbr=" + StringUtils.checkString(psaNumber);
			}

			if (psaType.equals("A")) {
				sql = "select act_id from activity where act_nbr=" + StringUtils.checkString(psaNumber);
			}

			logger.debug(sql);
			result = new Wrapper().select(sql);

			if (result.next()) {
				id = result.getInt(1);
			}

			if (result != null) {
				result.close();
			}

			if (id == -1) {
				logger.debug("ERROR: while getting  PSA id from  database, it is -1 ");
				throw new Exception("Error: unable to get id for the psaNumber " + psaNumber + " and type " + psaType);
			} else {
				return id;
			}
		} catch (Exception e) {
			logger.error("ERROR: while getting psa id from  database");
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * deletes the file from server physically
	 * 
	 * @param sourceFileName
	 * @throws Exception
	 */
	public void deleteFtpFile(String sourceFileName) throws Exception {
		logger.info("deleteFtpFile(" + sourceFileName + ")");

		boolean result = false;

		try {

			java.util.ResourceBundle obcProperties = elms.util.db.Wrapper.getResourceBundle();
			String ATTACHMENT_FOLDER = obcProperties.getString("ATTACHMENT_FOLDER");
			result = (new File(ATTACHMENT_FOLDER + sourceFileName)).delete();
			logger.debug("File deletion result is" + result);
			logger.debug("File Deletion path is" + ATTACHMENT_FOLDER + sourceFileName);
			logger.info("physical file deleted successfully");
		} catch (Exception e) {
			logger.error("Error in deleting File" + e.getMessage());
		}

	}

	/**
	 * Checks a duplicate attachment
	 * 
	 * @param attachment
	 * @return
	 * @throws Exception
	 */
	public boolean checkDuplicateAttachment(Attachment attachment) throws Exception {
		logger.info("checkDuplicateAttachment(attachment)");
		boolean exists = false;
		String smartName = renameTheFile(attachment.getLevelId(), attachment.getAttachmentLevel(), attachment.getFile().getFileName());

		String sql = "select * from attachments where file_name=" + StringUtils.checkString(smartName);
		logger.info(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			exists = true;
		}

		if (rs != null) {
			rs.close();
		}
		return exists;
	}

	/**
	 * Checks the attachment size
	 * 
	 * @param attachment
	 * @return
	 * @throws Exception
	 */
	public boolean checkAttachmentSize(Attachment attachment) throws Exception {
		logger.info("checkAttachmentSize(attachment)");
		boolean ok = false;
		java.util.ResourceBundle elmsConfig = Wrapper.getResourceBundle();
		// defaulted to 5MB
		String allowedSize = elmsConfig.getString("ATTACHMENT_SIZE_MAX") != null ? elmsConfig.getString("ATTACHMENT_SIZE_MAX") : "5";
		logger.debug("Max allowed size is " + allowedSize);

		int allowedSizeMax = StringUtils.s2i(allowedSize) * 1024 * 1024;
		logger.debug("Max allowed size in bytes is " + allowedSizeMax);

		String attachmentFileSize = attachment.getFile().getFileSize();
		logger.debug("attachmentFileSize is " + attachmentFileSize);

		int attachmentSize = StringUtils.s2i(attachmentFileSize);
		logger.debug("attachment size is " + attachmentSize);

		if (attachmentSize <= allowedSizeMax) {
			ok = true;
		}
		return ok;
	}

	private Hold populateHold(RowSet rs) {
		Hold hold = new Hold();

		try {
			Date enterDate = rs.getDate("creation_dt");
			Date updateDate = rs.getDate("update_dt");
			Date statusDate = rs.getDate("hold_stat_dt");

			hold.setHoldId(rs.getInt("hold_id"));
			hold.setHoldLevel(rs.getString("hold_level"));
			hold.setLevelId(rs.getInt("level_id"));

			if (rs.getString("hold_type").equals("H")) {
				hold.setType("Hard");
			} else if (rs.getString("hold_type").equals("S")) {
				hold.setType("Soft");
			} else if (rs.getString("hold_type").equals("W")) {
				hold.setType("Warning");
			}

			hold.setStatus(rs.getString("stat"));
			hold.setTitle(rs.getString("title"));
			hold.setComment(rs.getString("description"));
			hold.setEnterDate(StringUtils.getCalendar(enterDate));
			hold.setStatusDate(StringUtils.getCalendar(statusDate));
			hold.setEnteredBy(rs.getInt("created_by"));
			hold.setUpdateDate(StringUtils.getCalendar(updateDate));
			hold.setUpdatedBy(rs.getInt("updated_by"));
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in HoldAgent populateHold() ");
			logger.error(hold);
			logger.error(e);
			e.printStackTrace();
		}
		
		return hold;
	}

	public Hold getHold(int holdId) {
		logger.info("Entering HoldAgent getHold() with id " + holdId);

		Hold hold = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from HOLDS where hold_id=" + holdId + " order by hold_type";
			RowSet rs = db.select(sql);
			logger.info("Created RowSet (" + sql + ") rs " + rs);

			if (rs.next()) {
				hold = populateHold(rs);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in HoldAgent getHold() ");
			logger.error(hold);
			logger.error(e);
			e.printStackTrace();
		}

		logger.info("Exiting HoldAgent getHold()");

		return hold;
	}

	/**
	 * Gets the holds list for type and id
	 * 
	 * @param type
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CachedRowSet getHoldList(String type, int id) throws Exception {
		logger.info("getHoldList(" + type + ", " + id + ")");
		String sql = "";
		CachedRowSet rs;

		try {
			rs = new CachedRowSet();

			Wrapper db = new Wrapper();

			sql = "select h.title,h.created_by,h.creation_dt,h.hold_type,c.first_name || ' ' || c.last_name as created_by,h.stat,h.description as comnt,h.hold_stat_dt,h.update_dt,h.hold_level,h.title,h.hold_id as id from holds h,users c,users u where h.created_by = c.userid and h.updated_by = u.userid and h.hold_level='" + type + "' and h.level_id=" + id + " and h.update_dt in (Select max(update_dt) from holds group by hold_id,level_id having level_id =" + id + ") order by h.hold_type,h.creation_dt desc";

			logger.info(sql);
			rs.populate(db.select(sql));

			return rs;
		} catch (Exception e) {
			logger.error(e);
			throw e;
		}
	}

	public CachedRowSet getHolds(int holdId) {
		String sql = "";
		CachedRowSet rs;

		try {
			rs = new CachedRowSet();

			Wrapper db = new Wrapper();
			sql = "select * from HOLDS where hold_id=" + holdId + " order by update_dt";

			logger.debug("the Sql in get Hold List is " + sql);
			rs.populate(db.select(sql));

			return rs;
		} catch (java.sql.SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
			e.printStackTrace();
		}

		return (null);
	}

	public List getHolds(int id, String level, int numberOfRows) {
		List holds = new ArrayList();
		Hold hold = null;
		String sql = "";

		try {
			Wrapper db = new Wrapper();
			sql = "select * from v_active_hold where level_id=" + id + "and rownum <=" + numberOfRows + " and hold_level='" + level + "' order by hold_type ";

			logger.debug("3 holds row #### :" + sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				hold = populateHold(rs);
				holds.add(hold);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (java.sql.SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
		}

		return holds;
	}

	// Get count by Hold type
	public List getCountByHoldTypes(int levelId, String holdLevel) {
		String sql = "";
		List countByHoldTypes = new ArrayList();
		int softHoldCount = 0;
		int warnHoldCount = 0;
		int hardHoldCount = 0;

		try {
			Wrapper db = new Wrapper();

			if (holdLevel.equals("Q")) {
				/*
				 * sql = "select hold_type,count(*) as count from holds where level_Id=" + levelId + " and hold_level='Q'" + " and stat = 'A'" + " group by hold_type";
				 */
				sql = "select hold_type,count(*) as count from holds a where a.level_Id=" + levelId + " and hold_level='Q'" + " and stat = 'A'" + " and a.update_dt = (select max(b.update_dt) from holds b  where  b.level_id =" + levelId + " and b.hold_level='Q' and b.hold_id=a.hold_id )" + " group by hold_type";

				logger.debug("The sub project Hold  sql" + sql);

				RowSet rs = db.select(sql);

				while (rs.next()) {
					if (rs.getString("hold_type").equals("H")) {
						hardHoldCount = rs.getInt("count");
					} else if (rs.getString("hold_type").equals("S")) {
						softHoldCount = rs.getInt("count");
					} else if (rs.getString("hold_type").equals("W")) {
						warnHoldCount = rs.getInt("count");
					}
				}

				rs.close();

				// getting holdds for all activities in the sub project

				/*
				 * sql = "select hold_type,count(*) as count from holds where  level_id in (select act_id from activity where sproj_id=" + levelId + ")" + " and hold_level='A'" + " and stat = 'A'" + " group by hold_type";
				 */
				// sql = "select hold_type,count(*) as count from holds where  level_id in (select act_id from activity where sproj_id=" + levelId + ")" + " and hold_level='A'" + " and stat = 'A'" + " group by hold_type";
				sql = "select stat,hold_type,count(*) as count from holds a where a.level_Id in (select act_id from activity where sproj_id=" + levelId + ")" + "  and hold_level='A' and a.update_dt = (select max(b.update_dt) from holds b  where  b.level_id in (select act_id from activity where sproj_id=" + levelId + ")" + "  and b.hold_level='A' and  b.hold_id=a.hold_id ) group by stat,hold_type";

				logger.debug("The Sub projects activity hold sql" + sql);
				rs = db.select(sql);

				while (rs.next()) {
					if (rs.getString("hold_type").equals("H") && rs.getString("stat").equals("A")) {
						hardHoldCount = hardHoldCount + rs.getInt("count");
					} else if (rs.getString("hold_type").equals("S") && rs.getString("stat").equals("A")) {
						softHoldCount = softHoldCount + rs.getInt("count");
					} else if (rs.getString("hold_type").equals("W") && rs.getString("stat").equals("A")) {
						warnHoldCount = warnHoldCount + rs.getInt("count");
					}

					logger.debug("Hard Hold Count is" + hardHoldCount);
					logger.debug("soft Hold Count is" + softHoldCount);
					logger.debug("warn Hold Count is" + warnHoldCount);
				}

				if (rs != null) {
					rs.close();
				}
				countByHoldTypes.add(new CountByHoldType(hardHoldCount, "H"));
				countByHoldTypes.add(new CountByHoldType(softHoldCount, "S"));
				countByHoldTypes.add(new CountByHoldType(warnHoldCount, "W"));
			}
		} catch (java.sql.SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
			e.printStackTrace();
		}

		return countByHoldTypes;
	}

	/*
	 * saveHold decides whether the record needs to be updated or added and branches to the appropriate procedure
	 */
	public int saveHold(Hold aHold) throws Exception {
		int holdId = 0;

		if (aHold.getHoldId() == 0) {
			holdId = addHold(aHold);
		} else {
			holdId = updateHold(aHold);
		}

		return holdId;
	}

	private int updateHold(Hold hold) throws Exception {
		logger.info("updateHold(" + hold + ")");

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = new Wrapper().getConnectionForPreparedStatementOnly();
			statement = connection.prepareStatement(ADD_UPDATE_HOLD);
			logger.debug(ADD_UPDATE_HOLD);
			statement.setInt(1, hold.getHoldId());
			statement.setInt(2, hold.getLevelId());
			statement.setString(3, hold.getHoldLevel());
			statement.setString(4, hold.getTitle());
			statement.setString(5, hold.getComment());
			statement.setString(6, hold.getType());
			statement.setDate(7, StringUtils.cal2SQLDate(hold.getStatusDate()));
			statement.setString(8, hold.getStatus());
			statement.setInt(9, hold.getEnteredBy());
			statement.setInt(10, hold.getUpdatedBy());
			statement.execute();

			return hold.getHoldId();
		} catch (Exception e) {
			logger.error(e);
			throw new Exception("Problem while updating hold " + e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception ignored) {
				logger.error("Exception while closing conneciton " + ignored.getMessage());
			}
		}
	}

	private int addHold(Hold hold) throws Exception {
		logger.info("updateHold(" + hold + ")");

		int holdId = 0;

		Connection connection = null;
		PreparedStatement statement = null;
		Wrapper db = new Wrapper();
		holdId = db.getNextId("HOLD_ID");

		try {
			connection = new Wrapper().getConnectionForPreparedStatementOnly();
			statement = connection.prepareStatement(ADD_UPDATE_HOLD);
			logger.debug(ADD_UPDATE_HOLD);
			statement.setInt(1, holdId);
			statement.setInt(2, hold.getLevelId());
			statement.setString(3, hold.getHoldLevel());
			statement.setString(4, hold.getTitle());
			statement.setString(5, hold.getComment());
			statement.setString(6, hold.getType());
			statement.setDate(7, StringUtils.cal2SQLDate(hold.getStatusDate()));
			statement.setString(8, hold.getStatus());
			statement.setInt(9, hold.getEnteredBy());
			statement.setInt(10, hold.getUpdatedBy());
			statement.execute();

			return holdId;
		} catch (Exception e) {
			logger.error(e);
			throw new Exception("Problem while adding hold " + e.getMessage());
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception ignored) {
				logger.error("Exception while closing conneciton " + ignored.getMessage());
			}
		}
	}

	/**
	 * checks the editable for the given level id and level
	 * 
	 * @param levelId
	 * @param level
	 * @return
	 * @throws Exception
	 */
	public boolean editable(int levelId, String level) throws Exception {
		logger.info("editable(" + levelId + ", " + level + ")");

		Wrapper db = new Wrapper();
		boolean editable = true;
		String sql = "";
		String levelIds = "";
		int softHoldCount = 0;
		int hardHoldCount = 0;

		if (level == null) {
			level = "";
		}

		try {
			sql = "select hold_type,count(*) as count from holds a where ";

			if (level.equals("Z")) {
				return true;
			}

			if (level.equals("L")) {
				levelIds = "level_id = " + levelId + " and hold_level='L'";
			} else if (level.equals("S")) {
				levelIds = "level_id in (select land_id  from land_structure where structure_id=" + levelId + " union select structure_id from structure where structure_id=" + levelId + ")" + " and hold_level in('L','S')";
			} else if (level.equals("O")) {
				levelIds = "level_id in (select land_id from land_structure where structure_id =" + "(select structure_id from structure_occupant where occupancy_id= " + levelId + ")" + " union select structure_id from structure_occupant where occupancy_id= " + levelId + " union select occupancy_id from occupancy where occupancy_id=" + levelId + ")" + " and hold_level in('L','S','O')";
			} else {
				String sql1 = "";

				if (level.equals("P")) {
					sql1 = "select distinct vll.*,vpl.proj_id from v_lso_land vll,v_psa_list vpl where vll.lso_id=vpl.lso_id and vpl.proj_id=" + levelId;
				} else if (level.equals("Q")) {
					sql1 = "select distinct vll.*,vpl.proj_id,vpl.sproj_id from v_lso_land vll,v_psa_list vpl where vll.lso_id=vpl.lso_id and vpl.sproj_id=" + levelId;
				} else if (level.equals("A")) {
					sql1 = "select distinct vll.*,vpl.proj_id,vpl.sproj_id from v_lso_land vll,v_psa_list vpl where vll.lso_id=vpl.lso_id and vpl.act_id=" + levelId;
				}

				logger.debug(sql1);

				RowSet rs1 = db.select(sql1);

				if (rs1.next()) {
					char lsoType = 'X';
					char psaLevel = 'X';

					try {
						lsoType = rs1.getString("LSO_TYPE").charAt(0);
						psaLevel = level.charAt(0);
					} catch (Exception e) {
						logger.error("Error in converting level,type to char " + e.getMessage());
						throw new Exception("Error while converting level type to char " + e.getMessage());
					}

					StringBuffer selectFilter = new StringBuffer(" (");

					switch (lsoType) {
					case 'O':
						selectFilter.append("(level_id=" + rs1.getString("LSO_ID") + " and hold_level='O') or ");

					case 'S':
						selectFilter.append("(level_id=" + rs1.getString("STRUCTURE_ID") + " and hold_level='S') or ");

					case 'L':
						selectFilter.append("(level_id=" + rs1.getString("LAND_ID") + " and hold_level='L')");
					}

					switch (psaLevel) {
					case 'A':
						selectFilter.append(" or (level_id=" + levelId + " and hold_level='A')");

					case 'Q':
						selectFilter.append(" or (level_id=" + rs1.getString("SPROJ_ID") + " and hold_level='Q')");

					case 'P':
						selectFilter.append(" or (level_id=" + rs1.getString("PROJ_ID") + " and hold_level='P'))");
					}

					levelIds = selectFilter.toString();
					if (rs1 != null) {
						rs1.close();
					}
				}
			}

			sql = sql + levelIds;
			if (sql.trim().endsWith("where")) {
				sql = sql + " stat = 'A'" + " and a.update_dt = (select max(b.update_dt) from holds b where b.hold_id=a.hold_id )" + " group by hold_type";
			} else {
				sql = sql + " and stat = 'A'" + " and a.update_dt = (select max(b.update_dt) from holds b where b.hold_id=a.hold_id )" + " group by hold_type";
			}

			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				if (rs.getString("hold_type").equals("H")) {
					hardHoldCount = rs.getInt("count");
				} else if (rs.getString("hold_type").equals("S")) {
					softHoldCount = rs.getInt("count");
				}
			}

			if (rs != null) {
				rs.close();
			}

			if ((hardHoldCount > 0) || (softHoldCount > 0)) {
				editable = false;
			}

			return editable;
		} catch (Exception e) {
			logger.error("General Error in Hold Agent " + e.getMessage());
			throw new Exception("Exception occured in hold while checking editable " + e.getMessage());
		}
	}

	public String checkwithHolds(String levelId, String level) {
		String hasHold = "";
		Wrapper db = new Wrapper();
		String sql = "";

		// getting start dates and expiration dates
		try {
			// getting primary resident name
			// sql = "select count(HOLD_ID) as cnt"
			// + " from HOLDS where LEVEL_ID=" + levelId + " and HOLD_LEVEL='" + level + "'";
			sql = "select description from HOLDS where LEVEL_ID=" + levelId + " and HOLD_LEVEL='" + level + "'";

			logger.debug(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				hasHold = rs.getString("description");

				if (hasHold == null) {
					hasHold = "";
				}

				// hasHold = true;
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("General error in this mothod " + e.getMessage());
		}

		return hasHold;
	}

	// checks whether there is hold at levels for the given activity id
	public boolean hasHolds(int actid) throws Exception {
		boolean res = false;

		try {
			String sql =

			"select    *    from  v_Active_hold h  join   v_all_ids q  on  ( h.level_id =q.land_id and h.hold_level='L'  or h.level_id =q.structure_id and h.hold_level='S' " +

			" or h.level_id =q.occupancy_id and h.hold_level='O'  or h.level_id =q.proj_id and h.hold_level='P'  or " +

			" h.level_id = q.sproj_id  and h.hold_level='Q' or h.level_id =q.act_id   )" +

			" and HOLD_TYPE not in('W')  and q.act_id =" + actid;

			logger.debug("The hold check sql is " + sql);
            ResultSet rs=new Wrapper().select(sql);
			if (rs.next()) {
				res = true;

			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception occured " + e.getMessage());
		}

		return res;
	}

	private Comment populateComment(RowSet rs) {
		Comment comments = new Comment();

		try {
			logger.debug("in the populate");

			java.util.Date enterDate = rs.getDate("creation_dt");
			java.util.Date updateDate = rs.getDate("update_dt");
			
			
			comments.setCommentId(rs.getInt("comnt_id"));
			comments.setCommentLevel(StringUtils.nullReplaceWithEmpty(rs.getString("comnt_level")));
			comments.setLevelId(rs.getInt("level_id"));
			comments.setComment(StringUtils.nullReplaceWithEmpty(rs.getString("comnt")));
			comments.setInternal(StringUtils.nullReplaceWithEmpty(rs.getString("internal")));
			if(enterDate!=null)
			{
			  comments.setEnterDate(StringUtils.getCalendar(enterDate));
			}
			comments.setEnteredBy(rs.getInt("created_by"));
			if(updateDate!=null )
			{
			  comments.setUpdateDate(StringUtils.getCalendar(updateDate));
			}
			comments.setUpdatedBy(rs.getInt("updated_by"));
			logger.debug("after populaging comments obj");
			logger.debug(comments);
		} catch (Exception e) {
			logger.error("Error in CommentAgent populateComment() ");
			logger.error(comments);
			logger.error(e);
			e.printStackTrace();
		}
        
		return comments;
	}

	public Comment getComment(int commentId) {
		logger.debug("Entering CommentAgent getComment() with id " + commentId);

		Comment comments = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from COMMENTS where comnt_id=" + commentId;
			RowSet rs = db.select(sql);
			logger.debug("Created RowSet (" + sql + ") rs " + rs);

			if (rs.next()) {
				comments = populateComment(rs);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in CommentAgent getComment() ");
			logger.error(comments);
			logger.error(e);
			e.printStackTrace();
		}

		logger.info("Exiting CommentAgent getComment()");

		return comments;
	}

	public List getActivityTeamList(int id) {
		logger.info("getActivityTeamList(" + id);

		List activityTeamList = new ArrayList();
		RowSet rs = null;
		RowSet rsBL = null;
		RowSet rsBT = null;
		String firstName = null;
		String lastName = null;
		String fullName = null;

		try {
			// String sql = "select pt.lead,u.title,u.first_name,u.last_name from process_team pt join users u on u.userid=pt.userid where psa_id in (select act_id from activity where sproj_id= " + id + ")";
			String sql = "select r.name as title, pt.lead,u.first_name,u.last_name from process_team pt,users u,groups r where u.userid=pt.userid and pt.group_id = r.group_id and  psa_id in (select act_id from activity where sproj_id= " + id + ")";
			logger.debug(sql);

			ActivityTeamObject activityTeamObject = null;

			for (rs = (new Wrapper()).select(sql.toString()); rs.next(); activityTeamList.add(activityTeamObject)) {
				if (rs != null) {
					firstName = rs.getString("FIRST_NAME");
					lastName = rs.getString("LAST_NAME");
					fullName = firstName + " " + lastName;
					activityTeamObject = new ActivityTeamObject("false", fullName, rs.getString("TITLE"), rs.getString("LEAD"));
				}
			}
			/********* Teams added for BL activities can be viewed under BT sub project **********************/
			String sqlBL = "select * from v_bl_approval  vbla left outer join activity a on vbla.act_id = a.act_id where a.sproj_id= " + id;
			logger.debug(sqlBL);

			for (rsBL = (new Wrapper()).select(sqlBL.toString()); rsBL.next(); activityTeamList.add(activityTeamObject)) {
				if (rsBL != null) {

					activityTeamObject = new ActivityTeamObject("false", rsBL.getString("APPROVED_BY_NAME"), rsBL.getString("DEPARTMENT_NAME"), "off");
				}
			}

			/********* Teams added for BT activities can be viewed under BT sub project **********************/
			String sqlBT = "select * from v_bt_approval  vbta left outer join activity a on vbta.act_id = a.act_id where a.sproj_id= " + id;
			logger.debug(sqlBT);

			for (rsBT = (new Wrapper()).select(sqlBT.toString()); rsBT.next(); activityTeamList.add(activityTeamObject)) {
				if (rsBT != null) {

					activityTeamObject = new ActivityTeamObject("false", rsBT.getString("APPROVED_BY_NAME"), rsBT.getString("DEPARTMENT_NAME"), "off");

				}
			}
			
			if (rsBL != null) {
				rsBL.close();
			}
			
			if (rsBT != null) {
				rsBT.close();
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getActivityTeamList: " + e.getMessage());
		}

		return activityTeamList;
	}

	public List getCommentList(int id, int count, String level) throws Exception {
		logger.debug("Entered getCommentList with id :: "+id);
		List commentList = null;
		RowSet rs = null;

		try {
			commentList = new ArrayList();

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM (SELECT C.COMNT_ID,C.COMNT,C.INTERNAL,to_date(C.CREATION_DT) CREATEDDATE,");
			sql.append("U.FIRST_NAME || COALESCE(' ' || U.MI,'') || COALESCE(' ' || U.LAST_NAME,'') ENTEREDBY");
			sql.append(" FROM COMMENTS C LEFT OUTER JOIN USERS U ON C.CREATED_BY = U.USERID");
			sql.append(" WHERE C.LEVEL_ID=" + id + " AND COMNT_LEVEL = '" + level + "' ORDER BY CREATION_DT desc)");

			if (count > 0) {
				sql.append(" Where ROWNUM <= " + count);
			}

			logger.debug("The comments sql : " + sql.toString());
			logger.info(sql);

			Comment comments;

			for (rs = (new Wrapper()).select(sql.toString()); rs.next(); commentList.add(comments)) {
				comments = new Comment();
				comments.setCommentId(rs.getInt("COMNT_ID"));
				comments.setComment(rs.getString("COMNT"));
				comments.setInternal(rs.getString("INTERNAL"));
				comments.setEnterDate(StringUtils.dbDate2cal(rs.getString("CREATEDDATE")));
				comments.setDisplayEnterDate(StringUtils.date2str(rs.getDate("CREATEDDATE")));
				comments.setEnteredUserName(rs.getString("ENTEREDBY"));
			}

			if (rs != null) {
				rs.close();
			}

			return commentList;
		} catch (SQLException se) {
			logger.error("SQL exception thrown while getting comments list " + se.getMessage());
			throw se;
		} catch (Exception e) {
			logger.error("Exception thrown while getting comments list" + e.getMessage());
			throw e;
		}
	}

	public CachedRowSet getCommentList(int levelId, String commentLevel) {
		String sql = "";

		try {
			CachedRowSet rs = new CachedRowSet();
			logger.debug("rs value is : " + rs);

			Wrapper db = new Wrapper();
			sql = "select *  from comments c,users u where c.created_by = u.userid and c.level_id=" + levelId + " and c.comnt_level = '" + commentLevel + "'";
			logger.debug("rowset sql is " + sql);
			rs.populate(db.select(sql));
			logger.debug("rs value is : " + rs);

			return rs;
		} catch (SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
			e.printStackTrace();
		}

		return null;
	}

	public void saveComment(Comment aComment) {
		if (aComment.getCommentId() == 0) {
			tmpInsertComment(aComment);
		} else {
			updateComment(aComment);
		}
	}

	private void updateComment(Comment aComment) {
		try {
			String internal = (aComment.getInternal() != null) ? aComment.getInternal() : "N";
			Wrapper db = new Wrapper();
			StringBuffer sql = new StringBuffer();
			sql.append("UPDATE COMMENTS SET COMNT=" + StringUtils.checkString(aComment.getComment()) + ",");
			sql.append("INTERNAL=" + StringUtils.checkString(internal) + ",");
			sql.append("UPDATED_BY=" + aComment.getUpdatedBy() + ",");
			sql.append("UPDATE_DT=CURRENT_TIMESTAMP WHERE COMNT_ID = " + aComment.getCommentId());
			logger.info("Update comments SQL : " + sql);
			db.update(sql.toString());
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}

	public void tmpInsertComment(Comment cmt) {
		try {
			String tinternal = (cmt.getInternal() != null) ? cmt.getInternal() : "N";
			Wrapper db = new Wrapper();
			int commentId = db.getNextId("COMMENT_ID");

			tmpAddComment(cmt.getLevelId(), commentId, tinternal, cmt, cmt.getCommentLevel());

			if ("on".equals(cmt.getUpdateSubProject())) {
				String sql = "select sproj_id from activity where act_id=" + cmt.getLevelId();
				RowSet rs = db.select(sql);
				logger.debug("Sql: " + sql);

				if (rs.next()) {
					int id = rs.getInt("sproj_id");
					int nextId = db.getNextId("COMMENT_ID");
					tmpAddComment(id, nextId, tinternal, cmt, "Q");
				}
			}

			if ("on".equals(cmt.getUpdateActivities())) {
				String sql = "";

				if ("Q".equals(cmt.getCommentLevel())) {
					sql = "select act_id from activity where sproj_id=" + cmt.getLevelId();
				} else {
					sql = "select act_id from activity where sproj_id in (select sproj_id from activity where act_id=" + cmt.getLevelId() + ") and " + " act_id <> " + cmt.getLevelId();
				}

				logger.debug(sql);

				RowSet rs = db.select(sql);

				while (rs.next()) {
					int id = rs.getInt("act_id");
					int nextId = db.getNextId("COMMENT_ID");
					tmpAddComment(id, nextId, tinternal, cmt, "A");
				}
				if (rs != null) {
					rs.close();
				}
			}
						
		} catch (Exception e) {
			logger.error("Exception occured : " + e.getMessage());
		}
	}

	private void tmpAddComment(int levelId, int commentId, String internal, Comment aComment, String levelType) {
		try {
			Wrapper db = new Wrapper();
			String sql = "INSERT INTO COMMENTS (COMNT_ID,LEVEL_ID,COMNT_LEVEL,INTERNAL,CREATED_BY,CREATION_DT,UPDATED_BY,UPDATE_DT, COMNT) VALUES (" + commentId + "," + levelId + "," + StringUtils.checkString(levelType) + "," + StringUtils.checkString(internal) + "," + aComment.getEnteredBy() + "," + "sysdate" + "," + aComment.getUpdatedBy() + "," + "sysdate" + "," + StringUtils.checkString(aComment.getComment()) + ")";
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}

	private void addComment(Comment aComment) {
		try {
			String internal = (aComment.getInternal() != null) ? aComment.getInternal() : "N";
			Wrapper db = new Wrapper();
			int commentId = db.getNextId("COMMENT_ID");
			String sql = "INSERT INTO COMMENTS (COMNT_ID,LEVEL_ID,COMNT_LEVEL,INTERNAL,CREATED_BY,CREATION_DT,UPDATED_BY,UPDATE_DT, COMNT) VALUES (" + commentId + "," + aComment.getLevelId() + "," + StringUtils.checkString(aComment.getCommentLevel()) + "," + StringUtils.checkString(internal) + "," + aComment.getEnteredBy() + "," + "sysdate" + "," + aComment.getUpdatedBy() + "," + "sysdate" + "," + StringUtils.checkString(aComment.getComment()) + ")";
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}

	public void deleteComment(int commentId) {
		if (commentId > 0) {
			try {
				(new Wrapper()).update("DELETE FROM COMMENTS WHERE COMNT_ID=" + commentId);
			} catch (Exception e) {
				logger.error("Error in deleteCommnet(" + commentId + ") : " + e.getMessage());
			}
		}
	}

	private Condition populateCondition(RowSet rs) {
		Condition condition = new Condition();

		try {
			logger.debug("inside try populate");

			Date creationDate = rs.getDate("creation_dt");
			Date updationDate = rs.getDate("update_dt");

			logger.debug("creationDate" + creationDate);
			logger.debug("updationDate" + updationDate);

			condition.setConditionId(rs.getInt("cond_id"));
			logger.debug("conditionId" + condition.getConditionId());
			condition.setConditionLevel(rs.getString("cond_level"));
			logger.debug("conditionLevel" + condition.getConditionLevel());
			condition.setLevelId(rs.getInt("level_id"));
			logger.debug("LevelId" + condition.getLevelId());
			condition.setShortText(rs.getString("short_text"));
			logger.debug("Short Text" + condition.getShortText());

			String lib_id = "0";
			String cond_code = "";

			lib_id = rs.getString("library_id");
			condition.setLibraryId(StringUtils.s2i(lib_id));

			if (lib_id.equals("0")) {
				cond_code = rs.getString("cond_id");
			} else {
				cond_code = rs.getString("condition_code");
			}

			condition.setConditionCode(cond_code);
			// Added by Gayathri
			condition.setText(new String(rs.getString("text")).replaceAll("\\<.*?\\>", ""));

			logger.debug("condition text" + condition.getText());

			condition.setInspectable(rs.getString("inspectable"));
			logger.debug("inspectable" + condition.getInspectable());

			condition.setWarning(rs.getString("warning"));
			logger.debug("warning" + condition.getWarning());

			condition.setComplete(rs.getString("complete"));
			logger.debug("setcomplete" + condition.getComplete());

			condition.setUpdatedBy(rs.getInt("updated_by"));
			logger.debug("updatedBy " + condition.getUpdatedBy());

			condition.setCreatedBy(rs.getInt("created_by"));
			logger.debug("created By" + condition.getCreatedBy());

			condition.setCreatedDate(rs.getDate("creation_dt"));
			condition.setUpdatedDate(rs.getDate("update_dt"));

			logger.debug("finished try populate");
		} catch (Exception e) {
			logger.error("Error in ConditionAgent populateCondition() " + e.getMessage());
		}

		return condition;
	}

	/**
	 * Function to populate the form with initial records pertaining to this psaId
	 */
	public ConditionForm populateConditionForm(ConditionForm form) {
		int levelId = form.getLevelId();
		String condLevel = form.getConditionLevel();

		if (levelId == 0) {
			return new ConditionForm();
		}

		try {
			Wrapper db = new Wrapper();
			List list = new ArrayList();

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM CONDITIONS C LEFT OUTER JOIN LKUP_CONDITIONS LC ON C.LIBRARY_ID = LC.CONDITION_ID");
			sql.append(" WHERE C.LEVEL_ID= " + levelId + " AND C.COND_LEVEL ='" + condLevel + "' AND C.UPDATED_BY <> 0");
			sql.append(" ORDER BY C.UPDATE_DT desc,C.LIBRARY_ID desc");
			logger.debug("Condition SQL : " + sql.toString());

			RowSet rs = db.select(sql.toString());

			if (rs == null) {
				Condition rec = new Condition();
				rec.setConditionId(0);
				rec.setLevelId(0);
				rec.setConditionLevel("");
				rec.setShortText("none");
				list.add(rec);

				Condition[] tmp = new Condition[list.size()];
				list.toArray(tmp);

				form.setCondition(tmp);
			} else {
				while (rs.next()) {
					String lib_id = "0";
					String cond_code = "";
					Condition rec = new Condition();
					lib_id = rs.getString("library_id");
					rec.setLibraryId(StringUtils.s2i(lib_id));

					if (lib_id.equals("0")) {
						cond_code = rs.getString("cond_id");
					} else {
						cond_code = rs.getString("condition_code");
					}

					rec.setConditionCode(cond_code);
					rec.setConditionId(rs.getInt("COND_ID"));
					rec.setLevelId(rs.getInt("LEVEL_ID"));
					rec.setConditionLevel(rs.getString("COND_LEVEL"));
					rec.setShortText(rs.getString("SHORT_TEXT"));
					list.add(rec);
				}

				Condition[] tmp = new Condition[list.size()];
				list.toArray(tmp);

				form.setCondition(tmp);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception in method populateConditionForm. message - " + e.getMessage());
		}

		return form;
	}

	// ****** setting condition object with condition ID
	public Condition getCondition(int conditionId) {
		logger.info("Entering ConditionAgent getCondition() with id " + conditionId);

		Condition condition = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from conditions c left outer join lkup_conditions lc on c.library_id = lc.condition_id where c.cond_id=" + conditionId;
			logger.debug(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				condition = populateCondition(rs);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in ConditionAgent getCondition() " + e.getMessage());
		}

		logger.info("Exiting ConditionAgent getCondition()");

		
		return condition;
	}

	// ********** setting condition object with condition ID and levelId
	public Condition getCondition(int conditionId, int levelId) {
		logger.info("Entering ConditionAgent getCondition() with id " + conditionId + " and " + levelId);

		Condition condition = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from conditions where cond_id=" + conditionId + " and level_id=" + levelId;
			logger.debug(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				condition = populateCondition(rs);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in ConditionAgent getCondition() " + e.getMessage());
		}

		logger.info("Exiting ConditionAgent getCondition()");

		return condition;
	}

	public List getConditionList(String type, int id) {
		logger.debug("getConditionsList(" + type + "," + id + ")");

		List conditions = new ArrayList();
		StringBuffer sql = new StringBuffer();

		try {
			sql.append("SELECT * FROM CONDITIONS C LEFT OUTER JOIN LKUP_CONDITIONS LC ON C.LIBRARY_ID = LC.CONDITION_ID");
			sql.append(" WHERE C.COND_LEVEL = '" + type + "' AND C.LEVEL_ID = " + id);
			sql.append(" ORDER BY C.ORDER_ID,C.UPDATE_DT desc,C.LIBRARY_ID desc ");
			logger.debug(sql.toString());

			RowSet rs = new Wrapper().select(sql.toString());

			while (rs.next()) {
				Condition condition = new Condition();
				condition.setConditionId(rs.getInt("COND_ID"));
				condition.setLevelId(rs.getInt("LEVEL_ID"));
				condition.setConditionLevel(rs.getString("COND_LEVEL"));

				if (rs.getInt("LIBRARY_ID") == 0) {
					condition.setConditionCode(rs.getString("COND_ID"));
				} else {
					condition.setConditionCode(rs.getString("CONDITION_CODE"));
				}

				condition.setShortText(rs.getString("SHORT_TEXT"));
				conditions.add(condition);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getConditionList " + e.getMessage());
		}

		return conditions;
	}

	/*
	 * saveCondition decides whether the record needs to be updated or added and branches to the appropriate procedure
	 */
	public void saveCondition(Condition aCondition) {
		logger.debug("inside saveCondition");

		if (aCondition.getConditionId() == 0) {
			addCondition(aCondition);
			logger.debug("inside if of add");
		} else {
			updateCondition(aCondition);
		}
	}

	private void updateCondition(Condition aCondition) {
		logger.debug("update method");

		try {
			String shortText = "";

			if ((aCondition.getShortText() != null) ^ (aCondition.getShortText() != "")) {
				shortText = StringUtils.checkString(aCondition.getShortText());
				logger.debug("shorttext 1 : " + shortText);
			} else {
				shortText = "SUBSTR(" + StringUtils.checkString(aCondition.getText()) + ",1,15)";
				logger.debug("shorttext 3 : " + shortText);
			}

			logger.debug("shortText #### : " + shortText);

			Wrapper db = new Wrapper();
			String sql = "UPDATE CONDITIONS SET COND_LEVEL = " + StringUtils.checkString(aCondition.getConditionLevel()) + "," + "TEXT = " + StringUtils.checkString(aCondition.getText()) + "," + "INSPECTABLE =" + StringUtils.checkString(aCondition.getInspectable()) + "," + "WARNING = " + StringUtils.checkString(aCondition.getWarning()) + "," + "COMPLETE = " + StringUtils.checkString(aCondition.getComplete()) + "," + "UPDATE_DT =" + "CURRENT_TIMESTAMP " + "," + "UPDATED_BY = " + aCondition.getUpdatedBy() + " WHERE CONDITIONS.COND_ID = " + aCondition.getConditionId() + "  AND  CONDITIONS.LEVEL_ID = " + aCondition.getLevelId();
			logger.debug(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Exception occured while updateCondition " + e.getMessage());
		}

		return;
	}

	private void addCondition(Condition aCondition) {
		logger.debug("add method");

		try {
			logger.debug("add try");

			Wrapper db = new Wrapper();
			int conditionId = db.getNextId("CONDITION_ID");

			logger.debug("after getnextid : " + conditionId);

			String sql = "INSERT INTO CONDITIONS(COND_ID,LEVEL_ID,LIBRARY_ID,COND_LEVEL,TEXT,SHORT_TEXT,INSPECTABLE,WARNING,COMPLETE,CREATED_BY,CREATION_DT,UPDATED_BY,UPDATE_DT) VALUES ( " + conditionId + "," + aCondition.getLevelId() + "," + aCondition.getLibraryId() + "," + StringUtils.checkString(aCondition.getConditionLevel()) + "," + StringUtils.checkString(aCondition.getText()) + "," + StringUtils.checkString(aCondition.getShortText()) + "," + StringUtils.checkString(aCondition.getInspectable()) + "," + StringUtils.checkString(aCondition.getWarning()) + "," + StringUtils.checkString(aCondition.getComplete()) + "," + aCondition.getCreatedBy() + "," + "sysdate" + "," + aCondition.getUpdatedBy() + "," + "sysdate" + ")";
			logger.debug(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return;
	}

	/**
	 * Function to get items from library for a specific sub category
	 */
	public ConditionLibraryForm getLibraryItems(ConditionLibraryForm frm) {
		logger.debug("Inside getLibraryItems");

		String type = frm.getType();
		String level = frm.getConditionLevel(); // L,S,O,P,Q,A
		String sql = "";
		String levelType = "";
		String inspectability = "";
		String warning = "";
		String required = "";

		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();

			rs = new CachedRowSet();

			if (level.equals("P")) {
				sql = "SELECT * FROM LKUP_CONDITIONS WHERE LEVEL_TYPE = '" + level + "'";
			} else {
				sql = "SELECT * FROM LKUP_CONDITIONS WHERE CONDITION_TYPE = " + type + " AND LEVEL_TYPE = '" + level + "'";
			}
			logger.debug("frm.getCheckDate()" + frm.getCheckDate());
			if ((frm.getCheckDate() != null) && !(frm.getCheckDate().equals(""))) {
				sql = sql + " AND (EXPIRATION_DT >= to_date(" + StringUtils.checkString(frm.getCheckDate()) + " , 'MM/DD/YYYY'))";
			} else {
				sql = sql + " AND EXPIRATION_DT is null";
			}

			logger.debug("sql for conditionItem " + sql);

			rs = (CachedRowSet) db.select(sql);

			ArrayList list = new ArrayList();

			if (rs != null) {
				rs.beforeFirst();

				while (rs.next()) {
					String condTxt = new String(rs.getString("CONDITION_TEXT"));

					logger.debug("Condition Lib text #### : " + condTxt);
					logger.debug("condition code and library " + rs.getString("condition_code") + " : " + rs.getInt("condition_id"));

					levelType = rs.getString("LEVEL_TYPE");

					if (levelType == null) {
						levelType = "A";
					}

					if (levelType.equalsIgnoreCase("A")) {
						levelType = "Activity";
					}

					if (levelType.equalsIgnoreCase("Q")) {
						levelType = "Sub-Project";
					}

					if (levelType.equalsIgnoreCase("P")) {
						levelType = "Project";
					}

					if (levelType.equalsIgnoreCase("L")) {
						levelType = "Land";
					}

					if (levelType.equalsIgnoreCase("S")) {
						levelType = "Structure";
					}

					if (levelType.equalsIgnoreCase("O")) {
						levelType = "Occupancy";
					}

					inspectability = rs.getString("INSPECTABILITY");

					if (inspectability == null) {
						inspectability = "N";
					}

					if (inspectability.equalsIgnoreCase("N")) {
						inspectability = "No";
					}

					if (inspectability.equalsIgnoreCase("Y")) {
						inspectability = "Yes";
					}

					warning = rs.getString("WARNING");

					if (warning == null) {
						warning = "N";
					}

					if (warning.equalsIgnoreCase("N")) {
						warning = "No";
					}

					if (warning.equalsIgnoreCase("Y")) {
						warning = "Yes";
					}

					required = rs.getString("REQUIRED");

					if (required == null) {
						required = "N";
					}

					if (required.equalsIgnoreCase("N")) {
						required = "No";
					}

					if (required.equalsIgnoreCase("Y")) {
						required = "Yes";
					}

					int libId = rs.getInt("CONDITION_ID"); // This is the Id from lkup_condition

					ConditionLibraryRecord rec = new ConditionLibraryRecord("off", levelType, "", inspectability, warning, "", required, rs.getString("SHORT_TEXT"), condTxt, libId, rs.getString("CONDITION_CODE"));

					int conditionId = ifExistsGetConditionId(StringUtils.i2s(libId), frm.getLevelId(), level);
					rec.setConditionId(StringUtils.i2s(conditionId)); // This is the conditionId from condition Table

					if (conditionId > 0) {
						rec.setAddUpdateFlag("Update");
					} else {
						rec.setAddUpdateFlag("Add");
					}

					list.add(rec);
				}

				ConditionLibraryRecord[] recArray = new ConditionLibraryRecord[list.size()];
				list.toArray(recArray);

				frm.setConditionLibraryRecords(recArray);
				logger.debug("# of elements set in List - " + recArray.length);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception occured in getLibraryItems method of ConditionAgent. Message-" + e.getMessage());
		}

		return frm;
	}

	// end getLibraryItems}

	/**
	 * Function to get items from library for a specific sub category
	 */
	public ConditionLibraryForm getRequiredLibraryItems(ConditionLibraryForm frm) {
		logger.debug("Inside getRequiredLibraryItems");

		String type = frm.getType();
		String level = frm.getConditionLevel(); // L,S,O,P,Q,A
		String sql = "";
		String levelType = "";
		String inspectability = "";
		String warning = "";
		String required = "";

		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();

			rs = new CachedRowSet();

			sql = "SELECT * FROM LKUP_CONDITIONS WHERE CONDITION_TYPE = " + type + " AND LEVEL_TYPE = '" + level + "' and REQUIRED = 'Y' AND EXPIRATION_DT IS null";

			logger.debug("sql for conditionItem " + sql);

			rs = (CachedRowSet) db.select(sql);

			ArrayList list = new ArrayList();

			if (rs != null) {
				rs.beforeFirst();

				while (rs.next()) {
					String condTxt = new String(rs.getString("CONDITION_TEXT"));

					logger.debug("Condition Lib text #### : " + condTxt);
					logger.debug("condition code and library " + rs.getString("condition_code") + " : " + rs.getInt("condition_id"));

					levelType = rs.getString("LEVEL_TYPE");

					if (levelType == null) {
						levelType = "A";
					}

					inspectability = rs.getString("INSPECTABILITY");

					if (inspectability == null) {
						inspectability = "N";
					}

					warning = rs.getString("WARNING");

					if (warning == null) {
						warning = "N";
					}

					required = rs.getString("REQUIRED");

					if (required == null) {
						required = "N";
					}

					ConditionLibraryRecord rec = new ConditionLibraryRecord("off", levelType, "", inspectability, warning, "", required, rs.getString("SHORT_TEXT"), condTxt, rs.getInt("CONDITION_ID"), rs.getString("CONDITION_CODE"));
					list.add(rec);
				}

				ConditionLibraryRecord[] recArray = new ConditionLibraryRecord[list.size()];
				list.toArray(recArray);

				frm.setConditionLibraryRecords(recArray);
				logger.debug("# of elements set in List - " + recArray.length);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception occured in getLibraryItems method of ConditionAgent. Message-" + e.getMessage());
		}

		return frm;
	}

	// end getRequiredLibraryItems}

	public String getConditionType(String level, String levelId) {
		ResultSet rs = null;
		String sql = "";
		String type = "";

		try {
			Wrapper db = new Wrapper();

			if (level.equals("L")) {
				sql = "SELECT LSO_USE_ID FROM LAND_USAGE WHERE LAND_ID = " + levelId;
			} else if (level.equals("S")) {
				sql = "SELECT LSO_USE_ID FROM STRUCTURE_USAGE WHERE STRUCTURE_ID = " + levelId;
			} else if (level.equals("O")) {
				sql = "SELECT LSO_USE_ID FROM OCCUPANCY_USAGE WHERE OCCUPANCY_ID = " + levelId;
			} else if (level.equals("Q")) {
				sql = "SELECT SPROJ_TYPE FROM SUB_PROJECT WHERE SPROJ_ID = " + levelId;
			} else if (level.equals("A")) {
				sql = "SELECT TYPE_ID FROM LKUP_ACT_TYPE, ACTIVITY WHERE LKUP_ACT_TYPE.TYPE  = ACTIVITY.ACT_TYPE AND ACTIVITY.ACT_ID = " + levelId;
			}

			logger.debug("condition type sql " + sql);

			// dont make a query for projects
			if (!level.equals("P")) {
				rs = db.select(sql);
			}

			if (rs != null) {
				if (level.equals("L") || level.equals("S") || level.equals("O")) {
					if (rs.next()) {
						type = rs.getString("LSO_USE_ID");
					}

					/*
					 * } else if (level.equals("P")) { if (rs.next()) { form.setType(rs.getString("PROJ_TYPE")); }
					 */
				} else if (level.equals("Q")) {
					if (rs.next()) {
						type = rs.getString("SPROJ_TYPE");
					}
				} else if (level.equals("A")) {
					if (rs.next()) {
						type = rs.getString("TYPE_ID");
					}
				}
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception occured in getConditionType method of ConditionAgent. Message-" + e.getMessage());
		}

		// something must be returned otherwise application will crash
		if ((type == null) || type.equals("")) {
			type = "-1";
		}

		return type;
	}

	// end getConditionType

	public String getActTypeId(String type) {
		ResultSet rs = null;
		String sql = "";
		String typeId = "";

		try {
			Wrapper db = new Wrapper();

			// sql = "SELECT distinct TYPE_ID FROM LKUP_ACT_TYPE, ACTIVITY WHERE LKUP_ACT_TYPE.TYPE  = ACTIVITY.ACT_TYPE AND ACTIVITY.ACT_TYPE = '" + type + "'";
			sql = "select distinct type_id from lkup_act_type where type = " + StringUtils.checkString(type);

			logger.debug("condition type sql " + sql);

			// dont make a query for projects
			rs = db.select(sql);

			if (rs != null) {
				if (rs.next()) {
					typeId = rs.getString("TYPE_ID");
				}
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception occured in getActTypeId method of ConditionAgent. Message-" + e.getMessage());
		}

		// something must be returned otherwise application will crash
		if ((typeId == null) || type.equals("")) {
			typeId = "-1";
		}

		logger.debug("typeId is : " + typeId);

		return typeId;
	}

	// end getActTypeId

	public String getActType(String actTypeId) {
		ResultSet rs = null;
		String sql = "";
		String type = "";

		try {
			Wrapper db = new Wrapper();
			sql = "select type from lkup_act_type where type_id=" + actTypeId;
			rs = db.select(sql);

			if (rs.next()) {
				type = rs.getString("type");
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Exception occured in getActTypeId method of ConditionAgent. Message-" + e.getMessage());
		}

		return type;
	}

	public boolean copyRequiredCondition(String typeId, int actId, User user) {
		CommonAgent condAgent = new CommonAgent();
		ConditionLibraryForm conditionLibraryForm = new ConditionLibraryForm();
		boolean added = false;
		logger.debug("after init");

		try {
			conditionLibraryForm.setType(typeId);
			conditionLibraryForm.setConditionLevel("A");
			conditionLibraryForm.setLevelId(StringUtils.i2s(actId));
			conditionLibraryForm = condAgent.getRequiredLibraryItems(conditionLibraryForm);
			logger.debug("updated conditionLibraryForm");

			ConditionLibraryRecord[] recList = conditionLibraryForm.getConditionLibraryRecords();
			logger.debug("got library record");

			// create Condition objects for every record and send them to condition agent
			for (int i = 0; i < recList.length; i++) {
				ConditionLibraryRecord rec = recList[i];
				logger.debug("inside For 4 new condition");

				Condition condition = new Condition();

				// condition.setConditionId((new Integer(condId)).intValue());
				condition.setConditionId(0); // when Id is 0 a new record is created
				condition.setConditionLevel("A");
				condition.setLevelId(actId);
				condition.setShortText(rec.getShort_text());
				condition.setText(rec.getCondition_text());
				condition.setInspectable(rec.getInspectability());
				condition.setWarning(rec.getWarning());
				condition.setComplete("N");
				condition.setLibraryId(rec.getLibraryId());
				condition.setCreatedBy(user.getUserId());
				condition.setUpdatedBy(user.getUserId());
				logger.debug("before save");
				condAgent.saveCondition(condition);
				logger.debug("after save");
			}

			added = true;
		} catch (Exception e) {
			logger.debug("Exception occured in getActTypeId method of ConditionAgent. Message-" + e.getMessage());
		}

		return added;
	}

	// end getActTypeId

	public int getConditionCount(int levelId, String conditionLevel) {
		int conditionCount = 0;
		String sql = "";

		try {
			Wrapper db = new Wrapper();

			if (conditionLevel.equals("Q")) {
				sql = "select count(*) as count from conditions where level_Id=" + levelId + " and cond_level='Q'";
				logger.debug("The sub project Condition count sql" + sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					conditionCount = rs.getInt("count");
				}

				if (rs != null) {
					rs.close();
				}

			} else if (conditionLevel.equals("A")) {
				sql = "select count(*) as count from conditions where level_Id=" + levelId + " and cond_level='A'";
				logger.debug("The sub project Condition count sql" + sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					conditionCount = rs.getInt("count");
				}

				if (rs != null) {
					rs.close();
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}

		return conditionCount;
	}

	public int ifExistsGetConditionId(String libId, String levelId, String level) {
		int conditionId = 0;

		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT COND_ID FROM CONDITIONS WHERE LIBRARY_ID = " + libId);
			sql.append(" AND LEVEL_ID = " + levelId + " AND COND_LEVEL = '" + level + "'");
			logger.debug(sql.toString());

			RowSet rs = new Wrapper().select(sql.toString());

			if (rs.next()) {
				conditionId = rs.getInt("COND_ID");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return conditionId;
	}

	public int getWarningConditionCount(int levelId, String conditionLevel) {
		int conditionCount = 0;
		String sql = "";

		try {
			Wrapper db = new Wrapper();

			if (conditionLevel.equals("Q")) {
				sql = "select count(*) as count from conditions where level_Id=" + levelId + " and cond_level='Q' and warning='Y'";
				logger.debug("The sub project Condition count sql" + sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					conditionCount = rs.getInt("count");
				}

				if (rs != null) {
					rs.close();
				}

			} else if (conditionLevel.equals("A")) {
				sql = "select count(*) as count from conditions where level_Id=" + levelId + " and cond_level='A' and warning='Y'";
				logger.debug("The sub project Condition count sql" + sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					conditionCount = rs.getInt("count");
				}

				if (rs != null) {
					rs.close();
				}
			}
		} catch (Exception e) {
			logger.error(e);
		}

		return conditionCount;
	}

	public boolean conditionCodeExists(String conditionCode) {
		if ((conditionCode == null) || conditionCode.equals("")) {
			return false;
		}

		Wrapper db = new Wrapper();
		boolean exists = false;

		try {
			String sql = "select count(*) as count from lkup_conditions where condition_code=" + StringUtils.checkString(conditionCode);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				if (rs.getInt("count") == 0) {
					exists = false;
				} else {
					exists = true;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.debug("Error in conditionCodeExists() method" + e.getMessage());

			return true;
		}

		return exists;
	}

	public void deleteCondition(int conditionId) {
		if (conditionId > 0) {
			try {
				new Wrapper().update("DELETE FROM CONDITIONS WHERE COND_ID=" + conditionId);
			} catch (Exception e) {
				logger.error("Error in deleteCondition(" + conditionId + ") : " + e.getMessage());
			}
		}
	}

	/**
	 * Function to initialize conditions
	 * 
	 * @param iDate
	 * @return
	 * @throws Exception
	 * @author Hemavathi,Bharti
	 */

	public boolean initializeConditions(Calendar iDate, int moduleId) throws Exception {
		logger.info("initializeConditions(" + iDate + ")");

		boolean success = false;
		RowSet rs = null;
		RowSet rsSelect = null;
		String sql = "";
		int minLkupKey = 0;
		int maxLkupKey = 0;
		int lkupKeyIncrement = 0;

		logger.info("Entering initializeConditions()");

		try {
			Wrapper db = new Wrapper();

			sql = "select min(CONDITION_ID) as min_fee_lkup,max(CONDITION_ID) as max_fee_lkup from LKUP_CONDITIONS where EXPIRATION_DT is null";
			logger.debug(sql);
			rs = db.select(sql);

			if (rs.next()) {
				minLkupKey = rs.getInt("min_fee_lkup");
				logger.debug("the min lkup key is " + minLkupKey);
				maxLkupKey = rs.getInt("max_fee_lkup");
				logger.debug("the min lkup key is " + maxLkupKey);
			}
			lkupKeyIncrement = maxLkupKey - minLkupKey + 1;

			// select the values from LKUP_CONDITIONS table
			sql = "select lc.CONDITION_ID ,lc.LEVEL_TYPE,lc.CONDITION_CODE,lc.CONDITION_DESC,lc.SHORT_TEXT,lc.INSPECTABILITY,lc.WARNING,lc.CONDITION_TYPE,lc.REQUIRED,lc.CONDITION_TEXT,lc.ZONE_TYPE,lc.CREATION_DT,lc.EXPIRATION_DT,lc.CREATED_BY,lc.UPDATED_BY from LKUP_CONDITIONS lc LEFT OUTER JOIN LKUP_ACT_TYPE lct ON lc.CONDITION_TYPE= lct.TYPE_ID LEFT OUTER JOIN LKUP_MODULE  lm ON lct.MODULE_ID=lm.ID where (lm.ID= " + moduleId + "  and lc.EXPIRATION_DT is null)";
			logger.debug(sql);
			rsSelect = db.select(sql);
			db.beginTransaction();
			while (rsSelect.next()) {
				maxLkupKey = maxLkupKey + 1;

				// insert duplicate records to LKUP_CONDITIONS table
				String sqlInsert = " insert into LKUP_CONDITIONS (CONDITION_ID,LEVEL_TYPE,CONDITION_CODE,CONDITION_DESC,SHORT_TEXT,INSPECTABILITY,WARNING,CONDITION_TYPE,REQUIRED,CONDITION_TEXT,ZONE_TYPE,CREATION_DT,EXPIRATION_DT,CREATED_BY,UPDATED_BY)VALUES(" + maxLkupKey + "," + StringUtils.checkString(rsSelect.getString("LEVEL_TYPE")) + "," + StringUtils.checkString(rsSelect.getString("CONDITION_CODE")) + "," + StringUtils.checkString(rsSelect.getString("CONDITION_DESC")) + "," + StringUtils.checkString(rsSelect.getString("SHORT_TEXT")) + "," + StringUtils.checkString(rsSelect.getString("INSPECTABILITY")) + "," + StringUtils.checkString(rsSelect.getString("WARNING")) + "," + rsSelect.getString("CONDITION_TYPE") + "," + StringUtils.checkString(rsSelect.getString("REQUIRED")) + "," + StringUtils.checkString(rsSelect.getString("CONDITION_TEXT")) + "," + StringUtils.checkString(rsSelect.getString("ZONE_TYPE")) + "," + StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("CREATION_DT")))) + "," + StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("EXPIRATION_DT")))) + "," + rsSelect.getString("CREATED_BY") + "," + rsSelect.getString("UPDATED_BY") + ") ";
				db.addBatch(sqlInsert);
				// update the expiration date
				String sqlUpdate = "UPDATE LKUP_CONDITIONS SET EXPIRATION_DT= " + StringUtils.toOracleDate(StringUtils.cal2str(iDate)) + " WHERE (CONDITION_ID = " + rsSelect.getString("CONDITION_ID") + " AND EXPIRATION_DT IS NULL)";
				logger.debug(sqlUpdate);
				db.addBatch(sqlUpdate);

			}
			db.executeBatch();
			// update IDVALUE field in nextid table
			String sqlUpdateNextId = "update nextid set IDVALUE=(select max(CONDITION_ID) from LKUP_CONDITIONS) where IDNAME='LIB_CONDITION_ID'";
			logger.debug(sqlUpdateNextId);
			db.update(sqlUpdateNextId);

			success = true;
			
			if (rs != null) {
				rs.close();
			}
			
			if (rsSelect != null) {
				rsSelect.close();
			}

			return success;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Exception("Exception in initializeCondition, message is " + e.getMessage());
		}
	}

	/**
	 * @param Function
	 *            to get list of modules
	 * @return
	 * @author Hemavathi,Bharthi
	 * @throws Exception
	 */

	public static List getModuleList() throws Exception {
		logger.info("getModuleList()");

		List moduleList = new ArrayList();
		RowSet rs = null;

		try {

			String sql = "select * from lkup_module order by description";
			logger.debug(sql);

			rs = new Wrapper().select(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String description = StringUtils.properCase(rs.getString("description"));
				Module module = new Module();
				module.setId(id);
				module.setDescription(description);
				moduleList.add(module);
			}

			if (rs != null) {
				rs.close();
			}
			logger.debug("returning moduleList of size " + moduleList.size());
			return moduleList;
		} catch (Exception e) {
			logger.error("Exception occured in moduleList method . Message-" + e.getMessage());
			throw e;
		}
	}

	public void updateConditonOrder(String conditionId, int order) {
		logger.info("updateConditonOrder(" + conditionId + ", " + order + ")");

		int startOrder = order + 1;
		try {
			String sql = "UPDATE CONDITIONS SET ORDER_ID= " + startOrder + " WHERE COND_ID=" + conditionId;
			logger.debug(sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error in updateConditonOrder(" + conditionId + ") : " + e.getMessage());
		}

	}

	public void updateConditonLibraryOrder(String conditionId, int order) {
		logger.info("updateConditonLibraryOrder(" + conditionId + ", " + order + ")");

		int startOrder = order + 1;
		try {
			String sql = "UPDATE LKUP_CONDITIONS SET ORDER_ID= " + startOrder + " WHERE CONDITION_ID=" + conditionId;
			logger.debug(sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error in updateConditonLibraryOrder(" + conditionId + ") : " + e.getMessage());
		}

	}

	/**
	 * Insert a new row to process and activity team
	 * 
	 * @param form
	 * @return
	 */
	public ProcessTeamForm insertRow(ProcessTeamForm form) {
		logger.debug("insertRow called");

		ProcessTeamCollectionRecord[] processTeamCollectionRecord = form.getProcessTeamCollectionRecord();
		ProcessTeamCollectionRecord[] tmp = new ProcessTeamCollectionRecord[processTeamCollectionRecord.length + 1];
		ProcessTeamCollectionRecord rec = new ProcessTeamCollectionRecord();
		List titleList = this.getTeamTitles();
		rec.setTitleList(titleList);

		logger.debug("Final length is: " + tmp.length);

		List list = Arrays.asList(processTeamCollectionRecord);
		list.toArray(tmp);
		tmp[tmp.length - 1] = rec;
		form.setProcessTeamCollectionRecord(tmp);

		return form;
	}

	/**
	 * populate the team
	 * 
	 * @param form
	 * @return
	 */
	public ProcessTeamForm populateTeam(ProcessTeamForm form) throws Exception {
		String psaId = form.getPsaId();
		String psaTypeId = form.getPsaType();

		if ((psaId == null) || psaId.equals("")) {
			return new ProcessTeamForm();
		}

		if ((psaTypeId == null) || psaTypeId.equals("")) {
			return new ProcessTeamForm();
		}

		try {
			Wrapper db = new Wrapper();
			List list = new ArrayList();

			String sql = "SELECT PT.LEAD, PT.USERID, PT.GROUP_ID, pt.created_by,pt.created,pt.updated_by,pt.updated,R.NAME, U.FIRST_NAME, U.LAST_NAME FROM PROCESS_TEAM PT, GROUPS R, USERS U WHERE PT.PSA_ID = " + psaId + " AND PT.PSA_TYPE = '" + psaTypeId + "' AND PT.USERID = U.USERID AND PT.GROUP_ID = R.GROUP_ID order by PT.LEAD desc,pt.created desc";
			logger.debug("Process Team SQL : " + sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				ProcessTeamRecord rec = new ProcessTeamRecord();
				rec.setLead(rs.getString("LEAD"));
				rec.setName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
				rec.setNameId(rs.getString("USERID"));
				rec.setTitle(rs.getString("NAME"));
				rec.setTitleId(rs.getString("GROUP_ID"));
				rec.setIsNew(false);
				rec.setCreatedBy(rs.getInt("created_by"));
				rec.setCreated(rs.getDate("created"));
				rec.setUpdatedBy(rs.getInt("updated_by"));
				rec.setUpdated(rs.getDate("updated"));
				list.add(rec);
			}

			ProcessTeamRecord[] tmp = new ProcessTeamRecord[list.size()];
			list.toArray(tmp);

			form.setProcessTeamRecord(tmp);

			ProcessTeamCollectionRecord[] tmpNew = new ProcessTeamCollectionRecord[0];
			form.setProcessTeamCollectionRecord(tmpNew);
			
			if (rs != null) {
				rs.close();
			}
			
			return form;
		} catch (Exception e) {
			logger.warn("Exception in method populateTeam. message - " + e.getMessage());
			throw e;
		}

	}

	/**
	 * Populate name and titles for the process team
	 * 
	 * @param form
	 * @return
	 */
	public ProcessTeamForm populateNameAndTitle(ProcessTeamForm form) {
		ProcessTeamRecord[] processTeamRecord = form.getProcessTeamRecord();
		ProcessTeamRecord newRec = null;
		Wrapper db = new Wrapper();
		RowSet rs = null;
		String sqlName = "";
		String sqlTitle = "";
		String name = "";
		String title = "";

		try {
			for (int i = 0; i < processTeamRecord.length; i++) {
				newRec = processTeamRecord[i];
				name = newRec.getNameId();
				sqlName = "SELECT FIRST_NAME, LAST_NAME FROM USERS WHERE USERID = " + name;
				rs = db.select(sqlName);

				if (rs.next()) {
					newRec.setName(StringUtils.properCase(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME")));
				}

				title = newRec.getTitleId();
				sqlTitle = "SELECT NAME FROM GROUPS WHERE GROUP_ID = " + title;
				rs = db.select(sqlTitle);

				if (rs.next()) {
					newRec.setTitle(StringUtils.properCase(rs.getString("NAME")));
				}

				newRec.setIsNew(false);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception in method populateNameAndTitle. message - " + e.getMessage());
		}

		return form;
	}

	/**
	 * Gets a list of roles
	 */
	public List getTeamTitles() {
		List list = new ArrayList();
		list.add(new ProcessTeamTitleRecord());

		try {
			Wrapper db = new Wrapper();
			String sql = "SELECT * FROM GROUPS WHERE CATEGORY=1";
			RowSet rs = db.select(sql);

			while (rs.next()) {
				ProcessTeamTitleRecord rec = new ProcessTeamTitleRecord();
				rec.setTitleValue(rs.getString("GROUP_ID"));
				rec.setTitleLabel(rs.getString("NAME"));
				list.add(rec);
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception in method getTitleList. message - " + e.getMessage());
		}

		logger.debug("getitleList returning a length of: " + list.size());

		return list;
	}

	/**
	 * Gets a list of users for a specific role. Gets users which have not already been assigned to this psaId in this specific role
	 */
	public List getUsersForTitle(String role, String psaId, int deptId) {
		List list = new ArrayList();

		ProcessTeamNameRecord rec;

		if ((role == null) || role.equals("")) {
			logger.debug("improper role id: " + role); 

			return list;
		}

		try {
			Wrapper db = new Wrapper();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT U.USERID,U.FIRST_NAME || ' ' || U.LAST_NAME AS NAME");
			sql.append(" FROM USERS U JOIN USER_GROUPS UG ON U.USERID = UG.USER_ID");
			sql.append(" WHERE UG.GROUP_ID IN (" + role + ")");
			sql.append(" AND U.ACTIVE != 'N'");
			sql.append(" AND U.USERID NOT IN (SELECT USERID FROM PROCESS_TEAM WHERE PSA_ID = " + psaId + ")");
			sql.append(" AND U.DEPT_ID = " + deptId );
			sql.append(" ORDER BY U.FIRST_NAME || ' ' || U.LAST_NAME");
			logger.debug(sql);

			RowSet rs = db.select(sql.toString());

			rec = new ProcessTeamNameRecord();
			list.add(rec);

			while (rs.next()) {
				rec = new ProcessTeamNameRecord();
				rec.setNameValue(rs.getString("USERID"));
				rec.setNameLabel(rs.getString("NAME"));
				list.add(rec);
			}
			
			if (rs != null) {
				rs.close();
			}
			
		} catch (Exception e) {
			logger.error("Exception in method getUsersForTitle. message - " + e.getMessage());
		}

		logger.debug("getUsersFor returning a length of :" + list.size());

		return list;
	}

	public void saveProcessTeam(String psaId, String psaType, int userId, ProcessTeamRecord rec) {
		logger.debug("PsaId " + psaId + ",psaType " + psaType + ", userid " + userId);
		String sql;
		String projectId = "";
		int projCount = 0;

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();
			sql = processRecord(psaType, psaId, userId, rec);
			db.addBatch(sql);

			if (psaType.equalsIgnoreCase("A")) {
				projectId = new ActivityAgent().getProjectId(psaId);
				projCount = countProjTeam(projectId, rec.getNameId(), rec.getTitleId());
				logger.debug("The projcount is " + projCount);
			}

			db.executeBatch();

		} catch (Exception e) {
			logger.error("Exception Occured in " + e.getMessage());

		}
	}

	/**
	 * Save team
	 * 
	 * @param form
	 * @param userId
	 */
	public void saveTeam(ProcessTeamForm form, int userId) {
		logger.info("saveTeam(form, " + userId + ")");

		String projectId = "";
		int projCount = 0;

		ProcessTeamRecord[] processTeamRecord = form.getProcessTeamRecord();

		String sql;
		String psaId = form.getPsaId();
		String psaType = form.getPsaType();

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			for (int i = 0; i < processTeamRecord.length; i++) {
				if (processTeamRecord[i] != null) {
					if ((processTeamRecord[i].getNameId() != null) && !processTeamRecord[i].getNameId().trim().equals("")) {
						logger.debug("Then Name Id is : " + processTeamRecord[i].getNameId());
						sql = processRecord(psaType, psaId, userId, processTeamRecord[i]);
						db.addBatch(sql);

						if (psaType.equalsIgnoreCase("A")) {
							projectId = new ActivityAgent().getProjectId(psaId);
							projCount = countProjTeam(projectId, processTeamRecord[i].getNameId(), processTeamRecord[i].getTitleId());
							logger.debug("The projcount is " + projCount);
						}

						/*
						 * if ((psaType.equalsIgnoreCase("A")) && (projCount == 0)) { sql = processProjRecord("P", projectId, userId, processTeamRecord[i]); db.addBatch(sql); }
						 */
					}
				}
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error("Exception in method saveTeam. message - " + e.getMessage());
		}
	}

	public void addTeam(ProcessTeamForm form, int userId) {
		ProcessTeamCollectionRecord[] newTeam = form.getProcessTeamCollectionRecord();

		String sql;
		String psaId = form.getPsaId();
		String psaType = form.getPsaType();

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			for (int i = 0; i < newTeam.length; i++) {
				if (newTeam[i] != null) {
					if ((newTeam[i].getName() != null) && !newTeam[i].getName().trim().equals("")) {
						sql = processRecord(psaType, psaId, userId, newTeam[i]);
						db.addBatch(sql);
					}
				}
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error("Exception in method saveTeam. message - " + e.getMessage());
		}
	}

	public int countProjTeam(String psaId, String nameId, String titleId) {
		int nbr = 0;    

		try {
			String sql = "SELECT COUNT(PSA_ID) AS cnt FROM PROCESS_TEAM WHERE PSA_ID = " + psaId + " AND PSA_TYPE = 'P' AND USERID = " + nameId + " AND GROUP_ID =" + StringUtils.checkString(StringUtils.nullReplaceWithEmpty(titleId)) + "";

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				nbr = rs.getInt("cnt");
			}
			
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return nbr;
	}

	public String processProjRecord(String psaType, String psaId, int userId, ProcessTeamRecord rec) {
		String sql = "";

		sql = "insert into process_team values (" + psaId + "," + StringUtils.checkString(psaType) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getNameId())) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getTitleId())) + "," + StringUtils.checkString(rec.getLead().trim()) + "," + userId + "," + "current_timestamp" + "," + userId + "," + "current_timestamp" + ")";

		logger.debug(sql);

		return sql;
	}

	// end for

	/**
	 * Process the team record
	 * 
	 * @param psaType
	 * @param psaId
	 * @param userId
	 * @param rec
	 * @return
	 */
	public String processRecord(String psaType, String psaId, int userId, ProcessTeamRecord rec) {
		logger.info("processRecord(" + psaType + ", " + psaId + ", " + userId + ", ProcessTeamRecord )");

		String sql = "";

		if (!rec.getCheckRemove().equals("on")) {
			// insert if it is a new record
			if (rec.getIsNew()) {
				sql = "insert into process_team values (" + psaId + "," + StringUtils.checkString(psaType) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getNameId())) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getTitleId())) + "," + StringUtils.checkString(rec.getLead().trim()) + "," + userId + "," + "current_timestamp" + "," + userId + "," + "current_timestamp" + ")";
			} else {
				// update if it is an old record
				String lead = rec.getLead().trim();
				if (lead.equals(""))
					lead = "of";

				sql = "update process_team set lead=" + StringUtils.checkString(lead) + ", updated_by=" + userId + ", updated=current_timestamp where psa_id= " + psaId + " and userid=" + rec.getNameId() + " and group_id=" + rec.getTitleId();
			}
		} else {
			// case for records to be deleted
			sql = "delete from process_team where psa_id = " + psaId + " and userid = " + rec.getNameId() + " and group_id = " + rec.getTitleId();
		}

		logger.debug(sql);

		return sql;
	}

	public String processRecord(String psaType, String psaId, int userId, ProcessTeamCollectionRecord rec) {
		String sql = "";

		if (!rec.getCheckRemove().equals("on")) {
			sql = "insert into process_team values (" + psaId + "," + StringUtils.checkString(psaType) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getName())) + "," + Integer.parseInt(StringUtils.nullReplaceWithZero(rec.getTitle())) + "," + StringUtils.checkString(rec.getLead().trim()) + "," + userId + "," + "current_timestamp" + "," + userId + "," + "current_timestamp" + ")";

		}

		logger.debug(sql);

		return sql;
	}

	public ProcessTeamForm setCheckedCollection(ProcessTeamForm form, HttpServletRequest request) {
		ProcessTeamForm frm = form;
		String parameter;
		int i = 0;
		int j = 0;
		String indx = "";

		Enumeration enum1 = request.getParameterNames();

		while (enum1.hasMoreElements()) {
			parameter = (String) enum1.nextElement();
			logger.debug("parameter is: " + parameter);

			if (parameter.startsWith("processTeamCollectionRecord[")) {
				i = parameter.indexOf("].lead");
				indx = parameter.substring(28, i);
			}
		}

		logger.debug("indx is: " + indx);

		ProcessTeamCollectionRecord[] collArray = frm.getProcessTeamCollectionRecord();
		ProcessTeamCollectionRecord coll;

		for (j = 0; j < collArray.length; j++) {
			coll = collArray[j];
			coll.setLead("of");
		}

		try {
			int tmp = Integer.parseInt(indx);
			coll = collArray[tmp];
			coll.setLead("on");
		} catch (NumberFormatException e) {
			logger.error("NumberFormatException. message:" + e.getMessage());
		}

		frm.setProcessTeamCollectionRecord(collArray);

		return frm;
	}

	// end setCheckedCollection

	/**
	 * gets the list of department Ids
	 * 
	 * @param userId
	 * @return dept Id
	 * @author Hemavathi
	 * @throws Exception
	 */
	public static int getDepartmentId(int userId) throws Exception {
		logger.debug("getProjectNames()");

		List projectNames = new ArrayList();
		String sql = "";
		int deptId = 0;
		try {
			sql = "SELECT dept_id FROM users where userid=" + userId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			while (rs != null && rs.next()) {
				deptId = rs.getInt("dept_id");
			}

			if (rs != null) {
				rs.close();
			}

			return deptId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * records inserted into bl approval
	 * 
	 * @param userId
	 *            ,activity_id,deptId
	 * @author Hemavathi
	 * @throws Exception
	 */
	public void insertIntoBTapproval(int activityId, int deptId, int userId) throws Exception {

		String sql = "";
		Wrapper db = new Wrapper();

		try {
			int approvalId = db.getNextId("APPROVAL_ID");
			logger.debug("approval id obtained is " + approvalId);
			sql = "INSERT INTO BL_APPROVAL (APPROVAL_ID,REFERRED_DT,BL_APPR_ST,APPROVED_BY,CREATED,CREATED_BY,ACT_ID,DEPT_ID) values (";
			sql += approvalId;
			sql += ",";
			sql += "current_date";
			sql += ",";
			sql += "100"; // pending status //TODO: Get this from the constants --AB/SV Oct 12,2007
			sql += ",";
			sql += userId;
			sql += ",";
			sql += "current_timestamp";
			sql += ",";
			sql += userId;
			sql += ",";
			sql += activityId;
			sql += ",";
			sql += deptId;
			sql += ")";
			logger.info(sql);
			db.insert(sql);
		} catch (DuplicateRecordException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Exception occured while saving activity team" + e.getMessage());
			throw new Exception("Exception occured while saving activity team " + e.getMessage());
		}
	}
	
	public List getTenants(int id, String type, int numberOfRows) {
		logger.debug("Entered into getTenants  "+id+" "+type);
		List tenants = new ArrayList();
		Tenant tenant = null;
		String sql = "";

		try {
			Wrapper db = new Wrapper();
			if ("A".equalsIgnoreCase(type) ) {
				
				sql=" SELECT * FROM activity_people ap,people p,people_type pt, PEOPLE_LSOID pl  "
			        +" WHERE p.people_id=ap.people_id AND  p.people_type_id=pt.people_type_id "
			        +" AND ap.act_id IN (select occupancy_id"
			        +" FROM structure_occupant so,land_structure ls,lso_address ladd"
			        +" WHERE ladd.lso_id IN (select distinct(land_id)"
			        +" FROM structure_occupant so,land_structure ls,lso_address ladd"
			        +" WHERE so.occupancy_id= " + id + " "
			        +" AND so.structure_id=ls.structure_id"
			        +" AND ladd.lso_type='O')"
			        +" AND ls.land_id=ladd.lso_id"
			        +" AND ls.structure_id=so.structure_id) "
			        +" AND ap.psa_type='O'"
			        +" AND pt.description ='"+Constants.STENANT+"'";
			}
			if ( "O".equalsIgnoreCase(type)) {
				sql = "select * from activity_people ap,people p,people_type pt, PEOPLE_LSOID pl  where p.people_id=ap.people_id and p.people_type_id=pt.people_type_id and ap.act_id=" + id + " and ap.psa_type in ('" + type + "') and pt.description = '"+Constants.STENANT+"' ";
			}
			if("L".equalsIgnoreCase(type)){
				sql = "select * from activity_people ap,people p,people_type pt, PEOPLE_LSOID pl  where p.people_id=ap.people_id and  p.people_type_id=pt.people_type_id and ap.act_id in (select LSO_ID from V_LSO_LAND where LAND_ID = "+ id +" and LSO_TYPE = 'O') and pt.description = '"+Constants.STENANT+"'";
			}

			if (numberOfRows > 0) {
				sql = sql + "and rownum <= " + numberOfRows;
			}
			
			sql = sql + " and p.PEOPLE_ID = pl.PEOPLE_ID and ( pl.MOVE_OUT_DATE is null or  pl.MOVE_OUT_DATE > current_date ) ";
			sql = sql + " order by ap.psa_type desc, p.name";
			
			logger.info(sql);
			
			RowSet rs = db.select(sql);

			while (rs.next()) {
				tenant = new Tenant();
				tenant.setName(rs.getString("name"));
				tenant.setTenantId(rs.getString("PEOPLE_ID"));
				logger.debug("getTenantId  "+tenant.getTenantId());
				tenants.add(tenant);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (java.sql.SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
		}

		return tenants;
	}
	
	public List getNotices(int actId) {
		List notices = new ArrayList();
		CodeEnforcement codeEnforcement = null;
		String sql = "";

		try {
			Wrapper db = new Wrapper();
			sql = "select * from code_enforcement where act_id=" + actId;

			logger.debug("notices sql :" + sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				codeEnforcement = new CodeEnforcement();
				codeEnforcement.setId(rs.getInt("CE_ID"));
				codeEnforcement.setName(rs.getString("NAME"));
				notices.add(codeEnforcement);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (java.sql.SQLException se) {
			logger.error("SQL: " + sql);
			logger.error(se.getMessage());
		} catch (Exception e) {
			logger.error("SQL:" + sql);
			logger.error(e);
		}

		return notices;
	}

	public String getAttachmentTypeDesc(int attachmentTypeId) {
		RowSet rs = null;
		String attachmentTypeDesc = "";
		try {
			Wrapper db = new Wrapper();
			String sql = "select DESCRIPTION from LKUP_ATTACHMENT_TYPE where TYPE_ID=" + attachmentTypeId;

			logger.debug("notices sql :" + sql);

			rs = db.select(sql);

			while (rs.next()) {
				attachmentTypeDesc = rs.getString("DESCRIPTION");				
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(rs != null) {
					rs.close();
				}
			}catch(Exception e) {
				
			}
		}
		return attachmentTypeDesc;

	}
}

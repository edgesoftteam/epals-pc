package elms.agent;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.sql.RowSet;

import org.apache.log4j.Logger;
import org.jdom.Comment;
import org.jdom.Document;
import org.jdom.Element;

import elms.app.common.Agent;
import elms.common.Constants;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class ReportAgent extends Agent {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(ReportAgent.class.getName());
	int actId = -1;
	int projId = -1;
	int sprojId = -1;
	String apn = "";
	String imgPath = "";

	public ReportAgent() {
	}
	
	public ReportAgent(String imgPath) {
		logger.debug("imgPath  :: "+imgPath);
		this.imgPath = imgPath;
	}
	/**
	 * Get the general permit JDOM
	 * 
	 * @param actNbr
	 * @param departmentCode
	 * @param activityType
	 * @return
	 */
	public Document getGeneralPermitJDOM(String actNbr, String departmentCode, String activityType) {
		logger.info("getGeneralPermitJDOM(" + actNbr + ", " + departmentCode + ", " + activityType + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			// Create root Element
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			// Create data Element
			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Comment xmlDataElement = new Comment("************** XML DATA **************");
			dataElement.addContent(xmlDataElement);

			// Start getting elements and add to dataElement
			Element mainElement = getMainElement(actNbr);

			// Add Plan reviewer
			mainElement.addContent(getPlanReviewer(actId));

			// mainElement.addContent(PRE);
			dataElement.addContent(mainElement);
			logger.debug(" int id is " + actId);

			if (actId > 0) {
				logger.debug("just before peopleGrpElement ");

				Element peopleGrpElement = getPeopleGrpElement(actId);
				dataElement.addContent(peopleGrpElement);

				logger.debug("just before applicantGrpElement ");

				Element applicantGrpElement = getApplicantGrpElement(actId);
				dataElement.addContent(applicantGrpElement);

				logger.debug("just before ownerElement ");

				Element ownerElement = getOwnerElement(actId);
				dataElement.addContent(ownerElement);

				logger.debug("just before feesGrpElement ");

				Element feesGrpElement = getFeesGrpElement(actId);
				dataElement.addContent(feesGrpElement);

				logger.debug("just before ledgerGrpElement ");

				Element ledgerGrpElement = getLedgerGrpElement(actId);
				dataElement.addContent(ledgerGrpElement);

				logger.debug("just before permitfeesElement");

				Element permitfeesElement = getPermitFeesElement(actId);
				dataElement.addContent(permitfeesElement);

				logger.debug("just before plancheckfeesElemen ");

				Element plancheckfeesElement = getPlancheckFeesElement(actId);
				dataElement.addContent(plancheckfeesElement);

				Element developmentFeesElement = getDevelopmentFeesElement(actId);
				dataElement.addContent(developmentFeesElement);

				logger.debug("just before totalfeesElement ");

				Element totalfeesElement = getTotalFeesElement(actId);
				dataElement.addContent(totalfeesElement);

				logger.debug("just before conditionsGrpElement");

				Element conditionsGrpElement = getConditionsGrpElement(actId);
				dataElement.addContent(conditionsGrpElement);

				logger.debug("just before commentsGrpElement");

				Element commentsGrpElement = getCommentsGrpElement(actId);
				dataElement.addContent(commentsGrpElement);

				logger.debug("Just before planningStatusGrpElement");

				Element planningStatusGroup = ActivityAgent.getPlanningStatusGrpElement(actId, departmentCode);
				dataElement.addContent(planningStatusGroup);

				logger.debug("Just before planningResolutionGrpElement");

				Element planningResolutionGroup = ActivityAgent.getPlanningResolutionGrpElement(actId, departmentCode);
				dataElement.addContent(planningResolutionGroup);

				logger.debug("Just before planningEnvReviewGrpElement");

				Element planningEnvReviewGroup = ActivityAgent.getPlanningEnvReviewGrpElement(actId, departmentCode);
				dataElement.addContent(planningEnvReviewGroup);

			}
		} catch (Exception e) {
			logger.error(" Exception in ReportAgent -- getPermitJDOM() method " + e.getMessage());
		}

		return document;
	}

	// For Field Inspection Reports
	public Document getFieldInspectionJDOM(String actNbr, String departmentCode) {
		logger.info("getFieldInspectionJDOM(" + actNbr + "," + departmentCode + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			// Create root Element
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			// Create data Element
			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			// Start getting elements and add to dataElement
			Element mainElement = getMainElement(actNbr);
			dataElement.addContent(mainElement);
			logger.debug(" int id is " + actId);

			if (actId > 0) {
				logger.debug("just before peopleFIGrpElement ");

				// Element peopleFIGrpElement = getPeopleFIGrpElement(projId);
				// FIR was printing people of all the activities in the project.
				// So checking people for that particular activity is retrieved
				// below.
				Element peopleFIGrpElement = getPeopleFIGrpElement(actId);
				dataElement.addContent(peopleFIGrpElement);

				logger.debug("just before processTeamElement ");

				Element processTeamElement = getProcessTeamElement(actId);
				dataElement.addContent(processTeamElement);

				logger.debug("just before applicantGrpElement ");

				Element applicantGrpElement = getApplicantGrpElement(actId);
				dataElement.addContent(applicantGrpElement);

				logger.debug("just before ownerElement ");

				Element ownerElement = getOwnerElement(actId);
				dataElement.addContent(ownerElement);

				logger.debug("just before assessorElement ");

				// this section does not mean anything in FIR. it is taken from
				// assessor table.
				/*
				 * Element assessorElement = getAssessorElement(apn); dataElement.addContent(assessorElement);
				 */
				logger.debug("just before feesGrpElement ");

				Element feesGrpElement = getFeesGrpElement(actId);
				dataElement.addContent(feesGrpElement);

				logger.debug("just before ledgerGrpElement ");

				Element ledgerGrpElement = getLedgerGrpElement(actId, "FIR");
				dataElement.addContent(ledgerGrpElement);

				logger.debug("just before permitfeesElement");

				Element permitfeesElement = getPermitFeesElement(actId);
				dataElement.addContent(permitfeesElement);

				logger.debug("just before plancheckfeesElemen ");

				Element plancheckfeesElement = getPlancheckFeesElement(actId);
				dataElement.addContent(plancheckfeesElement);

				Element developmentFeesElement = getDevelopmentFeesElement(actId);
				dataElement.addContent(developmentFeesElement);

				logger.debug("just before totalfeesElement ");

				Element totalfeesElement = getTotalFeesElement(actId);
				dataElement.addContent(totalfeesElement);

				logger.debug("just before permitsGrpElement");

				Element permitsGrpElement = getPermitsGrpElement(sprojId);
				dataElement.addContent(permitsGrpElement);

				logger.debug("just before inspectionsGrpElement");

				Element inspectionsGrpElement = getInspectionsGrpElement(sprojId);
				dataElement.addContent(inspectionsGrpElement);

				logger.debug("just before Active holds");

				Element activeholds = getActiveHoldsForFIR(actId);
				dataElement.addContent(activeholds);

				logger.debug("just before conditionsGrpElement");

				Element conditionsGrpElement = getConditionsGrpElement(actId);
				dataElement.addContent(conditionsGrpElement);

				logger.debug("just before BusTaxGrpElement");

				Element busTaxGrpElement = getBusTaxGrpElement(actId);
				dataElement.addContent(busTaxGrpElement);

				logger.debug("Just before planningStatusGrpElement");

				Element planningStatusGroup = ActivityAgent.getPlanningStatusGrpElement(actId, departmentCode);
				dataElement.addContent(planningStatusGroup);

				logger.debug("Just before planningResolutionGrpElement");

				Element planningResolutionGroup = ActivityAgent.getPlanningResolutionGrpElement(actId, departmentCode);
				dataElement.addContent(planningResolutionGroup);

				logger.debug("Just before planningEnvReviewGrpElement");

				Element planningEnvReviewGroup = ActivityAgent.getPlanningEnvReviewGrpElement(actId, departmentCode);
				dataElement.addContent(planningEnvReviewGroup);

				Element commentElement = getCommentsGrpElement(actId);
				dataElement.addContent(commentElement);
			}
		} catch (Exception e) {
			logger.error(" Exception in ReportAgent -- getFieldInspectionJDOM() method " + e.getMessage());
		}

		return document;
	}

	public Document getBusinessTaxJDOM(String actNbr, String peopleId) {
		logger.debug("In PublicWorksReportAgent.getBusinessTaxJDOM(" + actNbr + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Element mainElement = getMainElement(actNbr);
			mainElement.addContent(getPlanReviewer(actId));

			dataElement.addContent(mainElement);

			logger.debug(" int id is " + actId);

			if (actId > 0) {
				Element contractors = null;
				logger.debug("just before contractors element ");

				if ((peopleId == null) || peopleId.equals("") || peopleId.equals("all")) {
					contractors = getBusinessTaxContractors(actId, 0);
				} else {
					contractors = getBusinessTaxContractors(actId, StringUtils.s2i(peopleId));
				}

				dataElement.addContent(contractors);
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getBusinessTaxJDOM()" + e.getMessage());
		}

		return document;
	}

	public Element getMainElement(String actNbr) {
		logger.info("getMainElement(" + actNbr + ")");

		Element mainElement = new Element("main");

		try {
			StringBuffer sql = new StringBuffer();
			if (actNbr.trim().startsWith("BL")) {
				sql.append("SELECT T1.ACT_ID C1,T1.ACT_NBR C2,T1.VALUATION C3,T1.description C4,T1.APPLIED_DATE C5,");
				sql.append("T1.ISSUED_DATE C6,T1.EXP_DATE C7,T1.FINAL_DATE C8,T3.description C9,T4.DL_ADDRESS C10, ( VAL.CITY || ' ' || VAL.STATE || ' ' || VAL.ZIP) C30, ");
				sql.append("T1.PROJECT_NBR C11,SP.SPROJ_NBR C24, SP.SPROJ_ID C25, T1.NAME C12,T6.description C13,UPPER(T7.description) C14,");
				sql.append("CASE WHEN NOT T9.USERNAME IS NULL THEN T9.USERNAME ELSE '' END C15,T1.PROJ_ID C18,T1.PROJDESC c19,");
				sql.append("LSP.description C20,LSSP.description C21,VLU.DESCRIPTION C22,T1.ACT_TYPE C23,");
				sql.append("BL.BUSINESS_NAME C27, BL.BUSINESS_ACC_NO C28");
				sql.append(" FROM (((((((((V_ACTIVITY T1 JOIN LKUP_ACT_ST T3 ON T1.STATUS = T3.STATUS_ID)");
				sql.append(" JOIN LKUP_ACT_TYPE T6 ON T1.ACT_TYPE = T6.TYPE)");
				sql.append(" LEFT OUTER JOIN DEPARTMENT T7 ON  T7.DEPT_CODE='LC')");
				sql.append(" JOIN SUB_PROJECT SP ON T1.SPROJ_ID = SP.SPROJ_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_SPROJ_TYPE LSP ON SP.STYPE_ID = LSP.SPROJ_TYPE_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_SPROJ_STYPE LSSP ON SP.SSTYPE_ID = LSSP.SPROJ_STYPE_ID)");
				sql.append(" LEFT OUTER JOIN USERS T9 ON T1.CREATED_BY = T9.USERID)");
				sql.append(" LEFT OUTER JOIN V_ALLADDRESS_LIST T4 on T1.ADDR_ID = T4.ADDR_ID)");
				sql.append(" LEFT OUTER JOIN V_ADDRESS_LIST VAL ON T4.ADDR_ID = VAL.ADDR_ID)");
				sql.append(" LEFT OUTER JOIN V_LSO_USE VLU ON T1.LSO_ID = VLU.LSO_ID");
				sql.append(" LEFT OUTER JOIN BL_ACTIVITY BL ON BL.ACT_ID = T1.ACT_ID");
				sql.append(" LEFT OUTER JOIN ACT_SUBTYPE AST ON T1.ACT_ID = AST.ACT_ID");
				sql.append(" LEFT OUTER JOIN LKUP_ACT_SUBTYPE LAS ON AST.ACT_SUBTYPE_ID = LAS.ACT_SUBTYPE_ID");
				sql.append(" WHERE T1.ACT_TYPE NOT LIKE 'PW%' AND T1.ACT_NBR = '" + actNbr + "'");

				logger.debug("Main Element for BLActivity : " + sql.toString());
			} else if (actNbr.trim().startsWith("BT")) {
				sql.append("SELECT T1.ACT_ID C1,T1.ACT_NBR C2,T1.VALUATION C3,T1.description C4,T1.APPLIED_DATE C5,");
				sql.append("T1.ISSUED_DATE C6,T1.EXP_DATE C7,T1.FINAL_DATE C8,T3.description C9, T4.DL_ADDRESS C10, (VAL.CITY || ' ' || VAL.STATE || ' ' || VAL.ZIP) C30,");
				sql.append("T1.PROJECT_NBR C11,SP.SPROJ_NBR C24, SP.SPROJ_ID C25, T1.NAME C12,T6.description C13,UPPER(T7.description) C14,");
				sql.append("CASE WHEN NOT T9.USERNAME IS NULL THEN T9.USERNAME ELSE '' END C15,T1.PROJ_ID C18,T1.PROJDESC c19,");
				sql.append("LSP.description C20,LSSP.description C21,VLU.DESCRIPTION C22,T1.ACT_TYPE C23,");
				sql.append("BT.BUSINESS_NAME C27, BT.BUSINESS_ACC_NO C28");
				sql.append(" FROM (((((((((V_ACTIVITY T1 JOIN LKUP_ACT_ST T3 ON T1.STATUS = T3.STATUS_ID)");
				sql.append(" JOIN LKUP_ACT_TYPE T6 ON T1.ACT_TYPE = T6.TYPE)");
				sql.append(" LEFT OUTER JOIN DEPARTMENT T7 ON T7.DEPT_CODE='LC')");
				sql.append(" JOIN SUB_PROJECT SP ON T1.SPROJ_ID = SP.SPROJ_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_SPROJ_TYPE LSP ON SP.STYPE_ID = LSP.SPROJ_TYPE_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_SPROJ_STYPE LSSP ON SP.SSTYPE_ID = LSSP.SPROJ_STYPE_ID)");
				sql.append(" LEFT OUTER JOIN USERS T9 ON T1.CREATED_BY = T9.USERID)");
				sql.append(" LEFT OUTER JOIN V_ALLADDRESS_LIST T4 on T1.ADDR_ID = T4.ADDR_ID)");
				sql.append(" LEFT OUTER JOIN V_ADDRESS_LIST VAL ON T4.ADDR_ID = VAL.ADDR_ID)");
				sql.append(" LEFT OUTER JOIN V_LSO_USE VLU ON T1.LSO_ID = VLU.LSO_ID");
				sql.append(" LEFT OUTER JOIN BT_ACTIVITY BT ON BT.ACT_ID = T1.ACT_ID");
				sql.append(" LEFT OUTER JOIN ACT_SUBTYPE AST ON T1.ACT_ID = AST.ACT_ID");
				sql.append(" LEFT OUTER JOIN LKUP_ACT_SUBTYPE LAS ON AST.ACT_SUBTYPE_ID = LAS.ACT_SUBTYPE_ID");
				sql.append(" WHERE T1.ACT_TYPE NOT LIKE 'PW%' AND T1.ACT_NBR = '" + actNbr + "'");

				logger.debug("Main Element for BTActivity : " + sql.toString());
			} else {
				sql.append("SELECT T1.ACT_ID C1,T1.ACT_NBR C2,T1.VALUATION C3,T1.description C4,T1.APPLIED_DATE C5,");
				sql.append("T1.ISSUED_DATE C6,T1.EXP_DATE C7,T1.FINAL_DATE C8,T3.description C9, T4.DL_ADDRESS C10,");
				sql.append("T1.PROJECT_NBR C11,SP.SPROJ_NBR C24, SP.SPROJ_ID C25, T1.NAME C12,T6.description C13,UPPER(T7.description) C14,");
				sql.append("CASE WHEN NOT T9.USERNAME IS NULL THEN T9.USERNAME ELSE '' END C15,T1.PROJ_ID C18,T1.PROJDESC c19,");
				sql.append("LSP.description C20,LSSP.description C21,VLU.DESCRIPTION C22,T1.ACT_TYPE C23,");
				sql.append("BL.BUSINESS_NAME C27, BL.BUSINESS_ACC_NO C28, LAS.ACT_SUBTYPE C29");
				sql.append(" FROM ((((((((V_ACTIVITY T1 JOIN LKUP_ACT_ST T3 ON T1.STATUS = T3.STATUS_ID)");
				sql.append(" JOIN LKUP_ACT_TYPE T6 ON T1.ACT_TYPE = T6.TYPE)");
				sql.append(" LEFT OUTER JOIN DEPARTMENT T7 ON SUBSTR(T1.ACT_NBR, 1 , 2) = T7.DEPT_CODE)");
				sql.append(" JOIN SUB_PROJECT SP ON T1.SPROJ_ID = SP.SPROJ_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_SPROJ_TYPE LSP ON SP.STYPE_ID = LSP.SPROJ_TYPE_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_SPROJ_STYPE LSSP ON SP.SSTYPE_ID = LSSP.SPROJ_STYPE_ID)");
				sql.append(" LEFT OUTER JOIN USERS T9 ON T1.CREATED_BY = T9.USERID)");
				sql.append(" LEFT OUTER JOIN V_ALLADDRESS_LIST T4 on T1.ADDR_ID = T4.ADDR_ID)");
				sql.append(" LEFT OUTER JOIN V_LSO_USE VLU ON T1.LSO_ID = VLU.LSO_ID");
				sql.append(" LEFT OUTER JOIN BL_ACTIVITY BL ON BL.ACT_ID = T1.ACT_ID");
				sql.append(" LEFT OUTER JOIN ACT_SUBTYPE AST ON T1.ACT_ID = AST.ACT_ID");
				sql.append(" LEFT OUTER JOIN LKUP_ACT_SUBTYPE LAS ON AST.ACT_SUBTYPE_ID = LAS.ACT_SUBTYPE_ID");
				sql.append(" WHERE T1.ACT_NBR = '" + actNbr + "'");

				logger.debug("Main Element : " + sql.toString());
			}

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql.toString());
			String c1 = "actid";
			String c2 = "actnbr";
			String c3 = "valuation";
			String c4 = "actdesc";
			String c5 = "applieddate";
			String c6 = "issueddate";
			String c7 = "expiredate";
			String c8 = "completeddate";
			String c9 = "actstatus";
			String c10 = "address";
			String c11 = "prjnbr";
			String c12 = "prjname";
			String c13 = "acttype";
			String c14 = "department";
			String c15 = "enteredby";
			String c16 = "prjdescription";
			String c17 = "rundate";
			String c20 = "stype";
			String c21 = "sstype";
			String c22 = "zonetype";
			String c23 = "acttypeid";
			String c24 = "sprojnbr";
			String c27 = "businessName";
			String c28 = "businessAccNo";
			String c29 = "imgPath1";
			String c30 = "cityStateZip";
			String c31 = "vehicleNo";

			Date curdate = new Date(); // to get current run date

			Element c1Element = new Element(c1);
			Element c2Element = new Element(c2);
			Element c3Element = new Element(c3);
			Element c4Element = new Element(c4);
			Element c5Element = new Element(c5);
			Element c6Element = new Element(c6);
			Element c7Element = new Element(c7);
			Element c8Element = new Element(c8);
			Element c9Element = new Element(c9);
			Element c10Element = new Element(c10);
			Element c11Element = new Element(c11);
			Element c12Element = new Element(c12);
			Element c13Element = new Element(c13);
			Element c14Element = new Element(c14);
			Element c15Element = new Element(c15);
			Element c16Element = new Element(c16);
			Element c17Element = new Element(c17);

			Element c20Element = new Element(c20);
			Element c21Element = new Element(c21);
			Element c22Element = new Element(c22);
			Element c23Element = new Element(c23);
			Element c24Element = new Element(c24);

			Element c27Element = new Element(c27);
			Element c28Element = new Element(c28);
			Element c29Element = new Element(c29);
			Element c31Element = new Element(c31);
			
			Element imgPathElement = new Element("imgPath");
			imgPathElement.addContent(this.imgPath);

			if (titleRs != null && titleRs.next()) {

				this.actId = titleRs.getInt("c1");
				this.projId = titleRs.getInt("c18");
				this.sprojId = titleRs.getInt("C25");
				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");

				c4 = titleRs.getString("C4");
				c5 = titleRs.getString("C5");
				c6 = titleRs.getString("C6");
				c7 = titleRs.getString("C7");
				c8 = titleRs.getString("C8");
				c9 = titleRs.getString("C9");

				c10 = titleRs.getString("C10");

				c11 = titleRs.getString("C11");
				c12 = titleRs.getString("C12");
				c13 = titleRs.getString("C13") != null && titleRs.getString("C13").equals(Constants.ONLINE_RESIDENTIAL_PERMIT) ? titleRs.getString("C29") : titleRs.getString("C13");
				c14 = titleRs.getString("C14");
				c15 = titleRs.getString("C15");
				c16 = titleRs.getString("C19");
				c17 = StringUtils.date2str(curdate);
				c20 = titleRs.getString("C20");
				c21 = titleRs.getString("C21");
				c22 = titleRs.getString("C22");
				c23 = titleRs.getString("C23");
				c24 = titleRs.getString("C24");
				if ((titleRs.getString("C27")) != null) {
					c27 = titleRs.getString("C27").toUpperCase();
				}
				c28 = titleRs.getString("C28");
				if (actNbr.trim().startsWith("BL") || actNbr.trim().startsWith("BT")) {
					c30 = titleRs.getString("C30");
				} else {
					c30 = "";
				}
				c31 = new ActivityAgent().getLncvVehicleNo(actNbr);
				logger.debug("actid is " + c1);
				logger.debug("actnbr  is " + c2);
				logger.debug("valuation  is " + c3);
				logger.debug("actdesc is " + c4);
				logger.debug("applieddate  is " + c5);
				logger.debug("issueddate  is " + c6);
				logger.debug("expiredate  is " + c7);
				logger.debug("completeddate  is " + c8);
				logger.debug("actstatus is " + c9);
				logger.debug("address  is " + c10);
				logger.debug("prjnbr  is " + c11);
				logger.debug("prjname  is " + c12);
				logger.debug("acttype is " + c13);
				logger.debug("department  is " + c14);
				logger.debug("enteredby  is " + c15);
				logger.debug("project description is " + c16);
				logger.debug("rundate  is " + c17);
				logger.debug("Sub Project Sub Type  is " + c20);
				logger.debug("Sub Project Sub Sub Type  is" + c21);
				logger.debug("Zone type  is " + c22);
				logger.debug("Act type  is " + c23);
				logger.debug("sub project number is" + c24);

				logger.debug("Business Name  is " + c27);
				logger.debug("Business Account Number is" + c28);
				logger.debug("imgFile Path ::" + c29);
				logger.debug("imgFile Path ::" + this.imgPath);
				logger.debug("cityStateZip is :" + c30);
				logger.debug("vehicleNo is :" + c31);

				if (c10 != null) {
					if ((c10.trim().toUpperCase()).endsWith("CITYWIDE")) {

						// means it is out of town address
						String sql1 = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BL_ACTIVITY WHERE ACT_ID=" + c1;
						logger.debug(sql);

						RowSet ootRs = new Wrapper().select(sql1.toString());

						if (ootRs.next()) {
							c10 = ootRs.getString(1) + " " + ootRs.getString(2);
							logger.debug("OOT city state zip is " + c10);
						}
						c10Element.addContent(c10);
					} else {
						c10Element.addContent(c10 + " " + c30);
					}
				} else {
					logger.debug("address (c10) is null, finding non locational address");
					c10 = "Non Locational";
				}

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);
				c8Element.addContent(c8);
				c9Element.addContent(c9);

				c11Element.addContent(c11);
				c12Element.addContent(c12);
				c13Element.addContent(c13);
				c14Element.addContent(c14);
				c15Element.addContent(c15);
				c16Element.addContent(c16);
				c17Element.addContent(c17);

				c20Element.addContent(c20);
				c21Element.addContent(c21);
				c22Element.addContent(c22);
				c23Element.addContent(c23);
				c24Element.addContent(c24);

				c27Element.addContent(c27);
				c28Element.addContent(c28);
				c29Element.addContent(c29);
				c31Element.addContent(c31);

				mainElement.addContent(c1Element);
				mainElement.addContent(c2Element);
				mainElement.addContent(c3Element);
				mainElement.addContent(c4Element);
				mainElement.addContent(c5Element);
				mainElement.addContent(c6Element);
				mainElement.addContent(c7Element);
				mainElement.addContent(c8Element);
				mainElement.addContent(c9Element);
				mainElement.addContent(c10Element);
				mainElement.addContent(c11Element);
				mainElement.addContent(c12Element);
				mainElement.addContent(c13Element);
				mainElement.addContent(c14Element);
				mainElement.addContent(c15Element);
				mainElement.addContent(c16Element);
				mainElement.addContent(c17Element);
				mainElement.addContent(c20Element);
				mainElement.addContent(c21Element);
				mainElement.addContent(c22Element);
				mainElement.addContent(c23Element);
				mainElement.addContent(c24Element);

				mainElement.addContent(c27Element);
				mainElement.addContent(c28Element);
				mainElement.addContent(c29Element);
				mainElement.addContent(c31Element);

				// get parcel element,pcstatus Element and add to main element
				mainElement.addContent(getParcelElement(actId));
				mainElement.addContent(getPCStatusElement(actId));
				mainElement.addContent(imgPathElement);

				logger.debug("main ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getMainElement() method " + e.getMessage());
		}

		return mainElement;
	}

	public Element getBusinessTaxContractors(int actId, int peopleId) {
		Wrapper db = new Wrapper();
		Element contractors = new Element("contractors");
		StringBuffer sql = new StringBuffer();

		sql.append("SELECT P.PEOPLE_ID,P.LIC_NO,P.NAME,AF.COMMENTS,AF.FEE_VALUATION,AF.FEE_AMNT,AF.FEE_PAID,AF.FEE_AMNT - AF.FEE_PAID BALANCE");
		sql.append(" FROM ACTIVITY_FEE AF JOIN PEOPLE P ON AF.PEOPLE_ID = P.PEOPLE_ID AND AF.FEE_ID = 0 AND AF.ACTIVITY_ID =" + actId);

		if (peopleId > 0) {
			sql.append(" WHERE P.PEOPLE_ID = " + peopleId);
		}

		sql.append(" ORDER BY AF.FEE_VALUATION desc");
		logger.debug("CONTRACTOR BUS TAX  FEES sql : " + sql.toString());

		try {
			RowSet rs = db.select(sql.toString());
			double taxSum = 0.00;
			double paidSum = 0.00;
			double dueSum = 0.00;

			while (rs.next()) {
				Element contractor = new Element("contractor");
				String licenseNumber = (rs.getString("LIC_NO") == null) ? "" : rs.getString("LIC_NO");
				contractor.addContent(new Element("licno").addContent(licenseNumber));

				String name = (rs.getString("NAME") == null) ? "" : rs.getString("NAME");
				contractor.addContent(new Element("name").addContent(name));
				contractor.addContent(new Element("valuation").addContent(rs.getString("FEE_VALUATION")));
				contractor.addContent(new Element("tax").addContent(rs.getString("FEE_AMNT")));
				contractor.addContent(new Element("paid").addContent(rs.getString("FEE_PAID")));
				contractor.addContent(new Element("due").addContent(rs.getString("BALANCE")));

				String feeComment = (rs.getString("COMMENTS") == null) ? "" : rs.getString("COMMENTS");
				contractor.addContent(new Element("workdesc").addContent(feeComment));

				// Sum Amounts
				taxSum = taxSum + rs.getDouble("FEE_AMNT");
				paidSum = paidSum + rs.getDouble("FEE_PAID");
				dueSum = dueSum + rs.getDouble("BALANCE");

				sql = new StringBuffer();
				sql.append("SELECT PD.TRANS_ID,P.PYMNT_DT,P.PYMNT_METHOD,PD.AMNT,LPT.DESCRIPTION TYPE");
				sql.append(" FROM PAYMENT P JOIN PAYMENT_DETAIL PD ON P.PYMNT_ID = PD.PYMNT_ID");
				sql.append(" JOIN LKUP_PAY_TYPE LPT ON P.PYMNT_TYPE = LPT.PAY_TYPE_ID");
				sql.append(" WHERE PD.FEE_ID = 0 AND PD.ACT_ID = " + actId + " AND PD.PEOPLE_ID = " + rs.getInt("PEOPLE_ID"));
				logger.debug("CONTRACTOR BUS TAX PAYMENTS sql : " + sql.toString());

				RowSet rsPayments = db.select(sql.toString());
				Element transactions = new Element("transactions");

				while (rsPayments.next()) {
					Element transaction = new Element("transaction");
					transaction.addContent(new Element("id").addContent(rsPayments.getString("TRANS_ID")));

					String date = (rsPayments.getDate("PYMNT_DT") == null) ? "" : StringUtils.date2str(rsPayments.getDate("PYMNT_DT"));
					transaction.addContent(new Element("date").addContent(date));

					String type = (rsPayments.getString("TYPE") == null) ? "" : rsPayments.getString("TYPE");
					transaction.addContent(new Element("type").addContent(type));

					String method = (rsPayments.getString("PYMNT_METHOD") == null) ? "" : rsPayments.getString("PYMNT_METHOD");
					transaction.addContent(new Element("method").addContent(method));
					transaction.addContent(new Element("amount").addContent(StringUtils.dbl2$(rsPayments.getDouble("AMNT"))));
					transactions.addContent(transaction);
				}

				if (rsPayments != null) {
					rsPayments.close();
				}

				contractor.addContent(transactions);
				contractors.addContent(contractor);
			}

			if (rs != null) {
				rs.close();
			}

			contractors.addContent(new Element("taxsum").addContent(StringUtils.dbl2$(taxSum)));
			contractors.addContent(new Element("paidsum").addContent(StringUtils.dbl2$(paidSum)));
			contractors.addContent(new Element("duesum").addContent(StringUtils.dbl2$(dueSum)));
		} catch (SQLException e) {
			logger.error("SQL Error in getBusinessTaxContractors()");
		} catch (Exception e) {
			logger.error("Error in getBusinessTaxContractors()");
		}

		return contractors;
	}

	public Element getPCStatusElement(int actid) {
		StringBuffer sql = new StringBuffer();
		String pcStatus = "";
		sql.append("SELECT PC_DESC FROM LKUP_PC_STATUS A JOIN PLAN_CHECK B ON A.PC_CODE = B.STAT_CODE");
		sql.append(" WHERE PC_CODE = (SELECT STAT_CODE FROM PLAN_CHECK WHERE ACT_ID = " + actId);
		sql.append(" AND CREATED = (SELECT MAX(CREATED) FROM PLAN_CHECK WHERE ACT_ID = " + actId + "))");
		logger.debug("PC STATUS : " + sql.toString());

		try {
			RowSet rs = new Wrapper().select(sql.toString());

			if (rs.next()) {
				pcStatus = rs.getString("PC_DESC");
			}
		} catch (SQLException e) {
			logger.error("SQL Error in getPcStatus");
		} catch (Exception e) {
			logger.error("Error in getPcStatus");
		}

		return new Element("pcstatus").addContent(pcStatus);
	}

	public Element getFeesGrpElement(int actId) {
		Element feesGrpElement = new Element("feesgrp");

		try {
			logger.debug("entered into PublicWorksReportAgent.getFeesGrpElement");

			String sql = "select T1.FEE_AMNT c1 , T1.FEE_PAID c2 , T1.FEE_UNITS c3 , T2.FEE_FACTOR c4 , T2.FEE_ACCOUNT c5 , T2.FEE_DESC c6 " + " FROM FEE T2 ,ACTIVITY_FEE T1 WHERE T2.FEE_ID = T1.FEE_ID AND T1.ACTIVITY_ID = " + actId + " AND T2.FEE_FL_4 in ('0','1','2','3','4') and FEE_FL_2 in ('Y','1') " + " ORDER BY T2.FEE_SEQUENCE ASC ";
			logger.debug("Fees : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "amount";
			String c2 = "paid";
			String c3 = "units";
			String c4 = "feeperunits";
			String c5 = "account";
			String c6 = "feedesc";

			Element c1Element = null;
			Element c2Element = null;
			Element c3Element = null;
			Element c4Element = null;
			Element c5Element = null;
			Element c6Element = null;

			while (titleRs.next()) {
				Element feesElement = new Element("fees");
				c1Element = new Element("amount");
				c2Element = new Element("paid");
				c3Element = new Element("units");
				c4Element = new Element("feeperunits");
				c5Element = new Element("account");
				c6Element = new Element("feedesc");

				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4");
				c5 = titleRs.getString("C5");
				c6 = titleRs.getString("C6");

				logger.debug("amount is " + c1);
				logger.debug("paid  is " + c2);
				logger.debug("units  is " + c3);
				logger.debug("feeperunits is " + c4);
				logger.debug("account  is " + c5);
				logger.debug("feedesc  is " + c6);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);

				feesElement.addContent(c1Element);
				feesElement.addContent(c2Element);
				feesElement.addContent(c3Element);
				feesElement.addContent(c4Element);
				feesElement.addContent(c5Element);
				feesElement.addContent(c6Element);

				feesGrpElement.addContent(feesElement);

				logger.debug("fees ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getFeesGrpElement() method " + e.getMessage());
		}

		return feesGrpElement;
	}

	public static Element getLedgerGrpElement(int actId) {
		return getLedgerGrpElement(actId, "");
	}

	public static Element getLedgerGrpElement(int actId, String reportType) {
		if (reportType == null) {
			reportType = "";
		}

		Element ledgerGrpElement = new Element("ledgergrp");

		try {
			logger.debug("entered into PublicWorksReportAgent.getLedgerGrpElement");

			StringBuffer sql = new StringBuffer();

			if (reportType.equals("FIR")) {
				sql.append("SELECT T1.PYMNT_DT c1,T1.PYMNT_METHOD c2,T2.AMNT c3,T3.DESCRIPTION c4");
				sql.append(" FROM  ((PAYMENT T1 LEFT OUTER JOIN PAYMENT_DETAIL T2 on T1.PYMNT_ID = T2.PYMNT_ID)");
				sql.append(" LEFT OUTER JOIN LKUP_PAY_TYPE T3 on T1.PYMNT_TYPE = T3.PAY_TYPE_ID)");
				sql.append(" WHERE T2.ACT_ID = " + actId + " order by c4 asc  ");
			} else {
				sql.append("SELECT T1.PYMNT_DT c1,T1.PYMNT_METHOD c2,T1.PYMNT_AMNT c3,T2.DESCRIPTION c4");
				sql.append(" FROM PAYMENT T1 LEFT OUTER JOIN LKUP_PAY_TYPE T2 ON T1.PYMNT_TYPE = T2.PAY_TYPE_ID");
				sql.append(" WHERE PYMNT_ID IN (SELECT DISTINCT PYMNT_ID FROM PAYMENT_DETAIL WHERE ACT_ID =" + actId + " AND FEE_ID > 0)");
			}

			logger.debug(" Common Ledger : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql.toString());

			String c1 = "pymntdate";
			String c2 = "pymntmethod";
			String c3 = "pymntamnt";
			String c4 = "pymntdesc";

			Element c1Element = null;
			Element c2Element = null;
			Element c3Element = null;
			Element c4Element = null;

			while (titleRs.next()) {
				Element ledgerElement = new Element("ledger");
				c1Element = new Element("pymntdate");
				c2Element = new Element("pymntmethod");
				c3Element = new Element("pymntamnt");
				c4Element = new Element("pymntdesc");

				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4");

				logger.debug("pymntdate is " + c1);
				logger.debug("pymntmethod  is " + c2);
				logger.debug("pymntamnt  is " + c3);
				logger.debug("pymntdesc is " + c4);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);

				ledgerElement.addContent(c1Element);
				ledgerElement.addContent(c2Element);
				ledgerElement.addContent(c3Element);
				ledgerElement.addContent(c4Element);

				ledgerGrpElement.addContent(ledgerElement);

				logger.debug("ledger ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getLedgerGrpElement() method " + e.getMessage());
		}

		return ledgerGrpElement;
	}

	public Element getPermitFeesElement(int actId) {
		Element permitfeesElement = new Element("permitfees");

		try {
			logger.debug("entered into PublicWorksReportAgent.getPermitfeesElement");

			String sql = "select sum(T1.FEE_AMNT) c1 , sum(T1.FEE_PAID) c2 , sum(T1.FEE_AMNT) - sum(T1.FEE_PAID) c3  from FEE T2, ACTIVITY_FEE T1  where T2.FEE_ID = T1.FEE_ID and T2.FEE_FL_4 in ('2') and T1.ACTIVITY_ID = " + actId + " ";
			logger.debug("Permit Fees in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "fees";
			String c2 = "payments";
			String c3 = "balance";

			Element c1Element = new Element(c1);
			Element c2Element = new Element(c2);
			Element c3Element = new Element(c3);

			while (titleRs.next()) {
				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");

				logger.debug("fees is " + c1);
				logger.debug("payments  is " + c2);
				logger.debug("balance  is " + c3);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);

				permitfeesElement.addContent(c1Element);
				permitfeesElement.addContent(c2Element);
				permitfeesElement.addContent(c3Element);

				logger.debug("permitfees ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getpermitfeesElement() method " + e.getMessage());
		}

		return permitfeesElement;
	}

	public Element getPlancheckFeesElement(int actId) {
		Element plancheckfeesElement = new Element("plancheckfees");

		try {
			logger.debug("entered into PublicWorksReportAgent.getPlancheckfeesElement");

			String sql = "select sum(T1.FEE_AMNT) c1 , sum(T1.FEE_PAID) c2 , sum(T1.FEE_AMNT) - sum(T1.FEE_PAID) c3  from FEE T2, ACTIVITY_FEE T1  where T2.FEE_ID = T1.FEE_ID and T2.FEE_FL_4 in ('1') and T1.ACTIVITY_ID = " + actId + "  ";
			logger.debug("PlanCheck Fees in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "fees";
			String c2 = "payments";
			String c3 = "balance";

			Element c1Element = new Element(c1);
			Element c2Element = new Element(c2);
			Element c3Element = new Element(c3);

			while (titleRs.next()) {
				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");

				logger.debug("fees is " + c1);
				logger.debug("payments  is " + c2);
				logger.debug("balance  is " + c3);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);

				plancheckfeesElement.addContent(c1Element);
				plancheckfeesElement.addContent(c2Element);
				plancheckfeesElement.addContent(c3Element);

				logger.debug("plancheckfees ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getplancheckfeesElement() method " + e.getMessage());
		}

		return plancheckfeesElement;
	}

	public Element getDevelopmentFeesElement(int actId) {
		Element developmentFeesElement = new Element("developmentfees");

		try {
			logger.debug("entered into PublicWorksReportAgent.getDevelopmentFeesElement");

			String sql = "select sum(T1.FEE_AMNT) c1 , sum(T1.FEE_PAID) c2 , sum(T1.FEE_AMNT) - sum(T1.FEE_PAID) c3  from FEE T2, ACTIVITY_FEE T1  where T2.FEE_ID = T1.FEE_ID and T2.FEE_FL_4 in ('3') and T1.ACTIVITY_ID = " + actId + "  ";
			logger.debug("Development Fees in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "fees";
			String c2 = "payments";
			String c3 = "balance";

			Element c1Element = new Element(c1);
			Element c2Element = new Element(c2);
			Element c3Element = new Element(c3);

			while (titleRs.next()) {
				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");

				logger.debug("fees is " + c1);
				logger.debug("payments  is " + c2);
				logger.debug("balance  is " + c3);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);

				developmentFeesElement.addContent(c1Element);
				developmentFeesElement.addContent(c2Element);
				developmentFeesElement.addContent(c3Element);

				logger.debug("DevelopmentFees ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getDevelopmentFeesElement() method " + e.getMessage());
		}

		return developmentFeesElement;
	}

	public Element getTotalFeesElement(int actId) {
		Element totalfeesElement = new Element("totalfees");

		try {
			logger.debug("entered into PublicWorksReportAgent.getTotalfeesElement");

			String sql = "select sum(T1.FEE_AMNT) c1 , sum(T1.FEE_PAID) c2 , sum(T1.FEE_AMNT) - sum(T1.FEE_PAID) c3  from FEE T2, ACTIVITY_FEE T1  where T2.FEE_ID = T1.FEE_ID and T2.FEE_FL_4 in ('1', '2','3','4') and T1.ACTIVITY_ID = " + actId + "  ";
			logger.debug("Total Fees in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "fees";
			String c2 = "payments";
			String c3 = "balance";

			Element c1Element = new Element(c1);
			Element c2Element = new Element(c2);
			Element c3Element = new Element(c3);

			while (titleRs.next()) {
				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");

				logger.debug("fees is " + c1);
				logger.debug("payments  is " + c2);
				logger.debug("balance  is " + c3);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);

				totalfeesElement.addContent(c1Element);
				totalfeesElement.addContent(c2Element);
				totalfeesElement.addContent(c3Element);

				logger.debug("totalfees ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getTotalfeesElement() method " + e.getMessage());
		}

		return totalfeesElement;
	}

	/**
	 * Applicant group element
	 * 
	 * @param actId
	 * @return
	 */
	public Element getApplicantGrpElement(int actId) {
		logger.info("getApplicantGrpElement(" + actId + ")");

		Element applicantGrpElement = new Element("applicantgrp");

		try {
			String sql = "select T1.NAME c1 , case  when  NOT T1.ADDR is null then T1.ADDR else ' ' end  || ' ' || case  when  NOT T1.CITY is null then T1.CITY else ' ' end  || ' ' || case  when  NOT T1.STATE is null then T1.STATE else ' ' end  || ' ' || case  when  NOT T1.ZIP is null then T1.ZIP else ' ' end  c2 , T1.PHONE c3  from V_ACTIVITY_PEOPLE T1  where T1.PEOPLE_TYPE_ID in (" + Constants.PEOPLE_APPLICANT + ") and T1.ACT_ID = " + actId + " order by c1 asc  ";
			logger.debug("Applicant Fees in Ledger : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String phone = "";

			Element nameElement = null;
			Element addrElement = null;
			Element phoneElement = null;

			while (titleRs.next()) {
				Element applicantElement = new Element("applicant");
				name = titleRs.getString("C1");
				logger.debug("name is " + name);
				addr = titleRs.getString("C2");
				logger.debug("addr is " + addr);
				phone = titleRs.getString("C3");
				logger.debug("phone is " + phone);

				nameElement = new Element("name");
				addrElement = new Element("addr");
				phoneElement = new Element("phone");

				logger.debug("applicant ELEMENT - Created ");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				phoneElement.addContent(phone);

				applicantElement.addContent(nameElement);
				applicantElement.addContent(addrElement);
				applicantElement.addContent(phoneElement);

				applicantGrpElement.addContent(applicantElement);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getApplicantGrpElement() method " + e.getMessage());
		}

		return applicantGrpElement;
	}

	/**
	 * People group element
	 * 
	 * @param actId
	 * @return
	 */
	public Element getPeopleGrpElement(int actId) {
		logger.info("getPeopleGrpElement(" + actId + ")");

		Element peopleGrpElement = new Element("peoplegrp");

		try {
			String sql = "select T1.NAME c1 , case  when  NOT T1.ADDR is null then T1.ADDR else ' ' end  || ' ' || case  when  NOT T1.CITY is null then T1.CITY else ' ' end  || ' ' || case  when  NOT T1.STATE is null then T1.STATE else ' ' end  || ' ' || case  when  NOT T1.ZIP is null then T1.ZIP else ' ' end  c2 , T1.PHONE c3 , pt.description c4 , T1.COMNTS c5 , T1.LIC_NO c6   from (ACTIVITY T2 LEFT OUTER JOIN V_ACTIVITY_PEOPLE T1 on T2.ACT_ID = T1.ACT_ID left outer join people_type pt on pt.people_type_id=t1.people_type_id) where T1.PSA_TYPE='A' and T1.ACT_ID = " + actId + " order by c1 asc ";
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String phone = "";
			String peopleType = "";
			String comments = "";
			String licNo = "";

			Element peopleElement = null;
			Element nameElement = null;
			Element addrElement = null;
			Element phoneElement = null;
			Element peopleTypeElement = null;
			Element commentsElement = null;
			Element licNoElement = null;

			while (titleRs.next()) {
				peopleElement = new Element("people");

				name = titleRs.getString("C1");
				logger.debug("name is " + name);
				addr = titleRs.getString("C2");
				logger.debug("addr is " + addr);
				phone = titleRs.getString("C3");
				logger.debug("phone is " + phone);
				peopleType = titleRs.getString("C4");
				logger.debug("peopletype is " + peopleType);
				comments = titleRs.getString("C5");
				logger.debug("comments is " + comments);
				licNo = titleRs.getString("C6");
				logger.debug("licNo is " + licNo);

				nameElement = new Element("name");
				addrElement = new Element("addr");
				phoneElement = new Element("phone");
				peopleTypeElement = new Element("peopletype");
				commentsElement = new Element("comments");
				licNoElement = new Element("licno");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				phoneElement.addContent(phone);
				peopleTypeElement.addContent(peopleType);
				commentsElement.addContent(comments);
				licNoElement.addContent(licNo);

				peopleElement.addContent(nameElement);
				peopleElement.addContent(addrElement);
				peopleElement.addContent(phoneElement);
				peopleElement.addContent(peopleTypeElement);
				peopleElement.addContent(commentsElement);
				peopleElement.addContent(licNoElement);

				logger.debug("people ELEMENT - Created ");

				peopleGrpElement.addContent(peopleElement);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in getPeopleGrpElement() method " + e.getMessage());
		}

		return peopleGrpElement;
	}

	public Element getContractorElement(int actId) {
		Element contractorElement = new Element("contractor");

		try {
			logger.debug("entered into PublicWorksReportAgent.getContractorElement");

			String sql = "select T1.NAME c1 , case  when  NOT T1.ADDR is null then T1.ADDR else ' ' end  || ' ' || case  when  NOT T1.CITY is null then T1.CITY else ' ' end  || ' ' || case  when  NOT T1.STATE is null then T1.STATE else ' ' end  || ' ' || case  when  NOT T1.ZIP is null then T1.ZIP else ' ' end  c2 , T1.PHONE c3 , case  when T1.PEOPLE_TYPE_ID = " + Constants.PEOPLE_CONTRACTOR + " then 'CONTRACTOR' when T1.PEOPLE_TYPE_ID = " + Constants.PEOPLE_ENGINEER + " then 'ENGINEER' when T1.PEOPLE_TYPE_ID = " + Constants.PEOPLE_ARCHITECT + " then 'ARCHITECT' when T1.PEOPLE_TYPE_ID = " + Constants.PEOPLE_DESIGNER + " then 'DESIGNER' else NULL end  c4 , T1.COMNTS c5 , T1.LIC_NO c6   from (ACTIVITY T2 LEFT OUTER JOIN V_ACTIVITY_PEOPLE T1 on T2.ACT_ID = T1.ACT_ID)   where T1.PEOPLE_TYPE_ID in (" + Constants.PEOPLE_CONTRACTOR + ") and T1.ACT_ID = " + actId + " order by c1 asc  ";
			logger.debug("Contractor in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String phone = "";
			String peopleType = "";
			String comments = "";
			String licNo = "";

			// Element contractorElement = null;
			Element nameElement = null;
			Element addrElement = null;
			Element phoneElement = null;
			Element peopleTypeElement = null;
			Element commentsElement = null;
			Element licNoElement = null;

			while (titleRs.next()) {
				name = titleRs.getString("C1");
				logger.debug("name is " + name);
				addr = titleRs.getString("C2");
				logger.debug("addr is " + addr);
				phone = titleRs.getString("C3");
				logger.debug("phone is " + phone);
				peopleType = titleRs.getString("C4");
				logger.debug("peopletype is " + peopleType);
				comments = titleRs.getString("C5");
				logger.debug("comments is " + comments);
				licNo = titleRs.getString("C6");
				logger.debug("licNo is " + licNo);

				nameElement = new Element("name");
				addrElement = new Element("addr");
				phoneElement = new Element("phone");
				peopleTypeElement = new Element("peopletype");
				commentsElement = new Element("comments");
				licNoElement = new Element("licno");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				phoneElement.addContent(phone);
				peopleTypeElement.addContent(peopleType);
				commentsElement.addContent(comments);
				licNoElement.addContent(licNo);

				contractorElement.addContent(nameElement);
				contractorElement.addContent(addrElement);
				contractorElement.addContent(phoneElement);
				contractorElement.addContent(peopleTypeElement);
				contractorElement.addContent(commentsElement);
				contractorElement.addContent(licNoElement);

				logger.debug("contractor ELEMENT - Created ");

				// peopleGrpElement.addContent(peopleElement);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getContractorElement() method " + e.getMessage());
		}

		return contractorElement;
	}

	public Element getPeopleFIGrpElement(int actId) {
		logger.info("getPeopleFIGrpElement(" + actId + ")");

		Element peopleGrpElement = new Element("peoplegrp");

		try {
			// modified to get all people type, but only to the particular
			// activity, not the whole project
			String sql = "select  T1.NAME c1 , case  when  NOT T1.ADDR is null then T1.ADDR else ' ' end  || ' ' || case  when  NOT T1.CITY is null then T1.CITY else ' ' end  || ' ' || case  when  NOT T1.STATE is null then T1.STATE else ' ' end  || ' ' || case  when  NOT T1.ZIP is null then T1.ZIP else ' ' end  c2 , T1.PHONE c3 , T.description c4, " + "T1.COMNTS c5 , T1.LIC_NO c6 ,T2.ACT_NBR c7,T3.description c8  " + "from ((V_ACTIVITY T2 LEFT OUTER JOIN LKUP_ACT_TYPE T3 ON T3.TYPE = T2.ACT_TYPE) " + "LEFT OUTER JOIN V_ACTIVITY_PEOPLE T1 on T2.ACT_ID = T1.ACT_ID) " + "LEFT OUTER JOIN PEOPLE_TYPE T on T1.PEOPLE_TYPE_ID=T.PEOPLE_TYPE_ID  " + "where T1.PSA_TYPE='A' and T2.ACT_ID=" + actId + " order by c1 asc  ";

			logger.debug("People in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String phone = "";
			String peopleType = "";
			String comments = "";
			String licNo = "";
			String actNbr = "";
			String actType = "";

			Element peopleElement = null;
			Element nameElement = null;
			Element addrElement = null;
			Element phoneElement = null;
			Element peopleTypeElement = null;
			Element commentsElement = null;
			Element licNoElement = null;
			Element actNbrElement = null;
			Element actTypeElement = null;

			while (titleRs.next()) {
				peopleElement = new Element("people");

				name = titleRs.getString("C1");
				logger.debug("name is " + name);
				addr = titleRs.getString("C2");
				logger.debug("addr is " + addr);
				phone = titleRs.getString("C3");
				logger.debug("phone is " + phone);
				peopleType = titleRs.getString("C4");
				logger.debug("peopletype is " + peopleType);
				comments = titleRs.getString("C5");
				logger.debug("comments is " + comments);
				licNo = titleRs.getString("C6");
				logger.debug("licNo is " + licNo);
				actNbr = titleRs.getString("C7");
				logger.debug("actNbr is " + actNbr);
				actType = titleRs.getString("C8");
				logger.debug("actType is " + actType);

				nameElement = new Element("name");
				addrElement = new Element("addr");
				phoneElement = new Element("phone");
				peopleTypeElement = new Element("peopletype");
				commentsElement = new Element("comments");
				licNoElement = new Element("licno");
				actNbrElement = new Element("actnbr");
				actTypeElement = new Element("acttype");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				phoneElement.addContent(phone);
				peopleTypeElement.addContent(peopleType);
				commentsElement.addContent(comments);
				licNoElement.addContent(licNo);
				actNbrElement.addContent(actNbr);
				actTypeElement.addContent(actType);

				peopleElement.addContent(nameElement);
				peopleElement.addContent(addrElement);
				peopleElement.addContent(phoneElement);
				peopleElement.addContent(peopleTypeElement);
				peopleElement.addContent(commentsElement);
				peopleElement.addContent(licNoElement);
				peopleElement.addContent(actNbrElement);
				peopleElement.addContent(actTypeElement);

				logger.debug("people ELEMENT - Created ");

				peopleGrpElement.addContent(peopleElement);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getPeopleGrpElement() method " + e.getMessage());
		}

		return peopleGrpElement;
	}

	public Element getOwnerElement(int actId) {
		logger.info("getOwnerElement(" + actId + ")");

		Element ownerElement = new Element("owner");

		try {
			String sql = "select NAME,ADDR,PHONE from V_ACTIVITY_PEOPLE where V_ACTIVITY_PEOPLE.PEOPLE_TYPE_ID =" + Constants.PEOPLE_OWNER + " and ACT_ID = " + actId;
			logger.debug(sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String phone = "";

			Element nameElement = null;
			Element addrElement = null;
			Element phoneElement = null;

			while (titleRs.next()) {
				name = titleRs.getString("NAME");
				logger.debug("name is " + name);
				addr = titleRs.getString("ADDR");
				logger.debug("addr is " + addr);
				phone = titleRs.getString("PHONE");
				logger.debug("phone is " + phone);

				nameElement = new Element("name");
				addrElement = new Element("addr");
				phoneElement = new Element("phone");

				logger.debug("owner ELEMENT - Created ");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				phoneElement.addContent(phone);

				ownerElement.addContent(nameElement);
				ownerElement.addContent(addrElement);
				ownerElement.addContent(phoneElement);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getOwnerElement() method " + e.getMessage());
		}

		return ownerElement;
	}

	public Element getApplicantElement(int actId) {
		Element applicantElement = new Element("applicant");

		try {
			logger.debug("entered into PublicWorksReportAgent.getApplicantElement");

			String sql = "select T1.NAME c1 , case  when  NOT T1.ADDR is null then T1.ADDR else ' ' end  || ' ' || case  when  NOT T1.CITY is null then T1.CITY else ' ' end  || ' ' || case  when  NOT T1.STATE is null then T1.STATE else ' ' end  || ' ' || case  when  NOT T1.ZIP is null then T1.ZIP else ' ' end  c2 , T1.PHONE c3  from V_ACTIVITY_PEOPLE T1  where T1.PEOPLE_TYPE_ID " + Constants.PEOPLE_APPLICANT + " and T1.ACT_ID = " + actId + " order by c1 asc   ";
			logger.debug("Applicant in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String phone = "";

			Element nameElement = null;
			Element addrElement = null;
			Element phoneElement = null;

			while (titleRs.next()) {
				// Element applicantElement = new Element("applicant");
				name = titleRs.getString("C1");
				logger.debug("name is " + name);
				addr = titleRs.getString("C2");
				logger.debug("addr is " + addr);
				phone = titleRs.getString("C3");
				logger.debug("phone is " + phone);

				nameElement = new Element("name");
				addrElement = new Element("addr");
				phoneElement = new Element("phone");

				logger.debug("applicant ELEMENT - Created ");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				phoneElement.addContent(phone);

				applicantElement.addContent(nameElement);
				applicantElement.addContent(addrElement);
				applicantElement.addContent(phoneElement);

				// applicantElement.addContent(applicantElement);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getApplicantElement() method " + e.getMessage());
		}

		return applicantElement;
	}

	public Element getActiveHoldsForFIR(int actId) throws Exception {

		Wrapper db = new Wrapper();

		String level = "A";
		int levelId = actId;

		String levelIds = "";
		if (level == null) {
			level = "";
		}

		String sql = "select a.CREATION_DT,a.HOLD_TYPE,u.username as CREATED_BY,a.UPDATE_DT,a.description from holds a left outer join users u on a.created_by=u.userid where ";

		String sql1 = "select distinct vll.*,vpl.proj_id,vpl.sproj_id from v_lso_land vll,v_psa_list vpl where vll.lso_id=vpl.lso_id and vpl.act_id=" + levelId;
		RowSet rs1 = db.select(sql1);

		if (rs1.next()) {
			char lsoType = 'X';
			char psaLevel = 'X';

			try {
				lsoType = rs1.getString("LSO_TYPE").charAt(0);
				psaLevel = level.charAt(0);
			} catch (Exception e) {
				logger.debug("Error in converting level,type to char");
			}

			StringBuffer selectFilter = new StringBuffer(" (");

			switch (lsoType) {
			case 'O':
				selectFilter.append("(level_id=" + rs1.getString("LSO_ID") + " and hold_level='O') or ");

			case 'S':
				selectFilter.append("(level_id=" + rs1.getString("STRUCTURE_ID") + " and hold_level='S') or ");

			case 'L':
				selectFilter.append("(level_id=" + rs1.getString("LAND_ID") + " and hold_level='L')");
			}

			switch (psaLevel) {
			case 'A':
				selectFilter.append(" or (level_id=" + levelId + " and hold_level='A')");

			case 'Q':
				selectFilter.append(" or (level_id=" + rs1.getString("SPROJ_ID") + " and hold_level='Q')");

			case 'P':
				selectFilter.append(" or (level_id=" + rs1.getString("PROJ_ID") + " and hold_level='P'))");
			}

			levelIds = selectFilter.toString();
			rs1.close();
		}

		sql = sql + levelIds;
		sql = sql + " and stat = 'A' and a.update_dt = (select max(b.update_dt) from holds b where b.hold_id=a.hold_id )";
		logger.info(sql);

		Element activeHolds = new Element("activeholds");

		try {
			RowSet rs = new Wrapper().select(sql.toString());

			while (rs.next()) {
				Element activeHold = new Element("activehold");
				activeHold.addContent(new Element("created").addContent(StringUtils.date2str(rs.getDate("CREATION_DT"))));
				activeHold.addContent(new Element("type").addContent(rs.getString("HOLD_TYPE")));
				activeHold.addContent(new Element("issuer").addContent(rs.getString("CREATED_BY")));
				activeHold.addContent(new Element("status").addContent("Active"));
				activeHold.addContent(new Element("statusdate").addContent(StringUtils.date2str(rs.getDate("UPDATE_DT"))));
				activeHold.addContent(new Element("description").addContent(rs.getString("description")));
				activeHolds.addContent(activeHold);
			}
		} catch (Exception e) {
			throw e;
		}

		return activeHolds;
	}

	/**
	 * Get business tax group element.
	 * 
	 * @param projectId
	 * @return
	 */
	public Element getBusTaxGrpElement(int activityId) {
		Element bustaxGrpElement = new Element("bustaxgrp");

		try {
			logger.debug("entered into getBusTaxGrpElement");

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT T4.LIC_NO c1,T4.NAME c2,T1.FEE_VALUATION c3,T1.FEE_AMNT c4,");
			sql.append("T1.FEE_PAID c5,T1.FEE_AMNT-T1.FEE_PAID c6,T2.ACT_NBR c7,T1.COMMENTS c8,");
			sql.append("CASE WHEN T2.VALUATION = T1.FEE_VALUATION THEN 'Y' ELSE 'N' END c9");
			sql.append(" FROM ((V_ACTIVITY T2 JOIN ACTIVITY_FEE T1 on T2.ACT_ID = T1.ACTIVITY_ID");
			sql.append(" AND T1.FEE_ID=0) JOIN PEOPLE T4 on T4.PEOPLE_ID=T1.PEOPLE_ID)");
			sql.append(" WHERE T2.ACT_ID = " + activityId + " ORDER BY c7,c9 ASC  ");

			logger.debug("BusTax in FIR : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql.toString());

			String c1 = "licno";
			String c2 = "name";
			String c3 = "valuation";
			String c4 = "tax";
			String c5 = "paid";
			String c6 = "due";
			String c7 = "permitno";
			String c8 = "workdesc";
			String c9 = "primary";

			Element c1Element = null;
			Element c2Element = null;
			Element c3Element = null;
			Element c4Element = null;
			Element c5Element = null;
			Element c6Element = null;
			Element c7Element = null;
			Element c8Element = null;
			Element c9Element = null;

			while (titleRs.next()) {
				Element bustaxElement = new Element("bustax");
				c1Element = new Element("licno");
				c2Element = new Element("name");
				c3Element = new Element("valuation");
				c4Element = new Element("tax");
				c5Element = new Element("paid");
				c6Element = new Element("due");
				c7Element = new Element("permitno");
				c8Element = new Element("workdesc");
				c9Element = new Element("primary");

				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4");
				c5 = titleRs.getString("C5");
				c6 = titleRs.getString("C6");
				c7 = titleRs.getString("C7");
				c8 = titleRs.getString("C8");
				c9 = titleRs.getString("C9");

				logger.debug("licno is " + c1);
				logger.debug("name is " + c2);
				logger.debug("valuation	 is " + c3);
				logger.debug("tax is " + c4);
				logger.debug("paid is " + c5);
				logger.debug("due is " + c6);
				logger.debug("permitno is " + c7);
				logger.debug("workdesc is " + c8);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);
				c8Element.addContent(c8);
				c9Element.addContent(c9);

				bustaxElement.addContent(c1Element);
				bustaxElement.addContent(c2Element);
				bustaxElement.addContent(c3Element);
				bustaxElement.addContent(c4Element);
				bustaxElement.addContent(c5Element);
				bustaxElement.addContent(c6Element);
				bustaxElement.addContent(c7Element);
				bustaxElement.addContent(c8Element);
				bustaxElement.addContent(c9Element);

				bustaxGrpElement.addContent(bustaxElement);

				logger.debug("bustax ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getBusTaxGrpElement() method " + e.getMessage());
		}

		return bustaxGrpElement;
	}

	public Element getUtilitiesElement(int actId) {
		Element utilitiesElement = new Element("utilities");

		try {
			logger.debug("entered into PublicWorksReportAgent.getUtilitiesElement");

			String sql = "SELECT T1.JOB_NO_UT_PERMIT_NO c1 , T1.JOB_ADDR_LOC1 c2 , T1.JOB_ADDR_LOC2 c3 , T1.JOB_ADDR_LOC3 c4 , T1.JOB_ADDR_LOC4 c5 , T1.JOB_ADDR_LOC5 c6 , T1.DAYS1 c7 , T1.DAYS2 c8 , T1.DAYS3 c9 , T1.DAYS4 c10 , T1.DAYS5 c11 , T1.HOURS1 c12 , T1.HOURS2 c13 , T1.HOURS3 c14 , T1.HOURS4 c15 , T1.HOURS5 c16  from UTILITIES_ACT T1  where T1.ACT_ID = " + actId + " ";
			logger.debug("Utilities in FIR : " + sql);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				utilitiesElement.addContent(new Element("utpermitno").addContent(rs.getString("c1")));
				utilitiesElement.addContent(new Element("loc1").addContent(rs.getString("c2")));
				utilitiesElement.addContent(new Element("loc2").addContent(rs.getString("c3")));
				utilitiesElement.addContent(new Element("loc3").addContent(rs.getString("c4")));
				utilitiesElement.addContent(new Element("loc4").addContent(rs.getString("c5")));
				utilitiesElement.addContent(new Element("loc5").addContent(rs.getString("c6")));
				utilitiesElement.addContent(new Element("days1").addContent(rs.getString("c7")));
				utilitiesElement.addContent(new Element("days2").addContent(rs.getString("c8")));
				utilitiesElement.addContent(new Element("days3").addContent(rs.getString("c9")));
				utilitiesElement.addContent(new Element("days4").addContent(rs.getString("c10")));
				utilitiesElement.addContent(new Element("days5").addContent(rs.getString("c11")));
				utilitiesElement.addContent(new Element("hours1").addContent(rs.getString("c12")));
				utilitiesElement.addContent(new Element("hours2").addContent(rs.getString("c13")));
				utilitiesElement.addContent(new Element("hours3").addContent(rs.getString("c14")));
				utilitiesElement.addContent(new Element("hours4").addContent(rs.getString("c15")));
				utilitiesElement.addContent(new Element("hours5").addContent(rs.getString("c16")));

				logger.debug("utilities ELEMENT - Created ");

				logger.debug("utpermitno is " + rs.getString("c1"));
				logger.debug("loc1 is " + rs.getString("c2"));
				logger.debug("loc2 is " + rs.getString("c3"));
				logger.debug("loc3 is " + rs.getString("c4"));
				logger.debug("loc4 is " + rs.getString("c5"));
				logger.debug("loc5 is " + rs.getString("c6"));
				logger.debug("days1is " + rs.getString("c7"));
				logger.debug("days2 is " + rs.getString("c8"));
				logger.debug("days3 is " + rs.getString("c9"));
				logger.debug("days4 is " + rs.getString("c10"));
				logger.debug("days5 is " + rs.getString("c11"));
				logger.debug("hours1 is " + rs.getString("c12"));
				logger.debug("hours2 is " + rs.getString("c13"));
				logger.debug("hours3 is " + rs.getString("c14"));
				logger.debug("hours4 is " + rs.getString("c15"));
				logger.debug("hours5 is " + rs.getString("c16"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getUtilitiesElement() method " + e.getMessage());
		}

		return utilitiesElement;
	}

	public Element getParcelElement(int actId) {
		Element parcelElement = new Element("parcelnumber");

		try {
			logger.debug("entered into PublicWorksReportAgent.getParcelElement");

			String sql = "select apn from lso_apn where lso_id = (Select lso_id from v_psa_list where act_id = " + actId + " )";
			logger.debug("parcel  : " + sql);

			Wrapper db = new Wrapper();
			RowSet parcelRs = db.select(sql);

			String c1 = "";

			while (parcelRs.next()) {
				// logger.debug("just befrore ");
				c1 = parcelRs.getString("apn");
				this.apn = c1;

				logger.debug("apn is " + c1);

				parcelElement.addContent(c1);

				logger.debug("parcel ELEMENT - Created ");
			}

			if (parcelRs != null) {
				parcelRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getParcelElement() method " + e.getMessage());
		}

		return parcelElement;
	}

	/*
	 * public Element getPCStatusElement(int actId) {
	 * 
	 * Element pcstatusElement = new Element("pcstatus"); try { logger.debug("entered into PublicWorksReportAgent.getPCStatusElement"); String sql = "select pc_desc from lkup_pc_status where pc_code = (select stat_code from v_plan_check_grp where act_id = " +actId+" )" ; Wrapper db = new Wrapper();
	 * 
	 * RowSet pcstatusRs = db.select(sql);
	 * 
	 * String c1 = ""; while (pcstatusRs.next()) { // logger.debug("just befrore ");
	 * 
	 * c1 = pcstatusRs.getString("pc_desc");
	 * 
	 * logger.debug("pcstatus is "+c1);
	 * 
	 * pcstatusElement.addContent(c1);
	 * 
	 * logger.debug("pcstatus ELEMENT - Created "); } if (pcstatusRs != null) pcstatusRs.close(); } catch (Exception e) { logger.error( " Exception in PublicWorksReportAgent -- getPCStatusElement() method " + e.getMessage()); } return pcstatusElement; }
	 */
	public Element getAssessorElement(String apn) {
		Element assessorElement = new Element("assessor");
		Element data1sqftElement = new Element("data1sqft");
		Element zoningcodeElement = new Element("zoningcode");

		try {
			logger.debug("entered into PublicWorksReportAgent.getAssessorElement");

			String sql = "select data1_sq_ft,zoning_code from assessor where apn ='" + apn + "'";
			Wrapper db = new Wrapper();

			RowSet assessorRs = db.select(sql);

			String data1sqft = "";
			String zoningcode = "";

			while (assessorRs.next()) {
				// logger.debug("just befrore ");
				data1sqft = assessorRs.getString("data1_sq_ft");
				zoningcode = assessorRs.getString("zoning_code");

				logger.debug("data1sqft is " + data1sqft);
				logger.debug("zoningcode is " + zoningcode);

				data1sqftElement.addContent(data1sqft);
				zoningcodeElement.addContent(zoningcode);

				assessorElement.addContent(data1sqftElement);
				assessorElement.addContent(zoningcodeElement);

				logger.debug("assessor ELEMENT - Created ");
			}

			if (assessorRs != null) {
				assessorRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getAssessorElement() method " + e.getMessage());
		}

		return assessorElement;
	}

	public Element getPlanReviewer(int actId) {
		String planReviewer = "";

		try {
			StringBuffer sql = new StringBuffer();

			/*
			 * sql.append("select user.username from users join plan_check pc on user.userid = pc.engineer where act_id = " + actId );sql.append( " and pc.created = (select max(created) from plan_check where act_id = " + actId + ")");
			 */

			// changed the query on 08/26/05 (Raj).
			sql.append("select users.username from users join plan_check pc on users.userid = " + "pc.engineer where act_id = " + actId + " " + "and pc.created = (select max(created) from plan_check where act_id = " + actId + ")");
			logger.debug("PlanReviewer : " + sql.toString());

			RowSet rs = new Wrapper().select(sql.toString());

			if (rs.next()) {
				planReviewer = rs.getString("username");
			}
		} catch (Exception e) {
			planReviewer = "";
			logger.error("Error in getting plan reviewer for activity");
		}

		return new Element("planreviewer").addContent(planReviewer);
	}

	public Element getFireInspector(int actId) {
		String fireInspector = "";

		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT U.USERNAME FROM USERS U JOIN PROCESS_TEAM PT ON U.USERID = PT.USERID");

			// GROUP ID 34 IS FIRE INSPECTOR
			sql.append(" AND PT.PSA_TYPE = 'A' AND PT.GROUP_ID = 34  AND PT.PSA_ID = " + actId);
			logger.debug("Fire Inspector : " + sql.toString());

			RowSet rs = new Wrapper().select(sql.toString());

			if (rs.next()) {
				fireInspector = rs.getString("USERNAME");
			}
		} catch (Exception e) {
			fireInspector = "";
			logger.error("Error in getting Fire Inspector for activity");
		}

		return new Element("FIRE_INSPECTOR").addContent(fireInspector);
	}

	public Element getProcessTeamElement(int activityId) {
		Element processTeamElement = new Element("processteam");
		Element inspectorElement = new Element("inspector");
		Element planreviewengineerElement = new Element("planreviewengineer");
		Element permittechElement = new Element("permittech");

		try {
			logger.debug("entered into getProcessTeamElement");

			StringBuffer sql = new StringBuffer();
			sql.append("select u.username,pt.group_id as groupid from (process_Team pt join users u on u.userid = pt.userid)");
			sql.append(" where psa_type='A' and pt.group_id in (" + Constants.GROUPS_INSPECTOR + "," + Constants.GROUPS_PERMIT_TECHNICIAN + "," + Constants.GROUPS_PLAN_REVIEW_ENGINEER + ") and psa_id = " + activityId);
			// sql.append(
			// " union select u.username,cast('11' as number) as groupid  from users u,project p"
			// );
			// sql.append(" where u.userid = p.created_by and p.proj_id=" +
			// activityId);
			logger.debug("ProcessTeam in FIR : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet pteamRs = db.select(sql.toString());

			String inspector = "";
			String permittech = "";
			String planreviewengineer = "";

			while (pteamRs.next()) {
				// logger.debug("just befrore ");
				if (pteamRs.getInt("groupid") == Constants.GROUPS_INSPECTOR) {
					inspector = pteamRs.getString("username");
				} else if (pteamRs.getInt("groupid") == Constants.GROUPS_PERMIT_TECHNICIAN) {
					permittech = pteamRs.getString("username");
				} else if (pteamRs.getInt("groupid") == Constants.GROUPS_PLAN_REVIEW_ENGINEER) {
					planreviewengineer = pteamRs.getString("username");
				}
			}

			logger.debug("inspector is " + inspector);
			logger.debug("permittech is " + permittech);
			logger.debug("planreviewengineer is " + planreviewengineer);

			inspectorElement.addContent(inspector);
			permittechElement.addContent(permittech);
			planreviewengineerElement.addContent(planreviewengineer);

			processTeamElement.addContent(inspectorElement);
			processTeamElement.addContent(permittechElement);
			processTeamElement.addContent(planreviewengineerElement);

			logger.debug("processTeam ELEMENT - Created ");

			if (pteamRs != null) {
				pteamRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getProcessTeamElement() method " + e.getMessage());
		}

		return processTeamElement;
	}

	public Element getPermitsGrpElement(int sprojectId) {
		Element permitsGrpElement = new Element("permitsgrp");

		try {
			logger.debug("entered into PublicWorksReportAgent.getPermitsGrpElement");

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT T1.ACT_NBR C1,T1.description C2,");
			sql.append("CASE WHEN T1.PLAN_CHK_REQ = 'Y' THEN 'PC req' ELSE  NULL END  C3,T3.description c4,");
			sql.append("T2.description C5,T1.FINAL_DATE C6,T1.ISSUED_DATE C7,T1.ACT_ID C8");
			sql.append(" FROM ((V_ACTIVITY T1 LEFT OUTER JOIN LKUP_ACT_ST T2 ON T1.STATUS = T2.STATUS_ID)");
			sql.append(" LEFT OUTER JOIN LKUP_ACT_TYPE T3 ON T1.ACT_TYPE = T3.TYPE )");
			sql.append(" WHERE T1.STATUS != 29 AND  T1.SPROJ_ID = " + sprojectId + " and (T1.status!=4 and T1.status!=38 and T1.status!=36 and T1.status!=39)");
			sql.append(" ORDER BY C7 desc ");

			logger.debug("permits : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql.toString());

			String c1 = "actnbr";
			String c2 = "permittodo";
			String c3 = "pcreq";
			String c4 = "acttype";
			String c5 = "actstatus";
			String c6 = "compdate";
			String c7 = "issdate";
			int c8 = -1;
			String paidamt = "";
			String amtdue = "";

			Element c1Element = null;
			Element c2Element = null;
			Element c3Element = null;
			Element c4Element = null;
			Element c5Element = null;
			Element c6Element = null;
			Element c7Element = null;

			Element paidElement = null;
			Element amtdueElement = null;

			while (titleRs.next()) {
				Element permitElement = new Element("permit");
				c1Element = new Element("actnbr");
				c2Element = new Element("permittodo");
				c3Element = new Element("pcreq");
				c4Element = new Element("acttype");
				c5Element = new Element("actstatus");
				c6Element = new Element("compdate");
				c7Element = new Element("issdate");

				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4");
				c5 = titleRs.getString("C5");
				c6 = titleRs.getString("C6");
				c7 = titleRs.getString("C7");
				c8 = titleRs.getInt("C8");
				logger.debug(" act id c8 is " + c8);

				// get fee details for each activity by taking actid as input
				sql = null;
				sql = new StringBuffer();

				sql.append("SELECT SUM(FEE_PAID) PAIDAMT,SUM(FEE_AMNT) - SUM(FEE_PAID) AMTDUE");
				sql.append(" FROM ACTIVITY_FEE A JOIN FEE F ON F.FEE_ID = A.FEE_ID");
				sql.append(" WHERE FEE_FL_4 IN ('1','2','5') AND ACTIVITY_ID = " + c8);
				logger.debug("Fee sql : " + sql.toString());

				RowSet feeRs = db.select(sql.toString());

				while (feeRs.next()) {
					paidamt = feeRs.getString("paidamt");
					amtdue = feeRs.getString("amtdue");
				}

				if (feeRs != null) {
					feeRs.close();
				}

				paidElement = new Element("paidamt");
				amtdueElement = new Element("amtdue");

				logger.debug("actnbr is " + c1);
				logger.debug("permittodo is " + c2);
				logger.debug("pcreq is " + c3);
				logger.debug("acttypenit is " + c4);
				logger.debug("actstatus  is " + c5);
				logger.debug("compdate  is " + c6);
				logger.debug("issdate is " + c7);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);
				paidElement.addContent(paidamt);
				amtdueElement.addContent(amtdue);

				permitElement.addContent(c1Element);
				permitElement.addContent(c2Element);
				permitElement.addContent(c3Element);
				permitElement.addContent(c4Element);
				permitElement.addContent(c5Element);
				permitElement.addContent(c6Element);
				permitElement.addContent(c7Element);
				permitElement.addContent(paidElement);
				permitElement.addContent(amtdueElement);

				permitsGrpElement.addContent(permitElement);

				logger.debug("permits ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getpermitsGrpElement() method " + e.getMessage());
		}

		return permitsGrpElement;
	}

	public Element getInspectionsGrpElement(int sprojectId) {
		Element inspectionsGrpElement = new Element("inspectionsgrp");

		try {
			logger.debug("entered into PublicWorksReportAgent.getInspectionsGrpElement");

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT T1.INSPCT_ITEM_ID C1,T3.description C2,T4.ACT_NBR C3,T1.INSPECTION_DT C4,");
			sql.append("T2.description c5,T1.USERNAME c6,T1.COMMENTS c7");
			sql.append(" FROM  (((V_INSPECTION T1 LEFT OUTER JOIN LKUP_INSPCT_CODE T2 ON T1.ACTN_CODE = T2.ACTN_CODE)");
			sql.append(" LEFT OUTER JOIN LKUP_INSPCT_ITEM T3 ON T1.INSPCT_ITEM_ID = T3.INSPCT_ITEM_ID)");
			sql.append(" LEFT OUTER JOIN V_ACTIVITY T4 ON T1.ACT_ID = T4.ACT_ID)");
			sql.append(" WHERE T4.SPROJ_ID = " + sprojectId + " and (T4.status!=4 and T4.status!=38 and T4.status!=36 and T4.status!=39)");
			sql.append(" ORDER BY C4 DESC ");

			logger.debug("Inspection in FIR : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql.toString());

			String c1 = "item";
			String c2 = "description";
			String c3 = "permit";
			String c4 = "inspdate";
			String c5 = "action";
			String c6 = "inspector";
			String c7 = "cmntstring";

			Element c1Element = null;
			Element c2Element = null;
			Element c3Element = null;
			Element c4Element = null;
			Element c5Element = null;
			Element c6Element = null;
			Element c7Element = null;

			while (titleRs.next()) {
				Element inspectionElement = new Element("inspection");
				c1Element = new Element("item");
				c2Element = new Element("description");
				c3Element = new Element("permit");
				c4Element = new Element("inspdate");
				c5Element = new Element("action");
				c6Element = new Element("inspector");
				c7Element = new Element("cmntstring");

				c1 = titleRs.getString("C1");
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4");
				c5 = titleRs.getString("C5");
				c6 = titleRs.getString("C6");
				c7 = titleRs.getString("C7");

				logger.debug("item	 is " + c1);
				logger.debug("description is " + c2);
				logger.debug("permit	 is " + c3);
				logger.debug("inspdate	 is " + c4);
				logger.debug("action	 is " + c5);
				logger.debug("inspector	 is " + c6);
				logger.debug("cmntstring is " + c7);

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);

				inspectionElement.addContent(c1Element);
				inspectionElement.addContent(c2Element);
				inspectionElement.addContent(c3Element);
				inspectionElement.addContent(c4Element);
				inspectionElement.addContent(c5Element);
				inspectionElement.addContent(c6Element);
				inspectionElement.addContent(c7Element);

				inspectionsGrpElement.addContent(inspectionElement);

				logger.debug("inspections ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getInspectionsGrpElement() method " + e.getMessage());
		}

		return inspectionsGrpElement;
	}

	/**
	 * Gets the business license conditons group element.
	 * 
	 * @param actId
	 * @return
	 */
	public Element getBusinessLicenseConditionsGrpElement(int actId) {
		Element conditionsGrpElement = new Element("conditionsgrp");

		try {
			logger.debug("Entered into getconditionsGrpElement");

			String sql = "SELECT T1.TEXT C1 FROM CONDITIONS T1 WHERE T1.LEVEL_ID = " + actId + " AND COND_LEVEL = 'A' ORDER BY T1.UPDATE_DT desc,T1.LIBRARY_ID desc";
			logger.debug("This SQL : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			StringBuffer c1 = new StringBuffer();

			Element c1Element = null;

			while (titleRs.next()) {
				c1.append(titleRs.getString("C1") + '\n');

				// truncate the conditions text to fit the license format.
				logger.debug("character count is " + c1.length());

				// logger.debug("# of line breaks is "+StringUtils.countLineBreaks
				// (c1));
			}

			logger.debug("c1=" + c1);

			// Added by Gayathri
			String plainText = (c1.toString()).replaceAll("\\<.*?\\>", "");
			logger.debug("$$$$$$$$$$$$$$ Text $$$$$$$$$$$$$$ " + plainText);

			String businessLicenceCertificateData = StringUtils.formatBusinessLicenceCertificateData(plainText);
			logger.debug("# formatBusinessLicenceCertificateData::" + businessLicenceCertificateData);

			String firstPageData = "";
			// for (int i=0; i<3; i++){
			firstPageData = StringUtils.getPageData(businessLicenceCertificateData, 25);

			// logger.debug("# dEBUGGING *****::"+firstPageData);
			// }
			logger.debug("# getPageData::" + firstPageData);

			Element conditionsElement = new Element("conditions");
			c1Element = new Element("condition");
			c1Element.addContent(firstPageData); // check
			conditionsElement.addContent(c1Element);
			conditionsGrpElement.addContent(conditionsElement);
			logger.debug("conditions ELEMENT - Created ");

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getConditionsGrpElement() method " + e.getMessage());
		}

		return conditionsGrpElement;
	}

	/**
	 * Gets the generic conditions group element.
	 * 
	 * @param actId
	 * @return
	 */
	public Element getConditionsGrpElement(int actId) {
		Element conditionsGrpElement = new Element("conditionsgrp");

		try {
			logger.debug("Entered into getconditionsGrpElement");

			String sql = "SELECT T1.TEXT C1 FROM CONDITIONS T1 LEFT OUTER JOIN LKUP_CONDITIONS T2 ON T2.CONDITION_ID = T1.COND_ID WHERE T1.LEVEL_ID = " + actId + " AND T1.COND_LEVEL = 'A' ORDER BY T1.ORDER_ID";
			logger.debug("This SQL : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "";

			Element c1Element = null;

			while (titleRs.next()) {
				Element conditionsElement = new Element("conditions");
				c1Element = new Element("condition");
				c1 = titleRs.getString("C1");
				c1Element.addContent(c1);
				conditionsElement.addContent(c1Element);
				conditionsGrpElement.addContent(conditionsElement);
				logger.debug("conditions ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getConditionsGrpElement() method " + e.getMessage());
		}

		return conditionsGrpElement;
	}

	/**
	 * Get comments group element.
	 * 
	 * @param actId
	 * @return
	 * @throws Exception
	 */
	public Element getCommentsGrpElement(int actId) throws Exception {
		logger.info("getCommentsGrpElement(" + actId + ")");

		Element commentsGrpElement = new Element("commentsgrp");

		try {
			String sql = "select T1.COMNT c1  from V_COMMENTS T1 where T1.LEVEL_ID = " + actId + " and T1.INTERNAL <> 'Y' and T1.COMNT_LEVEL='A'";

			Wrapper db = new Wrapper();
			logger.info(sql);

			RowSet titleRs = db.select(sql);

			String c1 = "";

			Element c1Element = null;

			while (titleRs.next()) {
				Element commentsElement = new Element("comments");
				c1Element = new Element("comments");

				c1 = titleRs.getString("c1");

				logger.debug("comments are " + c1);

				c1Element.addContent(c1);

				commentsElement.addContent(c1Element);

				commentsGrpElement.addContent(commentsElement);

				logger.debug("comments ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}

			return commentsGrpElement;
		} catch (Exception e) {
			logger.error(" Exception in ReportAgent -- getCommentsGrpElement() method " + e.getMessage());
			throw e;
		}
	}

	public Element getAfterHoursElement(int actId) {
		Element afterhoursElement = new Element("afterhours");

		try {
			logger.debug("entered into PublicWorksReportAgent.getAfterHoursElement");

			String sql = "select WORK_LOC c1 , PROPOSED_USE c2 , TYPE_OF_BLDG c3 , AREA c4 , HEIGHT c5 , NO_EMP c6 , NO_TYPE_VEHICLE c7 , NO_TYPE_MACHINES c8 , HOURS_REQD c9 , USE_PUBLIC_ROW c10 , EXPLAIN_USAGE_ROW c11 , BLDG_DEPT_APPR c12 , BLDG_DEPT_APPR_BY c13 , BLDG_DEPT_COND c14 , TRANS_DEPT_APPR c15 , TRANS_DEPT_APPR_BY c16 , TRANS_DEPT_COND c17 , POLICE_DEPT_APPR c18 , POLICE_DEPT_APPR_BY c19 , POLICE_DEPT_COND c20 from AFTER_HOURS_ACT where ACT_ID = " + actId + "  ";

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				afterhoursElement.addContent(new Element("worklocation").addContent(rs.getString("c1")));
				afterhoursElement.addContent(new Element("proposeduse").addContent(rs.getString("c2")));
				afterhoursElement.addContent(new Element("buildingtype").addContent(rs.getString("c3")));
				afterhoursElement.addContent(new Element("area").addContent(rs.getString("c4")));
				afterhoursElement.addContent(new Element("height").addContent(rs.getString("c5")));
				afterhoursElement.addContent(new Element("noofemployees").addContent(rs.getString("c6")));
				afterhoursElement.addContent(new Element("typeofvehicle").addContent(rs.getString("c7")));
				afterhoursElement.addContent(new Element("typeofmachines").addContent(rs.getString("c8")));
				afterhoursElement.addContent(new Element("hoursrequired").addContent(rs.getString("c9")));
				afterhoursElement.addContent(new Element("useofpublicrow").addContent(rs.getString("c10")));
				afterhoursElement.addContent(new Element("explainusagerow").addContent(rs.getString("c11")));
				afterhoursElement.addContent(new Element("bldgdeptapproval").addContent(rs.getString("c12")));
				afterhoursElement.addContent(new Element("bldgdeptapprovalby").addContent(rs.getString("c13")));
				afterhoursElement.addContent(new Element("bldgdeptcondtions").addContent(rs.getString("c14")));
				afterhoursElement.addContent(new Element("transportdeptapproval").addContent(rs.getString("c15")));
				afterhoursElement.addContent(new Element("transportdeptapprovalby").addContent(rs.getString("c16")));
				afterhoursElement.addContent(new Element("transportdeptconditions").addContent(rs.getString("c17")));
				afterhoursElement.addContent(new Element("policedeptapproval").addContent(rs.getString("c18")));
				afterhoursElement.addContent(new Element("policedeptapprovalby").addContent(rs.getString("c19")));
				afterhoursElement.addContent(new Element("policedeptconditions").addContent(rs.getString("c20")));

				logger.debug("utilities ELEMENT - Created ");

				logger.debug("worklocation  		is " + rs.getString("c1"));
				logger.debug("proposeduse 		is " + rs.getString("c2"));
				logger.debug("buildingtype 		is " + rs.getString("c3"));
				logger.debug("area          		is " + rs.getString("c4"));
				logger.debug("height 			is " + rs.getString("c5"));
				logger.debug("noofemployees 		is " + rs.getString("c6"));
				logger.debug("typeofvehicle 		is " + rs.getString("c7"));
				logger.debug("typeofmachines 		is " + rs.getString("c8"));
				logger.debug("hoursrequired		is " + rs.getString("c9"));
				logger.debug("useofpublicrow		is " + rs.getString("c10"));
				logger.debug("explainusagerow		is " + rs.getString("c11"));
				logger.debug("bldgdeptapproval	    	is " + rs.getString("c12"));
				logger.debug("bldgdeptapprovalby	is " + rs.getString("c13"));
				logger.debug("bldgdeptcondtions	    	is " + rs.getString("c14"));
				logger.debug("transportdeptapproval	is " + rs.getString("c15"));
				logger.debug("transportdeptapprovalby	is " + rs.getString("c16"));
				logger.debug("transportdeptconditions	is " + rs.getString("c17"));
				logger.debug("policedeptapproval	is " + rs.getString("c18"));
				logger.debug("policedeptapprovalby	is " + rs.getString("c19"));
				logger.debug("policedeptconditions	is " + rs.getString("c20"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getAfterHoursElement() method " + e.getMessage());
		}

		return afterhoursElement;
	}

	public Element getSpecialEventElement(int actId) {
		Element specialeventElement = new Element("specialevent");

		try {
			logger.debug("entered into PublicWorksReportAgent.getSpecialEventElement");

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT HOURS C1,LOCATION C2,VENUE C3,NO_PERSON_AT_LOCAT C4,");
			sql.append("ALTER_PHONE_NO1 C5,ALTER_PHONE_NO2 C6,ALTER_PHONE_NO3 C7,ALTER_PHONE_NO4 C8,ALTER_PHONE_NO5 C9,");
			sql.append("ELEMENTS C10,EQUIP_VEHICLES C11,PARKING_LOC_EQUIP C12,PARKING_LOC_PARTIC C13,");
			sql.append("CATERING_ACTIVITY C14,CERT_OF_INSURANCE C15,TYPE_OF_PROPERTY C16");
			sql.append(" FROM SPECIAL_EVT_ACT WHERE ACT_ID =" + actId);
			sql.append(" ");
			logger.debug("Special event act SQL : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());

			while (rs.next()) {
				specialeventElement.addContent(new Element("hours").addContent(rs.getString("c1")));
				specialeventElement.addContent(new Element("location").addContent(rs.getString("c2")));
				specialeventElement.addContent(new Element("venue").addContent(rs.getString("c3")));
				specialeventElement.addContent(new Element("noofpersonsatlocation").addContent(rs.getString("c4")));
				specialeventElement.addContent(new Element("phoneno1").addContent(rs.getString("c5")));

				specialeventElement.addContent(new Element("faxno").addContent(rs.getString("c6")));
				specialeventElement.addContent(new Element("cellphoneno").addContent(rs.getString("c7")));
				specialeventElement.addContent(new Element("pagerno").addContent(rs.getString("c8")));
				specialeventElement.addContent(new Element("phoneno2").addContent(rs.getString("c9")));
				specialeventElement.addContent(new Element("elements").addContent(rs.getString("c10")));
				specialeventElement.addContent(new Element("equipvehicles").addContent(rs.getString("c11")));
				specialeventElement.addContent(new Element("parkinglocequip").addContent(rs.getString("c12")));
				specialeventElement.addContent(new Element("parkinglocparticipants").addContent(rs.getString("c13")));
				specialeventElement.addContent(new Element("cateringactivityloc").addContent(rs.getString("c14")));
				specialeventElement.addContent(new Element("certofinsurance").addContent(rs.getString("c15")));
				specialeventElement.addContent(new Element("typeofproperty").addContent(rs.getString("c16")));

				logger.debug("hours is " + rs.getString("c1"));
				logger.debug("location is " + rs.getString("c2"));
				logger.debug("venue is " + rs.getString("c3"));
				logger.debug("noofpersonsatlocationis " + rs.getString("c4"));
				logger.debug("phoneno1 is " + rs.getString("c5"));
				logger.debug("phoneno2 is " + rs.getString("c6"));
				logger.debug("faxno is " + rs.getString("c7"));
				logger.debug("cellphoneno is " + rs.getString("c8"));
				logger.debug("pagerno is " + rs.getString("c9"));
				logger.debug("elements is " + rs.getString("c10"));
				logger.debug("equipvehicles is " + rs.getString("c11"));
				logger.debug("parkinglocequip 	is " + rs.getString("c12"));
				logger.debug("parkinglocparticipantsis " + rs.getString("c13"));
				logger.debug("cateringactivityloc 	is " + rs.getString("c14"));
				logger.debug("certofinsurance 	is " + rs.getString("c15"));
				logger.debug("typeofproperty 	is " + rs.getString("c16"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getSpecialEventElement() method " + e.getMessage());
		}

		return specialeventElement;
	}

	public Element getExcavationElement(int actId) {
		Element excavationElement = new Element("excavation");

		try {
			logger.debug("entered into PublicWorksReportAgent.getExcavationElement");

			String sql = "select WORK_LOC c1 , WORK_HRS c2 , REFER_TO c3 ,TRENCH_BACK_FILL c4 , RESURFACING_BY c5 ,  USA_NO c6,  PXT_THRU c7 ,CITY_BUS_LIC c8 , TRAFFIC_REQ c9  from  EXCAVATION_ACT where ACT_ID = " + actId + " ";

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				excavationElement.addContent(new Element("worklocation").addContent(rs.getString("c1")));
				excavationElement.addContent(new Element("hours").addContent(rs.getString("c2")));
				excavationElement.addContent(new Element("referto").addContent(rs.getString("c3")));
				excavationElement.addContent(new Element("trenchbackfill").addContent(rs.getString("c4")));
				excavationElement.addContent(new Element("resurfacingby").addContent(rs.getString("c5")));
				excavationElement.addContent(new Element("usano").addContent(rs.getString("c6")));
				excavationElement.addContent(new Element("pxtthru").addContent(rs.getString("c7")));
				excavationElement.addContent(new Element("citybusinesslicno").addContent(rs.getString("c8")));
				excavationElement.addContent(new Element("trafficrequirements").addContent(rs.getString("c9")));

				logger.debug("worklocation is " + rs.getString("c1"));
				logger.debug("hours is " + rs.getString("c2"));
				logger.debug("referto is " + rs.getString("c3"));
				logger.debug("trenchbackfill is " + rs.getString("c4"));
				logger.debug("resurfacingby is " + rs.getString("c5"));
				logger.debug("usano is " + rs.getString("c6"));
				logger.debug("pxtthru is " + rs.getString("c7"));
				logger.debug("citybusinesslicno is " + rs.getString("c8"));
				logger.debug("trafficrequirementsis " + rs.getString("c9"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getExcavationElement() method " + e.getMessage());
		}

		return excavationElement;
	}

	public Element getROWElement(int actId) {
		Element rowElement = new Element("publicrow");

		try {
			logger.debug("entered into PublicWorksReportAgent.getROWElement");

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT WORK_LOC c1,HOURS c2,REFER_TO c3,PXT_THRU c4,SITE_PH c5 FROM ROW_ACT WHERE ACT_ID = " + actId);

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());

			while (rs.next()) {
				rowElement.addContent(new Element("worklocation").addContent(rs.getString("c1")));
				rowElement.addContent(new Element("hours").addContent(rs.getString("c2")));
				rowElement.addContent(new Element("referto").addContent(rs.getString("c3")));
				rowElement.addContent(new Element("pxtthru").addContent(rs.getString("c4")));
				rowElement.addContent(new Element("phone").addContent(rs.getString("c5")));

				logger.debug("worklocation is " + rs.getString("c1"));
				logger.debug("hours is " + rs.getString("c2"));
				logger.debug("referto is " + rs.getString("c3"));
				logger.debug("pxtthru is " + rs.getString("c4"));
				logger.debug("phone is " + rs.getString("c5"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getROWElement() method " + e.getMessage());
		}

		return rowElement;
	}

	public Element getGarageSaleElement(int actId) {
		Element garagesaleElement = new Element("garagesale");

		try {
			logger.debug("entered into PublicWorksReportAgent.getGarageSaleElement");

			String sql = " select SALE_HRS c1 , ITEMS_DISP c2 , INDOORS c3 , OUTDOORS c4 , SIGNS c5 , COND c6 from GARAGE_SALE_ACT  where ACT_ID = " + actId + " ";

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				garagesaleElement.addContent(new Element("salehours").addContent(rs.getString("c1")));
				garagesaleElement.addContent(new Element("itemsdisplayed").addContent(rs.getString("c2")));
				garagesaleElement.addContent(new Element("indoor").addContent(rs.getString("c3")));
				garagesaleElement.addContent(new Element("outdoors").addContent(rs.getString("c4")));
				garagesaleElement.addContent(new Element("signs").addContent(rs.getString("c5")));
				garagesaleElement.addContent(new Element("conditions").addContent(rs.getString("c6")));
				logger.debug("salehours	is " + rs.getString("c1"));
				logger.debug("itemsdisplayed	is " + rs.getString("c2"));
				logger.debug("indoor 	is " + rs.getString("c3"));
				logger.debug("outdoors 	is " + rs.getString("c4"));
				logger.debug("signs 	is " + rs.getString("c5"));
				logger.debug("conditions 	is " + rs.getString("c6"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getGarageSaleElement() method " + e.getMessage());
		}

		return garagesaleElement;
	}

	public Element getSiteSurveyData(int lsoId) throws Exception {
		Element siteData = new Element("sitedata");
		String sql = "SELECT * FROM SITE_DATA WHERE LSOID = (SELECT LSO_ID FROM V_PSA_LIST WHERE ACT_ID = " + actId + ")";

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				siteData.addContent(new Element("LSOID").addContent(rs.getString("LSOID"))); // INTEGER
				siteData.addContent(new Element("ADDRESS").addContent(rs.getString("ADDRESS"))); // VARCHAR
				// (
				// 40
				// )
				// ,
				siteData.addContent(new Element("ALARMS").addContent(rs.getString("ALARMS"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("AREAOFPARKING").addContent(rs.getString("AREAOFPARKING"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("BLDGCONSTTYPECODE").addContent(rs.getString("BLDGCONSTTYPECODE"))); // VARCHAR
				// (
				// 40
				// )
				// ,
				siteData.addContent(new Element("BUILDINGNAME").addContent(rs.getString("BUILDINGNAME"))); // VARCHAR
				// (
				// 50
				// )
				// ,
				siteData.addContent(new Element("BUILDINGPERMITNO").addContent(rs.getString("BUILDINGPERMITNO"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("CENSUSTRACT").addContent(rs.getString("CENSUSTRACT"))); // VARCHAR
				// (
				// 40
				// )
				// ,
				siteData.addContent(new Element("COMMENTPRIMARYSYSTEM").addContent(rs.getString("COMMENTPRIMARYSYSTEM"))); // VARCHAR
				// (
				// 900
				// )
				// ,
				siteData.addContent(new Element("COMMENTSECONDARYSYSTEM").addContent(rs.getString("COMMENTSECONDARYSYSTEM"))); // VARCHAR
				// (
				// 600
				// )
				// ,
				siteData.addContent(new Element("COVENANTOFFSITE").addContent(rs.getString("COVENANTOFFSITE"))); // VARCHAR
				// (
				// 12
				// )
				// ,
				siteData.addContent(new Element("COVENANTONSITE").addContent(rs.getString("COVENANTONSITE"))); // VARCHAR
				// (
				// 12
				// )
				// ,
				siteData.addContent(new Element("CURRENTPRIMARYUSE").addContent(rs.getString("CURRENTPRIMARYUSE"))); // VARCHAR
				// (
				// 100
				// )
				// ,
				siteData.addContent(new Element("DATEOFBLDGPERMIT").addContent(rs.getString("DATEOFBLDGPERMIT"))); // DATE
				// ,
				siteData.addContent(new Element("DESIGNCODE").addContent(rs.getString("DESIGNCODE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("DIR1").addContent(rs.getString("DIR1"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("DIR2").addContent(rs.getString("DIR2"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ELEVATORENCLOSURE").addContent(rs.getString("ELEVATORENCLOSURE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ESTIMATEOFWIDTHFEET").addContent(rs.getString("ESTIMATEOFWIDTHFEET"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ESTIMATEOFWIDTHINCHES").addContent(rs.getString("ESTIMATEOFWIDTHINCHES"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("FLOODAREA").addContent(rs.getString("FLOODAREA"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("FOLLOWUP").addContent(rs.getString("FOLLOWUP"))); // VARCHAR
				// (
				// 500
				// )
				// ,
				siteData.addContent(new Element("HAZARDOUSMATERIALS").addContent(rs.getString("HAZARDOUSMATERIALS"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("HEIGHTFEET").addContent(rs.getString("HEIGHTFEET"))); // SMALLINT
				// ,
				siteData.addContent(new Element("HEIGHTINCHES").addContent(rs.getString("HEIGHTINCHES"))); // SMALLINT
				// ,
				siteData.addContent(new Element("HIGH1").addContent(rs.getString("HIGH1"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("HIGH2").addContent(rs.getString("HIGH2"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("HIGHFIRESEVERITYZONE").addContent(rs.getString("HIGHFIRESEVERITYZONE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("indx").addContent(rs.getString("indx"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("INLIEUPARKING").addContent(rs.getString("INLIEUPARKING"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("LIQUEFACTION").addContent(rs.getString("LIQUEFACTION"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("LOCALFAULTZONE").addContent(rs.getString("LOCALFAULTZONE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("LOW1").addContent(rs.getString("LOW1"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("LOW2").addContent(rs.getString("LOW2"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("MAJORSTRUCTURALADDITIONS").addContent(rs.getString("MAJORSTRUCTURALADDITIONS"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("MAJORSTRUCTURALALTERATIONS").addContent(rs.getString("MAJORSTRUCTURALALTERATIONS"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("MEZZANINE").addContent(rs.getString("MEZZANINE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("NOOFELEVATORS").addContent(rs.getString("NOOFELEVATORS"))); // SMALLINT
				// ,
				siteData.addContent(new Element("NOOFRESUNITS").addContent(rs.getString("NOOFRESUNITS"))); // INTEGER
				// ,
				siteData.addContent(new Element("NOOFSTORIESABOVEGRADE").addContent(rs.getString("NOOFSTORIESABOVEGRADE"))); // SMALLINT
				// ,
				siteData.addContent(new Element("NOOFSTORIESABOVEGRADEZ").addContent(rs.getString("NOOFSTORIESABOVEGRADEZ"))); // SMALLINT
				// ,
				siteData.addContent(new Element("NOOFSTORIESBELOWGRADE").addContent(rs.getString("NOOFSTORIESBELOWGRADE"))); // SMALLINT
				// ,
				siteData.addContent(new Element("NOOFSTRUCTURESONSITE").addContent(rs.getString("NOOFSTRUCTURESONSITE"))); // SMALLINT
				// ,
				siteData.addContent(new Element("NOTES").addContent(rs.getString("NOTES"))); // VARCHAR
				// (
				// 500
				// )
				// ,
				siteData.addContent(new Element("OCCUPANCY").addContent(rs.getString("OCCUPANCY"))); // VARCHAR
				// (
				// 100
				// )
				// ,
				siteData.addContent(new Element("OCCUPANTLOADCURRENT").addContent(rs.getString("OCCUPANTLOADCURRENT"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("OCCUPANTLOADRECORDED").addContent(rs.getString("OCCUPANTLOADRECORDED"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ONGRADE").addContent(rs.getString("ONGRADE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ONSITEPARKING").addContent(rs.getString("ONSITEPARKING"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("OTHERZONINGDETERMINATIONS").addContent(rs.getString("OTHERZONINGDETERMINATIONS"))); // VARCHAR
				// (
				// 600
				// )
				// ,
				siteData.addContent(new Element("PARCELNO").addContent(rs.getString("PARCELNO"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PARCELNUMBER1").addContent(rs.getString("PARCELNUMBER1"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PARCELNUMBER2").addContent(rs.getString("PARCELNUMBER2"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PARCELNUMBER3").addContent(rs.getString("PARCELNUMBER3"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PARCELNUMBER4").addContent(rs.getString("PARCELNUMBER4"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PARCELNUMBER5").addContent(rs.getString("PARCELNUMBER5"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PARKINGPROVIDED").addContent(rs.getString("PARKINGPROVIDED"))); // INTEGER
				// ,
				siteData.addContent(new Element("PARKINGREQUIRED").addContent(rs.getString("PARKINGREQUIRED"))); // INTEGER
				// ,
				siteData.addContent(new Element("PBASICSCORE").addContent(rs.getString("PBASICSCORE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PENTHOUSE").addContent(rs.getString("PENTHOUSE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("PFINALSCORE").addContent(rs.getString("PFINALSCORE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PHIGHRISE").addContent(rs.getString("PHIGHRISE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PLANSONFILE").addContent(rs.getString("PLANSONFILE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PLARGEHEAVYCLDG").addContent(rs.getString("PLARGEHEAVYCLDG"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PPLANIRREGULARITY").addContent(rs.getString("PPLANIRREGULARITY"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PPOORCONDITION").addContent(rs.getString("PPOORCONDITION"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PPOSTBENCHMARK").addContent(rs.getString("PPOSTBENCHMARK"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PPOSTBENCHMARKYR").addContent(rs.getString("PPOSTBENCHMARKYR"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PPOUNDING").addContent(rs.getString("PPOUNDING"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PRETROFITTED").addContent(rs.getString("PRETROFITTED"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PRIMARYSYSTEM").addContent(rs.getString("PRIMARYSYSTEM"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PRIMARYUSEDATE").addContent(rs.getString("PRIMARYUSEDATE"))); // DATE
				// ,
				siteData.addContent(new Element("PSHORTCOLUMNS").addContent(rs.getString("PSHORTCOLUMNS"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PSL2").addContent(rs.getString("PSL2"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PSL3").addContent(rs.getString("PSL3"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PSOFTSTORY").addContent(rs.getString("PSOFTSTORY"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PTORSION").addContent(rs.getString("PTORSION"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("PVERTICALIRREGULARITY").addContent(rs.getString("PVERTICALIRREGULARITY"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("RETROFITTED").addContent(rs.getString("RETROFITTED"))); // VARCHAR
				// (
				// 5
				// )
				// ,
				siteData.addContent(new Element("SBASICSCORE").addContent(rs.getString("SBASICSCORE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SECONDARYSYSTEM").addContent(rs.getString("SECONDARYSYSTEM"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SFINALSCORE").addContent(rs.getString("SFINALSCORE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SHIGHRISE").addContent(rs.getString("SHIGHRISE"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SLARGEHEAVYCLDG").addContent(rs.getString("SLARGEHEAVYCLDG"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SPLANIRREGULARITY").addContent(rs.getString("SPLANIRREGULARITY"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SPOORCONDITION").addContent(rs.getString("SPOORCONDITION"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SPOSTBENCHMARK").addContent(rs.getString("SPOSTBENCHMARK"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SPOUNDING").addContent(rs.getString("SPOUNDING"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SPRINKLERED").addContent(rs.getString("SPRINKLERED"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SRETROFITTED").addContent(rs.getString("SRETROFITTED"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SSHORTCOLUMNS").addContent(rs.getString("SSHORTCOLUMNS"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SSL2").addContent(rs.getString("SSL2"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SSL3").addContent(rs.getString("SSL3"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("SSOFTSTORY").addContent(rs.getString("SSOFTSTORY"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("STORSION").addContent(rs.getString("STORSION"))); // VARCHAR
				// (
				// 8
				// )
				// ,
				siteData.addContent(new Element("STREETNAME1").addContent(rs.getString("STREETNAME1"))); // VARCHAR
				// (
				// 40
				// )
				// ,
				siteData.addContent(new Element("STREETNAME2").addContent(rs.getString("STREETNAME2"))); // VARCHAR
				// (
				// 40
				// )
				// ,
				siteData.addContent(new Element("STRIPED").addContent(rs.getString("STRIPED"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("STRUCTADDRESS1").addContent(rs.getString("STRUCTADDRESS1"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("STRUCTADDRESS2").addContent(rs.getString("STRUCTADDRESS2"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("SUBTERRANEAN").addContent(rs.getString("SUBTERRANEAN"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("SVERTICALIRREGULARITY").addContent(rs.getString("SVERTICALIRREGULARITY"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("TOTALFLOORAREABLDGCODE").addContent(rs.getString("TOTALFLOORAREABLDGCODE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("TOTFLRAREAGRSWITHOUTPRKNG").addContent(rs.getString("TOTFLRAREAGRSWITHOUTPRKNG"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("URM").addContent(rs.getString("URM"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("WITHINSTRUCTURE").addContent(rs.getString("WITHINSTRUCTURE"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("YEARPERMITAPPLIED").addContent(rs.getString("YEARPERMITAPPLIED"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ZONECURRENT").addContent(rs.getString("ZONECURRENT"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("ZONEPERMIT").addContent(rs.getString("ZONEPERMIT"))); // VARCHAR
				// (
				// 25
				// )
				// ,
				siteData.addContent(new Element("SEQUENCE_NO").addContent(rs.getString("SEQUENCE_NO"))); // INTEGER
				// ,
				siteData.addContent(new Element("NEW_LOT_AREA").addContent(rs.getString("NEW_LOT_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("BASEMENT_AREA").addContent(rs.getString("BASEMENT_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("FAR_ALLOWED").addContent(rs.getString("FAR_ALLOWED"))); // INTEGER
				// ,
				siteData.addContent(new Element("EXISTING_BLDG_AREA").addContent(rs.getString("EXISTING_BLDG_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("FLOOR").addContent(rs.getString("FLOOR"))); // SMALLINT
				// ,
				siteData.addContent(new Element("COMMENTS").addContent(rs.getString("COMMENTS"))); // VARCHAR
				// (
				// 500
				// )
				// ,
				siteData.addContent(new Element("UNIT_AREA").addContent(rs.getString("UNIT_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("RESIDENTIAL_LOCATION").addContent(rs.getString("RESIDENTIAL_LOCATION"))); // CHARACTER
				// (
				// 1
				// )
				// ,
				siteData.addContent(new Element("PAD_AREA").addContent(rs.getString("PAD_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("SLOPE_AREA").addContent(rs.getString("SLOPE_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("FAR_ACTUAL").addContent(rs.getString("FAR_ACTUAL"))); // INTEGER
				// ,
				siteData.addContent(new Element("NEW_BLDG_AREA").addContent(rs.getString("NEW_BLDG_AREA"))); // INTEGER
				// ,
				siteData.addContent(new Element("BUILDING_USE").addContent(rs.getString("BUILDING_USE"))); // VARCHAR
				// (
				// 60
				// )
				// ,
				siteData.addContent(new Element("ROOFING").addContent(rs.getString("ROOFING"))); // VARCHAR
				// (
				// 40
				// )
				// ,
				siteData.addContent(new Element("NBR_BEDROOMS").addContent(rs.getString("NBR_BEDROOMS"))); // SMALLINT
				// ,
				siteData.addContent(new Element("SURVEY_TYPE").addContent(rs.getString("SURVEY_TYPE"))); // SMALLINT
				// ,
				siteData.addContent(new Element("GARAGE_AREA").addContent(rs.getString("GARAGE_AREA"))); // INTEGER
			}
		} catch (SQLException e) {
			logger.error("SQL Exception in getSiteSurveyData() : " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Exception : " + e.getMessage());
			throw e;
		}

		return siteData;
	}

	public Element getSiteSetbackInfo(int actId) throws Exception {
		Element setback = new Element("setbackinfo");
		String sql = "SELECT * FROM SITE_SETBACK WHERE LSO_ID = (SELECT LSO_ID FROM V_PSA_LIST WHERE ACT_ID = " + actId + ") ORDER BY UPDATED desc";

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				setback.addContent(new Element("SETBACK_ID").addContent(rs.getString("SETBACK_ID"))); // INTEGER
				// NOT
				// NULL
				// ,
				setback.addContent(new Element("LSO_ID").addContent(rs.getString("LSO_ID"))); // INTEGER
				// NOT
				// NULL
				// ,
				setback.addContent(new Element("FRONT_FT").addContent(rs.getString("FRONT_FT"))); // INTEGER
				// ,
				setback.addContent(new Element("FRONT_IN").addContent(rs.getString("FRONT_IN"))); // SMALLINT
				// ,
				setback.addContent(new Element("FRONT_COMMENT").addContent(rs.getString("FRONT_COMMENT"))); // VARCHAR
				// (
				// 50
				// )
				// ,
				setback.addContent(new Element("REAR_FT").addContent(rs.getString("REAR_FT"))); // INTEGER
				// ,
				setback.addContent(new Element("REAR_IN").addContent(rs.getString("REAR_IN"))); // SMALLINT
				// ,
				setback.addContent(new Element("REAR_COMMENT").addContent(rs.getString("REAR_COMMENT"))); // VARCHAR
				// (
				// 50
				// )
				// ,
				setback.addContent(new Element("SIDE1_FT").addContent(rs.getString("SIDE1_FT"))); // INTEGER
				// ,
				setback.addContent(new Element("SIDE1_IN").addContent(rs.getString("SIDE1_IN"))); // SMALLINT
				// ,
				setback.addContent(new Element("SIDE1_DIR").addContent(rs.getString("SIDE1_DIR"))); // CHARACTER
				// (
				// 1
				// )
				// ,
				setback.addContent(new Element("SIDE1_COMMENT").addContent(rs.getString("SIDE1_COMMENT"))); // VARCHAR
				// (
				// 50
				// )
				// ,
				setback.addContent(new Element("SIDE2_FT").addContent(rs.getString("SIDE2_FT"))); // INTEGER
				// ,
				setback.addContent(new Element("SIDE2_IN").addContent(rs.getString("SIDE2_IN"))); // SMALLINT
				// ,
				setback.addContent(new Element("SIDE2_DIR").addContent(rs.getString("SIDE2_DIR"))); // CHARACTER
				// (
				// 1
				// )
				// ,
				setback.addContent(new Element("SIDE2_COMMENT").addContent(rs.getString("SIDE2_COMMENT"))); // VARCHAR
				// (
				// 50
				// )
				// ,
				setback.addContent(new Element("comments").addContent(rs.getString("comments"))); // VARCHAR
				// (
				// 160
				// )
				// ,
				setback.addContent(new Element("UPDATED").addContent(rs.getString("UPDATED"))); // TIMESTAMP
				// DEFAULT
				// CURRENT
				// TIMESTAMP
				// ,
			}
		} catch (SQLException e) {
			logger.error("SQL Exception : " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Exception : " + e.getMessage());
			throw e;
		}

		return setback;
	}

	public Element getSiteSurveyUnit(String actNumber) throws Exception {
		Element siteUnit = new Element("siteunit");
		String sql = "SELECT * FROM SITE_SURVEY_UNIT WHERE ACT_NBR = '" + actNumber + "'";

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				siteUnit.addContent(new Element("UNIT_ID").addContent(rs.getString("UNIT_ID"))); // INTEGER
				// NOT
				// NULL
				// ,
				siteUnit.addContent(new Element("STRUCT_NBR").addContent(rs.getString("STRUCT_NBR"))); // INTEGER
				// NOT
				// NULL
				// ,
				siteUnit.addContent(new Element("UNIT_NBR").addContent(rs.getString("UNIT_NBR"))); // VARCHAR
				// (
				// 12
				// )
				// ,
				siteUnit.addContent(new Element("FLOOR_NBR").addContent(rs.getString("FLOOR_NBR"))); // VARCHAR
				// (
				// 4
				// )
				// ,
				siteUnit.addContent(new Element("UNIT_AREA").addContent(rs.getString("UNIT_AREA"))); // INTEGER
				// ,
				siteUnit.addContent(new Element("USE_TYPE").addContent(rs.getString("USE_TYPE"))); // SMALLINT
				// ,
				siteUnit.addContent(new Element("OCCUPANT_LOAD").addContent(rs.getString("OCCUPANT_LOAD"))); // INTEGER
				// ,
				siteUnit.addContent(new Element("STREET_NBR").addContent(rs.getString("STREET_NBR"))); // INTEGER
				// ,
				siteUnit.addContent(new Element("STREET_MOD").addContent(rs.getString("STREET_MOD"))); // VARCHAR
				// (
				// 6
				// )
				// ,
				siteUnit.addContent(new Element("STREET_ID").addContent(rs.getString("STREET_ID"))); // INTEGER
				// ,
				siteUnit.addContent(new Element("MEZZ_FLAG").addContent(rs.getString("MEZZ_FLAG"))); // CHARACTER
				// (
				// 1
				// )
				// ,
				siteUnit.addContent(new Element("MEZZ_DESC").addContent(rs.getString("MEZZ_DESC"))); // VARCHAR
				// (
				// 100
				// )
				// ,
				siteUnit.addContent(new Element("MEZZ_AREA").addContent(rs.getString("MEZZ_AREA"))); // INTEGER
				// ,
				siteUnit.addContent(new Element("MEZZ_USE_TYPE").addContent(rs.getString("MEZZ_USE_TYPE"))); // SMALLINT
				// ,
				siteUnit.addContent(new Element("MEZZ_OCC_LOAD").addContent(rs.getString("MEZZ_OCC_LOAD"))); // INTEGER
				// ,
				siteUnit.addContent(new Element("ACT_NBR").addContent(rs.getString("ACT_NBR"))); // VARCHAR
				// (
				// 9
				// )
				// ,
				siteUnit.addContent(new Element("DUMMY_ADDR_FLAG").addContent(rs.getString("DUMMY_ADDR_FLAG"))); // CHARACTER
				// (
				// 1
				// )
				// ,
				siteUnit.addContent(new Element("BALANCE_SQFT_FLAG").addContent(rs.getString("BALANCE_SQFT_FLAG"))); // CHARACTER
				// (
				// 1
				// )
				// ,
				siteUnit.addContent(new Element("FLOOR_AREA_FLAG").addContent(rs.getString("FLOOR_AREA_FLAG"))); // CHARACTER
				// (
				// 1
				// )
				// ,
			}
		} catch (SQLException e) {
			logger.error("SQL Exception : " + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Exception : " + e.getMessage());
			throw e;
		}

		return siteUnit;
	}

	public Element getBatchPrintElement(String activityNumber) {
		logger.info("getBatchPrintElement(" + activityNumber + ")");

		Element batchPrintGroupElement = new Element("batchPrintGroup");

		try {
		} catch (Exception e) {
			logger.error(" Exception in  getBatchPrintElement " + e.getMessage());
		}

		return batchPrintGroupElement;
	}

	public Document getBatchPrintJDOM(String activityNumber) throws Exception {
		logger.info("getBatchPrintJDOM(" + activityNumber + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			// Create root Element
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			// Create data Element
			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Comment xmlDataElement = new Comment("************** XML DATA **************");
			dataElement.addContent(xmlDataElement);

			// Start getting elements and add to dataElement
			// Element businessLicenseElement =
			// getBusinessLicenseElement(activityNumber);
			Element getBatchPrintElement = getBatchPrintElement(activityNumber);
			dataElement.addContent(getBatchPrintElement);
			logger.debug(" int id is " + activityNumber);
		} catch (Exception e) {
			logger.error(" Exception in getBatchPrintJDOM method " + e.getMessage());
		}

		return document;
	}

	public Element getBusinessLicenseElement(String activityNumber) {
		logger.info("getBusinessLicenseElement(" + activityNumber + ")");

		Element businessLicenseGroupElement = new Element("businessLicenseGroup");

		try {
			String addrSql = "select * from ";
			String sql = "select TO_CHAR(BA.RENEWAL_DT-1,'Month dd, yyyy') c1,lat.description c2 ,ba.business_name c3,val.dl_address c4,val.CITYSTATEZIPZIP4 c5,ba.business_acc_no c6,ba.class_code c7,ba.decal_code c8,a.act_id c9 from activity a join bl_activity ba on a.act_id=ba.act_id left outer join lkup_act_type lat on lat.type=a.act_type left outer join v_alladdress_list vaal on vaal.addr_id = a.addr_id left outer join v_address_list val on val.lso_id=vaal.lso_id where a.act_nbr= " + StringUtils.checkString(activityNumber);
			logger.debug("Query is : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "EXPIRATION_DATE";
			String c2 = "BUSINESS_TYPE";
			String c3 = "BUSINESS_OWNER_NAME";
			String c4 = "BUSINESS_ADDRESS";
			String c5 = "CITYSTATEZIP";
			String c6 = "ACCOUNT_NUMBER";
			String c7 = "CLASS_CODE";
			String c8 = "DECAL_CODE";
			int c9 = -1;
			String c10 = "FISICAL_YEAR";

			Element c1Element = null;
			Element c2Element = null;
			Element c3Element = null;
			Element c4Element = null;
			Element c5Element = null;
			Element c6Element = null;
			Element c7Element = null;
			Element c8Element = null;
			Element c9Element = null;
			Element c10Element = null;

			while (titleRs != null && titleRs.next()) {
				Element businessLicenseElement = new Element("businessLicense");
				c1Element = new Element("EXPIRATION_DATE");
				c2Element = new Element("BUSINESS_TYPE");
				c3Element = new Element("BUSINESS_NAME");
				c4Element = new Element("BUSINESS_ADDRESS");
				c5Element = new Element("CITYSTATEZIP");
				c6Element = new Element("ACCOUNT_NUMBER");
				c7Element = new Element("CLASS_CODE");
				c8Element = new Element("DECAL_CODE");
				c9Element = new Element("Activity_id");
				c10Element = new Element("FISICAL_YEAR");

				c1 = titleRs.getString("C1");
				if (c2 != null)
					c2 = titleRs.getString("C2").toUpperCase();
				if (c3 != null)
					c3 = titleRs.getString("C3").toUpperCase();
				if (c4 != null)
					c4 = titleRs.getString("C4").toUpperCase();
				c5 = titleRs.getString("C5").toUpperCase();
				c6 = titleRs.getString("C6");
				c7 = titleRs.getString("C7");
				c8 = titleRs.getString("C8");
				c9 = titleRs.getInt("C9");

				logger.debug("Expiration_Date is " + c1);
				logger.debug("Business_Type  is " + c2);
				logger.debug("Business_Owner  is " + c3);
				logger.debug("Business_Address is " + c4);
				logger.debug("CityStateZip is" + c5);
				logger.debug("Business_License_Number is " + c6);
				logger.debug("Class_Code  is " + c7);
				logger.debug("Decal_Code  is " + c8);
				logger.debug("Activity ID  is " + c9);

				if ((c4.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BL_ACTIVITY WHERE ACT_ID=" + c9;
					logger.debug(sql);

					RowSet ootRs = db.select(sql);

					if (ootRs.next()) {
						c4 = ootRs.getString(1).toUpperCase();
						logger.debug("OOT Business Address is " + c4);
						c5 = ootRs.getString(2).toUpperCase();
						logger.debug("OOT city state zip is " + c5);
					}
				}

				c1Element.addContent(c1);
				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);
				c8Element.addContent(c8);
				// c9Element.addContent(c9);
				businessLicenseElement.addContent(c1Element);
				businessLicenseElement.addContent(c2Element);
				businessLicenseElement.addContent(c3Element);
				businessLicenseElement.addContent(c4Element);
				businessLicenseElement.addContent(c5Element);
				businessLicenseElement.addContent(c6Element);
				businessLicenseElement.addContent(c7Element);
				businessLicenseElement.addContent(c8Element);

				if (c9 > 0) {
					logger.debug("Activity ID  is " + c9);
					logger.debug("just before conditionsGrpElement");

					Element conditionsGrpElement = getBusinessLicenseConditionsGrpElement(c9);
					businessLicenseElement.addContent(conditionsGrpElement);
					businessLicenseGroupElement.addContent(businessLicenseElement);
				}
			}

			Element Year = getFisicalYear(c9);

			businessLicenseGroupElement.addContent(Year);
			logger.debug("businessLicenseElement - Created ");

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in getBusinessLicenseElement " + e.getMessage());
		}

		return businessLicenseGroupElement;
	}

	/**
	 * Gets the business Tax element for printing the certificate.
	 * 
	 * @param activityNumber
	 * @return
	 */
	public Element getBusinessTaxElement(String activityNumber) {
		logger.info("getBusinessTaxElement(" + activityNumber + ")");

		Element businessTaxGroupElement = new Element("businessTaxGroup");

		try {
			String sql = "select (BA.MAIL_STR_NO || ' ' || BA.MAIL_STR_NAME || ' ' || BA.MAIL_UNIT) c1,  TO_CHAR(A.START_DATE,'MM/DD/YY') c3, LAT.DESCRIPTION c4, BA.BUSINESS_NAME c5, BA.NAME1 c6, VAL.DL_ADDRESS c7, BA.BUSINESS_ACC_NO c8, A.ACT_ID c9, BA.CLASS_CODE c10, BA.HOME_OCCUPATION c11, VAL.CITYSTATEZIPZIP4 c13,(BA.MAIL_CITY || ' ' || BA.MAIL_STATE || ' ' || BA.MAIL_ZIP || ' ' || BA.MAIL_ZIP4) c14, BA.ATTN c15, BA.CORPORATE_NAME c16, BA.NAME2  c17, BA.OWN_TYPE_ID as c18 from ACTIVITY A join BT_ACTIVITY BA on A.ACT_ID=BA.ACT_ID left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE left outer join V_ALLADDRESS_LIST VAAL on VAAL.ADDR_ID= A.ADDR_ID left outer join V_ADDRESS_LIST VAL on VAL.LSO_ID=VAAL.LSO_ID  where A.ACT_NBR= " + StringUtils.checkString(activityNumber);
			logger.debug("Query is : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String c1 = "MAILING_ADDRESS";
			String c3 = "ISSUED_DATE";
			String c4 = "ACT_TYPE";
			String c5 = "BUSINESS_NAME";
			String c6 = "BUSINESS_OWNER_NAME";
			String c7 = "BUSINESS_ADDRESS";
			String c8 = "BUSINESS_ACCOUNT_NUMBER";
			int c9 = -1;
			String c10 = "CLASS_CODE";
			String c11 = "HOME_OCCUPATION";
			String c12 = "LOGO_PATH";
			String c13 = "BUSINESS_CITY_STATE_ZIP";
			String c14 = "MAIL_CITY_STATE_ZIP";
			String c15 = "ATTN";
			String c16 = "CORPORATE_NAME";
			String c17 = "BUSINESS_OWNER_NAME2";
			int c18 = -1;

			Element c1Element = null;
			Element c3Element = null;
			Element c4Element = null;
			Element c5Element = null;
			Element c6Element = null;
			Element c7Element = null;
			Element c8Element = null;
			Element c10Element = null;
			Element c11Element = null;
			Element c12Element = null;
			Element c13Element = null;
			Element c14Element = null;
			Element c15Element = null;
			Element c16Element = null;

			while (titleRs.next()) {

				Element businessTaxElement = new Element("businessTax");
				c1Element = new Element("MAILING_ADDRESS");
				c3Element = new Element("ISSUED_DATE");
				c4Element = new Element("ACT_TYPE");
				c5Element = new Element("BUSINESS_NAME");
				c6Element = new Element("BUSINESS_OWNER_NAME");
				c7Element = new Element("BUSINESS_ADDRESS");
				c8Element = new Element("BUSINESS_ACCOUNT_NUMBER");
				c10Element = new Element("CLASS_CODE");
				c11Element = new Element("HOME_OCCUPATION");
				c12Element = new Element("LOGO_PATH");
				c13Element = new Element("BUSINESS_CITY_STATE_ZIP");
				c14Element = new Element("MAIL_CITY_STATE_ZIP");
				c15Element = new Element("ATTN");
				c16Element = new Element("CORPORATE_NAME");

				c1 = titleRs.getString("C1").toUpperCase();
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4").toUpperCase();
				c5 = titleRs.getString("C5").toUpperCase();

				if (titleRs.getInt("C18") > 0 && titleRs.getInt("C18") == Constants.BT_SOLE_OWNER_TYPE) {
					c6 = StringUtils.nullReplaceWithEmpty(titleRs.getString("C6")).toUpperCase();
				} else {
					c6 = "";
				}

				c7 = titleRs.getString("C7").toUpperCase();
				c8 = titleRs.getString("C8").toUpperCase();
				c9 = titleRs.getInt("C9");
				c10 = titleRs.getString("C10").toUpperCase();
				c11 = titleRs.getString("C11");
				c13 = titleRs.getString("C13").toUpperCase();
				c14 = titleRs.getString("C14").toUpperCase();
				c15 = StringUtils.nullReplaceWithEmpty(titleRs.getString("C15")).toUpperCase();
				c18 = titleRs.getInt("C18");

				if (c18 > 0 && (c18 == Constants.BT_CORPORATION) || (c18 == Constants.BT_LLC) || (c18 == Constants.BT_SEE_COMMENTS) || (c18 == Constants.BT_TRUST)) {
					c16 = StringUtils.nullReplaceWithEmpty(titleRs.getString("C16")).toUpperCase();
				} else {
					c16 = "";
				}

				c17 = StringUtils.nullReplaceWithEmpty(titleRs.getString("C17")).toUpperCase();

				if ((!("").equalsIgnoreCase(c17)) && (titleRs.getInt("C18") > 0 && titleRs.getInt("C18") == Constants.BT_SOLE_OWNER_TYPE)) {
					if (!(c6.equalsIgnoreCase(""))) {
						c6 = c6 + " & " + c17;
					} else {
						c6 = StringUtils.nullReplaceWithEmpty(titleRs.getString("C17")).toUpperCase();
					}
				}

				// Checking Mailing Address Empty
				if (((c1.trim().replace(',', ' ').equals("")) || (c1.trim().replace(',', ' ') == null)) && ((c14.trim().replace(',', ' ').equals("")) || (c14.trim().replace(',', ' ') == null))) {
					c1 = c7;
					c14 = c13;
				}

				logger.debug("MAILING_ADDRESS is " + c1);
				logger.debug("ISSUED_DATE  is " + c3);
				logger.debug("ACT_TYPE is " + c4);
				logger.debug("BUSINESS_NAME is" + c5);
				logger.debug("BUSINESS_OWNER1_NAME is " + c6);
				logger.debug("BUSINESS_ADDRESS  is " + c7);
				logger.debug("BUSINESS_ACCOUNT_NUMBER  is " + c8);
				logger.debug("Activity_id  is " + c9);
				logger.debug("CLASS_CODE is " + c10);
				logger.debug("HOME_OCCUPATION  is " + c11);
				logger.debug("LOGO_PATH  is " + c12);
				logger.debug("BUSINESS_CITY_STATE_ZIP  is " + c13);
				logger.debug("MAIL_CITY_STATE_ZIP  is " + c14);
				logger.debug("ATTN is " + c15);
				logger.debug("CORPORATE_NAME is " + c16);
				logger.debug("BUSINESS_OWNER2_NAME is " + c17);

				if ((c7.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT MAIL_STR_NO || ' ' || MAIL_STR_NAME || ' ' || MAIL_UNIT as OOT_MAIL_ADDRESS, MAIL_CITY || ' ' || MAIL_STATE || ' ' || MAIL_ZIP || ' ' || MAIL_ZIP4 AS OOT_MAIL_CITYSTATEZIP, OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_BUSINESS_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_BUSINESS_CITYSTATEZIP FROM BT_ACTIVITY WHERE ACT_ID=" + c9;
					logger.debug(sql);

					RowSet ootRs = db.select(sql);

					if (ootRs.next()) {
						logger.debug("inise rs");
						c1 = ootRs.getString(1).toUpperCase();
						logger.debug("OOT_MAIL_ADDRESS is " + c1);
						c14 = ootRs.getString(2).toUpperCase();
						logger.debug("OOT_MAIL_CITYSTATEZIP is " + c14);
						c7 = ootRs.getString(3).toUpperCase();
						logger.debug("OOT_BUSINESS_ADDRESS is " + c7);
						c13 = ootRs.getString(4).toUpperCase();
						logger.debug("OOT_BUSINESS_CITYSTATEZIP is " + c13);
					}
				}

				c1Element.addContent(c1);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);
				c8Element.addContent(c8);
				c10Element.addContent(c10);
				c11Element.addContent(c11);
				c12Element.addContent(c12);
				c13Element.addContent(c13);
				c14Element.addContent(c14);
				c15Element.addContent(c15);
				c16Element.addContent(c16);
				businessTaxElement.addContent(c1Element);
				businessTaxElement.addContent(c3Element);
				businessTaxElement.addContent(c4Element);
				businessTaxElement.addContent(c5Element);
				businessTaxElement.addContent(c6Element);
				businessTaxElement.addContent(c7Element);
				businessTaxElement.addContent(c8Element);
				businessTaxElement.addContent(c10Element);
				businessTaxElement.addContent(c11Element);
				businessTaxElement.addContent(c12Element);
				businessTaxElement.addContent(c13Element);
				businessTaxElement.addContent(c14Element);
				businessTaxElement.addContent(c15Element);
				businessTaxElement.addContent(c16Element);

				if (c9 > 0) {
					logger.debug("Activity ID  is " + c9);
					logger.debug("just before conditionsGrpElement");

					Element conditionsGrpElement = getBusinessLicenseConditionsGrpElement(c9);
					businessTaxElement.addContent(conditionsGrpElement);
					businessTaxGroupElement.addContent(businessTaxElement);
				}
			}

			Element Year = getFisicalYear(c9);

			businessTaxGroupElement.addContent(Year);
			logger.debug("businessTaxElement - Created ");

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in getBusinessTaxElement " + e.getMessage());
		}

		return businessTaxGroupElement;
	}

	/**
	 * Gets the business license JDOM document
	 * 
	 * @param actNbr
	 * @return
	 * @throws Exception
	 */
	public Document getBusinessLicenseJDOM(String activityNumber) throws Exception {
		logger.info("getBusinessLicenseJDOM(" + activityNumber + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			// Create root Element
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			// Create data Element
			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Comment xmlDataElement = new Comment("************** XML DATA **************");
			dataElement.addContent(xmlDataElement);

			Element businessLicenseGroupElement = getBusinessLicenseElement(activityNumber);
			dataElement.addContent(businessLicenseGroupElement);
		} catch (Exception e) {
			logger.error(" Exception in getBusinessLicenseJDOM -- getPermitJDOM() method " + e.getMessage());
		}

		return document;
	}

	/**
	 * Gets the business tax JDOM document
	 * 
	 * @param actNbr
	 * @return
	 * @throws Exception
	 */
	public Document getBusinessTaxJDOM(String activityNumber) throws Exception {
		logger.info("getBusinessTaxJDOM(" + activityNumber + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			// Create root Element
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			// Create data Element
			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Comment xmlDataElement = new Comment("************** XML DATA **************");
			dataElement.addContent(xmlDataElement);

			Element businessTaxGroupElement = getBusinessTaxElement(activityNumber);
			dataElement.addContent(businessTaxGroupElement);
		} catch (Exception e) {
			logger.error(" Exception in getBusinessTaxJDOM -- getPermitJDOM() method " + e.getMessage());
		}

		return document;
	}

	public Element getFisicalYear(int actId) {
		logger.info("getFisicalYear(" + actId + ")");

		String c1 = "FISICAL_YEAR";
		String month = "";
		int startdate = 0;
		int enddate = 0;

		Element FisicalYear = new Element("FisicalYear");

		try {
			String sql = "select FISICAL_YEAR C1 FROM V_BUSINESS_LICENSE where ACTIVITY_ID= '" + actId + "'";
			logger.debug("Query is : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);
			Element c1Element = null;
			Element c3Element = null;

			while (titleRs.next()) {
				c1Element = new Element("START_YEAR");
				c3Element = new Element("END_YEAR");
				c1 = titleRs.getString("C1");

				String yr = c1.substring(3, 7);
				month = c1.substring(0, 2);

				int curyear = Integer.valueOf(yr).intValue();
				int years = curyear;
				int months = Integer.valueOf(month).intValue();

				if (months >= 6) {
					startdate = curyear;
					enddate = ++years;
				} else {
					startdate = --years;
					enddate = curyear;
				}

				c1Element.addContent(startdate + "");
				c3Element.addContent(enddate + "");

				FisicalYear.addContent(c1Element);
				FisicalYear.addContent(c3Element);
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in getFisicalYear " + e.getMessage());
		}

		return FisicalYear;
	}
	
	public Document getLNCVPermitJDOM(String actNbr, String dates) {
		logger.info("getLNCVPermitJDOM(" + actNbr + "," + dates + ")");

		// Create JDOM Document
		Document document = new Document();

		try {
			// Create root Element
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			// Create data Element
			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Comment xmlDataElement = new Comment("************** XML DATA **************");
			dataElement.addContent(xmlDataElement);

			// Start getting elements and add to dataElement
			Element mainElement = getMainElement(actNbr);

			// Add Plan reviewer
			//mainElement.addContent();

			// mainElement.addContent(PRE);
			dataElement.addContent(mainElement);
			logger.debug(" int id is " + actId);

			if (actId > 0) {
				logger.debug("just before permit print ");

				Element ledgerGrpElement = getLncvDatesGrpElement(dates);
				dataElement.addContent(ledgerGrpElement);

				logger.debug("just before applicantGrpElement ");

							

			}
		} catch (Exception e) {
			logger.error(" Exception in ReportAgent -- getPermitJDOM() method " + e.getMessage());
		}

		return document;
	}
	
	public Element getLncvDatesGrpElement(String dates) {
		logger.info("getLncvDatesGrpElement(" + dates + ")");

		Element ledgerGrpElement = new Element("datesgrp");

		try {
			
			Element c1Element = null;
			Element c2Element = null;
			
			String c1 = "lncvdate";
			String c2 = "lncvmonth";
			
			Calendar c = Calendar.getInstance();
			
			if(dates.endsWith(",")){
				dates = dates.substring(0,dates.length()-1);
			}
			
			String [] lncvdt = StringUtils.stringtoArray(dates, ",");
			if(lncvdt.length>0){
				for(int i=0;i<lncvdt.length;i++){
					
					Element ledgerElement = new Element("date");
					c1Element = new Element("lncvdate");
					c2Element = new Element("lncvmonth");
					//lncvdate = lncvdt[i];
					c1 = lncvdt[i];
					
					
					c= StringUtils.str2cal(lncvdt[i]);
					c2 = new SimpleDateFormat("MMM").format(StringUtils.cal2SQLDate(c));
					
					c1Element.addContent(c1);
					c2Element.addContent(c2);
					ledgerElement.addContent(c1Element);
					ledgerElement.addContent(c2Element);

					ledgerGrpElement.addContent(ledgerElement);
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" Exception in getLncvDatesGrpElement() method " + e.getMessage());
		}

		return ledgerGrpElement;
	}
	
	
}

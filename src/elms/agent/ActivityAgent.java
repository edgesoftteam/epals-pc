package elms.agent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import javax.sql.RowSet;

import org.apache.commons.validator.GenericValidator;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;

import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivitySubType311;
import elms.app.admin.ActivityType;
import elms.app.admin.CustomField;
import elms.app.admin.EngineerUser;
import elms.app.admin.ParkingZone;
import elms.app.admin.PlanCheckStatus;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.bl.BusinessLicenseActivity;
import elms.app.bl.BusinessOwner;
import elms.app.bt.BusinessTaxActivity;
import elms.app.common.Agent;
import elms.app.common.DropdownValue;
import elms.app.common.LookupType;
import elms.app.common.MultiAddress;
import elms.app.common.PickList;
import elms.app.common.ProcessTeamNameRecord;
import elms.app.enforcement.CodeEnforcement;
import elms.app.finance.ActivityFinance;
import elms.app.finance.Fee;
import elms.app.finance.FinanceSummary;
import elms.app.lso.LsoAddress;
import elms.app.lso.LsoUse;
import elms.app.lso.Owner;
import elms.app.people.People;
import elms.app.planning.CalendarFeature;
import elms.app.planning.EnvReviewEdit;
import elms.app.planning.Planner;
import elms.app.planning.PlannerUpdateRecord;
import elms.app.planning.PlanningDetails;
import elms.app.planning.ResolutionEdit;
import elms.app.planning.StatusEdit;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.app.project.ActivityHistory;
import elms.app.project.ActivityLite;
import elms.app.project.Certificate;
import elms.app.project.CopyActivity;
import elms.app.project.GreenHalo;
import elms.app.project.GreenHaloFinalResponse;
import elms.app.project.GreenHaloNewProjectResponse;
import elms.app.project.PlanCheck;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.common.Constants;
import elms.control.beans.BasicSearchForm;
import elms.control.beans.BusinessLicenseApprovalForm;
import elms.control.beans.PlanningActivitiesForm;
import elms.control.beans.admin.AdminComboMappingForm;
import elms.control.beans.admin.CustomLabelsForm;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.control.beans.planning.EnvReviewForm;
import elms.control.beans.planning.EnvReviewSummary;
import elms.control.beans.planning.ResolutionForm;
import elms.control.beans.planning.StatusForm;
import elms.control.beans.project.ActivityHistoryForm;
import elms.control.beans.rfs.ActivityListDetail;
import elms.control.beans.rfs.AddRequesterForm;
import elms.control.beans.rfs.ListRequestServiceForm;
import elms.control.beans.rfs.RFSIncident;
import elms.control.beans.rfs.addIncidentForm;
import elms.exception.AgentException;
import elms.exception.DuplicateRecordException;
import elms.gsearch.GlobalSearch;
import elms.security.Department;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class ActivityAgent extends Agent {

	static Logger logger = Logger.getLogger(ActivityAgent.class.getName());

	/**
	 * default constructor
	 */
	public ActivityAgent() {
	}

	public ActivityDetail getActivityDetail(RowSet rs) throws Exception {
		logger.info("getActivityDetail(rs)");
		int activityId = -1;

		ActivityDetail activityDetail = new ActivityDetail();
		List<String> a = new ArrayList<>();

		try {
			activityDetail.setActivityNumber(rs.getString("act_nbr"));
			activityDetail.setActivityType(new ActivityType(rs.getString("type"), rs.getString("type_desc")));
			activityDetail.setAddressId(rs.getInt("addr_id"));
			String lsoAddress = getActivityAddressForId(activityId);
			activityDetail.setAddress(lsoAddress);
			activityDetail.setInspectionRequired(inspectionRequired(rs.getString("type")));
			activityDetail.setFinalDateLabel(rs.getString("type"));
			activityDetail.setExpirationDateLabel(rs.getString("type"));
			activityDetail.setDescription(rs.getString("DESCRIPTION"));
			activityDetail.setValuation(rs.getDouble("valuation"));
			activityDetail.setPlanCheckRequired(rs.getString("plan_chk_req"));
			activityDetail.setStatus(new ActivityStatus(rs.getInt("status_id"), rs.getString("status_code").trim(), rs.getString("status_desc").trim()));
			activityDetail.setStartDate(rs.getDate("start_date"));
			activityDetail.setAppliedDate(rs.getDate("applied_date"));
			activityDetail.setIssueDate(rs.getDate("issued_date"));
			activityDetail.setExpirationDate(rs.getDate("exp_date"));
			activityDetail.setFinaledDate(rs.getDate("final_date"));
			activityDetail.setPlanCheckFeeDate(rs.getDate("plan_chk_fee_date"));
			activityDetail.setDevelopmentFeeDate(rs.getDate("DEVELOPMENT_FEE_DATE"));
			activityDetail.setPermitFeeDate(rs.getDate("permit_fee_date"));
			activityDetail.setMicrofilm(rs.getString("microfilm"));
			activityDetail.setLabel(rs.getString("label"));

			String s1 = "(";

			try {
				String activitySubTypeSQL = "select act_subtype_id from act_subtype ast where ast.act_id= " + activityId;
				logger.debug(activitySubTypeSQL);
				Wrapper activitySubTypeDb = new Wrapper();
				rs = activitySubTypeDb.select(activitySubTypeSQL);

				while (rs != null && rs.next()) {
					a.add(rs.getString("act_subtype_id"));
					logger.debug("getActivity() --ActivitySubType Ids are  " + a.toString());
				}

				Iterator subTypeIds = a.iterator();

				while (subTypeIds.hasNext()) {
					s1 = s1 + (String) subTypeIds.next() + ",";
				}

				s1 = s1 + 0 + ")";
				logger.debug("The ActivitySubTypeIds are" + s1);
			} catch (Exception e) {
				logger.debug("exception in activity subtype:" + e.getMessage());
			}

			activityDetail.setActivitySubTypes(LookupAgent.getSubTypes(s1));
			logger.debug("ddmmyy" + LookupAgent.getSubTypes(s1));

			return activityDetail;
		} catch (Exception e) {
			logger.error("Error in getActivityDetail " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Gets the lite activity for the given activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public Activity getActivityLite(int activityId) throws Exception {
		logger.info("getActivityLite(" + activityId + ")");
		RowSet rs = null;
		List a = new ArrayList();
		Activity activity = new Activity();
		try {
			Wrapper db = new Wrapper();
			ActivityDetail activityDetail = null;

			String sql = "select a.*,vaa.address,las.status_id,las.stat_code as status_code,las.description as status_desc,lat.type as type,lat.description as type_desc from (activity a left outer join v_activity_address vaa on a.addr_id=vaa.addr_id) join lkup_act_st las on las.status_id=a.status ,lkup_act_type lat where lat.type=a.act_type and a.act_id=" + activityId;
			logger.debug(sql);
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				logger.debug("obtained result, creating the activity detail object");
				activityDetail = getActivityDetail(rs);
				logger.debug("created a activity detail object successfully");
				activity = new Activity(activityId, activityDetail);
				logger.debug("created a activity object successfully");
				activity.setSubProjectId(rs.getInt("sproj_id"));
			}

			String s1 = "(";

			try {
				String sql10 = "select act_subtype_id from act_subtype ast where ast.act_id= " + activityId;
				rs = db.select(sql10);

				while (rs != null && rs.next()) {
					a.add(rs.getString("act_subtype_id"));
					logger.debug("getActivity() --ActivitySubType Ids are  " + a.toString());
				}

				Iterator subTypeIds = a.iterator();

				while (subTypeIds.hasNext()) {
					s1 = s1 + (String) subTypeIds.next() + ",";
				}

				s1 = s1 + 0 + ")";
				logger.debug("The ActivitySubTypeIds are" + s1);
			} catch (Exception e) {
				logger.debug("exception in activity subtype:" + e.getMessage());
			}

			List subTypeList = LookupAgent.getSubTypes(s1);
			logger.debug("obtained the subtype list " + subTypeList);
			activityDetail.setActivitySubTypes(subTypeList);
			logger.debug("set the subtype list to the activity detail object");

			return activity;
		} catch (Exception e) {
			logger.error("Exception in getactivity()" + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Gets the activity for a given activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public Activity getActivity(int activityId) throws Exception {
		logger.info("getActivity(" + activityId + ")");

		Activity activity = new Activity();
		ActivityDetail activityDetail = new ActivityDetail();
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select a.*,vaa.dl_address from activity a left outer join v_activity_address vaa on a.act_id=vaa.act_id where a.act_id=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {

				activityDetail.setActivityNumber(rs.getString("act_nbr"));
				logger.debug("ActivityNumber is  " + activityDetail.getActivityNumber());
				activityDetail.setActivityType(LookupAgent.getActivityType(rs.getString("act_type")));
				logger.debug("ActivityType is  " + activityDetail.getActivityType());
				activityDetail.setAddressId(rs.getInt("addr_id"));
				logger.debug("address id is  " + activityDetail.getAddressId());
				String lsoAddress = getActivityAddressForId(activityId);
				activityDetail.setAddress(lsoAddress);
				logger.debug("address is  " + activityDetail.getAddress());
				activityDetail.setInspectionRequired(inspectionRequired(activityDetail.getActivityType().getType()));
				logger.debug("Inspection Required is  " + activityDetail.getInspectionRequired());
				activityDetail.setFinalDateLabel(rs.getString("act_type"));
				logger.debug("Completion Date Label is  " + activityDetail.getFinalDateLabel());
				activityDetail.setExpirationDateLabel(rs.getString("act_type"));
				logger.debug("Expiration Date Label is  " + activityDetail.getExpirationDateLabel());
				activityDetail.setDescription(rs.getString("description"));
				logger.debug("Description is  " + activityDetail.getDescription());
				activityDetail.setLabel(rs.getString("label"));
				logger.debug("Label is  " + activityDetail.getLabel());
				activityDetail.setValuation(rs.getDouble("valuation"));
				logger.debug("Valuation is  " + activityDetail.getValuation());
				activityDetail.setPlanCheckRequired(rs.getString("plan_chk_req"));
				logger.debug("PlanCheckRequired is  " + activityDetail.getPlanCheckRequired());
				activityDetail.setDevelopmentFeeRequired(rs.getString("dev_fee_req"));
				logger.debug("dev fee req is  " + activityDetail.getDevelopmentFeeRequired());
				activityDetail.setStatus(LookupAgent.getActivityStatus(rs.getInt("status")));
				logger.debug("status is  " + activityDetail.getStatus());
				activityDetail.setStartDate(rs.getDate("start_date"));
				logger.debug("StartDate is  " + StringUtils.cal2str(activityDetail.getStartDate()));
				activityDetail.setAppliedDate(rs.getDate("applied_date"));
				logger.debug("Applied Date is  " + StringUtils.cal2str(activityDetail.getAppliedDate()));
				activityDetail.setIssueDate(rs.getDate("issued_date"));
				logger.debug("Issued Date is  " + StringUtils.cal2str(activityDetail.getIssueDate()));
				activityDetail.setExpirationDate(rs.getDate("exp_date"));
				logger.debug("expiration date is  " + StringUtils.cal2str(activityDetail.getExpirationDate()));
				activityDetail.setFinaledDate(rs.getDate("final_date"));
				logger.debug("Final date is  " + StringUtils.cal2str(activityDetail.getFinaledDate()));
				activityDetail.setPlanCheckFeeDate(rs.getDate("plan_chk_fee_date"));
				logger.debug("Plan Check Fee date is  " + StringUtils.cal2str(activityDetail.getPlanCheckFeeDate()));
				activityDetail.setPermitFeeDate(rs.getDate("permit_fee_date"));
				logger.debug("permit fee date is  " + StringUtils.cal2str(activityDetail.getPermitFeeDate()));
				activityDetail.setMicrofilm(rs.getString("microfilm"));
				logger.debug("Microfilm is  " + activityDetail.getMicrofilm());

				activityDetail.setOnlineApplication(rs.getString("APPLICATION_ONLINE"));
				String applicationOnline = rs.getString("APPLICATION_ONLINE");
				if(applicationOnline.equalsIgnoreCase("Y")) {
					activityDetail.setCreatedBy(new AdminAgent().getOnlineUser(rs.getInt("created_by")));
					logger.debug("created user is  " + activityDetail.getCreatedBy().getUsername());
				}else {
					activityDetail.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
					logger.debug("created user is  " + activityDetail.getCreatedBy().getUsername());					
				}

				
				activityDetail.setNewUnits(StringUtils.nullReplaceWithEmpty(rs.getString("new_units")));
				logger.debug("newUnits is " + activityDetail.getNewUnits());
				activityDetail.setExistingUnits(StringUtils.nullReplaceWithEmpty(rs.getString("existing_units")));
				logger.debug("existingUnits is " + activityDetail.getExistingUnits());
				activityDetail.setDemolishedUnits(StringUtils.nullReplaceWithEmpty(rs.getString("demolished_units")));
				logger.debug("demolishedUnits is " + activityDetail.getDemolishedUnits());

				String parkingZoneId = rs.getString("pzone_id") != null ? rs.getString("pzone_id") : "";
				logger.debug("Parking zone id is " + parkingZoneId);
				ParkingZone parkingZone = new ParkingZone();
				if (!(parkingZoneId.equalsIgnoreCase(""))) {
					parkingZone = AddressAgent.getParkingZoneForZoneId(parkingZoneId);
				}
				activityDetail.setParkingZone(parkingZone);
				
				activityDetail.setGreenHalo(elms.util.StringUtils.YNtoOnOff(rs.getString("green_halo")));
				logger.debug("Green Halo is  " + activityDetail.getGreenHalo());

				activityDetail.setExtRefNumber(StringUtils.nullReplaceWithEmpty(rs.getString("EXT_REF_NUMBER")));
				logger.debug("ExtRefNumber is  " + activityDetail.getExtRefNumber());
				
				activityDetail.setCreated(rs.getDate("created"));

				if (activityDetail.getCreated() != null) {
					logger.debug("created is  " + activityDetail.getCreated().getTime());
				}

				activityDetail.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				logger.debug("updated user is  " + activityDetail.getUpdatedBy().getUsername());
				activityDetail.setUpdated(rs.getDate("updated"));

				if (activityDetail.getUpdated() != null) {
					logger.debug("updated is  " + activityDetail.getUpdated().getTime());
				}

				if (activityDetail == null) {
					activityDetail = new ActivityDetail();
				}

				activity = new Activity(activityId, activityDetail);
				activity.setGreenHalo(activityDetail.getGreenHalo());
				logger.debug("New activity Object is created");
				activity.setSubProjectId(rs.getInt("sproj_id"));
				logger.debug("subprojectId is  " + activity.getSubProjectId());
				activity.setPsaHold(new CommonAgent().getHolds(activityId, "A", 3));
				logger.debug("Holds set with size " + activity.getPsaHold().size());

				activity.setExtRefNumber(StringUtils.nullReplaceWithEmpty(rs.getString("EXT_REF_NUMBER")));
				// get Tenants list
				// int lsoId = LookupAgent.getLsoIdForPsaId("" + activityId, "A");
				// logger.debug("lsoId is " + lsoId);

				// get the lso id for activity
				int lsoid = LookupAgent.getLsoId(activityId, "A");
				if (lsoid == -1) {
					lsoid = LookupAgent.getLsoIdForPsaId("" + activityId, "A");
				}
				logger.debug("lsoId is " + lsoid);
				activity.setPsaTenant(new CommonAgent().getTenants(lsoid, "L", 0));
				logger.debug("Tenants set with size " + activity.getPsaTenant().size());

				// get Notices list
				if (activityDetail.getActivityType().getDepartmentId() == Constants.DEPARTMENT_HOUSING) {
					activity.setPsaNotice(getNoticetList(activityId));
					logger.debug("Notices set with size " + activity.getPsaNotice().size());
				} else {
					activity.setPsaNotice(new CommonAgent().getNotices(activityId));
					logger.debug("Notices set with size " + activity.getPsaNotice().size());
				}
				// Set the Activity Finance Object of Activity here
				FinanceSummary financeSummary = new FinanceAgent().getActivityFinance(activityId);

				if (financeSummary == null) {
					financeSummary = new FinanceSummary();
				}

				String amountDue = financeSummary.getTotalAmountDue();
				logger.debug("total amount due as " + financeSummary.getTotalAmountDue());
				logger.debug("total amount due as float " + amountDue);

				ActivityFinance activityFinance = new ActivityFinance();
				activityFinance.setAmountDue((float) StringUtils.$2dbl(amountDue));
				activity.setActivityFinance(activityFinance);
				logger.debug("AmountDue is set to" + activity.getActivityFinance().getAmountDue());

				if (activity.getActivityDetail().getActivityType().getDepartmentCode().equals("PL")) { // Planning
					// activities
					activity.setPlanningDetails(ActivityAgent.getPlanningDetails("A", activityId));
				}

				// Checking Inspectable and deletable
				if (((rs.getInt("status") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_ISSUED) || // permit issued
						(rs.getInt("status") == Constants.ACTIVITY_STATUS_BUILDING_ADMIN_PENDING) || // admin pending
						(rs.getInt("status") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_REISSUED) || // permit reissued
						(rs.getInt("status") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_EXTENDED) || // permit extended
						(rs.getInt("status") == Constants.ACTIVITY_STATUS_BUILDING_TCO) || // TCO
						(rs.getInt("status") == Constants.ACTIVITY_STATUS_PUBLIC_WORKS_ISSUED)) && (financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))) { // Status = Issued and balance due is zero
					activity.setInspectable(true);
				} else {

					activity.setInspectable(false);
				}

				logger.debug("The Total Amount Due : " + financeSummary.getTotalAmountDue());
				logger.debug("The Inspectable is : " + activity.isInspectable());

				if ((financeSummary.getTotalPaidAmt() == null) || financeSummary.getTotalPaidAmt().equals("") || financeSummary.getTotalPaidAmt().equals("$0.00")) {
					activity.setDeletable(true);
				} else {
					activity.setDeletable(false);
				}
			}

			// get multiple suptypes for a activity
			activityDetail.setActivitySubTypes(LookupAgent.getSubTypes(getMultipleActivitySubTypes(activityId)));

			return activity;
		} catch (Exception e) {
			logger.error("Exception in getactivity()" + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	public String getMultipleActivitySubTypes(int activityId) throws Exception {
		logger.info("getMultipleActivitySubTypes(" + activityId + ")");

		String s1 = "(";
		List a = new ArrayList();

		RowSet rs = null;

		try {
			String sql10 = "select act_subtype_id from act_subtype ast where ast.act_id= " + activityId;
			logger.info(sql10);
			Wrapper sql10Db = new Wrapper();
			rs = sql10Db.select(sql10);

			while (rs != null && rs.next()) {
				a.add(rs.getString("act_subtype_id"));
				logger.debug("getActivity() --ActivitySubType Ids are  " + a.toString());
			}

			Iterator subTypeIds = a.iterator();

			while (subTypeIds.hasNext()) {
				s1 = s1 + (String) subTypeIds.next() + ",";
			}

			s1 = s1 + 0 + ")";
			logger.debug("The ActivitySubTypeIds are" + s1);

			return s1;
		} catch (Exception e) {
			logger.debug("exception in activity subtype:" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Adds the activity to the database.
	 * 
	 * @param activity
	 * @return
	 * @throws Exception
	 */
	public int addActivity(Activity activity) throws Exception {
		logger.info("addActivity(activity)");
		int activityId = -1;
		try {
			String activityType = "";
			Wrapper db = new Wrapper();
			String activityNumber = "";
			activityId = db.getNextId("ACTIVITY_ID");
			logger.debug("Obtained new activity id is " + activityId);

			int activityNum = db.getNextId("ACT_NUM");
			logger.debug("Obtained new activity number is " + activityNum);

			String strActNum = StringUtils.i2s(activityNum);
			strActNum = "00000".substring(strActNum.length()) + strActNum;

			logger.debug("Obtained new activity number string is " + strActNum);

			if (activity.getActivityDetail().getActivityType() == null) {
				activityType = "";
			} else {
				activityType = activity.getActivityDetail().getActivityType().getType();

				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formatter = new SimpleDateFormat("yy");
				activityNumber = activity.getActivityDetail().getActivityType().getDepartmentCode() + formatter.format(calendar.getTime()) + strActNum.trim();
				logger.debug(" activityAgent -- addActivity() -- Department Code from activityObject as " + activityNumber);
			}

			logger.debug("activityAgent -- addActivity() --insert Sql is " + activityType);

			StringBuffer sql = new StringBuffer();
			sql.append("insert into activity(act_id,sproj_id,act_nbr,act_type,addr_id,description,valuation,");
			sql.append("start_date,applied_date,issued_date,final_date,exp_date,permit_fee_date,plan_chk_fee_date,DEVELOPMENT_FEE_DATE,");
			sql.append("status,plan_chk_req,dev_fee_req,created_by,created,new_units,existing_units,demolished_units,updated_by,updated)");
			sql.append(" values(" + activityId + ",");
			sql.append(activity.getSubProjectId() + ",");
			sql.append(StringUtils.checkString(activityNumber) + ",");
			sql.append(StringUtils.checkString(activity.getActivityDetail().getActivityType().getType()) + ",");
			sql.append(StringUtils.checkNumber(activity.getActivityDetail().getAddressId()) + ",");
			sql.append(StringUtils.checkString(activity.getActivityDetail().getDescription()) + ",");
			sql.append(activity.getActivityDetail().getValuation() + ",");
			sql.append("to_date(" + StringUtils.checkString(StringUtils.cal2str(activity.getActivityDetail().getStartDate())) + ",'MM/DD/YYYY')" + ",");
			sql.append("to_date(" + StringUtils.checkString(StringUtils.cal2str(activity.getActivityDetail().getStartDate())) + ",'MM/DD/YYYY')" + ",");
			sql.append("to_date(" + StringUtils.checkString(StringUtils.cal2str(activity.getActivityDetail().getIssueDate())) + ",'MM/DD/YYYY')" + ",");
			sql.append("to_date(" + StringUtils.checkString(StringUtils.cal2str(activity.getActivityDetail().getFinaledDate())) + ",'MM/DD/YYYY')" + ",");
			sql.append("to_date(" + StringUtils.checkString(StringUtils.cal2str(activity.getActivityDetail().getExpirationDate())) + ",'MM/DD/YYYY')" + ",");
			sql.append("current_date,");
			sql.append("current_date,");
			sql.append("current_date,");
			sql.append(activity.getActivityDetail().getStatus().getStatus() + ",");
			sql.append(StringUtils.checkString(activity.getActivityDetail().getPlanCheckRequired()) + ",");
			sql.append(StringUtils.checkString(activity.getActivityDetail().getDevelopmentFeeRequired()) + ",");
			sql.append(activity.getActivityDetail().getUpdatedBy().getUserId() + ",");
			sql.append("current_timestamp" + ",'");
			sql.append(StringUtils.nullReplaceWithEmpty(activity.getActivityDetail().getNewUnits()) + "','");
			sql.append(StringUtils.nullReplaceWithEmpty(activity.getActivityDetail().getExistingUnits()) + "','");
			sql.append(StringUtils.nullReplaceWithEmpty(activity.getActivityDetail().getDemolishedUnits()) + "',");
			sql.append(activity.getActivityDetail().getUpdatedBy().getUserId() + ",");
			sql.append("current_timestamp" + ")");
			logger.debug(sql.toString());

			db.beginTransaction();
			db.addBatch(sql.toString());

			int size = activity.getActivityDetail().getActivitySubTypes().size();
			logger.debug("size for subtype list is:" + size);

			if (size > 0) {
				for (int i = 0; i < size; i++) {
					String sql10 = "insert into act_subtype values(" + activityId + "," + LookupAgent.getSubTypeId((String) activity.getActivityDetail().getActivitySubTypes().get(i), activityType) + ")";
					logger.debug(sql10);
					logger.debug("The " + i + " th element is :" + (String) activity.getActivityDetail().getActivitySubTypes().get(i));
					db.addBatch(sql10);
				}
			}

			try {
				List selectedFees = activity.getFees();
				Iterator selectedFeesIter = selectedFees.iterator();

				while (selectedFeesIter.hasNext()) {
					Fee fee = (Fee) selectedFeesIter.next();
					String sql1 = "insert into activity_fee(activity_id,fee_id,people_id) values(" + activityId + "," + fee.getFeeId() + ",0)";
					logger.debug(" Sql adding fee in activity_fee Table " + sql1);
					db.addBatch(sql1);
				}
			} catch (Exception e) {
				logger.error("Exception on Fee List " + e.getMessage());
			}

			// executing the batch sqls
			db.executeBatch();
			/*
			 * For building permits
			 * 
			 * IF plan check required is checked then system will automatically create default plan check entries with unassigned user,
			 * 
			 * but various comments Planning, Fire, Zoning, Engineering...
			 */
			ActivityDetail activityDetail = activity.getActivityDetail();
			int deptId = 0;
			String deptType = activity.getActivityDetail().getActivityType().getDepartmentCode();
			if (deptType.equalsIgnoreCase(Constants.DEPT_CODE_PW_BUILDING_SAFETY) && activity.getActivityDetail().getPlanCheckRequired().equalsIgnoreCase("Y")) {

				List<PlanCheck> pcList = new ArrayList<PlanCheck>();

				int[] pcDept;
				// int[] pcDept = Constants.DEFAULT_PC_DEPT_ID;
				// String[] pcComments = Constants.DEFAULT_PC_DEPT_COMMENT;

				String allDepartmentIds = new PlanCheckAgent().getDefaultPlanCheck(activityDetail.getActivityType().getType());
				String[] deptIds = null;
				if (allDepartmentIds == null || allDepartmentIds.equals("")) {
					return activityId;
				}
				deptIds = allDepartmentIds.split(",");
				LookupAgent lookupAgent = new LookupAgent();
				for (int i = 0; i < deptIds.length; i++) {
					PlanCheck pc = new PlanCheck();
					pc.setActivityId(activityId);
				    deptId = Integer.parseInt(deptIds[i]);
					// Department department = lookupAgent.getDepartment(pcDept[i]);
					Department department = lookupAgent.getDepartment(deptId);
					pc.setDeptDesc(department.getDescription());
					pc.setComments("Building:");
					pc.setCreatedBy(activity.getActivityDetail().getUpdatedBy());
					pcList.add(pc);
				}
				PlanCheckAgent pcAgent = new PlanCheckAgent();
				pcAgent.addBasicPlanChecks(pcList);
			} 
			return activityId;
		} catch (Exception e) {
			// set back activity Id to -1
			activityId = -1;
			logger.error("Exception thrown while trying to add Activity  :" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of activities for a given sub project.
	 * 
	 * @param subProjectId
	 * @return
	 * @throws Exception
	 */
	public List getActivities(int subProjectId) throws Exception {
		logger.info("getActivities(" + subProjectId + ")");
		List<Activity> activities;

		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select act_id  from activity where sproj_id=" + subProjectId + " order by start_date desc,act_id desc";
			logger.debug(sql);
			rs = db.select(sql);
			activities = new ArrayList();

			while (rs != null && rs.next()) {
				Activity activity = new Activity();
				activity = this.getActivity(rs.getInt("act_id"));
				activities.add(activity);
			}

			logger.info("Exiting getActivities()");

			return activities;
		} catch (Exception e) {
			logger.error("Exception in getActivities " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Gets the lite activies list for a sub project
	 * 
	 * @param subProjectId
	 * @return
	 * @throws Exception
	 */
	public List getActivitiesLite(int subProjectId, String active) throws Exception {
		logger.info("getActivitiesLite(" + subProjectId + ")");
		List<Activity> activities;

		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select a.*,val.address,las.status_id,las.stat_code as status_code,las.description as status_desc,lat.type as type,lat.description as type_desc from ((activity a join v_psa_list vpl on vpl.act_id=a.act_id) join v_address_list val on vpl.lso_id=val.lso_id) join lkup_act_st las on las.status_id=a.status,lkup_act_type lat  where lat.type=a.act_type and a.sproj_id=" + subProjectId;
			if (active.equalsIgnoreCase("Y")) {
				sql = sql + " and  las.PSA_active='Y' ";
			}
			// sql += " order by a.applied_date desc,a.act_id desc";
			sql += " order by a.label asc , a.applied_date desc,a.act_id desc";
			logger.debug(sql);
			rs = db.select(sql);
			activities = new ArrayList();

			while (rs != null && rs.next()) {
				Activity activity = new Activity(rs.getInt("act_id"), getActivityDetail(rs));
				activities.add(activity);
			}

			logger.info("Exiting getActivitiesLite() with number of activities of " + activities.size());

			return activities;
		} catch (Exception e) {
			logger.error("Exception in getActivitiesLite" + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * get Activities for People
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List getActivitiesForPeople(String peopleId) throws Exception {
		logger.info("getActivitiesForPeople(" + peopleId + ")");

		RowSet rs = null;
		List activities = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select a.act_id, a.act_nbr,val.dl_address,lat.description as activity_type,a.description as activity_description,las.description as activity_status,p.lso_id from activity a join activity_people ap on a.act_id=ap.act_id and ap.people_id=" + peopleId + " left outer join lkup_act_type lat on a.act_type=lat.type left outer join lkup_act_st las on a.status = las.status_id left outer join v_address_list val on a.addr_id=val.addr_id left outer join sub_project sp on sp.sproj_id=a.sproj_id left outer join project p on p.proj_id=sp.proj_id order by a.created_by desc";
			logger.debug(sql);
			rs = db.select(sql);

			while (rs != null && rs.next()) {
				ActivityLite activity = new ActivityLite();
				activity.setActivityId(rs.getInt("act_id"));
				activity.setLsoId(rs.getInt("lso_id"));
				activity.setActivityNumber(rs.getString("act_nbr"));
				activity.setActivityDescription(rs.getString("activity_description"));
				activity.setAddress(rs.getString("dl_address"));
				activity.setActivityType(rs.getString("activity_type"));
				activity.setActivityStatus(rs.getString("activity_status"));
				activities.add(activity);
			}

			logger.info("Exiting getActivities()");

			return activities;
		} catch (Exception e) {
			logger.error("Exception in getActivities " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Creates the activity number for a activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getActivtiyNumber(int activityId) throws Exception {
		String activityNumber = "";
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select act_nbr  from activity where act_id=" + activityId;
			logger.debug(sql);
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				activityNumber = rs.getString("act_nbr");
			}

			return activityNumber;
		} catch (Exception e) {
			logger.error("Exception in -- Activity Agent  --  activity Number -- " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Saves the activity details to the database.
	 * 
	 * @param activity
	 * @return
	 * @throws Exception
	 */
	public int saveActivityDetails(Activity activity) throws Exception {
		ActivityDetail activityDetail = new ActivityDetail();
		try {
			Wrapper db = new Wrapper();
			activityDetail = activity.getActivityDetail();

			String sql = "";
			sql = "update activity set valuation=" + activityDetail.getValuation() + ",addr_id=" + StringUtils.checkNumber(activityDetail.getAddressId()) + ",plan_chk_req=" + StringUtils.checkString(activityDetail.getPlanCheckRequired()) + ",dev_fee_req=" + StringUtils.checkString(activityDetail.getDevelopmentFeeRequired()) + ",status=" + activityDetail.getStatus().getStatus() + ",issued_date=" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getIssueDate())) + ",start_date=" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getStartDate())) + ",applied_date=" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getStartDate())) + ",exp_date=" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getExpirationDate())) + ",final_date=" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getFinaledDate())) + ",description=" + StringUtils.checkString(activityDetail.getDescription()) + ",microfilm=" + StringUtils.checkString(activityDetail.getMicrofilm()) + ",updated=current_timestamp,updated_by=" + activityDetail.getUpdatedBy().getUserId() + "  where act_id=" + activity.getActivityId();
			logger.debug(sql);
			db.update(sql);

			return activity.getActivityId();
		} catch (Exception e) {
			logger.error("Exception in -- activityAgent --  saveActivityDetails()  -- " + e.getMessage());
			throw e;
		}
	}

	/**
	 * gets the save activity details sql.
	 * 
	 * @param activity
	 * @return
	 * @throws Exception
	 */
	public String getSaveActivityDetailsSql(Activity activity) throws Exception {
		String sql = "";
		ActivityDetail activityDetail = activity.getActivityDetail();

		sql = "update activity set  valuation=" + activityDetail.getValuation() + ",plan_chk_req=" + StringUtils.checkString(activityDetail.getPlanCheckRequired()) + ",dev_fee_req=" + StringUtils.checkString(activityDetail.getDevelopmentFeeRequired()) + ",status=" + activityDetail.getStatus().getStatus() + ",issued_date=to_date(" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getIssueDate())) + ",'MM/DD/YYYY'),start_date=to_date(" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getStartDate())) + ",'MM/DD/YYYY'),applied_date=to_date(" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getStartDate())) + ",'MM/DD/YYYY'),exp_date=to_date(" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getExpirationDate())) + ",'MM/DD/YYYY'),final_date=to_date(" + StringUtils.checkString(StringUtils.cal2str(activityDetail.getFinaledDate())) + ",'MM/DD/YYYY'),description=" + StringUtils.checkString(activityDetail.getDescription()) + ",label=" + StringUtils.checkString(activityDetail.getLabel()) + ",microfilm=" + StringUtils.checkString(activityDetail.getMicrofilm()) + ",updated=current_timestamp,updated_by=" + activityDetail.getUpdatedBy().getUserId() + ",new_units=" + StringUtils.checkString(activityDetail.getNewUnits()) + ",existing_units=" + StringUtils.checkString(activityDetail.getExistingUnits()) + ",demolished_units=" + StringUtils.checkString(activityDetail.getDemolishedUnits()) + ",pzone_id=" + (activityDetail.getParkingZone() != null ? (activityDetail.getParkingZone().getPzoneId() != null ? activityDetail.getParkingZone().getPzoneId() : null) : null) + ", green_halo='"+ activityDetail.getGreenHalo() +"'  where act_id=" + activity.getActivityId();
		logger.debug(sql);

		return sql;
	}

	/**
	 * finals the activity.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public int finalActivity(String activityId) throws Exception {
		int rs = 0;

		try {
			Wrapper db = new Wrapper();
			String sql = "";
			sql = "update activity set status = " + Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL + ",final_date = current_date,updated = current_timestamp where act_id=" + activityId;
			logger.debug(sql);
			rs = db.update(sql);

			return rs;
		} catch (Exception e) {
			logger.error("Exception in finalActivity  -- " + e.getMessage());
			throw e;
		}
	}

	/**
	 * check if the inspection is required or not.
	 * 
	 * @param activityType
	 * @return
	 * @throws Exception
	 */
	public String inspectionRequired(String activityType) throws Exception {
		String inspectionRequired = "N";
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select count(*) from lkup_inspct_item where act_type ='" + activityType + "'";
			logger.info(sql);
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				inspectionRequired = "Y";
			}

			return inspectionRequired;
		} catch (Exception e) {
			logger.error("Exception in -- Activity Agent  --  Inspection Required -- " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	/**
	 * Get the planning activities for a given activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public PlanningActivitiesForm getPlanningActivities(int activityId) throws Exception {
		PlanningActivitiesForm planningActivitiesForm = new PlanningActivitiesForm();

		try {
			planningActivitiesForm.setActivityId(StringUtils.i2s(activityId));

			Wrapper db = new Wrapper();
			String sql = "select * from plan_act where act_id=" + activityId;
			RowSet rs = db.select(sql);

			if (rs.next()) {
				if ((rs.getInt("din_nbr_tables") == 0) || (rs.getInt("din_nbr_tables") == -1)) {
					planningActivitiesForm.setNoOfTables("");
				} else {
					planningActivitiesForm.setNoOfTables(rs.getString("din_nbr_tables"));
				}

				logger.debug("*" + planningActivitiesForm.getNoOfTables());

				if ((rs.getInt("din_nbr_chairs") == 0) || (rs.getInt("din_nbr_chairs") == -1)) {
					planningActivitiesForm.setNoOfChairs("");
				} else {
					planningActivitiesForm.setNoOfChairs(rs.getString("din_nbr_chairs"));
				}

				logger.debug("**" + planningActivitiesForm.getNoOfChairs());

				if ((rs.getInt("din_sq_ft") == 0) || (rs.getInt("din_sq_ft") == -1)) {
					planningActivitiesForm.setSquareFeet("");
				} else {
					planningActivitiesForm.setSquareFeet(rs.getString("din_sq_ft"));
				}

				logger.debug("***" + planningActivitiesForm.getSquareFeet());

				if (rs.getString("din_hours_open") == null) {
					planningActivitiesForm.setHoursOpen("");
				} else {
					planningActivitiesForm.setHoursOpen(rs.getString("din_hours_open"));
				}

				logger.debug("****" + planningActivitiesForm.getHoursOpen());

				if (rs.getString("din_alcohol_bev") == null) {
					planningActivitiesForm.setAlcoholBev("");
				} else {
					planningActivitiesForm.setAlcoholBev(rs.getString("din_alcohol_bev"));
				}

				logger.debug("*****" + planningActivitiesForm.getAlcoholBev());

				if (rs.getString("din_council_appr") == null) {
					planningActivitiesForm.setCouncilApproval("");
				} else {
					planningActivitiesForm.setCouncilApproval(rs.getString("din_council_appr"));
				}

				logger.debug("******" + planningActivitiesForm.getCouncilApproval());

				if (rs.getString("ext_mf_hrs") == null) {
					planningActivitiesForm.setMondayThruFriday("");
				} else {
					planningActivitiesForm.setMondayThruFriday(rs.getString("ext_mf_hrs"));
				}

				logger.debug("*******" + planningActivitiesForm.getMondayThruFriday());

				if (rs.getString("ext_ss_hrs") == null) {
					planningActivitiesForm.setSaturdayAndSunday("");
				} else {
					planningActivitiesForm.setSaturdayAndSunday(rs.getString("ext_ss_hrs"));
				}

				logger.debug("********" + planningActivitiesForm.getSaturdayAndSunday());

				if ((rs.getInt("park_reqd") == 0) || (rs.getInt("park_reqd") == -1)) {
					planningActivitiesForm.setNoOfParkingSpacesReq("");
				} else {
					planningActivitiesForm.setNoOfParkingSpacesReq(rs.getString("park_reqd"));
				}

				logger.debug("*********" + planningActivitiesForm.getNoOfParkingSpacesReq());

				if ((rs.getInt("park_provided") == 0) || (rs.getInt("park_reqd") == -1)) {
					planningActivitiesForm.setNoOfParkingSpacesProv("");
				} else {
					planningActivitiesForm.setNoOfParkingSpacesProv(rs.getString("park_provided"));
				}

				logger.debug("**********" + planningActivitiesForm.getNoOfParkingSpacesProv());

				if ((rs.getInt("park_spaces") == 0) || (rs.getInt("park_spaces") == -1)) {
					planningActivitiesForm.setNoOfParkingSpacesUnderInlieu("");
				} else {
					planningActivitiesForm.setNoOfParkingSpacesUnderInlieu(rs.getString("park_spaces"));
				}

				logger.debug("***********" + planningActivitiesForm.getNoOfParkingSpacesUnderInlieu());

				if ((rs.getInt("ovnt_beds") == 0) || (rs.getInt("ovnt_beds") == -1)) {
					planningActivitiesForm.setNoOfBeds("");
				} else {
					planningActivitiesForm.setNoOfBeds(rs.getString("ovnt_beds"));
				}

				logger.debug("************" + planningActivitiesForm.getNoOfBeds());

				if ((rs.getInt("daycare_kids") == 0) || (rs.getInt("daycare_kids") == -1)) {
					planningActivitiesForm.setNoOfKids("");
				} else {
					planningActivitiesForm.setNoOfKids(rs.getString("daycare_kids"));
				}

				logger.debug("*************" + planningActivitiesForm.getNoOfKids());

				if (rs.getString("comments") == null) {
					planningActivitiesForm.setComments(rs.getString("comments"));
				} else {
					planningActivitiesForm.setComments(rs.getString("comments"));
				}

				logger.debug("**************" + planningActivitiesForm.getComments());
			}

			rs.close();
			rs = null;

			return planningActivitiesForm;
		} catch (Exception e) {
			logger.error("Exception in -- activityAgent --  getPalnningActivities()  -- " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get teh planning activities sqls
	 * 
	 * @param planningActivitiesForm
	 * @return
	 * @throws Exception
	 */
	public List getSavePlanningActivitiesSqls(PlanningActivitiesForm planningActivitiesForm) throws Exception {
		List sqls = new ArrayList();
		int activityId = -1;
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			activityId = StringUtils.s2i(planningActivitiesForm.getActivityId());

			// check the record in the table and if not found insert it
			sql = "select count(*) as count from plan_act where act_id=" + activityId;

			RowSet rs = db.select(sql);

			if (rs.next()) {
				if (rs.getInt("count") == 0) {
					sql = "insert into plan_act(act_id) values( " + activityId + ")";
					sqls.add(sql);
				}
			}

			sql = "update plan_act  set  " + "din_nbr_tables=" + StringUtils.s2i(planningActivitiesForm.getNoOfTables()) + "," + "din_nbr_chairs=" + StringUtils.s2i(planningActivitiesForm.getNoOfChairs()) + "," + "din_sq_ft=" + StringUtils.s2i(planningActivitiesForm.getSquareFeet()) + "," + "din_hours_open=" + StringUtils.checkString(planningActivitiesForm.getHoursOpen()) + "," + "din_alcohol_bev=" + StringUtils.checkString(planningActivitiesForm.getAlcoholBev()) + "," + "din_council_appr=" + StringUtils.checkString(planningActivitiesForm.getCouncilApproval()) + "," + "ext_mf_hrs=" + StringUtils.checkString(planningActivitiesForm.getMondayThruFriday()) + "," + "ext_ss_hrs=" + StringUtils.checkString(planningActivitiesForm.getSaturdayAndSunday()) + "," + "park_reqd=" + StringUtils.s2i(planningActivitiesForm.getNoOfParkingSpacesReq()) + "," + "park_provided=" + StringUtils.s2i(planningActivitiesForm.getNoOfParkingSpacesProv()) + "," + "park_spaces=" + StringUtils.s2i(planningActivitiesForm.getNoOfParkingSpacesUnderInlieu()) + "," + "ovnt_beds=" + StringUtils.s2i(planningActivitiesForm.getNoOfBeds()) + "," + "daycare_kids=" + StringUtils.s2i(planningActivitiesForm.getNoOfKids()) + "," + "comments=" + StringUtils.checkString(planningActivitiesForm.getComments()) + "  where act_Id=" + activityId;
			logger.debug(sql);
			sqls.add(sql);

			return sqls;
		} catch (Exception e) {
			logger.error("Exception in -- activityAgent --  saveOutdoorDiningActivity()  -- " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of activities after the search.
	 * 
	 * @param activityNumber
	 * @param activityType
	 * @param projectNumber
	 * @param projectName
	 * @param streetNum
	 * @param streetID
	 * @param apn
	 * @param name
	 * @param peopleName
	 * @param peopleLic
	 * @param personType
	 * @param personInfoType
	 * @param projectTypeId
	 * @param subProjectTypeId
	 * @param subProjectSubTypeId
	 * @param streetName
	 * @return
	 * @throws Exception
	 */

	public int searchActivitiesCount(String activityNumber, String[] activityType, String[] activitySubType, String projectNumber, String projectName, String streetNum, String streetID, String apn, String name, String peopleName, String peopleLic, String personType, String personInfoType, String projectTypeId, String subProjectTypeId, String subProjectSubTypeId, String streetName, String[] activityStatus, String ownerName1, String ownerName2, String crossStreetName1, String crossStreetName2, String landmark) throws Exception {
		logger.info("searchActivitiesCount(" + activityNumber + "," + activityType + "," + activitySubType + ", " + projectNumber + ", " + projectName + ", " + streetNum + ", " + streetID + ", " + apn + ", " + name + ", " + peopleName + ", " + peopleLic + ", " + personType + ", " + personInfoType + ", " + projectTypeId + ", " + subProjectTypeId + ", " + subProjectSubTypeId + ", " + streetName + ", " + activityStatus + "," + ownerName1 + "," + ownerName2 + "," + crossStreetName1 + "," + crossStreetName2 + "," + landmark + ")");

		String actSubTypeStr = "";
		int count = 0;
		// get intersection details.
		int addressId = -1;
		if (!(crossStreetName1.equalsIgnoreCase("-1"))) {
			addressId = new AddressAgent().getAddressIdForCrossStreet(crossStreetName1, crossStreetName2);
		}
		logger.debug("Address Id is " + addressId);

		for (int i = 0; i < activitySubType.length; i++) {
			if (i == 0) {
				actSubTypeStr = actSubTypeStr + activitySubType[i];
			} else {
				actSubTypeStr = actSubTypeStr + "," + activitySubType[i];
			}
		}

		logger.debug("Activity Sub Type string is " + actSubTypeStr);

		String actStatusStr = StringUtils.stringArrayToCommaSeperatedString(activityStatus);

		logger.debug("Activity status string is " + actStatusStr);

		String query = "";
		String where = " WHERE ";

		String fullQuery = "SELECT count(*) as count ";

		fullQuery = fullQuery + "  FROM ((((((ACTIVITY A JOIN SUB_PROJECT SP ON SP.SPROJ_ID=A.SPROJ_ID) JOIN PROJECT P ON P.PROJ_ID=SP.PROJ_ID) JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE=A.ACT_TYPE) JOIN LKUP_ACT_ST LAS ON LAS.STATUS_ID=A.STATUS) LEFT OUTER JOIN V_ACTIVITY_ADDRESS V ON V.ACT_ID=A.ACT_ID) LEFT OUTER JOIN BL_ACTIVITY BL ON A.ACT_ID=BL.ACT_ID) LEFT OUTER JOIN BT_ACTIVITY BT ON A.ACT_ID=BT.ACT_ID";

		List list = new ArrayList();

		if ((streetName == null) || streetName.equals("")) {
			streetName = "-1";
		}

		query = streetName.equals("-1") ? query : (query + " V.STREET_ID=" + streetName + " AND");

		String formattedActivityNumber = StringUtils.formatQueryForString(activityNumber);

		if (!formattedActivityNumber.equals("%")) {
			query = ((formattedActivityNumber == null) || (formattedActivityNumber.indexOf("%") < 0)) ? (query + " UPPER(ACT_NBR) = '" + formattedActivityNumber.toUpperCase() + "'  AND ") : (query + " UPPER(ACT_NBR) LIKE '" + formattedActivityNumber.toUpperCase() + "'  AND ");
		}

		String actTypeStr = "";

		for (int i = 0; i < activityType.length; i++) {
			if (i == 0) {
				actTypeStr = actTypeStr + activityType[i];
			} else {
				actTypeStr = actTypeStr + "','" + activityType[i];
			}
		}

		logger.debug(" Activity Type string is " + actTypeStr);

		query = ((activityType == null) || (activityType.length == 0)) ? query : (query + " ACT_TYPE IN ('" + actTypeStr + "') AND ");

		query = ((activityStatus == null) || (activityStatus.length == 0)) ? query : (query + " LAS.STATUS_ID IN (" + actStatusStr + ") AND ");

		query = ((projectNumber == null) || projectNumber.equals("")) ? query : (query + " UPPER(P.PROJECT_NBR) LIKE '" + (StringUtils.formatQueryForString(projectNumber).toUpperCase()) + "' AND ");

		query = ((projectName == null) || projectName.equals("")) ? query : (query + " UPPER(P.NAME) LIKE '%" + StringUtils.formatQueryForString(projectName).toUpperCase() + "%' AND ");

		query = ((ownerName1 == null) || ownerName1.equals("")) ? query : (query + " UPPER(BT.NAME1) LIKE '%" + StringUtils.formatQueryForString(ownerName1).toUpperCase() + "%' AND ");

		query = ((ownerName2 == null) || ownerName2.equals("")) ? query : (query + " UPPER(BT.NAME2) LIKE '%" + StringUtils.formatQueryForString(ownerName2).toUpperCase() + "%' AND ");

		String formattedApn = StringUtils.formatQueryForString(apn);

		if (!formattedApn.equals("%")) {
			query = ((formattedApn == null) || (formattedApn.indexOf("%") < 0)) ? (query + " P.LSO_ID IN ( SELECT LSO_ID FROM LSO_APN WHERE APN = '" + formattedApn + "') AND ") : (query + " P.LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN LIKE '" + formattedApn + "') AND ");
		}

		// user name
		if ((name != null) && !name.equals("")) {
			query += (" A.ACT_ID IN (SELECT PSA_ID AS ACT_ID FROM PROCESS_TEAM WHERE PSA_TYPE = 'A' AND USERID =" + name);
			query += ")  AND ";
		}

		// people licenseNumber
		query = ((peopleLic == null) || peopleLic.equals("")) ? query : (query + " A.ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PSA_TYPE='A' and PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(LIC_NO) LIKE '" + StringUtils.formatQueryForString(peopleLic).toUpperCase() + "'))  AND ");

		// people name
		query = ((peopleName == null) || peopleName.equals("")) ? query : (query + " A.ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(NAME) LIKE '%" + StringUtils.formatQueryForString(peopleName).toUpperCase() + "%'))  AND ");

		// cross streets
		query = (addressId == -1) ? query : query + " A.ADDR_ID IN (" + addressId + ")  AND ";

		// landmark
		query = (landmark.equalsIgnoreCase("-1")) ? query : query + " A.ADDR_ID = " + landmark + "  AND ";

		query = ((streetID == null) || streetID.equals("-1")) ? query : (query + " P.LSO_ID IN (SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE RTRIM (CAST (STR_NO AS CHARACTER(40))) like '" + StringUtils.formatQueryForNumber(streetNum) + "' AND STREET_ID = " + streetID + ") AND ");

		try {
			query = ((projectTypeId == null) || projectTypeId.equals("")) ? query : (query + " SP.SPROJ_TYPE = " + (Integer.parseInt(projectTypeId)) + " AND ");
		} catch (NumberFormatException num) {
			logger.error("NumberFormatException in searchActivities - " + num.getMessage());
		}

		try {
			query = ((subProjectTypeId == null) || subProjectTypeId.equals("")) ? query : (query + " SP.STYPE_ID = " + (Integer.parseInt(subProjectTypeId)) + " AND ");
		} catch (NumberFormatException num) {
			logger.error("NumberFormatException in searchActivities - " + num.getMessage());
		}

		try {
			query = ((subProjectSubTypeId == null) || subProjectSubTypeId.equals("")) ? query : (query + " SP.SSTYPE_ID = " + (Integer.parseInt(subProjectSubTypeId)) + " AND ");
		} catch (NumberFormatException num) {
			logger.error("NumberFormatException in searchActivities - " + num.getMessage());
		}

		query += " 1=1 ";

		// query += " NOT A.ACT_TYPE LIKE 'PW%' AND NOT A.ACT_TYPE LIKE 'DOT%'
		// "; // Filter out all the Public Works Module activities
		if (activitySubType.length > 0) {
			logger.debug("activity sub type length > 0");
			query += (" and a.act_id in (select distinct act_id from act_subtype where act_subtype_id in(" + actSubTypeStr + ")) ");
		}

		fullQuery = fullQuery + where + query + " order by a.applied_date desc,a.act_id desc";

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();

			RowSet rs = db.select(fullQuery);

			if (rs != null)
				rs.beforeFirst();

			while (rs != null && rs.next()) {
				count = rs.getInt("COUNT");
			}
		} catch (Exception e) {
		}
		logger.debug("&&& Count is &&& " + count);
		return count;
	}

	/**
	 * Get the list of activities after the search.
	 * 
	 * @param activityNumber
	 * @param activityType
	 * @param projectNumber
	 * @param projectName
	 * @param streetNum
	 * @param streetID
	 * @param apn
	 * @param name
	 * @param peopleName
	 * @param peopleLic
	 * @param personType
	 * @param personInfoType
	 * @param projectTypeId
	 * @param subProjectTypeId
	 * @param subProjectSubTypeId
	 * @param streetName
	 * @return
	 * @throws Exception
	 */

	public List searchActivities(String activityNumber, String[] activityType, String[] activitySubType, String projectNumber, String projectName, String streetNum, String streetID, String apn, String name, String peopleName, String peopleLic, String personType, String personInfoType, String projectTypeId, String subProjectTypeId, String subProjectSubTypeId, String streetName, String[] activityStatus, String ownerName1, String ownerName2, String crossStreetName1, String crossStreetName2, String landmark) throws Exception {
		logger.info("searchActivities(" + activityNumber + "," + activityType + "," + activitySubType + ", " + projectNumber + ", " + projectName + ", " + streetNum + ", " + streetID + ", " + apn + ", " + name + ", " + peopleName + ", " + peopleLic + ", " + personType + ", " + personInfoType + ", " + projectTypeId + ", " + subProjectTypeId + ", " + subProjectSubTypeId + ", " + streetName + ", " + activityStatus + "," + ownerName1 + "," + ownerName2 + "," + crossStreetName1 + "," + crossStreetName2 + "," + landmark + ")");

		String actSubTypeStr = "";

		for (int i = 0; i < activitySubType.length; i++) {
			if (i == 0) {
				actSubTypeStr = actSubTypeStr + activitySubType[i];
			} else {
				actSubTypeStr = actSubTypeStr + "," + activitySubType[i];
			}
		}

		logger.debug("Activity Sub Type string is " + actSubTypeStr);

		String actStatusStr = StringUtils.stringArrayToCommaSeperatedString(activityStatus);

		logger.debug("Activity status string is " + actStatusStr);

		// get intersection details.
		int addressId = -1;
		if (!(crossStreetName1.equalsIgnoreCase("-1"))) {
			addressId = new AddressAgent().getAddressIdForCrossStreet(crossStreetName1, crossStreetName2);
		}
		logger.debug("Address Id is " + addressId);

		String query = "";
		String where = " WHERE ";

		String fullQuery = "SELECT distinct P.PROJ_ID,P.LSO_ID,A.*,V.DOT_ADDRESS,LAT.description AS ACT_TYPE_DESC,LAS.STAT_CODE AS ACT_STS_CODE,LAS.description as ACT_STS_DESC,BL.BUSINESS_NAME AS BUSINESS_NAME, BL.BUSINESS_ACC_NO AS BUSINESS_ACC_NO, BL.OOT_STR_NO AS OOT_STR_NO, BL.OOT_STR_NAME AS OOT_STR_NAME, BL.OOT_UNIT AS OOT_UNIT, BL.OOT_CITY AS OOT_CITY, BL.OOT_STATE AS OOT_STATE, BL.OOT_ZIP AS OOT_ZIP, BL.OOT_ZIP4 AS OOT_ZIP4, BT.BUSINESS_NAME AS BT_BUSINESS_NAME,BT.NAME1 AS OWNER1,BT.NAME2 AS OWNER2, BT.BUSINESS_ACC_NO AS BT_BUSINESS_ACC_NO, BT.OOT_STR_NO AS BT_OOT_STR_NO, BT.OOT_STR_NAME AS BT_OOT_STR_NAME, BT.OOT_UNIT AS BT_OOT_UNIT, BT.OOT_CITY AS BT_OOT_CITY, BT.OOT_STATE AS BT_OOT_STATE, BT.OOT_ZIP AS BT_OOT_ZIP, BT.OOT_ZIP4 AS BT_OOT_ZIP4 ";

		fullQuery = fullQuery + "  FROM ((((((ACTIVITY A JOIN SUB_PROJECT SP ON SP.SPROJ_ID=A.SPROJ_ID) JOIN PROJECT P ON P.PROJ_ID=SP.PROJ_ID) JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE=A.ACT_TYPE) JOIN LKUP_ACT_ST LAS ON LAS.STATUS_ID=A.STATUS) LEFT OUTER JOIN V_ACTIVITY_ADDRESS V ON V.ACT_ID=A.ACT_ID) LEFT OUTER JOIN BL_ACTIVITY BL ON A.ACT_ID=BL.ACT_ID) LEFT OUTER JOIN BT_ACTIVITY BT ON A.ACT_ID=BT.ACT_ID";

		List list = new ArrayList();

		if ((streetName == null) || streetName.equals("")) {
			streetName = "-1";
		}

		query = streetName.equals("-1") ? query : (query + " V.STREET_ID=" + streetName + " AND");

		String formattedActivityNumber = StringUtils.formatQueryForString(activityNumber);

		if (!formattedActivityNumber.equals("%")) {
			query = ((formattedActivityNumber == null) || (formattedActivityNumber.indexOf("%") < 0)) ? (query + " UPPER(ACT_NBR) = '" + formattedActivityNumber.toUpperCase() + "'  AND ") : (query + " UPPER(ACT_NBR) LIKE '" + formattedActivityNumber.toUpperCase() + "'  AND ");
		}

		String actTypeStr = "";

		for (int i = 0; i < activityType.length; i++) {
			if (i == 0) {
				actTypeStr = actTypeStr + activityType[i];
			} else {
				actTypeStr = actTypeStr + "','" + activityType[i];
			}
		}

		logger.debug(" Activity Type string is " + actTypeStr);

		query = ((activityType == null) || (activityType.length == 0)) ? query : (query + " ACT_TYPE IN ('" + actTypeStr + "') AND ");

		query = ((activityStatus == null) || (activityStatus.length == 0)) ? query : (query + " LAS.STATUS_ID IN (" + actStatusStr + ") AND ");

		query = ((projectNumber == null) || projectNumber.equals("")) ? query : (query + " UPPER(P.PROJECT_NBR) LIKE '" + (StringUtils.formatQueryForString(projectNumber).toUpperCase()) + "' AND ");

		query = ((projectName == null) || projectName.equals("")) ? query : (query + " UPPER(P.NAME) LIKE '%" + StringUtils.formatQueryForString(projectName).toUpperCase() + "%' AND ");

		query = ((ownerName1 == null) || ownerName1.equals("")) ? query : (query + " UPPER(BT.NAME1) LIKE '%" + StringUtils.formatQueryForString(ownerName1).toUpperCase() + "%' AND ");

		query = ((ownerName2 == null) || ownerName2.equals("")) ? query : (query + " UPPER(BT.NAME2) LIKE '%" + StringUtils.formatQueryForString(ownerName2).toUpperCase() + "%' AND ");

		String formattedApn = StringUtils.formatQueryForString(apn);

		if (!formattedApn.equals("%")) {
			query = ((formattedApn == null) || (formattedApn.indexOf("%") < 0)) ? (query + " P.LSO_ID IN ( SELECT LSO_ID FROM LSO_APN WHERE APN = '" + formattedApn + "') AND ") : (query + " P.LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN LIKE '" + formattedApn + "') AND ");
		}

		// user name
		if ((name != null) && !name.equals("")) {
			query += (" A.ACT_ID IN (SELECT PSA_ID AS ACT_ID FROM PROCESS_TEAM WHERE PSA_TYPE = 'A' AND USERID =" + name);
			query += ")  AND ";
		}

		// people licenseNumber
		query = ((peopleLic == null) || peopleLic.equals("")) ? query : (query + " A.ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PSA_TYPE='A' and PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(LIC_NO) LIKE '" + StringUtils.formatQueryForString(peopleLic).toUpperCase() + "'))  AND ");

		// people name
		query = ((peopleName == null) || peopleName.equals("")) ? query : (query + " A.ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(NAME) LIKE '%" + StringUtils.formatQueryForString(peopleName).toUpperCase() + "%'))  AND ");

		// cross streets
		query = (addressId == -1) ? query : query + " A.ADDR_ID IN (" + addressId + ")  AND ";

		query = ((streetID == null) || streetID.equals("-1")) ? query : (query + " P.LSO_ID IN (SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE RTRIM (CAST (STR_NO AS CHARACTER(40))) like '" + StringUtils.formatQueryForNumber(streetNum) + "' AND STREET_ID = " + streetID + ") AND ");

		// landmark
		query = (landmark.equalsIgnoreCase("-1")) ? query : query + " A.ADDR_ID = " + landmark + "  AND ";

		try {
			query = ((projectTypeId == null) || projectTypeId.equals("")) ? query : (query + " SP.SPROJ_TYPE = " + (Integer.parseInt(projectTypeId)) + " AND ");
		} catch (NumberFormatException num) {
			logger.error("NumberFormatException in searchActivities - " + num.getMessage());
		}

		try {
			query = ((subProjectTypeId == null) || subProjectTypeId.equals("")) ? query : (query + " SP.STYPE_ID = " + (Integer.parseInt(subProjectTypeId)) + " AND ");
		} catch (NumberFormatException num) {
			logger.error("NumberFormatException in searchActivities - " + num.getMessage());
		}

		try {
			query = ((subProjectSubTypeId == null) || subProjectSubTypeId.equals("")) ? query : (query + " SP.SSTYPE_ID = " + (Integer.parseInt(subProjectSubTypeId)) + " AND ");
		} catch (NumberFormatException num) {
			logger.error("NumberFormatException in searchActivities - " + num.getMessage());
		}

		query += " 1=1 ";

		if (activitySubType.length > 0) {
			logger.debug("activity sub type length > 0");
			query += (" and a.act_id in (select distinct act_id from act_subtype where act_subtype_id in(" + actSubTypeStr + ")) ");
		}

		fullQuery = fullQuery + where + query + " order by a.applied_date desc,a.act_id desc";

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();

			int activityId = -1;
			RowSet rs = db.select(fullQuery);

			if (rs != null)
				rs.beforeFirst();

			while (rs != null && rs.next()) {
				activityId = Integer.parseInt(rs.getString("ACT_ID"));

				ActivityDetail activityDetail = new ActivityDetail();

				activityDetail.setActivityType(new ActivityType(rs.getString("ACT_TYPE"), rs.getString("ACT_TYPE_DESC")));
				activityDetail.setStatus(new ActivityStatus(rs.getInt("STATUS"), rs.getString("ACT_STS_CODE"), rs.getString("ACT_STS_DESC")));
				activityDetail.setDescription(rs.getString("description"));
				activityDetail.setActivityNumber(rs.getString("ACT_NBR"));
				activityDetail.setLsoId(rs.getInt("LSO_ID"));
				activityDetail.setAppliedDate(rs.getDate("APPLIED_DATE"));
				activityDetail.setIssueDate(rs.getDate("ISSUED_DATE"));
				activityDetail.setAddressId(rs.getInt("addr_id"));
				activityDetail.setAddress(rs.getString("DOT_ADDRESS"));

				if ((((StringUtils.nullReplaceWithEmpty(rs.getString("DOT_ADDRESS"))).replaceAll(",", "")).trim().toUpperCase()).endsWith("CITYWIDE")) {

					activityDetail.setAddress(((rs.getString("OOT_STR_NO")) != null ? (rs.getString("OOT_STR_NO")) : "") + " " + ((rs.getString("OOT_STR_NAME")) != null ? (rs.getString("OOT_STR_NAME")) : "") + " " + ((rs.getString("OOT_UNIT")) != null ? (rs.getString("OOT_UNIT")) : "") + " " + ((rs.getString("OOT_CITY")) != null ? (rs.getString("OOT_CITY")) : "") + " " + ((rs.getString("OOT_STATE")) != null ? (rs.getString("OOT_STATE")) : "") + " " + ((rs.getString("OOT_ZIP")) != null ? (rs.getString("OOT_ZIP")) : "") + " " + ((rs.getString("OOT_ZIP4")) != null ? (rs.getString("OOT_ZIP4")) : ""));
					if ((activityDetail.getAddress()).trim().equalsIgnoreCase("")) {
						activityDetail.setAddress(((rs.getString("BT_OOT_STR_NO")) != null ? (rs.getString("BT_OOT_STR_NO")) : "") + " " + ((rs.getString("BT_OOT_STR_NAME")) != null ? (rs.getString("BT_OOT_STR_NAME")) : "") + " " + ((rs.getString("BT_OOT_UNIT")) != null ? (rs.getString("BT_OOT_UNIT")) : "") + " " + ((rs.getString("BT_OOT_CITY")) != null ? (rs.getString("BT_OOT_CITY")) : "") + " " + ((rs.getString("BT_OOT_STATE")) != null ? (rs.getString("BT_OOT_STATE")) : "") + " " + ((rs.getString("BT_OOT_ZIP")) != null ? (rs.getString("BT_OOT_ZIP")) : "") + " " + ((rs.getString("BT_OOT_ZIP4")) != null ? (rs.getString("BT_OOT_ZIP4")) : ""));
					}
				}
				activityDetail.setActivitySubTypes(LookupAgent.getSubTypes(getMultipleActivitySubTypes(rs.getInt("act_id"))));

				Activity activity = new Activity(activityId, activityDetail);

				BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();

				businessLicenseActivity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_NAME")));

				if (rs.getString("BUSINESS_NAME") != null) {
					businessLicenseActivity.setBusinessName(rs.getString("BUSINESS_NAME").toUpperCase());
				}
				if (businessLicenseActivity.getBusinessName().trim().equalsIgnoreCase("")) {
					if (!(("").equalsIgnoreCase(StringUtils.nullReplaceWithEmpty(rs.getString("BT_BUSINESS_NAME"))))) {
						businessLicenseActivity.setBusinessName(StringUtils.nullReplaceWithEmpty(rs.getString("BT_BUSINESS_NAME").toUpperCase()));
					}
				}
				businessLicenseActivity.setBusinessAccountNumber(StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ACC_NO")));
				if (businessLicenseActivity.getBusinessAccountNumber().trim().equalsIgnoreCase("")) {
					businessLicenseActivity.setBusinessAccountNumber(StringUtils.nullReplaceWithEmpty(rs.getString("BT_BUSINESS_ACC_NO")));
				}
				businessLicenseActivity.setBusinessOwnerName(StringUtils.nullReplaceWithEmpty(this.getBusinessOwnerName(StringUtils.i2s(activityId))));
				if (businessLicenseActivity.getBusinessOwnerName().trim().equalsIgnoreCase("")) {
					businessLicenseActivity.setBusinessOwnerName(StringUtils.nullReplaceWithEmpty(this.getBusinessOwnerNameForBT(StringUtils.i2s(activityId))));
				}
				BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
				businessTaxActivity.setName1(StringUtils.nullReplaceWithEmpty(rs.getString("OWNER1")));
				businessTaxActivity.setName2(StringUtils.nullReplaceWithEmpty(rs.getString("OWNER2")));

				Owner owner = new Owner();
				owner.setName(StringUtils.nullReplaceWithEmpty(AddressAgent.getOwnerName(rs.getInt("LSO_ID"))));
				activity.setBusinessLicenseActivity(businessLicenseActivity);
				activity.setBusinessTaxActivity(businessTaxActivity);
				activity.setOwner(owner);
				list.add(activity);

			}
			logger.debug("returning list of size " + list.size());
			return list;
		} catch (Exception e) {
			logger.debug("Exception in method searchActivities - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of search results for the query
	 * 
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public List searchActivities(String query) throws Exception {
		logger.info("searchActivities(" + query + ")");

		List list = new ArrayList();

		try {
			Wrapper db = new Wrapper();

			String sql = "SELECT P.LSO_ID,A.ACT_TYPE,A.STATUS,A.description,A.ACT_NBR,A.ACT_ID FROM PROJECT P,SUB_PROJECT SP,ACTIVITY A WHERE SP.SPROJ_ID=A.SPROJ_ID AND P.PROJ_ID=SP.PROJ_ID AND A.ACT_ID IN (" + StringUtils.replace(query, "*", "ACT_ID") + ")";
			logger.debug("Search Query " + sql);

			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				ActivityDetail activityDetail = new ActivityDetail();

				activityDetail.setActivityType(new ActivityType(rs.getString("ACT_TYPE"), ""));
				activityDetail.setStatus(new ActivityStatus(rs.getInt("STATUS"), "", ""));
				activityDetail.setDescription(rs.getString("description"));
				activityDetail.setActivityNumber(rs.getString("ACT_NBR"));
				activityDetail.setLsoId(rs.getInt("LSO_ID"));

				Activity activity = new Activity(rs.getInt("ACT_ID"), activityDetail);

				list.add(activity);
			}

			if (rs != null) {
				rs.close();
			}

			return list;
		} catch (Exception e) {
			logger.debug("Exception in method searchActivities - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the project id for a corresponding activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getProjectId(String activityId) throws Exception {
		String projectId = "";

		try {
			Wrapper db = new Wrapper();

			String sql = "SELECT PROJECT.PROJ_ID AS PROJ_ID FROM PROJECT, SUB_PROJECT, ACTIVITY WHERE SUB_PROJECT.PROJ_ID = PROJECT.PROJ_ID AND ACTIVITY.SPROJ_ID = SUB_PROJECT.SPROJ_ID AND ACTIVITY.ACT_ID =" + activityId;
			logger.debug("Search Query " + sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				projectId = rs.getString("PROJ_ID");
			}

			if (rs != null) {
				rs.close();
			}

			return projectId;
		} catch (Exception e) {
			logger.debug("Exception in method searchActivities - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of sub projects for given criteria
	 * 
	 * @param projectNumber
	 * @param projectName
	 * @param name
	 * @param peopleName
	 * @param peopleLic
	 * @param personType
	 * @param personInfoType
	 * @param streetNum
	 * @param streetID
	 * @param apn
	 * @param projectTypeId
	 * @param subProjectTypeId
	 * @param subProjectSubTypeId
	 * @return
	 * @throws Exception
	 */
	public List searchSubProjects(String projectNumber, String projectName, String name, String peopleName, String peopleLic, String personType, String personInfoType, String streetNum, String streetID, String apn, String subProjectNumber, String projectTypeId, String subProjectTypeId, String subProjectSubTypeId) throws Exception {
		String query = "";
		String where = " WHERE ";
		String fullQuery = "SELECT VAL.DL_ADDRESS,LST.DESCRIPTION AS SUB_PROJECT_TYPE,LSS.DESCRIPTION AS SUB_PROJECT_SUB_TYPE,P.PROJ_ID,P.LSO_ID,SP.* FROM SUB_PROJECT SP JOIN PROJECT P ON P.PROJ_ID=SP.PROJ_ID JOIN V_ADDRESS_LIST VAL ON VAL.LSO_ID=P.LSO_ID JOIN V_STREET_LIST VSL ON VSL.STREET_ID=VAL.STREET_ID LEFT OUTER JOIN LKUP_SPROJ_TYPE LST ON LST.SPROJ_TYPE_ID = SP.STYPE_ID LEFT OUTER JOIN LKUP_SPROJ_STYPE LSS ON LSS.SPROJ_STYPE_ID= SP.SSTYPE_ID ";

		List list = new ArrayList();

		query = ((projectNumber == null) || projectNumber.equals("")) ? query : (query + " UPPER(P.PROJECT_NBR) LIKE '" + (StringUtils.formatQueryForString(projectNumber).toUpperCase()) + "' AND");

		query = ((projectName == null) || projectName.equals("")) ? query : (query + " UPPER(P.NAME) LIKE '" + StringUtils.formatQueryForString(projectName).toUpperCase() + "' AND");

		// user name
		if ((name != null) && !name.equals("")) {
			query += (" SP.SPROJ_ID IN (SELECT PSA_ID FROM PROCESS_TEAM WHERE PSA_TYPE = 'S' AND USERID = " + name + ") AND ");
		}

		// projectTeam holds the licenseNumber
		query = ((peopleLic == null) || peopleLic.equals("")) ? query : (query + " SP.SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(LIC_NO) LIKE '" + StringUtils.formatQueryForString(peopleLic).toUpperCase() + "')))  AND");

		// people name
		query = ((peopleName == null) || peopleName.equals("")) ? query : (query + " SP.SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(NAME) LIKE '" + StringUtils.formatQueryForString(peopleName).toUpperCase() + "')))  AND");

		String formattedApn = StringUtils.formatQueryForString(apn);

		if (!formattedApn.equals("%")) {
			query = ((formattedApn == null) || (formattedApn.indexOf("%") < 0)) ? (query + " P.LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN = '" + formattedApn + "')  AND") : (query + " P.LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN LIKE '" + formattedApn + "'))  AND");
		}

		query = ((streetID == null) || streetID.equals("-1")) ? query : (query + " P.LSO_ID IN (SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE RTRIM (CAST (STR_NO AS CHARACTER(40))) like '" + StringUtils.formatQueryForNumber(streetNum) + "' AND STREET_ID = " + streetID + ")  AND");

		// Sub Project Number
		query = ((subProjectNumber == null) || subProjectNumber.equals("")) ? query : (query + " UPPER(SP.SPROJ_NBR) LIKE '" + (StringUtils.formatQueryForString(subProjectNumber).toUpperCase()) + "' AND");

		try {
			query = ((projectTypeId == null) || projectTypeId.equals("")) ? query : (query + " SP.SPROJ_TYPE = " + (Integer.parseInt(projectTypeId)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		try {
			query = ((subProjectTypeId == null) || subProjectTypeId.equals("")) ? query : (query + " SP.STYPE_ID = " + (Integer.parseInt(subProjectTypeId)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		try {
			query = ((subProjectSubTypeId == null) || subProjectSubTypeId.equals("")) ? query : (query + " SP.SSTYPE_ID = " + (Integer.parseInt(subProjectSubTypeId)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		// form the full query string
		if ((query != null) && !query.equals("")) {
			query = query.substring(0, query.length() - 4); // to remove the
			// trailing AND

			fullQuery = fullQuery + where + query + " ORDER BY SP.SPROJ_NBR desc,VSL.STREET_NAME,VAL.STR_NO,P.APPLIED_DT desc";
		}

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(fullQuery);
			SubProjectSubType subProjectSubType;
			rs.beforeFirst();
			SubProjectType subProjectType;
			while (rs.next()) {
				SubProjectDetail subProjectDetail = new SubProjectDetail();

				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(rs.getInt("STYPE_ID"));
				subProjectType.setSubProjectType(rs.getString("SUB_PROJECT_TYPE"));
				subProjectDetail.setSubProjectType(subProjectType);

				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("SSTYPE_ID"));
				subProjectSubType.setType(rs.getString("SUB_PROJECT_SUB_TYPE"));
				subProjectDetail.setSubProjectSubType(subProjectSubType);
				subProjectDetail.setSubProjectNumber(rs.getString("SPROJ_NBR"));
				subProjectDetail.setDescription(rs.getString("description"));
				subProjectDetail.setLsoId(rs.getInt("LSO_ID"));
				subProjectDetail.setAddress(rs.getString("DL_ADDRESS"));

				SubProject subProject = new SubProject(rs.getInt("SPROJ_ID"), subProjectDetail);
				list.add(subProject);

				// GenericValidator.i;
			}

			return list;
		} catch (Exception e) {
			logger.debug("Exception in method searchSubProjects - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Perform the copy activity function.
	 * 
	 * @param copyActivity
	 * @return
	 * @throws Exception
	 */
	public int performCopyActivity(CopyActivity copyActivity) throws Exception {
		logger.info("performCopyActivity(CopyActivity)");

		int activityId = 0;
		int oldActivityId;

		logger.debug("Entering performCopyActivity");
		oldActivityId = copyActivity.getActivity().getActivityId();

		Activity activity = getActivity(oldActivityId);
		ActivityDetail activityDetail = activity.getActivityDetail();
		Calendar today = Calendar.getInstance();
		Calendar expiryDate;

		if (activity.getActivityDetail().getActivityType().getType().startsWith("CLS")) {
			expiryDate = null;
		} else {
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 180);
			expiryDate = cal;
		}

		logger.debug("plan check checked" + copyActivity.isPlanChecks());

		if (copyActivity.isPlanChecks()) {
			activityDetail.setStatus(new ActivityStatus(7, "", ""));
			logger.debug("Status is set to 7");

			activityDetail.setPlanCheckRequired("Y");
			activityDetail.setPermitFeeDate(today);
			activityDetail.setPlanCheckFeeDate(today);
		} else {
			activityDetail.setStatus(new ActivityStatus(28, "", ""));
			logger.debug("Status is set to 28");
		}

		activityDetail.setStartDate(today);
		activityDetail.setExpirationDate(expiryDate);
		activityDetail.setIssueDate(today);
		activityDetail.setCompletionDate(null);
		activityDetail.setFinaledDate((Date) null);
		activityDetail.getCreatedBy().setUserId(copyActivity.getUserId());
		activityDetail.getUpdatedBy().setUserId(copyActivity.getUserId());
		activity.setActivityDetail(activityDetail);

		activityId = this.addActivity(activity);

		List sqlList = new ArrayList();

		String sql = "";
		int keyId = 0;
		int userId = copyActivity.getUserId();
		Wrapper db = new Wrapper();

		if (copyActivity.isFees()) {
			sql = "insert into activity_fee (activity_id,fee_id,people_id,fee_units,fee_valuation,fee_amnt,comments) (select " + activityId + ",fee_id,people_id,fee_units,fee_valuation,fee_amnt,comments " + " from activity_fee where activity_id = " + oldActivityId + ")";
			logger.debug(sql);
			sqlList.add(sql);
		}

		if (copyActivity.isConditions()) {
			try {
				sql = "select cond_id from conditions where cond_level='A' and level_id=" + oldActivityId;
				logger.debug(sql);

				RowSet rsConditions = db.select(sql);

				while (rsConditions.next()) {
					keyId = db.getNextId("CONDITION_ID");
					sql = "insert into conditions (select " + keyId + "," + activityId + ", cond_level,library_id,short_text,inspectable,warning,complete," + userId + ",current_timestamp," + userId + ",current_timestamp,approved_by,approve_dt,text  from conditions where cond_id = " + rsConditions.getString("COND_ID") + ")";
					logger.debug(sql);
					sqlList.add(sql);
				}

				rsConditions.close();
			} catch (Exception e) {
				logger.error("Error : " + e.getMessage());
			}
		}

		if (copyActivity.isComments()) {
			try {
				sql = "select comnt_id from comments where comnt_level='A' and level_id=" + oldActivityId;
				logger.debug(sql);

				RowSet rsComments = db.select(sql);

				while (rsComments.next()) {
					keyId = db.getNextId("COMMENT_ID");

					String comnt = commentSelect(db, rsComments.getString("COMNT_ID"));
					sql = "insert into comments (select " + keyId + "," + activityId + ", comnt_level,internal," + userId + ",current_timestamp," + userId + ",current_timestamp," + StringUtils.checkString(comnt) + " from comments where comnt_id =  " + rsComments.getString("COMNT_ID") + ")";
					logger.debug(sql);
					sqlList.add(sql);
				}

				rsComments.close();
			} catch (Exception e) {
				logger.error("Error : " + e.getMessage());
			}
		}

		if (copyActivity.isHolds()) {
			try {
				sql = "select hold_id from holds where hold_level='A' and level_id=" + oldActivityId;
				logger.debug(sql);

				RowSet rsHolds = db.select(sql);

				while (rsHolds.next()) {
					keyId = db.getNextId("HOLD_ID");
					sql = "insert into holds (select " + keyId + "," + activityId + ", hold_level,title,description,hold_type,hold_stat_dt,stat," + userId + ",current_timestamp," + userId + ",current_timestamp from holds where hold_id = " + rsHolds.getString("HOLD_ID") + ")";
					logger.debug(sql);
					sqlList.add(sql);
				}

				rsHolds.close();
			} catch (Exception e) {
				logger.error("Error : " + e.getMessage());
			}
		}

		if (copyActivity.isActivityPeople()) {
			sql = "insert into activity_people (act_id,people_id,psa_type) (select " + activityId + ",people_id,psa_type from activity_people where act_id = " + oldActivityId + " and psa_type='A')";
			logger.debug(sql);
			sqlList.add(sql);
		}

		if (copyActivity.isActivityTeam()) {
			sql = "insert into process_team (psa_id,psa_type,userid,group_id,lead,created_by,created,updated_by,updated) " + " (select " + activityId + ",psa_type,userid,group_id,lead," + userId + ",current_timestamp," + userId + ",current_timestamp from process_team where psa_id = " + oldActivityId + " and psa_type='A')";
			logger.debug(sql);
			sqlList.add(sql);
		}

		if (copyActivity.isPlanChecks()) {
			try {
				sql = "select plan_chk_id,plan_chk_date from plan_check where act_id=" + oldActivityId + " order by plan_chk_id desc,plan_chk_date desc";
				logger.debug(sql);

				RowSet rsPlanChecks = db.select(sql);

				if (rsPlanChecks.next()) {
					keyId = db.getNextId("PLANCHECK_ID");
					// insert plancheck to the new address
					sql = "insert into plan_check (select " + keyId + ",current_timestamp," + activityId + ", stat_code,engineer,comnt,cat_code,title_code," + userId + ",current_timestamp," + userId + ",current_timestamp from plan_check where plan_chk_id = " + rsPlanChecks.getString("PLAN_CHK_ID") + " )";
					logger.debug(sql);
					sqlList.add(sql);
				}

				rsPlanChecks.close();
			} catch (Exception e) {
				logger.error("Error : " + e.getMessage());
			}
		}

		for (int i = 0; i < sqlList.size(); i++) {
			try {
				db.update((String) sqlList.get(i));
			} catch (Exception e) {
				logger.error("Update Error : " + (String) sqlList.get(i));
			}
		}

		logger.debug("Exiting performCopyActivity");

		return activityId;
	}

	/**
	 * Gets the activity address given a sub project id.
	 * 
	 * @param subProjectId
	 * @return
	 * @throws Exception
	 */
	public List getActivityAddresses(String subProjectId) throws Exception {
		List addresses = new ArrayList();
		String sql = "";
		sql = "select la.addr_id as id,rtrim(cast(la.str_no as char(8))) || coalesce(' ' || la.str_mod,'') || " + " coalesce(' ' || sl.str_name,'') || coalesce(' ' || sl.str_type,'') || coalesce(' ' || sl.suf_dir,'') ||" + " coalesce(', ' || sl.pre_dir,'') || coalesce(' ' || la.unit,'') as address from lso_address la,street_list sl" + " where la.street_id = sl.street_id and la.lso_id = (select distinct lso_id from v_psa_list where sproj_id=" + subProjectId + ")";
		logger.debug(sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				LsoAddress address = new LsoAddress();
				address.setAddressId(rs.getInt("id"));
				String addressDesc = rs.getString("address");
				
				if(addressDesc !=null && addressDesc.trim().endsWith(","))
					addressDesc = addressDesc.replace(",", "");
				
				address.setDescription(addressDesc);
				addresses.add(address);
			}

			rs.close();

			return addresses;
		} catch (Exception e) {
			logger.debug("General Exception in getting activity address list " + e.getMessage());
			throw e;
		}
	}

	public String getDepartmentCode(int activityId) throws Exception {
		logger.info("Entering getDepartmentCode");

		String departmentCode;

		String sql = "select d.dept_code from (activity a join lkup_act_type l on l.type=a.act_type) join department d on d.dept_id=l.dept_id " + "where a.act_id = " + activityId;
		logger.debug(sql);

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			departmentCode = rs.getString("DEPT_CODE");
		} else {
			departmentCode = "";
		}

		rs.close();
		logger.info("Exiting getDepartmentCode");

		return departmentCode;
	}

	/**
	 * Gets the activity address for a given activity id
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getActivityAddressForId(int activityId) throws Exception {
		logger.info("getActivityAddressForId(" + activityId + ")");

		String address = "";

		int locationType = -1;
		try {
			locationType = getActivityNonLocationalType(activityId);
			if (locationType == 1) {
				address = getActivityAddressForAddress(activityId);
			} else if (locationType == 2) {
				address = getActivityAddressForCrossStreet(activityId);
			} else if (locationType == 3) {
				address = getActivityAddressForRange(activityId);
			} else if (locationType == 4) {
				address = getActivityAddressForLandmark(activityId);
			}
			return StringUtils.properCase(address);
		} catch (Exception e) {
			logger.debug("Exception thrown while getting activity address " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity address for a given activity id address
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getActivityAddressForAddress(int activityId) throws Exception {
		logger.info("getActivityAddressForAddress(" + activityId + ")");

		String address = "";
		String sql = "select dl_address as address from v_activity_address where act_id=" + activityId;

		try {
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				address = rs.getString(1);
			}
			return address;
		} catch (Exception e) {
			logger.debug("Exception thrown while getting getActivityAddressForIdAddress " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity address for a given activity id cross street
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getActivityAddressForCrossStreet(int activityId) throws Exception {
		logger.info("getActivityAddressForCrossStreet(" + activityId + ")");

		String address = "";
		String sql = "select vsl1.street_name || '  & ' || vsl2.street_name as address from v_street_list vsl1,v_street_list vsl2 where vsl1.street_id = (select street_id1 from lkup_cross_street where addr_id=(select addr_id from activity where act_id=" + activityId + ")) and vsl2.street_id=(select street_id2 from lkup_cross_street where addr_id=(select addr_id from activity where act_id=" + activityId + "))";

		try {
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				address = rs.getString(1);
			}
			return address;
		} catch (Exception e) {
			logger.debug("Exception thrown while getting activity getActivityAddressForCrossStreet " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity address for a given activity id address range
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getActivityAddressForRange(int activityId) throws Exception {
		logger.info("getActivityAddressForRange(" + activityId + ")");

		String address = "";
		String sql = "";
		String sql1 = "select aar.FROM_STR_NO1 || ' - ' || aar.TO_STR_NO1 ||' '|| vsl.street_name as address,aar.street_id1 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id1 where a.act_id=" + activityId;
		String sql2 = "select aar.FROM_STR_NO2 || ' - ' || aar.TO_STR_NO2 ||' '|| vsl.street_name as address,aar.street_id2 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id2 where a.act_id=" + activityId;
		String sql3 = "select aar.FROM_STR_NO3 || ' - ' || aar.TO_STR_NO3 ||' '|| vsl.street_name as address,aar.street_id3 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id3 where a.act_id=" + activityId;
		String sql4 = "select aar.FROM_STR_NO4 || ' - ' || aar.TO_STR_NO4 ||' '|| vsl.street_name as address,aar.street_id4 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id4 where a.act_id=" + activityId;
		String sql5 = "select aar.FROM_STR_NO5 || ' - ' || aar.TO_STR_NO5 ||' '|| vsl.street_name as address,aar.street_id5 from activity a join act_address_range aar on aar.act_id=a.act_id left outer join v_street_list vsl on vsl.street_id=aar.street_id5 where a.act_id=" + activityId;
		Wrapper db = new Wrapper();
		try {
			logger.info(sql);

			RowSet rs = db.select(sql1);
			if (rs.next()) {
				address = rs.getString(1);
			}
			rs = db.select(sql2);
			if (rs.next()) {
				if (!(rs.getString("street_id2").equalsIgnoreCase("-1"))) {
					address = address + " , " + rs.getString("address");
				}
			}
			rs = db.select(sql3);
			if (rs.next()) {
				if (!(rs.getString("street_id3").equalsIgnoreCase("-1"))) {
					address = address + " , " + rs.getString("address");
				}
			}
			rs = db.select(sql4);
			if (rs.next()) {
				if (!(rs.getString("street_id4").equalsIgnoreCase("-1"))) {
					address = address + " , " + rs.getString("address");
				}
			}
			rs = db.select(sql5);
			if (rs.next()) {
				if (!(rs.getString("street_id5").equalsIgnoreCase("-1"))) {
					address = address + " , " + rs.getString("address");
				}
			}
			return address;
		} catch (Exception e) {
			logger.debug("Exception thrown while getting activity getActivityAddressForRange " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity address for a given activity id address landmark
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getActivityAddressForLandmark(int activityId) throws Exception {
		logger.info("getActivityAddressForLandmark(" + activityId + ")");

		String address = "";
		String sql = "select landmark_name as address from lkup_landmark where addr_id = (select addr_id from activity where act_id=" + activityId + ")";

		try {
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				address = rs.getString(1);
			}
			return StringUtils.properCase(address);
		} catch (Exception e) {
			logger.debug("Exception thrown while getting activity getActivityAddressForLandmark " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Activity Non Location Type for Activity Id
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public int getActivityNonLocationalType(int activityId) throws Exception {
		logger.info("getActivityNonLocationalType(" + activityId + ")");

		String sql = "select location_type from activity where act_id=" + activityId;
		logger.debug(sql);
		int locationType = -1;
		try {
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				locationType = rs.getInt("location_type");
			}
			return locationType;
		} catch (Exception e) {
			logger.debug("Exception thrown while executing getActivityNonLocationalType() " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity address for a given address id.
	 * 
	 * @param addressId
	 * @return
	 */
	public LsoAddress getActivityAddress(int addressId) throws Exception {
		LsoAddress address = new LsoAddress();
		String sql = "";
		sql = "select la.addr_id as id,rtrim(cast(la.str_no as char(8))) || coalesce(' ' || la.str_mod,'') || " + " coalesce(' ' || sl.str_name,'') || coalesce(' ' || sl.str_type,'') || coalesce(' ' || sl.suf_dir,'') ||" + " coalesce(', ' || sl.pre_dir,'') || coalesce(' ' || la.unit,'') as address from lso_address la,street_list sl" + " where la.street_id = sl.street_id and la.addr_id =" + addressId;
		logger.debug(sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				address.setAddressId(rs.getInt("id"));
				address.setDescription(rs.getString("address"));
			}

			return address;
		} catch (Exception e) {
			logger.debug("General Exception in getting activity address " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of sub project activities
	 * 
	 * @param activityType
	 * @param subProjectId
	 * @return
	 * @throws Exception
	 */
	public List getSubProjectActivities(String activityType, String subProjectId) throws Exception {
		List actList = new ArrayList();

		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT A.ACT_NBR,A.APPLIED_DATE ADATE, A.ISSUED_DATE AS IDATE,");
			sql.append("A.ACT_NBR  || COALESCE('  :  ' || RTRIM(SUBSTR(A.description,1,50)) || '   :   ' || RTRIM(LAS.description) || '  :  ',' ') AS DESCRIPTION");
			sql.append(" FROM ACTIVITY A LEFT OUTER JOIN LKUP_ACT_ST LAS ON A.STATUS = LAS.STATUS_ID");
			sql.append(" WHERE A.ACT_TYPE='BLDG' AND A.SPROJ_ID = " + subProjectId + " ORDER BY A.ACT_ID desc");

			logger.debug(sql.toString());

			RowSet rs = new Wrapper().select(sql.toString());

			while (rs != null && rs.next()) {
				LookupType look = new LookupType();
				look.setType(rs.getString("ACT_NBR"));

				String iDate = StringUtils.date2str(rs.getDate("IDATE"));

				if (iDate == null) {
					iDate = "";
				}

				String aDate = StringUtils.date2str(rs.getDate("ADATE"));

				if (aDate == null) {
					aDate = "";
				}

				look.setDescription((rs.getString("DESCRIPTION")) + iDate + "  :  " + aDate);
				actList.add(look);
			}

			if (rs != null) {
				rs.close();
			}

			return actList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public People getOwnerByActivityHistory(String subProjectId) throws Exception {
		logger.debug("getOwnerByActivityHistory(" + subProjectId + ")");

		People people = new People();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT AP.PEOPLE_ID,P.NAME");
		sql.append(" FROM ACTIVITY_PEOPLE AP JOIN PEOPLE P ON AP.PEOPLE_ID = P.PEOPLE_ID");
		sql.append(" AND AP.PSA_TYPE = 'A' AND P.PEOPLE_TYPE_ID = 7");
		sql.append(" WHERE AP.ACT_ID IN (SELECT ACT_ID FROM ACTIVITY WHERE SPROJ_ID =" + subProjectId + ")");
		sql.append(" ORDER BY AP.ACT_ID desc");
		logger.debug("SQL : " + sql.toString());

		try {
			RowSet rs = new Wrapper().select(sql.toString());

			// Getting the latest owner info of activity in that project
			if (rs.next()) {
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setName(rs.getString("NAME"));
				people.setPeopleType(new PeopleAgent().getPeopleTypeObj("W"));
			}
		} catch (Exception e) {
			logger.error("Error in getOwnerByActivityHistory() " + e.getMessage());
			throw e;
		}

		return people;
	}

	public int deleteActivity(int activityId) throws Exception {
		int subProjectId = -1;

		try {
			if (activityId != 0) {
				Wrapper db = new Wrapper();
				RowSet rs = db.select("select sproj_id from activity where act_id=" + activityId);

				if (rs.next()) {
					subProjectId = rs.getInt("sproj_id");
				}

				String sql = "delete from holds where level_id = " + activityId + " and hold_level = 'A'";
				logger.debug(" Delete hold Sql is " + sql);
				db.update(sql);

				sql = "delete from comments where level_id = " + activityId + " and comnt_level = 'A'";
				logger.debug(" Delete comments Sql is " + sql);
				db.update(sql);

				sql = "delete from activity_people where act_id = " + activityId + " and psa_type = 'A'";
				logger.debug(" Delete people Sql is " + sql);
				db.update(sql);

				sql = "delete from plan_check where act_id = " + activityId;
				logger.debug(" Delete plan check Sql is " + sql);
				db.update(sql);

				sql = "delete from inspection where act_id = " + activityId;
				logger.debug(" Delete inspection Sql is " + sql);
				db.update(sql);

				sql = "delete from conditions where level_id = " + activityId + " and cond_level= 'A'";
				logger.debug(" Delete condition Sql is " + sql);
				db.update(sql);

				sql = "delete from attachments where level_id = " + activityId + " and attach_level= 'A'";
				logger.debug(" Delete attachment Sql is " + sql);
				db.update(sql);

				sql = "delete from process_team where psa_id = " + activityId + " and psa_type= 'A'";
				logger.debug(" Delete Activity Team Sql is " + sql);
				db.update(sql);

				sql = "delete from activity_fee where activity_id = " + activityId;
				logger.debug(" Delete Activity fee Sql is " + sql);
				db.update(sql);

				sql = "delete from ACT_CUSTOM_FIELDS where act_id=" + activityId;
				logger.debug(" Delete ACT_CUSTOM_FIELDS Sql is " + sql);
				db.update(sql);

				String activityNumber = getActivtiyNumber(activityId);

				if (activityNumber.startsWith("BL")) {
					sql = "delete from bl_activity where act_id =" + activityId;
					logger.debug(" Delete bl_activity Sql is " + sql);
					db.update(sql);
				}

				if (activityNumber.startsWith("BT")) {
					sql = "delete from bt_activity where act_id =" + activityId;
					logger.debug(" Delete bt_activity Sql is " + sql);
					db.update(sql);
				}

				sql = "delete from activity where act_id =" + activityId;
				logger.debug(" Delete activity Sql is " + sql);
				db.update(sql);
			}
			
			try {
				String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
				if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){
				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK"), "id",StringUtils.i2s(activityId));
				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_INSPECTION"), "ref_id",StringUtils.i2s(activityId));
				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_PLANCHECK"), "ref_id",StringUtils.i2s(activityId));
				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_ATTACHMENT"), "act_id",StringUtils.i2s(activityId));
				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_FINANCE"), "act_id",StringUtils.i2s(activityId));
				GlobalSearch.deleteLoadInitials(GlobalSearch.getKeyValue("SOLR_LOAD_INITIAL_BURBANK_LEDGER"), "act_id",StringUtils.i2s(activityId));
				}
			} catch (Exception e) {
				e.getMessage();
			}

			return subProjectId;
		} catch (Exception e) {
			logger.error("Exception thrown while trying to delete Actvity :" + e.getMessage());
			throw e;
		}
	}

	private String commentSelect(Wrapper db, String id) throws Exception {
		String comnt = "";

		try {
			String sql = "select comnt from comments where comnt_id = " + id;

			logger.debug(sql);

			RowSet rsComments = db.select(sql);

			while (rsComments != null && rsComments.next()) {
				comnt = rsComments.getString("comnt");
			}
		} catch (Exception e) {
			logger.error("exception occured when select comment" + e.getMessage());
		}

		return comnt;
	}

	/**
	 * gets the business name for a given business license activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getBusinessName(String activityId) throws Exception {
		logger.info("getBusinessName(" + activityId + ")");

		String businessName = "";

		try {
			String sql = "select BUSINESS_NAME from BL_ACTIVITY where act_id = " + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				businessName = rs.getString("BUSINESS_NAME");
			}

			return businessName;
		} catch (Exception e) {
			logger.error("exception occured when getting business name" + e.getMessage());
			throw new Exception("Exception occured while getting business Name " + e.getMessage());
		}
	}

	/**
	 * gets the business account number for a given business license activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public String getBusinessAccountNumber(String activityId) throws Exception {
		logger.info("getBusinessAccountNumber(" + activityId + ")");

		String businessAccountNumber = "";

		try {
			String sql = "select BUSINESS_ACC_NO from BL_ACTIVITY where act_id = " + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				businessAccountNumber = rs.getString("BUSINESS_ACC_NO");
			}

			return businessAccountNumber;
		} catch (Exception e) {
			logger.error("exception occured when getting business account number" + e.getMessage());
			throw new Exception("Exception occured while getting business account number " + e.getMessage());
		}
	}

	public String getBusinessOwnerName(String activityId) throws Exception {
		logger.info("getBusinessOwnerName(" + activityId + ")");

		String businessOwnerName = "";

		try {
			String sql = "select BUSINESS_OWNER_NAME from V_BL_SEARCH where activity_Id = " + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				businessOwnerName = rs.getString("BUSINESS_OWNER_NAME");
			}

			return businessOwnerName;
		} catch (Exception e) {
			logger.error("exception occured when getting business account number" + e.getMessage());
			throw new Exception("Exception occured while getting business account number " + e.getMessage());
		}
	}

	public String getBusinessOwnerNameForBT(String activityId) throws Exception {
		logger.info("getBusinessOwnerNameForBT(" + activityId + ")");

		String businessOwnerName = "";

		try {
			String sql = "select BUSINESS_OWNER_NAME as BT_BUSINESS_OWNER_NAME from V_BT_SEARCH where activity_Id = " + activityId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs != null && rs.next()) {
				businessOwnerName = rs.getString("BT_BUSINESS_OWNER_NAME");
			}

			return businessOwnerName;
		} catch (Exception e) {
			logger.error("exception occured when getting business account number" + e.getMessage());
			throw new Exception("Exception occured while getting business account number " + e.getMessage());
		}
	}

	public void saveActivityTeam(BusinessLicenseApprovalForm businessLicenseApprovalForm, int loggedInUserId) throws Exception {
		logger.info("saveActivityTeam(" + businessLicenseApprovalForm + ")");

		String sql = "";
		Wrapper db = new Wrapper();

		try {
			int approvalId = db.getNextId("APPROVAL_ID");
			logger.debug("approval id obtained is " + approvalId);
			sql = "INSERT INTO BL_APPROVAL (APPROVAL_ID,REFERRED_DT,BL_APPR_ST,APPROVED_BY,CREATED,CREATED_BY,ACT_ID,DEPT_ID) values (";
			sql += approvalId;
			sql += ",";
			sql += "current_date";
			sql += ",";
			sql += Constants.BL_OR_BT_APPROVAL_STATUS_PENDING; // pending status
			sql += ",";
			sql += businessLicenseApprovalForm.getUserId();
			sql += ",";
			sql += "current_timestamp";
			sql += ",";
			sql += loggedInUserId;
			sql += ",";
			sql += businessLicenseApprovalForm.getActivityId();
			sql += ",";
			sql += businessLicenseApprovalForm.getDepartmentId();
			sql += ")";
			logger.info(sql);
			db.insert(sql);
		} catch (DuplicateRecordException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Exception occured while saving activity team" + e.getMessage());
			throw new Exception("Exception occured while saving activity team " + e.getMessage());
		}
	}

	/**
	 * getApprovalHistoryList delete the activity team record
	 * 
	 * @param activityId
	 * @param approvalId
	 * @throws Exception
	 */

	public List getApprovalHistoryList(int activityId, int approvalId, String activityNumber) throws Exception {
		logger.debug("getApprovalHistoryList(" + activityId + "," + approvalId + "," + activityNumber + ")");

		List activityTeamHistoryList = new ArrayList();
		String sql = "";
		// int size;

		try {
			if (activityNumber.startsWith("BL")) {
				sql = "select * from v_bl_approval_history where act_id=" + activityId + " and APPROVAL_ID=" + approvalId;
			} else {
				sql = "select * from v_bt_approval_history where act_id=" + activityId + " and APPROVAL_ID=" + approvalId;
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				BusinessLicenseApprovalForm businessLicneseApprovalForm = new BusinessLicenseApprovalForm();
				businessLicneseApprovalForm.setActivityId("" + activityId);
				businessLicneseApprovalForm.setApprovalId(rs.getInt("approval_id"));
				businessLicneseApprovalForm.setDepartmentId(rs.getInt("dept_id"));
				businessLicneseApprovalForm.setDepartmentName(rs.getString("department_name"));
				businessLicneseApprovalForm.setUserId(rs.getInt("approved_by"));
				businessLicneseApprovalForm.setUserName(rs.getString("approved_by_name"));
				businessLicneseApprovalForm.setReferredDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("referred_dt"))));
				businessLicneseApprovalForm.setApprovalStatusId(rs.getInt("appr_status_id"));
				businessLicneseApprovalForm.setApprovalStatus(rs.getString("APPR_STATUS_NAME"));
				businessLicneseApprovalForm.setApprovalDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("approved_dt"))));
				businessLicneseApprovalForm.setComments(StringUtils.nullReplaceWithEmpty(rs.getString("comments")));
				activityTeamHistoryList.add(businessLicneseApprovalForm);
			}

			rs.close();

			return activityTeamHistoryList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	@SuppressWarnings("unused")
	public List getActivityTeamList(int activityId, int size) throws Exception {
		logger.debug("getActivityTeamList(" + activityId + ")");

		List<BusinessLicenseApprovalForm> activityTeamList = new ArrayList<BusinessLicenseApprovalForm>();
		String sql = "";    

		try {
			sql = "select * from v_bl_approval where act_id=" + activityId;

			if (size > 0) {
				sql += (" and rownum<=" + size);
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				BusinessLicenseApprovalForm businessLicneseApprovalForm = new BusinessLicenseApprovalForm();
				businessLicneseApprovalForm.setActivityId("" + activityId);
				businessLicneseApprovalForm.setApprovalId(rs.getInt("approval_id"));
				businessLicneseApprovalForm.setDepartmentId(rs.getInt("dept_id"));
				businessLicneseApprovalForm.setDepartmentName(rs.getString("department_name"));
				businessLicneseApprovalForm.setUserId(rs.getInt("approved_by"));
				businessLicneseApprovalForm.setUserName(rs.getString("approved_by_name"));
				businessLicneseApprovalForm.setReferredDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("referred_dt"))));
				businessLicneseApprovalForm.setApprovalStatusId(rs.getInt("appr_status_id"));
				businessLicneseApprovalForm.setApprovalStatus(rs.getString("APPR_STATUS_NAME"));
				
				if(rs.getString("approved_dt")== null)
				{
					businessLicneseApprovalForm.setApprovalDate("");
				}else{
				businessLicneseApprovalForm.setApprovalDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("approved_dt"))));
				}
				businessLicneseApprovalForm.setComments(StringUtils.nullReplaceWithEmpty(rs.getString("comments")));
				activityTeamList.add(businessLicneseApprovalForm);
			}
   
			rs.close();
			
			if(activityTeamList==null) 
			{
			activityTeamList = new ArrayList<BusinessLicenseApprovalForm>();
			}   
			
			return activityTeamList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * delete the activity team record
	 * 
	 * @param approvalId
	 * @throws Exception
	 */
	public void deleteActivityTeam(int approvalId) throws Exception {
		try {
			String sql = "delete from bl_approval where approval_id=" + approvalId;

			logger.info(sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public boolean checkForDuplicates(BusinessLicenseApprovalForm businessLicenseApprovalForm, int loggedInUserId, String activityIds, String departmentId) throws Exception {
		logger.info("checkForDuplicates(" + businessLicenseApprovalForm + ")");

		String sql = "";
		Wrapper db = new Wrapper();
		int count = 0;
		boolean duplicateExists = false;

		try {
			// check if the entry exists for duplicates
			sql = "select count(*)as count from bl_approval where ACT_ID=" + activityIds + " and  APPROVED_BY=" + businessLicenseApprovalForm.getUserId() + " and dept_id=" + departmentId;
			logger.info(sql);

			RowSet rs = db.select(sql);
			while (rs != null && rs.next()) {
				count = rs.getInt("count");
			}
			logger.debug("**** count " + count);
			if (count > 0) {
				duplicateExists = true;
			}
			return duplicateExists;

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void updateBLApproval(BusinessLicenseApprovalForm businessLicenseApprovalForm) throws Exception {
		logger.info("updateBLApproval(" + businessLicenseApprovalForm + ")");

		String sql = "";
		String sql_select = "";
		Wrapper db = new Wrapper();
		String sqlSelect = null;
		String sqlInsert = null;
		int count = 0;

		try {
			if (!(businessLicenseApprovalForm.getActivityId().equals(null)) && businessLicenseApprovalForm.getDepartmentId() != 0) {

				sql_select = "select * from bl_approval where ACT_ID=" + businessLicenseApprovalForm.getActivityId() + " and  APPROVED_BY=" + businessLicenseApprovalForm.getUserId() + " and dept_id=" + businessLicenseApprovalForm.getDepartmentId();
				logger.info(sql_select);

				RowSet rs = db.select(sql_select);
				if (rs != null && rs.next()) {
					int existingApprovalId = rs.getInt("APPROVAL_ID");

					sqlSelect = "select * from BL_APPROVAL where APPROVAL_ID = " + existingApprovalId;
					logger.info(sqlSelect);
					ResultSet rsSelect = db.select(sqlSelect);

					if (rsSelect != null && rsSelect.next()) {
						sqlInsert = "insert into BL_APPROVAL_HISTORY (APPROVAL_ID,REFERRED_DT,BL_APPR_ST,APPROVED_BY,APPROVED_DT,COMMENTS,CREATED,UPDATED,CREATED_BY,UPDATED_BY,ACT_ID,DEPT_ID) values (" + existingApprovalId + "," + ((StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("REFERRED_DT")))) == null) ? "current_timestamp" : (StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("REFERRED_DT")))))) + "," + rsSelect.getString("BL_APPR_ST") + "," + rsSelect.getString("APPROVED_BY") + ",current_timestamp,'" + rsSelect.getString("COMMENTS") + "'," + ((StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("CREATED")))) == null) ? "current_timestamp" : (StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("CREATED")))))) + "," + ((StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("UPDATED")))) == null) ? "current_timestamp" : (StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rsSelect.getString("UPDATED")))))) + "," + rsSelect.getString("CREATED_BY") + "," + rsSelect.getString("UPDATED_BY") + "," + rsSelect.getString("ACT_ID") + "," + rsSelect.getString("DEPT_ID") + ")";
						logger.info(sqlInsert);
						db.update(sqlInsert);
					}
					sql = "update BL_APPROVAL set ";
					sql += "BL_APPR_ST = " + Constants.BL_OR_BT_APPROVAL_STATUS_PENDING;
					sql += ",";
					sql += "APPROVED_DT = " + ((StringUtils.toOracleDate(businessLicenseApprovalForm.getApprovalDate()) == null) ? "current_date" : (StringUtils.toOracleDate(businessLicenseApprovalForm.getApprovalDate())));
					sql += ",";
					sql += "COMMENTS = '" + businessLicenseApprovalForm.getComments();
					sql += "',";
					sql += "UPDATED = current_timestamp";
					sql += ",";
					sql += "UPDATED_BY = " + businessLicenseApprovalForm.getUpdatedBy();
					logger.debug("got Updated By " + businessLicenseApprovalForm.getUpdatedBy());
					sql += "  where APPROVAL_ID = " + existingApprovalId + " and BL_APPR_ST = " + Constants.BL_OR_BT_APPROVAL_STATUS_DENIED;

					logger.info(sql);
					db.update(sql);

				}
			}
		} catch (DuplicateRecordException e) {
			throw e;
		}
	}

	/**
	 * @param activityId
	 * @param size
	 * @return activityTeamList
	 * @author Hemavathi
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public List getBTActivityTeamList(int activityId, int size) throws Exception {
		logger.debug("getBTActivityTeamList(" + activityId + ")");

		List<BusinessLicenseApprovalForm> activityTeamList = new ArrayList<BusinessLicenseApprovalForm>();
		String sql = "";

		try {
			sql = "select * from v_bt_approval where act_id=" + activityId;

			if (size > 0) {
				sql += (" and rownum<=" + size);
			}

			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				BusinessLicenseApprovalForm businessLicneseApprovalForm = new BusinessLicenseApprovalForm();
				businessLicneseApprovalForm.setActivityId("" + activityId);
				businessLicneseApprovalForm.setApprovalId(rs.getInt("approval_id"));
				businessLicneseApprovalForm.setDepartmentId(rs.getInt("dept_id"));
				businessLicneseApprovalForm.setDepartmentName(rs.getString("department_name"));
				businessLicneseApprovalForm.setUserId(rs.getInt("approved_by"));
				businessLicneseApprovalForm.setUserName(rs.getString("approved_by_name"));
				businessLicneseApprovalForm.setReferredDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("referred_dt"))));
				businessLicneseApprovalForm.setApprovalStatusId(rs.getInt("appr_status_id"));
				businessLicneseApprovalForm.setApprovalStatus(rs.getString("appr_status_name"));
				
				if(rs.getString("approved_dt")== null) 
				{
					businessLicneseApprovalForm.setApprovalDate("");
				}else{   
					businessLicneseApprovalForm.setApprovalDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("approved_dt"))));
				}
				
				businessLicneseApprovalForm.setComments(StringUtils.nullReplaceWithEmpty(rs.getString("comments")));
				activityTeamList.add(businessLicneseApprovalForm);
			}

			rs.close();
			
			if(activityTeamList==null)
			{
			activityTeamList = new ArrayList<BusinessLicenseApprovalForm>();
			}
			
			

			return activityTeamList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public List getSubTypes(int activityId) throws Exception {
		logger.info("getSubTypes(" + activityId + ")");

		String sql10 = "select act_subtype from lkup_act_subtype where act_subtype_id in ( select act_subtype_id from act_subtype ast where ast.act_id=" + activityId + ") order by act_subtype";

		List activitySubTypes = new ArrayList();

		RowSet rs = null;

		try {
			rs = new Wrapper().select(sql10);

			while (rs != null && rs.next()) {
				String s1 = (rs.getString("act_subtype"));
				logger.debug("The multiple subtypes are: " + s1);
				activitySubTypes.add(s1);
			}

			if (rs != null)
				rs.close();

		} catch (Exception e) {
			logger.debug("exception in activity subtype:" + e.getMessage());
			throw e;
		}

		return activitySubTypes;
	}

	public List getEmailBlastforActivityStatusChange(int actId) {
		List emailAddressList = new ArrayList();
		String fromEmail = "";
		try {

			java.util.ResourceBundle obcProperties = elms.util.db.Wrapper.getResourceBundle();
			fromEmail = obcProperties.getString("EMAIL_FROM");

			People people = null;

			Wrapper db = new Wrapper();

			String sql = "select email_addr ,people_id , people_type_id ,name ,addr ,city, state ,zip  from people where people_id in (select people_id from activity_people where act_id =" + actId + ") and EMAIL_NOTIFICATION='Y'";

			logger.debug("The Email Blast Sql for  Activity is " + sql);

			RowSet rs = new Wrapper().select(sql);
			fromEmail = "";
			while (rs.next()) {
				people = new People();

				if (rs.getString("email_addr") != null) {
					logger.debug("Email address not  null");
					people.setEmailAddress(rs.getString("email_addr"));
				} else {
					logger.debug("Email address null");
					people.setEmailAddress(fromEmail);
				}

				people.setPeopleId(rs.getInt("people_id"));
				people.setAddress(rs.getString("addr") + ", " + rs.getString("city") + ", " + rs.getString("state") + "-" + rs.getString("zip"));
				people.setPeopleId(rs.getInt("people_type_id"));
				people.setName(rs.getString("name"));

				emailAddressList.add(people);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update PlanCheck  :" + e.getMessage());
		}

		return emailAddressList;
	}

	/**
	 * Create a new Activity
	 * 
	 * @param activity
	 * @return
	 * @throws Exception
	 */
	public int createActivity(int subProjectId, int addressId, String activityType, String description, double valuation, String planCheckRequired, String activityStatusId, String appliedDate, String expirationDate, int userId, String planNo, String subDivision, String issueDate, String locationType) throws Exception {
		logger.info("createActivity(" + subProjectId + "," + addressId + "," + activityType + "," + description + "," + valuation + "," + planCheckRequired + "," + activityStatusId + "," + appliedDate + ", " + expirationDate + "," + userId + "," + planNo + "," + subDivision + "," + issueDate + "," + locationType + ")");
		int activityId = -1;
		try {
			Wrapper db = new Wrapper();
			String activityNumber = "";

			activityId = db.getNextId("ACTIVITY_ID");
			logger.debug("Obtained new activity id is " + activityId);

			int activityNum = db.getNextId("ACT_NUM");
			logger.debug("Obtained new activity number is " + activityNum);

			String strActNum = StringUtils.i2s(activityNum);
			strActNum = "00000".substring(strActNum.length()) + strActNum;

			logger.debug("Obtained new activity number string is " + strActNum);

			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			activityNumber = Constants.DEPARTMENT_PUBLIC_WORKS_CODE + formatter.format(calendar.getTime()) + strActNum.trim();

			String sql = "insert into activity(act_id,sproj_id,act_nbr,act_type,addr_id,description,valuation,start_date,applied_date,issued_date,exp_date,permit_fee_date,plan_chk_fee_date,status,plan_chk_req,created_by,created,location_type) values(" + activityId + "," + subProjectId + "," + StringUtils.checkString(activityNumber) + "," + StringUtils.checkString(activityType) + "," + addressId + "," + StringUtils.checkString(description) + "," + valuation + ", " + "to_date(" + StringUtils.checkString(appliedDate) + ",'MM/DD/YYYY') , " + "to_date(" + StringUtils.checkString(appliedDate) + ",'MM/DD/YYYY') , " + "to_date(" + StringUtils.checkString(issueDate) + ",'MM/DD/YYYY') , " + "to_date(" + StringUtils.checkString(expirationDate) + ",'MM/DD/YYYY') , current_date, current_date," + activityStatusId + "," + StringUtils.checkString(planCheckRequired) + "," + userId + "," + "current_timestamp," + locationType + ")";

			logger.debug(sql);

			db.insert(sql);
			return activityId;
		} catch (Exception e) {
			// set back activity Id to -1
			activityId = -1;
			logger.error("Exception thrown while trying to add Activity  :" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Create a new Activity
	 * 
	 * @param activity
	 * @return
	 * @throws Exception
	 */
	public int createParkingActivity(int subProjectId, int addressId, String activityType, String description, double valuation, String planCheckRequired, String activityStatusId, String appliedDate, String expirationDate, int userId, String planNo, String subDivision, String issueDate, String locationType, String parkingZoneId, String label) throws Exception {
		logger.debug("createActivity(" + subProjectId + "," + addressId + "," + activityType + "," + description + "," + valuation + "," + planCheckRequired + "," + activityStatusId + "," + appliedDate + ", " + expirationDate + "," + userId + "," + planNo + "," + subDivision + "," + issueDate + "," + locationType + "," + parkingZoneId + "," + label + ")");
		int activityId = -1;
		try {

			Wrapper db = new Wrapper();
			String activityNumber = "";

			activityId = db.getNextId("ACTIVITY_ID");
			logger.debug("Obtained new activity id is " + activityId);

			int activityNum = db.getNextId("ACT_NUM");
			logger.debug("Obtained new activity number is " + activityNum);

			String strActNum = StringUtils.i2s(activityNum);
			strActNum = "00000".substring(strActNum.length()) + strActNum;

			logger.debug("Obtained new activity number string is " + strActNum);

			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			activityNumber = Constants.DEPARTMENT_PARKING_CODE + formatter.format(calendar.getTime()) + strActNum.trim();

			String sql = "insert into activity(act_id,sproj_id,act_nbr,act_type,addr_id,description,valuation,start_date,applied_date,issued_date,exp_date,permit_fee_date,plan_chk_fee_date,status,plan_chk_req,created_by,created,location_type,pzone_id,label) values(" + activityId + "," + subProjectId + "," + StringUtils.checkString(activityNumber) + "," + StringUtils.checkString(activityType) + "," + addressId + "," + StringUtils.checkString(description) + "," + valuation + ", " + "to_date(" + StringUtils.checkString(appliedDate) + ",'MM/DD/YYYY') , " + "to_date(" + StringUtils.checkString(appliedDate) + ",'MM/DD/YYYY') , " + "to_date(" + StringUtils.checkString(issueDate) + ",'MM/DD/YYYY') , " + "to_date(" + StringUtils.checkString(expirationDate) + ",'MM/DD/YYYY') , current_date, current_date," + activityStatusId + "," + StringUtils.checkString(planCheckRequired) + "," + userId + "," + "current_timestamp," + locationType + "," + parkingZoneId + "," + StringUtils.checkString(label) + ")";

			logger.debug(sql);

			db.insert(sql);
			return activityId;
		} catch (Exception e) {
			// set back activity Id to -1
			activityId = -1;
			logger.error("Exception thrown while trying to add Activity  :" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Create Activity for Address Range
	 * 
	 * @param activityId
	 * @param streetNumberStart1
	 * @param streetNumberEnd1
	 * @param streetNameId1
	 * @param streetNumberStart2
	 * @param streetNumberEnd2
	 * @param streetNameId2
	 * @param streetNumberStart3
	 * @param streetNumberEnd3
	 * @param streetNameId3
	 * @param streetNumberStart4
	 * @param streetNumberEnd4
	 * @param streetNameId4
	 * @param streetNumberStart5
	 * @param streetNumberEnd5
	 * @param streetNameId5
	 * @throws Exception
	 */
	public void createActivityAddressRange(int activityId, String streetNumberStart1, String streetNumberEnd1, String streetNameId1, String streetNumberStart2, String streetNumberEnd2, String streetNameId2, String streetNumberStart3, String streetNumberEnd3, String streetNameId3, String streetNumberStart4, String streetNumberEnd4, String streetNameId4, String streetNumberStart5, String streetNumberEnd5, String streetNameId5) throws Exception {
		logger.info("createActivityAddressRange(" + activityId + ", " + streetNumberStart1 + "," + streetNumberEnd1 + "," + streetNameId1 + "," + streetNumberStart2 + "," + streetNumberEnd2 + "," + streetNameId2 + "," + streetNumberStart3 + "," + streetNumberEnd3 + "," + streetNameId3 + "," + streetNumberStart4 + "," + streetNumberEnd4 + "," + streetNameId4 + "," + streetNumberStart5 + "," + streetNumberEnd5 + "," + streetNameId5 + ")");
		try {
			Wrapper db = new Wrapper();

			String sql = "insert into ACT_ADDRESS_RANGE (act_id,FROM_STR_NO1,TO_STR_NO1,STREET_ID1,FROM_STR_NO2,TO_STR_NO2,STREET_ID2,FROM_STR_NO3,TO_STR_NO3,STREET_ID3,FROM_STR_NO4,TO_STR_NO4,STREET_ID4,FROM_STR_NO5,TO_STR_NO5,STREET_ID5) values(" + activityId + "," + streetNumberStart1 + "," + streetNumberEnd1 + "," + streetNameId1 + "," + (GenericValidator.isBlankOrNull(streetNumberStart2) ? "-1" : streetNumberStart2) + "," + (GenericValidator.isBlankOrNull(streetNumberEnd2) ? "-1" : streetNumberEnd2) + "," + (GenericValidator.isBlankOrNull(streetNameId2) ? "-1" : streetNameId2) + "," + (GenericValidator.isBlankOrNull(streetNumberStart3) ? "-1" : streetNumberStart3) + "," + (GenericValidator.isBlankOrNull(streetNumberEnd3) ? "-1" : streetNumberEnd3) + "," + (GenericValidator.isBlankOrNull(streetNameId3) ? "-1" : streetNameId3) + "," + (GenericValidator.isBlankOrNull(streetNumberStart4) ? "-1" : streetNumberStart4) + "," + (GenericValidator.isBlankOrNull(streetNumberEnd4) ? "-1" : streetNumberEnd4) + "," + (GenericValidator.isBlankOrNull(streetNameId4) ? "-1" : streetNameId4) + "," + (GenericValidator.isBlankOrNull(streetNumberStart5) ? "-1" : streetNumberStart5) + "," + (GenericValidator.isBlankOrNull(streetNumberEnd5) ? "-1" : streetNumberEnd5) + "," + (GenericValidator.isBlankOrNull(streetNameId5) ? "-1" : streetNameId5) + ")";

			logger.debug(sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add Activity Address Range :" + e.getMessage());
			throw e;
		}
	}

	public List getComboMappingList(AdminComboMappingForm adminComboForm) throws Exception {
		logger.info("getComboMappingList()..");

		List comboList = new ArrayList();
		RowSet rs = null;
		RowSet rs1 = null;

		try {
			StringBuffer sql = new StringBuffer("select distinct(SPROJ_NAME),DEPT_CODE,ISACTIVE from lkup_sproj_act_mapp");
			if (!adminComboForm.getSubProjName().equals("") && !adminComboForm.getSubProjName().equals("null") && !adminComboForm.getDeptCode().equals("-1") && adminComboForm.getDeptCode() != "") {
				sql.append(" where SPROJ_NAME like " + StringUtils.checkString(adminComboForm.getSubProjName().toUpperCase() + "%") + " and " + "DEPT_CODE = " + StringUtils.checkString(adminComboForm.getDeptCode()));
			} else if (!adminComboForm.getSubProjName().equals("") && !adminComboForm.getSubProjName().equals("null")) {
				sql.append(" where SPROJ_NAME like " + StringUtils.checkString(adminComboForm.getSubProjName().toUpperCase() + "%"));
			} else if (!adminComboForm.getDeptCode().equals("-1") && adminComboForm.getDeptCode() != "") {
				sql.append(" where DEPT_CODE=" + StringUtils.checkString(adminComboForm.getDeptCode()));
			}
			sql.append(" order by ISACTIVE desc");

			logger.info(sql);
			rs = new Wrapper().select(sql.toString());

			while (rs != null && rs.next()) {
				// logger.debug("got the rs");
				String sprojname = rs.getString("SPROJ_NAME");
				// logger.debug(sprojname);
				String deptcode = rs.getString("DEPT_CODE");
				// logger.debug(deptcode);
				String isActive = rs.getString("ISACTIVE");
				// logger.debug(isActive);
				String sql1 = "select ACT_TYPE,MAP_ID from lkup_sproj_act_mapp where SPROJ_NAME=" + StringUtils.checkString(sprojname) + " and " + "DEPT_CODE = " + StringUtils.checkString(deptcode);
				logger.debug(sql1);
				rs1 = new Wrapper().select(sql1);
				StringBuffer strAct = new StringBuffer();
				int mapId = 0;
				while (rs1 != null && rs1.next()) {
					strAct.append(LookupAgent.getActivityType(rs1.getString("ACT_TYPE")).getDescription());
					strAct.append(',');
					mapId = rs1.getInt("MAP_ID");
				}
				String strActivity = strAct.toString();
				int index = strActivity.lastIndexOf(',');
				strActivity = strActivity.substring(0, index);
				// logger.debug("trimmed String *"+strActivity);
				AdminComboMappingForm adminComboMappingForm = new AdminComboMappingForm();
				adminComboMappingForm.setMapId(mapId);
				adminComboMappingForm.setSubProjName(sprojname);
				logger.debug("subProject Name is " + sprojname);
				// adminComboMappingForm.setActName(rs.getString("ACT_NAME"));
				adminComboMappingForm.setActType(strActivity);
				logger.debug("Activity type " + strActivity);
				adminComboMappingForm.setDeptCode(LookupAgent.getDepartment(LookupAgent.getDepartmentId(deptcode)).getDescription());
				adminComboMappingForm.setIsActive(isActive);

				comboList.add(adminComboMappingForm);
			}
			rs.close();
			logger.debug("returning comboList of size " + comboList.size());
			return comboList;
		} catch (Exception e) {
			logger.error("Exception occured in getComboMappingList method . Message-" + e.getMessage());
			throw e;
		}
	}

	public void saveCombo(AdminComboMappingForm adminComboMappingForm, String pcReq) {
		logger.debug("Entered saveCombo()..");

		Wrapper db = new Wrapper();

		String strCode = adminComboMappingForm.getActCode();
		// logger.debug(strCode);
		String strDesc = adminComboMappingForm.getActName();
		logger.debug(StringUtils.b2s(adminComboMappingForm.getIsPcReq()));
		String[] arrCode = StringUtils.stringtoArray(strCode, ",");
		String[] arrDesc = StringUtils.stringtoArray(strDesc, ",");
		String[] pcReqs = null;
		PreparedStatement psmt = null;
		Connection con = null;
		try {
			int mapId = db.getNextId("MAP_ID");

			String sql = "insert into LKUP_SPROJ_ACT_MAPP(MAP_ID,DEPT_CODE,SPROJ_NAME,ACT_TYPE,ACT_NAME,ISACTIVE,STYPE_ID,SSTYPE_ID,ISPCREQ,FORCEPC,ISONLINEREQ,APPROVAL,START_AFTER) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
			con = db.getConnectionForPreparedStatementOnly();
			psmt = con.prepareStatement(sql);
			psmt.setInt(1, mapId);
			psmt.setString(2, adminComboMappingForm.getDeptCode());
			psmt.setString(3, adminComboMappingForm.getSubProjName().toUpperCase());
			psmt.setString(4, arrCode[0]);
			psmt.setString(5, arrDesc[0]);
			psmt.setString(6, "Y");
			psmt.setInt(7, adminComboMappingForm.getStypeId());
			psmt.setInt(8, adminComboMappingForm.getStypeSubId());
			// Initially inserting 'N' as Plan check for all the activities
			psmt.setString(9, "N");
			psmt.setString(10, adminComboMappingForm.getForcePC());
			psmt.setString(11, adminComboMappingForm.getOnline());
			logger.debug("Approval Required is " + adminComboMappingForm.getApproval());
			psmt.setString(12, adminComboMappingForm.getApproval());
			psmt.setInt(13, StringUtils.s2i(adminComboMappingForm.getStartsAfter()));
			// psmt.setInt(13,(adminComboMappingForm.getLsoUseId()));
			logger.debug("Statement  is " + sql + mapId + "" + adminComboMappingForm.getDeptCode() + "" + adminComboMappingForm.getSubProjName().toUpperCase() + " " + arrCode[0] + " " + arrDesc[0] + "Y " + adminComboMappingForm.getStypeId() + " " + adminComboMappingForm.getStypeSubId() + "N " + " " + adminComboMappingForm.getForcePC() + " " + adminComboMappingForm.getOnline() + " " + adminComboMappingForm.getApproval() + " " + adminComboMappingForm.getStartsAfter());
			psmt.execute();

			for (int i = 1; i < arrCode.length; i++) {
				if (arrCode[i].equals("") && arrDesc[i].equals("")) {
					// do nothing
				} else {
					// logger.debug("saving again ");
					mapId = db.getNextId("MAP_ID");
					psmt.setInt(1, mapId);
					psmt.setString(4, arrCode[i]);
					psmt.setString(5, arrDesc[i]);
					psmt.execute();
				}

			}

			// Getting list of Activities to which plan check is added -added by Manjuprasad
			pcReqs = StringUtils.stringtoArray(pcReq, ",");

			// Updating Plan check ISREQ to 'Y' for which plan check is required -added by Manjuprasad
			for (int i = 0; i < pcReqs.length; i++) {
				String sqlUpdatePc = "Update LKUP_SPROJ_ACT_MAPP set ISPCREQ='Y' where ACT_TYPE='" + pcReqs[i] + "'";
				logger.debug("The Plan Check Update for Individual Activity is " + sqlUpdatePc);
				db.update(sqlUpdatePc);

			}

			// inserting land use for combo mapping-- used for checking LandUSe in combo mapping - Done by Manjuprasad
			logger.debug("Array of Land Use IDs " + StringUtils.nullReplaceWithEmpty(adminComboMappingForm.getUseIdArray()));

			String useIdArray = StringUtils.nullReplaceWithEmpty(adminComboMappingForm.getUseIdArray());
			int lastIndex = 0;
			String[] useIdArrays = null;
			if (!(useIdArray.equalsIgnoreCase(""))) {
				logger.debug("hai hello");
				lastIndex = useIdArray.lastIndexOf(",");
				useIdArray = useIdArray.substring(0, lastIndex);
				useIdArrays = StringUtils.stringtoArray(useIdArray, ",");
				logger.debug("The size of the array of Land Ids is " + useIdArrays.length);
			}

			int UseMapId = 0;

			String sqlDelete = "delete from LKUP_COMBO_USE_MAPP where sproj_name = '" + adminComboMappingForm.getSubProjName().toUpperCase() + "' and dept_code = '" + adminComboMappingForm.getDeptCode().toUpperCase() + "'";
			db.update(sqlDelete);
			for (int i = 0; i < useIdArrays.length; i++) {
				UseMapId = db.getNextId("USE_MAPID");
				String sql1 = "insert into LKUP_COMBO_USE_MAPP(USE_MAPID,SPROJ_NAME,LSO_USEID,Dept_code) values(" + UseMapId + ",'" + adminComboMappingForm.getSubProjName().toUpperCase() + "'," + useIdArrays[i] + ",'" + adminComboMappingForm.getDeptCode().toUpperCase() + "')";
				logger.debug("Inserting the sproject info based on land use " + sql1);
				db.update(sql1);
			}

		} catch (Exception e) {
			logger.error("Exception while trying to insert " + e.getMessage());
		} finally {
			try {
				if (psmt != null)
					psmt.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				// ignored
			}

		}

		logger.debug("Exit saveCombo()..");
	}

	public AdminComboMappingForm getComboForm(int mapId, String subProjName, String deptCode) throws Exception {

		logger.info("Entered getComboForm()..");

		RowSet rs = null;
		AdminComboMappingForm adminComboMappingForm = new AdminComboMappingForm();
		try {
			String sql = "select * from lkup_sproj_act_mapp where DEPT_CODE=" + StringUtils.checkString(deptCode) + " and " + "SPROJ_NAME = " + StringUtils.checkString(subProjName);

			logger.info(sql);
			rs = new Wrapper().select(sql);
			StringBuffer strAct = new StringBuffer();
			String sprjname = "";
			String deptcode = "";
			String pcReq = "";
			int stype = 0;
			int sStype = 0;
			String isPcReq = "";
			int lsoUseId = 0;
			String forcePC = "";
			String online = "";
			String approval = "";
			int startAfter = 0;

			while (rs != null && rs.next()) {
				strAct.append(rs.getString("ACT_TYPE"));
				strAct.append('\r');
				sprjname = rs.getString("SPROJ_NAME");
				deptcode = rs.getString("DEPT_CODE");
				forcePC = rs.getString("FORCEPC");
				online = rs.getString("ISONLINEREQ");
				approval = rs.getString("APPROVAL");
				startAfter = rs.getInt("START_AFTER");

				stype = rs.getInt("STYPE_ID");

				sStype = rs.getInt("SSTYPE_ID");
				if ((rs.getString("ISPCREQ")).equalsIgnoreCase("Y")) {

					pcReq = pcReq + rs.getString("ACT_TYPE") + ",";

				}
			}
			adminComboMappingForm.setActType(strAct.toString());
			adminComboMappingForm.setMapId(mapId);
			adminComboMappingForm.setSubProjName(sprjname);
			adminComboMappingForm.setDeptCode(deptcode);
			adminComboMappingForm.setStypeId(stype);
			adminComboMappingForm.setStypeSubId(sStype);
			adminComboMappingForm.setForcePC(forcePC);
			adminComboMappingForm.setOnline(online);
			adminComboMappingForm.setApproval(approval);
			adminComboMappingForm.setStartsAfter(StringUtils.i2s(startAfter));
			// adminComboMappingForm.setLsoUseId(lsoUseId);

			adminComboMappingForm.setPcReq(pcReq);
			// logger.debug(strAct.toString());
			rs.close();
			logger.debug("returning  adminComboMappingForm in getComboForm()..");
			return adminComboMappingForm;
		} catch (Exception e) {
			logger.error("Exception occured in getComboForm method . Message-" + e.getMessage());
			throw e;
		}
	}

	public String getSelectedActivityType(int mapId, String subProjName, String deptCode) throws Exception {

		logger.info("Entered getSelectedActivityType()..");
		// String DeptID = ""+deptId;
		RowSet rs = null;

		try {
			String sql = "select * from lkup_sproj_act_mapp where DEPT_CODE=" + StringUtils.checkString(deptCode) + " and " + "SPROJ_NAME = " + StringUtils.checkString(subProjName);

			logger.info(sql);
			rs = new Wrapper().select(sql);
			StringBuffer strAct = new StringBuffer();

			while (rs != null && rs.next()) {
				strAct.append(LookupAgent.getActivityType(rs.getString("ACT_TYPE")).getDescription());
				strAct.append(',');
			}
			// logger.debug(strAct.toString());
			rs.close();
			logger.debug("returning  SelectedActivityType in getSelectedActivityType()..");
			return (strAct.toString());
		} catch (Exception e) {
			logger.error("Exception occured in getSelectedActivityType method . Message-" + e.getMessage());
			throw e;
		}
	}

	public String getSelectedActivityCode(int mapId, String subProjName, String deptCode) throws Exception {

		logger.info("Entered getSelectedActivityCode()..");
		// String DeptID = ""+deptCode;
		RowSet rs = null;

		try {
			String sql = "select * from lkup_sproj_act_mapp where DEPT_CODE=" + StringUtils.checkString(deptCode) + " and " + "SPROJ_NAME = " + StringUtils.checkString(subProjName);

			logger.info(sql);
			rs = new Wrapper().select(sql);
			StringBuffer strAct = new StringBuffer();

			while (rs != null && rs.next()) {
				strAct.append(LookupAgent.getActivityType(rs.getString("ACT_TYPE")).getType());
				strAct.append(',');
			}
			// logger.debug(strAct.toString());
			rs.close();
			logger.debug("returning  SelectedActivityType in getSelectedActivityCode()..");
			return (strAct.toString());
		} catch (Exception e) {
			logger.error("Exception occured in getSelectedActivityCode method . Message-" + e.getMessage());
			throw e;
		}
	}

	public void deleteCombo(AdminComboMappingForm adminComboMappingForm) throws Exception {
		logger.info("Entered deleteCombo()..");
		Wrapper db = new Wrapper();
		try {
			String sql = "delete from lkup_sproj_act_mapp where DEPT_CODE=" + StringUtils.checkString(adminComboMappingForm.getDeptCode()) + " and " + "SPROJ_NAME = " + StringUtils.checkString(adminComboMappingForm.getSubProjName());
			logger.info(sql);
			db.update(sql);

		} catch (Exception e) {
			logger.error("Exception occured in deleteCombo method . Message-" + e.getMessage());
			throw e;
		}
	}

	public void updateActiveDeActiveCombo(AdminComboMappingForm adminComboMappingForm) throws Exception {
		logger.info("Entered updateActiveInActiveCombo()..");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			String chkActive = "select ISACTIVE from lkup_sproj_act_mapp where DEPT_CODE=" + StringUtils.checkString(adminComboMappingForm.getDeptCode()) + " and " + "SPROJ_NAME = " + StringUtils.checkString(adminComboMappingForm.getSubProjName());
			rs = new Wrapper().select(chkActive);
			String isActive = "";
			while (rs != null && rs.next()) {
				isActive = rs.getString("ISACTIVE").equals("Y") ? "N" : "Y";
			}
			rs.close();

			// logger.debug(isActive);
			String sql = "update lkup_sproj_act_mapp set ISACTIVE =" + StringUtils.checkString(isActive) + " where DEPT_CODE=" + StringUtils.checkString(adminComboMappingForm.getDeptCode()) + " and " + "SPROJ_NAME = " + StringUtils.checkString(adminComboMappingForm.getSubProjName());
			logger.info(sql);
			db.update(sql);

		} catch (Exception e) {
			logger.error("Exception occured in updateActiveInActiveCombo method . Message-" + e.getMessage());
			throw e;
		}
	}

	public AdminComboMappingForm getComboValues(String mapId) throws Exception {
		logger.info("Entered getComboValues().." + mapId);
		AdminComboMappingForm adminComboMappingForm = new AdminComboMappingForm();
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			String sql = "select SPROJ_NAME,DEPT_CODE from lkup_sproj_act_mapp where MAP_ID = " + mapId;
			logger.info(sql);
			rs = new Wrapper().select(sql);
			while (rs != null && rs.next()) {
				adminComboMappingForm.setSubProjName(rs.getString("SPROJ_NAME"));
				adminComboMappingForm.setDeptCode(rs.getString("DEPT_CODE"));
			}

		} catch (Exception e) {
			logger.error("Exception occured in getComboValues method . Message-" + e.getMessage());
			throw e;
		}
		return adminComboMappingForm;
	}

	public List searchCombos(BasicSearchForm basicsearchform) throws Exception {
		logger.info("getComboMappingList()..");

		List comboList = new ArrayList();
		RowSet rs = null;
		RowSet rs1 = null;
		String comboPermit = basicsearchform.getComboPermit();
		logger.debug("got comboPermit: " + comboPermit);

		try {
			StringBuffer sql = new StringBuffer("SELECT PROJECT.LSO_ID,SPROJ_ID,SPROJ_NBR,COMBO_NBR,COMBO_NAME FROM ((SUB_PROJECT JOIN PROJECT ON PROJECT.PROJ_ID=SUB_PROJECT.PROJ_ID) JOIN V_ADDRESS_LIST V ON V.LSO_ID=PROJECT.LSO_ID) JOIN V_STREET_LIST VSL ON VSL.STREET_ID=V.STREET_ID where  UPPER(SUB_PROJECT.COMBO_NBR) LIKE " + StringUtils.checkString(basicsearchform.getComboPermit()));
			sql.append("ORDER BY VSL.STREET_NAME,V.STR_NO,PROJECT.APPLIED_DT DESC");
			logger.info(sql);
			rs = new Wrapper().select(sql.toString());
			while (rs != null && rs.next()) {
				int sprojid = rs.getInt("SPROJ_ID");
				int lsoid = rs.getInt("LSO_ID");
				String sprojnumber = rs.getString("SPROJ_NBR");
				logger.debug("SPROJ_NBR " + rs.getString("SPROJ_NBR"));
				String comboname = rs.getString("COMBO_NAME");
				String sql1 = "select ACT_NBR from ACTIVITY where SPROJ_ID=" + sprojid;
				logger.debug(sql1);
				rs1 = new Wrapper().select(sql1);
				StringBuffer strCombo = new StringBuffer();
				while (rs1 != null && rs1.next()) {
					strCombo.append(rs1.getString("ACT_NBR"));
					logger.debug("ACT_NBR" + rs1.getString("ACT_NBR"));
					strCombo.append(',');
				}
				rs1.close();
				logger.debug("strCombo*******" + strCombo);
				String strComboPermit = strCombo.toString();
				int index = strComboPermit.lastIndexOf(',');
				strComboPermit = strComboPermit.substring(0, index);
				logger.debug("strComboPermit" + strComboPermit);
				// logger.debug("trimmed String *" + strComboPermit);
				BasicSearchForm basicSearchForm = new BasicSearchForm();
				basicSearchForm.setSubProjectNumber(sprojnumber);
				basicSearchForm.setSubProjectId(sprojid);
				basicSearchForm.setLsoId(lsoid);
				basicSearchForm.setComboName(comboname);
				basicSearchForm.setActivityNumber(strComboPermit);
				comboList.add(basicSearchForm);
			}
			rs.close();
			logger.debug("returning comboList of size " + comboList.size());
			return comboList;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in searchCombos method . Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of LSO USE for Land
	 * 
	 * @return
	 * @throws Exception
	 *             Sunil
	 */
	public static LsoUse getlsoUse() throws Exception {

		logger.debug("getlsoUse()");
		LsoUse lsoUse = null;

		try {
			String sql = "select * from lso_use where lso_type='L' ";
			RowSet rs = new Wrapper().select(sql);

			List lsoUseId = new ArrayList();
			List lsoType = new ArrayList();
			;
			List lsoUseDescription = new ArrayList();
			while (rs.next()) {

				lsoUseId.add(StringUtils.i2s(rs.getInt("lso_use_id")));

				lsoType.add(StringUtils.nullReplaceWithEmpty(rs.getString("lso_type").trim()));
				lsoUseDescription.add(StringUtils.nullReplaceWithEmpty(rs.getString("description").trim()));

				lsoUse = new LsoUse(lsoUseId, lsoType, lsoUseDescription);
				// lsoUseL.add(lsoUse);

			}
			rs.close();
			return lsoUse;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the list of LSO USE and set
	 * 
	 * @return
	 * @throws Exception
	 */
	public static LsoUse setlsoUse(String sprjname, String deptCode) throws Exception {

		logger.debug("setlsoUse()");
		Wrapper db = new Wrapper();
		// List lsoUseL = new ArrayList();
		LsoUse lsoUse = new LsoUse();
		String projectDescription = "";
		try {
			String sql = "select lu.lso_use_id,lu.lso_type,lu.description from LKUP_COMBO_USE_MAPP lcum,lso_use lu where lcum.SPROJ_NAME='" + sprjname + "' and lcum.DEPT_CODE='" + deptCode + "' and lcum.LSO_USEID=lu.lso_use_id";
			RowSet rs = new Wrapper().select(sql);
			logger.debug("The sub project based on land use is " + sql);

			List lsoUseId = new ArrayList();
			List lsoType = new ArrayList();
			;
			List lsoUseDescription = new ArrayList();
			while (rs.next()) {

				lsoUseId.add(StringUtils.i2s(rs.getInt("lso_use_id")));

				lsoType.add(StringUtils.nullReplaceWithEmpty(rs.getString("lso_type").trim()));
				lsoUseDescription.add(StringUtils.nullReplaceWithEmpty(rs.getString("description").trim()));

				lsoUse = new LsoUse(lsoUseId, lsoType, lsoUseDescription);

			}
			logger.debug("Use id size aftter set " + lsoUseId.size());
			logger.debug("Use id  type size aftter set " + lsoType.size());
			logger.debug("Use id Description aftter set " + lsoUseDescription.size());

			rs.close();
			return lsoUse;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Comparing list of selected landuse with list of land use
	 * 
	 * @return
	 * @throws Exception
	 */
	public static boolean compareListWithString(String useId, List useIdMaps) throws Exception {

		logger.debug("compareListWithString(" + +')');
		try {

			for (int i = 0; i < useIdMaps.size(); i++) {
				if (useIdMaps.get(i).equals(useId)) {
					return true;

				}

			}
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			throw e;
		}
	}

	public int addBSDeptProject(String lsoId, String projectName, String projectDesc, String statusId, String userId, String projectUseId) throws Exception {
		int projectId = -1;
		String projectDescription = "";
		logger.debug(" projectDescription " + projectDesc);
		projectDescription = projectDesc;
		String startDate = "current_date";
		Wrapper db = new Wrapper();
		String completionDate = "null";
		String deptId = "8"; // For Building and safety

		try {
			String sql = "";
			projectId = db.getNextId("PROJECT_ID");
			int projNum = db.getNextId("PR_NUM");
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String projectNbr = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);
			sql = "insert into project(proj_id,lso_id,project_nbr,name,description,status_id,applied_dt,created_by,created_dt,updated_by,update_dt,lso_use_id,dept_id) " + " values(" + projectId + "," + lsoId + "," + StringUtils.checkString(projectNbr) + "," + StringUtils.checkString(projectName) + "," + StringUtils.checkString(projectDescription) + "," + statusId + "," + startDate + "," + userId + "," + "current_timestamp" + "," + userId + "," + "current_timestamp" + "," + projectUseId + "," + deptId + ")";
			logger.debug(sql);
			db.insert(sql);
			return projectId;
		} catch (Exception e) {
			projectId = -1;
			logger.error("Error in Adding Project..");
			throw e;
		}
	}

	public int addSubProject(String comboName, String projectId, String projTypeId, String sprojTypeId, String subProjectSubTypeId, String status, String description, String userId, String valuation, String deptCode) throws Exception {
		int subProjectId = -1;
		Wrapper db = new Wrapper();
		String comboNumber = null;

		try {
			String sql = "";
			subProjectId = db.getNextId("SUB_PROJECT_ID");
			int sprojNum = db.getNextId("SP_NUM");

			Calendar calendar = Calendar.getInstance();

			int comboId = db.getNextId("COMBO_NBR_BS");
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);

			SimpleDateFormat formatter1 = new SimpleDateFormat("yyMMdd");
			String date = formatter1.format(calendar.getTime());
			comboNumber = deptCode + "C" + date + comboId;
			projTypeId = "10129";// Repair & Maintenance
			sql = "insert into sub_project(sproj_id,proj_id,sproj_type,stype_id,sstype_id,status,description,created_by,created,updated_by,updated,sproj_nbr) " + " values(" + subProjectId + "," + projectId + "," + projTypeId + "," + sprojTypeId + "," + subProjectSubTypeId + "," + status + "," + StringUtils.checkString("Repair & Maintenance") + "," + userId + "," + "current_date" + "," + userId + "," + "current_date" + "," + StringUtils.checkString(subProjectNumber) + ")";

			logger.debug("VALUATION " + valuation);

			logger.info(" subProjectAgent -- addSubProject() --insert Sql is " + sql);

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add SubProject  :" + e.getMessage());
		}

		return subProjectId;
	}

	public int addActivities(String deptCode, String[] actList, String sprojId, String addrId, String activityType, String actDesc, String valuation, String status, String userId, String planCheckReq, Calendar startDt) throws Exception {
		boolean success = false;

		int activityId = -1;
		String actSubtypeId = "-1";
		String expDate = null;

		Wrapper db = new Wrapper();

		try {
			SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");

			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, 180);
			expDate = StringUtils.checkString(new String(formatter1.format(cal.getTime())));

			logger.debug("The Expiry date is" + expDate);

			String activityNumber = "";
			activityId = db.getNextId("ACTIVITY_ID");

			int activityNum = db.getNextId("ACT_NUM");
			String strActNum = StringUtils.i2s(activityNum);
			strActNum = "00000".substring(strActNum.length()) + strActNum;

			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			activityNumber = deptCode + formatter.format(calendar.getTime()) + strActNum.trim();
			logger.debug("  Activity numbers for Combo Permits is  " + activityNumber);

			String startDate = StringUtils.cal2datetime(startDt);
			if (startDate != null && !startDate.equalsIgnoreCase(""))
				startDate = StringUtils.toOracleDate(startDate.substring(0, 10));
			else
				startDate = "current_date";

			if (planCheckReq.equalsIgnoreCase("null")) {
				planCheckReq = "N";
			} else {
				planCheckReq = "Y";
			}

			String sql = "insert into activity(act_id,sproj_id,act_nbr,act_type,addr_id," + "description,valuation,start_date, applied_date,exp_date,permit_fee_date" + ",plan_chk_fee_date, status,plan_chk_req,created_by,created,updated_by,updated,DEV_FEE_REQ,DEVELOPMENT_FEE_DATE)	values(" + activityId + "," + sprojId + "," + StringUtils.checkString(activityNumber) + "," + StringUtils.checkString(activityType) + "," + addrId + "," + StringUtils.checkString(actDesc) + "," + valuation + ", " + startDate + "  ,current_date , to_date(" + expDate + ",'yyyy/MM/dd')  , current_date , current_date " + "," + status + "," + StringUtils.checkString(planCheckReq) + "," + userId + ", current_timestamp ," + userId + ", current_timestamp ," + "'N', current_timestamp )";

			logger.debug("The Insert Sql is " + sql);

			db.beginTransaction();

			db.addBatch(sql);

			String sql1 = "insert into act_subtype values(" + activityId + "," + actSubtypeId + ")";

			db.addBatch(sql1);

			db.executeBatch();
			User user = new User();
			user.setUserId(0);
			CommonAgent condAgent = new CommonAgent();
			String typeId = condAgent.getActTypeId(activityType);
			boolean added = condAgent.copyRequiredCondition(typeId, activityId, user);
			if (valuation != null && StringUtils.s2d(valuation) > 0.00) {

				logger.debug(activityType);
				/*
				 * int dept_Id = Constants.DEPARTMENT_DEPARTMENT_BUILDING_SAFETY;
				 * 
				 * if (dept_Id != Constants.DEPARTMENT_DEPARTMENT_BUILDING_SAFETY) { logger.debug("calling function to calculate Plan Check****");
				 * 
				 * PlanCheck planCheck = new PlanCheck(); planCheck.setActivityId(activityId); planCheck.setCategoryCode(0); planCheck.setTitleCode(0); planCheck.setComments(""); cal = Calendar.getInstance(); cal.setTime(Calendar.getInstance().getTime()); planCheck.setPlanCheckDate(cal); user = new User(); user.setUserId(Integer.parseInt(userId)); planCheck.setCreatedBy(user); planCheck.setUpdatedBy(user); planCheck.setEngineer(elms.common.Constants.UNASSIGNED_ENGINEER); planCheck.setStatusCode(elms.common.Constants.PCSTATUSSUBMITTED);
				 * 
				 * new PlanCheckAgent().addPlanCheck(planCheck); } else {
				 * 
				 * }
				 */
			}

			db.commitTransaction();
			success = true;
		} catch (Exception e) {
			db.rollbackTransaction();
			success = false;
			throw e;
		}

		return activityId;
	}

	public void setCertificate(Certificate certificate,String actNbr) {
		try {
			if (certificate != null) {
				logger.debug("certificate is not null ");
				if (getCertificateCount(actNbr)) {
					updateCertificate(certificate,actNbr);
					logger.debug("update of certificate  completed successfully ");
				} else {
					logger.debug("Before adding of certificate");
					addCertificate(certificate,actNbr);
					logger.debug("certificate successfully added");

				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void updateCertificate(Certificate certificate_occupancy,String actNbr) {
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			db.beginTransaction();
			logger.debug("before creation of sql:  =" + certificate_occupancy);
			if (certificate_occupancy != null) {
				sql = "UPDATE CERTIFICATE_OCCUPANCY SET ";

				sql += " ACT_APN = ";

				String apn = "";
				if (certificate_occupancy.getACT_APN() == null || ("").equalsIgnoreCase(certificate_occupancy.getACT_APN())) {
					apn = "0";
				} else {
					apn = StringUtils.checkString(certificate_occupancy.getACT_APN());
				}

				sql += apn;

				sql += ",";
				sql += "ACT_NBR =";
				sql += StringUtils.checkString(certificate_occupancy.getACT_NBR());
				logger.debug(" OccGrp is set to  " + certificate_occupancy.getACT_NBR());

				sql += ",";
				sql += " ACT_ADDR =";

				sql += StringUtils.checkString(certificate_occupancy.getACT_ADDR());
				logger.debug(" BuildingType is set to  " + certificate_occupancy.getACT_ADDR());

				sql += ",";

				sql += " ISSUED_DATE = ";
				sql += "to_date(" + StringUtils.checkString(certificate_occupancy.getISSUED_DATE()) + ",'MM/DD/YYYY')";

				sql += ",";
				sql += " ACT_PEOPLE = ";
				sql += StringUtils.checkString(certificate_occupancy.getACT_PEOPLE());

				sql += ",";
				sql += " OCC_GRP =";
				sql += StringUtils.checkString(certificate_occupancy.getOCC_GRP());
				logger.debug(" PrintDate is set to  " + certificate_occupancy.getOCC_GRP());

				sql += ",";
				sql += " CONSTR_TYPE =";
				sql += StringUtils.checkString(certificate_occupancy.getCONSTR_TYPE());
				logger.debug(" PermitStatus is set to  " + certificate_occupancy.getCONSTR_TYPE());

				sql += ",";
				sql += " SPRINKLE = ";
				sql += StringUtils.checkString(certificate_occupancy.getSPRINKLE());

				sql += ",";
				sql += " GARAGE_AREA= ";
				sql += StringUtils.checkString(certificate_occupancy.getGARAGE_AREA());

				sql += ",";
				sql += " STORIES =";
				sql += StringUtils.s2i(certificate_occupancy.getSTORIES());

				sql += ",";
				sql += " BLDG_AREA =";
				sql += StringUtils.checkString(certificate_occupancy.getBLDG_AREA());

				sql += ",";
				sql += " BSMT_AREA =";
				sql += StringUtils.checkString(certificate_occupancy.getBSMT_AREA());

				sql += ",";
				sql += " DWELLING =";
				sql += StringUtils.s2i(certificate_occupancy.getDWELLING());

				sql += ",";
				sql += " PARKING = ";
				sql += StringUtils.checkString(certificate_occupancy.getPARKING());

				sql += ",";
				sql += " INSPECTOR =";
				sql += StringUtils.s2i(certificate_occupancy.getINSPECTOR());

				sql += ",";
				sql += " BLDG_OFFICIAL =";
				sql += StringUtils.checkString(certificate_occupancy.getBLDG_OFFICIAL());

				sql += ",";
				sql += " COND_CODE =";
				sql += StringUtils.checkString(certificate_occupancy.getCOND_CODE());

				sql += ",";
				sql += " ACT_DESCRIPTION =";   
				sql += StringUtils.checkString(certificate_occupancy.getACT_DESCRIPTION());

				sql += ",";
				sql += " SPEC_CONDITIONS =";
				sql += StringUtils.checkString(certificate_occupancy.getSPEC_CONDITIONS());

				sql += " WHERE ";   
				sql += " ACT_NBR =";
				sql += StringUtils.checkString(actNbr);
			}     

			db.insert(sql);
			logger.debug("Certificate update sql is :" + sql);
			//db.executeBatch();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public Certificate getCertificate(String actNBR, String actId) {
		Certificate certificate_occupancy = null;
		certificate_occupancy = new Certificate();
		certificate_occupancy.setACT_NBR(actNBR);
		logger.debug("actId set outside");
		try {
			String actNbr = "";
			String sql = "SELECT ACT_APN ,ACT_PEOPLE,ACT_NBR ,ACT_ADDR ,ISSUED_DATE ,OCC_GRP , CONSTR_TYPE ,SPRINKLE ,GARAGE_AREA ,STORIES ,BLDG_AREA , BSMT_AREA ,DWELLING ,PARKING  ,INSPECTOR ,BLDG_OFFICIAL , COND_CODE ,ACT_DESCRIPTION ,SPEC_CONDITIONS  from CERTIFICATE_OCCUPANCY  where ACT_nbr =" + StringUtils.checkString(actNBR);
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {

				logger.debug("ACT_NBR IN getCertificate " + actNbr);

				certificate_occupancy.setACT_APN(rs.getString("ACT_APN"));
				certificate_occupancy.setACT_ADDR(rs.getString("ACT_ADDR"));

				certificate_occupancy.setISSUED_DATE(StringUtils.yyyymmdd2date(rs.getString("ISSUED_DATE")));
				certificate_occupancy.setACT_PEOPLE(rs.getString("ACT_PEOPLE"));
				certificate_occupancy.setOCC_GRP(rs.getString("OCC_GRP"));
				certificate_occupancy.setCONSTR_TYPE(rs.getString("CONSTR_TYPE"));
				certificate_occupancy.setSPRINKLE(rs.getString("SPRINKLE"));
				certificate_occupancy.setGARAGE_AREA(rs.getString("GARAGE_AREA"));
				certificate_occupancy.setSTORIES(rs.getString("STORIES"));
				certificate_occupancy.setBLDG_AREA(rs.getString("BLDG_AREA"));
				certificate_occupancy.setBSMT_AREA(rs.getString("BSMT_AREA"));
				certificate_occupancy.setDWELLING(rs.getString("DWELLING"));
				certificate_occupancy.setPARKING(rs.getString("PARKING"));
				certificate_occupancy.setINSPECTOR(rs.getString("INSPECTOR"));
				certificate_occupancy.setBLDG_OFFICIAL(rs.getString("BLDG_OFFICIAL"));
				certificate_occupancy.setCOND_CODE(rs.getString("COND_CODE"));
				certificate_occupancy.setACT_DESCRIPTION(rs.getString("ACT_DESCRIPTION"));
				certificate_occupancy.setSPEC_CONDITIONS(rs.getString("SPEC_CONDITIONS"));
			}
			// if ((actNbr == null) || (actNbr.equalsIgnoreCase(""))) {
			// certificate_occupancy = getCertificateOthers(certificate_occupancy, "0");
			// }
			else {
				certificate_occupancy = getCertificatefromActivity(actId);
			}

			if (rs != null)
				rs.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		if (certificate_occupancy == null) {
			certificate_occupancy = new Certificate();
			certificate_occupancy.setACT_NBR(actNBR + "");
		}
		// return getCertificateOthers(certificate_occupancy);
		logger.debug("returning from Agent");
		return certificate_occupancy;
	}

	public void addCertificate(Certificate certificate_occupancy,String actNbr) {

		try {
			Wrapper db = new Wrapper();

			String sql = "";
			sql =
					// "insert into certificate_occupancy (ACT_ID,OCC_GRP,BUILDING_TYPE,BUILDING_USE,PRINT_DATE,PERMIT_STATUS,VERBAGE1,COMPLETION_FLAG,CERTIFICATE_TYPE,CREATED_BY,CREATED,UPDATED_BY,UPDATED) values (";
					"insert into certificate_occupancy (ACT_APN ,ACT_NBR ,ACT_ADDR ,ISSUED_DATE ,ACT_PEOPLE, OCC_GRP , CONSTR_TYPE ,SPRINKLE ," + "GARAGE_AREA ,STORIES ,BLDG_AREA , BSMT_AREA ,DWELLING ,PARKING  ,INSPECTOR ,BLDG_OFFICIAL , COND_CODE ,ACT_DESCRIPTION ,SPEC_CONDITIONS) values (";

			if (certificate_occupancy != null) {

				db.beginTransaction();

				String apn = "";
				if (certificate_occupancy.getACT_APN() == null || certificate_occupancy.getACT_APN().equals("")) {
					apn = "0";
				} else
					apn = certificate_occupancy.getACT_APN();

				sql += StringUtils.checkString(apn);

				sql += ",";
				sql += StringUtils.checkString(actNbr);
				logger.debug(" OccGrp is set to  " + actNbr);    

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getACT_ADDR());
				logger.debug(" BuildingType is set to  " + certificate_occupancy.getACT_ADDR());

				sql += ",";
				sql += "to_date(" + StringUtils.checkString(certificate_occupancy.getISSUED_DATE()) + ",'MM/DD/YYYY')";
				logger.debug(" Issued Date is set to  " + certificate_occupancy.getISSUED_DATE());

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getACT_PEOPLE());

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getOCC_GRP());
				logger.debug(" PrintDate is set to  " + certificate_occupancy.getOCC_GRP());

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getCONSTR_TYPE());
				logger.debug(" PermitStatus is set to  " + certificate_occupancy.getCONSTR_TYPE());

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getSPRINKLE());

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getGARAGE_AREA());

				sql += ",";
				int story = (certificate_occupancy.getSTORIES() != null && !certificate_occupancy.getSTORIES().equals("")) ? StringUtils.s2i(certificate_occupancy.getSTORIES()) : 0;
				sql += story;

				sql += ",";

				sql += StringUtils.checkString(certificate_occupancy.getBLDG_AREA());

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getBSMT_AREA());

				sql += ",";

				int dwell = (certificate_occupancy.getDWELLING() != null && !certificate_occupancy.getDWELLING().equals("")) ? StringUtils.s2i(certificate_occupancy.getDWELLING()) : 0;
				sql += dwell;

				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getPARKING());
				sql += ",";
				sql += StringUtils.s2i(certificate_occupancy.getINSPECTOR());
				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getBLDG_OFFICIAL());
				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getCOND_CODE());
				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getACT_DESCRIPTION());
				sql += ",";
				sql += StringUtils.checkString(certificate_occupancy.getSPEC_CONDITIONS());

				sql += ")";

				logger.debug(" Updated is set to Current Timestamp ");

				logger.debug(sql);
				db.addBatch(sql);
				db.executeBatch();

			} else {
				logger.error(" Certificate is not Created");
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public boolean getCertificateCount(String actNbr) {
		// Certificate certificate_occupancy = null;
		boolean cnt = false;
		try {

			String sql = "SELECT count(*) FROM CERTIFICATE_OCCUPANCY WHERE ACT_NBR = '" + actNbr + "'";

			logger.debug(sql);
			RowSet certificate_occupancyRs = new Wrapper().select(sql);

			if (certificate_occupancyRs.next()) {
				int count = certificate_occupancyRs.getInt(1);
				logger.debug("Count is " + count);
				if (count > 0) {
					cnt = true;
					logger.debug("actNbr exist");
				} else {
					cnt = false;
					logger.debug("actNbr dosen't exist");
				}
			}
			if (certificate_occupancyRs != null)
				certificate_occupancyRs.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return cnt;
	}

	public Certificate getCertificatefromActivity(String actId) {

		Certificate certificate_occupancy = new Certificate();

		try {

			String sql1 = "select NAME ,act_id  from activity_people ap, PEOPLE p, PEOPLE_type pt  where psa_type ='A'  and act_id = " + StringUtils.checkString(actId) + " and ap.people_id = p.people_id and p.PEOPLE_TYPE_ID = pt.PEOPLE_TYPE_ID and p.PEOPLE_TYPE_ID=" + Constants.PEOPLE_OWNER;

			String sql4 = "select  la.STR_NO,sl.PRE_DIR, sl.STR_NAME, sl.STR_TYPE,  la.city,la.STATE,la.zip     from  v_psa_list  vpl"

					+ " join  lso_address la  on  la.lso_id = vpl.lso_id"

					+ " join v_lso vl on  vl.land_id = la.lso_id  or vl.structure_id = la.lso_id  or vl.occupancy_id = la.lso_id  join street_list sl  on sl.STREET_ID = la.STREET_ID"

					+ " where act_id =" + actId;

			Wrapper db = new Wrapper();

			logger.debug(sql1);
			logger.debug(sql4);

			certificate_occupancy.setACT_NBR(LookupAgent.getActivityNumberForActivityId(actId));

			RowSet rs = db.select(sql1);
			if (rs.next()) {
				certificate_occupancy.setACT_PEOPLE(rs.getString("Name"));
				logger.debug("Owner name from People  manager in Activity is " + certificate_occupancy.getACT_PEOPLE());
			}
			rs.close();

			RowSet rs3 = db.select(sql4);
			if (rs3.next()) {
				String pre_dir = (rs3.getString("PRE_DIR") != null) ? rs3.getString("PRE_DIR") : "";
				String address = rs3.getString("STR_NO") + " " + pre_dir + " " + rs3.getString("STR_NAME") + " " + rs3.getString("STR_TYPE") + " " + rs3.getString("city") + " " + rs3.getString("STATE") + " " + rs3.getString("zip");
				certificate_occupancy.setACT_ADDR(address);
			}

			rs3.close();

			certificate_occupancy.setISSUED_DATE("");
			certificate_occupancy.setINSPECTOR("");
			certificate_occupancy.setBLDG_OFFICIAL("");
			certificate_occupancy.setCOND_CODE("");
			certificate_occupancy.setSPEC_CONDITIONS("");

		} catch (Exception e) {
			logger.debug(e.getMessage());
		}
		return certificate_occupancy;
	}

	public Document getCOOJDOM(String actNbr) {
		logger.info("getCOOJDOM(" + actNbr + ")");

		Document document = new Document();

		try {
			Element rootElement = new Element("root");
			document.setRootElement(rootElement);

			Element dataElement = new Element("data");
			rootElement.addContent(dataElement);

			Element cooElement = getCOOElement(actNbr);
			dataElement.addContent(cooElement);

			logger.debug("just before ownerElement ");

			Element ownerElement = getCOOOwnerElement(actNbr);
			cooElement.addContent(ownerElement);
		} catch (Exception e) {
			logger.error(" Exception in COOReportAgent -- getCOOJDOM() method " + e.getMessage());
		}

		return document;
	}

	public Element getCOOOwnerElement(String actNbr) {
		logger.info("getCOOOwnerElement(" + actNbr + ")");

		Element ownerElement = new Element("owner");

		try {
			logger.debug("entered into COOReportAgent.getOwnerElement");
			String sql = "select NAME,ADDR,COALESCE(CITY,'')|| COALESCE(' ' || STATE,'') ";
			sql = sql + (" || COALESCE(' ' || RTRIM(CAST(ZIP AS CHAR(15))),'')  AS citystatezip ");
			sql = sql + (" from V_ACTIVITY_PEOPLE where V_ACTIVITY_PEOPLE.PEOPLE_TYPE_ID = 8 ");
			sql = sql + (" and ACT_ID = ( select act_id from activity ");
			sql = sql + (" where ACT_NBR ='" + actNbr + "')");
			logger.debug("Owner in COO : " + sql);

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql);

			String name = "";
			String addr = "";
			String citystatezip = "";

			Element nameElement = null;
			Element addrElement = null;
			Element citystatezipElement = null;

			while (titleRs.next()) {
				name = titleRs.getString("NAME");
				logger.debug("name is " + name);
				addr = titleRs.getString("ADDR");
				logger.debug("addr is " + addr);
				citystatezip = titleRs.getString("citystatezip");
				logger.debug("citystatezip is " + citystatezip);

				nameElement = new Element("name");
				addrElement = new Element("address");
				citystatezipElement = new Element("citystatezip");

				logger.debug("owner ELEMENT - Created ");

				nameElement.addContent(name);
				addrElement.addContent(addr);
				citystatezipElement.addContent(citystatezip);

				ownerElement.addContent(nameElement);
				ownerElement.addContent(addrElement);
				ownerElement.addContent(citystatezipElement);

			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in COOReportAgent -- getOwnerElement() method " + e.getMessage());
		}

		return ownerElement;
	}

	public Element getCOOElement(String actNbr) {
		logger.info("getCOOElement(" + actNbr + ")");
		Element mainElement = new Element("coo");

		try {

			StringBuffer sql = new StringBuffer();
			sql.append(" ");

			sql.append("SELECT ACT_APN as C6,ACT_NBR AS C2,ACT_ADDR as C4,ISSUED_DATE as C21,OCC_GRP AS C7,");
			sql.append(" CONSTR_TYPE as C10,SPRINKLE AS C13,GARAGE_AREA AS C12,STORIES AS C8,BLDG_AREA AS C9,");
			sql.append(" BSMT_AREA AS C15,DWELLING AS C11,PARKING AS C14,INSPECTOR AS C17,BLDG_OFFICIAL as C20, u.first_name as c22  , last_name as c23, ");
			sql.append(" COND_CODE as C5,ACT_DESCRIPTION as C3,SPEC_CONDITIONS as C16 from CERTIFICATE_OCCUPANCY coo");
			sql.append(" , users u where u.userid = coo.INSPECTOR ");
			sql.append(" and  ACT_NBR ='" + actNbr + "'");

			logger.debug("COO Element : " + sql.toString());

			Wrapper db = new Wrapper();
			RowSet titleRs = db.select(sql.toString());

			String c2 = "permitnumber";
			String c3 = "projdesc";
			String c4 = "projaddress";
			String c5 = "activitycondition";
			String c6 = "apn";
			String c7 = "occgrp";
			String c8 = "stories";
			String c9 = "bldgarea";
			String c10 = "consttype";
			String c11 = "noofdwellingunits";
			String c12 = "garagearea";
			String c13 = "sprinklers";
			String c14 = "parkingspaces";
			String c15 = "basementarea";
			String c16 = "specialconditions";
			String c17 = "inspector";
			String c20 = "bldgofficial";
			String c21 = "coodate";

			Date curdate = new Date(); // to get current run date

			Element c2Element = new Element(c2);
			Element c3Element = new Element(c3);
			Element c4Element = new Element(c4);
			Element c5Element = new Element(c5);
			Element c6Element = new Element(c6);
			Element c7Element = new Element(c7);
			Element c8Element = new Element(c8);
			Element c9Element = new Element(c9);
			Element c10Element = new Element(c10);
			Element c11Element = new Element(c11);
			Element c12Element = new Element(c12);
			Element c13Element = new Element(c13);
			Element c14Element = new Element(c14);
			Element c15Element = new Element(c15);
			Element c16Element = new Element(c16);
			Element c17Element = new Element(c17);
			Element c20Element = new Element(c20);
			Element c21Element = new Element(c21);

			if (titleRs.next()) {
				c2 = titleRs.getString("C2");
				c3 = titleRs.getString("C3");
				c4 = titleRs.getString("C4");
				c5 = titleRs.getString("C5");
				c6 = titleRs.getString("C6");
				c7 = titleRs.getString("C7");
				c8 = titleRs.getString("C8");
				c9 = titleRs.getString("C9");
				c10 = titleRs.getString("C10");
				c11 = titleRs.getString("C11");
				c12 = titleRs.getString("C12");
				c13 = titleRs.getString("C13");
				c14 = titleRs.getString("C14");
				c15 = titleRs.getString("C15");
				c16 = titleRs.getString("C16");
				c17 = titleRs.getString("C22") + " " + titleRs.getString("C23");

				c20 = titleRs.getString("C20");
				c21 = titleRs.getString("C21");
				if (c21 != null) {
					c21 = StringUtils.yyyymmdd2date(c21).replace('/', '-');
				}
				logger.debug("permitnumber is " + c2);
				logger.debug("projdesc is " + c3);
				logger.debug("projaddress is " + c4);
				logger.debug("activitycondition is " + c5);
				logger.debug("apn is " + c6);
				logger.debug("occgrp  is " + c7);
				logger.debug("stories   is " + c8);
				logger.debug("bldgarea  is " + c9);
				logger.debug("consttype is " + c10);
				logger.debug("noofdwellingunits  is " + c11);
				logger.debug("garagearea  is " + c12);
				logger.debug("sprinklers is " + c13);
				logger.debug("parkingspaces   is " + c14);
				logger.debug("basementarea   is " + c15);
				logger.debug("specialconditions  is " + c16);
				logger.debug("inspector is " + c17);

				logger.debug("bldgofficial is " + c20);
				logger.debug("coodate  is " + c21);

				c2Element.addContent(c2);
				c3Element.addContent(c3);
				c4Element.addContent(c4);
				c5Element.addContent(c5);
				c6Element.addContent(c6);
				c7Element.addContent(c7);
				c8Element.addContent(c8);
				c9Element.addContent(c9);
				c10Element.addContent(c10);
				c11Element.addContent(c11);
				c12Element.addContent(c12);
				c13Element.addContent(c13);
				c14Element.addContent(c14);
				c15Element.addContent(c15);
				c16Element.addContent(c16);
				c17Element.addContent(c17);

				c20Element.addContent(c20);
				c21Element.addContent(c21);

				mainElement.addContent(c2Element);
				mainElement.addContent(c3Element);
				mainElement.addContent(c4Element);
				mainElement.addContent(c5Element);
				mainElement.addContent(c6Element);
				mainElement.addContent(c7Element);
				mainElement.addContent(c8Element);
				mainElement.addContent(c9Element);
				mainElement.addContent(c10Element);
				mainElement.addContent(c11Element);
				mainElement.addContent(c12Element);
				mainElement.addContent(c13Element);
				mainElement.addContent(c14Element);
				mainElement.addContent(c15Element);
				mainElement.addContent(c16Element);
				mainElement.addContent(c17Element);

				mainElement.addContent(c20Element);
				mainElement.addContent(c21Element);

				logger.debug("COO ELEMENT - Created ");
			}

			if (titleRs != null) {
				titleRs.close();
			}
		} catch (Exception e) {
			logger.error(" Exception in COOReportAgent -- getCOOElement() method " + e.getMessage());
		}

		return mainElement;
	}

	public List getCodeEnforcementTypes() throws Exception {
		logger.info("Entering getCodeEnforcementTypes()");
		Wrapper db = new Wrapper();
		List typeList = new ArrayList();
		String GET_CE_TYPES = "SELECT * FROM LKUP_CE_TYPE ORDER BY CATEGORY,CE_TYPE";

		RowSet rs = db.select(GET_CE_TYPES);

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setCeTypeId(rs.getInt("ce_type_id"));
			ce.setCategory(rs.getString("category"));
			ce.setName(rs.getString("ce_type"));
			typeList.add(ce);
		}

		logger.info("Exiting getCodeEnforcementTypes(" + typeList.size() + ")");
		return typeList;
	}

	public List getNoticeTypes() throws Exception {
		logger.info("Entering getNoticeTypes()");
		Wrapper db = new Wrapper();
		List typeList = new ArrayList();
		String GET_NOTICE_TYPES = "SELECT LNT.NOTICE_TYPE_ID,LNT.CATEGORY,LNT.DESCRIPTION FROM LKUP_NOTICE_TYPE LNT JOIN LKUP_NOTICE_TEMPLATE LNTE ON LNT.NOTICE_TYPE_ID=LNTE.NOTICE_TYPE_ID ORDER BY LNT.DESCRIPTION";
		logger.debug("GET_NOTICE_TYPES" + GET_NOTICE_TYPES);
		RowSet rs = db.select(GET_NOTICE_TYPES);

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setCeTypeId(rs.getInt("NOTICE_TYPE_ID"));
			ce.setCategory(rs.getString("CATEGORY"));
			ce.setName(rs.getString("DESCRIPTION"));
			typeList.add(ce);
		}

		logger.info("Exiting getNoticeTypes(" + typeList.size() + ")");
		return typeList;
	}

	public List getNoticeTypeList() throws Exception {
		logger.info("Entering getNoticeTypeList()");
		Wrapper db = new Wrapper();
		List typeList = new ArrayList();
		String GET_NOTICE_TYPES = "SELECT NOTICE_TEMPLATE_ID,NAME FROM LKUP_NOTICE_TEMPLATE ORDER BY NAME";
		Connection localCon = null;
		Statement statement = null;
		ResultSet rs = null;
		localCon = new Wrapper().getConnection();
		statement = localCon.createStatement();
		rs = statement.executeQuery(GET_NOTICE_TYPES);

		// RowSet rs = db.select(GET_NOTICE_TYPES);

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setCeTypeId(rs.getInt("NOTICE_TEMPLATE_ID"));
			ce.setCategory(rs.getString("NAME"));
			ce.setName(rs.getString("NAME"));
			typeList.add(ce);
		}

		logger.info("Exiting getNoticeTypeList(" + typeList.size() + ")");
		return typeList;
	}

	public List getCodeEnforcementSubTypes(long activityId) throws Exception {
		logger.info("getCodeEnforcementSubTypes(" + activityId + ")");
		Wrapper db = new Wrapper();
		List subTypeList = new ArrayList();
		String sql = "select * from LKUP_ACT_SUBTYPE where ACT_TYPE in (select ACT_TYPE from ACTIVITY where ACT_ID=" + activityId + ") ORDER BY ACT_TYPE,ACT_SUBTYPE";
		RowSet rs = db.select(sql);

		while (rs.next()) {
			PickList item = new PickList(rs.getString("act_subtype"), rs.getString("act_type") + "-" + rs.getString("act_subtype"));
			subTypeList.add(item);
		}

		logger.info("Exiting getCodeEnforcementSubTypes(" + subTypeList.size() + ")");
		return subTypeList;
	}

	public List<CodeEnforcement> getNoticetList(long activityId) throws Exception {
		logger.info("Entering getNoticetList(" + activityId + ")");
		Wrapper db = new Wrapper();
		List<CodeEnforcement> actionList = new ArrayList<CodeEnforcement>();
		String sql = "SELECT N.*,LNT.CATEGORY,LNT.NOTICE_TYPE_ID,LNT.DESCRIPTION FROM NOTICES N JOIN LKUP_NOTICE_TYPE LNT ON N.NOTICE_TYPE_ID=LNT.NOTICE_TYPE_ID WHERE N.ACT_ID=" + activityId + " ORDER BY NOTICE_ID";
		logger.debug("Get notice sql" + sql);
		RowSet rs = db.select(sql);

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setId(rs.getInt("NOTICE_ID"));
			ce.setCeTypeId(rs.getInt("NOTICE_TYPE_ID"));
			ce.setOfficerId(rs.getString("OFFICER_ID"));
			ce.setCategory("Notices");
			ce.setName(rs.getString("NAME"));
			ce.setPhone(rs.getString("PHONE"));
			ce.setCeDate(StringUtils.date2str(rs.getDate("NOTICE_DATE")));
			ce.setFeeAmount(StringUtils.dbl2$(rs.getDouble("FEE_AMT")));
			ce.setSubType(rs.getString("SUB_TYPE"));
			ce.setCeType(rs.getString("DESCRIPTION"));
			actionList.add(ce);
		}

		logger.info("Exiting getNoticetList(" + actionList.size() + ")");
		return actionList;
	}

	public List<CodeEnforcement> getCodeEnforcementList(long activityId) throws Exception {
		logger.info("Entering getCodeEnforcementList(" + activityId + ")");
		Wrapper db = new Wrapper();
		List<CodeEnforcement> actionList = new ArrayList<CodeEnforcement>();
		String sql = "SELECT A.*,B.CATEGORY,B.CE_TYPE FROM CODE_ENFORCEMENT A JOIN LKUP_CE_TYPE B ON A.CE_TYPE_ID=B.CE_TYPE_ID WHERE A.ACT_ID=" + activityId + " ORDER BY CE_ID";

		RowSet rs = db.select(sql);

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setId(rs.getInt("ce_id"));
			ce.setCeTypeId(rs.getInt("ce_type_id"));
			ce.setOfficerId(rs.getString("officer_id"));
			ce.setCategory(rs.getString("category"));
			ce.setName(rs.getString("name"));
			ce.setPhone(rs.getString("phone"));
			ce.setCeDate(StringUtils.date2str(rs.getDate("ce_date")));
			ce.setFeeAmount(StringUtils.dbl2$(rs.getDouble("fee_amt")));
			ce.setSubType(rs.getString("sub_type"));
			ce.setCeType(rs.getString("CE_TYPE"));
			actionList.add(ce);
		}

		logger.info("Exiting getCodeEnforcementList(" + actionList.size() + ")");
		return actionList;
	}

	public void saveNoticesList(long activityId, List actionList) throws Exception {
		logger.info("Entering saveNoticesList(" + activityId + "," + actionList.size() + ")");
		Wrapper db = new Wrapper();
		String sql;
		try {
			String INSERT_CE = "INSERT INTO NOTICES (NOTICE_ID,ACT_ID, NOTICE_TYPE_ID, OFFICER_ID, NAME, PHONE, NOTICE_DATE, FEE_AMT, SUB_TYPE ) VALUES ";

			db.beginTransaction();
			for (int i = 0; i < actionList.size(); i++) {
				CodeEnforcement ce = (CodeEnforcement) actionList.get(i);

				// Delete of an inserted row
				if ((ce.getId() == 0) && !ce.getDeleted().equals(""))
					continue;

				if (ce.getId() == 0) {
					logger.debug("Inserting :" + ce);
					if (ce.getCeTypeId() != -1) {
						sql = INSERT_CE + "(" + db.getNextId("NOTICE_ID") + "," + activityId + "," + ce.getCeTypeId() + "," + StringUtils.checkNumber(ce.getOfficerId()) + "," + StringUtils.checkString(ce.getName()) + "," + StringUtils.checkString(ce.getPhone()) + "," + "to_date(" + StringUtils.checkString(ce.getCeDate()) + ",'MM/DD/YY')" + "," + StringUtils.s2d(ce.getFeeAmount()) + "," + StringUtils.checkString(ce.getSubType()) + ")";
						db.addBatch(sql);
						logger.debug(sql);
					}
				} else if (!ce.getDeleted().equals("")) {
					sql = "DELETE FROM NOTICES " + " WHERE NOTICE_ID=" + ce.getId();
					db.addBatch(sql);
					logger.debug(sql);
				} else {
					sql = "UPDATE NOTICES SET " + "OFFICER_ID=" + StringUtils.checkNumber(ce.getOfficerId()) + "," + "NAME=" + StringUtils.checkString(ce.getName()) + "," + "PHONE=" + StringUtils.checkString(ce.getPhone()) + "," + "NOTICE_DATE=" + "to_date(" + StringUtils.checkString(ce.getCeDate()) + ",'MM/DD/YY')" + "," + "FEE_AMT=" + StringUtils.s2d(ce.getFeeAmount()) + "," + "SUB_TYPE=" + StringUtils.checkString(ce.getSubType()) + " WHERE NOTICE_ID=" + ce.getId();
					db.addBatch(sql);
					logger.debug(sql);
				}
			}
			db.executeBatch();
			db.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.info("Exiting saveCodeEnforcementList()");
	}

	public void saveCodeEnforcementList(long activityId, List actionList) throws Exception {
		logger.info("Entering saveCodeEnforcementList(" + activityId + "," + actionList.size() + ")");
		Wrapper db = new Wrapper();
		String sql;

		String INSERT_CE = "INSERT INTO CODE_ENFORCEMENT (CE_ID,ACT_ID,CE_TYPE_ID,OFFICER_ID,NAME,PHONE,CE_DATE,FEE_AMT,SUB_TYPE) VALUES ";

		db.beginTransaction();
		for (int i = 0; i < actionList.size(); i++) {
			CodeEnforcement ce = (CodeEnforcement) actionList.get(i);

			// Delete of an inserted row
			if ((ce.getId() == 0) && !ce.getDeleted().equals(""))
				continue;

			if (ce.getId() == 0) {
				logger.debug("Inserting :" + ce);
				if (ce.getCeTypeId() != -1) {
					sql = INSERT_CE + "(" + db.getNextId("CE_ID") + "," + activityId + "," + ce.getCeTypeId() + "," + StringUtils.checkNumber(ce.getOfficerId()) + "," + StringUtils.checkString(ce.getName()) + "," + StringUtils.checkString(ce.getPhone()) + "," + "to_date(" + StringUtils.checkString(ce.getCeDate()) + ",'MM/DD/YY')" + "," + StringUtils.s2d(ce.getFeeAmount()) + "," + StringUtils.checkString(ce.getSubType()) + ")";
					db.addBatch(sql);
					logger.debug(sql);
				}
			} else if (!ce.getDeleted().equals("")) {
				sql = "DELETE FROM CODE_ENFORCEMENT " + " WHERE CE_ID=" + ce.getId();
				db.addBatch(sql);
				logger.debug(sql);
			} else {
				sql = "UPDATE CODE_ENFORCEMENT SET " + "OFFICER_ID=" + StringUtils.checkNumber(ce.getOfficerId()) + "," + "NAME=" + StringUtils.checkString(ce.getName()) + "," + "PHONE=" + StringUtils.checkString(ce.getPhone()) + "," + "CE_DATE=" + "to_date(" + StringUtils.checkString(ce.getCeDate()) + ",'MM/DD/YY')" + "," + "FEE_AMT=" + StringUtils.s2d(ce.getFeeAmount()) + "," + "SUB_TYPE=" + StringUtils.checkString(ce.getSubType()) + " WHERE CE_ID=" + ce.getId();
				db.addBatch(sql);
				logger.debug(sql);
			}
		}
		db.executeBatch();
		db.commitTransaction();

		logger.info("Exiting saveCodeEnforcementList()");
	}

	public List getUserList() throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "SELECT USERID,USERNAME,LAST_NAME,FIRST_NAME FROM USERS WHERE DEPT_ID=" + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES + " and active='Y' ORDER BY LAST_NAME,FIRST_NAME";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			list.add(new PickList(rs.getInt("USERID"), rs.getString("LAST_NAME") + ", " + rs.getString("FIRST_NAME")));
		}
		return list;
	}

	public void insertPkActivity(int actId, String dt, int pblock) throws Exception {
		Wrapper db = new Wrapper();
		Calendar c = StringUtils.str2cal(dt);
		StringBuffer sb = new StringBuffer();
		sb.append("INSERT INTO PARKING_ACTIVITY (ACT_ID,LNC_DT,PBLOCK) VALUES (" + actId + ",");
		sb.append("to_date(" + StringUtils.checkString(StringUtils.cal2str(c)) + ",'MM/DD/YYYY')" + "," + pblock + " )");
		logger.info(sb.toString());
		db.insert(sb.toString());
	}

	public int getPBlock(int actId) {
		int pblock = 1;
		try {
			String sql = "select max(PBLOCK)+1 as PBLOCK from PARKING_ACTIVITY where ACT_ID=" + actId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				if (rs.getString("PBLOCK") != null) {
					pblock = rs.getInt("PBLOCK");
				} else {
					pblock = 1;
				}
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return pblock;
	}

	public int insertPeopleForParkingActivity(int actId, String email, String fname, String lname, String dlno, String phno, String vehicleno) throws Exception {
		Wrapper db = new Wrapper();

		int peopleId = 0;
		boolean check = checkPeopleExists(email);
		if (check == false) {
			peopleId = db.getNextId("PEOPLE_ID");
			String sql = "INSERT INTO PEOPLE (PEOPLE_ID,PEOPLE_TYPE_ID,EMAIL_ADDR,NAME,DL_ID_NO,PHONE,LAST_NAME) VALUES (" + peopleId + ",2," + StringUtils.checkString(email) + "," + StringUtils.checkString((fname)) + "," + StringUtils.checkString(dlno) + "," + StringUtils.checkString(phno) + "," + StringUtils.checkString(lname) + ")";
			logger.info(sql);
			db.insert(sql);

			String sql1 = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID,PSA_TYPE) VALUES (" + actId + "," + peopleId + ",'A')";
			logger.info(sql1);
			db.insert(sql1);

			String sql2 = "INSERT INTO EXT_USER (EXT_USER_ID, EXT_ACCT_NBR, EXT_USERNAME, CREATED, FIRSTNAME, LASTNAME, ACTIVEOBC, ACTIVEDOT, OBC, DOT,PHONE,VEHICLE_NO,LICENSE_NO)" + " values (" + db.getNextId("EXT_USER_ID") + ", 0, " + StringUtils.checkString(email) + ", current_timestamp," + StringUtils.checkString((fname)) + "," + StringUtils.checkString((lname)) + ",'Y','N','Y','N'," + StringUtils.checkString(phno) + "," + StringUtils.checkString(vehicleno) + "," + StringUtils.checkString(dlno) + ")";
			logger.info(sql2);
			db.insert(sql2);
		} else {

			peopleId = new ActivityAgent().insertPeopleForParkingActivity(actId, email);
			/*
			 * String sql1 = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID,PSA_TYPE) VALUES ("+actId+","+db.getNextId("PEOPLE_ID")+",'A')"; logger.info(sql1); db.insert(sql1);
			 */
		}
		return peopleId;

	}

	public List getLNCVList(int actId) throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM PARKING_ACTIVITY WHERE ACT_ID=" + actId + "  order by LNC_DT desc";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			People lncvact = new People();
			lncvact.setDate(StringUtils.date2str(rs.getDate("LNC_DT")));
			lncvact.setTitle(StringUtils.i2s(rs.getInt("PBLOCK")));
			lncvact.setALDate(getDtPblock(actId, rs.getInt("PBLOCK")));
			list.add(lncvact);
		}
		if (rs != null)
			rs.close();
		return list;
	}

	public String getLNCVCount(int actId) throws Exception {
		String count = "0";
		Wrapper db = new Wrapper();
		String sql = "SELECT COUNT(LNC_DT) as COUNT FROM PARKING_ACTIVITY WHERE ACT_ID=" + actId + " order by PBLOCK desc";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			count = rs.getString("COUNT");
		}
		if (rs != null)
			rs.close();
		return count;
	}

	public static String getLNCVPrintUrl() throws Exception {
		String url = "";
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM LKUP_SYSTEM WHERE NAME='LNCV_PRINT'";
		logger.info(sql);
		RowSet rs = db.select(sql);
		if (rs.next()) {
			url = StringUtils.nullReplaceWithEmpty(rs.getString("VALUE"));
		}
		if (rs != null)
			rs.close();
		return url;
	}

	public List getLNCVCurrentList(int actId) throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM PARKING_ACTIVITY WHERE LNC_DT >= current_date - 1 and ACT_ID=" + actId + "  order by PBLOCK,LNC_DT ";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			People lncvact = new People();
			lncvact.setDate(StringUtils.date2str(rs.getDate("LNC_DT")));
			lncvact.setTitle(StringUtils.i2s(rs.getInt("PBLOCK")));
			lncvact.setALDate(getDtPblock(actId, rs.getInt("PBLOCK")));
			list.add(lncvact);
		}
		if (rs != null)
			rs.close();
		return list;
	}

	public List getLNCVPreviousList(int actId) throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM PARKING_ACTIVITY WHERE LNC_DT <= current_date - 1 and ACT_ID=" + actId + "   order by PBLOCK,LNC_DT ";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			People lncvact = new People();
			lncvact.setDate(StringUtils.date2str(rs.getDate("LNC_DT")));
			lncvact.setTitle(StringUtils.i2s(rs.getInt("PBLOCK")));
			// lncvact.setALDate(getDtPblock(actId, rs.getInt("PBLOCK")));
			list.add(lncvact);
		}
		if (rs != null)
			rs.close();
		return list;
	}

	public List getLNCVInternalList(int actId) throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM PARKING_ACTIVITY WHERE ACT_ID=" + actId + "  order by LNC_DT desc";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			People lncvact = new People();
			lncvact.setDate(StringUtils.date2str(rs.getDate("LNC_DT")));
			lncvact.setTitle(StringUtils.i2s(rs.getInt("PBLOCK")));
			lncvact.setALDate(getDtPblock(actId, rs.getInt("PBLOCK")));
			list.add(lncvact);
		}
		if (rs != null)
			rs.close();
		return list;
	}

	public List searchPeopleForParkingActivity(String email, String fname, String lname, String dlno) throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		StringBuffer sb = new StringBuffer();

		sb.append(" select distinct(p.people_id),pa.act_id,p.email_addr,p.name,p.DL_ID_NO,pt.description,p.phone,p.LAST_NAME,a.LABEL,a.start_date from people p left outer join people_type pt on p.people_type_id=pt.people_type_id left outer join activity_people ap on p.people_id=ap.people_id  join parking_activity pa on ap.act_id=pa.act_id left outer join activity a on pa.act_id=a.act_id where p.people_id >0  ");
		if (!email.equals("")) {
			sb.append(" and lower (email_addr) like '%" + email + "%'");
		}
		if (!dlno.equals("")) {
			sb.append(" and DL_ID_NO= " + StringUtils.checkString(dlno) + "");
		}
		if (!fname.equals("")) {
			sb.append(" or lower (name) like '%" + fname + "%'");
		}
		if (!lname.equals("")) {
			sb.append(" or lower (last_name) like '%" + lname + "%'");
		}

		Calendar c = Calendar.getInstance();
		int yr = c.get(Calendar.YEAR);
		logger.info(sb.toString());
		RowSet rs = db.select(sb.toString());
		while (rs.next()) {
			People lncvpeople = new People();
			int actId = 0;
			lncvpeople.setLicenseNbr(StringUtils.nullReplaceWithEmpty(rs.getString("DL_ID_NO")));
			lncvpeople.setEmailAddress(StringUtils.nullReplaceWithEmpty(rs.getString("email_addr")));
			lncvpeople.setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
			lncvpeople.setLastName(StringUtils.nullReplaceWithEmpty(rs.getString("LAST_NAME")));
			lncvpeople.setPhoneNbr(StringUtils.nullReplaceWithEmpty(rs.getString("PHONE")));
			lncvpeople.setPeopleId(rs.getInt("PEOPLE_ID"));
			lncvpeople.setComments(rs.getString("Description"));
			lncvpeople.setAgentName(StringUtils.nullReplaceWithEmpty(rs.getString("LABEL")));

			if (rs.getInt("ACT_ID") != 0 && rs.getString("START_DATE").indexOf(StringUtils.i2s(yr)) != -1) {
				lncvpeople.setActivityId(rs.getInt("ACT_ID"));
			} else {
				lncvpeople.setActivityId(actId);
			}

			list.add(lncvpeople);
		}
		return list;
	}

	public Map searchedPeopleForParkingActivity(int actId, int peopleId) throws Exception {
		Map m = new HashMap();
		Wrapper db = new Wrapper();
		StringBuffer sb = new StringBuffer();

		if (actId > 0) {
			sb.append(" select distinct(p.people_id),pa.act_id,p.email_addr,p.name,p.DL_ID_NO,pt.description,p.phone,p.LAST_NAME,a.LABEL,a.START_DATE from people p left outer join people_type pt on p.people_type_id=pt.people_type_id left outer join activity_people ap on p.people_id=ap.people_id left outer join parking_activity pa on ap.act_id=pa.act_id left outer join activity a on pa.act_id=a.act_id where p.people_id >0  and ap.act_id=" + actId + "");
		} else {
			sb.append(" select distinct(p.people_id),p.email_addr,p.name,p.DL_ID_NO,pt.description,p.phone,p.LAST_NAME from people p left outer join people_type pt on p.people_type_id=pt.people_type_id where p.people_id >0  and p.people_Id=" + peopleId + "");
		}
		logger.info(sb.toString());

		RowSet rs = db.select(sb.toString());
		while (rs.next()) {
			m.put("LIC_NO", StringUtils.nullReplaceWithEmpty(rs.getString("DL_ID_NO")));
			m.put("EMAIL", StringUtils.nullReplaceWithEmpty(rs.getString("email_addr")));
			m.put("NAME", StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
			m.put("LNAME", StringUtils.nullReplaceWithEmpty(rs.getString("LAST_NAME")));
			m.put("PHONE", StringUtils.nullReplaceWithEmpty(rs.getString("PHONE")));
			m.put("PEOPLE_ID", StringUtils.i2s(rs.getInt("PEOPLE_ID")));

			if (actId > 0) {
				m.put("ACT_ID", StringUtils.i2s(rs.getInt("ACT_ID")));
				m.put("LABEL", StringUtils.nullReplaceWithEmpty(rs.getString("LABEL")));
				m.put("COUNT", getLncvCount(rs.getInt("ACT_ID")));
			} else {
				m.put("ACT_ID", "");
				m.put("LABEL", "");
				m.put("COUNT", 1 + "");
			}

		}
		if (rs != null)
			rs.close();
		return m;
	}

	public String getLncvCount(int actId) throws Exception {
		String count = "1";
		Wrapper db = new Wrapper();
		String sql = "SELECT COUNT(*)+1 as LNCVCOUNT FROM PARKING_ACTIVITY WHERE ACT_ID=" + actId + "   ";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			count = rs.getString("LNCVCOUNT");
		}
		if (rs != null)
			rs.close();
		return count;
	}

	public boolean checkPeopleExists(String email) throws Exception {
		boolean peopleExists = false;
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM PEOPLE WHERE EMAIL_ADDR=" + StringUtils.checkString(email);
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			peopleExists = true;
		}
		if (rs != null)
			rs.close();
		return peopleExists;
	}

	public String updateParkingActivity(int actId, String oldDt, String newdt) throws Exception {
		Wrapper db = new Wrapper();
		Calendar c = StringUtils.str2cal(newdt);
		String msg = "Successfully updated.";
		try {
			String sql1 = " UPDATE PARKING_ACTIVITY SET LNC_DT = to_date(" + StringUtils.checkString(StringUtils.cal2str(c)) + ",'MM/DD/YYYY') WHERE LNC_DT = " + StringUtils.toOracleDate((oldDt)) + " and ACT_ID = " + actId;
			logger.info(sql1);
			db.insert(sql1);
		} catch (Exception e) {
			logger.error(e.getMessage());
			msg = "Error while updating the date is already chosen.";
		}
		return msg;
	}

	public String getLncvVehicleNo(String actnbr) throws Exception {
		String vId = "";
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM ACTIVITY WHERE ACT_NBR=" + StringUtils.checkString(actnbr);
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			vId = rs.getString("LABEL");
		}
		if (rs != null)
			rs.close();
		return vId;
	}

	public String getDtPblock(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + "  ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				result += StringUtils.date2str(rs.getDate("LNC_DT")) + ",";

			}
			if (result.endsWith(",")) {
				result = result.substring(0, result.length() - 1);
			}
			logger.debug("DAtes55***********" + result);
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}

	public int getPLncvDtBlock(int actId) {
		int pblock = 0;
		try {
			String sql = "select MAX(PBLOCK) as PBLOCK from PARKING_ACTIVITY where ACT_ID=" + actId + "  ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				if (rs.getString("PBLOCK") != null) {
					pblock = rs.getInt("PBLOCK");
				} else {
					pblock = 0;
				}
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return pblock;
	}

	public String getActivityType(int actId) {
		String type = "";
		try {
			String sql = "select * from activity where act_id=" + actId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				type = rs.getString("ACT_TYPE");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return type;
	}

	public int getDepartmentIdForActivityId(String activityId) throws Exception {
		logger.info("Entering getDepartmentCode");

		int departmentId;

		String sql = "select d.DEPT_ID from (activity a join lkup_act_type l on l.type=a.act_type) join department d on d.dept_id=l.dept_id " + "where a.act_id = " + activityId;
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			departmentId = rs.getInt("DEPT_ID");
		} else {
			departmentId = -1;
		}

		rs.close();
		logger.info("Exiting getDepartmentCode");

		return departmentId;
	}

	public List<People> getPeopleForActivity(int actId, int peopleTypeId) throws Exception {

		List<People> peopleList = new ArrayList<People>();
		String sql = "SELECT P.* FROM PEOPLE P " + "LEFT JOIN ACTIVITY_PEOPLE AP ON AP.PEOPLE_ID=P.PEOPLE_ID " + "WHERE AP.ACT_ID=" + actId + "";

		sql = peopleTypeId == 0 ? sql : sql + " AND P.PEOPLE_TYPE_ID=" + peopleTypeId;

		logger.debug("People for activity SQL: " + sql);
		Wrapper db = new Wrapper();
		String address = "";
		try {
			RowSet rs = db.select(sql);

			while (rs.next()) {

				People people = new People();
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setPeopleType(new PeopleAgent().getPeopleType(rs.getInt("PEOPLE_TYPE_ID")));

				people.setName(rs.getString("NAME"));
				people.setAddress(rs.getString("ADDR"));
				people.setCity(rs.getString("CITY"));
				people.setState(rs.getString("STATE"));
				people.setZipCode(rs.getString("ZIP"));
				people.setPhoneNbr(rs.getString("PHONE"));
				people.setExt(rs.getString("EXT"));
				people.setFaxNbr(rs.getString("FAX"));
				people.setEmailAddress(rs.getString("EMAIL_ADDR"));
				people.setComments(rs.getString("COMNTS"));
				people.setLicenseNbr(rs.getString("LIC_NO"));
				people.setLicenseExpires(rs.getDate("LIC_EXP_DT"));
				people.setBusinessLicenseNbr(rs.getString("BUS_LIC_NO"));
				people.setGeneralLiabilityDate(rs.getDate("GEN_LIABILITY_DT"));
				people.setAutoLiabilityDate(rs.getDate("AUTO_LIABILITY_DT"));
				people.setBusinessLicenseExpires(rs.getDate("BUS_LIC_EXP_DT"));
				people.setOnHold(rs.getString("ON_HOLD"));
				people.setHoldDate(rs.getDate("HOLD_DT"));
				people.setHoldComments(rs.getString("HOLD_COMNT"));
				people.setAgentName(rs.getString("AGENT_NAME"));
				people.setAgentPhone(rs.getString("AGENT_PH"));
				people.setDepositAmount(rs.getFloat("DEPOSIT_BALANCE"));
				people.setWorkersCompExpires(rs.getDate("WORK_COMP_EXP_DT"));
				people.setWorkersCompensationWaive(rs.getString("WORKERS_COMP_WAIVE"));
				people.setCopyApplicant(rs.getString("COPY_APPLICANT"));
				people.setHicFlag(rs.getString("HIC_FLAG"));
				people.setUl(rs.getString("UL#"));
				people.setWorkersCompPolicy(rs.getString("WC_POLICY_NO"));

				peopleList.add(people);
			}
		} catch (Exception e) {
			logger.error("error in getActivityDetailForMeeting", e);
			throw new AgentException("", e);
		}
		return peopleList;
	}

	public String getEmailAddress(List incidentList) throws Exception {
		try {
			logger.debug("entered get email address");
			Wrapper db = new Wrapper();
			RowSet rs = null;
			List list1;
			String emailId = "";
			String issue = "";
			String description = "";
			String streetNumber = "";
			String streetName = "";
			String unit = "";
			String address = "";
			String streetId = "";
			int deptId = 0;
			RowSet rs1 = null;
			RowSet rs2 = null;
			RowSet rs3 = null;

			for (int i = 0; i < incidentList.size(); i++) {
				addIncidentForm inci = (addIncidentForm) incidentList.get(i);
				issue = inci.getProblem();
				description = inci.getDescription();
				streetNumber = inci.getIncidentstreetnumber() != null ? inci.getIncidentstreetnumber() : "";
				streetId = inci.getIncidentstreetname() != null ? inci.getIncidentstreetname() : "";
				if (streetId != null) {
					String sql2 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + streetId;
					logger.debug("sql2=" + sql2);
					rs2 = db.select(sql2);
					if (rs2.next()) {
						streetName = (rs2.getString("PRE_DIR") != null ? rs2.getString("PRE_DIR") : "") + " " + (rs2.getString("STR_NAME") != null ? rs2.getString("STR_NAME") : "") + " " + (rs2.getString("STR_TYPE") != null ? rs2.getString("STR_TYPE") : "");
						logger.debug("streetname=" + streetName);
					} else {
						streetName = "";
						logger.debug("streetname=" + streetName);
					}
					rs2.close();
				}

				unit = inci.getIncidentunit() != null ? inci.getIncidentunit() : "";
				address = streetNumber + " " + streetName + " " + unit;
				logger.debug("problem in inci loop********" + issue);
				String sql = "select DEPT_ID from LKUP_DEPT_PROBLEM where problem='" + issue + "'";
				rs = db.select(sql);
				logger.debug(sql);
				while (rs.next()) {
					deptId = rs.getInt("DEPT_ID");

					if (deptId == Constants.DEPARTMENT_BUILDING_SAFETY) {
						String sql1 = "select email_id from user_groups ug join users u on ug.user_id=u.userid where group_id IN (" + Constants.GROUPS_RFS_NOTIFICATION + "," + Constants.GROUPS_BUILDING + ")";
						rs1 = db.select(sql1);
						logger.debug(sql1);
						while (rs1.next()) {
							if (rs1.getString("email_id") != null) {
								emailId = emailId + rs1.getString("email_id") + ";";
							}
						}
						emailId = emailId + issue + "," + description + "*" + address + ":";
					}

					if (deptId == Constants.DEPARTMENT_PLANNING_AND_COMMUNITY_DEVELOPMENT) {
						String sql1 = "select email_id from user_groups ug join users u on ug.user_id=u.userid where group_id IN (" + Constants.GROUPS_RFS_NOTIFICATION + "," + Constants.GROUPS_PLANNING + ")";
						rs2 = db.select(sql1);
						logger.debug(sql1);
						while (rs2.next()) {
							if (rs2.getString("email_id") != null) {
								emailId = emailId + rs2.getString("email_id") + ";";
							}
						}
						emailId = emailId + issue + "," + description + "*" + address + ":";
					}

					if (deptId == Constants.DEPARTMENT_NEIGHBOURHOOD_SERVICES) {
						String sql1 = "select email_id from user_groups ug join users u on ug.user_id=u.userid where group_id IN (" + Constants.GROUPS_RFS_NOTIFICATION + "," + Constants.GROUPS_NEIGHBOURHOODSERVICES + ")";
						rs3 = db.select(sql1);
						logger.debug(sql1);
						while (rs3.next()) {
							if (rs3.getString("email_id") != null) {
								emailId = emailId + rs3.getString("email_id") + ";";
							}
						}
						emailId = emailId + issue + "," + description + "*" + address + ":";
					}
				}
			}

			logger.debug("email id in addrequesteragent****" + emailId);
			return emailId;

		} catch (Exception e) {
			logger.error("Exception occured in getEmailAddress method . Message-" + e.getMessage());
			throw e;
		}
	}

	public void saveRequester(AddRequesterForm addrequesterform, List incidentList) throws Exception {
		logger.debug("entering in AddRequesterAgent ..");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		Date now = new Date();
		String date = StringUtils.date2str(now);
		logger.debug("date is .." + now);

		int requesterid = db.getNextId("RFS_ID");
		logger.debug("requesterid=" + requesterid);
		String sql = "insert into RFS (RFS_ID,REQ_SRC,CONTACTED,CITY_EMP,DEPT_ID,FIRST_NAME,LAST_NAME,STREET_NO,STREET_NAME,UNIT,CITY,STATE,ZIP,PHONE_NO,PHONE_NO_EXT,ALT_PHONE_NO,ALT_PHONE_NO_EXT,EMAIL,CREATED)values(" + requesterid + "," + StringUtils.checkString(addrequesterform.getRequestsource()) + "," + StringUtils.checkString(StringUtils.b2s(addrequesterform.isContact())) + "," + StringUtils.checkString(StringUtils.b2s(addrequesterform.isCityemployee())) + "," + addrequesterform.getRequesterdepartment() + "," + StringUtils.checkString(addrequesterform.getFirstname()) + "," + StringUtils.checkString(addrequesterform.getLastname()) + "," + StringUtils.checkString(addrequesterform.getRequesterstreetnumber()) + "," + StringUtils.checkString(addrequesterform.getRequesterstreetname()) + "," + StringUtils.checkString(addrequesterform.getRequesterunit()) + "," + StringUtils.checkString(addrequesterform.getRequestercity()) + "," + StringUtils.checkString(addrequesterform.getRequesterstate()) + "," + StringUtils.checkString(addrequesterform.getRequesterzip()) + "," + StringUtils.checkString(addrequesterform.getPhonenumber()) + "," + StringUtils.checkString(addrequesterform.getExtension()) + "," + StringUtils.checkString(addrequesterform.getAltphonenumber()) + "," + StringUtils.checkString(addrequesterform.getAltextension()) + "," + StringUtils.checkString(addrequesterform.getEmail()) + ",to_date('" + date + "','mm/dd/yyyy'))";
		logger.debug("Add requester sql .." + sql);
		try {
			db.insert(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (incidentList.size() > 0) {
			logger.debug("INCIDENT LIST SIZE .." + incidentList.size());
			ActivityAgent activityForRFSAgent = new ActivityAgent();
			AddressAgent streetAgent = new AddressAgent();
			try {

				String unit = "";
				for (int i = 0; i < incidentList.size(); i++) {
					addIncidentForm inci = (addIncidentForm) incidentList.get(i);
					int lsoid = activityForRFSAgent.checkAddress(inci.getIncidentstreetnumber(), inci.getIncidentstreetname(), inci.getIncidentunit());
					String poi = "";
					if (lsoid == 0) {
						poi = inci.getPoi() + " " + inci.getIncidentstreetnumber();
						poi = poi + " " + streetAgent.getStreetName(inci.getIncidentstreetname());
						poi = poi + " " + inci.getIncidentunit();
						inci.setPoi(poi);
						inci.setIncidentstreetnumber(null);
						inci.setIncidentstreetname("-1");
						inci.setIncidentunit(null);
					}

					else if (lsoid != 0) {

						String unitSql = "select unit from lso_address where LSO_ID = " + lsoid;
						logger.debug("Unit" + unitSql);
						rs = db.select(unitSql);

						while (rs.next()) {
							unit = rs.getString("unit");
							logger.debug("Unit" + unit);
						}

						inci.setIncidentunit(unit);

					}

					else {
						inci.setIncidentunit(null);
					}

					int InciId = db.getNextId("INCIDENT_ID");

					logger.debug("unit is" + inci.getIncidentunit());
					String sql1 = "insert into INCIDENT (INCIDENT_ID,RFS_ID,POI,PRIORITY,STREET_NO,STREET_NAME,UNIT,CROSS_STREET1,CROSS_STREET2,CITY,STATE,ZIP,PROBLEM,DEPT_ID,DESCRIPTION,MEMO,CREATED,INCI_STATUS)values(" + InciId + "," + requesterid + "," + StringUtils.checkString(inci.getPoi()) + "," + StringUtils.checkString(inci.getPriority()) + "," + StringUtils.checkString(inci.getIncidentstreetnumber()) + "," + StringUtils.checkString(inci.getIncidentstreetname()) + "," + StringUtils.checkString(inci.getIncidentunit()) + "," + StringUtils.checkString(inci.getCrossstreet1()) + "," + StringUtils.checkString(inci.getCrossstreet2()) + "," + StringUtils.checkString(inci.getIncidentcity()) + "," + StringUtils.checkString(inci.getIncidentstate()) + "," + StringUtils.checkString(inci.getIncidentzip()) + "," + StringUtils.checkString(inci.getProblem()) + "," + inci.getIncidentdepartment() + "," + StringUtils.checkString(inci.getDescription()) + "," + StringUtils.checkString(inci.getMemo()) + ",to_date('" + date + "','mm/dd/yyyy')" + "," + StringUtils.checkNumber(7) + ")";
					// db.addBatch(sql1);
					logger.debug("Add incident sql .." + sql1);
					db.insert(sql1);
					// logger.debug("Add incident sql .." + sql1);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// db.executeBatch();
	}

	public int checkAddress(String strNo, String strId, String unit) {
		int lsoId = 0;
		logger.info("checkAddress(" + strNo + "," + strId + "," + unit + ")");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		String sql = "";
		String sql1 = "";
		try {
			sql = "SELECT LSO_ID FROM LSO_ADDRESS WHERE ";

			if (!(GenericValidator.isBlankOrNull(strNo))) {
				sql += (" STR_NO=" + strNo);
			}
			if (!(StringUtils.s2i(strId) == 0)) {
				if (sql.endsWith("WHERE "))
					sql += ("  STREET_ID=" + strId);
				else
					sql += (" and STREET_ID=" + strId);
			}

			if (!((unit == null) || (unit.trim().equalsIgnoreCase("")))) {
				String lsoId1 = "";
				sql += (" and lower(UNIT) = (lower(" + (StringUtils.checkString(unit)) + "))");

				sql += " and LSO_TYPE IN('L','S','O')";
				rs = db.select(sql);
				while (rs.next()) {
					lsoId = rs.getInt("LSO_ID");

					lsoId1 = StringUtils.i2s(lsoId);
				}

				if (lsoId1 == null || lsoId1.equalsIgnoreCase("")) {
					sql1 = "SELECT LSO_ID FROM LSO_ADDRESS WHERE  STR_NO=" + strNo + " and STREET_ID=" + strId + "and lower(TRIM(SUBSTR(UNIT,INSTR(UNIT,' ',1,1),LENGTH(UNIT)))) = lower('" + unit + "')";

					sql1 += " and LSO_TYPE IN('L','S','O') order by LSO_ID";

					rs = db.select(sql1);
					while (rs.next()) {
						lsoId = rs.getInt("LSO_ID");
					}
					logger.debug("sql :" + sql1);

					logger.debug("lsoId" + lsoId);
					return lsoId;
				}
			}

			else {
				sql += " and LSO_TYPE IN('L','S','O') order by DECODE(LSO_TYPE,'O','L','S') , LSO_ID ";

			}

			logger.debug("sql :" + sql);

			rs = db.select(sql);
			while (rs.next()) {
				lsoId = rs.getInt("LSO_ID");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("lsoId***" + lsoId);
		return lsoId;
	}

	public AddRequesterForm getRequestorDetails(String rfsNo) {
		logger.info("Entered getRequestorDetails(" + rfsNo + ")");
		AddRequesterForm addRequesterForm = new AddRequesterForm();
		String sqlRFS = "select * from RFS where RFS_ID=" + rfsNo;
		RowSet rsRFS = null;
		List incidentList = new ArrayList();
		RowSet rsInci = null;
		try {
			logger.debug(sqlRFS);
			rsRFS = new Wrapper().select(sqlRFS);

			if (rsRFS.next()) {
				logger.debug(rsRFS);
				String sqlInci = "select * from INCIDENT I left join LKUP_CATEGORIES LC on I.INCI_STATUS = LC.CATEGORY_ID where RFS_ID=" + rfsNo;
				logger.debug(sqlInci);
				rsInci = new Wrapper().select(sqlInci);

				while (rsInci.next()) {
					addIncidentForm addIncidentForm = new addIncidentForm();
					addIncidentForm.setIncidentid(StringUtils.s2i(rsInci.getString("INCIDENT_ID")));
					addIncidentForm.setPoi(rsInci.getString("POI"));
					addIncidentForm.setPriority(rsInci.getString("PRIORITY"));
					addIncidentForm.setIncidentstreetnumber(rsInci.getString("STREET_NO"));
					addIncidentForm.setIncidentstreetname(rsInci.getString("STREET_NAME"));
					addIncidentForm.setIncidentunit(rsInci.getString("UNIT"));
					addIncidentForm.setCrossstreet1(rsInci.getString("CROSS_STREET1"));
					addIncidentForm.setCrossstreet2(rsInci.getString("CROSS_STREET2"));
					addIncidentForm.setIncidentcity(rsInci.getString("CITY"));
					addIncidentForm.setIncidentstate(rsInci.getString("STATE"));
					addIncidentForm.setIncidentzip(rsInci.getString("ZIP"));
					addIncidentForm.setProblem(rsInci.getString("PROBLEM"));
					addIncidentForm.setIncidentdepartment(rsInci.getString("DEPT_ID"));
					addIncidentForm.setDescription(rsInci.getString("DESCRIPTION"));
					addIncidentForm.setMemo(rsInci.getString("MEMO"));
					addIncidentForm.setStatus(rsInci.getString("CATEGORY_VALUE"));
					incidentList.add(addIncidentForm);
					addRequesterForm.addIncidentList(addIncidentForm);
				}

				logger.debug("incidentList size is " + incidentList.size());

				logger.debug("incident list is  " + incidentList.size());
				logger.debug("incident list size is  " + addRequesterForm.getIncidentList().size());

				rsInci.close();
				addRequesterForm.setRequesterid(StringUtils.s2i(rsRFS.getString("RFS_ID")));
				addRequesterForm.setRequestsource(rsRFS.getString("REQ_SRC"));
				addRequesterForm.setContact(StringUtils.s2b(rsRFS.getString("CONTACTED")));
				addRequesterForm.setCityemployee(StringUtils.s2b(rsRFS.getString("CITY_EMP")));
				addRequesterForm.setRequesterdepartment(rsRFS.getString("DEPT_ID"));
				addRequesterForm.setFirstname(rsRFS.getString("FIRST_NAME"));
				addRequesterForm.setLastname(rsRFS.getString("LAST_NAME"));
				addRequesterForm.setRequesterstreetnumber(rsRFS.getString("STREET_NO"));
				addRequesterForm.setRequesterstreetname(rsRFS.getString("STREET_NAME"));
				addRequesterForm.setRequesterunit(rsRFS.getString("UNIT"));
				addRequesterForm.setRequestercity(rsRFS.getString("CITY"));
				addRequesterForm.setRequesterstate(rsRFS.getString("STATE"));
				addRequesterForm.setRequesterzip(rsRFS.getString("ZIP"));
				addRequesterForm.setPhonenumber(rsRFS.getString("PHONE_NO"));
				addRequesterForm.setExtension(rsRFS.getString("PHONE_NO_EXT"));
				addRequesterForm.setAltphonenumber(rsRFS.getString("ALT_PHONE_NO"));
				addRequesterForm.setAltextension(rsRFS.getString("ALT_PHONE_NO_EXT"));
				addRequesterForm.setEmail(rsRFS.getString("EMAIL"));

			}
			rsRFS.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while getting RFS" + e.getMessage());
		}
		return addRequesterForm;
	}

	public void updateRequester(AddRequesterForm addrequesterform, List incidentList, String rfsNo) throws Exception {
		logger.debug("entering in updateRequester ..");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		Date now = new Date();
		String date = StringUtils.date2str(now);
		logger.debug("date is .." + now);

		String sql = "update RFS set REQ_SRC=" + StringUtils.checkString(addrequesterform.getRequestsource()) + ",CONTACTED=" + StringUtils.checkString(StringUtils.b2s(addrequesterform.isContact())) + ",CITY_EMP=" + StringUtils.checkString(StringUtils.b2s(addrequesterform.isCityemployee())) + ",DEPT_ID=" + addrequesterform.getRequesterdepartment() + ",FIRST_NAME=" + StringUtils.checkString(addrequesterform.getFirstname()) + ",LAST_NAME=" + StringUtils.checkString(addrequesterform.getLastname()) + ",STREET_NO=" + StringUtils.checkString(addrequesterform.getRequesterstreetnumber()) + ",STREET_NAME=" + StringUtils.checkString(addrequesterform.getRequesterstreetname()) + ",UNIT=" + StringUtils.checkString(addrequesterform.getRequesterunit()) + ",CITY=" + StringUtils.checkString(addrequesterform.getRequestercity()) + ",STATE=" + StringUtils.checkString(addrequesterform.getRequesterstate()) + ",ZIP=" + StringUtils.checkString(addrequesterform.getRequesterzip()) + ",PHONE_NO=" + StringUtils.checkString(addrequesterform.getPhonenumber()) + ",PHONE_NO_EXT=" + StringUtils.checkString(addrequesterform.getExtension()) + ",ALT_PHONE_NO=" + StringUtils.checkString(addrequesterform.getAltphonenumber()) + ",ALT_PHONE_NO_EXT=" + StringUtils.checkString(addrequesterform.getAltextension()) + ",EMAIL=" + StringUtils.checkString(addrequesterform.getEmail()) + ",CREATED=to_date('" + date + "','mm/dd/yyyy') where RFS_ID=" + rfsNo;
		logger.debug("Add requester sql .." + sql);
		try {
			db.update(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (incidentList.size() > 0) {
			logger.debug("INCIDENT LIST SIZE .." + incidentList.size());
			try {
				ActivityAgent activityForRFSAgent = new ActivityAgent();
				AddressAgent streetAgent = new AddressAgent();
				int incidentId = 0;
				for (int i = 0; i < incidentList.size(); i++) {
					addIncidentForm inci = (addIncidentForm) incidentList.get(i);
					incidentId = inci.getIncidentid();
					int lsoid = activityForRFSAgent.checkAddress(inci.getIncidentstreetnumber(), inci.getIncidentstreetname(), inci.getIncidentunit());
					String poi = "";
					if (lsoid == 0) {
						poi = inci.getPoi() + " " + inci.getIncidentstreetnumber();
						poi = poi + " " + streetAgent.getStreetName(inci.getIncidentstreetname());
						poi = poi + " " + inci.getIncidentunit();
						inci.setPoi(poi);
						inci.setIncidentstreetnumber(null);
						inci.setIncidentstreetname("-1");
						inci.setIncidentunit(null);
					}
					String sql2 = "update INCIDENT set POI=" + StringUtils.checkString(inci.getPoi()) + ",PRIORITY=" + StringUtils.checkString(inci.getPriority()) + ",STREET_NO=" + StringUtils.checkString(inci.getIncidentstreetnumber()) + ",STREET_NAME=" + StringUtils.checkString(inci.getIncidentstreetname()) + ",UNIT=" + StringUtils.checkString(inci.getIncidentunit()) + ",CROSS_STREET1=" + StringUtils.checkString(inci.getCrossstreet1()) + ",CROSS_STREET2=" + StringUtils.checkString(inci.getCrossstreet2()) + ",CITY=" + StringUtils.checkString(inci.getIncidentcity()) + ",STATE=" + StringUtils.checkString(inci.getIncidentstate()) + ",ZIP=" + StringUtils.checkString(inci.getIncidentzip()) + ",PROBLEM=" + StringUtils.checkString(inci.getProblem()) + ",DEPT_ID=" + inci.getIncidentdepartment() + ",DESCRIPTION=" + StringUtils.checkString(inci.getDescription()) + ",MEMO=" + StringUtils.checkString(inci.getMemo()) + ",CREATED=to_date('" + date + "','mm/dd/yyyy') where INCIDENT_ID=" + incidentId;
					db.insert(sql2);
					logger.debug("Add incident sql .." + sql2);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// db.executeBatch();
	}

	public String getDepartmentIdForProblem(String problem) {
		logger.info("Entered getDepartmentIdForProblem(" + problem + ")");
		RowSet rsdept = null;
		String deptid = "";
		AddRequesterForm addRequesterForm = new AddRequesterForm();
		String sql = "select dept_id from LKUP_DEPT_PROBLEM where problem='" + problem + "'";
		logger.debug(sql);
		try {
			rsdept = new Wrapper().select(sql);

			if (rsdept.next()) {
				deptid = rsdept.getString("dept_id");
				logger.debug("deptid=" + deptid);

			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception while getting RFS" + e.getMessage());
		}
		return deptid;
	}

	public String getZip(int streetId) throws AgentException {
		logger.info("Entered getZip(" + streetId + ")");
		RowSet rsdept = null;
		String zipCode = "";
		AddRequesterForm addRequesterForm = new AddRequesterForm();
		String sql = "select * from lso_address where street_id=" + streetId + " and  rownum < 2";
		logger.debug(sql);
		try {
			rsdept = new Wrapper().select(sql);

			if (rsdept.next()) {
				zipCode = rsdept.getString("zip");
				logger.debug("zipCode=" + zipCode);

			}

		} catch (Exception e) {
			logger.error("Exception while getting ZIP", e);
			throw new AgentException("", e);
		}
		return zipCode;
	}

	public Map getAddressData(String streetId, String streetNo) throws Exception {

		String getAddressQuery = "";
		if (!streetId.equals("") && !streetNo.equals("")) {
			logger.debug("entered  1");
			getAddressQuery = "select * from LSO_ADDRESS where STREET_ID = ? and STR_NO = ? and LSO_TYPE = 'L'";
		} else {
			logger.debug("entered  2");
			getAddressQuery = "select * from LSO_ADDRESS where STREET_ID = ?  and LSO_TYPE = 'L' and rownum < 2 ";
		}
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		Map addressMap = new HashMap();

		try {
			connection = wrapper.getConnection();

			pstmt = connection.prepareStatement(getAddressQuery);

			pstmt.setString(1, streetId);

			if (!streetId.equals("") && !streetNo.equals("")) {
				logger.debug("entered  1");
				pstmt.setString(2, streetNo);
			}

			rs = pstmt.executeQuery();

			if (rs != null && rs.next()) {
				addressMap.put("city", rs.getString("CITY"));
				addressMap.put("state", rs.getString("STATE"));
				addressMap.put("zip", rs.getString("ZIP"));
			}
		} catch (Exception ex) {
			logger.error("Exception in getAddressData:", ex);
			throw ex;
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}

			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return addressMap;
	}



	public int checkAddress(String inciId) {
		int lsoId = 0;
		logger.info("checkAddress(" + inciId + ")");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		// String sql = "";
		String sql = "select STREET_NO,STREET_NAME,UNIT from INCIDENT where INCIDENT_ID=" + inciId;
		logger.debug("sql :" + sql);
		String strNo = "";
		String strId = "";
		String unit = "";
		try {
			rs = db.select(sql);
			if (rs.next()) {
				strNo = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : Constants.NONLOCATIONAL_STREET_NO;
				strId = !rs.getString("STREET_NAME").equals("-1") ? rs.getString("STREET_NAME") : Constants.NONLOCATIONAL_STREET_ID;
				unit = rs.getString("UNIT");
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lsoId = checkAddress(strNo, strId, unit);
	}

	public String getIncidentAddressId(String id) {
		logger.info("getIncidentAddressId(" + id + ")");
		String addressId = "-1";
		String strNo = "";
		String strID = "";

		try {
			String sql = "select STREET_NO,STREET_NAME from INCIDENT where INCIDENT_ID=" + id;
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			if (rs.next()) {
				strNo = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : Constants.NONLOCATIONAL_STREET_NO;
				strID = !rs.getString("STREET_NAME").equals("-1") ? rs.getString("STREET_NAME") : Constants.NONLOCATIONAL_STREET_ID;
				logger.debug("strID" + strID);
				if (!strID.equals("-1"))
					addressId = getAddressId(strNo, strID);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return addressId;
	}

	public String getAddressId(String strNo, String strID) {
		logger.info("getAddressId(" + strNo + "," + strID + ")");
		String addressId = "0";

		try {
			Wrapper db = new Wrapper();
			RowSet rs = null;
			String sql = "select ADDR_ID from LSO_ADDRESS where STR_NO=" + strNo + " and STREET_ID=" + strID + " order by lso_type ";

			logger.debug(sql);
			rs = db.select(sql);

			if (rs.next()) {
				addressId = rs.getString("ADDR_ID");
			}
			rs.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return addressId;
	}

	public String getIncidentCrossStreet(String id) {
		logger.info("getIncidentCrossStreet(" + id + ")");
		String crossStreet = "";
		List addressList = new ArrayList();

		try {
			String sql = "select CROSS_STREET1,CROSS_STREET2 from INCIDENT where INCIDENT_ID=" + id;
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			RowSet rs1 = null;
			if (rs.next()) {
				String sql2 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID IN (" + rs.getString("CROSS_STREET1") + "," + rs.getString("CROSS_STREET2") + ")";
				rs1 = db.select(sql2);
				logger.debug(sql2);
				while (rs1.next()) {
					crossStreet = crossStreet + " " + (rs1.getString("PRE_DIR") != null ? rs1.getString("PRE_DIR") : "") + " " + rs1.getString("STR_NAME") + " " + (rs1.getString("STR_TYPE") != null ? rs1.getString("STR_TYPE") : "") + " -";
				}
				rs1.close();
			}
			rs.close();
			if (crossStreet.length() != 0)
				crossStreet = crossStreet.substring(0, crossStreet.length() - 1);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return crossStreet;
	}

	public String getIncidentPOI(String id) {
		logger.info("getIncidentPOI(" + id + ")");
		String poi = "";

		try {
			String sql = "select POI from INCIDENT where INCIDENT_ID=" + id;
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			if (rs.next()) {
				poi = rs.getString("POI");
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.debug("poi" + poi);
		return poi;
	}

	public String getIncidentProblem(String id) {
		logger.info("getIncidentProblem(" + id + ")");
		String problem = "";

		try {
			String sql = "select PROBLEM,DESCRIPTION from INCIDENT where INCIDENT_ID=" + id;
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			if (rs.next()) {

				problem = rs.getString("PROBLEM") + "\n" + (rs.getString("DESCRIPTION") != null ? rs.getString("DESCRIPTION") : "");
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return problem;
	}

	public String getActivityTypeForProblem(String id) {
		logger.info("getActivityTypeForProblem(" + id + ")");
		String abc = "";

		String type = "";

		try {
			String sql = "select lat.type from INCIDENT I left outer join lkup_act_type lat on  I.PROBLEM= LAT.description where I.INCIDENT_ID=" + id;
			logger.debug("get act type" + sql);
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			if (rs.next()) {
				type = rs.getString("type") != null ? rs.getString("type") : "";
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return type;
	}

	public void mapIncidentActivity(int activityId, String inciId) {
		logger.info("getIncidentProblem( " + activityId + " , " + inciId + ")");
		String problem = "";

		try {
			int rfsID = 0;
			String sql = "SELECT RFS_ID FROM INCIDENT WHERE INCIDENT_ID =" + inciId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				rfsID = rs.getInt("RFS_ID");
			}
			rs.close();

			sql = "insert into RFS_ACT(RFS_ID,INCIDENT_ID,ACT_ID)values(" + rfsID + "," + inciId + "," + activityId + ")";
			logger.debug(sql);
			Wrapper db = new Wrapper();
			db.insert(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public String getUpdateStatus(int activityId, Activity activity, int userId) throws Exception {
		String sql = "";
		Wrapper db = new Wrapper();
		int actStatId = db.getNextId("act_stat_id");

		ActivityHistory status = new ActivityHistory();
		ActivityHistoryForm form = new ActivityHistoryForm();

		logger.debug("entered getUpdateStatus()");

		sql = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + activity.getActivityDetail().getStatus().getStatus() + ",current_date," + userId + ",null," + "null," + StringUtils.checkString("Status while adding activity") + ",current_timestamp)";
		db.insert(sql);

		logger.debug(sql);

		return sql;
	}

	public List getAddVMPActivitiesSqls(int actId) throws AgentException {
		logger.debug("getSaveVMPActivitiesSqls()");
		List sqls = new ArrayList();
		RowSet rs = null;
		int activityId = -1;
		try {
			Wrapper db = new Wrapper();
			int lsoId = new AddressAgent().getLsoId("" + actId, "A");
			String sql = "select FIRE_BLOCK,FIRE_ENGINE_ID from LSO_ADDRESS  where lso_id = " + lsoId;
			rs = db.select(sql);
			String fireBlock = "";
			String fireEngineId = "";
			if (rs != null && rs.next()) {
				fireBlock = rs.getString("FIRE_BLOCK");
				fireEngineId = rs.getString("FIRE_ENGINE_ID");
			}
			activityId = 0;
			logger.debug("**********" + activityId);

			sql = "INSERT INTO VMP_ACT (ACT_ID, FIRE_ENGINE_ID, FIRE_BLOCK) VALUES (" + actId + ", ";

			if (fireEngineId != null && !fireEngineId.equalsIgnoreCase("")) {
				sql = sql + "'" + fireEngineId + "',";
			} else {
				sql = sql + "'',";
			}

			sql = sql + "'" + fireBlock + "')";

			db.insert(sql);
		} catch (Exception e) {
			logger.error("Error : ", e);
			throw new AgentException("", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return sqls;
	}

	public void updateActivityExpirationDate(String date, int actId, String actionCode) throws Exception {

		ActivityType activityType = new LookupAgent().getActivityTypeForActId(actId);

		if (activityType.getModuleId() == Constants.MODULEID_BUILDING && activityType.getSubProjectType() == Constants.PROJECT_NAME_PERMITS_ID && actionCode.equals(StringUtils.i2s(Constants.INSPECTION_CODES_APPROVED))) {
			Calendar cal = new GregorianCalendar();
			cal.add(Calendar.DATE, Constants.DAYS_FOR_PERMIT_EXPIRATION);
			String sql = "UPDATE ACTIVITY SET EXP_DATE  = " + StringUtils.toOracleDate(StringUtils.cal2str(cal)) + " WHERE ACT_ID = " + actId;
			logger.debug("sql to Update Expiration Date:" + sql);
			Wrapper db = new Wrapper();
			db.update(sql);
		}

	}

	/**
	 * Function to save newly created inspection data. Called from SaveNewInspectionAction.
	 */
	public void saveNewCENotices(CodeEnforcement form) {
		ActivityAgent activityAgent = new ActivityAgent();

		try {
			Wrapper db = new Wrapper();
			String inspectionId = StringUtils.i2s(db.getNextId("INSPECTION_ID"));
			// String actionCode = form.getActionCode();
			// String activityId = form.getActivityId();
			// logger.debug("action code ### : " + actionCode);
			// logger.debug("outstanding Fees ### : " + outstandingFees);

			String sql = "INSERT INTO CODE_ENFORCEMENT (CE_ID,ACT_ID,CE_TYPE_ID,OFFICER_ID,CE_DATE) VALUES (" + db.getNextId("CE_ID") + ", " + form.getId() + ", " + form.getCeTypeId() + ", " + form.getOfficerId() + ", to_date('" + form.getCeDate() + "','MM/DD/YYYY'))";
			logger.debug(sql);
			db.insert(sql);

			// to insert newly created record in inspection history table
			// int historyid=db.getNextId("INSP_HISTORY_ID");
			// String sql1 = "INSERT INTO INSPECTION_HISTORY(INSPECTION_HISTORY_ID,INSPECTION_ID,ACT_ID,INSPECTOR_ID,INSPCT_ITEM_ID,INSPECTION_DT,ACTN_CODE,TIMESTAMP) VALUES (" +historyid + ", " + form.getInspectionId()+ ", " +form.getActivityId() + ", " + form.getInspectorId() + ", " + form.getInspectionItem() + ", to_date('" + form.getInspectionDate() + "','MM/DD/YYYY'), " + form.getActionCode() + ", " + " current_timestamp" + ")";
			// logger.debug("query to insert record"+sql1);
			// db.insert(sql1);

			// Update activity with expiration date 180 days after inspection date
			// Calendar cal = StringUtils.str2cal(form.getInspectionDate());
			// cal.add(Calendar.DAY_OF_YEAR, 180); //add 180 days to today's date.
			// sql = "update activity set exp_date=" + StringUtils.toOracleDate(StringUtils.cal2str(cal)) + " where act_id=" + activityId;
			// logger.debug(sql);
			// db.insert(sql);

			// if (!form.getActionCode().equals(StringUtils.i2s(Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION))) {
			// sql = "update inspection set actn_code = " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION_HISTORY + " where actn_code=Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION and act_id = " + form.getActivityId() + " and inspct_item_id = " + form.getInspectionItem();
			// logger.debug(sql);
			// db.update(sql);
			// }
		} catch (Exception e) {
			logger.error("Exception: " + e.getMessage());
		}
	}

	public int checkAddressForLand(String inciId) {
		int lsoId = 0;
		logger.info("checkAddressForLand(" + inciId + ")");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		// String sql = "";
		String sql = "select STREET_NO,STREET_NAME,UNIT from INCIDENT where INCIDENT_ID=" + inciId;
		logger.debug("sql :" + sql);
		String strNo = "";
		String strId = "";
		String unit = "";
		try {
			rs = db.select(sql);
			if (rs.next()) {
				strNo = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : Constants.NONLOCATIONAL_STREET_NO;
				strId = !rs.getString("STREET_NAME").equals("-1") ? rs.getString("STREET_NAME") : Constants.NONLOCATIONAL_STREET_ID;
				unit = rs.getString("UNIT");
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lsoId = checkAddressForLand(strNo, strId, unit);
	}

	public int checkAddressForLand(String strNo, String strId, String unit) {
		int lsoId = 0;
		logger.info("checkAddressForLand(" + strNo + "," + strId + "," + unit + ")");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		String sql = "";
		try {
			sql = "SELECT LSO_ID FROM LSO_ADDRESS WHERE ";

			if (!(GenericValidator.isBlankOrNull(strNo))) {
				sql += (" STR_NO=" + strNo);
			}
			if (!(StringUtils.s2i(strId) == 0)) {
				sql += (" and STREET_ID=" + strId);
			}
			if (!((unit == null) || (unit.equalsIgnoreCase("")))) {
				sql += (" and UNIT=" + StringUtils.checkString(unit));
			}

			sql += " and LSO_TYPE = 'L'";

			logger.debug("sql :" + sql);

			rs = db.select(sql);
			while (rs.next()) {
				lsoId = rs.getInt("LSO_ID");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		// logger.debug("lsoId***"+lsoId);
		return lsoId;
	}

	/**
	 * Gets the lite activies list for a sub project
	 * 
	 * @param subProjectId
	 * @return
	 * @throws Exception
	 */
	public List getActivitiesLite(int subProjectId) throws Exception {
		logger.info("getActivitiesLite(" + subProjectId + ")");
		List<Activity> activities;

		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select a.*,val.address,las.status_id,las.stat_code as status_code,las.description as status_desc,lat.type as type,lat.description as type_desc from ((activity a join v_psa_list vpl on vpl.act_id=a.act_id) join v_address_list val on vpl.lso_id=val.lso_id) join lkup_act_st las on las.status_id=a.status,lkup_act_type lat  where lat.type=a.act_type and a.sproj_id=" + subProjectId + " order by a.applied_date desc,a.act_id desc";

			logger.debug(sql);
			rs = db.select(sql);
			activities = new ArrayList();

			while (rs.next()) {
				Activity activity = new Activity(rs.getInt("act_id"), getActivityDetail(rs));
				activities.add(activity);
			}

			logger.info("Exiting getActivitiesLite() with number of activities of " + activities.size());

			return activities;
		} catch (Exception e) {
			logger.error("Exception in getActivitiesLite" + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}

	public static List getReviewActivities() {
		String statusIds = StringUtils.i2s(Constants.ACTIVITY_STATUS_UNDER_REVIEW);
		statusIds = statusIds + "," + StringUtils.i2s(Constants.ACTIVITY_STATUS_DELINQUENT_30);
		statusIds = statusIds + "," + StringUtils.i2s(Constants.ACTIVITY_STATUS_DELINQUENT_60);
		// statusIds = statusIds + "," +StringUtils.i2s(Constants.ACTIVITY_STATUS_DELINQUENT_90);
		logger.debug("statusIds  " + statusIds);
		List actList = new ArrayList();
		Activity a = null;
		ActivityDetail ad = null;
		People p = null;
		ActivityStatus as = null;
		String sql = "select a.act_id, p.name, p.email_addr, a.DESCRIPTION as aname, las.description as status, las.status_id  from people p, activity_people ap, activity a, lkup_act_st las " + "where p.email_addr is not null and p.people_id = ap.people_id and ap.act_id=a.act_id and las.status_id in (" + statusIds + ")" + " and a.act_nbr like 'HD%' and a.updated+30 > SYSDATE";
		logger.debug("Review Activities sql:" + sql);
		Wrapper db = new Wrapper();
		ResultSet rs = null;
		try {
			rs = db.select(sql);
			while (rs.next()) {
				a = new Activity();
				ad = new ActivityDetail();
				a.setActivityId(rs.getInt("act_id"));
				ad.setDescription(rs.getString("aname"));
				as = new ActivityStatus();
				as.setDescription(rs.getString("status"));
				as.setStatus(rs.getInt("status_id"));
				ad.setStatus(as);
				a.setActivityDetail(ad);
				p = new People();
				p.setName(rs.getString("name"));
				p.setEmailAddress(rs.getString("email_addr"));
				a.setPeople(p);
				actList.add(a);
			}

			logger.debug("review actList size...:" + actList.size());
		} catch (Exception exp) {
			logger.error(exp);
		}
		return actList;
	}

	public static void updateStatus(int actId, int statusId) {
		logger.debug("updateStatus " + actId);
		try {
			String sql = "";
			Wrapper db = new Wrapper();
			String nextId = "" + db.getNextId("ACT_STAT_ID");
			String comment = "Updated by System automatically";

			sql = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)" + " values(" + nextId + "," + actId + "," + statusId + ",current_date," + 0 + ",null," + "null," + StringUtils.checkString(comment) + ",current_timestamp)";

			db.beginTransaction();
			db.addBatch(sql);
			sql = "update activity set status=" + statusId + " where act_id=" + actId;
			logger.debug(sql);
			db.addBatch(sql);
			db.executeBatch();
			// db.commitTransaction();
			db.returnBatchConnection();
		} catch (Exception exp) {
			logger.error(exp);
		}
	}

	public String getUpdateStatusPlancheck(int activityId, Activity activity, int userId) throws Exception {
		String sql = "";
		Wrapper db = new Wrapper();
		int actStatId = db.getNextId("act_stat_id");

		ActivityHistory status = new ActivityHistory();
		ActivityHistoryForm form = new ActivityHistoryForm();

		/*
		 * comment = Status for plancheck required;
		 */
		String comment = null;

		logger.debug("entered getUpdateStatusPlancheck()");

		sql = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)" + " values(" + actStatId + "," + activityId + "," + activity.getActivityDetail().getStatus().getStatus() + ",current_date," + userId + ",null," + "null," + StringUtils.checkString(comment) + ",current_timestamp)";
		db.insert(sql);

		logger.debug(sql);

		return sql;
	}

	public List getCodeEnforcementTypes(String type, int deptId) throws Exception {
		logger.info("Entering getCodeEnforcementTypes()");

		String GET_CE_SYS_TYPES = "SELECT * FROM LKUP_CE_TYPE where ACTIVE = 'Y' and CATEGORY='Systematic' and DEPARTMENT = " + deptId + " ORDER BY CATEGORY,CE_TYPE";
		String GET_CE_TYPES = "SELECT * FROM LKUP_CE_TYPE  where ACTIVE = 'Y' and CATEGORY not like 'Systematic'  and DEPARTMENT = " + deptId + " ORDER BY CATEGORY,CE_TYPE";
		String GET_CE_VMP_TYPES = "SELECT * FROM LKUP_CE_TYPE  where ACTIVE = 'Y' and CATEGORY = 'VMP Notice'  and DEPARTMENT = " + deptId + " ORDER BY CATEGORY,CE_TYPE";

		Wrapper db = new Wrapper();
		List typeList = new ArrayList();
		RowSet rs = db.select(GET_CE_TYPES);

		if (type.equalsIgnoreCase("systematic")) {
			logger.debug("sql::" + GET_CE_SYS_TYPES);
			rs = db.select(GET_CE_SYS_TYPES);
		} else if (type.equalsIgnoreCase("VMP")) {
			logger.debug("sql::" + GET_CE_VMP_TYPES);
			rs = db.select(GET_CE_VMP_TYPES);
		} else {
			logger.debug("sql::" + GET_CE_TYPES);
			rs = db.select(GET_CE_TYPES);
		}

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setCeTypeId(rs.getInt("ce_type_id"));
			ce.setCategory(rs.getString("category"));
			ce.setName(rs.getString("ce_type"));
			typeList.add(ce);
		}

		logger.info("Exiting getCodeEnforcementTypes(" + typeList.size() + ")");
		return typeList;
	}

	public List getUserList(int deptId) throws Exception {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		// RowSet rs = db.select("SELECT USERID,USERNAME,LAST_NAME,FIRST_NAME FROM USERS WHERE DEPT_ID="+Constants.DEPARTMENT_NEIGHBOURHOOD_SERVICES+" and active='Y' ORDER BY LAST_NAME,FIRST_NAME");

		RowSet rs = db.select("SELECT DISTINCT USER_ID as USERID,USERNAME, FIRST_NAME, LAST_NAME FROM USER_GROUPS, GROUPS, USERS WHERE USERS.USERID = USER_GROUPS.USER_ID AND USER_GROUPS.GROUP_ID in (" + Constants.GROUPS_INSPECTOR + ") and users.dept_id =" + deptId + " and users.active='Y' ORDER BY LAST_NAME,FIRST_NAME");

		while (rs.next()) {
			list.add(new PickList(rs.getInt("USERID"), rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME")));
		}
		return list;
	}

	public List getAllUserList(int deptId) throws AgentException {
		List list = new ArrayList();
		Wrapper db = new Wrapper();
		try {
			String query = "SELECT DISTINCT USERID,USERNAME, FIRST_NAME, LAST_NAME FROM USERS WHERE users.dept_id =" + deptId + " and users.active='Y' ORDER BY LAST_NAME,FIRST_NAME";
			RowSet rs = db.select(query);

			while (rs.next()) {
				list.add(new PickList(rs.getInt("USERID"), rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME")));
			}
		} catch (Exception ex) {
			logger.error("", ex);
			throw new AgentException();
		}
		return list;
	}

	/*
	 * Returns List of Code Enforcement for Listing In the Activity screen
	 */
	public List getCodeEnforcementListForAct(long activityId) throws Exception {
		logger.info("Entering getCodeEnforcementListForAct(" + activityId + ")");
		Wrapper db = new Wrapper();
		List actionList = new ArrayList();
		String GET_CE_LIST_ACT = "SELECT A.CE_ID,B.CE_TYPE,A.OFFICER_ID,A.SUB_TYPE,A.CE_DATE,B.CATEGORY FROM CODE_ENFORCEMENT A JOIN LKUP_CE_TYPE B ON A.CE_TYPE_ID=B.CE_TYPE_ID WHERE A.ACT_ID=";
		RowSet rs = db.select(GET_CE_LIST_ACT + activityId);
		int countNot = 0;
		int countRes = 0;
		while (rs.next()) {

			String sql = "SELECT (LAST_NAME||' '||FIRST_NAME) as officer FROM USERS WHERE USERID = " + rs.getString("officer_id");
			logger.info("++++++++++++" + sql);
			RowSet rs1 = db.select(sql);
			String officer = "";
			while (rs1.next()) {
				officer = rs1.getString("officer");
			}
			CodeEnforcement ce = new CodeEnforcement();
			ce.setId(rs.getInt("ce_id"));
			ce.setName(rs.getString("ce_type")); // using setName for setting CE Type Description
			ce.setOfficerId(officer); // using setOfficerId for Setting OfficerName
			ce.setCategory(rs.getString("category"));
			ce.setCeDate(StringUtils.date2str(rs.getDate("ce_date")));
			ce.setSubType(rs.getString("sub_type"));
			if ((rs.getString("category").startsWith("Notices")) || (rs.getString("category").startsWith("Systematic"))) { // To show only 3 records in the Activity Manager of category Notices
				if (countNot < 3) {
					actionList.add(ce);
					countNot++;
				}
			} else if (rs.getString("category").startsWith("Resolution")) { // To show only 3 records in the Activity Manager of category Resolution
				if (countRes < 3) {
					actionList.add(ce);
					countRes++;
				}
			}
			rs1.close();
		}
		rs.close();

		logger.info("Exiting getCodeEnforcementListForAct(" + actionList.size() + ")");
		return actionList;
	}

	public int getCeIdforInspectionNotice(int activityId) throws Exception {
		int ceId = 0;
		Wrapper db = new Wrapper();
		// RowSet rs = db.select("SELECT USERID,USERNAME,LAST_NAME,FIRST_NAME FROM USERS WHERE DEPT_ID="+Constants.DEPARTMENT_NEIGHBOURHOOD_SERVICES+" and active='Y' ORDER BY LAST_NAME,FIRST_NAME");

		RowSet rs = db.select("select * from Code_Enforcement  where act_id=" + activityId);

		while (rs.next()) {
			ceId = rs.getInt("CE_ID");
		}
		return ceId;
	}

	public int checkAddress(String streetNumber, String streetFraction, int streetId, String unit, String zip4, BusinessLicenseActivity businessLicenseActivity) throws Exception {
		logger.info("checkAddress(" + streetNumber + "," + streetFraction + " , " + streetId + ", " + unit + ", " + zip4 + ")");
		String sql = "";
		int lsoId = 0;
		int addressId = 0;
		sql = "SELECT * FROM V_ADDRESS_LIST WHERE ";

		if (!(GenericValidator.isBlankOrNull(streetNumber))) {
			sql += (" STR_NO=" + streetNumber);
		}
		if (!(GenericValidator.isBlankOrNull(streetFraction))) {
			sql += (" and STR_MOD=" + StringUtils.checkString(streetFraction));
		}
		if (!(streetId == 0)) {
			sql += (" and STREET_ID=" + streetId);
		}

		if (!((unit == null) || (unit.equalsIgnoreCase("")))) {
			sql += (" and UNIT=" + StringUtils.checkString(unit));
		}
		if (!(GenericValidator.isBlankOrNull(zip4))) {
			sql += (" AND ZIP4=" + StringUtils.checkString(zip4));
		}

		sql += " and LSO_TYPE = 'O' order by addr_id";

		logger.info(sql);

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			lsoId = rs.getInt("LSO_ID");
			logger.debug("LSO Id Is " + lsoId);
			addressId = rs.getInt("ADDR_ID");
			logger.debug("Address Id is " + addressId);
			businessLicenseActivity.setAddressStreetNumber(rs.getString("STR_NO"));
			businessLicenseActivity.setAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
			businessLicenseActivity.setAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
			businessLicenseActivity.setAddressUnitNumber(rs.getString("UNIT"));
			businessLicenseActivity.setAddressCity(rs.getString("CITY"));
			businessLicenseActivity.setAddressState(rs.getString("STATE"));
			businessLicenseActivity.setAddressZip(rs.getString("ZIP"));
			businessLicenseActivity.setAddressZip4(rs.getString("ZIP4"));
		}
		return lsoId;
	}

	public int checkProjectName(String ProjectName, int lsoId) throws Exception {
		logger.info("checkProjectName(" + ProjectName + ", " + lsoId + ")");
		String sql = "";
		int projectId = 0;
		sql = "SELECT PROJ_ID,DESCRIPTION  FROM PROJECT WHERE UPPER(NAME) =" + StringUtils.checkString(ProjectName).toUpperCase() + " AND LSO_ID = " + lsoId;
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			projectId = rs.getInt("PROJ_ID");
			logger.debug("project Id is " + projectId);
		}

		return projectId;
	}

	/**
	 * @param subProjectName
	 * @return pTypeId
	 * @throws Exception
	 * @author Gayathri & Manjuprasad
	 */
	public int checklkupSubProjectName(String subProjectName) throws Exception {
		logger.info("checklkupSubProjectName(" + subProjectName + ")");
		String sql = "";
		int pTypeId = 0;
		sql = "SELECT PTYPE_ID,DESCRIPTION  FROM LKUP_PTYPE WHERE UPPER(DESCRIPTION) =" + StringUtils.checkString(subProjectName);
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			pTypeId = rs.getInt("PTYPE_ID");
			logger.debug("pTypeId Is " + pTypeId);
		}

		return pTypeId;
	}

	/**
	 * @param subProjectName
	 * @return pTypeId
	 * @throws Exception
	 * @author Gayathri & Manjuprasad
	 */
	public String getlkupSubProjectName(String subProjectName) throws Exception {
		logger.info("checklkupSubProjectName(" + subProjectName + ")");
		String sql = "";
		String description = "";

		sql = "SELECT DESCRIPTION  FROM LKUP_PTYPE WHERE UPPER(DESCRIPTION) =" + StringUtils.checkString(subProjectName);
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			description = rs.getString("DESCRIPTION");
			logger.debug("Description Is " + description);
		}

		return description;
	}

	/**
	 * @param pTypeId
	 *            , projectId
	 * @return subprojectId
	 * @throws Exception
	 * @author Gayathri & Manjuprasad
	 */
	public int checkSubProjectName(int pTypeId, int projectId) throws Exception {
		logger.info("checkSubProjectName(" + pTypeId + ", " + projectId + ")");
		String sql = "";
		String actSql = "";
		int count = 0;
		int subprojectId = 0;
		sql = "SELECT SPROJ_ID FROM SUB_PROJECT WHERE SPROJ_TYPE = " + pTypeId + " AND PROJ_ID =" + projectId;
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		while (rs.next()) {
			subprojectId = rs.getInt("SPROJ_ID");
			logger.debug("Sub project Id is " + subprojectId);

			actSql = "SELECT COUNT(*) AS COUNT FROM ACTIVITY WHERE SPROJ_ID =" + subprojectId;
			logger.debug("Activity Sql" + actSql);
			ResultSet actRs = new Wrapper().select(actSql);

			while (actRs.next()) {
				count = actRs.getInt("COUNT");
				logger.debug("actId ************** " + count);
			}

			if (count <= 0) {
				subprojectId = rs.getInt("SPROJ_ID");
				break;
			}
			logger.debug("Sub project Id is " + subprojectId);
		}
		return subprojectId;
	}

	/**
	 * @param pTypeId
	 *            , projectId
	 * @return subprojectId
	 * @throws Exception
	 * @author Gayathri
	 */
	public int checkActivityName(int subprojectId, String actBLType) throws Exception {
		logger.info("checkActivityName(" + subprojectId + ", " + actBLType + ")");
		String sql = "";
		int activityId = 0;

		sql = "SELECT ACT_ID FROM ACTIVITY WHERE SPROJ_ID = " + subprojectId + " AND ACT_TYPE ='" + actBLType + "'";
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		while (rs.next()) {
			activityId = rs.getInt("ACT_ID");
			logger.debug("Activity Id is " + activityId);
		}
		return activityId;
	}

	/**
	 * SaveBusinessLicenseActivity()
	 * 
	 * @param businesslicenseactivity
	 *            , lsoId
	 * @return activityId
	 * @param businessLicenseActivity
	 * @throws Exception
	 */
	public int saveBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity, int LsoId) throws Exception {
		logger.info("saveBusinessLicenseActivity(" + businessLicenseActivity + ", " + LsoId + ")");

		int projNum = -1;
		int sprojNum = -1;
		int activityId = -1;
		int activityNum = -1;
		int subprojectId = 0;
		int projectId = 0;
		int addressId = 0;
		String activityNumber = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yy");
		addressId=LookupAgent.getAddressIdForPsaId(LsoId+"");
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			String projectSql = "";
			String subProjectSql = "";
			String activitySql = "";
			String lkupPtypeSql = "";
			int businessAccNo = 0;
			int pTypeId = 0;
			String projName = Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES;
			projectId = checkProjectName(projName, LsoId);
			logger.debug("Project Id Returned is  " + projectId);

			if (businessLicenseActivity != null) {
				if (projectId == 0) {
					/**
					 * Project Number Generation
					 */
					projNum = db.getNextId("PR_NUM");

					String projectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);

					/**
					 * Inserting records into PROJECT Table
					 */
					projectId = db.getNextId("PROJECT_ID");
					logger.debug("The ID generated for new Project is " + projectId);
					projectSql = "insert into project(PROJ_ID, LSO_ID, PROJECT_NBR, NAME, DESCRIPTION, STATUS_ID, DEPT_ID, CREATED_BY, CREATED_DT) values(";
					projectSql = projectSql + projectId;
					logger.debug("got Project id " + projectId);
					projectSql = projectSql + ",";
					projectSql = projectSql + LsoId;
					logger.debug("got LSO id " + LsoId);
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projectNumber);
					logger.debug("got Project Number " + StringUtils.checkString(projectNumber));
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projName);
					logger.debug("got Project Name " + StringUtils.checkString(projName));
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projName);
					logger.debug("got Project Description " + StringUtils.checkString(projName));
					projectSql = projectSql + ",";
					projectSql = projectSql + 1;// STATUS OF ACTIVE
					logger.debug("got Status Id " + "1");
					projectSql = projectSql + ",";
					projectSql = projectSql + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES;
					logger.debug("got Department ID " + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES);
					projectSql = projectSql + ",";
					projectSql = projectSql + businessLicenseActivity.getCreatedBy();
					logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
					projectSql = projectSql + ", current_timestamp)";
					logger.info(projectSql);
					db.insert(projectSql);
				}

				/**
				 * Inserting records into LKUP_PTYPE Table
				 */
				String subProjectName = (Constants.SUB_PROJECT_NAME_STARTS_WITH + businessLicenseActivity.getBusinessName().trim()).toUpperCase();
				logger.debug("Generated subProjectName " + subProjectName);
				pTypeId = checklkupSubProjectName(subProjectName);
				logger.debug("pTypeId Returned is  " + pTypeId);

				if (pTypeId == 0) {
					pTypeId = db.getNextId("PTYPE_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					lkupPtypeSql = "insert into lkup_ptype(PTYPE_ID, DESCRIPTION, DEPT_CODE, NAME) values (";
					lkupPtypeSql = lkupPtypeSql + pTypeId;
					logger.debug("got pTypeId is " + pTypeId);
					lkupPtypeSql = lkupPtypeSql + ",";
					lkupPtypeSql = lkupPtypeSql + StringUtils.checkString(("BL - " + businessLicenseActivity.getBusinessName().trim()).toUpperCase());
					logger.debug("got Description " + StringUtils.checkString(("BL - " + businessLicenseActivity.getBusinessName().trim()).toUpperCase()));
					lkupPtypeSql = lkupPtypeSql + ",'";
					lkupPtypeSql = lkupPtypeSql + "LC";
					logger.debug("got Department Code " + "LC");
					lkupPtypeSql = lkupPtypeSql + "',";
					lkupPtypeSql = lkupPtypeSql + "null";
					logger.debug("got Name " + "null");
					lkupPtypeSql = lkupPtypeSql + ")";

					logger.info(lkupPtypeSql);
					db.insert(lkupPtypeSql);
				}

				// subprojectId = checkSubProjectName(pTypeId, projectId);
				subprojectId = 0;
				logger.debug("Sub Project Id Returned is  " + subprojectId);

				if (subprojectId == 0) {

					sprojNum = db.getNextId("SP_NUM");
					/**
					 * Sub Project Number Generation
					 */
					String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);
					/**
					 * Inserting records into SUB_PROJECT Table
					 */
					subprojectId = db.getNextId("SUB_PROJECT_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					subProjectSql = "insert into sub_project(SPROJ_ID, PROJ_ID, SPROJ_TYPE, CREATED_BY, CREATED, STATUS, SPROJ_NBR) values (";
					subProjectSql = subProjectSql + subprojectId;
					logger.debug("got Subproject Id " + subprojectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + projectId;
					logger.debug("got Project id " + projectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + pTypeId;
					logger.debug("got Sub Project Type " + pTypeId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + businessLicenseActivity.getCreatedBy();
					logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
					subProjectSql = subProjectSql + ", current_timestamp,";
					logger.debug("got Created ");
					subProjectSql = subProjectSql + 1;
					logger.debug("got Status " + 1);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + StringUtils.checkString(subProjectNumber);
					logger.debug("got Sub Project Number " + StringUtils.checkString(subProjectNumber));
					subProjectSql = subProjectSql + ")";

					logger.info(subProjectSql);
					db.insert(subProjectSql);
				}

				// Checking activity existing or not
				String actBLType = businessLicenseActivity.getActivityType().getType();
				activityId = checkActivityName(subprojectId, actBLType);
				if (activityId != 0) {

					String inactivateSql = "update activity set status =" + Constants.ACTIVITY_STATUS_BL_OUT_OF_BUSINESS + " where act_id = " + activityId;
					logger.info(inactivateSql);
					db.update(inactivateSql);
				}

				// Activity Id Generation
				activityId = db.getNextId("ACTIVITY_ID");
				logger.debug("The ID generated for new Activity is " + activityId);

				// Activity Number generation
				activityNum = db.getNextId("ACT_NUM");
				logger.debug("Obtained new activity number is " + activityNum);

				String strActNum = StringUtils.i2s(activityNum);
				strActNum = "00000".substring(strActNum.length()) + strActNum;
				logger.debug("Obtained new activity number string is " + strActNum);
				calendar = Calendar.getInstance();
				formatter = new SimpleDateFormat("yy");
				activityNumber = "BL" + formatter.format(calendar.getTime()) + strActNum.trim();

				/**
				 * Inserting records into ACTIVITY Table
				 * 
				 */
				activitySql = "insert into activity(ACT_ID, SPROJ_ID, ADDR_ID, ACT_NBR, PLAN_CHK_REQ, ACT_TYPE,  VALUATION, STATUS, START_DATE, APPLIED_DATE, ISSUED_DATE, PLAN_CHK_FEE_DATE, PERMIT_FEE_DATE, DEV_FEE_REQ, CREATED_BY, CREATED,UPDATED_BY,UPDATED) values(";

				activitySql = activitySql + activityId;
				logger.debug("got Activity id " + activityId);
				activitySql = activitySql + ",";
				activitySql = activitySql + subprojectId;
				logger.debug("got Sub Project Id " + subprojectId);
				activitySql = activitySql + ",";
				activitySql = activitySql + addressId;
				logger.debug("got Address Id " + addressId);
				activitySql = activitySql + ",";
				activitySql = activitySql + StringUtils.checkString(activityNumber);
				logger.debug("got Activity Number " + activityNumber);
				activitySql = activitySql + ",'N','";
				activitySql = activitySql + ((businessLicenseActivity.getActivityType() != null) ? businessLicenseActivity.getActivityType().getType() : "");
				logger.debug("got Activity Type " + ((businessLicenseActivity.getActivityType() != null) ? businessLicenseActivity.getActivityType().getType() : ""));
				activitySql = activitySql + "',";
				activitySql = activitySql + "0";
				logger.debug("got Valuation " + "0");
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null");
				logger.debug("got Activity Status " + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null"));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessLicenseActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate())) : "current_timestamp");
				logger.debug("got Start Date " + ((businessLicenseActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate())) : "current_timestamp"));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessLicenseActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate())) : null);
				logger.debug("got Applied Date " + ((businessLicenseActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate())) : null));
				activitySql = activitySql + ",";
				if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1053")) {
					activitySql = activitySql + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "current_timestamp");
					logger.debug("got Issue Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
				} else if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1054")) {
					activitySql = activitySql + null;
					logger.debug("got Issue Date null");
				} else {
					activitySql = activitySql + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "null");
					logger.debug("got Issue Date null");
				}
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",'N',";
				activitySql = activitySql + businessLicenseActivity.getCreatedBy();
				logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_timestamp";
				logger.debug("got Created " + "current_timestamp");
				activitySql = activitySql + ",";
				activitySql = activitySql + businessLicenseActivity.getCreatedBy();
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_timestamp";
				
				activitySql = activitySql + ")";
				logger.info(activitySql);
				db.insert(activitySql);

				/**
				 * Inserting records into BL_ACTIVITY Table
				 */
				businessAccNo = db.getNextId("BUSINESS_ACC_NO");
				logger.debug("Business Account No generated is " + businessAccNo);
				sql = "insert into bl_activity(ACT_ID, BURBANK_BUSINESS, APPL_TYPE_ID, OOT_STR_NO, OOT_STR_NAME, OOT_UNIT, OOT_CITY, OOT_STATE, OOT_ZIP, OOT_ZIP4, RENEWAL_DT, BUSINESS_NAME, BUSINESS_ACC_NO, CORPORATE_NAME, BUSINESS_PHONE, BUSINESS_PHONE_EXT, BUSINESS_FAX, MUNICIPAL_CODE, SIC_CODE, DESC_OF_BUSINESS, CLASS_CODE, HOME_OCCUPATION, DECAL_CODE, OOB_DT, OWN_TYPE_ID, FEDEREAL_ID, EMAIL, SSN, MAIL_STR_NO, MAIL_STR_NAME, MAIL_UNIT, MAIL_CITY, MAIL_STATE, MAIL_ZIP, MAIL_ZIP4, PRE_STR_NO, PRE_STR_NAME, PRE_CITY, PRE_STATE, PRE_ZIP, PRE_ZIP4, PRE_UNIT, INS_EXP_DT, BOND_EXP_DT, DOJ_EXP_DT, FFL_EXP_DT, SQ_FOOTAGE, QTY_ID, QTY_OTHER, EXEM_TYPE_ID, CREATED, CREATED_BY) values(";
				sql = sql + activityId;
				logger.debug("got Activity id " + activityId);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isBusinessLocation()));
				logger.debug("got Business Location " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isBusinessLocation())));
				sql = sql + ",";
				sql = sql + ((businessLicenseActivity.getApplicationType() != null) ? StringUtils.i2s(businessLicenseActivity.getApplicationType().getId()) : "null");
				logger.debug("got Application Type ID" + ((businessLicenseActivity.getApplicationType() != null) ? StringUtils.i2s(businessLicenseActivity.getApplicationType().getId()) : "null"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber());
				logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName());
				logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber());
				logger.debug("got out Of Town Unit Number " + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity());
				logger.debug("got Out Of Town City " + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownState());
				logger.debug("got Out Of Town State " + StringUtils.checkString(businessLicenseActivity.getOutOfTownState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip());
				logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4());
				logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4()));
				sql = sql + ",";
				sql = sql + ((businessLicenseActivity.getRenewalDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate())) : "");
				logger.debug("got Renewal Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessName().trim().toUpperCase());
				logger.debug("got Business Name " + StringUtils.checkString(businessLicenseActivity.getBusinessName().trim().toUpperCase()));
				sql = sql + ",";
				sql = sql + businessAccNo;
				logger.debug("got Business Account Number " + businessAccNo);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getCorporateName());
				logger.debug("got Corporate Name " + StringUtils.checkString(businessLicenseActivity.getCorporateName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessPhone());
				logger.debug("got Business Phone " + StringUtils.checkString(StringUtils.phoneFormat(businessLicenseActivity.getBusinessPhone())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessExtension());
				logger.debug("got Business Extension " + StringUtils.checkString(businessLicenseActivity.getBusinessExtension()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessFax());
				logger.debug("got Business Fax " + StringUtils.checkString(businessLicenseActivity.getBusinessFax()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getMuncipalCode());
				logger.debug("got Muncipal Code " + StringUtils.checkString(businessLicenseActivity.getMuncipalCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getSicCode());
				logger.debug("got Sic Code " + StringUtils.checkString(businessLicenseActivity.getSicCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness());
				logger.debug("got Description Of Business " + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getClassCode());
				logger.debug("got Class Code " + StringUtils.checkString(businessLicenseActivity.getClassCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation()));
				logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode()));
				logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode())));
				sql = sql + ",";
				if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1054")) {
					sql = sql + ((businessLicenseActivity.getOutOfBusinessDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())) : "current_timestamp");
					logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
				} else {
					sql = sql + null;
					logger.debug("got Out Of Business Date null");
				}
				sql = sql + ",";
				sql = sql + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "null");
				logger.debug("got Ownership Type " + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "-1"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber());
				logger.debug("got Federal Id Number " + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getEmailAddress());
				logger.debug("got Email " + StringUtils.checkString(businessLicenseActivity.getEmailAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber());
				logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber()));
				sql = sql + ",";

				sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber());
				logger.debug("got Mail Street Number 2: " + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetName());
				logger.debug("got Mail Street Name 2: " + StringUtils.checkString((businessLicenseActivity.getMailStreetName())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber()));
				logger.debug("got Mail Unit Number 2: " + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailCity()));
				logger.debug("got Mail City 2: " + StringUtils.checkString((businessLicenseActivity.getMailCity())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailState()));
				logger.debug("got Mail State 2: " + StringUtils.checkString((businessLicenseActivity.getMailState())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip()));
				logger.debug("got Mail Zip 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip4()));
				logger.debug("got Mail Zip4 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip4())));
				sql = sql + ",";

				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber());
				logger.debug("got Previous Street Number " + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetName());
				logger.debug("got Previous Street Name " + StringUtils.checkString(businessLicenseActivity.getPrevStreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevCity());
				logger.debug("got Previous City " + StringUtils.checkString(businessLicenseActivity.getPrevCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevState());
				logger.debug("got Previous State " + StringUtils.checkString(businessLicenseActivity.getPrevState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip());
				logger.debug("got Previous Zip " + StringUtils.checkString(businessLicenseActivity.getPrevZip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip4());
				logger.debug("got Previous Zip4 " + StringUtils.checkString(businessLicenseActivity.getPrevZip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber());
				logger.debug("got Previous Unit Number " + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate()));
				logger.debug("got Insurance Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate())));
				sql = sql + ",";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate()));
				logger.debug("got Bond Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate())));
				sql = sql + ",";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate()));
				logger.debug("got Department of Justice Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate())));
				sql = sql + ",";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate()));
				logger.debug("got Federal Firearms License Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getSquareFootage());
				logger.debug("got Square Footage " + StringUtils.checkString(businessLicenseActivity.getSquareFootage()));
				sql = sql + ",";
				sql = sql + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null");
				logger.debug("got Quantity " + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getQuantityNum());
				logger.debug("got Quantity Number " + StringUtils.checkString(businessLicenseActivity.getQuantityNum()));
				sql = sql + ",";
				sql = sql + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null");
				logger.debug("got Type Of Exemptions " + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null"));
				sql = sql + ", current_timestamp,";
				sql = sql + businessLicenseActivity.getCreatedBy();
				logger.debug("got created by " + businessLicenseActivity.getCreatedBy());
				sql = sql + ")";

				logger.info(sql);
				db.insert(sql);

			}
			return activityId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/*
	 * Inserting conditions
	 */

	public void InsertConditions(int typeId, int activityId, int userId) throws Exception {
		logger.info("InsertConditions(" + typeId + "," + activityId + ")");

		String sql = "";
		String insertConditionsSql = "";
		Wrapper db = new Wrapper();
		RowSet rs = null;

		sql = "Select * from lkup_conditions where level_type ='A' and condition_type=" + typeId + " AND EXPIRATION_DT IS null ";
		logger.info(sql);
		rs = db.select(sql);

		while (rs.next()) {

			insertConditionsSql = "INSERT INTO CONDITIONS VALUES (" + db.getNextId("CONDITION_ID") + "," + activityId + ",'A'," + rs.getInt("CONDITION_ID") + "," + StringUtils.checkString(rs.getString("SHORT_TEXT")) + "," + StringUtils.checkString(rs.getString("INSPECTABILITY")) + "," + StringUtils.checkString(rs.getString("WARNING")) + ",'Y'," + userId + ",current_timestamp,0,'',0,''," + StringUtils.checkString(rs.getString("CONDITION_TEXT")) + ")";
			logger.info(insertConditionsSql);
			db.insert(insertConditionsSql);
		}
	}

	/**
	 * Get business license activity
	 * 
	 * @param activityId
	 *            , lsoId
	 * @return
	 * @throws Exception
	 * 
	 */
	public BusinessLicenseActivity getBusinessLicenseActivity(int activityId, int lsoId) throws Exception {
		logger.info("getBusinessLicenseActivity(" + activityId + ", " + lsoId + ")");

		BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
		logger.info("lso id is " + lsoId);
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			sql = "SELECT * FROM  ACTIVITY A, BL_ACTIVITY BA, V_ADDRESS_LIST VAL WHERE  A.ACT_ID = BA.ACT_ID  AND  VAL.LSO_ID=" + lsoId + " AND LSO_TYPE='O'  AND  A.ACT_ID = " + activityId;
			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				businessLicenseActivity.setActivityNumber(rs.getString("ACT_NBR"));
				businessLicenseActivity.setActivityStatus(LookupAgent.getActivityStatus(rs.getInt("STATUS")));
				businessLicenseActivity.setApplicationType(LookupAgent.getApplicationType(rs.getInt("APPL_TYPE_ID")));
				businessLicenseActivity.setActivityType(LookupAgent.getActivityType(rs.getString("ACT_TYPE")));
				businessLicenseActivity.setAddressStreetNumber(StringUtils.i2s(rs.getInt("STR_NO")));
				businessLicenseActivity.setAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
				businessLicenseActivity.setAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
				businessLicenseActivity.setAddressUnitNumber(rs.getString("UNIT"));
				businessLicenseActivity.setAddressCity(rs.getString("CITY"));
				businessLicenseActivity.setAddressState(rs.getString("STATE"));
				businessLicenseActivity.setAddressZip(rs.getString("ZIP"));
				businessLicenseActivity.setAddressZip4(rs.getString("ZIP4"));
				businessLicenseActivity.setBusinessLocation(StringUtils.s2b(rs.getString("BURBANK_BUSINESS")));
				businessLicenseActivity.setOutOfTownStreetNumber(rs.getString("OOT_STR_NO"));
				if (rs.getString("OOT_STR_NO") == null) {
					businessLicenseActivity.setOutOfTownStreetNumber("");
				}
				businessLicenseActivity.setOutOfTownStreetName(rs.getString("OOT_STR_NAME"));
				businessLicenseActivity.setOutOfTownUnitNumber(rs.getString("OOT_UNIT"));
				businessLicenseActivity.setOutOfTownCity(rs.getString("OOT_CITY"));
				businessLicenseActivity.setOutOfTownState(rs.getString("OOT_STATE"));
				businessLicenseActivity.setOutOfTownZip(rs.getString("OOT_ZIP"));
				businessLicenseActivity.setOutOfTownZip4(rs.getString("OOT_ZIP4"));
				businessLicenseActivity.setBusinessName(rs.getString("BUSINESS_NAME").trim().toUpperCase());
				businessLicenseActivity.setBusinessAccountNumber(rs.getString("BUSINESS_ACC_NO"));
				businessLicenseActivity.setCorporateName(rs.getString("CORPORATE_NAME"));
				businessLicenseActivity.setBusinessPhone(rs.getString("BUSINESS_PHONE"));
				businessLicenseActivity.setBusinessExtension(rs.getString("BUSINESS_PHONE_EXT"));
				businessLicenseActivity.setBusinessFax(rs.getString("BUSINESS_FAX"));
				businessLicenseActivity.setClassCode(rs.getString("CLASS_CODE"));
				businessLicenseActivity.setMuncipalCode(rs.getString("MUNICIPAL_CODE"));
				businessLicenseActivity.setSicCode(rs.getString("SIC_CODE"));
				businessLicenseActivity.setDecalCode(StringUtils.s2b(rs.getString("DECAL_CODE")));
				businessLicenseActivity.setDescOfBusiness(rs.getString("DESC_OF_BUSINESS"));
				businessLicenseActivity.setHomeOccupation(StringUtils.s2b(rs.getString("HOME_OCCUPATION")));
				businessLicenseActivity.setCreationDate(StringUtils.dbDate2cal(rs.getString("CREATED")));
				businessLicenseActivity.setRenewalDate(StringUtils.dbDate2cal(rs.getString("RENEWAL_DT")));
				businessLicenseActivity.setApplicationDate(StringUtils.dbDate2cal(rs.getString("APPLIED_DATE")));
				businessLicenseActivity.setIssueDate(StringUtils.dbDate2cal(rs.getString("ISSUED_DATE")));
				businessLicenseActivity.setStartingDate(StringUtils.dbDate2cal(rs.getString("START_DATE")));
				businessLicenseActivity.setOwnershipType(LookupAgent.getOwnershipType(rs.getInt("OWN_TYPE_ID")));
				businessLicenseActivity.setOutOfBusinessDate(StringUtils.dbDate2cal(rs.getString("OOB_DT")));
				businessLicenseActivity.setFederalIdNumber(rs.getString("FEDEREAL_ID"));
				businessLicenseActivity.setEmailAddress(rs.getString("EMAIL"));
				businessLicenseActivity.setSocialSecurityNumber(rs.getString("SSN"));
				businessLicenseActivity.setMailStreetNumber(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NO")));
				businessLicenseActivity.setMailStreetName(StringUtils.nullReplaceWithEmpty(rs.getString("MAIL_STR_NAME")));
				businessLicenseActivity.setMailUnitNumber(rs.getString("MAIL_UNIT"));
				businessLicenseActivity.setMailCity(rs.getString("MAIL_CITY"));
				businessLicenseActivity.setMailState(rs.getString("MAIL_STATE"));
				businessLicenseActivity.setMailZip(rs.getString("MAIL_ZIP"));
				businessLicenseActivity.setMailZip4(rs.getString("MAIL_ZIP4"));
				businessLicenseActivity.setPrevStreetNumber(rs.getString("PRE_STR_NO"));
				businessLicenseActivity.setPrevStreetName(rs.getString("PRE_STR_NAME"));
				businessLicenseActivity.setPrevUnitNumber(rs.getString("PRE_UNIT"));
				businessLicenseActivity.setPrevCity(rs.getString("PRE_CITY"));
				businessLicenseActivity.setPrevState(rs.getString("PRE_STATE"));
				businessLicenseActivity.setPrevZip(rs.getString("PRE_ZIP"));
				businessLicenseActivity.setPrevZip4(rs.getString("PRE_ZIP4"));
				businessLicenseActivity.setInsuranceExpDate(StringUtils.dbDate2cal(rs.getString("INS_EXP_DT")));
				businessLicenseActivity.setBondExpDate(StringUtils.dbDate2cal(rs.getString("BOND_EXP_DT")));
				businessLicenseActivity.setDeptOfJusticeExpDate(StringUtils.dbDate2cal(rs.getString("DOJ_EXP_DT")));
				businessLicenseActivity.setFederalFirearmsLiscExpDate(StringUtils.dbDate2cal(rs.getString("FFL_EXP_DT")));
				businessLicenseActivity.setSquareFootage(rs.getString("SQ_FOOTAGE"));
				businessLicenseActivity.setQuantity(LookupAgent.getQuantityType(rs.getInt("QTY_ID")));
				businessLicenseActivity.setQuantityNum(rs.getString("QTY_OTHER"));
				businessLicenseActivity.setTypeOfExemptions(LookupAgent.getExemptionType(rs.getInt("EXEM_TYPE_ID")));
				businessLicenseActivity.setCreatedBy(rs.getInt("CREATED_BY"));
				businessLicenseActivity.setRenewalOnline(rs.getString("RENEWAL_ONLINE"));
				
				String applicationOnline = rs.getString("APPLICATION_ONLINE");
				if(applicationOnline.equalsIgnoreCase("Y")) {
					User user = new AdminAgent().getOnlineUser(rs.getInt("created_by"));
					if(user!=null &&user.getUserId()>0 ){
					businessLicenseActivity.setCreatedBy(user.getUserId());
					logger.debug("created user is  " + businessLicenseActivity.getCreatedBy());
					}
				
				}else {
					User user = new AdminAgent().getUser(rs.getInt("created_by"));
					businessLicenseActivity.setCreatedBy(user.getUserId());
					logger.debug("created user is  " + businessLicenseActivity.getCreatedBy());					
				}
				
				
				if (rs.getString("DEV_FEE_REQ") == null || rs.getString("PLAN_CHK_REQ") == null) {
					String devFeeSql = "UPDATE ACTIVITY SET DEV_FEE_REQ = 'N',PLAN_CHK_REQ = 'N' WHERE ACT_ID = " + activityId;
					logger.info("*** Development Fee Query **** " + devFeeSql);
					db.update(devFeeSql);
				}
				logger.debug("Values setted to BusinessLicense object");
			}
			businessLicenseActivity.setMultiAddress(getMultiAddress(activityId, "BL"));
			return businessLicenseActivity;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the activity address for a given activity id
	 * 
	 * @param businessLicenseActivity
	 * @return activityId
	 */
	public int updateBusinessLicenseActivity(BusinessLicenseActivity businessLicenseActivity, int activityId) {
		logger.debug("In updateBusinessLicenseActivity " + businessLicenseActivity);
		try {
			String sql_updateActivity = "";
			
			Wrapper db = new Wrapper();
			String sql = "";
			/**
			 * Updating records in ACTIVITY Table
			 */

			sql_updateActivity = "UPDATE activity SET STATUS=";
			sql_updateActivity = sql_updateActivity + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null");
			logger.debug("got activity status " + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null"));
			sql_updateActivity = sql_updateActivity + ",UPDATED_BY =";
			sql_updateActivity = sql_updateActivity + businessLicenseActivity.getUpdatedBy();
			logger.debug("got Updated By " + businessLicenseActivity.getUpdatedBy());
			sql_updateActivity = sql_updateActivity + ",UPDATED =";
			sql_updateActivity = sql_updateActivity + "current_timestamp";
			logger.debug("got Updated on " + "current_timestamp");
			sql_updateActivity = sql_updateActivity + ", START_DATE =";
			sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate()));
			logger.debug("got Starting Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate())));
			sql_updateActivity = sql_updateActivity + ", APPLIED_DATE =";
			sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate()));
			logger.debug("got Application Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate())));
			sql_updateActivity = sql_updateActivity + ", ISSUED_DATE =";
			if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1053")) {
				sql_updateActivity = sql_updateActivity + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "current_timestamp");
				logger.debug("got Issue Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
			} else if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1054")) {
				sql_updateActivity = sql_updateActivity + null;
				logger.debug("got Issue Date null");
			} else {
				sql_updateActivity = sql_updateActivity + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "null");
				logger.debug("got Issue Date null");
			}
			logger.debug("got Issue Date " + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "null"));
			sql_updateActivity = sql_updateActivity + " WHERE ACT_ID =";
			sql_updateActivity = sql_updateActivity + activityId;
			logger.info(sql_updateActivity);
			db.update(sql_updateActivity);

			/**
			 * Updating records in BL_ACTIVITY Table
			 */

			sql = "UPDATE BL_ACTIVITY SET OOT_STR_NO = ";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber());
			logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber()));
			sql = sql + ", OOT_STR_NAME =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName());
			logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName()));
			sql = sql + ", OOT_UNIT =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber());
			logger.debug("got Out Of Town Unit " + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber()));
			sql = sql + ", OOT_CITY=";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity());
			logger.debug("got Out Of Town City " + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity()));
			sql = sql + ", OOT_STATE =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownState());
			logger.debug("got Out Of Town State " + StringUtils.checkString(businessLicenseActivity.getOutOfTownState()));
			sql = sql + ", OOT_ZIP =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip());
			logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip()));
			sql = sql + ", OOT_ZIP4 =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4());
			logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4()));
			sql = sql + ", CORPORATE_NAME =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getCorporateName());
			logger.debug("got corporate name " + StringUtils.checkString(businessLicenseActivity.getCorporateName()));
			sql = sql + ", BUSINESS_PHONE =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessPhone());
			logger.debug("got Business Phone " + StringUtils.checkString(businessLicenseActivity.getBusinessPhone()));
			sql = sql + ", BUSINESS_PHONE_EXT =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessExtension());
			logger.debug("got Business Extension " + StringUtils.checkString(businessLicenseActivity.getBusinessExtension()));
			sql = sql + ", BUSINESS_FAX =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessFax());
			logger.debug("got Business Fax " + StringUtils.checkString(businessLicenseActivity.getBusinessFax()));
			sql = sql + ", HOME_OCCUPATION =";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation()));
			logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation())));
			sql = sql + ", DECAL_CODE =";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode()));
			logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode())));
			sql = sql + ", FEDEREAL_ID =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber());
			logger.debug("got Federal Id Number " + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber()));
			sql = sql + ", SSN =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber());
			logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber()));
			sql = sql + ", DESC_OF_BUSINESS =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness());
			logger.debug("got Description Of Business " + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness()));
			sql = sql + ", OWN_TYPE_ID =";
			sql = sql + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "null");
			logger.debug("got Ownership Type " + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "-1"));
			sql = sql + ", EMAIL =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getEmailAddress());
			logger.debug("got Email " + StringUtils.checkString(businessLicenseActivity.getEmailAddress()));

			sql = sql + ", MAIL_STR_NO =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber());
			logger.debug("got Mail Street Number 2: " + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber()));
			sql = sql + ", MAIL_STR_NAME =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetName());
			logger.debug("got Mail Street Name 2: " + StringUtils.checkString((businessLicenseActivity.getMailStreetName())));
			sql = sql + ", MAIL_UNIT =";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber()));
			logger.debug("got Mail Unit Number 2: " + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber())));
			sql = sql + ", MAIL_CITY =";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailCity()));
			logger.debug("got Mail City 2: " + StringUtils.checkString((businessLicenseActivity.getMailCity())));
			sql = sql + ", MAIL_STATE =";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailState()));
			logger.debug("got Mail State 2: " + StringUtils.checkString((businessLicenseActivity.getMailState())));
			sql = sql + ", MAIL_ZIP =";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip()));
			logger.debug("got Mail Zip 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip())));
			sql = sql + ", MAIL_ZIP4 =";
			sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip4()));
			logger.debug("got Mail Zip4 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip4())));

			sql = sql + ", PRE_STR_NO =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber());
			logger.debug("got Previous Street Number " + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber()));
			sql = sql + ", PRE_STR_NAME =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetName());
			logger.debug("got Previous Street Name " + StringUtils.checkString(businessLicenseActivity.getPrevStreetName()));
			sql = sql + ", PRE_CITY =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevCity());
			logger.debug("got Previous City " + StringUtils.checkString(businessLicenseActivity.getPrevCity()));
			sql = sql + ", PRE_STATE =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevState());
			logger.debug("got Previous State " + StringUtils.checkString(businessLicenseActivity.getPrevState()));
			sql = sql + ", PRE_ZIP =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip());
			logger.debug("got Previous Zip " + StringUtils.checkString(businessLicenseActivity.getPrevZip()));
			sql = sql + ", PRE_ZIP4 =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip4());
			logger.debug("got Previous Zip4 " + StringUtils.checkString(businessLicenseActivity.getPrevZip4()));
			sql = sql + ", PRE_UNIT =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber());
			logger.debug("got Previous Unit Number " + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber()));
			sql = sql + ", SQ_FOOTAGE =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getSquareFootage());
			logger.debug("got Square Footage " + StringUtils.checkString(businessLicenseActivity.getSquareFootage()));
			sql = sql + ", QTY_ID =";
			sql = sql + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null");
			logger.debug("got Quantity " + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null"));
			sql = sql + ", QTY_OTHER =";
			sql = sql + StringUtils.checkString(businessLicenseActivity.getQuantityNum());
			logger.debug("got Quantity Number " + StringUtils.checkString(businessLicenseActivity.getQuantityNum()));
			sql = sql + ", EXEM_TYPE_ID =";
			sql = sql + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null");
			logger.debug("got Type of exemptions " + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null"));
			sql = sql + ",RENEWAL_DT =";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate()));
			logger.debug("got Renewal Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate())));
			sql = sql + ",INS_EXP_DT =";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate()));
			logger.debug("got Insurance Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate())));
			sql = sql + ",OOB_DT =";
			if (StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()).trim().equalsIgnoreCase("1054")) {
				sql = sql + ((businessLicenseActivity.getOutOfBusinessDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())) : "current_timestamp");
				logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
			} else {
				sql = sql + null;
				logger.debug("got Out Of Business Date null");
			}
			sql = sql + ",BOND_EXP_DT =";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate()));
			logger.debug("got Bond Expiration Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate())));
			sql = sql + ",DOJ_EXP_DT =";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate()));
			logger.debug("got Dept Of Justice Expiration Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate())));
			sql = sql + ",FFL_EXP_DT =";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate()));
			logger.debug("got Federal Firearms Liscnse Expiration Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate())));
			sql = sql + ", UPDATED =  current_timestamp, ";
			logger.debug("got Updated on " + "current_timestamp");
			sql = sql + "UPDATED_BY = ";
			sql = sql + businessLicenseActivity.getUpdatedBy();
			logger.debug("got Updated By " + businessLicenseActivity.getUpdatedBy());
			sql = sql + " WHERE ACT_ID =";
			sql = sql + activityId;
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
		}
		return activityId;
	}

	// Update activity with BUSINESS NAME
	public int updateBusinessLicenseActivityWithBusinessName(BusinessLicenseActivity businessLicenseActivity, int activityId, String bName, int lsoId) {
		logger.debug("In updateBusinessLicenseActivity With Business Name " + businessLicenseActivity);
		try {
			String description = "";
			String sql_numberOfActivities = "";
			sql_numberOfActivities = "select count(*) from BL_ACTIVITY where UPPER(BUSINESS_NAME)=";
			sql_numberOfActivities = sql_numberOfActivities + StringUtils.checkString(bName.toUpperCase());
			int pTypeId = 0;
			int projectId = 0;
			int subprojectId = 0;
			logger.info(sql_numberOfActivities);
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql_numberOfActivities);
			while (rs.next()) {

				/**
				 * Inserting records into LKUP_PTYPE Table
				 */

				String subProjectName = (Constants.SUB_PROJECT_NAME_STARTS_WITH + businessLicenseActivity.getBusinessName().trim()).toUpperCase();
				logger.debug("Generated subProjectName " + subProjectName);
				pTypeId = checklkupSubProjectName(subProjectName);
				logger.debug("pTypeId Returned is  " + pTypeId);
				String lkupPtypeSql = "";
				if (pTypeId == 0) {
					pTypeId = db.getNextId("PTYPE_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					lkupPtypeSql = "insert into lkup_ptype(PTYPE_ID, DESCRIPTION, DEPT_CODE, NAME) values (";
					lkupPtypeSql = lkupPtypeSql + pTypeId;
					logger.debug("got pTypeId is " + pTypeId);
					lkupPtypeSql = lkupPtypeSql + ",";
					lkupPtypeSql = lkupPtypeSql + StringUtils.checkString(("BL - " + businessLicenseActivity.getBusinessName().trim()).toUpperCase());
					logger.debug("got Description " + StringUtils.checkString(("BL - " + businessLicenseActivity.getBusinessName().trim()).toUpperCase()));
					lkupPtypeSql = lkupPtypeSql + ",'";
					lkupPtypeSql = lkupPtypeSql + "LC";
					logger.debug("got Department Code " + "LC");
					lkupPtypeSql = lkupPtypeSql + "',";
					lkupPtypeSql = lkupPtypeSql + "null";
					logger.debug("got Name " + "null");
					lkupPtypeSql = lkupPtypeSql + ")";
					logger.info(lkupPtypeSql);
					db.insert(lkupPtypeSql);
				}

				if (pTypeId != 0) {

					description = getlkupSubProjectName(subProjectName.trim());
					description = description.trim().substring(4);
					logger.debug("description is  " + description);
					businessLicenseActivity.setBusinessName(description.trim().toUpperCase());
				}

				/**
				 * Sub Project Table
				 */
				String projName = Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES;
				projectId = checkProjectName(projName, lsoId);
				logger.debug("Project Id Returned is  " + projectId);

				int sprojNum = -1;
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formatter = new SimpleDateFormat("yy");
				String subProjectSql = "";
				subprojectId = 0;
				logger.debug("Sub Project Id Returned is  " + subprojectId);

				if (subprojectId == 0) {
					sprojNum = db.getNextId("SP_NUM");
					logger.debug("New sprojNum " + sprojNum);
					/**
					 * Sub Project Number Generation
					 */
					String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);

					/**
					 * Inserting records into SUB_PROJECT Table
					 */
					subprojectId = db.getNextId("SUB_PROJECT_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					subProjectSql = "insert into sub_project(SPROJ_ID, PROJ_ID, SPROJ_TYPE, CREATED_BY, CREATED, STATUS, SPROJ_NBR) values (";
					subProjectSql = subProjectSql + subprojectId;
					logger.debug("got Subproject Id " + subprojectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + projectId;
					logger.debug("got Project id " + projectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + pTypeId;
					logger.debug("got Sub Project Type " + pTypeId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + businessLicenseActivity.getCreatedBy();
					logger.debug("got Created By " + businessLicenseActivity.getCreatedBy());
					subProjectSql = subProjectSql + ", current_timestamp,";
					logger.debug("got Created ");
					subProjectSql = subProjectSql + 1;
					logger.debug("got Status " + 1);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + StringUtils.checkString(subProjectNumber);
					logger.debug("got Sub Project Number " + StringUtils.checkString(subProjectNumber));
					subProjectSql = subProjectSql + ")";
					logger.info(subProjectSql);
					db.insert(subProjectSql);
				}

				/**
				 * Updating records in ACTIVITY Table
				 */

				String sql_updateActivity = "";
				sql_updateActivity = "UPDATE activity SET STATUS=";
				sql_updateActivity = sql_updateActivity + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null");
				logger.debug("got activity status " + ((businessLicenseActivity.getActivityStatus() != null) ? StringUtils.i2s(businessLicenseActivity.getActivityStatus().getStatus()) : "null"));
				sql_updateActivity = sql_updateActivity + ",UPDATED_BY =";
				sql_updateActivity = sql_updateActivity + businessLicenseActivity.getUpdatedBy();
				logger.debug("got Updated By " + businessLicenseActivity.getUpdatedBy());
				sql_updateActivity = sql_updateActivity + ",UPDATED =";
				sql_updateActivity = sql_updateActivity + "current_timestamp";
				logger.debug("got Updated on " + "current_timestamp");
				sql_updateActivity = sql_updateActivity + ", START_DATE =";
				sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate()));
				logger.debug("got Starting Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getStartingDate())));
				sql_updateActivity = sql_updateActivity + ", APPLIED_DATE =";
				sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate()));
				logger.debug("got Application Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getApplicationDate())));
				sql_updateActivity = sql_updateActivity + ", ISSUED_DATE =";
				sql_updateActivity = sql_updateActivity + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "current_date");
				logger.debug("got Issue Date " + ((businessLicenseActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getIssueDate())) : "current_date"));
				sql_updateActivity = sql_updateActivity + ", SPROJ_ID =";
				sql_updateActivity = sql_updateActivity + subprojectId;
				logger.debug("got subprojectId " + subprojectId);
				sql_updateActivity = sql_updateActivity + " WHERE ACT_ID =";
				sql_updateActivity = sql_updateActivity + activityId;
				logger.info(sql_updateActivity);
				db.update(sql_updateActivity);

				/**
				 * Updating records in BL_ACTIVITY Table
				 */
				String sql = "";
				sql = "UPDATE BL_ACTIVITY SET OOT_STR_NO =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber());
				logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetNumber()));
				sql = sql + ", OOT_STR_NAME =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName());
				logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessLicenseActivity.getOutOfTownStreetName()));
				sql = sql + ", OOT_UNIT =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber());
				logger.debug("got Out Of Town Unit " + StringUtils.checkString(businessLicenseActivity.getOutOfTownUnitNumber()));
				sql = sql + ", OOT_CITY=";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity());
				logger.debug("got Out Of Town City " + StringUtils.checkString(businessLicenseActivity.getOutOfTownCity()));
				sql = sql + ", OOT_STATE =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownState());
				logger.debug("got Out Of Town State " + StringUtils.checkString(businessLicenseActivity.getOutOfTownState()));
				sql = sql + ", OOT_ZIP =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip());
				logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip()));
				sql = sql + ", OOT_ZIP4 =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4());
				logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessLicenseActivity.getOutOfTownZip4()));
				sql = sql + ", CORPORATE_NAME =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getCorporateName());
				logger.debug("got corporate name " + StringUtils.checkString(businessLicenseActivity.getCorporateName()));
				sql = sql + ", BUSINESS_PHONE =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessPhone());
				logger.debug("got Business Phone " + StringUtils.checkString(businessLicenseActivity.getBusinessPhone()));
				sql = sql + ", BUSINESS_PHONE_EXT =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessExtension());
				logger.debug("got Business Extension " + StringUtils.checkString(businessLicenseActivity.getBusinessExtension()));
				sql = sql + ", BUSINESS_FAX =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessFax());
				logger.debug("got Business Fax " + StringUtils.checkString(businessLicenseActivity.getBusinessFax()));
				sql = sql + ", HOME_OCCUPATION =";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation()));
				logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isHomeOccupation())));
				sql = sql + ", DECAL_CODE =";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode()));
				logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessLicenseActivity.isDecalCode())));
				sql = sql + ", FEDEREAL_ID =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber());
				logger.debug("got Federal Id Number " + StringUtils.checkString(businessLicenseActivity.getFederalIdNumber()));
				sql = sql + ", SSN =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber());
				logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessLicenseActivity.getSocialSecurityNumber()));
				sql = sql + ", DESC_OF_BUSINESS =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness());
				logger.debug("got Description Of Business " + StringUtils.checkString(businessLicenseActivity.getDescOfBusiness()));
				sql = sql + ", OWN_TYPE_ID =";
				sql = sql + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "null");
				logger.debug("got Ownership Type " + ((businessLicenseActivity.getOwnershipType() != null) ? StringUtils.i2s(businessLicenseActivity.getOwnershipType().getId()) : "-1"));
				sql = sql + ", EMAIL =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getEmailAddress());
				logger.debug("got Email " + StringUtils.checkString(businessLicenseActivity.getEmailAddress()));

				sql = sql + ", MAIL_STR_NO =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber());
				logger.debug("got Mail Street Number 2: " + StringUtils.checkString(businessLicenseActivity.getMailStreetNumber()));
				sql = sql + ", MAIL_STR_NAME =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getMailStreetName());
				logger.debug("got Mail Street Name 2: " + StringUtils.checkString((businessLicenseActivity.getMailStreetName())));
				sql = sql + ", MAIL_UNIT =";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber()));
				logger.debug("got Mail Unit Number 2: " + StringUtils.checkString((businessLicenseActivity.getMailUnitNumber())));
				sql = sql + ", MAIL_CITY =";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailCity()));
				logger.debug("got Mail City 2: " + StringUtils.checkString((businessLicenseActivity.getMailCity())));
				sql = sql + ", MAIL_STATE =";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailState()));
				logger.debug("got Mail State 2: " + StringUtils.checkString((businessLicenseActivity.getMailState())));
				sql = sql + ", MAIL_ZIP =";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip()));
				logger.debug("got Mail Zip 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip())));
				sql = sql + ", MAIL_ZIP4 =";
				sql = sql + StringUtils.checkString((businessLicenseActivity.getMailZip4()));
				logger.debug("got Mail Zip4 2: " + StringUtils.checkString((businessLicenseActivity.getMailZip4())));

				sql = sql + ", PRE_STR_NO =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber());
				logger.debug("got Previous Street Number " + StringUtils.checkString(businessLicenseActivity.getPrevStreetNumber()));
				sql = sql + ", PRE_STR_NAME =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevStreetName());
				logger.debug("got Previous Street Name " + StringUtils.checkString(businessLicenseActivity.getPrevStreetName()));
				sql = sql + ", PRE_CITY =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevCity());
				logger.debug("got Previous City " + StringUtils.checkString(businessLicenseActivity.getPrevCity()));
				sql = sql + ", PRE_STATE =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevState());
				logger.debug("got Previous State " + StringUtils.checkString(businessLicenseActivity.getPrevState()));
				sql = sql + ", PRE_ZIP =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip());
				logger.debug("got Previous Zip " + StringUtils.checkString(businessLicenseActivity.getPrevZip()));
				sql = sql + ", PRE_ZIP4 =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevZip4());
				logger.debug("got Previous Zip4 " + StringUtils.checkString(businessLicenseActivity.getPrevZip4()));
				sql = sql + ", PRE_UNIT =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber());
				logger.debug("got Previous Unit Number " + StringUtils.checkString(businessLicenseActivity.getPrevUnitNumber()));
				sql = sql + ", SQ_FOOTAGE =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getSquareFootage());
				logger.debug("got Square Footage " + StringUtils.checkString(businessLicenseActivity.getSquareFootage()));
				sql = sql + ", QTY_ID =";
				sql = sql + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null");
				logger.debug("got Quantity " + ((businessLicenseActivity.getQuantity() != null) ? StringUtils.i2s(businessLicenseActivity.getQuantity().getId()) : "null"));
				sql = sql + ", QTY_OTHER =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getQuantityNum());
				logger.debug("got Quantity Number " + StringUtils.checkString(businessLicenseActivity.getQuantityNum()));
				sql = sql + ", EXEM_TYPE_ID =";
				sql = sql + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null");
				logger.debug("got Type of exemptions " + ((businessLicenseActivity.getTypeOfExemptions() != null) ? StringUtils.i2s(businessLicenseActivity.getTypeOfExemptions().getId()) : "null"));
				sql = sql + ",RENEWAL_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate()));
				logger.debug("got Renewal Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getRenewalDate())));
				sql = sql + ",INS_EXP_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate()));
				logger.debug("got Insurance Expiry Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate())));
				sql = sql + ",OOB_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate()));
				logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate())));
				sql = sql + ",BOND_EXP_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate()));
				logger.debug("got Bond Expiration Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getBondExpDate())));
				sql = sql + ",DOJ_EXP_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate()));
				logger.debug("got Dept Of Justice Expiration Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate())));
				sql = sql + ",FFL_EXP_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate()));
				logger.debug("got Federal Firearms Liscnse Expiration Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate())));
				sql = sql + ", UPDATED =  current_timestamp, ";
				logger.debug("got Updated on " + "current_timestamp");
				sql = sql + "UPDATED_BY = ";
				sql = sql + businessLicenseActivity.getUpdatedBy();
				logger.debug("got Updated By " + businessLicenseActivity.getUpdatedBy());
				sql = sql + ", BUSINESS_NAME =";
				sql = sql + StringUtils.checkString(businessLicenseActivity.getBusinessName().trim().toUpperCase());
				logger.debug("got BUSINESS_NAME " + StringUtils.checkString(businessLicenseActivity.getBusinessName().trim().toUpperCase()));

				sql = sql + " WHERE ACT_ID =";
				sql = sql + activityId;
				logger.info(sql);
				db.update(sql);

				/**
				 * Deleting old sub project from sub_project table
				 */
				String oldSubProjectName = (Constants.SUB_PROJECT_NAME_STARTS_WITH + bName);
				logger.debug("Generated oldSubProjectName " + oldSubProjectName);
				int oldPTypeId = checklkupSubProjectName1(oldSubProjectName);
				logger.debug("oldPTypeId Returned is  " + oldPTypeId);

				subprojectId = checkSubProjectName(oldPTypeId, projectId);
				logger.debug("Sub Project Id Returned is  " + subprojectId);

				ProjectAgent subProjectAgent = new ProjectAgent();
				subProjectAgent.deleteSubProject(subprojectId);
				logger.info("optained projectId back as " + projectId);

			}
			rs.close();
		} catch (Exception e) {
		}
		return activityId;
	}

	/**
	 * Function to get a list of all approvals for an Activity. Used to create a list
	 * 
	 * @return
	 * @throws Exception
	 */
	public List getBusinessLicenseApprovalList(int department_id) throws Exception {
		logger.info("getBusinessLicenseApprovalList()");

		List businessLicenseApprovalList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select * from v_bl_approval where dept_id=" + department_id + "  and appr_status_id Not In('101','104','102') AND ACTIVITY_STATUS_ID NOT IN (1054) order by department_name,business_name";

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
				businessLicenseActivity.setActivityId(rs.getString("act_id"));
				logger.info("get Reffered date()" + businessLicenseActivity.getActivityId());
				businessLicenseActivity.setActivityNumber(rs.getString("ACT_NBR"));
				businessLicenseActivity.setBusinessName(rs.getString("business_name").trim().toUpperCase());
				businessLicenseActivity.setActivityType(LookupAgent.getActivityType(rs.getString("act_type")));
				logger.info("ActivityType()" + businessLicenseActivity.getActivityType());
				businessLicenseActivity.setActivityStatusDescription(rs.getString("activity_status"));
				logger.info("get getActivityType()" + businessLicenseActivity.getActivityType());
				businessLicenseActivity.setApprovalStatus(rs.getString("appr_status_name"));
				logger.info("Approval status()" + businessLicenseActivity.getApprovalStatus());
				businessLicenseActivity.setDescOfBusiness(rs.getString("desc_of_business"));
				String businessAddress = StringUtils.nullReplaceWithEmpty(rs.getString("DL_ADDRESS"));
				if ((businessAddress.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BL_ACTIVITY WHERE ACT_ID=" + rs.getString("act_id");
					logger.debug(sql);
					RowSet ootRs = new Wrapper().select(sql);
					if (ootRs.next()) {
						businessAddress = ootRs.getString(1) + " " + ootRs.getString(2);
						logger.debug("OOT Business Address is " + businessAddress);
					}
				}
				businessLicenseActivity.setAddress(businessAddress);
				businessLicenseActivity.setApplicationDate(StringUtils.dbDate2cal(rs.getString("referred_dt")));
				businessLicenseActivity.setDepartmentId(rs.getInt("dept_id"));
				businessLicenseActivity.setUserId(rs.getInt("approved_by"));
				businessLicenseActivity.setDepartmentName(StringUtils.nullReplaceWithEmpty(rs.getString("department_name")));
				businessLicenseActivity.setUserName(rs.getString("approved_by_name"));
				businessLicenseActivity.setApprovalStatus(rs.getString("APPR_STATUS_NAME"));
				businessLicenseActivity.setApprovalId(rs.getInt("approval_id"));
				businessLicenseActivity.setBusinessAccountNumber(rs.getString("BUSINESS_ACC_NO"));

				logger.info("get Reffered date()" + businessLicenseActivity.getApplicationDate());
				businessLicenseApprovalList.add(businessLicenseActivity);
			}
			rs.close();
			logger.debug("returning businessLicenseApprovalList of size " + businessLicenseApprovalList.size());
			return businessLicenseApprovalList;
		} catch (Exception e) {
			logger.error("Exception occured in getBusinessLicenseApprovalList method . Message-" + e.getMessage());
			throw e;
		}
	}

	public void updateBusinessLicenceApproval(BusinessLicenseApprovalForm businessLicenseApprovalForm, int approvalId) throws Exception {
		logger.info("saveBusinessLicenceApproval");

		String sql = null;
		String sqlSelect = null;
		String sqlInsert = null;
		try {

			Wrapper db = new Wrapper();

			sqlSelect = "select * from BL_APPROVAL where APPROVAL_ID = " + approvalId;
			logger.info(sqlSelect);
			ResultSet rs = db.select(sqlSelect);

			if (rs != null && rs.next()) {
				sqlInsert = "insert into BL_APPROVAL_HISTORY (APPROVAL_ID,REFERRED_DT,BL_APPR_ST,APPROVED_BY,APPROVED_DT,COMMENTS,CREATED,UPDATED,CREATED_BY,UPDATED_BY,ACT_ID,DEPT_ID) values (" + approvalId + "," + ((StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("REFERRED_DT")))) == null) ? "current_timestamp" : (StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("REFERRED_DT")))))) + "," + rs.getString("BL_APPR_ST") + "," + rs.getString("APPROVED_BY") + ",current_timestamp," + StringUtils.checkString(rs.getString("COMMENTS")) + "," + ((StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED")))) == null) ? "current_timestamp" : (StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED")))))) + "," + ((StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("UPDATED")))) == null) ? "current_timestamp" : (StringUtils.toOracleDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("UPDATED")))))) + "," + rs.getString("CREATED_BY") + "," + rs.getString("UPDATED_BY") + "," + rs.getString("ACT_ID") + "," + rs.getString("DEPT_ID") + ")";
				logger.info(sqlInsert);
				db.update(sqlInsert);
			}

			sql = "update BL_APPROVAL set ";
			sql += "BL_APPR_ST =" + businessLicenseApprovalForm.getApprovalStatusId();
			sql += ",";
			if ((businessLicenseApprovalForm.getApprovalStatusId() == Constants.BL_OR_BT_APPROVAL_STATUS_APPROVED) || (businessLicenseApprovalForm.getApprovalStatusId() == Constants.BL_OR_BT_APPROVAL_STATUS_APPROVED)) {
				sql += "APPROVED_DT = " + ((StringUtils.toOracleDate(businessLicenseApprovalForm.getApprovalDate()) == null) ? "current_date" : (StringUtils.toOracleDate(businessLicenseApprovalForm.getApprovalDate())));
			} else {
				sql += "APPROVED_DT = '' ";
			}
			sql += ", COMMENTS = " + StringUtils.checkString(businessLicenseApprovalForm.getComments());
			sql += ",";
			sql += "UPDATED = current_timestamp";
			sql += ",";
			sql += "UPDATED_BY = " + businessLicenseApprovalForm.getUpdatedBy();
			logger.debug("got Updated By " + businessLicenseApprovalForm.getUpdatedBy());
			sql += "  where APPROVAL_ID = " + approvalId;

			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to get a list of all approvals for an Activity. Used to create a list
	 * 
	 * @return
	 * @throws Exception
	 */
	public List getBusinessLicenseApprovalDepartmentList() throws Exception {
		logger.info("getBusinessLicenseApprovalDepartmentList()");

		List businessLicenseApprovalDepartmentList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select distinct(department_name),dept_id from v_bl_approval where appr_status_id Not In('101','104','102') order by department_name";

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
				businessLicenseActivity.setDepartmentName(rs.getString("department_name"));
				businessLicenseActivity.setDepartmentId(rs.getInt("dept_id"));
				businessLicenseActivity.setBusinessLicenseApprovals(getBusinessLicenseApprovalList(rs.getInt("dept_id")));
				businessLicenseApprovalDepartmentList.add(businessLicenseActivity);
			}
			rs.close();
			logger.debug("returning businessLicenseApprovalDepartmentList of size " + businessLicenseApprovalDepartmentList.size());
			return businessLicenseApprovalDepartmentList;
		} catch (Exception e) {
			logger.error("Exception occured in businessLicenseApprovalDepartmentList method . Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * used only to display departmentName for the particular department ID --In /listBusinessLicenseApproval.jsp
	 * 
	 * @param departmentId
	 * @return
	 * @throws Exception
	 */

	public List getOnlyDepartmentNameList(int departmentId) throws Exception {
		logger.info("getOnlyDepartmentNameList()");

		List getOnlyDepartmentNameList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select distinct(department_name) from v_bl_approval where dept_id=" + departmentId;

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				BusinessLicenseActivity businessLicenseActivity = new BusinessLicenseActivity();
				businessLicenseActivity.setDepartmentName(rs.getString("department_name"));

				getOnlyDepartmentNameList.add(businessLicenseActivity);
			}
			rs.close();
			logger.debug("returning getOnlyDepartmentNameList of size " + getOnlyDepartmentNameList.size());
			return getOnlyDepartmentNameList;
		} catch (Exception e) {
			logger.error("Exception occured in getOnlyDepartmentNameList method . Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * @param subProjectName
	 * @return pTypeId
	 * @throws Exception
	 * @author Gayathri & Manjuprasad
	 */
	public int checklkupSubProjectName1(String subProjectName) throws Exception {
		logger.info("checklkupSubProjectName(" + subProjectName + ")");
		String sql = "";
		int pTypeId = 0;
		sql = "SELECT PTYPE_ID,DESCRIPTION  FROM LKUP_PTYPE WHERE DESCRIPTION =" + StringUtils.checkString(subProjectName);
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		while (rs.next()) {
			pTypeId = rs.getInt("PTYPE_ID");
			logger.debug("pTypeId Is " + pTypeId);
		}

		return pTypeId;
	}

	/**
	 * Returns the list of business owners.
	 * 
	 * @param businessAccountNumber
	 * @param businessName
	 * @param businessOwnerName
	 * @return
	 * @throws Exception
	 */
	public List searchBusinessOwner(String businessAccountNumber, String businessName, String businessOwnerName) throws Exception {
		logger.info("searchBusinessOwner(" + businessAccountNumber + ", " + businessName + ", " + businessOwnerName + ")");
		List businessOwners = new ArrayList();  
		try {

			businessAccountNumber = businessAccountNumber.trim();
			businessName = businessName.trim();
			businessOwnerName = businessOwnerName.trim();

			Wrapper db = new Wrapper();

			// BL Search

			String sql = "SELECT * FROM V_BL_SEARCH WHERE ";

			sql = sql + ((!(businessName.equals(""))) ? "UPPER(BUSINESS_NAME) LIKE UPPER (" + StringUtils.checkString("%" + businessName + "%") + ")" : "");

			if (sql.trim().endsWith("WHERE"))

			{

				sql = sql + ((!(businessAccountNumber.equals(""))) ? " UPPER (ACCOUNT_NUMBER) = UPPER ('" + businessAccountNumber + "')" : "");

			}

			else

			{

				sql = sql + ((!(businessAccountNumber.equals(""))) ? " AND UPPER (ACCOUNT_NUMBER) = UPPER ('" + businessAccountNumber + "')" : "");

			}

			if (sql.trim().endsWith("WHERE"))

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " UPPER(BUSINESS_OWNER_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			else

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " AND UPPER(BUSINESS_OWNER_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			logger.debug("The sql is " + sql);

			ResultSet rs = db.select(sql);

			while (rs.next()) {
				logger.debug("BL Search***************************");
				if (rs.getString("BUSINESS_NAME") != null) {
					businessName = rs.getString("BUSINESS_NAME").toUpperCase();
				} else {
					businessName = rs.getString("BUSINESS_NAME");
				}
				businessOwnerName = rs.getString("BUSINESS_OWNER_NAME");
				int activity_id = rs.getInt("ACTIVITY_ID");
				String businessAddress = rs.getString("BUSINESS_ADDRESS");

				if ((businessAddress.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BL_ACTIVITY WHERE ACT_ID=" + activity_id;
					logger.debug(sql);
					RowSet ootRs = db.select(sql);
					if (ootRs.next()) {
						businessAddress = ootRs.getString(1) + " " + ootRs.getString(2);
						logger.debug("OOT Business Address is " + businessAddress);
					}
				}

				String activityType = rs.getString("ACTIVITY_TYPE");
				int accountNumber = rs.getInt("ACCOUNT_NUMBER");
				String activityStatus = rs.getString("ACTIVITY_STATUS");
				String activitySubType = rs.getString("ACTIVITY_SUBTYPE");
				int lso_id = rs.getInt("LSO_ID");

				logger.debug("the business name, business address, business ownername, activity type, activity sub-type, account number,activitystatus" + businessName + businessAddress + businessOwnerName + activityType + activitySubType + accountNumber + activityStatus);

				BusinessOwner businessOwner = new BusinessOwner();
				businessOwner.setBusinessName(StringUtils.nullReplaceWithEmpty(businessName));
				businessOwner.setBusinessOwnerName(StringUtils.nullReplaceWithEmpty(businessOwnerName));
				businessOwner.setBusinessAddress(StringUtils.nullReplaceWithEmpty(businessAddress));
				businessOwner.setActivityType(StringUtils.nullReplaceWithEmpty(activityType));
				businessOwner.setBusinessAcNumber(accountNumber);
				businessOwner.setActivityStatus(StringUtils.nullReplaceWithEmpty(activityStatus));
				businessOwner.setActivitySubType(StringUtils.nullReplaceWithEmpty(activitySubType));
				businessOwner.setActivity_id(activity_id);
				businessOwner.setLso_id(lso_id);
				businessOwners.add(businessOwner);
			}
			return businessOwners;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in searchBusinessOwner with OwnersName " + e.getMessage());
			throw new Exception("Exception occured while searching business owner " + e.getMessage());
		}
	}

	public List searchOutofTownAddress(String outofTownAddress1, String outofTownAddress2, String outofTownAddress3, String outofTownAddress4, String outofTownAddressState, String outofTownAddressCountry, String outofTownAddressZip) throws Exception {
		logger.info("searchForeignAddress(" + outofTownAddress1 + ", " + outofTownAddress2 + ", " + outofTownAddress3 + ", " + outofTownAddress4 + "," + outofTownAddressState + ", " + outofTownAddressCountry + "," + outofTownAddressZip + ")");
		List outOfTownAddressResults = new ArrayList();
		try {

			outofTownAddress1 = outofTownAddress1.trim();
			outofTownAddress2 = outofTownAddress2.trim();
			outofTownAddress3 = outofTownAddress3.trim();
			outofTownAddress4 = outofTownAddress4.trim();
			outofTownAddressState = outofTownAddressState.trim();
			outofTownAddressCountry = outofTownAddressCountry.trim();
			outofTownAddressZip = outofTownAddressZip.trim();

			Wrapper db = new Wrapper();

			// out of town address for BL
			String sql = "SELECT * FROM V_BL_SEARCH WHERE ";

			sql = sql + ((!(outofTownAddress1.equals(""))) ? "UPPER(OOT_STR_NO) LIKE UPPER ('%" + outofTownAddress1 + "%')" : "");

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddress2.equals(""))) ? " UPPER (OOT_STR_NAME) LIKE UPPER ('%" + outofTownAddress2 + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddress2.equals(""))) ? " AND UPPER (OOT_STR_NAME) LIKE UPPER ('%" + outofTownAddress2 + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddress3.equals(""))) ? " UPPER(OOT_UNIT) LIKE UPPER ('%" + outofTownAddress3 + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddress3.equals(""))) ? " AND UPPER(OOT_UNIT) LIKE UPPER ('%" + outofTownAddress3 + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddress4.equals(""))) ? " UPPER(OOT_CITY) LIKE UPPER ('%" + outofTownAddress4 + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddress4.equals(""))) ? " AND UPPER(OOT_CITY) LIKE UPPER ('%" + outofTownAddress4 + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddressState.equals(""))) ? " UPPER(OOT_STATE) LIKE UPPER ('%" + outofTownAddressState + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddressState.equals(""))) ? " AND UPPER(OOT_STATE) LIKE UPPER ('%" + outofTownAddressState + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddressCountry.equals(""))) ? " UPPER(OOT_COUNTRY) LIKE UPPER ('%" + outofTownAddressCountry + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddressCountry.equals(""))) ? " AND UPPER(OOT_COUNTRY) LIKE UPPER ('%" + outofTownAddressCountry + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddressZip.equals(""))) ? " UPPER(OOT_ZIP) LIKE UPPER ('%" + outofTownAddressZip + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddressZip.equals(""))) ? " AND UPPER(OOT_ZIP) LIKE UPPER ('%" + outofTownAddressZip + "%')" : "");
			}

			logger.debug("the sql for out of town address is" + sql);

			ResultSet rs = db.select(sql);

			while (rs.next()) {
				String businessName = "";
				if (rs.getString("BUSINESS_NAME") != null) {
					businessName = rs.getString("BUSINESS_NAME").toUpperCase();
				} else {
					businessName = rs.getString("BUSINESS_NAME");
				}
				String businessOwnerName = rs.getString("BUSINESS_OWNER_NAME");
				String businessAddress = rs.getString("BUSINESS_ADDRESS");
				int activity_id = rs.getInt("ACTIVITY_ID");

				if ((businessAddress.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BL_ACTIVITY WHERE ACT_ID=" + activity_id;
					logger.debug(sql);
					RowSet ootRs = db.select(sql);
					if (ootRs.next()) {
						businessAddress = ootRs.getString(1) + " " + ootRs.getString(2);
						logger.debug("OOT Business Address is " + businessAddress);
					}
				}
				String activityType = rs.getString("ACTIVITY_TYPE");
				int accountNumber = rs.getInt("ACCOUNT_NUMBER");
				String activityStatus = rs.getString("ACTIVITY_STATUS");
				String activitySubType = rs.getString("ACTIVITY_SUBTYPE");

				int lso_id = rs.getInt("LSO_ID");

				logger.debug("the business name, business address, business ownername, activity type, activity sub-type, account number,activitystatus" + businessName + businessAddress + businessOwnerName + activityType + activitySubType + accountNumber + activityStatus);

				BusinessOwner businessOwner = new BusinessOwner();
				businessOwner.setBusinessName(businessName);
				businessOwner.setBusinessOwnerName(businessOwnerName);
				businessOwner.setBusinessAddress(businessAddress);
				logger.debug("business address" + businessAddress);
				businessOwner.setActivityType(activityType);
				businessOwner.setBusinessAcNumber(accountNumber);
				businessOwner.setActivityStatus(activityStatus);
				businessOwner.setActivitySubType(activitySubType);
				businessOwner.setActivity_id(activity_id);
				businessOwner.setLso_id(lso_id);
				outOfTownAddressResults.add(businessOwner);
			}
			return outOfTownAddressResults;
		} catch (Exception e) {
			logger.error("Error in search out of town address" + e.getMessage());
			throw new Exception("Exception occured while searching for out of town address " + e.getMessage());
		}
	}

	/**
	 * @param businessAccountNumber
	 * @param businessName
	 * @param businessOwnerName
	 * @return businessOwners
	 * @author Hemavathi
	 * @throws Exception
	 */
	public List searchBusinessBTOwner(String businessAccountNumber, String businessName, String businessOwnerName) throws Exception {
		logger.info("searchBusinessBTOwner(" + businessAccountNumber + ", " + businessName + ", " + businessOwnerName + ")");
		List businessOwners = new ArrayList();
		String owner1Name = "";
		String owner2Name = "";
		try {

			businessAccountNumber = businessAccountNumber.trim();
			businessName = businessName.trim();
			businessOwnerName = businessOwnerName.trim();

			Wrapper db = new Wrapper();

			// BT Search

			String sql = "SELECT * FROM V_BT_SEARCH WHERE ";

			sql = sql + ((!(businessName.equals(""))) ? "UPPER(BUSINESS_NAME) LIKE UPPER (" + StringUtils.checkString("%" + businessName + "%") + ")" : "");

			if (sql.trim().endsWith("WHERE"))

			{

				sql = sql + ((!(businessAccountNumber.equals(""))) ? " UPPER (ACCOUNT_NUMBER) = UPPER ('" + businessAccountNumber + "')" : "");

			}

			else

			{

				sql = sql + ((!(businessAccountNumber.equals(""))) ? " AND UPPER (ACCOUNT_NUMBER) = UPPER ('" + businessAccountNumber + "')" : "");

			}

			if (sql.trim().endsWith("WHERE"))

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " UPPER(BUSINESS_OWNER_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			else

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " AND UPPER(BUSINESS_OWNER_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			if (sql.trim().endsWith("WHERE"))

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " UPPER(OWNER1_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			else

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " OR UPPER(OWNER1_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			if (sql.trim().endsWith("WHERE"))

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " UPPER(OWNER2_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			else

			{

				sql = sql + ((!(businessOwnerName.equals(""))) ? " OR UPPER(OWNER2_NAME) LIKE UPPER ('%" + businessOwnerName + "%')" : "");

			}

			logger.debug("The sql is " + sql);

			ResultSet rs = db.select(sql);

			while (rs.next()) {

				if (rs.getString("BUSINESS_NAME") != null) {
					businessName = rs.getString("BUSINESS_NAME").toUpperCase();
				} else {
					businessName = rs.getString("BUSINESS_NAME");
				}
				businessOwnerName = rs.getString("BUSINESS_OWNER_NAME");
				owner1Name = rs.getString("OWNER1_NAME");
				owner2Name = rs.getString("OWNER2_NAME");
				int activity_id = rs.getInt("ACTIVITY_ID");
				String businessAddress = StringUtils.nullReplaceWithEmpty(rs.getString("BUSINESS_ADDRESS"));

				if ((businessAddress.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BT_ACTIVITY WHERE ACT_ID=" + activity_id;
					logger.debug(sql);
					RowSet ootRs = db.select(sql);
					if (ootRs.next()) {
						businessAddress = ootRs.getString(1) + " " + ootRs.getString(2);
						logger.debug("OOT Business Address is " + businessAddress);
					}
				}

				String activityType = rs.getString("ACTIVITY_TYPE");
				int accountNumber = rs.getInt("ACCOUNT_NUMBER");
				String activityStatus = rs.getString("ACTIVITY_STATUS");
				String activitySubType = rs.getString("ACTIVITY_SUBTYPE");
				int lso_id = rs.getInt("LSO_ID");

				logger.debug("the business name, business address, business ownername, activity type, activity sub-type, account number,activitystatus" + businessName + businessAddress + businessOwnerName + activityType + activitySubType + accountNumber + activityStatus);

				BusinessOwner businessOwner = new BusinessOwner();
				businessOwner.setBusinessName(businessName);
				businessOwner.setBusinessOwnerName(businessOwnerName);
				businessOwner.setOwner1Name(owner1Name);
				businessOwner.setOwner2Name(owner2Name);
				businessOwner.setBusinessAddress(businessAddress);
				businessOwner.setActivityType(activityType);
				businessOwner.setBusinessAcNumber(accountNumber);
				businessOwner.setActivityStatus(activityStatus);
				businessOwner.setActivitySubType(activitySubType);
				businessOwner.setActivity_id(activity_id);
				businessOwner.setLso_id(lso_id);
				businessOwners.add(businessOwner);
			}
			return businessOwners;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in searchBusinessOwner with OwnersName " + e.getMessage());
			throw new Exception("Exception occured while searching business owner " + e.getMessage());
		}
	}

	/**
	 * @param outofTownAddress1
	 * @param outofTownAddress3
	 * @param outofTownAddress4
	 * @return outOfTownAddressResults
	 * @author Hemavathi
	 * @throws Exception
	 */

	public List searchBTOutofTownAddress(String outofTownAddress1, String outofTownAddress2, String outofTownAddress3, String outofTownAddress4, String outofTownAddressState, String outofTownAddressCountry, String outofTownAddressZip) throws Exception {
		logger.info("searchForeignAddress(" + outofTownAddress1 + ", " + outofTownAddress2 + ", " + outofTownAddress3 + ", " + outofTownAddress4 + "," + outofTownAddressState + ", " + outofTownAddressCountry + "," + outofTownAddressZip + ")");
		List outOfTownAddressResults = new ArrayList();
		try {

			outofTownAddress1 = outofTownAddress1.trim();
			outofTownAddress2 = outofTownAddress2.trim();
			outofTownAddress3 = outofTownAddress3.trim();
			outofTownAddress4 = outofTownAddress4.trim();
			outofTownAddressState = outofTownAddressState.trim();
			outofTownAddressCountry = outofTownAddressCountry.trim();
			outofTownAddressZip = outofTownAddressZip.trim();

			Wrapper db = new Wrapper();

			// out of town address for BT
			String sql = "SELECT * FROM V_BT_SEARCH WHERE ";

			sql = sql + ((!(outofTownAddress1.equals(""))) ? "UPPER(OOT_STR_NO) LIKE UPPER ('%" + outofTownAddress1 + "%')" : "");

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddress2.equals(""))) ? " UPPER (OOT_STR_NAME) LIKE UPPER ('%" + outofTownAddress2 + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddress2.equals(""))) ? " AND UPPER (OOT_STR_NAME) LIKE UPPER ('%" + outofTownAddress2 + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddress3.equals(""))) ? " UPPER(OOT_UNIT) LIKE UPPER ('%" + outofTownAddress3 + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddress3.equals(""))) ? " AND UPPER(OOT_UNIT) LIKE UPPER ('%" + outofTownAddress3 + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddress4.equals(""))) ? " UPPER(OOT_CITY) LIKE UPPER ('%" + outofTownAddress4 + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddress4.equals(""))) ? " AND UPPER(OOT_CITY) LIKE UPPER ('%" + outofTownAddress4 + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddressState.equals(""))) ? " UPPER(OOT_STATE) LIKE UPPER ('%" + outofTownAddressState + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddressState.equals(""))) ? " AND UPPER(OOT_STATE) LIKE UPPER ('%" + outofTownAddressState + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddressCountry.equals(""))) ? " UPPER(OOT_COUNTRY) LIKE UPPER ('%" + outofTownAddressCountry + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddressCountry.equals(""))) ? " AND UPPER(OOT_COUNTRY) LIKE UPPER ('%" + outofTownAddressCountry + "%')" : "");
			}

			if (sql.trim().endsWith("WHERE")) {
				sql = sql + ((!(outofTownAddressZip.equals(""))) ? " UPPER(OOT_ZIP) LIKE UPPER ('%" + outofTownAddressZip + "%')" : "");
			} else {
				sql = sql + ((!(outofTownAddressZip.equals(""))) ? " AND UPPER(OOT_ZIP) LIKE UPPER ('%" + outofTownAddressZip + "%')" : "");
			}

			logger.debug("the sql for out of town address is" + sql);

			ResultSet rs = db.select(sql);

			while (rs.next()) {
				String businessName = "";
				if (rs.getString("BUSINESS_NAME") != null) {
					businessName = rs.getString("BUSINESS_NAME").toUpperCase();
				} else {
					businessName = rs.getString("BUSINESS_NAME");
				}
				String businessOwnerName = rs.getString("BUSINESS_OWNER_NAME");
				String businessAddress = rs.getString("BUSINESS_ADDRESS");
				int activity_id = rs.getInt("ACTIVITY_ID");

				if ((businessAddress.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BT_ACTIVITY WHERE ACT_ID=" + activity_id;
					logger.debug(sql);
					RowSet ootRs = db.select(sql);
					if (ootRs.next()) {
						businessAddress = ootRs.getString(1) + " " + ootRs.getString(2);
						logger.debug("OOT Business Address is " + businessAddress);
					}
				}
				String activityType = rs.getString("ACTIVITY_TYPE");
				int accountNumber = rs.getInt("ACCOUNT_NUMBER");
				String activityStatus = rs.getString("ACTIVITY_STATUS");
				String activitySubType = rs.getString("ACTIVITY_SUBTYPE");

				int lso_id = rs.getInt("LSO_ID");

				logger.debug("the business name, business address, business ownername, activity type, activity sub-type, account number,activitystatus" + businessName + businessAddress + businessOwnerName + activityType + activitySubType + accountNumber + activityStatus);

				BusinessOwner businessOwner = new BusinessOwner();
				businessOwner.setBusinessName(businessName);
				businessOwner.setBusinessOwnerName(businessOwnerName);
				businessOwner.setBusinessAddress(businessAddress);
				logger.debug("business address" + businessAddress);
				businessOwner.setActivityType(activityType);
				businessOwner.setBusinessAcNumber(accountNumber);
				businessOwner.setActivityStatus(activityStatus);
				businessOwner.setActivitySubType(activitySubType);
				businessOwner.setActivity_id(activity_id);
				businessOwner.setLso_id(lso_id);
				outOfTownAddressResults.add(businessOwner);
			}
			return outOfTownAddressResults;
		} catch (Exception e) {
			logger.error("Error in search out of town address" + e.getMessage());
			throw new Exception("Exception occured while searching for out of town address " + e.getMessage());
		}
	}

	public int checkAddress(String streetNumber, String streetFraction, int streetId, String unit, String zip, String zip4, BusinessTaxActivity businessTaxActivity) throws Exception {
		logger.info("checkAddress(" + streetNumber + "," + streetFraction + " , " + streetId + ", " + unit + ", " + zip + ", " + zip4 + ")");
		int lsoId = 0;
		String sql = "";
		int addressId = 0;

		sql = "SELECT * FROM V_ADDRESS_LIST WHERE ";

		if (!(GenericValidator.isBlankOrNull(streetNumber))) {
			sql += (" STR_NO=" + streetNumber);
		}
		if (!(GenericValidator.isBlankOrNull(streetFraction))) {
			sql += (" and STR_MOD=" + StringUtils.checkString(streetFraction));
		}
		if (!(streetId == 0)) {
			sql += (" and STREET_ID=" + streetId);
		}
		if (!((unit == null) || (unit.equalsIgnoreCase("")))) {
			sql += (" and UNIT=" + StringUtils.checkString(unit));
		} else {
			sql += (" and UNIT is null ");
		}
		if (!((zip == null) || (zip.equalsIgnoreCase("")))) {
			sql += (" AND ZIP=" + StringUtils.checkString(zip));
		}
		if (!((zip4 == null) || (zip4.equalsIgnoreCase("")))) {
			sql += (" AND ZIP4=" + StringUtils.checkString(zip4));
		}

		sql += " and LSO_TYPE = 'O' order by addr_id";

		logger.debug("sql statement is " + sql);

		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			lsoId = rs.getInt("LSO_ID");
			logger.debug("LSO Id Is " + lsoId);
			addressId = rs.getInt("ADDR_ID");
			logger.debug("Address Id is " + addressId);
			businessTaxActivity.setBusinessAddressStreetNumber(rs.getString("STR_NO"));
			businessTaxActivity.setBusinessAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
			businessTaxActivity.setBusinessAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
			businessTaxActivity.setBusinessAddressUnitNumber(rs.getString("UNIT"));
			businessTaxActivity.setBusinessAddressCity(rs.getString("CITY"));
			businessTaxActivity.setBusinessAddressState(rs.getString("STATE"));
			businessTaxActivity.setBusinessAddressZip(rs.getString("ZIP"));
			businessTaxActivity.setBusinessAddressZip4(rs.getString("ZIP4"));
		}
		return lsoId;
	}

	/**
	 * SaveBusinessTaxActivity()
	 * 
	 * @author Gayathri
	 * @param BusinessTaxActivity
	 *            , lsoId
	 * @return activityId
	 * @param businessTaxActivity
	 * @throws Exception
	 */
	public int saveBusinessTaxActivity(BusinessTaxActivity businessTaxActivity, int LsoId) throws Exception {
		logger.info("saveBusinessTaxActivity(" + businessTaxActivity + ", " + LsoId + ")");

		int projNum = -1;
		int sprojNum = -1;
		int activityId = -1;
		int activityNum = -1;
		int subprojectId = 0;
		String activityNumber = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yy");

		try {
			Wrapper db = new Wrapper();
			String sql = "";
			String projectSql = "";
			String subProjectSql = "";
			String activitySql = "";
			String lkupPtypeSql = "";
			int businessAccNo = 0;
			int pTypeId = 0;
			int projectId = 0;
			int addressId = 0;
			
			addressId=LookupAgent.getAddressIdForPsaId(LsoId+"");
			String projName = Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES;
			projectId = checkProjectName(projName, LsoId);
			logger.debug("Project Id Returned is  " + projectId);

			if (businessTaxActivity != null) {
				if (projectId == 0) {
					/**
					 * Project Number Generation
					 */
					projNum = db.getNextId("PR_NUM");

					String projectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);

					/**
					 * Inserting records into PROJECT Table
					 */
					projectId = db.getNextId("PROJECT_ID");
					logger.debug("The ID generated for new Project is " + projectId);
					projectSql = "insert into project(PROJ_ID, LSO_ID, PROJECT_NBR, NAME, DESCRIPTION, STATUS_ID, DEPT_ID, CREATED_BY, CREATED_DT) values(";
					projectSql = projectSql + projectId;
					logger.debug("got Project id " + projectId);
					projectSql = projectSql + ",";
					projectSql = projectSql + LsoId;
					logger.debug("got LSO id " + LsoId);
					projectSql = projectSql + ",";

					projectSql = projectSql + StringUtils.checkString(projectNumber);
					logger.debug("got Project Number " + StringUtils.checkString(projectNumber));
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projName);
					logger.debug("got Project Name " + StringUtils.checkString(projName));
					projectSql = projectSql + ",";
					projectSql = projectSql + StringUtils.checkString(projName);
					logger.debug("got Project Description " + StringUtils.checkString(projName));
					projectSql = projectSql + ",";
					projectSql = projectSql + 1;// STATUS OF ACTIVE
					logger.debug("got Status Id " + "1");
					projectSql = projectSql + ",";
					projectSql = projectSql + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES;
					logger.debug("got Department ID " + Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES);
					projectSql = projectSql + ",";
					projectSql = projectSql + businessTaxActivity.getCreatedBy();
					logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
					projectSql = projectSql + ", current_timestamp)";
					logger.info(projectSql);
					db.insert(projectSql);
				}

				/**
				 * Inserting records into LKUP_PTYPE Table
				 */
				String subProjectName = (Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + businessTaxActivity.getBusinessName().trim()).toUpperCase();
				logger.debug("Generated subProjectName " + subProjectName);
				pTypeId = checklkupSubProjectName(subProjectName.trim());
				logger.debug("pTypeId Returned is  " + pTypeId);

				if (pTypeId == 0) {
					pTypeId = db.getNextId("PTYPE_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					lkupPtypeSql = "insert into lkup_ptype(PTYPE_ID, DESCRIPTION, DEPT_CODE, NAME) values (";
					lkupPtypeSql = lkupPtypeSql + pTypeId;
					logger.debug("got pTypeId is " + pTypeId);
					lkupPtypeSql = lkupPtypeSql + ",";
					lkupPtypeSql = lkupPtypeSql + StringUtils.checkString(("BT - " + businessTaxActivity.getBusinessName().trim()).toUpperCase());
					logger.debug("got Description " + StringUtils.checkString(("BT - " + businessTaxActivity.getBusinessName().trim()).toUpperCase()));
					lkupPtypeSql = lkupPtypeSql + ",'";
					lkupPtypeSql = lkupPtypeSql + "LC";
					logger.debug("got Department Code " + "LC");
					lkupPtypeSql = lkupPtypeSql + "',";
					lkupPtypeSql = lkupPtypeSql + "null";
					logger.debug("got Name " + "null");
					lkupPtypeSql = lkupPtypeSql + ")";

					logger.info(lkupPtypeSql);
					db.insert(lkupPtypeSql);
				}

				// subprojectId = checkSubProjectName(pTypeId, projectId);
				subprojectId = 0;
				logger.debug("Sub Project Id Returned is  " + subprojectId);

				if (subprojectId == 0) {

					sprojNum = db.getNextId("SP_NUM");
					/**
					 * Sub Project Number Generation
					 */
					String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);
					/**
					 * Inserting records into SUB_PROJECT Table
					 */
					subprojectId = db.getNextId("SUB_PROJECT_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					subProjectSql = "insert into sub_project(SPROJ_ID, PROJ_ID, SPROJ_TYPE, CREATED_BY, CREATED, STATUS, SPROJ_NBR) values (";
					subProjectSql = subProjectSql + subprojectId;
					logger.debug("got Subproject Id " + subprojectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + projectId;
					logger.debug("got Project id " + projectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + pTypeId;
					logger.debug("got Sub Project Type " + pTypeId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + businessTaxActivity.getCreatedBy();
					logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
					subProjectSql = subProjectSql + ", current_timestamp,";
					logger.debug("got Created ");
					subProjectSql = subProjectSql + 1;
					logger.debug("got Status " + 1);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + StringUtils.checkString(subProjectNumber);
					logger.debug("got Sub Project Number " + StringUtils.checkString(subProjectNumber));
					subProjectSql = subProjectSql + ")";

					logger.info(subProjectSql);
					db.insert(subProjectSql);
				}

				// Checking activity existing or not
				String actBtType = businessTaxActivity.getActivityType().getType();
				activityId = checkActivityName(subprojectId, actBtType);
				if (activityId != 0) {

					String inactivateSql = "update activity set status =" + Constants.ACTIVITY_STATUS_BT_OUT_OF_BUSINESS + " where act_id = " + activityId;
					logger.info(inactivateSql);
					db.update(inactivateSql);
				}

				// Activity Id Generation
				activityId = db.getNextId("ACTIVITY_ID");
				logger.debug("The ID generated for new Activity is " + activityId);

				// Activity Number generation
				activityNum = db.getNextId("BT_NUM");
				logger.debug("Obtained new activity number is " + activityNum);

				String strActNum = StringUtils.i2s(activityNum);
				strActNum = "00000".substring(strActNum.length()) + strActNum;
				logger.debug("Obtained new activity number string is " + strActNum);
				calendar = Calendar.getInstance();
				formatter = new SimpleDateFormat("yy");
				activityNumber = "BT" + formatter.format(calendar.getTime()) + strActNum.trim();

				/**
				 * Inserting records into ACTIVITY Table
				 * 
				 */
				activitySql = "insert into activity(ACT_ID, SPROJ_ID, ADDR_ID, ACT_NBR, PLAN_CHK_REQ, ACT_TYPE,  VALUATION, STATUS, START_DATE, APPLIED_DATE, ISSUED_DATE, PLAN_CHK_FEE_DATE, DEVELOPMENT_FEE_DATE, PERMIT_FEE_DATE, DEV_FEE_REQ, CREATED_BY, CREATED,UPDATED_BY,UPDATED) values(";

				activitySql = activitySql + activityId;
				logger.debug("got Activity id " + activityId);
				activitySql = activitySql + ",";
				activitySql = activitySql + subprojectId;
				logger.debug("got Sub Project Id " + subprojectId);
				activitySql = activitySql + ",";
				activitySql = activitySql + addressId;
				logger.debug("got Address Id " + addressId);
				activitySql = activitySql + ",";
				activitySql = activitySql + StringUtils.checkString(activityNumber);
				logger.debug("got Activity Number " + activityNumber);
				activitySql = activitySql + ",'N','";
				activitySql = activitySql + ((businessTaxActivity.getActivityType() != null) ? businessTaxActivity.getActivityType().getType() : "");
				logger.debug("got Activity Type " + ((businessTaxActivity.getActivityType() != null) ? businessTaxActivity.getActivityType().getType() : ""));
				activitySql = activitySql + "',";
				activitySql = activitySql + "0";
				logger.debug("got Valuation " + "0");
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null");
				logger.debug("got Activity Status " + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null"));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate())) : null);
				logger.debug("got Start Date " + ((businessTaxActivity.getStartingDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate())) : null));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate())) : null);
				logger.debug("got Applied Date " + ((businessTaxActivity.getApplicationDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate())) : null));
				activitySql = activitySql + ",";
				activitySql = activitySql + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null);
				logger.debug("got Issued Date " + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null));
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_date";
				activitySql = activitySql + ",'N',";
				activitySql = activitySql + businessTaxActivity.getCreatedBy();
				logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_timestamp";
				logger.debug("got Created " + "current_timestamp");
				activitySql = activitySql + ",";
				activitySql = activitySql + businessTaxActivity.getCreatedBy();
				activitySql = activitySql + ",";
				activitySql = activitySql + "current_timestamp";
				
				activitySql = activitySql + ")";
				logger.info(activitySql);
				db.insert(activitySql);

				/**
				 * Inserting records into BT_ACTIVITY Table
				 */
				businessAccNo = db.getNextId("BUSINESS_ACC_NO");
				logger.debug("Business Account No generated is " + businessAccNo);

				sql = "INSERT INTO BT_ACTIVITY(ACT_ID, BURBANK_BUSINESS, APPL_TYPE_ID, OOT_STR_NO, OOT_STR_NAME, OOT_UNIT, OOT_CITY, OOT_STATE,  OOT_ZIP, OOT_ZIP4, BUSINESS_NAME, BUSINESS_ACC_NO, CORPORATE_NAME, BUSINESS_PHONE, BUSINESS_PHONE_EXT, BUSINESS_FAX, SIC_CODE, MUNICIPAL_CODE, DESC_OF_BUSINESS, CLASS_CODE, HOME_OCCUPATION, DECAL_CODE, OTHER_BUSINESS_OCCUPANCY, OOB_DT, OWN_TYPE_ID, FEDEREAL_ID, EMAIL, SSN, MAIL_STR_NO, MAIL_STR_NAME, MAIL_UNIT, MAIL_CITY, MAIL_STATE, MAIL_ZIP, MAIL_ZIP4, PRE_STR_NO, PRE_STR_NAME, PRE_UNIT, PRE_CITY, PRE_STATE, PRE_ZIP, PRE_ZIP4, NAME1, TITLE1, OWNER1_STR_NO, OWNER1_STR_NAME, OWNER1_UNIT, OWNER1_CITY, OWNER1_STATE, OWNER1_ZIP, OWNER1_ZIP4, NAME2, TITLE2, OWNER2_STR_NO, OWNER2_STR_NAME, OWNER2_UNIT, OWNER2_CITY, OWNER2_STATE, OWNER2_ZIP, OWNER2_ZIP4, SQ_FOOTAGE, QTY_ID, QTY_OTHER, DRIVER_LICENSE, CREATED, CREATED_BY, ATTN) values(";
				sql = sql + activityId;
				logger.debug("got Activity id " + activityId);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isBusinessLocation()));
				logger.debug("got Business Location " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isBusinessLocation())));
				sql = sql + ",";
				sql = sql + ((businessTaxActivity.getApplicationType() != null) ? StringUtils.i2s(businessTaxActivity.getApplicationType().getId()) : "null");
				logger.debug("got Application Type ID" + ((businessTaxActivity.getApplicationType() != null) ? StringUtils.i2s(businessTaxActivity.getApplicationType().getId()) : "null"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber());
				logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName());
				logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber());
				logger.debug("got out Of Town Unit Number " + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownCity());
				logger.debug("got Out Of Town City " + StringUtils.checkString(businessTaxActivity.getOutOfTownCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownState());
				logger.debug("got Out Of Town State " + StringUtils.checkString(businessTaxActivity.getOutOfTownState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip());
				logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4());
				logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessName().trim().toUpperCase());
				logger.debug("got Business Name " + StringUtils.checkString(businessTaxActivity.getBusinessName().trim().toUpperCase()));
				sql = sql + ",";
				sql = sql + businessAccNo;
				logger.debug("got Business Account Number " + businessAccNo);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getCorporateName());
				logger.debug("got Corporate Name " + StringUtils.checkString(businessTaxActivity.getCorporateName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessPhone());
				logger.debug("got Business Phone " + StringUtils.checkString(StringUtils.phoneFormat(businessTaxActivity.getBusinessPhone())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessExtension());
				logger.debug("got Business Extension " + StringUtils.checkString(businessTaxActivity.getBusinessExtension()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessFax());
				logger.debug("got Business Fax " + StringUtils.checkString(businessTaxActivity.getBusinessFax()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSicCode());
				logger.debug("got Sic Code " + StringUtils.checkString(businessTaxActivity.getSicCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getMuncipalCode());
				logger.debug("got Muncipal Code " + StringUtils.checkString(businessTaxActivity.getMuncipalCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getDescOfBusiness());
				logger.debug("got Description Of Business " + StringUtils.checkString(businessTaxActivity.getDescOfBusiness()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getClassCode());
				logger.debug("got Class Code " + StringUtils.checkString(businessTaxActivity.getClassCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation()));
				logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode()));
				logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy()));
				logger.debug("got Other Business Occupancy " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy())));
				sql = sql + ",";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate()));
				logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate())));
				sql = sql + ",";
				sql = sql + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "null");
				logger.debug("got Ownership Type " + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "-1"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getFederalIdNumber());
				logger.debug("got Federal Id Number " + StringUtils.checkString(businessTaxActivity.getFederalIdNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getEmailAddress());
				logger.debug("got Email " + StringUtils.checkString(businessTaxActivity.getEmailAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber());
				logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber()));
				sql = sql + ",";

				sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetNumber());
				logger.debug("got Mail Street Number 2: " + StringUtils.checkString(businessTaxActivity.getMailStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetName());
				logger.debug("got Mail Street Name 2: " + StringUtils.checkString((businessTaxActivity.getMailStreetName())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailUnitNumber()));
				logger.debug("got Mail Unit Number 2: " + StringUtils.checkString((businessTaxActivity.getMailUnitNumber())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailCity()));
				logger.debug("got Mail City 2: " + StringUtils.checkString((businessTaxActivity.getMailCity())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailState()));
				logger.debug("got Mail State 2: " + StringUtils.checkString((businessTaxActivity.getMailState())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip()));
				logger.debug("got Mail Zip 2: " + StringUtils.checkString((businessTaxActivity.getMailZip())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip4()));
				logger.debug("got Mail Zip4 2: " + StringUtils.checkString((businessTaxActivity.getMailZip4())));
				sql = sql + ",";

				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber());
				logger.debug("got Previous Street Number " + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetName());
				logger.debug("got Previous Street Name " + StringUtils.checkString(businessTaxActivity.getPrevStreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber());
				logger.debug("got Previous Unit Number " + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevCity());
				logger.debug("got Previous City " + StringUtils.checkString(businessTaxActivity.getPrevCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevState());
				logger.debug("got Previous State " + StringUtils.checkString(businessTaxActivity.getPrevState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip());
				logger.debug("got Previous Zip " + StringUtils.checkString(businessTaxActivity.getPrevZip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip4());
				logger.debug("got Previous Zip4 " + StringUtils.checkString(businessTaxActivity.getPrevZip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getName1());
				logger.debug("got Name 1 " + StringUtils.checkString(businessTaxActivity.getName1()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getTitle1());
				logger.debug("got Title 1 " + StringUtils.checkString(businessTaxActivity.getTitle1()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber());
				logger.debug("got Owner1 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetName());
				logger.debug("got Owner1 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner1StreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber());
				logger.debug("got Owner1 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1City());
				logger.debug("got Owner1 City " + StringUtils.checkString(businessTaxActivity.getOwner1City()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1State());
				logger.debug("got Owner1 State " + StringUtils.checkString(businessTaxActivity.getOwner1State()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip());
				logger.debug("got Owner1 Zip " + StringUtils.checkString(businessTaxActivity.getOwner1Zip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip4());
				logger.debug("got Owner1 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner1Zip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getName2());
				logger.debug("got Name 2 " + StringUtils.checkString(businessTaxActivity.getName2()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getTitle2());
				logger.debug("got Title 2 " + StringUtils.checkString(businessTaxActivity.getTitle2()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber());
				logger.debug("got Owner2 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetName());
				logger.debug("got Owner2 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner2StreetName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber());
				logger.debug("got Owner2 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2City());
				logger.debug("got Owner2 City " + StringUtils.checkString(businessTaxActivity.getOwner2City()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2State());
				logger.debug("got Owner2 State " + StringUtils.checkString(businessTaxActivity.getOwner2State()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip());
				logger.debug("got Owner2 Zip " + StringUtils.checkString(businessTaxActivity.getOwner2Zip()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip4());
				logger.debug("got Owner2 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner2Zip4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSquareFootage());
				logger.debug("got Square Footage " + StringUtils.checkString(businessTaxActivity.getSquareFootage()));
				sql = sql + ",";
				sql = sql + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null");
				logger.debug("got Quantity " + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null"));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getQuantityNum());
				logger.debug("got Quantity Number " + StringUtils.checkString(businessTaxActivity.getQuantityNum()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getDriverLicense());
				logger.debug("got Driver's License Number " + StringUtils.checkString(businessTaxActivity.getDriverLicense()));
				sql = sql + ", current_timestamp,";
				sql = sql + businessTaxActivity.getCreatedBy();
				logger.debug("got created by " + businessTaxActivity.getCreatedBy());
				sql = sql + ",";
				sql = sql + StringUtils.checkString(businessTaxActivity.getAttn());
				logger.debug("got ATTN " + StringUtils.checkString(businessTaxActivity.getAttn()));
				sql = sql + ")";

				logger.info(sql);
				db.insert(sql);

			}
			return activityId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Business Tax activity
	 * 
	 * @param activityId
	 *            , lsoId
	 * @return BusinessTaxActivity Object
	 * @throws Exception
	 * @author Gayathri
	 * 
	 */
	public BusinessTaxActivity getBusinessTaxActivity(int activityId, int lsoId) throws Exception {
		logger.info("getBusinessTaxActivity(" + activityId + ", " + lsoId + ")");

		BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
		logger.info("lso id is " + lsoId);

		try {
			Wrapper db = new Wrapper();
			String sql = "";
			sql = "SELECT * FROM  ACTIVITY A, BT_ACTIVITY BA, V_ADDRESS_LIST VAL WHERE  A.ACT_ID = BA.ACT_ID  AND  VAL.LSO_ID=" + lsoId + " AND LSO_TYPE='O'  AND  A.ACT_ID = " + activityId;
			logger.info(sql);

			RowSet rs = db.select(sql);
			
		
		//	businessTaxActivity = getMultiActivityAddress(activityId);
			while (rs.next()) {
				businessTaxActivity.setActivityNumber(rs.getString("ACT_NBR"));
				businessTaxActivity.setActivityStatus(LookupAgent.getActivityStatus(rs.getInt("STATUS")));
				businessTaxActivity.setApplicationType(LookupAgent.getApplicationType(rs.getInt("APPL_TYPE_ID")));
				businessTaxActivity.setActivityType(LookupAgent.getActivityType(rs.getString("ACT_TYPE")));
				businessTaxActivity.setBusinessAddressStreetNumber(StringUtils.i2s(rs.getInt("STR_NO")));
				businessTaxActivity.setBusinessAddressStreetFraction(StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")));
				businessTaxActivity.setBusinessAddressStreetName(((rs.getString("PRE_DIR")) != null ? (rs.getString("PRE_DIR")) : "") + " " + ((rs.getString("STR_NAME")) != null ? (rs.getString("STR_NAME")) : "") + " " + ((rs.getString("STR_TYPE")) != null ? (rs.getString("STR_TYPE")) : ""));
				businessTaxActivity.setBusinessAddressUnitNumber(rs.getString("UNIT"));
				businessTaxActivity.setBusinessAddressCity(rs.getString("CITY"));
				businessTaxActivity.setBusinessAddressState(rs.getString("STATE"));
				businessTaxActivity.setBusinessAddressZip(rs.getString("ZIP"));
				businessTaxActivity.setBusinessAddressZip4(rs.getString("ZIP4"));
				businessTaxActivity.setBusinessLocation(StringUtils.s2b(rs.getString("BURBANK_BUSINESS")));
				businessTaxActivity.setOutOfTownStreetNumber(rs.getString("OOT_STR_NO"));
				if (rs.getString("OOT_STR_NO") == null) {
					businessTaxActivity.setOutOfTownStreetNumber("");
				}
				businessTaxActivity.setOutOfTownStreetName(rs.getString("OOT_STR_NAME"));
				businessTaxActivity.setOutOfTownUnitNumber(rs.getString("OOT_UNIT"));
				businessTaxActivity.setOutOfTownCity(rs.getString("OOT_CITY"));
				businessTaxActivity.setOutOfTownState(rs.getString("OOT_STATE"));
				businessTaxActivity.setOutOfTownZip(rs.getString("OOT_ZIP"));
				businessTaxActivity.setOutOfTownZip4(rs.getString("OOT_ZIP4"));
				businessTaxActivity.setBusinessName(rs.getString("BUSINESS_NAME").trim().toUpperCase());
				businessTaxActivity.setBusinessAccountNumber(rs.getString("BUSINESS_ACC_NO"));
				businessTaxActivity.setCorporateName(rs.getString("CORPORATE_NAME"));
				businessTaxActivity.setAttn(rs.getString("ATTN"));
				businessTaxActivity.setBusinessPhone(rs.getString("BUSINESS_PHONE"));
				businessTaxActivity.setBusinessExtension(rs.getString("BUSINESS_PHONE_EXT"));
				businessTaxActivity.setBusinessFax(rs.getString("BUSINESS_FAX"));
				businessTaxActivity.setClassCode(rs.getString("CLASS_CODE"));
				businessTaxActivity.setMuncipalCode(rs.getString("MUNICIPAL_CODE"));
				businessTaxActivity.setSicCode(rs.getString("SIC_CODE"));
				businessTaxActivity.setDecalCode(StringUtils.s2b(rs.getString("DECAL_CODE")));
				businessTaxActivity.setOtherBusinessOccupancy(StringUtils.s2b(rs.getString("OTHER_BUSINESS_OCCUPANCY")));
				businessTaxActivity.setDescOfBusiness(rs.getString("DESC_OF_BUSINESS"));
				businessTaxActivity.setHomeOccupation(StringUtils.s2b(rs.getString("HOME_OCCUPATION")));
				businessTaxActivity.setCreationDate(StringUtils.dbDate2cal(rs.getString("CREATED")));
				businessTaxActivity.setApplicationDate(StringUtils.dbDate2cal(rs.getString("APPLIED_DATE")));
				businessTaxActivity.setIssueDate(StringUtils.dbDate2cal(rs.getString("ISSUED_DATE")));
				businessTaxActivity.setStartingDate(StringUtils.dbDate2cal(rs.getString("START_DATE")));
				businessTaxActivity.setOwnershipType(LookupAgent.getOwnershipType(rs.getInt("OWN_TYPE_ID")));
				businessTaxActivity.setOutOfBusinessDate(StringUtils.dbDate2cal(rs.getString("OOB_DT")));
				businessTaxActivity.setFederalIdNumber(rs.getString("FEDEREAL_ID"));
				businessTaxActivity.setEmailAddress(rs.getString("EMAIL"));
				businessTaxActivity.setSocialSecurityNumber(rs.getString("SSN"));
				/*businessTaxActivity.setMailStreetNumber(rs.getString("MAIL_STR_NO"));
				businessTaxActivity.setMailStreetName(rs.getString("MAIL_STR_NAME"));
				businessTaxActivity.setMailUnitNumber(rs.getString("MAIL_UNIT"));
				businessTaxActivity.setMailCity(rs.getString("MAIL_CITY"));
				businessTaxActivity.setMailState(rs.getString("MAIL_STATE"));
				businessTaxActivity.setMailZip(rs.getString("MAIL_ZIP"));
				businessTaxActivity.setMailZip4(rs.getString("MAIL_ZIP4"));
				businessTaxActivity.setPrevStreetNumber(rs.getString("PRE_STR_NO"));
				businessTaxActivity.setPrevStreetName(rs.getString("PRE_STR_NAME"));
				businessTaxActivity.setPrevUnitNumber(rs.getString("PRE_UNIT"));
				businessTaxActivity.setPrevCity(rs.getString("PRE_CITY"));
				businessTaxActivity.setPrevState(rs.getString("PRE_STATE"));
				businessTaxActivity.setPrevZip(rs.getString("PRE_ZIP"));
				businessTaxActivity.setPrevZip4(rs.getString("PRE_ZIP4"));
				businessTaxActivity.setName1(rs.getString("NAME1"));
				businessTaxActivity.setTitle1(rs.getString("TITLE1"));
				businessTaxActivity.setOwner1StreetNumber(rs.getString("OWNER1_STR_NO"));
				businessTaxActivity.setOwner1StreetName(rs.getString("OWNER1_STR_NAME"));
				businessTaxActivity.setOwner1UnitNumber(rs.getString("OWNER1_UNIT"));
				businessTaxActivity.setOwner1City(rs.getString("OWNER1_CITY"));
				businessTaxActivity.setOwner1State(rs.getString("OWNER1_STATE"));
				businessTaxActivity.setOwner1Zip(rs.getString("OWNER1_ZIP"));
				businessTaxActivity.setOwner1Zip4(rs.getString("OWNER1_ZIP4"));
				businessTaxActivity.setName2(rs.getString("NAME2"));
				businessTaxActivity.setTitle2(rs.getString("TITLE2"));
				businessTaxActivity.setOwner2StreetNumber(rs.getString("OWNER2_STR_NO"));
				businessTaxActivity.setOwner2StreetName(rs.getString("OWNER2_STR_NAME"));
				businessTaxActivity.setOwner2UnitNumber(rs.getString("OWNER2_UNIT"));
				businessTaxActivity.setOwner2City(rs.getString("OWNER2_CITY"));
				businessTaxActivity.setOwner2State(rs.getString("OWNER2_STATE"));
				businessTaxActivity.setOwner2Zip(rs.getString("OWNER2_ZIP"));
				businessTaxActivity.setOwner2Zip4(rs.getString("OWNER2_ZIP4"));*/
				businessTaxActivity.setSquareFootage(rs.getString("SQ_FOOTAGE"));
				businessTaxActivity.setQuantity(LookupAgent.getBTQuantityType(rs.getInt("QTY_ID")));
				businessTaxActivity.setQuantityNum(rs.getString("QTY_OTHER"));
				businessTaxActivity.setDriverLicense(rs.getString("DRIVER_LICENSE"));
				businessTaxActivity.setCreatedBy(rs.getInt("CREATED_BY"));
				businessTaxActivity.setRenewalOnline(rs.getString("RENEWAL_ONLINE"));
				/*businessTaxActivity.setMailAttn(rs.getString("MAIL_ATTN"));
				businessTaxActivity.setPreAttn(rs.getString("PRE_ATTN"));
				businessTaxActivity.setOwner1Attn(rs.getString("OWNER1_ATTN"));
				businessTaxActivity.setOwner2Attn(rs.getString("OWNER2_ATTN"));*/
				
				logger.debug("Values setted to BusinessTax object");
			}

			
			businessTaxActivity.setMultiAddress(getMultiAddress(activityId, "BT"));

			
			return businessTaxActivity;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the updating activity on given address
	 * 
	 * @author Gayathri
	 * @param businessTaxActivity
	 * @return activityId
	 */
	public int updateBusinessTaxActivity(BusinessTaxActivity businessTaxActivity, int activityId) {
		logger.debug("In updateBusinessTaxActivity " + businessTaxActivity);
		try {
			String sql_updateActivity = "";
			Wrapper db = new Wrapper();
			String sql = "";
			/**
			 * Updating records in ACTIVITY Table
			 */

			sql_updateActivity = "UPDATE activity SET STATUS=";
			sql_updateActivity = sql_updateActivity + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null");
			logger.debug("got activity status " + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null"));
			sql_updateActivity = sql_updateActivity + ",UPDATED_BY =";
			sql_updateActivity = sql_updateActivity + businessTaxActivity.getUpdatedBy();
			logger.debug("got Updated By " + businessTaxActivity.getUpdatedBy());
			sql_updateActivity = sql_updateActivity + ",UPDATED =";
			sql_updateActivity = sql_updateActivity + "current_timestamp";
			logger.debug("got Updated on " + "current_timestamp");
			sql_updateActivity = sql_updateActivity + ", START_DATE =";
			sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate()));
			logger.debug("got Starting Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate())));
			sql_updateActivity = sql_updateActivity + ", APPLIED_DATE =";
			sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate()));
			logger.debug("got Application Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate())));
			sql_updateActivity = sql_updateActivity + ", ISSUED_DATE =";
			sql_updateActivity = sql_updateActivity + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null);
			logger.debug("got Issue Date " + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null));
			sql_updateActivity = sql_updateActivity + " WHERE ACT_ID =";
			sql_updateActivity = sql_updateActivity + activityId;
			logger.info(sql_updateActivity);
			db.update(sql_updateActivity);

			/**
			 * Updating records in BT_ACTIVITY Table
			 */

			sql = "UPDATE BT_ACTIVITY SET OOT_STR_NO = ";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber());
			logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber()));
			sql = sql + ", OOT_STR_NAME =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName());
			logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName()));
			sql = sql + ", OOT_UNIT =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber());
			logger.debug("got Out Of Town Unit " + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber()));
			sql = sql + ", OOT_CITY=";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownCity());
			logger.debug("got Out Of Town City " + StringUtils.checkString(businessTaxActivity.getOutOfTownCity()));
			sql = sql + ", OOT_STATE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownState());
			logger.debug("got Out Of Town State " + StringUtils.checkString(businessTaxActivity.getOutOfTownState()));
			sql = sql + ", OOT_ZIP =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip());
			logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip()));
			sql = sql + ", OOT_ZIP4 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4());
			logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4()));
			sql = sql + ", CORPORATE_NAME =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getCorporateName());
			logger.debug("got corporate name " + StringUtils.checkString(businessTaxActivity.getCorporateName()));
			sql = sql + ", BUSINESS_PHONE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessPhone());
			logger.debug("got Business Phone " + StringUtils.checkString(businessTaxActivity.getBusinessPhone()));
			sql = sql + ", BUSINESS_PHONE_EXT =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessExtension());
			logger.debug("got Business Extension " + StringUtils.checkString(businessTaxActivity.getBusinessExtension()));
			sql = sql + ", BUSINESS_FAX =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessFax());
			logger.debug("got Business Fax " + StringUtils.checkString(businessTaxActivity.getBusinessFax()));
			sql = sql + ", HOME_OCCUPATION =";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation()));
			logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation())));
			sql = sql + ", DECAL_CODE =";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode()));
			logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode())));
			sql = sql + ", OTHER_BUSINESS_OCCUPANCY =";
			sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy()));
			logger.debug("got Other occupancy  " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy())));
			sql = sql + ",OOB_DT =";
			sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate()));
			logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate())));
			sql = sql + ", FEDEREAL_ID =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getFederalIdNumber());
			logger.debug("got Federal Id Number " + StringUtils.checkString(businessTaxActivity.getFederalIdNumber()));
			sql = sql + ", SSN =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber());
			logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber()));
			sql = sql + ", DESC_OF_BUSINESS =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getDescOfBusiness());
			logger.debug("got Description Of Business " + StringUtils.checkString(businessTaxActivity.getDescOfBusiness()));
			sql = sql + ", OWN_TYPE_ID =";
			sql = sql + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "null");
			logger.debug("got Ownership Type " + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "-1"));
			sql = sql + ", EMAIL =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getEmailAddress());
			logger.debug("got Email " + StringUtils.checkString(businessTaxActivity.getEmailAddress()));

			sql = sql + ", MAIL_STR_NO =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetNumber());
			logger.debug("got Mail Street Number: " + StringUtils.checkString(businessTaxActivity.getMailStreetNumber()));
			sql = sql + ", MAIL_STR_NAME =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetName());
			logger.debug("got Mail Street Name: " + StringUtils.checkString((businessTaxActivity.getMailStreetName())));
			sql = sql + ", MAIL_UNIT =";
			sql = sql + StringUtils.checkString((businessTaxActivity.getMailUnitNumber()));
			logger.debug("got Mail Unit Number: " + StringUtils.checkString((businessTaxActivity.getMailUnitNumber())));
			sql = sql + ", MAIL_CITY =";
			sql = sql + StringUtils.checkString((businessTaxActivity.getMailCity()));
			logger.debug("got Mail City: " + StringUtils.checkString((businessTaxActivity.getMailCity())));
			sql = sql + ", MAIL_STATE =";
			sql = sql + StringUtils.checkString((businessTaxActivity.getMailState()));
			logger.debug("got Mail State: " + StringUtils.checkString((businessTaxActivity.getMailState())));
			sql = sql + ", MAIL_ZIP =";
			sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip()));
			logger.debug("got Mail Zip 2" + StringUtils.checkString((businessTaxActivity.getMailZip())));
			sql = sql + ", MAIL_ZIP4 =";
			sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip4()));
			logger.debug("got Mail Zip4 2" + StringUtils.checkString((businessTaxActivity.getMailZip4())));

			sql = sql + ", PRE_STR_NO =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber());
			logger.debug("got Previous Street Number " + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber()));
			sql = sql + ", PRE_STR_NAME =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetName());
			logger.debug("got Previous Street Name " + StringUtils.checkString(businessTaxActivity.getPrevStreetName()));
			sql = sql + ", PRE_UNIT =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber());
			logger.debug("got Previous Unit Number " + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber()));
			sql = sql + ", PRE_CITY =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevCity());
			logger.debug("got Previous City " + StringUtils.checkString(businessTaxActivity.getPrevCity()));
			sql = sql + ", PRE_STATE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevState());
			logger.debug("got Previous State " + StringUtils.checkString(businessTaxActivity.getPrevState()));
			sql = sql + ", PRE_ZIP =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip());
			logger.debug("got Previous Zip " + StringUtils.checkString(businessTaxActivity.getPrevZip()));
			sql = sql + ", PRE_ZIP4 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip4());
			logger.debug("got Previous Zip4 " + StringUtils.checkString(businessTaxActivity.getPrevZip4()));
			sql = sql + ", NAME1 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getName1());
			logger.debug("got Name1 " + StringUtils.checkString(businessTaxActivity.getName1()));
			sql = sql + ", TITLE1 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getTitle1());
			logger.debug("got Title " + StringUtils.checkString(businessTaxActivity.getTitle1()));
			sql = sql + ", OWNER1_STR_NO =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber());
			logger.debug("got Owner1 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber()));
			sql = sql + ", OWNER1_STR_NAME =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetName());
			logger.debug("got Owner1 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner1StreetName()));
			sql = sql + ", OWNER1_UNIT =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber());
			logger.debug("got Owner1 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber()));
			sql = sql + ", OWNER1_CITY=";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1City());
			logger.debug("got Owner1 City " + StringUtils.checkString(businessTaxActivity.getOwner1City()));
			sql = sql + ", OWNER1_STATE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1State());
			logger.debug("got Owner1 State " + StringUtils.checkString(businessTaxActivity.getOwner1State()));
			sql = sql + ", OWNER1_ZIP =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip());
			logger.debug("got Owner1 Zip " + StringUtils.checkString(businessTaxActivity.getOwner1Zip()));
			sql = sql + ", OWNER1_ZIP4 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip4());
			logger.debug("got Owner1 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner1Zip4()));
			sql = sql + ", NAME2 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getName2());
			logger.debug("got Name2 " + StringUtils.checkString(businessTaxActivity.getName2()));
			sql = sql + ", TITLE2 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getTitle2());
			logger.debug("got Title " + StringUtils.checkString(businessTaxActivity.getTitle2()));
			sql = sql + ", OWNER2_STR_NO =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber());
			logger.debug("got Owner2 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber()));
			sql = sql + ", OWNER2_STR_NAME =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetName());
			logger.debug("got Owner2 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner2StreetName()));
			sql = sql + ", OWNER2_UNIT =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber());
			logger.debug("got Owner2 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber()));
			sql = sql + ", OWNER2_CITY=";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2City());
			logger.debug("got Owner2 City " + StringUtils.checkString(businessTaxActivity.getOwner2City()));
			sql = sql + ", OWNER2_STATE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2State());
			logger.debug("got Owner2 State " + StringUtils.checkString(businessTaxActivity.getOwner2State()));
			sql = sql + ", OWNER2_ZIP =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip());
			logger.debug("got Owner2 Zip " + StringUtils.checkString(businessTaxActivity.getOwner2Zip()));
			sql = sql + ", OWNER2_ZIP4 =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip4());
			logger.debug("got Owner2 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner2Zip4()));
			sql = sql + ", SQ_FOOTAGE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getSquareFootage());
			logger.debug("got Square Footage " + StringUtils.checkString(businessTaxActivity.getSquareFootage()));
			sql = sql + ", QTY_ID =";
			sql = sql + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null");
			logger.debug("got Quantity " + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null"));
			sql = sql + ", QTY_OTHER =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getQuantityNum());
			logger.debug("got Quantity Number " + StringUtils.checkString(businessTaxActivity.getQuantityNum()));
			sql = sql + ", DRIVER_LICENSE =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getDriverLicense());
			logger.debug("got Driver's License " + StringUtils.checkString(businessTaxActivity.getDriverLicense()));
			sql = sql + ", ATTN =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getAttn());
			logger.debug("got ATTN " + StringUtils.checkString(businessTaxActivity.getAttn()));
			sql = sql + ", MAIL_ATTN =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getMailAttn());
			logger.debug("got MAIL_ATTN " + StringUtils.checkString(businessTaxActivity.getMailAttn()));
			sql = sql + ", PRE_ATTN =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getPreAttn());
			logger.debug("got PRE_ATTN " + StringUtils.checkString(businessTaxActivity.getPreAttn()));
			sql = sql + ", OWNER1_ATTN =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Attn());
			logger.debug("got OWNER1_ATTN " + StringUtils.checkString(businessTaxActivity.getOwner1Attn()));
			sql = sql + ", OWNER2_ATTN =";
			sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Attn());
			logger.debug("got OWNER2_ATTN " + StringUtils.checkString(businessTaxActivity.getOwner2Attn()));
			sql = sql + ", UPDATED =  current_timestamp, ";
			logger.debug("got Updated on " + "current_timestamp");
			sql = sql + "UPDATED_BY = ";
			sql = sql + businessTaxActivity.getUpdatedBy();
			logger.debug("got Updated By " + businessTaxActivity.getUpdatedBy());
			sql = sql + " WHERE ACT_ID =";
			sql = sql + activityId;
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
		}
		return activityId;
	}

	// Update activity with BUSINESS NAME
	public int updateBusinessTaxActivityWithBusinessName(BusinessTaxActivity businessTaxActivity, int activityId, String bName, int lsoId) {
		logger.debug("In updateBusinessTaxActivity With Business Name " + businessTaxActivity);
		
		try {
			String description = "";
			String sql_numberOfActivities = "";
			int pTypeId = 0;
			int projectId = 0;
			int subprojectId = 0;
			sql_numberOfActivities = "select count(*) from BT_ACTIVITY where UPPER(BUSINESS_NAME)=";
			sql_numberOfActivities = sql_numberOfActivities + StringUtils.checkString(bName.toUpperCase());
			/*if (businessTaxActivity.getMailStreetNumber() == null || (businessTaxActivity.getMailStreetNumber().equals("")) || (businessTaxActivity.getMailStreetNumber().equals(null))) {
				sql_numberOfActivities = sql_numberOfActivities + " and MAIL_STR_NO=";
				sql_numberOfActivities = sql_numberOfActivities + StringUtils.checkString(businessTaxActivity.getBusinessAddressStreetNumber().trim());
			} else {
				sql_numberOfActivities = sql_numberOfActivities + " and MAIL_STR_NO=";
				sql_numberOfActivities = sql_numberOfActivities + StringUtils.checkString(businessTaxActivity.getMailStreetNumber().trim());
			}
			if (businessTaxActivity.getMailStreetName() == null || (businessTaxActivity.getMailStreetName().equals("")) || (businessTaxActivity.getMailStreetName().equals(null))) {
				sql_numberOfActivities = sql_numberOfActivities + " and MAIL_STR_NAME=";
				sql_numberOfActivities = sql_numberOfActivities + StringUtils.checkString(businessTaxActivity.getBusinessAddressStreetName().trim());
			} else {
				sql_numberOfActivities = sql_numberOfActivities + " and MAIL_STR_NAME=";
				sql_numberOfActivities = sql_numberOfActivities + StringUtils.checkString(businessTaxActivity.getMailStreetName().trim());
			}*/
			logger.debug("sql_numberOfActivities  :"+sql_numberOfActivities);
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql_numberOfActivities);

			while (rs.next()) {
				int count = rs.getInt(1);
				logger.debug("count *************** " + count);

				/**
				 * Inserting records into LKUP_PTYPE Table
				 */
				String subProjectName = (Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + businessTaxActivity.getBusinessName().trim()).toUpperCase();
				logger.debug("Generated subProjectName " + subProjectName);
				pTypeId = checklkupSubProjectName(subProjectName);
				logger.debug("pTypeId Returned is  " + pTypeId);
				String lkupPtypeSql = "";
				if (pTypeId == 0) {
					pTypeId = db.getNextId("PTYPE_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					lkupPtypeSql = "insert into lkup_ptype(PTYPE_ID, DESCRIPTION, DEPT_CODE, NAME) values (";
					lkupPtypeSql = lkupPtypeSql + pTypeId;
					logger.debug("got pTypeId is " + pTypeId);
					lkupPtypeSql = lkupPtypeSql + ",";
					lkupPtypeSql = lkupPtypeSql + StringUtils.checkString(("BT - " + businessTaxActivity.getBusinessName().trim().toUpperCase()));
					logger.debug("got Description " + StringUtils.checkString(("BT - " + businessTaxActivity.getBusinessName().trim().toUpperCase())));
					lkupPtypeSql = lkupPtypeSql + ",'";
					lkupPtypeSql = lkupPtypeSql + "LC";
					logger.debug("got Department Code " + "LC");
					lkupPtypeSql = lkupPtypeSql + "',";
					lkupPtypeSql = lkupPtypeSql + "null";
					logger.debug("got Name " + "null");
					lkupPtypeSql = lkupPtypeSql + ")";
					logger.info(lkupPtypeSql);
					db.insert(lkupPtypeSql);
				}

				if (pTypeId != 0) {

					description = getlkupSubProjectName(subProjectName.trim());
					description = description.trim().substring(4);
					logger.debug("description is  " + description);
					businessTaxActivity.setBusinessName(description.trim().toUpperCase());
				}
				/**
				 * Sub Project Table
				 */
				String projName = Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES;
				projectId = checkProjectName(projName, lsoId);
				logger.debug("Project Id Returned is  " + projectId);

				int sprojNum = -1;
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat formatter = new SimpleDateFormat("yy");
				String subProjectSql = "";
				// subprojectId = checkSubProjectName(pTypeId, projectId);
				subprojectId = 0;
				logger.debug("Sub Project Id Returned is  " + subprojectId);

				if (subprojectId == 0) {
					sprojNum = db.getNextId("SP_NUM");
					logger.debug("New sprojNum " + sprojNum);
					/**
					 * Sub Project Number Generation
					 */
					String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);

					/**
					 * Inserting records into SUB_PROJECT Table
					 */
					subprojectId = db.getNextId("SUB_PROJECT_ID");
					logger.debug("The ID generated for new Sub Project is " + subprojectId);
					subProjectSql = "insert into sub_project(SPROJ_ID, PROJ_ID, SPROJ_TYPE, CREATED_BY, CREATED, STATUS, SPROJ_NBR) values (";
					subProjectSql = subProjectSql + subprojectId;
					logger.debug("got Subproject Id " + subprojectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + projectId;
					logger.debug("got Project id " + projectId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + pTypeId;
					logger.debug("got Sub Project Type " + pTypeId);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + businessTaxActivity.getCreatedBy();
					logger.debug("got Created By " + businessTaxActivity.getCreatedBy());
					subProjectSql = subProjectSql + ", current_timestamp,";
					logger.debug("got Created ");
					subProjectSql = subProjectSql + 1;
					logger.debug("got Status " + 1);
					subProjectSql = subProjectSql + ",";
					subProjectSql = subProjectSql + StringUtils.checkString(subProjectNumber);
					logger.debug("got Sub Project Number " + StringUtils.checkString(subProjectNumber));
					subProjectSql = subProjectSql + ")";
					logger.info(subProjectSql);
					db.insert(subProjectSql);
				}

				/**
				 * Updating records in ACTIVITY Table
				 */

				String sql_updateActivity = "";
				sql_updateActivity = "UPDATE activity SET STATUS=";
				sql_updateActivity = sql_updateActivity + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null");
				logger.debug("got activity status " + ((businessTaxActivity.getActivityStatus() != null) ? StringUtils.i2s(businessTaxActivity.getActivityStatus().getStatus()) : "null"));
				sql_updateActivity = sql_updateActivity + ",UPDATED_BY =";
				sql_updateActivity = sql_updateActivity + businessTaxActivity.getUpdatedBy();
				logger.debug("got Updated By " + businessTaxActivity.getUpdatedBy());
				sql_updateActivity = sql_updateActivity + ",UPDATED =";
				sql_updateActivity = sql_updateActivity + "current_timestamp";
				logger.debug("got Updated on " + "current_timestamp");
				sql_updateActivity = sql_updateActivity + ", START_DATE =";
				sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate()));
				logger.debug("got Starting Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getStartingDate())));
				sql_updateActivity = sql_updateActivity + ", APPLIED_DATE =";
				sql_updateActivity = sql_updateActivity + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate()));
				logger.debug("got Application Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getApplicationDate())));
				sql_updateActivity = sql_updateActivity + ", ISSUED_DATE =";
				sql_updateActivity = sql_updateActivity + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null);
				logger.debug("got Issue Date " + ((businessTaxActivity.getIssueDate() != null) ? StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getIssueDate())) : null));
				sql_updateActivity = sql_updateActivity + ", SPROJ_ID =";
				sql_updateActivity = sql_updateActivity + subprojectId;
				logger.debug("got subprojectId " + subprojectId);
				sql_updateActivity = sql_updateActivity + " WHERE ACT_ID =";
				sql_updateActivity = sql_updateActivity + activityId;
				logger.info(sql_updateActivity);
				db.update(sql_updateActivity);

				/**
				 * Updating records in BT_ACTIVITY Table
				 */
				String sql = "";
				sql = "UPDATE BT_ACTIVITY SET OOT_STR_NO = ";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber());
				logger.debug("got Out Of Town Street Number " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetNumber()));
				sql = sql + ", OOT_STR_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName());
				logger.debug("got Out Of Town Street Name " + StringUtils.checkString(businessTaxActivity.getOutOfTownStreetName()));
				sql = sql + ", OOT_UNIT =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber());
				logger.debug("got Out Of Town Unit " + StringUtils.checkString(businessTaxActivity.getOutOfTownUnitNumber()));
				sql = sql + ", OOT_CITY=";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownCity());
				logger.debug("got Out Of Town City " + StringUtils.checkString(businessTaxActivity.getOutOfTownCity()));
				sql = sql + ", OOT_STATE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownState());
				logger.debug("got Out Of Town State " + StringUtils.checkString(businessTaxActivity.getOutOfTownState()));
				sql = sql + ", OOT_ZIP =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip());
				logger.debug("got Out Of Town Zip " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip()));
				sql = sql + ", OOT_ZIP4 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4());
				logger.debug("got Out Of Town Zip4 " + StringUtils.checkString(businessTaxActivity.getOutOfTownZip4()));
				sql = sql + ", CORPORATE_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getCorporateName());
				logger.debug("got corporate name " + StringUtils.checkString(businessTaxActivity.getCorporateName()));
				sql = sql + ", BUSINESS_PHONE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessPhone());
				logger.debug("got Business Phone " + StringUtils.checkString(businessTaxActivity.getBusinessPhone()));
				sql = sql + ", BUSINESS_PHONE_EXT =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessExtension());
				logger.debug("got Business Extension " + StringUtils.checkString(businessTaxActivity.getBusinessExtension()));
				sql = sql + ", BUSINESS_FAX =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessFax());
				logger.debug("got Business Fax " + StringUtils.checkString(businessTaxActivity.getBusinessFax()));
				sql = sql + ", HOME_OCCUPATION =";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation()));
				logger.debug("got Home Occupation " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isHomeOccupation())));
				sql = sql + ", DECAL_CODE =";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode()));
				logger.debug("got Decal Code " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isDecalCode())));
				sql = sql + ", OTHER_BUSINESS_OCCUPANCY =";
				sql = sql + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy()));
				logger.debug("got Other occupancy  " + StringUtils.checkString(StringUtils.b2s(businessTaxActivity.isOtherBusinessOccupancy())));
				sql = sql + ",OOB_DT =";
				sql = sql + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate()));
				logger.debug("got Out Of Business Date " + StringUtils.toOracleDate(StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate())));
				sql = sql + ", FEDEREAL_ID =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getFederalIdNumber());
				logger.debug("got Federal Id Number " + StringUtils.checkString(businessTaxActivity.getFederalIdNumber()));
				sql = sql + ", SSN =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber());
				logger.debug("got SSN (Social Security Number) " + StringUtils.checkString(businessTaxActivity.getSocialSecurityNumber()));
				sql = sql + ", DESC_OF_BUSINESS =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getDescOfBusiness());
				logger.debug("got Description Of Business " + StringUtils.checkString(businessTaxActivity.getDescOfBusiness()));
				sql = sql + ", OWN_TYPE_ID =";
				sql = sql + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "null");
				logger.debug("got Ownership Type " + ((businessTaxActivity.getOwnershipType() != null) ? StringUtils.i2s(businessTaxActivity.getOwnershipType().getId()) : "-1"));
				sql = sql + ", EMAIL =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getEmailAddress());
				logger.debug("got Email " + StringUtils.checkString(businessTaxActivity.getEmailAddress()));

				sql = sql + ", MAIL_STR_NO =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetNumber());
				logger.debug("got Mail Street Number: " + StringUtils.checkString(businessTaxActivity.getMailStreetNumber()));
				sql = sql + ", MAIL_STR_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getMailStreetName());
				logger.debug("got Mail Street Name: " + StringUtils.checkString((businessTaxActivity.getMailStreetName())));
				sql = sql + ", MAIL_UNIT =";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailUnitNumber()));
				logger.debug("got Mail Unit Number: " + StringUtils.checkString((businessTaxActivity.getMailUnitNumber())));
				sql = sql + ", MAIL_CITY =";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailCity()));
				logger.debug("got Mail City: " + StringUtils.checkString((businessTaxActivity.getMailCity())));
				sql = sql + ", MAIL_STATE =";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailState()));
				logger.debug("got Mail State: " + StringUtils.checkString((businessTaxActivity.getMailState())));
				sql = sql + ", MAIL_ZIP =";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip()));
				logger.debug("got Mail Zip 2" + StringUtils.checkString((businessTaxActivity.getMailZip())));
				sql = sql + ", MAIL_ZIP4 =";
				sql = sql + StringUtils.checkString((businessTaxActivity.getMailZip4()));
				logger.debug("got Mail Zip4 2" + StringUtils.checkString((businessTaxActivity.getMailZip4())));

				sql = sql + ", PRE_STR_NO =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber());
				logger.debug("got Previous Street Number " + StringUtils.checkString(businessTaxActivity.getPrevStreetNumber()));
				sql = sql + ", PRE_STR_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevStreetName());
				logger.debug("got Previous Street Name " + StringUtils.checkString(businessTaxActivity.getPrevStreetName()));
				sql = sql + ", PRE_UNIT =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber());
				logger.debug("got Previous Unit Number " + StringUtils.checkString(businessTaxActivity.getPrevUnitNumber()));
				sql = sql + ", PRE_CITY =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevCity());
				logger.debug("got Previous City " + StringUtils.checkString(businessTaxActivity.getPrevCity()));
				sql = sql + ", PRE_STATE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevState());
				logger.debug("got Previous State " + StringUtils.checkString(businessTaxActivity.getPrevState()));
				sql = sql + ", PRE_ZIP =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip());
				logger.debug("got Previous Zip " + StringUtils.checkString(businessTaxActivity.getPrevZip()));
				sql = sql + ", PRE_ZIP4 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getPrevZip4());
				logger.debug("got Previous Zip4 " + StringUtils.checkString(businessTaxActivity.getPrevZip4()));
				sql = sql + ", NAME1 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getName1());
				logger.debug("got Name1 " + StringUtils.checkString(businessTaxActivity.getName1()));
				sql = sql + ", TITLE1 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getTitle1());
				logger.debug("got Title " + StringUtils.checkString(businessTaxActivity.getTitle1()));
				sql = sql + ", OWNER1_STR_NO =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber());
				logger.debug("got Owner1 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner1StreetNumber()));
				sql = sql + ", OWNER1_STR_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1StreetName());
				logger.debug("got Owner1 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner1StreetName()));
				sql = sql + ", OWNER1_UNIT =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber());
				logger.debug("got Owner1 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner1UnitNumber()));
				sql = sql + ", OWNER1_CITY=";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1City());
				logger.debug("got Owner1 City " + StringUtils.checkString(businessTaxActivity.getOwner1City()));
				sql = sql + ", OWNER1_STATE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1State());
				logger.debug("got Owner1 State " + StringUtils.checkString(businessTaxActivity.getOwner1State()));
				sql = sql + ", OWNER1_ZIP =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip());
				logger.debug("got Owner1 Zip " + StringUtils.checkString(businessTaxActivity.getOwner1Zip()));
				sql = sql + ", OWNER1_ZIP4 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner1Zip4());
				logger.debug("got Owner1 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner1Zip4()));
				sql = sql + ", NAME2 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getName2());
				logger.debug("got Name2 " + StringUtils.checkString(businessTaxActivity.getName2()));
				sql = sql + ", TITLE2 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getTitle2());
				logger.debug("got Title " + StringUtils.checkString(businessTaxActivity.getTitle2()));
				sql = sql + ", OWNER2_STR_NO =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber());
				logger.debug("got Owner2 Street Number " + StringUtils.checkString(businessTaxActivity.getOwner2StreetNumber()));
				sql = sql + ", OWNER2_STR_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2StreetName());
				logger.debug("got Owner2 Street Name " + StringUtils.checkString(businessTaxActivity.getOwner2StreetName()));
				sql = sql + ", OWNER2_UNIT =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber());
				logger.debug("got Owner2 Unit Number " + StringUtils.checkString(businessTaxActivity.getOwner2UnitNumber()));
				sql = sql + ", OWNER2_CITY=";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2City());
				logger.debug("got Owner2 City " + StringUtils.checkString(businessTaxActivity.getOwner2City()));
				sql = sql + ", OWNER2_STATE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2State());
				logger.debug("got Owner2 State " + StringUtils.checkString(businessTaxActivity.getOwner2State()));
				sql = sql + ", OWNER2_ZIP =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip());
				logger.debug("got Owner2 Zip " + StringUtils.checkString(businessTaxActivity.getOwner2Zip()));
				sql = sql + ", OWNER2_ZIP4 =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getOwner2Zip4());
				logger.debug("got Owner2 Zip4 " + StringUtils.checkString(businessTaxActivity.getOwner2Zip4()));
				sql = sql + ", SQ_FOOTAGE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getSquareFootage());
				logger.debug("got Square Footage " + StringUtils.checkString(businessTaxActivity.getSquareFootage()));
				sql = sql + ", QTY_ID =";
				sql = sql + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null");
				logger.debug("got Quantity " + ((businessTaxActivity.getQuantity() != null) ? StringUtils.i2s(businessTaxActivity.getQuantity().getId()) : "null"));
				sql = sql + ", QTY_OTHER =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getQuantityNum());
				logger.debug("got Quantity Number " + StringUtils.checkString(businessTaxActivity.getQuantityNum()));
				sql = sql + ", DRIVER_LICENSE =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getDriverLicense());
				logger.debug("got Driver's License " + StringUtils.checkString(businessTaxActivity.getDriverLicense()));
				sql = sql + ", ATTN =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getAttn());
				logger.debug("got ATTN " + StringUtils.checkString(businessTaxActivity.getAttn()));
				sql = sql + ", UPDATED =  current_timestamp, ";
				logger.debug("got Updated on " + "current_timestamp");
				sql = sql + "UPDATED_BY = ";
				sql = sql + businessTaxActivity.getUpdatedBy();
				logger.debug("got Updated By " + businessTaxActivity.getUpdatedBy());
				sql = sql + ", BUSINESS_NAME =";
				sql = sql + StringUtils.checkString(businessTaxActivity.getBusinessName().trim().toUpperCase());
				logger.debug("got BUSINESS_NAME " + StringUtils.checkString(businessTaxActivity.getBusinessName().trim().toUpperCase()));
				sql = sql + " WHERE ACT_ID =";
				sql = sql + activityId;
				logger.info(sql);
				db.update(sql);

				if (count <= 1) {
					logger.debug("SubProject contains only one activity");
					String oldSubProjectName = (Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + bName).toUpperCase();
					logger.debug("Generated oldSubProjectName " + oldSubProjectName);
					int oldPTypeId = checklkupSubProjectName(oldSubProjectName);
					logger.debug("oldPTypeId Returned is  " + oldPTypeId);

					subprojectId = checkSubProjectName(oldPTypeId, projectId);
					logger.debug("Sub Project Id Returned is  " + subprojectId);

					projectId = new ProjectAgent().deleteSubProject(subprojectId);
					logger.info("optained projectId back as " + projectId);
				}
			}

			rs.close();
		} catch (Exception e) {
		}
		return activityId;
	}

	/**
	 * Function to get a list of all approvals for an Activity. Used to create a list
	 * 
	 * @author Gayathri
	 * @return List of approvals
	 * @throws Exception
	 */
	public List getBusinessTaxApprovalList(int department_id) throws Exception {
		logger.info("getBusinessTaxApprovalList(" + department_id + ")");

		List businessTaxApprovalList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "SELECT * FROM V_BT_APPROVAL WHERE DEPT_ID=" + department_id + "  AND APPR_STATUS_ID NOT IN ('101','104','102') AND ACTIVITY_STATUS_ID NOT IN (1067) ORDER BY DEPARTMENT_NAME,BUSINESS_NAME";

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();

				businessTaxActivity.setActivityId(rs.getString("act_id"));
				businessTaxActivity.setActivityNumber(rs.getString("ACT_NBR"));
				businessTaxActivity.setBusinessName(rs.getString("business_name").trim().toUpperCase());
				businessTaxActivity.setBusinessAccountNumber(rs.getString("BUSINESS_ACC_NO"));
				businessTaxActivity.setActivityType(LookupAgent.getActivityType(rs.getString("act_type")));
				businessTaxActivity.setActivityStatusDescription(rs.getString("activity_status"));
				businessTaxActivity.setApprovalStatus(rs.getString("appr_status_name"));
				businessTaxActivity.setDescOfBusiness(rs.getString("desc_of_business"));
				String businessAddress = StringUtils.nullReplaceWithEmpty(rs.getString("DL_ADDRESS"));

				if ((businessAddress.trim().toUpperCase()).endsWith("CITYWIDE")) {
					// means it is out of town address
					sql = "SELECT OOT_STR_NO || ' ' || OOT_STR_NAME || ' ' || OOT_UNIT as OOT_ADDRESS,OOT_CITY || ' ' || OOT_STATE || ' ' || OOT_ZIP || ' ' || OOT_ZIP4 AS OOT_CITYSTATEZIP FROM BT_ACTIVITY WHERE ACT_ID=" + rs.getString("act_id");
					logger.debug(sql);
					RowSet ootRs = new Wrapper().select(sql);
					if (ootRs.next()) {
						businessAddress = ootRs.getString(1) + " " + ootRs.getString(2);
						logger.debug("OOT Business Address is " + businessAddress);
					}
				}

				businessTaxActivity.setAddress(businessAddress);
				businessTaxActivity.setApplicationDate(StringUtils.dbDate2cal(rs.getString("referred_dt")));
				businessTaxActivity.setDepartmentId(rs.getInt("dept_id"));
				businessTaxActivity.setUserId(rs.getInt("approved_by"));
				businessTaxActivity.setDepartmentName(StringUtils.nullReplaceWithEmpty(rs.getString("department_name")));
				businessTaxActivity.setUserName(rs.getString("approved_by_name"));
				businessTaxActivity.setApprovalId(rs.getInt("approval_id"));
				businessTaxActivity.setApprovalStatus(rs.getString("APPR_STATUS_NAME"));
				logger.info("get Reffered date()" + businessTaxActivity.getApplicationDate());
				businessTaxApprovalList.add(businessTaxActivity);
			}
			rs.close();
			logger.debug("returning businessTaxApprovalList of size " + businessTaxApprovalList.size());
			return businessTaxApprovalList;
		} catch (Exception e) {
			logger.error("Exception occured in getBusinessTaxApprovalList method . Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * used only to display departmentName for the particular department ID --In /listBusinessLicenseApproval.jsp
	 * 
	 * @author Gayathri
	 * @param departmentId
	 * @return List
	 * @throws Exception
	 */

	public List getOnlyBTDepartmentNameList(int departmentId) throws Exception {
		logger.info("getOnlyBTDepartmentNameList()");

		List getOnlyBTDepartmentNameList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select distinct(department_name) from v_bt_approval where dept_id=" + departmentId;

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
				businessTaxActivity.setDepartmentName(rs.getString("department_name"));

				getOnlyBTDepartmentNameList.add(businessTaxActivity);
			}
			rs.close();
			logger.debug("returning getOnlyDepartmentNameList of size " + getOnlyBTDepartmentNameList.size());
			return getOnlyBTDepartmentNameList;
		} catch (Exception e) {
			logger.error("Exception occured in getOnlyDepartmentNameList method . Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to get a list of all approvals for an Activity. Used to create a list
	 * 
	 * @author Gayathri
	 * @return List
	 * @throws Exception
	 */
	public List getBusinessTaxApprovalDepartmentList() throws Exception {
		logger.info("getBusinessTaxApprovalDepartmentList()");

		List businessTaxApprovalDepartmentList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select distinct(department_name),dept_id from v_bt_approval where appr_status_id Not In('101','104','102') order by department_name";

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
				businessTaxActivity.setDepartmentName(rs.getString("department_name"));
				businessTaxActivity.setDepartmentId(rs.getInt("dept_id"));
				businessTaxActivity.setBusinessLicenseApprovals(getBusinessTaxApprovalList(rs.getInt("dept_id")));
				businessTaxApprovalDepartmentList.add(businessTaxActivity);
			}
			rs.close();
			logger.debug("returning businessTaxApprovalDepartmentList of size " + businessTaxApprovalDepartmentList.size());
			return businessTaxApprovalDepartmentList;
		} catch (Exception e) {
			logger.error("Exception occured in businessTaxApprovalDepartmentList method . Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Plan checks
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public PlanCheck[] getPlanChecks(int activityId) throws Exception {
		logger.info("getPlanChecks(" + activityId + ")");

		List list = new ArrayList();
		PlanCheck planCheck;
		try {
			String date;
			Wrapper db = new Wrapper();

			String sql = "select PC.plan_chk_id, PC.plan_chk_date, PC.comnt, U.first_name || ' ' || U.last_name as name, S.pc_desc from plan_check PC left outer join users U on U.userid = PC.engineer, lkup_pc_status S where PC.act_id=" + activityId + " AND PC.stat_code = S.pc_code order by plan_chk_id desc,plan_chk_date desc";

			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				// create new planCheck objects and add them to list
				planCheck = new PlanCheck();
				planCheck.setPlanCheckId(rs.getInt("plan_chk_id"));
				planCheck.setStatusDesc(rs.getString("pc_desc"));

				date = rs.getString("plan_chk_date");
				logger.debug("date is " + date);

				Calendar c = StringUtils.dbDate2cal(date);
				logger.debug("calendar is " + c);
				date = StringUtils.cal2str(c);
				logger.debug("after date " + date);
				planCheck.setDate(date);
				planCheck.setPlanCheckDate(c);
				logger.debug("plancheck date form DB  ### " + c);

				planCheck.setEngineerName(rs.getString("name"));

				if ((rs.getString("name") == null) || (rs.getString("name").trim().equalsIgnoreCase(""))) {
					planCheck.setEngineerName("Unassigned");
				}

				if (rs.getString("comnt") == null) {
					planCheck.setComments("");
				} else {
					planCheck.setComments(rs.getString("comnt"));
				}

				logger.debug("befor add list");
				list.add(planCheck);
				logger.debug("after add list");
			}

			rs.close();
			logger.debug("returning getPlanChecks back to action  ..");

			PlanCheck[] tmpArray = new PlanCheck[list.size()];
			list.toArray(tmpArray);

			return tmpArray;
		} catch (Exception e) {
			logger.debug("Exception in getPlanChecks method in PlanCheckAgent " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Delete plan check
	 * 
	 * @param selectedPlanCheck
	 * @param psaId
	 * @throws Exception
	 */
	public void deletePlanCheck(String[] selectedPlanCheck, String psaId) throws Exception {
		logger.info("deletePlanCheck(" + selectedPlanCheck + ", " + psaId + ")");

		String sql = "";
		logger.debug("inside plan check delete");

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();
			logger.debug("inside try with plancheck list " + selectedPlanCheck);
			logger.debug("size of arrey is " + selectedPlanCheck.length);

			for (int i = 0; i < selectedPlanCheck.length; i++) {
				logger.debug("inside for");
				sql = "DELETE FROM PLAN_CHECK WHERE ACT_ID = " + psaId + " AND PLAN_CHK_ID =" + selectedPlanCheck[i];
				logger.debug("sql for delete plancheck " + sql);
				db.addBatch(sql);
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Plan Check
	 * 
	 * @param planCheckId
	 * @return
	 * @throws Exception
	 */
	public PlanCheck getPlanCheck(int planCheckId) throws Exception {
		logger.info("getPlanCheck(" + planCheckId + ")");
		PlanCheck planCheck;
		planCheck = new PlanCheck();

		try {
			String date;
			Wrapper db = new Wrapper();
			String sql = "select * from plan_check where plan_chk_id= " + planCheckId;

			logger.debug("The select Sql of plan Check  is " + sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				date = rs.getString("plan_chk_date");

				Calendar c = StringUtils.dbDate2cal(date);
				planCheck.setPlanCheckDate(c);
				date = StringUtils.cal2str(c);
				planCheck.setDate(date);
				planCheck.setActivityId(StringUtils.s2i(rs.getString("act_id")));
				planCheck.setStatusCode(StringUtils.s2i(rs.getString("stat_code")));
				planCheck.setEngineer(StringUtils.s2i(rs.getString("engineer")));
				planCheck.setComments((rs.getString("comnt") != null) ? rs.getString("comnt") : "");
				planCheck.setCategoryCode(StringUtils.s2i(rs.getString("cat_code")));
				planCheck.setTitleCode(StringUtils.s2i(rs.getString("title_code")));
				planCheck.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				planCheck.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
			}

			rs.close();
			logger.debug("returning getPlanCheck back to action  ..");

			return planCheck;
		} catch (Exception e) {
			logger.debug("Exception in getPlanCheck" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Engineer from project
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public String getEngFromProj(String projectId) throws Exception {
		ResultSet rs = null;

		try {
			Wrapper db = new Wrapper();

			String sql = "SELECT USERID FROM PROCESS_TEAM WHERE GROUP_ID = (SELECT GROUP_ID FROM GROUPS WHERE upper(GROUPS.NAME) = upper('Plan Review Engineer')) AND PSA_ID = " + projectId;
			logger.debug("inspector sql : " + sql);

			rs = db.select(sql);

			if (rs.next()) {
				logger.debug("returning inspectorid: " + rs.getString("USERID"));

				return rs.getString("USERID");
			}

			if (rs != null) {
				rs.close();
			}

			logger.debug("returning inspectorid: -1");

			return "-1";
		} catch (Exception e) {
			logger.debug("Exception occured in getUserForActivity method of InspectionAgent. Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get latest plan check
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public PlanCheck getLatestPlanCheck(int activityId) throws Exception {
		PlanCheck planCheck = new PlanCheck();

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from plan_check where act_Id= " + activityId + " order by plan_chk_id desc,plan_chk_date desc";

			logger.debug(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				planCheck.setPlanCheckId(rs.getInt("PLAN_CHK_ID"));
				planCheck.setPlanCheckDate(rs.getDate("plan_chk_date"));
				logger.debug("getLatestPlanCheck() .. Plan Check date is set to  .. " + StringUtils.cal2str(planCheck.getPlanCheckDate()));
				planCheck.setDate(StringUtils.date2str(rs.getDate("plan_chk_date")));
				logger.debug("getLatestPlanCheck() ..Plan Check String date .. " + planCheck.getDate());
				planCheck.setActivityId(StringUtils.s2i(rs.getString("act_id")));
				logger.debug("getLatestPlanCheck() ..Plan Check activity Id .. " + planCheck.getActivityId());
				planCheck.setStatusCode(StringUtils.s2i(rs.getString("stat_code")));
				logger.debug("getLatestPlanCheck() ..Plan Check Status .. " + planCheck.getStatusCode());

				PlanCheckStatus planCheckStatus = LookupAgent.getPlanCheckStatus(StringUtils.s2i(rs.getString("stat_code")));

				if (planCheckStatus == null) {
					planCheckStatus = new PlanCheckStatus();
				}

				planCheck.setStatusDesc(planCheckStatus.getDescription());
				logger.debug("getLatestPlanCheck() ..Plan Check Status Description is .. " + planCheck.getStatusDesc());
				planCheck.setEngineer(StringUtils.s2i(rs.getString("engineer")));
				logger.debug("getLatestPlanCheck() ..Plan Check Enginner ..  " + planCheck.getEngineer());
				planCheck.setComments(rs.getString("comnt"));
				logger.debug("getLatestPlanCheck() ..Plan Check comnt  .. " + planCheck.getComments());
				planCheck.setCategoryCode(StringUtils.s2i(rs.getString("cat_code")));
				logger.debug("getLatestPlanCheck() ..Plan Check CategoryCode .. " + planCheck.getCategoryCode());
				planCheck.setTitleCode(StringUtils.s2i(rs.getString("title_code")));
				logger.debug("getLatestPlanCheck() .. Plan Check Title code .. " + planCheck.getTitleCode());
				planCheck.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				logger.debug("getLatestPlanCheck() .. Plan Check Created by .. " + planCheck.getCreatedBy());
				planCheck.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				logger.debug("getLatestPlanCheck() .. Plan Check Updated By " + planCheck.getUpdatedBy());
			}

			if (rs != null) {
				rs.close();
			}

			logger.debug("returning PlanCheck  ..");

			return planCheck;
		} catch (Exception e) {
			logger.error("Exception in getLatestPlanCheck method in PlanCheckAgent " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Edit plan check
	 * 
	 * @param planCheck
	 * @return
	 * @throws Exception
	 */
	public int editPlanCheck(PlanCheck planCheck) throws Exception {
		try {
			int activityId = -1;
			Wrapper db = new Wrapper();
			String sql = "";
			db.beginTransaction();

			int pcStatusCode = planCheck.getStatusCode();
			String projectName = LookupAgent.getProjectNameForActivityId("" + activityId);

			sql = "UPDATE PLAN_CHECK SET PLAN_CHK_DATE = to_date(" + StringUtils.checkString(StringUtils.cal2str(planCheck.getPlanCheckDate())) + ",'MM/DD/YYYY') , ENGINEER = " + planCheck.getEngineer() + " , STAT_CODE = " + planCheck.getStatusCode() + " , COMNT = '" + planCheck.getComments() + "', UPDATED_BY = " + planCheck.getUpdatedBy().getUserId() + " , UPDAETD = current_timestamp where plan_chk_id = " + planCheck.getPlanCheckId();
			logger.debug(sql);

			db.addBatch(sql);

			// Activity status is getting changed according to plan check status
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_BUILDING)) {
				if (pcStatusCode > 0) {
					sql = "update activity set status = (select act_status_code from lkup_pc_status where pc_code=" + pcStatusCode + ")  where act_id=" + activityId;
					logger.debug(sql);
					db.addBatch(sql);
				}
			}

			db.executeBatch();

			return planCheck.getPlanCheckId();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Add Plan check
	 * 
	 * @param planCheck
	 * @return
	 * @throws Exception
	 */
	public int addPlanCheck(PlanCheck planCheck) throws Exception {
		logger.info("addPlanCheck(planCheck)");
		int activityId = -1;
		int planCheckId;

		try {
			// add the PlanCheck to the database
			Wrapper db = new Wrapper();
			String sql = "";
			activityId = planCheck.getActivityId();
			int pcStatusCode = planCheck.getStatusCode();
			String projectName = LookupAgent.getProjectNameForActivityId("" + activityId);

			planCheckId = db.getNextId("PLANCHECK_ID");
			db.beginTransaction();
			sql = "Insert into plan_check(plan_chk_id,plan_chk_date,act_id,engineer, stat_code,cat_code,title_code,comnt,created_by,created,updated_by,updaetd) " + " values(" + planCheckId + ",to_date('" + (StringUtils.cal2str(planCheck.getPlanCheckDate())) + "','MM/DD/YYYY')," + activityId + "," + planCheck.getEngineer() + "," + pcStatusCode + "," + planCheck.getCategoryCode() + "," + planCheck.getTitleCode() + "," + StringUtils.checkString(planCheck.getComments()) + "," + planCheck.getCreatedBy().getUserId() + "," + "current_timestamp" + "," + planCheck.getUpdatedBy().getUserId() + "," + "current_timestamp)";
			logger.debug(sql);
			db.addBatch(sql);

			// Activity status is getting changed according to plan check status
			if (projectName.equalsIgnoreCase(Constants.PROJECT_NAME_BUILDING)) {
				if (pcStatusCode > 0) {
					sql = "update activity set status = (select act_status_code from lkup_pc_status where pc_code=" + pcStatusCode + ")  where act_id=" + activityId;
					logger.debug(sql);
					db.addBatch(sql);
				}
			}

			logger.debug("addPlanCheck() -- update activity Status .. " + sql);
			db.executeBatch();

			return planCheckId;
		} catch (Exception e) {
			planCheckId = -1;
			logger.error("Exception When Adding Plan Check....." + e.getMessage());
			throw e;
		}
	}

	/**
	 * Save Plan check
	 * 
	 * @param planCheck
	 * @return
	 * @throws Exception
	 */
	public int savePlanCheck(PlanCheck planCheck) throws Exception {
		int activityId = -1;
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			db.beginTransaction();
			sql = "update plan_check set plan_check_date= to_date('" + StringUtils.checkString(StringUtils.cal2str(planCheck.getPlanCheckDate())) + ",'MM/DD/YYYY')," + "stat_code=" + planCheck.getStatusCode() + ",cat_code=" + planCheck.getCategoryCode() + ",title_code=" + planCheck.getTitleCode() + "," + "comnt=" + StringUtils.checkString(planCheck.getComments()) + "," + "update_by=" + planCheck.getUpdatedBy().getUserId() + "," + "updated=" + "current_timestamp" + "," + " where plan_chk_id=" + planCheck.getPlanCheckId();
			db.addBatch(sql);
			logger.debug("save Plan Check Sql in savePlanCheck Method is " + sql);

			// Activity status is getting changed according to plan check status
			if (planCheck.getStatusCode() > 0) {
				sql = "update activity set status = (select act_status_code from lkup_pc_status where pc_code=" + planCheck.getStatusCode() + ")  where act_id=" + activityId;
				db.addBatch(sql);
			}

			logger.debug("savePlanCheck() -- update activity Status .. " + sql);
			db.executeBatch();

			return planCheck.getPlanCheckId();
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update PlanCheck  :" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get engineers
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getEngineerUsers(int deptId) throws Exception {
		logger.info("getEngineerUsers()");

		List engineerUsers = new ArrayList();
		EngineerUser engineerUser = null;

		try {
			// 19 - Plan Review Engineer
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT U.USERID,U.FIRST_NAME,U.LAST_NAME");
			sql.append(" FROM USERS U JOIN USER_GROUPS UG ON U.USERID = UG.USER_ID");
			sql.append(" WHERE UG.GROUP_ID IN (19) and active='Y'");
			if (deptId != -1) {
				sql.append(" AND U.DEPT_ID=" + deptId);
			}
			sql.append(" ORDER BY U.FIRST_NAME");

			logger.debug("inspection users ########### " + sql);
			engineerUser = new EngineerUser(0, "Unassigned");
			engineerUsers.add(engineerUser);

			ResultSet rs = new Wrapper().select(sql.toString());

			while (rs.next()) {
				engineerUser = new EngineerUser(rs.getInt("USERID"), rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
				engineerUsers.add(engineerUser);
			}

			if (rs != null) {
				rs.close();
			}

			return engineerUsers;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	// get the dafault Plan check Engineer From Project Level Process team ( Engineer , Supervisor or Inspector ) of one plan check
	public int getPlanReviewEngineer(String activityId) throws Exception {
		int userId = 0;

		try {
			Wrapper db = new Wrapper();
			String sql = "select userid  from process_team where psa_type='P' and group_id = 19 and psa_id = (select proj_id from v_psa_list where act_id=" + activityId + ") order by created";
			logger.debug("The select Sql of plan Check  is " + sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				userId = rs.getInt("userId");
			}

			rs.close();

			return userId;
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update PlanCheck  :" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Planning details
	 * 
	 * @param psaType
	 * @param psaId
	 * @return
	 */
	public static PlanningDetails getPlanningDetails(String psaType, int psaId) {
		logger.info("getPlanningDetails(" + psaType + ", " + psaId + ")");

		PlanningDetails planningDetails = new PlanningDetails();
		planningDetails.setEnvReviewList(getEnvReviewList(psaType, psaId));
		planningDetails.setResolutionList(getResolutionList(psaType, psaId));
		planningDetails.setStatusList(getStatusList(psaType, psaId));
		planningDetails.setLinkedSubProjects(ProjectAgent.getLinkedSubProjects(psaId));
		logger.debug("Exiting getPlanningDetails()");

		return planningDetails;
	}

	/**
	 * Get the planner update records for user id and username.
	 * 
	 * @param fromUserId
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public Planner getPlannerUpdate(String fromUserId, String userName) throws Exception {
		logger.info("getPlannerUpdate(" + fromUserId + ", " + userName + ")");

		List plannerUpdateList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "SELECT distinct sp.sproj_nbr,lp.description as sub_project_name,sp.sproj_id,lst.description as sub_project_type,lss.description as sub_project_status,sp.description as sub_project_description,las.active as active,V.ADDRESS,a.applied_date,a.act_nbr,a.act_id FROM ACTIVITY A JOIN SUB_PROJECT SP ON SP.SPROJ_ID=A.SPROJ_ID JOIN PROJECT P ON P.PROJ_ID=SP.PROJ_ID JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE=A.ACT_TYPE JOIN LKUP_ACT_ST LAS ON LAS.STATUS_ID=A.STATUS LEFT OUTER JOIN V_ACTIVITY_ADDRESS V ON V.ACT_ID=A.ACT_ID join lkup_sproj_type lst on lst.sproj_type_id = sp.stype_id join lkup_sproj_st lss on lss.status_id=sp.status join lkup_ptype lp on lp.ptype_id = sp.sproj_type WHERE  A.ACT_ID IN (SELECT PSA_ID AS ACT_ID FROM PROCESS_TEAM WHERE PSA_TYPE = 'A' AND USERID =" + fromUserId + ") and A.ACT_NBR LIKE 'PL%' and sp.caselog_id!=4 order by sp.sproj_nbr desc";
			logger.info(sql);

			RowSet rs = db.select(sql);
			Planner planner = new Planner();
			PlannerUpdateRecord plannerUpdateRecord;

			for (; rs.next(); plannerUpdateList.add(plannerUpdateRecord)) {
				plannerUpdateRecord = new PlannerUpdateRecord(rs.getString("sproj_nbr"), rs.getString("act_nbr"), rs.getString("act_id"), rs.getString("sub_project_type"), rs.getString("sub_project_status"), rs.getString("sub_project_description"), rs.getString("ADDRESS"), StringUtils.changeDateFormate(rs.getString("applied_date")), "false", rs.getString("sub_project_name"), rs.getString("sproj_id"), fromUserId, rs.getString("active"));
			}

			List plannerUpdateListWithoutDuplicates = removeDuplicateSubProjects(plannerUpdateList);
			PlannerUpdateRecord[] plannerUpdateRecordArray = new PlannerUpdateRecord[plannerUpdateListWithoutDuplicates.size()];
			plannerUpdateListWithoutDuplicates.toArray(plannerUpdateRecordArray);
			planner.plannerUpdateRecord = plannerUpdateRecordArray;
			planner.setUserName(userName);

			return planner;
		} catch (Exception e) {
			logger.error("Exception in method getPlannerUpdate - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get the count for planner update user.
	 * 
	 * @param fromUserId
	 * @return
	 * @throws Exception
	 */
	public int getPlannerUpdateCount(String fromUserId) throws Exception {
		logger.info("getPlannerUpdateCount(" + fromUserId + ")");

		List plannerUpdateList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "SELECT distinct sp.sproj_nbr,lp.description as sub_project_name,sp.sproj_id,lst.description as sub_project_type,lss.description as sub_project_status,sp.description as sub_project_description,las.active as active,V.ADDRESS,a.applied_date,a.act_nbr,a.act_id FROM ACTIVITY A JOIN SUB_PROJECT SP ON SP.SPROJ_ID=A.SPROJ_ID JOIN PROJECT P ON P.PROJ_ID=SP.PROJ_ID JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE=A.ACT_TYPE JOIN LKUP_ACT_ST LAS ON LAS.STATUS_ID=A.STATUS LEFT OUTER JOIN V_ACTIVITY_ADDRESS V ON V.ACT_ID=A.ACT_ID join lkup_sproj_type lst on lst.sproj_type_id = sp.stype_id join lkup_sproj_st lss on lss.status_id=sp.status join lkup_ptype lp on lp.ptype_id = sp.sproj_type WHERE  A.ACT_ID IN (SELECT PSA_ID AS ACT_ID FROM PROCESS_TEAM WHERE PSA_TYPE = 'A' AND USERID =" + fromUserId + ") and A.ACT_NBR LIKE 'PL%' and sp.caselog_id!=4 order by sp.sproj_nbr desc";
			logger.info(sql);

			javax.sql.RowSet rs = db.select(sql);
			Planner planner = new Planner();
			PlannerUpdateRecord plannerUpdateRecord;

			for (; rs.next(); plannerUpdateList.add(plannerUpdateRecord)) {
				plannerUpdateRecord = new PlannerUpdateRecord(rs.getString("sproj_nbr"), rs.getString("act_nbr"), rs.getString("act_id"), rs.getString("sub_project_type"), rs.getString("sub_project_status"), rs.getString("sub_project_description"), rs.getString("ADDRESS"), StringUtils.changeDateFormate(rs.getString("applied_date")), "false", rs.getString("sub_project_name"), rs.getString("sproj_id"), fromUserId, rs.getString("active"));
			}

			List plannerUpdateListWithoutDuplicates = removeDuplicateSubProjects(plannerUpdateList);
			int count = plannerUpdateListWithoutDuplicates.size();

			return count;
		} catch (Exception e) {
			logger.error("Exception in method getPlannerUpdate - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Remove duplicate sub projects from the list
	 * 
	 * @param listWithDuplicates
	 * @return
	 * @throws Exception
	 */
	private List removeDuplicateSubProjects(List listWithDuplicates) throws Exception {
		List listWithoutDuplicates = new ArrayList();
		String previousSubProjectNumber = "";

		for (int i = 0; i < listWithDuplicates.size(); i++) {
			PlannerUpdateRecord currentPlannerUpdateRecord = (PlannerUpdateRecord) listWithDuplicates.get(i);
			String currentSubProjectNumber = currentPlannerUpdateRecord.getSubProjectNumber();
			logger.debug("currentSubProjectNumber=" + currentSubProjectNumber);

			if (currentSubProjectNumber.equalsIgnoreCase(previousSubProjectNumber)) {
				logger.debug("skipping record to the list");
			} else {
				listWithoutDuplicates.add(currentPlannerUpdateRecord);
			}

			previousSubProjectNumber = currentSubProjectNumber;
		}

		return listWithoutDuplicates;
	}

	/**
	 * Reassign planning activity to a different user.
	 * 
	 * @param userId
	 * @param activityNumber
	 * @param fromUserId
	 * @throws Exception
	 */
	public void reAssignPlanningActivity(String userId, String activityNumber, String fromUserId) throws Exception {
		logger.info("reAssignPlanningActivity(" + userId + "," + activityNumber + ")");

		String sql = "";
		Wrapper db = new Wrapper();

		try {
			sql = "update process_team set userid=" + userId + "  where psa_id in (select act_id from activity where act_nbr=" + StringUtils.checkString(activityNumber) + ") and psa_type='A' and userid = " + fromUserId + " ";
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the list of planners
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getPlanners() throws Exception {
		logger.info("getPlanners()");

		List planners = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT U.USERID,U.FIRST_NAME || ' ' || U.LAST_NAME AS NAME");
			sql.append(" FROM USERS U JOIN USER_GROUPS UG ON U.USERID = UG.USER_ID");
			sql.append(" WHERE UG.GROUP_ID IN (" + Constants.GROUPS_PLANNER + ")");
			sql.append(" ORDER BY U.FIRST_NAME || ' ' || U.LAST_NAME");
			logger.debug(sql);

			javax.sql.RowSet rs = db.select(sql.toString());
			ProcessTeamNameRecord rec = new ProcessTeamNameRecord();

			for (; rs.next(); planners.add(rec)) {
				rec = new ProcessTeamNameRecord();
				rec.setNameValue(rs.getString("USERID"));
				rec.setNameLabel(rs.getString("NAME"));
			}

			logger.debug("getPlanners returning a length of :" + planners.size());

			return planners;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Gets the calendar features list.
	 * 
	 * @param levelId
	 * @param numberOfRows
	 * @return
	 * @throws Exception
	 */
	public List getCalendarFeaturesList(int levelId, int numberOfRows) throws Exception {
		logger.info("getCalendarFeaturesList (" + levelId + ")");

		List calendarFeatures = new ArrayList();
		CalendarFeature calendarFeature = null;
		String meetingDate;

		try {
			Wrapper db = new Wrapper();

			String sql = "select sp.sp_calendar_id,sp.sproj_id,sp.meeting_date,mt.description as meeting_type,ay.description as action_type,sp.comments from sp_calendar sp join lkup_meeting_type mt on mt.meeting_type_id = sp.meeting_type join lkup_action_type ay on ay.action_type_id = sp.action_type where sproj_id = " + levelId;
			logger.debug(sql);
			sql += " order by meeting_date desc";

			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				calendarFeature = new CalendarFeature(rs.getInt("sp_calendar_id"), StringUtils.changeDateFormate(rs.getString("meeting_date")), rs.getString("meeting_type"), rs.getString("action_type"), rs.getString("comments"));
				calendarFeatures.add(calendarFeature);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return calendarFeatures;
	}

	/**
	 * Gets the calendar features for mini panel.
	 * 
	 * @param levelId
	 * @param numberOfRows
	 * @return
	 * @throws Exception
	 */
	public List getCalendarFeaturesListForMini(int levelId, int numberOfRows) throws Exception {
		logger.info("getCalendarFeaturesList (" + levelId + ")");

		List calendarFeatures = new ArrayList();
		CalendarFeature calendarFeature = null;
		String meetingDate;

		try {
			Wrapper db = new Wrapper();

			String sql = "select sp.sp_calendar_id,sp.sproj_id,sp.meeting_date,mt.description as meeting_type,ay.description as action_type,sp.comments from sp_calendar sp join lkup_meeting_type mt on mt.meeting_type_id = sp.meeting_type join lkup_action_type ay on ay.action_type_id = sp.action_type where sproj_id = " + levelId + " and meeting_date is not null order by meeting_date desc";
			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				calendarFeature = new CalendarFeature(rs.getInt("sp_calendar_id"), StringUtils.changeDateFormate(rs.getString("meeting_date")), rs.getString("meeting_type"), rs.getString("action_type"), rs.getString("comments"));
				calendarFeatures.add(calendarFeature);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return calendarFeatures;
	}

	/**
	 * Gets the calendar features array
	 * 
	 * @param levelId
	 * @param numberOfRows
	 * @return
	 * @throws Exception
	 */
	public CalendarFeature[] getAllCalendarFeaturesArray(int levelId, int numberOfRows) throws Exception {
		logger.info(" getAllCalendarFeaturesArray ( " + levelId + " ," + numberOfRows);

		List list1 = new ArrayList();

		try {
			list1 = getCalendarFeaturesList(levelId, numberOfRows);
		} catch (Exception e) {
			logger.debug("Error in getAllCalendarFeaturesArray: " + e.getMessage());
			throw e;
		}

		CalendarFeature[] calendarFeature = new CalendarFeature[list1.size()];
		list1.toArray(calendarFeature);

		return calendarFeature;
	}

	/**
	 * Make an entry to the calendar feature table
	 * 
	 * @param calendarFeature
	 * @return
	 * @throws Exception
	 */
	public int insertCalendarFeature(CalendarFeature calendarFeature) throws Exception {
		logger.info("insertCalendarFeature(calendarFeature)");

		try {
			Wrapper db = new Wrapper();

			// first i need to generate sp_calenderFeatureId . for that I should add column in nextId table
			int spCalendarFeatureId = db.getNextId("SP_CALENDAR_ID");
			String sql = "insert into sp_calendar(sp_calendar_id,sproj_id,meeting_date,meeting_type,action_type,comments) values (" + spCalendarFeatureId + "," + calendarFeature.getSubProjectId() + ",to_date('" + (calendarFeature.getMeetingDate()) + "','MM/DD/YYYY')," + StringUtils.checkString(calendarFeature.getMeetingType()) + "," + StringUtils.checkString(calendarFeature.getActionType()) + "," + StringUtils.checkString(calendarFeature.getComments()) + ")";
			logger.debug(sql);
			db.insert(sql);

			return spCalendarFeatureId;
		} catch (Exception e) {
			logger.error("Exception occured in insertCalendarFeature: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Remove a record from calendar feature.
	 * 
	 * @param selectedRecord
	 * @param subProjectId
	 * @throws Exception
	 */
	public void deleteCalendarFeature(String[] selectedRecord, int subProjectId) throws Exception {
		logger.info("deleteCalendarFeature (" + selectedRecord + ", " + subProjectId);

		try {
			String sql = "";
			Wrapper db = new Wrapper();
			db.beginTransaction();
			logger.debug("Inside try block selectedRecord : " + selectedRecord);
			logger.debug("size of array: " + selectedRecord.length);

			for (int i = 0; i < selectedRecord.length; i++) {
				sql = "delete from sp_calendar where sproj_id = " + subProjectId + "and sp_calendar_id = " + selectedRecord[i];
				logger.debug(sql);
				db.addBatch(sql);
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error("Exception occured in deleteCalendarFeature: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the meeting type list.
	 * 
	 * @return
	 * @throws Exception
	 */
	public List getMeetingTypeList() throws Exception {
		logger.info("getMeetingTypeList()");

		List list1 = new ArrayList();
		PlanCheckStatus planCheckStatus = null;

		try {
			planCheckStatus = new PlanCheckStatus(-1, "Please Select", -1);
			list1.add(planCheckStatus);

			Wrapper db = new Wrapper();
			String sql = "select * from lkup_meeting_type";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				planCheckStatus = new PlanCheckStatus(rs.getInt("meeting_type_id"), rs.getString("description"), rs.getInt("meeting_type_id"));
				list1.add(planCheckStatus);
			}

			return list1;
		} catch (Exception e) {
			logger.error("Exception occured in getMeetingtypeList: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the planning meeting action type list.
	 * 
	 * @param mid
	 * @return
	 * @throws Exception
	 */
	public List getActionTypeList(String mid) throws Exception {
		logger.info("getActionTypeList()");

		List actionTypes = new ArrayList();
		PlanCheckStatus planCheckStatus = null;

		try {
			planCheckStatus = new PlanCheckStatus(-1, "Please Select", -1);
			actionTypes.add(planCheckStatus);

			Wrapper db = new Wrapper();
			String sql = "select la.description,lma.action_type_id from lkup_action_type la join lkup_mtg_action lma on lma.action_type_id = la.action_type_id where lma.meeting_type_id in(" + mid + ")";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				planCheckStatus = new PlanCheckStatus(rs.getInt("action_type_id"), rs.getString("description"), rs.getInt("action_type_id"));
				actionTypes.add(planCheckStatus);
			}

			return actionTypes;
		} catch (Exception e) {
			logger.error("Exception occured in getActionTypeList : " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the calendar feature.
	 * 
	 * @param spCalendarFeatureId
	 * @return
	 * @throws Exception
	 */
	public CalendarFeature getCalendarFeature(int spCalendarFeatureId) throws Exception {
		logger.info("getCalendarFeature(" + spCalendarFeatureId + ")");

		CalendarFeature calendarFeature = new CalendarFeature();

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from sp_calendar where sp_calendar_id = " + spCalendarFeatureId;
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				calendarFeature.setCalendarFeatureId(rs.getInt("sp_calendar_id"));
				calendarFeature.setSubProjectId(rs.getInt("sproj_id"));
				calendarFeature.setMeetingType(rs.getString("meeting_type"));
				calendarFeature.setMeetingDate(StringUtils.changeDateFormate(rs.getString("meeting_date")));
				calendarFeature.setActionType(rs.getString("action_type"));
				calendarFeature.setComments(rs.getString("comments"));
			}

			rs.close();

			return calendarFeature;
		} catch (Exception e) {
			logger.error("Exception Occured in getCalendarFeature: " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Modify the calendar feature.
	 * 
	 * @param calendarFeature
	 * @return
	 * @throws Exception
	 */
	public int editCalendarFeature(CalendarFeature calendarFeature) throws Exception {
		logger.debug("editCalendarFeature(calendarFeature)");

		try {
			Wrapper db = new Wrapper();
			String sql = "update sp_calendar set meeting_date = to_date('" + (calendarFeature.getMeetingDate()) + "','MM/DD/YYYY'), meeting_type =" + StringUtils.checkString(calendarFeature.getMeetingType()) + ", action_type =" + StringUtils.checkString(calendarFeature.getActionType()) + ", comments =" + StringUtils.checkString(calendarFeature.getComments()) + " where sp_calendar_id =" + calendarFeature.getCalendarFeatureId();
			logger.debug(sql);
			db.update(sql);

			return calendarFeature.getCalendarFeatureId();
		} catch (Exception e) {
			logger.error("Exception Occured in editCalendarFeature: " + e.getMessage());
			throw e;
		}
	}

	public static List getEnvReviewList(String levelType, int levelId) {
		List envReviews = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select e.*,led.description as env_det_description,leda.description as env_action_description " + " from (env_review e left outer join lkup_env_det led on led.env_det_id=e.env_det_id) " + " left outer join lkup_env_det_action leda on leda.env_det_action_id=e.env_det_action_id where ";

			if (levelType.equals("A")) {
				sql += ("act_id = " + levelId);
			} else {
				sql += ("sproj_id =" + levelId);
			}

			sql += " order by env_review_id desc";
			logger.debug(sql);

			RowSet rs = db.select(sql);
			int i = 0;

			while (rs.next()) {
				EnvReviewEdit envReview = new EnvReviewEdit();
				envReview.setIndex(++i);
				envReview.setEnvReviewId(rs.getString("env_review_id"));
				envReview.setActionDate(StringUtils.date2str(rs.getDate("action_date")));
				envReview.setDeterminationDate(StringUtils.date2str(rs.getDate("determination_date")));
				envReview.setSubmitDate(StringUtils.date2str(rs.getDate("submit_date")));
				envReview.setReqDeterminationDate(StringUtils.date2str(rs.getDate("req_det_date")));
				envReview.setEnvDeterminationId(rs.getString("env_det_id"));
				envReview.setEnvDetermination(rs.getString("env_det_description"));
				envReview.setEnvDeterminationActionId(rs.getString("env_det_action_id"));
				envReview.setEnvDeterminationAction(rs.getString("env_action_description"));
				envReview.setComments(rs.getString("comments"));
				envReviews.add(envReview);
			}

			rs.close();

			if ((i == 0) && (levelType.equals("A"))) { // Get the Sub Project Header record from the Project if it exists
				sql = "select e.*" + " from env_review e join activity a on a.sproj_id=e.sproj_id " + " and a.act_id =" + levelId + " and e.env_det_id=0";
				logger.debug(sql);
				rs = db.select(sql);

				if (rs.next()) {
					EnvReviewEdit envReview = new EnvReviewEdit();
					envReview.setIndex(++i);
					envReview.setEnvReviewId(rs.getString("env_review_id"));
					envReview.setSubmitDate(StringUtils.date2str(rs.getDate("submit_date")));
					envReview.setReqDeterminationDate(StringUtils.date2str(rs.getDate("req_det_date")));
					envReview.setEnvDeterminationId(rs.getString("env_det_id"));
					envReviews.add(envReview);
				}

				rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getMessage());
		}

		return envReviews;
	}

	public static boolean saveEnvReviewList(EnvReviewForm form) throws Exception {
		int actId = 0;
		int sprojId = 0;

		Wrapper db = new Wrapper();
		List envReviewList = StringUtils.arrayToList(form.getEnvReviewList());

		db.beginTransaction();

		if (form.getLevel().equals("A")) {
			actId = form.getLevelId();
		} else {
			sprojId = form.getLevelId();
		}

		int i = envReviewList.size();
		ListIterator iter = envReviewList.listIterator(envReviewList.size());

		while (iter.hasPrevious()) {
			EnvReviewEdit review = (EnvReviewEdit) iter.previous();

			if ((!(review.getEnvDeterminationId().equals(""))) || (!(review.getEnvDeterminationActionId().equals("")))) {
				db.addBatch(getUpdateSql(i, sprojId, actId, review, form));
			}

			if (review.getDeleteFlag().equals("on")) {
				iter.remove();
			}

			i--;
		}

		db.executeBatch();

		form.setEnvReviewList(envReviewList);
		db = null;

		return true;
	}

	private static String getUpdateSql(int indx, int sprojId, int actId, EnvReviewEdit envReview, EnvReviewForm form) throws Exception {
		String sql = "";

		if (envReview.getDeleteFlag().equals("on")) {
			sql = "delete from env_review where env_review_id = " + envReview.getEnvReviewId();
		} else if (envReview.getEnvReviewId().equals("0")) {
			Wrapper db = new Wrapper();
			int envReviewId = db.getNextId("ENV_REVIEW_ID");
			envReview.setEnvReviewId("" + envReviewId);
			logger.debug("env det id is " + envReview.getEnvDeterminationId());
			logger.debug("env det action id is " + envReview.getEnvDeterminationActionId());
			db = null;
			sql = "INSERT INTO env_review (env_review_id,sproj_id,act_id,submit_date,action_date,determination_date,req_det_date,env_det_id,env_det_action_id,comments,updated_by,updated) values " + "(" + envReviewId + "," + sprojId + "," + actId + ",to_date('" + form.getSubmittedDate() + "','MM/DD/YYYY'),to_date('" + envReview.getActionDate() + "','MM/DD/YYYY'),to_date(" + StringUtils.checkString(form.getDeterminationDate()) + ",'MM/DD/YYYY'),to_date('" + form.getRequiredDeterminationDate() + "','MM/DD/YYYY')," + envReview.getEnvDeterminationId() + "," + envReview.getEnvDeterminationActionId() + "," + StringUtils.checkString(envReview.getComments()) + "," + form.getUserId() + ",CURRENT_TIMESTAMP)";
		} else {
			sql = "UPDATE env_review set submit_date = to_date(" + StringUtils.checkString(form.getSubmittedDate()) + ",'MM/DD/YYYY')," + "determination_date = to_date(" + StringUtils.checkString(form.getDeterminationDate()) + ",'MM/DD/YYYY')," + "req_det_date = to_date(" + StringUtils.checkString(form.getRequiredDeterminationDate()) + ",'MM/DD/YYYY')," + "action_date = to_date(" + StringUtils.checkString(envReview.getActionDate()) + ",'MM/DD/YYYY')," + "comments = " + StringUtils.checkString(envReview.getComments()) + "," + "updated_by = " + form.getUserId() + "," + "updated = CURRENT_TIMESTAMP " + "WHERE env_review_id = " + envReview.getEnvReviewId();
		}

		logger.debug("" + indx + ":" + sql);

		return sql;
	}

	public static boolean tmpsaveEnvReviewList(EnvReviewForm form) throws Exception {
		logger.debug("tmpsaveEnvReviewList(" + form + ")");
		int actId = 0;
		int sprojId = 0;

		Wrapper db = new Wrapper();
		EnvReviewEdit[] tmpEnvReviewEditArray = form.getEnvReviewList();

		db.beginTransaction();

		if (form.getLevel().equals("A")) {
			actId = form.getLevelId();
		} else {
			sprojId = form.getLevelId();
		}

		for (int i = tmpEnvReviewEditArray.length - 1; i >= 0; i--) {
			if (tmpEnvReviewEditArray[i].getEnvReviewId().equals("0")) {
				String nextId = "" + db.getNextId("ENV_REVIEW_ID");
				tmpEnvReviewEditArray[i].setEnvReviewId(nextId);
			}
			db.addBatch(tmpgetUpdateSql(sprojId, actId, tmpEnvReviewEditArray[i], form));

			try {
				if ("on".equals(tmpEnvReviewEditArray[i].getUptSubProject())) {
					String sql = "select sproj_id from activity where act_id=" + form.getLevelId();
					RowSet rs = db.select(sql);

					logger.debug("Print Sql: " + sql);

					if (rs.next()) {
						int id = rs.getInt("sproj_id");
						String nextId = "" + db.getNextId("ENV_REVIEW_ID");
						tmpEnvReviewEditArray[i].setEnvReviewId(nextId);
						db.addBatch(tmpgetUpdateSql(id, 0, tmpEnvReviewEditArray[i], form));
					}
				}
			} catch (Exception e) {
				logger.debug("Error in Updating SubProject");
			}

			try {
				if ("on".equals(tmpEnvReviewEditArray[i].getUptActivities())) {
					String sql = "";

					if ("Q".equals(form.getLevel())) {
						sql = "select act_id from activity where sproj_id=" + form.getLevelId();
					} else {
						sql = "select act_id from activity where sproj_id in (select sproj_id from activity where act_id=" + form.getLevelId() + ") and " + " act_id <> " + form.getLevelId();
					}

					logger.debug(sql);

					RowSet rs = db.select(sql);

					while (rs.next()) {
						int id = rs.getInt("act_id");
						String nextId = "" + db.getNextId("ENV_REVIEW_ID");
						tmpEnvReviewEditArray[i].setEnvReviewId(nextId);
						db.addBatch(tmpgetUpdateSql(0, id, tmpEnvReviewEditArray[i], form));
					}
				}
			} catch (Exception e) {
				logger.debug("Error in Updating SubProject");
			}
		}

		db.executeBatch();
		db = null;

		return true;
	}

	private static String tmpgetUpdateSql(int sprojId, int actId, EnvReviewEdit envReview, EnvReviewForm form) throws Exception {
		logger.debug("tmpgetUpdateSql(" + sprojId + "," + actId + "," + envReview + "," + form);
		String sql = "";
		try {
			if ("".equals(envReview.getDeleteFlag())) {
				if ("Yes".equals(envReview.getAdded())) {
					sql = "insert into env_review values(" + envReview.getEnvReviewId() + "," + actId + "," + sprojId + ",to_date('" + form.getSubmittedDate() + "','MM/DD/YYYY'),to_date('" + envReview.getActionDate() + "','MM/DD/YYYY'),to_date(" + StringUtils.checkString(form.getDeterminationDate()) + ",'MM/DD/YYYY'),to_date('" + form.getRequiredDeterminationDate() + "','MM/DD/YYYY')," + StringUtils.checkString(envReview.getEnvDeterminationId()) + "," + StringUtils.checkString(envReview.getEnvDeterminationActionId()) + "," + StringUtils.checkString(envReview.getComments()) + "," + form.getUserId() + ",CURRENT_TIMESTAMP)";
				} else {
					sql = "UPDATE env_review set submit_date = to_date(" + StringUtils.checkString(form.getSubmittedDate()) + ",'MM/DD/YYYY'),determination_date = to_date(" + StringUtils.checkString(form.getDeterminationDate()) + ",'MM/DD/YYYY'),req_det_date = to_date(" + StringUtils.checkString(form.getRequiredDeterminationDate()) + ",'MM/DD/YYYY'),action_date = to_date(" + StringUtils.checkString(envReview.getActionDate()) + ",'MM/DD/YYYY'),comments = " + StringUtils.checkString(envReview.getComments()) + ",updated_by = " + form.getUserId() + ",updated = CURRENT_TIMESTAMP WHERE env_review_id = " + envReview.getEnvReviewId();
				}
			} else {
				sql = "delete from env_review where env_review_id = " + envReview.getEnvReviewId();
			}
		} catch (Exception e) {
			logger.error("Exception occured in " + e.getMessage());
		}

		logger.debug(sql);

		return sql;
	}

	public static EnvReviewSummary getEnvReviewSummary(String levelType, int levelId) {
		logger.info("Entering getEnvReviewSummary(" + levelType + "," + levelId + ")");

		EnvReviewSummary summary = new EnvReviewSummary(levelType, levelId);
		Wrapper db;

		try {
			db = new Wrapper();

			String sql = "SELECT * FROM ENV_REVIEW_SUMMARY WHERE PSA_TYPE='" + levelType + "' AND PSA_ID=" + levelId;
			RowSet rs = db.select(sql);

			if (rs.next()) {
				logger.debug("Record Found");
				summary.setInsert("N");
				summary.setAdcActualDate(StringUtils.date2str(rs.getDate("APP_DEEM_COMP_A")));
				summary.setAdcLimitDate(StringUtils.date2str(rs.getDate("APP_DEEM_COMP_L")));
				summary.setDecisionActualDate(StringUtils.date2str(rs.getDate("DECISION_A")));
				summary.setDecisionLimitDate(StringUtils.date2str(rs.getDate("DECISION_L")));
				summary.setPisActualDate(StringUtils.date2str(rs.getDate("PREP_STUDY_A")));
				summary.setPisLimitDate(StringUtils.date2str(rs.getDate("PREP_STUDY_L")));
				summary.setPndActualDate(StringUtils.date2str(rs.getDate("PREP_NEG_DEC_A")));
				summary.setPndLimitDate(StringUtils.date2str(rs.getDate("PREP_NEG_DEC_L")));
				summary.setPublicNoticeActualDate(StringUtils.date2str(rs.getDate("PUB_NOTICE_A")));
				summary.setPublicNoticeLimitDate(StringUtils.date2str(rs.getDate("PUB_NOTICE_L")));
				summary.setDecEarliestActualDate(StringUtils.date2str(rs.getDate("DEC_EARLY_A")));
				summary.setDecEarliestLimitDate(StringUtils.date2str(rs.getDate("DEC_EARLY_L")));
				summary.setDecLastActualDate(StringUtils.date2str(rs.getDate("DEC_LAST_A")));
				summary.setDecLastLimitDate(StringUtils.date2str(rs.getDate("DEC_LAST_L")));
				summary.setNodActualDate(StringUtils.date2str(rs.getDate("DET_NOTICE_A")));
				summary.setNodLimitDate(StringUtils.date2str(rs.getDate("DET_NOTICE_L")));
				summary.setNopActualDate(StringUtils.date2str(rs.getDate("PREP_NOTICE_A")));
				summary.setNopLimitDate(StringUtils.date2str(rs.getDate("PREP_NOTICE_L")));
				summary.setNocActualDate(StringUtils.date2str(rs.getDate("COMP_NOTICE_A")));
				summary.setNocLimitDate(StringUtils.date2str(rs.getDate("COMP_NOTICE_L")));
				summary.setStateReview(StringUtils.YNtoOnOff(rs.getString("STATE_REVIEW")));
			}
		} catch (Exception e) {
			logger.error("Error :" + e.getMessage());
		}

		logger.info("Exiting getEnvReviewSummary(" + levelType + "," + levelId + ")");

		return summary;
	}

	public static void saveEnvReviewSummary(EnvReviewSummary summary) {
		StringBuffer sql;

		if (summary.getInsert().equals("Y")) {
			sql = new StringBuffer("INSERT INTO ENV_REVIEW_SUMMARY ");
			sql.append("(PSA_TYPE,PSA_ID,APP_DEEM_COMP_A,APP_DEEM_COMP_L,DECISION_A,DECISION_L,PREP_STUDY_A,PREP_STUDY_L,PREP_NEG_DEC_A,PREP_NEG_DEC_L,PUB_NOTICE_A,PUB_NOTICE_L,");
			sql.append("DEC_EARLY_A,DEC_EARLY_L,DEC_LAST_A,DEC_LAST_L,DET_NOTICE_A,DET_NOTICE_L,");
			sql.append("PREP_NOTICE_A,PREP_NOTICE_L,COMP_NOTICE_A,COMP_NOTICE_L,STATE_REVIEW) VALUES ( ");
			sql.append(StringUtils.checkString(summary.getLevel()) + ",");
			sql.append(StringUtils.checkNumber(summary.getLevelId()) + ",to_date(");
			sql.append(StringUtils.checkString(summary.getAdcActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getAdcLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getDecisionActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getDecisionLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getPisActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getPisLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getPndActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getPndLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getPublicNoticeActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getPublicNoticeLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getDecEarliestActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getDecEarliestLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getDecLastActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getDecLastLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getNodActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getNodLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getNopActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getNopLimitDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getNocActualDate()) + ",'MM/DD/YYYY'),to_date(");
			sql.append(StringUtils.checkString(summary.getNocLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append(StringUtils.checkString(summary.getStateReview()) + ")");
		} else {
			sql = new StringBuffer("UPDATE ENV_REVIEW_SUMMARY SET ");
			sql.append("APP_DEEM_COMP_A =to_date(" + StringUtils.checkString(summary.getAdcActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("APP_DEEM_COMP_L =to_date(" + StringUtils.checkString(summary.getAdcLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("DECISION_A =to_date(" + StringUtils.checkString(summary.getDecisionActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("DECISION_L =to_date(" + StringUtils.checkString(summary.getDecisionLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("PREP_STUDY_A =to_date(" + StringUtils.checkString(summary.getPisActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("PREP_STUDY_L =to_date(" + StringUtils.checkString(summary.getPisLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("PREP_NEG_DEC_A =to_date(" + StringUtils.checkString(summary.getPndActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("PREP_NEG_DEC_L =to_date(" + StringUtils.checkString(summary.getPndLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("PUB_NOTICE_A =to_date(" + StringUtils.checkString(summary.getPublicNoticeActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("PUB_NOTICE_L =to_date(" + StringUtils.checkString(summary.getPublicNoticeLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("DEC_EARLY_A =to_date(" + StringUtils.checkString(summary.getDecEarliestActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("DEC_EARLY_L =to_date(" + StringUtils.checkString(summary.getDecEarliestLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("DEC_LAST_A =to_date(" + StringUtils.checkString(summary.getDecLastActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("DEC_LAST_L =to_date(" + StringUtils.checkString(summary.getDecLastLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("DET_NOTICE_A =to_date(" + StringUtils.checkString(summary.getNodActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("DET_NOTICE_L =to_date(" + StringUtils.checkString(summary.getNodLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("PREP_NOTICE_A =to_date(" + StringUtils.checkString(summary.getNopActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("PREP_NOTICE_L =to_date(" + StringUtils.checkString(summary.getNopLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("COMP_NOTICE_A =to_date(" + StringUtils.checkString(summary.getNocActualDate()) + ",'MM/DD/YYYY'),");
			sql.append("COMP_NOTICE_L =to_date(" + StringUtils.checkString(summary.getNocLimitDate()) + ",'MM/DD/YYYY'),");
			sql.append("STATE_REVIEW =" + StringUtils.checkString(StringUtils.OnOfftoYN(summary.getStateReview())) + " ");
			sql.append(" WHERE PSA_TYPE='" + summary.getLevel() + "' AND PSA_ID=" + summary.getLevelId());
		}

		logger.debug(sql);

		try {
			Wrapper db = new Wrapper();
			db.update(sql.toString());
		} catch (Exception e) {
			logger.error("Error : " + e.getMessage());
		}
	}

	public static Element getPlanningStatusGrpElement(int actId, String departmentCode) {
		logger.debug("getPlanningStatusGrpElement(" + actId + "," + departmentCode + ")");
		Element group = new Element("planningstatusgroup");
		try {
			if (departmentCode == null)
				departmentCode = "";
			StringBuffer sql = new StringBuffer();
			sql.append("select a.date_entered c1 , lps.description c2, a.comments c3");
			if (departmentCode.equals("PL")) {
				sql.append(" from act_plan_status a join lkup_plan_status lps on a.plan_status_id=lps.plan_status_id");
				sql.append(" where a.act_id = " + actId);

			} else {
				sql.append(" from (act_plan_status a join lkup_plan_status lps on a.plan_status_id=lps.plan_status_id)");
				sql.append(" join sproj_links sl on sl.sproj_id_from = a.sproj_id");
				sql.append(" where sl.sproj_id_to = (select sproj_id from activity where act_id = " + actId + ")");
			}
			sql.append(" order by a.plan_status_id asc  ");

			logger.debug("Planning Status Group SQL : " + sql.toString());
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());

			while (rs.next()) {
				Element dateElement = new Element("statusdate");
				Element statusElement = new Element("status");
				Element commentElement = new Element("commentdata");

				dateElement.addContent(StringUtils.date2str(rs.getDate("C1")));
				statusElement.addContent(rs.getString("C2"));
				commentElement.addContent(rs.getString("C3"));

				Element element = new Element("planningstatus");
				element.addContent(dateElement);
				element.addContent(statusElement);
				element.addContent(commentElement);
				logger.debug("Status ELEMENT - Created ");

				group.addContent(element);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getPlanningStatusGrpElement() method " + e.getMessage());
		}
		return group;
	}

	public static Element getPlanningResolutionGrpElement(int actId, String departmentCode) {
		logger.debug(" getPlanningResolutionGrpElement(" + actId + "," + departmentCode + ")");
		Element group = new Element("planningresolutiongroup");
		try {

			if (departmentCode == null)
				departmentCode = "";
			StringBuffer sql = new StringBuffer();
			sql.append("select resolution_nbr as c1 , date_approved as c2, description as c3");
			if (departmentCode.equals("PL")) {
				sql.append(" from resolution r where r.act_id = " + actId);
			} else {
				sql.append(" from resolution r join sproj_links sl on sl.sproj_id_from = r.sproj_id");
				sql.append(" where sl.sproj_id_to = (select sproj_id from activity where act_id = " + actId + ")");
			}
			sql.append(" order by r.resolution_id asc  ");
			logger.debug("Planning Resolution Group SQL : " + sql.toString());
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());

			while (rs.next()) {
				Element resolutionElement = new Element("resolutionnbr");
				Element dateElement = new Element("resolutiondate");
				Element descriptionElement = new Element("description");

				resolutionElement.addContent(rs.getString("C1"));
				dateElement.addContent(StringUtils.date2str(rs.getDate("C2")));
				descriptionElement.addContent(rs.getString("C3"));

				Element element = new Element("planningresolution");
				element.addContent(resolutionElement);
				element.addContent(dateElement);
				element.addContent(descriptionElement);
				logger.debug("Resolution ELEMENT - Created ");

				group.addContent(element);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getPeopleGrpElement() method " + e.getMessage());
		}

		return group;
	}

	public static Element getPlanningEnvReviewGrpElement(int actId, String departmentCode) {
		logger.debug(" getPlanningEnvReviewGrpElement(" + actId + "," + departmentCode + ")");
		Element envReviewGroup = new Element("planningenvreviewgroup");
		try {
			if (departmentCode == null)
				departmentCode = "";
			StringBuffer sql = new StringBuffer();
			sql.append("select e.submit_date,e.determination_date,e.req_det_date,led.description as env_det_description,leda.description as env_action_description,e.action_date,e.comments");
			sql.append(" from ((env_review e join lkup_env_det led on led.env_det_id=e.env_det_id)");
			sql.append(" join lkup_env_det_action leda on leda.env_det_action_id=e.env_det_action_id)");
			if (departmentCode.equals("PL")) {
				sql.append(" where e.act_id = " + actId);
			} else {
				sql.append(" join sproj_links sl on sl.sproj_id_from = e.sproj_id");
				sql.append(" where sl.sproj_id_to = (select sproj_id from activity where act_id = " + actId + ")");
			}
			sql.append(" order by e.env_review_id asc ");
			logger.debug("Planning Environmental Review Group SQL : " + sql.toString());
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());

			boolean firstRow = true;
			while (rs.next()) {
				if (firstRow) {
					Element submittedDate = new Element("submitteddate");
					submittedDate.addContent(StringUtils.date2str(rs.getDate("submit_date")));
					Element determinationDate = new Element("determinationdate");
					determinationDate.addContent(StringUtils.date2str(rs.getDate("determination_date")));
					Element reqDeterminationDate = new Element("reqdeterminationdate");
					reqDeterminationDate.addContent(StringUtils.date2str(rs.getDate("req_det_date")));

					envReviewGroup.addContent(submittedDate);
					envReviewGroup.addContent(determinationDate);
					envReviewGroup.addContent(reqDeterminationDate);
					firstRow = false;
				}
				Element determinationElement = new Element("determination");
				Element actionElement = new Element("action");
				Element actionDateElement = new Element("actiondate");
				Element commentElement = new Element("commentdata");

				determinationElement.addContent(rs.getString("env_det_description"));
				actionElement.addContent(rs.getString("env_action_description"));
				actionDateElement.addContent(StringUtils.date2str(rs.getDate("action_date")));
				commentElement.addContent(rs.getString("comments"));

				Element element = new Element("planningenvreview");
				element.addContent(determinationElement);
				element.addContent(actionElement);
				element.addContent(actionDateElement);
				element.addContent(commentElement);

				logger.debug("EnvReview ELEMENT - Created ");

				envReviewGroup.addContent(element);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getPlanningEnvReviewElement() method " + e.getMessage());
		}

		return envReviewGroup;
	}

	/**
	 */
	public static List getResolutionList(String levelType, int levelId) {
		List resolutions = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from resolution where ";

			if (levelType.equals("A")) {
				sql += ("act_id = " + levelId);
			} else {
				sql += ("sproj_id =" + levelId);
			}

			sql += " order by date_approved desc, resolution_id desc";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				ResolutionEdit resolution = new ResolutionEdit();
				resolution.setResolutionId(rs.getString("resolution_id"));
				resolution.setResolutionNbr(rs.getString("resolution_nbr"));
				resolution.setDescription(rs.getString("description"));
				resolution.setApprovedDate(StringUtils.date2str(rs.getDate("date_approved")));
				resolutions.add(resolution);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception in --  getActivity List  -- " + e.getMessage());
		}

		return resolutions;
	}

	public static void saveResoultion(ResolutionForm resolutionForm) throws Exception {
		int subprojectId = 0;
		int activityId = 0;
		Wrapper db = new Wrapper();
		db.beginTransaction();

		if ("A".equals(resolutionForm.getLevelType())) {
			activityId = resolutionForm.getLevelId();
		} else {
			subprojectId = resolutionForm.getLevelId();
		}

		ResolutionEdit[] tmpResoultionList = resolutionForm.getResolutionList();

		for (int i = tmpResoultionList.length - 1; i >= 0; i--) {
			if (tmpResoultionList[i].getResolutionId().equals("0")) {
				String nextId = "" + db.getNextId("RESOLUTION_ID");
				tmpResoultionList[i].setResolutionId(nextId);
			}

			// Generate the SQL for the status
			db.addBatch(getUpdateSql(subprojectId, activityId, tmpResoultionList[i], resolutionForm));

			try {
				if ("on".equals(tmpResoultionList[i].getUptSubProject())) {
					String sql = "select sproj_id from activity where act_id=" + resolutionForm.getLevelId();
					RowSet rs = db.select(sql);
					logger.debug(sql);

					if (rs.next()) {
						int id = rs.getInt("sproj_id");
						String nextId = "" + db.getNextId("RESOLUTION_ID");
						tmpResoultionList[i].setResolutionId(nextId);
						db.addBatch(getUpdateSql(id, 0, tmpResoultionList[i], resolutionForm));
					}
				}
			} catch (Exception e) {
				logger.error("Exception occured in updateSubProject " + e.getMessage());
			}

			try {

				if ("on".equals(tmpResoultionList[i].getUptActivities())) {
					String sql = "";

					if (resolutionForm.getLevelType().equals("Q")) {
						sql = "select act_id from activity where sproj_id=" + resolutionForm.getLevelId();
					} else {
						sql = "select act_id from activity where sproj_id in (select sproj_id from activity where act_id=" + resolutionForm.getLevelId() + ") and " + " act_id <> " + resolutionForm.getLevelId();
					}

					logger.debug(sql);

					RowSet rs = db.select(sql);

					while (rs.next()) {
						int id = rs.getInt("act_id");
						String nextId = "" + db.getNextId("RESOLUTION_ID");
						tmpResoultionList[i].setResolutionId(nextId);
						db.addBatch(getUpdateSql(0, id, tmpResoultionList[i], resolutionForm));
					}
				}
			} catch (Exception e) {
				logger.error("Exception occured in updateActivities " + e.getMessage());
			}
		}

		db.executeBatch();
		db = null;

		return;
	}

	public static String getUpdateSql(int sprojId, int actId, ResolutionEdit status, ResolutionForm resolutionForm) {
		String sql = "";

		try {
			if (status.getDelete().equals("")) {
				if ("Yes".equals(status.getAdded())) {
					sql = "insert into resolution values(" + status.getResolutionId() + "," + actId + "," + sprojId + "," + StringUtils.checkString(status.getResolutionNbr()) + "," + StringUtils.checkString(status.getDescription()) + ",to_date('" + status.getApprovedDate() + "','mm/dd/yyyy')," + resolutionForm.getUserId() + ",CURRENT_TIMESTAMP)";
				} else {
					sql = "update resolution set resolution_nbr = " + StringUtils.checkString(status.getResolutionNbr()) + ",description = " + StringUtils.checkString(status.getDescription()) + ",date_approved = to_date('" + status.getApprovedDate() + "','mm/dd/yyyy'),updated_by = " + resolutionForm.getUserId() + ",updated = CURRENT_TIMESTAMP where resolution_id = " + status.getResolutionId() + "";
				}
			} else {
				sql = "delete from resolution where resolution_id = " + status.getResolutionId() + "";
			}

		} catch (Exception e) {
			logger.error("Exception occured in getUpdateSql " + e.getMessage());
		}

		logger.debug(sql);

		return sql;
	}

	public static List getResolutionSqls(String levelType, int levelId, int userId, List resolutionEdits) throws Exception {
		List sqls = new ArrayList();
		Wrapper db = new Wrapper();

		String sql = "";
		ListIterator iter = resolutionEdits.listIterator(resolutionEdits.size());

		while (iter.hasPrevious()) {
			ResolutionEdit resolution = (ResolutionEdit) iter.previous();

			if (!(resolution.getDelete() == null) && resolution.getDelete().equals("on")) {
				sql = "delete from resolution where resolution_id = " + resolution.getResolutionId();
				iter.remove();
				sqls.add(sql);
			} else if (resolution.getResolutionId().equals("0")) {
				String resolutionId = "" + db.getNextId("RESOLUTION_ID");
				resolution.setResolutionId(resolutionId);
				sql = "insert into resolution(resolution_id,act_id,sproj_id,resolution_nbr,description,date_approved,updated_by,updated)" + " values(" + resolutionId + ",";

				if (levelType.equals("A")) {
					sql += (levelId + ",0,");
				} else {
					sql += ("0," + levelId + ",");
				}

				sql += (StringUtils.checkString(resolution.getResolutionNbr()) + "," + StringUtils.checkString(resolution.getDescription()) + ",to_date(" + StringUtils.checkString(resolution.getApprovedDate()) + ",'MM/DD/YYYY')," + userId + "," + "CURRENT_TIMESTAMP)");
				sqls.add(sql);
			} else {
				sql = "update resolution set resolution_nbr = " + StringUtils.checkString(resolution.getResolutionNbr()) + ",description = " + StringUtils.checkString(resolution.getDescription()) + ",date_approved = to_date(" + StringUtils.checkString(resolution.getApprovedDate()) + ",'MM/DD/YYYY'),updated_by = " + userId + ",updated = CURRENT_TIMESTAMP  where resolution_id = " + resolution.getResolutionId();
				sqls.add(sql);
			}
		}

		return sqls;
	}

	public static List getStatusList(String levelType, int levelId) {
		List statuses = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select aps.*,lps.description from act_plan_status aps join lkup_plan_status lps on lps.plan_status_id=aps.plan_status_id where ";

			if (levelType.equals("A")) {
				sql += ("act_id = " + levelId);
			} else {
				sql += ("sproj_id =" + levelId);
			}

			sql += " order by aps.date_entered desc,lps.sort_order desc";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				StatusEdit status = new StatusEdit();
				status.setUpdate("Y");
				status.setAdded("0");
				status.setStatusId(rs.getString("status_id"));
				status.setPlanStatusId(rs.getString("plan_status_id"));
				status.setStatus(rs.getString("description"));
				status.setComment(rs.getString("comments"));
				status.setStatusDate(StringUtils.date2str(rs.getDate("date_entered")));
				statuses.add(status);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception: " + e.getMessage());
		}

		return statuses;
	}

	public static List getStatusDisplay(String levelType, int levelId) {
		List statuses = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String lastPlanStatusId = "";
			String sql = "select lps.plan_status_id,lps.description,aps.status_id  from lkup_plan_status lps left outer join act_plan_status aps on lps.plan_status_id=aps.plan_status_id and ";

			if (levelType.equals("A")) {
				sql += ("act_id = " + levelId);
			} else {
				sql += ("sproj_id =" + levelId);
			}

			sql += " order by lps.sort_order asc";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				if (!(rs.getString("plan_status_id").equals(lastPlanStatusId))) {
					StatusEdit status = new StatusEdit();
					status.setAdded(rs.getString("status_id"));
					status.setPlanStatusId(rs.getString("plan_status_id"));
					status.setStatus(rs.getString("description"));
					statuses.add(status);
					lastPlanStatusId = rs.getString("plan_status_id");
				}
			}

			rs.close();
			rs = null;

			// Set the checkboxes for 'County NOA & County NOD'
			// sql = "select env_det_action_id from env_review where env_det_id=4 and env_det_action_id in (8,11) and ";
			sql = "select env_det_action_id from env_review where ";

			if (levelType.equals("A")) {
				sql += ("act_id = " + levelId);
			} else {
				sql += ("sproj_id =" + levelId);
			}

			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				String actionId = rs.getString("env_det_action_id");
				String updateStatusId = "";

				if (actionId.equals("8")) {
					updateStatusId = "7";
				} else {
					updateStatusId = "10";
				}

				ListIterator iter = statuses.listIterator();
				boolean notFound = true;

				while (iter.hasNext() && notFound) {
					StatusEdit status = (StatusEdit) iter.next();

					if (status.getPlanStatusId().equals(updateStatusId)) {
						status.setAdded("0");
						notFound = false;
					}
				}
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception: " + e.getMessage());
		}

		return statuses;
	}

	public static void saveStatusList(StatusForm form) throws Exception {
		int actId = 0;
		int sprojId = 0;
		boolean envReviewAdded = false;

		Wrapper db = new Wrapper();
		StatusEdit[] statusList = form.getStatusList();

		db.beginTransaction();

		if (form.getLevelType().equals("A")) {
			actId = form.getLevelId();
		} else {
			sprojId = form.getLevelId();
		}

		for (int i = statusList.length - 1; i >= 0; i--) {
			// For records being inserted get NEXTID from DB
			if (statusList[i].getStatusId().equals("0")) {
				String nextId = "" + db.getNextId("STATUS_ID");
				statusList[i].setStatusId(nextId);
			}

			// Generate the SQL for the status
			db.addBatch(getUpdateSql(sprojId, actId, statusList[i], form));

			// Generate the Env Review Header if there is none present. Check
			// this only when records are being added
			if ((envReviewAdded == false) && (statusList[i].getAdded().equals("Yes")) && (statusList[i].getPlanStatusId().equals("1") || statusList[i].getPlanStatusId().equals("3"))) {
				try {
					String sql;
					int lSprojId = sprojId;

					// For Activity get the subproject id
					if (form.getLevelType().equals("A")) {
						sql = "select sproj_id from activity where act_id = " + form.getLevelId();

						RowSet rsSproj = db.select(sql);

						if (rsSproj.next()) {
							lSprojId = rsSproj.getInt("sproj_id");
						}
					}

					sql = "select count(*) as rowcount from env_review where sproj_id = " + lSprojId;
					logger.debug(sql);

					RowSet rsCount = db.select(sql);

					if (rsCount.next()) {
						Calendar submittedDate = StringUtils.str2cal(statusList[i].getStatusDate());
						Calendar reqDeterminationDate = StringUtils.str2cal(statusList[i].getStatusDate());

						if (statusList[i].getPlanStatusId().equals("1")) {
							reqDeterminationDate.add(Calendar.DATE, 60);
						} else {
							reqDeterminationDate.add(Calendar.DATE, 30);
						}

						if (rsCount.getInt("rowcount") == 0) {
							logger.debug("Insert into Env Review Header");

							int nextId = db.getNextId("ENV_REVIEW_ID");
							sql = "insert into env_review (env_review_id,act_id,sproj_id,submit_date,req_det_date,env_det_id,env_det_action_id,updated_by,updated) values (" + nextId + ",0," + lSprojId + ",to_date('" + StringUtils.cal2str(submittedDate) + "','MM/DD/YYYY'),to_date('" + StringUtils.cal2str(reqDeterminationDate) + "','MM/DD/YYYY'),0,0,0,CURRENT_TIMESTAMP)";
							logger.debug(sql);
							db.addBatch(sql);
						} else {
							logger.debug("Update the dates for the current records both project and subproject");
							sql = "update env_review set req_det_date=to_date('" + StringUtils.cal2str(reqDeterminationDate) + "','MM/DD/YYYY'), submit_date=to_date('" + StringUtils.cal2str(submittedDate) + "','MM/DD/YYYY') where sproj_id = " + lSprojId;
							logger.debug(sql);
							db.addBatch(sql);

							sql = "update env_review set req_det_date=to_date('" + StringUtils.cal2str(reqDeterminationDate) + "','MM/DD/YYYY'), submit_date=to_date('" + StringUtils.cal2str(submittedDate) + "','MM/DD/YYYY') where act_id in (select act_id from activity where sproj_id = " + lSprojId + ")";
							logger.debug(sql);
							db.addBatch(sql);
						}
					}

					envReviewAdded = true;
				} catch (Exception ex) {
					logger.warn("Error :" + ex.getMessage());
				}
			}

			try {
				if (statusList[i].getUpdateSubProject().equals("on")) {
					String sql = "select sproj_id from activity where act_id=" + form.getLevelId();
					RowSet rs = db.select(sql);

					logger.debug("Print Sql: " + sql);

					if (rs.next()) {
						int id = rs.getInt("sproj_id");
						String nextId = "" + db.getNextId("STATUS_ID");
						statusList[i].setStatusId(nextId);
						db.addBatch(getUpdateSql(id, 0, statusList[i], form));
					}
				}
			} catch (Exception e) {
				logger.debug("Error in Updating SubProject");
			}

			try {
				if (statusList[i].getUpdateActivities().equals("on")) {
					String sql = "";

					if (form.getLevelType().equals("Q")) {
						sql = "select act_id from activity where sproj_id=" + form.getLevelId();
					} else {
						sql = "select act_id from activity where sproj_id in (select sproj_id from activity where act_id=" + form.getLevelId() + ") and " + " act_id <> " + form.getLevelId();
					}

					logger.debug(sql);

					RowSet rs = db.select(sql);

					while (rs.next()) {
						int id = rs.getInt("act_id");
						String nextId = "" + db.getNextId("STATUS_ID");
						statusList[i].setStatusId(nextId);
						db.addBatch(getUpdateSql(0, id, statusList[i], form));
					}
				}
			} catch (Exception e) {
				logger.debug("Error in Updating SubProject");
			}
		}

		db.executeBatch();
		db = null;

		return;
	}

	private static String getUpdateSql(int sprojId, int actId, StatusEdit status, StatusForm form) {
		String sql = "";

		if (status.getDeleteFlag().equals("")) {
			if (status.getAdded().equals("Yes")) {
				sql = "insert into act_plan_status (status_id,act_id,sproj_id,date_entered,plan_status_id,comments,updated_by,updated) values (" + status.getStatusId() + "," + actId + "," + sprojId + ",to_date('" + status.getStatusDate() + "','MM/DD/YYYY')," + status.getPlanStatusId() + "," + StringUtils.checkString(status.getComment()) + "," + form.getUserId() + ",CURRENT_TIMESTAMP)";
			} else {
				sql = "update act_plan_status set comments=" + StringUtils.checkString(status.getComment()) + ",updated_by=" + form.getUserId() + ",updated=CURRENT_TIMESTAMP where STATUS_ID = " + status.getStatusId();
			}
		} else {
			sql = "delete from act_plan_status where status_id = " + status.getStatusId();
		}

		logger.debug(sql);

		return sql;
	}

	public List getRequestServiceList(String departmentid, String date) throws Exception {
		logger.info("Entering requestServiceList....");
		User user = new User();

		ActivityListDetail activityListDetail = new ActivityListDetail();
		List requestList = new ArrayList();
		List activityList = null;
		boolean csr = true;

		RowSet rs = null;
		RowSet rs1 = null;
		RowSet rs2 = null;
		RowSet rs3 = null;
		RowSet rs4 = null;
		RowSet rs5 = null;
		int i, k;

		try {
			Wrapper db = new Wrapper();
			/*
			 * logger.debug("userType"+userType); String str1[]= userType.split(","); for(int j=0;j<str1.length;j++){ //Checking if user belongs to Request For Service group if(str1[j].equals(StringUtils.i2s(Constants.GROUPS_REQUEST_FOR_SERVICE))){ csr=false;
			 * 
			 * } }
			 */

			String sql = "select I.DEPT_ID,I.RFS_ID,I.CREATED,I.INCIDENT_ID,I.UNIT,I.STREET_NO,I.STREET_NAME,I.PROBLEM,I.POI,I.CROSS_STREET1,I.CROSS_STREET2 FROM INCIDENT I ";

			sql += " WHERE I.CREATED <= " + StringUtils.toOracleDate(date) + " AND I.INCI_STATUS=" + 7 + " AND DEPT_ID IN (" + departmentid + ") ORDER BY I.CREATED DESC, RFS_ID desc";

			logger.info(sql);

			AddressAgent streetAgent = new AddressAgent();
			Map streetNameMap = streetAgent.getStreetNameMap();

			logger.debug("Test::" + streetNameMap.get("98").toString());

			rs = db.select(sql);

			while (rs.next()) {

				String rfsid = rs.getString("RFS_ID");
				String incidentid = rs.getString("INCIDENT_ID");
				String requestdate = StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED")));
				String unit = rs.getString("UNIT") != null ? rs.getString("UNIT") : "";
				String streetno = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : "";
				String streetname = rs.getString("STREET_NAME");
				String poi = rs.getString("POI") != null ? rs.getString("POI") : "";
				String crossstreet1 = rs.getString("CROSS_STREET1");
				String crossstreet2 = rs.getString("CROSS_STREET2");
				String problem = rs.getString("PROBLEM");

				activityList = new ArrayList();
				activityListDetail = new ActivityListDetail();
				activityListDetail.setActivityId(0 + "");
				activityListDetail.setActivityNumber("Add");
				activityList.add(activityListDetail);
				String sql1 = "select A.act_id,A.act_nbr,A.STATUS from rfs_act R left outer join activity A on R.act_id = A.act_id where R.incident_id=" + incidentid;
				rs1 = db.select(sql1);
				logger.info(sql1);
				i = 0;
				k = 0;

				while (rs1.next()) {
					i++;
					String activityid = rs1.getString("ACT_ID");

					// String sql2="select STATUS from activity where ACT_ID="+activityid;
					// rs3=db.select(sql2);
					// logger.info(sql2);
					// while(rs3.next()){
					int status = rs1.getInt("STATUS");
					if (status == Constants.ACTIVITY_STATUS_NS_CLOSED) {
						k++;
					}
					// }

					String activityNumber = rs1.getString("ACT_NBR");
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(activityid);
					activityListDetail.setActivityNumber(activityNumber);
					activityList.add(activityListDetail);
				}
				rs1.close();

				ListRequestServiceForm listRequestServiceForm = new ListRequestServiceForm();
				listRequestServiceForm.setRfsId(rfsid);
				listRequestServiceForm.setIncidentId(incidentid);
				listRequestServiceForm.setRequestDate(requestdate);
				listRequestServiceForm.setUnit(unit);
				listRequestServiceForm.setStreetNumber(streetno);
				listRequestServiceForm.setStreetName(streetname);

				if (streetname != null) {
					if (streetNameMap.get(streetname) != null) {
						streetname = streetNameMap.get(streetname).toString();
					} else {
						streetname = "";
					}
					/*
					 * String sql2="select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID="+streetname; logger.debug("sql2="+sql2); rs2=db.select(sql2); if(rs2.next()){ streetname = (rs2.getString("PRE_DIR")!=null?rs2.getString("PRE_DIR"):"")+" "+(rs2.getString("STR_NAME")!=null?rs2.getString("STR_NAME"):"")+" "+(rs2.getString("STR_TYPE")!=null?rs2.getString("STR_TYPE"):""); logger.debug("streetname="+streetname); }else{ streetname=""; logger.debug("streetname="+streetname); } rs2.close();
					 */
				}
				if (crossstreet1 != null) {
					logger.debug("::crossstreet1::" + crossstreet1 + "::");
					if (streetNameMap.get(crossstreet1) != null) {
						crossstreet1 = streetNameMap.get(crossstreet1).toString();
					} else {
						crossstreet1 = "";
					}

					/*
					 * String sql4="select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID="+crossstreet1; logger.debug("sql4="+sql4); rs4=db.select(sql4); if(rs4.next()){ crossstreet1 = (rs4.getString("PRE_DIR")!=null?rs4.getString("PRE_DIR"):"")+" "+(rs4.getString("STR_NAME")!=null?rs4.getString("STR_NAME"):"")+" "+(rs4.getString("STR_TYPE")!=null?rs4.getString("STR_TYPE"):""); logger.debug("crossstreet1="+crossstreet1); }else{ crossstreet1=""; logger.debug("crossstreet1="+crossstreet1); } rs4.close();
					 */
				}
				if (crossstreet2 != null) {
					if (streetNameMap.get(crossstreet2) != null) {
						crossstreet2 = streetNameMap.get(crossstreet2).toString();
					} else {
						crossstreet2 = "";
					}
					/*
					 * String sql5="select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID="+crossstreet2; logger.debug("sql5="+sql5); rs5=db.select(sql5); if(rs5.next()){ crossstreet2 = (rs5.getString("PRE_DIR")!=null?rs5.getString("PRE_DIR"):"")+" "+(rs5.getString("STR_NAME")!=null?rs5.getString("STR_NAME"):"")+" "+(rs5.getString("STR_TYPE")!=null?rs5.getString("STR_TYPE"):""); logger.debug("crossstreet2="+crossstreet2); }else{ crossstreet2=""; logger.debug("crossstreet2="+crossstreet2); } rs5.close();
					 */
				}

				// if(streetno!="" && streetname!=""){
				// listRequestServiceForm.setAddress(streetno + " " + streetname + " " + unit);
				// }else if(poi!=""){
				// listRequestServiceForm.setAddress(poi);
				// }else{
				// listRequestServiceForm.setAddress(crossstreet1 + "," + crossstreet2);
				// }
				listRequestServiceForm.setAddress(poi + " " + streetno + " " + streetname + " " + unit + " " + crossstreet1 + " " + crossstreet2);
				listRequestServiceForm.setProblem(problem);
				listRequestServiceForm.setActivityList(activityList);

				if (i == k && (i != 0) && (k != 0)) {
					// The above condition checks if all the activities of a incident are closed
					// requestList.remove(listRequestServiceForm);
				} else
					requestList.add(listRequestServiceForm);
			}
			rs.close();
			logger.debug("returning requestList of size " + requestList.size());
			return requestList;
		} catch (Exception e) {
			logger.error("Exception occured in requestServiceList method . ", e);
			throw e;
		}
	}

	public Map getRequestServiceListTest(String departmentid, String dateFrom, String dateTo) throws Exception {
		logger.info("Entering requestServiceList....");
		User user = new User();

		ActivityListDetail activityListDetail = new ActivityListDetail();
		Map requestListMap = new HashMap();
		List requestList = new ArrayList();
		List requestListFromOtherDept = new ArrayList();
		List activityList = null;

		RowSet rs = null;
		RowSet rs1 = null;

		int i, k;

		try {
			Wrapper db = new Wrapper();

			String sql = "select I.DEPT_ID,I.INCI_STATUS,I.RFS_ID,I.CREATED,I.INCIDENT_ID,I.UNIT,I.STREET_NO,I.STREET_NAME,I.PROBLEM,I.POI,I.CROSS_STREET1,I.CROSS_STREET2 FROM INCIDENT I ";

			sql += " WHERE I.CREATED >= " + StringUtils.toOracleDate(dateFrom) + " and I.CREATED <= " + StringUtils.toOracleDate(dateTo) + "AND I.INCI_STATUS=" + Constants.RFS_STATUS_OPEN + " ORDER BY RFS_ID desc,I.CREATED DESC ";

			logger.info(sql);

			String incidentIds = "";
			AddressAgent streetAgent = new AddressAgent();
			Map streetNameMap = streetAgent.getStreetNameMap();

			rs = db.select(sql);

			while (rs.next()) {

				String rfsid = rs.getString("RFS_ID");
				String incidentid = rs.getString("INCIDENT_ID");
				incidentIds = incidentIds + ",";
				String requestdate = StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED")));
				String unit = rs.getString("UNIT") != null ? rs.getString("UNIT") : "";
				String streetno = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : "";
				String streetname = rs.getString("STREET_NAME");
				String poi = rs.getString("POI") != null ? rs.getString("POI") : "";
				String crossstreet1 = rs.getString("CROSS_STREET1");
				String crossstreet2 = rs.getString("CROSS_STREET2");
				String problem = rs.getString("PROBLEM");
				String deptIdfromDb = rs.getString("DEPT_ID");
				String inciStatus = rs.getString("INCI_STATUS");

				activityList = new ArrayList();
				if (departmentid.equalsIgnoreCase(deptIdfromDb)) {
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(0 + "");
					activityListDetail.setActivityNumber("Add");
					activityList.add(activityListDetail);
				}
				String sql1 = "select R.incident_id,A.act_id,A.act_nbr,A.STATUS from rfs_act R inner join activity A on R.act_id = A.act_id where R.incident_id=" + incidentid;
				rs1 = db.select(sql1);
				logger.info(sql1);
				i = 0;
				k = 0;

				while (rs1.next()) {
					i++;
					String activityid = rs1.getString("ACT_ID");

					int status = rs1.getInt("STATUS");
					if (status == Constants.ACTIVITY_STATUS_NS_CLOSED || status == Constants.ACTIVITY_STATUS_FIRE_CASES_CLOSE) {
						k++;
					}

					String activityNumber = rs1.getString("ACT_NBR");
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(activityid);
					activityListDetail.setActivityNumber(activityNumber);
					activityList.add(activityListDetail);
				}
				rs1.close();

				ListRequestServiceForm listRequestServiceForm = new ListRequestServiceForm();
				listRequestServiceForm.setRfsId(rfsid);
				listRequestServiceForm.setIncidentId(incidentid);
				listRequestServiceForm.setRequestDate(requestdate);
				listRequestServiceForm.setUnit(unit);
				listRequestServiceForm.setStreetNumber(streetno);
				listRequestServiceForm.setStreetName(streetname);
				listRequestServiceForm.setDeptId(deptIdfromDb);
				listRequestServiceForm.setIncistatus(inciStatus);

				if (streetname != null) {
					if (streetNameMap.get(streetname) != null) {
						streetname = streetNameMap.get(streetname).toString();
					} else {
						streetname = "";
					}
				}
				if (crossstreet1 != null) {
					if (streetNameMap.get(crossstreet1) != null) {
						crossstreet1 = streetNameMap.get(crossstreet1).toString();
					} else {
						crossstreet1 = "";
					}

				}
				if (crossstreet2 != null) {
					if (streetNameMap.get(crossstreet2) != null) {
						crossstreet2 = streetNameMap.get(crossstreet2).toString();
					} else {
						crossstreet2 = "";
					}
				}

				listRequestServiceForm.setAddress(poi + " " + streetno + " " + streetname + " " + unit + " " + crossstreet1 + " " + crossstreet2);
				listRequestServiceForm.setProblem(problem);
				listRequestServiceForm.setActivityList(activityList);

				if (i == k && (i != 0) && (k != 0)) {
					// The above condition checks if all the activities of a incident are closed
					// requestList.remove(listRequestServiceForm);
				} else if (listRequestServiceForm.getDeptId() != null) {
					if (listRequestServiceForm.getDeptId().equalsIgnoreCase(departmentid)) {
						requestList.add(listRequestServiceForm);
					} else {
						requestListFromOtherDept.add(listRequestServiceForm);
					}
				}

			}

			requestListMap.put("requestList", requestList);
			requestListMap.put("requestListFromOtherDept", requestListFromOtherDept);
			rs.close();
			logger.debug("returning requestList of size " + requestList.size());
			return requestListMap;
		} catch (Exception e) {
			logger.error("Exception occured in requestServiceList method . ", e);
			throw e;
		}
	}

	public Map getRequestServiceSearchList(String departmentid, String dateFrom, String dateTo, String criteria, boolean closed) throws Exception {
		logger.info("Entering requestServiceList....");
		User user = new User();
		if (criteria != null) {
			criteria = criteria.toUpperCase();
		}
		ActivityListDetail activityListDetail = new ActivityListDetail();
		Map requestListMap = new HashMap();
		List requestList = new ArrayList();
		List requestListFromOtherDept = new ArrayList();
		List activityList = null;

		RowSet rs = null;
		RowSet rs1 = null;

		String statusFilter = "";
		if (!closed) {
			statusFilter = "AND I.INCI_STATUS=" + 7;
		}

		int i, k;

		try {
			Wrapper db = new Wrapper();

			String sql = "select I.DEPT_ID,I.RFS_ID,I.INCI_STATUS,I.CREATED,I.INCIDENT_ID,I.UNIT,I.STREET_NO,I.STREET_NAME,I.PROBLEM,I.POI,I.CROSS_STREET1,I.CROSS_STREET2 FROM INCIDENT I" + " left join rfs r on r.rfs_id=i.rfs_id " + " left outer join street_list s on s.street_id=i.street_name " + " left join street_list sl1 on sl1.street_id=i.cross_street1 " + " left join street_list sl2 on sl2.street_id=i.cross_street2 " + " left outer join rfs_act ra on ra.rfs_id=r.rfs_id " + " left join activity act on act.act_id=ra.act_id " + " where (upper(replace(i.street_no || ' ' || s.pre_dir || ' ' || s.str_name || ' ' || s.str_type || ' ' || s.suf_dir,' ',''))" + " like replace('%" + criteria + "%',' ','')" + " or upper(i.poi) like '%" + criteria + "%' or upper(sl1.str_name) like '%" + criteria + "%'" + " or upper(sl2.str_name) like '%" + criteria + "%'" + " or upper(i.description) like '%" + criteria + "%'" + " or upper(i.memo)  like '%" + criteria + "%' or upper(r.rfs_id) like '%" + criteria + "%'" + " or upper(r.first_name) like '%" + criteria + "%' or upper(r.last_name) like '%" + criteria + "%'" + " or upper(r.street_name) like '%" + criteria + "%' or upper(r.phone_no) like '%" + criteria + "%' or upper(r.email) like '%" + criteria + "%' " + " or upper(act.act_nbr) like '%" + criteria + "%'" + " or upper(i.PROBLEM) like '%" + criteria + "%' ) " +
			// " and I.CREATED >= " + StringUtils.toOracleDate(dateFrom) + " and I.CREATED <= " + StringUtils.toOracleDate(dateTo)+
					statusFilter + " order by i.rfs_id desc,i.created  desc";
			// sql+=" WHERE I.CREATED >= " + StringUtils.toOracleDate(dateFrom) + " and I.CREATED <= " + StringUtils.toOracleDate(dateTo)+ "AND I.INCI_STATUS="+ 7 +" ORDER BY I.CREATED DESC, RFS_ID desc";

			logger.info(sql);

			String incidentIds = "";
			AddressAgent streetAgent = new AddressAgent();
			Map streetNameMap = streetAgent.getStreetNameMap();

			rs = db.select(sql);

			while (rs.next()) {

				String rfsid = rs.getString("RFS_ID");
				String incidentid = rs.getString("INCIDENT_ID");
				incidentIds = incidentIds + ",";
				String requestdate = StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED")));
				String unit = rs.getString("UNIT") != null ? rs.getString("UNIT") : "";
				String streetno = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : "";
				String streetname = rs.getString("STREET_NAME");
				String poi = rs.getString("POI") != null ? rs.getString("POI") : "";
				String crossstreet1 = rs.getString("CROSS_STREET1");
				String crossstreet2 = rs.getString("CROSS_STREET2");
				String problem = rs.getString("PROBLEM");
				String deptIdfromDb = rs.getString("DEPT_ID");
				String inciStatus = rs.getString("INCI_STATUS");

				activityList = new ArrayList();
				/*
				 * showing 'Add Activity' Option Based on Department, Bulding user can add activity only for building related incidents, For other incidents he can only view
				 */
				if (departmentid.equalsIgnoreCase(deptIdfromDb)) {
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(0 + "");
					activityListDetail.setActivityNumber("Add");
					activityList.add(activityListDetail);
				}
				String sql1 = "select R.incident_id,A.act_id,A.act_nbr,A.STATUS from rfs_act R inner join activity A on R.act_id = A.act_id where R.incident_id=" + incidentid;
				rs1 = db.select(sql1);
				logger.info(sql1);
				i = 0;
				k = 0;

				while (rs1.next()) {
					i++;
					String activityid = rs1.getString("ACT_ID");

					int status = rs1.getInt("STATUS");
					if (status == Constants.ACTIVITY_STATUS_NS_CLOSED || status == Constants.ACTIVITY_STATUS_FIRE_CASES_CLOSE) {
						k++;
					}

					String activityNumber = rs1.getString("ACT_NBR");
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(activityid);
					activityListDetail.setActivityNumber(activityNumber);
					activityList.add(activityListDetail);
				}
				rs1.close();

				ListRequestServiceForm listRequestServiceForm = new ListRequestServiceForm();
				listRequestServiceForm.setRfsId(rfsid);
				listRequestServiceForm.setIncidentId(incidentid);
				listRequestServiceForm.setRequestDate(requestdate);
				listRequestServiceForm.setUnit(unit);
				listRequestServiceForm.setStreetNumber(streetno);
				listRequestServiceForm.setStreetName(streetname);
				listRequestServiceForm.setDeptId(deptIdfromDb);

				if (streetname != null) {
					if (streetNameMap.get(streetname) != null) {
						streetname = streetNameMap.get(streetname).toString();
					} else {
						streetname = "";
					}
				}
				if (crossstreet1 != null) {
					if (streetNameMap.get(crossstreet1) != null) {
						crossstreet1 = streetNameMap.get(crossstreet1).toString();
					} else {
						crossstreet1 = "";
					}

				}
				if (crossstreet2 != null) {
					if (streetNameMap.get(crossstreet2) != null) {
						crossstreet2 = streetNameMap.get(crossstreet2).toString();
					} else {
						crossstreet2 = "";
					}
				}

				listRequestServiceForm.setAddress(poi + " " + streetno + " " + streetname + " " + unit + " " + crossstreet1 + " " + crossstreet2);
				listRequestServiceForm.setProblem(problem);
				listRequestServiceForm.setIncistatus(inciStatus);
				listRequestServiceForm.setActivityList(activityList);

				if (i == k && (i != 0) && (k != 0)) {
					// The above condition checks if all the activities of a incident are closed
					// requestList.remove(listRequestServiceForm);
				} else if (listRequestServiceForm.getDeptId() != null) {
					if (listRequestServiceForm.getDeptId().equalsIgnoreCase(departmentid)) {
						requestList.add(listRequestServiceForm);
					} else {
						requestListFromOtherDept.add(listRequestServiceForm);
					}
				}

			}

			requestListMap.put("requestList", requestList);
			requestListMap.put("requestListFromOtherDept", requestListFromOtherDept);
			rs.close();
			logger.debug("returning requestList of size " + requestList.size());
			return requestListMap;
		} catch (Exception e) {
			logger.error("Exception occured in requestServiceList method . ", e);
			throw e;
		}
	}

	public void changeDepartment(ListRequestServiceForm listRequestServiceForm) throws Exception {
		try {
			String[] items = listRequestServiceForm.getSelectedItems();
			Wrapper db = new Wrapper();

			for (int i = 0; i < items.length; i++) {
				String sql = "update incident set DEPT_ID=" + StringUtils.checkNumber(listRequestServiceForm.getDeptId()) + " WHERE INCIDENT_ID = " + items[i];
				db.update(sql);
			}
		} catch (Exception e) {
			logger.error("Exception occured in changeDepartment method . Message-" + e.getMessage());
			throw e;
		}
	}

	public int getLsoId(String activityid) throws Exception {
		try {
			Wrapper db = new Wrapper();
			RowSet rs = null;
			int lsoId = 0;
			String sql = "select addr_id from  activity  where act_id=" + activityid;
			rs = db.select(sql);
			while (rs.next()) {
				lsoId = rs.getInt("addr_id");
			}
			logger.debug(sql);
			return lsoId;
		} catch (Exception e) {
			logger.error("Exception occured in getLsoId method . Message-" + e.getMessage());
			throw e;
		}

	}

	public void changeStatus(String incidentId, String status) throws Exception {
		try {
			logger.debug("changeStatus****" + status);
			String sql = "update incident set INCI_STATUS=" + status + " WHERE INCIDENT_ID = " + incidentId;
			logger.debug("sql=" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Exception occured in changeStatus method . Message-" + e.getMessage());
			throw e;
		}
	}

	public String getAddressFromRfsList(int actId) throws Exception {
		try {
			RowSet rs = null;
			RowSet rs1 = null;
			RowSet rs2 = null;
			RowSet rs3 = null;
			RowSet rs4 = null;
			RowSet rs5 = null;
			String address = "";
			String sql1 = "select I.UNIT,I.STREET_NO,I.STREET_NAME,I.PROBLEM,I.POI,I.CROSS_STREET1,I.CROSS_STREET2 FROM INCIDENT I where INCIDENT_ID=" + "(select INCIDENT_ID from rfs_act where act_id=" + actId + ")";
			logger.info(sql1);
			Wrapper db = new Wrapper();

			rs = db.select(sql1);

			while (rs.next()) {
				String unit = rs.getString("UNIT") != null ? rs.getString("UNIT") : "";
				String streetno = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : "";
				String streetname = rs.getString("STREET_NAME");
				String poi = rs.getString("POI") != null ? rs.getString("POI") : "";
				String crossstreet1 = rs.getString("CROSS_STREET1");
				String crossstreet2 = rs.getString("CROSS_STREET2");
				if (streetname != null) {
					String sql2 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + streetname;
					logger.debug("sql2=" + sql2);
					rs2 = db.select(sql2);
					if (rs2.next()) {
						streetname = (rs2.getString("PRE_DIR") != null ? rs2.getString("PRE_DIR") : "") + " " + (rs2.getString("STR_NAME") != null ? rs2.getString("STR_NAME") : "") + " " + (rs2.getString("STR_TYPE") != null ? rs2.getString("STR_TYPE") : "");
						logger.debug("streetname=" + streetname);
					} else {
						streetname = "";
						logger.debug("streetname=" + streetname);
					}
					rs2.close();
				}
				if (crossstreet1 != null) {
					String sql4 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + crossstreet1;
					logger.debug("sql4=" + sql4);
					rs4 = db.select(sql4);
					if (rs4.next()) {
						crossstreet1 = (rs4.getString("PRE_DIR") != null ? rs4.getString("PRE_DIR") : "") + " " + (rs4.getString("STR_NAME") != null ? rs4.getString("STR_NAME") : "") + " " + (rs4.getString("STR_TYPE") != null ? rs4.getString("STR_TYPE") : "");
						logger.debug("crossstreet1=" + crossstreet1);
					} else {
						crossstreet1 = "";
						logger.debug("crossstreet1=" + crossstreet1);
					}
					rs4.close();
				}
				if (crossstreet2 != null) {
					String sql5 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + crossstreet2;
					logger.debug("sql5=" + sql5);
					rs5 = db.select(sql5);
					if (rs5.next()) {
						crossstreet2 = (rs5.getString("PRE_DIR") != null ? rs5.getString("PRE_DIR") : "") + " " + (rs5.getString("STR_NAME") != null ? rs5.getString("STR_NAME") : "") + " " + (rs5.getString("STR_TYPE") != null ? rs5.getString("STR_TYPE") : "");
						logger.debug("crossstreet2=" + crossstreet2);
					} else {
						crossstreet2 = "";
						logger.debug("crossstreet2=" + crossstreet2);
					}
					rs5.close();
				}

				address = (streetno + " " + streetname + " " + unit + " " + poi + " " + crossstreet1 + "," + crossstreet2);
			}
			return address;
		} catch (Exception e) {
			logger.error("Exception occured in changeDepartment method . Message-" + e.getMessage());
			throw e;
		}
	}

	public List getRequestServiceListForOtherDept(String departmentid, String date) throws Exception {
		logger.info("Entering requestServiceList....");
		User user = new User();

		ActivityListDetail activityListDetail = new ActivityListDetail();
		List requestList = new ArrayList();
		List activityList = null;
		boolean csr = true;

		RowSet rs = null;
		RowSet rs1 = null;
		RowSet rs2 = null;
		RowSet rs3 = null;
		RowSet rs4 = null;
		RowSet rs5 = null;
		int i, k;

		try {
			Wrapper db = new Wrapper();
			/*
			 * logger.debug("userType"+userType); String str1[]= userType.split(","); for(int j=0;j<str1.length;j++){ //Checking if user belongs to Request For Service group if(str1[j].equals(StringUtils.i2s(Constants.GROUPS_REQUEST_FOR_SERVICE))){ csr=false;
			 * 
			 * } }
			 */

			String sql = "select I.DEPT_ID,I.RFS_ID,I.CREATED,I.INCIDENT_ID,I.UNIT,I.STREET_NO,I.STREET_NAME,I.PROBLEM,I.POI,I.CROSS_STREET1,I.CROSS_STREET2 FROM INCIDENT I ";

			// if(csr==false)
			sql += " WHERE I.CREATED <= " + StringUtils.toOracleDate(date) + "AND I.INCI_STATUS=" + 7 + " AND DEPT_ID NOT IN (" + departmentid + ") ORDER BY I.CREATED DESC,RFS_ID desc";
			// if(csr==true)
			// sql+=" WHERE DEPT_ID IN ("+userType+")" + " AND I.CREATED <= " + StringUtils.toOracleDate(date)+"AND I.INCI_STATUS="+ 1 + " ORDER BY RFS_ID";

			logger.info(sql);

			rs = db.select(sql);

			while (rs.next()) {

				String rfsid = rs.getString("RFS_ID");
				String incidentid = rs.getString("INCIDENT_ID");
				String requestdate = StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("CREATED")));
				String unit = rs.getString("UNIT") != null ? rs.getString("UNIT") : "";
				String streetno = rs.getString("STREET_NO") != null ? rs.getString("STREET_NO") : "";
				String streetname = rs.getString("STREET_NAME");
				String poi = rs.getString("POI") != null ? rs.getString("POI") : "";
				String crossstreet1 = rs.getString("CROSS_STREET1");
				String crossstreet2 = rs.getString("CROSS_STREET2");
				String problem = rs.getString("PROBLEM");

				activityList = new ArrayList();
				/*
				 * showing 'Add Activity' Option Based on Department, Bulding user can add activity only for building related incidents, For other incidents he can only view
				 */
				if (departmentid.equals(rs.getString("dept_id")) /* ||departmentid.equals(Constants.DEPARTMENT_NEIGHBOURHOOD_SERVICES+"") */) {
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(0 + "");
					activityListDetail.setActivityNumber("Add");
					activityList.add(activityListDetail);
				}
				String sql1 = "select A.act_id,A.act_nbr from rfs_act R left outer join activity A on R.act_id = A.act_id where R.incident_id=" + incidentid;
				rs1 = db.select(sql1);
				logger.info(sql1);
				i = 0;
				k = 0;
				while (rs1.next()) {
					i++;
					String activityid = rs1.getString("ACT_ID");

					String sql2 = "select STATUS from activity where ACT_ID=" + activityid;
					rs3 = db.select(sql2);
					logger.info(sql2);
					while (rs3.next()) {
						int status = rs3.getInt("STATUS");
						if (status == Constants.ACTIVITY_STATUS_NS_CLOSED) {
							k++;
						}
					}

					String activityNumber = rs1.getString("ACT_NBR");
					activityListDetail = new ActivityListDetail();
					activityListDetail.setActivityId(activityid);
					activityListDetail.setActivityNumber(activityNumber);
					activityList.add(activityListDetail);
				}
				rs1.close();

				ListRequestServiceForm listRequestServiceForm = new ListRequestServiceForm();
				listRequestServiceForm.setRfsId(rfsid);
				listRequestServiceForm.setIncidentId(incidentid);
				listRequestServiceForm.setRequestDate(requestdate);
				listRequestServiceForm.setUnit(unit);
				listRequestServiceForm.setStreetNumber(streetno);
				listRequestServiceForm.setStreetName(streetname);
				if (streetname != null) {
					String sql2 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + streetname;
					rs2 = db.select(sql2);
					if (rs2.next()) {
						streetname = (rs2.getString("PRE_DIR") != null ? rs2.getString("PRE_DIR") : "") + " " + (rs2.getString("STR_NAME") != null ? rs2.getString("STR_NAME") : "") + " " + (rs2.getString("STR_TYPE") != null ? rs2.getString("STR_TYPE") : "");
					} else {
						streetname = "";
						logger.debug("streetname=" + streetname);
					}
					rs2.close();
				}
				if (crossstreet1 != null) {
					String sql4 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + crossstreet1;
					logger.debug("sql4=" + sql4);
					rs4 = db.select(sql4);
					if (rs4.next()) {
						crossstreet1 = (rs4.getString("PRE_DIR") != null ? rs4.getString("PRE_DIR") : "") + " " + (rs4.getString("STR_NAME") != null ? rs4.getString("STR_NAME") : "") + " " + (rs4.getString("STR_TYPE") != null ? rs4.getString("STR_TYPE") : "");
						logger.debug("crossstreet1=" + crossstreet1);
					} else {
						crossstreet1 = "";
						logger.debug("crossstreet1=" + crossstreet1);
					}
					rs4.close();
				}
				if (crossstreet2 != null) {
					String sql5 = "select PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST where STREET_ID=" + crossstreet2;
					logger.debug("sql5=" + sql5);
					rs5 = db.select(sql5);
					if (rs5.next()) {
						crossstreet2 = (rs5.getString("PRE_DIR") != null ? rs5.getString("PRE_DIR") : "") + " " + (rs5.getString("STR_NAME") != null ? rs5.getString("STR_NAME") : "") + " " + (rs5.getString("STR_TYPE") != null ? rs5.getString("STR_TYPE") : "");
						logger.debug("crossstreet2=" + crossstreet2);
					} else {
						crossstreet2 = "";
						logger.debug("crossstreet2=" + crossstreet2);
					}
					rs5.close();
				}

				// if(streetno!="" && streetname!=""){
				// listRequestServiceForm.setAddress(streetno + " " + streetname + " " + unit);
				// }else if(poi!=""){
				// listRequestServiceForm.setAddress(poi);
				// }else{
				// listRequestServiceForm.setAddress(crossstreet1 + "," + crossstreet2);
				// }
				listRequestServiceForm.setAddress(poi + " " + streetno + " " + streetname + " " + unit + " " + crossstreet1 + " " + crossstreet2);

				listRequestServiceForm.setProblem(problem);
				listRequestServiceForm.setActivityList(activityList);

				if (i == k && (i != 0) && (k != 0)) {
					// The above condition checks if all the activities of a incident are closed
					// requestList.remove(listRequestServiceForm);
				} else
					requestList.add(listRequestServiceForm);
			}
			rs.close();
			logger.debug("returning requestList of size " + requestList.size());
			return requestList;
		} catch (Exception e) {
			logger.error("Exception occured in requestServiceList method . Message-" + e.getMessage());
			throw e;
		}
	}

	public List getRFSMatches(String criteria) throws AgentException {
		List results = new ArrayList();
		/*
		 * String sql = "select LTRIM(r.rfs_id) as 'RFS No', LTRIM(i.incident_id) as 'Incident No', i.inci_status as Status, LTRIM(RTRIM(i.street_no+' '+s.pre_dir+' '+s.str_name+' '+s.str_type+' '+s.suf_dir)) as Address, " + "i.poi as POI, " + "'('+sl1.str_name+' '+sl1.str_type+' / '+sl2.str_name+' '+sl1.str_type+')' as Intersection, " + "i.location+' '+i.location_type as Location, " + "lkat.description as Type, last.act_subtype as SubType, i.description as Description  from incident i " + "join rfs r on r.rfs_id=i.rfs_id " + "left join street_list sl1 on sl1.street_id=i.cross_street1 " + "left join street_list sl2 on sl2.street_id=i.cross_street2 " + "left outer join street_list s on s.street_id=i.street_name " + "left outer join street_list s1 on s1.street_id=i.cross_street1 " + "left outer join street_list s2 on s2.street_id=i.cross_street2 " + "join lkup_act_type lkat on lkat.type=i.problem " + "left join lkup_act_subtype last on last.act_subtype_id=i.problem_subtype " + "left outer join rfs_act ra on ra.rfs_id=r.rfs_id "+ "left outer join activity act on act.act_id=ra.act_id "+ "where replace(i.street_no+' '+s.pre_dir+' '+s.str_name+' '+s.str_type+' '+s.suf_dir,' ','') like replace('%"+criteria+"%',' ','') " + "or i.poi like '%"+criteria+"%' or i.apn='"+criteria+"' " + "or s1.str_name like '%"+criteria+"%' or s2.str_name like '%"+criteria+"%' " + "or i.location like '%"+criteria+"%' or i.location_type like '%"+criteria+"%' " + "or lkat.description like '%"+criteria+"%' or last.act_subtype like '%"+criteria+"%' or i.description like '%"+criteria+"%' or i.memo like '%"+criteria+"%' " + "or r.rfs_id like '%"+criteria+"%' or r.first_name like '%"+criteria+"%' or r.last_name like '%"+criteria+"%' " + "or r.street_name like '%"+criteria+"%' or r.phone_no like '%"+criteria+"%' or r.email like '%"+criteria+"%' "+ "or act.act_nbr like '%"+criteria+"%' "+ "order by i.created desc, i.rfs_id desc";
		 */

		String sql = " select r.rfs_id as RFS_No, i.incident_id as Incident_No, i.inci_status Status," + " i.street_no || ' ' || s.pre_dir || ' ' || s.str_name || ' ' || s.str_type || ' ' || s.suf_dir as Address ," + " i.poi POI,'(' || sl1.str_name || ' ' || sl1.str_type || ' / ' || sl2.str_name" + " || ' ' || sl1.str_type || ')' as Intersection , i.description as Description , i.problem as type" + " from incident i left join rfs r on r.rfs_id=i.rfs_id " + " left outer join street_list s on  s.street_id=i.street_name " + " left join street_list sl1 on      sl1.street_id=i.cross_street1 " + " left join street_list sl2 on      sl2.street_id=i.cross_street2 " + " left outer join rfs_act ra on ra.rfs_id=r.rfs_id left outer join activity act on act.act_id=ra.act_id " + " where replace(i.street_no || ' ' || s.pre_dir || ' ' || s.str_name || ' ' || s.str_type || ' ' || s.suf_dir,' ','')" + " like replace('%" + criteria + "%',' ','')" + " or i.poi like '%" + criteria + "%' or sl1.str_name like '%" + criteria + "%'" + " or sl2.str_name like '%" + criteria + "%'" + " or  i.description like '%" + criteria + "%'" + " or i.memo  like '%" + criteria + "%' or r.rfs_id like '%" + criteria + "%' or r.first_name like '%" + criteria + "%' or r.last_name like '%" + criteria + "%'" + " or r.street_name like '%" + criteria + "%' or r.phone_no like '%" + criteria + "%' or r.email like '%" + criteria + "%' or act.act_nbr like '%" + criteria + "%'" + " or i.PROBLEM like '%" + criteria + "%' order by i.created desc, i.rfs_id desc ";

		logger.debug("RFS search query: " + sql);

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				RFSIncident rfs = new RFSIncident();
				rfs.setRfsNo(rs.getString("RFS_No"));
				rfs.setIncidentNo(rs.getString("Incident_No"));
				String status = rs.getString("Status").equals("7") ? "Open" : "Close";
				rfs.setStatus(status);
				String address = rs.getString("Address");
				if (address == null)
					address = "";
				if (address.equals(""))
					address = StringUtils.nullReplaceWithEmpty(rs.getString("POI")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("Intersection"));
				else
					address = address.replaceAll("1 NONLOCATIONAL", "");

				address = address.replaceAll("�", " ");
				address = address.trim();
				rfs.setAddress(address);
				rfs.setDescription(rs.getString("Description"));
				String type = rs.getString("Type");
				rfs.setType(type);
				results.add(rfs);
			}

		} catch (Exception e) {
			logger.error("Exception occured in searching RFS: ", e);
			throw new AgentException("", e);
		}
		return results;
	}

	public int getOpenActivityCount(int actId) throws AgentException {
		int openActCount = 0;
		String query = "select COUNT(ACT.ACT_ID) as ACT_COUNT from RFS_ACT RA left join activity ACT on RA.ACT_ID = ACT.ACT_ID " + " where INCIDENT_ID in (select INCIDENT_ID from RFS_ACT where ACT_ID = " + actId + " )" + " and ACT.STATUS in (select STATUS_ID from lkup_ACT_ST where PSA_ACTIVE = 'Y')";

		logger.debug("Open Activity count query::" + query);
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			rs = db.select(query);
			if (rs != null && rs.next()) {
				openActCount = rs.getInt("ACT_COUNT");
			}

		} catch (Exception e) {
			logger.debug("", e);
			throw new AgentException(",e");
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.warn("", e);
				}
			}
		}
		return openActCount;
	}

	public void closeRFS(int act_id) throws AgentException {
		String query = "UPDATE INCIDENT SET INCI_STATUS = " + Constants.RFS_STATUS_CLOSED + " where " + "INCIDENT_ID in (select INCIDENT_ID from RFS_ACT where ACT_ID = " + act_id + ")";

		logger.debug("Close RFS query::" + query);
		Wrapper db = new Wrapper();
		try {
			db.update(query);
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}
	}

	public void saveCustomLabels(CustomLabelsForm customLabelsForm) {
		logger.debug("Entered saveCustomLabels()..");

		String strCode = customLabelsForm.getLevelCode();
		logger.debug("Get All Ids" + strCode);
		String[] arrCode = StringUtils.stringtoArray(strCode, ",");
		PreparedStatement psmt = null;
		Connection con = null;
		Wrapper con1 = new Wrapper();

		try {

			logger.debug("customLabelsForm.getFieldType()::::::::   " + customLabelsForm.getFieldType());
			int fieldId = con1.getNextId("FIELD_ID");
			String fieldReq = "'N'";

			if (checkFieldLabels(customLabelsForm.getFieldLabel()) == false) {
				fieldReq = "N";

				if (customLabelsForm.getFieldRequired() != null && customLabelsForm.getFieldRequired().equals("on")) {
					fieldReq = "Y";
				}

				String sql = "INSERT INTO LKUP_CUSTOM_FIELDS(FIELD_ID,FIELD_LABEL,FIELD_DESC,FIELD_TYPE,LEVEL_TYPE,LEVEL_TYPE_ID,FIELD_REQ,SEQUENCE,LEVEL_SUB_TYPE_ID) values (?,?,?,?,?,?,?,?,?)";

				logger.debug(sql);
				con = con1.getConnectionForPreparedStatementOnly();
				psmt = con.prepareStatement(sql);

				for (int i = 0; i < arrCode.length; i++) {
					if (arrCode[i].equals("")) {
						// do nothing
					} else {
						psmt.setInt(1, con1.getNextId("FIELD_ID"));

						psmt.setString(2, customLabelsForm.getFieldLabel());
						psmt.setString(3, customLabelsForm.getFieldDesc());
						psmt.setString(4, customLabelsForm.getFieldType());
						psmt.setString(5, customLabelsForm.getLevelType());
						psmt.setInt(6, StringUtils.s2i(arrCode[i]));
						psmt.setString(7, fieldReq);
						psmt.setString(8, customLabelsForm.getSequence());
						psmt.setLong(9, customLabelsForm.getLevelSubTypeId());
						psmt.execute();
					}

				}

				customLabelsForm.setMessage("COMPLETE");
			} else {
				customLabelsForm.setMessage("INCOMPLETE");
				logger.debug("STAGE  INCOMPLETE");
			}

			if (customLabelsForm.getFieldType().equalsIgnoreCase("DROPDOWN")) {

				logger.debug("Drop down options save");

				logger.debug(customLabelsForm.getDropDownOption());

				String customFieldOptions = customLabelsForm.getDropDownOption();

				String[] customFieldsOptionArray = customFieldOptions.split(",");
				con1.beginTransaction();
				for (int i = 0; i < customFieldsOptionArray.length; i++) {

					/*
					 * Blank option is not allowed
					 */
					if (customFieldsOptionArray[i] == null || customFieldsOptionArray[i].trim().equalsIgnoreCase("")) {
						continue;
					}

					int optionId = con1.getNextId("CUS_FIELD_OPT_ID");

					String optionQuery = "INSERT INTO CUSTOM_FIELDS_OPTION (OPTION_ID, FIELD_TYPE_ID, OPTION_LABEL, OPTION_VALUE) VALUES (" + optionId + "," + fieldId + ", '" + customFieldsOptionArray[i] + "', '" + customFieldsOptionArray[i] + "')";

					logger.debug("optionQuery::" + optionQuery);

					con1.addBatch(optionQuery);

					logger.debug("option value" + i + "::" + customFieldsOptionArray[i]);
				}
				con1.executeBatch();
				con1.commitTransaction();
			}
		} catch (Exception e) {
			logger.error("Exception while trying to insert " + e.getMessage());
			customLabelsForm.setMessage("ERROR " + e.getMessage() + "");
		} finally {
			try {
				if (psmt != null)
					psmt.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				// ignored
			}
		}

		logger.debug("Exit saveCustomLabels()..");
		return;

	}

	public List getCustomLabelsMappingList(CustomLabelsForm clForm) throws Exception {
		logger.info("getCustomLabelsMappingList()..");

		List comboList = new ArrayList();
		RowSet rs = null;
		RowSet rs1 = null;

		StringBuffer sb = new StringBuffer();
		try {
			sb.append("select * from LKUP_CUSTOM_FIELDS where ");
			if (!clForm.getFieldLabel().equals("")) {
				sb.append("FIELD_LABEL like '%" + clForm.getFieldLabel() + "%' and ");
			}
			if (!clForm.getFieldDesc().equals("")) {
				sb.append("FIELD_DESC like '%" + clForm.getFieldDesc() + "%' and ");
			}
			if (!clForm.getFieldType().equals("")) {
				sb.append("FIELD_TYPE like '%" + clForm.getFieldType() + "%' and ");
			}
			if (!clForm.getLevelType().equals("0")) {
				sb.append("LEVEL_TYPE like '%" + clForm.getLevelType() + "%' and ");
			}
			if (clForm.getFieldRequired() != null && clForm.getFieldRequired().equals("on")) {
				sb.append("FIELD_REQ = 'Y' and ");
			}

			if (!clForm.getLevelName().equals("null") && !clForm.getLevelName().equals("") && !clForm.getLevelName().equals("0")) {
				sb.append("LEVEL_TYPE_ID IN (" + clForm.getLevelName() + ") and ");
			}

			sb.append(" FIELD_ID !=0 order by FIELD_DESC");
			logger.debug(sb.toString());
			rs = new Wrapper().select(sb.toString());

			while (rs != null && rs.next()) {
				CustomLabelsForm customLabelsForm = new CustomLabelsForm();
				customLabelsForm.setFieldId(rs.getInt("FIELD_ID"));
				customLabelsForm.setFieldLabel(rs.getString("FIELD_LABEL"));
				customLabelsForm.setFieldDesc(rs.getString("FIELD_DESC"));
				customLabelsForm.setFieldType(rs.getString("FIELD_TYPE"));
				customLabelsForm.setLevelType(rs.getString("LEVEL_TYPE"));
				customLabelsForm.setLevelTypeDesc(getLevelDescription(rs.getInt("LEVEL_TYPE_ID"), rs.getString("LEVEL_TYPE")));
				logger.debug("LEVEL_TYPE_ID    :::::::::     " + rs.getInt("LEVEL_TYPE_ID"));
				customLabelsForm.setFieldRequired(rs.getString("FIELD_REQ"));
				if (fieldTypeRemovable(rs.getInt("FIELD_ID"))) {
					customLabelsForm.setDeleteDisplay("Y");
				} else {
					customLabelsForm.setDeleteDisplay("N");
				}
				comboList.add(customLabelsForm);

			}

			rs.close();
			logger.debug("returning getCustomLabelsMappingList of size " + comboList.size());
			return comboList;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in getCustomLabelsMappingList method . Message-" + e.getMessage());
			throw e;
		}

	}

	public boolean fieldTypeRemovable(int id) {
		String sql = "SELECT COUNT(*) AS COUNT FROM ACT_CUSTOM_FIELDS WHERE FIELD_ID=" + id;
		boolean removable = false;

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("COUNT") == 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("Sql error is activityTypeRemovable() " + e.getMessage());
		} catch (Exception e) {
			logger.error("General error is activityTypeRemovable() " + e.getMessage());
		}

		return removable;
	}

	// Delete logical delete with status

	public void deleteCustomLabels(String mapId) throws Exception {
		logger.info("Entered deleteCustomLabels()..");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {

			String sql = "delete from LKUP_CUSTOM_FIELDS where FIELD_ID=" + mapId;
			logger.info(sql);
			new Wrapper().update(sql);

		} catch (Exception e) {
			logger.error("Exception occured in deleteCustomLabels method . Message-" + e.getMessage());
			throw e;
		}
	}

	public CustomLabelsForm getCustomLabelForm(String fieldId) throws Exception {

		logger.info("Entered getCustomLabelForm()..");

		RowSet rs = null;
		RowSet rs1 = null;
		CustomLabelsForm customLabelsForm = new CustomLabelsForm();

		try {
			String sql = "select * from LKUP_CUSTOM_FIELDS where FIELD_ID=" + fieldId;

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				customLabelsForm.setFieldId(rs.getInt("FIELD_ID"));
				customLabelsForm.setFieldLabel(rs.getString("FIELD_LABEL"));
				customLabelsForm.setFieldDesc(rs.getString("FIELD_DESC"));
				customLabelsForm.setFieldType(rs.getString("FIELD_TYPE"));
				customLabelsForm.setLevelType(rs.getString("LEVEL_TYPE"));
				customLabelsForm.setLevelTypeId(rs.getInt("LEVEL_TYPE_ID"));
				customLabelsForm.setFieldRequired(rs.getString("FIELD_REQ"));
				customLabelsForm.setSequence(rs.getString("SEQUENCE"));

				if (rs.getString("LEVEL_SUB_TYPE_ID") != null) {
					customLabelsForm.setLevelSubTypeId(Integer.parseInt(rs.getString("LEVEL_SUB_TYPE_ID")));
				}

				if (rs.getString("FIELD_REQ").equals("Y")) {
					customLabelsForm.setFieldRequired("on");
				} else {
					customLabelsForm.setFieldRequired("off");
				}

			}
			if (customLabelsForm.getFieldType().equalsIgnoreCase("DROPDOWN")) {
				customLabelsForm.setDropDownOption(getOptionsAsString(customLabelsForm.getFieldId()));
			}
			customLabelsForm.setLevelCode(getLevelDescription(customLabelsForm.getLevelTypeId(), "ACTIVITY") + ',');
			customLabelsForm.setLevelStat("" + customLabelsForm.getLevelTypeId() + ",");
			customLabelsForm.setStrFieldIds("" + customLabelsForm.getFieldId() + ",");
			rs.close();

			logger.debug("returning  getCustomLabelForm in getCustomLabelForm()..");
			return customLabelsForm;
		} catch (Exception e) {
			logger.error("Exception occured in getCustomLabelForm method . Message-" + e.getMessage());
			throw e;
		}
	}

	// EDIT updateCustomLabels
	public void updateCustomLabels(CustomLabelsForm customLabelsForm) throws Exception {
		logger.info("Entered updateCustomLabels()..");
		String strCode = customLabelsForm.getLevelCode();
		logger.debug("Get All Ids" + strCode);
		Wrapper db = new Wrapper();

		String[] arrCode = StringUtils.stringtoArray(strCode, ",");
		try {

			String fieldReq = "'N'";
			if (customLabelsForm.getFieldRequired() != null && customLabelsForm.getFieldRequired().equals("on")) {
				fieldReq = "'Y'";
			}

			for (int i = 0; i < arrCode.length; i++) {
				if (arrCode[i].equals("")) {
					// do nothing
				} else {

					if (doUpdate(arrCode[i], customLabelsForm.getFieldId()) == true) {

						String sql = "update LKUP_CUSTOM_FIELDS set  FIELD_LABEL =" + StringUtils.checkString(customLabelsForm.getFieldLabel()) + ",FIELD_DESC=" + StringUtils.checkString(customLabelsForm.getFieldDesc()) + ",FIELD_TYPE=" + StringUtils.checkString(customLabelsForm.getFieldType()) + ",LEVEL_TYPE=" + StringUtils.checkString(customLabelsForm.getLevelType()) + ",LEVEL_TYPE_ID=" + arrCode[i] + ",FIELD_REQ= " + fieldReq + ",SEQUENCE = " + StringUtils.checkString(customLabelsForm.getSequence()) + ",LEVEL_SUB_TYPE_ID = " + customLabelsForm.getLevelSubTypeId() + " where  FIELD_ID=" + customLabelsForm.getFieldId();

						logger.debug("update" + arrCode[i]);
						logger.debug(sql);
						db.update(sql);

					}

				}

			}
			if (customLabelsForm.getFieldType().equalsIgnoreCase("DROPDOWN")) {

				logger.debug("Drop down options save");

				logger.debug(customLabelsForm.getDropDownOption());

				String customFieldOptions = customLabelsForm.getDropDownOption();

				String[] customFieldsOptionArray = customFieldOptions.split(",");
				db.beginTransaction();

				String deleteOptionQuery = "DELETE FROM CUSTOM_FIELDS_OPTION WHERE FIELD_TYPE_ID=" + customLabelsForm.getFieldId();

				db.addBatch(deleteOptionQuery);

				for (int i = 0; i < customFieldsOptionArray.length; i++) {

					/*
					 * Blank option is not allowed
					 */
					if (customFieldsOptionArray[i] == null || customFieldsOptionArray[i].trim().equalsIgnoreCase("")) {
						continue;
					}

					int optionId = db.getNextId("CUS_FIELD_OPT_ID");

					String optionQuery = "INSERT INTO CUSTOM_FIELDS_OPTION (OPTION_ID, FIELD_TYPE_ID, OPTION_LABEL, OPTION_VALUE) " + " VALUES (" + optionId + "," + customLabelsForm.getFieldId() + ", '" + customFieldsOptionArray[i] + "', '" + customFieldsOptionArray[i] + "')";

					logger.debug("optionQuery::" + optionQuery);

					db.addBatch(optionQuery);

					logger.debug("option value" + i + "::" + customFieldsOptionArray[i]);
				}
				db.executeBatch();
				db.commitTransaction();
			}

			customLabelsForm.setMessage("COMPLETE");
			logger.debug("STAGE COMPLETED");

		} catch (Exception e) {
			customLabelsForm.setMessage("ERROR" + e.getMessage() + "");
			logger.error("Exception occured in updateCustomLabels method . Message-" + e.getMessage());
			throw e;
		}

	}

	public boolean doUpdate(String arrCode, int arrFieldIds) throws Exception {

		logger.debug("doUpdate()");

		boolean levelType = false;
		try {

			String sql = "";
			sql = " select * from LKUP_CUSTOM_FIELDS where LEVEL_TYPE_ID=" + arrCode + " and FIELD_ID=" + arrFieldIds;
			logger.debug("---------------" + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				levelType = true;
			}

			logger.debug("BOOLEAN:::::::::::::" + levelType);
			rs.close();

			return levelType;
		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}

	}

	public boolean checkFieldId(String mapId) throws Exception {

		logger.debug("doUpdate()");

		boolean fieldIdPresent = false;
		try {

			String sql = "";
			sql = "  select * from ACT_CUSTOM_FIELDS where FIELD_ID=" + mapId;
			logger.debug("---------------" + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				fieldIdPresent = true;
			}

			logger.debug("BOOLEAN:::::::::::::" + fieldIdPresent);
			rs.close();

			return fieldIdPresent;
		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}

	}

	public boolean checkFieldLabels(String oldFieldLabel) throws Exception {

		logger.debug("checkFieldLabels");

		boolean fieldLabel = false;
		try {

			String sql = "";
			sql = " select * from LKUP_CUSTOM_FIELDS where FIELD_LABEL=" + StringUtils.checkString(oldFieldLabel) + "";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				fieldLabel = true;
			}

			logger.debug("BOOLEAN:::::::::::::" + fieldLabel);
			rs.close();

			return fieldLabel;
		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}

	}

	public static String getLevelTypeAjaxList(String levelId) throws Exception {

		logger.debug("getLevelTypeAjaxList()");

		String levelType = "";
		try {

			String sql = "";
			sql = "select * from lkup_act_type order by DESCRIPTION ";
			logger.debug("---------------" + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				int Id = rs.getInt("TYPE_ID");
				String name = rs.getString("DESCRIPTION").trim();
				String code = (rs.getString("TYPE"));
				levelType = levelType + Id + ";" + name + "||";
			}

			// Iterating over the elements in the set
			logger.debug("getLevelTypeAjaxList() query::" + levelType);
			rs.close();
			return levelType.substring(0, levelType.length() - 2);

		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}

	}

	/**
	 * Get Level Description
	 * 
	 * @param levelTypeId
	 * @param levelType
	 * @return
	 * @throws Exception
	 */
	public String getLevelDescription(int levelTypeId, String levelType) throws Exception {

		logger.debug("getLevelDescription()");

		logger.debug("levelTypeId" + levelTypeId);
		logger.debug("levelType" + levelType);

		String levelTypeDesc = "Unknown";
		try {
			if (levelType.equalsIgnoreCase("ACTIVITY")) {
				String sql = "";
				sql = "select lat.type_id,lp.description as sub_project_type,lat.description as activity_type from lkup_act_type lat join lkup_ptype lp on lat.ptype_id=lp.ptype_id where TYPE_ID=" + levelTypeId + "  order by sub_project_type,activity_type";
				logger.debug(sql);

				RowSet rs = new Wrapper().select(sql);

				while (rs.next()) {
					levelTypeDesc = rs.getString("SUB_PROJECT_TYPE").trim() + " - " + rs.getString("ACTIVITY_TYPE").trim();
				}

				logger.debug("getLevelDescription() query::" + levelType);
				rs.close();

			}
			return levelTypeDesc;

		} catch (Exception e) {

			logger.error(e.getMessage());
			throw e;
		}

	}

	/**
	 * Gets the list of activity types
	 * 
	 * @return
	 */
	public static List<ActivityType> getActivityTypes() throws Exception {
		logger.debug("getActivityTypes()");

		List<ActivityType> activityTypes = new ArrayList<ActivityType>();

		try {
			String sql = "select lat.type_id,lp.description as sub_project_type,lp.dept_code,lat.description as activity_type from lkup_act_type lat join lkup_ptype lp on lat.ptype_id=lp.ptype_id where dept_id in(" + Constants.DEPARTMENT_PUBLIC_WORKS + "," + Constants.DEPARTMENT_PARKING + "," + Constants.DEPARTMENT_HOUSING + ") order by dept_code,sub_project_type,activity_type";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				ActivityType activityType = new ActivityType();
				activityType.setTypeId(rs.getString("TYPE_ID"));
				String description = rs.getString("dept_code") + " - " + rs.getString("sub_project_type") + " - " + rs.getString("activity_type");
				activityType.setDescription(description);
				activityTypes.add(activityType);
			}

			rs.close();

			return activityTypes;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	// Used in Add Activity Tab Screen
	public RowSet getCustomFieldList(int sTypeId) throws Exception {
		logger.info("Entering getCustomFieldList() with id " + sTypeId);

		RowSet rs = null;
		String sql = "";

		try {

			sql = "select field_id,field_label,field_desc,field_type,level_type,level_type_id,field_req,'' as field_value from LKUP_CUSTOM_FIELDS where level_type='ACTIVITY' and level_type_id= " + sTypeId + "  order by FIELD_DESC";
			logger.info(sql);
			rs = new Wrapper().select(sql);

			logger.info("Exiting ActivityAgent getCustomFieldList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getCustomFieldList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	public void saveCustomActvityFields(int actId, CustomField[] customFieldList) throws Exception {
		String sql;

		try {
			for (int i = 0; i < customFieldList.length; i++) {

				String fldValue = "";
				if (customFieldList[i].getFieldType().equalsIgnoreCase("STRING")) {
					fldValue = StringUtils.checkString(customFieldList[i].getFieldValue());
				} else {
					fldValue = "'" + customFieldList[i].getFieldValue() + "'";
				}

				sql = "insert into ACT_CUSTOM_FIELDS(ACT_ID,FIELD_ID,FIELD_VALUE) values (" + actId + "," + customFieldList[i].getFieldId() + "," + fldValue + ")";
				logger.info(sql);
				new Wrapper().update(sql);
			}
		} catch (Exception e) {
			logger.warn("Empty saveCustomActvityFields");
		}

	}

	// Used in View Activity Screen
	public List getCustomFieldActivityList(int actId, int actTypeId) throws Exception {
		logger.info("Entering getCustomFieldActivityList() with id " + actId);

		RowSet rs = null;
		String sql = "";
		List customFields = new ArrayList();

		try {

			sql = "select LCF.FIELD_ID,LCF.FIELD_LABEL,ACF.FIELD_VALUE from LKUP_CUSTOM_FIELDS LCF left join ACT_CUSTOM_FIELDS ACF on ACF.FIELD_ID=LCF.FIELD_ID  and act_id = " + actId + " where LEVEL_TYPE_ID = " + actTypeId + " order by LCF.FIELD_DESC";
			logger.info(sql);
			rs = new Wrapper().select(sql);
			while (rs.next()) {
				CustomField customField = new CustomField();
				customField.setFieldId(rs.getInt("FIELD_ID"));
				customField.setFieldValue(rs.getString("FIELD_VALUE"));
				customField.setFieldLabel(rs.getString("FIELD_Label"));
				customFields.add(customField);

			}
			logger.info("Exiting ActivityAgent getCustomFieldActivityList()");

			return customFields;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getCustomFieldActivityList() :" + e.getMessage());
			throw e;
		}
	}

	// Edit
	public RowSet getEditCustomFieldList(int actId, int actTypeId) throws Exception {
		logger.info("Entering getEditCustomFieldList() with id " + actId);

		RowSet rs = null;
		String sql = "";

		try {

			sql = "select * from LKUP_CUSTOM_FIELDS LCF left join ACT_CUSTOM_FIELDS ACF on ACF.FIELD_ID=LCF.FIELD_ID and act_id=" + actId + " where LEVEL_TYPE_ID = " + actTypeId + " order by LCF.FIELD_DESC";
			logger.info(sql);
			rs = new Wrapper().select(sql);

			logger.info("Exiting ActivityAgent getEditCustomFieldList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getEditCustomFieldList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	// EDIT SAVE
	public void saveEditedCustomActvityFields(int actId, CustomField[] customFieldList) throws Exception {
		String sql;

		logger.info("Entering saveEditedCustomActvityFields() with id " + actId);
		try {
			Wrapper db = new Wrapper();

			String deleteFieldValues = "DELETE FROM ACT_CUSTOM_FIELDS where act_id=" + actId;
			db.update(deleteFieldValues);
			for (int i = 0; i < customFieldList.length; i++) {
				String fldValue = "";
				if (customFieldList[i].getFieldType().equalsIgnoreCase("STRING")) {
					fldValue = StringUtils.checkString(customFieldList[i].getFieldValue());
				} else {
					fldValue = "'" + customFieldList[i].getFieldValue() + "'";
				}

				sql = "INSERT INTO ACT_CUSTOM_FIELDS (ACT_ID, FIELD_ID, FIELD_VALUE) VALUES (" + actId + ", " + customFieldList[i].getFieldId() + ", " + fldValue + ")";
				logger.debug(sql);
				db.update(sql);
			}
		} catch (Exception e) {
			logger.error("Empty saveEditedCustomActvityFields", e);
		}
	}

	public int getActivityTypeId(String type) throws Exception {
		logger.info("Entering getActivityTypeId() with id " + type);

		RowSet rs = null;
		String sql = "";
		int typeId = 0;

		try {

			// rs = new CachedRowSet();
			sql = "select TYPE_ID from LKUP_ACT_TYPE where TYPE='" + type + "'";
			logger.info(sql);
			rs = new Wrapper().select(sql);
			while (rs.next()) {
				typeId = rs.getInt("TYPE_ID");

			}
			logger.info("Exiting ActivityAgent getActivityTypeId()");

			return typeId;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getActivityTypeId() :" + e.getMessage());
			throw e;
		}
	}

	private String getOptionsAsString(int fieldId) throws Exception {
		String query = "SELECT OPTION_VALUE FROM CUSTOM_FIELDS_OPTION WHERE FIELD_TYPE_ID = " + fieldId;

		String optionValues = null;

		Wrapper db = new Wrapper();

		RowSet rs = db.select(query);

		if (rs != null) {
			optionValues = "";
			while (rs.next()) {
				optionValues = optionValues + rs.getString("OPTION_VALUE") + ",";
			}
			if (optionValues.lastIndexOf(",") != -1) {
				optionValues = optionValues.substring(0, optionValues.lastIndexOf(","));
			}
		}
		return optionValues;
	}

	/*
	 * //testing To do in future
	 * 
	 * // EDIT updateCustomLabels public void updateCustomLabelsEdit(CustomLabelsForm customLabelsForm) throws Exception{ logger.info("Entered updateCustomLabels().."); String strCode = customLabelsForm.getLevelCode(); logger.debug("Get All Ids"+strCode); String strFieldId = customLabelsForm.getStrFieldIds(); strFieldId= strFieldId.substring(0,strFieldId.length()-1); logger.debug("Get All Ids"+strFieldId); String[] arrFieldId = StringUtils.stringtoArray(strFieldId,","); String[] arrCode = StringUtils.stringtoArray(strCode,","); try {
	 * 
	 * String fieldReq = "'N'"; if(customLabelsForm.getFieldRequired()!=null && customLabelsForm.getFieldRequired().equals("on")){ fieldReq ="'Y'"; } int k= arrCode.length-arrFieldId.length; if(k>0){ for(int i=0;i<k;i++){ strFieldId+=",0"; } arrFieldId = StringUtils.stringtoArray(strFieldId,","); }
	 * 
	 * logger.debug("arrCode.length"+arrCode.length); logger.debug("arrFieldId.length"+arrFieldId.length);
	 * 
	 * 
	 * for(int i=0;i<arrCode.length && i<arrFieldId.length;i++){ if(arrCode[i].equals("") && arrFieldId[i].equals("")){ //do nothing }else{
	 * 
	 * if(doUpdate(arrCode[i],arrFieldId[i])==true){
	 * 
	 * String sql = "update LKUP_CUSTOM_FIELDS set  FIELD_LABEL ="+StringUtils.checkString(customLabelsForm.getFieldLabel())+",FIELD_DESC="+StringUtils.checkString(customLabelsForm.getFieldDesc())+",LEVEL_TYPE_ID="+arrCode[i]+",FIELD_REQ= "+fieldReq+" where  FIELD_ID=" + arrFieldId[i]; logger.debug("update"+arrCode[i]); logger.debug(sql); new Wrapper().update(sql);
	 * 
	 * }else{ logger.debug("insert"+arrCode[i]); int fieldId = new Wrapper().getNextId("FIELD_ID"); String sql1 ="INSERT INTO LKUP_CUSTOM_FIELDS(FIELD_ID,FIELD_LABEL,FIELD_DESC,FIELD_TYPE,LEVEL_TYPE,LEVEL_TYPE_ID,FIELD_REQ) values ("+fieldId+","+StringUtils.checkString(customLabelsForm.getFieldLabel())+","+StringUtils.checkString(customLabelsForm.getFieldDesc())+","+StringUtils.checkString(customLabelsForm.getFieldType())+","+StringUtils.checkString(customLabelsForm.getLevelType())+","+arrCode[i]+","+fieldReq+")"; logger.debug("insert LKUP_CUSTOM_FIELDS-->"+sql1); new Wrapper().insert(sql1);
	 * 
	 * 
	 * }
	 * 
	 * }
	 * 
	 * } customLabelsForm.setMessage("Updated Successfully"); logger.debug("STAGE  COMPLETED");
	 * 
	 * 
	 * } catch (Exception e) { e.printStackTrace(); logger.error("Exception occured in updateCustomLabels method . Message-" + e.getMessage()); throw e; } }
	 * 
	 * public void saveCustomLabelsOld(CustomLabelsForm customLabelsForm){ logger.debug("Entered saveCustomLabels()..");
	 * 
	 * try{
	 * 
	 * int fieldId = new Wrapper().getNextId("FIELD_ID"); logger.debug("fieldId Id-->"+fieldId); String fieldReq = "'N'";
	 * 
	 * if(customLabelsForm.getFieldRequired()!=null && customLabelsForm.getFieldRequired().equals("on")){ fieldReq ="'Y'"; }
	 * 
	 * String sql1 ="INSERT INTO LKUP_CUSTOM_FIELDS(FIELD_ID,FIELD_LABEL,FIELD_DESC,FIELD_TYPE,LEVEL_TYPE,LEVEL_TYPE_ID,FIELD_REQ) values ("+fieldId+","+StringUtils.checkString(customLabelsForm.getFieldLabel())+","+StringUtils.checkString(customLabelsForm.getFieldDesc())+","+StringUtils.checkString(customLabelsForm.getFieldType())+","+StringUtils.checkString(customLabelsForm.getLevelType())+","+customLabelsForm.getLevelTypeId()+","+fieldReq+")"; logger.debug("insert LKUP_CUSTOM_FIELDS-->"+sql1); new Wrapper().insert(sql1);
	 * 
	 * }catch(Exception e){
	 * 
	 * logger.error("Exception while trying to insert "+e.getMessage()); }
	 * 
	 * logger.debug("Exit saveCustomLabels().."); }
	 * 
	 * // EDIT updateCustomLabels public void updateCustomLabelsOld(CustomLabelsForm customLabelsForm) throws Exception{ logger.info("Entered updateCustomLabels()..");
	 * 
	 * try { String fieldReq = "'N'"; if(customLabelsForm.getFieldRequired()!=null && customLabelsForm.getFieldRequired().equals("on")){ fieldReq ="'Y'"; } // Update LKUP String sql = "update LKUP_CUSTOM_FIELDS set  FIELD_LABEL ="+StringUtils.checkString(customLabelsForm.getFieldLabel())+",FIELD_DESC="+StringUtils.checkString(customLabelsForm.getFieldDesc())+",FIELD_TYPE="+StringUtils.checkString(customLabelsForm.getFieldType())+",LEVEL_TYPE="+StringUtils.checkString(customLabelsForm.getLevelType())+",LEVEL_TYPE_ID="+(customLabelsForm.getLevelTypeId())+",FIELD_REQ= "+fieldReq+" where  FIELD_ID=" + customLabelsForm.getFieldId(); logger.debug(sql); new Wrapper().update(sql);
	 * 
	 * logger.debug("STAGE  COMPLETED"); } catch (Exception e) { e.printStackTrace(); logger.error("Exception occured in updateCustomLabels method . Message-" + e.getMessage()); throw e; } }
	 */

	public List<DropdownValue> getOptionsAsList(int fieldId) throws AgentException {

		String query = "SELECT OPTION_LABEL,OPTION_VALUE FROM CUSTOM_FIELDS_OPTION WHERE FIELD_TYPE_ID = " + fieldId;

		List<DropdownValue> optionList = null;

		Wrapper db = new Wrapper();

		RowSet rs;
		try {
			rs = db.select(query);

			DropdownValue dpValue = null;

			if (rs != null) {
				optionList = new ArrayList<DropdownValue>();
				while (rs.next()) {
					dpValue = new DropdownValue();
					dpValue.setId(rs.getString("OPTION_VALUE"));
					dpValue.setLabel(rs.getString("OPTION_LABEL"));
					optionList.add(dpValue);
				}

			}
		} catch (Exception e) {
			logger.error("error while getting custom fields options");
			throw new AgentException();
		}

		return optionList;
	}

	// Edit
	/**
	 * @param actId
	 * @return
	 * @throws Exception
	 */
	public CustomField[] getEditCustomFieldArray(int actId, String levelType, String actTypeId) throws Exception {
		logger.info("Entering getEditCustomFieldList() with id " + actId);
		RowSet rs = null;
		String sql = "";
		try {

			// rs = new CachedRowSet();
			sql = "select * from LKUP_CUSTOM_FIELDS LCF left join ACT_CUSTOM_FIELDS ACF on ACF.FIELD_ID=LCF.FIELD_ID " + " and ACF.ACT_ID=" + actId + " where LEVEL_TYPE_ID = '" + actTypeId + "' order by LCF.SEQUENCE ASC,LCF.Field_ID asc";
			logger.info(sql);
			rs = new Wrapper().select(sql);
			List<CustomField> customFieldList = new ArrayList<CustomField>();
			CustomField[] customFieldArray = new CustomField[0];
			if (rs != null) {
				customFieldList = new ArrayList<CustomField>();
				CustomField customField = null;
				while (rs.next()) {
					CustomField cf = new CustomField();
					cf.setFieldId(rs.getInt("FIELD_ID"));
					cf.setFieldLabel(rs.getString("FIELD_LABEL"));
					cf.setFieldDesc(rs.getString("FIELD_DESC"));
					cf.setFieldType(rs.getString("FIELD_TYPE"));
					cf.setLevelType(rs.getString("LEVEL_TYPE"));
					cf.setLevelTypeId(rs.getInt("LEVEL_TYPE_ID"));
					cf.setFieldRequired(rs.getString("FIELD_REQ"));
					cf.setFieldValue(rs.getString("FIELD_VALUE"));
				//	cf.setFieldValue(rs.getString("GREEN_HALO"));

					logger.debug("----------------- ");
					logger.debug("CustomField FIELD_ID : " + cf.getFieldId());
					logger.debug("CustomField FIELD_LABEL : " + cf.getFieldLabel());
					logger.debug("CustomField FIELD_DESC : " + cf.getFieldDesc());
					logger.debug("CustomField FIELD_TYPE : " + cf.getFieldType());
					logger.debug("CustomField LEVEL_TYPE : " + cf.getLevelType());
					logger.debug("CustomField LEVEL_TYPE_ID : " + cf.getLevelTypeId());
					logger.debug("CustomField FIELD_REQ : " + cf.getFieldRequired());
					customFieldList.add(cf);
				}

			}
			customFieldArray = customFieldList.toArray(customFieldArray);

			logger.info("Exiting ActivityAgent getEditCustomFieldList()");

			return customFieldArray;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getEditCustomFieldList() : sql : " + sql + ":", e);
			throw new AgentException("", e);
		}
	}

	// Used in Activity Screen
	/**
	 * @param actId
	 * @return
	 * @throws Exception
	 */
	public List getCustomFieldActivityList(int actId, String levelType, String actTypeId) throws Exception {
		logger.info("Entering getCustomFieldActivityList() with id " + actId);
		RowSet rs = null;
		String sql = "";
		List customFields = new ArrayList();
		try {
			// rs = new CachedRowSet();
			sql = "select LCF.FIELD_ID,LCF.FIELD_LABEL,ACF.FIELD_VALUE from LKUP_CUSTOM_FIELDS LCF left join ACT_CUSTOM_FIELDS ACF on ACF.FIELD_ID=LCF.FIELD_ID " + " and act_id = " + actId + " where UPPER(LCF.LEVEL_TYPE) = UPPER('" + levelType + "') AND LEVEL_TYPE_ID = '" + actTypeId + "' order by SEQUENCE ASC,Field_ID asc";
			logger.info(sql);
			rs = new Wrapper().select(sql);
			while (rs.next()) {
				CustomField customField = new CustomField();
				customField.setFieldId(rs.getInt("FIELD_ID"));
				if (rs.getString("FIELD_VALUE") != null && !"null".equalsIgnoreCase(rs.getString("FIELD_VALUE"))) {
					customField.setFieldValue(rs.getString("FIELD_VALUE"));
				} else {
					customField.setFieldValue("");
				}
				customField.setFieldLabel(rs.getString("FIELD_Label"));
				customFields.add(customField);

			}
			logger.info("Exiting ActivityAgent getCustomFieldActivityList()");
			return customFields;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getCustomFieldActivityList() :", e);
			throw new AgentException("", e);
		}
	}

	// Used in Activity Screen
	/**
	 * @param sTypeId
	 * @return
	 * @throws AgentException
	 */
	public CustomField[] getCustomFieldArray(int sTypeId) throws AgentException {
		logger.info("Entering getCustomFieldList() with id " + sTypeId);
		RowSet rs = null;
		String sql = "";
		CustomField[] customFieldArray = new CustomField[0];
		try {
			sql = "select * from LKUP_CUSTOM_FIELDS where level_type='ACTIVITY' and level_type_id=" + sTypeId + " order by SEQUENCE ASC,Field_ID asc";
			logger.info(sql);
			rs = new Wrapper().select(sql);
			logger.info("Exiting ActivityAgent getCustomFieldList()");
			List<CustomField> customFieldList = null;
			if (rs != null) {
				customFieldList = new ArrayList<CustomField>();
				CustomField customField = null;
				while (rs.next()) {
					CustomField cf = new CustomField();
					cf.setFieldId(rs.getInt("FIELD_ID"));
					cf.setFieldLabel(rs.getString("FIELD_LABEL"));
					cf.setFieldDesc(rs.getString("FIELD_DESC"));
					cf.setFieldType(rs.getString("FIELD_TYPE"));
					cf.setLevelType(rs.getString("LEVEL_TYPE"));
					cf.setLevelTypeId(rs.getInt("LEVEL_TYPE_ID"));
					cf.setFieldRequired(rs.getString("FIELD_REQ"));

					logger.debug("----------------- ");
					logger.debug("CustomField FIELD_ID : " + cf.getFieldId());
					logger.debug("CustomField FIELD_LABEL : " + cf.getFieldLabel());
					logger.debug("CustomField FIELD_DESC : " + cf.getFieldDesc());
					logger.debug("CustomField FIELD_TYPE : " + cf.getFieldType());
					logger.debug("CustomField LEVEL_TYPE : " + cf.getLevelType());
					logger.debug("CustomField LEVEL_TYPE_ID : " + cf.getLevelTypeId());
					logger.debug("CustomField FIELD_REQ : " + cf.getFieldRequired());
					customFieldList.add(cf);
				}

			}
			customFieldArray = customFieldList.toArray(customFieldArray);
			return customFieldArray;
		} catch (Exception e) {
			logger.error("Error in ActivityAgent getCustomFieldList() : sql : " + sql + ":", e);
			throw new AgentException("", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException ignore) {
				}
			}
		}
	}

	public String checkExisitingDates(int actId) {
		logger.info("inside checkExisitingDates...");
		String result = "";
		boolean check = false;

		try {
			String sql = "select * from PARKING_ACTIVITY where ACT_ID=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) ";
			logger.debug(sql);

			Set s = new HashSet();
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				result += "'" + StringUtils.date2str(rs.getDate("LNC_DT")) + "',";
				result += getMaxDtPblock(actId, rs.getInt("PBLOCK"));
				result += getMaxDtPblockPreDates(actId, rs.getInt("PBLOCK"));
			}

			if (result.endsWith(",")) {
				result = result.substring(0, result.length() - 1);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting existing online checkExisitingDates " + e.getMessage());
		}
		return result;
	}

	public String getMaxDtPblock(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select max(LNC_DT) as LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				Date d = rs.getDate("LNC_DT");
				Calendar c = Calendar.getInstance();
				c = StringUtils.str2cal(StringUtils.date2str(d));
				c.add(Calendar.DATE, 1);
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
				logger.debug("DAtes33***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				c.add(Calendar.DATE, 1);
				logger.debug("DAtes 44***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}

	public String getMaxDtPblockPreDates(int actId, int pblock) {
		logger.info("inside getMaxDtPblock...");
		String result = "";

		try {
			String sql = "select min(LNC_DT) as LNC_DT from parking_activity where pblock=" + pblock + " and act_id=" + actId + " AND  EXTRACT( YEAR FROM LNC_DT) = EXTRACT(YEAR FROM sysdate) ";
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				Date d = rs.getDate("LNC_DT");
				Calendar c = Calendar.getInstance();
				logger.debug("DAtes%%%%%FINAL *****" + d);
				c = StringUtils.str2cal(StringUtils.date2str(d));
				c.add(Calendar.DATE, -1);
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
				logger.debug("DAtes66***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				c.add(Calendar.DATE, -1);
				logger.debug("DAtes 77***********" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)));
				result += "'" + new SimpleDateFormat("MM/dd/yyyy").format(StringUtils.cal2SQLDate(c)) + "',";
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			// e.printStackTrace();
			logger.error("error in getting  online getMaxDtPblock " + e.getMessage());
		}
		return result;
	}

	public int insertPeopleForParkingActivity(int actId, String userName) throws Exception {
		int peopleId = 0;

		String sql = "select * from PEOPLE where lower(EMAIL_ADDR) = lower('" + userName + "')";
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			peopleId = rs.getInt("PEOPLE_ID");
		}
		if (rs != null)
			rs.close();

		if (peopleId > 0) {
			Wrapper db = new Wrapper();
			String sql1 = "INSERT INTO ACTIVITY_PEOPLE (ACT_ID,PEOPLE_ID,PSA_TYPE) VALUES (" + actId + "," + peopleId + ",'A')";
			logger.info(sql1);
			db.insert(sql1);
		}

		return peopleId;
	}

	public void updateActivity_fee(int actId, int feeUnit, String feeAmt) throws Exception {
		logger.debug("updateActivity_fee" + feeUnit + "feeamt" + feeAmt);
		String sql = "select * from ACTIVITY_FEE where ACTIVITY_ID = " + actId;
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			feeUnit = feeUnit + rs.getInt("FEE_UNITS");
			logger.debug("updateActivity_fee" + feeUnit + "rs" + rs.getInt("FEE_AMNT"));
			int feeamount = rs.getInt("FEE_AMNT") + StringUtils.s2i(feeAmt.substring(1, feeAmt.length()));
			sql = "update activity_fee set fee_units = " + feeUnit + ",fee_amnt = " + feeamount + " where activity_id =" + actId;
			logger.debug(sql);
			new Wrapper().update(sql);
		}
		if (rs != null)
			rs.close();

	}

	public int getFeeIdForLncv(String actType) throws Exception {
		int feeId = 0;

		String sql = "select * from fee where act_type ='" + actType + "' and fee_expiration_dt is null or lower(fee_desc) like '%lncv fees%'";
		logger.debug(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			feeId = rs.getInt("FEE_ID");
		}
		if (rs != null)
			rs.close();

		return feeId;
	}

	public void insertActivity_fee(int actId, int peopleId, int feeId, int feeUnit, String feeAmt, String payfull) throws Exception {
		Wrapper db = new Wrapper();
		if (payfull.equals("Y")) {
			feeUnit = 96;
		}
		String sql = "insert into activity_fee(activity_id,fee_id,people_id,fee_units,fee_amnt) values(" + actId + "," + feeId + "," + peopleId + "," + feeUnit + "," + feeAmt.substring(1, feeAmt.length()) + ")";
		logger.debug(sql);
		db.insert(sql);

	}

	public int getFeeUnits(int actId) {
		logger.info("inside getFeeUnits...");
		int feeUnits = 0;
		try {
			String sql = "select FEE_UNITS from activity_fee where activity_id=" + actId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				feeUnits = rs.getInt("FEE_UNITS");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return feeUnits;
	}

	public int getActivityCreatedBy(int actId) {
		logger.info("inside getActivityCreatedBy...");
		int online = 0;
		try {
			String sql = "select * from ACTIVITY where ACT_ID=" + actId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				if (getIsOnlineUser(rs.getInt("CREATED_BY"))) {
					online = rs.getInt("CREATED_BY");
				}
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return online;
	}

	public boolean getIsOnlineUser(int userId) {
		logger.info("inside getIsOnlineUser...");
		boolean online = false;
		try {
			String sql = "select * from EXT_USER where EXT_USER_ID=" + userId;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				online = true;
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error("error in getting existing online users " + e.getMessage());
		}
		return online;
	}

	/**
	 * get questions
	 * 
	 * @param tempOnlineId
	 * @return
	 * @throws Exception
	 */
	public static List getQuestionAnswers(String actNbr) throws Exception {
		logger.info("getQuestionAnswers(" + actNbr + ")");

		List questionList = new ArrayList();
		String tempQuestionID = "";
		String sql;
		String description = "";
		String acknowledgements = "";

		try {

			sql = "SELECT Q.OPTION_VALUE,LQ.QUES_DESC FROM TEMP_ONLINE_LSO_ADDRESS T JOIN TEMP_ONLINE_QUESTIONAIRE Q on T.TEMP_ONLINEID=Q.TEMP_ONLINEID join LKUP_QUESTIONS LQ on Q.QUES_ID=LQ.QUES_ID where lower(COMBO_NBR) =lower('" + actNbr + "') ";
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);
			int i = 0;
			while (rs != null && rs.next()) {
				tempQuestionID = rs.getString("OPTION_VALUE");
				description = rs.getString("QUES_DESC");
				i = i + 1;
				ApplyOnlinePermitForm applyOnlinePermitForm = new ApplyOnlinePermitForm();
				applyOnlinePermitForm.setTempOnlineID(i);
				applyOnlinePermitForm.setTempQuestionId(tempQuestionID);
				applyOnlinePermitForm.setQuestionaireDescription(description);
				questionList.add(applyOnlinePermitForm);

				logger.debug("questionList.size();" + questionList.size());
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}

		return questionList;
	}
	
	/*public void updateMultiAddress(BusinessTaxActivity businessTaxActivity, int activityId){
		
		String actId="";
		Wrapper db = new Wrapper();
		String query ="select * from MULTI_ADDRESS where ACT_ID="+activityId+"";
		logger.debug("MULTI ADDRESS Query :"+query);
		RowSet rs = null;
		try {
		 rs = new Wrapper().select(query);
		int i = 0;
		if(rs != null){
			while (  rs.next()) {
			actId = rs.getString("ACT_ID");	
				
			}
		}
		
		if(actId == null || actId.equalsIgnoreCase("")){
			// Mailing Address
			int mailingId = db.getNextId("ADDRESS_TYPE_ID");
		String mailingInsertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
				+ "VALUES ("+mailingId+", '"+activityId+"', '1', '"+businessTaxActivity.getMailStreetNumber()+"', '"+businessTaxActivity.getMailStreetName()+"', "
						+ "'',  '"+businessTaxActivity.getMailAttn()+"', '"+businessTaxActivity.getMailUnitNumber()+"', '"+businessTaxActivity.getMailCity()+"',"
								+ " '"+businessTaxActivity.getMailState()+"',  '"+businessTaxActivity.getMailZip()+"', '"+businessTaxActivity.getMailZip4()+"')";
		logger.debug("mailingInsertQuery  :"+mailingInsertQuery);
		db.update(mailingInsertQuery);
		
		int previousId = db.getNextId("ADDRESS_TYPE_ID");
		String previousInsertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
				+ "VALUES ("+previousId+", '"+activityId+"', '2', '"+businessTaxActivity.getPrevStreetNumber()+"', '"+businessTaxActivity.getPrevStreetName()+"', "
						+ "'',  '"+businessTaxActivity.getPreAttn()+"', '"+businessTaxActivity.getPrevUnitNumber()+"', '"+businessTaxActivity.getPrevCity()+"',"
								+ " '"+businessTaxActivity.getPrevState()+"',  '"+businessTaxActivity.getPrevZip()+"', '"+businessTaxActivity.getPrevZip4()+"')";
		logger.debug("previousInsertQuery  :"+previousInsertQuery);
		db.update(previousInsertQuery);
		
		int owner1Id = db.getNextId("ADDRESS_TYPE_ID");
		String owner1InsertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
				+ "VALUES ("+owner1Id+", '"+activityId+"', '3', '"+businessTaxActivity.getOwner1StreetNumber()+"', '"+businessTaxActivity.getOwner1StreetName()+"', "
						+ "'',  '"+businessTaxActivity.getOwner1Attn()+"', '"+businessTaxActivity.getOwner1UnitNumber()+"', '"+businessTaxActivity.getOwner1City()+"',"
								+ " '"+businessTaxActivity.getOwner1State()+"',  '"+businessTaxActivity.getOwner1Zip()+"', '"+businessTaxActivity.getOwner1Zip4()+"')";
		logger.debug("owner1InsertQuery  :"+owner1InsertQuery);
		db.update(owner1InsertQuery);
		
		int owner2Id = db.getNextId("ADDRESS_TYPE_ID");
		String owner2InsertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
				+ "VALUES ("+owner2Id+", '"+activityId+"', '4', '"+businessTaxActivity.getOwner2StreetNumber()+"', '"+businessTaxActivity.getOwner2StreetName()+"', "
						+ "'',  '"+businessTaxActivity.getOwner2Attn()+"', '"+businessTaxActivity.getOwner2UnitNumber()+"', '"+businessTaxActivity.getOwner2City()+"',"
								+ " '"+businessTaxActivity.getOwner2State()+"',  '"+businessTaxActivity.getOwner2Zip()+"', '"+businessTaxActivity.getOwner2Zip4()+"')";
		logger.debug("owner2InsertQuery  :"+owner2InsertQuery);
		db.update(owner2InsertQuery);
		
		}else{
			
		    String	mailingUpdateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+businessTaxActivity.getMailStreetNumber()+"',STREET_NAME1= '"+businessTaxActivity.getMailStreetName()+"',"
					+ "ATTN='"+businessTaxActivity.getMailAttn()+"',UNIT='"+businessTaxActivity.getMailUnitNumber()+"',CITY='"+businessTaxActivity.getMailCity()+"',STATE='"+businessTaxActivity.getMailState()+"',"
							+ "ZIP= '"+businessTaxActivity.getMailZip()+"',ZIP4='"+businessTaxActivity.getMailZip4()+"' WHERE ADDRESS_TYPE_ID=1 AND ACT_ID="+activityId+" ";
			logger.debug("mailingUpdateQuery :"+mailingUpdateQuery);
			db.update(mailingUpdateQuery);
			
			String	previousUpdateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+businessTaxActivity.getPrevStreetNumber()+"',STREET_NAME1= '"+businessTaxActivity.getPrevStreetName()+"',"
					+ "ATTN='"+businessTaxActivity.getPreAttn()+"',UNIT='"+businessTaxActivity.getPrevUnitNumber()+"',CITY='"+businessTaxActivity.getPrevCity()+"',STATE='"+businessTaxActivity.getPrevState()+"',"
							+ "ZIP= '"+businessTaxActivity.getPrevZip()+"',ZIP4='"+businessTaxActivity.getPrevZip4()+"' WHERE ADDRESS_TYPE_ID=2 AND ACT_ID="+activityId+" ";
			logger.debug("previousUpdateQuery :"+previousUpdateQuery);

			db.update(previousUpdateQuery);
			
			String	owner1UpdateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+businessTaxActivity.getOwner1StreetNumber()+"',STREET_NAME1= '"+businessTaxActivity.getOwner1StreetName()+"',"
					+ "ATTN='"+businessTaxActivity.getOwner1Attn()+"',UNIT='"+businessTaxActivity.getOwner1UnitNumber()+"',CITY='"+businessTaxActivity.getOwner1City()+"',STATE='"+businessTaxActivity.getOwner1State()+"',"
							+ "ZIP= '"+businessTaxActivity.getOwner1Zip()+"',ZIP4='"+businessTaxActivity.getOwner1Zip4()+"' WHERE ADDRESS_TYPE_ID=3 AND ACT_ID="+activityId+" ";
			logger.debug("owner1UpdateQuery :"+owner1UpdateQuery);
			db.update(owner1UpdateQuery);
			
			String	owner2UpdateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+businessTaxActivity.getOwner2StreetNumber()+"',STREET_NAME1= '"+businessTaxActivity.getOwner2StreetName()+"',"
					+ "ATTN='"+businessTaxActivity.getOwner2Attn()+"',UNIT='"+businessTaxActivity.getOwner2UnitNumber()+"',CITY='"+businessTaxActivity.getOwner2City()+"',STATE='"+businessTaxActivity.getOwner2State()+"',"
							+ "ZIP= '"+businessTaxActivity.getOwner2Zip()+"',ZIP4='"+businessTaxActivity.getOwner2Zip4()+"' WHERE ADDRESS_TYPE_ID=4 AND ACT_ID="+activityId+" ";
			logger.debug("owner2UpdateQuery :"+owner2UpdateQuery);
			db.update(owner2UpdateQuery);
			
		}
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
			if(rs!=null){
					rs.close();
			}		
			} catch (SQLException e) {
					e.printStackTrace();
			}
			
		}
		
	}*/
	
	public BusinessTaxActivity getMultiActivityAddress(int activityId) throws Exception {
		logger.info("getMultiActivityAddress(" + activityId +  ")");

		BusinessTaxActivity businessTaxActivity = new BusinessTaxActivity();
		RowSet rs = null;
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			sql = "select * from MULTI_ADDRESS where ACT_ID=" + activityId;
			logger.info(sql);

			 rs = db.select(sql);
			if(rs !=null){
			while (rs.next()) {
				if(rs.getInt("ADDRESS_TYPE_ID")==1){
					
					businessTaxActivity.setMailStreetNumber(rs.getString("STREET_NUMBER"));
					businessTaxActivity.setMailStreetName(rs.getString("STREET_NAME1"));
					businessTaxActivity.setMailAttn(rs.getString("ATTN"));
					businessTaxActivity.setMailUnitNumber(rs.getString("UNIT"));
					businessTaxActivity.setMailCity(rs.getString("CITY"));
					businessTaxActivity.setMailState(rs.getString("STATE"));
					businessTaxActivity.setMailZip(rs.getString("ZIP"));
					businessTaxActivity.setMailZip4(rs.getString("ZIP4"));
				}else if(rs.getInt("ADDRESS_TYPE_ID")==2){
					
					businessTaxActivity.setPrevStreetNumber(rs.getString("STREET_NUMBER"));
					businessTaxActivity.setPrevStreetName(rs.getString("STREET_NAME1"));
					businessTaxActivity.setPreAttn(rs.getString("ATTN"));
					businessTaxActivity.setPrevUnitNumber(rs.getString("UNIT"));
					businessTaxActivity.setPrevCity(rs.getString("CITY"));
					businessTaxActivity.setPrevState(rs.getString("STATE"));
					businessTaxActivity.setPrevZip(rs.getString("ZIP"));
					businessTaxActivity.setPrevZip4(rs.getString("ZIP4"));
				}else if(rs.getInt("ADDRESS_TYPE_ID")==3){
					
					businessTaxActivity.setOwner1StreetNumber(rs.getString("STREET_NUMBER"));
					businessTaxActivity.setOwner1StreetName(rs.getString("STREET_NAME1"));
					businessTaxActivity.setOwner1Attn(rs.getString("ATTN"));
					businessTaxActivity.setOwner1UnitNumber(rs.getString("UNIT"));
					businessTaxActivity.setOwner1City(rs.getString("CITY"));
					businessTaxActivity.setOwner1State(rs.getString("STATE"));
					businessTaxActivity.setOwner1Zip(rs.getString("ZIP"));
					businessTaxActivity.setOwner1Zip4(rs.getString("ZIP4"));
				}else if(rs.getInt("ADDRESS_TYPE_ID")==4){
					
					businessTaxActivity.setOwner2StreetNumber(rs.getString("STREET_NUMBER"));
					businessTaxActivity.setOwner2StreetName(rs.getString("STREET_NAME1"));
					businessTaxActivity.setOwner2Attn(rs.getString("ATTN"));
					businessTaxActivity.setOwner2UnitNumber(rs.getString("UNIT"));
					businessTaxActivity.setOwner2City(rs.getString("CITY"));
					businessTaxActivity.setOwner2State(rs.getString("STATE"));
					businessTaxActivity.setOwner2Zip(rs.getString("ZIP"));
					businessTaxActivity.setOwner2Zip4(rs.getString("ZIP4"));
				}
			}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
			if(rs!=null){
					rs.close();
			}		
			} catch (SQLException e) {
					e.printStackTrace();
			}
			
		}
		return businessTaxActivity;
	}	
	
	public MultiAddress[] getMultiAddress(int activityId,String flag) throws Exception {
		logger.info("getMultiAddress(" + activityId +  ")");
		MultiAddress[] multiAddressArray = null;
		RowSet rs = null;
		RowSet rs1 = null;
		List<MultiAddress> multiAddressList = new ArrayList<MultiAddress>();
		String sqlQuery ="select * from LKUP_ADDRESS_TYPE  where ";
		if(flag.equalsIgnoreCase("BT")){
			sqlQuery =sqlQuery+ "FLAG_BT='Y'  order by ID asc";
			}else if(flag.equalsIgnoreCase("BL")){
				sqlQuery =sqlQuery+ "FLAG_BL='Y'  order by ID asc";
			}
		
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			sql = "select MA.*,LAT.ADDRESS_TYPE,LAT.ID AS ADDRESS_TYPE_ID from MULTI_ADDRESS MA left outer join LKUP_ADDRESS_TYPE LAT on LAT.ID=MA.ADDRESS_TYPE_ID"
					+ " where ";
			//sql =sql+ "LAT.FLAG_"+flag+"='Y'";
			if(flag.equalsIgnoreCase("BT")){
			sql =sql+ "LAT.FLAG_BT='Y'";
			}else if(flag.equalsIgnoreCase("BL")){
			sql =sql+ "LAT.FLAG_BL='Y'";
			}
			sql =sql+ "and MA.ACT_ID="+ activityId+" order by MA.ID asc";
			logger.info(sql);

			rs = db.select(sql);
			if(rs !=null){
			while (rs.next()) {
				
				MultiAddress multiAddress = new MultiAddress();
				multiAddress.setId(rs.getString("ADDRESS_TYPE_ID"));
				multiAddress.setAddressType(rs.getString("ADDRESS_TYPE"));
				multiAddress.setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
				multiAddress.setTitle(StringUtils.nullReplaceWithEmpty(rs.getString("TITLE")));
				multiAddress.setStreetNumber(StringUtils.nullReplaceWithEmpty(rs.getString("STREET_NUMBER")));
				multiAddress.setStreetName1(StringUtils.nullReplaceWithEmpty(rs.getString("STREET_NAME1")));
				multiAddress.setStreetName2(StringUtils.nullReplaceWithEmpty(rs.getString("STREET_NAME2")));
				multiAddress.setAttn(StringUtils.nullReplaceWithEmpty(rs.getString("ATTN")));
				multiAddress.setUnit(StringUtils.nullReplaceWithEmpty(rs.getString("UNIT")));
				multiAddress.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("CITY")));
				multiAddress.setState(StringUtils.nullReplaceWithEmpty(rs.getString("STATE")));
				multiAddress.setZip(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP")));
				multiAddress.setZip4(StringUtils.nullReplaceWithEmpty(rs.getString("ZIP4")));
				multiAddressList.add(multiAddress);
				}
			}
			if(multiAddressList !=null || !multiAddressList.isEmpty()){
				logger.debug("sql query :"+sqlQuery);
				boolean addressType =false;
				rs1 = db.select(sqlQuery);
				if(rs1 !=null){
					while (rs1.next()) {
						for(int i=0; i<multiAddressList.size(); i++){
					if(multiAddressList.get(i).getId().equalsIgnoreCase(rs1.getString("ID"))){
						addressType=true;
					}
					logger.debug(" ID 216 :"+rs1.getString("ID")+" addressType :"+addressType);
					}
						if(!addressType){
						MultiAddress multiAddress = new MultiAddress();
						multiAddress.setId(rs1.getString("ID"));
						multiAddress.setAddressType(rs1.getString("ADDRESS_TYPE"));
						multiAddress.setName("");
						multiAddress.setTitle("");
						multiAddress.setStreetNumber("");
						multiAddress.setStreetName1("");
						multiAddress.setStreetName2("");
						multiAddress.setAttn("");
						multiAddress.setUnit("");
						multiAddress.setCity("");
						multiAddress.setState("");
						multiAddress.setZip("");
						multiAddress.setZip4("");
						multiAddressList.add(multiAddress);
						}
						addressType=false;
					}
				}
				logger.debug("addressType  :"+addressType);
			}
			
			if(multiAddressList ==null || multiAddressList.isEmpty()){
			logger.debug("sql query :"+sqlQuery);
			rs1 = db.select(sqlQuery);
			if(rs1 !=null){
				while (rs1.next()) {
					MultiAddress multiAddress = new MultiAddress();
					multiAddress.setId(rs1.getString("ID"));
					multiAddress.setAddressType(rs1.getString("ADDRESS_TYPE"));
					multiAddress.setStreetNumber("");
					multiAddress.setStreetName1("");
					multiAddress.setStreetName2("");
					multiAddress.setAttn("");
					multiAddress.setUnit("");
					multiAddress.setCity("");
					multiAddress.setState("");
					multiAddress.setZip("");
					multiAddress.setZip4("");
					multiAddressList.add(multiAddress);
				}
			}
			}
			
			multiAddressArray = new MultiAddress[multiAddressList.size()];
			for(int i=0;i<multiAddressList.size();i++){
				multiAddressArray[i] = multiAddressList.get(i);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(rs !=null){
			rs.close();
			}
			if(rs1 !=null){
			rs1.close();
			}
			
		}
		return multiAddressArray;
	}	

public void updateMultiAddress(MultiAddress[] multiAddessList, int activityId){
		
		String actId="";
		Wrapper db = new Wrapper();
		/*String query ="select * from MULTI_ADDRESS where ACT_ID="+activityId+" ";
		logger.debug("MULTI ADDRESS Query :"+query);
		*/
		RowSet rs = null;
		try {
		/* rs = new Wrapper().select(query);
		int i = 0;
		if(rs != null){
			while (  rs.next()) {
			actId = rs.getString("ACT_ID");	
				
			}
		}*/
		if(multiAddessList!=null){
		for(int j=0; j< multiAddessList.length; j++){
			MultiAddress rec = (MultiAddress) multiAddessList[j];
			int id = checkMultiAddress(rec.getId() , activityId);
			if(id==0){
				String insertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID,NAME,TITLE, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
						+ "VALUES (SEQUENCE_MULTI_ADDRESS_ID.NEXTVAL, '"+activityId+"', '"+getAddressTypeId(rec.getAddressType())+"', '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getName()))+"', '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getTitle()))+"', '"+rec.getStreetNumber()+"', '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"', "
								+ "'"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',  '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getAttn()))+"', '"+rec.getUnit()+"', '"+rec.getCity()+"',"
										+ " '"+rec.getState()+"',  '"+rec.getZip()+"', '"+rec.getZip4()+"')";
				logger.debug("addressType InsertQuery  :"+insertQuery);
				db.update(insertQuery);
			}else{
				String	updateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+rec.getStreetNumber()+"',STREET_NAME1= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"',STREET_NAME2= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',"
						+ "ATTN='"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getAttn()))+"',UNIT='"+rec.getUnit()+"',CITY='"+rec.getCity()+"',STATE='"+rec.getState()+"',"
						+ "ZIP= '"+rec.getZip()+"',ZIP4='"+rec.getZip4()+"',NAME='"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getName()))+"',TITLE='"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getTitle()))+"' "
						+ "WHERE ADDRESS_TYPE_ID="+getAddressTypeId(rec.getAddressType())+" AND ACT_ID="+activityId+" ";
				logger.debug("updateQuery :"+updateQuery);
				db.update(updateQuery);	
			}
		}
		}
		
		/*if(actId == null || actId.equalsIgnoreCase("")){
			
			for(int j=0; j< multiAddessList.length; j++){
			MultiAddress rec = (MultiAddress) multiAddessList[j];
			
			//int mailingId = db.getNextId("ADDRESS_TYPE_ID");
			String insertQuery ="INSERT INTO MULTI_ADDRESS (ID, ACT_ID, ADDRESS_TYPE_ID,NAME,TITLE, STREET_NUMBER, STREET_NAME1, STREET_NAME2, ATTN, UNIT, CITY, STATE, ZIP, ZIP4) "
					+ "VALUES (SEQUENCE_MULTI_ADDRESS_ID.NEXTVAL, '"+activityId+"', '"+getAddressTypeId(rec.getAddressType())+"', '"+rec.getName()+"', '"+rec.getTitle()+"', '"+rec.getStreetNumber()+"', '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"', "
							+ "'"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',  '"+rec.getAttn()+"', '"+rec.getUnit()+"', '"+rec.getCity()+"',"
									+ " '"+rec.getState()+"',  '"+rec.getZip()+"', '"+rec.getZip4()+"')";
			logger.debug("addressType InsertQuery  :"+insertQuery);
			db.update(insertQuery);
			}
				}else{
			
					for(int j=0; j< multiAddessList.length; j++){
						MultiAddress rec = (MultiAddress) multiAddessList[j];
						
						String	updateQuery ="UPDATE MULTI_ADDRESS SET STREET_NUMBER='"+rec.getStreetNumber()+"',STREET_NAME1= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName1()))+"',STREET_NAME2= '"+StringUtils.nullReplaceWithEmpty(StringUtils.handleSingleQuot(rec.getStreetName2()))+"',"
								+ "ATTN='"+rec.getAttn()+"',UNIT='"+rec.getUnit()+"',CITY='"+rec.getCity()+"',STATE='"+rec.getState()+"',"
								+ "ZIP= '"+rec.getZip()+"',ZIP4='"+rec.getZip4()+"',NAME='"+rec.getName()+"',TITLE='"+rec.getTitle()+"' "
								+ "WHERE ADDRESS_TYPE_ID="+getAddressTypeId(rec.getAddressType())+" AND ACT_ID="+activityId+" ";
						logger.debug("updateQuery :"+updateQuery);
						db.update(updateQuery);
			
					}
		}*/
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
			if(rs!=null){
					rs.close();
			}		
			} catch (SQLException e) {
					e.printStackTrace();
			}
			
		}
		
	}
	
	public String getAddressTypeId(String addressType){
	Wrapper db = new Wrapper();
	ResultSet rs = null;
	String addressTypeId = "";
	String sql = "select * from LKUP_ADDRESS_TYPE where ADDRESS_TYPE='"+addressType+"' ";
	
	logger.info(sql);

	try {
		rs = db.select(sql);
	
	if(rs !=null){
	while (rs.next()) {
		
		addressTypeId = rs.getString("ID");
		
	}
	}
	} catch (Exception e) {
		e.printStackTrace();
	}finally {
		try {
		if(rs != null){
			
				rs.close();
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	return addressTypeId;

}
	
	public int checkMultiAddress(String addressTypeId,int actId){
		Wrapper db = new Wrapper();
		ResultSet rs = null;
		int id = 0;
		String sql = "select * from MULTI_ADDRESS where ADDRESS_TYPE_ID='"+addressTypeId+"' AND ACT_ID="+actId+" ";
		
		logger.debug("checkMultiAddress : "+sql);

		try {
			rs = db.select(sql);
		
		if(rs !=null){
		while (rs.next()) {
			
			id = rs.getInt("ID");
			
		}
		}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
			if(rs != null){
				
					rs.close();
			}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return id;

	}

	public String getWMRequiredFlag(String type) {
		logger.debug("getWMRequiredFlag(" + type + ")");		
		RowSet rs = null;
		String wmFlag = "";
		try {

			String sql = "SELECT WM_REQUIRED FROM LKUP_ACT_TYPE WHERE TYPE = '" + type+"'"; 
			logger.info(sql);
			rs = new Wrapper().select(sql);
			
			if (rs.next()) {
				wmFlag = rs.getString("WM_REQUIRED");
			}
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			
		}finally{
			try{
				if(rs != null){
				    rs.close();
				}
			}catch(Exception e){
				
			}
		}
		return wmFlag;
	}
	
	/*
	 * get Green Halo method. 
	 */
	
	/**
	 * Gets the activity for a given activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public GreenHalo getGreenHalo(int activityId) throws Exception {
		logger.info("getActivity(" + activityId + ")");

		GreenHalo greenHalo = new GreenHalo();
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from activity_green_halo where act_id=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {

				greenHalo.setId(rs.getInt("id")); 
				logger.debug("Green Halo ID is  " + greenHalo.getId());
				
				greenHalo.setActId(rs.getInt("act_id")); 
				logger.debug("Green Halo ACT_ID is  " + greenHalo.getActId());
				
				greenHalo.setCompanyName(rs.getString("company_name"));
				logger.debug("Green Halo CompanyName is  " + greenHalo.getCompanyName());
				
				
				greenHalo.setPhone(rs.getString("phone"));
				logger.debug("Green Halo Phone is  " + greenHalo.getPhone());
				
				greenHalo.setProjectName(rs.getString("project_Name"));
				logger.debug("Green Halo projectName is  " + greenHalo.getProjectName());
				
				greenHalo.setEmail(rs.getString("email"));
				logger.debug("Green Halo email is  " + greenHalo.getEmail());
				
				greenHalo.setStreet(rs.getString("street"));
				logger.debug("Green Halo street is  " + greenHalo.getStreet());
				
				greenHalo.setAptSuite(rs.getString("apt_Suite"));
				logger.debug("Green Halo email is  " + greenHalo.getAptSuite());
				
				greenHalo.setZipcode(rs.getString("zipcode"));
				logger.debug("Green Halo zipcode is  " + greenHalo.getZipcode());
				String startDate = StringUtils.date2str(rs.getDate("project_start_date"));
				greenHalo.setProjectStartDate(startDate);
				logger.debug("Green Halo ProjectStartDate is  " +startDate);
				
				String endDate = StringUtils.date2str(rs.getDate("project_end_date"));
				greenHalo.setProjectEndDate(endDate);
				logger.debug("Green Halo ProjectEndDate is  " + endDate);
				
				greenHalo.setValue(rs.getInt("value")); 
				logger.debug("Green Halo VALUE is  " + greenHalo.getValue());
				
				greenHalo.setSquareFootage(rs.getInt("square_Footage")); 
				logger.debug("Green Halo squareFootage is  " + greenHalo.getSquareFootage());
				
				greenHalo.setDescription(rs.getString("description")); 
				logger.debug("Green Halo description is  " + greenHalo.getDescription());
				
				greenHalo.setProjectType(rs.getString("project_Type")); 
				logger.debug("Green Halo projectType is  " + greenHalo.getProjectType());
				
				greenHalo.setBuildingType(rs.getString("building_Type")); 
				logger.debug("Green Halo buildingType is  " + greenHalo.getBuildingType());
				
				greenHalo.setPermit(rs.getString("permit")); 
				logger.debug("Green Halo permit is  " + greenHalo.getPermit());
				
				greenHalo.setPermitSqFootage(rs.getInt("permit_sq_Footage")); 
				logger.debug("Green Halo PermitSqFootage is  " + greenHalo.getPermitSqFootage());
				
				greenHalo.setPermitNote(rs.getString("permit_note")); 
				logger.debug("Green Halo permitNote is  " + greenHalo.getPermitNote());
				
				greenHalo.setPermitValue(rs.getInt("permit_value")); 
				logger.debug("Green Halo PermitValue is  " + greenHalo.getPermitValue());
				
				greenHalo.setPermitProjectType(rs.getString("permit_Project_Type")); 
				logger.debug("Green Halo permitProjectType is  " + greenHalo.getPermitProjectType());
			}
			return greenHalo;
		} catch (Exception e) {
			logger.error("Exception in getGreanHalo()" + e.getMessage());
			throw e;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}
	
	public void setGreenHalo(GreenHalo greenHalo) {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			if (greenHalo != null) {
				logger.debug("Grean Halo is not null ");
				if (isGreenHaloExists(greenHalo.getId())) {
					String updateQuery =" UPDATE ACTIVITY_GREEN_HALO SET ACT_ID="+  greenHalo.getActId() + ", COMPANY_NAME= '"+  greenHalo.getCompanyName() +
										"', PHONE= '"+greenHalo.getPhone() +"', PROJECT_NAME='" + greenHalo.getProjectName()+"', EMAIL='"+ greenHalo.getEmail() +
										"', STREET='"+greenHalo.getStreet()+"', APT_SUITE='"+ greenHalo.getAptSuite()+"', ZIPCODE='"+greenHalo.getZipcode()+
										"'" +", PROJECT_START_DATE=TO_DATE('"+ greenHalo.getProjectStartDate()+"', 'MM/DD/YYYY'), PROJECT_END_DATE=TO_DATE('"+ greenHalo.getProjectEndDate() +
										"' , 'MM/DD/YYYY'), VALUE="+ greenHalo.getValue()+", SQUARE_FOOTAGE="+greenHalo.getSquareFootage()+", DESCRIPTION='"+greenHalo.getDescription()+
										"', PROJECT_TYPE='"+greenHalo.getProjectType()+"', BUILDING_TYPE='"+ greenHalo.getBuildingType()+"', PERMIT='"+
										greenHalo.getPermit() + "', PERMIT_SQ_FOOTAGE="+ greenHalo.getPermitSqFootage()+", PERMIT_NOTE='"+ greenHalo.getPermitNote()+
										"', PERMIT_VALUE="+greenHalo.getPermitValue()+ ", PERMIT_PROJECT_TYPE='"+ greenHalo.getPermitProjectType()+ "'"+
										" WHERE id =" + greenHalo.getId();
					logger.debug("UpdateQuery  :"+updateQuery);
					db.update(updateQuery);
					logger.debug("update of Green Halo  completed successfully ");
				} else {
					logger.debug("Before adding of Green Halo");
					String	insertQuery =" INSERT INTO ACTIVITY_GREEN_HALO (ID, ACT_ID, COMPANY_NAME, PHONE, PROJECT_NAME, EMAIL, STREET, APT_SUITE, "+
							" ZIPCODE, PROJECT_START_DATE, PROJECT_END_DATE, VALUE, SQUARE_FOOTAGE, DESCRIPTION, PROJECT_TYPE, "+
							" BUILDING_TYPE, PERMIT, PERMIT_SQ_FOOTAGE, PERMIT_NOTE, PERMIT_VALUE, PERMIT_PROJECT_TYPE) "+
							" VALUES (GREEN_HALO_ID_SEQ.NEXTVAL,"+  greenHalo.getActId() + ", '"+  greenHalo.getCompanyName() + "', '"+ greenHalo.getPhone()+ "', '"+ 
							greenHalo.getProjectName()+ "', '"+ greenHalo.getEmail()+ "', '"+greenHalo.getStreet()+ "', '"+greenHalo.getAptSuite()+ "', '"+
							greenHalo.getZipcode()+ "', TO_DATE('"+ greenHalo.getProjectStartDate() + "', 'MM/DD/YYYY'), "+
							"TO_DATE('"+greenHalo.getProjectEndDate()+ "', 'MM/DD/YYYY'), "+ 
							greenHalo.getValue()+ ", "+
							greenHalo.getSquareFootage()+ ", '"+greenHalo.getDescription()+ "', '"+ greenHalo.getProjectType()+ "', '"+
							greenHalo.getBuildingType()+ "', '"+ greenHalo.getPermit() + "', "+ greenHalo.getPermitSqFootage()+ ", '"+
							greenHalo.getPermitNote()+ "', "+greenHalo.getPermitValue()+ ", '"+greenHalo.getPermitProjectType()+ "')";
					logger.debug("InsertQuery :"+insertQuery);
					int result = db.update(insertQuery);	
					logger.debug("Green Halo successfully added.(result:"+ result+")");

				}
				
				String	updateQuery ="UPDATE ACTIVITY SET GREEN_HALO = 'Y' WHERE ACT_ID = " + greenHalo.getActId();
				logger.debug("updateQuery :"+updateQuery);
				db.update(updateQuery);	
				logger.debug("Activity.Green_Halo flag is set successfully.");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			try {
			if(rs!=null){
					rs.close();
			}		
			} catch (SQLException e) {
					e.printStackTrace();
			}
			
		}
	}
	
	private boolean isGreenHaloExists(int id) throws AgentException{
		return (isGreenHaloCount(id)>0);
	}
	
	private int isGreenHaloCount(int id) throws AgentException{
		int count = 0;
		String query = "select COUNT(ID) as GREEN_HALO_COUNT from activity_green_halo where ID = " + id;

		logger.debug("Green Halo count query::" + query);
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			rs = db.select(query);
			if (rs != null && rs.next()) {
				count = rs.getInt("GREEN_HALO_COUNT");
			}

		} catch (Exception e) {
			logger.debug("", e);
			throw new AgentException(",e");
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.warn("", e);
				}
			}
		}
		return count;
	}
	
	public void setGreenHaloProject(GreenHaloNewProjectResponse response, int actId) {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			GreenHalo greenHalo =  getGreenHalo(actId);
			if (response != null) {
				logger.debug("Grean Halo new project response is not null ");
					logger.debug("Before adding of Green Halo New Project Response.");
					String	insertQuery =" INSERT INTO ACT_GREEN_HALO_PRJ_RES (ID, GH_ID, USER_ID, CONTRACTOR_ID, PROJECT_ID, WMP_NUMBER, STATUS, NOTE) "+
							" VALUES (ACT_GREEN_HALO_PRJ_RES_ID_SEQ.NEXTVAL, "+  greenHalo.getId() + ", '"+  response.getUserId() + "', '"+ response.getContractorId()+ "', '"+ 
							response.getProjectId()+ "', '"+ response.getWmpNumber()+ "', '"+response.getStatus()+ "', '"+response.getNote()+ "')";
					logger.debug("InsertQuery :"+insertQuery);
					int result = db.update(insertQuery);	
					logger.debug("Green Halo New Project response successfully added (Result:"+result+")");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			try {
			if(rs!=null){
					rs.close();
			}		
			} catch (SQLException e) {
					e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * Gets the activity for a given activity id.
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public GreenHaloNewProjectResponse getGreenHaloProjectResponse(int actId) throws Exception {
		logger.info("actID(" + actId + ")");

		GreenHaloNewProjectResponse response = new GreenHaloNewProjectResponse();
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from ACT_GREEN_HALO_PRJ_RES where gh_id = (select id from activity_green_halo where act_id = "+actId+" )";
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {

				response.setId(rs.getInt("id")); 
				logger.debug("Green Halo project response ID is  " + response.getId());
				
				response.setGhId(rs.getInt("gh_id")); 
				logger.debug("Green Halo project response  gh_id is  " + response.getGhId());
				
				response.setUserId(rs.getInt("USER_ID"));
				logger.debug("Green Halo project response USER_ID is  " + response.getUserId());
				
				
				response.setContractorId(rs.getInt("CONTRACTOR_ID"));
				logger.debug("Green Halo project response CONTRACTOR_ID is  " + response.getContractorId());
				
				response.setProjectId(rs.getInt("PROJECT_ID"));
				logger.debug("Green Halo project response project ID is  " + response.getProjectId());
				
				response.setWmpNumber(rs.getString("WMP_NUMBER"));
				logger.debug("Green Halo project response WmpNnumber is  " + response.getWmpNumber() );
				
				response.setStatus(rs.getString("STATUS"));
				logger.debug("Green Halo project response Status is  " + response.getStatus());
				
				response.setNote(rs.getString("note"));
				logger.debug("Green Halo project response Note is  " + response.getNote());
			}
			return response;
		} catch (Exception e) {
			logger.error("Exception in getGreanHalo()" + e.getMessage());
			return null;
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
	}
	
	public Map<Integer,Integer> getGreenHaloProjectIds() throws AgentException{
		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		int count = 0;
		String query = "SELECT ID, PROJECT_ID FROM ACT_GREEN_HALO_PRJ_RES ";

		logger.debug("Get all green Halo Project ID's query::" + query);
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			rs = db.select(query);
			if (rs != null) {
				while (rs.next()) {
					map.put(rs.getInt("ID"), rs.getInt("PROJECT_ID"));
				}
			}

		} catch (Exception e) {
			logger.debug("", e);
			throw new AgentException(",e");
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.warn("", e);
				}
			}
		}
		return map;
	}
	
	private boolean isGreenHaloFinalResponseExists(int id) throws AgentException{
		return (isGreenHaloFinalResponseCount(id)>0);
	}
	
	private int isGreenHaloFinalResponseCount(int id) throws AgentException{
		int count = 0;
		String query = "select COUNT(ID) as GREEN_HALO_COUNT from ACT_GREEN_HALO_FINAL_RESPONSE where GHR_ID = " + id;

		logger.debug("Green Halo final response count query::" + query);
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			rs = db.select(query);
			if (rs != null && rs.next()) {
				count = rs.getInt("GREEN_HALO_COUNT");
			}

		} catch (Exception e) {
			logger.debug("", e);
			throw new AgentException(",e");
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					logger.warn("", e);
				}
			}
		}
		return count;
	}
	public void setUpdateFinalResponse(GreenHaloFinalResponse greenHalo) {
		Wrapper db = new Wrapper();
		RowSet rs = null;
		try {
			if (greenHalo != null) {
				logger.debug("Grean Halo final response is not null ");
				if (isGreenHaloFinalResponseExists(greenHalo.getGhrId())) {
					String updateQuery =" UPDATE ACT_GREEN_HALO_FINAL_RESPONSE SET DIV_RATE="+  greenHalo.getDivRate() + ", STATUS= '"+  greenHalo.getStatus() +
										"', NOTE= '"+greenHalo.getNote() +"' WHERE id =" + greenHalo.getId();
					logger.debug("UpdateQuery  :"+updateQuery);
					db.update(updateQuery);
					logger.debug("update of Green Halo final response completed successfully ");
				} else {
					logger.debug("Before adding of Green Halo final response");
					String	insertQuery =" INSERT INTO ACT_GREEN_HALO_FINAL_RESPONSE (ID, GHR_ID, PROJECT_ID, DIV_RATE, STATUS, NOTE) "+
							" VALUES (ACT_GREEN_HALO_RESPONSE_id_seq.NEXTVAL, "+  greenHalo.getGhrId() + ", "+  greenHalo.getProjectId()+", " + greenHalo.getDivRate() + ", '"+  greenHalo.getStatus()+ "', '"+ 
							greenHalo.getNote()+ "')";
					logger.debug("InsertQuery :"+insertQuery);
					int result = db.update(insertQuery);	
					logger.debug("Green Halo final response successfully added.(result:"+ result+")");

				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}finally {
			try {
			if(rs!=null){
					rs.close();
			}		
			} catch (SQLException e) {
					e.printStackTrace();
			}
			
		}
	}
	
	public String getActivityNumber(int actId) throws AgentException {
		String sql = "select ACT_NBR from Activity where ACT_ID = "+ actId;
		String actNbr = "";
		Wrapper db = new Wrapper();
		RowSet rs = null;
		
		try {
			rs = db.select(sql);
			
			if(rs != null) {
				while(rs.next()) {
					actNbr = rs.getString("ACT_NBR");
				}
			}
			
		} catch (Exception e) {
			logger.error("",e);
			throw new AgentException("",e);
		}
		
		
		return actNbr;
		
		
	}
	
	public static ActivityDetail getActivityDetailFor311(int actId) {
		logger.info("Entered getActivityDetailFor311...");
		String sql = "select CREATED_BY,EXT_REF_NUMBER from Activity where ACT_ID = "+ actId;
		int userId = -1;
		String refNumber = "";
		
		ActivityDetail activityDetail = new ActivityDetail();
		
		Wrapper db = new Wrapper();
		RowSet rs = null;
		
		try {
			rs = db.select(sql);
			
			if(rs.next()) {
				userId = rs.getInt("CREATED_BY");
				refNumber = rs.getString("EXT_REF_NUMBER");
			}
			
		} catch (Exception e) {
			logger.error("",e);
		}finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}

		User createdBy = new User(userId);
		activityDetail.setCreatedBy(createdBy);
		activityDetail.setExtRefNumber(refNumber);
		
		logger.debug("getActivityCreatedByInternal returning ext number : "+activityDetail.getExtRefNumber() + " user as "+activityDetail.getCreatedBy().getUserId());
		return activityDetail;
	}
	
	public static List<ActivityDetail> getActivityListDetailFor311(int subProjId) {
		logger.info("Entered getActivityListDetailFor311...");
		String sql = "SELECT CREATED_BY,EXT_REF_NUMBER FROM ACTIVITY WHERE SPROJ_ID = "+ subProjId +" AND ACT_TYPE="+StringUtils.checkString(Constants.THREE11_SERVICE_ACT_TYPE);
		List<ActivityDetail> activityDetailList = new ArrayList<ActivityDetail>();
		
		Wrapper db = new Wrapper();
		RowSet rs = null;
		
		try {
			rs = db.select(sql);
			
			while(rs.next()) {
				ActivityDetail ad = new ActivityDetail();
				User createdBy = new User(rs.getInt("CREATED_BY"));
				ad.setCreatedBy(createdBy);
				ad.setExtRefNumber(rs.getString("EXT_REF_NUMBER"));
				activityDetailList.add(ad);
			}
			
		} catch (Exception e) {
			logger.error("",e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
		logger.debug("getActivityListDetailFor311 returning");
		return activityDetailList;
	}
	
	public String get311StatusName(int actId) throws Exception {
		logger.info("Entered get311StatusName...");
		String status = null;
		RowSet rs = null;
		String sql = "select status_311 from lkup_act_st where status_id = (select status from activity where act_id="+actId+")";
		
		logger.debug("SQL is " + sql);
		try {
			rs = new Wrapper().select(sql);
			if (rs != null && rs.next()) {
				status = rs.getString("status_311");
			}

			return status;
		} catch (Exception e) {
			logger.error("Exception in method get311StatusName ");
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
		}
	}
	
	public List<ActivitySubType311> getActivitySubType311Mapping(int actId) {
		logger.info("Entered getActivitySubType311Mapping..."+actId);
		
		String sql = "select * from  ACT_SUBTYPE_311_MAPPING where ACT_SUBTYPE_ID in (select ACT_SUBTYPE_ID from act_subtype where act_id="+actId+")";
		List<ActivitySubType311> activitySubTypeMappingList = new ArrayList<ActivitySubType311>();
		
		Wrapper db = new Wrapper();
		RowSet rs = null;
		
		try {
			rs = db.select(sql);
			
			while(rs.next()) {
				ActivitySubType311 at = new ActivitySubType311();
				at.setMappingId(rs.getInt("MAPPING_ID"));
				at.setSubTypeId(rs.getInt("ACT_SUBTYPE_ID"));
				at.setViolationType(rs.getString("VIOLATION_TYPE"));
				at.setViolationType311(rs.getString("VIOLATION_TYPE_311"));
				activitySubTypeMappingList.add(at);
			}
		} catch (Exception e) {
			logger.error("",e);
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
		logger.debug("getActivitySubType311Mapping returning");
		return activitySubTypeMappingList;
	}
}
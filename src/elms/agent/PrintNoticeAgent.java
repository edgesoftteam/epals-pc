package elms.agent;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.common.Agent;
import elms.app.finance.ActivityFee;
import elms.app.finance.Payment;
import elms.app.inspection.Inspection;
import elms.app.lso.Use;
import elms.app.people.People;
import elms.common.Constants;
import elms.exception.AgentException;
import elms.security.User;
import elms.util.Operator;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * This class performs the database related functions for the Violations.
 * 
 * @author
 * 
 */
public class PrintNoticeAgent extends Agent {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(PrintNoticeAgent.class.getName());

	/**
	 * default constructor
	 */
	public PrintNoticeAgent() {
	}

	private static final String owenrQuery = "select P.NAME,P.ADDR,P.CITY,P.STATE,P.ZIP,P.PHONE,P.PEOPLE_TYPE_ID from PEOPLE P" + " JOIN ACTIVITY_PEOPLE AP ON AP.PEOPLE_ID = P.PEOPLE_ID where ACT_ID=?";

	private static final String complaintAddressQuery = "SELECT * FROM V_ADDRESS_LIST va LEFT OUTER JOIN ACTIVITY A ON va.ADDR_ID = A.ADDR_ID" + " LEFT OUTER JOIN STREET_LIST ON va.STREET_ID=STREET_LIST.STREET_ID " + " WHERE A.ACT_ID = ?";
	private static final String violationsQuery = "SELECT LC.CONDITION_CODE,C.SHORT_TEXT,C.Text,A.ACT_NBR,C.UPDATE_DT,C.CREATION_DT FROM CONDITIONS C LEFT OUTER JOIN ACTIVITY A ON A.ACT_ID=C.LEVEL_ID" + " LEFT OUTER JOIN LKUP_CONDITIONS LC ON C.LIBRARY_ID=LC.CONDITION_ID" + " WHERE A.ACT_ID = ? AND COND_LEVEL='A'  AND C.COMPLETE ! ='Y' order by CREATION_DT asc,CONDITION_CODE ASC";
	private static final String inspectorQuery = "SELECT First_Name , last_name,U.TITLE  FROM CODE_ENFORCEMENT CE " + " JOIN LKUP_CE_TYPE LK_CE ON CE.CE_TYPE_ID=LK_CE.CE_TYPE_ID" + " JOIN USERS U ON CE.OFFICER_ID = U.USERID WHERE CE.CE_ID=?";
	// private static final String ceDetailQuery = "SELECT CE.*,LNF.FILENAME FROM CODE_ENFORCEMENT CE,LKUP_NOTICE_FILENAME LNF WHERE CE.CE_TYPE_ID = LNF.CE_TYPE_ID AND CE_ID=?";
	private static final String ceDetailQuery = "SELECT CE.*,LNT.NAME AS TEMPLATE_NAME ,LNT.TEMPLATETXT AS TEMPLATETXT,LNT.NOTICE_TYPE_ID AS NOTICE_TYPE_ID , LNT.ACTION_ID AS ACTION_ID ,LNT.NOTICE_TEMPLATE_ID AS NOTICE_TEMPLATE_ID FROM NOTICES CE LEFT OUTER JOIN LKUP_NOTICE_TEMPLATE LNT  ON CE.NOTICE_TYPE_ID = LNT.NOTICE_TYPE_ID WHERE CE.NOTICE_ID=?";

	private static final String activityDetailQuery = "select ACT_NBR,APPLIED_DATE,EXP_DATE,lat.DESCRIPTION from activity a left join lkup_ACT_TYPE lat on a.ACT_TYPE = lat.TYPE where ACT_ID = ?";

	private static final String inspectiondateQuery = "select * from inspection where act_id = ? and actn_code not in (" + Constants.OPEN_INSPECTION_CODE + "," + Constants.SYSTEMATIC_INSP_STAT_CANCELLED + ") order by inspection_dt desc";

	private static final String inspectionCurrentdateQuery = "select * from inspection where act_id = ? and actn_code not in (" + Constants.OPEN_INSPECTION_CODE + "," + Constants.SYSTEMATIC_INSP_STAT_CANCELLED + ") order by inspection_dt desc";

	private static final String defConditonQuery = "select LC.CONDITION_CODE, C.SHORT_TEXT from conditions c left outer join LKUP_CONDITIONS  LC on c.LIBRARY_ID= LC.CONDITION_ID  where c.level_id =? and c.cond_level ='A'";

	private static final String insptimeQuery = "select * from inspection where act_id = ?  order by TIMESTAMP desc";

	private String sortComplaintAddress = "";

	/**
	 * 
	 * @param violation
	 * @return List
	 */
	public List getViolationDetails(String violation) {
		logger.info("Entered getViolationDetails(" + violation + ")");
		List violationList = new ArrayList();
		try {

			logger.debug(violation);
			String sql = "SELECT CE.CE_ID,CE.ACT_ID,LK_CE.CE_TYPE,CE.OFFICER_ID,(U.LAST_NAME||' '||U.FIRST_NAME) as officer,";
			sql = sql + " CE.SUB_TYPE,CE.CE_DATE,LK_CE.CATEGORY FROM CODE_ENFORCEMENT CE ";
			sql = sql + " JOIN LKUP_CE_TYPE LK_CE ON CE.CE_TYPE_ID=LK_CE.CE_TYPE_ID ";
			sql = sql + " JOIN USERS U ON CE.OFFICER_ID = U.USERID WHERE CE.CE_ID=" + violation;
			Wrapper db = new Wrapper();
			List actionList = new ArrayList();
			RowSet rs = db.select(sql);
			logger.debug(sql);
			if (rs.next()) {
				violationList.add(rs.getInt("ce_id") + "");
				violationList.add(rs.getString("ce_type")); // using setName for
															// setting CE Type
															// Description
				violationList.add(rs.getString("officer")); // using
															// setOfficerId for
															// Setting
															// OfficerName
				violationList.add(rs.getString("category"));
				violationList.add(dateFormat(rs.getDate("ce_date")));
				String sql1 = "select P.NAME,P.ADDR,P.CITY,P.STATE,P.ZIP,P.PHONE,P.PEOPLE_TYPE_ID from PEOPLE P JOIN ACTIVITY_PEOPLE AP ON AP.PEOPLE_ID = P.PEOPLE_ID where ACT_ID =" + rs.getInt("act_id");
				RowSet rs1 = db.select(sql1);
				while (rs1.next()) {
					if (rs1.getString("PEOPLE_TYPE_ID").equals("8") || rs1.getString("PEOPLE_TYPE_ID").equals("11")) {
						violationList.add(rs1.getString("name"));
						violationList.add(rs1.getString("addr"));
						violationList.add(rs1.getString("city"));
						violationList.add(rs1.getString("state"));
						violationList.add(rs1.getString("zip"));
						violationList.add(rs1.getString("phone"));
					}
				}
				rs1.close();
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		logger.info("returning violationList");
		return violationList;
	}

	public Map getNoticeDetails(String ceId, User user, int actId) throws Exception {
		Map noticeDetail = new HashMap();

		String sql = "SELECT * FROM V_NOTICE_PRINT WHERE NOTICE_ID=" + ceId;

		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);

			logger.debug(" SQL " + sql);
			boolean result1 = false;

			int rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
			}
			logger.debug(" size " + rowcount);
			if (rowcount <= 0) {
				sql = "SELECT * FROM V_NOTICE_PRINT WHERE ROWNUM <2";
				logger.debug(" SQL " + sql);
				rs = db.select(sql);
				// get result set meta data

			} else {
				result1 = true;
				logger.debug(" result1 " + result1);
			}
			if (rs != null && rs.next()) {
				// get result set meta data
				ResultSetMetaData rsMetaData = rs.getMetaData();
				int numberOfColumns = rsMetaData.getColumnCount();

				// get the column names; column indexes start from 1

				for (int i = 1; i < numberOfColumns + 1; i++) {

					String columnName = rsMetaData.getColumnName(i);
					String columnValue = rs.getString(columnName);
					if (!Operator.hasValue(columnValue) || !result1) {
						columnValue = "";
					}
					noticeDetail.put("#" + columnName + "#", columnValue);
				}

			}

			sql = "SELECT FEE_AMOUNT FROM V_NOTICE_FEE WHERE ACTIVITY_ID=" + actId;
			logger.debug(" sql " + sql);
			db = new Wrapper();

			float balance = 0;
			float amount = 0;
			float totalAmount = 0;
			float paidAmount = 0;
			rs = null;

			rs = db.select(sql);
			// get result set meta data
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();

			// rsMetaData = rs.getMetaData();
			// numberOfColumns = rsMetaData.getColumnCount();

			while (rs.next()) {
				if (!rs.getString("feeFlagFour").equals("1")) {
					amount = Float.parseFloat(rs.getString("feeAmount"));
					balance = balance + amount;
					totalAmount = totalAmount + amount;
				}

			}

			noticeDetail.put("#FEE_AMOUNT#", StringUtils.f2$(totalAmount));

			sql = "SELECT * FROM V_CUSTOM_FIELDS_NOTICES WHERE ACT_ID=" + actId;
			logger.debug(" sql " + sql);
			db = new Wrapper();
			rs = null;
			rs = db.select(sql);
			// get result set meta data
			boolean result = false;

			rowcount = 0;
			if (rs.last()) {
				rowcount = rs.getRow();
				rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
			}
			logger.debug(" size" + rowcount);
			if (rowcount <= 0) {
				sql = "SELECT * FROM V_CUSTOM_FIELDS_NOTICES WHERE ROWNUM <2";
				logger.debug(sql);
				rs = db.select(sql);
				// get result set meta data

			} else {
				result = true;
			}
			if (rs != null && rs.next()) {
				// get result set meta data
				rsMetaData = rs.getMetaData();
				numberOfColumns = rsMetaData.getColumnCount();

				// get the column names; column indexes start from 1

				for (int i = 1; i < numberOfColumns + 1; i++) {

					String columnName = rsMetaData.getColumnName(i);
					String columnValue = rs.getString(columnName);
					if (!Operator.hasValue(columnValue) || !result) {
						columnValue = "";
					}
					noticeDetail.put("#" + columnName + "#", columnValue);
				}

			}

		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
					logger.warn("Not able to close the Rowset");
				}
			}
		}

		return noticeDetail;
	}

	private Map getFireNoticeDetail(Map noticeDetail, String ceId, String actId) throws Exception {

		String inspectionDate = getCurrentInspectionDetail(actId);
		noticeDetail.put(Constants.CODE_INSPECTION_DATE, inspectionDate);

		return noticeDetail;
	}

	public String replaceValueText(String noticeTemplate, Map map, int actId) throws Exception {
		logger.debug("replaceValueText method()");

		String sql = "SELECT * FROM V_NOTICE_PRINT WHERE ROWNUM < 2 ";

		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);
			// get result set meta data
			ResultSetMetaData rsMetaData = rs.getMetaData();
			int numberOfColumns = rsMetaData.getColumnCount();

			if (rs != null && rs.next()) {
				// get result set meta data
				rsMetaData = rs.getMetaData();
				numberOfColumns = rsMetaData.getColumnCount();

				// get the column names; column indexes start from 1
				for (int i = 1; i < numberOfColumns + 1; i++) {
					String columnName = rsMetaData.getColumnName(i);
					// noticeDetail.put(columnName , rs.getString(columnName));

					if (map.get("#" + columnName + "#") != null) {

						noticeTemplate = noticeTemplate.replaceAll("#" + columnName + "#", map.get("#" + columnName + "#").toString());
					} else {
						noticeTemplate = noticeTemplate.replaceAll("#" + columnName + "#", "");
					}
				}

			}
			rs.close();

			if (map.get("#FEE_AMOUNT#") != null) {

				noticeTemplate = noticeTemplate.replace("#FEE_AMOUNT#", map.get("#FEE_AMOUNT#").toString());
			} else {
				noticeTemplate = noticeTemplate.replace("#FEE_AMOUNT#", "");
			}

			sql = "SELECT * FROM V_CUSTOM_FIELDS_NOTICES WHERE ROWNUM < 2 ";
			db = new Wrapper();
			rs = null;

			rs = db.select(sql);
			// get result set meta data
			rsMetaData = rs.getMetaData();
			numberOfColumns = rsMetaData.getColumnCount();

			if (rs != null && rs.next()) {
				// get result set meta data
				rsMetaData = rs.getMetaData();
				numberOfColumns = rsMetaData.getColumnCount();

				// get the column names; column indexes start from 1
				for (int i = 1; i < numberOfColumns + 1; i++) {
					String columnName = rsMetaData.getColumnName(i);
					// noticeDetail.put(columnName , rs.getString(columnName));

					if (map.get("#" + columnName + "#") != null) {
						noticeTemplate = noticeTemplate.replaceAll("#" + columnName + "#", map.get("#" + columnName + "#").toString());
					} else {
						noticeTemplate = noticeTemplate.replaceAll("#" + columnName + "#", "");
					}
				}

			}
			rs.close();

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
					logger.warn("Not able to close the Rowset");
				}
			}

		}
		return noticeTemplate;

	}

	private Map getBSDNoticeDetail(Map noticeDetail, String ceId, String actId) throws Exception {

		// noticeDetail.put("",ceDetailMap.get(""));

		String inspectorName = getInspectorDetail(ceId);
		logger.debug("inspectorName::" + inspectorName);

		Map activityDetail = getActivityDetail(actId);
		String caseNumber = "";
		String appliedDate = "";
		String expDate = "";
		String actType = "";

		if (activityDetail.get("actNbr") != null) {
			caseNumber = activityDetail.get("actNbr").toString().trim();
		}
		if (activityDetail.get("appliedDate") != null) {
			appliedDate = activityDetail.get("appliedDate").toString().trim();
		}
		if (activityDetail.get("expDate") != null) {
			expDate = activityDetail.get("expDate").toString().trim();
		}
		if (activityDetail.get("actType") != null) {
			actType = activityDetail.get("actType").toString().trim();
		}

		String subType = getActivitySubtypeDetail(actId);
		noticeDetail.put(Constants.CODE_ACT_SUBTYPE, subType);

		noticeDetail.put(Constants.CODE_CASE_NO, caseNumber);
		noticeDetail.put(Constants.CODE_APPLIED_DATE, appliedDate);
		noticeDetail.put(Constants.CODE_EXP_DATE, expDate);
		noticeDetail.put(Constants.CODE_ACT_TYPE_DESC, actType);
		noticeDetail.put(Constants.CODE_INSPECTOR_NAME, inspectorName);

		Map<String, ActivityFee> gusdFeeDetail = getGUSDFeeDetail(actId);
		if (gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_COMMERCIAL) != null) {
			ActivityFee fee = gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_COMMERCIAL);
			noticeDetail.put(Constants.CODE_GUSD_COMM_SQFT, fee.getFeeUnits());
			noticeDetail.put(Constants.CODE_GUSD_COMM_AMNT, fee.getFeeAmount());
		}

		if (gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL) != null) {
			ActivityFee fee = gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL);
			noticeDetail.put(Constants.CODE_GUSD_RESI_SQFT, fee.getFeeUnits());
			noticeDetail.put(Constants.CODE_GUSD_RESI_AMNT, fee.getFeeAmount());
		}

		if (gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL_CUMULATIVE) != null) {
			ActivityFee fee = gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL_CUMULATIVE);
			noticeDetail.put(Constants.CODE_GUSD_RESI_CUMU_SQFT, fee.getFeeUnits());
			noticeDetail.put(Constants.CODE_GUSD_RESI_CUMU_AMNT, fee.getFeeAmount());
		}

		if (gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL_CURRENT) != null) {
			ActivityFee fee = gusdFeeDetail.get(Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL_CURRENT);
			noticeDetail.put(Constants.CODE_GUSD_RESI_CURRENT_SQFT, fee.getFeeUnits());
			noticeDetail.put(Constants.CODE_GUSD_RESI_CURRENT_AMNT, fee.getFeeAmount());
		}

		Map owner = getOwnerDetail(actId);
		if (owner.get("name") != null) {
			noticeDetail.put(Constants.CODE_OWNER_NAME, owner.get("name").toString());
		} else {
			noticeDetail.put(Constants.CODE_OWNER_NAME, "");
		}

		if (owner.get("apn") != null) {
			noticeDetail.put(Constants.APN_DETAIL, owner.get("apn").toString());
		} else {
			noticeDetail.put(Constants.APN_DETAIL, "");
		}
		if (owner.get("address") != null) {
			noticeDetail.put(Constants.CODE_OWNER_ADDRESS, owner.get("address").toString());
		} else {
			noticeDetail.put(Constants.CODE_OWNER_ADDRESS, "");
		}
		if (owner.get("phone") != null) {
			noticeDetail.put(Constants.CODE_OWNER_PHONE, owner.get("phone"));
		} else {
			noticeDetail.put(Constants.CODE_OWNER_PHONE, "");
		}

		noticeDetail.put(Constants.CODE_COMPLAINT_ADDRESS, getComplaintAddress(actId));

		noticeDetail.put(Constants.CODE_SHORT_COMPLAINT_ADDRESS, this.sortComplaintAddress);

		Map violation = getViolations(actId);
		logger.debug("violation" + violation.get("violation"));
		logger.debug("newViolation" + violation.get("newViolation"));
		logger.debug("oldViolation" + violation.get("oldViolation"));

		noticeDetail.put(Constants.CODE_VIOLATIONS, violation.get("violation"));
		noticeDetail.put(Constants.CODE_NEW_VIOLATIONS, violation.get("newViolation"));
		noticeDetail.put(Constants.CODE_OLD_VIOLATIONS, violation.get("oldViolation"));

		Inspection lastClosedInspection = new InspectionAgent().getLastClosedInspection(actId);

		String lastInspectionDate = "";
		if (lastClosedInspection != null) {
			lastInspectionDate = StringUtils.toNoticeDateFormat(lastClosedInspection.getDate());
		}

		noticeDetail.put(Constants.CODE_LAST_CLOSED_INSPECTION_DATE, lastInspectionDate);

		FinanceAgent financeAgent = new FinanceAgent();

		double activityBalance = financeAgent.getActivityBalance(actId);
		DecimalFormat df = new DecimalFormat("#0.00");

		if (activityBalance < 0) {
			activityBalance = activityBalance * (-1);
		}

		noticeDetail.put(Constants.CODE_ACT_BALANCE, df.format(activityBalance));

		Payment payment = financeAgent.getLastPayment();

		String lastPaymentDate = StringUtils.cal2NoticeDateFormat(payment.getPaymentDate());

		noticeDetail.put(Constants.LAST_PAYMENT_DATE, lastPaymentDate);

		ActivityAgent actAgent = new ActivityAgent();

		List<People> contractors = new ArrayList<People>();
		contractors = actAgent.getPeopleForActivity(Integer.parseInt(actId), Constants.PEOPLE_CONTRACTOR);

		if (contractors != null && contractors.size() > 0) {
			People contractor = contractors.get(0);
			if (contractor.getName() != null) {
				noticeDetail.put(Constants.CODE_CONTRACTOR_NAME, contractor.getName());
			} else {
				noticeDetail.put(Constants.CODE_CONTRACTOR_NAME, "");
			}

			if (contractor.getAddress() != null) {
				noticeDetail.put(Constants.CODE_CONTRACTOR_ADDRESS, contractor.getAddress());
			} else {
				noticeDetail.put(Constants.CODE_CONTRACTOR_ADDRESS, "");
			}

			if (contractor.getCity() != null) {
				noticeDetail.put(Constants.CODE_CONTRACTOR_CITY_STATE_ZIP, contractor.getCity() + " " + contractor.getState() + " " + contractor.getZipCode());
			} else {
				noticeDetail.put(Constants.CODE_CONTRACTOR_CITY_STATE_ZIP, "");
			}

			if (contractor.getLicenseNbr() != null) {
				noticeDetail.put(Constants.CODE_CONTRACTOR_LICENSE_NO, contractor.getLicenseNbr());
			} else {
				noticeDetail.put(Constants.CODE_CONTRACTOR_LICENSE_NO, "");
			}

			if (contractor.getBusinessLicenseNbr() != null) {
				noticeDetail.put(Constants.CODE_CONTRACTOR_STATE_LICENSE_NO, contractor.getBusinessLicenseNbr());
			} else {
				noticeDetail.put(Constants.CODE_CONTRACTOR_STATE_LICENSE_NO, "");
			}

			if (contractor.getBusinessLicenseExpires() != null) {
				noticeDetail.put(Constants.CODE_CONTRACTOR_STATE_EXP, StringUtils.cal2NoticeDateFormat(contractor.getBusinessLicenseExpires()));
			} else {
				noticeDetail.put(Constants.CODE_CONTRACTOR_STATE_EXP, "");
			}
		} else {
			noticeDetail.put(Constants.CODE_CONTRACTOR_NAME, "");
			noticeDetail.put(Constants.CODE_CONTRACTOR_ADDRESS, "");
			noticeDetail.put(Constants.CODE_CONTRACTOR_CITY_STATE_ZIP, "");
			noticeDetail.put(Constants.CODE_CONTRACTOR_LICENSE_NO, "");
			noticeDetail.put(Constants.CODE_CONTRACTOR_STATE_LICENSE_NO, "");
			noticeDetail.put(Constants.CODE_CONTRACTOR_STATE_EXP, "");
		}

		// get the lso id for the given psa id and psa type.
		int lsoId = LookupAgent.getLsoIdForPsaId(actId, "A");
		String lsoType = LookupAgent.getLSOType(lsoId);

		AddressAgent useAgent = new AddressAgent();
		List<Use> useList = useAgent.getLSOUse(lsoId, lsoType);
		String use = "";

		if (useList != null && useList.size() > 0) {
			for (int i = 0; i < useList.size(); i++) {
				use = use + useList.get(i).getDescription() + ", ";
			}
		}

		if (use.lastIndexOf(",") != -1) {
			use = use.substring(0, use.lastIndexOf(","));
		}
		noticeDetail.put(Constants.CODE_LSO_USE, use);

		return noticeDetail;
	}

	private Map getBusinessInfo() {
		Map businessInfoMap = new HashMap();

		return businessInfoMap;
	}

	public Map getOwnerDetail(String act_id) throws Exception {
		Map owner = new HashMap();

		try {
			logger.debug("entered into PrintNoticeAgent.getOwnerDetail");

			String sql = "";
			String addr_id = "";
			String lso_type = "";
			String lso_id = "";

			Wrapper db = new Wrapper();

			sql = "select addr_id,lso_type,LSO_ID from v_activity_address where ACT_ID =" + act_id;
			RowSet numberRs = db.select(sql);

			while (numberRs.next()) {

				String name = "";
				String address1 = "";
				String address2 = "";
				String address3 = "";
				String apn = "";
				String address = "";

				addr_id = numberRs.getString("ADDR_ID");
				logger.debug("addr_id" + addr_id);
				lso_type = numberRs.getString("LSO_TYPE");
				logger.debug("lso_type" + lso_type);
				lso_id = numberRs.getString("LSO_ID");
				logger.debug("lso_id::" + lso_id);

				if (lso_type.equalsIgnoreCase("S")) {
					sql = "select * from land_structure  where structure_id =" + lso_id;
					RowSet rs = db.select(sql);

					if (rs.next()) {
						lso_id = rs.getString("LAND_ID");
					}
					rs.close();
					sql = "select * from v_lso_owner where  LSO_ID =" + lso_id + " and LSO_TYPE = 'L' order by LAST_UPDATED";
				}

				else {
					sql = "select * from v_lso_owner where  addr_id =" + addr_id + " order by LAST_UPDATED";
				}
				logger.debug("final sql query is " + sql);

				RowSet rs1 = db.select(sql);

				if (rs1.next()) {
					owner.put("name", StringUtils.nullReplaceWithEmpty(rs1.getString("NAME")));
					logger.debug("Name is " + name);
					String zip4 = "";
					if (zip4.length() < 9 || zip4.equalsIgnoreCase(null) || zip4.equalsIgnoreCase("")) {
						zip4 = "";
					} else {
						zip4 = rs1.getString("ZIP4");
					}
					logger.debug("Zip 4 is " + zip4);
					address1 = StringUtils.nullReplaceWithEmpty(rs1.getString("STR_NO")) + " " + StringUtils.nullReplaceWithEmpty(rs1.getString("STR_MOD")) + " " + StringUtils.nullReplaceWithEmpty(rs1.getString("PRE_DIR"));
					address2 = StringUtils.nullReplaceWithEmpty(rs1.getString("STR_NAME")) + " " + StringUtils.nullReplaceWithEmpty(rs1.getString("STR_TYPE")) + " " + StringUtils.nullReplaceWithEmpty(rs1.getString("SUF_DIR")) + " " + StringUtils.nullReplaceWithEmpty(rs1.getString("UNIT"));
					address1 = address1 + "" + address2;
					address3 = StringUtils.nullReplaceWithEmpty(rs1.getString("CITY")) + " , " + StringUtils.nullReplaceWithEmpty(rs1.getString("STATE")) + " " + StringUtils.nullReplaceWithEmpty(rs1.getString("ZIP")) + " " + zip4;
					address = address1 + "\r" + address3;

					logger.debug("address     " + address);
					owner.put("address", address);
					apn = StringUtils.nullReplaceWithEmpty(rs1.getString("APN"));
					owner.put("apn", apn);
					owner.put("phone", StringUtils.nullReplaceWithEmpty(rs1.getString("PHONE")));
				}
				if (numberRs != null) {
					numberRs.close();
				}
			}
		} catch (Exception e) {
			logger.error(" Exception in PublicWorksReportAgent -- getOwnerGrpElement() method " + e.getMessage());
		}

		return owner;
	}

	private Map getViolations(String act_id) throws Exception {
		String violation = "";
		String newViolation = "";
		String oldViolation = "";
		String actNbr = "";
		String newViolationDate = "";
		boolean isNew = true;
		Map map = null;

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(violationsQuery);
			logger.debug("violationsQuery::" + violationsQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			int i = 1;
			int newViolationsCount = 1;
			int oldViolationsCount = 1;
			while (rs != null && rs.next() && rs.getString("Text") != null) {
				// int j=
				// StringUtils.nullReplaceWithEmpty((rs.getString("Text"))
				// ).indexOf("-");

				// logger.debug(i+
				// "violationsQuery text ::"+StringUtils.nullReplaceWithEmpty
				// (rs.getString("Text").substring(j++).trim()));
				violation = violation + "\n" + i + ". " + StringUtils.nullReplaceWithEmpty(rs.getString("Text").trim()) + "\n";

				actNbr = rs.getString("ACT_NBR");
				if (isNew) {
					newViolationDate = rs.getString("UPDATE_DT");
					isNew = false;

				}

				if (rs.getString("UPDATE_DT").equalsIgnoreCase(newViolationDate)) {
					newViolation = newViolation + "\n" + newViolationsCount + ". " + StringUtils.nullReplaceWithEmpty(rs.getString("Text").trim()) + "\n";
					// newViolation = newViolation + i +". " +
					// StringUtils.nullReplaceWithEmpty
					// (rs.getString("CONDITION_CODE")) + " " +
					// StringUtils.nullReplaceWithEmpty
					// (rs.getString("SHORT_TEXT")) + "^";
					actNbr = rs.getString("ACT_NBR");
					newViolationsCount++;

				} else {
					oldViolation = oldViolation + "\n" + oldViolationsCount + ". " + StringUtils.nullReplaceWithEmpty(rs.getString("Text").trim()) + "\n";
					// oldViolation = oldViolation + i +". " +
					// StringUtils.nullReplaceWithEmpty
					// (rs.getString("CONDITION_CODE")) + " " +
					// StringUtils.nullReplaceWithEmpty
					// (rs.getString("SHORT_TEXT")) + "^";
					actNbr = rs.getString("ACT_NBR");
					oldViolationsCount++;
				}
				i++;
			}

			map = new HashMap();
			map.put("violation", StringUtils.nullReplaceWithEmpty(violation));
			map.put("newViolation", StringUtils.nullReplaceWithEmpty(newViolation));
			map.put("oldViolation", StringUtils.nullReplaceWithEmpty(oldViolation));
			map.put("actNbr", StringUtils.nullReplaceWithEmpty(actNbr));

			return map;
		} catch (Exception ex) {
			logger.error("", ex);
			throw new Exception("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
	}

	public Map getActivityDetail(String act_id) throws Exception {
		String actNbr = "";
		String appliedDate = "";
		String expDate = "";
		String actType = "";
		Map map = null;

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(activityDetailQuery);
			logger.debug("activityDetailQuery::" + activityDetailQuery);
			pstmt.setString(1, act_id);

			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					actNbr = rs.getString("ACT_NBR");
					actType = rs.getString("DESCRIPTION");
					Date AppDate = rs.getDate("APPLIED_DATE");
					Date expDateObj = rs.getDate("EXP_DATE");
					if (AppDate != null) {
						appliedDate = sdf.format(AppDate);
					}
					if (expDateObj != null) {
						expDate = sdf.format(expDateObj);
					}
				}
			}
			map = new HashMap();
			map.put("actNbr", actNbr);
			map.put("appliedDate", appliedDate);
			map.put("expDate", expDate);
			map.put("actType", actType);
			return map;
		} catch (Exception ex) {
			logger.error("", ex);
			throw new Exception("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
	}

	private String getActivitySubtypeDetail(String act_id) throws AgentException {
		String subType = "";
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
		try {
			connection = wrapper.getConnection();
			String sql = "select * from ACT_SUBTYPE acts left join lkup_act_subtype las on acts.ACT_SUBTYPE_ID = las.ACT_SUBTYPE_ID where ACT_ID = ?";
			pstmt = connection.prepareStatement(sql);
			logger.debug("activitysubTypeQuery::" + sql);
			pstmt.setString(1, act_id);

			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					subType = subType + rs.getString("ACT_SUBTYPE") + ", ";
				}
			}

			if (subType != null && subType.lastIndexOf(",") != -1) {
				subType = subType.substring(0, subType.lastIndexOf(","));
			}

		} catch (Exception ex) {
			logger.error("", ex);
			throw new AgentException("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return subType;
	}

	private Map getGUSDFeeDetail(String act_id) throws AgentException {
		Map feeMap = new HashMap<String, ActivityFee>();
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
		try {
			connection = wrapper.getConnection();
			String sql = "select * from ACTIVITY_FEE af left join FEE fee on af.FEE_ID = fee.FEE_ID where fee.FEE_DESC in ('" + Constants.GUSD_DEVELOPER_FEE_COMMERCIAL + "', '" + Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL + "','" + Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL_CUMULATIVE + "','" + Constants.GUSD_DEVELOPER_FEE_RESIDENTIAL_CURRENT + "') and af.ACTIVITY_ID = ?";
			pstmt = connection.prepareStatement(sql);
			logger.debug("GUSDFEEQuery::" + sql);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();

			if (rs != null) {
				ActivityFee fee = null;
				while (rs.next()) {
					fee = new ActivityFee();
					fee.setFeeUnits(rs.getDouble("FEE_UNITS"));
					fee.setFeeAmount(rs.getDouble("FEE_AMNT"));
					feeMap.put(rs.getString("FEE_DESC"), fee);
				}
			}

		} catch (Exception ex) {
			logger.error("", ex);
			throw new AgentException("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return feeMap;
	}

	private String getInspectorDetail(String act_id) throws Exception {
		String name = "";

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(inspectorQuery);
			logger.debug("inspectorQuery::" + inspectorQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {

					String title = StringUtils.nullReplaceWithEmpty(rs.getString("TITLE"));
					title = title.toUpperCase();
					name = name + StringUtils.nullReplaceWithEmpty(rs.getString("First_Name").toUpperCase()) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("last_name").toUpperCase()) + " ~ " + title;

					// name = name.replaceAll("\n", "");
				}
			}
			return name;
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

	}

	public String getComplaintAddress(String act_id) throws Exception {
		String address = "";

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(complaintAddressQuery);
			logger.debug("complaintAddressQuery::" + complaintAddressQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					address = rs.getString("STR_NO") + " ";
					if (rs.getString("PRE_DIR") != null && !rs.getString("PRE_DIR").equalsIgnoreCase("")) {
						address = address + rs.getString("PRE_DIR") + " ";
					}
					address = address + rs.getString("STR_NAME") + " ";
					if (rs.getString("STR_TYPE") != null && !rs.getString("STR_TYPE").equalsIgnoreCase("")) {
						address = address + rs.getString("STR_TYPE") + " ";
					}
					this.sortComplaintAddress = address;

					address = address + "\n" + rs.getString("CITYSTATEZIP");

				}
			}
			return address;
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

	}

	public Map gerCEDetail(String ce_id) throws Exception {
		Map ceDetail = new HashMap();
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		logger.debug("ce_id  :::  " + ce_id);
		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(ceDetailQuery);
			pstmt.setString(1, ce_id);
			logger.debug("ceDetailQuery::" + ceDetailQuery);

			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					ceDetail.put("actId", rs.getString("ACT_ID"));

					logger.debug("ACT_ID   ::   " + rs.getString("ACT_ID"));

					ceDetail.put("ceTypeId", rs.getString("NOTICE_TYPE_ID"));
					ceDetail.put("officerId", rs.getString("OFFICER_ID"));
					ceDetail.put("ceDate", rs.getString("NOTICE_DATE"));
					logger.debug("name::" + rs.getString("TEMPLATE_NAME"));
					ceDetail.put("name", rs.getString("TEMPLATE_NAME"));
					// logger.debug("ceDate::" + rs.getString("TEMPLATETXT"));
					if (rs.getClob("TEMPLATETXT") != null) {
						ceDetail.put("noticeTemplate", StringUtils.clobToString(rs.getClob("TEMPLATETXT")));
					} else {
						ceDetail.put("noticeTemplate", "");
					}
					ceDetail.put("actionId", rs.getInt("ACTION_ID"));
					ceDetail.put("noticeTemplateId", rs.getInt("NOTICE_TEMPLATE_ID"));
					logger.debug("ceDate::" + rs.getString("NOTICE_DATE"));
					Date date = rs.getDate("NOTICE_DATE");
					ceDetail.put("ceDate", dateFormat(date));
				}
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
		return ceDetail;
	}

	public String getLatestInspectionDetail(String act_id) throws Exception {

		String inspectionDate = "";
		SimpleDateFormat sdfForDb = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);

		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(inspectiondateQuery);
			logger.debug("inspectiondateQuery::" + inspectiondateQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {

				java.util.Date date = sdfForDb.parse(rs.getString("INSPECTION_DT"));
				inspectionDate = sdf.format(date);
				logger.debug("inspectionDate::" + inspectionDate);
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
		logger.debug("inspectionDate::" + inspectionDate);
		return inspectionDate;
	}

	public String getApnForActivityId(String act_id) throws Exception {
		logger.debug("getApnForActivityId(" + act_id + ")");

		String apn = "";

		try {
			// String sql =
			// "select * from v_lso_owner where lso_id in (select occupancy_id from structure_occupant where structure_id in ( select lso_id from v_activity_address where act_id="
			// + act_id + "))";
			String sql = "select * from v_lso_owner  VLO	left outer join  V_ACTIVITY VA on VLO.ADDR_ID=VA.ADDR_ID where VA.ACT_ID=" + act_id;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				apn = StringUtils.nullReplaceWithEmpty(rs.getString("apn"));
			}

			rs.close();

			return apn;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public String getCurrentInspectionDetail(String act_id) throws Exception {

		String inspectionDate = "";
		SimpleDateFormat sdfForDb = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);

		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(inspectionCurrentdateQuery);
			logger.debug("inspectionCurrentdateQuery::" + inspectionCurrentdateQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				java.util.Date date = sdfForDb.parse(rs.getString("INSPECTION_DT"));
				inspectionDate = sdf.format(date);
				logger.debug("inspectionDate::" + inspectionDate);
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
		logger.debug("inspectionDate::" + inspectionDate);
		return inspectionDate;
	}

	public String getLastInspectionDate(String act_id) throws Exception {

		String inspectionDate = "";
		SimpleDateFormat sdfForDb = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);

		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			String lastInspectionDateQuery = "select * from inspection  where ACT_ID = ? and INSPECTION_DT < current_date order by INSPECTION_DT desc";

			pstmt = connection.prepareStatement(lastInspectionDateQuery);
			logger.debug("lastInspectionDateQuery::" + lastInspectionDateQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				java.util.Date date = sdfForDb.parse(rs.getString("INSPECTION_DT"));
				inspectionDate = sdf.format(date);
				logger.debug("inspectionDate::" + inspectionDate);
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
		logger.debug("inspectionDate::" + inspectionDate);
		return inspectionDate;
	}

	public Map getCurrentInspectionTime(String act_id) throws Exception {

		Map time = new HashMap();

		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(insptimeQuery);
			logger.debug("Time Query is::" + insptimeQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();

			String a = "";
			String b = "";
			if (rs.next()) {

				a = StringUtils.nullReplaceWithEmpty(rs.getString("ARRIVAL_TIME"));

				if (a.equalsIgnoreCase("") || a == null) {
					a = "0.00";
				}
				double arrTime = Double.parseDouble(a);
				logger.debug("arrTime" + arrTime);
				String arrivalTime = "";

				if (arrTime >= 13.00 && arrTime < 13.30) {
					arrTime = 1.00;

					arrivalTime = arrTime + "0" + " " + "PM";
				}

				else if (arrTime >= 13.30 && arrTime < 14.00) {
					arrTime = 1.30;

					arrivalTime = arrTime + "0" + "PM";
				} else if (arrTime >= 14.00 && arrTime < 14.30) {
					arrTime = 2.00;

					arrivalTime = arrTime + "0" + " " + "PM";
				} else if (arrTime >= 14.30 && arrTime < 15.00) {
					arrTime = 2.30;

					arrivalTime = arrTime + "0" + "PM";
				} else if (arrTime >= 15.00 && arrTime < 15.30) {
					arrTime = 3.00;

					arrivalTime = arrTime + "0" + "PM";
				} else if (arrTime >= 15.30 && arrTime < 16.00) {
					arrTime = 3.30;

					arrivalTime = arrTime + "0" + "PM";
				} else if (arrTime >= 16.00 && arrTime < 16.30) {
					arrTime = 4.00;

					arrivalTime = arrTime + "0" + "PM";
				} else if (arrTime >= 16.30 && arrTime < 17.00) {
					arrTime = 4.30;

					arrivalTime = arrTime + "0" + "PM";
				} else if (arrTime == 17.00) {
					arrTime = 5.00;

					arrivalTime = arrTime + "0" + "PM";
				}

				else if (arrTime == 8.00) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 8.30) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 9.00) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 9.30) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 10.00) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 10.30) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 11.00) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 11.30) {

					arrivalTime = arrTime + "0" + "AM";
				}

				else if (arrTime == 12.00) {

					arrivalTime = arrTime + "0" + "PM";
				}

				else if (arrTime == 12.30) {

					arrivalTime = arrTime + "PM";
				}

				arrivalTime = StringUtils.nullReplaceWithEmpty(arrivalTime);

				logger.debug("Arrival Time **** " + arrivalTime);

				b = StringUtils.nullReplaceWithEmpty(rs.getString("DEPARTURE_TIME"));

				if (b.equalsIgnoreCase("") || b == null) {
					b = "0.00";
				}

				double deptTime = Double.parseDouble(b);

				String departureTime = "";

				if (deptTime >= 13.00 && deptTime < 13.30) {
					deptTime = 1.00;

					departureTime = deptTime + "0" + " " + "PM";
				}

				else if (deptTime >= 13.30 && deptTime < 14.00) {
					deptTime = 1.30;

					departureTime = deptTime + "0" + "PM";
				} else if (deptTime >= 14.00 && deptTime < 14.30) {
					deptTime = 2.00;

					departureTime = deptTime + "0" + " " + "PM";
				} else if (deptTime >= 14.30 && deptTime < 15.00) {
					deptTime = 2.30;

					departureTime = deptTime + "0" + "PM";
				} else if (deptTime >= 15.00 && deptTime < 15.30) {
					deptTime = 3.00;

					departureTime = deptTime + "0" + "PM";
				} else if (deptTime >= 15.30 && deptTime < 16.00) {
					deptTime = 3.30;

					departureTime = deptTime + "0" + "PM";
				} else if (deptTime >= 16.00 && deptTime < 16.30) {
					deptTime = 4.00;

					departureTime = deptTime + "0" + "PM";
				} else if (deptTime >= 16.30 && deptTime < 17.00) {
					deptTime = 4.30;

					departureTime = deptTime + "0" + "PM";
				} else if (deptTime == 17.00) {
					deptTime = 5.00;

					departureTime = deptTime + "0" + "PM";
				}

				else if (deptTime == 8.00) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 8.30) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 9.00) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 9.30) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 10.00) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 10.30) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 11.00) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 11.30) {

					departureTime = deptTime + "0" + "AM";
				}

				else if (deptTime == 12.00) {

					departureTime = deptTime + "0" + "PM";
				}

				else if (deptTime == 12.30) {

					departureTime = deptTime + "0" + "AM";
				}

				departureTime = StringUtils.nullReplaceWithEmpty(departureTime);

				logger.debug("Departure Time **** " + departureTime);

				String totalTime = "";

				totalTime = arrivalTime + "-" + departureTime;
				logger.debug("TOTAL TIME IS" + totalTime);

				time.put("inspectionTime", StringUtils.nullReplaceWithEmpty(totalTime));

			}
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return time;
	}

	/*
	 * public List getDeficiencesList(String act_id) { List deficiencyCondList = new ArrayList();
	 * 
	 * String defCode = ""; String defText = "";
	 * 
	 * try { String sql = " select LC.CONDITION_CODE, C.SHORT_TEXT from conditions c left outer join LKUP_CONDITIONS  LC on c.LIBRARY_ID= LC.CONDITION_ID  where c.level_id =" + act_id + " and c.cond_level ='A'  order by update_dt desc"; logger.debug("inspections action codes Query::" + sql); ResultSet rs = new Wrapper().select(sql); int i = 1; while (rs.next()) {
	 * 
	 * defText = "^" +i + ". "; if ((rs.getString("CONDITION_CODE")) != null) { defText += StringUtils.nullReplaceWithEmpty( rs.getString("CONDITION_CODE")).trim() + "^" + "    " + StringUtils.nullReplaceWithEmpty( rs.getString("SHORT_TEXT")).trim(); } else { defText += StringUtils.nullReplaceWithEmpty( rs.getString("SHORT_TEXT")).trim(); } logger.debug(defText);
	 * 
	 * deficiencyCondList.add(defText); i++; }
	 * 
	 * if (rs != null) { rs.close(); } } catch (Exception e) { logger.error(e.getMessage()); }
	 * 
	 * return deficiencyCondList; }
	 */

	public String getComplaintAddressPDF(String act_id) throws Exception {
		String address = "";

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(complaintAddressQuery);
			logger.debug("complaintAddressQuery::" + complaintAddressQuery);
			pstmt.setString(1, act_id);
			rs = pstmt.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					address = rs.getString("STR_NO") + " ";
					if (rs.getString("PRE_DIR") != null && !rs.getString("PRE_DIR").equalsIgnoreCase("")) {
						address = address + rs.getString("PRE_DIR") + " ";
					}
					address = address + rs.getString("STR_NAME") + " ";

					if (rs.getString("STR_TYPE") != null && !rs.getString("STR_TYPE").equalsIgnoreCase("")) {
						address = address + rs.getString("STR_TYPE") + " ";
					}

					address = address + rs.getString("CITYSTATEZIP");
				}
			}
			return address;
		} catch (Exception e) {
			logger.error("", e);
			throw new Exception("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

	}

	public Map getOwnerDetailPDF(String act_id) throws Exception {
		Map owner = null;

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(owenrQuery);
			pstmt.setString(1, act_id);
			logger.debug("owenrQuery::" + owenrQuery);
			rs = pstmt.executeQuery();
			if (rs != null) {
				owner = new HashMap();
				while (rs.next()) {
					owner.put("name", StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
					String address = StringUtils.nullReplaceWithEmpty(rs.getString("ADDR"));
					address = address + " " + StringUtils.nullReplaceWithEmpty(rs.getString("CITY")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STATE")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("ZIP"));
					owner.put("address", address);
				}
			}

		} catch (Exception ex) {
			logger.error("", ex);
			throw new Exception("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return owner;
	}

	public String dateFormat(Date date) {
		SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT_NOTICES);
		String formattedDate = "";
		if (date != null) {
			formattedDate = sdf.format(date);
		}
		return formattedDate;
	}

	public List getDeficiencesList(String act_id) {
		List deficiencyCondList = new ArrayList();

		String defCode = "";
		String defText = "";

		try {
			String sql = " select COMNT from comments where level_id=" + act_id + "and COMNT_LEVEL='A'";
			logger.debug("inspections action codes Query::" + sql);
			ResultSet rs = new Wrapper().select(sql);
			int i = 1;
			while (rs.next()) {

				// defText = "^" +i + ". ";
				if ((rs.getString("COMNT")) != null) {
					defText = StringUtils.nullReplaceWithEmpty(rs.getString("COMNT")).replaceAll("\n", "\r");

					logger.debug(defText);

					deficiencyCondList.add(defText);

				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return deficiencyCondList;
	}

	public void imageLoad() {
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection c = null;
		FileInputStream fis;
		PreparedStatement ps;
		File file;

		file = new File("D:/Workspace/Glendale/CSI/WebContent/jsp/images/glendalelogo.jpg");
		try {
			fis = new FileInputStream(file);

			c = wrapper.getConnection();

			ps = c.prepareStatement("insert into LOGO values(?,?)");
			ps.setInt(1, 2);
			ps.setBinaryStream(2, fis, (int) file.length());
			ps.execute();
			ps.close();
			c.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Map<String, String> getPeopleOwner(String actId) throws AgentException {
		Map<String, String> owner = new HashMap<String, String>();
		Wrapper db = new Wrapper();
		String sql = "select AP.ACT_ID,AP.PEOPLE_ID,P.NAME,P.ADDR,P.CITY,P.STATE,P.ZIP from ACTIVITY_PEOPLE AP LEFT JOIN PEOPLE P on AP.PEOPLE_ID = P.PEOPLE_ID and P.PEOPLE_TYPE_ID = " + Constants.PEOPLE_OWNER + " where AP.ACT_ID = " + actId;

		RowSet rs;
		try {
			rs = db.select(sql);

			while (rs.next()) {
				String name = rs.getString("NAME");
				String address = rs.getString("ADDR");
				String cityStateZip = rs.getString("CITY") + " " + rs.getString("STATE") + " " + rs.getString("ZIP");
				owner.put("name", name);
				owner.put("address", address + " " + cityStateZip);
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}

		return owner;
	}

	public String getLatestNoticeId(String actId) throws AgentException {
		String noticeId = "";
		Wrapper db = new Wrapper();
		String sql = "SELECT CE_ID from (select CE_ID FROM CODE_ENFORCEMENT  WHERE ACT_ID= " + actId + " order by CE_ID desc)temp where rownum = 1";
		RowSet rs = null;
		try {
			rs = db.select(sql);
			if (rs != null && rs.next()) {
				noticeId = rs.getString("CE_ID");
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}

		return noticeId;
	}

	public String getFireEngineDetail(String actId) throws AgentException {
		String val = "";
		String sql = "select * from VMP_ACT V join LKUP_FIRE_ENGINE LFA on V.FIRE_ENGINE_ID= LFA.ENGINE_ID where act_id= " + actId;
		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);
			if (rs != null && rs.next()) {
				val = rs.getString("TYPE");
			}

		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException();
		}
		return val;
	}

}

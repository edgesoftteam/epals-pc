package elms.agent;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import elms.app.admin.ActivitySubType311;
import elms.app.common.Agent;
import elms.app.common.Comment;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.security.User;
import elms.util.StringUtils;

public class Epals311Agent extends Agent {

	static Logger logger = Logger.getLogger(Epals311Agent.class.getName());

	private static Map<String, String> lkupSystemDataMap = null;
	
	/**
	 * default constructor
	 */
	public Epals311Agent() {
	}
	
	private static void set311Properties(){
		logger.debug("Setting 311Properties");
		lkupSystemDataMap = LookupAgent.getLkupSystemDataMapFor311();
	}
	
	public static void addCommentIn311System(User user, Comment comment){
		
		set311Properties();
		
		int loginIdFor311 = StringUtils.s2i(lkupSystemDataMap.get(Constants.THREE11_SYSTEM_LOGIN_ID));

		int actId = comment.getLevelId();
		ActivityDetail activityDetails = ActivityAgent.getActivityDetailFor311(actId);
		int actCreatedBy = activityDetails.getCreatedBy().getUserId();
				
		logger.debug("loginIdFor311 : "+loginIdFor311 + ", activity created user id : "+actCreatedBy);
		
		if(comment.getCommentLevel().equalsIgnoreCase("A") && activityDetails.getExtRefNumber() != ""){
			try{
				String CommentType = (comment.getInternal() != null) ? "Internal" : "External";
				
				String caseId = activityDetails.getExtRefNumber();
				if(caseId != null){
					logger.debug("Add comment to 311 system.");
					
					String apiURLFor311 = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_BASE_URL);
					String apiUsername = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_USERNAME);
					String apiPassword = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_PASSWORD);
					
					apiURLFor311 = apiURLFor311 + "AddCaseComment";
					logger.debug(" URL "+apiURLFor311);
					
					String json = "{\"CaseId\":\""+activityDetails.getExtRefNumber() +"\", "+ "\"CommentText\":\""+comment.getComment()+"\", "+ "\"CommentType\":\""+CommentType+"\",}";
					
					DefaultHttpClient httpclient = new DefaultHttpClient();
					
					HttpPost httppost = new HttpPost(apiURLFor311);
					
					httppost.setHeader(Constants.THREE11_SERVICE_USERNAME, apiUsername);
					httppost.setHeader(Constants.THREE11_SERVICE_PASSWORD, apiPassword);			
					
					MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
					entity.addPart("jsonData",  new StringBody(json));
							
					httppost.setEntity(entity);
					HttpResponse  response = httpclient.execute(httppost);
					logger.debug("Add Comment response code : "+response.getStatusLine().getStatusCode());
					logger.debug("Response is : "+response);
					
					httpclient.getConnectionManager().shutdown();
				}
			} catch(Exception e){
				logger.error("Failed to update details in 311 system."+e.getMessage());
			}
		}
	}
	
	public static void updateStructureTypeIn311System(String description311,int sprojId){
		set311Properties();
		
		List<ActivityDetail> activityDetails = ActivityAgent.getActivityListDetailFor311(sprojId);
		
		logger.debug("Update structure type details to 311 system.");
		String apiURLFor311 = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_BASE_URL);
		try{
			String apiUsername = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_USERNAME);
			String apiPassword = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_PASSWORD);
			
			apiURLFor311 = apiURLFor311 + "UpdateCaseAttributes";
			logger.debug(" URL "+apiURLFor311);
			
			for(int index = 0 ; index < activityDetails.size();index++){
				String caseId = activityDetails.get(index).getExtRefNumber();
				
				if(description311 !=null && caseId != null){
					String json = "{\"CaseId\":"+ caseId +",\"Attributes\":{\"Type of Structure\" :\""+description311+"\" }}";
	
					logger.debug("Payload "+json);
					
					DefaultHttpClient httpclient = new DefaultHttpClient();
					
					HttpPost httppost = new HttpPost(apiURLFor311);
					
					httppost.setHeader(Constants.THREE11_SERVICE_USERNAME, apiUsername);
					httppost.setHeader(Constants.THREE11_SERVICE_PASSWORD, apiPassword);			
				
					StringEntity params = new StringEntity(json);
					httppost.addHeader("content-type", "application/json");
					httppost.setEntity(params);
					
					HttpResponse  response = httpclient.execute(httppost);
					logger.debug("Update Case response code : "+response.getStatusLine().getStatusCode());
					logger.debug("Response is : "+response);
					
					httpclient.getConnectionManager().shutdown();
				}
			}
		} catch(Exception e){
			logger.error("Failed to update details in 311 system."+e.getMessage());
		}
	}

	public static void updateCaseIn311System(User user, Activity activity){
		set311Properties();
		
		int loginIdFor311 = StringUtils.s2i(lkupSystemDataMap.get(Constants.THREE11_SYSTEM_LOGIN_ID));
		
		ActivityDetail activityDetails = ActivityAgent.getActivityDetailFor311(activity.getActivityId());
		int actCreatedBy = activityDetails.getCreatedBy().getUserId();
		
		logger.debug("loginIdFor311 : "+loginIdFor311 + ", activity created by id : "+actCreatedBy);
		
		//if(loginIdFor311 == actCreatedBy){
			logger.debug("Update case details to 311 system.");
			String baseApiURLFor311 = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_BASE_URL);
			
			String apiUsername = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_USERNAME);
			String apiPassword = lkupSystemDataMap.get(Constants.THREE11_SYSTEM_API_PASSWORD);
			
			ActivityAgent activityAgent = new ActivityAgent();
			String caseId = activity.getActivityDetail().getExtRefNumber();
			try{
				String action =	activityAgent.get311StatusName(activity.getActivityId());
				
				// Call UpdateCasebyCaseId API
				if(caseId !=null && action != null){
					
					String json = "{"+
							  "\"CaseId\":"+caseId+", "+
							  "\"CaseTypeId\":\""+Constants.THREE11_CASE_TYPE_ID+"\", "+
							  "\"Action\":\""+action+"\" }";
					
					logger.debug("Payload "+json);
					DefaultHttpClient httpclient = new DefaultHttpClient();
					
					HttpPost httppost = new HttpPost(baseApiURLFor311 + "UpdateCasebyCaseId");
					
					httppost.setHeader(Constants.THREE11_SERVICE_USERNAME, apiUsername);
					httppost.setHeader(Constants.THREE11_SERVICE_PASSWORD, apiPassword);			
					
					MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
					entity.addPart("jsonData",  new StringBody(json));
							
					httppost.setEntity(entity);
					HttpResponse  response = httpclient.execute(httppost);
					logger.debug("Update Case response code : "+response.getStatusLine().getStatusCode());
					logger.debug("Response is : "+response);
					
					httpclient.getConnectionManager().shutdown();
				}
			} catch(Exception e){
				logger.error("Failed to update details in 311 system."+e.getMessage());
			}
			
			// Call UpdateCaseAttributes API
			try{
				List<ActivitySubType311> mappingList =	activityAgent.getActivitySubType311Mapping(activity.getActivityId());
				
				String typeOfViolation = mappingList.stream().map(ActivitySubType311::getViolationType311).distinct().collect(Collectors.joining("$,$","$$","$$")).replace("$$", "\"").replace("$,$","\",\"");
				logger.debug("typeOfViolation "+typeOfViolation);
				
				List<String> violationTypes311 = mappingList.stream().map(ActivitySubType311::getViolationType311).distinct().collect(Collectors.toList());
				
				StringBuffer violationGroupedData = new StringBuffer();
				for(int v=0; v<violationTypes311.size(); v++){
					String groupName = violationTypes311.get(v);
					String groupValue = mappingList.stream().filter(vtype311 -> vtype311.getViolationType311().equalsIgnoreCase(groupName)).map(ActivitySubType311::getViolationType).collect(Collectors.joining("$,$","$$","$$")).replace("$$", "\"").replace("$,$","\",\"");
					violationGroupedData.append(",\""+ groupName +"\": [" + groupValue +"]");
				}
				
				if(mappingList.size()> 0){
					String json = "{\"CaseId\":"+ caseId +",\"Attributes\":{\"Type of Violation\" : ["+typeOfViolation+"] "+violationGroupedData+"}}";
					logger.debug("Payload "+json);

					DefaultHttpClient httpclient = new DefaultHttpClient();
					
					HttpPost httppost = new HttpPost(baseApiURLFor311 + "UpdateCaseAttributes");
					
					httppost.setHeader(Constants.THREE11_SERVICE_USERNAME, apiUsername);
					httppost.setHeader(Constants.THREE11_SERVICE_PASSWORD, apiPassword);			
					
					StringEntity params = new StringEntity(json);
					httppost.addHeader("content-type", "application/json");
					httppost.setEntity(params);
					
					HttpResponse  response = httpclient.execute(httppost);
					logger.debug("Update Case response code : "+response.getStatusLine().getStatusCode());
					logger.debug("Response is : "+response);
					
					httpclient.getConnectionManager().shutdown();
				}
			} catch(Exception e){
				logger.error("Failed to update details in 311 system."+e.getMessage());
			}
		//}
	}

}
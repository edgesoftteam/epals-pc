package elms.agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.common.Agent;
import elms.app.lso.Assessor;
import elms.app.lso.OwnerApnAddress;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class AssessorAgent extends Agent {

	static Logger logger = Logger.getLogger(AssessorAgent.class.getName());

	/*
	 * Static SQL Queries
	 */
	static private final String ASSESSOR_SQL = "INSERT INTO ASSESSOR VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	static private final String INSERT_ASSESSOR_IMPORT_LOG = "INSERT INTO ASSESSOR_IMPORT_LOG VALUES (?,?,?,current_date)";
	static private final String GET_LATEST_STATUS = "select status from assessor_import_log where updated in (select max(updated) from assessor_import_log)";
	static private final String DELETE_ALL_LOGS = "delete from assessor_import_log";
	static ResourceBundle assessorProperties = null;

	/*
	 * Status
	 */
	static final String start = "START";
	static final String progress = "PROGRESS";
	static final String complete = "COMPLETE";

	/*
	 * Statements
	 */
	private PreparedStatement stmtAssessor;

	/*
	 * Other items.
	 */
	public String fileName;

	/**
	 * 3CD0322702D9
	 */
	public AssessorAgent() {

	}

	/**
	 * Access method for the land property.
	 * 
	 * @return the current value of the land property
	 */
	public Assessor getAssessor(String apn) {
		Assessor assessor = null;
		try {

			Wrapper db = new Wrapper();

			String sql = "select * FROM ASSESSOR WHERE APN='" + apn + "'";
			logger.debug(sql);
			RowSet rs = db.select(sql);
			assessor = new Assessor();
			if (rs.next()) {
				assessor.setParcelNbr(StringUtils.apnWithHyphens(rs.getString("apn")));
				assessor.setSITUS_HOUSE_NO(rs.getString("SITUS_HOUSE_NO"));
				assessor.setSITUS_FRACTION(rs.getString("SITUS_FRACTION"));
				assessor.setSITUS_STREET_NAME(rs.getString("SITUS_STREET_NAME"));
				assessor.setSITUS_DIRECTION(rs.getString("SITUS_DIRECTION"));
				assessor.setSITUS_UNIT(rs.getString("SITUS_UNIT"));
				assessor.setSITUS_CITY_STATE(rs.getString("SITUS_CITY_STATE"));
				assessor.setSITUS_ZIP(rs.getString("SITUS_ZIP"));
				assessor.setMAIL_ADDRESS_HOUSE_NO(rs.getString("MAIL_ADDR_HOUSE_NO"));
				assessor.setMAIL_ADDRESS_FRACTION(rs.getString("MAIL_ADDR_FRACTION"));
				assessor.setMAIL_ADDRESS_DIRECTION(rs.getString("MAIL_ADDR_DIR"));
				assessor.setMAIL_ADDRESS_STREET_NAME(rs.getString("MAIL_ADDR_ST_NAME"));
				assessor.setMAIL_ADDRESS_UNIT(rs.getString("MAIL_ADDR_UNIT"));
				assessor.setMAIL_ADDRESS_CITY_STATE(rs.getString("MAIL_ADDR_CTY_STA"));
				assessor.setMAIL_ADDRESS_ZIP(rs.getString("MAIL_ADDR_ZIP"));
				assessor.setFIRST_OWNER_ASSESSEE_NAME(rs.getString("FRST_OWNR_NAME"));
				assessor.setFIRST_OWNER_NAME_OVERFLOW(rs.getString("FRST_OWNR_NAME_OVR"));
				assessor.setLEGAL_DESCRIPTION_1(rs.getString("LEGAL_DESC1"));
				assessor.setLEGAL_DESCRIPTION_2(rs.getString("LEGAL_DESC2"));
				assessor.setLEGAL_DESCRIPTION_3(rs.getString("LEGAL_DESC3"));
				assessor.setLEGAL_DESCRIPTION_4(rs.getString("LEGAL_DESC4"));
				assessor.setLEGAL_DESCRIPTION_5(rs.getString("LEGAL_DESC5"));
				assessor.setLEGAL_DESCRIPTION_6(rs.getString("LEGAL_DESC6"));
				assessor.setTAX_RATE_AREA(rs.getString("TAX_RATE_AREA"));
				assessor.setAGENCY_CLASS_NO(rs.getString("AGENCY_CLASS_NO"));
				assessor.setZONING_CODE(rs.getString("ZONING_CODE"));
				assessor.setUSE_CODE(rs.getString("USE_CODE"));
				assessor.setLAND_CURRENT_ROLL_YEAR(rs.getString("LAND_CURR_ROLL_YR"));
				assessor.setLAND_CURRENT_VALUE(StringUtils.str2$(rs.getString("LAND_CURR_VAL").trim()));
				assessor.setIMPROVE_CURRENT_ROLL_YEAR(rs.getString("IMP_CURR_ROLL_YR"));
				assessor.setIMPROVE_CURRENT_VALUE(StringUtils.str2$(rs.getString("IMP_CURR_VAL").trim()));
				assessor.setSPECIAL_NAME_ASSESSEE(rs.getString("SPEC_NAME_ASSESSEE"));
				assessor.setTAX_STATUS_KEY(rs.getString("TAX_STATUS_KEY"));
				String date = rs.getString("RECORDING_DATE");
				if (date != null && !date.equals("") && (date.length() == 8))
					date = date.substring(4, 6) + "/" + date.substring(6) + "/" + date.substring(0, 4);
				else
					date = "";
				assessor.setRECORDING_DATE(date);
				assessor.setHAZARD_ABATEMENT_CITY_KEY(rs.getString("HAZ_ABATE_CTY_KEY"));
				assessor.setTAX_STATUS_YEAR_SOLD(rs.getString("TAX_STAT_YR_SOLD"));
				assessor.setPARTIAL_INTEREST(rs.getString("PARTIAL_INTEREST"));
				assessor.setHAZARD_ABATEMENT_INFORMATION(rs.getString("HAZ_ABATE_INFO"));
				assessor.setOWNERSHIP_CODE(rs.getString("OWNERSHIP_CODE"));
				assessor.setDOCUMENT_REASON_CODE(rs.getString("DOC_REASON_CODE"));
				assessor.setPERSONAL_PROPERTY_KEY(rs.getString("PER_PROP_KEY"));
				assessor.setEXEMPTION_CLAIM_TYPE(rs.getString("EXEM_CLAIM_TYPE"));
				assessor.setPERSONAL_PROPERTY_EXEMPTION_VA(StringUtils.str2$(rs.getString("PERL_PROP_EXEM_VAL").trim()));
				assessor.setPERSONAL_PROPERTY_VALUE(StringUtils.str2$(rs.getString("PER_PROP_VAL").trim()));
				assessor.setFIXTURE_EXEMPTION_VALUE(StringUtils.str2$(rs.getString("FIXTURE_EXEM_VAL").trim()));
				assessor.setFIXTURE_VALUE(StringUtils.str2$(rs.getString("FIXTURE_VALUE").trim()));
				assessor.setHOMEOWNER_EXEMPTION_VALUE(StringUtils.str2$(rs.getString("HOMEOWNER_EXEM_VAL").trim()));
				assessor.setHOMEOWNER_NO_OF_EXEMPTIONS(rs.getString("HOMEOWNER_NO_EXEM"));
				assessor.setREAL_ESTATE_EXEMPTION_VALUE(rs.getString("REAL_ESTATE_EXEMPT"));
				assessor.setLAST_SALE_ONE_VERIFY_KEY(rs.getString("LAST_SL1_VER_KEY"));
				date = rs.getString("LAST_SL1_SL_DT");
				if (date != null && !date.equals("") && (date.length() == 8))
					date = date.substring(4, 6) + "/" + date.substring(6) + "/" + date.substring(0, 4);
				else
					date = "";
				assessor.setLAST_SALE_ONE_DATE(date);
				assessor.setLAST_SALE_ONE_SALE_AMOUNT(StringUtils.str2$(rs.getString("LAST_SL1_SL_AMNT").trim()));
				assessor.setLAST_SALE_TWO_VERIFY_KEY(rs.getString("LAST_SL2_VER_KEY"));
				date = rs.getString("LAST_SL2_SL_DT");
				if (date != null && !date.equals("") && (date.length() == 8))
					date = date.substring(4, 6) + "/" + date.substring(6) + "/" + date.substring(0, 4);
				else
					date = "";
				assessor.setLAST_SALE_TWO_SALE_DATE(date);
				assessor.setLAST_SALE_TWO_SALE_AMOUNT(StringUtils.str2$(rs.getString("LAST_SL2_SL_AMNT").trim()));
				assessor.setLAST_SALE_THREE_VERIFY_KEY(rs.getString("LAST_SL3_VER_KEY"));
				date = rs.getString("LAST_SL3_SL_DT");
				if (date != null && !date.equals("") && (date.length() == 8))
					date = date.substring(4, 6) + "/" + date.substring(6) + "/" + date.substring(0, 4);
				else
					date = "";
				assessor.setLAST_SALE_THREE_SALE_DATE(date);
				assessor.setLAST_SALE_THREE_SALE_AMOUNT(StringUtils.str2$(rs.getString("LAST_SL3_SL_AMNT").trim()));
				assessor.setBUILDING_DATA_LINE_1_YEAR_BUIL(rs.getString("DATA1_YR_BUILT"));
				assessor.setBUILDING_DATA_LINE_1_NO_OF_UNI(rs.getString("DATA1_NO_UNITS"));
				assessor.setBUILDING_DATA_LINE_1_NO_OF_BED(rs.getString("DATA1_NO_BDRMS"));
				assessor.setBUILDING_DATA_LINE_1_NO_OF_BAT(rs.getString("DATA1_NO_BATHS"));
				assessor.setBUILDING_DATA_LINE_1_SQUARE_FE(rs.getString("DATA1_SQ_FT"));
				assessor.setBUILDING_DATA_LINE_1_SUBPART(rs.getString("DATA1_SUBPART"));
				assessor.setBUILDING_DATA_LINE_1_DESIGN_TY(rs.getString("DATA1_DESIGN_TYPE"));
				assessor.setBUILDING_DATA_LINE_1_QUALITY(rs.getString("DATA1_QUALITY"));
				assessor.setBUILDING_DATA_LINE_2_YEAR_BUIL(rs.getString("DATA2_YR_BUILT"));
				assessor.setBUILDING_DATA_LINE_2_NO_OF_UNI(rs.getString("DATA2_NO_UNITS"));
				assessor.setBUILDING_DATA_LINE_2_NO_OF_BED(rs.getString("DATA2_NO_BDRMS"));
				// logger.debug("setBUILDING_DATA_LINE_2_NO_OF_BED ");
				assessor.setBUILDING_DATA_LINE_2_NO_OF_BAT(rs.getString("DATA2_NO_BATHS"));
				// logger.debug("setBUILDING_DATA_LINE_2_NO_OF_BAT ");
				assessor.setBUILDING_DATA_LINE_2_SQUARE_FE(rs.getString("DATA2_SQ_FT"));
				// logger.debug("setBUILDING_DATA_LINE_2_SQUARE_FE ");
				assessor.setBUILDING_DATA_LINE_2_SUBPART(rs.getString("DATA2_SUBPART"));
				// logger.debug("setBUILDING_DATA_LINE_2_SUBPART ");
				assessor.setBUILDING_DATA_LINE_2_DESIGN_TY(rs.getString("DATA2_DESIGN_TYPE"));
				// logger.debug("setBUILDING_DATA_LINE_2_DESIGN_TY ");
				assessor.setBUILDING_DATA_LINE_2_QUALITY(rs.getString("DATA2_QUALITY"));
				// logger.debug("setBUILDING_DATA_LINE_2_QUALITY ");

				assessor.setBUILDING_DATA_LINE_3_YEAR_BUIL(rs.getString("DATA3_YR_BUILT"));
				// logger.debug("setBUILDING_DATA_LINE_3_YEAR_BUIL ");
				assessor.setBUILDING_DATA_LINE_3_NO_OF_UNI(rs.getString("DATA3_NO_UNITS"));
				// logger.debug("setBUILDING_DATA_LINE_3_NO_OF_UNI ");
				assessor.setBUILDING_DATA_LINE_3_NO_OF_BED(rs.getString("DATA3_NO_BDRMS"));
				// logger.debug("setBUILDING_DATA_LINE_3_NO_OF_BED ");
				assessor.setBUILDING_DATA_LINE_3_NO_OF_BAT(rs.getString("DATA3_NO_BATHS"));
				// logger.debug("setBUILDING_DATA_LINE_3_NO_OF_BAT ");
				assessor.setBUILDING_DATA_LINE_3_SQUARE_FE(rs.getString("DATA3_SQ_FT"));
				// logger.debug("setBUILDING_DATA_LINE_3_SQUARE_FE ");
				assessor.setBUILDING_DATA_LINE_3_SUBPART(rs.getString("DATA3_SUBPART"));
				// logger.debug("setBUILDING_DATA_LINE_3_SUBPART ");
				assessor.setBUILDING_DATA_LINE_3_DESIGN_TY(rs.getString("DATA3_DESIGN_TYPE"));
				// logger.debug("setBUILDING_DATA_LINE_3_DESIGN_TY ");
				assessor.setBUILDING_DATA_LINE_3_QUALITY(rs.getString("DATA3_QUALITY"));
				// logger.debug("setBUILDING_DATA_LINE_3_QUALITY ");

				assessor.setBUILDING_DATA_LINE_4_YEAR_BUIL(rs.getString("DATA4_YR_BUILT"));
				// logger.debug("setBUILDING_DATA_LINE_4_YEAR_BUIL ");
				assessor.setBUILDING_DATA_LINE_4_NO_OF_UNI(rs.getString("DATA4_NO_UNITS"));
				// logger.debug("setBUILDING_DATA_LINE_4_NO_OF_UNI ");
				assessor.setBUILDING_DATA_LINE_4_NO_OF_BED(rs.getString("DATA4_NO_BDRMS"));
				// logger.debug("setBUILDING_DATA_LINE_4_NO_OF_BED ");
				assessor.setBUILDING_DATA_LINE_4_NO_OF_BAT(rs.getString("DATA4_NO_BATHS"));
				// logger.debug("setBUILDING_DATA_LINE_4_NO_OF_BAT ");
				assessor.setBUILDING_DATA_LINE_4_SQUARE_FE(rs.getString("DATA4_SQ_FT"));
				// logger.debug("setBUILDING_DATA_LINE_4_SQUARE_FE ");
				assessor.setBUILDING_DATA_LINE_4_SUBPART(rs.getString("DATA4_SUBPART"));
				// logger.debug("setBUILDING_DATA_LINE_4_SUBPART ");
				assessor.setBUILDING_DATA_LINE_4_DESIGN_TY(rs.getString("DATA4_DESIGN_TYPE"));
				// logger.debug("setBUILDING_DATA_LINE_4_DESIGN_TY ");
				assessor.setBUILDING_DATA_LINE_4_QUALITY(rs.getString("DATA4_QUALITY"));
				// logger.debug("setBUILDING_DATA_LINE_4_QUALITY ");

				assessor.setBUILDING_DATA_LINE_5_YEAR_BUIL(rs.getString("DATA5_YR_BUILT"));
				// logger.debug("setBUILDING_DATA_LINE_5_YEAR_BUIL ");
				assessor.setBUILDING_DATA_LINE_5_NO_OF_UNI(rs.getString("DATA5_NO_UNITS"));
				// logger.debug("setBUILDING_DATA_LINE_5_NO_OF_UNI ");
				assessor.setBUILDING_DATA_LINE_5_NO_OF_BED(rs.getString("DATA5_NO_BDRMS"));
				// logger.debug("setBUILDING_DATA_LINE_5_NO_OF_BED ");
				assessor.setBUILDING_DATA_LINE_5_NO_OF_BAT(rs.getString("DATA5_NO_BATHS"));
				// logger.debug("setBUILDING_DATA_LINE_5_NO_OF_BAT ");
				assessor.setBUILDING_DATA_LINE_5_SQUARE_FE(rs.getString("DATA5_SQ_FT"));
				// logger.debug("setBUILDING_DATA_LINE_5_SQUARE_FE ");
				assessor.setBUILDING_DATA_LINE_5_SUBPART(rs.getString("DATA5_SUBPART"));
				// logger.debug("setBUILDING_DATA_LINE_5_SUBPART ");
				assessor.setBUILDING_DATA_LINE_5_DESIGN_TY(rs.getString("DATA5_DESIGN_TYPE"));
				// logger.debug("setBUILDING_DATA_LINE_5_DESIGN_TY ");
				assessor.setBUILDING_DATA_LINE_5_QUALITY(rs.getString("DATA5_QUALITY"));
				// logger.debug("setBUILDING_DATA_LINE_5_QUALITY ");

				assessor.setSPECIAL_NAME_LEGEND(rs.getString("SPEC_NAME_LEGEND"));
				// logger.debug("setSPECIAL_NAME_LEGEND");
				assessor.setSECOND_OWNER_ASSESSEE_NAME(rs.getString("SECND_OWNR_NAME"));
				// logger.debug("setSECOND_OWNER_ASSESSEE_NAME ");
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return assessor;
	}

	/**
	 * Gets the assessor list using the following parameters
	 * 
	 * @param useCode
	 * @param zoneCode
	 * @param numberOfUnits
	 * @param hazardAbatementCode
	 * @param dateOfLastSaleGT
	 * @param dateOfLastSaleLT
	 * @param improvementValueGT
	 * @param improvementValueLT
	 * @param totalAssessmentGT
	 * @param totalAssessmentLT
	 * @return
	 */
	public List<OwnerApnAddress> searchAssessorData(String useCode, String zoneCode, String numberOfUnits, String hazardAbatementCode, String dateOfLastSaleGT, String dateOfLastSaleLT, String improvementValueGT, String improvementValueLT, String totalAssessmentGT, String totalAssessmentLT) throws Exception {
		List<OwnerApnAddress> OwnerApnAddressList = new ArrayList<OwnerApnAddress>();
		String sql = "select apn from assessor ";
		if ((!(useCode.equals(""))) || (!(zoneCode.equals(""))) || (!(numberOfUnits.equals(""))) || (!(hazardAbatementCode.equals(""))) || (!(dateOfLastSaleGT.equals(""))) || (!(dateOfLastSaleLT.equals(""))) || (!(improvementValueGT.equals(""))) || (!(improvementValueLT.equals(""))) || (!(totalAssessmentGT.equals(""))) || (!(totalAssessmentLT.equals("")))) {

		}
		sql += "where ";
		logger.debug("after where" + sql);
		if ((!(useCode.equals("")))) {
			sql += "rtrim(use_code)=" + StringUtils.checkString(useCode) + " and ";
		}
		if ((!(zoneCode.equals("")))) {
			sql += "rtrim(zoning_code)=" + StringUtils.checkString(zoneCode) + " and ";
		}
		if ((!(numberOfUnits.equals("")))) {
			sql += "rtrim(DATA1_NO_UNITS)=" + StringUtils.checkString(numberOfUnits) + " and ";
		}
		if ((!(hazardAbatementCode.equals("")))) {
			sql += "rtrim(HAZ_ABATE_CTY_KEY)=" + StringUtils.checkString(hazardAbatementCode) + " and ";
		}
		if ((!(dateOfLastSaleGT.equals("")))) {
			sql += "rtrim(LAST_SL1_SL_DT)=" + StringUtils.checkString(dateOfLastSaleGT) + " and ";
		}
		if ((!(dateOfLastSaleLT.equals("")))) {
			sql += "rtrim(LAST_SL2_SL_DT)=" + StringUtils.checkString(dateOfLastSaleLT) + " and ";
		}
		if ((!(improvementValueGT.equals("")))) {
			sql += "rtrim(IMP_CURR_VAL)=" + StringUtils.checkString(improvementValueGT) + " and ";
		}
		if ((!(improvementValueLT.equals("")))) {
			sql += "rtrim(LAND_CURR_VAL)=" + StringUtils.checkString(improvementValueLT) + " and ";
		}
		sql += " rownum<101";
		logger.info(sql);
		RowSet rs = new Wrapper().select(sql);
		while (rs.next()) {
			OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
			String apn = rs.getString("APN");
			ownerApnAddress.setApn(apn);
			ownerApnAddress.setAddress(LookupAgent.getAddressForApn(apn));
			ownerApnAddress.setOwnerName(LookupAgent.getOwnerForApn(apn));
			ownerApnAddress.setLsoId(LookupAgent.getLsoIdForApn(apn));
			OwnerApnAddressList.add(ownerApnAddress);
		}
		return OwnerApnAddressList;

	}

	public AssessorAgent(String fileName) {
		logger.info("ImportAssessorAgent(" + fileName + ")");
		setFileName(fileName);
	}

	public void execute() {
		logger.info("Assessor Import started ....");
		insertAssessorImportLog(start, "START", "Assessor Import started.Checking configuration...");

		// configuration setup
		logger.debug("CONFIG : Reading 'elms.properties' file for configuration");

		try {
			assessorProperties = Wrapper.getResourceBundle();
		} catch (Exception e) {
			logger.error("CONFIG Problems : 'elms.properties' not found in classpath");
		}

		String checkinFolderPath = null;

		try {
			checkinFolderPath = assessorProperties.getString("ASSESSOR_CHECKIN_FOLDER");
		} catch (Exception e) {
			logger.error("CONFIG Problems : ASSESSOR_CHECKIN_FOLDER value not found in 'elms.properties' file");
		}

		logger.debug("CONFIG: Reading check-in folder path as " + checkinFolderPath);

		String checkoutFolderPath = null;

		try {
			checkoutFolderPath = assessorProperties.getString("ASSESSOR_CHECKOUT_FOLDER");
		} catch (Exception e) {
			logger.error("CONFIG Problems : ASSESSOR_CHECKOUT_FOLDER value not found in 'elms.properties' file");
		}

		logger.debug("CONFIG: Reading check-out folder path as " + checkoutFolderPath);

		String exceptionFolderPath = null;

		try {
			exceptionFolderPath = assessorProperties.getString("ASSESSOR_EXCEPTION_FOLDER");
		} catch (Exception e) {
			logger.error("CONFIG Problems : ASSESSOR_EXCEPTION_FOLDER value not found in 'elms.properties' file");
		}

		logger.debug("CONFIG: Reading exception folder path as " + exceptionFolderPath);
		insertAssessorImportLog(start, "CONFIGURATION", "configuration setup successful.Starting import...");

		Wrapper db = new Wrapper();
		int j = 0;

		BufferedReader input = null;

		try {
			String readLine;
			stmtAssessor = new Wrapper().getConnectionForPreparedStatementOnly().prepareStatement(ASSESSOR_SQL);

			// delete the records in the backup database.
			logger.info("Deleting Assessor Backup Records");
			db.update("DELETE FROM ASSESSOR_BAK");
			logger.info("Deleting Owner Backup Records");
			db.update("DELETE FROM OWNER_BAK");

			logger.info("deleting of backup tables data complete");
			insertAssessorImportLog(progress, "BACKUP", "Deleting of assessor backup data complete");

			// copy contents of tables to corresponding backups
			logger.info("Copying Assessor Records");
			db.update("INSERT INTO ASSESSOR_BAK SELECT * FROM ASSESSOR");
			logger.info("Copying Owner Records");
			db.update("INSERT INTO OWNER_BAK SELECT * FROM OWNER");

			logger.info("copying of tables data to corresponding backups complete");
			insertAssessorImportLog(progress, "BACKUP", "Backup of assessor tables data complete");

			// delete the records in the assessor table.
			logger.info("Deleting Assessor  Records");
			db.update("DELETE FROM ASSESSOR");
			logger.info("deleting of tables data complete");
			insertAssessorImportLog(progress, "DELETE", "Deleting of assessor tables data complete");

			/*
			 * Reading from Flat file and inserting to Assessor table
			 */
			logger.info("Reading from Flat File");
			insertAssessorImportLog(progress, "FILE", "Start reading data from flat file");

			String newFileName = checkinFolderPath + File.separator + getFileName();

			logger.debug("The updated filename is " + newFileName);

			FileInputStream fin = null;

			try {
				fin = new FileInputStream(newFileName);
			} catch (Exception e) {
				logger.error("Problem while reading import file " + e.getMessage());
			}

			input = new BufferedReader(new InputStreamReader(fin));
			while (((readLine = input.readLine()) != null)) {
				if ((++j % 1000) == 0) {
					logger.info("Processing Record : " + j);
				}
				try {
					processRecord(readLine);
				} catch (Exception e) {
					logger.error("Unable to insert record #" + j + ". Error is " + e.getMessage());
					insertAssessorImportLog(progress, "ERROR", "Unable to insert record#  " + j + " Error is " + e.getMessage());

				}
			}

			input.close();
			stmtAssessor.close();
			logger.info("Finished Reading " + j + " records from Flat File");
			insertAssessorImportLog(progress, "FILE", "Finished Reading " + j + " records from Flat File");
			insertAssessorImportLog(progress, "ASSESSOR", "Finished inserting " + j + " records from Flat File to Assessor table");

			logger.info("Processing Owner Records");
			processOwner(db);

			logger.info("Assessor data import complete.");
			insertAssessorImportLog(complete, "IMPORT", "Assessor data import complete.");
		} catch (Exception ex) {
			logger.error("Exception thrown, program did not compelte" + ex.getMessage());
			insertAssessorImportLog(complete, "ERROR", "ERROR Occurred and the assessor data import did not compelte.");
		} finally {
			try {
				if (input != null) {
					input.close();
				}
			} catch (Exception ex) {
				logger.error(ex.getMessage());
			}
		}
	}

	/**
	 * Insert the rows into the corresponding table
	 */
	private void processRecord(String record) throws SQLException {
		stmtAssessor.setString(1, record.substring(0, 10));
		stmtAssessor.setString(2, "" + StringUtils.s2i(record.substring(10, 15)));
		stmtAssessor.setString(3, "" + StringUtils.s2i(record.substring(15, 21)));
		stmtAssessor.setString(4, "" + StringUtils.s2i(record.substring(21, 25)));
		stmtAssessor.setString(5, "" + StringUtils.s2i(record.substring(25, 34)));
		stmtAssessor.setString(6, "" + StringUtils.s2i(record.substring(34, 38)));
		stmtAssessor.setString(7, "" + StringUtils.s2i(record.substring(38, 47)));
		stmtAssessor.setString(8, "" + StringUtils.s2i(record.substring(47, 52)));
		stmtAssessor.setString(9, record.substring(52, 55));
		stmtAssessor.setString(10, record.substring(55, 56));
		stmtAssessor.setString(11, record.substring(56, 88));
		stmtAssessor.setString(12, record.substring(88, 96));
		stmtAssessor.setString(13, record.substring(96, 120));
		stmtAssessor.setString(14, record.substring(120, 129));
		stmtAssessor.setString(15, "" + StringUtils.s2i(record.substring(129, 134)));
		stmtAssessor.setString(16, record.substring(134, 137));
		stmtAssessor.setString(17, record.substring(137, 138));
		stmtAssessor.setString(18, record.substring(138, 170));
		stmtAssessor.setString(19, record.substring(170, 178));
		stmtAssessor.setString(20, record.substring(178, 202));
		stmtAssessor.setString(21, record.substring(202, 211));
		stmtAssessor.setString(22, record.substring(211, 243));
		stmtAssessor.setString(23, record.substring(243, 275));
		stmtAssessor.setString(24, record.substring(275, 280));
		stmtAssessor.setString(25, record.substring(280, 312));
		stmtAssessor.setString(26, record.substring(312, 344));
		stmtAssessor.setString(27, record.substring(344, 352));
		stmtAssessor.setString(28, record.substring(352, 353));
		stmtAssessor.setString(29, "" + StringUtils.s2i(record.substring(353, 357)));
		stmtAssessor.setString(30, record.substring(357, 358));
		stmtAssessor.setString(31, "" + StringUtils.s2i(record.substring(358, 368)));
		stmtAssessor.setString(32, record.substring(368, 383));
		stmtAssessor.setString(33, record.substring(383, 387));
		stmtAssessor.setString(34, "" + StringUtils.s2i(record.substring(387, 390)));
		stmtAssessor.setString(35, record.substring(390, 391));
		stmtAssessor.setString(36, record.substring(391, 392));
		stmtAssessor.setString(37, record.substring(392, 393));
		stmtAssessor.setString(38, record.substring(393, 394));
		stmtAssessor.setString(39, "" + StringUtils.s2i(record.substring(394, 403)));
		stmtAssessor.setString(40, "" + StringUtils.s2i(record.substring(403, 412)));
		stmtAssessor.setString(41, "" + StringUtils.s2i(record.substring(412, 421)));
		stmtAssessor.setString(42, "" + StringUtils.s2i(record.substring(421, 430)));
		stmtAssessor.setString(43, "" + StringUtils.s2i(record.substring(430, 433)));
		stmtAssessor.setString(44, "" + StringUtils.s2i(record.substring(433, 442)));
		stmtAssessor.setString(45, "" + StringUtils.s2i(record.substring(442, 451)));
		stmtAssessor.setString(46, record.substring(451, 452));
		stmtAssessor.setString(47, record.substring(452, 461));
		stmtAssessor.setString(48, record.substring(461, 469));
		stmtAssessor.setString(49, record.substring(469, 470));
		stmtAssessor.setString(50, record.substring(470, 479));
		stmtAssessor.setString(51, record.substring(479, 487));
		stmtAssessor.setString(52, record.substring(487, 488));
		stmtAssessor.setString(53, record.substring(488, 497));
		stmtAssessor.setString(54, record.substring(497, 505));
		stmtAssessor.setString(55, "" + StringUtils.s2i(record.substring(505, 509)));
		stmtAssessor.setString(56, record.substring(509, 513));
		stmtAssessor.setString(57, record.substring(513, 518));
		stmtAssessor.setString(58, "" + StringUtils.s2i(record.substring(518, 522)));
		stmtAssessor.setString(59, "" + StringUtils.s2i(record.substring(522, 525)));
		stmtAssessor.setString(60, "" + StringUtils.s2i(record.substring(525, 527)));
		stmtAssessor.setString(61, "" + StringUtils.s2i(record.substring(527, 529)));
		stmtAssessor.setString(62, "" + StringUtils.s2i(record.substring(529, 536)));
		stmtAssessor.setString(63, "" + StringUtils.s2i(record.substring(536, 540)));
		stmtAssessor.setString(64, record.substring(540, 544));
		stmtAssessor.setString(65, record.substring(544, 549));
		stmtAssessor.setString(66, "" + StringUtils.s2i(record.substring(549, 553)));
		stmtAssessor.setString(67, "" + StringUtils.s2i(record.substring(553, 556)));
		stmtAssessor.setString(68, "" + StringUtils.s2i(record.substring(556, 558)));
		stmtAssessor.setString(69, "" + StringUtils.s2i(record.substring(558, 560)));
		stmtAssessor.setString(70, "" + StringUtils.s2i(record.substring(560, 567)));
		stmtAssessor.setString(71, "" + StringUtils.s2i(record.substring(567, 571)));
		stmtAssessor.setString(72, record.substring(571, 575));
		stmtAssessor.setString(73, record.substring(575, 580));
		stmtAssessor.setString(74, "" + StringUtils.s2i(record.substring(580, 584)));
		stmtAssessor.setString(75, "" + StringUtils.s2i(record.substring(584, 587)));
		stmtAssessor.setString(76, "" + StringUtils.s2i(record.substring(587, 589)));
		stmtAssessor.setString(77, "" + StringUtils.s2i(record.substring(589, 591)));
		stmtAssessor.setString(78, "" + StringUtils.s2i(record.substring(591, 598)));
		stmtAssessor.setString(79, "" + StringUtils.s2i(record.substring(598, 602)));
		stmtAssessor.setString(80, record.substring(602, 606));
		stmtAssessor.setString(81, record.substring(606, 611));
		stmtAssessor.setString(82, "" + StringUtils.s2i(record.substring(611, 615)));
		stmtAssessor.setString(83, "" + StringUtils.s2i(record.substring(615, 618)));
		stmtAssessor.setString(84, "" + StringUtils.s2i(record.substring(618, 620)));
		stmtAssessor.setString(85, "" + StringUtils.s2i(record.substring(620, 622)));
		stmtAssessor.setString(86, "" + StringUtils.s2i(record.substring(622, 629)));
		stmtAssessor.setString(87, "" + StringUtils.s2i(record.substring(629, 633)));
		stmtAssessor.setString(88, record.substring(633, 637));
		stmtAssessor.setString(89, record.substring(637, 642));
		stmtAssessor.setString(90, "" + StringUtils.s2i(record.substring(642, 646)));
		stmtAssessor.setString(91, "" + StringUtils.s2i(record.substring(646, 649)));
		stmtAssessor.setString(92, "" + StringUtils.s2i(record.substring(649, 651)));
		stmtAssessor.setString(93, "" + StringUtils.s2i(record.substring(651, 653)));
		stmtAssessor.setString(94, "" + StringUtils.s2i(record.substring(653, 660)));
		stmtAssessor.setString(95, record.substring(660, 700));
		stmtAssessor.setString(96, record.substring(700, 740));
		stmtAssessor.setString(97, record.substring(740, 780));
		stmtAssessor.setString(98, record.substring(780, 820));
		stmtAssessor.setString(99, record.substring(820, 860));
		stmtAssessor.setString(100, record.substring(860, 900));
		stmtAssessor.execute();
	}

	/**
	 * process owner information.
	 * 
	 * @param db
	 * @throws SQLException
	 * @throws Exception
	 */
	private void processOwner(Wrapper db) throws Exception {
		insertAssessorImportLog(progress, "OWNER", "Started updating the OWNER table");

		String city = "", state = "", addrHouseNo = "", addrFraction = "", addrDir = "", addrSt = "", addrUnit = "", addrCity = "", addrZip = "", ownerName = "", apn = "";

		try {
			insertAssessorImportLog(progress, "OWNER", "Started import of OWNER table");

			String assessorSql = "SELECT FRST_OWNR_NAME,MAIL_ADDR_HOUSE_NO,MAIL_ADDR_FRACTION,MAIL_ADDR_DIR,MAIL_ADDR_ST_NAME,MAIL_ADDR_UNIT,MAIL_ADDR_CTY_STA,MAIL_ADDR_ZIP,APN FROM ASSESSOR ORDER BY FRST_OWNR_NAME,MAIL_ADDR_HOUSE_NO,MAIL_ADDR_FRACTION,MAIL_ADDR_DIR,MAIL_ADDR_ST_NAME,MAIL_ADDR_UNIT,MAIL_ADDR_CTY_STA,MAIL_ADDR_ZIP,APN";
			logger.debug(assessorSql);

			RowSet assessorResults = new Wrapper().select(assessorSql);
			while (assessorResults.next()) {

				ownerName = assessorResults.getString("FRST_OWNR_NAME").trim();
				addrHouseNo = assessorResults.getString("MAIL_ADDR_HOUSE_NO").trim();
				addrFraction = assessorResults.getString("MAIL_ADDR_FRACTION").trim();
				addrDir = assessorResults.getString("MAIL_ADDR_DIR").trim();
				addrSt = assessorResults.getString("MAIL_ADDR_ST_NAME").trim();
				addrUnit = assessorResults.getString("MAIL_ADDR_UNIT").trim();
				addrCity = assessorResults.getString("MAIL_ADDR_CTY_STA");
				addrZip = assessorResults.getString("MAIL_ADDR_ZIP").substring(0, 5);
				apn = assessorResults.getString("APN").trim();

				city = addrCity.trim().replace(',', ' ');
				city = addrCity.trim().replace('"', ' ');

				if (city.endsWith("BURBANK")) {
					city = "BUBANK CA";
				}
				if (city.length() > 0) {
					if (city.lastIndexOf(' ') > 0) {
						state = city.substring(city.lastIndexOf(' ')).trim();
						if (state.equals("CALIF")) {
							state = "CA";
							city = city.substring(0, city.length() - 3);
						} else if (state.equals("TEXAS")) {
							state = "TX";
							city = city.substring(0, city.length() - 5) + state;
						} else if (state.equals("ARIZ")) {
							state = "AZ";
							city = city.substring(0, city.length() - 4) + state;
						} else if (city.endsWith(",CA") || city.endsWith("CAL") || city.endsWith("C A") || city.endsWith("CAA")) {
							state = "CA";
							city = city.substring(0, city.length() - 3) + state;
						} else if (city.endsWith("NEV") || city.endsWith("NVA")) {
							state = "NV";
							city = city.substring(0, city.length() - 5) + state;
						} else if (city.endsWith("ILL")) {
							state = "IL";
							city = city.substring(0, city.length() - 1);
						} else if (city.endsWith("N Y")) {
							state = "NY";
							city = city.substring(0, city.length() - 3).trim() + " " + state;
						} else if (city.endsWith("N J")) {
							state = "NJ";
							city = city.substring(0, city.length() - 3).trim() + " " + state;
						} else if (city.endsWith("CA") && !state.equals("CA")) {
							state = "CA";
							city = city.substring(0, city.length() - 2).trim() + " " + state;
						}

					}
				}

				logger.debug("City :" + city + ":" + state + ":");

				String ownerSql = "Select owner_id from apn_owner where apn=" + StringUtils.checkString(apn);

				logger.debug(ownerSql);
				int ownerId = -1;
				RowSet ownerResults = null;
				ownerResults = new Wrapper().select(ownerSql);
				if (ownerResults.next()) {
					// owner record exists, we are updating the record
					ownerId = ownerResults.getInt("owner_id");
					if (state.length() == 2) {
						// US address
						logger.debug("US Address");
						if (city.length() > 2)
							city = city.substring(0, city.length() - 2);
						String usOwnerSql = "UPDATE OWNER SET FOREIGN_ADDRESS = 'N',NAME=" + StringUtils.checkString(ownerName) + ",STR_NO=" + addrHouseNo + ",STR_MOD=" + StringUtils.checkString(addrFraction) + ",PRE_DIR=" + StringUtils.checkString(addrDir) + ",STR_NAME=" + StringUtils.checkString(addrSt) + ",UNIT=" + StringUtils.checkString(addrUnit) + ",CITY=" + StringUtils.checkString(city) + ",STATE=" + StringUtils.checkString(state) + ",ZIP=" + addrZip + " WHERE OWNER_ID=" + ownerId;
						logger.debug(usOwnerSql);
						new Wrapper().insert(usOwnerSql);
					} else {
						// foreign address
						logger.debug("Foreign Address");
						String addressLine1 = addrHouseNo.equals("0") ? addrSt : (addrHouseNo + " " + addrSt);
						String foreignOwnerSql = "UPDATE OWNER SET FOREIGN_ADDRESS='Y',NAME=" + StringUtils.checkString(ownerName) + ",ADDRESS_LINE1=" + StringUtils.checkString(addressLine1) + ",ADDRESS_LINE2=" + StringUtils.checkString(addrUnit) + ",ADDRESS_LINE3=" + StringUtils.checkString(city) + " WHERE OWNER_ID=" + ownerId;
						logger.debug(foreignOwnerSql);
						new Wrapper().insert(foreignOwnerSql);
					}
				} else {
					// Owner record does not exist, output exception file.
					String message = "APN:" + apn + " not found in LSO database, Please add manually. Related data are Owner name:" + ownerName + ",Street number:" + addrHouseNo + ",Street Mod:" + addrFraction + ",Prefix direction:" + addrDir + ",Street Name:" + addrSt + ",Unit:" + addrUnit + ",City:" + city + ",State:" + state + ",Zip:" + addrZip;
					logger.error(message);
					writeLog(message);
				}

			}
		} catch (Exception e) {
			throw e;
		}

	}

	/**
	 * @return
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param string
	 */
	public void setFileName(String string) {
		fileName = string;
	}

	/**
	 * insert data to the assessor import log
	 * 
	 * @param status
	 * @param table
	 * @param message
	 */
	public void insertAssessorImportLog(String status, String table, String message) {
		logger.info("insertAssessorImportLog(" + status + ", " + table + ", " + message + ")");

		Connection connection = null;
		PreparedStatement statement = null;

		try {
			connection = new Wrapper().getConnectionForPreparedStatementOnly();
			statement = connection.prepareStatement(INSERT_ASSESSOR_IMPORT_LOG);
			statement.setString(1, status);
			statement.setString(2, table);
			statement.setString(3, message);
			statement.execute();
		} catch (Exception e) {
			logger.error("Exception occured while updating database log ");
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("ignored exception, not able to close connection");
			}
		}
	}

	/**
	 * insert data to the assessor import log
	 * 
	 * @param status
	 * @param table
	 * @param message
	 */
	public void writeLog(String message) {
		logger.info("writeLog(" + message + ")");

		FileWriter aWriter = null;

		try {
			// format the date
			TimeZone tz = TimeZone.getTimeZone("PST"); // or PST, MID, etc ...
			java.util.Date now = new java.util.Date();
			DateFormat df = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
			df.setTimeZone(tz);

			String currentTime = df.format(now);
			logger.info("writeLog(" + currentTime + ")");

			// get the file path and file
			java.util.ResourceBundle obcProperties = Wrapper.getResourceBundle();
			String cfString = obcProperties.getString("ASSESSOR_EXCEPTION_FOLDER");
			logger.info("writeLog(" + "before naming file " + ")");
			String defaultLogFile = cfString + File.separator + "ASSESSOR_IMPORT_EXCEPTION.txt";

			logger.info("writeLog(" + "before writing log" + defaultLogFile + ")");
			aWriter = new FileWriter(defaultLogFile, true);
			logger.info("created file");
			aWriter.write(currentTime + " " + message + System.getProperty("line.separator"));
		} catch (Exception e) {
			logger.error("Exception occured while updating file log ");
		} finally {
			try {
				aWriter.flush();
				aWriter.close();
			} catch (Exception e) {
				// logger.error("ignored exception, not able to close file");
			}
		}
	}

	/**
	 * Get status
	 * 
	 * @return
	 */
	public RowSet getStatus() {
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from assessor_import_log order by updated";
			rs = db.select(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return rs;
	}

	/**
	 * Get status
	 * 
	 * @return
	 */
	public String getLatestStatus() {
		String latestStatus = null;

		try {
			RowSet rs = new Wrapper().select(GET_LATEST_STATUS);

			if (rs.next()) {
				latestStatus = rs.getString(1);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return latestStatus;
	}

	/**
	 * Delete all the logs of the assessor import
	 * 
	 */
	public void deleteLogs() {
		try {
			new Wrapper().update(DELETE_ALL_LOGS);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
package elms.agent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import sun.jdbc.rowset.CachedRowSet;
import elms.app.admin.ParkingZone;
import elms.app.admin.lso.AddressRangeEdit;
import elms.app.admin.lso.OccupancyEdit;
import elms.app.admin.lso.StructureEdit;
import elms.app.common.Agent;
import elms.app.common.Attachment;
import elms.app.common.Comment;
import elms.app.common.Condition;
import elms.app.common.DisplayItem;
import elms.app.common.Hold;
import elms.app.lso.Address;
import elms.app.lso.AddressSummary;
import elms.app.lso.ForeignAddress;
import elms.app.lso.Land;
import elms.app.lso.LandAddress;
import elms.app.lso.LandApn;
import elms.app.lso.LandDetails;
import elms.app.lso.LandSiteData;
import elms.app.lso.LotEdit;
import elms.app.lso.LsoAddress;
import elms.app.lso.LsoTree;
import elms.app.lso.Occupancy;
import elms.app.lso.OccupancyAddress;
import elms.app.lso.OccupancyApn;
import elms.app.lso.OccupancyDetails;
import elms.app.lso.OccupancySiteData;
import elms.app.lso.Owner;
import elms.app.lso.OwnerApnAddress;
import elms.app.lso.Street;
import elms.app.lso.Structure;
import elms.app.lso.StructureAddress;
import elms.app.lso.StructureDetails;
import elms.app.lso.StructureSiteData;
import elms.app.lso.Use;
import elms.app.lso.Zone;
import elms.app.sitedata.SiteData;
import elms.app.sitedata.SiteSetback;
import elms.app.sitedata.SiteStructureData;
import elms.common.Constants;
import elms.control.beans.LsoSearchForm;
import elms.control.beans.admin.StreetListAdminForm;
import elms.exception.AgentException;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class AddressAgent extends Agent {

	static Logger logger = Logger.getLogger(AddressAgent.class.getName());
	String lsoType = "";

	public String[] zone = null;

	protected Occupancy occupancy;
	protected OccupancyDetails occupancyDetails;
	protected List occupancyAddress;
	protected OccupancyAddress occAddrListObj;
	protected List occupancyApn;
	protected OccupancyApn occApnListObj;
	protected List occupancyHold;
	protected List occupancyCondition;
	protected List occupancyAttachment;
	protected OccupancySiteData occupancySiteData;
	protected List occupancyComment;

	static final String GET_UNIT_LIST = "SELECT * FROM SITE_SURVEY_UNIT WHERE STRUCT_NBR = ";

	/**
	 * The database queries
	 */
	public static final String GET_ACTIVE_STRUCTURE_331 = "SELECT DISTINCT LAND_ID AS LSO_ID FROM ( LSO_ADDRESS LA join LAND_STRUCTURE LS on LA.LSO_ID=LS.LAND_ID) join STRUCTURE S on LS.STRUCTURE_ID = S.STRUCTURE_ID  WHERE S.ACTIVE ='Y' and LSO_TYPE='S' and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ACTIVE_OCCUPANCY_331 = "SELECT DISTINCT LAND_ID AS LSO_ID FROM (LSO_ADDRESS LA join V_LSO VLSO on LA.LSO_ID=VLSO.LAND_ID) join OCCUPANCY O on VLSO.OCCUPANCY_ID = O.OCCUPANCY_ID  WHERE O.ACTIVE= 'Y' and LSO_TYPE='O'  and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ALL_STRUCTURE_331 = "SELECT DISTINCT LAND_ID AS LSO_ID FROM LSO_ADDRESS LA JOIN LAND_STRUCTURE LS ON LA.LSO_ID=LS.STRUCTURE_ID WHERE LSO_TYPE='S' and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ALL_OCCUPANCY_331 = "SELECT DISTINCT LAND_ID AS LSO_ID FROM LSO_ADDRESS LA JOIN V_LSO VLSO ON LA.LSO_ID=VLSO.OCCUPANCY_ID WHERE LSO_TYPE='O' and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ACTIVE_LAND = "SELECT DISTINCT L.LAND_ID  AS LSO_ID, LA.STR_NO FROM LAND L join LSO_ADDRESS LA  on L.LAND_ID = LA.LSO_ID WHERE LA.ACTIVE='Y' and LA.LSO_TYPE='L' and LA.STR_NO like ? AND LA.STREET_ID = ? ORDER BY STR_NO";
	public static final String GET_ACTIVE_STRUCTURE = "SELECT DISTINCT LAND_ID AS LSO_ID FROM ( LSO_ADDRESS LA join LAND_STRUCTURE LS on LA.LSO_ID=LS.LAND_ID) join STRUCTURE S on LS.STRUCTURE_ID = S.STRUCTURE_ID  WHERE S.ACTIVE ='Y' and LSO_TYPE='S' and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ACTIVE_OCCUPANCY = "SELECT DISTINCT LAND_ID AS LSO_ID FROM (LSO_ADDRESS LA join V_LSO VLSO on LA.LSO_ID=VLSO.LAND_ID) join OCCUPANCY O on VLSO.OCCUPANCY_ID = O.OCCUPANCY_ID  WHERE O.ACTIVE= 'Y' and LSO_TYPE='O'  and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ALL_LAND = "SELECT DISTINCT LSO_ID, STR_NO FROM LSO_ADDRESS WHERE lso_type='L' and STR_NO like ? AND STREET_ID = ? ORDER BY STR_NO";
	public static final String GET_ALL_STRUCTURE = "SELECT DISTINCT LAND_ID AS LSO_ID FROM LSO_ADDRESS LA JOIN LAND_STRUCTURE LS ON LA.LSO_ID=LS.LAND_ID WHERE LSO_TYPE='S' and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ALL_OCCUPANCY = "SELECT DISTINCT LAND_ID AS LSO_ID FROM LSO_ADDRESS LA JOIN V_LSO VLSO ON LA.LSO_ID=VLSO.LAND_ID WHERE LSO_TYPE='O' and STR_NO like ? AND STREET_ID = ? ";
	public static final String GET_ACTIVE_STRUCTURE_TREE = "SELECT LS.STRUCTURE_ID,'S' AS LSO_TYPE,V.ADDRESS,S.DESCRIPTION FROM STRUCTURE S left outer join V_ADDRESS_LIST V on V.LSO_ID=S.STRUCTURE_ID,LAND_STRUCTURE LS WHERE S.STRUCTURE_ID = LS.STRUCTURE_ID and S.ACTIVE= 'Y'  AND LAND_ID = ? ORDER BY V.DL_ADDRESS";
	public static final String GET_ACTIVE_OCCUPANCY_TREE = "SELECT SO.OCCUPANCY_ID,'O' AS LSO_TYPE,V.ADDRESS,O.DESCRIPTION,O.UNIT FROM OCCUPANCY O left outer join V_ADDRESS_LIST V on V.LSO_ID=O.OCCUPANCY_ID,STRUCTURE_OCCUPANT SO WHERE O.OCCUPANCY_ID = SO.OCCUPANCY_ID and O.ACTIVE= 'Y'  AND STRUCTURE_ID = ?  and (o.UNIT is not null) ORDER BY UNIT";
	public static final String GET_ACTIVE_OCCUPANCY_TREE_NULL_UNIT = "SELECT SO.OCCUPANCY_ID,'O' AS LSO_TYPE,V.ADDRESS,O.DESCRIPTION,O.UNIT FROM OCCUPANCY O left outer join V_ADDRESS_LIST V on V.LSO_ID=O.OCCUPANCY_ID,STRUCTURE_OCCUPANT SO WHERE O.OCCUPANCY_ID = SO.OCCUPANCY_ID and O.ACTIVE= 'Y'  AND STRUCTURE_ID = ?  and (o.UNIT is null) ORDER BY UNIT";
	public static final String GET_ALL_STRUCTURE_TREE = "SELECT LS.STRUCTURE_ID,'S' AS LSO_TYPE,V.ADDRESS,S.DESCRIPTION FROM STRUCTURE S left outer join V_ADDRESS_LIST V on V.LSO_ID=S.STRUCTURE_ID,LAND_STRUCTURE LS WHERE S.STRUCTURE_ID = LS.STRUCTURE_ID AND LAND_ID = ? ORDER BY V.ADDRESS";
	public static final String GET_ALL_OCCUPANCY_TREE = "SELECT SO.OCCUPANCY_ID,'O' AS LSO_TYPE,V.ADDRESS,O.DESCRIPTION,O.UNIT FROM OCCUPANCY O left outer join V_ADDRESS_LIST V on V.LSO_ID=O.OCCUPANCY_ID,STRUCTURE_OCCUPANT SO WHERE O.OCCUPANCY_ID = SO.OCCUPANCY_ID AND STRUCTURE_ID = ? and (o.UNIT is not null) ORDER BY UNIT";
	public static final String GET_ALL_OCCUPANCY_UNIT_NULL_TREE = "SELECT SO.OCCUPANCY_ID,'O' AS LSO_TYPE,V.ADDRESS,O.DESCRIPTION,O.UNIT FROM OCCUPANCY O left outer join V_ADDRESS_LIST V on V.LSO_ID=O.OCCUPANCY_ID,STRUCTURE_OCCUPANT SO WHERE O.OCCUPANCY_ID = SO.OCCUPANCY_ID AND STRUCTURE_ID = ? and (o.UNIT is null) ORDER BY UNIT";
	public static final String GET_LSO = "SELECT DISTINCT ADDR_ID,LSO_ID,LSO_TYPE,STR_NO,L.UNIT, L.DESCRIPTION, V.STREET_NAME AS STREET_NAME, STR_MOD FROM LSO_ADDRESS L,V_STREET_LIST V WHERE v.STREET_ID=l.STREET_ID AND l.PRIMARY='Y' AND LSO_ID=? ORDER BY STR_NO";
	public static final String SEARCH_ADDRESS_RANGE = "SELECT LSO_ID,START_RANGE,END_RANGE FROM LSO_ADDR_RANGE WHERE STREET_ID =? AND START_RANGE <= ? AND END_RANGE >= ?";
	public static final String GET_LSO_TYPE = "SELECT LSO_TYPE FROM LSO_ADDRESS WHERE LSO_ID = ?";
	public static final String GET_LAND_FOR_STRUCTURE = "SELECT LAND_ID FROM LAND_STRUCTURE WHERE STRUCTURE_ID = ?";
	public static final String GET_LAND_FOR_OCCUPANCY = "SELECT LAND_ID FROM LAND_STRUCTURE WHERE STRUCTURE_ID IN(SELECT STRUCTURE_ID FROM STRUCTURE_OCCUPANT WHERE OCCUPANCY_ID = ?)";
	public static final String GET_COMPLETE_LSO_LIST = "SELECT LSO_ID,LSO_TYPE,STR_NO,V.STREET_NAME AS STREET_NAME FROM LSO_ADDRESS L,V_STREET_LIST V WHERE v.STREET_ID=l.STREET_ID AND l.PRIMARY='Y' AND LSO_ID=? ORDER BY STR_NO,STREET_NAME";
	public static final String GET_COMPLETE_STRUCTURE_LIST = "SELECT LS.STRUCTURE_ID,LA.LSO_TYPE,LA.STR_NO,V.STREET_NAME FROM LAND_STRUCTURE LS,LSO_ADDRESS LA,V_STREET_LIST V WHERE LA.LSO_ID=LS.STRUCTURE_ID  AND LA.STREET_ID=V.STREET_ID AND LAND_ID = ?";
	public static final String GET_COMPLETE_STRUCTURE_REVERSE_LIST = "SELECT LS.LAND_ID,LA.LSO_TYPE,LA.STR_NO,V.STREET_NAME FROM LAND_STRUCTURE LS,LSO_ADDRESS LA,V_STREET_LIST V WHERE LA.LSO_ID=LS.LAND_ID AND LA.STREET_ID=V.STREET_ID AND STRUCTURE_ID = ?";
	public static final String GET_COMPLETE_OCCUPANCY_LIST = "SELECT SO.OCCUPANCY_ID,LA.LSO_TYPE,LA.STR_NO,V.STREET_NAME FROM STRUCTURE_OCCUPANT SO,LSO_ADDRESS LA,V_STREET_LIST V WHERE LA.LSO_ID=SO.OCCUPANCY_ID AND LA.STREET_ID=V.STREET_ID AND STRUCTURE_ID = ?";
	public static final String GET_COMPLETE_OCCUPANCY_REVERSE_LIST = "SELECT SO.STRUCTURE_ID,LA.LSO_TYPE,LA.STR_NO,V.STREET_NAME FROM STRUCTURE_OCCUPANT SO,LSO_ADDRESS LA,V_STREET_LIST V WHERE LA.LSO_ID=SO.STRUCTURE_ID AND LA.STREET_ID=V.STREET_ID AND OCCUPANCY_ID = ?";
	public static final String GET_STREET_FOR_LSO = "SELECT STREET_ID FROM LSO_ADDRESS WHERE LSO_ID = ? FETCH FIRST 1 ROWS ONLY";
	public static final String GET_LAND_LSO_ID_2 = "SELECT DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS WHERE LSO_ADDRESS.lso_type='L' AND STR_NO LIKE ? AND STREET_ID = ?";
	public static final String GET_LAND_LSO_ID_3 = "SELECT DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS JOIN LSO_APN ON LSO_ADDRESS.LSO_ID = LSO_APN.LSO_ID WHERE LSO_ADDRESS.LSO_TYPE='L' AND LSO_ADDRESS.LSO_ID = LSO_APN.LSO_ID and STR_NO LIKE ? AND STREET_ID = ?  AND LSO_APN.APN LIKE ?";

	protected ResultSet rsDescription;

	/**
	 * The land
	 */
	protected Land land = null;

	protected LandDetails landDetails = null;
	protected List landAddressList = null;
	protected List landApnList = null;
	protected List landHold = null;
	protected List landTenant = null;
	protected List landCondition = null;
	protected List landAttachment = null;
	protected LandSiteData landSiteData = null;
	protected List landComment = null;
	protected List useList = null;
	protected List zoneList = null;
	protected Zone zoneObj = null;
	protected Use useObj = null;
	private LandAddress landAddressObj = null;
	private LandApn landApnObj = null;
	private int lsoId = -1;
	public static ResourceBundle obcProperties = null;
	// Structure Object itself
	protected Structure structure = null;
	protected StructureDetails structureDetails = null;
	protected StructureSiteData structureSiteData = null;
	private int landId = -1;

	// List of multiple objects in the structure object
	protected List structureAddressList = null;
	protected List occupancyApnList = null;
	protected List structureHold = null;
	protected List structureCondition = null;
	protected List structureAttachment = null;
	protected List structureComment = null;

	// individual objects in the list - which are objects themselves
	protected StructureAddress structureAddressObj = null;
	protected Hold structureHoldObj = null;
	protected Condition structureConditionObj = null;
	protected Attachment structureAttachmentObj = null;
	protected Comment structureCommentObj = null;

	Street street = null;

	protected Use use = null;
	protected String[] landUse;
	protected String[] structureUse;
	protected String[] occupancyUse;

	/**
	 * 3CD0322702D9
	 */
	public AddressAgent() {
		// default constructor
	}

	public String getLsoType(int lsoId) {
		try {
			Wrapper db = new Wrapper();

			String sql = "select lso_type from lso_address where lso_id=" + lsoId;
			ResultSet rs = db.select(sql);

			if (rs.next()) {
				lsoType = rs.getString(1);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update land details :" + e.getMessage());
		}

		return lsoType;
	}

	public Object getLso(int lsoId) {
		Object object = null;

		try {
			String lsoType = this.getLsoType(lsoId);

			if (lsoType.equalsIgnoreCase("L")) {
				Land land = getLand(lsoId);
				object = (Land) land;
				logger.debug("returning land object");
			} else if (lsoType.equalsIgnoreCase("S")) {
				Structure structure = getStructure(lsoId);
				object = (Structure) structure;
				logger.debug("returning structure object");
			} else if (lsoType.equalsIgnoreCase("O")) {
				Occupancy occupancy = getOccupancy(lsoId);
				object = (Occupancy) occupancy;
				logger.debug("returning occupancy object");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return object;
	}

	public LsoAddress getLsoAddress(int addressId) {
		LsoAddress lsoAddress = null;
		RowSet rs = null;

		try {
			String sql = "select la.addr_id,la.lso_id,la.str_no,la.str_mod,la.street_id,la.unit,la.city,la.state,la.zip,la.zip4,la.description,la.primary,la.active,vsl.street_name from lso_address la,v_street_list vsl where la.street_id=vsl.street_id and la.addr_id=" + addressId;
			logger.debug(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				lsoAddress = new LsoAddress();
				lsoAddress.setAddressId(rs.getInt("addr_id"));
				lsoAddress.setActive(rs.getString("active"));
				lsoAddress.setDescription(StringUtils.nullReplaceWithEmpty(rs.getString("description")));
				lsoAddress.setCity(StringUtils.nullReplaceWithEmpty(rs.getString("city")));
				lsoAddress.setState(rs.getString("state"));
				lsoAddress.setZip(rs.getString("zip"));
				lsoAddress.setZip4(rs.getString("zip4"));
				lsoAddress.setPrimary(rs.getString("primary"));
				lsoAddress.setStreetNbr(rs.getInt("str_no"));
				lsoAddress.setStreetModifier(StringUtils.nullReplaceWithEmpty(rs.getString("str_mod")));
				lsoAddress.setUnit(rs.getString("unit"));

				Street addressStreet = new Street();
				addressStreet.setStreetId(rs.getInt("street_id"));
				addressStreet.setStreetName(StringUtils.nullReplaceWithEmpty(rs.getString("street_name")));
				lsoAddress.setStreet(addressStreet);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lsoAddress;
	}

	public RowSet getLsoAddressList(String lsoId) {
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();

			String sql = "select * from lso_address la,v_street_list vsl where la.lso_id = " + lsoId + " and vsl.street_id = la.street_id" + " order by la.primary desc,vsl.street_name asc,la.str_no asc";
			rs = db.select(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return rs;
	}

	public RowSet getDigitalLibraryLogList() {
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			String sql = "select * from dl_exception order by created desc";
			rs = db.select(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return rs;
	}

	public void clearDigitalLibraryLogs(int recordId) throws Exception {
		try {
			String sql = "delete from dl_exception ";

			if (recordId != -1) {
				sql += ("where ID=" + recordId);
			}

			logger.debug(sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public LsoAddress updateLsoAddress(LsoAddress lsoAddress) {
		try {
			Wrapper db = new Wrapper();
			StringBuffer sqlBuffer = new StringBuffer();
			String sql = "";

			if (lsoAddress != null) {
				sqlBuffer.append("UPDATE LSO_ADDRESS SET CITY=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getCity()));
				logger.debug("got address city " + StringUtils.checkString(lsoAddress.getCity()));
				sqlBuffer.append(",STATE=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getState()));
				logger.debug("got address state " + StringUtils.checkString(lsoAddress.getState()));
				sqlBuffer.append(",DESCRIPTION=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getDescription()));
				logger.debug("got address description " + StringUtils.checkString(lsoAddress.getDescription()));
				sqlBuffer.append(",ACTIVE=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getActive()));
				logger.debug("got address active " + StringUtils.checkString(lsoAddress.getActive()));
				sqlBuffer.append(",PRIMARY=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getPrimary()));
				logger.debug("got address primary " + StringUtils.checkString(lsoAddress.getPrimary()));
				sqlBuffer.append(",STR_NO=");
				sqlBuffer.append(lsoAddress.getStreetNbr());
				logger.debug("got address street number " + lsoAddress.getStreetNbr());
				sqlBuffer.append(",STR_MOD=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getStreetModifier()));
				logger.debug("got address street modifier " + StringUtils.checkString(lsoAddress.getStreetModifier()));
				sqlBuffer.append(",UNIT=" + StringUtils.checkString(lsoAddress.getUnit()));
				sqlBuffer.append(",ZIP=");
				sqlBuffer.append(StringUtils.checkNumber(lsoAddress.getZip()));
				logger.debug("got address zip " + lsoAddress.getZip());
				sqlBuffer.append(",ZIP4=");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getZip4()));
				logger.debug("got address zip4 " + lsoAddress.getZip4());
				sqlBuffer.append(",ADDR_PRIMARY = ");
				sqlBuffer.append(lsoAddress.getAddressId());
				logger.debug("got address primary id " + lsoAddress.getAddressId());
				sqlBuffer.append(",LAST_UPDATED=current_timestamp");
				sqlBuffer.append(",UPDATED_BY=");
				sqlBuffer.append(lsoAddress.getUpdatedBy().getUserId());
				logger.debug("got updated user id " + lsoAddress.getUpdatedBy().getUserId());
				sqlBuffer.append(" WHERE ADDR_ID=");
				sqlBuffer.append(lsoAddress.getAddressId());
				logger.debug("got address id " + lsoAddress.getAddressId());
				db.beginTransaction();

				db.addBatch(sqlBuffer.toString());
				logger.debug("Lso Address update sql is :" + sqlBuffer.toString());

				// checking if primary address is changed and updating
				if (lsoAddress.getPrimary().equals("Y")) {
					sql = "update lso_address set primary = 'N',addr_primary =" + lsoAddress.getAddressId() + " where lso_id=" + lsoAddress.getLsoId() + " and addr_id !=" + lsoAddress.getAddressId();
					logger.debug("Add LSO address -- primary and addr_primary fields  update sql is :" + sql);
				} else {
					sql = "select addr_id from lso_address where primary='Y' and lso_id=" + lsoAddress.getLsoId() + " and addr_id !=" + lsoAddress.getAddressId();
					logger.debug("select Sql is" + sql);

					RowSet rs = db.select(sql);

					if (rs.next()) {
						sql = "update lso_address set addr_primary=" + rs.getInt("addr_id") + "  where addr_id=" + lsoAddress.getAddressId();
						logger.debug("updating addr_primary with corrosponding addressfield" + sql);
					} else {
						sql = "update lso_address set primary='Y',  addr_primary=" + lsoAddress.getAddressId() + "  where addr_id=" + lsoAddress.getAddressId();
						logger.debug("resetting primary to Y and addr_primary to addressId" + sql);
					}

					rs.close();
					rs = null;
				}

				db.addBatch(sql);
				db.executeBatch();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lsoAddress;
	}

	public LsoAddress addLsoAddress(LsoAddress lsoAddress, String lsoType) {
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			StringBuffer sqlBuffer = new StringBuffer();

			if (lsoAddress != null) {
				int addressId = db.getNextId("ADDRESS_ID");
				logger.debug("The ID generated for new Address is " + addressId);
				db.beginTransaction();

				sqlBuffer.append("insert into lso_address(addr_id,lso_id,lso_type,str_no,str_mod,street_id,unit,city,state,zip,zip4,description,primary,active,last_updated,updated_by,addr_primary) values(");
				sqlBuffer.append(addressId);
				logger.debug("got address id " + addressId);
				sqlBuffer.append(",");
				sqlBuffer.append(lsoAddress.getLsoId());
				logger.debug("got lso id " + lsoAddress.getLsoId());
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoType));
				logger.debug("got lso type " + lsoType);
				sqlBuffer.append(",");
				sqlBuffer.append(lsoAddress.getStreetNbr());
				logger.debug("got street number " + lsoAddress.getStreetNbr());
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getStreetModifier()));
				logger.debug("got address street modifier " + StringUtils.checkString(lsoAddress.getStreetModifier()));
				sqlBuffer.append(",");
				sqlBuffer.append(lsoAddress.getStreet().getStreetId());
				logger.debug("got address street id " + lsoAddress.getStreet().getStreetId());
				sqlBuffer.append("," + StringUtils.checkString(lsoAddress.getUnit()));
				sqlBuffer.append("," + StringUtils.checkString(lsoAddress.getCity()));
				logger.debug("got address city " + StringUtils.checkString(lsoAddress.getCity()));
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getState()));
				logger.debug("got address state " + StringUtils.checkString(lsoAddress.getState()));
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.s2i(lsoAddress.getZip()));
				logger.debug("got address zip " + StringUtils.s2i(lsoAddress.getZip()));
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getZip4()));
				logger.debug("got address zip4 " + StringUtils.s2i(lsoAddress.getZip4()));
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getDescription()));
				logger.debug("got address description " + StringUtils.checkString(lsoAddress.getDescription()));
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getPrimary()));
				logger.debug("got address primary " + StringUtils.checkString(lsoAddress.getPrimary()));
				sqlBuffer.append(",");
				sqlBuffer.append(StringUtils.checkString(lsoAddress.getActive()));
				logger.debug("got address active " + StringUtils.checkString(lsoAddress.getActive()));
				sqlBuffer.append(",current_timestamp");
				logger.debug("got current_timestamp ");
				sqlBuffer.append(",");
				sqlBuffer.append(lsoAddress.getUpdatedBy().getUserId());
				logger.debug("got updated user id " + lsoAddress.getUpdatedBy().getUserId());
				sqlBuffer.append(",");
				sqlBuffer.append(addressId);
				logger.debug("got addr primary user id " + addressId);
				sqlBuffer.append(")");

				db.addBatch(sqlBuffer.toString());

				logger.debug("Lso address insert sql is :" + sql);

				// checking if primary address is changed and updating
				if (lsoAddress.getPrimary().equals("Y")) {
					sql = "update lso_address set primary = 'N',addr_primary =" + addressId + " where lso_id=" + lsoAddress.getLsoId() + " and addr_id !=" + addressId;
					logger.debug("Add LSO address -- primary and addr_primary fields  update sql is :" + sql);
				} else {
					sql = "select addr_id from lso_address where primary='Y' and lso_id=" + lsoAddress.getLsoId() + " and addr_id !=" + lsoAddress.getAddressId();
					logger.debug("select Sql is" + sql);

					RowSet rs = db.select(sql);

					if (rs.next()) {
						sql = "update lso_address set addr_primary=" + rs.getInt("addr_id") + "  where addr_id=" + addressId;
						logger.debug("updating addr_primary with corrosponding addressfield" + sql);
					} else {
						sql = "update lso_address set primary='Y',  addr_primary=" + addressId + "  where addr_id=" + addressId;
						logger.debug("resetting primary to Y and addr_primary to addressId" + sql);
					}
				}

				db.addBatch(sql);
				db.executeBatch();
				lsoAddress = new AddressAgent().getLsoAddress(addressId);
			} else {
				logger.debug(" Lso Address is not Created");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lsoAddress;
	}

	public String searchAddress(Address address) {
		String apn = "";
		String sql = "SELECT APN FROM LSO_ADDRESS LA JOIN LSO_APN APN ON APN.LSO_ID=LA.LSO_ID " + " WHERE LA.STREET_ID =" + address.getStreet().getStreetId() + " AND LA.STR_NO =" + address.getStreetNbr();

		if (!address.getStreetModifier().equals("")) {
			sql += (" AND STR_MOD='" + address.getStreetModifier() + "'");
		}

		if (!address.getUnit().equals("")) {
			sql += (" AND UNIT='" + address.getUnit() + "'");
		}

		logger.debug(sql);

		Wrapper db = new Wrapper();

		try {
			RowSet rs = db.select(sql);

			if (rs.next()) {
				apn = rs.getString("APN");
			}

			rs.close();
		} catch (Exception e) {
			logger.warn("Error:" + e.getMessage());
		}

		return apn;
	}

	public List searchAddresses(String streetNumber, String streetName, String apn, String streetName2) throws Exception {
		logger.info("searchAddresses(" + streetNumber + ", " + streetName + ", " + apn + ", " + streetName2 + ")");

		List addressResults = new ArrayList();
		int intStreetName = -1;
		if (streetName != null && !("").equalsIgnoreCase(streetName)) {
			intStreetName = Integer.parseInt(streetName);
		}
		List lsoIds = new ArrayList();

		if (streetName2 != null && !(("").equalsIgnoreCase(streetName2)) && !(("-1").equalsIgnoreCase(streetName2))) {
			logger.debug("streetName2 ********** " + streetName2);
			String streetIdSql = "SELECT STREET_ID FROM STREET_LIST WHERE UPPER(STR_NAME || ' ' || STR_TYPE)='" + streetName2.toUpperCase() + "'";
			logger.info("streetIdSql :::: " + streetIdSql);

			RowSet streetIdRs = new Wrapper().select(streetIdSql);
			int streetId = 0;
			String streetId2 = "";

			while (streetIdRs != null && streetIdRs.next()) {
				streetId = streetIdRs.getInt("STREET_ID");
				logger.debug("streetIdList :::: " + streetId);
				streetId2 = streetId2 + streetId + "!";
			}
			lsoIds = getLsoIdList(streetNumber, 0, apn, streetId2);
		} else {
			lsoIds = getLsoIdList(streetNumber, intStreetName, apn, "");
		}

		logger.debug("obtained lso id list of size " + lsoIds.size());
		lsoIds = getCompleteLsoIdList(lsoIds);

		logger.debug("obtained complete lso id list of size " + lsoIds.size());

		String address = "";
		Iterator iter = lsoIds.iterator();

		while (iter.hasNext()) {
			String lsoId = (String) iter.next();
			String lsoType = new AddressAgent().getLsoType(StringUtils.s2i(lsoId));
			logger.debug("lso id is " + lsoId + "and lso type is " + lsoType);

			// rajs new rule|| lsoType.equalsIgnoreCase("O")

			String occApnSql = "SELECT * FROM LSO_APN WHERE LSO_ID = " + lsoId;
			logger.info(occApnSql);
			Wrapper db = new Wrapper();

			RowSet occApnRs = db.select(occApnSql);
			String occApn = "";
			while (occApnRs.next()) {
				occApn = occApnRs.getString("APN");
			}

			if (lsoType.equalsIgnoreCase("L")) {
				Land land = (Land) new AddressAgent().getLso(StringUtils.s2i(lsoId));
				logger.debug("got object land");

				if (land != null) {
					List landAddress = land.getLandAddress();

					if (landAddress != null) {
						if (landAddress.size() > 0) {
							LandAddress landAddressObj = (LandAddress) landAddress.get(0);

							if (landAddressObj != null) {
								String addressStreetNumber = StringUtils.i2s(landAddressObj.getStreetNbr());

								if (addressStreetNumber == null) {
									addressStreetNumber = "";
								}

								logger.debug("setting addressStreetNumber to the request " + addressStreetNumber);

								String addressStreetModifier = landAddressObj.getStreetModifier();

								if (addressStreetModifier == null) {
									addressStreetModifier = "";
								}

								logger.debug("setting addressStreetModifier to the request " + addressStreetModifier);

								Street addressStreet = landAddressObj.getStreet();
								String addressStreetName = "";

								if (addressStreet != null) {
									addressStreetName = addressStreet.getStreetName();

									if (addressStreetName == null) {
										addressStreetName = "";
									}

									logger.debug("setting addressStreetName to the request " + addressStreetName);
								}

								String addressCity = landAddressObj.getCity();

								if (addressCity == null) {
									addressCity = "";
								}

								String addressState = landAddressObj.getState();

								if (addressState == null) {
									addressState = "";
								}

								String addressZip = landAddressObj.getZip();

								if (addressZip == null) {
									addressZip = "";
								}

								String addressZip4 = landAddressObj.getZip4();

								if ((addressZip4 == null) || addressZip4.equals("0")) {
									addressZip4 = "";
								} else {
									addressZip4 = "-" + addressZip4;
								}

								address = addressStreetNumber + " " + addressStreetModifier + " " + addressStreetName + ", " + addressCity + ", " + addressState + " " + addressZip + addressZip4 + "|" + lsoId + "|" + lsoType;
								logger.debug(address);
								addressResults.add(address);
							}
						}
					}
				}
			} else if ((!apn.equals("")) && occApn.equalsIgnoreCase(apn) && lsoType.equalsIgnoreCase("O")) {
				Occupancy occupancy = (Occupancy) new AddressAgent().getLso(StringUtils.s2i(lsoId));
				logger.debug("got object Occupancy");

				if (occupancy != null) {
					List occupancyAddress = occupancy.getOccupancyAddress();

					if (occupancyAddress != null) {
						if (occupancyAddress.size() > 0) {
							OccupancyAddress occupancyAddressObj = (OccupancyAddress) occupancyAddress.get(0);

							if (occupancyAddressObj != null) {
								String addressStreetNumber = StringUtils.i2s(occupancyAddressObj.getStreetNbr());

								if (addressStreetNumber == null) {
									addressStreetNumber = "";
								}

								logger.debug("setting addressStreetNumber to the request " + addressStreetNumber);

								String addressStreetModifier = occupancyAddressObj.getStreetModifier();

								if (addressStreetModifier == null) {
									addressStreetModifier = "";
								}

								logger.debug("setting addressStreetModifier to the request " + addressStreetModifier);

								Street addressStreet = occupancyAddressObj.getStreet();
								String addressStreetName = "";

								if (addressStreet != null) {
									addressStreetName = addressStreet.getStreetName();

									if (addressStreetName == null) {
										addressStreetName = "";
									}

									logger.debug("setting addressStreetName to the request " + addressStreetName);
								}

								String addressCity = occupancyAddressObj.getCity();

								if (addressCity == null) {
									addressCity = "";
								}

								String addressState = occupancyAddressObj.getState();

								if (addressState == null) {
									addressState = "";
								}

								String addressZip = occupancyAddressObj.getZip();

								if (addressZip == null) {
									addressZip = "";
								}

								String addressZip4 = occupancyAddressObj.getZip4();

								if ((addressZip4 == null) || addressZip4.equals("0")) {
									addressZip4 = "";
								} else {
									addressZip4 = "-" + addressZip4;
								}

								address = addressStreetNumber + ", " + addressStreetModifier + " " + addressStreetName + ", " + addressCity + ", " + addressState + " " + addressZip + addressZip4 + "|" + lsoId + "|" + lsoType;
								logger.debug(address);
								addressResults.add(address);
							}
						}
					}
				}
			}
		}

		logger.info("Address List being returned of size: " + addressResults.size());

		return addressResults;
	}

	// end searchAddresses
	public List searchOwner(String apn) {
		List lsoIds = new ArrayList();
		List ownerResults = new ArrayList();
		int intStreetName = -1;

		try {
			Wrapper db = new Wrapper();
			String sql = " select lso_id  from lso_apn where apn in (" + apn + ") and lso_type = 'L'";
			logger.debug("The sql is " + sql);

			ResultSet rs = db.select(sql);

			while (rs.next()) {
				lsoIds.add(rs.getString("lso_id"));
				logger.debug("the lso id is  " + rs.getString("lso_id"));
			}

			rs.close();

			// lsoIds = new LsoTreeAgent().getCompleteLsoIdList(lsoIds);
			// logger.debug("lso id list received has size: " + lsoIds.size());
			String address = "";
			String parcelNumber = "";
			String apnOwnerName = "";
			Iterator iter = lsoIds.iterator();

			while (iter.hasNext()) {
				OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
				String lsoId = (String) iter.next();
				String lsoType = new AddressAgent().getLsoType(StringUtils.s2i(lsoId));
				logger.debug("lso id is " + lsoId + "and lso type is " + lsoType);

				if (lsoType.equalsIgnoreCase("L")) {
					Land land = (Land) new AddressAgent().getLso(StringUtils.s2i(lsoId));
					logger.debug("got object land");

					if (land != null) {
						List landApn = land.getLandApn();

						if (landApn != null) {
							logger.debug("apn list size " + landApn.size());

							if (landApn.size() > 0) {
								LandApn landApnObj = (LandApn) landApn.get(0);

								if (landApnObj != null) {
									parcelNumber = landApnObj.getApn();
									logger.debug("setting ParcelNumber to the request " + parcelNumber);

									Owner owner = landApnObj.getOwner();

									if (owner != null) {
										Address mailingAddress = owner.getLocalAddress();

										if (mailingAddress != null) {
											ownerApnAddress.setMailingAddress(owner.getLocalAddress());
										} else {
											ownerApnAddress.setMailingAddress(new Address());
										}

										String foreignAddressFlag = owner.getForeignFlag();

										if ((foreignAddressFlag != null) && foreignAddressFlag.equals("Y")) {
											ForeignAddress foreignMailingAddress = owner.getForeignAddress();

											if (foreignMailingAddress != null) {
												ownerApnAddress.setForeignMailingAddress(foreignMailingAddress);
											} else {
												ownerApnAddress.setForeignMailingAddress(new ForeignAddress());
											}
										} else {
											ownerApnAddress.setForeignMailingAddress(new ForeignAddress());
										}

										// viewLandForm.setApnOwnerId(StringUtils.i2s(owner.getOwnerId()));
										apnOwnerName = owner.getName();
										logger.debug("setting APN Owner Name to the request " + apnOwnerName);
									}
								}
							}
						}

						List landAddress = land.getLandAddress();

						if (landAddress != null) {
							if (landAddress.size() > 0) {
								LandAddress landAddressObj = (LandAddress) landAddress.get(0);

								if (landAddressObj != null) {
									String addressStreetNumber = StringUtils.i2s(landAddressObj.getStreetNbr());
									logger.debug("setting addressStreetNumber to the request " + addressStreetNumber);

									if (addressStreetNumber == null) {
										addressStreetNumber = "";
									}

									String addressStreetModifier = landAddressObj.getStreetModifier();
									logger.debug("setting addressStreetModifier to the request " + addressStreetModifier);

									if (addressStreetModifier == null) {
										addressStreetModifier = "";
									}

									Street addressStreet = landAddressObj.getStreet();
									String addressStreetName = "";

									if (addressStreet != null) {
										addressStreetName = addressStreet.getStreetName();
										logger.debug("setting addressStreetName to the request " + addressStreetName);

										if (addressStreetName == null) {
											addressStreetName = "";
										}
									}

									String addressCity = landAddressObj.getCity();

									if (addressCity == null) {
										addressCity = "";
									}

									String addressState = landAddressObj.getState();

									if (addressState == null) {
										addressState = "";
									}

									String addressZip = landAddressObj.getZip();

									if (addressZip == null) {
										addressZip = "";
									}

									String addressZip4 = landAddressObj.getZip4();

									if ((addressZip4 == null) || addressZip4.equals("0")) {
										addressZip4 = "";
									} else {
										addressZip4 = "-" + addressZip4;
									}

									address = addressStreetNumber + ", " + addressStreetModifier + " " + addressStreetName + ", " + addressCity + ", " + addressState + " " + addressZip + addressZip4;
									logger.debug(address);

									// OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
									ownerApnAddress.setSiteAddress(landAddressObj);

									ownerApnAddress.setApn(parcelNumber);
									logger.debug("setting parcelnumber to the owner object " + ownerApnAddress.getApn());
									ownerApnAddress.setOwnerName(apnOwnerName);
									ownerApnAddress.setAddress(address);
									ownerApnAddress.setLsoId(StringUtils.s2i(lsoId));
									ownerResults.add(ownerApnAddress);
								}
							}
						}
					}
				}
			}

			logger.info("Address List being returned of size: " + ownerResults.size());
		} catch (Exception e) {
			logger.error("Error in searchOwner with apns " + e.getMessage());
		}

		return ownerResults;
	}

	public List searchOwner(String streetNumber, String streetName, String apn, String ownerName) throws Exception {
		logger.info("searchOwner(" + streetNumber + ", " + streetName + ", " + apn + ", " + ownerName + ")");

		String owner = "'%" + ownerName + "%'";
		List ownerResults = new ArrayList();
		int intStreetName = -1;

		try {
			intStreetName = Integer.parseInt(streetName);
		} catch (NumberFormatException format) {
			// intStreetName remains -1
		}

		logger.debug("ownerName" + ownerName);

		List lsoIds = new ArrayList();

		lsoIds = getOwnerList(streetNumber, intStreetName, apn, ownerName);

		logger.debug("getOwnerList size: " + lsoIds.size());

		String lsoId = "(";
		Iterator iter = lsoIds.iterator();

		while (iter.hasNext()) {
			lsoId += (((Long) iter.next()).toString() + ",");
		}

		lsoId += "0)";

		logger.debug("String OwnerList String: " + lsoIds);

		String sql = "SELECT DISTINCT LA.LSO_ID,LA.LSO_TYPE,VAL.ADDRESS,VAL.DL_ADDRESS,LAPN.APN,O.NAME,SL.STR_NAME FROM ((((LSO_ADDRESS LA JOIN V_ADDRESS_LIST VAL ON LA.LSO_ID=VAL.LSO_ID) JOIN STREET_LIST SL ON SL.STREET_ID=LA.STREET_ID) LEFT OUTER JOIN LSO_APN LAPN ON LAPN.LSO_ID=LA.LSO_ID) LEFT OUTER JOIN APN_OWNER AO ON AO.APN = LAPN.APN)  LEFT OUTER JOIN OWNER O ON O.OWNER_ID = AO.OWNER_ID  WHERE LA.LSO_ID IN " + lsoId + " AND LA.PRIMARY='Y'";

		if (!owner.equals("'%%'")) {
			sql += (" AND UPPER(O.NAME) LIKE " + owner);
		}

		logger.debug("The sql is :" + sql);

		Wrapper db = new Wrapper();

		try {
			RowSet rs = db.select(sql);

			while (rs.next()) {
				OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
				ownerApnAddress.setApn(rs.getString("APN"));
				ownerApnAddress.setOwnerName(rs.getString("NAME"));
				ownerApnAddress.setAddress(rs.getString("DL_ADDRESS"));
				ownerApnAddress.setLsoId(rs.getInt("LSO_ID"));
				ownerResults.add(ownerApnAddress);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Exception("Exception occured while getting owner results " + e.getMessage());
		}

		logger.info("Exiting searchOwner (" + ownerResults.size() + ")");

		return ownerResults;
	}

	public List searchForeignAddress(String ownerName, String foreignAddressLine1, String foreignAddressLine2, String foreignAddressLine3, String foreignAddressLine4, String foreignAddressState, String foreignAddressCountry, String foreignAddressZip) throws Exception {
		logger.info("searchForeignAddress(" + ownerName + ", " + foreignAddressState + ", " + foreignAddressCountry + ", " + foreignAddressZip + ", " + foreignAddressLine1 + ", " + foreignAddressLine2 + ", " + foreignAddressLine3 + ", " + foreignAddressLine4 + ")");

		List foreignAddressResults = new ArrayList();
		String foreignAddressSQL = "";

		String owner = "'%" + ownerName + "%'";
		String country = "'%" + foreignAddressCountry + "%'";
		String state = "'%" + foreignAddressState + "%'";
		String zip = "'%" + foreignAddressZip + "%'";
		String line1 = "'%" + foreignAddressLine1 + "%'";
		String line2 = "'%" + foreignAddressLine2 + "%'";
		String line3 = "'%" + foreignAddressLine3 + "%'";
		String line4 = "'%" + foreignAddressLine4 + "%'";

		try {
			foreignAddressSQL = getForeignAddressSQL(ownerName, foreignAddressState, foreignAddressCountry, foreignAddressZip, foreignAddressLine1, foreignAddressLine2, foreignAddressLine3, foreignAddressLine4);
		} catch (Exception e1) {
			logger.error("Exception occured while getting searching ForeignAddress " + e1.getMessage());
			throw new Exception("Exception while searching ForeignAddress " + e1.getMessage());
		}

		String sql = "SELECT DISTINCT LA.LSO_ID,LA.LSO_TYPE,VAL.ADDRESS,LAPN.APN,O.NAME,SL.STR_NAME,O.Country,O.State,O.zip,O.address_line1,O.address_line2,O.address_line3,O.address_line4 FROM ((((LSO_ADDRESS LA JOIN V_ADDRESS_LIST VAL ON LA.LSO_ID=VAL.LSO_ID) JOIN STREET_LIST SL ON SL.STREET_ID=LA.STREET_ID) LEFT OUTER JOIN LSO_APN LAPN ON LAPN.LSO_ID=LA.LSO_ID) LEFT OUTER JOIN APN_OWNER AO ON AO.APN = LAPN.APN) LEFT OUTER JOIN OWNER O ON O.OWNER_ID = AO.OWNER_ID WHERE LA.LSO_ID IN (" + foreignAddressSQL + ") AND LA.PRIMARY='Y'";

		if (!owner.equals("'%%'")) {
			sql += (" AND UPPER(O.NAME) LIKE " + owner);
		}

		if (!country.equals("'%%'")) {
			sql += (" AND UPPER(O.COUNTRY) LIKE " + country);
		}

		if (!state.equals("'%%'")) {
			sql += (" AND UPPER(O.STATE) LIKE " + state);
		}

		if (!zip.equals("'%%'")) {
			sql += (" AND UPPER(O.ZIP) LIKE " + zip);
		}

		if (!line1.equals("'%%'")) {
			sql += (" AND UPPER(O.ADDRESS_LINE1) LIKE " + line1);
		}

		if (!line2.equals("'%%'")) {
			sql += (" AND UPPER(O.ADDRESS_LINE2) LIKE " + line2);
		}

		if (!line3.equals("'%%'")) {
			sql += (" AND UPPER(O.ADDRESS_LINE3) LIKE " + line3);
		}

		if (!line4.equals("'%%'")) {
			sql += (" AND UPPER(O.ADDRESS_LINE4) LIKE " + line4);
		}

		sql += " ORDER BY SL.STR_NAME";

		logger.debug("The sql is :" + sql);

		// logger.debug("The foreignAddressCountry is :" + foreignAddress);
		Wrapper db = new Wrapper();

		try {
			RowSet rs = db.select(sql);

			while (rs.next()) {
				OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
				ownerApnAddress.setApn(rs.getString("APN"));
				ownerApnAddress.setOwnerName(rs.getString("NAME"));
				ownerApnAddress.setAddress(rs.getString("ADDRESS"));
				ownerApnAddress.setLsoId(rs.getInt("LSO_ID"));

				String s1 = "";

				if ((rs.getString("ADDRESS_LINE1")) != null) {
					s1 = s1 + rs.getString("ADDRESS_LINE1") + ", ";
				}

				if ((rs.getString("ADDRESS_LINE2")) != null) {
					s1 = s1 + rs.getString("ADDRESS_LINE2") + ", ";
				}

				if ((rs.getString("ADDRESS_LINE3")) != null) {
					s1 = s1 + rs.getString("ADDRESS_LINE3") + ", ";
				}

				if ((rs.getString("ADDRESS_LINE4")) != null) {
					s1 = s1 + rs.getString("ADDRESS_LINE4") + ", ";
				}

				if ((rs.getString("STATE")) != null) {
					s1 = s1 + rs.getString("STATE") + ", ";
				}

				if ((rs.getString("COUNTRY")) != null) {
					s1 = s1 + rs.getString("COUNTRY") + ", ";
				}

				if ((rs.getString("ZIP")) != null) {
					s1 = s1 + rs.getString("ZIP") + " ";
				}

				// ownerApnAddress.setForeignAdd(""+rs.getString("ADDRESS_LINE1")+ ", " + rs.getString("ADDRESS_LINE2") + ", " + rs.getString("ADDRESS_LINE3")+ ", " + rs.getString("ADDRESS_LINE4")+ ", " + rs.getString("STATE") + ", " + rs.getString("COUNTRY") + ", " + rs.getString("ZIP")+ "");
				ownerApnAddress.setForeignAdd(s1);

				foreignAddressResults.add(ownerApnAddress);
			}

			logger.info("Exiting searchOwner (" + foreignAddressResults.size() + ")");

			// ("fadd",foreignAddress);
			return foreignAddressResults;
		} catch (Exception e) {
			logger.error("searchForeignAddress" + e.getMessage());
			throw new Exception("searchForeignAddress" + e.getMessage());
		}
	}

	/**
	 * Retrieve Address Summary Information for the specified landId
	 */
	public AddressSummary getAddressSummary(String lsoId) {
		int i = new Integer(lsoId).intValue();

		return getAddressSummary(i);
	}

	/**
	 * This method finds the landId corresponding to the passed lsoId which may be a land,structure or occupancy
	 */
	public int lsoIdtoLandId(Wrapper db, int lsoId) {
		String sql = "select * from v_lso_land where lso_id=" + lsoId;
		int landId = 0;

		try {
			RowSet rs = db.select(sql);

			if (rs.next()) {
				landId = rs.getInt("LAND_ID");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return landId;
	}

	public List getAddressRangeList(int landId) {
		Wrapper db = new Wrapper();

		return getAddressRangeList(db, landId);
	}

	/**
	 * Gets the List of Address ranges for a corresponding piece of Land
	 * 
	 * @param db
	 * @param landId
	 * @return List of Address ranges
	 */
	public List getAddressRangeList(Wrapper db, int landId) {
		List addressRanges = new ArrayList();

		try {
			// String sql = "select (rtrim(char(lar.start_range)) ||' - '||rtrim(char(lar.end_range)) || '  ' || vsl.street_name)" + " from lso_addr_range lar join v_street_list vsl on  lar.street_id = vsl.street_id where lso_id=" + landId;
			String sql = "select  concat( concat(concat(concat(lar.start_range,' - '), lar.end_range),' '), vsl.street_name) from lso_addr_range lar join v_street_list vsl on  lar.street_id = vsl.street_id where lso_id=" + landId;
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				String addressRange = rs.getString(1);

				if (addressRange != null) {
					addressRanges.add(addressRange);
				}
			}

			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return addressRanges;
	}

	public AddressSummary getAddressSummary(int lsoId) {
		logger.debug("Entered into getAddressSummary(" + lsoId + ")");

		if (lsoId <= 0) {
			return new AddressSummary();
		}

		AddressSummary addressSummary = new AddressSummary();

		// Setting lsoId for address Summary
		try {
			Wrapper db = new Wrapper();
			int landId = lsoIdtoLandId(db, lsoId);

			addressSummary.setLsoId(landId);

			// Setting Description
			String sql = "select lso_id,rtrim(cast(l.str_no as char(8))) || coalesce (' ' || l.str_mod,'') as str_no," + " l.unit, l.description,v.street_name as street_name" + " from lso_address l,v_street_list v" + " where v.street_id=l.street_id and l.lso_id=" + landId + " and l.primary='Y'";
			logger.debug(sql);

			ResultSet rs = db.select(sql);

			if (rs.next()) {
				String description = rs.getString("STR_NO") + " " + rs.getString("STREET_NAME");

				if (description == null) {
					description = "";
				}

				addressSummary.setDescription(description);
				logger.debug("addressSummary - description - " + addressSummary.getDescription());
			}

			rs.close();

			// setting the Apn # and owner name
			sql = "select distinct o.name as ownername ,la.apn as apn from (lso_apn la left outer join apn_owner ao on la.apn = ao.apn )" + " left outer join owner o on ao.owner_id=o.owner_id where la.lso_id=" + landId;
			logger.debug(sql);
			rs = db.select(sql);

			List owners = new ArrayList();
			List apns = new ArrayList();

			while (rs.next()) {
				String ownername = rs.getString("ownername");
				String apn = rs.getString("apn");

				if ((ownername != null) && !ownername.equals("")) {
					owners.add(ownername);
				}

				if ((apn != null) && !apn.equals("")) {
					apns.add(apn);
				}
			}

			rs.close();
			addressSummary.setOwnerList(owners);
			addressSummary.setApnList(apns);
			logger.debug("addressSummary - owner List size - " + addressSummary.getOwnerList().size());
			logger.debug("addressSummary - apn List size - " + addressSummary.getApnList().size());

			// setting the List of Zone Description
			sql = "select z.zone from land_zone lz join zone z on lz.zone_id = z.zone_id where lz.land_id=" + landId;
			logger.debug(sql);
			rs = db.select(sql);

			List zones = new ArrayList();

			while (rs.next()) {
				String zone = rs.getString(1);
				logger.debug("The Zone String is " + zone);

				if (zone != null) {
					zones.add(zone);
				}
			}

			rs.close();
			addressSummary.setZoneList(zones);
			logger.debug("addressSummary - Zones List size - " + addressSummary.getZoneList().size());

			// setting the List of Use Description
			sql = "select lsu.description from land_usage lau join lso_use lsu on lau.lso_use_id = lsu.lso_use_id where lsu.lso_type='L' and lau.land_id=" + landId;
			rs = db.select(sql);

			List uses = new ArrayList();

			while (rs.next()) {
				String use = rs.getString(1);

				if (use != null) {
					uses.add(use);
				}
			}

			rs.close();
			addressSummary.setUseList(uses);
			logger.debug("addressSummary - Zones List size - " + addressSummary.getUseList().size());
			logger.debug(sql);

			// setting the List of AddressRange Description
			addressSummary.setAddressRangeList(getAddressRangeList(db, landId));

			// setting the List of Project names
			sql = "select name from project where status_id not in (2,3,4,7,8,9) and lso_id=" + landId;
			rs = db.select(sql);

			List projects = new ArrayList();

			while (rs.next()) {
				String project = rs.getString(1);

				if (project != null) {
					projects.add(project);
				}
			}

			rs.close();
			addressSummary.setProjectList(projects);
			logger.debug("addressSummary - Projects List size - " + addressSummary.getProjectList().size());
			logger.debug(sql);

			// setting the Counts of holds
			addressSummary.setHoldList(new ArrayList());
			sql = "select count(*) as count,hold_type from holds where level_id=" + landId + " and hold_level = 'L' group by hold_type";
			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				String holdType = rs.getString("hold_type");
				int count = rs.getInt("count");

				if (holdType.equals("H")) {
					addressSummary.setHardHoldCount(count);
				} else if (holdType.equals("S")) {
					addressSummary.setSoftHoldCount(count);
				} else if (holdType.equals("W")) {
					addressSummary.setWarnHoldCount(count);
				}
			}

			rs.close();
			logger.debug("addressSummary - hard hold count - " + addressSummary.getHardHoldCount());
			logger.debug("addressSummary - Soft hold count - " + addressSummary.getSoftHoldCount());
			logger.debug("addressSummary - Warn hold count - " + addressSummary.getWarnHoldCount());

			// setting the Condition Count
			sql = "select count(*) as count from conditions where level_id=" + landId + " and cond_level = 'L'";
			logger.debug(sql);
			rs = db.select(sql);

			if (rs.next()) {
				int count = rs.getInt("count");
				addressSummary.setConditionCount(count);
				logger.debug("addressSummary - Condition count - " + addressSummary.getConditionCount());
			}

			rs.close();

			// getting structure list
			addressSummary.setStructureList(getStructures(landId));

			// logger.debug("addressSummary - structure List -   - " + addressSummary.getStructureList().size());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return addressSummary;
	}

	public void setAddressSummaryMatch(AddressSummary addressSummary, String lsoId) {
		StructureEdit[] structures = addressSummary.getStructureList();

		for (int i = 0; i < structures.length; i++) {
			if (structures[i].getStructureId().equals(lsoId)) {
				structures[i].setMatch("Y");
				addressSummary.setStructureList(structures);

				return;
			}

			OccupancyEdit[] occupancies = structures[i].getOccupancyList();

			for (int j = 0; j < occupancies.length; j++) {
				if (occupancies[j].getOccupancyId().equals(lsoId)) {
					occupancies[j].setMatch("Y");
					structures[i].setOccupancyList(occupancies);
					addressSummary.setStructureList(structures);

					return;
				}
			}
		}
	}

	// get structures
	public List getStructures(int landId) {
		List structures = new ArrayList();

		try {
			Wrapper db = new Wrapper();

			// setting the Lsit of Structures

			/*
			 * String sql = "select l.addr_id,l.lso_id as structureId,l.lso_type,l.str_no,rtrim(cast(l.str_no as char(8))) || coalesce (' ' || l.str_mod,'') as str_no_mod," + " l.unit, l.description,v.street_id,v.street_name as street_name" + " from lso_address l,v_street_list v where v.street_id=l.street_id and" + " l.lso_id in (select structure_id from land_structure where land_id=" + landId +")";
			 */
			String sql = "select ls.structure_id as structureId,v.str_no,v.str_mod,v.street_id,v.address,v.lso_type,v.address,s.description,s.created_by,s.created,s.updated_by,s.updated" + " from structure s left outer join v_address_list v on v.lso_id=s.structure_id," + "land_structure ls where s.structure_id  = ls.structure_id and s.active = 'Y'  and land_id =" + landId + " order by v.address";
			RowSet rs = db.select(sql);

			while (rs.next()) {
				StructureEdit structure = new StructureEdit();

				// setting the structureId
				String structureId = rs.getString("structureId");

				structure.setStructureId(structureId);
				logger.debug("addressSummary - StructureEdit - structure Id - " + structure.getStructureId());

				// Setting the description
				// /String description = rs.getString("str_no_mod") + " " + rs.getString("street_name");
				// /if ( description == null) description = "" ;
				String description = rs.getString("address");

				if (description == null) {
					structure.setDescription(rs.getString("description"));
					structure.setIsLot("Y");
				} else {
					structure.setDescription(description);
					structure.setIsLot("N");
				}

				logger.debug("addressSummary - StructureEdit - Description - " + structure.getDescription());
				structure.setStreetNumber(rs.getString("str_no"));
				logger.debug("addressSummary - StructureEdit - Street Number " + structure.getStreetNumber());
				structure.setStreetModifier(rs.getString("str_mod"));
				logger.debug("addressSummary - StructureEdit - Street Modifier " + structure.getStreetModifier());
				structure.setStreetId(rs.getString("street_id"));
				logger.debug("addressSummary - StructureEdit - Street Number " + structure.getStreetId());
				structure.setStreetName(rs.getString("address"));
				logger.debug("addressSummary - StructureEdit - Street Name " + structure.getStreetName());
				structure.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				structure.setCreated(rs.getDate("created"));
				structure.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				structure.setUpdated(rs.getDate("updated"));

				// setting Structure project count
				sql = "select count(*) as count from project where lso_id =" + structureId;
				logger.debug(sql);

				RowSet countRs = db.select(sql);

				if (countRs.next()) {
					int count = countRs.getInt("count");
					structure.setProjectCount(count);
					logger.debug("addressSummary - StructureEdit - project Count  - " + structure.getProjectCount());
				}

				countRs.close();
				logger.debug(sql);

				// setting holds Exists String
				sql = "select count(*) as count from holds where level_id =" + structureId + " and hold_level='S'";
				logger.debug(sql);
				countRs = db.select(sql);

				if (countRs.next()) {
					int count = countRs.getInt("count");

					if (count == 0) {
						structure.setHoldsExists("");
					} else {
						structure.setHoldsExists("(H)");
					}

					logger.debug("addressSummary - StructureEdit - Holds Exists String  - " + structure.getHoldsExists());
				}

				countRs.close();

				// setting OccupancyList
				structure.setOccupancyList(getOccupancies(StringUtils.s2i(structureId)));

				// logger.debug("addressSummary - StructureEdit - occupancy List -   - " + StructureEdit.getOccupancyList().size());
				structures.add(structure);
			}

			rs.close();
		} catch (Exception e) {
		}

		return structures;
	}

	// get structures
	public StructureEdit getStructureEdit(int structureId) {
		StructureEdit structure = new StructureEdit();

		// setting the structureId
		structure.setStructureId(StringUtils.i2s(structureId));

		try {
			Wrapper db = new Wrapper();

			// setting the Lsit of Structures
			String sql = "select s.structure_id,v.str_no,v.str_mod,v.street_id,v.address,v.lso_type,v.address,s.description,s.created_by,s.created,s.updated_by,s.updated" + " from structure s left outer join v_address_list v on v.lso_id=s.structure_id" + " where s.structure_id  =" + structureId + " order by v.address";

			RowSet rs = db.select(sql);

			if (rs.next()) {
				logger.debug("addressSummary - StructureEdit - structure Id - " + structure.getStructureId());

				// Setting the description
				String description = rs.getString("address");

				if (description == null) {
					description = "";
				}

				structure.setDescription(description);
				logger.debug("addressSummary - StructureEdit - Description - " + structure.getDescription());
				structure.setStreetNumber(rs.getString("str_no"));
				structure.setStreetModifier(rs.getString("str_mod"));
				logger.debug("addressSummary - StructureEdit - Street Number " + structure.getStreetNumber());
				structure.setStreetId(rs.getString("street_id"));
				logger.debug("addressSummary - StructureEdit - Street Number " + structure.getStreetId());
				structure.setStreetName(rs.getString("address"));
				logger.debug("addressSummary - StructureEdit - Street Name " + structure.getStreetName());

				// setting Structure project count
				sql = "select count(*) as count from project where lso_id =" + structureId;
				logger.debug(sql);

				RowSet countRs = db.select(sql);

				if (countRs.next()) {
					int count = countRs.getInt("count");
					structure.setProjectCount(count);
					logger.debug("addressSummary - StructureEdit - project Count  - " + structure.getProjectCount());
				}

				countRs.close();
				logger.debug(sql);

				// setting hilds Exists String
				sql = "select count(*) as count from holds where level_id =" + structureId + " and hold_level='S'";
				logger.debug(sql);
				countRs = db.select(sql);

				if (countRs.next()) {
					int count = countRs.getInt("count");

					if (count == 0) {
						structure.setHoldsExists("");
					} else {
						structure.setHoldsExists("(H)");
					}

					logger.debug("addressSummary - StructureEdit - Holds Exists String  - " + structure.getHoldsExists());
				}

				countRs.close();

				// setting OccupancyList
				structure.setOccupancyList(getOccupancies(structureId));

				// logger.debug("addressSummary - StructureEdit - occupancy List -   - " + StructureEdit.getOccupancyList().size());
			}

			rs.close();
		} catch (Exception e) {
		}

		return structure;
	}

	// Add structures
	public StructureEdit addStructure(StructureEdit structure, int landId) {
		String sql = "";

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			// adding to Structure table
			int structureId = db.getNextId("LSO_ID");
			sql = "insert into structure(structure_id,active,created_by,created,updated_by,updated) values(" + structureId + "," + "'Y'" + "," + structure.getCreatedBy().getUserId() + "," + "current_timestamp" + "," + structure.getUpdatedBy().getUserId() + "," + "current_timestamp" + ")";
			logger.debug(sql);
			db.addBatch(sql);

			// adding to LSO ADDRESS TABLE
			int addressId = db.getNextId("ADDRESS_ID");
			sql = "insert into lso_address(addr_id,lso_id,lso_type,str_no,str_mod,street_id,primary,active," + "last_updated,updated_by,addr_primary) values(" + addressId + "," + structureId + "," + "'S'" + "," + structure.getStreetNumber() + "," + StringUtils.checkString(structure.getStreetModifier()) + "," + structure.getStreetId() + "," + "'Y'" + "," + "'Y'" + "," + "current_timestamp" + "," + structure.getUpdatedBy().getUserId() + "," + addressId + ")";
			logger.debug(sql);
			db.addBatch(sql);

			// adding to Land Structure TABLE
			sql = "insert into land_structure(land_id,structure_id) values(" + landId + "," + structureId + ")";
			logger.debug(sql);
			db.addBatch(sql);

			// processing Occupancies
			List occupancies = StringUtils.arrayToList(structure.getOccupancyList());
			Iterator iter = occupancies.iterator();

			while (iter.hasNext()) {
				OccupancyEdit occupancy = (OccupancyEdit) iter.next();

				if ((occupancy.getDelete() == null) || occupancy.getDelete().equals("")) {
					if ((occupancy.getOccupancyId() == null) || occupancy.getOccupancyId().equals("")) {
						// add occupancy to database
						List sqls = addOccupancySqls(occupancy, structureId);
						Iterator sqlIter = sqls.iterator();

						while (sqlIter.hasNext()) {
							sql = (String) sqlIter.next();
							db.addBatch(sql);
						}
					}
				} else {
					if ((occupancy.getOccupancyId() != null) && !occupancy.getOccupancyId().equals("")) {
						// delete occupancy from database
						List sqls = deleteOccupancySqls(occupancy);
						Iterator sqlIter = sqls.iterator();

						while (sqlIter.hasNext()) {
							sql = (String) sqlIter.next();
							db.addBatch(sql);
						}
					}
				}
			}

			db.executeBatch();
			structure = getStructureEdit(structureId);
		} catch (Exception e) {
			logger.error("Error while adding Structure " + e.getMessage());
		}

		return structure;
	}

	// Add structures
	public StructureEdit updateStructure(StructureEdit structure) {
		logger.debug("Entering updateStructure()");

		String sql = "";

		try {
			Wrapper db = new Wrapper();
			int structureId = StringUtils.s2i(structure.getStructureId());

			db.beginTransaction();

			// Updating to LSO ADDRESS TABLE
			sql = "update lso_address set str_no=" + structure.getStreetNumber() + "," + "str_mod=" + StringUtils.checkString(structure.getStreetModifier()) + "," + "street_id=" + structure.getStreetId() + "," + "last_updated=current_timestamp," + "updated_by=" + structure.getUpdatedBy().getUserId() + " where lso_id=" + structureId + " and primary='Y'";
			logger.debug(sql);
			db.addBatch(sql);

			// update structure table
			sql = "update structure set updated_by=" + structure.getUpdatedBy().getUserId() + "," + "updated=current_timestamp where structure_id=" + structureId;
			logger.debug(sql);
			db.addBatch(sql);

			// processing Occupancies
			List occupancies = StringUtils.arrayToList(structure.getOccupancyList());
			Iterator iter = occupancies.iterator();

			while (iter.hasNext()) {
				OccupancyEdit occupancy = (OccupancyEdit) iter.next();

				if ((occupancy.getDelete() == null) || occupancy.getDelete().equals("")) {
					if ((occupancy.getOccupancyId() == null) || occupancy.getOccupancyId().equals("")) {
						// add occupancy to database
						List sqls = addOccupancySqls(occupancy, structureId);
						Iterator sqlIter = sqls.iterator();

						while (sqlIter.hasNext()) {
							sql = (String) sqlIter.next();
							db.addBatch(sql);
						}
					} else {
						List sqls = updateOccupancySqls(occupancy);
						Iterator sqlIter = sqls.iterator();

						while (sqlIter.hasNext()) {
							sql = (String) sqlIter.next();
							db.addBatch(sql);
						}
					}
				} else {
					if ((occupancy.getOccupancyId() != null) && !occupancy.getOccupancyId().equals("")) {
						// delete occupancy from database
						List sqls = deleteOccupancySqls(occupancy);
						Iterator sqlIter = sqls.iterator();

						while (sqlIter.hasNext()) {
							sql = (String) sqlIter.next();
							db.addBatch(sql);
						}
					}
				}
			}

			db.executeBatch();
			structure = getStructureEdit(structureId);
		} catch (Exception e) {
			logger.error("Error while adding Structure " + e.getMessage());
		}

		logger.debug("Exiting updateStructure()");

		return structure;
	}

	public List getOccupancies(int structureId) {
		List occupancies = new ArrayList();

		try {
			Wrapper db = new Wrapper();

			// setting Structure's Occupancy List
			String sql = "select so.occupancy_id as occupancyId,v.str_no,v.str_mod,v.street_id,v.address,o.description,o.unit,o.created_by,o.created,o.updated_by,o.updated" + " from occupancy o left outer join v_address_list v on v.lso_id=o.occupancy_id," + "structure_occupant so where o.occupancy_id = so.occupancy_id" + " and structure_id=" + structureId + " order by o.active desc,v.street_id,v.str_no,v.str_mod,o.unit";
			logger.debug(sql);

			RowSet occupancyRs = db.select(sql);

			while (occupancyRs.next()) {
				OccupancyEdit occupancyEdit = new OccupancyEdit();

				// setting Occupancy Id
				String occupancyId = occupancyRs.getString("occupancyId");
				occupancyEdit.setOccupancyId(occupancyId);
				logger.debug("addressSummary - StructureEdit - occupancyEdit - occupancy Id  - " + occupancyEdit.getOccupancyId());

				String description = occupancyRs.getString("address");

				if (description == null) {
					occupancyEdit.setDescription(occupancyRs.getString("description"));
					occupancyEdit.setIsLot("Y");
				} else {
					occupancyEdit.setDescription(description);
					occupancyEdit.setIsLot("N");
				}

				logger.debug("addressSummary - StructureEdit - occupancyEdit - description  - " + occupancyEdit.getDescription());

				// str_no and str_id
				occupancyEdit.setStreetNumber(occupancyRs.getString("str_no"));
				occupancyEdit.setStreetId(occupancyRs.getString("street_id"));
				occupancyEdit.setStreetModifier(occupancyRs.getString("str_mod"));

				// setting updated and updated by
				occupancyEdit.setCreatedBy(new AdminAgent().getUser(occupancyRs.getInt("created_by")));
				occupancyEdit.setCreated(occupancyRs.getDate("created"));

				occupancyEdit.setUpdatedBy(new AdminAgent().getUser(occupancyRs.getInt("updated_by")));
				occupancyEdit.setUpdated(occupancyRs.getDate("updated"));

				// setting APN, unit and Active on Occupancy
				sql = "select o.unit,o.active,la.apn as apn from (occupancy o left outer join lso_apn la on o.occupancy_id = la.lso_id)" + " where o.occupancy_id=" + occupancyId;
				logger.debug(sql);

				RowSet occRs = db.select(sql);

				if (occRs.next()) {
					if (!(occRs.getString("unit") == null)) {
						occupancyEdit.setUnitNumber(occRs.getString("unit"));
					}

					if (occRs.getString("active").equals("Y")) {
						occupancyEdit.setActive("on");
					}

					if (!(occRs.getString("apn") == null)) {
						occupancyEdit.setApnNumber(occRs.getString("apn"));
						occupancyEdit.setOldApnNumber(occRs.getString("apn"));
					}
				}

				occRs.close();

				// setting Occupancy project count
				sql = "select count(*) as count from project where lso_id =" + occupancyId;
				logger.debug(sql);

				RowSet countRs = db.select(sql);

				if (countRs.next()) {
					int count = countRs.getInt("count");
					occupancyEdit.setProjectCount(count);
				}

				countRs.close();
				logger.debug("addressSummary - StructureEdit - occupancyEdit - project Count  - " + occupancyEdit.getProjectCount());

				// setting Holds Exists String
				sql = "select count(*) as count from holds where level_id =" + occupancyId + " and hold_level='O'";
				logger.debug(sql);
				countRs = db.select(sql);

				if (countRs.next()) {
					int count = countRs.getInt("count");

					if (count == 0) {
						occupancyEdit.setHoldsExists("");
					} else {
						occupancyEdit.setHoldsExists("(H)");
					}

					logger.debug("addressSummary - StructureEdit - occupancyEdit - Holds Exists String  - " + occupancyEdit.getHoldsExists());
				}

				countRs.close();
				occupancies.add(occupancyEdit);
			}

			occupancyRs.close();
		} catch (Exception e) {
			logger.error("Error found in getting Occupancies");
		}

		return occupancies;
	}

	// adding Occupancy to database
	public List addOccupancySqls(OccupancyEdit occupancy, int structureId) {
		logger.debug("Entering addOccupancySqls()" + occupancy.getOccupancyId());

		List sqls = new ArrayList();
		int occupancyId = -1;

		try {
			Wrapper db = new Wrapper();
			occupancyId = db.getNextId("LSO_ID");

			if (occupancy.getActive() == null) {
				occupancy.setActive("N");
			} else {
				occupancy.setActive("Y");
			}

			// inserting into occupancy table
			String sql = "insert into occupancy(occupancy_id,unit,active,condo,created_by,created,updated_by,updated) values(" + occupancyId + "," + StringUtils.checkString(occupancy.getUnitNumber().trim()) + "," + StringUtils.checkString(occupancy.getActive().trim()) + "," + "'N'" + "," + occupancy.getCreatedBy().getUserId() + "," + "current_timestamp" + "," + occupancy.getUpdatedBy().getUserId() + "," + "current_ timestamp" + ")";
			logger.debug(sql);
			sqls.add(sql);

			// inserting into lso_address table
			int addressId = db.getNextId("ADDRESS_ID");
			sql = "insert into lso_address(addr_id,lso_id,lso_type,str_no,str_mod,street_id,unit,active,primary,addr_primary,last_updated,updated_by) values(" + addressId + "," + occupancyId + "," + "'O'" + "," + occupancy.getStreetNumber() + "," + StringUtils.checkString(occupancy.getStreetModifier()) + "," + occupancy.getStreetId() + "," + StringUtils.checkString(occupancy.getUnitNumber().trim()) + "," + StringUtils.checkString(occupancy.getActive().trim()) + "," + "'Y'" + "," + addressId + "," + "current_timestamp" + "," + occupancy.getUpdatedBy().getUserId() + ")";
			logger.debug(sql);
			sqls.add(sql);

			// insert in to structure_occupancy table;
			sql = "insert into structure_occupant(structure_id,occupancy_id) values(" + structureId + "," + occupancyId + ")";
			logger.debug("Insert into structure_occupancy sql " + sql);
			sqls.add(sql);

			// insert in to lso apn table;
			if (!occupancy.getApnNumber().trim().equals("")) {
				sql = "insert into lso_apn(lso_id,apn,lso_type) values(" + occupancyId + "," + StringUtils.checkString(occupancy.getApnNumber().trim()) + "," + "'O'" + ")";
				logger.debug("Insert into LSO APN sql " + sql);
				sqls.add(sql);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("Exiting addOccupancySqls()");

		return sqls;
	}

	// end method add Occupancy
	public List updateOccupancySqls(OccupancyEdit occupancy) {
		logger.debug("Entering updateOccupancySqls()" + occupancy.getOccupancyId());

		List sqls = new ArrayList();
		int occupancyId = -1;

		try {
			Wrapper db = new Wrapper();

			// setting active to Y or N
			if (occupancy.getActive() == null) {
				occupancy.setActive("N");
			} else {
				occupancy.setActive("Y");
			}

			// updating occupancy table
			String sql = "update occupancy set unit=" + StringUtils.checkString(occupancy.getUnitNumber().trim()) + "," + "active=" + StringUtils.checkString(occupancy.getActive().trim()) + "," + "updated_by=" + occupancy.getUpdatedBy().getUserId() + "," + "updated=current_timestamp" + " where occupancy_id=" + occupancy.getOccupancyId();
			logger.debug("SQL:" + sql);
			sqls.add(sql);

			// updating lso_address table
			sql = "update lso_address set str_no=" + occupancy.getStreetNumber() + "," + "street_id=" + occupancy.getStreetId() + "," + "str_mod=" + StringUtils.checkString(occupancy.getStreetModifier().trim()) + "," + "unit=" + StringUtils.checkString(occupancy.getUnitNumber().trim()) + "," + "active=" + StringUtils.checkString(occupancy.getActive().trim()) + "," + "last_updated=current_timestamp," + "updated_by=" + occupancy.getUpdatedBy().getUserId() + " where lso_id=" + occupancy.getOccupancyId() + " and primary='Y'";
			logger.debug("SQL: " + sql);
			sqls.add(sql);

			// update LSO APN Table.
			logger.debug("APN# " + occupancy.getApnNumber() + ": Old APN#:" + occupancy.getOldApnNumber());

			if (!occupancy.getApnNumber().trim().equals(occupancy.getOldApnNumber().trim())) {
				if (occupancy.getOldApnNumber().trim().equals("")) {
					sql = "insert into lso_apn(lso_id,apn,lso_type) values(" + occupancy.getOccupancyId() + "," + StringUtils.checkString(occupancy.getApnNumber().trim()) + "," + "'O'" + ")";
				} else if (!occupancy.getApnNumber().trim().equals("")) {
					sql = "update lso_apn set apn=" + StringUtils.checkString(occupancy.getApnNumber().trim()) + " where lso_id=" + occupancy.getOccupancyId() + " and apn=" + StringUtils.checkString(occupancy.getOldApnNumber().trim()) + " and lso_type='O'";
				} else {
					sql = "delete from lso_apn " + " where lso_id=" + occupancy.getOccupancyId() + " and apn=" + StringUtils.checkString(occupancy.getOldApnNumber().trim()) + " and lso_type='O'";
				}

				logger.debug(sql);
				sqls.add(sql);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("Exiting updateOccupancySqls()");

		return sqls;
	}

	// end method update Occupancy
	public List deleteOccupancySqls(OccupancyEdit occupancy) {
		List sqls = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			// delete from structure_occupant table
			String sql = "delete from structure_occupant where occupancy_id=" + occupancy.getOccupancyId();
			logger.debug(sql);
			sqls.add(sql);

			// delete lso_address table
			sql = "delete from lso_address where lso_id=" + occupancy.getOccupancyId() + " and primary='Y'";
			logger.debug("delete lso address sql " + sql);
			sqls.add(sql);

			// delete lso_apn table;
			sql = "delete from lso_apn where lso_id=" + occupancy.getOccupancyId() + " and apn=" + StringUtils.checkString(occupancy.getOldApnNumber()) + " and lso_type='O'";
			logger.debug("delete lso apn sql " + sql);
			sqls.add(sql);

			// delete from occupancy table
			sql = "delete from occupancy where occupancy_id=" + occupancy.getOccupancyId();
			logger.debug(sql);
			sqls.add(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return sqls;
	}

	// end method updateOccupancySqls
	// Getting the list of lots
	public LotEdit getLot(String lotId) {
		LotEdit lot = new LotEdit();
		lot.setLotId(lotId);

		try {
			Wrapper db = new Wrapper();
			String sql = "select s.description,s.lot_type,s.active,s.created_by,s.created,s.updated_by,s.updated,ls.land_id" + " from structure s,land_structure ls" + " where s.structure_id=ls.structure_id and s.structure_id=" + lotId;
			RowSet rs = db.select(sql);

			if (rs.next()) {
				lot.setParentId(rs.getString("land_id"));
				lot.setType(rs.getString("lot_type"));
				lot.setDescription(rs.getString("description"));
				lot.setActive(rs.getString("active"));
				lot.setDelete("");
				lot.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				lot.setCreated(rs.getDate("created"));
				lot.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				lot.setCreated(rs.getDate("updated"));
			} else {
				rs.close();
				sql = "select o.description,o.lot_type,0.active,o.created_by,o.created,o.updated_by,o.updated,so.structure_id" + " from occupancy o,structure_occupant so" + " where o.occupancy_id=so.occupancy_id and o.occupancy_id=" + lotId;
				rs = db.select(sql);

				if (rs.next()) {
					lot.setParentId(rs.getString("structure_id"));
					lot.setType(rs.getString("lot_type"));
					lot.setDescription(rs.getString("description"));
					lot.setActive(rs.getString("active"));
					lot.setDelete("");
					lot.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
					lot.setCreated(rs.getDate("created"));
					lot.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
					lot.setCreated(rs.getDate("updated"));
				}

				rs.close();
			}

			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lot;
	}

	// end method getLots
	// Getting the list of lots
	public List getLots(String lsoId) {
		String sql = "";
		List lots = new ArrayList();
		String lsoType = new AddressAgent().getLsoType(StringUtils.s2i(lsoId));

		if (lsoType == null) {
			lsoType = "";
		}

		if (lsoType.equals("L")) {
			sql = "select s.structure_id as lot_id,s.description,s.lot_type,s.active,s.created_by,s.created,s.updated_by,s.updated,ls.land_id" + " from structure s,land_structure ls" + " where s.structure_id=ls.structure_id and s.lot_type > 0 and s.structure_id in" + " (select structure_id from land_structure where land_id=" + lsoId + ")";
		} else if (lsoType.equals("S")) {
			sql = "select o.occupancy_id as lot_id,o.description,o.lot_type,o.active,o.created_by,o.created,o.updated_by,o.updated,so.structure_id" + " from occupancy o,structure_occupant so" + " where o.occupancy_id=so.occupancy_id and o.lot_type > 0  and o.occupancy_id in" + " (select occupancy_id from structure_occupant where structure_id=" + lsoId + ")";
		}

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			logger.debug(sql);

			while (rs.next()) {
				LotEdit lot = new LotEdit();
				lot.setParentId(lsoId);
				lot.setLotId(rs.getString("lot_id"));
				lot.setType(rs.getString("lot_type"));
				lot.setDescription(rs.getString("description"));

				if ((rs.getString("active") != null) && !rs.getString("active").equals("")) {
					lot.setActive("on");
				}

				lot.setDelete("");
				lot.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				lot.setCreated(rs.getDate("created"));
				lot.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				lot.setCreated(rs.getDate("updated"));

				lots.add(lot);
			}

			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		logger.debug("lotSize is " + lots.size());

		return lots;
	}

	// end method getLots
	// adding lot to database
	public LotEdit addLot(LotEdit lot) {
		int lotId = -1;
		String sql = "";

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();
			lotId = db.getNextId("LSO_ID");

			if ((lot.getActive() != null) && !lot.getActive().equals("")) {
				lot.setActive("Y");
			}

			String lsoType = new AddressAgent().getLsoType(StringUtils.s2i(lot.getParentId()));

			if (lsoType == null) {
				lsoType = "";
			}

			if (lsoType.equals("L")) {
				sql = "insert into structure(structure_id,lot_type,description,active,created_by,created,updated_by,updated) values(" + lotId + "," + lot.getType() + "," + StringUtils.checkString(lot.getDescription()) + "," + StringUtils.checkString(lot.getActive()) + "," + lot.getUpdatedBy().getUserId() + "," + "current_timestamp" + "," + lot.getUpdatedBy().getUserId() + "," + "current_timestamp" + ")";
				logger.debug(sql);
				db.addBatch(sql);
				sql = "insert into land_structure(land_id,structure_id) values(" + lot.getParentId() + "," + lotId + ")";
				logger.debug(sql);
				db.addBatch(sql);
			} else if (lsoType.equals("S")) {
				sql = "insert into occupancy(occupancy_id,lot_type,description,active,created_by,created,updated_by,updated) values(" + lotId + "," + lot.getType() + "," + StringUtils.checkString(lot.getDescription()) + "," + StringUtils.checkString(lot.getActive()) + "," + lot.getUpdatedBy().getUserId() + "," + "current_timestamp" + "," + lot.getUpdatedBy().getUserId() + "," + "current_timestamp" + ")";
				logger.debug(sql);
				db.addBatch(sql);
				sql = "insert into structure_occupant(structure_id,occupancy_id) values(" + lot.getParentId() + "," + lotId + ")";
				logger.debug(sql);
				db.addBatch(sql);
			}

			db.executeBatch();
			lot.setLotId(StringUtils.i2s(lotId));
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lot;
	}

	// end method addlot
	// update lot to database
	public LotEdit updateLot(LotEdit lot) {
		String sql = "";

		try {
			Wrapper db = new Wrapper();
			logger.debug("Active is " + lot.getActive());

			if ((lot.getActive() != null) && !lot.getActive().equals("")) {
				lot.setActive("Y");
			}

			String lsoType = new AddressAgent().getLsoType(StringUtils.s2i(lot.getParentId()));

			if (lsoType == null) {
				lsoType = "";
			}

			if (lsoType.equals("L")) {
				sql = "update structure set lot_type=" + lot.getType() + "," + "description=" + StringUtils.checkString(lot.getDescription()) + "," + "active=" + StringUtils.checkString(lot.getActive()) + "," + "updated_by=" + lot.getUpdatedBy().getUserId() + "," + "updated=current_timestamp" + " where structure_id=" + lot.getLotId();
				logger.debug(sql);
				db.update(sql);
			} else if (lsoType.equals("S")) {
				sql = "update occupancy set lot_type=" + lot.getType() + "," + "description=" + StringUtils.checkString(lot.getDescription()) + "," + "active=" + StringUtils.checkString(lot.getActive()) + "," + "updated_by=" + lot.getUpdatedBy().getUserId() + "," + "updated=current_timestamp" + " where occupancy_id=" + lot.getLotId();
				logger.debug(sql);
				db.update(sql);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lot;
	}

	// end method addlot
	public int deleteLot(int lotId) {
		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			String sql = "select count(*) as count from structure where structure_id=" + lotId;
			RowSet rs = db.select(sql);

			if (rs.next()) {
				int count = rs.getInt("count");

				if (count == 0) {
					sql = "delete from structure_occupant where occupancy_id=" + lotId;
					logger.debug(sql);
					db.addBatch(sql);
					sql = "delete from occupancy where occupancy_id=" + lotId;
					logger.debug(sql);
					db.addBatch(sql);
				} else {
					sql = "delete from land_structure where structure_id=" + lotId;
					logger.debug(sql);
					db.addBatch(sql);
					sql = "delete from structure where structure_id=" + lotId;
					logger.debug(sql);
					db.addBatch(sql);
				}

				db.executeBatch();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());

			return -1;
		}

		return lotId;
	}

	// end method delete lot
	public Owner getOwnerByAssessorData(int lsoId) throws Exception {
		logger.debug("getOwnerByAssessorData(" + lsoId + ")");

		Owner owner = new Owner();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT O.OWNER_ID,O.NAME,VLT.LSO_TYPE");
		sql.append(" FROM ((V_LSO_TYPE VLT LEFT OUTER JOIN LSO_APN LA ON VLT.LSO_ID = LA.LSO_ID AND VLT.LSO_ID = " + lsoId + ")");
		sql.append(" LEFT OUTER JOIN APN_OWNER AO ON LA.APN = AO.APN )");
		sql.append(" LEFT OUTER JOIN OWNER O ON AO.OWNER_ID = O.OWNER_ID");
		logger.debug(" SQL1 : " + sql.toString());

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());

			// Getting the latest owner info of activity in that project
			if (rs.next()) {
				if (rs.getInt("OWNER_ID") == 0) {
					if ((rs.getString("LSO_TYPE") != null) && !rs.getString("LSO_TYPE").equals("L")) { // LSO_TYPE equals S or O
						sql = null;
						sql = new StringBuffer();
						sql.append(" SELECT O.OWNER_ID,O.NAME");
						sql.append(" FROM (LSO_APN LA JOIN APN_OWNER AO ON LA.APN = AO.APN)");
						sql.append(" JOIN OWNER O ON AO.OWNER_ID = O.OWNER_ID");
						sql.append(" WHERE LA.LSO_ID =(SELECT LAND_ID FROM V_LSO_LAND WHERE LSO_ID = " + lsoId + ")");
						logger.debug("SQL2 : " + sql.toString());

						RowSet rs1 = db.select(sql.toString());

						if (rs1.next()) {
							owner.setOwnerId(rs1.getInt("OWNER_ID"));
							owner.setName(rs1.getString(""));
						}

						if (rs1 != null) {
							rs1.close();
						}
					} else {
						owner.setOwnerId(rs.getInt("OWNER_ID"));
						owner.setName(rs.getString(""));
					}
				} else {
					owner.setOwnerId(rs.getInt("OWNER_ID"));
					owner.setName(rs.getString("NAME"));
				}

				owner.setOwnerId(rs.getInt("OWNER_ID"));
				owner.setName(rs.getString("NAME"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getOwnerByAssessorData() " + e.getMessage());
			throw e;
		}

		return owner;
	}

	/**
	 * gets the owner name for lso id
	 * 
	 * @param lsoId
	 * @return
	 * @throws Exception
	 */
	public static String getOwnerName(int lsoId) throws Exception {
		logger.info("getOwnerName(" + lsoId + ")");
		String ownerName = "";

		try {

			String sql = "select NAME from V_LSO_OWNER where lso_id = " + lsoId;
			logger.debug(sql);
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			if (rs != null && rs.next()) {
				ownerName = rs.getString("NAME");
			}
			return ownerName;
		}

		catch (Exception e) {
			logger.error("exception occured when getting owner name" + e.getMessage());
			throw new Exception("Exception occured while getting owner name " + e.getMessage());
		}

	}

	/**
	 * Get Structure LSO ID given the street number and street name id
	 * 
	 * @param lsoId
	 * @return
	 */
	public static int getStructureLSOId(int streetNumber, int streetNameId) throws Exception {
		logger.info("getStructureLSOId(" + streetNumber + ", " + streetNameId + ")");
		int lsoId = -1;
		try {

			String sql = "select lso_id from v_address_list where str_no=" + streetNumber + " and street_id=" + streetNameId + " and lso_type='S'";
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				lsoId = rs.getInt("lso_id");
			}
			return lsoId;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * Get Land Address ID given the street number and street name id
	 * 
	 * @param lsoId
	 * @return
	 */
	public static int getLandAddressId(int streetNumber, int streetNameId) throws Exception {
		logger.info("getLandAddressId(" + streetNumber + ", " + streetNameId + ")");
		int lsoId = -1;
		try {

			String sql = "select addr_id from v_address_list where str_no=" + streetNumber + " and street_id=" + streetNameId + " and lso_type='L'";
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				lsoId = rs.getInt("addr_id");
			}
			return lsoId;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}

	/**
	 * Get Lso id for street number and street name
	 * 
	 * @param streetNumber
	 * @param streetNameId
	 * @return
	 * @throws Exception
	 */
	public int getLsoId(int streetNumber, int streetNameId) throws Exception {
		logger.info("getLsoId(" + streetNumber + ", " + streetNameId + ")");
		int lsoId = -1;
		try {

			String sql = "select lso_id from lso_address where str_no=" + streetNumber + " and street_id=" + streetNameId + " and lso_type='L' and active='Y'";
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				lsoId = rs.getInt("lso_id");
			}
			return lsoId;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}

	}



	private String getStreetIdForStreetNameAndStreetType(String streetName, String streetType) throws Exception {
		String streetId = "";
		String sql = "SELECT STREET_ID FROM STREET_LIST WHERE STR_NAME=" + StringUtils.checkString(streetName) + " and str_type=" + StringUtils.checkString(streetType);
		logger.info(sql);
		RowSet rs = new Wrapper().select(sql);
		if (rs.next()) {
			return rs.getString(1);
		} else {
			return "-1";
		}

	}





	/**
	 * Access method for the land property.
	 * 
	 * @return the current value of the land property
	 */
	public Land getLand(int lsoId) {
		try {
			Wrapper db = new Wrapper();

			// begin of geting the LandDetail Object
			String sql = "select * from land where land_id=" + lsoId;
			logger.debug(sql);

			RowSet rs = db.select(sql);
			landDetails = new LandDetails();

			if (rs.next()) {
				landDetails.setXCordinate(rs.getString("x_coord"));
				landDetails.setYCordinate(rs.getString("y_coord"));
				landDetails.setAlias(rs.getString("alias"));
				landDetails.setDescription(rs.getString("description"));
				landDetails.setParkingZone(LookupAgent.getParkingZone(rs.getInt("pzone_id")));
				landDetails.setRZone(rs.getString("r_zone"));
				landDetails.setActive(rs.getString("active"));
				landDetails.setLabel(rs.getString("label"));
				landDetails.setHighFireArea(rs.getString("HIGH_FIRE_AREA"));
			}

			rs.close();

			// Get the Address Range
			List addressRangeList = new ArrayList();
			sql = "select lar.start_range,lar.end_range,street_id " + " from lso_addr_range lar where lso_id=" + lsoId;
			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				AddressRangeEdit addressRange = new AddressRangeEdit(rs.getString("START_RANGE"), rs.getString("END_RANGE"), rs.getString("STREET_ID"));
				addressRangeList.add(addressRange);
			}

			rs.close();
			landDetails.setAddressRangeList(addressRangeList);
			logger.debug("AddressRangeList (" + addressRangeList.size() + ")");

			// get Zone Info
			zoneList = new ArrayList();
			sql = "select  a.zone_id as zone_id, b.zone as zone from land_zone a, zone b  where  a.land_id =" + lsoId + " and a.zone_id=b.zone_id";
			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				zoneObj = new Zone(rs.getInt("zone_id"), rs.getString("zone"));
				zoneList.add(zoneObj);
			}

			rs.close();

			landDetails.setZone(zoneList);

			// get Use Info
			useList = new ArrayList();
			sql = "select a.lso_use_id as id,b.lso_type as type,b.description as description from land_usage a,lso_use b where a.land_id=" + lsoId + " and a.lso_use_id=b.lso_use_id";
			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				useObj = new Use(rs.getInt("id"), rs.getString("type"), rs.getString("description"));
				useList.add(useObj);
			}

			rs.close();
			landDetails.setUse(useList);

			// get list of land addresses
			landAddressList = new ArrayList();
			sql = "select la.addr_id,la.str_no,la.str_mod,la.street_id,la.city,la.state,la.zip,la.zip4,la.description,la.primary,la.active,vsl.street_name from lso_address la,v_street_list vsl where la.street_id=vsl.street_id and la.primary='Y' and la.lso_id=" + lsoId;
			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				landAddressObj = new LandAddress();
				landAddressObj.setAddressId(rs.getInt("addr_id"));
				landAddressObj.setActive(rs.getString("active"));
				landAddressObj.setDescription(rs.getString("description"));
				landAddressObj.setCity(rs.getString("city"));
				landAddressObj.setState(rs.getString("state"));
				landAddressObj.setZip(rs.getString("zip"));
				landAddressObj.setZip4(rs.getString("zip4"));
				landAddressObj.setPrimary(rs.getString("primary"));
				landAddressObj.setStreetNbr(rs.getInt("str_no"));
				landAddressObj.setStreetModifier(rs.getString("str_mod"));

				Street addressStreet = new Street();
				addressStreet.setStreetId(rs.getInt("street_id"));
				addressStreet.setStreetName(rs.getString("street_name"));
				landAddressObj.setStreet(addressStreet);
				landAddressList.add(landAddressObj);
			}

			rs.close();

			// get list of land apns
			landApnList = new ArrayList();

			String apn = "";
			sql = "select apn from lso_apn where lso_id=" + lsoId;
			logger.debug(sql);
			rs = db.select(sql);

			while (rs.next()) {
				landApnObj = new LandApn();
				apn = rs.getString("apn");
				landApnObj.setApn(apn);
				sql = "select * from owner o,apn_owner ao where o.owner_id=ao.owner_id and ao.apn='" + apn + "'";

				logger.debug(sql);

				RowSet ownerRs = new Wrapper().select(sql);

				if (ownerRs.next()) {
					landApnObj.setActive(ownerRs.getString("active"));

					Owner owner = new Owner();
					owner.setEmail(ownerRs.getString("email_id"));
					owner.setFax(StringUtils.phoneFormat(ownerRs.getString("fax")));
					logger.debug("Got fax no as " + owner.getFax());

					ForeignAddress foreignAddress = new ForeignAddress();
					foreignAddress.setCountry(ownerRs.getString("country"));
					foreignAddress.setLineOne(ownerRs.getString("address_line1"));
					foreignAddress.setLineTwo(ownerRs.getString("address_line2"));
					foreignAddress.setLineThree(ownerRs.getString("address_line3"));
					foreignAddress.setLineFour(ownerRs.getString("address_line4"));
					owner.setForeignAddress(foreignAddress);
					owner.setForeignFlag(ownerRs.getString("foreign_address"));

					Address localAddress = new Address();
					localAddress.setCity(ownerRs.getString("city"));
					localAddress.setState(ownerRs.getString("state"));

					Street localAddressStreet = new Street();
					String streetName = "" + ownerRs.getString("str_name");
					localAddressStreet.setStreetName(streetName);
					localAddress.setStreet(localAddressStreet);
					localAddress.setStreetModifier(ownerRs.getString("str_mod"));
					localAddress.setStreetNbr(ownerRs.getInt("str_no"));
					localAddress.setZip(ownerRs.getString("zip"));
					localAddress.setZip4(ownerRs.getString("zip4"));
					owner.setLocalAddress(localAddress);
					owner.setName(ownerRs.getString("name"));
					owner.setOwnerId(ownerRs.getInt("owner_id"));
					owner.setPhone(StringUtils.phoneFormat(ownerRs.getString("phone")));
					logger.debug("Got fax no as " + owner.getPhone());
					landApnObj.setOwner(owner);
				}

				if (ownerRs != null) {
					ownerRs = null;
				}

				landApnList.add(landApnObj);
			}

			if (rs != null) {
				rs.close();
			}

			// get list of land holds
			landHold = new CommonAgent().getHolds(lsoId, "L", 3);
			// get list of land Tenants
			landTenant = new CommonAgent().getTenants(lsoId, "L", 0);

			landCondition = new ArrayList();
			landAttachment = new ArrayList();
			landComment = new ArrayList();
			landSiteData = new LandSiteData();
			logger.debug("lsoId " + lsoId);
			logger.debug("landDetails " + landDetails);
			logger.debug("landAddressList " + landAddressList);
			logger.debug("landApnList " + landApnList);
			logger.debug("landHold " + landHold);
			logger.debug("landTenant " + landTenant);
			logger.debug("landCondition " + landCondition);
			logger.debug("landAttachment " + landAttachment);
			logger.debug("landSiteData " + landSiteData);
			logger.debug("landComment " + landComment);

			land = new Land(lsoId, landDetails, landAddressList, landApnList, landHold, landTenant, landCondition, landAttachment, landSiteData, landComment);

		} catch (Exception e) {
			logger.error("problem with getting land " + e.getMessage());
		}

		return land;
	}

	/**
	 * Sets the value of the land property.
	 * 
	 * @param land
	 *            the new value of the land property
	 */
	public Land updateLandDetails(Land land) {
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			LandDetails landDetails = land.getLandDetails();
			lsoId = land.getLsoId();

			// Begin the transaction
			db.beginTransaction();

			sql = "UPDATE LAND SET ALIAS=" + StringUtils.checkString(landDetails.getAlias()) + ",DESCRIPTION=" + StringUtils.checkString(landDetails.getDescription()) + ",X_COORD=" + StringUtils.checkString(landDetails.getXCordinate()) + ",Y_COORD=" + StringUtils.checkString(landDetails.getYCordinate()) + ",PZONE_ID=" + landDetails.getParkingZone().getPzoneId() + ",R_ZONE=" + landDetails.getRZone() + ",ACTIVE=" + StringUtils.checkString(landDetails.getActive()) + ",LABEL=" + StringUtils.checkString(landDetails.getLabel()) + " WHERE LAND_ID=" + lsoId;

			logger.debug("LAND UPDATE:" + sql);
			db.addBatch(sql);

			// inserting data into Land_Zone table.
			List zoneList = land.getLandDetails().getZone();
			Iterator zoneiter = zoneList.iterator();
			sql = "delete from land_zone where land_id=" + lsoId;
			db.addBatch(sql);

			while (zoneiter.hasNext()) {
				Zone zoneObj = (Zone) zoneiter.next();
				sql = "insert into land_zone(land_id,zone_id) values(" + lsoId + "," + zoneObj.getId() + ")";
				db.addBatch(sql);
				logger.debug("Insert Land_Zone SQL " + sql);
			}

			// inserting data into Land_Usage Table
			List useList = land.getLandDetails().getUse();
			Iterator useiter = useList.iterator();
			sql = "delete from land_usage where land_id=" + lsoId;
			db.addBatch(sql);

			while (useiter.hasNext()) {
				Use useObj = (Use) useiter.next();
				sql = "insert into land_usage(land_id,lso_use_id) values(" + lsoId + "," + useObj.getId() + ")";
				db.addBatch(sql);
				logger.debug("Insert Land_usage SQL " + sql);
			}

			// Updating the Address Range List for a lso
			db.addBatch("delete from lso_addr_range where lso_id=" + lsoId);

			List addressRangeList = land.getLandDetails().getAddressRangeList();
			Iterator iRange = addressRangeList.iterator();

			while (iRange.hasNext()) {
				AddressRangeEdit aRange = (AddressRangeEdit) iRange.next();
				sql = "insert into lso_addr_range (lso_id,start_range,end_range,street_id) values (" + lsoId + "," + aRange.getStartRange() + "," + aRange.getEndRange() + "," + aRange.getStreetId() + ")";
				db.addBatch(sql);
			}

			db.executeBatch();
			logger.debug("Database updated successfully for Land Details id = " + land.getLsoId());
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update land details :" + e.getMessage());
		}

		return this.getLand(lsoId);
	}

	/**
	 * updates the land address using a land object
	 * 
	 * @param land
	 * @return
	 */
	public Land updateLandAddress(Land land) {
		try {
			Wrapper db = new Wrapper();
			StringBuffer sql = new StringBuffer();

			landAddressList = land.getLandAddress();

			if (landAddressList != null) {
				logger.debug("the size of land address list is :" + landAddressList.size());

				if (landAddressList.size() > 0) {
					landAddressObj = (LandAddress) landAddressList.get(0);

					if (landAddressObj != null) {
						sql.append("UPDATE LSO_ADDRESS SET CITY=");
						sql.append(StringUtils.checkString(landAddressObj.getCity()));
						logger.debug("got address city " + StringUtils.checkString(landAddressObj.getCity()));
						sql.append(",STATE=");
						sql.append(StringUtils.checkString(landAddressObj.getState()));
						logger.debug("got address state " + StringUtils.checkString(landAddressObj.getState()));
						sql.append(",DESCRIPTION=");
						sql.append(StringUtils.checkString(landAddressObj.getDescription()));
						logger.debug("Address description:" + StringUtils.checkString(landAddressObj.getDescription()));
						sql.append(",ACTIVE=");
						sql.append(StringUtils.checkString(landAddressObj.getActive()));
						logger.debug("got address active " + StringUtils.checkString(landAddressObj.getActive()));
						sql.append(",PRIMARY=");
						sql.append(StringUtils.checkString(landAddressObj.getPrimary()));
						logger.debug("got address primary " + StringUtils.checkString(landAddressObj.getPrimary()));
						sql.append(",STR_NO=");
						sql.append(StringUtils.checkNumber(landAddressObj.getStreetNbr()));
						logger.debug("got address street number " + landAddressObj.getStreetNbr());
						sql.append(",STR_MOD=");
						sql.append(StringUtils.checkString(landAddressObj.getStreetModifier()));
						logger.debug("Address Modifier " + StringUtils.checkString(landAddressObj.getStreetModifier()));
						sql.append(",STREET_ID=");
						sql.append(StringUtils.checkNumber(landAddressObj.getStreet().getStreetId()));
						logger.debug("got address street id " + landAddressObj.getStreet().getStreetId());
						sql.append(",ZIP=");
						sql.append(StringUtils.checkNumber(landAddressObj.getZip()));
						logger.debug("got address zip " + landAddressObj.getZip());
						sql.append(",ZIP4=");
						sql.append(StringUtils.checkString(landAddressObj.getZip4()));
						logger.debug("got address zip4 " + landAddressObj.getZip4());
						logger.debug("got address primary id " + landAddressObj.getAddressId());
						sql.append(",LAST_UPDATED=current_timestamp");
						sql.append(",UPDATED_BY=");
						sql.append(landAddressObj.getUpdatedBy().getUserId());
						logger.debug("Updated UserId " + landAddressObj.getUpdatedBy().getUserId());
						sql.append(" WHERE ADDR_ID=");
						sql.append(landAddressObj.getAddressId());
						logger.debug("got address id " + landAddressObj.getAddressId());

						// Begin the transaction
						db.beginTransaction();

						db.addBatch(sql.toString());
						logger.debug("Land address update sql is :" + sql);

						db.executeBatch();
					}

					int a = land.getLsoId();
					logger.debug("Database updated successfully for land address id = " + land.getLsoId());
					land = getLand(a);
				}
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update land address :" + e.getMessage());
			land = null;
		}

		return land;
	}

	/**
	 * updates the land apn using land object
	 * 
	 * @param land
	 * @return
	 */
	public Land updateLandApn(Land land) {
		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();
			landApnList = land.getLandApn();
			logger.debug("the size of land apnlist is :" + landApnList.size());
			lsoId = land.getLsoId();

			if (landApnList.size() > 0) {
				landApnObj = (LandApn) landApnList.get(0);

				String sql = "UPDATE OWNER SET name=" + StringUtils.checkString(landApnObj.getOwner().getName()) + ",STR_NO=" + StringUtils.checkNumber(landApnObj.getOwner().getLocalAddress().getStreetNbr()) + ",STR_MOD=" + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreetModifier()) + ",STR_NAME=" + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreet().getStreetName()) + ",CITY=" + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getCity()) + ",STATE=" + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getState()) + ",PHONE=" + StringUtils.checkString(StringUtils.stripPhone(landApnObj.getOwner().getPhone())) + ",FAX=" + StringUtils.checkString(StringUtils.stripPhone(landApnObj.getOwner().getFax())) + ",EMAIL_ID=" + StringUtils.checkString(landApnObj.getOwner().getEmail()) + ",FOREIGN_ADDRESS=" + StringUtils.checkString(landApnObj.getOwner().getForeignFlag()) + ",ADDRESS_LINE1=" + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineOne()) + ",ADDRESS_LINE2=" + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineTwo()) + ",ADDRESS_LINE3=" + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineThree()) + ",ADDRESS_LINE4=" + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineFour()) + ",COUNTRY=" + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getCountry()) + ",ZIP=" + StringUtils.checkNumber(landApnObj.getOwner().getLocalAddress().getZip()) + ",ZIP4=" + StringUtils.checkNumber(landApnObj.getOwner().getLocalAddress().getZip4()) + ",LAST_UPDATED=current_timestamp" + ",UPDATED_BY=" + landApnObj.getOwner().getUpdatedBy().getUserId() + " WHERE OWNER_ID=" + landApnObj.getOwner().getOwnerId();

				db.addBatch(sql);

				String apn = landApnObj.getApn();

				if (apn == null) {
					apn = "";
				}

				String oldApn = landApnObj.getOldApn();

				if (oldApn == null) {
					oldApn = "";
				}

				if (!(apn.equals("") || oldApn.equals("") || apn.equals(oldApn))) {
					sql = "update lso_apn set apn=" + StringUtils.checkString(apn) + " where apn=" + StringUtils.checkString(oldApn) + " and lso_id = " + lsoId;
					db.addBatch(sql);
					sql = "update apn_owner set apn=" + StringUtils.checkString(apn) + " where owner_id = " + landApnObj.getOwner().getOwnerId();
					db.addBatch(sql);
				}

				db.executeBatch();
				logger.debug("Database updated successfully for land = " + lsoId);
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update land apn:" + e.getMessage());
			land = null;
		}

		return this.getLand(lsoId);
	}

	/**
	 * adds a land using land object
	 * 
	 * @param land
	 * @return
	 */
	public int addLand(Land land) {
		try {
			logger.debug("started addind land");

			Wrapper db = new Wrapper();
			String sql = "";

			LandDetails landDetails = land.getLandDetails();

			if (landDetails != null) {
				db.beginTransaction();

				lsoId = db.getNextId("LSO_ID");

				// inserting data into Land Table
				sql = "Insert into land(land_id,x_coord,y_coord,alias,description,pzone_id,r_zone,active,label) values(" + lsoId + "," + StringUtils.checkString(landDetails.getXCordinate()) + "," + StringUtils.checkString(landDetails.getYCordinate()) + "," + StringUtils.checkString(landDetails.getAlias()) + "," + StringUtils.checkString(landDetails.getDescription()) + "," + landDetails.getParkingZone().getPzoneId() + "," + landDetails.getRZone() + "," + StringUtils.checkString(landDetails.getActive()) + "," + StringUtils.checkString(landDetails.getLabel()) + ")";
				logger.debug("Insert Land SQL " + sql);
				db.addBatch(sql);

				// inserting data into Land_Zone table.
				List zoneList = landDetails.getZone();
				Iterator zoneiter = zoneList.iterator();

				while (zoneiter.hasNext()) {
					Zone zoneObj = (Zone) zoneiter.next();
					sql = "insert into land_zone(land_id,zone_id) values(" + lsoId + "," + zoneObj.getId() + ")";
					logger.debug("Insert Land_Zone SQL " + sql);
					db.addBatch(sql);
				}

				// inserting data into Land_Usage Table
				List useList = landDetails.getUse();
				Iterator useiter = useList.iterator();

				while (useiter.hasNext()) {
					Use useObj = (Use) useiter.next();
					sql = "insert into land_usage(land_id,lso_use_id) values(" + lsoId + "," + useObj.getId() + ")";
					logger.debug("Insert Land_usage SQL " + sql);
					db.addBatch(sql);
				}
			}

			// *** Begin Inserting Land Address
			landAddressList = land.getLandAddress();

			if (landAddressList != null) {
				if (landAddressList.size() > 0) {
					landAddressObj = (LandAddress) landAddressList.get(0);

					if (landAddressObj != null) {
						int addressId = db.getNextId("ADDRESS_ID");
						logger.debug("The ID generated for new Address is " + addressId);
						sql = "insert into lso_address(addr_id,lso_id,lso_type,str_no,str_mod,street_id,city,state,zip,zip4,description,primary,active,last_updated,updated_by,addr_primary) values(" + addressId + "," + lsoId + "," + "'L'," + landAddressObj.getStreetNbr() + "," + StringUtils.checkString(landAddressObj.getStreetModifier()) + "," + landAddressObj.getStreet().getStreetId() + "," + StringUtils.checkString(landAddressObj.getCity()) + "," + StringUtils.checkString(landAddressObj.getState()) + "," + StringUtils.checkNumber(landAddressObj.getZip()) + "," + StringUtils.checkString(landAddressObj.getZip4()) + "," + StringUtils.checkString(landAddressObj.getDescription()) + ",'Y'," + StringUtils.checkString(landAddressObj.getActive()) + "," + "current_timestamp" + "," + landAddressObj.getUpdatedBy().getUserId() + "," + addressId + ")";

						logger.debug(sql);
						db.addBatch(sql);
						sql = "insert into lso_addr_range (lso_id,start_range,end_range,street_id) values (" + lsoId + "," + landAddressObj.getStreetNbr() + "," + landAddressObj.getStreetNbr() + "," + landAddressObj.getStreet().getStreetId() + ")";
						logger.debug(sql);
						db.addBatch(sql);
						logger.debug(sql);
					} else {
						logger.debug(" Land Address is not Created");
					}
				}
			}

			// *** End Inserting Land Address
			// *** Begin Inserting Land APN
			landApnList = land.getLandApn();

			if (landApnList != null) {
				if (landApnList.size() > 0) {
					landApnObj = (LandApn) landApnList.get(0);

					if (landApnObj != null) {
						if ((landApnObj.getApn() != null) && !(landApnObj.getApn().equals(""))) {
							// inserting values to lso_apn table
							sql = "insert into lso_apn(apn,lso_id,lso_type) values(" + StringUtils.checkString(landApnObj.getApn()) + "," + lsoId + ",'L')";
							logger.debug(sql);
							db.addBatch(sql);

							// inserting values to owner table
							int ownerId = db.getNextId("OWNER_ID");
							logger.debug("ID generated for owner  " + ownerId);

							sql = "insert into owner(owner_id,name,str_no,str_mod,pre_dir,str_name,str_type,suf_dir,unit," + "city,state,zip,phone,fax,email_id,foreign_address," + "address_line1,address_line2,address_line3,address_line4,country," + "last_updated,updated_by) " + "values(" + ownerId + "," + StringUtils.checkString(landApnObj.getOwner().getName()) + "," + landApnObj.getOwner().getLocalAddress().getStreetNbr() + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreetModifier()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreet().getStreetPrefix()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreet().getStreetName()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreet().getStreetType()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getStreet().getStreetSuffix()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getUnit()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getCity()) + "," + StringUtils.checkString(landApnObj.getOwner().getLocalAddress().getState()) + "," + StringUtils.checkInteger(landApnObj.getOwner().getLocalAddress().getZip()) + "," + StringUtils.checkString(StringUtils.stripPhone(landApnObj.getOwner().getPhone())) + "," + StringUtils.checkString(StringUtils.stripPhone(landApnObj.getOwner().getFax())) + "," + StringUtils.checkString(landApnObj.getOwner().getEmail()) + "," + StringUtils.checkString(landApnObj.getOwner().getForeignFlag()) + "," + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineOne()) + "," + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineTwo()) + "," + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineThree()) + "," + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getLineFour()) + "," + StringUtils.checkString(landApnObj.getOwner().getForeignAddress().getCountry()) + "," + "current_timestamp" + "," + landApnObj.getOwner().getUpdatedBy().getUserId() + ")";
							logger.debug(sql);
							db.addBatch(sql);

							// begin inserting values to apn_owner table
							sql = "insert into apn_owner(apn,owner_id,active,last_updated,updated_by) " + "values(" + StringUtils.checkString(landApnObj.getApn()) + "," + ownerId + "," + StringUtils.checkString(landApnObj.getActive()) + "," + "current_timestamp," + landApnObj.getOwner().getUpdatedBy().getUserId() + ")";
							logger.debug(sql);
							db.addBatch(sql);

							// end inserting values to apn_owner table
						} else {
							logger.debug("Land apn is empty");
						}
					} else {
						logger.debug(" Land APN  is not  Created");
					}
				}
			}

			// *** End Inserting Land APN
			db.executeBatch();
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add land :" + e.getMessage());
		}

		return lsoId;
	}

	/**
	 * Finds all the Land Addresses matching a particular lsoId
	 * 
	 * @return A CachedRowSet of all matching addresses
	 */
	public RowSet getLandList(String lsoId) {
		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();

			String sql = "SELECT LSO_ID, STR_NO, CITY, STATE, ZIP, PRIMARY, STREET_NAME  FROM LSO_ADDRESS, V_STREET_LIST WHERE LSO_ADDRESS.LSO_ID =" + lsoId + " AND V_STREET_LIST.STREET_ID = LSO_ADDRESS.STREET_ID AND LSO_ADDRESS.PRIMARY='Y'";

			db.select(sql);

		} catch (Exception e) {
			logger.debug("getLandList method failed in LandAgent - " + e.getMessage());
		}

		return rs;
	}

	/**
	 * add a land apn
	 * 
	 * @param land
	 * @return
	 */
	public Land addLandApn(Land land) {
		try {
			Wrapper db = new Wrapper();

			// Begin the transaction
			db.beginTransaction();

			int ownerId = db.getNextId("OWNER_ID");
			logger.debug("new owner id generated is " + ownerId);

			int lsoId = land.getLsoId();
			logger.debug("obtained lso id is" + lsoId);

			List landApnList = land.getLandApn();
			logger.debug("the size of land apn list is :" + landApnList.size());

			if (landApnList.size() > 0) {
				LandApn landApn = (LandApn) landApnList.get(0);

				String lsoApn = "INSERT INTO LSO_APN(LSO_ID,APN,LSO_TYPE) VALUES(" + lsoId + ",'" + landApn.getApn() + "','L')";

				db.addBatch(lsoApn);
				logger.debug("Insert land SQL " + lsoApn);

				String owner = "INSERT INTO OWNER(OWNER_ID ,NAME ,STR_NO,STR_MOD,STR_NAME,CITY,STATE,ZIP,ZIP4,PHONE,FAX,EMAIL_ID ,FOREIGN_ADDRESS ,ADDRESS_LINE1,ADDRESS_LINE2 ,ADDRESS_LINE3 ,ADDRESS_LINE4 ,COUNTRY ,LAST_UPDATED,UPDATED_BY) VALUES (" + ownerId + "," + StringUtils.checkString(landApn.getOwner().getName()) + "," + StringUtils.checkNumber(landApn.getOwner().getLocalAddress().getStreetNbr()) + "," + StringUtils.checkString(landApn.getOwner().getLocalAddress().getStreetModifier()) + "," + StringUtils.checkString(landApn.getOwner().getLocalAddress().getStreet().getStreetName()) + "," + StringUtils.checkString(landApn.getOwner().getLocalAddress().getCity()) + "," + StringUtils.checkString(landApn.getOwner().getLocalAddress().getState()) + "," + StringUtils.checkInteger(landApn.getOwner().getLocalAddress().getZip()) + "," + StringUtils.checkInteger(landApn.getOwner().getLocalAddress().getZip4()) + "," + StringUtils.checkString(StringUtils.stripPhone(landApn.getOwner().getPhone())) + "," + StringUtils.checkString(StringUtils.stripPhone(landApn.getOwner().getFax())) + "," + StringUtils.checkString(landApn.getOwner().getEmail()) + "," + StringUtils.checkString(landApn.getOwner().getForeignFlag()) + "," + StringUtils.checkString(landApn.getOwner().getForeignAddress().getLineOne()) + "," + StringUtils.checkString(landApn.getOwner().getForeignAddress().getLineTwo()) + "," + StringUtils.checkString(landApn.getOwner().getForeignAddress().getLineThree()) + "," + StringUtils.checkString(landApn.getOwner().getForeignAddress().getLineFour()) + "," + StringUtils.checkString(landApn.getOwner().getForeignAddress().getCountry()) + ",sysdate," + landApn.getUpdatedBy() + ")";
				db.addBatch(owner);
				logger.debug("Insert SQL " + owner);

				String apnOwner = "INSERT INTO APN_OWNER(APN,OWNER_ID,ACTIVE,LAST_UPDATED,UPDATED_BY) VALUES(" + StringUtils.checkString(landApn.getApn()) + "," + ownerId + ",'Y',sysdate," + landApn.getUpdatedBy() + ")";

				db.addBatch(apnOwner);
				logger.debug("Insert land SQL " + apnOwner);

				db.executeBatch();

				int aLsoId = land.getLsoId();
				logger.debug("Database updated successfully for land apn id = " + land.getLsoId());
				land = getLand(aLsoId);
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add land Apn :" + e.getMessage());
		}

		return land;
	}

	/**
	 * get a land apn for this apn
	 * 
	 * @param apn
	 * @return LandApn
	 */
	public LandApn getLandApn(String apn) throws Exception {
		LandApn landApnObj = null;
		RowSet ownerRs = null;

		try {
			landApnObj = new LandApn();

			landApnObj.setApn(apn);

			String sql = "select * from owner o,apn_owner ao where o.owner_id=ao.owner_id and ao.apn='" + apn + "'";
			logger.debug(sql);

			ownerRs = new Wrapper().select(sql);

			if (ownerRs.next()) {
				landApnObj.setActive(ownerRs.getString("active"));

				Owner owner = new Owner();
				owner.setEmail(ownerRs.getString("email_id"));
				owner.setFax(StringUtils.phoneFormat(ownerRs.getString("fax")));
				logger.debug("Got fax no as " + owner.getFax());

				ForeignAddress foreignAddress = new ForeignAddress();
				foreignAddress.setCountry(ownerRs.getString("country"));
				foreignAddress.setLineOne(ownerRs.getString("address_line1"));
				foreignAddress.setLineTwo(ownerRs.getString("address_line2"));
				foreignAddress.setLineThree(ownerRs.getString("address_line3"));
				foreignAddress.setLineFour(ownerRs.getString("address_line4"));
				owner.setForeignAddress(foreignAddress);
				owner.setForeignFlag(ownerRs.getString("foreign_address"));

				Address localAddress = new Address();
				localAddress.setCity(ownerRs.getString("city"));
				localAddress.setState(ownerRs.getString("state"));

				Street localAddressStreet = new Street();
				String streetName = ownerRs.getString("str_name");
				logger.debug(" Street name in land agent is " + streetName);

				localAddressStreet.setStreetName(streetName);
				logger.debug(" get Street name from localaddressstreet " + streetName);

				localAddress.setStreet(localAddressStreet);
				localAddress.setStreetModifier(ownerRs.getString("str_mod"));
				localAddress.setStreetNbr(ownerRs.getInt("str_no"));
				localAddress.setZip(ownerRs.getString("zip"));
				localAddress.setZip4(ownerRs.getString("zip4"));
				logger.debug(" localaddress streetnbr " + localAddress.getStreetNbr());

				owner.setLocalAddress(localAddress);
				owner.setName(ownerRs.getString("name"));
				owner.setOwnerId(ownerRs.getInt("owner_id"));
				owner.setPhone(StringUtils.phoneFormat(ownerRs.getString("phone")));
				logger.debug("Got phone no as " + owner.getPhone());
				landApnObj.setOwner(owner);
			}

			return landApnObj;
		} catch (Exception e) {
			logger.warn("Exception thrown while trying to add land Apn :" + e.getMessage());
			throw new Exception("Exception thrown while trying to add land " + e.getMessage());
		} finally {
			try {
				if (ownerRs != null) {
					ownerRs = null;
				}
			} catch (Exception e) {
				logger.warn("unable to close resultset " + e.getMessage());
			}
		}
	}

	public boolean checkLandAlreadyExist(String streetModifier, int streetNumber, String streetName) throws Exception {
		logger.info("checkLandAlreadyExist(" + streetModifier + ", " + streetNumber + ", " + streetName + ")");

		boolean result = false;

		try {
			Wrapper db = new Wrapper();
			StringBuffer sql = new StringBuffer("select * from lso_address where lso_type='L' and active='Y' and ");

			if ((streetModifier == null) || (streetModifier.equals(""))) {
				sql.append(" str_mod is null");
			} else {
				sql.append(" str_mod =" + StringUtils.checkString(streetModifier));
			}

			sql.append(" and str_no=" + streetNumber);

			if ((streetName == null) || (streetName.equals(""))) {
				sql.append(" and street_id is null");
			} else {
				sql.append(" and street_id =" + StringUtils.checkString(streetName));
			}

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			if (rs.next()) {
				result = true;
			}

			return result;
		} catch (Exception e) {
			throw new Exception("Error in method checkLandAlreadyExist" + e.getMessage());
		}
	}

	/**
	 * get the lso tree list for one LSO id
	 * 
	 * @param lsoId
	 * @return
	 */
	public List getLsoTreeList(String lsoId, String contextRoot) throws Exception {
		Long lso = new Long(0);

		try {
			lso = new Long(lsoId);

			List list = new ArrayList();
			list.add(lso);

			return this.getLsoTreeList(list, false, contextRoot);
		} catch (Exception e) {
			logger.debug("exception in \"getLsoTreeList(String lsoId)\" of LsoTreeAgent. - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * gets the lso tree list for multiple lso ids
	 * 
	 * @param lsoIds
	 * @return
	 * @throws Exception
	 */
	public List getLsoTreeList(List lsoIds, String contextRoot) throws Exception {
		logger.info("getLsoTreeList(List lsoIds)");

		try {
			return getLsoTreeList(lsoIds, false, contextRoot);
		} catch (Exception e) {
			logger.error("Exception thrown in getLsoTreeList(List lsoIds)");
			throw e;
		}
	}

	/**
	 * gets the lso tree list
	 * 
	 * @param lsoIds
	 * @param active
	 * @return
	 */
	public List getLsoTreeList(List lsoIds, boolean active, String contextRoot) throws Exception {
		logger.info("getLsoTreeList(" + lsoIds + "," + active + ")");

		List lsoTreeList = new ArrayList(); // used to return result

		// the list
		if (lsoIds.size() <= 0) {
			return lsoTreeList;
		}

		// the wrapper
		Wrapper wrapper = new Wrapper();

		// the connections
		Connection connection = null;

		// the statements
		PreparedStatement stmtLso = null;
		PreparedStatement pstmtStructure = null; // to query Structures for a Land
		PreparedStatement pstmtOccupancy = null; // to query not null unit in the Occupancy for a Structure
		PreparedStatement pstmtOccupancyNullUnit = null; // to query null unit in the Occupancy for a Structure

		// the result sets
		ResultSet rsLso = null;
		ResultSet rsStructure = null; // stores structures for a land
		ResultSet rsOccupancy = null; // stores occupancy for a structure
		ResultSet rsOccupancyNullUnit = null; // stores occupancy for a structure with unit null

		// the lso tree
		LsoTree lsoTree = null;

		try {
			// get the database connections.
			connection = wrapper.getConnectionForPreparedStatementOnly();

			// prepare statements
			stmtLso = connection.prepareStatement(GET_LSO);

			long sequence = 1;
			long currentLand = 0;
			long currentStructure = 0;
			long landId = 0;
			long lsoId = 0;

			String sql = "";

			// char type = ' ';
			String alt = "";
			String title = "";
			String label = "";

			if (active == true) {
				pstmtStructure = connection.prepareStatement(GET_ACTIVE_STRUCTURE_TREE);
				logger.debug("Structure SQL:" + GET_ACTIVE_STRUCTURE_TREE);

				pstmtOccupancy = connection.prepareStatement(GET_ACTIVE_OCCUPANCY_TREE);
				logger.debug("Occupancy with out null values of unit no SQL:" + GET_ACTIVE_OCCUPANCY_TREE);

				pstmtOccupancyNullUnit = connection.prepareStatement(GET_ACTIVE_OCCUPANCY_TREE_NULL_UNIT);
				logger.debug("Occupancy with null values of unit no SQL:" + GET_ACTIVE_OCCUPANCY_TREE_NULL_UNIT);
			} else {
				pstmtStructure = connection.prepareStatement(GET_ALL_STRUCTURE_TREE);
				logger.debug("Structure SQL:" + GET_ALL_STRUCTURE_TREE);

				pstmtOccupancy = connection.prepareStatement(GET_ALL_OCCUPANCY_TREE);
				logger.debug("Occupancy SQL:" + GET_ALL_OCCUPANCY_TREE);

				pstmtOccupancyNullUnit = connection.prepareStatement(GET_ALL_OCCUPANCY_UNIT_NULL_TREE);
				logger.debug("Occupancy SQL:" + GET_ALL_OCCUPANCY_UNIT_NULL_TREE);
			}

			Iterator iLsoId = lsoIds.iterator();

			// Keep a record of what lso ids have been traversed.
			// Don't allow duplicates
			Set setLsoId = Collections.synchronizedSet(new HashSet());

			while (iLsoId.hasNext()) {
				Long newValue = (Long) iLsoId.next();

				// Ignore duplicate lsoid values
				if (setLsoId.contains(newValue)) {
					continue;
				}

				setLsoId.add(newValue);

				// has to be a List of Long objects
				lsoId = (newValue).longValue();
				stmtLso.setLong(1, lsoId);
				logger.debug("land sql is " + GET_LSO);
				rsLso = stmtLso.executeQuery();

				if (rsLso.next()) {
					// update sequence number for this Land
					currentLand = sequence;
					sequence++;

					landId = rsLso.getLong("LSO_ID");

					String sLandId = rsLso.getString("LSO_ID");
					Map map = new Hashtable();
					map.put("lsoId", sLandId);

					label = getLabel(landId, "land");

					alt = rsLso.getString("STR_NO") + " "; 
					
					if(rsLso.getString("STR_MOD") != null && !rsLso.getString("STR_MOD").equalsIgnoreCase("")) {
						alt = alt + rsLso.getString("STR_MOD") +" ";
					}
					alt = alt+rsLso.getString("STREET_NAME") + " " + label;
					
					title = "";
					title = "LAND&#10;&#13;" + ((rsLso.getString("DESCRIPTION") != null) ? rsLso.getString("DESCRIPTION") : "");

					// create a node for this Land
					lsoTree = new LsoTree((new Long(currentLand)).toString(), "0", StringUtils.replaceDoubleQuot(alt), contextRoot + "/viewLand.do?from=tree&lsoId=" + landId , StringUtils.replaceDoubleQuot(title), landId, "L", map);

					// check and update the hold status.
					// lsoTree = updateLsoHoldStatus(lsoTree, "L", sLandId);
					lsoTreeList.add(lsoTree);

					pstmtStructure.setLong(1, landId);

					// however LSO_ID is what we received as argument which can be directly used
					rsStructure = pstmtStructure.executeQuery();

					// iterate over all structures for this land
					while (rsStructure.next()) {
						logger.debug("Getting Structure " + rsStructure.getString("STRUCTURE_ID"));

						// update sequence numbers for this Structure
						currentStructure = sequence;
						sequence++;

						// get a list of occupancies with out null unit value
						pstmtOccupancy.setLong(1, rsStructure.getLong("STRUCTURE_ID"));
						rsOccupancy = pstmtOccupancy.executeQuery();

						// get a list of occupancies with out null unit value
						pstmtOccupancyNullUnit.setLong(1, rsStructure.getLong("STRUCTURE_ID"));
						rsOccupancyNullUnit = pstmtOccupancyNullUnit.executeQuery();

						String tempString = rsStructure.getString("STRUCTURE_ID");

						label = getLabel(rsStructure.getLong("STRUCTURE_ID"), "structure");

						alt = "" + rsStructure.getString("ADDRESS") + " " + label;
						if (alt.equals("null")) {
							alt = rsStructure.getString("DESCRIPTION");
							logger.debug("No Address");
						}

						title = "";
						title = "STRUCTURE&#10;&#13;" + ((rsStructure.getString("DESCRIPTION") != null) ? rsStructure.getString("DESCRIPTION") : "");

						lsoTree = new LsoTree((new Long(currentStructure)).toString(), new Long(currentLand).toString(), StringUtils.replaceDoubleQuot(alt), contextRoot + "/viewStructure.do?from=tree&lsoId=" + tempString , StringUtils.replaceDoubleQuot(title), rsStructure.getLong("STRUCTURE_ID"), "S", map);

						// checking to be done here
						// lsoTree = updateLsoHoldStatus(lsoTree, "S", tempString);
						lsoTreeList.add(lsoTree);
						setLsoId.add(new Long(rsStructure.getLong("STRUCTURE_ID")));

						// iterate over all occupancies of null unit for this structure
						while (rsOccupancyNullUnit.next()) {
							String tempString3 = rsOccupancyNullUnit.getString("OCCUPANCY_ID");

							label = getLabel(rsOccupancyNullUnit.getLong("OCCUPANCY_ID"), "occupancy");

							alt = "" + rsOccupancyNullUnit.getString("ADDRESS") + " " + label;

							if (alt.equals("null")) {
								alt = rsOccupancyNullUnit.getString("DESCRIPTION");
								logger.debug("No Address");
							}

							title = "";
							title = "OCCUPANCY&#10;&#13;" + ((rsOccupancyNullUnit.getString("DESCRIPTION") != null) ? rsOccupancyNullUnit.getString("DESCRIPTION") : "");
							lsoTree = new LsoTree((new Long(sequence)).toString(), new Long(currentStructure).toString(), StringUtils.replaceDoubleQuot(alt), contextRoot + "/viewOccupancy.do?from=tree&lsoId=" + tempString3 , StringUtils.replaceDoubleQuot(title), rsOccupancyNullUnit.getLong("OCCUPANCY_ID"), "O", map);
							// add APN and Owner name to the object (to be displayed in assessor search screen)
							String apn = LookupAgent.getApnForLsoId(tempString3);
							logger.debug("obtained apn is " + apn);
							lsoTree.setApn(apn);
							String owner = LookupAgent.getOwnerForLsoId(tempString3);
							logger.debug("obtained owner is " + owner);
							lsoTree.setOwner(owner);
							// checking to be done here
							// lsoTree = updateLsoHoldStatus(lsoTree, "O", tempString2);
							lsoTreeList.add(lsoTree);
							setLsoId.add(new Long(rsOccupancyNullUnit.getLong("OCCUPANCY_ID")));
							sequence++;
						}

						// iterate over all occupancies for this structure
						while (rsOccupancy.next()) {
							String tempString2 = rsOccupancy.getString("OCCUPANCY_ID");
							label = getLabel(rsOccupancy.getLong("OCCUPANCY_ID"), "occupancy");
							alt = "" + rsOccupancy.getString("ADDRESS") + " " + label;

							if (alt.equals("null")) {
								alt = rsOccupancy.getString("DESCRIPTION");
								logger.debug("No Address");
							}

							title = "";
							title = "OCCUPANCY&#10;&#13;" + ((rsOccupancy.getString("DESCRIPTION") != null) ? rsOccupancy.getString("DESCRIPTION") : "");
							lsoTree = new LsoTree((new Long(sequence)).toString(), new Long(currentStructure).toString(), StringUtils.replaceDoubleQuot(alt), "javascript:parent.f_content.location.href='" + contextRoot + "/viewOccupancy.do?lsoId=" + tempString2 + "';parent.f_psa.location.href='" + contextRoot + "/viewPsaTree.do?lsoId=" + tempString2 + "'", StringUtils.replaceDoubleQuot(title), rsOccupancy.getLong("OCCUPANCY_ID"), "O", map);
							// add APN and Owner name to the object (to be displayed in assessor search screen)
							String apn = LookupAgent.getApnForLsoId(tempString2);
							logger.debug("obtained apn is " + apn);
							lsoTree.setApn(apn);
							String owner = LookupAgent.getOwnerForLsoId(tempString2);
							logger.debug("obtained owner is " + owner);
							lsoTree.setOwner(owner);
							// checking to be done here
							lsoTreeList.add(lsoTree);
							setLsoId.add(new Long(rsOccupancy.getLong("OCCUPANCY_ID")));
							sequence++;
						}

						if (rsOccupancy != null) {
							rsOccupancy.close();
						}
						if (rsOccupancyNullUnit != null) {
							rsOccupancyNullUnit.close();
						}

						logger.debug("Finished with Structure ");
					}

					if (rsStructure != null) {
						rsStructure.close();
					}
				} else {
					logger.debug("LSO_ID not found");
				}

				if (rsLso != null) {
					rsLso.close();
				}
			}

			// end while iterator.hasNext()
			logger.info("Exiting getLsoTreeList (" + lsoTreeList.size() + ")");
			logger.info("Getting hold status");
			getHoldInfo(lsoTreeList);
			logger.info("After hold status");

			return lsoTreeList;
		} catch (Exception e) {
			logger.error("Exception Message :" + e.getMessage());
			throw e;
		} finally {
			try {

				if (rsLso != null) {
					rsLso.close();
				}
				if (rsStructure != null) {
					rsStructure.close();
				}

				if (rsOccupancy != null) {
					rsOccupancy.close();
				}

				if (rsOccupancyNullUnit != null) {
					rsOccupancyNullUnit.close();
				}

				if (stmtLso != null) {
					stmtLso.close();
				}

				if (pstmtOccupancy != null) {
					pstmtOccupancy.close();
				}

				if (pstmtOccupancyNullUnit != null) {
					pstmtOccupancyNullUnit.close();
				}

				if (pstmtStructure != null) {
					pstmtStructure.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Exception occured while trying to close database handles " + e.getMessage());
			}
		}
	}

	/**
	 * Returns a list of LSO_IDs matching the search criteria
	 */
	public List getLsoIdList(LsoSearchForm lsoSearchForm) throws Exception {
		logger.info("Entering getLsoIdList with form");

		try {
			return this.getLsoIdList(lsoSearchForm.getStreetNumber(), StringUtils.s2i(lsoSearchForm.getStreetName()), false, "search", "");
		} catch (Exception e) {
			logger.error("Exception thrown in getLsoIdList(LsoSearchForm lsoSearchForm)");
			throw e;
		}
	}

	/**
	 * Returns a list of LSO_IDs matching the search criteria. This shall return all matching Active and Inactive Land Ids.
	 * 
	 */
	public List getLsoIdList(String streetNum, long streetId) throws Exception {
		logger.info("getLsoIdList (" + streetNum + "," + streetId + ")");

		try {
			return getLsoIdList(streetNum, streetId, false, "", "");
		} catch (Exception e) {
			logger.error("Exception thrown in getLsoIdList(" + streetNum + ", " + streetId + ") ");
			throw e;
		}
	}

	public List getLsoIdList(String streetNum, String streetId) throws Exception {
		logger.info("getLsoIdList (" + streetNum + "," + streetId + ")");

		try {
			return getLsoIdList(streetNum, (long) StringUtils.s2i(streetId));
		} catch (Exception e) {
			logger.error("Exception thrown in getLsoIdList(" + streetNum + ", " + streetId + ") ");
			throw e;
		}
	}

	/**
	 * Gets the lso id list.
	 * 
	 * @param streetNum
	 * @param streetId
	 * @param active
	 * @return
	 */
	public List getLsoIdList(String streetNum, long streetId, boolean active, String uthavakarai, String streetId2) throws Exception {
		logger.info("getLsoIdList (" + streetNum + "," + streetId + "," + active + "," + streetId2 + ")");

		// the database wrapper
		Wrapper wrapper = new Wrapper();

		// the connection objects
		Connection connection = null;

		PreparedStatement pstmtLand = null;
		PreparedStatement pstmtStructure = null;
		PreparedStatement pstmtOccupancy = null;

		ResultSet rsLso = null;

		// the lso id list
		List lsoIdList = new ArrayList(); // used to return result

		if (streetId == -1) {
			return lsoIdList;
		}

		// format the query string in streetNum to make it suitable for SQL
		String query = "";

		if (streetNum.equals("") || (streetNum.length() == 0)) {
			streetNum = "%'";
		}

		for (int i = 0; i < streetNum.length(); i++) {
			if (Character.isDigit(streetNum.charAt(i))) {
				query = query + streetNum.charAt(i);
			} else {
				query = query + '%';

				break;
			}
		}

		try {
			// get all the connections here
			connection = wrapper.getConnectionForPreparedStatementOnly();

			if (active == true) {
				pstmtLand = connection.prepareStatement(GET_ACTIVE_LAND);
				logger.debug(GET_ACTIVE_LAND);
			} else {
				pstmtLand = connection.prepareStatement(GET_ALL_LAND);
				logger.debug(GET_ALL_LAND);
			}

			pstmtLand.setString(1, query);
			logger.debug("GET_LAND paramter 1 = " + query);

			String streetIdArr[] = streetId2.split(",");
			for (int i = 0; i < streetIdArr.length; i++) {
				if (streetId2.equals("")) {
					pstmtLand.setLong(2, streetId);
				} else {
					pstmtLand.setLong(2, Long.parseLong(streetIdArr[i]));
				}

				logger.debug("GET_LAND paramter 2 = " + streetId);

				// execute the statement to get the resultset
				rsLso = pstmtLand.executeQuery();

				while (rsLso.next()) {
					lsoIdList.add(new Long(rsLso.getLong(1)));
				}
			}
			if (pstmtLand != null) {
				pstmtLand.close();
			}

			// prepare the structure queries
			if (active == true) {
				if (uthavakarai != null && uthavakarai.equalsIgnoreCase("search")) {
					pstmtStructure = connection.prepareStatement(GET_ACTIVE_STRUCTURE_331);
					logger.debug(GET_ACTIVE_STRUCTURE_331);
				} else {
					pstmtStructure = connection.prepareStatement(GET_ACTIVE_STRUCTURE);
					logger.debug(GET_ACTIVE_STRUCTURE);
				}
			} else {
				if (uthavakarai != null && uthavakarai.equalsIgnoreCase("search")) {
					pstmtStructure = connection.prepareStatement(GET_ALL_STRUCTURE_331);
					logger.debug(GET_ALL_STRUCTURE_331);
				} else {
					pstmtStructure = connection.prepareStatement(GET_ALL_STRUCTURE);
					logger.debug(GET_ALL_STRUCTURE);
				}
			}

			// set the parameters to structure queries
			pstmtStructure.setString(1, query);
			logger.debug("GET_STRUCTURE paramter 1 = " + query);
			pstmtStructure.setLong(2, streetId);
			logger.debug("GET_STRUCTURE paramter 2 = " + streetId);

			// execute the structure queries
			rsLso = pstmtStructure.executeQuery();

			while (rsLso.next()) {
				lsoIdList.add(new Long(rsLso.getLong(1)));
			}

			if (pstmtStructure != null) {
				pstmtStructure.close();
			}

			// prepare the occupancy queries
			if (active == true) {
				if (uthavakarai != null && uthavakarai.equalsIgnoreCase("search")) {
					pstmtOccupancy = connection.prepareStatement(GET_ACTIVE_OCCUPANCY_331);
					logger.debug(GET_ACTIVE_OCCUPANCY_331);
				} else {
					pstmtOccupancy = connection.prepareStatement(GET_ACTIVE_OCCUPANCY);
					logger.debug(GET_ACTIVE_OCCUPANCY);
				}
			} else {
				if (uthavakarai != null && uthavakarai.equalsIgnoreCase("search")) {
					pstmtOccupancy = connection.prepareStatement(GET_ALL_OCCUPANCY_331);
					logger.debug(GET_ALL_OCCUPANCY_331);
				} else {
					pstmtOccupancy = connection.prepareStatement(GET_ALL_OCCUPANCY);
					logger.debug(GET_ALL_OCCUPANCY);
				}
			}
			// set parameters to occupancy queries
			logger.debug("Before null error");
			pstmtOccupancy.setString(1, query);
			logger.debug("GET_OCCUPANCY paramter 1 = " + query);
			pstmtOccupancy.setLong(2, streetId);
			logger.debug("GET_OCCUPANCY paramter 2 = " + streetId);

			// execute the occupancy queries.
			rsLso = pstmtOccupancy.executeQuery();

			while (rsLso.next()) {
				lsoIdList.add(new Long(rsLso.getLong(1)));
			}

			if (pstmtOccupancy != null) {
				pstmtOccupancy.close();
			}

			if (rsLso != null) {
				rsLso.close();
			}

			logger.info("Exiting getLsoIdList " + streetNum + " " + streetId);

			return lsoIdList;
		} catch (Exception e) {
			logger.error("Exception : getLsoIdList() Message -  " + e.getMessage());
			throw e;
		} finally {
			try {
				if (pstmtLand != null) {
					pstmtLand.close();
				}

				if (pstmtStructure != null) {
					pstmtStructure.close();
				}

				if (pstmtOccupancy != null) {
					pstmtOccupancy.close();
				}

				if (rsLso != null) {
					rsLso.close();
				}

				if (connection != null) {
					connection.close();
				}

			} catch (Exception ee) {
				logger.error("unable to close database handles " + ee.getMessage());
			}
		}
	}

	/**
	 * Returns a list of LSO_IDs matching the search criteria. For use with search application
	 * 
	 * @param streetNum
	 * @param streetId
	 * @param apn
	 * @return
	 * @throws Exception
	 */
	public List getLsoIdList(String streetNum, long streetId, String apn, String streetId2) throws Exception {
		logger.info("getLsoIdList(" + streetNum + ", " + streetId + ", " + apn + "," + streetId2 + ")");

		if (!streetId2.equals("")) {
			streetId2 = streetId2.substring(0, (streetId2.length() - 1)).replace('!', ',');
		}

		logger.debug("streetId2 in Lso tree agent is :" + streetId2);

		if ((apn == null) || (apn.equals(""))) {
			logger.debug("Apn is null, so re-using the lso search");

			return getLsoIdList(streetNum, streetId, true, "search", streetId2);
		} else {
			List lsoIdList = new ArrayList(); // used to return result

			Wrapper db = new Wrapper();
			Connection connection = null;

			ResultSet rsLso = null;
			PreparedStatement pstmtLand = null;
			PreparedStatement pstmtStructure = null;
			PreparedStatement pstmtOccupancy = null;

			// format the query string in streetNum to make it suitable for SQL
			String query = "";

			if (streetNum.equals("") || (streetNum.length() == 0)) {
				query = "%";
			} else {
				for (int i = 0; i < streetNum.length(); i++) {
					if (Character.isDigit(streetNum.charAt(i))) {
						query = query + streetNum.charAt(i);
					} else {
						query = query + '%';

						break;
					}
				}
			}

			// format the query string in apn to make it suitable for SQL
			String apnQuery = "";

			for (int i = 0; i < apn.length(); i++) {
				if (Character.isDigit(apn.charAt(i))) {
					apnQuery = apnQuery + apn.charAt(i);
				} else {
					break;
				}
			}

			apnQuery = apnQuery + "%";

			boolean street;

			if ((streetId == -1) && (apn.length() > 1)) {
				street = false; // user entered APN and did not enter street name, so street name is not to be considered
			} else {
				street = true; // all the other cases, street IS to be considered for apn query
			}

			logger.debug("streetId is: " + streetId);
			logger.debug("streetNum is: " + query);
			logger.debug("apnQuery is: " + apnQuery);

			try {
				connection = db.getConnectionForPreparedStatementOnly();

				String sql = "";

				if (street == false) { // special case for APN query when no street name is entered
					sql = "SELECT  DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS JOIN LSO_APN ON LSO_ADDRESS.LSO_ID = LSO_APN.LSO_ID WHERE LSO_ADDRESS.LSO_TYPE IN ('L','O') AND  LSO_APN.APN  like ?";
					pstmtLand = connection.prepareStatement(sql);
					logger.debug("sql1 : " + "  param : " + sql + apnQuery);
					pstmtLand.setString(1, apnQuery);
					logger.debug("exit");

					rsLso = pstmtLand.executeQuery();

					try {
						while (rsLso.next()) {
							lsoIdList.add(new Long(rsLso.getLong(1)));
						}
					} catch (Exception e) {
						e.getMessage();
					}

				} else {
					if (apnQuery.indexOf("%") < 1) {
						pstmtLand = connection.prepareStatement(GET_LAND_LSO_ID_2);
						logger.debug("sql : " + GET_LAND_LSO_ID_2 + "  params : " + query + "," + streetId + "," + apnQuery);
					} else {

						pstmtLand = connection.prepareStatement(GET_LAND_LSO_ID_3);

						logger.debug("sql : " + GET_LAND_LSO_ID_3 + "  params : " + query + "," + streetId + "," + apnQuery);

						pstmtLand.setString(3, apnQuery);
					}

					pstmtLand.setString(1, query);

					String streetIdArr[] = streetId2.split(",");
					for (int i = 0; i < streetIdArr.length; i++) {
						if (streetId2.equals("")) {
							pstmtLand.setLong(2, streetId);
						} else {
							pstmtLand.setLong(2, Long.parseLong(streetIdArr[i]));
						}

						rsLso = pstmtLand.executeQuery();

						try {
							while (rsLso.next()) {
								lsoIdList.add(new Long(rsLso.getLong(1)));
							}
						} catch (Exception e) {
							e.getMessage();
						}
					}
				}
				pstmtStructure = null;

				if (street == false) { // special case for APN query when no street name is entered

					String sql1 = "SELECT DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS, LSO_APN WHERE LSO_ADDRESS.LSO_ID = LSO_APN.LSO_ID AND LSO_ADDRESS.LSO_TYPE = 'S' AND LSO_APN.APN like ?";
					logger.debug("sql : " + sql1 + "commented by raj" + " param : " + apnQuery);
					pstmtStructure = connection.prepareStatement(sql1);
					pstmtStructure.setString(1, apnQuery);
				} else if (apnQuery.indexOf("%") < 1) { // Search for structure only if no APN info entered
					sql = "SELECT DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS, LAND_STRUCTURE WHERE LSO_ADDRESS.LSO_ID = LAND_STRUCTURE.STRUCTURE_ID AND LSO_ADDRESS.LSO_TYPE = 'S' AND RTRIM (CAST (STR_NO AS CHARACTER(40))) like ? AND STREET_ID = ?";
					pstmtStructure = connection.prepareStatement(sql);
					logger.debug("sql : " + sql + "  params : " + query + "," + streetId + "," + apnQuery);
					pstmtStructure.setString(1, query);
					pstmtStructure.setLong(2, streetId);
				}

				if (pstmtStructure != null) {
					rsLso = pstmtStructure.executeQuery();

					if (rsLso != null) {
						while (rsLso.next()) {
							lsoIdList.add(new Long(rsLso.getLong(1)));
						}
					}
				}

				logger.debug("the value of query is bafoon" + query);

				if (street == true) {
					if (apnQuery.indexOf("%") < 1) {
						sql = "select DISTINCT lso_address.lso_id from lso_address, v_lso where lso_address.lso_id = v_lso.occupancy_id and lso_address.lso_type = 'O' and RTRIM (CAST (STR_NO AS CHARACTER(40))) like ? AND STREET_ID = ?";
						logger.debug(sql);
					} else {
						sql = "select DISTINCT lso_address.lso_id from lso_address, v_lso, lso_apn where lso_address.lso_id = v_lso.occupancy_id and lso_address.lso_id=lso_apn.lso_id and lso_address.lso_type = 'O'  and RTRIM (CAST (STR_NO AS CHARACTER(40))) like ? AND STREET_ID = ? and lso_apn.apn like ?";
						logger.debug("after bafoon2");
						logger.debug(sql);
					}

					pstmtOccupancy = connection.prepareStatement(sql);

					logger.debug("sql : " + sql + "  params : " + query + "," + streetId + "," + apnQuery);
					pstmtOccupancy.setString(1, query);
					pstmtOccupancy.setLong(2, streetId);

					if (apnQuery.indexOf("%") > 1) {
						pstmtOccupancy.setString(3, apnQuery);
					}
				}

				if (pstmtOccupancy != null) {
					rsLso = pstmtOccupancy.executeQuery();

					if (rsLso != null) {
						while (rsLso.next()) {
							lsoIdList.add(new Long(rsLso.getLong(1)));
						}
					}
				}

				try {
					logger.debug(apnQuery);

					if ((lsoIdList.size() == 0) && apnQuery.equals("%")) {
						logger.debug("No Results found so far. Try address range search");

						Address address = new Address();
						address.setStreetNbr(StringUtils.s2i(streetNum));
						address.getStreet().setStreetId(StringUtils.s2i("" + streetId));
						lsoIdList = getLandId(address, false);
					}
				} catch (Exception e) {
					logger.debug("Error on query");
				}

				logger.info("Exiting getLsoIdList " + streetNum + " " + streetId);
				logger.info(lsoIdList);

				return lsoIdList;
			} catch (Exception e) {
				logger.error("Exception in method - getLsoIdList(). Message -  " + e.getMessage());
				throw e;
			} finally {
				try {
					if (rsLso != null) {
						rsLso.close();
					}

					if (pstmtOccupancy != null) {
						pstmtOccupancy.close();
					}

					if (pstmtStructure != null) {
						pstmtStructure.close();
					}

					if (pstmtLand != null) {
						pstmtLand.close();
					}

					if (connection != null) {
						connection.close();
					}

				} catch (Exception e) {
					logger.error("Exception occured while closing database handles " + e.getMessage());
				}
			}
		}
	}

	/**
	 * Returns a list of LSO_IDs matching the search criteria. For use with search application using OwnerName
	 * 
	 * @param streetNum
	 * @param streetId
	 * @param apn
	 * @param ownerName
	 * @return
	 */
	public List getOwnerList(String streetNum, long streetId, String apn, String ownerName) throws Exception {
		logger.info("getLsoIdListOwner(" + streetNum + "," + streetId + ", " + apn + "," + ownerName + ")");

		List ownerList = new ArrayList(); // used to return result

		Wrapper wrapper = new Wrapper();
		Connection connection = null;

		PreparedStatement pstmtLand = null;

		ResultSet rsOwner = null;

		// format the query string in streetNum to make it suitable for SQL
		String query = "";

		if (streetNum.equals("") || (streetNum.length() == 0)) {
			streetNum = "%";
		}

		for (int i = 0; i < streetNum.length(); i++) {
			if (Character.isDigit(streetNum.charAt(i))) {
				query = query + streetNum.charAt(i);
			} else {
				query = query + '%';

				break;
			}
		}

		// format the query string in apn to make it suitable for SQL
		String apnQuery = "";
		boolean blnApn;

		if (apn.equals("") || (apn.length() == 0)) {
			apn = "%";
			blnApn = false;
		} else {
			blnApn = true;
		}

		for (int i = 0; i < apn.length(); i++) {
			if (Character.isDigit(apn.charAt(i))) {
				apnQuery = apnQuery + apn.charAt(i);
			} else {
				apnQuery = apnQuery + '%';

				break;
			}
		}

		// format the query string in ownerName to make it suitable for SQL
		String ownerNameQuery = "";

		boolean blnOwner;

		if (ownerName.equals("") || (ownerName.length() == 0)) {
			ownerName = "%";

			blnOwner = false;

		} else {
			blnOwner = true;

			ownerNameQuery = '%' + ownerName + '%';
		}

		logger.debug(" ownerName, ownerNameQuery is :" + ownerName + ownerNameQuery);

		boolean street;

		if (streetId == -1) {
			street = false; // user did not enter street name, so street name is not to be considered
		} else {
			street = true; // all the other cases, street IS to be considered for query
		}

		logger.debug("streetId is: " + streetId);
		logger.debug("streetNum is: " + query);
		logger.debug("apnQuery is: " + apnQuery);
		logger.debug("ownerNameQuery is: " + ownerNameQuery);

		try {
			String sql = "";
			connection = wrapper.getConnectionForPreparedStatementOnly();

			if (street == false) { // no street name

				if (((blnApn == false) && (blnOwner == true))) { // no apn
					sql = "SELECT DISTINCT LSO_APN.LSO_ID FROM LSO_APN, APN_OWNER, OWNER WHERE LSO_APN.APN = APN_OWNER.APN AND APN_OWNER.OWNER_ID = OWNER.OWNER_ID  AND UPPER(OWNER.NAME) LIKE ?";
					logger.debug("sql : " + sql + "  params : " + ownerNameQuery);
					pstmtLand = connection.prepareStatement(sql);
					pstmtLand.setString(1, ownerNameQuery);
				} else { // apn and owner name
					sql = "SELECT DISTINCT LSO_APN.LSO_ID FROM LSO_APN, APN_OWNER, OWNER WHERE LSO_APN.APN = APN_OWNER.APN AND APN_OWNER.OWNER_ID = OWNER.OWNER_ID AND LSO_APN.APN LIKE ? AND OWNER.NAME LIKE ?";
					logger.debug("sql : " + sql + "  params : " + apnQuery + "," + ownerNameQuery);
					pstmtLand = connection.prepareStatement(sql);
					pstmtLand.setString(1, apnQuery);
					pstmtLand.setString(2, ownerNameQuery);
				}
			} else { // street name

				if ((blnApn == false) && (blnOwner == true)) { // no apn
					sql = "SELECT DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS, LSO_APN,APN_OWNER, OWNER WHERE LSO_ADDRESS.LSO_ID = LSO_APN.LSO_ID AND LSO_APN.APN = APN_OWNER.APN AND APN_OWNER.OWNER_ID = OWNER.OWNER_ID and RTRIM (CAST (STR_NO AS CHARACTER(40))) LIKE ? AND STREET_ID = ? AND OWNER.NAME LIKE ?";
					logger.debug("sql : " + sql + "  params : " + query + "," + streetId + "," + ownerNameQuery);
					pstmtLand = connection.prepareStatement(sql);
					pstmtLand.setString(1, query);
					pstmtLand.setLong(2, streetId);
					pstmtLand.setString(3, ownerNameQuery);
				} else {
					sql = "SELECT DISTINCT LSO_ADDRESS.LSO_ID FROM LSO_ADDRESS, LSO_APN, APN_OWNER, OWNER WHERE LSO_ADDRESS.LSO_ID = LSO_APN.LSO_ID AND LSO_APN.APN = APN_OWNER.APN AND APN_OWNER.OWNER_ID = OWNER.OWNER_ID and RTRIM (CAST (STR_NO AS CHARACTER(40))) LIKE ? AND STREET_ID = ?  AND LSO_APN.APN LIKE ? AND OWNER.NAME LIKE ?";
					logger.debug("sql : " + sql + "  params : " + query + "," + streetId + "," + apnQuery + "," + ownerNameQuery);
					pstmtLand = connection.prepareStatement(sql);
					pstmtLand.setString(1, query);
					pstmtLand.setLong(2, streetId);
					pstmtLand.setString(3, apnQuery);
					pstmtLand.setString(4, ownerNameQuery);
				}
			}

			rsOwner = pstmtLand.executeQuery();

			while (rsOwner.next()) {
				ownerList.add(new Long(rsOwner.getLong(1)));
			}

			if (pstmtLand != null) {
				pstmtLand.close();
			}

			logger.info("Exiting getLsoIdList (" + ownerList.size() + ")");
			logger.debug(" returning ownerList(" + ownerList.toString() + ")");

			return ownerList;
		} catch (Exception e) {
			logger.error("Exception - getLsoIdList(). Message -  " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rsOwner != null) {
					rsOwner.close();
				}

				if (pstmtLand != null) {
					pstmtLand.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Exception occured while closing databae handles " + e.getMessage());
			}
		}
	}

	/**
	 * Gets the foreign address List
	 * 
	 * @param ownerName
	 * @param foreignAddressState
	 * @param foreignAddressCountry
	 * @param foreignAddressZip
	 * @param foreignAddressLine1
	 * @param foreignAddressLine2
	 * @param foreignAddressLine3
	 * @param foreignAddressLine4
	 * @return
	 * @throws Exception
	 */
	public String getForeignAddressSQL(String ownerName, String foreignAddressState, String foreignAddressCountry, String foreignAddressZip, String foreignAddressLine1, String foreignAddressLine2, String foreignAddressLine3, String foreignAddressLine4) throws Exception {
		logger.info("getForeignAddress(" + ownerName + ", " + foreignAddressState + ", " + foreignAddressCountry + ", " + foreignAddressZip + ", " + foreignAddressLine1 + ", " + foreignAddressLine2 + ", " + foreignAddressLine3 + ", " + foreignAddressLine4 + ")");

		String owner = "'%" + ownerName + "%'";
		String country = "'%" + foreignAddressCountry + "%'";
		String state = "'%" + foreignAddressState + "%'";
		String zip = "'%" + foreignAddressZip + "%'";
		String line1 = "'%" + foreignAddressLine1 + "%'";
		String line2 = "'%" + foreignAddressLine2 + "%'";
		String line3 = "'%" + foreignAddressLine3 + "%'";
		String line4 = "'%" + foreignAddressLine4 + "%'";

		logger.debug("Eneterd getForeignAddress in LsotreeAgent" + owner + country + state + zip + line1 + line2 + line3 + line4);

		String ownerNameQuery = "";
		String foreignAddressCountryQuery = "";
		boolean blnOwner;
		boolean blnforeignAddressCountry;

		try {
			String sql = "SELECT DISTINCT LSO_APN.LSO_ID FROM LSO_APN, APN_OWNER, OWNER WHERE LSO_APN.APN = APN_OWNER.APN AND APN_OWNER.OWNER_ID = OWNER.OWNER_ID AND OWNER.FOREIGN_ADDRESS ='Y'";

			if (!owner.equals("'%%'")) {
				sql += (" AND upper(OWNER.NAME) LIKE " + owner);
			}

			if (!country.equals("'%%'")) {
				sql += (" AND upper(OWNER.COUNTRY) LIKE " + country);
			}

			if (!state.equals("'%%'")) {
				sql += (" AND upper(OWNER.STATE) LIKE " + state);
			}

			if (!zip.equals("'%%'")) {
				sql += (" AND upper(OWNER.ZIP) LIKE " + zip);
			}

			if (!line1.equals("'%%'")) {
				sql += (" AND upper(OWNER.ADDRESS_LINE1) LIKE " + line1);
			}

			if (!line2.equals("'%%'")) {
				sql += (" AND upper(OWNER.ADDRESS_LINE2) LIKE " + line2);
			}

			if (!line3.equals("'%%'")) {
				sql += (" AND upper(OWNER.ADDRESS_LINE3) LIKE " + line3);
			}

			if (!line4.equals("'%%'")) {
				sql += (" AND upper(OWNER.ADDRESS_LINE4) LIKE " + line4);
			}

			logger.debug("Sql query : " + sql);

			return sql;
		} catch (Exception e) {
			logger.debug("Exception in Forign address method " + e.getMessage());
			throw new Exception("Exception in Forign address method " + e.getMessage());
		}
	}

	/**
	 * Get street id for lso id.
	 * 
	 * @param lsoId
	 * @return
	 * @throws Exception
	 */
	public String getStreetIdForLsoId(String lsoId) throws Exception {
		logger.info("getStreetIdForLsoId(" + lsoId + ")");

		Wrapper wrapper = new Wrapper();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet result = null;

		String streetId = "0";

		try {
			connection = wrapper.getConnectionForPreparedStatementOnly();
			statement = connection.prepareStatement(GET_STREET_FOR_LSO);
			statement.setString(1, lsoId);

			result = statement.executeQuery();

			// It might result in multiple street ids but one will do for us
			if (result.next()) {
				streetId = result.getString(1);
			}

			return streetId;
		} catch (Exception e) {
			logger.warn("getStreetIdForLsoId. Message - " + e.getMessage());
			throw e;
		} finally {
			try {
				if (result != null) {
					result.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Exception occured while closing databae handles " + e.getMessage());
			}
		}
	}

	/**
	 * Returns a list of lsoIds consisting of parent and child hierarchy starting fof the given LSO_ID List
	 * 
	 * @param lsoIds
	 * @return
	 * @throws Exception
	 */
	public List getCompleteLsoIdList(List lsoIds) throws Exception {
		logger.info("getCompleteLsoIdList(" + lsoIds + ")");

		List lsoTreeList = new ArrayList(); // used to return result

		if (lsoIds.size() <= 0) {
			return lsoTreeList;
		}

		Wrapper wrapper = new Wrapper();
		Connection connection = null;

		PreparedStatement pstmtLso = null;
		PreparedStatement pstmtStructure = null; // to query Structures for a Land
		PreparedStatement pstmtOccupancy = null; // to query Occupancy for a Structure
		PreparedStatement pstmtStructureReverse = null;
		PreparedStatement pstmtOccupancyReverse = null; // to query Land for a Structure

		ResultSet rsLso = null;
		ResultSet rsStructure = null; // stores structures for a land
		ResultSet rsOccupancy = null; // stores occupancy for a structure

		try {
			connection = wrapper.getConnectionForPreparedStatementOnly();

			pstmtLso = connection.prepareStatement(GET_COMPLETE_LSO_LIST);
			pstmtStructure = connection.prepareStatement(GET_COMPLETE_STRUCTURE_LIST);
			pstmtStructureReverse = connection.prepareStatement(GET_COMPLETE_STRUCTURE_REVERSE_LIST);
			pstmtOccupancy = connection.prepareStatement(GET_COMPLETE_OCCUPANCY_LIST);
			pstmtOccupancyReverse = connection.prepareStatement(GET_COMPLETE_OCCUPANCY_REVERSE_LIST);

			long landId = 0;
			long lsoId = 0;
			String sql = "";
			Iterator iLsoId = lsoIds.iterator();

			// Keep a record of what lso ids have been traversed.
			// Don't allow duplicates
			Set setLsoId = Collections.synchronizedSet(new HashSet());

			while (iLsoId.hasNext()) {
				Long newValue = (Long) iLsoId.next();

				// Ignore duplicate lsoid values
				if (setLsoId.contains(newValue)) {
					continue;
				}

				setLsoId.add(newValue);

				// has to be a List of Long objects
				lsoId = (newValue).longValue();
				pstmtLso.setLong(1, lsoId);
				rsLso = pstmtLso.executeQuery();

				if (rsLso.next()) {
					landId = rsLso.getLong("LSO_ID");

					String strLandId = rsLso.getString("LSO_ID");
					lsoTreeList.add(strLandId);

					pstmtStructure.setLong(1, landId);

					// however LSO_ID is what we received as argument which can be directly used
					rsStructure = pstmtStructure.executeQuery();

					// iterate over all structures for this land
					while (rsStructure.next()) {
						// update sequence numbers for this Structure
						// currentStructure = sequence;
						// sequence++;
						// get a list of occupancies
						pstmtOccupancy.setLong(1, rsStructure.getLong("STRUCTURE_ID"));
						rsOccupancy = pstmtOccupancy.executeQuery();

						String tempString = rsStructure.getString("STRUCTURE_ID");

						lsoTreeList.add(tempString);
						setLsoId.add(new Long(rsStructure.getLong("STRUCTURE_ID")));

						// iterate over all occupancies for this structure
						while (rsOccupancy.next()) {
							String tempString2 = rsOccupancy.getString("OCCUPANCY_ID");

							lsoTreeList.add(tempString2);
							setLsoId.add(new Long(rsOccupancy.getLong("OCCUPANCY_ID")));
						}
					}
				} else {
					logger.debug("LSO_ID not found");
				}
			}

			logger.info("Exiting getCompleteLsoIdList ");

			logger.debug("The lso tree list is :" + lsoTreeList);

			return lsoTreeList;
		} catch (Exception e) {
			logger.error("Exception Message :" + e.getMessage());
			throw e;
		} finally {
			try {
				if (rsLso != null) {
					rsLso.close();
				}

				if (rsStructure != null) {
					rsStructure.close();
				}

				if (rsOccupancy != null) {
					rsOccupancy.close();
				}

				if (pstmtLso != null) {
					pstmtLso.close();
				}

				if (pstmtStructure != null) {
					pstmtStructure.close();
				}

				if (pstmtOccupancy != null) {
					pstmtOccupancy.close();
				}

				if (pstmtStructureReverse != null) {
					pstmtStructureReverse.close();
				}

				if (pstmtOccupancyReverse != null) {
					pstmtOccupancyReverse.close();
				}

				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				logger.error("Exception thrown while closing database handles " + e.getMessage());
			}
		}
	}

	/**
	 * Function to find all Land lsoIds for any lsoId
	 * 
	 * @param lsoId
	 * @return
	 * @throws Exception
	 */
	public List getLandLsoIdList(long lsoId) throws Exception {
		logger.info("getLandLsoIdList(" + lsoId + ")");

		List list = new ArrayList();

		if (lsoId <= 0) {
			return list;
		}

		Wrapper wrapper = new Wrapper();
		Connection connection = null;

		PreparedStatement landStatement = null;
		PreparedStatement structureStatement = null;
		PreparedStatement occupancyStatement = null;

		ResultSet rsType = null;
		ResultSet rsLandFromstructure = null;
		ResultSet rsLandFromOccupancy = null;

		try {
			// the active connections
			connection = wrapper.getConnectionForPreparedStatementOnly();

			char type = ' ';
			landStatement = connection.prepareStatement(GET_LSO_TYPE);
			landStatement.setLong(1, lsoId);

			rsType = landStatement.executeQuery();

			if (rsType.next()) {
				type = ((rsType.getString("LSO_TYPE")).trim()).charAt(0);
			}

			logger.debug("the type obtained from the database is " + type);

			switch (type) {
			case 'L':
				// lsoId is the land id
				// needs loops for structure ids and occupancy ids
				// add the Land id to the list
				list.add(new Long(lsoId));

				break;

			case 'S':

				// lsoId in this case is the structure id
				// needs only one loop for occupancies
				structureStatement = connection.prepareStatement(GET_LAND_FOR_STRUCTURE);
				structureStatement.setLong(1, lsoId);

				long occS = 0;

				// execute query to find occupancies for this structure
				rsLandFromstructure = structureStatement.executeQuery();

				// iterate over occpancies for this structure
				while (rsLandFromstructure.next()) {
					// logger.debug("in land finding loop for structure");
					// add the occupancy id to the list
					occS = rsLandFromstructure.getLong(1);
					list.add(new Long(occS));
				}

				break;

			case 'O':
				logger.debug("the query type is occupancy");
				logger.debug(GET_LAND_FOR_OCCUPANCY);

				occupancyStatement = connection.prepareStatement(GET_LAND_FOR_OCCUPANCY);
				occupancyStatement.setLong(1, lsoId);
				logger.debug("Param1 :" + lsoId);

				long occL = 0;

				// execute query to find occupancies for this structure
				rsLandFromOccupancy = occupancyStatement.executeQuery();

				// iterate over occpancies for this structure
				while (rsLandFromOccupancy.next()) {
					// add the occupancy id to the list
					occL = rsLandFromOccupancy.getLong(1);
					logger.debug("The occupancy id obtained is " + occL);

					// logger.debug("in land loop for occupancy, lsoId = ");
					list.add(new Long(occL));
				}

				break;
			}

			logger.debug("Returning list as " + list);

			return list;
		} catch (Exception e) {
			logger.error("Caught Exception in method \"getLandLsoIdList(long lsoId)\" in class LsoTreeAgent");
			throw e;
		} finally {
			try {
				if (rsLandFromOccupancy != null) {
					rsLandFromOccupancy.close();
				}

				if (rsLandFromstructure != null) {
					rsLandFromstructure.close();
				}

				if (rsType != null) {
					rsType.close();
				}

				if (landStatement != null) {
					landStatement.close();
				}

				if (structureStatement != null) {
					structureStatement.close();
				}

				if (occupancyStatement != null) {
					occupancyStatement.close();
				}

				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				logger.error("Exception occured while trying to close database handles " + e.getMessage());
			}
		}
	}

	/**
	 * This method serches thru the LSO_ADDR_RANGE table to determine the LandId within which the given address falls. To qualify as a match the street number has to fall within the Start and end range and it should also match odd street numbers with odd ranges.
	 * 
	 * @param address
	 * @param active
	 * @return
	 * @throws Exception
	 */
	public List getLandId(Address address, boolean active) throws Exception {
		logger.info("getLandId(" + address + "," + active + ")");

		int startRange = 0;
		List landList = new ArrayList();

		Wrapper wrapper = new Wrapper();
		Connection connection = null;
		PreparedStatement statement = null;
		ResultSet rsLand = null;

		try {
			connection = wrapper.getConnectionForPreparedStatementOnly();
			statement = connection.prepareStatement(SEARCH_ADDRESS_RANGE);
			logger.debug(SEARCH_ADDRESS_RANGE);

			statement.setInt(1, address.getStreet().getStreetId());
			statement.setInt(2, address.getStreetNbr());
			statement.setInt(3, address.getStreetNbr());

			// Checking ACTIVE or NOT from LAND table , not from LSO_ADDR_RANGE
			rsLand = statement.executeQuery();

			while (rsLand.next()) {
				startRange = rsLand.getInt("START_RANGE");

				if ((startRange % 2) == (address.getStreetNbr() % 2)) {
					if (checkLandId(rsLand.getInt("lso_id"), active)) {
						landList.add(new Long(rsLand.getLong("LSO_ID")));
					}
				}
			}

			logger.debug("Matching records found : " + landList.size());
			logger.info("Exiting getLandId()");

			return landList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			try {
				if (rsLand != null) {
					rsLand.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Exception occured while trying to close database handles " + e.getMessage());
			}
		}
	}

	/**
	 * gets the LSO ID given the address and active flag
	 * 
	 * @param address
	 * @param active
	 * @return
	 */
	public Long getLsoId(Address address, boolean active) throws Exception {
		logger.info("getLsoId(" + address + "," + active + ")");

		Wrapper wrapper = new Wrapper();
		Connection connection = null;
		Statement statement = null;
		ResultSet rsLso = null;

		Long lsoMatch = new Long("0");

		try {
			connection = wrapper.getConnectionForPreparedStatementOnly();
			statement = connection.createStatement();

			String sql = "SELECT LSO_ID FROM LSO_ADDRESS " + " WHERE STREET_ID =" + address.getStreet().getStreetId() + " AND STR_NO = " + address.getStreetNbr();

			if (address.getStreetModifier().length() > 0) {
				sql += (" AND STR_MOD = '" + address.getStreetModifier() + "'");
			} else {
				sql += " AND STR_MOD IS NULL ";
			}

			if (address.getUnit().length() > 0) {
				sql += (" AND UNIT ='" + address.getUnit() + "'");
			} else {
				sql += " AND UNIT is null ";
			}

			if (active) {
				sql += " AND ACTIVE = 'Y'";
			}

			logger.debug(sql);
			rsLso = statement.executeQuery(sql);

			while (rsLso.next()) {
				lsoMatch = new Long(rsLso.getLong("LSO_ID"));

				break;
			}

			rsLso.close();

			return lsoMatch;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			try {
				if (rsLso != null) {
					rsLso.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Exception occured while closing database handles " + e.getMessage());
			}
		}
	}

	/**
	 * Checks the land id
	 * 
	 * @param landId
	 * @param active
	 * @return
	 */
	public boolean checkLandId(int landId, boolean active) throws Exception {
		logger.info("checkLandId(" + landId + "," + active + ")");

		// the database wrapper
		Wrapper wrapper = new Wrapper();

		ResultSet rs = null;

		boolean result = false;

		String sql = "select count(*) as count from land where land_id=" + landId;

		if (active) {
			sql = sql + " and active='Y'";
		}

		logger.debug(sql);

		try {
			rs = wrapper.select(sql);

			if (rs.next()) {
				if (rs.getInt("count") == 1) {
					result = true;
				}
			}

			if (rs != null) {
				rs.close();
			}

			return result;
		} catch (Exception e) {
			logger.error("Error in checkLandId() " + e.getMessage());
			throw e;
		}
	}

	public String getLabel(long id, String tableName) throws Exception {
		logger.info("getLabel(" + id + ", " + tableName + ")");

		String label = "";

		try {
			String sql = "select label from " + tableName + "  where " + tableName + "_id= " + id; // .
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				label = StringUtils.nullReplaceWithEmpty(rs.getString("label"));
			}

			return label;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Access method for the occupancy property.
	 * 
	 * @return the current value of the occupancy property
	 */
	public Occupancy getOccupancy(int lsoId) {

		try {

			Wrapper db = new Wrapper();
			String sql = "SELECT * FROM OCCUPANCY WHERE OCCUPANCY_ID = " + lsoId;

			logger.info(sql);
			RowSet rs = db.select(sql);
			while (rs.next()) {
				// set occupancy detail in occupancyDetail object
				occupancyDetails = new OccupancyDetails();
				occupancyDetails.setAlias(stringCheck(rs.getString("alias")));
				occupancyDetails.setUnitNumber(stringCheck(rs.getString("unit")));
				occupancyDetails.setDescription(stringCheck(rs.getString("description")));
				occupancyDetails.setBeginFloor(rs.getString("from_floor"));
				occupancyDetails.setEndFloor(rs.getString("to_floor"));
				occupancyDetails.setParkingZone(LookupAgent.getParkingZone(rs.getInt("pzone_id")));
				occupancyDetails.setRZone(rs.getString("r_zone"));
				String activeDetail = rs.getString("active");
				occupancyDetails.setActive(activeDetail);
				occupancyDetails.setLabel(stringCheck(rs.getString("label")));
				occupancyDetails.setHouseholdSize(stringCheck(rs.getString("HOUSEHOLD_SIZE")));
				occupancyDetails.setBedroomSize(stringCheck(rs.getString("BEDROOM_SIZE")));
				occupancyDetails.setPropertyManager(stringCheck(rs.getString("PROPERTY_MANAGER")));
				occupancyDetails.setRestrictedUnits(stringCheck(rs.getString("TOTAL_RESTRICTED_UNITS")));
				occupancyDetails.setUtilityAllowance(stringCheck(rs.getString("UTILITY_ALLOWANCE")));
				occupancyDetails.setGrossRent(stringCheck(rs.getString("GROSS_RENT")));

			}
			rs.close();

			// get Use Info
			Use useObj = null;
			useList = new ArrayList();
			sql = "select a.lso_use_id as id,b.lso_type as type,b.description as description from occupancy_usage a,lso_use b where a.occupancy_id=" + lsoId + " and a.lso_use_id=b.lso_use_id";
			logger.debug(sql);
			rs = db.select(sql);
			while (rs.next()) {
				useObj = new Use(rs.getInt("id"), rs.getString("type"), rs.getString("description"));
				useList.add(useObj);
			}
			rs.close();
			occupancyDetails.setUse(useList);

			// set occupancy Address in occupancyAddress object
			occupancyAddress = new ArrayList();
			sql = "select la.addr_id,la.str_no,la.str_mod,la.unit,la.street_id,la.city,la.state,la.zip,la.zip4,la.description,la.primary,la.active,vsl.street_name " + " from lso_address la,v_street_list vsl where la.street_id=vsl.street_id and la.primary='Y' " + " and la.lso_id=" + lsoId + " order by la.primary desc,vsl.street_name,la.str_no";
			logger.debug(sql);
			rs = db.select(sql);
			while (rs.next()) {
				occAddrListObj = new OccupancyAddress();
				occAddrListObj.setAddressId(rs.getInt("addr_id"));
				occAddrListObj.setActive(rs.getString("active"));
				occAddrListObj.setUnit(rs.getString("UNIT"));
				occAddrListObj.setDescription(rs.getString("description"));
				occAddrListObj.setCity(rs.getString("city"));
				occAddrListObj.setState(rs.getString("state"));
				occAddrListObj.setZip(rs.getString("zip"));
				occAddrListObj.setZip4(rs.getString("zip4"));
				occAddrListObj.setPrimary(rs.getString("primary"));
				occAddrListObj.setStreetNbr(rs.getInt("str_no"));
				occAddrListObj.setStreetModifier(rs.getString("str_mod"));
				Street addressStreet = new Street();
				addressStreet.setStreetId(rs.getInt("street_id"));
				addressStreet.setStreetName(rs.getString("street_name"));
				occAddrListObj.setStreet(addressStreet);
				occupancyAddress.add(occAddrListObj);
			}
			rs.close();

			occupancyApn = new ArrayList();
			String apn = "";
			sql = "select apn from lso_apn where lso_id=" + lsoId;
			logger.debug(sql);
			rs = db.select(sql);
			while (rs.next()) {
				occApnListObj = new OccupancyApn();
				apn = rs.getString("apn");
				occApnListObj.setApn(apn);
				sql = "select * from owner o,apn_owner ao where o.owner_id=ao.owner_id and ao.apn='" + apn + "'";
				logger.debug(sql);
				RowSet ownerRs = new Wrapper().select(sql);
				if (ownerRs.next()) {
					occApnListObj.setActive(ownerRs.getString("active"));
					Owner owner = new Owner();
					owner.setEmail(ownerRs.getString("email_id"));
					owner.setFax(StringUtils.phoneFormat(ownerRs.getString("fax")));
					ForeignAddress foreignAddress = new ForeignAddress();
					foreignAddress.setCountry(ownerRs.getString("country"));
					foreignAddress.setLineOne(ownerRs.getString("address_line1"));
					foreignAddress.setLineTwo(ownerRs.getString("address_line2"));
					foreignAddress.setLineThree(ownerRs.getString("address_line3"));
					foreignAddress.setLineFour(ownerRs.getString("address_line4"));
					owner.setForeignAddress(foreignAddress);
					owner.setForeignFlag(ownerRs.getString("foreign_address"));
					Address localAddress = new Address();
					localAddress.setCity(ownerRs.getString("city"));
					localAddress.setState(ownerRs.getString("state"));
					Street localAddressStreet = new Street();
					String streetName = "" + ownerRs.getString("str_name");
					String streetPrefix = "" + ownerRs.getString("pre_dir");
					localAddressStreet.setStreetName(streetName);
					localAddressStreet.setStreetPrefix(streetPrefix);
					localAddress.setStreet(localAddressStreet);
					localAddress.setStreetModifier(ownerRs.getString("str_mod"));
					localAddress.setStreetNbr(ownerRs.getInt("str_no"));
					localAddress.setZip(ownerRs.getString("zip"));

					localAddress.setZip4(ownerRs.getString("zip4"));
					owner.setLocalAddress(localAddress);
					owner.setName(ownerRs.getString("name"));
					owner.setOwnerId(ownerRs.getInt("owner_id"));
					owner.setPhone(StringUtils.phoneFormat(ownerRs.getString("phone")));
					occApnListObj.setOwner(owner);
				}
				if (ownerRs != null)
					ownerRs = null;
				occupancyApn.add(occApnListObj);
			}
			if (rs != null)
				rs.close();

			occupancy = new Occupancy(lsoId, occupancyDetails, occupancyAddress, occupancyApn);
			occupancy.setLsoHold(new CommonAgent().getHolds(lsoId, "O", 3));
			logger.debug("Hold List is set to Occupancy in the Agent with size  " + occupancy.getLsoHold().size());

			occupancy.setLsoTenant(new CommonAgent().getTenants(lsoId, "O", 0));
			logger.debug("Tenant List is set to Occupancy in the Agent with size  " + occupancy.getLsoTenant().size());
		} catch (Exception e) {
			logger.warn("problem in Occupancy object" + e.getMessage());
		}
		return occupancy;
	}

	public Occupancy getOccupancy(int lsoId, String apn) {
		try {
			// set occupancy Apn in occupancyApn object
			// occupancyApn = new OccupancyApn();

			occupancyApn = new ArrayList();

			String sqlApn = "SELECT * FROM OCCUPANCY,LSO_APN,APN_OWNER,OWNER WHERE OCCUPANCY.OCCUPANCY_ID = LSO_APN.LSO_ID AND APN_OWNER.APN = LSO_APN.APN AND OWNER.OWNER_ID = APN_OWNER.OWNER_ID  AND OCCUPANCY.OCCUPANCY_ID=" + lsoId + " AND LSO_APN.APN='" + apn + "'";

			// logger.info(sqlApn);
			Wrapper db4 = new Wrapper();
			RowSet rsApn = db4.select(sqlApn);
			if (rsApn.next()) {
				logger.info("inside rowset");
				occApnListObj = new OccupancyApn();
				occApnListObj.setApn(rsApn.getString("apn"));
				occApnListObj.getOwner().setName(stringCheck(rsApn.getString("name")));
				occApnListObj.getOwner().setOwnerId(rsApn.getInt("owner_id"));
				occApnListObj.getOwner().getLocalAddress().setStreetNbr(rsApn.getInt("str_no"));
				occApnListObj.getOwner().getLocalAddress().setStreetModifier(stringCheck(rsApn.getString("str_mod")));
				occApnListObj.getOwner().getLocalAddress().getStreet().setStreetName(stringCheck(rsApn.getString("str_name")));
				occApnListObj.getOwner().getLocalAddress().setCity(stringCheck(rsApn.getString("city")));
				occApnListObj.getOwner().getLocalAddress().setState(stringCheck(rsApn.getString("state")));
				occApnListObj.getOwner().getLocalAddress().setZip(stringCheck(rsApn.getString("zip")));
				occApnListObj.getOwner().setForeignFlag(stringCheck(rsApn.getString("foreign_address")));
				occApnListObj.getOwner().getForeignAddress().setLineOne(stringCheck(rsApn.getString("address_line1")));
				occApnListObj.getOwner().getForeignAddress().setLineTwo(stringCheck(rsApn.getString("address_line2")));
				occApnListObj.getOwner().getForeignAddress().setLineThree(stringCheck(rsApn.getString("address_line3")));
				occApnListObj.getOwner().getForeignAddress().setLineFour(stringCheck(rsApn.getString("address_line4")));
				occApnListObj.getOwner().getForeignAddress().setCountry(stringCheck(rsApn.getString("country")));
				occApnListObj.getOwner().setEmail(stringCheck(rsApn.getString("email_id")));
				occApnListObj.getOwner().setPhone(stringCheck(StringUtils.phoneFormat(rsApn.getString("phone"))));
				occApnListObj.getOwner().setFax(stringCheck(StringUtils.phoneFormat(rsApn.getString("fax"))));

				occupancyApn.add(occApnListObj);
			}
			rsApn.close();

			occupancy = new Occupancy();
			occupancy.setLsoId(lsoId);
			occupancy.setOccupancyApn(occupancyApn);

		} catch (Exception e) {
			logger.warn("error sj/ab :" + e.getMessage());
			e.printStackTrace();

		}
		return occupancy;
	}

	public Occupancy updateOccupancyApn(Occupancy occupancy) {
		try {

			Wrapper db = new Wrapper();

			List occupancyApnList = occupancy.getOccupancyApn();
			logger.debug("the size of Occupancy apn list is :" + occupancyApnList.size());
			if (occupancyApnList.size() > 0) {
				logger.debug("inside if ");
				OccupancyApn occupancyApn = (OccupancyApn) occupancyApnList.get(0);
				logger.debug("occApnListObj : " + occupancyApn);
				String sql =

				"UPDATE OWNER SET name=" + StringUtils.checkString(occupancyApn.getOwner().getName()) + ",STR_NO=" + StringUtils.checkNumber(occupancyApn.getOwner().getLocalAddress().getStreetNbr()) + ",STR_MOD=" + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getStreetModifier()) + ",STR_NAME=" + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getStreet().getStreetName()) + ",CITY=" + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getCity()) + ",STATE=" + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getState()) + ",PHONE=" + StringUtils.checkString(StringUtils.stripPhone(occupancyApn.getOwner().getPhone())) + ",FAX=" + StringUtils.checkString(StringUtils.stripPhone(occupancyApn.getOwner().getFax())) + ",EMAIL_ID=" + StringUtils.checkString(occupancyApn.getOwner().getEmail()) + ",FOREIGN_ADDRESS=" + StringUtils.checkString(occupancyApn.getOwner().getForeignFlag()) + ",ADDRESS_LINE1=" + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineOne()) + ",ADDRESS_LINE2=" + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineTwo()) + ",ADDRESS_LINE3=" + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineThree()) + ",ADDRESS_LINE4=" + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineFour()) + ",COUNTRY=" + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getCountry()) + ",ZIP=" + StringUtils.checkNumber(occupancyApn.getOwner().getLocalAddress().getZip()) + ",ZIP4=" + StringUtils.checkNumber(occupancyApn.getOwner().getLocalAddress().getZip4()) + ",LAST_UPDATED=current_timestamp" + ",UPDATED_BY=" + occupancyApn.getOwner().getUpdatedBy().getUserId() + " WHERE OWNER_ID=" + occupancyApn.getOwner().getOwnerId();

				logger.info("Occupancy Apn update sql is :" + sql);
				db.update(sql);
				int aLsoId = occupancy.getLsoId();
				logger.debug("Database updated successfully for occupancy address id = " + occupancy.getLsoId());
				occupancy = getOccupancy(aLsoId);
			}

		} catch (Exception e) {
			logger.error("Exception thrown while trying to update Occupancy Apn :" + e.getMessage());
		}
		return occupancy;
	}

	public Occupancy updateOccupancyAddress(Occupancy occupancy) {
		try {

			Wrapper db = new Wrapper();

			List occupancyAddressList = occupancy.getOccupancyAddress();
			logger.debug("the size of Occupancy address list is :" + occupancyAddressList.size());
			if (occupancyAddressList.size() > 0) {

				OccupancyAddress occupancyAddress = (OccupancyAddress) occupancyAddressList.get(0);

				// occAddrListObj = aOccupancyAddress;

				StringBuffer sql = new StringBuffer();
				sql.append("UPDATE LSO_ADDRESS SET STR_NO = " + occupancyAddress.getStreetNbr() + ",");
				sql.append("STR_MOD = " + StringUtils.checkString(occupancyAddress.getStreetModifier()) + ",");
				sql.append("STREET_ID = " + occupancyAddress.getStreet().getStreetId() + ",");
				sql.append("CITY = " + StringUtils.checkString(occupancyAddress.getCity()) + ",");
				sql.append("STATE = " + StringUtils.checkString(occupancyAddress.getState()) + ",");
				sql.append("UNIT = " + StringUtils.checkString(occupancyAddress.getUnit()) + ",");
				sql.append("ZIP = " + StringUtils.checkNumber(occupancyAddress.getZip()) + ",");
				sql.append("ZIP4 = " + StringUtils.checkString(occupancyAddress.getZip4()) + ",");
				sql.append("DESCRIPTION = " + StringUtils.checkString(occupancyAddress.getDescription()) + ",");
				sql.append("PRIMARY = " + StringUtils.checkString(occupancyAddress.getPrimary()) + ",");
				sql.append("ACTIVE = " + StringUtils.checkString(occupancyAddress.getActive()));
				sql.append(" WHERE  LSO_ADDRESS.ADDR_ID = " + occupancyAddress.getAddressId());

				logger.warn("Occupancy Address update sql is :" + sql.toString());
				db.update(sql.toString());
				int aLsoId = occupancy.getLsoId();
				logger.debug("Database updated successfully for occupancy address id = " + occupancy.getLsoId());
				occupancy = getOccupancy(aLsoId);

			}

		} catch (Exception e) {
			logger.error("Exception thrown while trying to update Occupancy Apn :" + e.getMessage());
		}
		return occupancy;
	}

	public int setOccupancyDetails(OccupancyDetails aOccupancyDetails, int lsoId) {
		try {

			Wrapper db = new Wrapper();
			occupancyDetails = aOccupancyDetails;

			StringBuffer sql = new StringBuffer();
			sql.append("UPDATE OCCUPANCY SET UNIT = " + StringUtils.checkString(occupancyDetails.getUnitNumber().trim()) + ",");
			sql.append("ALIAS = " + StringUtils.checkString(occupancyDetails.getAlias()) + ",");
			sql.append("DESCRIPTION = " + StringUtils.checkString(occupancyDetails.getDescription()) + ",");
			sql.append("FROM_FLOOR = " + StringUtils.checkString(occupancyDetails.getBeginFloor()) + ",");
			sql.append("TO_FLOOR = " + StringUtils.checkString(occupancyDetails.getEndFloor()) + ",");
			sql.append("PZONE_ID = " + occupancyDetails.getParkingZone().getPzoneId() + ",");
			sql.append("R_ZONE = " + occupancyDetails.getRZone() + ",");
			sql.append("ACTIVE = " + StringUtils.checkString(occupancyDetails.getActive()) + ",");
			sql.append("HOUSEHOLD_SIZE = " + StringUtils.checkString(occupancyDetails.getHouseholdSize()) + ",");
			sql.append("BEDROOM_SIZE = " + StringUtils.checkString(occupancyDetails.getBedroomSize()) + ",");
			sql.append("PROPERTY_MANAGER = " + StringUtils.checkString(occupancyDetails.getPropertyManager()) + ",");
			sql.append("TOTAL_RESTRICTED_UNITS = " + StringUtils.checkString(occupancyDetails.getRestrictedUnits()) + ",");
			sql.append("UTILITY_ALLOWANCE = " + StringUtils.checkString(occupancyDetails.getUtilityAllowance()) + ",");
			sql.append("GROSS_RENT = " + StringUtils.checkString(occupancyDetails.getGrossRent()) + ",");
			sql.append("LABEL = " + StringUtils.checkString(occupancyDetails.getLabel()));
			sql.append(" WHERE OCCUPANCY.OCCUPANCY_ID = " + lsoId);
			logger.warn("Occupancy Details update sql is :" + sql.toString());

			db.update(sql.toString());

			// inserting data into Occupancy_Usage Table
			sql = null;
			sql = new StringBuffer();
			List useList = aOccupancyDetails.getUse();
			Iterator useiter = useList.iterator();

			while (useiter.hasNext()) {
				Use useObj = (Use) useiter.next();
				sql.append("SELECT LSO_USE_ID FROM LSO_USE WHERE DESCRIPTION = " + StringUtils.checkString(useObj.getDescription()));
				sql.append(" AND LSO_TYPE ='O'");
				logger.debug(sql.toString());
				RowSet rs = db.select(sql.toString());
				sql = null;
				sql = new StringBuffer();
				if (rs.next()) {
					sql.append("INSERT INTO OCCUPANCY_USAGE(OCCUPANCY_ID,LSO_USE_ID) VALUES(" + lsoId + "," + rs.getString("lso_use_id") + ")");
					logger.debug(sql.toString());
					db.update(sql.toString());
				}
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update Occupancy Apn :" + e.getMessage());
		}
		return lsoId;
	}

	private String stringCheck(String aValue) {
		return (aValue == null) ? "" : aValue;
	}

	public Occupancy addOccupancyAddress(Occupancy occupancy) {
		try {

			Wrapper db = new Wrapper();
			int addressId = db.getNextId("ADDRESS_ID");
			logger.debug("****** The ID generated from addOccupancyAddress is " + addressId);

			int lsoId = occupancy.getLsoId();
			logger.debug("****** Lso Id is : " + lsoId);

			List occupancyAddressList = occupancy.getOccupancyAddress();
			logger.debug("the size of Occupancy address list is :" + occupancyAddressList.size());
			if (occupancyAddressList.size() > 0) {

				OccupancyAddress occupancyAddress = (OccupancyAddress) occupancyAddressList.get(0);
				StringBuffer sql = new StringBuffer();
				sql.append("INSERT INTO LSO_ADDRESS (ADDR_ID,LSO_ID,LSO_TYPE,STR_NO,STR_MOD,STREET_ID,UNIT,");
				sql.append("CITY,STATE,ZIP,ZIP4,DESCRIPTION,PRIMARY,ACTIVE,LAST_UPDATED,UPDATED_BY) VALUES (" + addressId + ",");
				sql.append(lsoId + ",'O',");
				sql.append(occupancyAddress.getStreetNbr() + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getStreetModifier()) + ",");
				sql.append(StringUtils.checkNumber(occupancyAddress.getStreet().getStreetId()) + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getUnit()) + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getCity()) + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getState()) + ",");
				sql.append(occupancyAddress.getZip() + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getZip4()) + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getDescription()) + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getPrimary()) + ",");
				sql.append(StringUtils.checkString(occupancyAddress.getActive()) + ",");
				sql.append("CURRENT_TIMESTAMP" + ",");
				sql.append(occupancyAddress.getUpdatedBy().getUserId() + ")");

				logger.warn("Occupancy Address insert sql is :" + sql);
				db.update(sql.toString());
				int aLsoId = occupancy.getLsoId();
				logger.debug("Database updated successfully for occupancy address id = " + occupancy.getLsoId());
				occupancy = getOccupancy(aLsoId);

			}

		} catch (Exception e) {
			logger.error("Exception thrown while trying to add Occupancy Address :" + e.getMessage());
		}
		return occupancy;
	}

	private void addApnToBatch(Wrapper db, int ownerId, Occupancy occupancy) {

		List occupancyApnList = occupancy.getOccupancyApn();
		logger.debug("the size of Occupancy apn list is :" + occupancyApnList.size());

		if (occupancyApnList.size() > 0) {
			OccupancyApn occupancyApn = (OccupancyApn) occupancyApnList.get(0);
			// if apn is null no insert is made to datanbase
			if ((occupancyApn.getApn() != null) && !(occupancyApn.getApn().equals(""))) {

				String lsoApn = "INSERT INTO LSO_APN(LSO_ID,APN,LSO_TYPE) VALUES(" + occupancy.getLsoId() + "," + StringUtils.checkString(occupancyApn.getApn()) + ",'O')";

				db.addBatch(lsoApn);
				logger.debug("LSOAPN :" + lsoApn);

				String owner = "INSERT INTO OWNER(OWNER_ID ,NAME ,STR_NO,STR_MOD,STR_NAME,CITY,STATE,ZIP,ZIP4,PHONE,FAX,EMAIL_ID ,FOREIGN_ADDRESS ,ADDRESS_LINE1,ADDRESS_LINE2 ,ADDRESS_LINE3 ,ADDRESS_LINE4 ,COUNTRY ,LAST_UPDATED,UPDATED_BY) VALUES (" + ownerId + "," + StringUtils.checkString(occupancyApn.getOwner().getName()) + "," + StringUtils.checkNumber(occupancyApn.getOwner().getLocalAddress().getStreetNbr()) + "," + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getStreetModifier()) + "," + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getStreet().getStreetName()) + "," + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getCity()) + "," + StringUtils.checkString(occupancyApn.getOwner().getLocalAddress().getState()) + "," + StringUtils.checkNumber(occupancyApn.getOwner().getLocalAddress().getZip()) + "," + StringUtils.checkNumber(occupancyApn.getOwner().getLocalAddress().getZip4()) + "," + StringUtils.checkString(StringUtils.stripPhone(occupancyApn.getOwner().getPhone())) + "," + StringUtils.checkString(StringUtils.stripPhone(occupancyApn.getOwner().getFax())) + "," + StringUtils.checkString(occupancyApn.getOwner().getEmail()) + "," + StringUtils.checkString(occupancyApn.getOwner().getForeignFlag()) + "," + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineOne()) + "," + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineTwo()) + "," + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineThree()) + "," + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getLineFour()) + "," + StringUtils.checkString(occupancyApn.getOwner().getForeignAddress().getCountry()) + ",CURRENT_TIMESTAMP," + occupancyApn.getUpdatedBy() + ")";

				logger.debug("SQL :" + owner);
				db.addBatch(owner);

				String apnOwner = "INSERT INTO APN_OWNER(APN,OWNER_ID,ACTIVE,LAST_UPDATED,UPDATED_BY) VALUES(" + StringUtils.checkString(occupancyApn.getApn()) + "," + ownerId + ",'Y',CURRENT_TIMESTAMP," + occupancyApn.getUpdatedBy() + ")";

				logger.debug("SQL :" + apnOwner);
				db.addBatch(apnOwner);

			} else
				logger.debug("apn is null ");

		}
	}

	public Occupancy addOccupancyApn(Occupancy occupancy) {
		try {

			Wrapper db = new Wrapper();

			// Begin the transaction

			int ownerId = db.getNextId("OWNER_ID");
			logger.debug("new owner id generated is " + ownerId);

			int lsoId = occupancy.getLsoId();
			logger.debug("obtained lso id is" + lsoId);

			db.beginTransaction();

			occupancy.setLsoId(lsoId);
			addApnToBatch(db, ownerId, occupancy);

			db.executeBatch();
			int aLsoId = occupancy.getLsoId();
			logger.debug("Database updated successfully for occupancy apn id = " + occupancy.getLsoId());
			occupancy = getOccupancy(aLsoId);

		} catch (Exception e) {
			logger.error("Exception thrown while trying to add Occupancy Apn :" + e.getMessage());
		}
		return occupancy;
	}

	public int addOccupancy(Occupancy occupancy) {
		int lsoId = -1;
		int ownerId = -1;
		try {
			Wrapper db = new Wrapper();
			StringBuffer sql = new StringBuffer();
			// Generating the ID for Occupancy.
			lsoId = db.getNextId("LSO_ID");
			logger.debug("****** The ID generated from add occupancy is " + lsoId);
			occupancy.setLsoId(lsoId);

			ownerId = db.getNextId("OWNER_ID");

			// Begin the transaction
			db.beginTransaction();

			logger.debug("1 " + occupancy.getOccupancyDetails().getAlias());
			logger.debug("2 " + occupancy.getOccupancyDetails().getDescription());
			logger.debug("3 " + occupancy.getOccupancyDetails().getUnitNumber());
			logger.debug("4 " + occupancy.getOccupancyDetails().getBeginFloor());
			logger.debug("5 " + occupancy.getOccupancyDetails().getEndFloor());
			logger.debug("6 " + occupancy.getOccupancyDetails().getParkingZone().getPzoneId());
			logger.debug("7 " + occupancy.getOccupancyDetails().getRZone());
			logger.debug("8 " + occupancy.getOccupancyDetails().getActive());
			logger.debug("9 " + occupancy.getOccupancyDetails().getHouseholdSize());
			logger.debug("10 " + occupancy.getOccupancyDetails().getBedroomSize());
			logger.debug("11 " + occupancy.getOccupancyDetails().getGrossRent());
			logger.debug("12 " + occupancy.getOccupancyDetails().getUtilityAllowance());
			logger.debug("13 " + occupancy.getOccupancyDetails().getPropertyManager());
			logger.debug("14 " + occupancy.getOccupancyDetails().getRestrictedUnits());

			sql.append("INSERT INTO OCCUPANCY(OCCUPANCY_ID,ALIAS,DESCRIPTION,UNIT,FROM_FLOOR,TO_FLOOR,");
			sql.append("PZONE_ID,R_ZONE,ACTIVE,LABEL,HOUSEHOLD_SIZE, BEDROOM_SIZE, PROPERTY_MANAGER, TOTAL_RESTRICTED_UNITS, UTILITY_ALLOWANCE, GROSS_RENT) values(" + lsoId + ",");
			sql.append(StringUtils.checkString(occupancy.getOccupancyDetails().getAlias()) + ",");
			sql.append(StringUtils.checkString(occupancy.getOccupancyDetails().getDescription()) + ",");
			sql.append(StringUtils.checkString(occupancy.getOccupancyDetails().getUnitNumber()) + ",");
			sql.append(StringUtils.checkString(occupancy.getOccupancyDetails().getBeginFloor()) + ",");
			sql.append(StringUtils.checkString(occupancy.getOccupancyDetails().getEndFloor()) + ",");
			sql.append(occupancy.getOccupancyDetails().getParkingZone().getPzoneId() + ",");
			sql.append(occupancy.getOccupancyDetails().getRZone() + ",");
			sql.append("'Y'" + ","); // Force Active on Add // occupancy.getOccupancyDetails().getActive()
			sql.append(StringUtils.checkString(occupancy.getOccupancyDetails().getLabel()) + ",");
			sql.append(StringUtils.checkNumber(occupancy.getOccupancyDetails().getHouseholdSize()) + ",");
			sql.append(StringUtils.checkNumber(occupancy.getOccupancyDetails().getBedroomSize()) + ",");
			sql.append(StringUtils.checkNumber(occupancy.getOccupancyDetails().getGrossRent()) + ",");
			sql.append(StringUtils.checkNumber(occupancy.getOccupancyDetails().getUtilityAllowance()) + ",");
			sql.append(StringUtils.checkNumber(occupancy.getOccupancyDetails().getPropertyManager()) + ",");
			sql.append(StringUtils.checkNumber(occupancy.getOccupancyDetails().getRestrictedUnits()) + ")");

			db.addBatch(sql.toString());
			logger.debug("Insert Occupancy SQL " + sql);

			// inserting data into Occupancy_Usage Table

			List useList = occupancy.getOccupancyDetails().getUse();
			Iterator useiter = useList.iterator();
			logger.debug("Use List size : " + useList.size());
			while (useiter.hasNext()) {
				Use useObj = (Use) useiter.next();
				if (useObj == null)
					logger.debug("The is no use Object");
				else
					logger.debug(StringUtils.checkString(useObj.getDescription()));
				String s1 = null;
				s1 = "select lso_use_id from lso_use where description=" + StringUtils.checkString(useObj.getDescription()) + " and lso_type='O'";

				// db.addBatch(sql.toString());
				logger.debug(s1);

				RowSet rs = db.select(s1);

				while (rs.next()) {
					String s = null;
					s = "insert into occupancy_usage(occupancy_id,lso_use_id) values(" + lsoId + "," + rs.getString("lso_use_id") + ")";
					db.addBatch(s);
					logger.debug("abcd" + s.toString());
				}

				if (rs != null)
					rs.close();

			}

			// *** Begin Inserting Occupancy Address

			// occupancy = addOccupancyAddress(occupancy);

			occupancyAddress = occupancy.getOccupancyAddress();
			if (occupancyAddress.size() > 0) {
				occAddrListObj = (OccupancyAddress) occupancyAddress.get(0);
				int addressId = db.getNextId("ADDRESS_ID");
				logger.debug("The ID generated for new Address is " + addressId);
				sql = null;
				sql = new StringBuffer();
				sql.append("insert into lso_address(addr_id,lso_id,lso_type,str_no,str_mod,street_id,unit,");
				sql.append("city,state,zip,zip4,description,primary,active,last_updated,updated_by,");
				sql.append("addr_primary) values(" + addressId + ",");
				logger.debug("got address id " + addressId);
				sql.append(lsoId + "," + "'O'" + "," + occAddrListObj.getStreetNbr() + ",");
				sql.append(StringUtils.checkString(occAddrListObj.getStreetModifier()) + ",");
				sql.append(occAddrListObj.getStreet().getStreetId() + ",");
				sql.append(StringUtils.checkString(occAddrListObj.getUnit()) + ",");
				sql.append(StringUtils.checkString(occAddrListObj.getCity()) + ",");
				sql.append(StringUtils.checkString(occAddrListObj.getState()) + ",");
				sql.append(StringUtils.checkNumber(occAddrListObj.getZip()) + ",");
				sql.append(StringUtils.checkString(occAddrListObj.getZip4()) + ",");
				sql.append(StringUtils.checkString(occAddrListObj.getDescription()) + ",");
				sql.append("'Y'" + ","); // Force Primary on Add Occupancy
				sql.append("'Y'" + ","); // Force Active on Add Occupancy
				sql.append("current_timestamp" + ",");
				sql.append(occAddrListObj.getUpdatedBy().getUserId() + ",");
				sql.append(addressId + ")");

				logger.debug(sql.toString());
				db.addBatch(sql.toString());
				logger.debug("Occupancy address INSERT SQL IS :" + sql);

			} else {
				logger.debug(" occupancy Address is not Created");
			}

			// *** End Inserting occupancy Address

			// Begin Inserting structure_occupant

			sql = null;
			sql = new StringBuffer();
			sql.append("INSERT INTO STRUCTURE_OCCUPANT(STRUCTURE_ID,OCCUPANCY_ID) VALUES(" + occupancy.getStructureId() + "," + lsoId + ")");
			logger.debug(sql.toString());
			db.addBatch(sql.toString());
			logger.debug(" Insert Structure_occupant Sql  " + sql.toString());
			// End Inserting structure_occupant

			// Add APN Information to the database
			this.addApnToBatch(db, ownerId, occupancy);

			db.executeBatch();
		} catch (SQLException sqle) {
			logger.error("Exception thrown while trying to add occupancy :" + sqle.getMessage());
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add occupancy :" + e.getMessage());
		}
		return lsoId;
	}

	public OccupancyApn getOccupancyApn(String apn) {
		OccupancyApn occupancyApnObj = null;
		try {
			occupancyApnObj = new OccupancyApn();
			occupancyApnObj.setApn(apn);
			String sql = "select * from owner o,apn_owner ao where o.owner_id=ao.owner_id and ao.apn='" + apn + "'";
			logger.debug(sql);
			RowSet ownerRs = new Wrapper().select(sql);
			if (ownerRs.next()) {
				occupancyApnObj.setActive(ownerRs.getString("active"));
				Owner owner = new Owner();
				owner.setEmail(ownerRs.getString("email_id"));
				owner.setFax(StringUtils.phoneFormat(ownerRs.getString("fax")));
				ForeignAddress foreignAddress = new ForeignAddress();
				foreignAddress.setCountry(ownerRs.getString("country"));
				foreignAddress.setLineOne(ownerRs.getString("address_line1"));
				foreignAddress.setLineTwo(ownerRs.getString("address_line2"));
				foreignAddress.setLineThree(ownerRs.getString("address_line3"));
				foreignAddress.setLineFour(ownerRs.getString("address_line4"));
				owner.setForeignAddress(foreignAddress);
				owner.setForeignFlag(ownerRs.getString("foreign_address"));
				Address localAddress = new Address();
				localAddress.setCity(ownerRs.getString("city"));
				localAddress.setState(ownerRs.getString("state"));
				Street localAddressStreet = new Street();
				String streetName = ownerRs.getString("str_name");
				localAddressStreet.setStreetName(streetName);
				logger.debug(" localaddressstreet streetname deva " + localAddressStreet.getStreetName());
				localAddress.setStreet(localAddressStreet);
				localAddress.setStreetModifier(ownerRs.getString("str_mod"));
				localAddress.setStreetNbr(StringUtils.s2i(StringUtils.zeroReplaceWithEmpty(ownerRs.getInt("str_no"))));
				logger.debug(" localaddress streetnbr deva" + localAddress.getStreetNbr());
				localAddress.setZip(ownerRs.getString("zip"));
				localAddress.setZip4(ownerRs.getString("zip4"));
				owner.setLocalAddress(localAddress);
				owner.setName(ownerRs.getString("name"));
				owner.setOwnerId(ownerRs.getInt("owner_id"));
				owner.setPhone(StringUtils.phoneFormat(ownerRs.getString("phone")));
				occupancyApnObj.setOwner(owner);
			}
			if (ownerRs != null)
				ownerRs = null;

		} catch (Exception e) {
			logger.error("Exception thrown while trying to add land Apn :" + e.getMessage());
		}
		return occupancyApnObj;
	}

	public SiteData getSetbackData(long lsoId) throws Exception {
		Wrapper db = new Wrapper();
		SiteData siteData = new SiteData();

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
		String sql = "SELECT * FROM SITE_SETBACK WHERE LSO_ID=" + lsoId + " ORDER BY SETBACK_ID DESC";
		logger.debug(sql);

		RowSet rs = db.select(sql);
		int i = 0;
		while (rs.next()) {
			SiteSetback setback = new SiteSetback();
			setback.setSetbackId(rs.getInt("SETBACK_ID"));
			setback.setLsoId(rs.getInt("LSO_ID"));
			setback.setFrontFt(rs.getString("FRONT_FT"));
			setback.setFrontIn(rs.getString("FRONT_IN"));
			setback.setFrontComment(rs.getString("FRONT_COMMENT"));
			setback.setRearFt(rs.getString("REAR_FT"));
			setback.setRearIn(rs.getString("REAR_IN"));
			setback.setRearComment(rs.getString("REAR_COMMENT"));
			setback.setSide1Ft(rs.getString("SIDE1_FT"));
			setback.setSide1In(rs.getString("SIDE1_IN"));
			setback.setSide1Dir(rs.getString("SIDE1_DIR"));
			setback.setSide1Comment(rs.getString("SIDE1_COMMENT"));
			setback.setSide2Ft(rs.getString("SIDE2_FT"));
			setback.setSide2In(rs.getString("SIDE2_IN"));
			setback.setSide2Dir(rs.getString("SIDE2_DIR"));
			setback.setSide2Comment(rs.getString("SIDE2_COMMENT"));
			setback.setComment(rs.getString("COMMENTS"));
			setback.setUpdated(dateFormat.format(rs.getDate("UPDATED")));
			if (++i == 1) {
				siteData.setSetback(setback);
				logger.debug("Setback Data Set");
			}
		}
		return siteData;
	}

	/**
	 * Get Site Structure
	 * 
	 * @param lsoId
	 * @param activeSite
	 * @return
	 * @throws Exception
	 */
	public List getSiteStructures(long lsoId, String activeSite) throws Exception {
		List structures = new ArrayList();
		String sql = "";
		if (activeSite.equalsIgnoreCase("Y")) {
			sql = "SELECT LSOID,BUILDINGNAME,SEQUENCE_NO FROM SITE_DATA WHERE ACTIVE ='Y' and LSOID=" + lsoId;
		} else {
			sql = "SELECT LSOID,BUILDINGNAME,SEQUENCE_NO FROM SITE_DATA WHERE LSOID=" + lsoId;
		}
		logger.debug(sql);
		Wrapper db = new Wrapper();
		RowSet rs = db.select(sql);
		while (rs.next()) {
			SiteStructureData structure = new SiteStructureData();
			structure.setLsoId(lsoId);
			structure.setStructureId(rs.getLong("SEQUENCE_NO"));
			structure.setBuildingName(rs.getString("BUILDINGNAME"));
			structures.add(structure);
		}
		logger.info("Exiting getSiteStructures(" + structures.size() + ")");
		return structures;
	}

	/**
	 * Get Site Data History
	 * 
	 * @param structureId
	 * @return
	 * @throws Exception
	 */
	public List getStructureHistory(long structureId) throws Exception {
		logger.info("getStructureHistory(" + structureId + ")");
		List structureHistory = new ArrayList();
		Wrapper db = new Wrapper();
		String sql = "SELECT * FROM SITE_DATA_HISTORY WHERE SEQUENCE_NO=" + structureId + " ORDER BY UPDATED DESC";
		logger.info(sql);
		RowSet rs = db.select(sql);
		while (rs.next()) {
			SiteStructureData structure = new SiteStructureData();
			structure.setVersion(rs.getInt("VERSION_NO"));
			structure.setLsoId(rs.getLong("LSOID"));
			structure.setStructureId(rs.getLong("SEQUENCE_NO"));
			structure.setBuildingName(rs.getString("BUILDINGNAME"));
			structure.setBuildingType(rs.getString("SURVEY_TYPE"));
			structure.setBuildingUse(rs.getString("BUILDING_USE"));
			structure.setOccupancyGroup(rs.getString("OCCUPANCY"));
			structure.setConstructionType(rs.getString("BLDGCONSTTYPECODE"));
			structure.setStories(rs.getString("NOOFSTORIESABOVEGRADE"));
			structure.setActualStories(rs.getString("ACTUAL_NOSABOVEGRADE"));
			structure.setBasementLevels(rs.getString("NOOFSTORIESBELOWGRADE"));
			structure.setActualBasementLevels(rs.getString("ACTUAL_NOSBELOWGRADE"));
			structure.setHeightFeet(rs.getString("HEIGHTFEET"));
			structure.setAddHeightFeet(rs.getString("ADDHEIGHTFEET"));
			structure.setHeightInches(rs.getString("HEIGHTINCHES"));
			structure.setSprinklered(rs.getString("SPRINKLERED"));
			structure.setLotArea(rs.getString("NEW_LOT_AREA"));
			structure.setPadArea(rs.getString("PAD_AREA"));
			structure.setGarageArea(rs.getString("GARAGE_AREA"));
			structure.setActualGarageArea(rs.getString("ACTUAL_GARAGE_AREA"));
			structure.setSlopeArea(rs.getString("SLOPE_AREA"));
			structure.setBuildingArea(rs.getString("EXISTING_BLDG_AREA"));
			structure.setBasementArea(rs.getString("BASEMENT_AREA"));
			structure.setBasementArea(rs.getString("ACTUAL_BASEMENT_AREA"));
			structure.setNewBuildingArea(rs.getString("NEW_BLDG_AREA"));
			structure.setActualNewBuildingArea(rs.getString("ACTUAL_NEW_BLDG_AREA"));
			structure.setDwellingUnits(rs.getString("NOOFRESUNITS"));
			structure.setActualDwellingUnits(rs.getString("ACTUAL_NOOFRESUNITS"));
			structure.setBedrooms(rs.getString("NBR_BEDROOMS"));
			structure.setActualBedrooms(rs.getString("ACTUAL_NBR_BEDROOMS"));
			structure.setRoofing(rs.getString("ROOFING"));
			structure.setZoning(rs.getString("ZONECURRENT"));
			structure.setLocation(rs.getString("RESIDENTIAL_LOCATION"));
			structure.setAllowedFar(rs.getString("FAR_ALLOWED"));
			structure.setActualFar(rs.getString("FAR_ACTUAL"));
			structure.setFloorAreaAdded(rs.getString("DEMO_BLDG_AREA"));
			structure.setRequiredParking(rs.getString("PARKINGREQUIRED"));
			structure.setProvidedParking(rs.getString("PARKINGPROVIDED"));
			structure.setComment(rs.getString("COMMENTS"));
			structure.setUpdatedBy(rs.getString("UPDATED_BY"));
			structure.setUpdated(rs.getDate("UPDATED"));
			structure.setModuleNumber(rs.getString("BWP_MODULE_NUM"));
			structure.setKwSize(rs.getString("BWP_SOLAR_KW_SIZE"));
			structure.setConfNumber(rs.getString("BWP_CONF_NUM"));
			structure.setMeterSpotDate(rs.getDate("METER_SPOT_DATE"));
			structure.setBatteryIncluded(rs.getString("BATTERY_INCLUDED"));
			
			if (rs.getString("ACTIVE").equalsIgnoreCase("Y")) {
				structure.setActive("on");
			} else {
				structure.setActive("off");
			}
			structureHistory.add(structure);
		}
		return structureHistory;
	}

	/**
	 * Get Site Data
	 * 
	 * @param structureId
	 * @return
	 * @throws Exception
	 */
	public SiteStructureData getSiteStructure(long structureId) throws Exception {
		logger.info("Entering getSiteStructure(" + structureId + ")");
		SiteStructureData structure = new SiteStructureData();
		String sql = "SELECT * FROM SITE_DATA WHERE SEQUENCE_NO=" + structureId;
		logger.debug(sql);
		Wrapper db = new Wrapper();
		RowSet rs = db.select(sql);
		if (rs.next()) {
			structure.setLsoId(rs.getLong("LSOID"));
			structure.setStructureId(rs.getLong("SEQUENCE_NO"));
			structure.setBuildingName(rs.getString("BUILDINGNAME"));
			structure.setBuildingType(rs.getString("SURVEY_TYPE"));
			structure.setBuildingUse(rs.getString("BUILDING_USE"));
			structure.setOccupancyGroup(rs.getString("OCCUPANCY"));
			structure.setConstructionType(rs.getString("BLDGCONSTTYPECODE"));
			structure.setStories(rs.getString("NOOFSTORIESABOVEGRADE"));
			structure.setActualStories(rs.getString("ACTUAL_NOSABOVEGRADE"));
			structure.setBasementLevels(rs.getString("NOOFSTORIESBELOWGRADE"));
			structure.setActualBasementLevels(rs.getString("ACTUAL_NOSBELOWGRADE"));
			structure.setHeightFeet(rs.getString("HEIGHTFEET"));
			structure.setAddHeightFeet(rs.getString("ADDHEIGHTFEET"));
			structure.setHeightInches(rs.getString("HEIGHTINCHES"));
			structure.setSprinklered(rs.getString("SPRINKLERED"));
			structure.setLotArea(rs.getString("NEW_LOT_AREA"));
			structure.setPadArea(rs.getString("PAD_AREA"));
			structure.setGarageArea(rs.getString("GARAGE_AREA"));
			structure.setActualGarageArea(rs.getString("ACTUAL_GARAGE_AREA"));
			structure.setSlopeArea(rs.getString("SLOPE_AREA"));
			structure.setBuildingArea(rs.getString("EXISTING_BLDG_AREA"));
			structure.setBasementArea(rs.getString("BASEMENT_AREA"));
			structure.setBasementArea(rs.getString("ACTUAL_BASEMENT_AREA"));
			structure.setNewBuildingArea(rs.getString("NEW_BLDG_AREA"));
			structure.setActualNewBuildingArea(rs.getString("ACTUAL_NEW_BLDG_AREA"));
			structure.setDwellingUnits(rs.getString("NOOFRESUNITS"));
			structure.setActualDwellingUnits(rs.getString("ACTUAL_NOOFRESUNITS"));
			structure.setBedrooms(rs.getString("NBR_BEDROOMS"));
			structure.setActualBedrooms(rs.getString("ACTUAL_NBR_BEDROOMS"));
			structure.setRoofing(rs.getString("ROOFING"));
			structure.setZoning(rs.getString("ZONECURRENT"));
			structure.setLocation(rs.getString("RESIDENTIAL_LOCATION"));
			structure.setAllowedFar(rs.getString("FAR_ALLOWED"));
			structure.setActualFar(rs.getString("FAR_ACTUAL"));
			structure.setFloorAreaAdded(rs.getString("DEMO_BLDG_AREA"));
			structure.setRequiredParking(rs.getString("PARKINGREQUIRED"));
			structure.setProvidedParking(rs.getString("PARKINGPROVIDED"));
			structure.setUpdatedBy(rs.getString("UPDATED_BY"));
			structure.setUpdated(rs.getDate("UPDATED"));
			structure.setComment(rs.getString("COMMENTS"));
			structure.setModuleNumber(rs.getString("BWP_MODULE_NUM"));
			structure.setKwSize(rs.getString("BWP_SOLAR_KW_SIZE"));
			structure.setConfNumber(rs.getString("BWP_CONF_NUM"));
			structure.setMeterSpotDate(rs.getDate("METER_SPOT_DATE"));
			structure.setBatteryIncluded(rs.getString("BATTERY_INCLUDED"));
			
			if (rs.getString("ACTIVE").equalsIgnoreCase("Y")) {
				structure.setActive("on");
			} else {
				structure.setActive("off");
			}

		}
		return structure;
	}

	/**
	 * Get Site Data History
	 * 
	 * @param structureId
	 * @return
	 * @throws Exception
	 */
	public SiteStructureData getSiteStructureHistory(long structureId, int version) throws Exception {
		logger.info("Entering getSiteStructureHistory(" + structureId + "," + version + ")");

		SiteStructureData structure = new SiteStructureData();
		String sql = "SELECT * FROM SITE_DATA_HISTORY WHERE SEQUENCE_NO=" + structureId + " AND VERSION_NO=" + version + " order by updated desc";
		logger.debug(sql);
		Wrapper db = new Wrapper();
		RowSet rs = db.select(sql);
		if (rs.next()) {
			structure.setLsoId(rs.getLong("LSOID"));
			structure.setStructureId(rs.getLong("SEQUENCE_NO"));
			structure.setBuildingName(rs.getString("BUILDINGNAME"));
			structure.setBuildingType(rs.getString("SURVEY_TYPE"));
			structure.setBuildingUse(rs.getString("BUILDING_USE"));
			structure.setOccupancyGroup(rs.getString("OCCUPANCY"));
			structure.setConstructionType(rs.getString("BLDGCONSTTYPECODE"));
			structure.setStories(rs.getString("NOOFSTORIESABOVEGRADE"));
			structure.setActualStories(rs.getString("ACTUAL_NOSABOVEGRADE"));
			structure.setBasementLevels(rs.getString("NOOFSTORIESBELOWGRADE"));
			structure.setActualBasementLevels(rs.getString("ACTUAL_NOSBELOWGRADE"));
			structure.setHeightFeet(rs.getString("HEIGHTFEET"));
			structure.setAddHeightFeet(rs.getString("ADDHEIGHTFEET"));
			structure.setHeightInches(rs.getString("HEIGHTINCHES"));
			structure.setSprinklered(rs.getString("SPRINKLERED"));
			structure.setLotArea(rs.getString("NEW_LOT_AREA"));
			structure.setPadArea(rs.getString("PAD_AREA"));
			structure.setGarageArea(rs.getString("GARAGE_AREA"));
			structure.setActualGarageArea(rs.getString("ACTUAL_GARAGE_AREA"));
			structure.setSlopeArea(rs.getString("SLOPE_AREA"));
			structure.setBuildingArea(rs.getString("EXISTING_BLDG_AREA"));
			structure.setBasementArea(rs.getString("BASEMENT_AREA"));
			structure.setBasementArea(rs.getString("ACTUAL_BASEMENT_AREA"));
			structure.setNewBuildingArea(rs.getString("NEW_BLDG_AREA"));
			structure.setActualNewBuildingArea(rs.getString("ACTUAL_NEW_BLDG_AREA"));
			structure.setDwellingUnits(rs.getString("NOOFRESUNITS"));
			structure.setActualDwellingUnits(rs.getString("ACTUAL_NOOFRESUNITS"));
			structure.setBedrooms(rs.getString("NBR_BEDROOMS"));
			structure.setActualBedrooms(rs.getString("ACTUAL_NBR_BEDROOMS"));
			structure.setRoofing(rs.getString("ROOFING"));
			structure.setZoning(rs.getString("ZONECURRENT"));
			structure.setLocation(rs.getString("RESIDENTIAL_LOCATION"));
			structure.setAllowedFar(rs.getString("FAR_ALLOWED"));
			structure.setActualFar(rs.getString("FAR_ACTUAL"));
			structure.setFloorAreaAdded(rs.getString("DEMO_BLDG_AREA"));
			structure.setRequiredParking(rs.getString("PARKINGREQUIRED"));
			structure.setProvidedParking(rs.getString("PARKINGPROVIDED"));
			structure.setUpdatedBy(rs.getString("UPDATED_BY"));
			structure.setUpdated(rs.getDate("UPDATED"));
			structure.setComment(rs.getString("COMMENTS"));
			structure.setModuleNumber(rs.getString("BWP_MODULE_NUM"));
			structure.setKwSize(rs.getString("BWP_SOLAR_KW_SIZE"));
			structure.setConfNumber(rs.getString("BWP_CONF_NUM"));
			structure.setMeterSpotDate(rs.getDate("METER_SPOT_DATE"));
			structure.setBatteryIncluded(rs.getString("BATTERY_INCLUDED"));

			if (rs.getString("ACTIVE").equalsIgnoreCase("Y")) {
				structure.setActive("on");
			} else {
				structure.setActive("off");
			}

		}
		return structure;
	}

	public void saveSiteStructure(SiteStructureData structure) throws Exception {
		logger.debug("Entering saveSiteStructure(" + structure.getStructureId() + ")");
		try {
			Wrapper db = new Wrapper();
			String sql;
			if (structure.getStructureId() == 0) {
				structure.setStructureId(db.getNextId("SITE_STRUCT_NBR"));
				sql = addSiteStructure(structure);
			} else
				sql = updateSiteStructure(structure);
			logger.debug(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new Exception(e.getMessage());
		}
		logger.info("Exiting saveSiteStructure");
	}

	private String addSiteStructure(SiteStructureData structure) {
		logger.info("Entering addSiteStructure()");
		StringBuffer sql = new StringBuffer("INSERT INTO SITE_DATA (LSOID,SEQUENCE_NO,BUILDINGNAME,SURVEY_TYPE,BUILDING_USE,OCCUPANCY,BLDGCONSTTYPECODE,NOOFSTORIESABOVEGRADE,ACTUAL_NOSABOVEGRADE,NOOFSTORIESBELOWGRADE,ACTUAL_NOSBELOWGRADE,HEIGHTFEET,ADDHEIGHTFEET,HEIGHTINCHES,SPRINKLERED,NEW_LOT_AREA,PAD_AREA,GARAGE_AREA,ACTUAL_GARAGE_AREA,SLOPE_AREA,EXISTING_BLDG_AREA,BASEMENT_AREA,ACTUAL_BASEMENT_AREA,NEW_BLDG_AREA,ACTUAL_NEW_BLDG_AREA,NOOFRESUNITS,ACTUAL_NOOFRESUNITS,NBR_BEDROOMS,ACTUAL_NBR_BEDROOMS,ROOFING,ZONECURRENT,RESIDENTIAL_LOCATION,FAR_ALLOWED,FAR_ACTUAL,DEMO_BLDG_AREA,PARKINGREQUIRED,PARKINGPROVIDED,COMMENTS,UPDATED_BY,ACTIVE,BWP_MODULE_NUM,BWP_SOLAR_KW_SIZE,BWP_CONF_NUM,BATTERY_INCLUDED,METER_SPOT_DATE) VALUES (");
		sql.append(structure.getLsoId());
		sql.append("," + structure.getStructureId());
		sql.append("," + StringUtils.checkString(structure.getBuildingName()));
		sql.append("," + StringUtils.checkNumber(structure.getBuildingType()));
		sql.append("," + StringUtils.checkString(structure.getBuildingUse()));
		sql.append("," + StringUtils.checkString(structure.getOccupancyGroup()));
		sql.append("," + StringUtils.checkString(structure.getConstructionType()));
		sql.append("," + StringUtils.checkNumber(structure.getStories()));
		sql.append("," + StringUtils.checkNumber(structure.getActualStories()));
		sql.append("," + StringUtils.checkNumber(structure.getBasementLevels()));
		sql.append("," + StringUtils.checkNumber(structure.getActualBasementLevels()));
		sql.append("," + StringUtils.checkNumber(structure.getHeightFeet()));
		sql.append("," + StringUtils.checkNumber(structure.getAddHeightFeet()));
		sql.append("," + StringUtils.checkNumber(structure.getHeightInches()));
		sql.append("," + StringUtils.checkString(structure.getSprinklered()));
		sql.append("," + StringUtils.checkNumber(structure.getLotArea()));
		sql.append("," + StringUtils.checkNumber(structure.getPadArea()));
		sql.append("," + StringUtils.checkNumber(structure.getGarageArea()));
		sql.append("," + StringUtils.checkNumber(structure.getActualGarageArea()));
		sql.append("," + StringUtils.checkNumber(structure.getSlopeArea()));
		sql.append("," + StringUtils.checkNumber(structure.getBuildingArea()));
		sql.append("," + StringUtils.checkNumber(structure.getBasementArea()));
		sql.append("," + StringUtils.checkNumber(structure.getActualBasementArea()));
		sql.append("," + StringUtils.checkNumber(structure.getNewBuildingArea()));
		sql.append("," + StringUtils.checkNumber(structure.getActualNewBuildingArea()));
		sql.append("," + StringUtils.checkNumber(structure.getDwellingUnits()));
		sql.append("," + StringUtils.checkNumber(structure.getActualDwellingUnits()));
		sql.append("," + StringUtils.checkNumber(structure.getBedrooms()));
		sql.append("," + StringUtils.checkNumber(structure.getActualBedrooms()));
		sql.append("," + StringUtils.checkString(structure.getRoofing()));
		sql.append("," + StringUtils.checkString(structure.getZoning()));
		sql.append("," + StringUtils.checkString(structure.getLocation()));
		sql.append("," + StringUtils.checkNumber(structure.getAllowedFar()));
		sql.append("," + StringUtils.checkNumber(structure.getActualFar()));
		sql.append("," + StringUtils.checkNumber(structure.getFloorAreaAdded()));
		sql.append("," + StringUtils.checkNumber(structure.getRequiredParking()));
		sql.append("," + StringUtils.checkNumber(structure.getProvidedParking()));
		sql.append("," + StringUtils.checkString(structure.getComment()));
		sql.append("," + structure.getUpdatedBy());
		sql.append(",'" + structure.getActive() + "'");
		sql.append("," + StringUtils.checkString(structure.getModuleNumber()));
		sql.append("," + StringUtils.checkString(structure.getKwSize()));
		sql.append("," + StringUtils.checkString(structure.getConfNumber()));
		
		String batteryIncluded = "N";
		if(structure.getBatteryIncluded() != "" )
			batteryIncluded = structure.getBatteryIncluded();
		
		logger.debug("batteryIncluded "+batteryIncluded);
		
		sql.append("," + StringUtils.checkString(batteryIncluded));
		sql.append(",TO_DATE('"+structure.getMeterSpotDate()+"','"+Constants.DATE_FORMAT+"') )");
		logger.info("Exiting addSiteStructure()");
		return (sql.toString());
	}

	private void addSiteStructureHistory(SiteStructureData structure) throws Exception {
		logger.info("Entering addSiteStructureHistory()");
		Wrapper db = new Wrapper();
		int versionNumber = -1;
		String versionNumberSql = "select max(version_no)+1 from site_data_history where sequence_no=" + structure.getStructureId();
		logger.info(versionNumberSql);
		RowSet rs = db.select(versionNumberSql);
		if (rs.next()) {
			if (rs.getString(1) == null) {
				versionNumber = 1;
			} else {
				versionNumber = rs.getInt(1);
			}
		}
		logger.info("versionNumber =" + versionNumber);

		StringBuffer sql = new StringBuffer("INSERT INTO SITE_DATA_HISTORY (VERSION_NO,LSOID,SEQUENCE_NO,BUILDINGNAME,SURVEY_TYPE,BUILDING_USE,OCCUPANCY,BLDGCONSTTYPECODE,NOOFSTORIESABOVEGRADE,ACTUAL_NOSABOVEGRADE,NOOFSTORIESBELOWGRADE,ACTUAL_NOSBELOWGRADE,HEIGHTFEET,ADDHEIGHTFEET,HEIGHTINCHES,SPRINKLERED,NEW_LOT_AREA,PAD_AREA,GARAGE_AREA,ACTUAL_GARAGE_AREA,SLOPE_AREA,EXISTING_BLDG_AREA,BASEMENT_AREA,ACTUAL_BASEMENT_AREA,NEW_BLDG_AREA,ACTUAL_NEW_BLDG_AREA,NOOFRESUNITS,ACTUAL_NOOFRESUNITS,NBR_BEDROOMS,ACTUAL_NBR_BEDROOMS,ROOFING,ZONECURRENT,RESIDENTIAL_LOCATION,FAR_ALLOWED,FAR_ACTUAL,DEMO_BLDG_AREA,PARKINGREQUIRED,PARKINGPROVIDED,COMMENTS,UPDATED,UPDATED_BY,ACTIVE,BWP_MODULE_NUM,BWP_SOLAR_KW_SIZE,BWP_CONF_NUM,BATTERY_INCLUDED,METER_SPOT_DATE) VALUES (");
		sql.append(versionNumber);
		sql.append("," + structure.getLsoId());
		sql.append("," + structure.getStructureId());
		sql.append("," + StringUtils.checkString(structure.getBuildingName()));
		sql.append("," + StringUtils.checkNumber(structure.getBuildingType()));
		sql.append("," + StringUtils.checkString(structure.getBuildingUse()));
		sql.append("," + StringUtils.checkString(structure.getOccupancyGroup()));
		sql.append("," + StringUtils.checkString(structure.getConstructionType()));
		sql.append("," + StringUtils.checkNumber(structure.getStories()));
		sql.append("," + StringUtils.checkNumber(structure.getActualStories()));
		sql.append("," + StringUtils.checkNumber(structure.getBasementLevels()));
		sql.append("," + StringUtils.checkNumber(structure.getActualBasementLevels()));
		sql.append("," + StringUtils.checkNumber(structure.getHeightFeet()));
		sql.append("," + StringUtils.checkNumber(structure.getAddHeightFeet()));
		sql.append("," + StringUtils.checkNumber(structure.getHeightInches()));
		sql.append("," + StringUtils.checkString(structure.getSprinklered()));
		sql.append("," + StringUtils.checkNumber(structure.getLotArea()));
		sql.append("," + StringUtils.checkNumber(structure.getPadArea()));
		sql.append("," + StringUtils.checkNumber(structure.getGarageArea()));
		sql.append("," + StringUtils.checkNumber(structure.getActualGarageArea()));
		sql.append("," + StringUtils.checkNumber(structure.getSlopeArea()));
		sql.append("," + StringUtils.checkNumber(structure.getBuildingArea()));
		sql.append("," + StringUtils.checkNumber(structure.getBasementArea()));
		sql.append("," + StringUtils.checkNumber(structure.getActualBasementArea()));
		sql.append("," + StringUtils.checkNumber(structure.getNewBuildingArea()));
		sql.append("," + StringUtils.checkNumber(structure.getActualNewBuildingArea()));
		sql.append("," + StringUtils.checkNumber(structure.getDwellingUnits()));
		sql.append("," + StringUtils.checkNumber(structure.getActualDwellingUnits()));
		sql.append("," + StringUtils.checkNumber(structure.getBedrooms()));
		sql.append("," + StringUtils.checkNumber(structure.getActualBedrooms()));
		sql.append("," + StringUtils.checkString(structure.getRoofing()));
		sql.append("," + StringUtils.checkString(structure.getZoning()));
		sql.append("," + StringUtils.checkString(structure.getLocation()));
		sql.append("," + StringUtils.checkNumber(structure.getAllowedFar()));
		sql.append("," + StringUtils.checkNumber(structure.getActualFar()));
		sql.append("," + StringUtils.checkNumber(structure.getFloorAreaAdded()));
		sql.append("," + StringUtils.checkNumber(structure.getRequiredParking()));
		sql.append("," + StringUtils.checkNumber(structure.getProvidedParking()));
		sql.append("," + StringUtils.checkString(structure.getComment()));
		sql.append("," + StringUtils.toOracleDate(StringUtils.date2str(structure.getUpdated())));
		sql.append("," + structure.getUpdatedBy());
		sql.append(",'" + structure.getActive() + "'");
		sql.append("," + StringUtils.checkString(structure.getModuleNumber()));
		sql.append("," + StringUtils.checkString(structure.getKwSize()));
		sql.append("," + StringUtils.checkString(structure.getConfNumber()));
		
		String batteryIncluded = "N";
		if(structure.getBatteryIncluded() != "" )
			batteryIncluded = structure.getBatteryIncluded();
		
		sql.append("," + StringUtils.checkString(batteryIncluded));
		sql.append(",TO_DATE('"+structure.getMeterSpotDate()+"','"+Constants.DATE_FORMAT+"') )");
		logger.info(sql.toString());
		db.insert(sql.toString());
	}

	private String updateSiteStructure(SiteStructureData structure) throws Exception {
		logger.info("updateSiteStructure(SiteStructureData)");

		long sequenceNumber = structure.getStructureId();
		addSiteStructureHistory(structure);

		StringBuffer sql = new StringBuffer("UPDATE SITE_DATA SET ");
		sql.append("BUILDINGNAME =" + StringUtils.checkString(structure.getBuildingName()));
		sql.append(",SURVEY_TYPE =" + StringUtils.checkNumber(structure.getBuildingType()));
		sql.append(",BUILDING_USE =" + StringUtils.checkString(structure.getBuildingUse()));
		sql.append(",OCCUPANCY =" + StringUtils.checkString(structure.getOccupancyGroup()));
		sql.append(",BLDGCONSTTYPECODE =" + StringUtils.checkString(structure.getConstructionType()));
		sql.append(",NOOFSTORIESABOVEGRADE =" + StringUtils.checkNumber(structure.getStories()));
		sql.append(",ACTUAL_NOSABOVEGRADE =" + StringUtils.checkNumber(structure.getActualStories()));
		sql.append(",NOOFSTORIESBELOWGRADE =" + StringUtils.checkNumber(structure.getBasementLevels()));
		sql.append(",ACTUAL_NOSBELOWGRADE =" + StringUtils.checkNumber(structure.getActualBasementLevels()));
		sql.append(",HEIGHTFEET =" + StringUtils.checkNumber(structure.getHeightFeet()));
		sql.append(",ADDHEIGHTFEET =" + StringUtils.checkNumber(structure.getAddHeightFeet()));
		sql.append(",HEIGHTINCHES =" + StringUtils.checkNumber(structure.getHeightInches()));
		sql.append(",SPRINKLERED =" + StringUtils.checkString(structure.getSprinklered()));
		sql.append(",NEW_LOT_AREA =" + StringUtils.checkNumber(structure.getLotArea()));
		sql.append(",PAD_AREA =" + StringUtils.checkNumber(structure.getPadArea()));
		sql.append(",GARAGE_AREA =" + StringUtils.checkNumber(structure.getGarageArea()));
		sql.append(",ACTUAL_GARAGE_AREA =" + StringUtils.checkNumber(structure.getActualGarageArea()));
		sql.append(",SLOPE_AREA =" + StringUtils.checkNumber(structure.getSlopeArea()));
		sql.append(",EXISTING_BLDG_AREA =" + StringUtils.checkNumber(structure.getBuildingArea()));
		sql.append(",BASEMENT_AREA =" + StringUtils.checkNumber(structure.getBasementArea()));
		sql.append(",ACTUAL_BASEMENT_AREA =" + StringUtils.checkNumber(structure.getActualBasementArea()));
		sql.append(",NEW_BLDG_AREA =" + StringUtils.checkNumber(structure.getNewBuildingArea()));
		sql.append(",ACTUAL_NEW_BLDG_AREA =" + StringUtils.checkNumber(structure.getActualNewBuildingArea()));
		sql.append(",NOOFRESUNITS =" + StringUtils.checkNumber(structure.getDwellingUnits()));
		sql.append(",ACTUAL_NOOFRESUNITS =" + StringUtils.checkNumber(structure.getActualDwellingUnits()));
		sql.append(",NBR_BEDROOMS =" + StringUtils.checkNumber(structure.getBedrooms()));
		sql.append(",ACTUAL_NBR_BEDROOMS =" + StringUtils.checkNumber(structure.getActualBedrooms()));
		sql.append(",ROOFING =" + StringUtils.checkString(structure.getRoofing()));
		sql.append(",ZONECURRENT =" + StringUtils.checkString(structure.getZoning()));
		sql.append(",RESIDENTIAL_LOCATION =" + StringUtils.checkString(structure.getLocation()));
		sql.append(",FAR_ALLOWED =" + StringUtils.checkNumber(structure.getAllowedFar()));
		sql.append(",FAR_ACTUAL =" + StringUtils.checkNumber(structure.getActualFar()));
		sql.append(",DEMO_BLDG_AREA =" + StringUtils.checkNumber(structure.getFloorAreaAdded()));
		sql.append(",PARKINGREQUIRED =" + StringUtils.checkNumber(structure.getRequiredParking()));
		sql.append(",PARKINGPROVIDED =" + StringUtils.checkNumber(structure.getProvidedParking()));
		sql.append(",COMMENTS =" + StringUtils.checkString(structure.getComment()));
		sql.append(",UPDATED = current_date");
		sql.append(",UPDATED_BY =" + structure.getUpdatedBy());
		sql.append(",ACTIVE='" + structure.getActive() + "' ");
		sql.append(",BWP_MODULE_NUM =" + StringUtils.checkString(structure.getModuleNumber()));
		sql.append(",BWP_SOLAR_KW_SIZE =" + StringUtils.checkString(structure.getKwSize()));
		sql.append(",BWP_CONF_NUM =" + StringUtils.checkString(structure.getConfNumber()));
		sql.append(",METER_SPOT_DATE=TO_DATE('"+structure.getMeterSpotDate()+"','"+Constants.DATE_FORMAT+"')");
		
		String batteryIncluded = "N";
		if(structure.getBatteryIncluded() != "" )
			batteryIncluded = structure.getBatteryIncluded();
		
		sql.append(",BATTERY_INCLUDED =" + StringUtils.checkString(batteryIncluded));
		sql.append(" WHERE SEQUENCE_NO=" + sequenceNumber);
		logger.info("Exiting updateSiteStructure()");
		return (sql.toString());
	}

	public void addSetbackData(long lsoId, SiteSetback setback, long structureId) throws Exception {
		logger.info("addSetbackData(" + lsoId + "," + setback + ", " + structureId + ")");

		StringBuffer sbSql = new StringBuffer();
		if (setback.isChanged()) {
			Wrapper db = new Wrapper();

			backupSetbackData(lsoId, structureId);
			sbSql.append("INSERT INTO SITE_SETBACK (SETBACK_ID,LSO_ID,FRONT_FT,FRONT_IN,FRONT_COMMENT,REAR_FT,REAR_IN,REAR_COMMENT,SIDE1_FT,SIDE1_IN,SIDE1_DIR,SIDE1_COMMENT,SIDE2_FT,SIDE2_IN,SIDE2_DIR,SIDE2_COMMENT,SEQUENCE_NO,UPDATED) VALUES (");
			sbSql.append(db.getNextId("SETBACK_ID") + ",");
			sbSql.append(lsoId + ",");
			sbSql.append(StringUtils.checkNumber(setback.getFrontFt()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getFrontIn()) + ",");
			sbSql.append(StringUtils.checkString(setback.getFrontComment()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getRearFt()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getRearIn()) + ",");
			sbSql.append(StringUtils.checkString(setback.getRearComment()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getSide1Ft()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getSide1In()) + ",");
			sbSql.append(StringUtils.checkString(setback.getSide1Dir()) + ",");
			sbSql.append(StringUtils.checkString(setback.getSide1Comment()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getSide2Ft()) + ",");
			sbSql.append(StringUtils.checkNumber(setback.getSide2In()) + ",");
			sbSql.append(StringUtils.checkString(setback.getSide2Dir()) + ",");
			sbSql.append(structureId + ",");
			sbSql.append(StringUtils.checkString(setback.getSide2Comment()) + ",CURRENT_TIMESTAMP)");
			logger.debug(sbSql.toString());
			db.update(sbSql.toString());
			logger.debug(sbSql);
		}
		return;
	}

	private void backupSetbackData(long lsoId, long structureId) throws Exception {
		logger.info("backupSetbackData(" + lsoId + "," + structureId + ")");
		Wrapper db = new Wrapper();
		String sql = "insert into SITE_SETBACK_HISTORY select * from SITE_SETBACK where LSO_ID=" + lsoId + " AND SEQUENCE_NO=" + structureId;
		db.insert(sql);

	}

	public long getLandId(String level, long levelId) {
		String sql;
		RowSet rs;

		try {
			if (level.equals("L"))
				return levelId;

			sql = "";
			if (level.equals("S") || level.equals("O"))
				sql = "SELECT LAND_ID FROM V_LSO_LAND WHERE LSO_ID=" + levelId;
			else if (level.equals("A"))
				sql = "SELECT V.LAND_ID FROM (V_ALLADDRESS_LIST VA JOIN ACTIVITY A ON A.ADDR_ID=VA.ADDR_ID) JOIN V_LSO_LAND V ON V.LSO_ID=VA.LSO_ID WHERE A.ACT_ID=" + levelId;
			else if (level.equals("P"))
				sql = "SELECT V.LAND_ID FROM PROJECT P JOIN V_LSO_LAND V ON V.LSO_ID=P.LSO_ID WHERE P.PROJ_ID=" + levelId;
			else if (level.equals("Q"))
				sql = "select land_id from v_lso_land where lso_id = (select lso_id from project where proj_id in (select proj_id  from sub_project where sproj_id=" + levelId + "))";
			logger.debug(sql);

			Wrapper db = new Wrapper();
			rs = db.select(sql);
			if (rs.next()) {
				return rs.getLong("LAND_ID");
			}
		} catch (Exception e) {
			logger.error("Error in getLandId() :" + e.getMessage());
		}
		return 0;
	}

	public void setSiteData(SiteData siteData, String lsoId) {
		try {
			if (siteData != null) {
				logger.debug("Site Data is not null ");

				if (getSiteData(siteData.getLSOID()) != null) {
					updateSiteData(siteData);
					logger.debug("update of site data completed successfully ");
				} else {
					logger.debug("Before adding of Site data");
					addSiteData(siteData, lsoId);
					logger.debug("Site Data successfully added");
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void updateSiteData(SiteData siteData) {
		try {
			// code to update site data
			Wrapper db = new Wrapper();
			String sql = "";
			db.beginTransaction();
			logger.debug("Before creation of sql:  =" + siteData);
			if (siteData != null) {
				// Insert the Site Setback data
				siteData.getSetback().setLsoId(StringUtils.s2i(siteData.getLSOID()));
				addSetbackData(db, siteData);

				// Update the Site Survery Form Data
				sql = "UPDATE SITE_DATA SET ADDRESS =";
				sql += StringUtils.checkString(siteData.getADDRESS());
				logger.debug("got siteData ADDRESS " + StringUtils.checkString(siteData.getADDRESS()));
				sql += ", ALARMS =";
				sql += StringUtils.checkString(siteData.getALARMS());
				logger.debug("got siteData ALARMS " + StringUtils.checkString(siteData.getALARMS()));
				sql += ", AREAOFPARKING =";
				sql += StringUtils.checkString(siteData.getAREAOFPARKING());
				logger.debug("got siteData AREAOFPARKING " + StringUtils.checkString(siteData.getAREAOFPARKING()));
				sql += ", BLDGCONSTTYPECODE =";
				sql += StringUtils.checkString(siteData.getBLDGCONSTTYPECODE());
				logger.debug("got siteData BLDGCONSTTYPECODE " + StringUtils.checkString(siteData.getBLDGCONSTTYPECODE()));
				sql += ", BUILDINGNAME =";
				sql += StringUtils.checkString(siteData.getBUILDINGNAME());
				logger.debug("got siteData BUILDINGNAME " + StringUtils.checkString(siteData.getBUILDINGNAME()));
				sql += ", BUILDINGPERMITNO =";
				sql += StringUtils.checkString(siteData.getBUILDINGPERMITNO());
				logger.debug("got siteData BUILDINGPERMITNO " + StringUtils.checkString(siteData.getBUILDINGPERMITNO()));
				sql += ", CENSUSTRACT =";
				sql += StringUtils.checkString(siteData.getCENSUSTRACT());
				logger.debug("got siteData CENSUSTRACT " + StringUtils.checkString(siteData.getCENSUSTRACT()));
				sql += ", COMMENTPRIMARYSYSTEM =";
				sql += StringUtils.checkString(siteData.getCOMMENTPRIMARYSYSTEM());
				logger.debug("got siteData COMMENTPRIMARYSYSTEM " + StringUtils.checkString(siteData.getCOMMENTPRIMARYSYSTEM()));
				sql += ", COMMENTSECONDARYSYSTEM =";
				sql += StringUtils.checkString(siteData.getCOMMENTSECONDARYSYSTEM());
				logger.debug("got siteData COMMENTSECONDARYSYSTEM " + StringUtils.checkString(siteData.getCOMMENTSECONDARYSYSTEM()));
				sql += ", COVENANTOFFSITE =";
				sql += StringUtils.checkString(siteData.getCOVENANTOFFSITE());
				logger.debug("got siteData COVENANTOFFSITE " + StringUtils.checkString(siteData.getCOVENANTOFFSITE()));
				sql += ", COVENANTONSITE =";
				sql += StringUtils.checkString(siteData.getCOVENANTONSITE());
				logger.debug("got siteData COVENANTONSITE " + StringUtils.checkString(siteData.getCOVENANTONSITE()));
				sql += ", CURRENTPRIMARYUSE =";
				sql += StringUtils.checkString(siteData.getCURRENTPRIMARYUSE());
				logger.debug("got siteData CURRENTPRIMARYUSE " + StringUtils.checkString(siteData.getCURRENTPRIMARYUSE()));
				sql += ", DATEOFBLDGPERMIT =";
				sql += StringUtils.checkString(siteData.getDATEOFBLDGPERMIT());
				logger.debug("got siteData DATEOFBLDGPERMIT " + StringUtils.checkString(siteData.getDATEOFBLDGPERMIT()));
				sql += ", DESIGNCODE =";
				sql += StringUtils.checkString(siteData.getDESIGNCODE());
				logger.debug("got siteData DESIGNCODE " + StringUtils.checkString(siteData.getDESIGNCODE()));
				sql += ", DIR1 =";
				sql += StringUtils.checkString(siteData.getDIR1());
				logger.debug("got siteData DIR1 " + StringUtils.checkString(siteData.getDIR1()));
				sql += ", DIR2 =";
				sql += StringUtils.checkString(siteData.getDIR2());
				logger.debug("got siteData DIR2 " + StringUtils.checkString(siteData.getDIR2()));
				sql += ", ELEVATORENCLOSURE =";
				sql += StringUtils.checkString(siteData.getELEVATORENCLOSURE());
				logger.debug("got siteData ELEVATORENCLOSURE " + StringUtils.checkString(siteData.getELEVATORENCLOSURE()));
				sql += ", ESTIMATEOFWIDTHFEET =";
				sql += StringUtils.checkString(siteData.getESTIMATEOFWIDTHFEET());
				logger.debug("got siteData ESTIMATEOFWIDTHFEET " + StringUtils.checkString(siteData.getESTIMATEOFWIDTHFEET()));
				sql += ", ESTIMATEOFWIDTHINCHES =";
				sql += StringUtils.checkString(siteData.getESTIMATEOFWIDTHINCHES());
				logger.debug("got siteData ESTIMATEOFWIDTHINCHES " + StringUtils.checkString(siteData.getESTIMATEOFWIDTHINCHES()));
				sql += ", FLOODAREA =";
				sql += StringUtils.checkString(siteData.getFLOODAREA());
				logger.debug("got siteData FLOODAREA " + StringUtils.checkString(siteData.getFLOODAREA()));
				sql += ", FOLLOWUP =";
				sql += StringUtils.checkString(siteData.getFOLLOWUP());
				logger.debug("got siteData FOLLOWUP " + StringUtils.checkString(siteData.getFOLLOWUP()));
				sql += ", HAZARDOUSMATERIALS =";
				sql += StringUtils.checkString(siteData.getHAZARDOUSMATERIALS());
				logger.debug("got siteData HAZARDOUSMATERIALS " + StringUtils.checkString(siteData.getHAZARDOUSMATERIALS()));
				sql += ", HEIGHTFEET =";
				sql += StringUtils.checkString(siteData.getHEIGHTFEET());
				logger.debug("got siteData HEIGHTFEET " + StringUtils.checkString(siteData.getHEIGHTFEET()));
				sql += ", HEIGHTINCHES =";
				sql += StringUtils.checkString(siteData.getHEIGHTINCHES());
				logger.debug("got siteData HEIGHTINCHES " + StringUtils.checkString(siteData.getHEIGHTINCHES()));
				sql += ", HIGH1 =";
				sql += StringUtils.checkString(siteData.getHIGH1());
				logger.debug("got siteData HIGH1 " + StringUtils.checkString(siteData.getHIGH1()));
				sql += ", HIGH2 =";
				sql += StringUtils.checkString(siteData.getHIGH2());
				logger.debug("got siteData HIGH2 " + StringUtils.checkString(siteData.getHIGH2()));
				sql += ", HIGHFIRESEVERITYZONE =";
				sql += StringUtils.checkString(siteData.getHIGHFIRESEVERITYZONE());
				logger.debug("got siteData HIGHFIRESEVERITYZONE " + StringUtils.checkString(siteData.getHIGHFIRESEVERITYZONE()));
				sql += ", indx =";
				sql += StringUtils.checkString(siteData.getINDEX());
				logger.debug("got siteData indx " + StringUtils.checkString(siteData.getINDEX()));
				sql += ", INLIEUPARKING =";
				sql += StringUtils.checkString(siteData.getINLIEUPARKING());
				logger.debug("got siteData INLIEUPARKING " + StringUtils.checkString(siteData.getINLIEUPARKING()));
				sql += ", LIQUEFACTION =";
				sql += StringUtils.checkString(siteData.getLIQUEFACTION());
				logger.debug("got siteData LIQUEFACTION " + StringUtils.checkString(siteData.getLIQUEFACTION()));
				sql += ", LOCALFAULTZONE =";
				sql += StringUtils.checkString(siteData.getLOCALFAULTZONE());
				logger.debug("got siteData LOCALFAULTZONE " + StringUtils.checkString(siteData.getLOCALFAULTZONE()));
				sql += ", LOW1 =";
				sql += StringUtils.checkString(siteData.getLOW1());
				logger.debug("got siteData LOW1 " + StringUtils.checkString(siteData.getLOW1()));
				sql += ", LOW2 =";
				sql += StringUtils.checkString(siteData.getLOW2());
				logger.debug("got siteData LOW2 " + StringUtils.checkString(siteData.getLOW2()));
				sql += ", MAJORSTRUCTURALADDITIONS =";
				sql += StringUtils.checkString(siteData.getMAJORSTRUCTURALADDITIONS());
				logger.debug("got siteData MAJORSTRUCTURALADDITIONS " + StringUtils.checkString(siteData.getMAJORSTRUCTURALADDITIONS()));
				sql += ", MAJORSTRUCTURALALTERATIONS =";
				sql += StringUtils.checkString(siteData.getMAJORSTRUCTURALALTERATIONS());
				logger.debug("got siteData MAJORSTRUCTURALALTERATIONS " + StringUtils.checkString(siteData.getMAJORSTRUCTURALALTERATIONS()));
				sql += ", MEZZANINE =";
				sql += StringUtils.checkString(siteData.getMEZZANINE());
				logger.debug("got siteData MEZZANINE " + StringUtils.checkString(siteData.getMEZZANINE()));
				sql += ", NOOFELEVATORS =";
				sql += StringUtils.checkString(siteData.getNOOFELEVATORS());
				logger.debug("got siteData NOOFELEVATORS " + StringUtils.checkString(siteData.getNOOFELEVATORS()));
				sql += ", NOOFRESUNITS =";
				sql += StringUtils.checkString(siteData.getNOOFRESUNITS());
				logger.debug("got siteData NOOFRESUNITS " + StringUtils.checkString(siteData.getNOOFRESUNITS()));
				sql += ", NOOFSTORIESABOVEGRADE =";
				sql += StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADE());
				logger.debug("got siteData NOOFSTORIESABOVEGRADE " + StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADE()));
				sql += ", NOOFSTORIESABOVEGRADEZ =";
				sql += StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADEZ());
				logger.debug("got siteData NOOFSTORIESABOVEGRADEZ " + StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADEZ()));
				sql += ", NOOFSTORIESBELOWGRADE =";
				sql += StringUtils.checkString(siteData.getNOOFSTORIESBELOWGRADE());
				logger.debug("got siteData NOOFSTORIESBELOWGRADE " + StringUtils.checkString(siteData.getNOOFSTORIESBELOWGRADE()));
				sql += ", NOOFSTRUCTURESONSITE =";
				sql += StringUtils.checkString(siteData.getNOOFSTRUCTURESONSITE());
				logger.debug("got siteData NOOFSTRUCTURESONSITE " + StringUtils.checkString(siteData.getNOOFSTRUCTURESONSITE()));
				sql += ", NOTES =";
				sql += StringUtils.checkString(siteData.getNOTES());
				logger.debug("got siteData NOTES " + StringUtils.checkString(siteData.getNOTES()));
				sql += ", OCCUPANCY =";
				sql += StringUtils.checkString(siteData.getOCCUPANCY());
				logger.debug("got siteData OCCUPANCY " + StringUtils.checkString(siteData.getOCCUPANCY()));
				sql += ", OCCUPANTLOADCURRENT =";
				sql += StringUtils.checkString(siteData.getOCCUPANTLOADCURRENT());
				logger.debug("got siteData OCCUPANTLOADCURRENT " + StringUtils.checkString(siteData.getOCCUPANTLOADCURRENT()));
				sql += ", OCCUPANTLOADRECORDED =";
				sql += StringUtils.checkString(siteData.getOCCUPANTLOADRECORDED());
				logger.debug("got siteData OCCUPANTLOADRECORDED " + StringUtils.checkString(siteData.getOCCUPANTLOADRECORDED()));
				sql += ", ONGRADE =";
				sql += StringUtils.checkString(siteData.getONGRADE());
				logger.debug("got siteData ONGRADE " + StringUtils.checkString(siteData.getONGRADE()));
				sql += ", ONSITEPARKING =";
				sql += StringUtils.checkString(siteData.getONSITEPARKING());
				logger.debug("got siteData ONSITEPARKING " + StringUtils.checkString(siteData.getONSITEPARKING()));
				sql += ", OTHERZONINGDETERMINATIONS =";
				sql += StringUtils.checkString(siteData.getOTHERZONINGDETERMINATIONS());
				logger.debug("got siteData OTHERZONINGDETERMINATIONS " + StringUtils.checkString(siteData.getOTHERZONINGDETERMINATIONS()));
				sql += ", PARCELNO =";
				sql += StringUtils.checkString(siteData.getPARCELNO());
				logger.debug("got siteData PARCELNO " + StringUtils.checkString(siteData.getPARCELNO()));
				sql += ", PARCELNUMBER1 =";
				sql += StringUtils.checkString(siteData.getPARCELNUMBER1());
				logger.debug("got siteData PARCELNUMBER1 " + StringUtils.checkString(siteData.getPARCELNUMBER1()));
				sql += ", PARCELNUMBER2 =";
				sql += StringUtils.checkString(siteData.getPARCELNUMBER2());
				logger.debug("got siteData PARCELNUMBER2 " + StringUtils.checkString(siteData.getPARCELNUMBER2()));
				sql += ", PARCELNUMBER3 =";
				sql += StringUtils.checkString(siteData.getPARCELNUMBER3());
				logger.debug("got siteData PARCELNUMBER3 " + StringUtils.checkString(siteData.getPARCELNUMBER3()));
				sql += ", PARCELNUMBER4 =";
				sql += StringUtils.checkString(siteData.getPARCELNUMBER4());
				logger.debug("got siteData PARCELNUMBER4 " + StringUtils.checkString(siteData.getPARCELNUMBER4()));
				sql += ", PARCELNUMBER5 =";
				sql += StringUtils.checkString(siteData.getPARCELNUMBER5());
				logger.debug("got siteData PARCELNUMBER5 " + StringUtils.checkString(siteData.getPARCELNUMBER5()));
				sql += ", PARKINGPROVIDED =";
				sql += StringUtils.checkString(siteData.getPARKINGPROVIDED());
				logger.debug("got siteData PARKINGPROVIDED " + StringUtils.checkString(siteData.getPARKINGPROVIDED()));
				sql += ", PARKINGREQUIRED =";
				sql += StringUtils.checkString(siteData.getPARKINGREQUIRED());
				logger.debug("got siteData PARKINGREQUIRED " + StringUtils.checkString(siteData.getPARKINGREQUIRED()));
				sql += ", PBASICSCORE =";
				sql += StringUtils.checkString(siteData.getPBASICSCORE());
				logger.debug("got siteData PBASICSCORE " + StringUtils.checkString(siteData.getPBASICSCORE()));
				sql += ", PENTHOUSE =";
				sql += StringUtils.checkString(siteData.getPENTHOUSE());
				logger.debug("got siteData PENTHOUSE " + StringUtils.checkString(siteData.getPENTHOUSE()));
				sql += ", PFINALSCORE =";
				sql += StringUtils.checkString(siteData.getPFINALSCORE());
				logger.debug("got siteData PFINALSCORE " + StringUtils.checkString(siteData.getPFINALSCORE()));
				sql += ", PHIGHRISE =";
				sql += StringUtils.checkString(siteData.getPHIGHRISE());
				logger.debug("got siteData PHIGHRISE " + StringUtils.checkString(siteData.getPHIGHRISE()));
				sql += ", PLANSONFILE =";
				sql += StringUtils.checkString(siteData.getPLANSONFILE());
				logger.debug("got siteData PLANSONFILE " + StringUtils.checkString(siteData.getPLANSONFILE()));
				sql += ", PLARGEHEAVYCLDG =";
				sql += StringUtils.checkString(siteData.getPLARGEHEAVYCLDG());
				logger.debug("got siteData PLARGEHEAVYCLDG " + StringUtils.checkString(siteData.getPLARGEHEAVYCLDG()));
				sql += ", PPLANIRREGULARITY =";
				sql += StringUtils.checkString(siteData.getPPLANIRREGULARITY());
				logger.debug("got siteData PPLANIRREGULARITY " + StringUtils.checkString(siteData.getPPLANIRREGULARITY()));
				sql += ", PPOORCONDITION =";
				sql += StringUtils.checkString(siteData.getPPOORCONDITION());
				logger.debug("got siteData PPOORCONDITION " + StringUtils.checkString(siteData.getPPOORCONDITION()));
				sql += ", PPOSTBENCHMARK =";
				sql += StringUtils.checkString(siteData.getPPOSTBENCHMARK());
				logger.debug("got siteData PPOSTBENCHMARK " + StringUtils.checkString(siteData.getPPOSTBENCHMARK()));
				sql += ", PPOSTBENCHMARKYR =";
				sql += StringUtils.checkString(siteData.getPPOSTBENCHMARKYR());
				logger.debug("got siteData PPOSTBENCHMARKYR " + StringUtils.checkString(siteData.getPPOSTBENCHMARKYR()));
				sql += ", PPOUNDING =";
				sql += StringUtils.checkString(siteData.getPPOUNDING());
				logger.debug("got siteData PPOUNDING " + StringUtils.checkString(siteData.getPPOUNDING()));
				sql += ", PRETROFITTED =";
				sql += StringUtils.checkString(siteData.getPRETROFITTED());
				logger.debug("got siteData PRETROFITTED " + StringUtils.checkString(siteData.getPRETROFITTED()));
				sql += ", PRIMARYSYSTEM =";
				sql += StringUtils.checkString(siteData.getPRIMARYSYSTEM());
				logger.debug("got siteData PRIMARYSYSTEM " + StringUtils.checkString(siteData.getPRIMARYSYSTEM()));
				sql += ", PRIMARYUSEDATE =";
				sql += StringUtils.checkString(siteData.getPRIMARYUSEDATE());
				logger.debug("got siteData PRIMARYUSEDATE " + StringUtils.checkString(siteData.getPRIMARYUSEDATE()));
				sql += ", PSHORTCOLUMNS =";
				sql += StringUtils.checkString(siteData.getPSHORTCOLUMNS());
				logger.debug("got siteData PSHORTCOLUMNS " + StringUtils.checkString(siteData.getPSHORTCOLUMNS()));
				sql += ", PSL2 =";
				sql += StringUtils.checkString(siteData.getPSL2());
				logger.debug("got siteData PSL2 " + StringUtils.checkString(siteData.getPSL2()));
				sql += ", PSL3 =";
				sql += StringUtils.checkString(siteData.getPSL3());
				logger.debug("got siteData PSL3 " + StringUtils.checkString(siteData.getPSL3()));
				sql += ", PSOFTSTORY =";
				sql += StringUtils.checkString(siteData.getPSOFTSTORY());
				logger.debug("got siteData PSOFTSTORY " + StringUtils.checkString(siteData.getPSOFTSTORY()));
				sql += ", PTORSION =";
				sql += StringUtils.checkString(siteData.getPTORSION());
				logger.debug("got siteData PTORSION " + StringUtils.checkString(siteData.getPTORSION()));
				sql += ", PVERTICALIRREGULARITY =";
				sql += StringUtils.checkString(siteData.getPVERTICALIRREGULARITY());
				logger.debug("got siteData PVERTICALIRREGULARITY " + StringUtils.checkString(siteData.getPVERTICALIRREGULARITY()));
				sql += ", RETROFITTED =";
				sql += StringUtils.checkString(siteData.getRETROFITTED());
				logger.debug("got siteData RETROFITTED " + StringUtils.checkString(siteData.getRETROFITTED()));
				sql += ", SBASICSCORE =";
				sql += StringUtils.checkString(siteData.getSBASICSCORE());
				logger.debug("got siteData SBASICSCORE " + StringUtils.checkString(siteData.getSBASICSCORE()));
				sql += ", SECONDARYSYSTEM =";
				sql += StringUtils.checkString(siteData.getSECONDARYSYSTEM());
				logger.debug("got siteData SECONDARYSYSTEM " + StringUtils.checkString(siteData.getSECONDARYSYSTEM()));
				sql += ", SFINALSCORE =";
				sql += StringUtils.checkString(siteData.getSFINALSCORE());
				logger.debug("got siteData SFINALSCORE " + StringUtils.checkString(siteData.getSFINALSCORE()));
				sql += ", SHIGHRISE =";
				sql += StringUtils.checkString(siteData.getSHIGHRISE());
				logger.debug("got siteData SHIGHRISE " + StringUtils.checkString(siteData.getSHIGHRISE()));
				sql += ", SLARGEHEAVYCLDG =";
				sql += StringUtils.checkString(siteData.getSLARGEHEAVYCLDG());
				logger.debug("got siteData SLARGEHEAVYCLDG " + StringUtils.checkString(siteData.getSLARGEHEAVYCLDG()));
				sql += ", SPLANIRREGULARITY =";
				sql += StringUtils.checkString(siteData.getSPLANIRREGULARITY());
				logger.debug("got siteData SPLANIRREGULARITY " + StringUtils.checkString(siteData.getSPLANIRREGULARITY()));
				sql += ", SPOORCONDITION =";
				sql += StringUtils.checkString(siteData.getSPOORCONDITION());
				logger.debug("got siteData SPOORCONDITION " + StringUtils.checkString(siteData.getSPOORCONDITION()));
				sql += ", SPOSTBENCHMARK =";
				sql += StringUtils.checkString(siteData.getSPOSTBENCHMARK());
				logger.debug("got siteData SPOSTBENCHMARK " + StringUtils.checkString(siteData.getSPOSTBENCHMARK()));
				sql += ", SPOUNDING =";
				sql += StringUtils.checkString(siteData.getSPOUNDING());
				logger.debug("got siteData SPOUNDING " + StringUtils.checkString(siteData.getSPOUNDING()));
				sql += ", SPRINKLERED =";
				sql += StringUtils.checkString(siteData.getSPRINKLERED());
				logger.debug("got siteData SPRINKLERED " + StringUtils.checkString(siteData.getSPRINKLERED()));
				sql += ", SRETROFITTED =";
				sql += StringUtils.checkString(siteData.getSRETROFITTED());
				logger.debug("got siteData SRETROFITTED " + StringUtils.checkString(siteData.getSRETROFITTED()));
				sql += ", SSHORTCOLUMNS =";
				sql += StringUtils.checkString(siteData.getSSHORTCOLUMNS());
				logger.debug("got siteData SSHORTCOLUMNS " + StringUtils.checkString(siteData.getSSHORTCOLUMNS()));
				sql += ", SSL2 =";
				sql += StringUtils.checkString(siteData.getSSL2());
				logger.debug("got siteData SSL2 " + StringUtils.checkString(siteData.getSSL2()));
				sql += ", SSL3 =";
				sql += StringUtils.checkString(siteData.getSSL3());
				logger.debug("got siteData SSL3 " + StringUtils.checkString(siteData.getSSL3()));
				sql += ", SSOFTSTORY =";
				sql += StringUtils.checkString(siteData.getSSOFTSTORY());
				logger.debug("got siteData SSOFTSTORY " + StringUtils.checkString(siteData.getSSOFTSTORY()));
				sql += ", STORSION =";
				sql += StringUtils.checkString(siteData.getSTORSION());
				logger.debug("got siteData STORSION " + StringUtils.checkString(siteData.getSTORSION()));
				sql += ", STREETNAME1 =";
				sql += StringUtils.checkString(siteData.getSTREETNAME1());
				logger.debug("got siteData STREETNAME1 " + StringUtils.checkString(siteData.getSTREETNAME1()));
				sql += ", STREETNAME2 =";
				sql += StringUtils.checkString(siteData.getSTREETNAME2());
				logger.debug("got siteData STREETNAME2 " + StringUtils.checkString(siteData.getSTREETNAME2()));
				sql += ", STRIPED =";
				sql += StringUtils.checkString(siteData.getSTRIPED());
				logger.debug("got siteData STRIPED " + StringUtils.checkString(siteData.getSTRIPED()));
				sql += ", STRUCTADDRESS1 =";
				sql += StringUtils.checkString(siteData.getSTRUCTADDRESS1());
				logger.debug("got siteData STRUCTADDRESS1 " + StringUtils.checkString(siteData.getSTRUCTADDRESS1()));
				sql += ", STRUCTADDRESS2 =";
				sql += StringUtils.checkString(siteData.getSTRUCTADDRESS2());
				logger.debug("got siteData STRUCTADDRESS2 " + StringUtils.checkString(siteData.getSTRUCTADDRESS2()));
				sql += ", SUBTERRANEAN =";
				sql += StringUtils.checkString(siteData.getSUBTERRANEAN());
				logger.debug("got siteData SUBTERRANEAN " + StringUtils.checkString(siteData.getSUBTERRANEAN()));
				sql += ", SVERTICALIRREGULARITY =";
				sql += StringUtils.checkString(siteData.getSVERTICALIRREGULARITY());
				logger.debug("got siteData SVERTICALIRREGULARITY " + StringUtils.checkString(siteData.getSVERTICALIRREGULARITY()));
				sql += ", TOTALFLOORAREABLDGCODE =";
				sql += StringUtils.checkString(siteData.getTOTALFLOORAREABLDGCODE());
				logger.debug("got siteData TOTALFLOORAREABLDGCODE " + StringUtils.checkString(siteData.getTOTALFLOORAREABLDGCODE()));
				sql += ", TOTFLRAREAGRSWITHOUTPRKNG  =";
				sql += StringUtils.checkString(siteData.getTOTFLRAREAGRSWITHOUTPRKNG());
				logger.debug("got siteData TOTFLRAREAGRSWITHOUTPRKNG  " + StringUtils.checkString(siteData.getTOTFLRAREAGRSWITHOUTPRKNG()));
				sql += ", URM =";
				sql += StringUtils.checkString(siteData.getURM());
				logger.debug("got siteData URM " + StringUtils.checkString(siteData.getURM()));
				sql += ", WITHINSTRUCTURE =";
				sql += StringUtils.checkString(siteData.getWITHINSTRUCTURE());
				logger.debug("got siteData WITHINSTRUCTURE " + StringUtils.checkString(siteData.getWITHINSTRUCTURE()));
				sql += ", YEARPERMITAPPLIED =";
				sql += StringUtils.checkString(siteData.getYEARPERMITAPPLIED());
				logger.debug("got siteData YEARPERMITAPPLIED " + StringUtils.checkString(siteData.getYEARPERMITAPPLIED()));
				sql += ", ZONECURRENT =";
				sql += StringUtils.checkString(siteData.getZONECURRENT());
				logger.debug("got siteData ZONECURRENT " + StringUtils.checkString(siteData.getZONECURRENT()));
				sql += ", ZONEPERMIT =";
				sql += StringUtils.checkString(siteData.getZONEPERMIT());
				logger.debug("got siteData ZONEPERMIT " + StringUtils.checkString(siteData.getZONEPERMIT()));
				sql += " WHERE LSOID=";
				sql += siteData.getLSOID();
			}

			db.addBatch(sql);

			logger.debug("SiteData update sql is :" + sql);

			db.executeBatch();
			// db.update(sql);

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public SiteData getSiteData(String lsoId) {
		SiteData siteData = null;
		try {

			Wrapper db = new Wrapper();
			String sql = "SELECT * FROM SITE_DATA WHERE LSOID = " + lsoId;

			logger.debug(sql);
			RowSet rs = db.select(sql);
			siteData = new SiteData();
			if (rs.next()) {
				siteData.setLSOID(rs.getString("LSOID"));
				logger.debug("set LSOID");
				siteData.setADDRESS(rs.getString("ADDRESS"));
				logger.debug("set ADDRESS");
				siteData.setALARMS(rs.getString("ALARMS"));
				logger.debug("set ALARMS");
				siteData.setAREAOFPARKING(rs.getString("AREAOFPARKING"));
				logger.debug("set AREAOFPARKING");
				siteData.setBLDGCONSTTYPECODE(rs.getString("BLDGCONSTTYPECODE"));
				logger.debug("set BLDGCONSTTYPECODE");
				siteData.setBUILDINGNAME(rs.getString("BUILDINGNAME"));
				logger.debug("set BUILDINGNAME");
				siteData.setBUILDINGPERMITNO(rs.getString("BUILDINGPERMITNO"));
				logger.debug("set BUILDINGPERMITNO");
				siteData.setCENSUSTRACT(rs.getString("CENSUSTRACT"));
				logger.debug("set CENSUSTRACT");
				siteData.setCOMMENTPRIMARYSYSTEM(rs.getString("COMMENTPRIMARYSYSTEM"));
				logger.debug("set COMMENTPRIMARYSYSTEM");
				siteData.setCOMMENTSECONDARYSYSTEM(rs.getString("COMMENTSECONDARYSYSTEM"));
				logger.debug("set COMMENTSECONDARYSYSTEM");
				siteData.setCOVENANTOFFSITE(rs.getString("COVENANTOFFSITE"));
				logger.debug("set COVENANTOFFSITE");
				siteData.setCOVENANTONSITE(rs.getString("COVENANTONSITE"));
				logger.debug("set COVENANTONSITE");
				siteData.setCURRENTPRIMARYUSE(rs.getString("CURRENTPRIMARYUSE"));
				logger.debug("set CURRENTPRIMARYUSE");
				siteData.setDATEOFBLDGPERMIT(StringUtils.date2str(rs.getDate("DATEOFBLDGPERMIT")));
				logger.debug("set OFBLDGPERMIT");
				siteData.setDESIGNCODE(rs.getString("DESIGNCODE"));
				logger.debug("set DESIGNCODE");
				siteData.setDIR1(rs.getString("DIR1"));
				logger.debug("set DIR1");
				siteData.setDIR2(rs.getString("DIR2"));
				logger.debug("set DIR2");
				siteData.setELEVATORENCLOSURE(rs.getString("ELEVATORENCLOSURE"));
				logger.debug("set ELEVATORENCLOSURE");
				siteData.setESTIMATEOFWIDTHFEET(rs.getString("ESTIMATEOFWIDTHFEET"));
				logger.debug("set ESTIMATEOFWIDTHFEET");
				siteData.setESTIMATEOFWIDTHINCHES(rs.getString("ESTIMATEOFWIDTHINCHES"));
				logger.debug("set ESTIMATEOFWIDTHINCHES");
				siteData.setFLOODAREA(rs.getString("FLOODAREA"));
				logger.debug("set FLOODAREA");
				siteData.setFOLLOWUP(rs.getString("FOLLOWUP"));
				logger.debug("set FOLLOWUP");
				siteData.setHAZARDOUSMATERIALS(rs.getString("HAZARDOUSMATERIALS"));
				logger.debug("set HAZARDOUSMATERIALS");
				siteData.setHEIGHTFEET(rs.getString("HEIGHTFEET"));
				logger.debug("set HEIGHTFEET");
				siteData.setHEIGHTINCHES(rs.getString("HEIGHTINCHES"));
				logger.debug("set HEIGHTINCHES");
				siteData.setHIGH1(rs.getString("HIGH1"));
				logger.debug("set HIGH1");
				siteData.setHIGH2(rs.getString("HIGH2"));
				logger.debug("set HIGH2");
				siteData.setHIGHFIRESEVERITYZONE(rs.getString("HIGHFIRESEVERITYZONE"));
				logger.debug("set HIGHFIRESEVERITYZONE");
				siteData.setINDEX(rs.getString("indx"));
				logger.debug("set indx");
				siteData.setINLIEUPARKING(rs.getString("INLIEUPARKING"));
				logger.debug("set INLIEUPARKING");
				siteData.setLIQUEFACTION(rs.getString("LIQUEFACTION"));
				logger.debug("set LIQUEFACTION");
				siteData.setLOCALFAULTZONE(rs.getString("LOCALFAULTZONE"));
				logger.debug("set LOCALFAULTZONE");
				siteData.setLOW1(rs.getString("LOW1"));
				logger.debug("set LOW1");
				siteData.setLOW2(rs.getString("LOW2"));
				logger.debug("set LOW2");
				siteData.setMAJORSTRUCTURALADDITIONS(rs.getString("MAJORSTRUCTURALADDITIONS"));
				logger.debug("set MAJORSTRUCTURALADDITIONS");
				siteData.setMAJORSTRUCTURALALTERATIONS(rs.getString("MAJORSTRUCTURALALTERATIONS"));
				logger.debug("set MAJORSTRUCTURALALTERATIONS");
				siteData.setMEZZANINE(rs.getString("MEZZANINE"));
				logger.debug("set MEZZANINE");
				siteData.setNOOFELEVATORS(rs.getString("NOOFELEVATORS"));
				logger.debug("set NOOFELEVATORS");
				siteData.setNOOFRESUNITS(rs.getString("NOOFRESUNITS"));
				logger.debug("set NOOFRESUNITS");
				siteData.setNOOFSTORIESABOVEGRADE(rs.getString("NOOFSTORIESABOVEGRADE"));
				logger.debug("set NOOFSTORIESABOVEGRADE");
				siteData.setNOOFSTORIESABOVEGRADEZ(rs.getString("NOOFSTORIESABOVEGRADEZ"));
				logger.debug("set NOOFSTORIESABOVEGRADEZ");
				siteData.setNOOFSTORIESBELOWGRADE(rs.getString("NOOFSTORIESBELOWGRADE"));
				logger.debug("set NOOFSTORIESBELOWGRADE");
				siteData.setNOOFSTRUCTURESONSITE(rs.getString("NOOFSTRUCTURESONSITE"));
				logger.debug("set NOOFSTRUCTURESONSITE");
				siteData.setNOTES(rs.getString("NOTES"));
				logger.debug("set NOTES");
				siteData.setOCCUPANCY(rs.getString("OCCUPANCY"));
				logger.debug("set OCCUPANCY");
				siteData.setOCCUPANTLOADCURRENT(rs.getString("OCCUPANTLOADCURRENT"));
				logger.debug("set OCCUPANTLOADCURRENT");
				siteData.setOCCUPANTLOADRECORDED(rs.getString("OCCUPANTLOADRECORDED"));
				logger.debug("set OCCUPANTLOADRECORDED");
				siteData.setONGRADE(rs.getString("ONGRADE"));
				logger.debug("set ONGRADE");
				siteData.setONSITEPARKING(rs.getString("ONSITEPARKING"));
				logger.debug("set ONSITEPARKING");
				siteData.setOTHERZONINGDETERMINATIONS(rs.getString("OTHERZONINGDETERMINATIONS"));
				logger.debug("set OTHERZONINGDETERMINATIONS");
				siteData.setPARCELNO(rs.getString("PARCELNO"));
				logger.debug("set PARCELNO");
				siteData.setPARCELNUMBER1(rs.getString("PARCELNUMBER1"));
				logger.debug("set PARCELNUMBER1");
				siteData.setPARCELNUMBER2(rs.getString("PARCELNUMBER2"));
				logger.debug("set PARCELNUMBER2");
				siteData.setPARCELNUMBER3(rs.getString("PARCELNUMBER3"));
				logger.debug("set PARCELNUMBER3");
				siteData.setPARCELNUMBER4(rs.getString("PARCELNUMBER4"));
				logger.debug("set PARCELNUMBER4");
				siteData.setPARCELNUMBER5(rs.getString("PARCELNUMBER5"));
				logger.debug("set PARCELNUMBER5");
				siteData.setPARKINGPROVIDED(rs.getString("PARKINGPROVIDED"));
				logger.debug("set PARKINGPROVIDED");
				siteData.setPARKINGREQUIRED(rs.getString("PARKINGREQUIRED"));
				logger.debug("set PARKINGREQUIRED");
				siteData.setPBASICSCORE(rs.getString("PBASICSCORE"));
				logger.debug("set PBASICSCORE");
				siteData.setPENTHOUSE(rs.getString("PENTHOUSE"));
				logger.debug("set PENTHOUSE");
				siteData.setPFINALSCORE(rs.getString("PFINALSCORE"));
				logger.debug("set PFINALSCORE");
				siteData.setPHIGHRISE(rs.getString("PHIGHRISE"));
				logger.debug("set PHIGHRISE");
				siteData.setPLANSONFILE(rs.getString("PLANSONFILE"));
				logger.debug("set PLANSONFILE");
				siteData.setPLARGEHEAVYCLDG(rs.getString("PLARGEHEAVYCLDG"));
				logger.debug("set PLARGEHEAVYCLDG");
				siteData.setPPLANIRREGULARITY(rs.getString("PPLANIRREGULARITY"));
				logger.debug("set PPLANIRREGULARITY");
				siteData.setPPOORCONDITION(rs.getString("PPOORCONDITION"));
				logger.debug("set PPOORCONDITION");
				siteData.setPPOSTBENCHMARK(rs.getString("PPOSTBENCHMARK"));
				logger.debug("set PPOSTBENCHMARK");
				siteData.setPPOSTBENCHMARKYR(rs.getString("PPOSTBENCHMARKYR"));
				logger.debug("set PPOSTBENCHMARKYR");
				siteData.setPPOUNDING(rs.getString("PPOUNDING"));
				logger.debug("set PPOUNDING");
				siteData.setPRETROFITTED(rs.getString("PRETROFITTED"));
				logger.debug("set PRETROFITTED");
				siteData.setPRIMARYSYSTEM(rs.getString("PRIMARYSYSTEM"));
				logger.debug("set PRIMARYSYSTEM");
				siteData.setPRIMARYUSEDATE(StringUtils.date2str(rs.getDate("PRIMARYUSEDATE")));
				logger.debug("set PRIMARYUSE");
				siteData.setPSHORTCOLUMNS(rs.getString("PSHORTCOLUMNS"));
				logger.debug("set PSHORTCOLUMNS");
				siteData.setPSL2(rs.getString("PSL2"));
				logger.debug("set PSL2");
				siteData.setPSL3(rs.getString("PSL3"));
				logger.debug("set PSL3");
				siteData.setPSOFTSTORY(rs.getString("PSOFTSTORY"));
				logger.debug("set PSOFTSTORY");
				siteData.setPTORSION(rs.getString("PTORSION"));
				logger.debug("set PTORSION");
				siteData.setPVERTICALIRREGULARITY(rs.getString("PVERTICALIRREGULARITY"));
				logger.debug("set PVERTICALIRREGULARITY");
				siteData.setRETROFITTED(rs.getString("RETROFITTED"));
				logger.debug("set RETROFITTED");
				siteData.setSBASICSCORE(rs.getString("SBASICSCORE"));
				logger.debug("set SBASICSCORE");
				siteData.setSECONDARYSYSTEM(rs.getString("SECONDARYSYSTEM"));
				logger.debug("set SECONDARYSYSTEM");
				siteData.setSFINALSCORE(rs.getString("SFINALSCORE"));
				logger.debug("set SFINALSCORE");
				siteData.setSHIGHRISE(rs.getString("SHIGHRISE"));
				logger.debug("set SHIGHRISE");
				siteData.setSLARGEHEAVYCLDG(rs.getString("SLARGEHEAVYCLDG"));
				logger.debug("set SLARGEHEAVYCLDG");
				siteData.setSPLANIRREGULARITY(rs.getString("SPLANIRREGULARITY"));
				logger.debug("set SPLANIRREGULARITY");
				siteData.setSPOORCONDITION(rs.getString("SPOORCONDITION"));
				logger.debug("set SPOORCONDITION");
				siteData.setSPOSTBENCHMARK(rs.getString("SPOSTBENCHMARK"));
				logger.debug("set SPOSTBENCHMARK");
				siteData.setSPOUNDING(rs.getString("SPOUNDING"));
				logger.debug("set SPOUNDING");
				siteData.setSPRINKLERED(rs.getString("SPRINKLERED"));
				logger.debug("set SPRINKLERED");
				siteData.setSRETROFITTED(rs.getString("SRETROFITTED"));
				logger.debug("set SRETROFITTED");
				siteData.setSSHORTCOLUMNS(rs.getString("SSHORTCOLUMNS"));
				logger.debug("set SSHORTCOLUMNS");
				siteData.setSSL2(rs.getString("SSL2"));
				logger.debug("set SSL2");
				siteData.setSSL3(rs.getString("SSL3"));
				logger.debug("set SSL3");
				siteData.setSSOFTSTORY(rs.getString("SSOFTSTORY"));
				logger.debug("set SSOFTSTORY");
				siteData.setSTORSION(rs.getString("STORSION"));
				logger.debug("set STORSION");
				siteData.setSTREETNAME1(rs.getString("STREETNAME1"));
				logger.debug("set STREETNAME1");
				siteData.setSTREETNAME2(rs.getString("STREETNAME2"));
				logger.debug("set STREETNAME2");
				siteData.setSTRIPED(rs.getString("STRIPED"));
				logger.debug("set STRIPED");
				siteData.setSTRUCTADDRESS1(rs.getString("STRUCTADDRESS1"));
				logger.debug("set STRUCTADDRESS1");
				siteData.setSTRUCTADDRESS2(rs.getString("STRUCTADDRESS2"));
				logger.debug("set STRUCTADDRESS2");
				siteData.setSUBTERRANEAN(rs.getString("SUBTERRANEAN"));
				logger.debug("set SUBTERRANEAN");
				siteData.setSVERTICALIRREGULARITY(rs.getString("SVERTICALIRREGULARITY"));
				logger.debug("set SVERTICALIRREGULARITY");
				siteData.setTOTALFLOORAREABLDGCODE(rs.getString("TOTALFLOORAREABLDGCODE"));
				logger.debug("set TOTALFLOORAREABLDGCODE");
				siteData.setTOTFLRAREAGRSWITHOUTPRKNG(rs.getString("TOTFLRAREAGRSWITHOUTPRKNG"));
				logger.debug("set TOTFLRAREAGRSWITHOUTPRKNG");
				siteData.setURM(rs.getString("URM"));
				logger.debug("set URM");
				siteData.setWITHINSTRUCTURE(rs.getString("WITHINSTRUCTURE"));
				logger.debug("set WITHINSTRUCTURE");
				siteData.setYEARPERMITAPPLIED(rs.getString("YEARPERMITAPPLIED"));
				logger.debug("set YEARPERMITAPPLIED");
				siteData.setZONECURRENT(rs.getString("ZONECURRENT"));
				logger.debug("set ZONECURRENT");
				siteData.setZONEPERMIT(rs.getString("ZONEPERMIT"));
				logger.debug("set ZONEPERMIT");
			}
			if (rs != null)
				rs.close();

			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
			sql = "SELECT * FROM SITE_SETBACK WHERE LSO_ID=" + lsoId + " ORDER BY SETBACK_ID desc";
			logger.debug(sql);

			rs = db.select(sql);
			int i = 0;
			while (rs.next()) {
				SiteSetback setback = new SiteSetback();
				setback.setSetbackId(rs.getInt("SETBACK_ID"));
				setback.setLsoId(rs.getInt("LSO_ID"));
				setback.setFrontFt(rs.getString("FRONT_FT"));
				setback.setFrontIn(rs.getString("FRONT_IN"));
				setback.setFrontComment(rs.getString("FRONT_COMMENT"));
				setback.setRearFt(rs.getString("REAR_FT"));
				setback.setRearIn(rs.getString("REAR_IN"));
				setback.setRearComment(rs.getString("REAR_COMMENT"));
				setback.setSide1Ft(rs.getString("SIDE1_FT"));
				setback.setSide1In(rs.getString("SIDE1_IN"));
				setback.setSide1Dir(rs.getString("SIDE1_DIR"));
				setback.setSide1Comment(rs.getString("SIDE1_COMMENT"));
				setback.setSide2Ft(rs.getString("SIDE2_FT"));
				setback.setSide2In(rs.getString("SIDE2_IN"));
				setback.setSide2Dir(rs.getString("SIDE2_DIR"));
				setback.setSide2Comment(rs.getString("SIDE2_COMMENT"));
				setback.setComment(rs.getString("comments"));
				setback.setUpdated(dateFormat.format(rs.getDate("UPDATED")));
				siteData.setSetback(setback);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return siteData;
	}

	public void addSiteData(SiteData siteData, String lsoId) {

		try {
			Wrapper db = new Wrapper();

			if (siteData != null) {
				db.beginTransaction();

				// Insert the Site Setback data
				siteData.getSetback().setLsoId(StringUtils.s2i(lsoId));
				addSetbackData(db, siteData);

				// Insert the Site Survey Form Data
				StringBuffer sql = new StringBuffer("insert into SITE_DATA (lsoid, address, alarms, areaOfParking, bldgConstTypeCode, buildingName, buildingPermitNo, censusTract, commentPrimarySystem, commentSecondarySystem, covenantOffSite, covenantOnSite, currentPrimaryUse, dateOfBldgPermit, designCode, dir1, dir2, elevatorEnclosure, estimateOfWidthFeet, estimateOfWidthInches, floodArea, followUp, hazardousMaterials, heightFeet, heightInches, high1, high2, highFireSeverityZone, indx, inLieuParking, liquefaction, localFaultZone, low1, low2, majorStructuralAdditions, majorStructuralAlterations, mezzanine, noOfElevators, noOfResUnits, noOfStoriesAboveGrade, noOfStoriesAboveGradeZ, noOfStoriesBelowGrade, noOfStructuresOnSite, notes, occupancy, occupantLoadCurrent, occupantLoadRecorded, onGrade, onsiteParking, otherZoningDeterminations, parcelNo, parcelNumber1, parcelNumber2, parcelNumber3, parcelNumber4, parcelNumber5, parkingProvided, parkingRequired, pBasicScore, penthouse, pFinalScore, pHighRise, plansOnFile, pLargeHeavyCldg, pPlanIrregularity, pPoorCondition, pPostBenchmark, pPostBenchmarkYr, pPounding, pRetrofitted, primarySystem, primaryUsedate, pShortColumns, pSL2, pSL3, pSoftStory, pTorsion, pVerticalIrregularity, retrofitted, sbasicScore, secondarySystem, sFinalScore, sHighRise, sLargeHeavyCldg, sPlanIrregularity, sPoorCondition, sPostBenchmark, sPounding, sprinklered, sRetrofitted, sShortColumns, sSL2, sSL3, sSoftStory, sTorsion, streetName1, streetName2, striped, structAddress1, structAddress2, subterranean, sVerticalIrregularity, totalFloorAreaBldgCode, totFlrAreaGrsWithoutPrkng , urm, withinStructure, yearPermitApplied, zoneCurrent, zonePermit) values (");
				sql.append(lsoId.trim());
				logger.debug("got lso id " + lsoId);
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getADDRESS()));
				logger.debug("got siteData address" + siteData.getADDRESS());
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getALARMS()));
				logger.debug("got siteData alarms " + StringUtils.checkString(siteData.getALARMS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getAREAOFPARKING()));
				logger.debug("got SiteData area of parking " + StringUtils.checkString(siteData.getAREAOFPARKING()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getBLDGCONSTTYPECODE()));
				logger.debug("got SiteData bldg construction  Code " + StringUtils.checkString(siteData.getBLDGCONSTTYPECODE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getBUILDINGNAME()));
				logger.debug("got  SiteData bldg name " + StringUtils.checkString(siteData.getBUILDINGNAME()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getBUILDINGPERMITNO()));
				logger.debug("got SiteData  bldg permit no " + StringUtils.checkString(siteData.getBUILDINGPERMITNO()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getCENSUSTRACT()));
				logger.debug("got  SiteData census tract  " + StringUtils.checkString(siteData.getCENSUSTRACT()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getCOMMENTPRIMARYSYSTEM()));
				logger.debug("got SiteData  comments primary system " + StringUtils.checkString(siteData.getCOMMENTPRIMARYSYSTEM()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getCOMMENTSECONDARYSYSTEM()));
				logger.debug("got  SiteData comments secondary system " + StringUtils.checkString(siteData.getCOMMENTSECONDARYSYSTEM()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getCOVENANTOFFSITE()));
				logger.debug("got  SiteData covenant offsite" + StringUtils.checkString(siteData.getCOVENANTOFFSITE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getCOVENANTONSITE()));
				logger.debug("got  SiteData covenant onsite" + StringUtils.checkString(siteData.getCOVENANTONSITE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getCURRENTPRIMARYUSE()));
				logger.debug("got  SiteData current primary use  " + StringUtils.checkString(siteData.getCURRENTPRIMARYUSE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getDATEOFBLDGPERMIT()));
				logger.debug("got  SiteData date of bldgpermit " + StringUtils.checkString(siteData.getDATEOFBLDGPERMIT()));
				sql.append(",");

				sql.append(StringUtils.checkString(siteData.getDESIGNCODE()));
				logger.debug("got  SiteData design code" + StringUtils.checkString(siteData.getDESIGNCODE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getDIR1()));
				logger.debug("got  SiteData dir1 " + StringUtils.checkString(siteData.getDIR1()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getDIR2()));
				logger.debug("got  SiteData dir2 " + StringUtils.checkString(siteData.getDIR2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getELEVATORENCLOSURE()));
				logger.debug("got  SiteData elevator enclosure " + StringUtils.checkString(siteData.getELEVATORENCLOSURE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getESTIMATEOFWIDTHFEET()));
				logger.debug("got  SiteData estimatedof width feet" + StringUtils.checkString(siteData.getESTIMATEOFWIDTHFEET()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getESTIMATEOFWIDTHINCHES()));
				logger.debug("got  SiteData estimated width inches" + StringUtils.checkString(siteData.getESTIMATEOFWIDTHINCHES()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getFLOODAREA()));
				logger.debug("got  SiteData flood area " + StringUtils.checkString(siteData.getFLOODAREA()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getFOLLOWUP()));
				logger.debug("got  SiteData followup " + StringUtils.checkString(siteData.getFOLLOWUP()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getHAZARDOUSMATERIALS()));
				logger.debug("got  SiteData hazardous material" + StringUtils.checkString(siteData.getHAZARDOUSMATERIALS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getHEIGHTFEET()));
				logger.debug("got  SiteData height feet " + StringUtils.checkString(siteData.getHEIGHTFEET()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getHEIGHTINCHES()));
				logger.debug("got  SiteData height inches " + StringUtils.checkString(siteData.getHEIGHTINCHES()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getHIGH1()));
				logger.debug("got  SiteData high1" + StringUtils.checkString(siteData.getHIGH1()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getHIGH2()));
				logger.debug("got  SiteData high2 " + StringUtils.checkString(siteData.getHIGH2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getHIGHFIRESEVERITYZONE()));
				logger.debug("got  SiteData high fire severity zone" + StringUtils.checkString(siteData.getHIGHFIRESEVERITYZONE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getINDEX()));
				logger.debug("got  SiteData indx " + StringUtils.checkString(siteData.getINDEX()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getINLIEUPARKING()));
				logger.debug("got  SiteData in liew parking " + StringUtils.checkString(siteData.getINLIEUPARKING()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getLIQUEFACTION()));
				logger.debug("got  SiteData liquefaction " + StringUtils.checkString(siteData.getLIQUEFACTION()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getLOCALFAULTZONE()));
				logger.debug("got  SiteData local faultzone " + StringUtils.checkString(siteData.getLOCALFAULTZONE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getLOW1()));
				logger.debug("got  SiteData low1" + StringUtils.checkString(siteData.getLOW1()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getLOW2()));
				logger.debug("got  SiteData low2 " + StringUtils.checkString(siteData.getLOW2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getMAJORSTRUCTURALADDITIONS()));
				logger.debug("got  SiteData major structureal additions " + StringUtils.checkString(siteData.getMAJORSTRUCTURALADDITIONS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getMAJORSTRUCTURALALTERATIONS()));
				logger.debug("got  SiteData major structural alterations " + StringUtils.checkString(siteData.getMAJORSTRUCTURALALTERATIONS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getMEZZANINE()));
				logger.debug("got  SiteData mezzanine " + StringUtils.checkString(siteData.getMEZZANINE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOOFELEVATORS()));
				logger.debug("got  SiteData no of elevators" + StringUtils.checkString(siteData.getNOOFELEVATORS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOOFRESUNITS()));
				logger.debug("got  SiteData no of res units" + StringUtils.checkString(siteData.getNOOFRESUNITS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADE()));
				logger.debug("got  SiteData no of stories above grade " + StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADEZ()));
				logger.debug("got  SiteData noof stories above gradez " + StringUtils.checkString(siteData.getNOOFSTORIESABOVEGRADEZ()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOOFSTORIESBELOWGRADE()));
				logger.debug("got  SiteData no of stories below grade" + StringUtils.checkString(siteData.getNOOFSTORIESBELOWGRADE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOOFSTRUCTURESONSITE()));
				logger.debug("got  SiteData no of structures onsite " + StringUtils.checkString(siteData.getNOOFSTRUCTURESONSITE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getNOTES()));
				logger.debug("got  SiteData notes" + StringUtils.checkString(siteData.getNOTES()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getOCCUPANCY()));
				logger.debug("got  SiteData occupancy" + StringUtils.checkString(siteData.getOCCUPANCY()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getOCCUPANTLOADCURRENT()));
				logger.debug("got  SiteData occ load current" + StringUtils.checkString(siteData.getOCCUPANTLOADCURRENT()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getOCCUPANTLOADRECORDED()));
				logger.debug("got  SiteData  occ load recarded" + StringUtils.checkString(siteData.getOCCUPANTLOADRECORDED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getONGRADE()));
				logger.debug("got  SiteData on grade" + StringUtils.checkString(siteData.getONGRADE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getONSITEPARKING()));
				logger.debug("got  SiteData onsite parking" + StringUtils.checkString(siteData.getONSITEPARKING()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getOTHERZONINGDETERMINATIONS()));
				logger.debug("got  SiteData otherzoning determinations " + StringUtils.checkString(siteData.getOTHERZONINGDETERMINATIONS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARCELNO()));
				logger.debug("got  SiteData parcel no " + StringUtils.checkString(siteData.getPARCELNO()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARCELNUMBER1()));
				logger.debug("got  SiteData parcelno1" + StringUtils.checkString(siteData.getPARCELNUMBER1()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARCELNUMBER2()));
				logger.debug("got  SiteData parcelno2" + StringUtils.checkString(siteData.getPARCELNUMBER2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARCELNUMBER3()));
				logger.debug("got  SiteData parcelno3" + StringUtils.checkString(siteData.getPARCELNUMBER3()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARCELNUMBER4()));
				logger.debug("got  SiteData parcelno4" + StringUtils.checkString(siteData.getPARCELNUMBER4()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARCELNUMBER5()));
				logger.debug("got  SiteData parcelno5" + StringUtils.checkString(siteData.getPARCELNUMBER5()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPARKINGPROVIDED()));
				logger.debug("got  SiteData parking provided" + StringUtils.checkString(siteData.getPARKINGPROVIDED()));
				sql.append(",");

				sql.append(StringUtils.checkString(siteData.getPARKINGREQUIRED()));
				logger.debug("got  SiteData parking required " + StringUtils.checkString(siteData.getPARKINGREQUIRED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPBASICSCORE()));
				logger.debug("got  SiteData pbasic score" + StringUtils.checkString(siteData.getPBASICSCORE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPENTHOUSE()));
				logger.debug("got  SiteData ppenthouse" + StringUtils.checkString(siteData.getPENTHOUSE()));
				sql.append(",");

				sql.append(StringUtils.checkString(siteData.getPFINALSCORE()));
				logger.debug("got  SiteData pfinal score" + StringUtils.checkString(siteData.getPFINALSCORE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPHIGHRISE()));
				logger.debug("got  SiteData phigh rise " + StringUtils.checkString(siteData.getPHIGHRISE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPLANSONFILE()));
				logger.debug("got  SiteData pplansonfile" + StringUtils.checkString(siteData.getPLANSONFILE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPLARGEHEAVYCLDG()));
				logger.debug("got  SiteData plargeheavy cldg " + StringUtils.checkString(siteData.getPLARGEHEAVYCLDG()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPPLANIRREGULARITY()));
				logger.debug("got  SiteData pplan iiregularity" + StringUtils.checkString(siteData.getPPLANIRREGULARITY()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPPOORCONDITION()));
				logger.debug("got  SiteData ppoorcondition" + StringUtils.checkString(siteData.getPPOORCONDITION()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPPOSTBENCHMARK()));
				logger.debug("got  SiteData ppostbenchmark" + StringUtils.checkString(siteData.getPPOSTBENCHMARK()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPPOSTBENCHMARKYR()));
				logger.debug("got  SiteData ppostbenchmark yr " + StringUtils.checkString(siteData.getPPOSTBENCHMARKYR()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPPOUNDING()));
				logger.debug("got  SiteData ppounding " + StringUtils.checkString(siteData.getPPOUNDING()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPRETROFITTED()));
				logger.debug("got  SiteData pretrofitted" + StringUtils.checkString(siteData.getPRETROFITTED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPRIMARYSYSTEM()));
				logger.debug("got  SiteData pprimary system" + StringUtils.checkString(siteData.getPRIMARYSYSTEM()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPRIMARYUSEDATE()));
				logger.debug("got  SiteData pprimary usedate" + StringUtils.checkString(siteData.getPRIMARYUSEDATE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPSHORTCOLUMNS()));
				logger.debug("got  SiteData pshortcloumns" + StringUtils.checkString(siteData.getPSHORTCOLUMNS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPSL2()));
				logger.debug("got  SiteData psl2 " + StringUtils.checkString(siteData.getPSL2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPSL3()));
				logger.debug("got  SiteData psl3 " + StringUtils.checkString(siteData.getPSL3()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPSOFTSTORY()));
				logger.debug("got  SiteData psoftstory" + StringUtils.checkString(siteData.getPSOFTSTORY()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPTORSION()));
				logger.debug("got  SiteData ptorsion" + StringUtils.checkString(siteData.getPTORSION()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getPVERTICALIRREGULARITY()));
				logger.debug("got  SiteData pvertical irregularity" + StringUtils.checkString(siteData.getPVERTICALIRREGULARITY()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getRETROFITTED()));
				logger.debug("got  SiteData retrofitted " + StringUtils.checkString(siteData.getRETROFITTED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSBASICSCORE()));
				logger.debug("got  SiteData sbasicscore" + StringUtils.checkString(siteData.getSBASICSCORE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSECONDARYSYSTEM()));
				logger.debug("got  SiteData secondary system" + StringUtils.checkString(siteData.getSECONDARYSYSTEM()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSFINALSCORE()));
				logger.debug("got  SiteData sfinalscore " + StringUtils.checkString(siteData.getSFINALSCORE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSHIGHRISE()));
				logger.debug("got  SiteData shighrise" + StringUtils.checkString(siteData.getSHIGHRISE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSLARGEHEAVYCLDG()));
				logger.debug("got  SiteData slargeheavy cldg " + StringUtils.checkString(siteData.getSLARGEHEAVYCLDG()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSPLANIRREGULARITY()));
				logger.debug("got  SiteData splan irregularity" + StringUtils.checkString(siteData.getSPLANIRREGULARITY()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSPOORCONDITION()));
				logger.debug("got  SiteData spoor condition" + StringUtils.checkString(siteData.getSPOORCONDITION()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSPOSTBENCHMARK()));
				logger.debug("got  SiteData sposted benchmark" + StringUtils.checkString(siteData.getSPOSTBENCHMARK()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSPOUNDING()));
				logger.debug("got  SiteData spounding" + StringUtils.checkString(siteData.getSPOUNDING()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSPRINKLERED()));
				logger.debug("got  SiteData ssprinkered" + StringUtils.checkString(siteData.getSPRINKLERED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSRETROFITTED()));
				logger.debug("got  SiteData sretrofitted " + StringUtils.checkString(siteData.getSRETROFITTED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSSHORTCOLUMNS()));
				logger.debug("got  SiteData sshort columns " + StringUtils.checkString(siteData.getSSHORTCOLUMNS()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSSL2()));
				logger.debug("got  SiteData ssl2 " + StringUtils.checkString(siteData.getSSL2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSSL3()));
				logger.debug("got  SiteData ssl3 " + StringUtils.checkString(siteData.getSSL3()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSSOFTSTORY()));
				logger.debug("got  SiteData ssoftstory" + StringUtils.checkString(siteData.getSSOFTSTORY()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSTORSION()));
				logger.debug("got  SiteData storsion" + StringUtils.checkString(siteData.getSTORSION()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSTREETNAME1()));
				logger.debug("got  SiteData streetname1 " + StringUtils.checkString(siteData.getSTREETNAME1()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSTREETNAME2()));
				logger.debug("got  SiteData streetname2" + StringUtils.checkString(siteData.getSTREETNAME2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSTRIPED()));
				logger.debug("got  SiteData striped" + StringUtils.checkString(siteData.getSTRIPED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSTRUCTADDRESS1()));
				logger.debug("got  SiteData structaddress1" + StringUtils.checkString(siteData.getSTRUCTADDRESS1()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSTRUCTADDRESS2()));
				logger.debug("got  SiteData structaddress2" + StringUtils.checkString(siteData.getSTRUCTADDRESS2()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSUBTERRANEAN()));
				logger.debug("got  SiteData subterranean" + StringUtils.checkString(siteData.getSUBTERRANEAN()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getSVERTICALIRREGULARITY()));
				logger.debug("got  SiteData svertical irregularity" + StringUtils.checkString(siteData.getSVERTICALIRREGULARITY()));
				sql.append(",");

				sql.append(StringUtils.checkString(siteData.getTOTALFLOORAREABLDGCODE()));
				logger.debug("got  SiteData totalfloorareabldgcode " + StringUtils.checkString(siteData.getTOTALFLOORAREABLDGCODE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getTOTFLRAREAGRSWITHOUTPRKNG()));
				logger.debug("got  SiteData totflr area gross without parking " + StringUtils.checkString(siteData.getTOTFLRAREAGRSWITHOUTPRKNG()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getURM()));
				logger.debug("got  SiteData urm" + StringUtils.checkString(siteData.getURM()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getWITHINSTRUCTURE()));
				logger.debug("got  SiteData withinstructure " + StringUtils.checkString(siteData.getWITHINSTRUCTURE()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getYEARPERMITAPPLIED()));
				logger.debug("got  SiteData yearpermitapplied" + StringUtils.checkString(siteData.getYEARPERMITAPPLIED()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getZONECURRENT()));
				logger.debug("got  SiteData zonecurrent " + StringUtils.checkString(siteData.getZONECURRENT()));
				sql.append(",");
				sql.append(StringUtils.checkString(siteData.getZONEPERMIT()));
				logger.debug("got  SiteData zonepermit " + StringUtils.checkString(siteData.getZONEPERMIT()));
				sql.append(")");
				logger.debug(sql);
				db.addBatch(sql.toString());
				db.executeBatch();
			} else {
				logger.error(" SiteData is not Created");
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	protected void addSetbackData(Wrapper db, SiteData siteData) throws Exception {

		logger.debug("Entering addSetbackData(" + siteData.getSetback().isChanged() + ")");
		StringBuffer sbSql = new StringBuffer();
		int setbackId = db.getNextId("SETBACK_ID");
		if (siteData.getSetback().isChanged()) {
			sbSql.append("INSERT INTO SITE_SETBACK (SETBACK_ID,LSO_ID,FRONT_FT,FRONT_IN,FRONT_COMMENT,REAR_FT,REAR_IN,REAR_COMMENT,SIDE1_FT,SIDE1_IN,SIDE1_DIR,SIDE1_COMMENT,SIDE2_FT,SIDE2_IN,SIDE2_DIR,SIDE2_COMMENT) VALUES (");
			sbSql.append(setbackId + ",");
			sbSql.append(siteData.getSetback().getLsoId() + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getFrontFt()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getFrontIn()) + ",");
			sbSql.append(StringUtils.checkString(siteData.getSetback().getFrontComment()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getRearFt()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getRearIn()) + ",");
			sbSql.append(StringUtils.checkString(siteData.getSetback().getRearComment()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getSide1Ft()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getSide1In()) + ",");
			sbSql.append(StringUtils.checkString(siteData.getSetback().getSide1Dir()) + ",");
			sbSql.append(StringUtils.checkString(siteData.getSetback().getSide1Comment()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getSide2Ft()) + ",");
			sbSql.append(StringUtils.checkNumber(siteData.getSetback().getSide2In()) + ",");
			sbSql.append(StringUtils.checkString(siteData.getSetback().getSide2Dir()) + ",");
			sbSql.append(StringUtils.checkString(siteData.getSetback().getSide2Comment()) + ")");
			db.addBatch(sbSql.toString());
			logger.debug(sbSql);
		}
		return;
	}

	public Structure getStructure(int lsoId) {
		logger.debug("Entered into  StructureAgent -- getstructure() ");

		try {
			Wrapper db = new Wrapper();
			String sql = "SELECT * FROM STRUCTURE WHERE STRUCTURE_ID=" + lsoId;
			RowSet rs = db.select(sql);

			if (rs.next()) {
				structureDetails = new StructureDetails();
				structureDetails.setTotalFloors(rs.getString("total_floors"));
				logger.debug("StructureAgent -- getstructure() --  set total floors as " + structureDetails.getTotalFloors());
				structureDetails.setAlias(rs.getString("alias"));
				logger.debug("StructureAgent -- getstructure() -- set alias as " + structureDetails.getTotalFloors());
				structureDetails.setDescription(rs.getString("description"));
				logger.debug("StructureAgent -- getstructure() --  set description as " + structureDetails.getTotalFloors());
				structureDetails.setActive(rs.getString("active"));
				logger.debug("StructureAgent -- getstructure() -- set activity as " + structureDetails.getTotalFloors());
				structureDetails.setLabel(rs.getString("label"));
				logger.debug("StructureAgent -- getLabel() -- set label as " + structureDetails.getLabel());
			}

			rs.close();

			// get Use Info
			Use useObj = null;
			useList = new ArrayList();
			sql = "select a.lso_use_id as id,b.lso_type as type,b.description as description from structure_usage a,lso_use b where a.structure_id=" + lsoId + " and a.lso_use_id=b.lso_use_id";
			rs = db.select(sql);

			while (rs.next()) {
				useObj = new Use(rs.getInt("id"), rs.getString("type"), rs.getString("description"));
				useList.add(useObj);
				logger.debug("StructureAgent -- getstructure() -- Added to useList  " + rs.getInt("id") + "--" + rs.getString("description"));
			}

			rs.close();
			structureDetails.setUse(useList);
			logger.debug("StructureAgent -- getstructure() -- set Use as List ");

			// get structure address
			structureAddressList = new ArrayList();
			sql = "select la.str_no,la.str_mod,la.street_id,la.city,la.state,la.zip,la.zip4,la.description,la.primary,la.active,vsl.street_name from lso_address la,v_street_list vsl where la.street_id=vsl.street_id and la.lso_id=" + lsoId + " order by la.primary desc,vsl.street_name asc,la.str_no asc";
			rs = db.select(sql);

			while (rs.next()) {
				structureAddressObj = new StructureAddress();
				structureAddressObj.setActive(rs.getString("active"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- activity as  " + structureAddressObj.getActive());
				structureAddressObj.setDescription(rs.getString("description"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- description as " + structureAddressObj.getDescription());
				structureAddressObj.setCity(rs.getString("city"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- City as " + structureAddressObj.getCity());
				structureAddressObj.setState(rs.getString("state"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- State as " + structureAddressObj.getState());
				structureAddressObj.setZip(rs.getString("zip"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress --  Zip as " + structureAddressObj.getZip());
				structureAddressObj.setZip4(rs.getString("zip4"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- Zip4 as  " + structureAddressObj.getZip4());
				structureAddressObj.setPrimary(rs.getString("primary"));
				logger.debug("StructureAgent -- getstructure() --set StructureAddress --  primary as  " + structureAddressObj.getPrimary());
				structureAddressObj.setStreetNbr(rs.getInt("str_no"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- streetNumber as  " + structureAddressObj.getStreetNbr());
				structureAddressObj.setStreetModifier(rs.getString("str_mod"));
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress --  streetModifier as " + structureAddressObj.getStreetModifier());

				Street addressStreet = new Street();
				addressStreet.setStreetId(rs.getInt("street_id"));
				logger.debug("StructureAgent -- getstructure() -- set Street -- streetId as " + addressStreet.getStreetId());
				addressStreet.setStreetName(rs.getString("street_name"));
				logger.debug("StructureAgent -- getstructure() -- set Street -- street Name as  " + addressStreet.getStreetName());
				structureAddressObj.setStreet(addressStreet);
				logger.debug("StructureAgent -- getstructure() -- set StructureAddress -- Street as  " + structureAddressObj.getStreet().toString());
				structureAddressList.add(structureAddressObj);
				logger.debug("StructureAgent -- getstructure() -- added  StructureAddressObj to AddressList  ");
			}

			rs.close();

			// get the list of all occupancy apns and owner details for this structure.
			occupancyApnList = getLsoOwner(lsoId);

			// get the landid of the structure
			sql = "select land_id from land_structure where structure_id=" + lsoId;
			logger.debug(sql);
			rs = db.select(sql);

			if (rs.next()) {
				landId = rs.getInt("land_id");
				logger.debug("StructureAgent -- getstructure() --    Got the Land_id for this Structure from the database as " + landId);
			}

			rs.close();

			structureHold = new CommonAgent().getHolds(lsoId, "S", 3);
			structureCondition = new ArrayList();
			structureAttachment = new ArrayList();
			structureSiteData = new StructureSiteData();
			structureComment = new ArrayList();
			structure = new Structure(lsoId, landId, structureDetails, structureAddressList, structureHold, structureCondition, structureAttachment, structureSiteData, structureComment);
			structure.setOccupancyApn(occupancyApnList);
		} catch (Exception e) {
			logger.error("problem with structure details" + e.getMessage());
		}

		return structure;
	}

	public Structure updateStructureDetails(Structure structure) {
		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			StructureDetails structureDetails = structure.getStructureDetails();
			lsoId = structure.getLsoId();

			String totalFloors = null;

			if (structure.getStructureDetails().getTotalFloors() != null) {
				totalFloors = structureDetails.getTotalFloors();
			}

			String sql = "UPDATE STRUCTURE SET ALIAS=" + StringUtils.checkString(structureDetails.getAlias()) + "," + "DESCRIPTION=" + StringUtils.checkString(structureDetails.getDescription()) + "," + "TOTAL_FLOORS=" + StringUtils.checkString(totalFloors) + "," + "ACTIVE=" + StringUtils.checkString(structureDetails.getActive()) + "," + "LABEL=" + StringUtils.checkString(structureDetails.getLabel()) + " WHERE STRUCTURE_ID=" + lsoId;
			db.addBatch(sql);
			logger.debug("Structure details update sql is :" + sql);

			// inserting data into Land_Usage Table
			List useList = structure.getStructureDetails().getUse();
			Iterator useiter = useList.iterator();
			sql = "delete from structure_usage where structure_id=" + lsoId;
			logger.debug("delete structure_usage sql ");
			db.addBatch(sql);

			while (useiter.hasNext()) {
				Use useObj = (Use) useiter.next();
				sql = "insert into structure_usage(structure_id,lso_use_id) values(" + lsoId + "," + useObj.getId() + ")";
				db.addBatch(sql);
				logger.debug("Insert structure_usage SQL " + sql);
			}

			db.executeBatch();
			logger.debug("Database updated successfully for Structure Details  -- id = " + lsoId);
		} catch (Exception e) {
			logger.error("***Exception thrown while trying to update structure details :" + e.getMessage());
		}

		return this.getStructure(lsoId);
	}

	public Structure updateStructureAddress(Structure structure) {
		try {
			Wrapper db = new Wrapper();
			structureAddressList = structure.getStructureAddress();
			logger.debug("the size of structure address list is :" + structureAddressList.size());

			if (structureAddressList.size() > 0) {
				structureAddressObj = (StructureAddress) structureAddressList.get(0);

				String sql = "UPDATE LSO_ADDRESS SET CITY=" + StringUtils.checkString(structureAddressObj.getCity()) + "," + "STATE=" + StringUtils.checkString(structureAddressObj.getState()) + "," + "DESCRIPTION=" + StringUtils.checkString(structureAddressObj.getDescription()) + "," + "ACTIVE=" + StringUtils.checkString(structureAddressObj.getActive()) + "," + "PRIMARY=" + StringUtils.checkString(structureAddressObj.getPrimary()) + "," + "STR_NO=" + structureAddressObj.getStreetNbr() + "," + "STR_MOD=" + StringUtils.checkString(structureAddressObj.getStreetModifier()) + "," + "STREET_ID=" + structureAddressObj.getStreet().getStreetId() + "," + "ZIP=" + structureAddressObj.getZip() + "," + "ZIP4=" + StringUtils.checkString(structureAddressObj.getZip4()) + " WHERE LSO_ID=" + structure.getLsoId();
				logger.debug("Structure address update sql is :" + sql);
				db.update(sql);

				int a = structure.getLsoId();
				logger.debug("Database updated successfully for structure address id = " + structure.getLsoId());
				structure = this.getStructure(a);
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update structure address :" + e.getMessage());
			structure = null;
		}

		return structure;
	}

	/**
	 * Sets the value of the structure property.
	 * 
	 * @param aStructure
	 *            the new value of the structure property
	 */
	public void setStructure(Structure aStructure) {
		structure = aStructure;
	}

	public Structure addStructure(Structure structure) {
		try {
			logger.debug("started addind Structure");

			Wrapper db = new Wrapper();
			String sql = "";

			// Generating the ID for Land.
			lsoId = db.getNextId("LSO_ID");
			logger.debug("The ID generated for new Structure is " + lsoId);

			// Begin the transaction
			db.beginTransaction();

			String totalFloors = null;

			if (structure.getStructureDetails().getTotalFloors() != null) {
				totalFloors = structure.getStructureDetails().getTotalFloors();
			}

			// inserting data into Land Table
			sql = "Insert into structure(structure_id,alias,description,total_floors,active,label)" + " values(" + lsoId + "," + StringUtils.checkString(structure.getStructureDetails().getAlias()) + "," + StringUtils.checkString(structure.getStructureDetails().getDescription()) + "," + StringUtils.checkString(totalFloors) + "," + "'Y'," + StringUtils.checkString(structure.getStructureDetails().getLabel()) + " "// Force Active on Add // StringUtils.checkString(structure.getStructureDetails().getActive())
					+ ")";
			db.addBatch(sql);
			logger.debug("Insert Structure SQL " + sql);

			// inserting data into Land_Usage Table
			List useList = structure.getStructureDetails().getUse();
			Iterator useiter = useList.iterator();

			while (useiter.hasNext()) {
				Use useObj = (Use) useiter.next();
				sql = "select lso_use_id from lso_use where description='" + useObj.getDescription() + "' and lso_type='S'";

				RowSet rs = db.select(sql);

				if (rs.next()) {
					sql = "insert into structure_usage(structure_id,lso_use_id) values(" + lsoId + "," + rs.getString("lso_use_id") + ")";
					db.addBatch(sql);
					logger.debug("Insert structure_usage SQL " + sql);
				}

				rs.close();
				rs = null;
			}

			// *** Begin Inserting Structure Address
			structureAddressList = structure.getStructureAddress();

			if (structureAddressList.size() > 0) {
				structureAddressObj = (StructureAddress) structureAddressList.get(0);

				int addressId = db.getNextId("ADDRESS_ID");
				logger.debug("The ID generated for new Address is " + addressId);
				sql = "insert into lso_address(addr_id,lso_id,lso_type,str_no,str_mod," + "street_id,city,state,zip,zip4,description,primary,active,last_updated," + "updated_by,addr_primary) " + "values(" + addressId + "," + lsoId + "," + "'S'," + structureAddressObj.getStreetNbr() + "," + StringUtils.checkString(structureAddressObj.getStreetModifier()) + "," + structureAddressObj.getStreet().getStreetId() + "," + StringUtils.checkString(structureAddressObj.getCity()) + "," + StringUtils.checkString(structureAddressObj.getState()) + "," + StringUtils.checkNumber(structureAddressObj.getZip()) + "," + StringUtils.checkString(structureAddressObj.getZip4()) + "," + StringUtils.checkString(structureAddressObj.getDescription()) + ",'Y'," + "'Y'," // Force Active on Add // StringUtils.checkString(structureAddressObj.getActive())
						+ "current_timestamp" + "," + structureAddressObj.getUpdatedBy().getUserId() + "," + addressId + ")";

				db.addBatch(sql);
				logger.debug("Structure address insert sql is :" + sql);
			} else {
				logger.debug(" Structure Address is not Created");
			}

			// *** End Inserting Structure Address
			// Begin Inserting Land_Structure
			sql = "Insert into Land_Structure(land_id,structure_id) values(" + structure.getLandId() + "," + lsoId + ")";
			db.addBatch(sql);
			logger.debug(" Insert Land_Structure Sql  " + sql);

			// End Inserting Land_Structure
			db.executeBatch();
		} catch (SQLException sqle) {
			logger.error("Exception thrown while trying to add land :" + sqle.getMessage());
		} catch (Exception e) {
			logger.error("Exception thrown while trying to add land :" + e.getMessage());
		}

		return this.getStructure(lsoId);
	}

	/**
	 * Gets the LSO Owner for the given structure id.
	 * 
	 * @param structureId
	 * @return
	 * @throws Exception
	 */
	public List getLsoOwner(int structureId) throws Exception {
		logger.info("getLsoOwner(" + structureId + ")");
		try {
			List occupancyApns = new ArrayList();
			Wrapper db = new Wrapper();

			String sql = "select * from v_lso_owner where lso_id in (select occupancy_id from structure_occupant where structure_id=" + structureId + ") and apn is not null and rownum <500";

			logger.info(sql);
			RowSet ownerRs = db.select(sql);

			while (ownerRs.next()) {
				OccupancyApn occupancyApn = new OccupancyApn();
				occupancyApn.setApn(ownerRs.getString("APN"));
				occupancyApn.setActive(ownerRs.getString("active"));

				Owner owner = new Owner();
				owner.setEmail(ownerRs.getString("email_id"));
				owner.setFax(StringUtils.phoneFormat(ownerRs.getString("fax")));

				ForeignAddress foreignAddress = new ForeignAddress();
				foreignAddress.setCountry(ownerRs.getString("country"));
				foreignAddress.setLineOne(ownerRs.getString("address_line1"));
				foreignAddress.setLineTwo(ownerRs.getString("address_line2"));
				foreignAddress.setLineThree(ownerRs.getString("address_line3"));
				foreignAddress.setLineFour(ownerRs.getString("address_line4"));
				owner.setForeignAddress(foreignAddress);
				owner.setForeignFlag(ownerRs.getString("foreign_address"));

				Address localAddress = new Address();
				localAddress.setCity(ownerRs.getString("city"));
				localAddress.setState(ownerRs.getString("state"));

				Street localAddressStreet = new Street();
				String streetName = "" + ownerRs.getString("str_name");
				localAddressStreet.setStreetName(streetName);
				localAddress.setStreet(localAddressStreet);
				localAddress.setStreetModifier(ownerRs.getString("str_mod"));
				localAddress.setStreetNbr(ownerRs.getInt("str_no"));
				localAddress.setZip(ownerRs.getString("zip"));

				localAddress.setZip4(ownerRs.getString("zip4"));
				owner.setLocalAddress(localAddress);
				owner.setName(ownerRs.getString("name"));
				owner.setOwnerId(ownerRs.getInt("owner_id"));
				owner.setPhone(StringUtils.phoneFormat(ownerRs.getString("phone")));
				occupancyApn.setOwner(owner);

				occupancyApns.add(occupancyApn);
			}

			ownerRs.close();

			return occupancyApns;
		} catch (Exception e) {
			logger.error("Exception thrown while getLsoOwner :" + e.getMessage());
			throw e;
		}
	}

	public Street getStreet(int streetId) {
		logger.info("getStreet(" + streetId + ")");

		String sql = "select * from street_list where street_id=" + streetId;

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				String prefixDirection = (rs.getString("pre_dir") != null) ? rs.getString("pre_dir") : "";
				String streetName = (rs.getString("str_name") != null) ? rs.getString("str_name") : "";
				String streetType = (rs.getString("str_type") != null) ? rs.getString("str_type") : "";
				String suffixDirection = (rs.getString("suf_dir") != null) ? rs.getString("suf_dir") : "";
				String fullStreetName = prefixDirection + " " + streetName + " " + streetType;
				street = new Street(streetId, fullStreetName);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return street;
	}

	public List getStreets(String lsoId) {
		String sql = "select street_id,street_name from v_street_list where street_id in " + "(select street_id from lso_addr_range where lso_id=" + lsoId + ") order by street_name";
		List streetList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				street = new Street(rs.getInt("street_id"), StringUtils.properCase(rs.getString("street_name")));
				streetList.add(street);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return streetList;
	}

	/**
	 * Get street list
	 * 
	 * @return
	 */
	public static List<Street> getStreetArrayList() {
		String sql = "select * from street_list where street_id > 0 order by str_name";
		List streetList = new ArrayList();
		String fullStreetName = "";
		Street street = null;
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				String prefixDirection = (rs.getString("pre_dir") != null) ? rs.getString("pre_dir") : "";
				String streetName = (rs.getString("str_name") != null) ? rs.getString("str_name") : "";
				String streetType = (rs.getString("str_type") != null) ? rs.getString("str_type") : "";
				String suffixDirection = (rs.getString("suf_dir") != null) ? rs.getString("suf_dir") : "";

				streetName = StringUtils.replaceSmartChars(streetName);

				if (!("").equalsIgnoreCase(prefixDirection) && !("").equalsIgnoreCase(streetType)) {
					fullStreetName = prefixDirection + " " + streetName + " " + streetType;
				} else if (("").equalsIgnoreCase(prefixDirection) && !("").equalsIgnoreCase(streetType)) {
					fullStreetName = streetName + " " + streetType;
				} else if (!("").equalsIgnoreCase(prefixDirection) && ("").equalsIgnoreCase(streetType)) {
					fullStreetName = prefixDirection + " " + streetName;
				} else if (("").equalsIgnoreCase(prefixDirection) && ("").equalsIgnoreCase(streetType)) {
					fullStreetName = streetName;
				}

				street = new Street(rs.getInt("street_id"), StringUtils.properCase(fullStreetName));
				streetList.add(street);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return streetList;
	}

	// Created by Gai3 for multiple selections of stree list in search screen

	public List getOnlyStreetNameArrayList() {
		// String sql = "select distinct STR_NAME,STR_TYPE from street_list where street_id > 0 group by (STR_NAME,STR_TYPE) having count(*)>=2 order by str_name";
		String sql = "select distinct STR_NAME,STR_TYPE from street_list where street_id > 0 group by STR_NAME,STR_TYPE order by str_name";
		List streetList = new ArrayList();
		int streetId = 1;

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			logger.info("Multiple street query is : " + sql);

			while (rs != null && rs.next()) {
				String streetName = (rs.getString("str_name") != null) ? rs.getString("str_name") : "";
				String streetType = (rs.getString("str_type") != null) ? rs.getString("str_type") : "";
				String fullStreetName = streetName + " " + streetType;

				street = new Street(streetId, StringUtils.properCase(fullStreetName));
				streetList.add(street);
				streetId++;
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return streetList;
	}

	// Created by Gayathri to get Street name list order by Street id.
	public List getBTBLStreetArrayList() {
		String sql = "select * from street_list where street_id > 0 order by street_id";
		List streetList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs != null && rs.next()) {
				String prefixDirection = (rs.getString("pre_dir") != null) ? rs.getString("pre_dir") : "";
				String streetName = (rs.getString("str_name") != null) ? rs.getString("str_name") : "";
				String streetType = (rs.getString("str_type") != null) ? rs.getString("str_type") : "";
				String suffixDirection = (rs.getString("suf_dir") != null) ? rs.getString("suf_dir") : "";
				String fullStreetName = "";

				if (!("").equalsIgnoreCase(prefixDirection) && !("").equalsIgnoreCase(streetType)) {
					fullStreetName = prefixDirection + " " + streetName + " " + streetType;
				} else if (("").equalsIgnoreCase(prefixDirection) && !("").equalsIgnoreCase(streetType)) {
					fullStreetName = streetName + " " + streetType;
				} else if (!("").equalsIgnoreCase(prefixDirection) && ("").equalsIgnoreCase(streetType)) {
					fullStreetName = prefixDirection + " " + streetName;
				} else if (("").equalsIgnoreCase(prefixDirection) && ("").equalsIgnoreCase(streetType)) {
					fullStreetName = streetName;
				}

				street = new Street(rs.getInt("street_id"), StringUtils.properCase(fullStreetName));
				streetList.add(street);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return streetList;
	}

	public String getCrossStreet(int street1, int street2) {
		String crossStreet = "";
		String sql = "select * from v_street_list where street_id in (" + street1 + "," + street2 + ") order by street_name";

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				crossStreet = crossStreet + rs.getString("street_name") + " x ";
			}

			if (rs != null) {
				rs.close();
			}

			if ((crossStreet != null) && !crossStreet.equals("")) {
				crossStreet = crossStreet.substring(0, crossStreet.length() - 3);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return crossStreet;
	}

	/**
	 * Get cross streets for a street
	 * 
	 * @param streetId
	 * @return
	 */
	public List<Street> getCrossStreets(String streetId) {
		logger.info("getCrossStreets(" + streetId + ")");

		String sql = "select street_id,street_name from v_street_list where street_id in (select street_id2 from lkup_cross_street where street_id1=" + streetId + ") union select street_id,street_name from v_street_list where street_id in (select street_id1 from lkup_cross_street where street_id2=" + streetId + ") order by street_name";
		List streetList = new ArrayList();

		try {
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				Street street = new Street(rs.getInt("street_id"), StringUtils.properCase(rs.getString("street_name")));
				streetList.add(street);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return streetList;
	}

	/**
	 * Get Address Id for cross street
	 * 
	 * @param streetId1
	 * @param streetId2
	 * @return
	 */
	public int getAddressIdForCrossStreet(String streetId1, String streetId2) throws Exception {
		logger.info("getAddressIdForCrossStreet(" + streetId1 + "," + streetId2 + ")");

		String sql = "select addr_id from lkup_cross_street where (street_id1=" + streetId1 + " and street_id2=" + streetId2 + ") or (street_id1=" + streetId2 + " and street_id2=" + streetId1 + ")";
		// String sql = "select addr_id from lkup_cross_street where street_id1=" + streetId1 + " and street_id2=" + streetId2;
		int addressId = -1;
		try {
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				addressId = rs.getInt(1);
			}
			return addressId;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the landUse
	 * 
	 * @return Returns a String[]
	 */
	public String[] getLandUse() {
		try {
			List useList = this.getUseList("L");
			landUse = new String[useList.size()];

			Iterator useIterator = useList.iterator();
			int j = 0;

			while (useIterator.hasNext()) {
				Use obj = (Use) useIterator.next();
				landUse[j] = obj.getDescription();
				j++;
			}
		} catch (Exception e) {
			logger.error("Exception thrown :" + e.getMessage());
		}

		return landUse;
	}

	/**
	 * Sets the landUse
	 * 
	 * @param landUse
	 *            The landUse to set
	 */
	public void setLandUse(String[] landUse) {
		this.landUse = landUse;
	}

	/**
	 * Gets the structureUse
	 * 
	 * @return Returns a String[]
	 */
	public String[] getStructureUse() {
		try {
			List useList = this.getUseList("S");
			structureUse = new String[useList.size()];

			Iterator useIterator = useList.iterator();
			int j = 0;

			while (useIterator.hasNext()) {
				Use obj = (Use) useIterator.next();
				structureUse[j] = obj.getDescription();
				j++;
			}
		} catch (Exception e) {
			logger.error("Exception thrown in get Structure use:" + e.getMessage());
		}

		return structureUse;
	}

	/**
	 * Sets the structureUse
	 * 
	 * @param structureUse
	 *            The structureUse to set
	 */
	public void setStructureUse(String[] structureUse) {
		this.structureUse = structureUse;
	}

	/**
	 * Gets the occupancyUse
	 * 
	 * @return Returns a String[]
	 */
	public String[] getOccupancyUse() {
		try {
			List useList = this.getUseList("O");
			occupancyUse = new String[useList.size()];

			Iterator useIterator = useList.iterator();
			int j = 0;

			while (useIterator.hasNext()) {
				Use obj = (Use) useIterator.next();
				occupancyUse[j] = obj.getDescription();
				j++;
			}
		} catch (Exception e) {
			logger.error("Exception thrown in get occupancy use:" + e.getMessage());
		}

		return occupancyUse;
	}

	/**
	 * Sets the occupancyUse
	 * 
	 * @param occupancyUse
	 *            The occupancyUse to set
	 */
	public void setOccupancyUse(String[] occupancyUse) {
		this.occupancyUse = occupancyUse;
	}

	/**
	 * Gets the use
	 * 
	 * @return Returns a Use
	 */
	public Use getUse() {
		return use;
	}

	public Use getUse(String description, String lsoType) {
		String sql = "SELECT * FROM LSO_USE WHERE DESCRIPTION='" + description + "' and lso_type='" + lsoType + "'";
		logger.debug("use : " + sql);

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			if (rs.next()) {
				use = new Use(Integer.parseInt(rs.getString("LSO_USE_ID")), rs.getString("LSO_TYPE"), rs.getString("DESCRIPTION"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return use;
	}

	public Use getUse(int useId) {
		String sql = "SELECT * FROM LSO_USE WHERE LSO_USE_ID=" + useId;

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			if (rs.next()) {
				use = new Use(rs.getInt("LSO_USE_ID"), StringUtils.nullReplaceWithEmpty(rs.getString("LSO_TYPE")), StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return use;
	}

	/**
	 * Sets the use
	 * 
	 * @param use
	 *            The use to set
	 */
	public void setUse(Use use) {
		this.use = use;
	}

	public List getUseList(String lso) {
		String sql = "select * from lso_use where lso_type='" + lso + "'";
		List useList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				Use useObj = new Use(Integer.parseInt(rs.getString("lso_use_id")), rs.getString("description"));
				useList.add(useObj);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}

		return useList;
	}

	/**
	 * Gets the zone
	 * 
	 * @return Returns a String[]
	 */
	public String[] getZone() {

		try {
			List zoneList = this.getZoneList();
			zone = new String[zoneList.size()];
			Iterator zoneIterator = zoneList.iterator();
			int j = 0;
			while (zoneIterator.hasNext()) {
				Zone obj = (Zone) zoneIterator.next();
				zone[j] = obj.getDescription();
				j++;
			}
		} catch (Exception e) {
			logger.error("Exception thrown :" + e.getMessage());
		}
		return zone;
	}

	public Zone getZone(String description) {
		String sql = "SELECT * FROM ZONE WHERE ZONE='" + description + "'";

		try {

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			if (rs.next()) {
				zoneObj = new Zone(Integer.parseInt(rs.getString("ZONE_ID")), rs.getString("ZONE"));
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		return zoneObj;
	}

	public static ParkingZone getParkingZone(String lsoId) {
		ParkingZone parkingZone = new ParkingZone();

		try {
			Wrapper db = new Wrapper();

			String sql = "select * from parking_zone where pzone_id = (select pzone_id from land where land_id=" + lsoId + ")";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {

				parkingZone.setPzoneId(rs.getString("pzone_id"));
				parkingZone.setDesc(rs.getString("description"));
				parkingZone.setName(rs.getString("name"));
				parkingZone.setComnt(rs.getString("comments"));
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Exception occured while getting zone " + e.getMessage());
		}
		return parkingZone;
	}

	public static ParkingZone getParkingZoneForZoneId(String parkingZoneId) {
		ParkingZone parkingZone = new ParkingZone();

		try {
			Wrapper db = new Wrapper();

			String sql = "select * from parking_zone where pzone_id = " + parkingZoneId;
			logger.debug(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {

				parkingZone.setPzoneId(rs.getString("pzone_id"));
				parkingZone.setDesc(rs.getString("description"));
				parkingZone.setName(rs.getString("name"));
				parkingZone.setComnt(rs.getString("comments"));
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Exception occured while getting zone " + e.getMessage());
		}
		return parkingZone;
	}

	/**
	 * Sets the zone
	 * 
	 * @param zone
	 *            The zone to set
	 */
	public void setZone(String[] zone) {
		this.zone = zone;
	}

	public List getZoneList() {
		String sql = "SELECT * FROM ZONE";
		List zoneList = new ArrayList();

		try {

			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);

			while (rs.next()) {
				int id = rs.getInt("zone_id");
				String description = rs.getString("zone");
				if (description == null)
					description = "";
				description = description.trim();
				Zone zoneObj = new Zone(id, description);
				zoneList.add(zoneObj);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		return zoneList;
	}

	public int getLsoId(String id, String type) throws AgentException {

		int lsoId = 0;
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		String getProjLso = "SELECT vpl.LSO_ID, la.lso_type  FROM v_psa_list vpl JOIN lso_address la ON la.lso_id = vpl.lso_id AND vpl.proj_id =?";

		String getSProjLso = "SELECT vpl.LSO_ID, la.lso_type  FROM v_psa_list vpl JOIN lso_address la ON la.lso_id = vpl.lso_id AND vpl.SPROJ_ID =?";

		String getActivityLso = "SELECT vpl.LSO_ID, la.lso_type  FROM v_psa_list vpl JOIN lso_address la ON la.lso_id = vpl.lso_id AND vpl.ACT_ID =?";

		try {
			connection = wrapper.getConnection();

			if (type.equalsIgnoreCase("P")) {
				pstmt = connection.prepareStatement(getProjLso);
			} else if (type.equalsIgnoreCase("sp")) {
				pstmt = connection.prepareStatement(getSProjLso);
			} else {
				pstmt = connection.prepareStatement(getActivityLso);
			}
			pstmt.setString(1, id);

			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				lsoId = rs.getInt("LSO_ID");
			}
		} catch (Exception ex) {
			logger.error("Error while getting data for PSA", ex);
			throw new AgentException("", ex);
		} finally {
			if (connection != null) {

				try {
					connection.close();
				} catch (Exception Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (Exception Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (Exception Ignore) {
				}
			}
		}
		return lsoId;
	}

	public List getStreetList() {
		String sql = "select street_id,street_name from v_street_list order by street_name";
		List streetList = new ArrayList();

		try {
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				streetList.add(new elms.util.DDList(new Integer(rs.getString("street_id")), rs.getString("street_name")));
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public List getStreetNameArrayList() {
		String sql = "select unique(STR_NAME),STR_TYPE,STREET_ID  from street_list where street_id > 0 order by str_name";
		List streetList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = rs.getString("str_type") != null ? rs.getString("str_type") : "";
				String fullStreetName = streetName + " " + streetType;
				int streetId = rs.getInt("STREET_ID");

				street = new Street(streetId, StringUtils.properCase(streetName), StringUtils.properCase(streetType));
				streetList.add(street);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public List getUniqueStreetNameArrayList() {
		String sql = "select Unique(STR_NAME || ' ' ||STR_TYPE) as STR_NAME  from street_list where street_id > 0 order by str_name";
		List streetList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = "";

				street = new Street(StringUtils.properCase(streetName), StringUtils.properCase(streetName));

				// street.setStreetId(rs.getInt("STREET_ID"));
				streetList.add(street);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	/**
	 * gets the Street List .
	 * 
	 * @return
	 * @author Hemavathi
	 */
	public List getStreetListAdmin() throws Exception {
		String sql = "";

		// sql = "select sl.*, from street_list sl join lso_address la on  la.street_id = sl.street_id where   sl.street_id > 0  order by str_name";

		sql = " select DISTINCT sl.*,la.STREET_ID  AS checkStreetId from street_list sl left join lso_address la on la.street_id = sl.street_id where   sl.street_id > 0  order by str_name";

		List displayList = new ArrayList();

		try {
			ResultSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				DisplayItem displayItem = new DisplayItem();
				displayItem.setFieldOne(rs.getString("street_id"));
				displayItem.setFieldTwo(rs.getString("str_name"));
				displayItem.setFieldThree(rs.getString("pre_dir"));
				displayItem.setFieldFour(rs.getString("str_type"));
				displayItem.setFieldFive(rs.getString("suf_dir"));
				if (rs.getString("checkStreetId") != null) {
					displayItem.setFieldSix("N");
				} else
					displayItem.setFieldSix("Y");

				displayList.add(displayItem);
			}
			if (rs != null) {
				rs.close();
			}
			return displayList;
		}

		catch (Exception e) {
			logger.error("General Error in getusers " + e.getMessage());
			throw e;
		}

	}

	/**
	 * gets the street list for a perticular street name.
	 * 
	 * @param streetname
	 * @return form object * @author Hemavathi
	 */

	public StreetListAdminForm getstreetIdList(String streetid, StreetListAdminForm streetListAdminForm) throws Exception {
		logger.info("getActivityStatus(" + streetid + ")");
		int result = 0;

		// StreetListAdminForm streetListAdminForm1 = new StreetListAdminForm();
		// begining of try block
		try {

			String sql = "select * from street_list where STREET_ID=" + streetid;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {

				streetListAdminForm.setStreetName(rs.getString("str_name"));
				streetListAdminForm.setStreetPrefix(rs.getString("pre_dir"));
				streetListAdminForm.setStreetType(rs.getString("str_type"));
				streetListAdminForm.setStreetSuffix(rs.getString("suf_dir"));
				streetListAdminForm.setStreetListId(rs.getInt("STREET_ID"));

			}
			rs.close();
			return streetListAdminForm;

		}// end of try block

		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in getting street list" + e.getMessage());
			throw new Exception("Problem in Agent during getting the list");

		}

	}

	/**
	 * Function to Update an Save the street details.
	 * 
	 * @param streetid
	 *            ,streetName,streetPrefix,streetType,streetSuffix
	 * @return * @author Hemavathi
	 */

	public int saveStreetList(int streetid, String streetName, String streetPrefix, String streetType, String streetSuffix) {

		logger.info("saveStreetList(" + streetid + "," + streetName + ", " + streetPrefix + ", " + streetType + "," + streetSuffix + ")");
		Wrapper db = new Wrapper();
		int result = 0;
		String sql = "";
		String sql1 = "";

		// begining of try block
		try {

			sql = "SELECT * FROM street_list WHERE STREET_ID='" + streetid + "'";
			RowSet rs = db.select(sql);

			// Values are updated
			if (rs.next()) {
				sql = "update street_list set str_name=" + StringUtils.checkString(streetName).toUpperCase() + "," + " pre_dir=" + StringUtils.checkString(streetPrefix).toUpperCase() + "," + " str_type=" + StringUtils.checkString(streetType).toUpperCase() + "," + " suf_dir=" + StringUtils.checkString(streetSuffix).toUpperCase() + " WHERE STREET_ID='" + streetid + "'";
				logger.debug(sql);
				db.update(sql);
				result = 2;
			}

			// values are inserted
			else {
				String streetlistid = "";
				sql1 = "select max(street_id)+1 as id from street_list";
				RowSet rs1 = db.select(sql1);
				while (rs1.next()) {
					streetlistid = rs1.getString("id");
				}

				sql = "insert into street_list(STREET_ID,str_name,pre_dir,str_type,suf_dir) values (";
				sql = sql + streetlistid;
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetName).toUpperCase();
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetPrefix).toUpperCase();
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetType).toUpperCase();
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetSuffix).toUpperCase();
				sql = sql + ")";
				result = 1;
				db.insert(sql);

			}
		} // end of try block
		catch (Exception e) {
			e.printStackTrace();
			result = -1;
			logger.error("Error in saveStreetList() method" + e.getMessage());
		}
		return result;
	}

	/**
	 * Function to delete the street list.
	 * 
	 * @param streetid
	 * @return * @author Hemavathi
	 */

	public int deleteStreetList(String streetId) {
		int result = 0;
		String sql = "delete from street_list where street_id=" + streetId;
		logger.debug(sql);

		// begining of try block
		try {
			new Wrapper().update(sql);
		} catch (SQLException e) {
			result = -1;
			logger.error("Sql error is deleteStreetList() " + e.getMessage());
		} catch (Exception e) {
			result = -1;
			logger.error("General error is deleteStreetList() " + e.getMessage());
		}

		return result;
	}

	public String getStreetName(String StreetNameId) throws AgentException {
		String streetName = "";

		String getStreetNameQuery = "Select STR_NAME from STREET_LIST where STREET_ID = " + StreetNameId;

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();

			pstmt = connection.prepareStatement(getStreetNameQuery);

			rs = pstmt.executeQuery();

			if (rs != null && rs.next()) {
				streetName = rs.getString("STR_NAME");
			}
		} catch (Exception ex) {
			logger.error("Exception in getStreetName", ex);
			throw new AgentException();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception Ignore) {
			}
		}
		return streetName;
	}

	public Map getStreetNameMap() throws AgentException {
		Map streetMap = new HashMap();

		String getStreetQuery = "select STREET_ID,PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST ";

		String streetName = "";

		logger.debug(getStreetQuery);

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();

			pstmt = connection.prepareStatement(getStreetQuery);

			rs = pstmt.executeQuery();

			if (rs != null) {
				String name = "";
				String id = "";
				String preDir = "";
				String strType = "";
				while (rs.next()) {
					streetName = "";
					name = rs.getString("STR_NAME");
					id = rs.getString("STREET_ID");
					preDir = rs.getString("PRE_DIR");
					strType = rs.getString("STR_TYPE");

					if (preDir != null) {
						streetName = streetName + preDir + " ";
					}

					if (name != null) {
						streetName = streetName + name + " ";
					}

					if (strType != null) {
						streetName = streetName + strType + " ";
					}
					streetMap.put(id, streetName);
				}
			}
		} catch (Exception ex) {
			logger.error("Exception in getStreetName", ex);
			throw new AgentException();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception Ignore) {
			}
		}

		logger.debug("Map Size::" + streetMap.size());
		return streetMap;
	}

	public int getStreetIDForLSO(String lsoId) {
		String sql = "select STREET_ID from v_address_list where lso_id=" + lsoId;
		int id = -1;
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			if (rs.next()) {
				id = rs.getInt("street_id");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return id;
	}

	public int getStreetNOForLSO(String lsoId) {
		String sql = "select STR_NO from v_address_list where lso_id=" + lsoId;
		int id = -1;
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			if (rs.next()) {
				id = rs.getInt("STR_NO");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return id;
	}

	public int getStreetId(String streetDir, String streetName) throws AgentException {
		int streetId = 0;
		String sql = " SELECT * FROM STREET_LIST where (" + " UPPER(STR_NAME || ' ' || STR_TYPE) = UPPER('" + streetName + "')" + " or" + " UPPER(STR_NAME) = UPPER('" + streetName + "'))";

		if (streetDir != null && !streetDir.equalsIgnoreCase("")) {
			sql = sql + " and PRE_DIR = '" + streetDir + "'";
		} else {
			sql = sql + " and PRE_DIR is null";

		}

		logger.debug("sql for streetID::" + sql);
		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				streetId = rs.getInt("STREET_ID");
			}

		} catch (Exception e) {
			logger.debug("", e);
			throw new AgentException("", e);
		}

		return streetId;
	}
	
	public List getLSOUse(int lsoId, String lsoType) throws AgentException {
		List<Use> lsoUseList = null;
		String query = "";
		if (lsoType.equalsIgnoreCase("L")) {
			query = "SELECT LSOU.* FROM LAND_USAGE LU LEFT JOIN LSO_USE LSOU ON LU.LSO_USE_ID = LSOU.LSO_USE_ID WHERE LAND_ID =" + lsoId;
		} else if (lsoType.equalsIgnoreCase("S")) {
			query = "SELECT LU.* FROM STRUCTURE_USAGE SU LEFT JOIN LSO_USE LU ON LU.LSO_USE_ID = SU.LSO_USE_ID WHERE STRUCTURE_ID =" + lsoId;
		} else {
			query = "SELECT LU.* FROM OCCUPANCY_USAGE OU LEFT JOIN LSO_USE LU ON LU.LSO_USE_ID = OU.LSO_USE_ID WHERE OCCUPANCY_ID = " + lsoId;
		}
		Wrapper db = new Wrapper();
		try {
			RowSet rs = db.select(query);
			if (rs != null) {
				lsoUseList = new ArrayList<Use>();
				while (rs.next()) {
					Use useObj = new Use();
					useObj.setId(rs.getInt("LSO_USE_ID"));
					useObj.setDescription(rs.getString("DESCRIPTION"));
					lsoUseList.add(useObj);
				}
			}
		} catch (Exception e) {
			logger.error("error while getting Use" + e);
			throw new AgentException("", e);
		}

		return lsoUseList;
	}

}

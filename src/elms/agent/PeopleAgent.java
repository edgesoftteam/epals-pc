package elms.agent;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.PeopleType;
import elms.app.common.Agent;
import elms.app.common.ViewPeople;
import elms.app.people.People;
import elms.app.people.PeopleList;
import elms.common.Constants;
import elms.control.beans.PeopleForm;
import elms.control.beans.PeopleVersionForm;
import elms.exception.AgentException;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class PeopleAgent extends Agent {
	static Logger logger;
	PeopleList peopleL = null;

	static {
		logger = Logger.getLogger(elms.agent.PeopleAgent.class.getName());
	}

	public PeopleAgent() {
	}

	public List getLsoOwners(String lsoId, String lsoType, String psaType, String psaId) throws Exception {
		logger.info("getLsoOwners(" + lsoId + ", " + lsoType + ", " + psaType
				+ ", " + psaId + ")");

		List peopleList = new ArrayList();
		People owner = new People();

		try {

			StringBuffer sql = new StringBuffer();
			sql.append("SELECT O.* FROM (LSO_APN  LA JOIN APN_OWNER AO ON LA.APN = A0.APN AND LA.LSO_ID =  JOIN OWNER "
					+ lsoId + ")");
			sql.append(" JOIN OWNER O ON AO.OWNER_ID = O.OWNER_ID");
			sql.append(" WHERE 0.NAME NOT IN (SELECT P.NAME FROM ACTIVITY_PEOPLE AP JOIN PEOPLE P ON AP.PEOPLE_ID = P.PEOPLE_ID");
			sql.append(" AND AP.ACT_ID = " + psaId + " AND AP.PSA_TYPE = '"
					+ psaType + "' AND P.PEOPLE_TYPE_ID = 7 )");
			logger.info(sql.toString());

			javax.sql.RowSet rs;

			for (rs = (new Wrapper()).select(sql.toString()); rs.next(); peopleList
					.add(owner)) {
				owner.setName(rs.getString("NAME"));
				logger.debug("set name" + owner.getName());
				owner.setPeopleId(-1);
				logger.debug("set people id");
				owner.setPeopleType(new PeopleType(7, "Owner", "W"));
				logger.debug("peopletype " + owner.getPeopleType().getTypeId()
						+ "  " + owner.getPeopleType().getCode());
				logger.debug("set people type id");

				String strNo = rs.getString("STR_NO");
				if (strNo == null) {
					strNo = "";
				}
				String strMod = rs.getString("STR_MOD");
				if (strMod == null) {
					strMod = "";
				}
				String strDir = rs.getString("PRE_DIR");
				if (strDir == null) {
					strDir = "";
				}
				String strName = rs.getString("STR_NAME");
				if (strName == null) {
					strName = "";
				}
				owner.setAddress(strNo + " " + strMod + " " + strDir + " "
						+ strName);
				logger.debug("set address");
				owner.setPhoneNbr(StringUtils.nullReplaceWithEmpty(StringUtils
						.phoneFormat(rs.getString("PHONE"))));
				logger.debug("set phone number");
				owner.setEmailAddress(rs.getString("EMAIL_ID"));
				logger.debug("set email address");
				owner.setCity(rs.getString("CITY"));
				logger.debug("set city");
				owner.setState(rs.getString("STATE"));
				logger.debug("set state");
				owner.setZipCode(rs.getString("ZIP"));
				logger.debug("set zip code");
				owner.setFaxNbr(StringUtils.phoneFormat(rs.getString("FAX")));
				logger.debug("set fax number");
				owner.setCopyApplicant("N");
				logger.debug("set copy applicant");
			}
			if (rs != null) {
				rs.close();
			}
			return peopleList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public List getAllPeopleBySubProjectId(int subProjectId) {
		logger.debug("getAllPeopleBySubProjectId(" + subProjectId);

		List tmpPeopleList = new ArrayList();
		javax.sql.RowSet rs = null;
		try {
			String sql = "select pt.people_type_id,pt.description,pt.CODE,p.name,p.phone,p.lic_no,p.on_hold,ap.psa_type from activity_people ap,people p,people_type pt where p.people_id=ap.people_id and  p.people_type_id=pt.people_type_id and ap.act_id in (select act_id from activity where sproj_id="
					+ subProjectId + ")";
			logger.info(sql);

			ViewPeople viewPeople = null;

			for (rs = (new Wrapper()).select(sql.toString()); rs.next(); tmpPeopleList
					.add(viewPeople)) {
				String description = rs.getString("description");
				if (description == null) {
					description = "";
				}
				String code = rs.getString("CODE");
				if (code == null) {
					code = "";
				}
				String licNo = rs.getString("lic_no");
				if ((licNo == null) || licNo.equals("0")) {
					licNo = "";
				}
				String name = rs.getString("name");
				if ((name == null) || (name == "")) {
					name = "";
				}
				String phone = rs.getString("phone");
				phone = StringUtils.phoneFormat(phone);
				if ((phone == null) || (phone == "")) {
					phone = "";
				}
				String onHold = rs.getString("on_hold");
				if ((onHold == null) || (onHold == "")) {
					onHold = "";
				}
				int peopleTypeId = 0;
				peopleTypeId = rs.getInt("people_type_id");
				String levelType = rs.getString("PSA_TYPE");
				String tmpLevelType = "";
				if ("C".equalsIgnoreCase(levelType)) {
					tmpLevelType = "Plan Check";
				}
				if ("A".equalsIgnoreCase(levelType)) {
					tmpLevelType = "Activity";
				}
				if ("P".equalsIgnoreCase(levelType)) {
					tmpLevelType = "Project";
				}
				viewPeople = new ViewPeople(new PeopleType(peopleTypeId,
						description, code), name, licNo, phone, onHold,
						tmpLevelType);
			}
		} catch (Exception e) {
			logger.error("Error in getAllPeopleBySubProjectId:  "
					+ e.getMessage());
		}

		return tmpPeopleList;
	}

	public List getPeoples(int id, String type, int numberOfRows)
			throws Exception {
		logger.info("getPeoples(" + id + ", " + type + ", " + numberOfRows
				+ ")");

		List peopleList = new ArrayList();
		String sql = "";

		try {
			sql = getPeopleListSql(id, type, numberOfRows);

			javax.sql.RowSet rs;
			People people;

			for (rs = (new Wrapper()).select(sql); rs.next(); peopleList.add(people)) {
				String description = rs.getString("description");

				if (description == null) {
					description = "";
				}

				String code = rs.getString("CODE");

				if (code == null) {
					code = "";
				}

				String licNo = rs.getString("lic_no");

				if ((licNo == null) || licNo.equals("0")) {
					licNo = "";
				}

				String name = StringUtils.nullReplaceWithEmpty(rs
						.getString("name"))
						+ " "
						+ StringUtils.nullReplaceWithEmpty(rs
								.getString("last_name"));

				if ((name == null) || (name == "")) {
					name = "";
				}

				String phone = rs.getString("phone");
				phone = StringUtils.phoneFormat(phone);

				if ((phone == null) || (phone == "")) {
					phone = "";
				}

				int peopleTypeId = 0;
				peopleTypeId = rs.getInt("people_type_id");

				int peopleId = 0;
				peopleId = rs.getInt("PEOPLE_ID");

				String levelType = rs.getString("PSA_TYPE");
				people = new People(peopleId, new PeopleType(peopleTypeId,
						description, code), licNo, name, phone);
				people.setTitle(rs.getString("TITLE"));
				// String holdSql =
				// "select * from holds where hold_level='Z' and stat='A' and level_id="
				// + people.getPeopleId()
				// +" and ROWNUM<=1 order by update_dt desc";
				String holdSql = "select * from ( select * from holds where  hold_level='Z' and level_id="
						+ people.getPeopleId()
						+ " order by update_dt desc) where ROWNUM<=1";
				logger.info("Recent Holds sql :: " + holdSql);

				javax.sql.RowSet holdRs = (new Wrapper()).select(holdSql);

				if (holdRs.next()) {
					if (holdRs.getString("STAT").trim().equalsIgnoreCase("A")) {
						people.setOnHold("Y");
					} else {
						people.setOnHold("N");
					}
					logger.debug("got hold for person " + people.getPeopleId());
				}

				holdRs.close();
				people.setLevelType(levelType);

				if (levelType.equals("C")) {
					people.setPcPeople(true);
				} else if (levelType.equals("A")) {
					people.setPcPeople(false);
				} else if (levelType.equals("P")) {
					people.setPcPeople(false);
				}
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setAutoLiabilityDate(rs.getDate("AUTO_LIABILITY_DT"));
				people.setGeneralLiabilityDate(rs.getDate("GEN_LIABILITY_DT"));
				people.setBusinessLicenseExpires(rs.getDate("BUS_LIC_EXP_DT"));
				people.setLicenseExpires(rs.getDate("LIC_EXP_DT"));
				people.setWorkersCompExpires(rs.getDate("WORK_COMP_EXP_DT"));
				
				// People_lsoId table
				
				String peopleLsoIdSql = "SELECT * FROM PEOPLE_LSOID PL LEFT JOIN LSO_ADDRESS LA ON PL.LSO_ID = LA.LSO_ID WHERE PL.PEOPLE_ID="+ peopleId +"  ORDER BY PL.MOVE_OUT_DATE DESC";
				
				logger.info("Recent peopleLsoId sql :: " + peopleLsoIdSql);

				javax.sql.RowSet peopleLsoIdRs = (new Wrapper()).select(peopleLsoIdSql);

				if (peopleLsoIdRs.next()) {
					people.setMoveOutDate(StringUtils.date2str(peopleLsoIdRs.getDate(("MOVE_OUT_DATE"))));
					people.setMoveInDate(StringUtils.date2str(peopleLsoIdRs.getDate("MOVE_IN_DATE")));
					people.setNetRent(StringUtils.str2$(peopleLsoIdRs.getString("NET_RENT")));
					people.setUnitDesignation(peopleLsoIdRs.getString("UNIT_DESIGNATION"));
					logger.debug("got for person " + people.getPeopleId());
				}

				peopleLsoIdRs.close();
			}

			if (rs != null) {
				rs.close();
			}

			return peopleList;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	private String getPeopleListSql(int id, String type, int numberOfRows) {
		String sql = "";

		if ("A".equalsIgnoreCase(type) || "P".equalsIgnoreCase(type)
				|| "O".equalsIgnoreCase(type) || "S".equalsIgnoreCase(type) ) {
			sql = "select * from activity_people ap,people p,people_type pt where p.people_id=ap.people_id and p.people_type_id=pt.people_type_id and ap.act_id="
					+ id + " and ap.psa_type in ('" + type + "') ";
		}

		if ("Q".equalsIgnoreCase(type)) {
			sql = "select * from activity_people ap,people p,people_type pt where p.people_id=ap.people_id and  p.people_type_id=pt.people_type_id and ap.act_id in (select act_id from activity where sproj_id="
					+ id + ")";
		}
		if ("L".equalsIgnoreCase(type)) {
			sql = "select * from activity_people ap,people p,people_type pt where p.people_id=ap.people_id and  p.people_type_id=pt.people_type_id and ap.act_id in (select LSO_ID from V_LSO_LAND where LAND_ID = "
					+ id + " and LSO_TYPE = 'O')";
		}

		if (numberOfRows > 0) {
			sql = sql + "and rownum <= " + numberOfRows;
		}

		sql = sql + " order by ap.psa_type desc, p.name";
		logger.info(sql); 

		return sql;
	}

	public People getPeople(int peopleId) throws Exception {
		logger.info("getPeople(" + peopleId + ")");

		People contractor = new People();

		try {
			String sql = "SELECT * FROM PEOPLE P,PEOPLE_TYPE PT WHERE P.PEOPLE_TYPE_ID=PT.PEOPLE_TYPE_ID AND P.PEOPLE_ID="
					+ peopleId;
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {

				contractor.setName(rs.getString("NAME"));
				contractor.setPeopleType(new PeopleType(rs
						.getInt("people_type_id"), rs.getString("description"),
						rs.getString("CODE")));
				contractor
						.setFirstName((rs.getString("LAST_NAME") != null) ? rs
								.getString("LAST_NAME") : "");
				contractor.setLicenseNbr(rs.getString("LIC_NO"));
				contractor.setBusinessLicenseNbr(rs.getString("BUS_LIC_NO"));
				contractor.setLicenseExpires(rs.getDate("LIC_EXP_DT"));
				contractor.setBusinessLicenseExpires(rs
						.getDate("BUS_LIC_EXP_DT"));
				contractor.setAddress(rs.getString("ADDR"));
				contractor.setPhoneNbr(StringUtils
						.nullReplaceWithEmpty(StringUtils.phoneFormat(rs
								.getString("PHONE"))));
				contractor.setExt(rs.getString("EXT"));
				contractor.setEmailAddress(rs.getString("EMAIL_ADDR"));
				contractor.setAgentName(rs.getString("AGENT_NAME"));
				contractor.setAgentPhone(StringUtils.phoneFormat(rs
						.getString("AGENT_PH")));
				contractor.setCity(rs.getString("CITY"));
				contractor.setState(rs.getString("STATE"));
				contractor.setZipCode(rs.getString("ZIP"));
				contractor.setFaxNbr(StringUtils.phoneFormat(rs
						.getString("FAX")));
				contractor.setOnHold(rs.getString("ON_HOLD"));
				contractor.setCopyApplicant("N");
				contractor.setGeneralLiabilityDate(rs
						.getDate("GEN_LIABILITY_DT"));
				contractor.setHoldDate(rs.getDate("HOLD_DT"));
				contractor.setHoldComments(rs.getString("HOLD_COMNT"));
				contractor
						.setAutoLiabilityDate(rs.getDate("AUTO_LIABILITY_DT"));
				contractor
						.setWorkersCompExpires(rs.getDate("WORK_COMP_EXP_DT"));
				contractor.setWorkersCompensationWaive(rs
						.getString("WORKERS_COMP_WAIVE"));
				contractor.setComments(rs.getString("COMNTS"));
				contractor.setHicFlag(rs.getString("HIC_FLAG"));
				contractor.setIsExpired("Y");
				contractor.setTitle(rs.getString("TITLE"));
				contractor.setBusinessName(rs.getString("BUSINESS_NAME"));
				contractor.setCorporateName(rs.getString("CORPORATE_NAME"));
				contractor.setHomeAddress(rs.getString("HA_ADDRESS"));
				contractor.setHomeCity(rs.getString("HA_CITY"));
				contractor.setHomeState(rs.getString("HA_STATE"));
				contractor.setHomeZipCode(rs.getString("HA_ZIP"));
				contractor.setMailingAddress(rs.getString("MA_ADDRESS"));
				contractor.setMailingCity(rs.getString("MA_CITY"));
				contractor.setMailingState(rs.getString("MA_STATE"));
				contractor.setMailingZipCode(rs.getString("MA_ZIP"));
				contractor.setEyeColor(rs.getString("EYE_COLOR"));
				contractor.setHairColor(rs.getString("HAIR_COLOR"));
				contractor.setSsn(rs.getString("SSN"));
				contractor.setConfirmSsn(rs.getString("SSN"));
				contractor.setHeight(rs.getString("HEIGHT"));
				contractor.setWeight(rs.getString("WEIGHT"));
				contractor.setGender(rs.getString("GENDER"));
				contractor.setDlExpiryDate(rs.getDate("DL_ID_EXP_DT"));
				contractor.setConfirmStrDlExpiryDate(StringUtils.date2str(rs
						.getDate("DL_ID_EXP_DT")));
				contractor.setDateOfBirth(rs.getDate("DOB"));
				contractor.setDlNumber(rs.getString("DL_ID_NO"));
				contractor.setInsuranceType(rs.getString("INS_TYPE"));
				contractor.setInsuranceAmount(rs.getDouble("INS_AMOUNT"));
				contractor.setInsuranceExpirationDate(rs
						.getDate("INS_EXP_DATE"));
				contractor.setInsuranceProvider(rs.getString("INS_PROVIDER"));
				contractor.setConfirmDlNumber(rs.getString("DL_ID_NO"));
				contractor.setPeopleId(rs.getInt("PEOPLE_ID"));
				contractor.setHomePhone(StringUtils.phoneFormat(rs
						.getString("HA_PHONE")));
				contractor.setHouseholdIncome(StringUtils.str2$(rs.getString("HOUSEHOLD_INCOME"))); 
				logger.debug("Values setted to people object");
			}
			String lsoId =contractor.getLsoId();
			sql = "SELECT * FROM PEOPLE_LSOID PL, PEOPLE P WHERE P.PEOPLE_ID=PL.PEOPLE_ID AND PL.PEOPLE_ID = '"+peopleId+"'" ;
			
			logger.info(sql);

			javax.sql.RowSet rs1 = (new Wrapper()).select(sql);

			if (rs1.next()) {

				contractor.setMoveInDate(StringUtils.date2str(rs1.getDate("MOVE_IN_DATE")));
				contractor.setMoveOutDate(StringUtils.date2str(rs1.getDate("MOVE_OUT_DATE")));
				contractor.setNetRent(StringUtils.str2$(rs1.getString("NET_RENT")));
				contractor.setUnitDesignation(rs1.getString("UNIT_DESIGNATION"));
			}
			if (rs1 != null) {
				rs1.close();
			}
			if (rs != null) {
				rs.close();
			}

			return contractor;
		} catch (Exception e) {
			logger.error(e.getMessage());

			throw e;
		}
	}

	public String getPeopleLevelType(int peopleId, String psaId)
			throws Exception {
		logger.debug("getPeopleLevelType(" + peopleId + "," + psaId + ")");

		String levelType = "";
		String sql = "SELECT PSA_TYPE FROM ACTIVITY_PEOPLE WHERE ACT_ID="
				+ psaId + " AND PEOPLE_ID=" + peopleId;
		logger.info(sql);

		for (javax.sql.RowSet rs = (new Wrapper()).select(sql); rs.next();) {
			levelType = rs.getString("PSA_TYPE");
		}

		return levelType;
	}

	public PeopleType getPeopleType(int peopleId) throws Exception {
		PeopleType ptype = new PeopleType();

		String peopleType = "";

		try {
			String sql = "SELECT PEOPLE_TYPE.CODE FROM PEOPLE, PEOPLE_TYPE WHERE PEOPLE.PEOPLE_TYPE_ID = PEOPLE_TYPE.PEOPLE_TYPE_ID  AND PEOPLE.PEOPLE_ID = "
					+ peopleId;
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {
				ptype.setCode(rs.getString("CODE"));
				ptype.setTypeId(rs.getInt("PEOPLE_TYPE_ID"));
				ptype.setType(rs.getString("DESCRIPTION"));
				peopleType = rs.getString("CODE");
			}

			if (rs != null) {
				rs.close();
			}

			return ptype;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public String getPeopleTypeAsString(int peopleId) throws Exception {
		String peopleType = "";

		try {
			String sql = "SELECT PEOPLE_TYPE.CODE FROM PEOPLE, PEOPLE_TYPE WHERE PEOPLE.PEOPLE_TYPE_ID = PEOPLE_TYPE.PEOPLE_TYPE_ID  AND PEOPLE.PEOPLE_ID = "
					+ peopleId;
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {
				peopleType = rs.getString("CODE");
			}

			if (rs != null) {
				rs.close();
			}

			return peopleType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public String getPeopleName(int peopleId) throws Exception {
		String peopleName = "";

		try {
			String sql = "SELECT name FROM PEOPLE WHERE PEOPLE_ID = "
					+ peopleId;
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {
				peopleName = StringUtils.nullReplaceWithEmpty(rs
						.getString("name"));
			}

			if (rs != null) {
				rs.close();
			}

			return peopleName;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public boolean checkAppName(int psaId, String licenseNbr, String strName)
			throws Exception {
		logger.info("checkAppName(" + psaId + ", " + licenseNbr + ", "
				+ strName + ")");

		boolean dupApplicant = false;
		int count = 0;

		try {
			String sql = "select count(p.people_id) as count from activity_people ap,people p where p.people_id = ap.people_id and ap.act_id ="
					+ psaId
					+ " and p.people_type_id = 3 and ((upper(p.name) like upper('%"
					+ strName
					+ "%')) or (upper(p.lic_no) = upper('"
					+ licenseNbr + "')))";
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {
				count = rs.getInt("count");
			}

			rs.close();

			if (count > 0) {
				dupApplicant = true;
			}

			return dupApplicant;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public boolean checkLicenseNbr(int peopleTypeId, String licenseNbr)
			throws Exception {
		logger.info("checkLicenseNbr(" + peopleTypeId + ", " + licenseNbr + ")");

		return checkLicenseNbr(0, peopleTypeId, licenseNbr);
	}

	public boolean checkLicenseNbr(int peopleId, int peopleTypeId,
			String licenseNbr) throws Exception {
		logger.info("checkLicenseNbr(" + peopleId + ", " + peopleTypeId + ", "
				+ licenseNbr + ")");

		boolean dupLicenseNbr = false;
		int count = 0;
		String sql = "";
		try {
			switch (peopleTypeId) {
			case 3: // '\003'
			case 4: // '\004'
			case 6: // '\006'
			case 7: // '\007'
				if (licenseNbr != "") {
					sql = "select count(people_id) as count from people where people_type_id ="
							+ peopleTypeId
							+ " and lic_no = '"
							+ licenseNbr
							+ "'  and people_id !=" + peopleId;
					logger.info(sql);
					javax.sql.RowSet rs = (new Wrapper()).select(sql);

					if (rs.next()) {
						count = rs.getInt("count");
					}

					rs.close();

					if (count > 0) {
						dupLicenseNbr = true;
					}
				}
				break;
			}

			logger.debug("dupLicenseNbr is : " + dupLicenseNbr);

			return dupLicenseNbr;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public PeopleType getPeopleTypeObj(String peopleTypeCode) throws Exception {
		logger.info("getPeopleTypeObj(" + peopleTypeCode + ")");

		PeopleType peopleType = null;

		try {
			String sql = "SELECT * from people_type where code = "
					+ StringUtils.checkString(peopleTypeCode) + "";
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {
				peopleType = new PeopleType(rs.getInt("people_type_id"),
						rs.getString("description"), rs.getString("code"));
			}

			if (rs != null) {
				rs.close();
			}

			return peopleType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public int checkDuplicateVersion(People people, String peopleTypeId,
			PeopleForm peopleForm, People oldPeople, PeopleForm oldPeopleForm)
			throws Exception {
		logger.info("checkDuplicateVersion(People , " + peopleTypeId
				+ ", PeopleForm )");
		Wrapper db = new Wrapper();
		String sql_selectprevversion = "";
		int rowCount = 0;
		if (people != null) {
			/**
			 * Compare People Old Version and Updating version
			 */
			if (peopleTypeId.equalsIgnoreCase("C")
					|| peopleTypeId.equalsIgnoreCase("G")
					|| peopleTypeId.equalsIgnoreCase("I")) {

				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getLicenseNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getLicenseNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "LIC_NO="
							+ StringUtils.checkString(people.getLicenseNbr())
							+ " AND ";
					logger.debug("got people license number "
							+ StringUtils.checkString(people.getLicenseNbr()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrLicenseExpires())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrLicenseExpires())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " LIC_EXP_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrLicenseExpires()) + " AND ";
					logger.debug("got people lic_exp_date "
							+ StringUtils.toOracleDate(peopleForm
									.getStrLicenseExpires()));
				}
				if ((!(StringUtils.isBlankOrNull(people
						.getBusinessLicenseNbr())))
						|| (!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(oldPeople
								.getBusinessLicenseNbr()))))) {
					sql_selectprevversion = sql_selectprevversion
							+ " BUS_LIC_NO=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people
									.getBusinessLicenseNbr())) + " AND ";
					logger.debug("got Business License Number "
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people
									.getBusinessLicenseNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrBusinessLicenseExpires())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrBusinessLicenseExpires())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " BUS_LIC_EXP_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrBusinessLicenseExpires()) + " AND ";
					logger.debug("got Business License Expiration  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrBusinessLicenseExpires()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got Agent name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getHicFlag())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHicFlag())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " HIC_FLAG=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHicFlag())
							+ " AND ";
					logger.debug("got HIC flag "
							+ StringUtils.checkString(people.getHicFlag()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + " STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + " ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion + " PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion + " EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					sql_selectprevversion = sql_selectprevversion + " FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils
						.isBlankOrNull(people.getCopyApplicant())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCopyApplicant())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " COPY_APPLICANT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils
									.checkString(people.getCopyApplicant())
							+ " AND ";
					logger.debug("got Copy Applicant "
							+ StringUtils.checkString(people.getCopyApplicant()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrGeneralLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrGeneralLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " GEN_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()) + " AND ";
					logger.debug("got General Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrAutoLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrAutoLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " AUTO_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()) + " AND ";
					logger.debug("got Auto Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrWorkersCompExpires())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrWorkersCompExpires())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " WORK_COMP_EXP_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrWorkersCompExpires()) + " AND ";
					logger.debug("got Work comp expiration  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrWorkersCompExpires()));
				}
				if ((!(StringUtils.isBlankOrNull(people
						.getWorkersCompensationWaive())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getWorkersCompensationWaive())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " WORKERS_COMP_WAIVE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people
									.getWorkersCompensationWaive()) + " AND ";
					logger.debug("got Work comp waive "
							+ StringUtils.checkString(people
									.getWorkersCompensationWaive()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();

			} else if (peopleTypeId.equalsIgnoreCase("R")
					|| peopleTypeId.equalsIgnoreCase("E")) {

				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getLicenseNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getLicenseNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "LIC_NO="
							+ StringUtils.checkString(people.getLicenseNbr())
							+ " AND ";
					logger.debug("got people license number "
							+ StringUtils.checkString(people.getLicenseNbr()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrLicenseExpires())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrLicenseExpires())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " LIC_EXP_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrLicenseExpires()) + " AND ";
					logger.debug("got people lic_exp_date "
							+ StringUtils.toOracleDate(peopleForm
									.getStrLicenseExpires()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got Agent name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + " ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion + " EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					logger.debug("Hello");
					sql_selectprevversion = sql_selectprevversion + " FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils
						.isBlankOrNull(people.getCopyApplicant())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCopyApplicant())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " COPY_APPLICANT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils
									.checkString(people.getCopyApplicant())
							+ " AND ";
					logger.debug("got Copy Applicant "
							+ StringUtils.checkString(people.getCopyApplicant()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();

			} else if (peopleTypeId.equalsIgnoreCase("D")) {

				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getLicenseNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getLicenseNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "LIC_NO="
							+ StringUtils.checkString(people.getLicenseNbr())
							+ " AND ";
					logger.debug("got people license number "
							+ StringUtils.checkString(people.getLicenseNbr()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrLicenseExpires())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrLicenseExpires())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " LIC_EXP_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrLicenseExpires()) + " AND ";
					logger.debug("got people lic_exp_date "
							+ StringUtils.toOracleDate(peopleForm
									.getStrLicenseExpires()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got Agent name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + " ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion + " EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					logger.debug("Hello");
					sql_selectprevversion = sql_selectprevversion + " FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getAgentName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAgentName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " AGENT_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getAgentName() + "%") + " AND ";
					logger.debug("got Agent Name " + people.getAgentName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAgentPhone())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAgentPhone())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " AGENT_PH=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getAgentPhone()))
							+ " AND ";
					logger.debug("got Agent Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getAgentPhone())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();
			}

			else if ((peopleTypeId.equalsIgnoreCase("F"))
					|| (peopleTypeId.equalsIgnoreCase("H"))) {
				logger.debug("people type is either A or J " + peopleTypeId);
				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getTitle())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getTitle())))) {
					sql_selectprevversion = sql_selectprevversion + "TITLE="
							+ StringUtils.checkString(people.getTitle())
							+ " AND ";
					logger.debug("got Title "
							+ StringUtils.checkString(people.getTitle()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFirstName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFirstName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  LAST_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getFirstName() + "%") + " AND ";
					logger.debug("got Last name " + people.getFirstName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got First name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getBusinessName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getBusinessName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  BUSINESS_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getBusinessName() + "%") + " AND ";
					logger.debug("got Business name "
							+ people.getBusinessName());
				}
				if ((!(StringUtils
						.isBlankOrNull(people.getCorporateName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCorporateName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "CORPORATE_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getCorporateName() + "%")
							+ " AND ";
					logger.debug("got Corporate name "
							+ people.getCorporateName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Business Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_ADDRESS LIKE "
							+ StringUtils.checkString("%"
									+ people.getHomeAddress() + "%") + " AND ";
					logger.debug("got Home Address " + people.getHomeAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeCity())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHomeCity())
							+ " AND ";
					logger.debug("got Home city "
							+ StringUtils.checkString(people.getHomeCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeState())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHomeState())
							+ " AND ";
					logger.debug("got Home State "
							+ StringUtils.checkString(people.getHomeState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  HA_ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHomeZipCode())
							+ " AND ";
					logger.debug("got Home Zip "
							+ StringUtils.checkString(people.getHomeZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomePhone())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomePhone())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getHomePhone()))
							+ " AND ";
					logger.debug("got Home Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getHomePhone())));
				}
				if ((!(StringUtils.isBlankOrNull(people
						.getMailingAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  MA_ADDRESS LIKE "
							+ StringUtils.checkString("%"
									+ people.getMailingAddress() + "%")
							+ " AND ";
					logger.debug("got Mailing Address "
							+ people.getMailingAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getMailingCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingCity())))) {
					sql_selectprevversion = sql_selectprevversion + " MA_CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getMailingCity())
							+ " AND ";
					logger.debug("got Mailing city "
							+ StringUtils.checkString(people.getMailingCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getMailingState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingState())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  MA_STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getMailingState())
							+ " AND ";
					logger.debug("got Mailing State "
							+ StringUtils.checkString(people.getMailingState()));
				}
				if ((!(StringUtils.isBlankOrNull(people
						.getMailingZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  MA_ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people
									.getMailingZipCode()) + " AND ";
					logger.debug("got Mailing Zip "
							+ StringUtils.checkString(people
									.getMailingZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  B_PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Business Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  B_PHONE_EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Business Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if (((StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getAgentName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAgentName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  AGENT_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getAgentName() + "%") + " AND ";
					logger.debug("got Agent Name " + people.getAgentName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAgentPhone())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAgentPhone())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " AGENT_PH=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getAgentPhone()))
							+ " AND ";
					logger.debug("got Agent Phone Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getAgentPhone())));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrGeneralLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrGeneralLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  GEN_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()) + " AND ";
					logger.debug("got General Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrAutoLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrAutoLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  AUTO_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()) + " AND ";
					logger.debug("got Auto Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();

			} else if (peopleTypeId.equalsIgnoreCase("A")) {
				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getCity())));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getState()))))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getState()))
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getState())));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getZipCode()))))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getZipCode()))
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getZipCode())));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getPhoneNbr()))))
						|| (!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(oldPeople
								.getPhoneNbr()))))) {
					sql_selectprevversion = sql_selectprevversion + "  PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(StringUtils.nullReplaceWithEmpty(people.getPhoneNbr())))
							+ " AND ";
					logger.debug("got Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getExt()))))
						|| (!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(oldPeople.getExt()))))) {
					sql_selectprevversion = sql_selectprevversion + "  EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getFaxNbr()))))
						|| (!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(oldPeople
								.getFaxNbr()))))) {
					sql_selectprevversion = sql_selectprevversion + "  FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(StringUtils.nullReplaceWithEmpty(people.getFaxNbr()))) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getEmailAddress()))))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getEmailAddress()))
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(StringUtils.nullReplaceWithEmpty(people.getEmailAddress())));
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getAgentName()))))
						|| (!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(oldPeople
								.getAgentName()))))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  AGENT_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getAgentName() + "%") + " AND ";
					logger.debug("got Agent Name " + people.getAgentName());
				}
				if ((!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(people.getAgentPhone()))))
						|| (!(StringUtils.isBlankOrNull(StringUtils.nullReplaceWithEmpty(oldPeople
								.getAgentPhone()))))) {
					sql_selectprevversion = sql_selectprevversion
							+ " AGENT_PH=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(StringUtils.nullReplaceWithEmpty(people.getAgentPhone())))
							+ " AND ";
					logger.debug("got Agent Phone Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(StringUtils.nullReplaceWithEmpty(people.getAgentPhone()))));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrGeneralLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrGeneralLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  GEN_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()) + " AND ";
					logger.debug("got General Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrAutoLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrAutoLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  AUTO_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()) + " AND ";
					logger.debug("got Auto Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();
			} else if (peopleTypeId.equalsIgnoreCase("B")
					|| peopleTypeId.equalsIgnoreCase("T")
					|| peopleTypeId.equalsIgnoreCase("S")
					|| peopleTypeId.equalsIgnoreCase("N")
					|| peopleTypeId.equalsIgnoreCase("Y")
					|| peopleTypeId.equalsIgnoreCase("W")) {

				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion + "  EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils
						.isBlankOrNull(people.getCopyApplicant())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCopyApplicant())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  COPY_APPLICANT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils
									.checkString(people.getCopyApplicant())
							+ " AND ";
					logger.debug("got Copy Applicant "
							+ StringUtils.checkString(people.getCopyApplicant()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();

			} else if (peopleTypeId.equalsIgnoreCase("O") || peopleTypeId.equalsIgnoreCase("X")) {

				sql_selectprevversion = "Select count(*) as count from people where ";
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got Agent name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion + "  EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getAgentName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAgentName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  AGENT_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getAgentName() + "%") + " AND ";
					logger.debug("got Agent Name " + people.getAgentName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAgentPhone())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAgentPhone())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " AGENT_PH=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getAgentPhone()))
							+ " AND ";
					logger.debug("got Agent Phone Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getAgentPhone())));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrGeneralLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrGeneralLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  GEN_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()) + " AND ";
					logger.debug("got General Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrGeneralLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrAutoLiabilityDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrAutoLiabilityDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  AUTO_LIABILITY_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()) + " AND ";
					logger.debug("got Auto Liability  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrAutoLiabilityDate()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();

			} else if ((peopleTypeId.equalsIgnoreCase("J"))
					|| (peopleTypeId.equalsIgnoreCase("Z"))) {

				logger.debug("people type is either J  or Z " + peopleTypeId);
				sql_selectprevversion = "Select count(*) as count from people where ";

				if ((!(StringUtils.isBlankOrNull(people.getTitle())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getTitle())))) {
					sql_selectprevversion = sql_selectprevversion + "TITLE="
							+ StringUtils.checkString(people.getTitle())
							+ " AND ";
					logger.debug("got Title "
							+ StringUtils.checkString(people.getTitle()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFirstName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFirstName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  LAST_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getFirstName() + "%") + " AND ";
					logger.debug("got Last name " + people.getFirstName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  NAME LIKE "
							+ StringUtils.checkString("%" + people.getName()
									+ "%") + " AND ";
					logger.debug("got First name " + people.getName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getBusinessName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getBusinessName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  BUSINESS_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getBusinessName() + "%") + " AND ";
					logger.debug("got Business name "
							+ people.getBusinessName());
				}
				if ((!(StringUtils
						.isBlankOrNull(people.getCorporateName())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCorporateName())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "CORPORATE_NAME LIKE "
							+ StringUtils.checkString("%"
									+ people.getCorporateName() + "%")
							+ " AND ";
					logger.debug("got Corporate name "
							+ people.getCorporateName());
				}
				if ((!(StringUtils.isBlankOrNull(people.getAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " ADDR LIKE "
							+ StringUtils.checkString("%" + people.getAddress()
									+ "%") + " AND ";
					logger.debug("got Business Address " + people.getAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getCity())))) {
					sql_selectprevversion = sql_selectprevversion + " CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getCity())
							+ " AND ";
					logger.debug("got city "
							+ StringUtils.checkString(people.getCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getState())))) {
					sql_selectprevversion = sql_selectprevversion + "  STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getState())
							+ " AND ";
					logger.debug("got State "
							+ StringUtils.checkString(people.getState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getZipCode())
							+ " AND ";
					logger.debug("got Zip "
							+ StringUtils.checkString(people.getZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_ADDRESS LIKE "
							+ StringUtils.checkString("%"
									+ people.getHomeAddress() + "%") + " AND ";
					logger.debug("got Home Address " + people.getHomeAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeCity())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHomeCity())
							+ " AND ";
					logger.debug("got Home city "
							+ StringUtils.checkString(people.getHomeCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeState())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHomeState())
							+ " AND ";
					logger.debug("got Home State "
							+ StringUtils.checkString(people.getHomeState()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomeZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomeZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  HA_ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHomeZipCode())
							+ " AND ";
					logger.debug("got Home Zip "
							+ StringUtils.checkString(people.getHomeZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHomePhone())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHomePhone())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HA_PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getHomePhone()))
							+ " AND ";
					logger.debug("got Home Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getHomePhone())));
				}
				if ((!(StringUtils.isBlankOrNull(people
						.getMailingAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  MA_ADDRESS LIKE "
							+ StringUtils.checkString("%"
									+ people.getMailingAddress() + "%")
							+ " AND ";
					logger.debug("got Mailing Address "
							+ people.getMailingAddress());
				}
				if ((!(StringUtils.isBlankOrNull(people.getMailingCity())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingCity())))) {
					sql_selectprevversion = sql_selectprevversion + " MA_CITY=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getMailingCity())
							+ " AND ";
					logger.debug("got Mailing city "
							+ StringUtils.checkString(people.getMailingCity()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getMailingState())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingState())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  MA_STATE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getMailingState())
							+ " AND ";
					logger.debug("got Mailing State "
							+ StringUtils.checkString(people.getMailingState()));
				}
				if ((!(StringUtils.isBlankOrNull(people
						.getMailingZipCode())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getMailingZipCode())))) {
					sql_selectprevversion = sql_selectprevversion + "  MA_ZIP=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people
									.getMailingZipCode()) + " AND ";
					logger.debug("got Mailing Zip "
							+ StringUtils.checkString(people
									.getMailingZipCode()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getPhoneNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getPhoneNbr())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  B_PHONE=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr()))
							+ " AND ";
					logger.debug("got Business Phone "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getPhoneNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getExt())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getExt())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  B_PHONE_EXT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getExt())
							+ " AND ";
					logger.debug("got Business Extension "
							+ StringUtils.checkString(people.getExt()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getFaxNbr())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getFaxNbr())))) {
					sql_selectprevversion = sql_selectprevversion + "  FAX=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())) + " AND ";
					logger.debug("got Fax "
							+ StringUtils.checkString(StringUtils
									.phoneFormat(people.getFaxNbr())));
				}
				if ((!(StringUtils.isBlankOrNull(people.getEmailAddress())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEmailAddress())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  EMAIL_ADDR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEmailAddress())
							+ " AND ";
					logger.debug("got Email address "
							+ StringUtils.checkString(people.getEmailAddress()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getDateOfBirthString())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getDateOfBirthString())))) {
					sql_selectprevversion = sql_selectprevversion + "  DOB=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getDateOfBirthString()) + " AND ";
					logger.debug("got  of Birth "
							+ StringUtils.toOracleDate(peopleForm
									.getDateOfBirthString()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getSsn())))
						|| (!(StringUtils
								.isBlankOrNull(oldPeople.getSsn())))) {
					sql_selectprevversion = sql_selectprevversion + "  SSN=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getSsn())
							+ " AND ";
					logger.debug("got SSN " + people.getSsn());
				}
				if ((!(StringUtils.isBlankOrNull(people.getDlNumber())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getDlNumber())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " DL_ID_NO=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getDlNumber())
							+ " AND ";
					logger.debug("got Dl/Id Number "
							+ StringUtils.checkString(people.getDlNumber()));
				}
				if ((!(StringUtils.isBlankOrNull(peopleForm
						.getStrDlExpiryDate())))
						|| (!(StringUtils.isBlankOrNull(oldPeopleForm
								.getStrDlExpiryDate())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  DL_ID_EXP_DT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.toOracleDate(peopleForm
									.getStrDlExpiryDate()) + " AND ";
					logger.debug("got DL expiry  "
							+ StringUtils.toOracleDate(peopleForm
									.getStrDlExpiryDate()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHairColor())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHairColor())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  HAIR_COLOR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHairColor())
							+ " AND ";
					logger.debug("got Hair Color " + people.getHairColor());
				}
				if ((!(StringUtils.isBlankOrNull(people.getEyeColor())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getEyeColor())))) {
					sql_selectprevversion = sql_selectprevversion
							+ " EYE_COLOR=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getEyeColor())
							+ " AND ";
					logger.debug("got Eye color "
							+ StringUtils.checkString(people.getEyeColor()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getHeight())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getHeight())))) {
					sql_selectprevversion = sql_selectprevversion + "  HEIGHT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getHeight())
							+ " AND ";
					logger.debug("got Height " + people.getHeight());
				}
				if ((!(StringUtils.isBlankOrNull(people.getWeight())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getWeight())))) {
					sql_selectprevversion = sql_selectprevversion + " WEIGHT=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getWeight())
							+ " AND ";
					logger.debug("got Weight "
							+ StringUtils.checkString(people.getWeight()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getGender())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getGender())))) {
					sql_selectprevversion = sql_selectprevversion + " GENDER=";
					sql_selectprevversion = sql_selectprevversion
							+ StringUtils.checkString(people.getGender())
							+ " AND ";
					logger.debug("got Gender "
							+ StringUtils.checkString(people.getGender()));
				}
				if ((!(StringUtils.isBlankOrNull(people.getComments())))
						|| (!(StringUtils.isBlankOrNull(oldPeople
								.getComments())))) {
					sql_selectprevversion = sql_selectprevversion
							+ "  COMNTS LIKE "
							+ StringUtils.checkString("%"
									+ people.getComments() + "%") + " AND ";
					logger.debug("got Comments " + people.getComments());
				}
				sql_selectprevversion = sql_selectprevversion + " PEOPLE_ID=";
				sql_selectprevversion = sql_selectprevversion
						+ people.getPeopleId();

			}
			logger.debug("sql_selectprevversion is " + sql_selectprevversion);
			ResultSet rs_peoplecompare = db.select(sql_selectprevversion);

			while (rs_peoplecompare.next()) {
				rowCount = rs_peoplecompare.getInt("count");
				logger.debug("count " + rowCount);
			}
		}

		return rowCount;
	}

	public People setPeople(People people, int count,int userId) throws Exception {
		logger.info("setPeople( people, " + count + ")");

		try {
			Wrapper db = new Wrapper();
			String sql = "";
			String sql1 = "";
			String insertpeopleversionSql = "";
			String selectpeopleversionSql = "";
			String selectpeopleSql = "";
			String selectpeopleSql1 = "";
			int versionNumber = 0;

			int peopleTypeId = 0;

			String name = "";
			String address = "";
			String city = "";
			String state = "";
			String zip = "";
			String phone = "";
			String extension = "";
			String fax = "";
			String emailAddr = "";
			String comments = "";
			String license_no = "";
			Calendar licenseExpDt = Calendar.getInstance();
			String businessLicNo = "";
			Calendar genLiabilityDt = Calendar.getInstance();
			Calendar autoLiabilityDt = Calendar.getInstance();
			Calendar busLicExpDt = Calendar.getInstance();
			String onHold = "";
			Calendar holdDt = Calendar.getInstance();
			String holdComment = "";
			String agentName = "";
			String agentPhone = "";
			double depositBalance = 0;

			Calendar workCompExpDt = Calendar.getInstance();
			String workersCompWaive = "";
			String copyApplicant = "";
			String hicFlag = "";
			String title = "";
			String lastName = "";
			String businessName = "";
			String corporateName = "";
			String haCity = "";
			String haState = "";
			String haZip = "";
			String haZip4 = "";
			String maCity = "";
			String maState = "";
			String maZip = "";
			String maZip4 = "";
			String bPhone = "";
			String hPhone = "";
			String bPhoneExt = "";
			String dlIdNo = "";
			Calendar dlIdExpDt = Calendar.getInstance();
			String ssn = "";
			Calendar dob = Calendar.getInstance();
			String height = "";
			String weight = "";
			String hairColor = "";
			String eyeColor = "";
			String gender = "";
			String haAddress = "";
			String maAddress = "";
			String insuranceType = "";
			String insuranceAmount = "";
			Calendar insuranceExpirationDate = Calendar.getInstance();
			String insuranceProvider = "";
			Calendar created = Calendar.getInstance();
			Calendar updated = Calendar.getInstance();
			int createdBy = 0;
			int updatedBy = 0;
			
			Calendar moveInDate = null;
			Calendar moveOutDate = null;

			db.beginTransaction();
			logger.debug("before creation of sql: contractor =" + people);

			if (people != null) {

				/**
				 * Insert into PEOPLE VERSION table
				 */
				selectpeopleSql = "Select * from people where PEOPLE_ID="
						+ people.getPeopleId();

				logger.debug("Query is " + selectpeopleSql);

				ResultSet rs = db.select(selectpeopleSql);
				while (rs.next()) {
					peopleTypeId = rs.getInt("PEOPLE_TYPE_ID");
					logger.debug("got people id" + peopleTypeId);
					name = rs.getString("NAME");
					address = rs.getString("ADDR");
					city = rs.getString("CITY");
					state = rs.getString("STATE");
					zip = rs.getString("ZIP");
					phone = rs.getString("PHONE");
					extension = rs.getString("EXT");
					fax = rs.getString("FAX");
					emailAddr = rs.getString("EMAIL_ADDR");
					comments = rs.getString("COMNTS");
					license_no = rs.getString("LIC_NO");
					licenseExpDt = StringUtils.dbDate2cal(rs
							.getString("LIC_EXP_DT"));
					businessLicNo = rs.getString("BUS_LIC_NO");
					genLiabilityDt = StringUtils.dbDate2cal(rs
							.getString("GEN_LIABILITY_DT"));
					autoLiabilityDt = StringUtils.dbDate2cal(rs
							.getString("AUTO_LIABILITY_DT"));
					busLicExpDt = StringUtils.dbDate2cal(rs
							.getString("BUS_LIC_EXP_DT"));
					onHold = rs.getString("ON_HOLD");
					holdDt = StringUtils.dbDate2cal(rs.getString("HOLD_DT"));
					holdComment = rs.getString("HOLD_COMNT");
					agentName = rs.getString("AGENT_NAME");
					agentPhone = rs.getString("AGENT_PH");
					depositBalance = rs.getDouble("DEPOSIT_BALANCE");
					workCompExpDt = StringUtils.dbDate2cal(rs
							.getString("WORK_COMP_EXP_DT"));
					workersCompWaive = rs.getString("WORKERS_COMP_WAIVE");
					copyApplicant = rs.getString("COPY_APPLICANT");
					hicFlag = rs.getString("HIC_FLAG");
					title = rs.getString("TITLE");
					lastName = rs.getString("LAST_NAME");
					businessName = rs.getString("BUSINESS_NAME");
					corporateName = rs.getString("CORPORATE_NAME");
					haCity = rs.getString("HA_CITY");
					haState = rs.getString("HA_STATE");
					haZip = rs.getString("HA_ZIP");
					haZip4 = rs.getString("HA_ZIP4");
					maCity = rs.getString("MA_CITY");
					maState = rs.getString("MA_STATE");
					maZip = rs.getString("MA_ZIP");
					maZip4 = rs.getString("MA_ZIP4");
					bPhone = rs.getString("B_PHONE");
					hPhone = rs.getString("HA_PHONE");
					bPhoneExt = rs.getString("B_PHONE_EXT");
					dlIdNo = rs.getString("DL_ID_NO");
					dlIdExpDt = StringUtils.dbDate2cal(rs
							.getString("DL_ID_EXP_DT"));
					ssn = rs.getString("SSN");
					dob = StringUtils.dbDate2cal(rs.getString("DOB"));
					height = rs.getString("HEIGHT");
					weight = rs.getString("WEIGHT");
					hairColor = rs.getString("HAIR_COLOR");
					eyeColor = rs.getString("EYE_COLOR");
					gender = rs.getString("GENDER");
					haAddress = rs.getString("HA_ADDRESS");
					maAddress = rs.getString("MA_ADDRESS");
					insuranceType = rs.getString("INS_TYPE");
					insuranceAmount = rs.getString("INS_AMOUNT");
					insuranceExpirationDate = StringUtils.dbDate2cal(rs
							.getString("INS_EXP_DATE"));
					insuranceProvider = rs.getString("INS_PROVIDER");
					created = StringUtils.dbDate2cal(rs.getString("CREATED"));
					updated = StringUtils.dbDate2cal(rs.getString("UPDATED"));
					createdBy = rs.getInt("CREATED_BY");
					updatedBy = rs.getInt("UPDATED_BY");

					logger.debug("People type id is" + peopleTypeId);
				}
				selectpeopleversionSql = "Select max(VERSION_NO) as version_no from people_version where PEOPLE_ID="
						+ people.getPeopleId();
				logger.debug("selectpeopleversionSql is "
						+ selectpeopleversionSql);
				ResultSet rs_peopleid = db.select(selectpeopleversionSql);

				while (rs_peopleid.next()) {
					versionNumber = rs_peopleid.getInt("version_no");
				}
				if (versionNumber == 0) {
					versionNumber = 1;
				} else {
					versionNumber++;
				}
				
				selectpeopleSql1 = "Select * from PEOPLE_LSOID where PEOPLE_ID="
						+ people.getPeopleId();

				logger.debug("Query to get move in date and  move out date " + selectpeopleSql1);

				ResultSet rs1 = db.select(selectpeopleSql1);
				while(rs1.next()){
					moveInDate = StringUtils.dbDate2cal(rs1.getString("MOVE_IN_DATE"));
					moveOutDate = StringUtils.dbDate2cal(rs1.getString("MOVE_OUT_DATE"));
				}
				
				

				if (count <= 0) {
					insertpeopleversionSql = "insert into PEOPLE_VERSION (PEOPLE_ID, PEOPLE_TYPE_ID, NAME, ADDR, CITY, STATE, ZIP, PHONE, EXT, FAX, EMAIL_ADDR, COMNTS, LIC_NO, LIC_EXP_DT, BUS_LIC_NO, GEN_LIABILITY_DT, AUTO_LIABILITY_DT, BUS_LIC_EXP_DT, ON_HOLD, HOLD_DT, HOLD_COMNT, AGENT_NAME, AGENT_PH, DEPOSIT_BALANCE, WORK_COMP_EXP_DT, WORKERS_COMP_WAIVE, COPY_APPLICANT, HIC_FLAG, TITLE, LAST_NAME, BUSINESS_NAME, CORPORATE_NAME, HA_CITY, HA_STATE, HA_ZIP, HA_ZIP4, MA_CITY, MA_STATE, MA_ZIP, MA_ZIP4, B_PHONE, B_PHONE_EXT, DL_ID_NO, DL_ID_EXP_DT, SSN, DOB, HEIGHT, WEIGHT, HAIR_COLOR, EYE_COLOR, GENDER, HA_ADDRESS, MA_ADDRESS, CREATED, UPDATED, CREATED_BY, UPDATED_BY, VERSION_NO, INS_TYPE, INS_AMOUNT, INS_EXP_DATE, INS_PROVIDER, HA_PHONE, MOVE_IN_DATE, MOVE_OUT_DATE) values (";
					insertpeopleversionSql = insertpeopleversionSql
							+ people.getPeopleId();
					logger.debug("got people id " + people.getPeopleId());
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ peopleTypeId;
					logger.debug("got peopleType id " + peopleTypeId);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(name);
					logger.debug("got Name " + name);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(address);
					logger.debug("got Address " + address);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(city);
					logger.debug("got City " + city);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(state);
					logger.debug("got state " + state);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(zip);
					logger.debug("got zip " + zip);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(phone);
					logger.debug("got zip " + phone);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(extension);
					logger.debug("got ext " + extension);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(fax);
					logger.debug("got fax " + fax);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(emailAddr);
					logger.debug("got email address " + emailAddr);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(comments);
					logger.debug("got comments " + comments);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(license_no);
					logger.debug("got Licence Number " + license_no);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(licenseExpDt));
					logger.debug("got Licence  "
							+ StringUtils.cal2str(licenseExpDt));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(businessLicNo);
					logger.debug("got Business Licence Number " + businessLicNo);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(genLiabilityDt));
					logger.debug("got General Liability  "
							+ StringUtils.checkString(genLiabilityDt));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(autoLiabilityDt));
					logger.debug("got Auto Liability  "
							+ StringUtils.checkString(autoLiabilityDt));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(busLicExpDt));
					logger.debug("got Business Licence  "
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(busLicExpDt)));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(onHold);
					logger.debug("got On Hold " + onHold);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(holdDt));
					logger.debug("got Hold  "
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(holdDt)));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(holdComment);
					logger.debug("got Hold Comment " + holdComment);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(agentName);
					logger.debug("got agent name " + agentName);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(agentPhone);
					logger.debug("got agent phone " + agentPhone);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ depositBalance;
					logger.debug("got Deposit Balance " + depositBalance);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(workCompExpDt));
					logger.debug("got Workers Comp Expiry  "
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(workCompExpDt)));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(workersCompWaive);
					logger.debug("got Workers Comp Waive "
							+ StringUtils.checkString(workersCompWaive));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(copyApplicant);
					logger.debug("got Copy Applicant "
							+ StringUtils.checkString(copyApplicant));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(hicFlag);
					logger.debug("got HIC flag : "
							+ StringUtils.checkString(hicFlag));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(title);
					logger.debug("got Title : "
							+ StringUtils.checkString(title));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(lastName);
					logger.debug("got Last Name : "
							+ StringUtils.checkString(lastName));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(businessName);
					logger.debug("got Business Name : "
							+ StringUtils.checkString(businessName));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(corporateName);
					logger.debug("got Corporate Name : "
							+ StringUtils.checkString(corporateName));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(haCity);
					logger.debug("got Ha City : "
							+ StringUtils.checkString(haCity));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(haState);
					logger.debug("got Ha State : "
							+ StringUtils.checkString(haState));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(haZip);
					logger.debug("got Ha Zip: "
							+ StringUtils.checkString(haZip));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(haZip4);
					logger.debug("got Ha Zip4: "
							+ StringUtils.checkString(haZip4));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(maCity);
					logger.debug("got Ma City : "
							+ StringUtils.checkString(maCity));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(maState);
					logger.debug("got Ma State : "
							+ StringUtils.checkString(maState));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(maZip);
					logger.debug("got Ma Zip: "
							+ StringUtils.checkString(maZip));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(maZip4);
					logger.debug("got Ma Zip4: "
							+ StringUtils.checkString(maZip4));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(bPhone);
					logger.debug("got Business Phone : "
							+ StringUtils.checkString(bPhone));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(bPhoneExt);
					logger.debug("got Business Phone Extension : "
							+ StringUtils.checkString(bPhoneExt));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(dlIdNo);
					logger.debug("got Dl Id No : "
							+ StringUtils.checkString(dlIdNo));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(dlIdExpDt));
					logger.debug("got Dl Id Expiry  "
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(dlIdExpDt)));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(ssn);
					logger.debug("got SSN : " + StringUtils.checkString(ssn));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils
									.toOracleDate(StringUtils.cal2str(dob));
					logger.debug("got DOB "
							+ StringUtils.toOracleDate(StringUtils.cal2str(dob)));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(height);
					logger.debug("got Height : "
							+ StringUtils.checkString(height));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(weight);
					logger.debug("got Weight : "
							+ StringUtils.checkString(weight));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(hairColor);
					logger.debug("got Hair Color : "
							+ StringUtils.checkString(hairColor));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(eyeColor);
					logger.debug("got Eye Color : "
							+ StringUtils.checkString(eyeColor));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(gender);
					logger.debug("got Gender : "
							+ StringUtils.checkString(gender));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(haAddress);
					logger.debug("got Ha Address : "
							+ StringUtils.checkString(haAddress));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(maAddress);
					logger.debug("got Ma Address : "
							+ StringUtils.checkString(maAddress));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ "current_timestamp";
					logger.debug("got Created  " + "current_timestamp");
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ "current_timestamp";
					logger.debug("got Updated  " + "current_timestamp");
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql + "null";
					logger.debug("got Created By " + "");
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql + "null";
					logger.debug("got Updated By " + "");
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ versionNumber;
					logger.debug("got version Number : " + versionNumber);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(insuranceType);
					logger.debug("got insurance type : "
							+ StringUtils.checkString(insuranceType));
					insertpeopleversionSql = insertpeopleversionSql + ",";

					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(insuranceAmount);
					logger.debug("got insurance amount : "
							+ StringUtils.checkString(insuranceAmount));
					insertpeopleversionSql = insertpeopleversionSql + ",";

					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(insuranceExpirationDate));
					logger.debug("got insurance expiration  "
							+ StringUtils.toOracleDate(StringUtils
									.cal2str(insuranceExpirationDate)));
					insertpeopleversionSql = insertpeopleversionSql + ",";

					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(insuranceProvider);
					logger.debug("got insurance provider : "
							+ StringUtils.checkString(insuranceProvider));
					insertpeopleversionSql = insertpeopleversionSql + ",";

					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.checkString(hPhone);
					logger.debug("got Home Phone : " + hPhone);
					insertpeopleversionSql = insertpeopleversionSql + ",";
					
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils.cal2str(moveInDate));
					logger.debug("got moveInDate  : " + StringUtils.toOracleDate(StringUtils.cal2str(moveInDate)));
					insertpeopleversionSql = insertpeopleversionSql + ",";
					
					insertpeopleversionSql = insertpeopleversionSql
							+ StringUtils.toOracleDate(StringUtils.cal2str(moveOutDate));
					logger.debug("got moveOutDate : " + StringUtils.toOracleDate(StringUtils.cal2str(moveOutDate)));
					insertpeopleversionSql = insertpeopleversionSql + ")";
					logger.info(insertpeopleversionSql);
					db.insert(insertpeopleversionSql);

				}

				/*
				 * Update The Data Into People Table
				 */
				
				sql = "UPDATE PEOPLE SET NAME=";
				sql = sql + StringUtils.checkString(people.getName());
				logger.debug("got people name "
						+ StringUtils.checkString(people.getName()));
				sql = sql + ",ADDR=";
				sql = sql + StringUtils.checkString(people.getAddress());
				logger.debug("got people Address "
						+ StringUtils.checkString(people.getAddress()));
				sql = sql + ",CITY=";
				sql = sql + StringUtils.checkString(people.getCity());
				logger.debug("got people City "
						+ StringUtils.checkString(people.getCity()));
				sql = sql + ",STATE=";
				sql = sql + StringUtils.checkString(people.getState());
				logger.debug("got people state "
						+ StringUtils.checkString(people.getState()));
				sql = sql + ",ZIP=";
				sql = sql + StringUtils.checkString(people.getZipCode());
				logger.debug("got people zipcode "
						+ StringUtils.checkString(people.getZipCode()));
				sql = sql + ",PHONE=";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr()));
				logger.debug("got people phone number "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr())));
				sql = sql + ",EXT=";
				sql = sql + StringUtils.checkString(people.getExt());
				logger.debug("got people ext number "
						+ StringUtils.checkString(people.getExt()));
				sql = sql + ",FAX=";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getFaxNbr()));
				logger.debug("got people fax number "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getFaxNbr())));
				sql = sql + ",EMAIL_ADDR=";
				sql = sql + StringUtils.checkString(people.getEmailAddress());
				logger.debug("got people email addr "
						+ StringUtils.checkString(people.getEmailAddress()));
				sql = sql + ",COMNTS=";
				sql = sql + StringUtils.checkString(people.getComments());
				logger.debug("got people comments "
						+ StringUtils.checkString(people.getComments()));
				sql = sql + ",COPY_APPLICANT=";
				sql = sql + StringUtils.checkString(people.getCopyApplicant());
				logger.debug("got copy applicant "
						+ StringUtils.checkString(people.getCopyApplicant()));
				sql = sql + ",AGENT_NAME=";
				sql = sql + StringUtils.checkString(people.getAgentName());
				logger.debug("got agent Name "
						+ StringUtils.checkString(people.getAgentName()));
				sql = sql + ",AGENT_PH=";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getAgentPhone()));
				logger.debug("got agent phone "
						+ StringUtils.checkString(people.getAgentPhone()));
				sql = sql + ",LIC_NO=";
				sql = sql + StringUtils.checkString(people.getLicenseNbr());
				logger.debug("got people license number "
						+ StringUtils.checkString(people.getLicenseNbr()));
				sql = sql + ",LIC_EXP_DT=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getLicenseExpires()));
				logger.debug("got people lic_exp_date "
						+ StringUtils.checkString(StringUtils.cal2str(people
								.getLicenseExpires())));
				sql = sql + ",ON_HOLD=";
				sql = sql + StringUtils.checkString(people.getOnHold());
				logger.debug("got on hold "
						+ StringUtils.checkString(people.getOnHold()));
				sql = sql + ",BUS_LIC_NO=";
				sql = sql
						+ StringUtils.checkString(people
								.getBusinessLicenseNbr());
				logger.debug("got people BUS_LIC_NO "
						+ StringUtils.checkString(people
								.getBusinessLicenseNbr()));
				sql = sql + ",GEN_LIABILITY_DT=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getGeneralLiabilityDate()));
				logger.debug("got people GEN_LIABILITY_DT "
						+ StringUtils.checkString(StringUtils.cal2str(people
								.getGeneralLiabilityDate())));
				sql = sql + ",HOLD_DT=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getHoldDate()));
				logger.debug("got people hold  "
						+ StringUtils.checkString(StringUtils.cal2str(people
								.getHoldDate())));
				sql = sql + ",HOLD_COMNT=";
				sql = sql + StringUtils.checkString(people.getHasHold());
				logger.debug("got people GEN_LIABILITY_DT "
						+ StringUtils.checkString(people.getHasHold()));
				sql = sql + ",AUTO_LIABILITY_DT=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getAutoLiabilityDate()));
				logger.debug("got AUTO_LIABILITY_DT "
						+ StringUtils.checkString(StringUtils.cal2str(people
								.getAutoLiabilityDate())));
				sql = sql + ",BUS_LIC_EXP_DT=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getBusinessLicenseExpires()));
				logger.debug("got BUS_LIC_EXP_DT "
						+ StringUtils.checkString(StringUtils.cal2str(people
								.getBusinessLicenseExpires())));
				sql = sql + ",WORK_COMP_EXP_DT=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getWorkersCompExpires()));
				logger.debug("got WORK_COMP_EXP_DT "
						+ StringUtils.checkString(StringUtils.cal2str(people
								.getWorkersCompExpires())));
				sql = sql + ",WORKERS_COMP_WAIVE=";
				sql = sql
						+ StringUtils.checkString(people
								.getWorkersCompensationWaive());
				logger.debug("got WORKERS_COMP_WAIVE "
						+ StringUtils.checkString(people
								.getWorkersCompensationWaive()));
				sql = sql + ",HIC_FLAG="
						+ StringUtils.checkString(people.getHicFlag());
				logger.debug("got hic_flag : "
						+ StringUtils.checkString(people.getHicFlag()));
				sql = sql + ",DEPOSIT_BALANCE=" + depositBalance;
				logger.debug("got Deposit Balance : " + depositBalance);
				sql = sql + ",TITLE="
						+ StringUtils.checkString(people.getTitle());
				logger.debug("got Title : "
						+ StringUtils.checkString(people.getTitle()));
				sql = sql + ",LAST_NAME="
						+ StringUtils.checkString(people.getFirstName());
				logger.debug("got Last Name : "
						+ StringUtils.checkString(people.getFirstName()));
				sql = sql + ",BUSINESS_NAME="
						+ StringUtils.checkString(people.getBusinessName());
				logger.debug("got Business Name : "
						+ StringUtils.checkString(people.getBusinessName()));
				sql = sql + ",CORPORATE_NAME="
						+ StringUtils.checkString(people.getCorporateName());
				logger.debug("got Corporate Name : "
						+ StringUtils.checkString(people.getCorporateName()));
				sql = sql + ",HA_CITY="
						+ StringUtils.checkString(people.getHomeCity());
				logger.debug("got Home city : "
						+ StringUtils.checkString(people.getHomeCity()));
				sql = sql + ",HA_STATE="
						+ StringUtils.checkString(people.getHomeState());
				logger.debug("got Home State : "
						+ StringUtils.checkString(people.getHomeState()));
				sql = sql + ",HA_ZIP="
						+ StringUtils.checkString(people.getHomeZipCode());
				logger.debug("got Home zipCode : "
						+ StringUtils.checkString(people.getHomeZipCode()));
				sql = sql + ",HA_ZIP4="
						+ StringUtils.checkString(people.getHomeZipCode4());
				logger.debug("got Home zipCode4 : "
						+ StringUtils.checkString(people.getHomeZipCode4()));
				sql = sql + ",MA_CITY="
						+ StringUtils.checkString(people.getMailingCity());
				logger.debug("got Mailing city : "
						+ StringUtils.checkString(people.getMailingCity()));
				sql = sql + ",MA_STATE="
						+ StringUtils.checkString(people.getMailingState());
				logger.debug("got Mailing State : "
						+ StringUtils.checkString(people.getMailingState()));
				sql = sql + ",MA_ZIP="
						+ StringUtils.checkString(people.getMailingZipCode());
				logger.debug("got Mailing zipCode : "
						+ StringUtils.checkString(people.getMailingZipCode()));
				sql = sql + ",MA_ZIP4="
						+ StringUtils.checkString(people.getMailingZipCode4());
				logger.debug("got Mailing zipCode4 : "
						+ StringUtils.checkString(people.getMailingZipCode4()));
				sql = sql + ",B_PHONE=";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr()));
				logger.debug("got Business phone number "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr())));
				sql = sql + ",B_PHONE_EXT=";
				sql = sql + StringUtils.checkString(people.getExt());
				logger.debug("got Business ext number "
						+ StringUtils.checkString(people.getExt()));
				sql = sql + ",DL_ID_NO="
						+ StringUtils.checkString(people.getDlNumber());
				logger.debug("got Drivinig License Id Number : "
						+ StringUtils.checkString(people.getDlNumber()));
				sql = sql
						+ ",DL_ID_EXP_DT="
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDlExpiryDate()));
				logger.debug("got Drivinig License Id Exp  : "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDlExpiryDate())));
				sql = sql + ",SSN=" + StringUtils.checkString(people.getSsn());
				logger.debug("got SSN : "
						+ StringUtils.checkString(people.getSsn()));
				sql = sql + ",DOB=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDateOfBirth()));
				logger.debug("got  of birth "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDateOfBirth())));
				sql = sql + ",HEIGHT="
						+ StringUtils.checkString(people.getHeight());
				logger.debug("got HEIGHT : "
						+ StringUtils.checkString(people.getHeight()));
				sql = sql + ",WEIGHT="
						+ StringUtils.checkString(people.getWeight());
				logger.debug("got WEIGHT : "
						+ StringUtils.checkString(people.getWeight()));
				sql = sql + ",HAIR_COLOR="
						+ StringUtils.checkString(people.getHairColor());
				logger.debug("got Hair color : "
						+ StringUtils.checkString(people.getHairColor()));
				sql = sql + ",EYE_COLOR="
						+ StringUtils.checkString(people.getEyeColor());
				logger.debug("got Eye color : "
						+ StringUtils.checkString(people.getEyeColor()));
				sql = sql + ",GENDER="
						+ StringUtils.checkString(people.getGender());
				logger.debug("got Gender : "
						+ StringUtils.checkString(people.getGender()));
				sql = sql + ",HA_ADDRESS="
						+ StringUtils.checkString(people.getHomeAddress());
				logger.debug("got Home Address : "
						+ StringUtils.checkString(people.getHomeAddress()));
				sql = sql + ",MA_ADDRESS="
						+ StringUtils.checkString(people.getMailingAddress());
				logger.debug("got Mailing Address : "
						+ StringUtils.checkString(people.getMailingAddress()));
				sql = sql + ",INS_TYPE="
						+ StringUtils.checkString(people.getInsuranceType());
				logger.debug("got INSURANCE TYPE : "
						+ StringUtils.checkString(people.getInsuranceType()));
				sql = sql + ",INS_AMOUNT=" + people.getInsuranceAmount();
				logger.debug("got INSURANCE AMOUNT : "
						+ people.getInsuranceAmount());
				sql = sql + ",UPDATED=" + "current_timestamp";
				logger.debug("got Updated  : " + "current_timestamp");
				sql = sql + ",UPDATED_BY=" + userId;
				logger.debug("got Updated by : " + userId);

				sql = sql + ",INS_EXP_DATE=";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getInsuranceExpirationDate()));
				logger.debug("got insurance provider "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getInsuranceExpirationDate())));

				sql = sql + ",INS_PROVIDER=";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getInsuranceProvider()));
				logger.debug("got insurance provider "
						+ StringUtils.checkString(people.getInsuranceProvider()));
				
				sql = sql + ",HOUSEHOLD_INCOME=";
				sql = sql
						+ StringUtils.$2dbl(people.getHouseholdIncome());
				logger.debug("got getHousehold Income "
						+ StringUtils.$2dbl(people.getHouseholdIncome()));

				sql = sql + " WHERE PEOPLE_ID=";
				sql = sql + people.getPeopleId();

				int count1 = 0;
				String peopleSql = "select count(*) as count from PEOPLE_LSOID where PEOPLE_ID=" + people.getPeopleId();
				ResultSet rs2 = db.select(peopleSql);
				if(rs2.next()){
					count1 = rs2.getInt("count");					
				}
				if(count1 > 0){
					// Update PEOPLE_LSOID 
					sql1 = "UPDATE PEOPLE_LSOID SET MOVE_IN_DATE=";
					sql1 = sql1 + StringUtils.toOracleDate((people.getMoveInDate()));
					logger.debug("got people MoveInDate "
							+ StringUtils.toOracleDate((people.getMoveInDate())));
					sql1 = sql1 + ",MOVE_OUT_DATE=";
					sql1 = sql1 + StringUtils.toOracleDate((people.getMoveOutDate()));
					logger.debug("got people MoveOutDate "
							+ StringUtils.toOracleDate((people.getMoveOutDate())));
					sql1 = sql1 + ",NET_RENT=";
					sql1 = sql1 + StringUtils.$2dbl(people.getNetRent());
					logger.debug("got people NET_RENT "
							+ StringUtils.$2dbl(people.getNetRent()));
					sql1 = sql1 + ",UNIT_DESIGNATION=";
					sql1 = sql1 + StringUtils.checkString(people.getUnitDesignation());
					logger.debug("got people UNIT_DESIGNATION "
							+ StringUtils.checkString(people.getUnitDesignation()));
					sql1 = sql1 + " WHERE PEOPLE_ID=";
					sql1 = sql1 + people.getPeopleId();
				} else{
					// INSERT PEOPLE_LSOID 
					int peopleLsoId = db.getNextId("PEOPLE_LSO_ID");
					sql1="INSERT INTO PEOPLE_LSOID (ID, PEOPLE_ID, LSO_ID, MOVE_IN_DATE, MOVE_OUT_DATE, NET_RENT, UNIT_DESIGNATION) VALUES (";
					sql1 = sql1 + peopleLsoId;
					logger.debug("got people Lso id " + peopleLsoId);
					sql1 = sql1 + ",";
					sql1 = sql1 + people.getPeopleId();
					logger.debug("got people id " + people.getPeopleId());
					sql1 = sql1 + ",";
					sql1 = sql1 + StringUtils.checkNumber(people.getLsoId());
					logger.debug("got Lso id " + StringUtils.checkNumber(people.getLsoId()));
					sql1 = sql1 + ",";
					sql1 = sql1 + StringUtils.toOracleDate((people.getMoveInDate()));
					logger.debug("got MoveInDAte " + StringUtils.toOracleDate((people.getMoveInDate())));
					sql1 = sql1 + ",";
					sql1 = sql1 + StringUtils.toOracleDate(people.getMoveOutDate());
					logger.debug("got MoveOutDate" + StringUtils.toOracleDate(people.getMoveOutDate()));
					sql1 = sql1 + ",";
					sql1 = sql1 + StringUtils.$2dbl(people.getNetRent());
					logger.debug("got NetRent" + StringUtils.$2dbl(people.getNetRent()));
					sql1 = sql1 + ",";
					sql1 = sql1 + StringUtils.checkString(people.getUnitDesignation());
					logger.debug("got UnitDesignation" + StringUtils.checkString((people.getUnitDesignation())));
					sql1 = sql1 + ")";
				}
				
			}

			logger.info(sql);
			db.update(sql);
			
			logger.info(sql1);
			db.update(sql1);

			updateOnlineUser(people);

			return people;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public People addPeople(People people,int userId) throws Exception {
		logger.info("addPeople(people)");

		try {
			Wrapper db = new Wrapper();
			String sql = "";

			if (people != null) {
				int peopleId = db.getNextId("PEOPLE_ID");
				logger.debug("The ID generated for new People is " + peopleId);
				sql = "insert into people(PEOPLE_ID, PEOPLE_TYPE_ID, NAME, ADDR, CITY, STATE, ZIP, PHONE, EXT, FAX, EMAIL_ADDR, COMNTS, LIC_NO, LIC_EXP_DT, BUS_LIC_NO, GEN_LIABILITY_DT, AUTO_LIABILITY_DT, BUS_LIC_EXP_DT, ON_HOLD, HOLD_DT, HOLD_COMNT, AGENT_NAME, AGENT_PH, DEPOSIT_BALANCE, WORK_COMP_EXP_DT, WORKERS_COMP_WAIVE,COPY_APPLICANT, HIC_FLAG, TITLE, LAST_NAME, BUSINESS_NAME, CORPORATE_NAME, HA_CITY, HA_STATE, HA_ZIP, HA_ZIP4, MA_CITY, MA_STATE, MA_ZIP, MA_ZIP4, B_PHONE, B_PHONE_EXT, DL_ID_NO, DL_ID_EXP_DT, SSN, DOB, HEIGHT, WEIGHT, HAIR_COLOR, EYE_COLOR, GENDER, HA_ADDRESS, MA_ADDRESS, CREATED, CREATED_BY,UPDATED,UPDATED_BY, INS_TYPE,INS_AMOUNT,INS_EXP_DATE,INS_PROVIDER,HA_PHONE,HOUSEHOLD_INCOME) values(";
				sql = sql + peopleId;
				logger.debug("got people id " + peopleId);
				sql = sql + ",";
				sql = sql + people.getPeopleType().getTypeId();
				logger.debug("got peopleType id "
						+ people.getPeopleType().getTypeId());
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getName());
				logger.debug("got Name "
						+ StringUtils.checkString(people.getName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getAddress());
				logger.debug("got Address "
						+ StringUtils.checkString(people.getAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getCity());
				logger.debug("got City "
						+ StringUtils.checkString(people.getCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getState());
				logger.debug("got state "
						+ StringUtils.checkString(people.getState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getZipCode());
				logger.debug("got zip "
						+ StringUtils.checkString(people.getZipCode()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr()));
				logger.debug("got Phone : "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getExt());
				logger.debug("got ext "
						+ StringUtils.checkString(people.getExt()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getFaxNbr()));
				logger.debug("got fax : "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getFaxNbr())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getEmailAddress());
				logger.debug("got email address "
						+ StringUtils.checkString(people.getEmailAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getComments());
				logger.debug("got comments "
						+ StringUtils.checkString(people.getComments()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getLicenseNbr());
				logger.debug("got Licence Number "
						+ StringUtils.checkString(people.getLicenseNbr()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getLicenseExpires()));
				logger.debug("got Licence  "
						+ StringUtils.cal2str(people.getLicenseExpires()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(people
								.getBusinessLicenseNbr());
				logger.debug("got Business Licence Number "
						+ StringUtils.checkString(people
								.getBusinessLicenseNbr()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getGeneralLiabilityDate()));
				logger.debug("got General Liability  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getGeneralLiabilityDate())));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getAutoLiabilityDate()));
				logger.debug("got Auto Liability  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getAutoLiabilityDate())));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getBusinessLicenseExpires()));
				logger.debug("got Business Licence  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getBusinessLicenseExpires())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getOnHold());
				logger.debug("got On Hold "
						+ StringUtils.checkString(people.getOnHold()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getHoldDate()));
				logger.debug("got Hold  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getHoldDate())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHoldComments());
				logger.debug("got Hold comment "
						+ StringUtils.checkString(people.getHoldComments()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getAgentName());
				logger.debug("got agent name "
						+ StringUtils.checkString(people.getAgentName()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getAgentPhone()));
				logger.debug("got agent phone "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getAgentPhone())));
				sql = sql + ",";
				sql = sql + people.getDepositAmount();
				logger.debug("got Deposit Amount " + people.getDepositAmount());
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getWorkersCompExpires()));
				logger.debug("got Workers Comp Expiry  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getWorkersCompExpires())));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(people
								.getWorkersCompensationWaive());
				logger.debug("got Workers Comp Waive "
						+ StringUtils.checkString(people
								.getWorkersCompensationWaive()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getCopyApplicant());
				logger.debug("got Copy Applicant "
						+ StringUtils.checkString(people.getCopyApplicant()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHicFlag());
				logger.debug("got HIC flag : "
						+ StringUtils.checkString(people.getHicFlag()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getTitle());
				logger.debug("got Title : "
						+ StringUtils.checkString(people.getTitle()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getFirstName());
				logger.debug("got Last Name : "
						+ StringUtils.checkString(people.getFirstName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getBusinessName());
				logger.debug("got Business Name : "
						+ StringUtils.checkString(people.getBusinessName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getCorporateName());
				logger.debug("got Corporate Name : "
						+ StringUtils.checkString(people.getCorporateName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHomeCity());
				logger.debug("got Home City : "
						+ StringUtils.checkString(people.getHomeCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHomeState());
				logger.debug("got Home State : "
						+ StringUtils.checkString(people.getHomeState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHomeZipCode());
				logger.debug("got Home zip : "
						+ StringUtils.checkString(people.getHomeZipCode()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHomeZipCode4());
				logger.debug("got Home zip4 : "
						+ StringUtils.checkString(people.getHomeZipCode4()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getMailingCity());
				logger.debug("got Mailing City : "
						+ StringUtils.checkString(people.getMailingCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getMailingState());
				logger.debug("got Mailing state : "
						+ StringUtils.checkString(people.getMailingState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getMailingZipCode());
				logger.debug("got Mailing Zip : "
						+ StringUtils.checkString(people.getMailingZipCode()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(people.getMailingZipCode4());
				logger.debug("got Mailing Zip4 : "
						+ StringUtils.checkString(people.getMailingZipCode4()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr()));
				logger.debug("got Business Phone : "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getExt());
				logger.debug("got Business phone ext "
						+ StringUtils.checkString(people.getExt()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getDlNumber());
				logger.debug("got Driving License ID number  : "
						+ StringUtils.checkString(people.getDlNumber()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDlExpiryDate()));
				logger.debug("got Driving License ID Expiry  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDlExpiryDate())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getSsn());
				logger.debug("got SSN  : "
						+ StringUtils.checkString(people.getSsn()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDateOfBirth()));
				logger.debug("got  of Birth "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getDateOfBirth())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHeight());
				logger.debug("got Height  : "
						+ StringUtils.checkString(people.getHeight()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getWeight());
				logger.debug("got Weight  : "
						+ StringUtils.checkString(people.getWeight()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHairColor());
				logger.debug("got Hair Color  : "
						+ StringUtils.checkString(people.getHairColor()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getEyeColor());
				logger.debug("got Eye Color : "
						+ StringUtils.checkString(people.getEyeColor()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getGender());
				logger.debug("got Gender  : "
						+ StringUtils.checkString(people.getGender()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getHomeAddress());
				logger.debug("got Home Address  : "
						+ StringUtils.checkString(people.getHomeAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getMailingAddress());
				logger.debug("got Mailing Address  : "
						+ StringUtils.checkString(people.getMailingAddress()));
				sql = sql + ",";
				sql = sql + "current_timestamp";
				logger.debug("got Created  " + "current_timestamp");
				sql = sql + ",";
				sql = sql + userId;
				logger.debug("got Created by  : " + "null");
				sql = sql + ",";
				sql = sql + "current_timestamp";
				logger.debug("got updated  " + "current_timestamp");
				sql = sql + ",";
				sql = sql + userId;
				logger.debug("got updated by  : " + "null");
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getInsuranceType());
				logger.debug("got insurance type  : "
						+ StringUtils.checkString(people.getInsuranceType()));
				sql = sql + ",";
				sql = sql + people.getInsuranceAmount();
				logger.debug("got insurance amount  : "
						+ people.getInsuranceAmount());
				sql = sql + ",";
				sql = sql
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getInsuranceExpirationDate()));
				logger.debug("got insurance expiration  "
						+ StringUtils.toOracleDate(StringUtils.cal2str(people
								.getInsuranceExpirationDate())));
				sql = sql + ",";
				sql = sql
						+ StringUtils
								.checkString(people.getInsuranceProvider());
				logger.debug("got insurance provider  : "
						+ StringUtils.checkString(people.getInsuranceProvider()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getHomePhone()));
				logger.debug("got Home Phone : "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getHomePhone())));
				sql = sql + ",";
				sql = sql
						+ StringUtils.$2dbl(people.getHouseholdIncome());
				logger.debug("got Household Income : "
						+ StringUtils.$2dbl(people.getHouseholdIncome()));
				sql = sql + ")";
				logger.info(sql);
				db.insert(sql);
				people.setPeopleId(people.getPeopleId());
				

				logger.debug("getLsoId ::  "+people.getLsoId());
				logger.debug("getIsFromOccupancy ::   "+people.getIsFromOccupancy());
				if(people.getLsoId() != null && people.getIsFromOccupancy().equalsIgnoreCase("O")){
					

					int peopleLsoId = db.getNextId("PEOPLE_LSO_ID");
					sql="INSERT INTO PEOPLE_LSOID (ID, PEOPLE_ID, LSO_ID, MOVE_IN_DATE, MOVE_OUT_DATE, NET_RENT, UNIT_DESIGNATION) " +
							"VALUES (";

					sql = sql + peopleLsoId;
					logger.debug("got people Lso id " + peopleLsoId);
					sql = sql + ",";
					sql = sql + peopleId;
					logger.debug("got people id " + peopleId);
					sql = sql + ",";
					sql = sql + StringUtils.checkNumber(people.getLsoId());
					logger.debug("got Lso id " + StringUtils.checkNumber(people.getLsoId()));
					sql = sql + ",";
					sql = sql + StringUtils.toOracleDate((people.getMoveInDate()));
					logger.debug("got MoveInDAte " + StringUtils.toOracleDate((people.getMoveInDate())));
					sql = sql + ",";
					sql = sql + StringUtils.toOracleDate(people.getMoveOutDate());
					logger.debug("got MoveOutDate" + StringUtils.toOracleDate(people.getMoveOutDate()));
					sql = sql + ",";
					sql = sql + StringUtils.$2dbl(people.getNetRent());
					logger.debug("got NetRent" + StringUtils.$2dbl(people.getNetRent()));
					sql = sql + ",";
					sql = sql + StringUtils.checkString(people.getUnitDesignation());
					logger.debug("got UnitDesignation" + StringUtils.checkString((people.getUnitDesignation())));

					sql = sql + ")";
					logger.info(sql);
					db.insert(sql);
				}
				return getPeople(peopleId);
				
			} else {
				return new People();
			}
		} catch (Exception e) {
			logger.error(""+e+e.getMessage());
			throw e;
		}
	}

	public int getLicenseNbr() throws Exception {
		logger.info("getLicenseNbr()");

		int licenseNbr = -1;

		try {
			Wrapper db = new Wrapper();
			licenseNbr = db.getNextId("LICENSE_NO");
			logger.debug("The ID generated for new Address is " + licenseNbr);

			return licenseNbr;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public People copyApplicant(People people) throws Exception {
		logger.info("copyApplicant(people)");

		try {
			Wrapper db = new Wrapper();
			String sql = "";

			if (people != null) {
				int peopleId = db.getNextId("PEOPLE_ID");
				logger.debug("The ID generated for new Address is " + peopleId);
				db.beginTransaction();
				sql = "insert into people(PEOPLE_ID,PEOPLE_TYPE_ID,NAME,ADDR,CITY,STATE,ZIP,PHONE,EXT,EMAIL_ADDR,FAX,COMNTS) values(";
				sql = sql + peopleId;
				logger.debug("got people id " + peopleId);
				sql = sql + ",2,";
				sql = sql + StringUtils.checkString(people.getName());
				logger.debug("got Name "
						+ StringUtils.checkString(people.getName()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getAddress());
				logger.debug("got Address "
						+ StringUtils.checkString(people.getAddress()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getCity());
				logger.debug("got City "
						+ StringUtils.checkString(people.getCity()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getState());
				logger.debug("got state "
						+ StringUtils.checkString(people.getState()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getZipCode());
				logger.debug("got zip "
						+ StringUtils.checkString(people.getZipCode()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr()));
				logger.debug("got Phone number "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getPhoneNbr())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getExt());
				logger.debug("got ext "
						+ StringUtils.checkString(people.getExt()));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getEmailAddress());
				logger.debug("got email address "
						+ StringUtils.checkString(people.getEmailAddress()));
				sql = sql + ",";
				sql = sql
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getFaxNbr()));
				logger.debug("got fax "
						+ StringUtils.checkString(StringUtils
								.phoneFormat(people.getFaxNbr())));
				sql = sql + ",";
				sql = sql + StringUtils.checkString(people.getComments());
				logger.debug("got comments "
						+ StringUtils.checkString(people.getComments()));
				sql = sql + ")";
				logger.info(sql);
				db.insert(sql);
				people = (new PeopleAgent()).getPeople(peopleId);
			} else {
				logger.error(" People is not Created");
			}

			return people;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public static String getPeopleType(String code) throws Exception {
		logger.info("getPeopleType(" + code + ")");

		String description = "";

		try {
			Map m = new HashMap();
			m.put("C", "Contractor");
			m.put("E", "Engineer");
			m.put("R", "Architect");
			m.put("D", "Designer");
			m.put("A", "Applicant");
			m.put("O", "Other");
			m.put("W", "Owner");
			m.put("B", "Owner/Builder");
			m.put("G", "Agent");
			m.put("T", "Tenant");
			description = (String) m.get(code);

			return description;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public int checkPeople4PSAlevel(String peopleTypeId, String licenseNumber,
			int psaId, String psaType) throws Exception {
		logger.info("checkPeople4PSAlevel(" + peopleTypeId + ", "
				+ licenseNumber + ", " + psaId + ", " + psaType + ")");

		int nbr = -1;

		try {
			String sql = "SELECT count(ap.PEOPLE_ID) nbr FROM activity_people ap WHERE ap.PEOPLE_id =(SELECT DISTINCT P.PEOPLE_ID FROM PEOPLE P ,PEOPLE_TYPE PT WHERE (PT.CODE = '"
					+ peopleTypeId
					+ "') AND P.PEOPLE_TYPE_ID=PT.PEOPLE_TYPE_ID  and UPPER(P.LIC_NO) LIKE upper('"
					+ licenseNumber
					+ "')) and act_id = "
					+ psaId
					+ " and psa_type='" + psaType + "'";
			logger.info(sql);

			for (javax.sql.RowSet rs = (new Wrapper()).select(sql); rs.next();) {
				nbr = rs.getInt("nbr");
			}

			return nbr;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public int countProjPeople(String psaId, String ownerName,
			String peopleTypeId, String LicenseNo) throws Exception {
		logger.info("countProjPeople(" + psaId + ", " + ownerName + ", "
				+ peopleTypeId + ", " + LicenseNo + ")");

		int nbr = -1;
		ownerName = "%" + ownerName + "%";

		try {
			String sql = "SELECT  count(ap.PEOPLE_ID) as cnt FROM ACTIVITY_PEOPLE ap,PEOPLE p WHERE p.PEOPLE_ID = ap.PEOPLE_ID AND ap.ACT_ID ="
					+ psaId + " AND ap.PSA_TYPE = 'P' ";

			if (peopleTypeId.equalsIgnoreCase("W")
					|| peopleTypeId.equalsIgnoreCase("A")
					|| peopleTypeId.equalsIgnoreCase("O")) {
				sql = sql + " AND  p.NAME like "
						+ StringUtils.checkString(ownerName) + "";
			}

			if (peopleTypeId.equalsIgnoreCase("C")
					|| peopleTypeId.equalsIgnoreCase("E")
					|| peopleTypeId.equalsIgnoreCase("R")
					|| peopleTypeId.equalsIgnoreCase("D")) {
				sql = sql + " AND p.LIC_NO='" + LicenseNo + "'";
			}

			logger.info(sql);

			for (javax.sql.RowSet rs = (new Wrapper()).select(sql); rs.next();) {
				nbr = rs.getInt("cnt");
			}

			return nbr;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public List getPeopleOwnerList(String projectId, String activityId,
			String peopleTypeId) throws Exception {
		logger.info("getPeopleOwnerList(" + projectId + ", " + activityId
				+ ", " + peopleTypeId + ")");

		List peoples = new ArrayList();

		try {
			String sql = "SELECT DISTINCT P.PEOPLE_ID,P.NAME,P.LIC_NO,H.HOLD_LEVEL FROM PEOPLE P LEFT OUTER JOIN HOLDS H ON H.LEVEL_ID=P.PEOPLE_ID AND H.HOLD_LEVEL='Z' AND H.STAT='A' WHERE P.PEOPLE_ID IN((SELECT DISTINCT AP.PEOPLE_ID FROM (ACTIVITY_PEOPLE AP JOIN PEOPLE P ON AP.PEOPLE_ID = P.PEOPLE_ID) JOIN PEOPLE_TYPE PT ON P.PEOPLE_TYPE_ID = PT.PEOPLE_TYPE_ID AND UPPER(PT.CODE) =UPPER('"
					+ peopleTypeId
					+ "') "
					+ "WHERE AP.ACT_ID = "
					+ projectId
					+ " AND AP.PSA_TYPE ='P')) "
					+ "AND (P.PEOPLE_ID NOT IN(SELECT DISTINCT AP.PEOPLE_ID "
					+ "FROM (ACTIVITY_PEOPLE AP JOIN PEOPLE P ON AP.PEOPLE_ID = P.PEOPLE_ID) JOIN PEOPLE_TYPE PT ON P.PEOPLE_TYPE_ID = PT.PEOPLE_TYPE_ID AND UPPER(PT.CODE) =UPPER('"
					+ peopleTypeId
					+ "') "
					+ "WHERE AP.ACT_ID = "
					+ activityId
					+ " AND AP.PSA_TYPE ='A'))";
			logger.info(sql);

			People people;

			for (javax.sql.RowSet rs = (new Wrapper()).select(sql); rs.next(); peoples
					.add(people)) {
				people = new People();
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setName(rs.getString("NAME"));

				if (rs.getString("LIC_NO") != null) {
					people.setLicenseNbr(rs.getString("LIC_NO"));
				} else {
					people.setLicenseNbr("0");
				}

				String holdSql = "select * from ( select * from holds where  hold_level='Z' and level_id="
						+ people.getPeopleId()
						+ " order by update_dt desc) where ROWNUM<=1";
				logger.info("Recent Holds sql :: " + holdSql);

				javax.sql.RowSet holdRs = (new Wrapper()).select(holdSql);

				if (holdRs.next()) {
					if (holdRs.getString("STAT").trim().equalsIgnoreCase("A")) {
						people.setOnHold("Y");
					} else {
						people.setOnHold("N");
					}
				}
			}

			return peoples;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public List getSearchResults(String peopleTypeId, String licenseNumber,
			String name, String phoneNumber, String phoneExt) throws Exception {
		logger.info("getSearchResults(" + peopleTypeId + ", " + licenseNumber
				+ ", " + name + ", " + phoneNumber + ", " + phoneExt + ")");

		List peoples = new ArrayList();

		try {
			String sql = "";
			String strName = "";
			String strPhoneNo = "";
			String strLicenseNo = "";
			String strPhoneExt = "";
			String strPeopleType = "";
			name = "%" + name + "%";

			if (licenseNumber.length() != 0) {
				strLicenseNo = " and UPPER(P.LIC_NO) LIKE '"
						+ StringUtils.replaceStarWithPcnt(licenseNumber
								.toUpperCase()) + "%'";
			}

			if ((peopleTypeId != null) && !peopleTypeId.equals("")) {
				/*
				 * Commented a portion of query as it was giving other info
				 * along with Applicants as Results
				 */
				/*
				 * if (peopleTypeId.equalsIgnoreCase("A")) { peopleTypeId =
				 * peopleTypeId + "' or p.copy_applicant = 'Y"; }
				 */
				strPeopleType = " AND (PT.CODE = '" + peopleTypeId + "') ";
			}

			if (name.length() != 0) {
				strName = " and UPPER(P.NAME) LIKE "
						+ StringUtils.checkString(name.toUpperCase());
			}

			if (phoneNumber.length() != 0) {
				strPhoneNo = " and P.PHONE LIKE '"
						+ StringUtils.replaceStarWithPcnt(phoneNumber) + "'";
			}

			if (phoneExt.length() != 0) {
				strPhoneExt = " and P.EXT LIKE '"
						+ StringUtils.replaceStarWithPcnt(phoneExt) + "'";
			}

			sql = "SELECT DISTINCT P.PEOPLE_ID,P.NAME,P.LAST_NAME,P.EMAIL_ADDR,P.LIC_NO,H.HOLD_LEVEL,DEPOSIT_BALANCE,PHONE,PT.PEOPLE_TYPE_ID,PT.description AS PEOPLE_TYPE,PT.CODE AS PEOPLE_TYPE_CODE FROM PEOPLE P LEFT OUTER JOIN HOLDS H ON H.LEVEL_ID=P.PEOPLE_ID AND H.HOLD_LEVEL='Z' AND H.STAT='A',PEOPLE_TYPE PT  WHERE P.PEOPLE_TYPE_ID=PT.PEOPLE_TYPE_ID "
					+ strPeopleType
					+ strName
					+ strPhoneNo
					+ strLicenseNo
					+ strPhoneExt;
			sql = sql + " ORDER BY P.NAME,P.LIC_NO";
			logger.info(sql);

			People people;

			for (javax.sql.RowSet rs = (new Wrapper()).select(sql); rs.next(); peoples
					.add(people)) {
				people = new People();
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setName(StringUtils.nullReplaceWithEmpty(rs
						.getString("NAME"))
						+ " "
						+ StringUtils.nullReplaceWithEmpty(rs
								.getString("LAST_NAME")));
				people.setPhoneNbr(StringUtils.nullReplaceWithEmpty(StringUtils
						.phoneFormat(rs.getString("PHONE"))));

				if (rs.getString("LIC_NO") != null) {
					people.setLicenseNbr(rs.getString("LIC_NO"));
				} else {
					people.setLicenseNbr("");
				}
				
				if(rs.getString("EMAIL_ADDR") != null) {
					people.setEmailAddress(rs.getString("EMAIL_ADDR"));
				}else {
					people.setEmailAddress("");
				}
				
				
				String activityCountSql = "select count(*) as ACT_COUNT from ACTIVITY_PEOPLE where ACTIVITY_PEOPLE.PEOPLE_ID = " + people.getPeopleId();
				javax.sql.RowSet activityCountRs = (new Wrapper()).select(activityCountSql);
				
				if(activityCountRs != null && activityCountRs.next()) {
					people.setActivityCount(activityCountRs.getInt("ACT_COUNT"));
				}else {
					people.setActivityCount(0);
				}
				
				

				String holdSql = "select STAT from ( select STAT from holds where  hold_level='Z' and level_id="
						+ people.getPeopleId()
						+ " order by update_dt desc) where ROWNUM<=1";
				logger.info("Recent Holds sql :: " + holdSql);

				javax.sql.RowSet holdRs = (new Wrapper()).select(holdSql);

				if (holdRs.next()) {
					if (holdRs.getString("STAT").trim().equalsIgnoreCase("A")) {
						people.setOnHold("Y");
					} else {
						people.setOnHold("N");
					}
				}

				people.setDepositAmount(rs.getFloat("DEPOSIT_BALANCE"));
				people.setPeopleType(new PeopleType(
						rs.getInt("PEOPLE_TYPE_ID"), rs
								.getString("PEOPLE_TYPE"), rs
								.getString("PEOPLE_TYPE_CODE")));
			}

			return peoples;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw e;
		}
	}

	public void assignPeople(String[] selectedPeople, String psaId,
			String psaType) throws Exception {
		logger.info("assignPeople(selectedPeople, " + psaId + ", " + psaType
				+ ")");

		String sql = "";

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			if (!(psaId.equals(""))) {
				for (int i = 0; i < selectedPeople.length; i++) {
					sql = "SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE ACT_ID="
							+ psaId + " AND PEOPLE_ID=" + selectedPeople[i]
							+ " AND PSA_TYPE='" + psaType + "'";
					logger.info(sql);

					javax.sql.RowSet rs = db.select(sql);

					if (rs.next()) {
						logger.debug("record already exists...so skipping");
					} else {
						sql = "INSERT INTO ACTIVITY_PEOPLE(ACT_ID,PEOPLE_ID,PSA_TYPE) VALUES (";
						sql = sql + psaId;
						sql = sql + ",";
						sql = sql + selectedPeople[i];
						sql = sql + ",";
						sql = sql + StringUtils.checkString(psaType);
						sql = sql + ")";
						logger.info(sql);
						db.addBatch(sql);
					}
					updateActivity(StringUtils.s2i(psaId), 0);
					if (rs != null) {
						rs.close();
					}
				}

				db.executeBatch();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public void deletePeople(String[] selectedPeople) throws Exception {
		logger.info("deletePeople(selectedPeople)");

		String sql = "";

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();

			for (int i = 0; i < selectedPeople.length; i++) {
				StringTokenizer st = new StringTokenizer(selectedPeople[i], "-");
				logger.debug("st : "+st.toString());
				sql = "DELETE FROM ACTIVITY_PEOPLE WHERE ACT_ID = ";
				sql = sql + st.nextToken();
				sql = sql + " AND PEOPLE_ID =";
				sql = sql + st.nextToken();
				sql = sql + " AND PSA_TYPE =";
				sql = sql + StringUtils.checkString(st.nextToken());
				sql = sql + " ";
				logger.info(sql);
				db.addBatch(sql);
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	public void updateActivity(int actId,int userId) throws Exception {
		logger.info("updateActivity(" + actId + ")");
		Wrapper db = new Wrapper();

		try {
			String sql ="UPDATE ACTIVITY SET ";
			if(userId!=0 && userId!=-1){
			sql=sql+ "UPDATED_BY="+userId+",";
			}		
			sql=sql+ " UPDATED=CURRENT_TIMESTAMP WHERE ACT_ID="+actId+"";
			db.update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	public People getOwnerFromAssessorAsPeople(String ownerId) throws Exception {
		logger.info("getOwnerFromAssessorAsPeople(" + ownerId + ")");

		People people = new People();

		try {
			javax.sql.RowSet rs = (new Wrapper())
					.select("SELECT * FROM OWNER WHERE OWNER_ID = " + ownerId);

			if (rs.next()) {
				people.setPeopleId(-1);
				people.setName(rs.getString("NAME"));
				people.setPeopleType(new PeopleType(7, "Owner", "W"));

				String strNo = rs.getString("STR_NO");

				if (strNo == null) {
					strNo = "";
				}

				String strMod = rs.getString("STR_MOD");

				if (strMod == null) {
					strMod = "";
				}

				String strDir = rs.getString("PRE_DIR");

				if (strDir == null) {
					strDir = "";
				}

				String strName = rs.getString("STR_NAME");

				if (strName == null) {
					strName = "";
				}

				people.setAddress(strNo + " " + strMod + " " + strDir + " "
						+ strName);
				people.setPhoneNbr(StringUtils.nullReplaceWithEmpty(StringUtils
						.phoneFormat(rs.getString("PHONE"))));
				people.setEmailAddress(rs.getString("EMAIL_ID"));
				people.setCity(rs.getString("CITY"));
				people.setState(rs.getString("STATE"));
				people.setZipCode(rs.getString("ZIP"));
				people.setFaxNbr(StringUtils.phoneFormat(rs.getString("FAX")));
				people.setCopyApplicant("N");
			}

			return people;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	public int checkAssessorExistsAsOwnerInPeople(String name) throws Exception {
		logger.info("checkAssessorExistsAsOwnerInPeople(" + name + ")");

		if (name == null) {
			name = "";
		}

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT PEOPLE_ID FROM PEOPLE WHERE PEOPLE_TYPE_ID = 7 AND UPPER(LTRIM(RTRIM(NAME))) = '"
				+ name.toUpperCase() + "'");

		try {
			javax.sql.RowSet rs = (new Wrapper()).select(sql.toString());

			if (rs.next()) {
				return rs.getInt("PEOPLE_ID");
			} else {
				return -1;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * @param People
	 *            History
	 * @return
	 * @throws Exception
	 */

	public List getContractHistoryList(String peopleId) throws Exception {
		logger.info("getContractHistoryList()");

		List getContractHistoryList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select * from  people_version where people_id="
					+ StringUtils.s2i(peopleId) + "  order by version_no desc";

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				PeopleVersionForm peopleVersionForm = new PeopleVersionForm();
				peopleVersionForm.setVersionNumber(StringUtils.s2i(rs
						.getString("VERSION_NO")));
				peopleVersionForm.setTitle(rs.getString("TITLE"));
				peopleVersionForm.setName(StringUtils.nullReplaceWithEmpty(rs
						.getString("NAME"))
						+ " "
						+ StringUtils.nullReplaceWithEmpty(rs
								.getString("LAST_NAME")));
				peopleVersionForm
						.setBusinessName(rs.getString("BUSINESS_NAME"));
				peopleVersionForm.setBusinessLicenseNumber(rs
						.getString("BUS_LIC_NO"));
				peopleVersionForm.setComments(rs.getString("COMNTS"));
				peopleVersionForm.setFirstName(rs.getString("LAST_NAME"));
				peopleVersionForm.setAddress(rs.getString("ADDR"));
				peopleVersionForm.setCity(rs.getString("CITY"));
				peopleVersionForm.setState(rs.getString("STATE"));
				peopleVersionForm.setZip(rs.getString("ZIP"));
				peopleVersionForm.setUpdated(StringUtils.date2str(rs
						.getDate("UPDATED")));
				peopleVersionForm.setPhone(StringUtils
						.nullReplaceWithEmpty(StringUtils.phoneFormat(rs
								.getString("PHONE"))));

				getContractHistoryList.add(peopleVersionForm);
			}
			rs.close();
			logger.debug("returning getContractHistoryList of size "
					+ getContractHistoryList.size());
			return getContractHistoryList;
		} catch (Exception e) {
			logger.error("Exception occured in getContractHistoryList method . Message-"
					+ e.getMessage());
			throw e;
		}
	}

	/**
	 * @param People
	 *            History
	 * @return
	 * @throws Exception
	 */

	public List getPeopleHistoryList(String peopleId) throws Exception {
		logger.info("getPeopleHistoryList()");

		List getPeopleHistoryList = new ArrayList();
		RowSet rs = null;

		try {
			String sql = "select * from  people where people_id="
					+ StringUtils.s2i(peopleId);

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				PeopleVersionForm peopleVersionForm = new PeopleVersionForm();

				peopleVersionForm.setTitle(rs.getString("TITLE"));
				peopleVersionForm.setName(StringUtils.nullReplaceWithEmpty(rs
						.getString("NAME"))
						+ " "
						+ StringUtils.nullReplaceWithEmpty(rs
								.getString("LAST_NAME")));
				peopleVersionForm
						.setBusinessName(rs.getString("BUSINESS_NAME"));
				peopleVersionForm.setBusinessLicenseNumber(rs
						.getString("BUS_LIC_NO"));
				peopleVersionForm.setComments(rs.getString("COMNTS"));
				peopleVersionForm.setFirstName(rs.getString("LAST_NAME"));
				peopleVersionForm.setAddress(rs.getString("ADDR"));
				peopleVersionForm.setCity(rs.getString("CITY"));
				peopleVersionForm.setState(rs.getString("STATE"));
				peopleVersionForm.setZip(rs.getString("ZIP"));
				peopleVersionForm.setUpdated(StringUtils
						.nullReplaceWithEmpty(StringUtils.date2str(rs
								.getDate("UPDATED"))));
				peopleVersionForm.setPhone(StringUtils
						.nullReplaceWithEmpty(StringUtils.phoneFormat(rs
								.getString("PHONE"))));

				getPeopleHistoryList.add(peopleVersionForm);
			}
			rs.close();
			logger.debug("returning getPeopleHistoryList of size "
					+ getPeopleHistoryList.size());
			return getPeopleHistoryList;
		} catch (Exception e) {
			logger.error("Exception occured in getContractHistoryList method . Message-"
					+ e.getMessage());
			throw e;
		}
	}

	public People getVersionValueList(String peopleId, String version,
			String ptype) throws Exception {
		logger.info("getVersionValueList(" + peopleId + "," + version + ")");
		Wrapper db = new Wrapper();
		RowSet rs = null;
		RowSet rsActivityId = null;
		List versionList = new ArrayList();
		People people = new People();
		String versionSql = "";
		PeopleForm peopleForm = new PeopleForm();
		try {
			int versionInt = StringUtils.s2i(version);

			if (version.equals("I")) {
				versionSql = "SELECT * FROM PEOPLE WHERE PEOPLE_ID = "
						+ peopleId;
			} else {
				versionSql = "SELECT * FROM PEOPLE_VERSION WHERE PEOPLE_ID = "
						+ peopleId + " AND VERSION_NO = " + versionInt;
			}
			logger.debug(versionSql);
			rs = db.select(versionSql);
			while (rs.next()) {
				people.setPType((ptype != null) ? ptype : "");
				people.setPeopleId((StringUtils.s2i(peopleId) != 0) ? StringUtils
						.s2i(peopleId) : 0);
				if (version.equals("I")) {
					people.setCurrentVersionNumber("I");
				} else {
					people.setVersionNumber((rs.getInt("VERSION_NO") != 0) ? rs
							.getInt("VERSION_NO") : 0);
				}
				people.setLicenseNbr((rs.getString("LIC_NO") != null) ? rs
						.getString("LIC_NO") : "");
				people.setLicenseExpires(rs.getDate("LIC_EXP_DT"));
				people.setBusinessLicenseNbr((rs.getString("BUS_LIC_NO") != null) ? rs
						.getString("BUS_LIC_NO") : "");
				people.setBusinessLicenseExpires(rs.getDate("BUS_LIC_EXP_DT"));
				people.setName((rs.getString("NAME") != null) ? rs
						.getString("NAME") : "");
				people.setFirstName((rs.getString("LAST_NAME") != null) ? rs
						.getString("LAST_NAME") : "");
				people.setHicFlag((rs.getString("HIC_FLAG") != null) ? rs
						.getString("HIC_FLAG") : "");
				people.setAddress((rs.getString("ADDR") != null) ? rs
						.getString("ADDR") : "");
				people.setCity((rs.getString("CITY") != null) ? rs
						.getString("CITY") : "");
				people.setState((rs.getString("STATE") != null) ? rs
						.getString("STATE") : "");
				people.setZipCode((rs.getString("ZIP") != null) ? rs
						.getString("ZIP") : "");
				people.setPhoneNbr((rs.getString("PHONE") != null) ? StringUtils
						.phoneFormat(rs.getString("PHONE")) : "");
				people.setExt((rs.getString("EXT") != null) ? rs
						.getString("EXT") : "");
				people.setFaxNbr((rs.getString("FAX") != null) ? StringUtils
						.phoneFormat(rs.getString("FAX")) : "");
				people.setEmailAddress((rs.getString("EMAIL_ADDR") != null) ? rs
						.getString("EMAIL_ADDR") : "");
				people.setCopyApplicant((rs.getString("COPY_APPLICANT") != null) ? rs
						.getString("COPY_APPLICANT") : "");
				people.setAutoLiabilityDate(StringUtils.dbDate2cal(rs
						.getString("AUTO_LIABILITY_DT")));
				people.setGeneralLiabilityDate(rs.getDate("GEN_LIABILITY_DT"));
				people.setWorkersCompExpires(rs.getDate("WORK_COMP_EXP_DT"));
				people.setHoldDate(rs.getDate("HOLD_DT"));
				people.setDlNumber((rs.getString("DL_ID_NO") != null) ? rs
						.getString("DL_ID_NO") : "");
				people.setOnHold((rs.getString("ON_HOLD") != null) ? rs
						.getString("ON_HOLD") : "");
				people.setHoldComments((rs.getString("HOLD_COMNT") != null) ? rs
						.getString("HOLD_COMNT") : "");
				people.setAgentName((rs.getString("AGENT_NAME") != null) ? rs
						.getString("AGENT_NAME") : "");
				people.setAgentPhone((rs.getString("AGENT_PH") != null) ? StringUtils
						.phoneFormat(rs.getString("AGENT_PH")) : "");
				people.setTitle((rs.getString("TITLE") != null) ? rs
						.getString("TITLE") : "");
				people.setLastName((rs.getString("LAST_NAME") != null) ? rs
						.getString("LAST_NAME") : "");
				people.setBusinessName((rs.getString("BUSINESS_NAME") != null) ? rs
						.getString("BUSINESS_NAME") : "");
				people.setCorporateName((rs.getString("CORPORATE_NAME") != null) ? rs
						.getString("CORPORATE_NAME") : "");
				people.setHomeCity((rs.getString("HA_CITY") != null) ? rs
						.getString("HA_CITY") : "");
				people.setHomeState((rs.getString("HA_STATE") != null) ? rs
						.getString("HA_STATE") : "");
				people.setHomeZipCode((rs.getString("HA_ZIP") != null) ? rs
						.getString("HA_ZIP") : "");
				people.setHomeZipCode4((rs.getString("HA_ZIP4") != null) ? rs
						.getString("HA_ZIP4") : "");
				people.setMailingCity((rs.getString("MA_CITY") != null) ? rs
						.getString("MA_CITY") : "");
				people.setMailingState((rs.getString("MA_STATE") != null) ? rs
						.getString("MA_STATE") : "");
				people.setMailingZipCode((rs.getString("MA_ZIP") != null) ? rs
						.getString("MA_ZIP") : "");
				people.setMailingZipCode4((rs.getString("MA_ZIP4") != null) ? rs
						.getString("MA_ZIP4") : "");
				people.setBusinessPhone((rs.getString("B_PHONE") != null) ? StringUtils
						.phoneFormat(rs.getString("B_PHONE")) : "");
				people.setBusinessPhoneExt((rs.getString("B_PHONE_EXT") != null) ? StringUtils
						.phoneFormat(rs.getString("B_PHONE_EXT")) : "");
				people.setSsn((rs.getString("SSN") != null) ? rs
						.getString("SSN") : "");
				people.setDateOfBirth(rs.getDate("DOB"));
				people.setHeight((rs.getString("HEIGHT") != null) ? rs
						.getString("HEIGHT") : "");
				people.setWeight((rs.getString("WEIGHT") != null) ? rs
						.getString("WEIGHT") : "");
				people.setHairColor((rs.getString("HAIR_COLOR") != null) ? rs
						.getString("HAIR_COLOR") : "");
				people.setEyeColor((rs.getString("EYE_COLOR") != null) ? rs
						.getString("EYE_COLOR") : "");
				people.setGender((rs.getString("GENDER") != null) ? rs
						.getString("GENDER") : "");
				people.setHomeAddress((rs.getString("HA_ADDRESS") != null) ? rs
						.getString("HA_ADDRESS") : "");
				people.setMailingAddress((rs.getString("MA_ADDRESS") != null) ? rs
						.getString("MA_ADDRESS") : "");
				people.setHomePhone((rs.getString("HA_PHONE") != null) ? StringUtils
						.phoneFormat(rs.getString("HA_PHONE")) : "");
				people.setDlExpiryDate(rs.getDate("DL_ID_EXP_DT"));
				people.setWorkersCompensationWaive((rs
						.getString("WORKERS_COMP_WAIVE") != null) ? rs
						.getString("WORKERS_COMP_WAIVE") : "");
				people.setComments((rs.getString("COMNTS") != null) ? rs
						.getString("COMNTS") : "");
				logger.debug("Values setted exiting from agent");
			}

			/**
			 * Getting Activity Id From Activity_People table using people Id
			 */

			String activityIdSql = "SELECT * FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID = "
					+ peopleId;
			logger.debug(activityIdSql);
			rsActivityId = db.select(activityIdSql);

			while (rsActivityId.next()) {
				people.setPsaType((rsActivityId.getString("PSA_TYPE") != null) ? rsActivityId
						.getString("PSA_TYPE") : "");
			}

		} catch (Exception e) {
		}

		return people;
	}

	public List getSearchResults(String peopleTypeId, String licenseNumber,
			String name, String email, String phoneNumber, String phoneExt) {
		logger.debug("Entering getSearchResults(" + peopleTypeId + ":"
				+ licenseNumber + ":" + email + ")");

		List peoples = new ArrayList();

		try {
			String sql = "";
			String strName = "";
			String strEmail = "";
			String strPhoneNo = "";
			String strLicenseNo = "";
			String strPhoneExt = "";
			String strPeopleType = "";

			if (licenseNumber.length() != 0) {
				strLicenseNo = " and UPPER(P.LIC_NO) LIKE '"
						+ StringUtils.replaceStarWithPcnt(licenseNumber
								.toUpperCase()) + "%'";
			}

			if ((peopleTypeId != null) && !peopleTypeId.equals("")) {
				// if peopletype = applicant need to get people who has
				// copy_applicant as Y also
				if (peopleTypeId.equalsIgnoreCase("A")) {
					peopleTypeId = peopleTypeId + "' or p.copy_applicant = 'Y";
				}

				strPeopleType = " AND (PT.CODE = '" + peopleTypeId + "') ";
			}

			if (name.length() != 0) {
				strName = " and UPPER(P.NAME) LIKE '"
						+ StringUtils.replaceStarWithPcnt(name.toUpperCase())
						+ "%'";
			}

			if ((email != null) && (email.length() != 0)) {
				strEmail = " and  lower (P.email_addr)  LIKE '"
						+ email.toLowerCase() + "%'";
			}

			if (phoneNumber.length() != 0) {
				strPhoneNo = " and P.PHONE LIKE '"
						+ StringUtils.replaceStarWithPcnt(phoneNumber) + "'";
			}

			if (phoneExt.length() != 0) {
				strPhoneExt = " and P.EXT LIKE '"
						+ StringUtils.replaceStarWithPcnt(phoneExt) + "'";
			}

			sql = "SELECT DISTINCT P.AGENT_NAME, P.PEOPLE_ID,P.NAME,P.LIC_NO,H.HOLD_LEVEL,DEPOSIT_BALANCE,PHONE,PT.PEOPLE_TYPE_ID,PT.DESC AS PEOPLE_TYPE,PT.CODE AS PEOPLE_TYPE_CODE ,p.Email_addr "
					+ " FROM PEOPLE P LEFT OUTER JOIN HOLDS H ON H.LEVEL_ID=P.PEOPLE_ID AND H.HOLD_LEVEL='Z' AND H.STAT='A',PEOPLE_TYPE PT "
					+ " WHERE P.PEOPLE_TYPE_ID=PT.PEOPLE_TYPE_ID "
					+ strPeopleType
					+ strName
					+ strEmail
					+ strPhoneNo
					+ strLicenseNo + strPhoneExt;
			sql += " ORDER BY P.NAME,P.LIC_NO";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				People people = new People();
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setName(rs.getString("NAME"));
				people.setPhoneNbr(StringUtils.phoneFormat(rs
						.getString("PHONE")));

				if (rs.getString("LIC_NO") != null) {
					people.setLicenseNbr(rs.getString("LIC_NO"));
				} else {
					people.setLicenseNbr("");
				}

				if (rs.getString("HOLD_LEVEL") != null) {
					people.setOnHold("Y");
					logger.debug("got hold for person " + people.getPeopleId());
				}

				if (rs.getString("AGENT_NAME") == null) {
					people.setAgentName(rs.getString("AGENT_NAME"));
				}

				people.setAgentName(rs.getString("AGENT_NAME"));

				people.setEmailAddress(rs.getString("email_addr"));

				people.setDepositAmount(rs.getFloat("DEPOSIT_BALANCE"));
				people.setPeopleType(new PeopleType(
						rs.getInt("PEOPLE_TYPE_ID"), rs
								.getString("PEOPLE_TYPE"), rs
								.getString("PEOPLE_TYPE_CODE")));
				peoples.add(people);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return peoples;
	}

	/**
	 * param people object returns peopleId of type int
	 * 
	 * Function checks if any existing people is present of the given type
	 */
	public int checkPeople(People people) {
		logger.info("Enterd into checkPeople...");
		String sql = "select people_id from people where Upper(name)="
				+ StringUtils.checkString(people.getName().toUpperCase())
				+ " and phone="
				+ StringUtils.checkString(StringUtils.replaceString(
						people.getPhoneNbr(), "-", ""))
				+ " and Upper(email_addr)="
				+ StringUtils.checkString(people.getEmailAddress()
						.toUpperCase()) + " and people_type_id = "
				+ people.getPeopleType().getTypeId() + " and lic_no = "
				+ StringUtils.checkString(people.getLicenseNbr());
		int peopleId = 0;
		try {
			RowSet rs = new Wrapper().select(sql);
			logger.debug(sql);
			while (rs.next()) {
				peopleId = rs.getInt("people_id");
			}
		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		logger.info("Exiting checkPeople...");
		return peopleId;
	}

	public List getPeopleList(String levelId, String level) {

		String sql = " ";
		if (level.equals("A")) {
			sql = "SELECT ACTIVITY_PEOPLE.PEOPLE_ID AS PEOPLE_ID,PEOPLE.NAME AS NAME FROM ACTIVITY_PEOPLE, PEOPLE WHERE PEOPLE.PEOPLE_ID = ACTIVITY_PEOPLE.PEOPLE_ID AND ACTIVITY_PEOPLE.ACT_ID ="
					+ levelId;
		} else {
			sql = "SELECT ACTIVITY_PEOPLE.PEOPLE_ID AS PEOPLE_ID,PEOPLE.NAME AS NAME FROM ACTIVITY_PEOPLE, PEOPLE, ACTIVITY WHERE PEOPLE.PEOPLE_ID = ACTIVITY_PEOPLE.PEOPLE_ID AND ACTIVITY_PEOPLE.ACT_ID =ACTIVITY.ACT_ID AND ACTIVITY.SPROJ_ID ="
					+ levelId;
		}

		List peopleList = new ArrayList();

		try {
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				peopleL = new PeopleList(rs.getInt("people_id"),
						StringUtils.properCase(rs.getString("name")));
				peopleList.add(peopleL);

			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());

		}
		logger.debug("exiting peoplelistagent");
		return peopleList;
	}

	public List getPeople(String levelId, String level) {
		String sql = "";
		String depoBalSql = "";
		if (level.equals("A")) {
			sql = "select ap.people_id as people_id,p.name as name, p.deposit_balance as deposit,p.last_name as lastname, pt.DESCRIPTION as people_type from activity_people ap join people p on p.people_id = ap.people_id join people_type pt on pt.PEOPLE_TYPE_ID = p.PEOPLE_TYPE_ID and ap.act_id="
					+ levelId;
			logger.debug("$$ In if sql is $$" + sql);
		} else {
			sql = "SELECT ACTIVITY_PEOPLE.PEOPLE_ID AS PEOPLE_ID,PEOPLE.NAME AS NAME,people.DEPOSIT_BALANCE as deposit,PEOPLE.LAST_NAME AS LASTNAME FROM ACTIVITY_PEOPLE, PEOPLE, ACTIVITY WHERE PEOPLE.PEOPLE_ID = ACTIVITY_PEOPLE.PEOPLE_ID AND ACTIVITY_PEOPLE.ACT_ID =ACTIVITY.ACT_ID AND ACTIVITY.SPROJ_ID ="
					+ levelId;
			logger.debug("@@ In else sql is @@" + sql);
		}
		List peopleList = new ArrayList();
		try {
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				int peopleID = 0;
				String deposit = "($0.00)";
				String name = "";
				String peopleType = "";
				String balanceDeposit = "";

				if (rs.getDouble("deposit") > 0.00)
					deposit = "(" + StringUtils.dbl2$(rs.getDouble("deposit"))
							+ ")";

				name = rs.getString("name")
						+ " "
						+ ((rs.getString("lastname") != null) ? rs
								.getString("lastname") : "") + " " + deposit;
				peopleType = rs.getString("people_type");
				peopleID = rs.getInt("people_id");
				deposit = StringUtils.dbl2str(rs.getDouble("deposit"));

				depoBalSql = "select SUM(PYMNT_AMNT) AS depositBalance from payment p left outer join people pp on p.people_id = pp.people_id where p.pymnt_id in (select distinct pymnt_id from payment_detail where act_id ="
						+ levelId
						+ ") AND p.people_id ="
						+ rs.getString("people_id");
				logger.info("Balance Deposit is  :: " + depoBalSql);

				RowSet depoBalRs = new Wrapper().select(depoBalSql);
				while (depoBalRs.next()) {
					balanceDeposit = StringUtils.dbl2$(depoBalRs
							.getDouble("depositBalance"));
					PeopleList peopleObj = new PeopleList(peopleID, name,
							deposit, peopleType, balanceDeposit);
					peopleList.add(peopleObj);
					// logger.debug("exiting from while(depoBalRs.next())");
				}
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());

		}
		logger.debug("exiting peoplelistagent");
		return peopleList;
	}

	public void updateOnlineUser(People people) {
		logger.info("inside updateOnlineUser...");
		try {

			String sql = "update EXT_USER set ADDRESS = "
					+ StringUtils.checkString(people.getAddress())
					+ ", PHONE ="
					+ StringUtils.checkString(StringUtils.stripPhone(people
							.getPhoneNbr()))
					+ ", PHONE_EXT ="
					+ StringUtils.checkString(people.getExt())
					+ ", CITY ="
					+ StringUtils.checkString(people.getCity())
					+ ", STATE ="
					+ StringUtils.checkString(people.getState())
					+ ", ZIP ="
					+ StringUtils.checkString(people.getZipCode())
					+ ",LICENSE_NO= "
					+ StringUtils.checkString(people.getDlNumber())
					+ ", FAX ="
					+ StringUtils.checkString(StringUtils.stripPhone(people
							.getFaxNbr()));
			sql = sql
					+ " where lower(EXT_USERNAME) = "
					+ StringUtils.checkString(people.getEmailAddress()
							.toLowerCase());
			logger.debug("update query :" + sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error("Error while updating ext_user :" + e.getMessage());
		}
	}

	public int getPeopleId(String emailAddress) throws Exception {
		logger.info("getPeopleId(" + emailAddress + ")");

		String sql = "select people_id from people where Upper(email_addr) = "
				+ StringUtils.checkString(emailAddress.toUpperCase());
		int peopleId = 0;
		try {
			RowSet rs = new Wrapper().select(sql);
			logger.debug(sql);
			while (rs.next()) {
				peopleId = rs.getInt("people_id");
			}
		} catch (Exception e) {
			logger.error("Exception occured " + e.getMessage());
		}
		logger.info("Exiting checkPeople...");
		return peopleId;
	}

	public People getApplicant(String actId) throws AgentException {
		People applicant = new People();
		String sql = "select * from activity_people ap left join people p on ap.people_id = p.people_id where ap.act_id = "
				+ actId
				+ " and p.people_type_id = "
				+ Constants.PEOPLE_APPLICANT;
		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);
			if (rs != null && rs.next()) {
				applicant.setPeopleId(rs.getInt("PEOPLE_ID"));
				applicant.setName(rs.getString("Name"));
				applicant.setAddress(rs.getString("ADDR"));
				applicant.setCity(rs.getString("CITY"));
				applicant.setState(rs.getString("State"));
				applicant.setZipCode(rs.getString("ZIP"));
				applicant.setPhoneNbr("PHONE");
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}
		return applicant;
	}

	public People getPeople(String actId, int typeId) throws AgentException {
		People people = new People();
		String sql = "select * from activity_people ap left join people p on ap.people_id = p.people_id where ap.act_id = "
				+ actId + " and p.people_type_id = " + typeId;
		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);
			if (rs != null && rs.next()) {
				people.setPeopleId(rs.getInt("PEOPLE_ID"));
				people.setName(rs.getString("Name"));
				people.setAddress(rs.getString("ADDR"));
				people.setCity(rs.getString("CITY"));
				people.setState(rs.getString("State"));
				people.setZipCode(rs.getString("ZIP"));
				people.setPhoneNbr(rs.getString("PHONE"));
				people.setEmailAddress(rs.getString("EMAIL_ADDR"));

			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}
		return people;
	}
	
	public List getPeopleLsoIdHistoryList(String id) throws Exception {
		logger.info("getPeopleLsoIdHistoryList()");

		List getPeopleLsoIdHistoryList = new ArrayList();
		RowSet rs = null;
		String sql = null;

		try {
//			sql = "select * from  people_version where people_id="+ StringUtils.s2i(id) +"  order by version_no desc" ;
//			sql ="SELECT * FROM PEOPLE_LSOID PL LEFT JOIN PEOPLE P ON P.PEOPLE_ID = PL.PEOPLE_ID LEFT JOIN LSO_ADDRESS LA ON PL.LSO_ID = LA.LSO_ID LEFT JOIN PEOPLE_VERSION PV ON PV.PEOPLE_ID="+ StringUtils.s2i(id) +" WHERE PL.PEOPLE_ID="+ StringUtils.s2i(id) +" or PL.LSO_ID ="+ StringUtils.s2i(id) +" and PL.MOVE_OUT_DATE < current_timestamp ORDER BY PL.MOVE_OUT_DATE DESC";
			sql = "SELECT * FROM PEOPLE_LSOID PL LEFT OUTER JOIN PEOPLE P ON P.PEOPLE_ID = PL.PEOPLE_ID LEFT JOIN LSO_ADDRESS LA ON PL.LSO_ID = LA.LSO_ID WHERE PL.LSO_ID="+ StringUtils.s2i(id) +" and pl.MOVE_OUT_DATE is not null or pl.MOVE_OUT_DATE < current_date ORDER BY PL.MOVE_OUT_DATE DESC";
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				PeopleForm peopleForm = new PeopleForm();
//				peopleForm.getPeople().setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME"))+ " "+ StringUtils.nullReplaceWithEmpty(rs.getString("LAST_NAME")));
				peopleForm.getPeople().setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
				peopleForm.getPeople().setAddress(rs.getString("ADDR"));
				peopleForm.getPeople().setMoveInDate(StringUtils.date2str(rs.getDate("MOVE_IN_DATE")));
				peopleForm.getPeople().setMoveOutDate(StringUtils.nullReplaceWithEmpty(StringUtils.date2str(rs.getDate("MOVE_OUT_DATE"))));
				
				getPeopleLsoIdHistoryList.add(peopleForm);
			}
			rs.close();
			logger.debug("returning getPeopleLsoIdHistoryList of size "
					+ getPeopleLsoIdHistoryList.size());
			return getPeopleLsoIdHistoryList;
		} catch (Exception e) {
			logger.error("SQL with error"+sql);
			logger.error("Exception occured in getPeopleLsoIdHistoryList method . Message-"+ e.getMessage());
			throw new AgentException(e);
		}
	}
	
	public static boolean checkTenant(int lsoId) throws Exception {
		logger.info("checkTenant" + lsoId );

		boolean checkTenant = false;
		int count = 0;
		String moveOutDate;
		String sql = null;

		try {
			sql = "SELECT  people_id FROM PEOPLE_LSOID WHERE LSO_ID = "+lsoId+" AND MOVE_OUT_DATE > "+"current_timestamp"+" ";
			logger.info(sql);

			javax.sql.RowSet rs = (new Wrapper()).select(sql);

			if (rs.next()) {
//				moveOutDate = StringUtils.date2str(rs.getDate("MOVE_OUT_DATE"));
				count = rs.getInt("people_id");
			}
			rs.close();

			if (count > 0) {
				checkTenant = true;
			}

			return checkTenant;
		} catch (Exception e) {
			logger.error("SQL with error"+sql);
			logger.error(""+e.getMessage());
			throw new AgentException(e);
		}
	}

	public List<People> getAssociatedContractorOrAgent(int peopleId,String peopleType) throws AgentException{
		logger.debug("entered contractorAgentList");
		List<People> contractorAgentList = new ArrayList<People>();
		Wrapper wrapper = new Wrapper();
		String sql = "SELECT CA.CA_ID,P.PEOPLE_ID,P.NAME,P.LIC_NO,CA.START_DATE,EXP_DATE,CA.STATUS FROM CONTRACTOR_AGENT CA JOIN PEOPLE P ON CA.CONTRACTOR_PEOPLE_ID = P.PEOPLE_ID WHERE AGENT_PEOPLE_ID = "+peopleId +" AND STATUS != 'DELETED'";
		
		if(peopleType.equalsIgnoreCase(Constants.CONTRACTOR)){
			sql = "SELECT CA.CA_ID,P.PEOPLE_ID,P.NAME,P.LIC_NO,CA.START_DATE,EXP_DATE,CA.STATUS FROM CONTRACTOR_AGENT CA JOIN PEOPLE P ON CA.AGENT_PEOPLE_ID = P.PEOPLE_ID WHERE CONTRACTOR_PEOPLE_ID = "+peopleId +" AND STATUS != 'DELETED'";
		}
		
		logger.debug(sql);
		javax.sql.RowSet  rs = null;
		try {
			rs = wrapper.select(sql);
			
			if(rs != null) {
				while(rs.next()) {
					People contractor = new People();
					contractor.setPeopleId(rs.getInt("PEOPLE_ID"));
					contractor.setName(rs.getString("NAME"));
					contractor.setLicenseNbr(rs.getString("LIC_NO"));
					contractor.setStatus(rs.getString("STATUS"));
					contractor.setStartDate(rs.getDate("START_DATE"));
					contractor.setExpirationDate(rs.getDate("EXP_DATE"));
					contractor.setCaId(rs.getInt("CA_ID"));
					contractorAgentList.add(contractor);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("fetched contractorAgentList as "+contractorAgentList.size());
		return contractorAgentList;
	}
	
	public List<People> getContractorListForLicNo(String licNbr){
		List<People> contractorList = new ArrayList<People>();
		Wrapper wrapper = new Wrapper();

		String sql = "select * from people where LIC_NO = '"+licNbr+"' and PEOPLE_TYPE_ID = "+Constants.PEOPLE_CONTRACTOR ;
		logger.debug(sql);
		javax.sql.RowSet  rs = null;
		
		try {
			rs = wrapper.select(sql);
			
			if(rs != null) {
				while(rs.next()) {
					People contractor = new People();
					contractor.setPeopleId(rs.getInt("PEOPLE_ID"));
					contractor.setName(rs.getString("NAME"));
					contractor.setLicenseNbr(rs.getString("LIC_NO"));
					contractorList.add(contractor);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return contractorList;
	}
	
	public void addContractorAgent(int agentId, int contractorId) throws Exception {
		logger.info("inside addContractorAgent...");
		String sql = "";
		try {
			Wrapper db = new Wrapper();
			
			sql = "select count(*) as count   from  CONTRACTOR_AGENT  where CONTRACTOR_PEOPLE_ID = "+ contractorId + " AND AGENT_PEOPLE_ID= "+agentId +" AND STATUS !='DELETED'";
			logger.debug("sql" + sql);
			RowSet rs = db.select(sql);
			int count = 0;

			if (rs.next()) {
				count = rs.getInt("count");
			}

			rs.close();
			logger.debug("count "+count);
			
			if( count == 0 && agentId > 0 && contractorId > 0) {
				int caId = db.getNextId("CA_ID");
				sql = "insert into CONTRACTOR_AGENT (CA_ID,CONTRACTOR_PEOPLE_ID,AGENT_PEOPLE_ID,STATUS,APPLIED_DATE) values ("+caId+" ,"+contractorId+ "," + agentId + ",'UNVERIFIED',CURRENT_DATE)";
				logger.debug("sql : " + sql);
				db.insert(sql);
			} 
		
		} catch (Exception e) {
			logger.error("error while inserting into CONTRACTOR_AGENT " + e.getMessage());
		}
	}
	
	public List<People> getContractorAgentList(String status , String filter) throws AgentException{
		logger.debug("entered getContractorAgentList");
		List<People> contractorAgentList = new ArrayList<People>();
		Wrapper wrapper = new Wrapper();
		
		String sql = " WITH all_agents AS ( SELECT NAME,PEOPLE_ID FROM PEOPLE WHERE PEOPLE_TYPE_ID= "+Constants.PEOPLE_AGENT+")"
				+ " , contractor_data as ("
				+ " SELECT CA.CA_ID,P.PEOPLE_ID,P.NAME,P.LIC_NO,CA.START_DATE,EXP_DATE,CA.STATUS,CA.AGENT_PEOPLE_ID"  
				+ " FROM CONTRACTOR_AGENT CA JOIN PEOPLE P ON CA.CONTRACTOR_PEOPLE_ID = P.PEOPLE_ID)"
				+ " select cd.*,aa.name as agent_name from contractor_data cd left join all_agents aa on cd.AGENT_PEOPLE_ID = aa.PEOPLE_ID"
				+ " WHERE cd.STATUS= "+StringUtils.checkString(status);
		
		if(filter != null && filter.length() > 0)
			sql = sql + " AND (UPPER(aa.name) LIKE '%"+filter.toUpperCase()+"%' OR UPPER(cd.NAME) LIKE '%"+filter.toUpperCase()+"%' OR UPPER(cd.LIC_NO) LIKE '%"+filter.toUpperCase()+"%')";
		
		logger.debug(sql);
		javax.sql.RowSet  rs = null;
		try {
			rs = wrapper.select(sql);
			
			if(rs != null) {
				while(rs.next()) {
					People contractor = new People();
					contractor.setPeopleId(rs.getInt("PEOPLE_ID"));
					contractor.setCaId(rs.getInt("CA_ID"));
					contractor.setName(rs.getString("NAME"));
					contractor.setLicenseNbr(rs.getString("LIC_NO"));
					contractor.setStatus(rs.getString("STATUS"));
					contractor.setStartDate(rs.getDate("START_DATE"));
					contractor.setExpirationDate(rs.getDate("EXP_DATE"));
					contractor.setAgentName(rs.getString("agent_name"));
					contractorAgentList.add(contractor);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		logger.debug("fetched getContractorAgentList as "+contractorAgentList.size());
		return contractorAgentList;
	}
	
	public void verfiyContractorAgent(String caId,int userId,String startDate, String expDate) throws Exception {
		logger.info("inside verfiyContractorAgent...");
		try {
			Wrapper db = new Wrapper();
			
			String sql = "UPDATE CONTRACTOR_AGENT SET APPROVED_DATE=CURRENT_DATE, APPROVED_BY ="+userId+", STATUS='VERIFIED' , START_DATE=TO_DATE('"+startDate+"','"+Constants.DATE_FORMAT+"'), EXP_DATE=TO_DATE('"+expDate+"','"+Constants.DATE_FORMAT+"') WHERE CA_ID="+caId;
			logger.debug("sql : " + sql);
			db.insert(sql);
		
		} catch (Exception e) {
			logger.error("error while updating CONTRACTOR_AGENT " + e.getMessage());
		}
	}
	
	public void deleteContractorAgent(String caId) throws Exception {
		logger.info("inside deleteContractorAgent...");
		try {
			Wrapper db = new Wrapper();
			
			String sql = "UPDATE CONTRACTOR_AGENT SET STATUS='DELETED' WHERE CA_ID="+caId;
			logger.debug("sql : " + sql);
			db.insert(sql);
		
		} catch (Exception e) {
			logger.error("error while deleting CONTRACTOR_AGENT " + e.getMessage());
		}
	}
}
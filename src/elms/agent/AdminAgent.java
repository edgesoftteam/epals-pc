package elms.agent;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.sql.RowSet;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import elms.app.admin.ActivitySubType;
import elms.app.admin.ActivityType;
import elms.app.admin.InspectionCode;
import elms.app.admin.NoticeType;
import elms.app.admin.PlanCheckStatus;
import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.admin.Transaction;
import elms.app.admin.lso.MoveStructureDetail;
import elms.app.common.Agent;
import elms.app.common.ConditionLibraryRecord;
import elms.app.common.DisplayItem;
import elms.app.common.Names;
import elms.app.enforcement.CodeEnforcement;
import elms.app.lso.Lso;
import elms.app.lso.LsoAddress;
import elms.app.lso.PsaTree;
import elms.app.lso.Street;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.common.Constants;
import elms.control.beans.MoveVerifyForm;
import elms.control.beans.admin.ActivityStatusAdminForm;
import elms.control.beans.admin.ActivitySubTypeForm;
import elms.control.beans.admin.ActivityTypeForm;
import elms.control.beans.admin.AddressTypeAdminForm;
import elms.control.beans.admin.AttachmentTypeAdmin;
import elms.control.beans.admin.AttachmentTypeForm;
import elms.control.beans.admin.AttachmentTypeMappingForm;
import elms.control.beans.admin.ConditionsLibraryForm;
import elms.control.beans.admin.EmailTemplateTypeAdmin;
import elms.control.beans.admin.InspectionCodeForm;
import elms.control.beans.admin.InspectionItemForm;
import elms.control.beans.admin.MoveLsoForm;
import elms.control.beans.admin.NoticeTemplateAdminForm;
import elms.control.beans.admin.NoticeTypeAdminForm;
import elms.control.beans.admin.PCStatusForm;
import elms.control.beans.admin.SubProjectLookupsForm;
import elms.control.beans.admin.UserForm;
import elms.control.beans.online.EmailAdminForm;
import elms.control.beans.online.EmailTemplateAdminForm;
import elms.control.beans.online.EmailTemplateTypeAdminForm;
import elms.exception.AgentException;
import elms.exception.UserNotFoundException;
import elms.security.Department;
import elms.security.Group;
import elms.security.Role;
import elms.security.User;
import elms.util.Operator;
import elms.util.StringUtils;
import elms.util.jcrypt;
import elms.util.db.Wrapper;

public class AdminAgent extends Agent {
	static Logger logger = Logger.getLogger(AdminAgent.class.getName());
	protected List adminTreeList;
	protected PsaTree psaTree;

	protected ProjectType projectType;

	protected SubProjectType subProjectType;

	protected SubProjectSubType subProjectSubType;

	protected SubProject subProject;

	protected SubProjectDetail subProjectDetail;

	protected Activity activity = null;

	protected ActivityDetail activityDetail = null;

	public AdminAgent() {
	}

	// Start of Sub Project Names related methods - Gai3
	/**
	 * gets the sub project names
	 * 
	 * @return
	 */
	public List getSubProjectNames(SubProjectLookupsForm subProjectLookupsForm) {
		List projectTypeList = new ArrayList();
		String sql = "";

		try {
			Wrapper db = new Wrapper();

			sql = ((subProjectLookupsForm.getDescription() == null) || subProjectLookupsForm.getDescription().equals("")) ? sql : (sql + " UPPER(LPT.DESCRIPTION) LIKE '%" + subProjectLookupsForm.getDescription().toUpperCase() + "%' AND");

			sql = ((subProjectLookupsForm.getDepartment() == null) || subProjectLookupsForm.getDepartment().equals("")) ? sql : (sql + " D.DEPT_ID = " + subProjectLookupsForm.getDepartment() + " AND");

			if (("").equalsIgnoreCase(sql)) {
				sql = "SELECT LPT.PTYPE_ID AS PTYPE_ID, LPT.DESCRIPTION AS DESCRIPTION, COALESCE(D.DESCRIPTION,'') AS DEPT FROM LKUP_PTYPE LPT LEFT OUTER JOIN DEPARTMENT D ON LPT.DEPT_CODE = D.DEPT_CODE WHERE " + sql + " LPT.DESCRIPTION NOT LIKE '" + Constants.SUB_PROJECT_NAME_STARTS_WITH + "%' AND LPT.DESCRIPTION NOT LIKE '" + Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + "%' ORDER BY DEPT";
			} else {
				sql = "SELECT LPT.PTYPE_ID AS PTYPE_ID, LPT.DESCRIPTION AS DESCRIPTION, COALESCE(D.DESCRIPTION,'') AS DEPT FROM LKUP_PTYPE LPT LEFT OUTER JOIN DEPARTMENT D ON LPT.DEPT_CODE = D.DEPT_CODE WHERE " + sql + " LPT.DESCRIPTION NOT LIKE '" + Constants.SUB_PROJECT_NAME_STARTS_WITH + "%' AND LPT.DESCRIPTION NOT LIKE '" + Constants.BT_SUB_PROJECT_NAME_STARTS_WITH + "%' ORDER BY DESCRIPTION";
			}

			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(StringUtils.i2s(rs.getInt("PTYPE_ID")));
				items.setFieldTwo(rs.getString("DESCRIPTION"));
				items.setFieldThree(rs.getString("DEPT"));

				if (subProjectNameRemovable(rs.getInt("PTYPE_ID"))) {
					items.setFieldSix("Y");
				} else {
					items.setFieldSix("N");
				}

				projectTypeList.add(items);
			}

			if (rs != null) {
				rs.close();
			}

		} catch (Exception e) {
			logger.error("The Error is " + e.getMessage());
		}

		logger.debug("The project name Size is " + projectTypeList.size());

		return projectTypeList;
	}

	/**
	 * checks if the activity sub type is removable or not
	 * 
	 * @param type
	 * @return
	 */
	public boolean subProjectNameRemovable(int id) {
		String sql = "SELECT COUNT(*) AS COUNT FROM SUB_PROJECT WHERE SPROJ_TYPE=" + id;
		boolean removable = false;

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("COUNT") == 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("Sql error is subProjectNameRemovable() " + e.getMessage());
		} catch (Exception e) {
			logger.error("General error is subProjectNameRemovable() " + e.getMessage());
		}

		return removable;
	}

	/**
	 * gets the activity type for a activity type id.
	 * 
	 * @param actSubTypeId
	 * @return
	 */
	public ProjectType getSubProjectName(int subProjNameId) {
		logger.info("getSubProjectName(" + subProjNameId + ")");

		ProjectType projectType = new ProjectType();

		try {
			String sql = "SELECT LPT.*, D.* FROM LKUP_PTYPE LPT LEFT OUTER JOIN DEPARTMENT D ON LPT.DEPT_CODE = D.DEPT_CODE WHERE PTYPE_ID =" + subProjNameId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				projectType.setProjectTypeId(rs.getInt("PTYPE_ID"));
				projectType.setDepartment(rs.getString("DEPT_ID"));
				projectType.setDescription(rs.getString("DESCRIPTION"));
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Error in get Sub Project Name " + e.getMessage());
		}

		return projectType;
	}

	/**
	 * @param projectType
	 * @param departmentCode
	 * @param description
	 * @return result *
	 * @throws Exception
	 */
	public int saveSubProjectName(String description, String departmentCode, int subProjectTypeId) {
		int result = 0;
		String sql = "";
		int ptypeId = 0;
		Wrapper db = new Wrapper();

		try {
			String deptSql = "SELECT DEPT_CODE FROM DEPARTMENT WHERE DEPT_ID = " + departmentCode;
			logger.info(deptSql);
			RowSet deptRs = db.select(deptSql);

			if (deptRs.next()) {
				departmentCode = deptRs.getString("DEPT_CODE");
			}

			if (subProjectTypeId == 0) {
				ptypeId = db.getNextId("PTYPE_ID");
				sql = "INSERT INTO LKUP_PTYPE(PTYPE_ID,DESCRIPTION,DEPT_CODE,NAME) VALUES(" + ptypeId + "," + StringUtils.checkString(description) + "," + StringUtils.checkString(departmentCode) + ", '' " + ")";
				logger.info(sql);
				db.insert(sql);
				result = 1;
			} else {
				sql = "UPDATE LKUP_PTYPE SET DESCRIPTION=" + StringUtils.checkString(description) + " WHERE PTYPE_ID=" + subProjectTypeId;
				logger.info(sql);
				db.update(sql);
				result = 2;
			}

			if (deptRs != null) {
				deptRs.close();
			}
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveSubProjectName() :: " + e.getMessage());
		}
		return result;
	}

	/**
	 * deletes the sub project name
	 * 
	 * @param id
	 * @return
	 */
	public int deleteSubProjectName(String id) {
		logger.debug("Entered into deleteSubProjectName...");
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = id.split(",");
		logger.debug("id List " + idArray.length);
		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_PTYPE WHERE PTYPE_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			// Beginning of try block
			try {
				new Wrapper().update(sql);
			} catch (SQLException e) {
				result = -1;
				logger.error("Sql error is deleteSubProjectName() " + e.getMessage());
			} catch (Exception e) {
				result = -1;
				logger.error("General error is deleteSubProjectName() " + e.getMessage());
			}
		}
		return result;
	}

	// End of Sub Project Names related methods

	// Start of Sub Project Type related methods - Gai3

	/**
	 * gets the sub project type for a sub project type id.
	 * 
	 * @param subProjectTypeId
	 * @return
	 */
	public SubProjectType getSubProjectType(int subProjectTypeId) {
		SubProjectType subProjectType = new SubProjectType();

		try {
			String sql = "SELECT LST.*,D.DEPT_ID DEPTID FROM LKUP_SPROJ_TYPE LST LEFT OUTER JOIN DEPARTMENT D ON D.DEPT_CODE=LST.DEPT_CODE WHERE SPROJ_TYPE_ID=" + subProjectTypeId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				subProjectType.setSubProjectTypeId(rs.getInt("SPROJ_TYPE_ID"));
				subProjectType.setPtypeId(rs.getInt("PTYPE_ID"));
				subProjectType.setDepartmentId(rs.getString("DEPTID"));
				subProjectType.setSubProjectType(rs.getString("SPROJ_TYPE").trim());
				subProjectType.setDescription(rs.getString("DESCRIPTION"));
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return subProjectType;
	}

	/**
	 * gets the list of sub project types
	 * 
	 * @return
	 */
	public List getSubProjectTypes(SubProjectLookupsForm subProjectLookupsForm) {

		List subProjectSubTypes = new ArrayList();
		String sql = "";

		try {
			Wrapper db = new Wrapper();

			sql = ((subProjectLookupsForm.getType() == null) || subProjectLookupsForm.getType().equals("")) ? sql : (sql + " UPPER(LST.SPROJ_TYPE) like '%" + subProjectLookupsForm.getType().toUpperCase() + "%' AND");

			sql = ((subProjectLookupsForm.getDescription() == null) || subProjectLookupsForm.getDescription().equals("")) ? sql : (sql + " LST.DESCRIPTION like '%" + subProjectLookupsForm.getDescription() + "%' AND");

			sql = ((subProjectLookupsForm.getSubProjectName() == null) || subProjectLookupsForm.getSubProjectName().equals("")) ? sql : (sql + " LST.PTYPE_ID = " + subProjectLookupsForm.getSubProjectName() + " AND");

			sql = ((subProjectLookupsForm.getDepartment() == null) || subProjectLookupsForm.getDepartment().equals("")) ? sql : (sql + " D.DEPT_ID = " + subProjectLookupsForm.getDepartment() + " AND");

			if ((sql != null) && !("").equalsIgnoreCase(sql)) {
				sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
				sql = " WHERE " + sql;
			}

			sql = "SELECT LST.*,D.DESCRIPTION DEPTDESC, LPT.DESCRIPTION AS SPROJDESC FROM LKUP_SPROJ_TYPE LST LEFT OUTER JOIN DEPARTMENT D ON D.DEPT_CODE=LST.DEPT_CODE LEFT OUTER JOIN LKUP_PTYPE LPT ON LPT.PTYPE_ID = LST.PTYPE_ID " + sql + " ORDER BY LST.PTYPE_ID, LST.DESCRIPTION";
			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(StringUtils.i2s(rs.getInt("SPROJ_TYPE_ID")));
				items.setFieldTwo(rs.getString("SPROJ_TYPE"));
				items.setFieldThree(rs.getString("DESCRIPTION"));
				items.setFieldFour(rs.getString("SPROJDESC"));
				items.setFieldFive(rs.getString("DEPTDESC"));

				String delSql = "SELECT COUNT(*) AS COUNT FROM SUB_PROJECT WHERE STYPE_ID=" + rs.getInt("SPROJ_TYPE_ID");
				logger.info(delSql);

				RowSet delRs = db.select(delSql);

				if (delRs.next()) {
					int count = delRs.getInt("COUNT");
					if (count == 0) {
						items.setFieldSix("Y");
					} else {
						items.setFieldSix("N");
					}
				}
				if (delRs != null) {
					delRs.close();
				}

				subProjectSubTypes.add(items);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("The Error in getSubProjectTypes() " + e.getMessage());
		}

		logger.debug("The project type Size is " + subProjectSubTypes.size());

		return subProjectSubTypes;
	}

	/**
	 * saves the sub project type
	 * 
	 * @param subProjectType
	 * @return
	 */
	public int saveSubProjectType(String type, String description, int pTypeId, String departmentCode, int subProjectTypeId) {
		int result = 0;
		String sql = "";
		int sprojTypeId = 0;
		Wrapper db = new Wrapper();

		try {
			String deptSql = "SELECT DEPT_CODE FROM DEPARTMENT WHERE DEPT_ID = " + departmentCode;
			logger.info(deptSql);
			RowSet deptRs = db.select(deptSql);

			if (deptRs.next()) {
				departmentCode = deptRs.getString("DEPT_CODE");
			}

			if (subProjectTypeId == 0) {
				sprojTypeId = db.getNextId("SPROJ_TYPE_ID");
				sql = "INSERT INTO LKUP_SPROJ_TYPE(SPROJ_TYPE_ID,PTYPE_ID,DESCRIPTION,DEPT_CODE,SPROJ_TYPE) VALUES(" + sprojTypeId + "," + pTypeId + "," + StringUtils.checkString(description) + "," + StringUtils.checkString(departmentCode) + "," + StringUtils.checkString(type) + ")";
				logger.info(sql);
				db.insert(sql);
				result = 1;
			} else {
				sql = "UPDATE LKUP_SPROJ_TYPE SET PTYPE_ID=" + pTypeId + "," + "DESCRIPTION=" + StringUtils.checkString(description) + "," + "DEPT_CODE=" + StringUtils.checkString(departmentCode.trim()) + "," + "SPROJ_TYPE=" + StringUtils.checkString(type.trim()) + " WHERE SPROJ_TYPE_ID=" + subProjectTypeId;
				logger.info(sql);
				db.update(sql);
				result = 2;
			}

			if (deptRs != null) {
				deptRs.close();
			}
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveSubProjectName() method" + e.getMessage());
		}
		return result;
	}

	/**
	 * deletes the sub project type for a sub project id.
	 * 
	 * @param id
	 * @return
	 */
	public int deleteSubProjectType(String id) {
		logger.debug("Entered into deleteSubProjectType...");
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = id.split(",");
		logger.debug("id List " + idArray.length);
		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_SPROJ_TYPE WHERE SPROJ_TYPE_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			// Beginning of try block
			try {
				new Wrapper().update(sql);
			} catch (SQLException e) {
				result = -1;
				logger.error("Sql error is deleteSubProjectName() " + e.getMessage());
			} catch (Exception e) {
				result = -1;
				logger.error("General error is deleteSubProjectName() " + e.getMessage());
			}
		}
		return result;
	}

	// End of Sub Project Type related methods

	// Start of Sub Project Sub Type related methods - Gai3
	/**
	 * gets the sub project sub type list
	 * 
	 * @return
	 */
	public List getSubProjectSubTypes(SubProjectLookupsForm subProjectLookupsForm) {
		List subProjectSubTypes = new ArrayList();
		String sql = "";

		try {
			Wrapper db = new Wrapper();

			sql = ((subProjectLookupsForm.getType() == null) || subProjectLookupsForm.getType().equals("")) ? sql : (sql + " UPPER(TYPE) LIKE '%" + subProjectLookupsForm.getType().toUpperCase() + "%' AND");

			sql = ((subProjectLookupsForm.getDescription() == null) || subProjectLookupsForm.getDescription().equals("")) ? sql : (sql + " DESCRIPTION LIKE '%" + subProjectLookupsForm.getDescription() + "%' AND");

			if ((sql != null) && !("").equalsIgnoreCase(sql)) {
				sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
				sql = " WHERE " + sql;
			}

			sql = "SELECT * FROM LKUP_SPROJ_STYPE " + sql + " ORDER BY DESCRIPTION";
			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(StringUtils.i2s(rs.getInt("SPROJ_STYPE_ID")));
				items.setFieldTwo(rs.getString("TYPE"));
				items.setFieldThree(rs.getString("DESCRIPTION"));

				String delSql = "SELECT COUNT(*) AS COUNT FROM SUB_PROJECT WHERE SSTYPE_ID=" + rs.getInt("SPROJ_STYPE_ID");
				logger.info(delSql);

				RowSet delRs = db.select(delSql);

				if (delRs.next()) {
					int count = delRs.getInt("COUNT");
					if (count == 0) {
						items.setFieldSix("Y");
					} else {
						items.setFieldSix("N");
					}
				}
				if (delRs != null) {
					delRs.close();
				}

				subProjectSubTypes.add(items);
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("The Error is " + e.getMessage());
		}

		logger.debug("The project type Size is " + subProjectSubTypes.size());

		return subProjectSubTypes;
	}

	/**
	 * gets the Sub Project Sub Type for id.
	 * 
	 * @param subProjSubTypeId
	 * @return
	 */
	public SubProjectSubType getSubProjectSubType(int subProjSubTypeId) {
		logger.info("getSubProjectSubType(" + subProjSubTypeId + ")");

		SubProjectSubType subProjectSubType = new SubProjectSubType();

		try {
			String sql = "SELECT * FROM LKUP_SPROJ_STYPE WHERE SPROJ_STYPE_ID =" + subProjSubTypeId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("SPROJ_STYPE_ID"));
				subProjectSubType.setType(rs.getString("TYPE"));
				subProjectSubType.setDescription(rs.getString("DESCRIPTION"));
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Error in getSubProjectSubType() " + e.getMessage());
		}
		return subProjectSubType;
	}

	/**
	 * saves the sub project sub type
	 * 
	 * @param subProjectSubType
	 * @return
	 */
	public int saveSubProjectSubType(String type, String description, int subProjectSubTypeId, String activityType ,String subProjectType) {
		int result = 0;
		String sql = "";
		int sPrSTyId = 0;
		Wrapper db = new Wrapper();

		try {

			if (subProjectSubTypeId == 0) {
				sPrSTyId = db.getNextId("SPROJ_STYPE_ID");
				sql = "INSERT INTO LKUP_SPROJ_STYPE(SPROJ_STYPE_ID,TYPE,DESCRIPTION) VALUES(" + sPrSTyId + "," + StringUtils.checkString(type) + "," + StringUtils.checkString(description) + ")";
				logger.info(sql);
				db.insert(sql);
				result = 1;
			} else {
				sql = "UPDATE LKUP_SPROJ_STYPE SET TYPE=" + StringUtils.checkString(type.trim()) + "," + "DESCRIPTION=" + StringUtils.checkString(description) + " WHERE SPROJ_STYPE_ID=" + subProjectSubTypeId;
				logger.info(sql);
				db.update(sql);
				result = 2;
			}
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveSubProjectName() method" + e.getMessage());
		}
		return result;
	}

	/**
	 * deletes the sub project sub type
	 * 
	 * @param id
	 * @return
	 */
	public int deleteSubProjectSubType(String id) {
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = id.split(",");
		logger.debug("id List " + idArray.length);
		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_SPROJ_STYPE WHERE SPROJ_STYPE_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			// Beginning of try block
			try {
				new Wrapper().update(sql);
			} catch (SQLException e) {
				result = -1;
				logger.error("Sql error is deleteSubProjectSubType() " + e.getMessage());
			} catch (Exception e) {
				result = -1;
				logger.error("General error is deleteSubProjectSubType() " + e.getMessage());
			}
		}
		return result;
	}

	// End of Sub Project Sub Type related methods

	// Start of Activity Type related methods - Gai3

	/**
	 * gets the activity type for a activity type id.
	 * 
	 * @param actTypeId
	 * @return
	 */
	public ActivityType getActivityType(int actTypeId) {
		logger.info("getActivityType(" + actTypeId + ")");

		ActivityType activityType = new ActivityType();

		try {
			String sql = "SELECT * FROM LKUP_ACT_TYPE WHERE TYPE_ID=" + actTypeId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activityType.setTypeId(rs.getString("TYPE_ID"));
				activityType.setType(rs.getString("TYPE"));
				activityType.setDescription(rs.getString("DESCRIPTION"));
				activityType.setDepartmentId(rs.getInt("DEPT_ID"));
				activityType.setSubProjectType(rs.getInt("PTYPE_ID"));
				activityType.setModuleId(rs.getInt("MODULE_ID"));
				activityType.setMuncipalCode(StringUtils.nullReplaceWithEmpty(rs.getString("MUNI_CODE")));
				activityType.setSicCode(StringUtils.nullReplaceWithEmpty(rs.getString("SIC_CODE")));
				activityType.setClassCode(StringUtils.nullReplaceWithEmpty(rs.getString("CLASS_CODE")));
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Error in get activity type " + e.getMessage());
		}

		return activityType;
	}

	/**
	 * gets the activity types from the form
	 * 
	 * @param actTypeForm
	 * @return
	 */
	public List getActivityTypes(ActivityTypeForm actTypeForm) {
		List actTypes = new ArrayList();

		try {
			String sql = "";
			sql = ((actTypeForm.getActivityType() == null) || actTypeForm.getActivityType().equals("")) ? sql : (sql + " upper(lat.type) like '%" + actTypeForm.getActivityType().toUpperCase() + "%' AND");

			sql = ((actTypeForm.getModuleId() == null) || ("").equalsIgnoreCase(actTypeForm.getModuleId()) || actTypeForm.getModuleId().equals("-1")) ? sql : (sql + " MODULE_ID = " + actTypeForm.getModuleId() + " AND");

			sql = ((actTypeForm.getDescription() == null) || actTypeForm.getDescription().equals("")) ? sql : (sql + " UPPER(LAT.DESCRIPTION) LIKE '%" + actTypeForm.getDescription().toUpperCase() + "%' AND");

			sql = ((actTypeForm.getSubProjectType() == null) || actTypeForm.getSubProjectType().equals("-1") || actTypeForm.getSubProjectType().equals("")) ? sql : (sql + " LAT.PTYPE_ID=" + actTypeForm.getSubProjectType() + " AND");

			sql = ((actTypeForm.getDepartment() == null) || actTypeForm.getDepartment().equals("-1") || actTypeForm.getDepartment().equals("")) ? sql : (sql + " LAT.DEPT_ID=" + actTypeForm.getDepartment() + " AND");

			sql = ((actTypeForm.getMuncipalCode() == null) || actTypeForm.getMuncipalCode().equals("")) ? sql : (sql + " UPPER(LAT.MUNI_CODE) LIKE '%" + actTypeForm.getMuncipalCode().toUpperCase() + "%' AND");

			sql = ((actTypeForm.getSicCode() == null) || actTypeForm.getSicCode().equals("")) ? sql : (sql + " UPPER(LAT.SIC_CODE) LIKE '%" + actTypeForm.getSicCode().toUpperCase() + "%' AND");

			sql = ((actTypeForm.getClassCode() == null) || actTypeForm.getClassCode().equals("")) ? sql : (sql + " UPPER(LAT.CLASS_CODE) LIKE '%" + actTypeForm.getClassCode().toUpperCase() + "%' AND");

			if ((sql != null) && !("").equalsIgnoreCase(sql)) {
				sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
				sql = " WHERE " + sql;
			}

			sql = "SELECT LAT.MODULE_ID,LAT.TYPE_ID,LAT.TYPE,COALESCE(LAT.DESCRIPTION,'') AS ACTTYPE ,COALESCE(LPT.DESCRIPTION,'') AS PDESC,COALESCE(D.DESCRIPTION,'') AS DEPT FROM LKUP_ACT_TYPE LAT LEFT OUTER JOIN LKUP_PTYPE LPT ON LAT.PTYPE_ID=LPT.PTYPE_ID LEFT OUTER JOIN DEPARTMENT D ON LAT.DEPT_ID = D.DEPT_ID" + sql + " ORDER BY TYPE";

			logger.info("getActivityTypes() Sql is " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(rs.getString("TYPE_ID"));
				items.setFieldTwo(rs.getString("TYPE"));

				if (activityTypeRemovable(rs.getString("TYPE"))) {
					items.setFieldSix("Y");
				} else {
					items.setFieldSix("N");
				}

				items.setFieldThree(rs.getString("ACTTYPE"));
				items.setFieldFour(rs.getString("PDESC"));
				items.setFieldFive(rs.getString("DEPT"));
				actTypes.add(items);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return actTypes;
	}

	/**
	 * saves the activity type
	 * 
	 * @param actType
	 * @param description
	 * @param deptId
	 * @param ptypeId
	 * @return
	 */
	public int saveActivityType(String actType, String description, String deptId, String ptypeId, int moduleId, String muncipalCode, String sicCode, String classCode) {
		logger.info("saveActivityType(" + actType + ", " + description + ", " + deptId + ", " + ptypeId + "," + moduleId + ", " + muncipalCode + ", " + sicCode + ", " + classCode + ")");

		Wrapper db = new Wrapper();
		int result = 0;

		try {
			String sql = "SELECT * FROM LKUP_ACT_TYPE WHERE UPPER(TYPE)=" + StringUtils.checkString(actType.toUpperCase());
			RowSet rs = db.select(sql);

			if (rs.next()) {
				sql = "UPDATE LKUP_ACT_TYPE SET MODULE_ID=" + moduleId + ",DESCRIPTION=" + StringUtils.checkString(description) + "," + " DEPT_ID=" + deptId + "," + " PTYPE_ID=" + ptypeId + "," + " MUNI_CODE = " + StringUtils.checkString(muncipalCode) + "," + " SIC_CODE = " + StringUtils.checkString(sicCode) + "," + " CLASS_CODE = " + StringUtils.checkString(classCode) + " WHERE UPPER(TYPE)=" + StringUtils.checkString(actType.toUpperCase());
				result = 2;
			} else {
				int actTypeId = db.getNextId("ACTIVITY_TYPE");
				sql = "INSERT INTO LKUP_ACT_TYPE( TYPE_ID, TYPE, DESCRIPTION, DEPT_ID, PTYPE_ID, MODULE_ID, MUNI_CODE, SIC_CODE, CLASS_CODE) VALUES(" + actTypeId + "," + StringUtils.checkString(actType.toUpperCase()) + "," + StringUtils.checkString(description) + "," + deptId + "," + ptypeId + "," + moduleId + "," + StringUtils.checkString(muncipalCode) + "," + StringUtils.checkString(sicCode) + "," + StringUtils.checkString(classCode) + ")";
				result = 1;
			}

			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveActivityType() method" + e.getMessage());
		}

		return result;
	}

	/**
	 * checks if the activity type is removable or not
	 * 
	 * @param type
	 * @return
	 */
	public boolean activityTypeRemovable(String type) {
		String sql = "SELECT COUNT(*) AS COUNT FROM ACTIVITY WHERE ACT_TYPE='" + type + "'";
		boolean removable = false;

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("COUNT") == 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("Sql error is activityTypeRemovable() " + e.getMessage());
		} catch (Exception e) {
			logger.error("General error is activityTypeRemovable() " + e.getMessage());
		}

		return removable;
	}

	/**
	 * deletes the activity type
	 * 
	 * @param typeId
	 * @return
	 */
	public int deleteActivityType(String typeId) {
		logger.debug("Entered into deleteActivityTypes.");
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = typeId.split(",");
		logger.debug("id List " + idArray.length);

		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_ACT_TYPE WHERE TYPE_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			// Beginning of try block
			try {
				new Wrapper().update(sql);
			} catch (SQLException e) {
				result = -1;
				logger.error("Sql error is activityTypeRemovable() " + e.getMessage());
			} catch (Exception e) {
				result = -1;
				logger.error("General error is activityTypeRemovable() " + e.getMessage());
			}
		}
		return result;
	}

	// End of Activity Type related methods - Gai3

	// Start of Activity SubType related methods - Gai3
	
	
	
	
	
	/**
	 * gets the activity sub types from the form
	 * 
	 * @param actSubTypeForm
	 * @return
	 */
	public List getNoticeTypes(NoticeTypeAdminForm noticeTypeAdminForm) {
		List noticeTypes = new ArrayList();

		try {
			String sql = "";

			sql = ((noticeTypeAdminForm.getDescription() == null) ) ? sql : (sql + " UPPER(DESCRIPTION) LIKE '%" + noticeTypeAdminForm.getDescription().toUpperCase() + "%'");

			sql = "SELECT * FROM LKUP_NOTICE_TYPE WHERE " + sql + " ORDER BY DESCRIPTION ";

			logger.debug("getNoticeTypes() Sql is " + sql);

			RowSet rs = new Wrapper().select(sql);
  
			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(StringUtils.i2s(rs.getInt("NOTICE_TYPE_ID")));
				items.setFieldTwo(rs.getString("DESCRIPTION"));

				if (noticeTypeRemovable(rs.getInt("NOTICE_TYPE_ID"))) {
					items.setFieldSix("Y");
					logger.debug("Yes  ");
				} else {
					items.setFieldSix("N");
					logger.debug("No ");
				}

				
				noticeTypes.add(items);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return noticeTypes;
	}


	/**
	 * gets the activity sub types from the form
	 * 
	 * @param actSubTypeForm
	 * @return
	 */
	public List getActivitySubTypes(ActivitySubTypeForm activitySubTypeForm) {
		List actSubTypes = new ArrayList();

		try {
			String sql = "";
			sql = ((activitySubTypeForm.getActivityType() == null) || activitySubTypeForm.getActivityType().equals("")) ? sql : (sql + " UPPER(LAT.TYPE) = '" + activitySubTypeForm.getActivityType().toUpperCase() + "' AND");

			sql = ((activitySubTypeForm.getActivitySubType() == null) || activitySubTypeForm.getActivitySubType().equals("")) ? sql : (sql + " UPPER(LAT.DESCRIPTION) LIKE '%" + activitySubTypeForm.getActivitySubType().toUpperCase() + "%' AND");

			if ((sql != null) && !("").equalsIgnoreCase(sql)) {
				sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
				sql = " WHERE " + sql;
			}

			sql = "SELECT LST.ACT_SUBTYPE_ID,LST.ACT_TYPE,LST.ACT_SUBTYPE,COALESCE(LAT.DESCRIPTION,'') AS ACTTYPE FROM LKUP_ACT_SUBTYPE LST LEFT OUTER JOIN LKUP_ACT_TYPE LAT ON LST.ACT_TYPE = LAT.TYPE " + sql + " ORDER BY ACT_SUBTYPE";

			logger.debug("getActivityTypes() Sql is " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(StringUtils.i2s(rs.getInt("ACT_SUBTYPE_ID")));
				items.setFieldTwo(rs.getString("ACT_TYPE"));

				if (activitySubTypeRemovable(rs.getInt("ACT_SUBTYPE_ID"))) {
					items.setFieldSix("Y");
				} else {
					items.setFieldSix("N");
				}

				items.setFieldThree(rs.getString("ACT_SUBTYPE"));
				actSubTypes.add(items);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return actSubTypes;
	}

	/**
	 * checks if the activity sub type is removable or not
	 * 
	 * @param type
	 * @return
	 */
	public boolean activitySubTypeRemovable(int type) {
		String sql = "SELECT COUNT(*) AS COUNT FROM ACT_SUBTYPE WHERE ACT_SUBTYPE_ID = " + type + "";
		boolean removable = false;

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("COUNT") == 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("Sql error is activitySubTypeRemovable() " + e.getMessage());
		} catch (Exception e) {
			logger.error("General error is activitySubTypeRemovable() " + e.getMessage());
		}

		return removable;
	}
	
	
	
	/**
	 * checks if the Notice type is removable or not
	 * 
	 * @param type
	 * @return
	 */
	public boolean noticeTypeRemovable(int type) {
		String sql = "SELECT COUNT(*) AS COUNT FROM LKUP_NOTICE_TYPE WHERE NOTICE_TYPE_ID = " + type + "";
		boolean removable = false;

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("COUNT") > 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("Sql error is noticeTypeRemovable() " + e.getMessage());
		} catch (Exception e) {
			logger.error("General error is noticeTypeRemovable() " + e.getMessage());
		}

		return removable;
	}

	

	/**
	 * gets the notice type for a activity.
	 * 
	 * @param noticeTypeId
	 * @return
	 */
	public NoticeType getNoticeType(int noticeTypeId) {
		logger.info("getNoticeType(" + noticeTypeId + ")");

		NoticeType noticeType = new NoticeType();

		try {
			String sql = "SELECT * FROM LKUP_NOTICE_TYPE WHERE NOTICE_TYPE_ID=" + noticeTypeId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				noticeType.setId(rs.getInt("NOTICE_TYPE_ID"));
				noticeType.setDescription(rs.getString("DESCRIPTION"));				
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Error in get activity Sub type " + e.getMessage());
		}

		return noticeType;
	}

	
	
	/**
	 * gets the activity sub type for a activity type id.
	 * 
	 * @param actSubTypeId
	 * @return
	 */
	public ActivitySubType getActivitySubType(int actSubTypeId) {
		logger.info("getActivitySubType(" + actSubTypeId + ")");

		ActivitySubType activitySubType = new ActivitySubType();

		try {
			String sql = "SELECT * FROM LKUP_ACT_SUBTYPE WHERE ACT_SUBTYPE_ID=" + actSubTypeId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activitySubType.setId(rs.getInt("ACT_SUBTYPE_ID"));
				activitySubType.setActType(rs.getString("ACT_TYPE"));
				activitySubType.setDescription(rs.getString("ACT_SUBTYPE"));
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Error in get activity Sub type " + e.getMessage());
		}

		return activitySubType;
	}

	
	
	
	/**
	 * saves the notice type
	 * @param noticeType
	 * @returnresult
	 * @author Manjuprasad
	 */
	public int saveNoticeType(String noticeType, String noticeTypeId) {
		logger.info("saveNoticeType(" + noticeType + ", " + noticeTypeId + ")");

		Wrapper db = new Wrapper();
		int result = 0;

		try {
			String sql = "SELECT * FROM LKUP_NOTICE_TYPE WHERE NOTICE_TYPE_ID= " + noticeTypeId;
			logger.info(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				String checkSql = "SELECT * FROM LKUP_NOTICE_TYPE WHERE UPPER(DESCRIPTION)=" + StringUtils.checkString(noticeType.toUpperCase()) ;
				RowSet checkRs = db.select(checkSql);
				if (checkRs.next()) {
					result = 3;
				} else {
					sql = "UPDATE LKUP_NOTICE_TYPE SET DESCRIPTION=" + StringUtils.checkString(noticeType)  + " WHERE  NOTICE_TYPE_ID =" + noticeTypeId;
					db.update(sql);
					result = 2;
				}
			} else {
				int nTypeId = db.getNextId("NOTICE_TYPE_ID");
				
					sql = "INSERT INTO LKUP_NOTICE_TYPE(NOTICE_TYPE_ID, CATEGORY, DESCRIPTION, ACTIVE, DEPARTMENT) VALUES(" + nTypeId + ", 'A' " + "," + StringUtils.checkString(noticeType) + ", 'Y' , " +Constants.DEPARTMENT_HOUSING+" )";
					logger.debug(sql);
					db.insert(sql);
     
					result = 1;
				
			}
			
			if(rs!=null) rs.close();
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveNoticeType() method" + e.getMessage());
		}

		return result;
	}

	
	
	/**
	 * saves the activity sub type
	 * 
	 * @param actType
	 * @param actSubType
	 * @returnresult
	 * @author Gayathri
	 */
	public int saveActivitySubType(String actType, String actSubType, String subTypeId) {
		logger.info("saveActivityType(" + actType + ", " + actSubType + ")");

		Wrapper db = new Wrapper();
		int result = 0;

		try {
			String sql = "SELECT * FROM LKUP_ACT_SUBTYPE WHERE ACT_SUBTYPE_ID= " + subTypeId;
			logger.info(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				String checkSql = "SELECT * FROM LKUP_ACT_SUBTYPE WHERE UPPER(ACT_TYPE)=" + StringUtils.checkString(actType.toUpperCase()) + " AND UPPER(ACT_SUBTYPE)=" + StringUtils.checkString(actSubType.toUpperCase());
				RowSet checkRs = db.select(checkSql);
				if (checkRs.next()) {
					result = 3;
				} else {
					sql = "UPDATE LKUP_ACT_SUBTYPE SET ACT_TYPE=" + StringUtils.checkString(actType) + "," + " ACT_SUBTYPE = " + StringUtils.checkString(actSubType) + "WHERE  ACT_SUBTYPE_ID =" + subTypeId;
					db.update(sql);
					result = 2;
				}
			} else {
				int actSubTypeId = db.getNextId("ACTIVITY_SUB_TYPE");
				String checkSql = "SELECT * FROM LKUP_ACT_TYPE WHERE UPPER(TYPE)=" + StringUtils.checkString(actType.toUpperCase());
				RowSet checkRs = db.select(checkSql);
				if (checkRs.next()) {
					sql = "INSERT INTO LKUP_ACT_SUBTYPE(ACT_SUBTYPE_ID, ACT_TYPE, ACT_SUBTYPE) VALUES(" + actSubTypeId + "," + StringUtils.checkString(actType.toUpperCase()) + "," + StringUtils.checkString(actSubType) + ")";
					logger.debug(sql);
					db.insert(sql);

					String sqlUpdateNextId = "UPDATE NEXTID SET IDVALUE=(SELECT MAX(ACT_SUBTYPE_ID) FROM LKUP_ACT_SUBTYPE) WHERE IDNAME='ACTIVITY_SUB_TYPE'";
					logger.info(sqlUpdateNextId);
					db.update(sqlUpdateNextId);

					result = 1;
				}
			}
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveActivityType() method" + e.getMessage());
		}

		return result;
	}

	
	
	/**
	 * deletes the notice type
	 * 
	 * @param noticetypeId
	 * @return
	 */
	public int deleteNoticeType(String noticetypeId) {
		logger.debug("Entered into deleteNoticeType...");
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = noticetypeId.split(",");
		logger.debug("id List " + idArray.length);
		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_NOTICE_TYPE WHERE NOTICE_TYPE_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			try {
				new Wrapper().update(sql);
			} catch (SQLException e) {
				result = -1;
				logger.error("Sql error is deleteNoticeType() " + e.getMessage());
			} catch (Exception e) {
				result = -1;
				logger.error("General error is deleteNoticeType() " + e.getMessage());
			}
		}
		return result;
	}
	
	
	/**
	 * deletes the activity sub type
	 * 
	 * @param subtypeId
	 * @return
	 */
	public int deleteActivitySubType(String subtypeId) {
		logger.debug("Entered into deleteActivitySubType...");
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = subtypeId.split(",");
		logger.debug("id List " + idArray.length);
		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_ACT_SUBTYPE WHERE ACT_SUBTYPE_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			try {
				new Wrapper().update(sql);
			} catch (SQLException e) {
				result = -1;
				logger.error("Sql error is deleteActivitySubType() " + e.getMessage());
			} catch (Exception e) {
				result = -1;
				logger.error("General error is deleteActivitySubType() " + e.getMessage());
			}
		}
		return result;
	}

	// End of Activity SubType related methods - Gai3

	// Start of Activity Status related methods - Gai3

	/**
	 * gets the activity status .
	 * 
	 * @param actStatusForm
	 * @return List * @author Hemavathi and Bharti
	 */
	public List getActivityStatus(ActivityStatusAdminForm actStatusForm) {
		logger.info("getActivityStatus(actStatusForm)");
		List actStatus = new ArrayList();

		try {
			String sql = "";

			sql = ((actStatusForm.getStatusCode() == null) || actStatusForm.getStatusCode().equals("")) ? sql : (sql + " UPPER(LAS.STAT_CODE) LIKE '%" + StringUtils.nullReplaceWithEmpty(actStatusForm.getStatusCode().toUpperCase()) + "%' AND");

			if (actStatusForm.isActive() == true)
				sql = sql + " ACTIVE = 'Y' AND";

			if (actStatusForm.isPsaActive() == true)
				sql = sql + " PSA_ACTIVE = 'Y' AND";

			sql = ((actStatusForm.getModuleId() == null) || actStatusForm.getModuleId().equals("-1")) ? sql : (sql + " MODULE_ID = " + StringUtils.nullReplaceWithEmpty(actStatusForm.getModuleId()) + " AND");

			sql = ((actStatusForm.getProjectNameId() == null) || actStatusForm.getProjectNameId().equals("-1")) ? sql : (sql + " LAS.PNAME_ID = " + StringUtils.nullReplaceWithEmpty(actStatusForm.getProjectNameId()) + " AND");

			sql = ((actStatusForm.getDescriptionType() == null) || actStatusForm.getDescriptionType().equals("")) ? sql : (sql + " UPPER(LAS.DESCRIPTION) LIKE '%" + actStatusForm.getDescriptionType().toUpperCase() + "%' AND");

			if ((sql != null) && !("").equalsIgnoreCase(sql)) {
				sql = sql.substring(0, sql.length() - 4); // to remove the trailing
				sql = " WHERE " + sql;
			}

			sql = "SELECT LAS.STATUS_ID,LAS.STAT_CODE,LAS.DESCRIPTION AS DESCRIPTION,LP.DESCRIPTION AS PDESC,LM.DESCRIPTION AS MDESC,LAS.PNAME_ID FROM LKUP_ACT_ST LAS LEFT OUTER JOIN LKUP_PNAME LP ON LAS.PNAME_ID=LP.PNAME_ID  LEFT OUTER JOIN LKUP_MODULE LM ON LAS.MODULE_ID=LM.ID " + sql + " ";
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem items = new DisplayItem();
				items.setFieldOne(StringUtils.nullReplaceWithEmpty(rs.getString("STATUS_ID")));
				items.setFieldTwo(StringUtils.nullReplaceWithEmpty(rs.getString("STAT_CODE")));

				if (activityStatusRemovable(rs.getString("STATUS_ID"))) {
					items.setFieldSix("Y");
				} else {
					items.setFieldSix("N");
				}

				items.setFieldThree(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));
				items.setFieldFour(StringUtils.nullReplaceWithEmpty(rs.getString("PDESC")));
				items.setFieldFive(StringUtils.nullReplaceWithEmpty(rs.getString("MDESC")));
				actStatus.add(items);
			} // end of while loop

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return actStatus;
	}

	/**
	 * checks if the activity status is removable or not
	 * 
	 * @param statuscode
	 * @return * @author Hemavathi and Bharti
	 */
	public boolean activityStatusRemovable(String statusid) {
		String sql = "SELECT COUNT(*) AS COUNT FROM ACTIVITY WHERE STATUS='" + statusid + "'";
		boolean removable = false;

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("COUNT") == 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("Sql error is activitystatusRemovable() " + e.getMessage());
		} catch (Exception e) {
			logger.error("General error is activityStatusRemovable() " + e.getMessage());
		}

		// logger.debug("removable:" + removable);

		return removable;
	}

	/**
	 * gets the activity status for a activity status id.
	 * 
	 * @param actStatusId
	 * @return * @author Hemavathi and Bharti
	 */
	public ActivityStatusAdminForm getActivityStatus(int actStatusId, ActivityStatusAdminForm activityStatusForm) {
		logger.info("getActivityStatus(" + actStatusId + ")");

		try {
			String sql = "SELECT * FROM LKUP_ACT_ST WHERE STATUS_ID=" + actStatusId;
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				activityStatusForm.setActivityStatusId(rs.getString("STATUS_ID"));
				activityStatusForm.setStatusCode(rs.getString("STAT_CODE"));
				activityStatusForm.setDescriptionType(rs.getString("DESCRIPTION"));
				activityStatusForm.setActive(StringUtils.s2b(rs.getString("ACTIVE")));
				activityStatusForm.setPsaActive(StringUtils.s2b(rs.getString("PSA_ACTIVE")));
				activityStatusForm.setModuleId(rs.getString("MODULE_ID"));
				activityStatusForm.setProjectNameId(rs.getString("PNAME_ID"));
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in get activity type " + e.getMessage());
		}

		return activityStatusForm;
	}

	/**
	 * saves the activity status
	 * 
	 * @param actStatus
	 * @param description
	 * @param active
	 * @param projectId
	 * @param psaactive
	 * @param moduleId
	 * @return
	 */
	public int saveActivityStatus(String statusId, String actStatus, String description, boolean active, int projectId, boolean psaactive, int moduleId) {
		logger.info("saveActivityStatus(" + statusId + "," + actStatus + ", " + description + ", " + active + "," + projectId + ",, " + psaactive + "," + moduleId + ")");

		Wrapper db = new Wrapper();
		int result = 0;
		String sql = "";

		try {
			sql = "SELECT * FROM LKUP_ACT_ST WHERE STATUS_ID=" + StringUtils.checkString(statusId);
			logger.info(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				sql = "UPDATE LKUP_ACT_ST SET MODULE_ID=" + moduleId + ",DESCRIPTION=" + StringUtils.checkString(description) + ", ACTIVE=" + StringUtils.checkString(StringUtils.b2s(active)) + ", PNAME_ID=" + projectId + ", PSA_ACTIVE=" + StringUtils.checkString(StringUtils.b2s(psaactive)) + " WHERE UPPER(STAT_CODE)=" + StringUtils.checkString(actStatus.toUpperCase()) + " AND STATUS_ID = " + statusId;
				logger.info(sql);
				result = 2;
				db.update(sql);
			} else {
				int actStatusId = db.getNextId("ACTIVITY_STATUS");

				sql = "INSERT INTO LKUP_ACT_ST (STATUS_ID,STAT_CODE,DESCRIPTION,ACTIVE,PNAME_ID,PSA_ACTIVE,MODULE_ID) VALUES (";
				sql = sql + actStatusId;
				logger.debug("got actStatusId: " + actStatusId);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(actStatus);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(description);
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(active));
				sql = sql + ",";
				sql = sql + projectId;
				sql = sql + ",";
				sql = sql + StringUtils.checkString(StringUtils.b2s(psaactive));
				sql = sql + ",";
				sql = sql + moduleId;
				sql = sql + ")";
				result = 1;
				db.insert(sql);
				logger.info("sql :: " + sql);

			}
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveActivityStatus() method" + e.getMessage());
		}
		return result;
	}

	/**
	 * deletes the activity status
	 * 
	 * @param statusId
	 * @return * @author Hemavathi and Bharti
	 */
	public int deleteActivityStatus(String statusId) {
		logger.debug("Entered into deleteActivityStatus...");
		int result = 0;
		String sql = "";

		String[] idArray = {};

		idArray = statusId.split(",");
		logger.debug("id List " + idArray.length);
		for (int i = 0; i < idArray.length; i++) {
			sql = "DELETE FROM LKUP_ACT_ST WHERE STATUS_ID=" + idArray[i];
			logger.info(sql);
			result = 3;
			try {
				new Wrapper().update(sql);
			} catch (Exception e) {
				result = -1;
				logger.error("General error is activityTypeRemovable() " + e.getMessage());
			}
		}

		return result;
	}

	/**
	 * performs the year end initialize operation.
	 * 
	 * @return
	 */
	public String yearEndInitialize() throws Exception {
		String message = "Initialization Successful.";

		try {
			Wrapper db = new Wrapper();

			String sql = "update nextid set idvalue=1 where idname='BATCH_ID'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='BS'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='CC'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='CM'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='DOTCC'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='DOTFPI'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='DOTFPD'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='FA'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='FR'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='GS'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='HS'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='IT'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='LS'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='P2'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='PL'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='PM'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='PO'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='PR_NUM'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='PW'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='RP'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='SP_NUM'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='T2'";
			db.update(sql);

			sql = "update nextid set idvalue=1 where idname='TR'";
			db.update(sql);
		} catch (Exception e) {
			message = "Initialization Failed";
			logger.error("Error in Year End Initialize Method " + e.getMessage());
		}

		return message;
	}

	/**
	 * resets the year end initialize function.
	 * 
	 */
	public void reSetYearEndInitialize() {
		try {
			Wrapper db = new Wrapper();
			String sql = "update nextid set idvalue=(select 1 where idname='BS'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue= (select 1 where idname='CC'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='CM'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='DOTCC'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='DOTFPI'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='DOTFPD'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='FA'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='FR'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='GS'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='HS'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='IT'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='LS'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='P2'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='PC'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='PM'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='PO'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='PW'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='RP'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='T2'";
			logger.info(sql);
			db.update(sql);
			sql = "update nextid set idvalue=1 where idname='TR'";
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Error in Year End Initialize Method " + e.getMessage());
		}
	}

	/**
	 * gets the activity mapping.
	 * 
	 * @param landUse
	 * @param spType
	 * @param spsType
	 * @return
	 */
	public List getUseSPTypeSPSTypeActTypeMaping(String spType, String spsType) {
		
		StringBuilder sb = new StringBuilder();
		sb.append(" select  ussa.id,lst.sproj_type_id,lst.description as typedesc,lsst.sproj_stype_id,lsst.description subtypedesc,ussa.act_type,lat.description acttypedesc from SPTYPE_SPSTYPE_ACTTYPE ussa join lkup_sproj_type lst on ussa.sproj_type = lst.sproj_type_id join lkup_sproj_stype lsst on ussa.sproj_stype = lsst.sproj_stype_id join lkup_act_type lat on ussa.act_type = lat.type ");
		sb.append(" where ussa.ACTIVE ='Y' and ussa.id>0 ");
		if(Operator.hasValue(spType)){
			sb.append(" and  ussa.sproj_type=").append(spType);
		}
		if(Operator.hasValue(spsType)){
			sb.append(" and ussa.sproj_stype=").append(spsType);
		}	
		//String sql = "select  ussa.id,lst.sproj_type_id,lst.description as typedesc,lsst.sproj_stype_id,lsst.description subtypedesc,ussa.act_type,lat.description acttypedesc from SPTYPE_SPSTYPE_ACTTYPE ussa join lkup_sproj_type lst on ussa.sproj_type = lst.sproj_type_id join lkup_sproj_stype lsst on ussa.sproj_stype = lsst.sproj_stype_id join lkup_act_type lat on ussa.act_type = lat.type where ussa.sproj_type=" + spType + " and ussa.sproj_stype=" + spsType;
		String sql = sb.toString();
		List mapings = new ArrayList();

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem maping = new DisplayItem();
				maping.setFieldOne(rs.getString("id"));
				maping.setFieldTwo(rs.getString("sproj_type_id"));
				maping.setFieldThree(rs.getString("typedesc"));
				maping.setFieldFour(rs.getString("subtypedesc"));
				maping.setFieldFive(rs.getString("acttypedesc"));
				maping.setFieldSix(rs.getString("sproj_stype_id"));
				maping.setFieldSeven(rs.getString("act_type"));
				mapings.add(maping);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getUseSPTypeSPSTypeActTypeMaping method");
		}

		return mapings;
	}

	/**
	 * gets the activity mapping.
	 * 
	 * @param frm
	 * @return
	 */
	public List getUseSPTypeSPSTypeActTypeMaping(ActivityTypeForm frm) {
		String sql = "select  ussa.id,lst.sproj_type_id,lst.description as typedesc,lsst.sproj_stype_id,lsst.description subtypedesc,ussa.act_type,lat.description acttypedesc from SPTYPE_SPSTYPE_ACTTYPE ussa  join lkup_sproj_type lst on ussa.sproj_type = lst.sproj_type_id join lkup_sproj_stype lsst on ussa.sproj_stype = lsst.sproj_stype_id join lkup_act_type lat on ussa.act_type = lat.type";

		String whereSql = "";

		whereSql = ((frm.getSubProjectSubType() == null) || frm.getSubProjectSubType().equals("-1") || frm.getSubProjectSubType().equals("")) ? whereSql : (whereSql + " ussa.sproj_type=" + frm.getSubProjectSubType() + " and");

		whereSql = ((frm.getSubProjectSubSubType() == null) || frm.getSubProjectSubSubType().equals("-1") || frm.getSubProjectSubSubType().equals("")) ? whereSql : (whereSql + " ussa.sproj_stype=" + frm.getSubProjectSubSubType() + " and");

		whereSql = ((frm.getActivityType() == null) || frm.getActivityType().equals("-1") || frm.getActivityType().equals("")) ? whereSql : (whereSql + " ussa.act_type='" + frm.getActivityType() + "' and");

		if ((whereSql != null) && !whereSql.equals("")) {
			whereSql = whereSql.substring(0, whereSql.length() - 4); // to remove the trailing and
			whereSql = " where " + whereSql;
		}

		sql = sql + whereSql + " and ussa.ACTIVE ='Y' order by 2,3,4";
		logger.info("SQL in get mapings from form is " + sql);

		List mapings = new ArrayList();

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem maping = new DisplayItem();
				maping.setFieldOne(rs.getString("id"));
				maping.setFieldTwo(rs.getString("sproj_type_id"));
				maping.setFieldThree(rs.getString("typedesc"));
				maping.setFieldFour(rs.getString("subtypedesc"));
				maping.setFieldFive(rs.getString("acttypedesc"));
				maping.setFieldSix(rs.getString("sproj_stype_id"));
				maping.setFieldSeven(rs.getString("act_type"));
				mapings.add(maping);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getSPTypeSPSTypeActTypeMaping method" + e.getMessage());
		}

		return mapings;
	}

	/**
	 * saves the activity type relationship mapping.
	 * 
	 * @param landUse
	 * @param spType
	 * @param spsType
	 * @param actType
	 * @return
	 */
	public int saveUseSPTypeSPSTypeActTypeMaping(String id,String spType, String spsType, String actType) {
		Wrapper db = new Wrapper();
		int result = 0;

		try {
			String sql = "select * from SPTYPE_SPSTYPE_ACTTYPE where ACTIVE ='Y' and sproj_type=" + spType + " and sproj_stype=" + spsType + " and act_type=" + StringUtils.checkString(actType);
			logger.info(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
					result = 0;
			}else if(id!="" && id!=null){
				sql = "UPDATE SPTYPE_SPSTYPE_ACTTYPE SET sproj_type=" + spType + ",sproj_stype=" + spsType + "," + " act_type=" + StringUtils.checkString(actType) +"  WHERE  ACTIVE ='Y' and id=" +id;
				logger.info("update : "+sql);
				db.insert(sql);
				result = 1;
			}else  {
				int mapId = db.getNextId("ACTTYPE_MAP_ID");
				sql = "insert into SPTYPE_SPSTYPE_ACTTYPE(id,sproj_type,sproj_stype,act_type) values(" + mapId + "," + spType + "," + spsType + "," + StringUtils.checkString(actType) + ")";
				logger.info("insert : "+ sql);
				db.insert(sql);
				result = 1;
			}
		} catch (Exception e) {
			result = -1;
			logger.error("Error in saveUseSPTypeSPSTypeActTypeMaping method" + e.getMessage());
		}

		return result;
	}

	/**
	 * deletes the activity type mapping.
	 * 
	 * @param mapId
	 */
	public void deleteUseSPTypeSPSTypeActTypeMaping(String mapId) {
		Wrapper db = new Wrapper();

		try {
			String sql = "delete from SPTYPE_SPSTYPE_ACTTYPE where id=" + mapId;
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Error in deleteUseSPTypeSPSTypeActTypeMaping method");
		}
	}

	/**
	 * gets the user form
	 * 
	 * @param user
	 * @return
	 */
	public UserForm getUserForm(User user) {
		UserForm userForm = new UserForm();

		if (user != null) {
			userForm.setUserId(StringUtils.i2s(user.getUserId()));
			userForm.setUserName(user.getUsername());
			userForm.setPassword("notshown");
			userForm.setFirstName(user.getFirstName());
			userForm.setMi(user.getMiddleInitial());
			userForm.setLastName(user.getLastName());
			userForm.setEmployeeNumber(user.getEmployeeNumber());
			userForm.setActive(StringUtils.s2b(user.getActive()));
			userForm.setUserEmail(user.getUserEmail());

			if (user.getDepartment() != null) {
				userForm.setDepartmentId(StringUtils.i2s(user.getDepartment().getDepartmentId()));
				userForm.setDepartmentName(user.getDepartment().getDescription());
			} else {
				userForm.setDepartmentId("-1");
				userForm.setDepartmentName("");
			}

			userForm.setTitle(user.getTitle());

			if (user.getRole() == null) {
				userForm.setRole("");
			} else {
				userForm.setRole(StringUtils.i2s(user.getRole().getRoleId()));
			}

			// getting user group ids
			List groups = user.getGroups();

			if (groups == null) {
				groups = new ArrayList();
			}

			Iterator itr = groups.iterator();
			String[] userGroups = new String[groups.size()];
			int i = 0;

			while (itr.hasNext()) {
				Group group = (Group) itr.next();

				if (group == null) {
					userGroups[i] = "";
				} else {
					userGroups[i] = StringUtils.i2s(group.getGroupId());
				}

				i++;
			}

			userForm.setUserGroups(userGroups);
		}

		return userForm;
	}

	/**
	 * gets the list of users from the form
	 * 
	 * @param userForm
	 * @return
	 */
	public List getUsers(UserForm userForm) {
		String sql = "";

		sql = ((userForm.getUserName() == null) || userForm.getUserName().equals("")) ? sql : (sql + " u.username like '%" + userForm.getUserName() + "%' and");

		sql = ((userForm.getPassword() == null) || userForm.getPassword().equals("")) ? sql : (sql + " upper(u.password) like '%" + userForm.getPassword().toUpperCase() + "%' and");

		sql = ((userForm.getFirstName() == null) || userForm.getFirstName().equals("")) ? sql : (sql + " upper(u.first_name) like '%" + userForm.getFirstName().toUpperCase() + "%' and");

		sql = ((userForm.getMi() == null) || userForm.getMi().equals("")) ? sql : (sql + " upper(u.mi) like '%" + userForm.getMi().toUpperCase() + "%' and");

		sql = ((userForm.getLastName() == null) || userForm.getLastName().equals("")) ? sql : (sql + " upper(u.last_name) like '%" + userForm.getLastName().toUpperCase() + "%' and");

		sql = ((userForm.getEmployeeNumber() == null) || userForm.getEmployeeNumber().equals("")) ? sql : (sql + " upper(u.emp_no) like '%" + userForm.getEmployeeNumber().toUpperCase() + "%' and");

		sql = ((("").equalsIgnoreCase(StringUtils.nullReplaceWithEmpty(userForm.getDepartmentId()))) || userForm.getDepartmentId().equals("-1")) ? sql : (sql + " u.dept_id=" + userForm.getDepartmentId() + " and");

		sql = ((userForm.getTitle() == null) || userForm.getTitle().equals("")) ? sql : (sql + " upper(u.title) like '%" + userForm.getTitle().toUpperCase() + "%' and");

		sql = ((userForm.getRole() == null) || userForm.getRole().equals("-1")) ? sql : (sql + " u.role_id=" + userForm.getRole() + " and");

		if (userForm.getActive()) {
			sql = sql + " u.active='Y' and";
		}

		String[] userGroups = userForm.getUserGroups();

		if (!(userGroups == null)) {
			String groupIds = "";

			for (int i = 0; i < userGroups.length; i++) {
				if ((userGroups[i] != null) && !userGroups[i].equals("")) {
					groupIds = groupIds + userGroups[i] + ",";
				}
			}

			if (!groupIds.equals("")) {
				groupIds = groupIds.substring(0, groupIds.length() - 1); // to remove the trailing ,
				sql = sql + " userid in( select distinct user_id from user_groups where group_id in(" + groupIds + ")) and";
			}
		}

		String selectSql = "select u.userid,u.username,coalesce(u.first_name,'') || coalesce(' ' || u.mi,'') || coalesce(' ' || u.last_name,'') as fullname," + "u.title,u.emp_no,d.description,u.active from users u left outer join department d on u.dept_id=d.dept_id";

		if (sql.equals("")) {
			sql = selectSql;
		} else {
			sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
			sql = selectSql + " where " + sql;
		}

		sql = sql + " order by d.description,fullname";
		logger.info("Get Users Sql is " + sql);

		List users = new ArrayList();

		try {
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				DisplayItem displayItem = new DisplayItem();
				displayItem.setFieldOne(rs.getString("userid"));
				displayItem.setFieldTwo(rs.getString("username"));
				displayItem.setFieldThree(rs.getString("fullname"));
				displayItem.setFieldFour(rs.getString("title"));
				displayItem.setFieldFive(rs.getString("description"));

				if ((rs.getString("active") != null) && rs.getString("active").equals("Y")) {
					displayItem.setFieldSix("Yes");
				} else {
					displayItem.setFieldSix("No");
				}

				// displayItem.setFieldSix("Yes");
				users.add(displayItem);
			}
		} catch (SQLException e) {
			logger.error("SQL Error in getusers " + e.getMessage());
		} catch (Exception e) {
			logger.error("General Error in getusers " + e.getMessage());
		}

		return users;
	}

	/**
	 * saves the users to the database.
	 * 
	 * @param userForm
	 * @return
	 * @throws Exception
	 */
	public int saveUser(UserForm userForm) throws Exception {
		logger.debug("saveUser(userForm)");

		int userId = StringUtils.s2i(userForm.getUserId());
		logger.debug("obtained user id from the form is :" + userId);

		Wrapper db = new Wrapper();
		db.beginTransaction();

		try {
			String sql = "select * from users where userid=" + userId;
			logger.info(sql);

			RowSet rs = db.select(sql);
			String password = userForm.getPassword();

			if ((password == null) || (password.equals(""))) {
				password = "notshown";
			}

			if (rs.next()) {
				sql = "update users set ";

				if (!(password.equals("notshown"))) {
					sql += ("  password=" + StringUtils.checkString(jcrypt.crypt("X6", userForm.getPassword())) + ",");
				}

				sql += (" first_name=" + StringUtils.checkString(userForm.getFirstName()) + "," + " mi=" + StringUtils.checkString(userForm.getMi()) + "," + " last_name=" + StringUtils.checkString(userForm.getLastName()) + "," + " emp_no=" + StringUtils.checkString(userForm.getEmployeeNumber()) + "," + " dept_id=" + StringUtils.checkNumber(userForm.getDepartmentId()) + "," + " title=" + StringUtils.checkString(userForm.getTitle()) + "," + " active=" + StringUtils.checkString(StringUtils.b2s(userForm.getActive())) + "," + " role_id=" + userForm.getRole() + "," + " email_id=" + StringUtils.checkString(userForm.getUserEmail()) + " where userid=" + userId);
				logger.info(sql);
				db.addBatch(sql);
				sql = "delete from user_groups where user_id=" + userId;
				logger.info(sql);
				db.addBatch(sql);
			} else {
				// check if the username already exists
				if (usernameExists(userForm.getUserName())) {
					return -1;
				}

				userId = db.getNextId("USER_ID");
				sql = "insert into users(userid,username,password,first_name,mi,last_name,emp_no,dept_id,title,active,role_id,email_id) values(" + userId + "," + StringUtils.checkString(userForm.getUserName()) + "," + StringUtils.checkString(jcrypt.crypt("X6", userForm.getPassword())) + "," + StringUtils.checkString(userForm.getFirstName()) + "," + StringUtils.checkString(userForm.getMi()) + "," + StringUtils.checkString(userForm.getLastName()) + "," + StringUtils.checkString(userForm.getEmployeeNumber()) + "," + userForm.getDepartmentId() + "," + StringUtils.checkString(userForm.getTitle()) + "," + StringUtils.checkString(StringUtils.b2s(userForm.getActive())) + "," + userForm.getRole() + "," + StringUtils.checkString(userForm.getUserEmail()) + ")";
				db.addBatch(sql);
				logger.info("Add user sql .." + sql);
			}

			String[] userGroups = userForm.getUserGroups();

			if (userGroups == null) {
				userGroups = new String[0];
			}

			for (int i = 0; i < userGroups.length; i++) {
				String group = userGroups[i];

				if ((group != null) && !group.equals("")) {
					sql = "insert into user_groups values(" + userId + "," + userGroups[i] + ")";
					logger.info(sql);
					db.addBatch(sql);
				}
			}

			db.executeBatch();

			return userId;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Check if the username exists
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public boolean usernameExists(String username) throws Exception {
		boolean returnType = false;
		Wrapper db = new Wrapper();
		String sql = "select * from users where username=" + StringUtils.checkString(username);
		logger.info(sql);

		RowSet rs = db.select(sql);

		if (rs.next()) {
			returnType = true;
		}

		rs.close();

		return returnType;
	}

	/**
	 * gets the list of library conditions from the form
	 * 
	 * @param frm
	 * @return
	 */
	public List getLibraryConditions(ConditionsLibraryForm frm) {
		String sql = "";
		List conditions = new ArrayList();

		if (frm == null) {
			frm = new ConditionsLibraryForm();
		}

		// level type and condition type
		String levelType = frm.getLevelType();

		if (!((levelType == null) || levelType.equals(""))) {
			sql = sql + " level_type=" + StringUtils.checkString(levelType) + " and";

			String conditionType = frm.getConditionType();

			if (!((conditionType == null) || conditionType.equals("") || conditionType.equals("-1"))) {
				if (levelType.equals("A")) {
					conditionType = new CommonAgent().getActTypeId(conditionType);
				}

				sql = sql + " condition_type=" + conditionType + " and";
			}
		}

		// condition code
		sql = ((frm.getConditionCode() == null) || frm.getConditionCode().equals("")) ? sql : (sql + " upper(condition_code) like " + "'%" + frm.getConditionCode().toUpperCase() + "%' and");

		// inspectability
		sql = ((frm.getInspectability() == null) || frm.getInspectability().equals("")) ? sql : (sql + " inspectability='" + frm.getInspectability() + "' and");

		// Warning
		sql = ((frm.getWarning() == null) || frm.getWarning().equals("")) ? sql : (sql + " warning='" + frm.getWarning() + "' and");

		// Required
		sql = ((frm.getRequired() == null) || frm.getRequired().equals("")) ? sql : (sql + " required='" + frm.getRequired() + "' and");

		// description
		sql = ((frm.getConditionDesc() == null) || frm.getConditionDesc().equals("")) ? sql : (sql + " condition_desc='" + frm.getConditionDesc() + "' and");

		// description
		sql = ((frm.getShortText() == null) || frm.getShortText().equals("")) ? sql : (sql + " upper(short_text) like " + StringUtils.checkString("%" + frm.getShortText().toUpperCase() + "%") + " and");

		// description
		sql = ((frm.getConditionText() == null) || frm.getConditionText().equals("")) ? sql : (sql + " upper(condition_text) like " + StringUtils.checkString("%" + frm.getConditionText().toUpperCase() + "%") + " and");

		// expiration date is null
		sql = sql + " expiration_dt is null and";

		if ((sql != null) && !("").equalsIgnoreCase(sql)) {
			sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
			sql = " where " + sql;
		}

		sql = "select * from lkup_conditions" + sql + " order by level_type,condition_type,short_text";

		try {
			logger.info("The Condition Library Sql is " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				ConditionLibraryRecord libRec = new ConditionLibraryRecord();

				libRec.setLibraryId(rs.getInt("condition_id"));

				if (libraryRecordRemovable(rs.getInt("condition_id"))) {
					libRec.setRemovable("YES");
				} else {
					libRec.setRemovable("NO");
				}

				libRec.setShort_text(rs.getString("short_text"));
				levelType = rs.getString("level_Type");

				if (levelType == null) {
					levelType = "";
				}

				if (levelType.equals("L") || levelType.equals("S") || levelType.equals("O")) {
					libRec.setConditionType(new AddressAgent().getUse(rs.getInt("condition_type")).getDescription());
				} else if (levelType.equals("P")) {
					libRec.setConditionType("");
				} else if (levelType.equals("Q")) {
					libRec.setConditionType(new AdminAgent().getSubProjectType(rs.getInt("condition_type")).getDescription());
				} else if (levelType.equals("A")) {
					libRec.setConditionType(new AdminAgent().getActivityType(rs.getInt("condition_type")).getDescription());
				}

				if (levelType.equals("L")) {
					levelType = "Land";
				} else if (levelType.equals("S")) {
					levelType = "Structure";
				} else if (levelType.equals("O")) {
					levelType = "Occupancy";
				} else if (levelType.equals("P")) {
					levelType = "Project";
				} else if (levelType.equals("Q")) {
					levelType = "Sub Project";
				} else if (levelType.equals("A")) {
					levelType = "Activity";
				}

				libRec.setLevelType(levelType);
				libRec.setConditionCode(rs.getString("condition_code"));

				conditions.add(libRec);
			}
		} catch (Exception e) {
			logger.error("The error in getLibraryConditions is " + e.getMessage());
		}

		return conditions;
	}

	/**
	 * gets the condition library form for
	 * 
	 * @param libConditionId
	 * @return
	 */
	public ConditionsLibraryForm getLibraryCondition(int libConditionId) {
		String sql = "select * from lkup_conditions where condition_id=" + libConditionId;

		ConditionsLibraryForm libForm = new ConditionsLibraryForm();

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				libForm.setConditionId(rs.getString("condition_id"));
				logger.debug("id .." + libForm.getConditionId());
				libForm.setLevelType(rs.getString("level_type"));
				logger.debug("level type .." + libForm.getLevelType());

				if ((libForm.getLevelType() != null) && libForm.getLevelType().equals("A")) {
					libForm.setConditionType(new CommonAgent().getActType(rs.getString("condition_type")));
				} else {
					libForm.setConditionType(rs.getString("condition_type"));
				}

				logger.debug("condition type .." + libForm.getConditionType());
				libForm.setConditionCode(rs.getString("condition_code"));
				logger.debug("condition code .." + libForm.getConditionCode());
				libForm.setInspectability(rs.getString("inspectability"));
				logger.debug("inspectability .." + libForm.getInspectability());
				libForm.setWarning(rs.getString("warning"));
				logger.debug("warning .." + libForm.getWarning());
				libForm.setRequired(rs.getString("required"));
				logger.debug("required .." + libForm.getRequired());
				libForm.setConditionDesc(rs.getString("condition_desc"));
				logger.debug("condition description .." + libForm.getConditionDesc());
				libForm.setShortText(rs.getString("short_text"));
				logger.debug("short text .." + libForm.getShortText());
				// libForm.setRte1(rs.getString("condition_text"));
				// logger.debug("condition text .." + libForm.getRte1());
				libForm.setConditionText(rs.getString("condition_text"));
				logger.debug("condition text .." + libForm.getConditionText());
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("The Error in getLibraryCondition()" + e.getMessage());
		}

		return libForm;
	}

	/**
	 * is the library record removable.
	 * 
	 * @param libRec
	 * @return
	 */
	public boolean libraryRecordRemovable(int libRec) {
		boolean removable = false;
		String sql = "select count(*) as count from conditions where library_id=" + libRec;

		try {
			logger.info(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				if (rs.getInt("count") == 0) {
					removable = true;
				} else {
					removable = false;
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.info("Sql Error in libraryRecordRemovable() " + sql);
		} catch (Exception e) {
			logger.info("General Error in libraryRecordRemovable() " + sql);
		}

		return removable;
	}

	/**
	 * save the condition library
	 * 
	 * @param libForm
	 * @return
	 */
	public ConditionsLibraryForm saveLibraryCondition(ConditionsLibraryForm libForm, int userId) throws Exception {
		String sql = "";
		int libConditionId = 0;
		Wrapper db = new Wrapper();

		if (libForm != null) {
			String conditionType = "0";

			if ((libForm.getLevelType() != null) && libForm.getLevelType().equals("A")) {
				conditionType = new CommonAgent().getActTypeId(libForm.getConditionType());
			} else {
				conditionType = libForm.getConditionType();
			}

			logger.debug("Condition ID .. " + libForm.getConditionId());

			if ((libForm.getConditionId() == null) || libForm.getConditionId().equals("")) {
				libConditionId = db.getNextId("LIB_CONDITION_ID");
				sql = "insert into lkup_conditions(condition_id,level_type,condition_type,condition_code," + "condition_desc,inspectability,warning,required,short_text,condition_text,creation_dt,created_by) values(" + libConditionId + "," + StringUtils.checkString(libForm.getLevelType()) + "," + conditionType + "," + StringUtils.checkString(libForm.getConditionCode()) + "," + StringUtils.checkString(libForm.getConditionDesc()) + "," + StringUtils.checkString(libForm.getInspectability()) + "," + StringUtils.checkString(libForm.getWarning()) + "," + StringUtils.checkString(libForm.getRequired()) + "," + StringUtils.checkString(libForm.getShortText()) + "," + StringUtils.checkString(libForm.getConditionText()) + "," + "current_date" + "," + userId + ")";
			} else {
				libConditionId = StringUtils.s2i(libForm.getConditionId());
				sql = "update lkup_conditions set level_type=" + StringUtils.checkString(libForm.getLevelType()) + "," + "condition_type=" + conditionType + "," + "condition_code=" + StringUtils.checkString(libForm.getConditionCode()) + "," + "condition_desc=" + StringUtils.checkString(libForm.getConditionDesc()) + "," + "inspectability=" + StringUtils.checkString(libForm.getInspectability()) + "," + "warning=" + StringUtils.checkString(libForm.getWarning()) + "," + "required=" + StringUtils.checkString(libForm.getRequired()) + "," + "short_text=" + StringUtils.checkString(libForm.getShortText()) + "," + "condition_text=" + StringUtils.checkString(libForm.getConditionText()) + "," + "updated_by =" + userId + " where condition_id=" + libForm.getConditionId();
			}

		}

		logger.debug("Save Library condition sql is " + sql);

		db.update(sql);

		return getLibraryCondition(libConditionId);
	}

	/**
	 * delete the library condition.
	 * 
	 * @param libId
	 * @return
	 */
	public int deleteLibraryCondition(String libId) {
		int result = 0;
		String sql = "delete from lkup_conditions where condition_id=" + libId;

		try {
			logger.info(sql);
			new Wrapper().update(sql);
		} catch (SQLException e) {
			result = -1;
			logger.error("Sql error is deleteLibraryCondition() " + e.getMessage());
		} catch (Exception e) {
			result = -1;
			logger.error("General error is deleteLibraryCondition() " + e.getMessage());
		}

		return result;
	}

	/**
	 * The Assessor export file format. This output conforms with the county of Los Angeles office of the assessor memo dated April 30,2002 permit file specifications.
	 * 
	 * @param Month
	 * @param Year
	 * @return a static string
	 * @throws Exception
	 */
	public static String assessorReport(String mm, String yyyy, String pdfPath) throws Exception {
		logger.debug("Entering into assessorReport(" + mm + " ," + yyyy + "," + pdfPath + ")");
		Wrapper db = new Wrapper();

		try {

			String sql = "SELECT DISTINCT CONCAT('CB ', SUBSTR(VA.ACT_NBR,5,10)) ACT_NBR,SUBSTR(VA.DESCRIPTION,1,25) DESCRIPTION,VA.ACT_TYPE, TO_NUMBER(VA.VALUATION)AS VALUATION, SUBSTR(TO_CHAR(ISSUED_DATE),1,2) || SUBSTR(TO_CHAR(ISSUED_DATE),4,3) || SUBSTR(TO_CHAR(ISSUED_DATE,'YYYY'),1) AS ISSUE_DATE, val.ADDRESS AS ADDRESS,VAL.CITYSTATEZIP AS CITY, VLO.NAME AS NAME, VLO.APN AS APN FROM (V_ACTIVITY VA left JOIN LSO_APN VLA ON VA.ADDR_ID = VLA.LSO_ID) LEFT JOIN V_ALLADDRESS_LIST VAL ON VA.ADDR_ID  = VAL.ADDR_ID LEFT JOIN V_LSO_OWNER VLO ON VA.ADDR_ID =VLO.ADDR_ID WHERE VA.ACT_TYPE IN (SELECT type  from lkup_act_type where dept_id = 8) AND TO_CHAR(VA.ISSUED_DATE,'mm') = " + mm + " AND TO_CHAR(VA.ISSUED_DATE,'yyyy') = " + yyyy + " ORDER BY ACT_NBR";
			logger.info("Assessor export SQL : " + sql);

			RowSet rs = db.select(sql);

			logger.debug("pdf location is " + pdfPath);

			String fileName = "Permit_File_To_Assessor_" + mm + yyyy + ".txt";

			logger.debug("pdf file name is: " + fileName);

			String fileLocation = pdfPath + File.separator + fileName;

			logger.debug("file location is " + fileLocation);

			FileWriter assessor = new FileWriter(fileLocation);
			logger.debug("File writer created");
			PrintWriter fileOutput = new PrintWriter(assessor);

			logger.debug("Before While");

			while (rs.next()) {
				StringBuffer row = new StringBuffer();

				// APN
				if ((rs.getString("APN") == null) || rs.getString("APN").equals("")) {
					row.append("0000000000");
				} else {
					row.append(rs.getString("APN")); // 10
				}
				// PERMIT NUMBER
				row.append(rs.getString("ACT_NBR")); // 10 + 9 = 19
				// ISSUE DATE

				// row.append(rs.getString("ISSUE_DATE")); //

				String issuedDate = "";

				if (rs.getString("ISSUE_DATE") != null) {

					String mon = (rs.getString("ISSUE_DATE")).substring(2, 5);

					String day = (rs.getString("ISSUE_DATE")).substring(0, 2);
					String year = (rs.getString("ISSUE_DATE")).substring(7, 9);
					// String dayYear = (day).concat(year);

					String[] months = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

					int j = 0;
					for (int i = 0; i < months.length; i++) {

						if (months[i].equalsIgnoreCase(mon)) {
							j = i;
							break;
						}
					}
					String m = StringUtils.lpad(++j + "", 2, "0");
					issuedDate = year + m + day;
					logger.debug("$$$$$$$$$ " + issuedDate);
					row.append(issuedDate); // 19 + 6 = 25
				}

				// UNITS
				row.append("000"); // 25 + 3 = 28
				// SQ FEET - CU YARDS
				row.append("          "); // 28 + 10 = 38
				// VALUATION
				row.append(StringUtils.lpad(rs.getString("VALUATION"), 9, "0")); // 38 + 9 = 47
				// PERMIT INFORMATION AS TRACK-LOT-BLOCK
				row.append("             "); // 47 + 13 = 60
				// WORK DESCRIPTION
				row.append(StringUtils.rpad((rs.getString("DESCRIPTION") != null ? rs.getString("DESCRIPTION") : "").trim(), 25, " ")); // 60 + 25 = 85
				// OWNER NAME
				row.append(StringUtils.rpad((rs.getString("NAME") != null ? rs.getString("NAME") : "").trim(), 32, " ")); // 85 + 32 = 117
				// SITUS ADDRESS
				row.append(StringUtils.rpad((rs.getString("ADDRESS") != null ? rs.getString("ADDRESS") : "").trim(), 26, " ")); // 117 + 26 = 143
				// PHONE NUMBER
				row.append("          "); // 143 + 10 = 153
				// FILLER
				row.append("       "); // 153 + 7 = 160
				fileOutput.println(row.toString());
				logger.debug(row.toString());
			}
			logger.debug("Before While");
			fileOutput.close();

			if (rs != null) {
				rs.close();
			}

			return fileName;
		} catch (Exception e) {
			logger.error("Exception occured while creating assessor export file : " + e.getMessage());
			throw e;
		}
	}

	public static void changeOccupancyAsLand(int occupancyId) throws Exception {
		Wrapper db = new Wrapper();
		db.beginTransaction();

		int projectId = db.getNextId("PROJECT_ID");
		db.addBatch("INSERT INTO LAND(LAND_ID,ACTIVE,PZONE_ID,R_ZONE) VALUES(" + occupancyId + ",'Y',11,66)");
		db.addBatch("INSERT INTO land_usage(land_id,lso_use_id) values(" + occupancyId + ",47)");
		db.addBatch("update lso_address set lso_type = 'L' where lso_id = " + occupancyId);
		db.addBatch("update lso_apn set lso_type = 'L' where lso_id = " + occupancyId);
		db.addBatch("delete from structure_occupant where occupancy_id = " + occupancyId);
		db.addBatch("delete from occupancy where occupancy_id = " + occupancyId);
		db.addBatch("delete from occupancy_usage where occupancy_id=" + occupancyId);
		db.addBatch("insert into project(proj_id,lso_id,name,description,status_id,dept_id,microfilm) values(" + projectId + "," + occupancyId + ",'PARKING','PARKING',6,9,'N')");
		db.addBatch("update sub_project set proj_id = " + projectId + " where dot_id = " + occupancyId);
		db.executeBatch();
	}

	/**
	 * Email admin form
	 * 
	 * @return
	 * @throws Exception
	 */
	public EmailAdminForm getEmailTemplates() throws Exception {
		logger.info("getEmailTemplates()");

		String sql = "SELECT * FROM LKUP_EMAIL_TEMPLATE";
		RowSet rs = new Wrapper().select(sql);
		EmailAdminForm emailAdminForm = new EmailAdminForm();

		while (rs.next()) {
			if (rs.getString("TITLE").equals("REGISTRATION_SUCCESS")) {
				emailAdminForm.setRegistrationSuccessMessage(rs.getString("MESSAGE"));
				emailAdminForm.setRegistrationSuccessSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("FORGOT_PASSWORD")) {
				emailAdminForm.setForgotPasswordMessage(rs.getString("MESSAGE"));
				emailAdminForm.setForgotPasswordSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("INSPECTION_REQUESTED")) {
				emailAdminForm.setInspectionRequestedMessage(rs.getString("MESSAGE"));
				emailAdminForm.setInspectionRequestedSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("INSPECTION_SCHEDULED")) {
				emailAdminForm.setInspectionScheduledMessage(rs.getString("MESSAGE"));
				emailAdminForm.setInspectionScheduledSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("INSPECTION_CANCELLED")) {
				emailAdminForm.setInspectionCancelledMessage(rs.getString("MESSAGE"));
				emailAdminForm.setInspectionCancelledSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("PLANCHECK_STATUS_CHANGE")) {
				emailAdminForm.setPlanCheckStatusChangeMessage(rs.getString("MESSAGE"));
				emailAdminForm.setPlanCheckStatusChangeSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("PERMIT_STATUS_CHANGE")) {
				emailAdminForm.setPermitStatusChangeMessage(rs.getString("MESSAGE"));
				emailAdminForm.setPermitStatusChangeSubject(rs.getString("SUBJECT"));
			}
			if (rs.getString("TITLE").equals("TEMPORARY_PASSWORD")) {
				emailAdminForm.setTempPasswordChangeMessage(rs.getString("MESSAGE"));
				emailAdminForm.setTempPasswordChangeSubject(rs.getString("SUBJECT"));
			}

			if (rs.getString("TITLE").equals("PAYMENT_SUCCESSFULL")) {
				emailAdminForm.setPaymentChangeMessage(rs.getString("MESSAGE"));
				emailAdminForm.setPaymentChangeSubject(rs.getString("SUBJECT"));
			}
		}

		return emailAdminForm;
	}

	/**
	 * Email admin form
	 * 
	 * @return
	 * @throws Exception
	 */
	public String getEmailMessage(String title) throws Exception {
		logger.info("getEmailTemplates(" + title + ")");

		String message = "";
		//String sql = "SELECT * FROM LKUP_EMAIL_TEMPLATE where title=" + StringUtils.checkString(title);
		String sql = "SELECT * FROM EMAIL_TEMPLATE ET left outer join LKUP_EMAIL_TEMPLATE_TYPE LET on ET.EMAIL_TEMPLATE_ID = LET.EMAIL_TEMP_TYPE_ID where LET.EMAIL_TEMP_TYPE =" + StringUtils.checkString(title);
		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			//message = rs.getString("MESSAGE");
			message = rs.getString("EMAIL_BODY");
		}

		return message;
	}

	public String getEmailSubject(String title) throws Exception {
		logger.info("getEmailTemplates(" + title + ")");

		String message = "";
		//String sql = "SELECT * FROM LKUP_EMAIL_TEMPLATE where title=" + StringUtils.checkString(title);
		String sql = "SELECT * FROM EMAIL_TEMPLATE ET left outer join LKUP_EMAIL_TEMPLATE_TYPE LET on ET.EMAIL_TEMPLATE_ID = LET.EMAIL_TEMP_TYPE_ID where LET.EMAIL_TEMP_TYPE =" + StringUtils.checkString(title);
		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			//message = rs.getString("SUBJECT");
			message = rs.getString("EMAIL_SUBJECT");
		}

		return message;
	}

	public void saveEmailTemplates(EmailAdminForm emailAdminForm) throws Exception {
		logger.info("saveEmailTemplates(emailAdminForm)");
		Wrapper db = new Wrapper();
		db.beginTransaction();
		String sql = "";
		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getRegistrationSuccessMessage()) + " WHERE TITLE='REGISTRATION_SUCCESS'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getForgotPasswordMessage()) + " WHERE TITLE='FORGOT_PASSWORD'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getInspectionRequestedMessage()) + " WHERE TITLE='INSPECTION_REQUESTED'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getInspectionScheduledMessage()) + " WHERE TITLE='INSPECTION_SCHEDULED'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getInspectionCancelledMessage()) + " WHERE TITLE='INSPECTION_CANCELLED'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getPlanCheckStatusChangeMessage()) + " WHERE TITLE='PLANCHECK_STATUS_CHANGE'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getPermitStatusChangeMessage()) + " WHERE TITLE='PERMIT_STATUS_CHANGE'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getTempPasswordChangeMessage()) + " WHERE TITLE='TEMPORARY_PASSWORD'";
		logger.info(sql);
		db.addBatch(sql);

		sql = "UPDATE LKUP_EMAIL_TEMPLATE SET MESSAGE=" + StringUtils.checkString(emailAdminForm.getPaymentChangeMessage()) + " WHERE TITLE='PAYMENT_SUCCESSFULL'";
		logger.info(sql);
		db.addBatch(sql);

		db.executeBatch();
	}

	/**
	 * changes the password, given the old password and new password.
	 * 
	 * @param username
	 * @param oldPassword
	 * @param newPassword
	 * @throws Exception
	 */
	public void changePassword(String username, String oldPassword, String newPassword) throws Exception {
		logger.info("changePassword(" + username + "," + oldPassword + "," + newPassword + ")");

		try {
			// create an instance of the database wrapper.
			Wrapper db = new Wrapper();
			String selectSql = "SELECT * FROM USERS WHERE USERNAME=" + StringUtils.checkString(username) + " AND PASSWORD=" + StringUtils.checkString(oldPassword);
			logger.info(selectSql);
			RowSet result = db.select(selectSql);
			if (result.next()) {
				logger.debug("The record exists, going to update it");
				String updateSql = "UPDATE USERS SET PASSWORD=" + StringUtils.checkString(newPassword) + " WHERE USERNAME=" + StringUtils.checkString(username) + " AND PASSWORD=" + StringUtils.checkString(oldPassword);
				logger.info(updateSql);
				db.update(updateSql);
				logger.debug("database update successful");
			} else {
				logger.error("record does not exist , throwing exception ");
				throw new SQLException("record does not exist");
			}
		} catch (Exception e) {
			logger.error("Exception occured while updating password " + e.getMessage());
			throw e;
		}
	}

	public boolean getAddresses(MoveLsoForm moveLsoForm, MoveVerifyForm verifyForm) {
		try {
			Wrapper db = new Wrapper();
			verifyForm.setMoveType(moveLsoForm.getMoveType());
			StringBuffer sql = new StringBuffer();
			boolean success = true;
			if (moveLsoForm.getMoveType().equals("O")) {
				sql.append("select s.structure_id as lso_id,val.address,val.citystatezip,s.alias, s.description " + "from (structure s left outer join lso_address la on la.lso_id=s.structure_id) " + " left outer join v_address_list val on val.lso_id=s.structure_id " + " where la.lso_type='S' and la.str_no=" + moveLsoForm.getToNumber() + " and la.street_id = " + moveLsoForm.getToName());
				if (moveLsoForm.getToFraction().length() > 0)
					sql.append(" and la.str_mod = '" + moveLsoForm.getToFraction() + "'");
			} else {
				sql.append("select l.land_id as lso_id,val.address,val.citystatezip,l.alias, l.description " + "from (land l left outer join lso_address la on la.lso_id=l.land_id) " + " left outer join v_address_list val on val.lso_id=l.land_id " + " where la.lso_type='L' and la.str_no=" + moveLsoForm.getToNumber() + " and la.street_id = " + moveLsoForm.getToName());
				if (moveLsoForm.getToFraction().length() > 0)
					sql.append(" and la.str_mod = '" + moveLsoForm.getToFraction() + "'");
			}
			logger.debug("Select sql : " + sql);

			RowSet rs = db.select(sql.toString());
			if (rs.next()) {
				verifyForm.setToId(rs.getString("lso_id"));
				verifyForm.setToAlias(rs.getString("alias"));
				verifyForm.setToDescription(rs.getString("description"));
				verifyForm.setToAddressOne(rs.getString("address"));
				verifyForm.setToAddressTwo(rs.getString("citystatezip"));
			} else
				success = false;
			rs.close();
			rs = null;

			sql = new StringBuffer();
			if (moveLsoForm.getMoveType().equals("S")) {
				sql.append("select s.structure_id as lso_id,val.address,val.citystatezip,s.alias, s.description " + "from (structure s left outer join lso_address la on la.lso_id=s.structure_id) " + " left outer join v_address_list val on val.lso_id=s.structure_id " + " where la.lso_type='S' and la.str_no=" + moveLsoForm.getMoveNumber() + " and la.street_id = " + moveLsoForm.getMoveName());
				if (moveLsoForm.getMoveFraction().length() > 0)
					sql.append(" and la.str_mod = '" + moveLsoForm.getMoveFraction() + "'");
			} else {
				sql.append("select o.occupancy_id as lso_id,val.address,val.citystatezip,o.alias, o.description " + "from (occupancy o left outer join lso_address la on la.lso_id=o.occupancy_id) " + " left outer join v_address_list val on val.lso_id=o.occupancy_id " + " where la.lso_type='O' and la.str_no=" + moveLsoForm.getMoveNumber() + " and la.street_id = " + moveLsoForm.getMoveName());
				if (moveLsoForm.getMoveFraction().length() > 0)
					sql.append(" and la.str_mod = '" + moveLsoForm.getMoveFraction() + "'");
				if (moveLsoForm.getMoveUnit().length() > 0)
					sql.append(" and la.unit = '" + moveLsoForm.getMoveUnit() + "'");
			}
			logger.debug("Select sql : " + sql);

			rs = db.select(sql.toString());
			List moveStructureDetails = new ArrayList();
			while (rs.next()) {
				MoveStructureDetail moveStructureDetail = new MoveStructureDetail();
				moveStructureDetail.setMoveId(rs.getString("lso_id"));
				moveStructureDetail.setMoveAlias(rs.getString("alias"));
				moveStructureDetail.setMoveDescription(rs.getString("description"));
				moveStructureDetail.setMoveAddressOne(rs.getString("address"));
				moveStructureDetail.setMoveAddressTwo(rs.getString("citystatezip"));
				moveStructureDetails.add(moveStructureDetail);
			}
			verifyForm.setMoveStructureDetails(moveStructureDetails);
			rs.close();
			rs = null;
			return success;
		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return false;
		}
	}

	public boolean moveLso(String moveType, String moveLsoId, String toLsoId) {
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			if (moveType.equals("O")) {
				sql = "update structure_occupant set structure_id=" + toLsoId + " where occupancy_id =" + moveLsoId;
			} else {
				sql = "update land_structure set land_id=" + toLsoId + " where structure_id =" + moveLsoId;
			}
			logger.debug("Update sql : " + sql);

			db.update(sql);
			return true;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return false;
		}
	}

	public MoveVerifyForm getLso(String lsoId, MoveVerifyForm form) {
		try {
			Wrapper db = new Wrapper();
			// getting subproject Details;
			String sql = "select val.lso_type,val.address,val.citystatezip from v_address_list val" + " where val.lso_id = " + lsoId;
			logger.debug("select sql : " + sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				form.setLsoNbr(lsoId);
				form.setLsoAddressOne(rs.getString("address"));
				form.setLsoAddressTwo(rs.getString("citystatezip"));
				String lsoType = rs.getString("lso_type");
				if (lsoType == null)
					lsoType = "";
				sql = "select alias,description from";
				if (lsoType.equals("L"))
					sql = sql + " land where land_id=";
				else if (lsoType.equals("S"))
					sql = sql + " structure where structure_id=";
				else if (lsoType.equals("O"))
					sql = sql + " occupancy where occupancy_id=";
				sql = sql + lsoId;
				RowSet rs1 = db.select(sql);
				if (rs1.next()) {
					form.setLsoAlias(rs1.getString("alias"));
					form.setLsoDescription(rs1.getString("description"));
				}
				if (rs1 != null)
					rs1.close();
			}
			if (rs != null)
				rs.close();
			return form;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	public MoveVerifyForm getProject(String projectNbr, MoveVerifyForm form) {
		try {
			Wrapper db = new Wrapper();
			// getting subproject Details;
			String sql = "select p.project_nbr, p.name,p.description,val.address,val.citystatezip from project p, v_address_list val where upper(p.project_nbr) =upper('" + projectNbr + "') and p.lso_id = val.lso_id";

			logger.debug("Select sql : " + sql);

			RowSet rs = db.select(sql);

			// MoveVerifyForm form = new MoveVerifyForm();

			if (rs.next()) {
				form.setProjectNbr(rs.getString("project_nbr"));
				logger.debug("Project Number" + rs.getString("project_nbr"));
				form.setProjectName(rs.getString("name"));
				form.setProjectDescription(rs.getString("description"));
				form.setAddressOne(rs.getString("address"));
				form.setAddressTwo(rs.getString("citystatezip"));
			}

			rs.close();
			rs = null;
			return form;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	public MoveVerifyForm getSubProject(String subProjNbr, MoveVerifyForm form) {
		try {
			Wrapper db = new Wrapper();
			// getting subproject Details;
			String sql = "select sp.sproj_id,sp.sproj_nbr,sp.proj_id,sp.sproj_type,sp.stype_id,sp.sstype_id,sp.status,sp.description,lpt.description as lpt_desc,lspt.description as lspt_desc,lspt.dept_code,lspt.sproj_type as lspt_sproj_type,lspst.type,lspst.description as lspst_desc  from sub_project sp join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id left outer join lkup_sproj_type lspt on sp.stype_id = lspt.sproj_type_id left outer join lkup_sproj_stype lspst on sp.sstype_id=lspst.sproj_stype_id  where upper(sp.sproj_nbr)=upper('" + subProjNbr + "')";
			logger.debug("Select sql : " + sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				form.setSubProjectId(rs.getString("sproj_id"));
				form.setSubProjectNbr(rs.getString("sproj_nbr"));
				form.setSubProjectName(rs.getString("description"));
				if (rs.getString("lpt_desc") != null && (rs.getString("lpt_desc").startsWith("BL") || rs.getString("lpt_desc").startsWith("BT"))) {
					form.setSubProjectDescription(rs.getString("lpt_desc").toUpperCase());
					logger.debug("subProject Name is : " + rs.getString("lpt_desc").toUpperCase());
				} else {
					form.setSubProjectDescription(rs.getString("lpt_desc"));
				}
				form.setSubProjectType(rs.getString("lspt_desc"));
				form.setSubProjectSubType(rs.getString("lspst_desc"));
			}

			rs.close();
			rs = null;
			return form;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	public MoveVerifyForm getActivity(String activityNbr, MoveVerifyForm form) {
		try {
			Wrapper db = new Wrapper();
			// getting subproject Details;
			String sql = "select a.act_id, act_nbr,a.description description,lkat.description type,af.due due,af.paid paid " + "from activity a,lkup_act_type lkat,(select sum(af.fee_amnt) due,sum(af.fee_paid) paid from activity a,activity_fee af where upper(a.act_nbr) =upper('" + activityNbr + "') and a.act_id = af.activity_id) af " + "where upper(a.act_nbr) =upper('" + activityNbr + "') and upper(lkat.type) = upper(a.act_type)";
			logger.debug("Select sql : " + sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				form.setActivityId(rs.getString("act_id"));
				form.setActivityNbr(rs.getString("act_nbr"));
				form.setActivityType(rs.getString("type"));
				form.setActivityDescription(rs.getString("description"));
				form.setAmtDue(rs.getString("due"));
				form.setAmtPaid(rs.getString("paid"));
			}

			rs.close();
			rs = null;
			return form;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	public LsoAddress getLsoDetails(String lsoId) {
		logger.info("Entering getLsoDetails() with id " + lsoId);
		LsoAddress lsoAddress = new LsoAddress();
		Street street = new Street();
		try {
			Wrapper db = new Wrapper();

			// begin of geting the LsoAddress Object

			String sql = "select * from lso_address la,v_street_list v " + " where v.street_id = la.street_id and la.primary = 'Y' and la.lso_id = " + lsoId;

			logger.debug("The select Sql of LsoAddress is " + sql);
			RowSet rs = db.select(sql);
			if (rs.next()) {
				// setting the Address Details Object of lsoAddress Object
				lsoAddress.setLsoId(rs.getInt("lso_id"));
				logger.debug("getProjectDetail -- Primary Address Details ..lso id is .. " + StringUtils.i2s(lsoAddress.getLsoId()));
				lsoAddress.setLsoType(rs.getString("lso_type"));
				if (lsoAddress.getLsoType() == null)
					lsoAddress.setLsoType("");
				logger.debug("getProjectDetail -- Primary Address Details ..lso type is .. " + lsoAddress.getLsoType());

				lsoAddress.setCity(rs.getString("city"));
				if (lsoAddress.getCity() == null)
					lsoAddress.setCity("");
				logger.debug("getProjectDetail -- Primary Address Details ..City is .. " + lsoAddress.getCity());
				lsoAddress.setState(rs.getString("state"));
				if (lsoAddress.getState() == null)
					lsoAddress.setState("");
				logger.debug("getProjectDetail -- Primary Address Details ..State is .. " + lsoAddress.getState());
				lsoAddress.setZip(rs.getString("zip"));
				if (lsoAddress.getZip() == null)
					lsoAddress.setZip("");
				logger.debug("getProjectDetail -- Primary Address Details ..zip is .. " + lsoAddress.getZip());
				lsoAddress.setZip4(rs.getString("zip4"));
				if (lsoAddress.getZip4() == null)
					lsoAddress.setZip4("");
				logger.debug("getProjectDetail -- Primary Address Details ..zip4 is .. " + lsoAddress.getZip4());
				lsoAddress.setStreetModifier(rs.getString("str_mod"));
				if (lsoAddress.getStreetModifier() == null)
					lsoAddress.setStreetModifier("");
				logger.debug("getProjectDetail -- Primary Address Details ..str mod is .. " + lsoAddress.getStreetModifier());
				lsoAddress.setStreetNbr(rs.getInt("str_no"));
				logger.debug("getProjectDetail -- Primary Address Details ..Str No  is .. " + lsoAddress.getStreetNbr());
				lsoAddress.setUnit(rs.getString("unit"));
				if (lsoAddress.getUnit() == null)
					lsoAddress.setUnit("");
				logger.debug("getProjectDetail -- Primary Address Details ..unit is .. " + lsoAddress.getUnit());
				lsoAddress.setDescription(rs.getString("description"));
				if (lsoAddress.getDescription() == null)
					lsoAddress.setDescription("");
				logger.debug("getProjectDetail -- Primary Address Details ..description is .. " + lsoAddress.getDescription());

				street.setStreetId(rs.getInt("str_no"));
				street.setStreetName(rs.getString("street_name"));
				if (street.getStreetName() == null)
					street.setStreetName("");
				if (street == null) {
					street = new Street();
				}
				lsoAddress.setStreet(street);

			}
			if (lsoAddress == null) {
				lsoAddress = new LsoAddress();
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Error in MoveProjectAgent getLsoDetails() : sql : " + e.getMessage());
		}

		logger.info("Exiting MoveProjectAgent getLsoDetails()");
		return lsoAddress;

	}

	public int moveProject(String projectNbr, int lsoId) {
		logger.info("Entering moveProject() with proj nbr " + projectNbr + " Lso Id " + lsoId);
		try {

			Wrapper db = new Wrapper();
			RowSet rs = null;
			RowSet subProjId_rs = null;
			String addrId_sql = "select addr_Id from v_address_list  where lso_Id =" + lsoId;
			logger.debug("addrId_sql is::::::: " + addrId_sql);
			rs = db.select(addrId_sql);
			int addr_Id = -1;
			while (rs.next()) {
				addr_Id = rs.getInt("ADDR_ID");
			}

			String sql = "";
			if (addr_Id > 0) {
				sql = "update project set  " + "lso_id=" + lsoId + "  where upper(project_Nbr)=upper('" + projectNbr + "')";
				logger.debug(sql);
				db.update(sql);
			}

			String subProjId_sql = "select sproj_Id from sub_project where proj_Id in (select proj_Id from project where lso_id =" + lsoId + ")";
			logger.debug("subProjId_sql is::::::: " + subProjId_sql);
			subProjId_rs = db.select(subProjId_sql);
			int subProjId = -1;
			int activityId = -1;
			while (subProjId_rs.next()) {
				subProjId = subProjId_rs.getInt("sproj_id");
				logger.debug("subProjId***** " + subProjId);

				String actId_sql = "select act_id from activity where sproj_id =" + subProjId;
				logger.debug("subProjId_sql is::::::: " + actId_sql);
				RowSet actId_rs = db.select(actId_sql);

				while (actId_rs.next()) {
					activityId = actId_rs.getInt("act_id");
					String activity_sql = "";
					activity_sql = "update activity set  " + "sproj_id=" + subProjId + ", addr_id=" + addr_Id + "  where act_id=" + activityId;
					logger.debug("activity_sql is :: " + activity_sql);
					db.update(activity_sql);
				}
			}
			if (addr_Id < 0 && activityId < 0) {
				lsoId = -1;
				logger.debug("*****Project cannot be Moved *****");
			} else {
				logger.debug("*****Project Moved successfully*****");
			}
		} catch (Exception e) {
			lsoId = -1;
			logger.error("Exception in -- MoveProjectAgent --   moveProject()  -- " + e.getMessage());
		}
		return lsoId;
	}

	public String moveSubProject(String subProjectNbr, String projectNbr) {
		logger.info("Entering moveSubProject() with subprojectid " + subProjectNbr + " project nbr " + projectNbr);

		String projectId = StringUtils.i2s(getProjectId((projectNbr)));
		try {
			Wrapper db = new Wrapper();
			String sql = "";

			if ((StringUtils.s2i(projectId) != 0) || subProjectNbr.equals(null)) {
				sql = "update sub_project set  " + "proj_id=" + projectId + "  where upper(sproj_nbr)=upper('" + subProjectNbr + "')";
				logger.debug(sql);
				db.update(sql);

				String addrId_sql = "select addr_Id from v_address_list where lso_Id = (select lso_Id from project where proj_Id =" + StringUtils.nullReplaceWithEmpty(projectId) + ")";
				RowSet addId_rs = db.select(addrId_sql);
				int addrId = -1;
				while (addId_rs.next()) {
					addrId = addId_rs.getInt("addr_Id");
				}
				String subProj_sql = "select sproj_id from sub_project where sproj_nbr =" + StringUtils.nullReplaceWithEmpty(subProjectNbr) + "";
				logger.debug("subProjId_sql is  " + subProj_sql);
				RowSet subProj_rs = db.select(subProj_sql);
				if (subProj_rs.next()) {
					String actId_sql = "select act_id, sproj_id from activity where sproj_id = (select sproj_id from sub_project where sproj_nbr =" + StringUtils.nullReplaceWithEmpty(subProjectNbr) + ")";
					logger.debug("subProjId_sql is  " + actId_sql);
					RowSet actId_rs = db.select(actId_sql);
					int activityId = -1;
					int subProjId = -1;
					while (actId_rs.next()) {
						activityId = actId_rs.getInt("act_id");
						subProjId = actId_rs.getInt("sproj_id");
						String activity_sql = "";
						activity_sql = "update activity set  " + "sproj_id=" + subProjId + ", addr_id=" + addrId + "  where act_id=" + activityId;
						logger.debug("activity_sql is :: " + activity_sql);
						db.update(activity_sql);
					}
				} else
					addrId = -1;
				if (addrId < 0)
					projectId = "0";
			}

		} catch (Exception e) {
			projectId = null;
			logger.error("Exception in -- MoveProjectAgent --   moveSubProject()  -- " + e.getMessage());
		}
		return projectId;
	}

	/**
	 * @param subProjectName
	 * @return pTypeId
	 * @throws Exception
	 * @author Gayathri & Manjuprasad
	 */
	public String getlkupSubProjectName(String subProjectId) throws Exception {
		logger.info("checklkupSubProjectName(" + subProjectId + ")");
		Wrapper db = new Wrapper();
		String sql = "";
		String description = "";

		sql = "SELECT LKP.DESCRIPTION  FROM LKUP_PTYPE LKP LEFT OUTER JOIN SUB_PROJECT SP ON LKP.PTYPE_ID = SP.SPROJ_TYPE WHERE SP.SPROJ_ID = " + subProjectId;
		logger.debug(" sql statement " + sql);

		RowSet rs = new Wrapper().select(sql);

		while (rs.next()) {
			description = rs.getString("DESCRIPTION");
			logger.debug("Description Is " + description);
		}

		return description;
	}

	public int moveActivity(String activityId, String subProjectId) {
		logger.info("Entering moveActivity() with SProjId " + subProjectId + " activity nbr " + activityId);
		String activityNumber = "";
		String sql = "";
		String description = "";
		int flag = 0;
		try {
			Wrapper db = new Wrapper();
			if (!subProjectId.equals(null) || !activityId.equals(null)) {
				flag = 1;
				String addrId_sql = "select addr_Id from v_address_list  where lso_Id = (select lso_Id from project where proj_Id = (select proj_Id from sub_project where sproj_Id =" + subProjectId + "))";
				logger.debug("addrId_sql is::::::: " + addrId_sql);
				RowSet rs = db.select(addrId_sql);
				int addr_Id = -1;
				if (rs.next()) {
					addr_Id = rs.getInt("ADDR_ID");
				}

				activityNumber = LookupAgent.getActivityNumberForActivityId(activityId);

				description = getlkupSubProjectName(subProjectId);
				description = description.trim().substring(4);
				logger.debug("description is  " + description);
				if (activityNumber.trim().startsWith("BL")) {
					String updateBLSql = "UPDATE BL_ACTIVITY SET BUSINESS_NAME=" + StringUtils.checkString(description.trim()) + "  where act_id=" + activityId;
					logger.debug(updateBLSql);
					db.update(updateBLSql);
				} else if (activityNumber.trim().startsWith("BT")) {
					String updateBTSql = "UPDATE BT_ACTIVITY SET BUSINESS_NAME=" + StringUtils.checkString(description.trim()) + "  where act_id=" + activityId;
					logger.debug(updateBTSql);
					db.update(updateBTSql);
				}

				sql = "update activity set sproj_id=" + subProjectId + ", addr_id=" + addr_Id + "  where act_id=" + activityId;
				logger.debug(sql);
				db.update(sql);

			}

		} catch (Exception e) {
			logger.error("Exception in -- MoveProjectAgent --   moveActivity()  -- " + e.getMessage());
		}
		return flag;
	}

	public Project getProjectDetail(String projectNbr) {
		ProjectDetail projectDetail = null;
		Lso lso = null;
		Project project = new Project();
		LsoAddress lsoAddress = new LsoAddress();

		try {
			Wrapper db = new Wrapper();

			// begin of geting the ProjectDetail Object

			String sql = "select * from project where project_nbr= '" + projectNbr + "'";
			logger.debug("The select Sql of Project is " + sql);
			RowSet rs = db.select(sql);
			projectDetail = new ProjectDetail();
			lso = new Lso();
			if (rs.next()) {
				// setting the ProjectDetails Object of Project Object
				projectDetail.setProjectNumber(rs.getString("project_nbr"));
				if (projectDetail.getProjectNumber() == null)
					projectDetail.setProjectNumber("");
				logger.debug("getProjectDetail -- Project Details ..Project Number is .. " + projectDetail.getProjectNumber());
				projectDetail.setName(rs.getString("name"));
				if (projectDetail.getName() == null)
					projectDetail.setName("");
				logger.debug("getProjectDetail -- Project Details ..Name is .. " + projectDetail.getName());
				projectDetail.setDescription(rs.getString("description"));
				if (projectDetail.getDescription() == null)
					projectDetail.setDescription("");
				logger.debug("getProjectDetail -- Project Details ..Description  is .. " + projectDetail.getDescription());
				projectDetail.setProjectStatus(LookupAgent.getProjectStatus(rs.getInt("status_id")));
				logger.debug("getPprojectDetail -- Project Details ..Status is .. " + projectDetail.getProjectStatus().getStatusId());

				lso.setLsoId(rs.getInt("lso_id"));
				logger.debug("getProjectDetail -- Project Details ..lso id  is .. " + lso.getLsoId());

			}

			if (projectDetail != null) {
				project.setProjectDetail(projectDetail);
			}
			if (lso != null) {
				project.setLso(lso);
			}
			if ((projectDetail == null) && (lso == null)) {
				project = new Project();
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Exception in getprojectDetail method in MoveProjectAgent " + e.getMessage());
		}
		return project;

	}

	public int getActivityId(String activityNbr) {

		int activityId = 0;
		try {
			Wrapper db = new Wrapper();

			String sql = "select act_id from activity where act_nbr='" + activityNbr + "'";
			logger.debug("The select Sql of activity id  is " + sql);
			RowSet rs = db.select(sql);
			if (rs.next()) {
				activityId = rs.getInt("act_id");

				logger.debug("get activity id is .. " + activityId);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Exception in getActivityId method in MoveProjectAgent " + e.getMessage());
		}
		return activityId;

	}

	public int getProjectId(String projectNbr) {

		int projectId = 0;
		try {
			Wrapper db = new Wrapper();

			String sql = "select proj_id from project where project_nbr='" + projectNbr + "'";
			logger.debug("The select Sql of project id  is " + sql);
			RowSet rs = db.select(sql);
			if (rs.next()) {
				projectId = rs.getInt("proj_id");

				logger.debug("get project id is .. " + projectId);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Exception in getProjectId method in MoveProjectAgent " + e.getMessage());
		}
		return projectId;

	}

	public int getSubProjectId(String subProjectNbr) {

		int subProjectId = 0;
		try {
			Wrapper db = new Wrapper();

			String sql = "select sproj_id from sub_project where sproj_nbr='" + subProjectNbr + "'";
			logger.debug("The select Sql of sub project id  is " + sql);
			RowSet rs = db.select(sql);
			if (rs.next()) {
				subProjectId = rs.getInt("sproj_id");

				logger.debug("get sub project id is .. " + subProjectId);
			}
			rs.close();
		} catch (Exception e) {
			logger.error("Exception in getSubProjectId method in MoveProjectAgent " + e.getMessage());
		}
		return subProjectId;

	}

	/**
	 * @param Function
	 *            to check empty project
	 * @return
	 * @author Hemavathi
	 * @throws Exception
	 */

	public boolean checkProject(String projectNbr) {
		try {
			Wrapper db = new Wrapper();
			boolean flag = false;
			// getting subproject Details;

			String sql = "select p.proj_id,p.project_nbr, p.name,p.description,val.address,val.citystatezip from project p, v_address_list val, sub_project s where upper(p.project_nbr) =upper('" + projectNbr + "') and (s.proj_id=p.proj_id) and (p.lso_id = val.lso_id)";
			logger.debug("Select sql : " + sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				flag = true;
			}

			rs.close();
			rs = null;
			return flag;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());

		}
		return false;

	}

	/**
	 * @param get
	 *            pname and ptype based on lsoId
	 * @return
	 * @author Hemavathi
	 * @throws Exception
	 */
	public String getLsoForOcc(String lsoId) {
		try {
			Wrapper db = new Wrapper();
			String pName = "";
			String pType = "";
			List pNameList = new ArrayList();
			// getting subproject Details;
			String sql = "select lso_type from v_address_list where lso_id = " + lsoId;
			logger.debug("select sql : " + sql);
			RowSet rs = db.select(sql);
			while (rs.next()) {

				pType = rs.getString("lso_type");

			}
			rs.close();
			return pType;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	/**
	 * @param get
	 *            subProjName
	 * @return
	 * @author Hemavathi
	 * @throws Exception
	 */
	public List getsubProjectName(String projectNbr) {
		try {
			Wrapper db = new Wrapper();
			String SubProjName = "";
			List subProjNameList = new ArrayList();

			String subProjSql = "select lp.DESCRIPTION as DESCRIPTION from lkup_ptype lp left outer join sub_project sp on lp.PTYPE_ID = sp.SPROJ_TYPE left outer join project p on p.proj_id = sp.Proj_id where p.PROJECT_NBR = " + projectNbr;
			logger.debug("Select sql : " + subProjSql);

			RowSet subProjRs = db.select(subProjSql);

			while (subProjRs.next()) {
				SubProjName = subProjRs.getString("DESCRIPTION");
				subProjNameList.add(SubProjName);

			}

			subProjRs.close();
			return subProjNameList;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	/**
	 * @param get
	 *            subProjName
	 * @return
	 * @author Hemavathi
	 * @throws Exception
	 */
	public String getsubProjectNameBySPNo(String subProjectNbr) {
		try {
			Wrapper db = new Wrapper();
			String SubProjName = "";

			String subProjSql = "select lp.DESCRIPTION as DESCRIPTION from lkup_ptype lp left outer join sub_project sp on lp.PTYPE_ID = sp.SPROJ_TYPE  where sp.sproj_nbr = " + subProjectNbr;
			logger.debug("Select sql : " + subProjSql);

			RowSet subProjRs = db.select(subProjSql);

			if (subProjRs.next()) {
				SubProjName = subProjRs.getString("DESCRIPTION");
			}

			subProjRs.close();
			return SubProjName;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	/**
	 * @param get
	 *            subProjName
	 * @return
	 * @author Hemavathi
	 * @throws Exception
	 */
	public String getsubProjectNameBySPId(String subProjectId) {
		try {
			Wrapper db = new Wrapper();
			String SubProjName = "";

			String subProjSql = "select lp.DESCRIPTION as DESCRIPTION from lkup_ptype lp left outer join sub_project sp on lp.PTYPE_ID = sp.SPROJ_TYPE  where sp.SPROJ_ID = " + subProjectId;
			logger.debug("Select sql : " + subProjSql);

			RowSet subProjRs = db.select(subProjSql);

			if (subProjRs.next()) {
				SubProjName = subProjRs.getString("DESCRIPTION");
			}

			subProjRs.close();
			return SubProjName;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	/**
	 * @param get
	 *            pname and ptype based on projNbr
	 * @return
	 * @author Hemavathi
	 * @throws Exception
	 */
	public List getpType(String projectNbr) {
		try {
			Wrapper db = new Wrapper();
			String pName = "";
			String pType = "";
			List pNameList = new ArrayList();
			// getting subproject Details;
			String sql = "select l.lso_type,p.name from project p left outer join v_address_list l on p.lso_id=l.lso_id where p.PROJECT_NBR  =  " + projectNbr;
			logger.debug("select sql : " + sql);
			RowSet rs = db.select(sql);
			while (rs.next()) {

				pType = rs.getString("lso_type");
				pName = rs.getString("name");
				pNameList.add(pType);
				pNameList.add(pName);

			}
			rs.close();
			return pNameList;

		} catch (Exception e) {
			logger.debug("Exception " + e.getMessage());
			return null;
		}
	}

	public int getAddressId(String lsoId) throws AgentException {
		int addressId = 0;
		String GET_ADDRESS_ID = "SELECT ADDR_ID FROM V_ADDRESS_LIST WHERE LSO_ID = " + lsoId;

		try {
			RowSet rs = new Wrapper().select(GET_ADDRESS_ID);

			if (rs != null && rs.next()) {
				addressId = rs.getInt("ADDR_ID");
			}
		} catch (Exception ex) {
			logger.error("Exception in getAddressId", ex);
		}

		return addressId;

	}

	/**
	 * Gets the user object for the given user id.
	 * 
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public User getUser(int userId) throws Exception {
		User user = new User();

		try {
			logger.debug("entered into userAgent with ID" + userId);

			Wrapper db = new Wrapper();
			RowSet userRs = db.select("select * from users where userid = " + userId);

			if (userRs.next()) {
				// user = new User();
				user.setUserId(userRs.getInt("userid"));
				logger.debug(" userid  is set to  " + user.getUserId());
				user.setUsername(userRs.getString("username"));
				logger.debug("  Username  is set to  " + user.getUsername());
				user.setFirstName(userRs.getString("first_name"));
				logger.debug("  FirstName  is set to  " + user.getFirstName());
				user.setLastName(userRs.getString("last_name"));
				logger.debug(" LastName  is set to  " + user.getLastName());
				user.setMiddleInitial(userRs.getString("mi"));
				logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
				user.setEmployeeNumber(userRs.getString("emp_no"));
				logger.debug(" EmployeeNumber  is set to  " + user.getEmployeeNumber());
				user.setTitle(userRs.getString("title"));
				logger.debug(" Title  is set to  " + user.getTitle());
				user.setPassword(userRs.getString("password"));
				// getting the Department of Created User
				int deptId = userRs.getInt("dept_id");
				user.setDepartment(LookupAgent.getDepartment(deptId));
				logger.debug("set department to user");

				Role role = getRole(userRs.getInt("role_id"));
				logger.debug("obtained role object");
				user.setRole(role);
				logger.debug("set role to user  " + user.getRole().getRoleId());
				user.setGroups(getUserGroups(user.getUserId()));
				user.setIsSupervisor(isSupervisor(user.getUserId()));
				logger.debug("set role to user  " + user.getIsSupervisor());
				user.setActive(userRs.getString("active"));
				logger.debug("set active to user  " + user.getActive());
				user.setUserEmail(userRs.getString("email_id"));
				logger.debug("set active to user  " + user.getUserEmail());
				user.setHomePage(userRs.getString("homepage"));
				if (user.getHomePage() == null) {
					user.setHomePage("HOME");
				}
				logger.debug("set homepage of user to  " + user.getHomePage());
			}
			
			
			if (userRs != null)
				userRs.close();
			
			/*if(userId > 0 && new OnlineAgent().getIsOnlineUser(userId)){
				user = new User();
				user = getOnlineDotUserId(user, userId);
				logger.debug("user  " + user.getUsername());
			}*/

		} catch (Exception e) {
			logger.error(" Exception in userAgent -- getUser() method " + e.getMessage());
			throw e;
		}

		if (user == null)
			user = new User();

		return user;
	}

	/**
	 * Gets the final user object for hardwired user administrator
	 * 
	 * @return User
	 * @throws Exception
	 */
	public final User getSuperUser() throws Exception {
		logger.info("getSuperUser()");

		User user = new User();
		user.setUsername("edge");
		user.setFirstName("ELMS");
		user.setLastName("Admin");
		user.setEmployeeNumber("9999");
		user.setTitle("Administrator");
		Department department = new Department(14, "IT", "Information Technology");
		user.setDepartment(department);
		Role role = new Role(3, "Administrator");
		user.setRole(role);
		List groups = new ArrayList();

		Group inspector = new Group(1, "Inspector");
		groups.add(inspector);
		Group manager = new Group(2, "Manager");
		groups.add(manager);
		Group feeMaintenance = new Group(3, "Fee Maintenance");
		groups.add(feeMaintenance);
		Group peopleMaintenance = new Group(4, "People Maintenance");
		groups.add(peopleMaintenance);
		Group projectMaintenance = new Group(5, "Project Maintenance");
		groups.add(projectMaintenance);
		Group supervisor = new Group(6, "Supervisor");
		groups.add(supervisor);
		Group planner = new Group(8, "Planner");
		groups.add(planner);
		Group moveProject = new Group(10, "Move Project");
		groups.add(moveProject);
		Group codeInspector = new Group(12, "Code Inspector");
		groups.add(codeInspector);
		Group userMaintenance = new Group(13, "User Maintenance");
		groups.add(userMaintenance);
		Group addLandMaintenance = new Group(14, "Add Land Maintenance");
		groups.add(addLandMaintenance);
		Group businessTax = new Group(20, "Business Tax");
		groups.add(businessTax);
		Group inspectionManager = new Group(35, "Inspection Manager");
		groups.add(inspectionManager);
		Group pentamationInterface = new Group(21, "Pentamation Interface");
		groups.add(pentamationInterface);
		Group lsoMaintenance = new Group(25, "LSO maintenance");
		groups.add(lsoMaintenance);
		Group conditionsLibraryMaintenance = new Group(26, "Condition Library Maintenance");
		groups.add(conditionsLibraryMaintenance);

		user.setGroups(groups);
		user.setIsSupervisor(true);
		user.setActive("Y");

		return user;
	}

	/**
	 * Gets the user object for the given user name
	 * 
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	public User getUser(String userName) throws Exception {
		int userId = this.getUserId(userName);

		return this.getUser(userId);
	}

	/**
	 * Gets the list of user objects for displaying at various places on the OBC application.
	 * 
	 * @return
	 */
	public List getUsers() {
		List userList = new ArrayList();
		User user = null;
		RowSet rs = null;

		try {
			String sql = "SELECT * FROM USERS ORDER BY USERID";
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				int userId = rs.getInt("USERID");
				String userName = rs.getString("USERNAME");
				String firstName = rs.getString("FIRST_NAME");
				String lastName = rs.getString("LAST_NAME");
				user = new User();
				user.setUserId(userId);
				user.setUsername(userName);
				user.setFirstName(firstName);
				user.setLastName(lastName);
				userList.add(user);
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return userList;
	}

	/**
	 * Gets the list of names of users of OBC application.
	 * 
	 * @return
	 */
	public List getNameList() {
		String sql = "Select userid,first_name,last_name from users order by first_name";
		List nameList = new ArrayList();

		try {
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				Names name = new Names(rs.getInt("userid"), rs.getString("first_name") +" "+rs.getString("last_name"));
				nameList.add(name);
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return nameList;
	}

	/**
	 * Gets the first name list
	 * 
	 * @return
	 */
	public List getFirstNameList() {
		String sql = "Select first_name as firstName from users order by first_name";
		List firstNameList = new ArrayList();
		Names names = null;

		try {
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				names = new Names(StringUtils.properCase(rs.getString("firstName")), "");
				firstNameList.add(names);
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return firstNameList;
	}

	/**
	 * gets the last name list
	 * 
	 * @return
	 */
	public List getLastNameList() {
		String sql = "Select last_name as lastName from users order by last_name";
		List lastNameList = new ArrayList();
		Names names = null;

		try {
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				names = new Names("", StringUtils.properCase(rs.getString("lastName")));
				lastNameList.add(names);
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return lastNameList;
	}

	/**
	 * Gets the user id for a given username
	 * 
	 * @param username
	 * @return
	 * @throws Exception
	 */
	private int getUserId(String username) throws Exception {
		int userId = -1;
		String sql = "SELECT USERID FROM USERS WHERE USERNAME='" + username + "'";
		RowSet rs = new Wrapper().select(sql);

		if (rs.next()) {
			userId = rs.getInt(1);
		} else {
			throw new UserNotFoundException("User Not found");
		}

		return userId;
	}

	/**
	 * gets the role object for a given role id.
	 * 
	 * @param roleId
	 * @return
	 */
	public Role getRole(int roleId) {
		Role role = null;

		try {
			role = new Role();

			String sql = "select * from roles where role_id=" + roleId;
			logger.debug(sql);

			RowSet roleRs = new Wrapper().select(sql);

			if (roleRs.next()) {
				role.setRoleId(roleId);
				role.setDescription(roleRs.getString("description"));
				logger.debug("setting role description");
			}

			if (roleRs != null)
				roleRs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return role;
	}

	/**
	 * Gets the user groups for a given user id.
	 * 
	 * @param userId
	 * @return
	 */
	public List getUserGroups(int userId) {
		List userGroups = new ArrayList();

		try {
			String sql = "select g.* from groups g,user_groups ug where g.group_id = ug.group_id and ug.user_id = " + userId;
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				Group group = new Group();
				group.setGroupId(rs.getInt("group_id"));
				group.setName(rs.getString("name"));
				group.setDescription(rs.getString("description"));
				group.setModuleId(rs.getInt("MODULE_ID"));
				userGroups.add(group);
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return userGroups;
	}

	/**
	 * checks if the given user id is a supervisor user id.
	 * 
	 * @param userId
	 * @return
	 */
	public boolean isSupervisor(int userId) {
		boolean isSupervisor = false;

		try {
			String sql = "select * from user_groups where group_id=6 and user_id=" + userId;
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				isSupervisor = true;
			}

			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return isSupervisor;
	}

	public String userHasEditPrivilege(User user) {
		if (user == null)
			return "NO";

		String editPrivilege = "NO";
		List groups = user.getGroups();

		if (groups == null)
			groups = new ArrayList();

		Iterator iter = groups.iterator();

		while (iter.hasNext()) {
			Group group = (Group) iter.next();

			if (group.getGroupId() == 24) { // Public works department and PW
				// Supervisor group
				editPrivilege = "YES";
			}
		}

		return editPrivilege;
	}

	/**
	 * set home page
	 */
	public void setHomepage(String homepage, int userid) {
		logger.info("setHomepage(" + homepage + "," + userid + ")");

		try {
			String sql = "update users set homepage=" + StringUtils.checkString(homepage) + " where userid=" + userid;
			logger.info(sql);
			new Wrapper().update(sql);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public boolean updateOnlinePassword(User user, String newPassword) {
		boolean result = false;

		try {
			String sql = "update people set password = " + StringUtils.checkString(newPassword) + " where lower(email_addr) =lower('" + user.getFirstName() + "')";

			int i = new Wrapper().update(sql);

			if (i > 0) {
				result = true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return result;
	}

	/**
	 * Get Online User
	 * @param user
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public User getOnlineUser(User user, String username) throws Exception {
		logger.info("getOnlineDotUser(User , " + username + ")");

		try {

			Wrapper db = new Wrapper();
			String sql = "select * from ext_user where lower(ext_username) = lower('" + username + "') order by ext_password";
			logger.info(sql);
			RowSet userRs = db.select(sql);

			if (userRs.next()) {
				if (user == null) {
					user = new User();
					user.setUserId(StringUtils.s2i(StringUtils.d2s(userRs.getDouble("EXT_USER_ID"))));
					user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
					logger.debug(" userid  is set to  " + user.getUserId());
					user.setUsername(userRs.getString("EXT_USERNAME"));
					logger.debug("  Username  is set to  " + user.getUsername());
					user.setFirstName(StringUtils.nullReplaceWithEmpty(userRs.getString("FIRSTNAME")));
					logger.debug("  FirstName  is set to  " + user.getFirstName());
					user.setLastName(StringUtils.nullReplaceWithEmpty(userRs.getString("LASTNAME")));
					logger.debug(" LastName  is set to  " + user.getLastName());
					user.setMiddleInitial("");
					logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
					user.setTitle(userRs.getString("EXT_USER_TYPE"));
					user.setPassword(userRs.getString("ext_password"));
				}
				user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
				user.setIsObc("Y");
				if (user.getIsObc().equals("Y") || (user.getIsDot().equals("Y") && userRs.getString("ACTIVEDOT").equals("Y"))) {
					user.setActive("Y");
				} else {
					user.setActive("N");
				}
			}
			return user;
		} catch (Exception e) {
			logger.warn(" Exception in userAgent -- getUser() method " + e.getMessage());
			throw new UserNotFoundException("user not found " + e.getMessage());
		}
	}

	public boolean updateOnlinePwd(User user, String newPassword) {
		boolean result = false;
		logger.debug("****newPassword =="+newPassword);
		String password = jcrypt.crypt("X6",newPassword);
		try {
			String sql = "update EXT_USER set EXT_PASSWORD = " + StringUtils.checkString(password) + " where lower(ext_username) =lower('" + user.getFirstName() + "')";
			logger.debug("****newPassword =="+sql);
			int i = new Wrapper().update(sql);

			if (i > 0) {
				result = true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return result;
	}

	public boolean oldPwdCheck(User user, String oldPassword) {
		boolean result = false;

		try {
			String sql = "select * from  EXT_USER where  EXT_PASSWORD = " + StringUtils.checkString(oldPassword) + " and lower(ext_username) =lower('" + user.getFirstName() + "')";

			int i = new Wrapper().update(sql);

			if (i > 0) {
				result = true;
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return result;
	}
	
	public User getOnlineDotUserId(User user,int userId) throws Exception {
		logger.info("getOnlineDotUser(User , " + userId + ")");

		try {

			Wrapper db = new Wrapper();
			String sql = "select * from ext_user where EXT_USER_ID="+userId;
			logger.info(sql);
			RowSet userRs = db.select(sql);

			if (userRs.next()) {
					logger.debug("****************************");
					user.setUserId(StringUtils.s2i(StringUtils.d2s(userRs.getDouble("EXT_USER_ID"))));
					user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
					logger.debug(" userid  is set to  " + user.getUserId());
					user.setUsername(userRs.getString("EXT_USERNAME"));
					logger.debug("  Username  is set to  " + user.getUsername());
					user.setFirstName(userRs.getString("FIRSTNAME"));
					logger.debug("  FirstName  is set to  " + user.getFirstName());
					user.setLastName(userRs.getString("LASTNAME"));
					logger.debug(" LastName  is set to  " + user.getLastName());
					user.setMiddleInitial("");
					logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
					user.setTitle(userRs.getString("EXT_USER_TYPE"));
					user.setPassword(userRs.getString("ext_password"));
					user.setTitle("Online");
					int deptId = 0;
					user.setDepartment(LookupAgent.getDepartment(deptId));
					logger.debug("set department to user");
			}
			return user;
		} catch (Exception e) {
			logger.warn(" Exception in userAgent -- getUser() method " + e.getMessage());
			throw new UserNotFoundException("user not found " + e.getMessage());
		}
	}
	
	
	//Notice template
	public boolean deleteWorker(int id,int user) throws Exception {
		logger.info("getEmailTemplates()");
		boolean result = false;
		try{
		String sql = "UPDATE WORKER SET ACTIVE='N',UPDATED= getDate(),UPDATED_BY=" + user + " WHERE ID="+id;
		logger.info(sql);
		result = (new Wrapper().update(sql))>0;
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result;
	}
	
	
	//Notice template
	/**public ArrayList getEmailTemplatesList() throws Exception {
		logger.info("getEmailTemplates()");
		ArrayList a = new ArrayList();
		try{
		String sql = "SELECT * FROM LKUP_NOTICE_TEMPLATE WHERE ACTIVE='Y' ORDER BY TITLE";
		RowSet rs = new Wrapper().select(sql);
		

		while (rs.next()) {
			NoticeTemplateAdminForm noticeTemplateAdminForm = new NoticeTemplateAdminForm();
			noticeTemplateAdminForm.setId(rs.getInt("NOTICE_TEMPLATE_ID"));
			noticeTemplateAdminForm.setNoticeTemplateName(StringUtils.nullReplaceWithEmpty(rs.getString("TITLE")));
			noticeTemplateAdminForm.setActionId(rs.getInt("ACTION_ID"));
			noticeTemplateAdminForm.setNoticeTemplate(StringUtils.nullReplaceWithEmpty(rs.getString("MESSAGE")));
			a.add(noticeTemplateAdminForm);
		}
		if(rs!=null) rs.close();
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return a;
	}**/
	
	public NoticeTemplateAdminForm getNoticeTemplate(int id) throws Exception {
		logger.info("getNoticeTemplate()");
		NoticeTemplateAdminForm noticeTemplateAdminForm = new NoticeTemplateAdminForm();
		try{
		String sql = "SELECT * FROM LKUP_NOTICE_TEMPLATE WHERE  NOTICE_TEMPLATE_ID ="+id;
		//RowSet rs = new Wrapper().select(sql);
		Connection localCon = null;
		Statement statement = null;
		ResultSet rs = null;
		localCon = new Wrapper().getConnection();
		statement = localCon.createStatement();
		rs = statement.executeQuery(sql);
		while (rs.next()) {
			noticeTemplateAdminForm.setId(rs.getInt("NOTICE_TEMPLATE_ID"));
			noticeTemplateAdminForm.setNoticeTemplateName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
			noticeTemplateAdminForm.setNoticeTemplate(StringUtils.nullReplaceWithEmpty(StringUtils.clobToString(rs.getClob("TEMPLATETXT"))));
			noticeTemplateAdminForm.setMessage(noticeTemplateAdminForm.getNoticeTemplate());
			noticeTemplateAdminForm.setActionId(rs.getInt("ACTION_ID"));
			noticeTemplateAdminForm.setNoticeTypeId(rs.getInt("NOTICE_TYPE_ID"));
		}
		if(rs!=null) rs.close();
		if(localCon!=null) localCon.close();
		if(statement!=null) statement.close();
		
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return noticeTemplateAdminForm;
	}
	
	public static NoticeTemplateAdminForm getNoticeTemplate(String action) throws Exception {
		logger.info("getNoticeTemplate()");
		NoticeTemplateAdminForm noticeTemplateAdminForm = new NoticeTemplateAdminForm();
		try{
		String sql = "SELECT * FROM LKUP_NOTICE_TEMPLATE L JOIN LKUP_ACTION A on L.ACTION_ID= A.ID WHERE  A.ACTION_NAME ="+StringUtils.checkString(action);
		logger.info(sql);
		Connection localCon = null;
		Statement statement = null;
		ResultSet rs = null;
		localCon = new Wrapper().getConnection();
		statement = localCon.createStatement();
		rs = statement.executeQuery(sql);
		//RowSet rs = new Wrapper().select(sql);
		while (rs.next()) {
			noticeTemplateAdminForm.setId(rs.getInt("NOTICE_TEMPLATE_ID"));
			noticeTemplateAdminForm.setName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
			noticeTemplateAdminForm.setNoticeTemplate(StringUtils.nullReplaceWithEmpty(StringUtils.clobToString(rs.getClob("TEMPLATETXT"))));
			noticeTemplateAdminForm.setActionId(rs.getInt("ACTION_ID"));
			noticeTemplateAdminForm.setNoticeTypeId(rs.getInt("NOTICE_TYPE_ID"));
		}
		if(rs!=null) rs.close();
		if(localCon!=null) localCon.close();
		if(statement!=null) statement.close();
		
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return noticeTemplateAdminForm;
	}
	

	
	public boolean saveNoticeTemplate(NoticeTemplateAdminForm f,int user) throws Exception {
		logger.info("saveNoticeTemplate()");
		boolean result = false;
		int noticeTemplateId=0;
		
		String noticeTemplate= f.getNoticeTemplate();
		String sql = "";
		
		try{
			Wrapper db = new Wrapper();
			
			noticeTemplateId = db.getNextId("NOTICE_TEMPLATE_ID"); 

			logger.info(noticeTemplateId+"-"+": noticeTemplateId :: -");
			
			sql = "SELECT * FROM LKUP_NOTICE_TEMPLATE WHERE ACTIVE='Y' AND NOTICE_TYPE_ID="+f.getNoticeTypeId();
			logger.info(sql);
			
			//ResultSet rs = null;
			RowSet rs =  db.select(sql);
			//RowSet rs = new Wrapper().select(sql);
			int count=0;
			while (rs.next()) {
				f.setId(rs.getInt("NOTICE_TEMPLATE_ID"));
				count++;
			}
			if(rs!=null) rs.close();
			
			logger.info(count+"-"+": count :: -"+f.getMessage()+"%%f.getNoticeTemplate().length()"+f.getNoticeTemplate().length());
	        if(noticeTemplate ==null){
	          noticeTemplate="";
	        }
	        
			if(count>0)
			{
				if(f.getNoticeTemplate().length() > 3990)  
		        {
	     	     sql = "UPDATE LKUP_NOTICE_TEMPLATE SET NOTICE_TYPE_ID="+f.getNoticeTypeId()+" ,TEMPLATETXT=TO_CLOB('" + f.getNoticeTemplate().substring(0, 3990)+"') || TO_CLOB('" +f.getNoticeTemplate().substring(3991)+"'), NAME=" + StringUtils.checkString(f.getNoticeTemplateName()) + ", UPDATED_BY=" + user + ",UPDATED= CURRENT_TIMESTAMP WHERE ACTIVE='Y' AND NOTICE_TEMPLATE_ID="+f.getId();
		        }else{
		         sql = "UPDATE LKUP_NOTICE_TEMPLATE SET NOTICE_TYPE_ID="+f.getNoticeTypeId()+" ,TEMPLATETXT=to_clob('" + f.getNoticeTemplate() +"'), NAME=" + StringUtils.checkString(f.getNoticeTemplateName()) + ", UPDATED_BY=" + user + ",UPDATED= CURRENT_TIMESTAMP WHERE ACTIVE='Y'  AND NOTICE_TEMPLATE_ID="+f.getId();
		        }	
				
				logger.info(sql);
				result = (new Wrapper().update(sql))>0;
			}else{    
		        if(f.getNoticeTemplate().length() > 3990)
		        {
				  sql = "INSERT INTO LKUP_NOTICE_TEMPLATE (NOTICE_TEMPLATE_ID,NAME,ACTION_ID,NOTICE_TYPE_ID,TEMPLATETXT,ACTIVE,CREATED_BY) VALUES("+noticeTemplateId+","+StringUtils.checkString(f.getNoticeTemplateName())+","+f.getActionId()+","+ f.getNoticeTypeId()+ ",TO_CLOB('"+f.getNoticeTemplate().substring(0, 3990)+"')||TO_CLOB('"+f.getNoticeTemplate().substring(3991)+"'),'Y',"+user+")";
		        }else{
		          sql = "INSERT INTO LKUP_NOTICE_TEMPLATE (NOTICE_TEMPLATE_ID,NAME,ACTION_ID,NOTICE_TYPE_ID,TEMPLATETXT,ACTIVE,CREATED_BY) VALUES("+noticeTemplateId+","+StringUtils.checkString(f.getNoticeTemplateName())+","+f.getActionId()+","+ f.getNoticeTypeId()+ ",TO_CLOB('"+f.getNoticeTemplate()+"'),'Y',"+user+")";
		        }  
				logger.info(sql);
				result = (new Wrapper().insert(sql))>0;
			}
			
		
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result;
		
	}
	  
	public boolean updateNoticeTemplate(NoticeTemplateAdminForm f,int user) throws Exception {
		logger.info("updateNoticeTemplate()");
		boolean result = false;
		String noticeTemplate= f.getNoticeTemplate();
		String sql = "";
		try{
			
		 if(noticeTemplate ==null){
	          noticeTemplate="";
	        }
			     
	        if(f.getNoticeTemplate().length() > 3990)
	        {
     	     sql = "UPDATE LKUP_NOTICE_TEMPLATE SET NOTICE_TYPE_ID="+f.getNoticeTypeId()+" ,TEMPLATETXT=TO_CLOB('" + f.getNoticeTemplate().substring(0, 3990)+"') || TO_CLOB('" +f.getNoticeTemplate().substring(3991)+"'), NAME=" + StringUtils.checkString(f.getNoticeTemplateName()) + ", UPDATED_BY=" + user + ",UPDATED= CURRENT_TIMESTAMP WHERE ACTIVE='Y' AND NOTICE_TEMPLATE_ID="+f.getId();
	        }else{
	         sql = "UPDATE LKUP_NOTICE_TEMPLATE SET NOTICE_TYPE_ID="+f.getNoticeTypeId()+" ,TEMPLATETXT=to_clob('" + f.getNoticeTemplate() +"'), NAME=" + StringUtils.checkString(f.getNoticeTemplateName()) + ", UPDATED_BY=" + user + ",UPDATED= CURRENT_TIMESTAMP WHERE ACTIVE='Y' AND NOTICE_TEMPLATE_ID="+f.getId();
	        }
		logger.info(sql);
		result = (new Wrapper().update(sql))>0;
		
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		return result;
	}
	
	public void deleteNoticeTemplate(int id,int user) throws Exception {
		logger.info("deleteNoticeTemplate()");
		boolean result = false;
		try{
		//String sql = "UPDATE LKUP_NOTICE_TEMPLATE SET ACTIVE='N',UPDATED_BY=" + user + " WHERE NOTICE_TEMPLATE_ID="+id;
			
		String sql="DELETE FROM LKUP_NOTICE_TEMPLATE WHERE  NOTICE_TEMPLATE_ID="+id;
		logger.info(sql);
		result = (new Wrapper().update(sql))>0;
		}catch(Exception e){
			logger.error(e.getMessage());
		}
		
		//return result;
	}
	
	/**
	 * NoticeTemplateAdminForm
	 * 
	 * @return
	 * @throws Exception
	 */
	public NoticeTemplateAdminForm getNoticeTemplates() throws Exception {
		logger.info("getNoticeTemplates()");

		String sql = "SELECT * FROM LKUP_NOTICE_TEMPLATE";
		RowSet rs = new Wrapper().select(sql);
		NoticeTemplateAdminForm noticeTemplateAdminForm = new NoticeTemplateAdminForm();

		while (rs.next()) {
			    noticeTemplateAdminForm.setId(rs.getInt("NOTICE_TEMPLATE_ID"));
			    noticeTemplateAdminForm.setNoticeTemplate(StringUtils.nullReplaceWithEmpty(StringUtils.clobToString(rs.getClob("TEMPLATETXT"))));
				noticeTemplateAdminForm.setNoticeTemplateName(rs.getString("NAME"));
				noticeTemplateAdminForm.setActionId(rs.getInt("ACTION_ID"));
				noticeTemplateAdminForm.setNoticeTypeId(rs.getInt("NOTICE_TYPE_ID"));
		}

		if(rs!=null) rs.close();

		return noticeTemplateAdminForm;
	}
	   
	//Notice template
	
	public ArrayList getNoticeTypes() throws Exception {
		logger.info("Entering getCodeEnforcementTypes()");
		Wrapper db = new Wrapper();
		ArrayList typeList = new ArrayList();
		String GET_CE_TYPES = "SELECT * FROM LKUP_NOTICE_TYPE ORDER BY DESCRIPTION";


		RowSet rs = db.select(GET_CE_TYPES);

		while (rs.next()) {
			CodeEnforcement ce = new CodeEnforcement();
			ce.setCeTypeId(rs.getInt("NOTICE_TYPE_ID"));
			ce.setCategory(rs.getString("CATEGORY"));
			ce.setName(rs.getString("DESCRIPTION"));
			typeList.add(ce);
		}

		if(rs!=null) rs.close();
		
		logger.info("Exiting getCodeEnforcementTypes(" + typeList.size() + ")");
		return typeList;
	}
	
		public ArrayList getNoticeTemplatesList() throws Exception {
			logger.info("getNoticeTemplatesList()");
			ArrayList noticeTemplateList = new ArrayList();
			try{
			String sql = "SELECT * FROM LKUP_NOTICE_TEMPLATE WHERE ACTIVE='Y' ORDER BY NAME";
			Connection localCon = null;
			Statement statement = null;
			ResultSet rs = null;
			localCon = new Wrapper().getConnection();
			statement = localCon.createStatement();
			rs = statement.executeQuery(sql);
			//ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				
				NoticeTemplateAdminForm noticeTemplateAdminForm = new NoticeTemplateAdminForm();
				noticeTemplateAdminForm.setId(rs.getInt("NOTICE_TEMPLATE_ID"));
				noticeTemplateAdminForm.setNoticeTemplateName(StringUtils.nullReplaceWithEmpty(rs.getString("NAME")));
				noticeTemplateAdminForm.setActionId(rs.getInt("ACTION_ID"));
				//logger.debug(StringUtils.clobToString(rs.getClob("TEMPLATETXT"))+"  *** Clob data");
				noticeTemplateAdminForm.setNoticeTemplate(StringUtils.nullReplaceWithEmpty(StringUtils.clobToString(rs.getClob("TEMPLATETXT"))));
				noticeTemplateAdminForm.setMessage(noticeTemplateAdminForm.getNoticeTemplate());
				noticeTemplateAdminForm.setNoticeTypeId(rs.getInt("NOTICE_TYPE_ID"));
				noticeTemplateList.add(noticeTemplateAdminForm);    
			}
			
			logger.debug("size "+noticeTemplateList.size());
			if(rs!=null) rs.close();
			if(localCon!=null) localCon.close();
			if(statement!=null) statement.close();
			
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			return noticeTemplateList;
		}
		
		/*public List<DisplayItem> getNoticeTemplateFieldsList() throws Exception {
			logger.info("getNoticeTemplatesList()");
			List<DisplayItem> noticeTemplateFieldsList = new ArrayList<DisplayItem>();
			try{
			String sql = "SELECT * FROM LKUP_NOTICE_FIELDS ORDER BY NOTICE_FIELDS_ID";   
			Connection localCon = null;
			Statement statement = null;
			ResultSet rs = null;
			localCon = new Wrapper().getConnection();
			statement = localCon.createStatement();
			rs = statement.executeQuery(sql);
			//ResultSet rs = new Wrapper().select(sql);
			NoticeTemplateAdminForm noticeTemplateAdminForm = new NoticeTemplateAdminForm();
			while (rs.next()) {   				
				DisplayItem displayItem = new DisplayItem();
				displayItem.setFieldOne(rs.getInt("NOTICE_FIELDS_ID")+"");
				displayItem.setFieldTwo(StringUtils.nullReplaceWithEmpty(rs.getString("FIELD_ALIAS")).replace("#",""));
				displayItem.setFieldThree(rs.getString("FIELD_NAME"));
				noticeTemplateFieldsList.add(displayItem);
				   
			}
			
			logger.debug("size "+noticeTemplateFieldsList.size());
			if(rs!=null) rs.close();
			if(localCon!=null) localCon.close();
			if(statement!=null) statement.close();
			
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			return noticeTemplateFieldsList;
		}          */
		
		/*public boolean updateNoticeTemplateFields(NoticeTemplateAdminForm frm) throws Exception {
			logger.info("updateNoticeTemplateFields()");
			boolean result = false;
				String sql = "";  
			try{
				
			sql = "UPDATE LKUP_NOTICE_FIELDS SET FIELD_NAME='"+frm.getFieldDescription()+"' WHERE NOTICE_FIELDS_ID="+frm.getFieldId();
		    
			logger.info(sql);
			result = (new Wrapper().update(sql))>0;
			
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return result;
		}*/
		
		
		/**
		 * 
		 * @return
		 * @throws Exception
		 * 
		 * bad method not to used Sunil
		 */
		
		/*public static List getNoticeFieldList() throws Exception {
			logger.debug("getNoticeFieldList()");
			List views = new ArrayList();
			String sql="";
			try {
				sql = "SELECT * FROM V_NOTICE_PRINT WHERE ROWNUM <2";
				logger.debug(sql);
				Connection localCon = null;
				Statement statement = null;
				ResultSet rs = null;
				localCon = new Wrapper().getConnection();
				statement = localCon.createStatement();
				rs = statement.executeQuery(sql);
				if (rs == null) {
				      return views;
				    }
				    // get result set meta data
				    ResultSetMetaData rsMetaData = rs.getMetaData();
				    int numberOfColumns = rsMetaData.getColumnCount();

				    // get the column names; column indexes start from 1
				    for (int i = 1; i < numberOfColumns + 1; i++) {
				      String columnName = rsMetaData.getColumnName(i);
				      // Get the name of the column's table name
				      String tableName = rsMetaData.getTableName(i);
				      logger.info(columnName+"---V_NOTICE_PRINT-----"+tableName);
				      DisplayItem d = new DisplayItem();
				      d.setFieldOne(columnName);
				      
				      views.add(d);
				    }
				    
				    sql = "SELECT * FROM V_CUSTOM_FIELDS_NOTICES WHERE ROWNUM <2";
					logger.debug(sql);
					
					localCon = null;
					statement = null;
					rs = null;
					localCon = new Wrapper().getConnection();
					statement = localCon.createStatement();
					rs = statement.executeQuery(sql);
					if (rs == null) {
					      return views;
					    }
    					// get result set meta data
					    rsMetaData = rs.getMetaData();
					    numberOfColumns = rsMetaData.getColumnCount();

					    // get the column names; column indexes start from 1
					    for (int i = 1; i < numberOfColumns + 1; i++) {
					      String columnName = rsMetaData.getColumnName(i);
					      // Get the name of the column's table name
					      String tableName = rsMetaData.getTableName(i);
					      DisplayItem d = new DisplayItem();
					      d.setFieldOne(columnName);
					      logger.info(columnName+"---V_CUSTOM_FIELDS_NOTICES-----"+tableName);
					      views.add(d);
					    }   
				    
				    DisplayItem d = new DisplayItem();
				    d.setFieldOne("FEE_AMOUNT");
				    views.add(d);
				

				while (rs.next()) {
					DisplayItem d = new DisplayItem();
					String value = rs.getString("FIELD_ALIAS");
					String name = rs.getString("FIELD_NAME");
					//String option = rs.getString("ACTION_OPTION_1");
					String id = rs.getString("NOTICE_FIELDS_ID");
					d.setFieldOne(id);
					d.setFieldTwo(name);
					d.setFieldThree(value);
					
					views.add(d);
				}   
				 
			    if(rs!=null) rs.close();
				if(localCon!=null) localCon.close();
				if(statement!=null) statement.close();
				return views;
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}
		}*/

		
		//changed by sunil 
		public static List getNoticeFieldList() throws Exception {
			logger.debug("getNoticeFieldList()");
			List views = new ArrayList();
			String sql="";
			try {
				
				Wrapper db = new Wrapper();
				//sql = "SELECT * FROM V_NOTICE_PRINT WHERE ROWNUM <2";
				StringBuilder sb = new StringBuilder();
				sb.append(" SELECT table_name view_name,column_name, data_type FROM user_tab_columns  WHERE table_name = 'V_NOTICE_PRINT'   ");
				sb.append(" union ");
				sb.append(" SELECT table_name view_name,column_name, data_type FROM user_tab_columns  WHERE table_name = 'V_CUSTOM_FIELDS_NOTICES'  ORDER BY column_name ");
				
				sql = sb.toString();
				logger.debug(sql);
				RowSet rs = db.select(sql);

				while(rs.next()){
			      String columnName = rs.getString("COLUMN_NAME");
			      DisplayItem d = new DisplayItem();
			      d.setFieldOne(columnName);
			      views.add(d);
			    }
			    
			    if(rs!=null) rs.close();
			 	    
			    DisplayItem d = new DisplayItem();
			    d.setFieldOne("FEE_AMOUNT");
			    views.add(d);
				 
			
				
			} catch (Exception e) {
				logger.error(e.getMessage());
				views = new ArrayList();
				throw e;
			}
			return views;
		}
		public List getAddressType(AddressTypeAdminForm addressTypeAdminForm) {
			logger.info("getAddressType(addressTypeAdminForm)");
			List addressType = new ArrayList();

			try {
				String sql = "";

				
				sql = ((addressTypeAdminForm.getAddressType() == null) || addressTypeAdminForm.getAddressType().equals("")) ? sql : (sql + " UPPER(ADDRESS_TYPE) LIKE '%" + StringUtils.nullReplaceWithEmpty(addressTypeAdminForm.getAddressType().toUpperCase()) + "%' AND");

				if (addressTypeAdminForm.isBtFlag() == true)
					sql = sql + " FLAG_BT = 'Y' AND";

				if (addressTypeAdminForm.isBlFlag() == true)
					sql = sql + " FLAG_BL = 'Y' AND";

			
				if ((sql != null) && !("").equalsIgnoreCase(sql)) {
					sql = sql.substring(0, sql.length() - 4); // to remove the trailing
					sql = " WHERE " + sql;
				}
				sql = sql+" order by id asc";
			
				sql = "select * from LKUP_ADDRESS_TYPE " + sql + " ";
				
								
				logger.debug(sql);

				RowSet rs = new Wrapper().select(sql);

				while (rs.next()) {
					DisplayItem items = new DisplayItem();
					items.setFieldOne(StringUtils.nullReplaceWithEmpty(rs.getString("ID")));
					items.setFieldTwo(StringUtils.nullReplaceWithEmpty(rs.getString("ADDRESS_TYPE")));
					if(rs.getString("FLAG_BT")!=null && rs.getString("FLAG_BT").equalsIgnoreCase("Y")){
						items.setFieldThree("Yes");	
					}else{
						items.setFieldThree("No");
					}
					if(rs.getString("FLAG_BL")!=null && rs.getString("FLAG_BL").equalsIgnoreCase("Y")){
						items.setFieldFour("Yes");	
					}else{
						items.setFieldFour("No");
					}
						if(addressTypeRemovable(rs.getString("ID"))){
							items.setFieldFive("Y");
						}else{
							items.setFieldFive("N");
						}
					addressType.add(items);
				} // end of while loop

				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return addressType;
		}

		public int saveAddressType(String id, String addressType,boolean btFlag, boolean blFlag) {
			logger.info("saveAddressType(" + id + "," + addressType + ", " + btFlag + "," + blFlag + ")");

			Wrapper db = new Wrapper();
			int result = 0;
			String sql = "";

			try {
				sql = "SELECT * FROM LKUP_ADDRESS_TYPE WHERE ID=" + StringUtils.checkString(id);
				logger.info(sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					sql = "UPDATE LKUP_ADDRESS_TYPE SET ADDRESS_TYPE=" + StringUtils.checkString(addressType) + ", FLAG_BT=" + StringUtils.checkString(StringUtils.b2s(btFlag)) + ", FLAG_BL=" +  StringUtils.checkString(StringUtils.b2s(blFlag)) + " WHERE  ID = " + id;
					logger.info(sql);
					result = 2;
					db.update(sql);
				} else {

					sql = "INSERT INTO LKUP_ADDRESS_TYPE (ID,ADDRESS_TYPE,FLAG_BT,FLAG_BL) VALUES (";
					sql = sql + "(select max(ID)+1 AS ID from LKUP_ADDRESS_TYPE)";
					sql = sql + ",";
					sql = sql + StringUtils.checkString(addressType);
					sql = sql + ",";
					sql = sql + StringUtils.checkString(StringUtils.b2s(btFlag));
					sql = sql + ",";
					sql = sql + StringUtils.checkString(StringUtils.b2s(blFlag));
					sql = sql + ")";
					result = 1;
					db.insert(sql);
					logger.info("sql :: " + sql);

				}
			} catch (Exception e) {
				result = -1;
				logger.error("Error in saveActivityStatus() method" + e.getMessage());
			}
			return result;
		}
		public AddressTypeAdminForm getAddressType(int id, AddressTypeAdminForm addressTypeAdminForm) {
			logger.info("getAddressType(" + id + ")");
			RowSet rs = null;
			try {
				String sql = "SELECT * FROM LKUP_ADDRESS_TYPE WHERE ID=" + id;
				logger.info(sql);

				 rs = new Wrapper().select(sql);

				if (rs.next()) {
					addressTypeAdminForm.setId(rs.getString("ID"));
					addressTypeAdminForm.setAddressType(rs.getString("ADDRESS_TYPE"));
					addressTypeAdminForm.setBlFlag(StringUtils.s2b(rs.getString("FLAG_BL")));
					addressTypeAdminForm.setBtFlag(StringUtils.s2b(rs.getString("FLAG_BT")));
			
				}

			} catch (Exception e) {
				logger.error("Error in get activity type " + e.getMessage());
				e.printStackTrace();
			}finally {
				
					try {
						if(rs!=null){
						rs.close();
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
			}

			return addressTypeAdminForm;
		}
		public boolean addressTypeRemovable(String id) {
			String sql = "select count(*) as count from MULTI_ADDRESS  where ADDRESS_TYPE_ID='" + id + "'";
			boolean removable = false;

			try {
				RowSet rs = new Wrapper().select(sql);

				if (rs.next()) {
					if (rs.getInt("COUNT") == 0) {
						removable = true;
					} else {
						removable = false;
					}
				}

				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Sql error is addressTypeRemovable() " + e.getMessage());
			} catch (Exception e) {
				logger.error("General error is addressTypeRemovable() " + e.getMessage());
			}

			// logger.debug("removable:" + removable);

			return removable;
		}
		
		public int deleteAddressType(String id) {
			logger.debug("Entered into deleteAddressType..."+id);
			int result = 0;
			String sql = "";

			String[] idArray = {};

			idArray = id.split(",");
			logger.debug("id List " + idArray.length);
			for (int i = 0; i < idArray.length; i++) {
				sql = "DELETE FROM LKUP_ADDRESS_TYPE WHERE ID=" + idArray[i];
				logger.info(sql);
				result = 3;
				try {
					new Wrapper().update(sql);
				} catch (Exception e) {
					result = -1;
					logger.error("General error is deleteAddressType() " + e.getMessage());
				}
			}

			return result;
		}
		
		public int checkAddressType(String addressType) {
			String sql = "select COUNT(*) AS COUNT from LKUP_ADDRESS_TYPE  WHERE  UPPER(ADDRESS_TYPE) IN ('"+addressType.toUpperCase()+"')";
			int id =0;

			try {
				RowSet rs = new Wrapper().select(sql);

				if (rs.next()) {
					if (rs.getInt("COUNT") == 0) {
						id=0;
					} else {
						id=3;
					}
				}

				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Sql error is checkAddressType() " + e.getMessage());
			} catch (Exception e) {
				logger.error("General error is checkAddressType() " + e.getMessage());
			}


			return id;
		}

	/**
	     * gets the List of EmailTemplateTypeAdmin forms for
	     * @param emailTemplateTypeAdminForm
	     * @return List
		 * @throws Exception 
	     */
		public List getAttachmentTypes(AttachmentTypeForm attachmentTypeForm) throws Exception {
			/*StringBuilder sql = new StringBuilder();
			//String type = StringUtils.nullReplaceWithBlank(emailTemplateTypeAdminForm.getEmailTemplateType()).trim();
			String desc = StringUtils.nullReplaceWithBlank(attachmentTypeForm.getAttachmentTypeDesc());	
			logger.debug("desc = "+desc);
        	String documentUrl = StringUtils.nullReplaceWithBlank(attachmentTypeForm.getDocumentUrl());
        	logger.debug("documentUrl = "+documentUrl);
        	String bmcPopupWindowUrl = StringUtils.nullReplaceWithBlank(attachmentTypeForm.getBmcPopupWindowUrl());
        	logger.debug("bmcPopupWindowUrl = "+bmcPopupWindowUrl);
        	String bmcDescription = StringUtils.nullReplaceWithBlank(attachmentTypeForm.getBmcDescription());
        	logger.debug("bmcDescription = "+bmcDescription);
        	String comments = StringUtils.nullReplaceWithBlank(attachmentTypeForm.getComments());
        	logger.debug("comments = "+comments);
        	String uploadOrDownloadType = StringUtils.nullReplaceWithBlank(attachmentTypeForm.getUploadOrDownloadType());
        	logger.debug("uploadOrDownloadType = "+uploadOrDownloadType);
        	String departments[] = attachmentTypeForm.getDepartment();
        	String levels[] = attachmentTypeForm.getLevel();
        	
			List attachmentTypeList = new ArrayList();
			List<Department> allDepartments = LookupAgent.getDepartmentList();            
			AttachmentTypeAdmin attachmentTypeAdmin;
			RowSet rs = null;
	        try {
	        	
	        	sql.append("SELECT * FROM LKUP_ATTACHMENT_TYPE ");
	        	
	        	if(!"".equals(desc) || !"".equals(documentUrl) ||!"".equals(bmcPopupWindowUrl) || !"".equals(bmcDescription) || !"".equals(comments)||
	        			!"".equals(uploadOrDownloadType) || departments.length > 0 || levels.length > 0) {
	        		sql.append(" WHERE");
	        	}
	        	
	        	if(!"".equals(desc)){
	        		sql.append(" DESCRIPTION LIKE '%" +desc+ "%'");        		
	        	}
	        	if(!"".equals(documentUrl)){
	        		sql.append(" AND DOCUMENT_URL LIKE '%" +documentUrl+ "%'");        		
	        	}
	        	if(!"".equals(bmcPopupWindowUrl)){
	        		sql.append(" AND BMC_POPUP_WINDOW_URL LIKE '%" +bmcPopupWindowUrl+ "%'");        		
	        	}
	        	if(!"".equals(bmcDescription)){
	        		sql.append(" AND BMC_DESCRIPTION LIKE '%" +bmcDescription+ "%'");        		
	        	}
	        	if(!"".equals(comments)){
	        		sql.append(" AND COMMENTS LIKE '%" +comments+ "%'");        		
	        	}
	        	if(!"".equals(uploadOrDownloadType)){
	        		sql.append(" AND UPLOAD_OR_DOWNLOAD_TYPE LIKE '%" +uploadOrDownloadType+ "%'");        		
	        	}
	        	if(departments.length > 0) {
	        		for(int i = 0; i < departments.length; i++) {
	        			String deptId = departments[i];
	        			Department dept = LookupAgent.getDepartment(Integer.parseInt(deptId));
	        			sql.append(" AND "+dept.getDepartmentCode()+"_FLAG = 'Y'");
	        		}
	        	}
	        	if(levels.length > 0) {
	        		for(int i = 0; i < levels.length; i++) {
	        			String level = levels[i];
	        			if(level.equals("L")) {
	        				sql.append(" AND LAND_FLAG = 'Y'");
	        			}else if(level.equals("S")) {
	        				sql.append(" AND STRUCTURE_FLAG = 'Y'");
	        			}else if(level.equals("O")) {
	        				sql.append(" AND OCCUPANCY_FLAG = 'Y'");
	        			}else if(level.equals("P")) {
	        				sql.append(" AND PROJECT_FLAG = 'Y'");
	        			}else if(level.equals("Q")) {
	        				sql.append(" AND SUBPROJECT_FLAG = 'Y'");
	        			}
	        		}
	        	}
	        	sql.append("ORDER BY DESCRIPTION ASC");*/ 
			
			    List attachmentTypeList = new ArrayList();
			    List<Department> allDepartments = LookupAgent.getDepartmentList();         
				AttachmentTypeAdmin attachmentTypeAdmin;
				RowSet rs = null;
			try {
				String sql = "select att.*,at.ID,lat.TYPE_ID,lat.DESCRIPTION as actTypeDesc from REF_ACT_ATTACHMENT_TYPE at join LKUP_ACT_TYPE lat on lat.TYPE_ID=at.LKUP_ACT_TYPE_ID " + 
						" join LKUP_ATTACHMENT_TYPE att on att.TYPE_ID = at.LKUP_ATTACHMENT_TYPE_ID";
			
	            logger.debug("Sql is " + sql);                     
	            
	            rs = new Wrapper().select(sql);
	            while (rs.next()) {
	            	attachmentTypeAdmin= new AttachmentTypeAdmin();
	            	List<String> departmentsList = new ArrayList<String>();  
	                List<String> levelsList = new ArrayList<String>();
	                
	                List<String> departmentIds = new ArrayList<String>();  
	                List<String> levelIds = new ArrayList<String>();
	                
	            	attachmentTypeAdmin.setAttachmentTypeId(rs.getInt("TYPE_ID"));
	            	attachmentTypeAdmin.setAttachmentTypeDesc((rs.getString("DESCRIPTION") != null ? rs.getString("DESCRIPTION") : "").trim());
	            	attachmentTypeAdmin.setDocumentUrl((rs.getString("DOCUMENT_URL") != null ? rs.getString("DOCUMENT_URL") : "").trim());
	            	attachmentTypeAdmin.setBmcPopupWindowUrl(rs.getString("BMC_DESCRIPTION"));
	            	attachmentTypeAdmin.setUploadOrDownloadType(rs.getString("UPLOAD_OR_DOWNLOAD_TYPE"));
	            	attachmentTypeAdmin.setComments(rs.getString("COMMENTS"));
	            	attachmentTypeAdmin.setBmcdescription(rs.getString("BMC_POPUP_WINDOW_URL"));	 
	            	attachmentTypeAdmin.setRefId(rs.getInt("ID"));
	            	attachmentTypeAdmin.setActTypeDesc(rs.getString("actTypeDesc"));
	            	
	            	
	            	if(rs.getString("AS_FLAG") != null && rs.getString("AS_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "AS"));
	            		departmentIds.add(getDepartmentId(allDepartments, "AS"));  //For showing in edit page
	            	}
	            	if(rs.getString("FD_FLAG") != null && rs.getString("FD_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "FD"));
	            		departmentIds.add(getDepartmentId(allDepartments, "FD"));
	            	}
	            	if(rs.getString("CC_FLAG") != null && rs.getString("CC_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "CC"));
	            		departmentIds.add(getDepartmentId(allDepartments, "CC"));
	            	}
	            	if(rs.getString("FS_FLAG") != null && rs.getString("FS_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "FS"));
	            		departmentIds.add(getDepartmentId(allDepartments, "FS"));
	            	}
	            	if(rs.getString("HL_FLAG") != null && rs.getString("HL_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "HL"));
	            		departmentIds.add(getDepartmentId(allDepartments, "HL"));
	            	}
	            	if(rs.getString("BS_FLAG") != null && rs.getString("BS_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "BS"));
	            		departmentIds.add(getDepartmentId(allDepartments, "BS"));
	            	}
	            	if(rs.getString("BW_FLAG") != null && rs.getString("BW_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "BW"));
	            		departmentIds.add(getDepartmentId(allDepartments, "BW"));
	            	}
	            	if(rs.getString("PW_FLAG") != null && rs.getString("PW_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "PW"));
	            		departmentIds.add(getDepartmentId(allDepartments, "PW"));
	            	}
	            	if(rs.getString("PR_FLAG") != null && rs.getString("PR_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "PR"));
	            		departmentIds.add(getDepartmentId(allDepartments, "PR"));
	            	}
	            	if(rs.getString("LS_FLAG") != null && rs.getString("LS_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "LS"));
	            		departmentIds.add(getDepartmentId(allDepartments, "LS"));
	            	}
	            	if(rs.getString("IT_FLAG") != null && rs.getString("IT_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "IT"));
	            		departmentIds.add(getDepartmentId(allDepartments, "IT"));
	            	}
	            	if(rs.getString("MS_FLAG") != null && rs.getString("MS_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "MS"));
	            		departmentIds.add(getDepartmentId(allDepartments, "MS"));
	            	}
	            	if(rs.getString("PD_FLAG") != null && rs.getString("PD_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "PD"));
	            		departmentIds.add(getDepartmentId(allDepartments, "PD"));
	            	}
	            	if(rs.getString("PL_FLAG") != null && rs.getString("PL_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "PL"));
	            		departmentIds.add(getDepartmentId(allDepartments, "PL"));
	            	}
	            	if(rs.getString("LC_FLAG") != null && rs.getString("LC_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "LC"));
	            		departmentIds.add(getDepartmentId(allDepartments, "LC"));
	            	}
	            	if(rs.getString("CA_FLAG") != null && rs.getString("CA_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "CA"));
	            		departmentIds.add(getDepartmentId(allDepartments, "CA"));
	            	}
	            	if(rs.getString("PK_FLAG") != null && rs.getString("PK_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "PK"));
	            		departmentIds.add(getDepartmentId(allDepartments, "PK"));
	            	}
	            	if(rs.getString("HD_FLAG") != null && rs.getString("HD_FLAG").equals("Y")) {
	            		departmentsList.add(getDepartmentName(allDepartments, "HD"));
	            		departmentIds.add(getDepartmentId(allDepartments, "HD"));
	            	}	
	            	
	            	String[] depts = Arrays.copyOf(departmentsList.toArray(), departmentsList.toArray().length, String[].class);
	            	String deptValue = Arrays.toString(depts);	            	
	            	attachmentTypeAdmin.setDepartment(deptValue.replace("[", "").replace("]", ""));
	            	
	            	String[] deptIds = Arrays.copyOf(departmentIds.toArray(), departmentIds.toArray().length, String[].class);
	            	String deptIdValues = Arrays.toString(deptIds);	            	
	            	//attachmentTypeAdmin.setDepartmentIds(deptIdValues.replace("[", "").replace("]", ""));
	            	attachmentTypeAdmin.setDepartmentIds(deptIdValues);
	            	
	            	if(rs.getString("LAND_FLAG") != null && rs.getString("LAND_FLAG").equals("Y")) {
	            		levelsList.add("Land");
	            		levelIds.add("L");
	            	}
	            	if(rs.getString("STRUCTURE_FLAG") != null && rs.getString("STRUCTURE_FLAG").equals("Y")){
	            		levelsList.add("Structure");
	            		levelIds.add("S");
	            	}
	            	if(rs.getString("OCCUPANCY_FLAG") != null && rs.getString("OCCUPANCY_FLAG").equals("Y")){
	            		levelsList.add("Occupancy");
	            		levelIds.add("O");
	            	}
	            	if(rs.getString("PROJECT_FLAG") != null && rs.getString("PROJECT_FLAG").equals("Y")){
	            		levelsList.add("Project");
	            		levelIds.add("P");
	            	}
	            	if(rs.getString("SUBPROJECT_FLAG") != null && rs.getString("SUBPROJECT_FLAG").equals("Y")){
	            		levelsList.add("Sub Project");
	            		levelIds.add("Q");
	            	}
	            	
	            	String[] levelArray = Arrays.copyOf(levelsList.toArray(), levelsList.toArray().length, String[].class);	            	
	            	String levelValue = Arrays.toString(levelArray);	            	
	            	attachmentTypeAdmin.setLevel(levelValue.replace("[", "").replace("]", ""));
	            	
	            	String[] levelId = Arrays.copyOf(levelIds.toArray(), levelIds.toArray().length, String[].class);	            	
	            	String levelIdsValue = Arrays.toString(levelId);	            	
	            	//attachmentTypeAdmin.setLevelIds(levelIdsValue.replace("[", "").replace("]", ""));
	            	attachmentTypeAdmin.setLevelIds(levelIdsValue);
	            	
	            	attachmentTypeList.add(attachmentTypeAdmin);
	            }
	        	
	        }catch (Exception e) {
	            logger.error(e.getMessage());
	        }finally {
	        	try {
	        		if(rs != null) {
	        			rs.close();
	        		}
	        	}catch(Exception e) {
	        		
	        	}
	        }
	        return attachmentTypeList;
		}

    private String getDepartmentName(List<Department> departments, String deptCode) {
			String deptDescription = "";
			for(int i = 0; i < departments.size(); i++) {
				Department dept = departments.get(i);
				if(dept.getDepartmentCode().equals(deptCode)) {
					deptDescription = dept.getDescription();
				}
			}
			return deptDescription;			
		}	
    
    private String getDepartmentId(List<Department> departments, String deptCode) {
			String deptId = "";
			for(int i = 0; i < departments.size(); i++) {
				Department dept = departments.get(i);
				if(dept.getDepartmentCode().equals(deptCode)) {
					deptId = String.valueOf(dept.getDepartmentId());
				}
			}
			return deptId;			
		}
    
    public int saveAttachmentType(AttachmentTypeForm attachmentTypeForm) {
			Wrapper db = new Wrapper();
			int result = 0;
			
			String[] deptIds = attachmentTypeForm.getDepartment();
			logger.debug("deptIds = "+Arrays.toString(deptIds));
			String[] levels = attachmentTypeForm.getLevel();
			logger.debug("levels = "+Arrays.toString(levels));
			String onlineFlag = attachmentTypeForm.getOnlineFlag();
			logger.debug("onlineFlag = "+onlineFlag);
			String uploadOrDownloadType = attachmentTypeForm.getUploadOrDownloadType();

	        try {
	            StringBuilder sb = null;
	            if (attachmentTypeForm.getAttachmentTypeId() > 0) {
	            	
	            	String attachmentTypeDesc = "";
	            	if(attachmentTypeForm.getOther() != null && (attachmentTypeForm.getOther()).equalsIgnoreCase("on")){
	            		attachmentTypeDesc = attachmentTypeForm.getAttachmentDesc();
	            	}else {
	            		attachmentTypeDesc = getAttachmentTypeDesc(attachmentTypeForm.getAttachmentTypeDesc());
	            	}
	            	
	            	sb = new StringBuilder("UPDATE LKUP_ATTACHMENT_TYPE SET DESCRIPTION =");
	                sb.append(StringUtils.checkString(attachmentTypeDesc));
	                sb.append(", DOCUMENT_URL =");
	                sb.append(StringUtils.checkString(attachmentTypeForm.getDocumentUrl()));
	                sb.append(", BMC_POPUP_WINDOW_URL =");
	                sb.append(StringUtils.checkString(attachmentTypeForm.getBmcPopupWindowUrl()));
	                sb.append(", BMC_DESCRIPTION =");
	                sb.append(StringUtils.checkString(attachmentTypeForm.getBmcDescription()));
	                sb.append(", COMMENTS =");
	                sb.append(StringUtils.checkString(attachmentTypeForm.getComments()));
	                sb.append(", UPLOAD_OR_DOWNLOAD_TYPE =");
	                sb.append(StringUtils.checkString(attachmentTypeForm.getUploadOrDownloadType()));	               
	                sb.append(", ONLINE_AVAILABLE =");
	                if(onlineFlag.equals("on")) {
	                	sb.append("'Y'");
	                }else {
	                	sb.append("'N'");
	                }       
	                
	                if(deptIds.length > 0 && !deptIds[0].equals("")) {
	        	        for(int i = 0; i < deptIds.length; i++) {	        	        	
	        	        	String deptId = deptIds[i];
	        	        	Department dept = LookupAgent.getDepartment(Integer.parseInt(deptId));
	        	        	sb.append(", ").append(dept.getDepartmentCode()+"_FLAG = 'Y'");
	        	        }
	        	    }
	                if(levels.length > 0 && !levels[0].equals("")) {
	            		for(int i=0; i<levels.length; i++) {
	            			String level = levels[i];
	            			if(level.equals("L")) {
	            				sb.append(", ").append("LAND_FLAG = 'Y'");
	            			}else if(level.equals("S")) {
	            				sb.append(", ").append("STRUCTURE_FLAG = 'Y'");
	            			}else if(level.equals("O")) {
	            				sb.append(", ").append("OCCUPANCY_FLAG = 'Y'");
	            			}else if(level.equals("P")) {
	            				sb.append(", ").append("PROJECT_FLAG = 'Y'");
	            			}else if(level.equals("Q")) {
	            				sb.append(", ").append("SUBPROJECT_FLAG = 'Y'");
	            			}
	            		}
	            	}
	                sb.append(" WHERE TYPE_ID =");
	                sb.append(attachmentTypeForm.getAttachmentTypeId());
	                
	                result = db.update(sb.toString());
	                if(result > 0) {		            	
		            	return attachmentTypeForm.getAttachmentTypeId();
		            }else {
		            	return 0;
		            } 
	            }else {
	            	sb = new StringBuilder("insert into LKUP_ATTACHMENT_TYPE(TYPE_ID,DESCRIPTION, DOCUMENT_URL, BMC_POPUP_WINDOW_URL, BMC_DESCRIPTION, COMMENTS,UPLOAD_OR_DOWNLOAD_TYPE,ONLINE_AVAILABLE");
	            	
	            	int count = 0;		
	            	if(deptIds.length > 0) {
	        	        for(int i = 0; i < deptIds.length; i++) {
	        	        	count++;
	        	        	String deptId = deptIds[i];
	        	        	Department dept = LookupAgent.getDepartment(Integer.parseInt(deptId));
	        	        	sb.append(", ").append(dept.getDepartmentCode()+"_FLAG");
	        	        }
	        	    }	
	            	
	            	if(levels.length > 0) {
	            		for(int i=0; i<levels.length; i++) {
	            			String level = levels[i];
	            			if(level.equals("L")) {
	            				sb.append(", ").append("LAND_FLAG");
	            			}else if(level.equals("S")) {
	            				sb.append(", ").append("STRUCTURE_FLAG");
	            			}else if(level.equals("O")) {
	            				sb.append(", ").append("OCCUPANCY_FLAG");
	            			}else if(level.equals("P")) {
	            				sb.append(", ").append("PROJECT_FLAG");
	            			}else if(level.equals("Q")) {
	            				sb.append(", ").append("SUBPROJECT_FLAG");
	            			}
	            		}
	            	}
	            			
	            	sb.append(") values((SELECT MAX(TYPE_ID)+1 FROM LKUP_ATTACHMENT_TYPE),");
	            	
	            	String attachmentTypeDesc = "";
	            	if(attachmentTypeForm.getOther() != null && (attachmentTypeForm.getOther()).equalsIgnoreCase("on")){
	            		attachmentTypeDesc = attachmentTypeForm.getAttachmentDesc();
	            	}else {
	            		attachmentTypeDesc = getAttachmentTypeDesc(attachmentTypeForm.getAttachmentTypeDesc());
	            	}	            	
	            	
	            	sb.append(StringUtils.checkString(attachmentTypeDesc)).append(",");	            	
	            	sb.append(StringUtils.checkString(attachmentTypeForm.getDocumentUrl())).append(",");
	            	sb.append(StringUtils.checkString(attachmentTypeForm.getBmcPopupWindowUrl())).append(",");
	            	sb.append(StringUtils.checkString(attachmentTypeForm.getBmcDescription())).append(",");
	            	sb.append(StringUtils.checkString(attachmentTypeForm.getComments())).append(",");	
	            	sb.append(StringUtils.checkString(attachmentTypeForm.getUploadOrDownloadType())).append(",");	            	
	            	if(onlineFlag != null && onlineFlag.equals("on")) {
	                	sb.append("'Y'");
	                }else {
	                	sb.append("'N'");
	                }	
	            	
	            	for(int i = 0; i<count; i++) {
	            		sb.append(",").append("'Y'");
	            	}
	            	if(levels.length > 0) {
	            		for(int i=0; i<levels.length; i++) {
	            			sb.append(",").append("'Y'");
	            		}
	            	}	            	
	            	sb.append(")");
	            	
	            	logger.debug("sql = "+sb.toString());
		            result = db.update(sb.toString());
		            
		            if(result > 0) {
		            	int attachmentTypeId = getAttachmentTypeId();
		            	return attachmentTypeId;
		            }else {
		            	return 0;
		            }
	            }
	            
	        }
	        catch (Exception e) {
	            result = -1;
	            logger.error("Error in saveActivityType() method" + e.getMessage());
	        }
	        return result;
		}


public AttachmentTypeForm getAttachmentType(int refId, String depts,String levels) {
	        
	    	String sql = "select att.*,at.ID,lat.TYPE_ID as actTypeId,lat.DESCRIPTION as actTypeDesc from REF_ACT_ATTACHMENT_TYPE at join LKUP_ACT_TYPE lat on lat.TYPE_ID=at.LKUP_ACT_TYPE_ID\n" + 
	    			" join LKUP_ATTACHMENT_TYPE att on att.TYPE_ID = at.LKUP_ATTACHMENT_TYPE_ID where ID = " + refId;

	        AttachmentTypeForm attachmentTypeForm = new AttachmentTypeForm();
	        RowSet rs = null;

	        try {
	            logger.debug(sql);
	            rs = new Wrapper().select(sql);

	            if (rs.next()) {
	            	attachmentTypeForm.setAttachmentTypeId(rs.getInt("TYPE_ID"));
	            	logger.debug("id .." + attachmentTypeForm.getAttachmentTypeId());
	            	attachmentTypeForm.setAttachmentTypeDesc(rs.getString("DESCRIPTION"));
	                logger.debug("Description .." + attachmentTypeForm.getAttachmentTypeDesc());
	                attachmentTypeForm.setDocumentUrl(rs.getString("DOCUMENT_URL"));
	                logger.debug("Document url .." + attachmentTypeForm.getDocumentUrl());
	                attachmentTypeForm.setBmcPopupWindowUrl(rs.getString("BMC_POPUP_WINDOW_URL"));
	                logger.debug("Bmc desc .." + attachmentTypeForm.getBmcDescription());
	                attachmentTypeForm.setBmcDescription(rs.getString("BMC_DESCRIPTION"));
	                logger.debug("Bmc url .." + attachmentTypeForm.getBmcPopupWindowUrl());
	                attachmentTypeForm.setComments(rs.getString("COMMENTS"));
	                logger.debug("Comments .." + attachmentTypeForm.getComments());
	                attachmentTypeForm.setUploadOrDownloadType(rs.getString("UPLOAD_OR_DOWNLOAD_TYPE"));	
	                logger.debug("upload or download type .." + attachmentTypeForm.getUploadOrDownloadType());
	                attachmentTypeForm.setOnlineFlag(rs.getString("ONLINE_AVAILABLE"));
	                logger.debug("online flag .." + attachmentTypeForm.getOnlineFlag());	  
	                attachmentTypeForm.setActivityType(rs.getString("actTypeId"));
	                logger.debug("act type id .." + attachmentTypeForm.getActivityType());	 
	                
	    			String[] departments = depts.split(",");
	    			String[] deptIds = new String[departments.length];
	    			for(int i = 0; i < departments.length; i++) {
	    				deptIds[i] = departments[i].trim();
	    			}	                
	                attachmentTypeForm.setDepartment(deptIds);                
	                logger.debug("depts .." + depts);
	                
	                String[] levelIds = levels.split(",");
	    			String[] level = new String[levelIds.length];
	    			for(int i = 0; i < levelIds.length; i++) {
	    				level[i] = levelIds[i].trim();
	    			}
	                attachmentTypeForm.setLevel(level);	                
	                logger.debug("levels .." + levels);
	                
	            }	            
	        }
	        catch (Exception e) {
	            logger.error("The Error in getAttachmentType()" + e.getMessage());
	        }finally {
	        	try {
	        		if (rs != null) {
		                rs.close();
		            }
	        	}catch(Exception e) {
	        		
	        	}
	        }

	        return attachmentTypeForm;
	    }

      /**
	     * delete the Attachment Type.
	     * @param attachmentTypeId
	     * @return int
	     */
	    public int deleteAttachmentType(String attachmentTypeId) {
	        int result = 0;
	        String sql = "DELETE FROM LKUP_ATTACHMENT_TYPE WHERE TYPE_ID=" + attachmentTypeId;

	        try {
	            logger.debug(sql);
	            new Wrapper().update(sql);
	        }
	        catch (SQLException e) {
	            result = -1;
	            logger.error("Sql error is deleteAttachmentType() " + e.getMessage());
	        }
	        catch (Exception e) {
	            result = -1;
	            logger.error("General error is deleteAttachmentType() " + e.getMessage());
	        }
	        return result;
	    }

@SuppressWarnings("rawtypes")
		public List getAttachmentTypes() {
			StringBuilder sql = new StringBuilder();
			List attachmentList = new ArrayList();
			AttachmentTypeMappingForm attachmentTypeMappingForm;
			RowSet rs = null;
	        try {
	        	
	        	sql.append("SELECT A.TYPE_ID,A.DESCRIPTION,B.ID,C.TYPE_ID as actTypeId,C.DESCRIPTION as activity_desc FROM LKUP_ATTACHMENT_TYPE A, REF_ACT_ATTACHMENT_TYPE B, LKUP_ACT_TYPE C " + 
	        			"WHERE A.TYPE_ID = B.LKUP_ATTACHMENT_TYPE_ID AND B.LKUP_ACT_TYPE_ID = C.TYPE_ID ORDER BY A.DESCRIPTION ASC");
	        	       	
	        	logger.debug("Sql is " + sql.toString());
	            rs = new Wrapper().select(sql.toString());
	            
	            while (rs.next()) {
	            	attachmentTypeMappingForm= new AttachmentTypeMappingForm();
	            	attachmentTypeMappingForm.setAttachmentTypeId(rs.getInt("TYPE_ID"));
	            	attachmentTypeMappingForm.setDescription(rs.getString("DESCRIPTION"));
	            	attachmentTypeMappingForm.setRefId(rs.getInt("ID"));
	            	attachmentTypeMappingForm.setActTypeId(rs.getInt("actTypeId"));
	            	attachmentTypeMappingForm.setActType(rs.getString("activity_desc"));
	            	attachmentList.add(attachmentTypeMappingForm);
	            }	        	
	        }catch (Exception e) {
	            logger.error(e.getMessage());
	        }finally {
	        	try {
	        		if (rs != null) {
		                rs.close();
		            }
	        	}catch(Exception e) {
	        		
	        	}
	        }
	        return attachmentList;
		}
		
		
		public List getAttachmentTypesForAdd() {	
        	
			List attachmentTypeList = new ArrayList();
			AttachmentTypeAdmin attachmentTypeAdmin = null;
					
			RowSet rs = null;
	        try {
	        	
	        	String sql = "SELECT * FROM LKUP_ATTACHMENT_TYPE ORDER BY DESCRIPTION ASC "; 
	            logger.debug("Sql is " + sql);

	            rs = new Wrapper().select(sql.toString());
	            while (rs.next()) {
	            	attachmentTypeAdmin= new AttachmentTypeAdmin();
	            	attachmentTypeAdmin.setAttachmentTypeId(rs.getInt("TYPE_ID"));
	            	attachmentTypeAdmin.setAttachmentTypeDesc((rs.getString("DESCRIPTION") != null ? rs.getString("DESCRIPTION") : "").trim());	            		            	
	            	attachmentTypeList.add(attachmentTypeAdmin);
	            }
	        	
	        }catch (Exception e) {
	            logger.error(e.getMessage());
	        }finally {
	        	try {
	        		if(rs != null) {
	        			rs.close();
	        		}
	        	}catch(Exception e) {
	        		
	        	}
	        }
	        return attachmentTypeList;
		}	
		
		public List getAttachment(int id) throws Exception {
			logger.info("getAttachment()");
			logger.debug("reference id = "+id);
			StringBuilder sql = new StringBuilder();
			AttachmentTypeAdmin attachmentTypeAdmin = new AttachmentTypeAdmin();
			List attachmentTypeList = new ArrayList();
			RowSet rs = null;
	        
			 try {
		        	
		        	sql.append("SELECT A.TYPE_ID,A.DESCRIPTION,B.ID,C.TYPE_ID,C.TYPE FROM LKUP_ATTACHMENT_TYPE A, REF_ACT_ATTACHMENT_TYPE B, LKUP_ACT_TYPE C " + 
		        			"WHERE A.TYPE_ID = B.LKUP_ATTACHMENT_TYPE_ID AND B.LKUP_ACT_TYPE_ID = C.TYPE_ID and B.ID = "+id);
		        	       	
		        	logger.debug("Sql is " + sql.toString());
		            rs = new Wrapper().select(sql.toString());
		            
		            while (rs.next()) {
		            	attachmentTypeAdmin= new AttachmentTypeAdmin();
		            	attachmentTypeAdmin.setAttachmentTypeId(rs.getInt("TYPE_ID"));
		            	attachmentTypeAdmin.setAttachmentTypeDesc(rs.getString("DESCRIPTION"));
		            	//attachmentTypeAdmin.setRefId(rs.getInt("ID"));
		            	attachmentTypeAdmin.setActTypeId(rs.getInt("TYPE_ID"));
		            	attachmentTypeAdmin.setActTypeDesc(rs.getString("TYPE"));	
		            	attachmentTypeList.add(attachmentTypeAdmin);
		            }	        	
		        }catch (Exception e) {
		            logger.error(e.getMessage());
		        }finally {
		        	try {
		        		if (rs != null) {
			                rs.close();
			            }
		        	}catch(Exception e) {
		        		
		        	}
		        }
		        return attachmentTypeList;			
		}
		
		public boolean deleteAttachment(int id) throws Exception {
			logger.info("deleteAttachment()");
			boolean result = false;
			Wrapper db = new Wrapper();
			db.beginTransaction();
			try{
			String sql = "DELETE from REF_ACT_ATTACHMENT_TYPE WHERE ID = " +id;
			logger.info(sql);
			db.addBatch(sql);
			
			/*sql = "DELETE from LKUP_ATTACHMENT_TYPE WHERE ID = "+id;
			logger.info(sql);
			db.addBatch(sql);*/
			
			result = (new Wrapper().update(sql))>0;
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return result;
		}
		
		public int updateAttachment(AttachmentTypeForm f) throws Exception {
			logger.info("updateAttachment()");
			int count = 0;
			Wrapper db = new Wrapper();
			
			logger.debug("attachment type id = "+f.getAttachmentTypeId());
			logger.debug("act type id = "+f.getActivityType());
			
			try{
			/*String sql = "UPDATE LKUP_ATTACHMENT_TYPE SET DESCRIPTION=" + StringUtils.checkString(f.getDescription()) +" WHERE TYPE_ID = "+f.getAttachmentTypeId();
			logger.info(sql);
			db.addBatch(sql);*/
			
			String sql = "UPDATE REF_ACT_ATTACHMENT_TYPE SET LKUP_ACT_TYPE_ID = " + f.getActivityType() +" WHERE LKUP_ATTACHMENT_TYPE_ID = "+f.getAttachmentTypeId();
			logger.info(sql);
			count = db.update(sql);
			
			
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return count;
		}
		
		public int saveAttachment(AttachmentTypeForm f) throws Exception {
			logger.info("saveAttachment()");
			int count = 0;
			
			logger.debug("attachment type id = "+f.getAttachmentTypeId());
			logger.debug("act type id = "+f.getActivityType());
			
			Wrapper db = new Wrapper();
			
			try{			
			
			String sql = "INSERT INTO REF_ACT_ATTACHMENT_TYPE (ID,LKUP_ACT_TYPE_ID,LKUP_ATTACHMENT_TYPE_ID) VALUES ((select Max(ID)+1 from REF_ACT_ATTACHMENT_TYPE),"+f.getActivityType()+","+f.getAttachmentTypeId()+")";
			
			logger.info(sql);
			count = db.insert(sql);		
			
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return count;
		}

    /**
		 * Gets the list of activity types
		 * 
		 * @return
		 */
		public static List getActivityTypes() throws Exception {
			logger.debug("getActivityTypes()");
			
			List activityTypes = new ArrayList();
			AttachmentTypeAdmin attachmentTypeAdmin = null;
			RowSet rs = null;

			try {
				String sql = "select * from lkup_act_type order by description";
				logger.debug(sql);

				rs = new Wrapper().select(sql);

				while (rs != null && rs.next()) {
					attachmentTypeAdmin = new AttachmentTypeAdmin();
					attachmentTypeAdmin.setActTypeId(rs.getInt("TYPE_ID"));
					attachmentTypeAdmin.setActTypeDesc(StringUtils.properCase(rs.getString("description")));					
					activityTypes.add(attachmentTypeAdmin);
				}

				return activityTypes;
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw e;
			}finally {
				try {
					if(rs != null) {
						rs.close();
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}

    public boolean checkIdExistOrNot(int attachmentTypeId) {
			String sql = "SELECT COUNT(*) AS COUNT FROM REF_ACT_ATTACHMENT_TYPE WHERE LKUP_ATTACHMENT_TYPE_ID=" + attachmentTypeId;
			boolean isExist = false;
			RowSet rs = null;
			try {
				logger.info(sql);

				rs = new Wrapper().select(sql);

				if (rs.next()) {
					if (rs.getInt("COUNT") == 0) {
						isExist = false;
					} else {
						isExist = true;
					}
				}
				
			} catch (SQLException e) {
				logger.error("Sql error is subProjectNameRemovable() " + e.getMessage());
			} catch (Exception e) {
				logger.error("General error is subProjectNameRemovable() " + e.getMessage());
			}finally {
				try {
					if(rs != null) {
						rs.close();
					}
				}catch(Exception e) {
					
				}
			}

			return isExist;
		}		

		
		 // Admin for Email Template Types -- Start
		/**
	     * gets the List of EmailTemplateTypeAdmin forms for
	     * @param emailTemplateTypeAdminForm
	     * @return List
	     */
		public List getEmailTemplateTypes(EmailTemplateTypeAdminForm emailTemplateTypeAdminForm) {
			StringBuilder sql = new StringBuilder();
			String type = StringUtils.nullReplaceWithBlank(emailTemplateTypeAdminForm.getEmailTemplateType()).trim();
			String desc = StringUtils.nullReplaceWithBlank(emailTemplateTypeAdminForm.getEmailTemplateDesc()).trim();
			List emailTempTypeList = new ArrayList();
			EmailTemplateTypeAdmin emailTemplateTypeAdmin;
			RowSet rs = null;
	        try {        	
	        	sql.append("SELECT EMAIL_TEMP_TYPE_ID, EMAIL_TEMP_TYPE, EMAIL_TEMP_TYPE_DESCRIPTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE "
	        			+ "FROM LKUP_EMAIL_TEMPLATE_TYPE ");
	        	
	        	if(!type.equals("") || !desc.equals("")){
	        		sql.append(" WHERE ");
	        	}
	        	if(!type.equalsIgnoreCase("")) {
	        		sql.append(" EMAIL_TEMP_TYPE LIKE '%" +type+"%'");
	        	}        	
	        	if(!desc.equals("") && ! type.equals("")){
	        		sql.append(" AND  ");
	        	}        	
	        	if(!desc.equals("")){
	        		sql.append(" EMAIL_TEMP_TYPE_DESCRIPTION LIKE '%" +desc+ "%'");        		
	        	}
	        	sql.append("ORDER BY EMAIL_TEMP_TYPE");
	            logger.debug("Sql is " + sql.toString());

	            rs = new Wrapper().select(sql.toString());
	            while (rs!=null && rs.next()) {
	            	emailTemplateTypeAdmin= new EmailTemplateTypeAdmin();
	            	emailTemplateTypeAdmin.setEmailTemplateTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	emailTemplateTypeAdmin.setEmailTemplateType((rs.getString("EMAIL_TEMP_TYPE") != null ? rs.getString("EMAIL_TEMP_TYPE") : "").trim());
	            	emailTemplateTypeAdmin.setEmailTemplateDesc((rs.getString("EMAIL_TEMP_TYPE_DESCRIPTION") != null ? rs.getString("EMAIL_TEMP_TYPE_DESCRIPTION") : "").trim());
	            	emailTempTypeList.add(emailTemplateTypeAdmin);
	            }
	        	if (rs != null) {
	                rs.close();
	            }
	        }catch (Exception e) {
	            logger.error(e.getMessage());
	        }	//Closing Rowset Added by Gayathri
			finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
			}
	        return emailTempTypeList;
		}
		
		/**
	     * gets the List of EmailTemplateTypeAdmin forms for
	     * @param emailTemplateTypeAdminForm
	     * @return List
	     */
		public List getEmailTemplateTypesForAdd(EmailTemplateTypeAdminForm emailTemplateTypeAdminForm) {
			StringBuilder sql = new StringBuilder();
			String type = StringUtils.nullReplaceWithBlank(emailTemplateTypeAdminForm.getEmailTemplateType()).trim();
			String desc = StringUtils.nullReplaceWithBlank(emailTemplateTypeAdminForm.getEmailTemplateDesc()).trim();
			List emailTempTypeList = new ArrayList();
			EmailTemplateTypeAdmin emailTemplateTypeAdmin;
			RowSet rs = null;
	        try {
	        	
	        	sql.append("SELECT EMAIL_TEMP_TYPE_ID, EMAIL_TEMP_TYPE, EMAIL_TEMP_TYPE_DESCRIPTION, CREATED_BY, CREATED_DATE, UPDATED_BY, UPDATED_DATE"
	        			+ " FROM LKUP_EMAIL_TEMPLATE_TYPE WHERE EMAIL_TEMP_TYPE_ID NOT IN (SELECT DISTINCT EMAIL_TEMP_TYPE_ID FROM REF_EMAIL_TEMPLATE)");
	        	
	        	if(!type.equals("") || !desc.equals("")){
	        		sql.append(" WHERE ");
	        	}
	        	if(!type.equalsIgnoreCase("")) {
	        		sql.append(" EMAIL_TEMP_TYPE LIKE '%" +type+"%'");
	        	}        	
	        	if(!desc.equals("") && ! type.equals("")){
	        		sql.append(" AND  ");
	        	}        	
	        	if(!desc.equals("")){
	        		sql.append(" EMAIL_TEMP_TYPE_DESCRIPTION LIKE '%" +desc+ "%'");        		
	        	}
	            logger.debug("Sql is " + sql.toString());

	            rs = new Wrapper().select(sql.toString());
	            while (rs!=null && rs.next()) {
	            	emailTemplateTypeAdmin= new EmailTemplateTypeAdmin();
	            	emailTemplateTypeAdmin.setEmailTemplateTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	emailTemplateTypeAdmin.setEmailTemplateType((rs.getString("EMAIL_TEMP_TYPE") != null ? rs.getString("EMAIL_TEMP_TYPE") : "").trim());
	            	emailTemplateTypeAdmin.setEmailTemplateDesc((rs.getString("EMAIL_TEMP_TYPE_DESCRIPTION") != null ? rs.getString("EMAIL_TEMP_TYPE_DESCRIPTION") : "").trim());
	            	emailTempTypeList.add(emailTemplateTypeAdmin);
	            }
	        	if (rs != null) {
	                rs.close();
	            }
	        }catch (Exception e) {
	            logger.error(e.getMessage());
	        }	//Closing Rowset Added by Gayathri
			finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
			}
	        return emailTempTypeList;
		}
		
		/**
	     * gets the condition library form for
	     * @param libConditionId
	     * @return
	     */
	    public EmailTemplateTypeAdminForm getEmailTemplateType(int emailTempTypeId) {
	        
	    	String sql = "SELECT * FROM LKUP_EMAIL_TEMPLATE_TYPE WHERE EMAIL_TEMP_TYPE_ID=" + emailTempTypeId;

	        EmailTemplateTypeAdminForm emailTempTypeForm = new EmailTemplateTypeAdminForm();
	        RowSet rs = null;

	        try {
	            logger.debug(sql);
	            rs = new Wrapper().select(sql);

	            if (rs.next()) {
	            	emailTempTypeForm.setEmailTemplateTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	logger.debug("id .." + emailTempTypeForm.getEmailTemplateTypeId());
	            	emailTempTypeForm.setEmailTemplateType(rs.getString("EMAIL_TEMP_TYPE"));
	                logger.debug("Email temp type .." + emailTempTypeForm.getEmailTemplateType());
	                emailTempTypeForm.setEmailTemplateDesc(rs.getString("EMAIL_TEMP_TYPE_DESCRIPTION"));
	                logger.debug("Email temp type desc .." + emailTempTypeForm.getEmailTemplateDesc());
	            }

	            if (rs != null) rs.close();
	        }catch (Exception e) {
	            logger.error("The Error in getEmailTemplateType()" + e.getMessage());
	        }	//Closing Rowset Added by Gayathri
			finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
			}
	        return emailTempTypeForm;
	    }
		
		public int saveEmailTemplateType(EmailTemplateTypeAdminForm emailTemplateTypeAdminForm) {
			Wrapper db = new Wrapper();
			int result = 0;

	        try {
	            StringBuilder sb = null;
	            if (emailTemplateTypeAdminForm.getEmailTemplateTypeId() > 0) {
	            	sb = new StringBuilder("UPDATE LKUP_EMAIL_TEMPLATE_TYPE SET EMAIL_TEMP_TYPE =");
	                sb.append(StringUtils.checkString(emailTemplateTypeAdminForm.getEmailTemplateType()));
	                sb.append(", EMAIL_TEMP_TYPE_DESCRIPTION =");
	                sb.append(StringUtils.checkString(emailTemplateTypeAdminForm.getEmailTemplateDesc()));
	                sb.append(", UPDATED_BY =");
	                sb.append(StringUtils.checkNumber(emailTemplateTypeAdminForm.getUpdatedBy()));
	                sb.append(" WHERE EMAIL_TEMP_TYPE_ID =");
	                sb.append(emailTemplateTypeAdminForm.getEmailTemplateTypeId());
	            }else {
	            	int templateId = db.getNextId("EMAIL_TEMP_TYPE_ID");
	            	sb = new StringBuilder("insert into LKUP_EMAIL_TEMPLATE_TYPE(EMAIL_TEMP_TYPE_ID,EMAIL_TEMP_TYPE, EMAIL_TEMP_TYPE_DESCRIPTION, CREATED_BY, CREATED_DATE) values(");
	            	sb.append(templateId+",");
	            	sb.append(StringUtils.checkString(emailTemplateTypeAdminForm.getEmailTemplateType()) + ",");
	            	sb.append(StringUtils.checkString(emailTemplateTypeAdminForm.getEmailTemplateDesc()) + ",");
	            	sb.append(StringUtils.checkNumber(emailTemplateTypeAdminForm.getCreatedBy()) + ",");
	            	sb.append("current_timestamp");
	            	sb.append(")");
	            }
	            logger.debug(sb);
	            result = db.update(sb.toString());
	        }
	        catch (Exception e) {
	            result = -1;
	            logger.error("Error in saveActivityType() method" + e.getMessage());
	        }
	        return result;
		}
		
		/**
	     * delete the Email Template Type.
	     * @param emailTemplateTypeAdminForm
	     * @return int
	     */
	    public int deleteEmailTemplateType(String emailTemplateTypeId) {
	        int result = 0;
	        String sql = "DELETE FROM LKUP_EMAIL_TEMPLATE_TYPE WHERE EMAIL_TEMP_TYPE_ID=" + emailTemplateTypeId;

	        try {
	            logger.debug(sql);
	            new Wrapper().update(sql);
	        }
	        catch (SQLException e) {
	            result = -1;
	            logger.error("Sql error is deleteEmailTemplateType() " + e.getMessage());
	        }
	        catch (Exception e) {
	            result = -1;
	            logger.error("General error is deleteEmailTemplateType() " + e.getMessage());
	        }
	        return result;
	    }
	    
	    /**
	     * gets the List of EmailTemplateTypeAdmin forms for
	     * @param emailTemplateTypeAdminForm
	     * @return List
	     */
		@SuppressWarnings("rawtypes")
		public List getEmailTemplateTypes() {
			StringBuilder sql = new StringBuilder();
			List emailTempList = new ArrayList();
			EmailTemplateAdminForm emailTemplateAdminForm;
			RowSet rs = null;
	        try {
	        	
	        	sql.append("SELECT C.REF_EMAIL_TEMP_ID, A.EMAIL_TEMP_TYPE_ID, B.EMAIL_TEMPLATE_ID, A.EMAIL_TEMP_TYPE, B.EMAIL_SUBJECT, B.EMAIL_BODY"
	        			+ " FROM LKUP_EMAIL_TEMPLATE_TYPE A, EMAIL_TEMPLATE B, REF_EMAIL_TEMPLATE C WHERE A.EMAIL_TEMP_TYPE_ID = C.EMAIL_TEMP_TYPE_ID"
	        			+ " AND B.EMAIL_TEMPLATE_ID = C.EMAIL_TEMPLATE_ID ORDER BY A.EMAIL_TEMP_TYPE");
	        	       	
	        	logger.debug("Sql is " + sql.toString());

	            rs = new Wrapper().select(sql.toString());
	            while (rs!=null && rs.next()) {
	            	emailTemplateAdminForm= new EmailTemplateAdminForm();
	            	emailTemplateAdminForm.setRefEmailTempId(rs.getInt("REF_EMAIL_TEMP_ID"));
	            	emailTemplateAdminForm.setEmailTempTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	emailTemplateAdminForm.setEmailTempId(rs.getInt("EMAIL_TEMPLATE_ID"));
	            	emailTemplateAdminForm.setTitle((rs.getString("EMAIL_TEMP_TYPE") != null ? rs.getString("EMAIL_TEMP_TYPE") : "").trim());
	            	emailTemplateAdminForm.setEmailSubject((rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "").trim());
	            	emailTemplateAdminForm.setEmailMessage((rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "").trim());
	            	emailTempList.add(emailTemplateAdminForm);
	            }
	        	if (rs != null) {
	                rs.close();
	            }
	        }catch (Exception e) {
	            logger.error(e.getMessage());
	        }	//Closing Rowset Added by Gayathri
			finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
			}
	        return emailTempList;
		}
		
		public EmailTemplateAdminForm getEmailTemplate(int id, String flag) throws Exception {
			logger.info("getEmailTemplates()");
			StringBuilder sql = new StringBuilder();
			EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
			RowSet rs = null;
	        
			try{
				sql.append("SELECT C.REF_EMAIL_TEMP_ID, A.EMAIL_TEMP_TYPE_ID, B.EMAIL_TEMPLATE_ID, A.EMAIL_TEMP_TYPE, B.EMAIL_SUBJECT, B.EMAIL_BODY"
	        			+ " FROM LKUP_EMAIL_TEMPLATE_TYPE A, EMAIL_TEMPLATE B, REF_EMAIL_TEMPLATE C WHERE A.EMAIL_TEMP_TYPE_ID = C.EMAIL_TEMP_TYPE_ID"
	        			+ " AND B.EMAIL_TEMPLATE_ID = C.EMAIL_TEMPLATE_ID ");
	        	if(id > 0) {
	        		if(flag !=null && flag.equalsIgnoreCase(Constants.EMIAL_WORKER_FLAG)) sql.append(" AND C.EMAIL_TEMP_TYPE_ID = " +id);
	        		else if(flag !=null && flag.equalsIgnoreCase(Constants.EMIAL_TMEPLATE_FLAG)) sql.append(" AND C.REF_EMAIL_TEMP_ID = " +id);
	        	}
	        	
	        	sql.append(" ORDER BY A.EMAIL_TEMP_TYPE");
	        	
	        	logger.debug("Sql is :: " + sql.toString());

	        	rs = new Wrapper().select(sql.toString());
	        	
	            if(rs!=null) {
		            while (rs.next()) {
		            	emailTemplateAdminForm= new EmailTemplateAdminForm();
		            	emailTemplateAdminForm.setRefEmailTempId(rs.getInt("REF_EMAIL_TEMP_ID"));
		            	emailTemplateAdminForm.setEmailTempTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
		            	emailTemplateAdminForm.setEmailTempId(rs.getInt("EMAIL_TEMPLATE_ID"));
		            	emailTemplateAdminForm.setTitle((rs.getString("EMAIL_TEMP_TYPE") != null ? rs.getString("EMAIL_TEMP_TYPE") : "").trim());
		            	emailTemplateAdminForm.setEmailSubject((rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "").trim());
		            	emailTemplateAdminForm.setEmailMessage((rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "").trim());            	
		            }
	            }
			
			}catch(Exception e){
				logger.error(e.getMessage());
			}	//Closing Rowset Added by Gayathri
			finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
			}
			return emailTemplateAdminForm;
		}
		
		public EmailTemplateAdminForm getEmailTempByTempTypeId(int tempTypeId) throws Exception {
			logger.info("getEmailTemplates()");
			StringBuilder sql = new StringBuilder();
			EmailTemplateAdminForm emailTemplateAdminForm = new EmailTemplateAdminForm();
			RowSet rs = null;
			try{
				sql.append("SELECT A.EMAIL_TEMP_TYPE_ID, B.EMAIL_TEMPLATE_ID, B.EMAIL_SUBJECT, B.EMAIL_BODY"
	        			+ " FROM LKUP_EMAIL_TEMPLATE_TYPE A, EMAIL_TEMPLATE B, REF_EMAIL_TEMPLATE C WHERE A.EMAIL_TEMP_TYPE_ID = C.EMAIL_TEMP_TYPE_ID"
	        			+ " AND B.EMAIL_TEMPLATE_ID = C.EMAIL_TEMPLATE_ID ");
	        	if(tempTypeId > 0) {
	        		sql.append(" AND C.EMAIL_TEMP_TYPE_ID = " +tempTypeId);
	        	}
	        	
	        	sql.append(" ORDER BY A.EMAIL_TEMP_TYPE");
	        	
	        	logger.debug("Sql is :: " + sql.toString());

	            rs = new Wrapper().select(sql.toString());
	            while (rs!=null && rs.next()) {
	            	emailTemplateAdminForm= new EmailTemplateAdminForm();
	            	emailTemplateAdminForm.setEmailTempTypeId(rs.getInt("EMAIL_TEMP_TYPE_ID"));
	            	emailTemplateAdminForm.setEmailTempId(rs.getInt("EMAIL_TEMPLATE_ID"));
	            	emailTemplateAdminForm.setEmailSubject((rs.getString("EMAIL_SUBJECT") != null ? rs.getString("EMAIL_SUBJECT") : "").trim());
	            	emailTemplateAdminForm.setEmailMessage((rs.getString("EMAIL_BODY") != null ? rs.getString("EMAIL_BODY") : "").trim());            	
	            }
			if(rs!=null) rs.close();
			}catch(Exception e){
				logger.error(e.getMessage());
			}	//Closing Rowset Added by Gayathri
			finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
			}
			return emailTemplateAdminForm;
		}
		
		public boolean deleteEmailTemplate(int id,int user) throws Exception {
			logger.info("getEmailTemplates()");
			boolean result = false;
			Wrapper db = new Wrapper();
			db.beginTransaction();
			try{
			String sql = "DELETE from REF_EMAIL_TEMPLATE WHERE REF_EMAIL_TEMP_ID = " +id;
			logger.info(sql);
			db.addBatch(sql);
			
			sql = "DELETE from LKUP_EMAIL_TEMPLATE_TYPE WHERE EMAIL_TEMP_TYPE_ID = (SELECT EMAIL_TEMP_TYPE_ID FROM REF_EMAIL_TEMPLATE WHERE REF_EMAIL_TEMP_ID ="+id +")";
			logger.info(sql);
			db.addBatch(sql);
			
			result = (new Wrapper().update(sql))>0;
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return result;
		}
		
		public int[] saveEmailTemplate(EmailTemplateAdminForm f,int user) throws Exception {
			logger.info("getEmailTemplates()");
			int[] recordseffected = {};
			int emailTempId = 0;
			Wrapper db = new Wrapper();
			db.beginTransaction();
			try{
			emailTempId = db.getNextId("EMAIL_TEMPLATE_ID");
			String sql = "INSERT INTO EMAIL_TEMPLATE (EMAIL_TEMPLATE_ID, EMAIL_SUBJECT, EMAIL_BODY, CREATED_BY) "
						 + "VALUES("+emailTempId+", "+StringUtils.checkString(f.getEmailSubject())+","+StringUtils.checkString(f.getEmailMessage())+","+user+")";
			
			logger.info(sql);
			db.addBatch(sql);
			int refEmailTempId = new Wrapper().getNextId("REF_EMAIL_TEMP_ID");
			sql = "INSERT INTO REF_EMAIL_TEMPLATE (REF_EMAIL_TEMP_ID,EMAIL_TEMP_TYPE_ID,EMAIL_TEMPLATE_ID,CREATED_BY) VALUES ("+refEmailTempId+","+f.getTitle() +","+emailTempId+","+user+")";
			
			logger.info(sql);
			db.addBatch(sql);
			
			recordseffected = db.executeBatch();
			
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return recordseffected;
		}
		
		public int[] updateEmailTemplate(EmailTemplateAdminForm f,int user) throws Exception {
			logger.info("getEmailTemplates()");
			int[] recordseffected = {};
			Wrapper db = new Wrapper();
			db.beginTransaction();
			
			try{
			String sql = "UPDATE EMAIL_TEMPLATE SET EMAIL_SUBJECT=" + StringUtils.checkString(f.getEmailSubject()) +", EMAIL_BODY=" + StringUtils.checkString(f.getEmailMessage()) + ""
					+ ", UPDATED_BY=" + user + ", UPDATED_DATE = CURRENT_TIMESTAMP WHERE EMAIL_TEMPLATE_ID = "+f.getEmailTempId();
			logger.info(sql);
			db.addBatch(sql);
			
			sql = "UPDATE REF_EMAIL_TEMPLATE SET EMAIL_TEMPLATE_ID = " + f.getEmailTempId() +", EMAIL_TEMP_TYPE_ID = " + f.getEmailTempTypeId() + ""
					+ ", UPDATED_BY=" + user + ", UPDATED_DATE = CURRENT_TIMESTAMP WHERE REF_EMAIL_TEMP_ID = "+f.getRefEmailTempId();
			logger.info(sql);
			db.addBatch(sql);
			
			recordseffected = db.executeBatch();
			}catch(Exception e){
				logger.error(e.getMessage());
			}
			return recordseffected;
		}
		
	    // Admin for Email Template Types -- End

		// Admin for Plan Check Status -- Start
		
		
		public void updatePlanCheckStatuses(PlanCheckStatus[] pcStatusArray) throws AgentException {
			if (pcStatusArray != null && pcStatusArray.length > 0) {
				// Wrapper
				Wrapper wrapper = new Wrapper();
				String updateSql = "";
				String insertSql = "";
				wrapper.beginTransaction();
				try {
					for (int i = 0; i < pcStatusArray.length; i++) {
						if (pcStatusArray[i].getCode() == 0) {
							int PcCode = wrapper.getNextId("PC_STATUS_CODE");
							insertSql = "insert into LKUP_PC_STATUS (PC_CODE, PC_DESC, ACT_STATUS_CODE, ACTIVE, SHOW_PC_TAB) values ("
									+ PcCode + ", '" + pcStatusArray[i].getDescription() + "', " + PcCode + ", '" + pcStatusArray[i].getActive() + "', '" + pcStatusArray[i].getVisible() + "')";
							logger.debug("insertSql::" + insertSql);
							wrapper.addBatch(insertSql);
						} else {
							updateSql = "update lkup_pc_status set pc_desc = '" + pcStatusArray[i].getDescription()
									+ "',ACTIVE = '" + pcStatusArray[i].getActive() + "',SHOW_PC_TAB = '" + pcStatusArray[i].getVisible() + "' where PC_CODE = "
									+ pcStatusArray[i].getCode();
							logger.debug("updateSql::" + updateSql);
							wrapper.addBatch(updateSql);
						}
					}
					wrapper.executeBatch();
					wrapper.commitTransaction();
				} catch (Exception e) {
					logger.error("", e);
					wrapper.rollbackTransaction();
					throw new AgentException("", e);
				}
			}
		}

		public int savePCStatus(int code, String description, boolean active, boolean visible) {
			logger.info("savePCStatus(" + code + ", " + description + ", " + active + ", " + visible + ")");

			Wrapper db = new Wrapper();
			int result = 0;

			try {
				String sql = "SELECT * FROM lkup_pc_status WHERE pc_desc = " +StringUtils.checkString(description);
				RowSet rs = db.select(sql);

				if (rs != null && rs.next()) {
					sql = "update lkup_pc_status set pc_desc = " + StringUtils.checkString(description)
					+ ",ACTIVE = " + StringUtils.checkString(StringUtils.b2s(active)) + ",SHOW_PC_TAB = " +StringUtils.checkString(StringUtils.b2s(visible)) + " where PC_CODE = "
					+ rs.getInt("PC_CODE");
					result = 2;
				} else {
					int PcCode = db.getNextId("PC_STATUS_CODE");
					sql = "insert into LKUP_PC_STATUS (PC_CODE, PC_DESC, ACTIVE, SHOW_PC_TAB) values ("
							+ PcCode + ", " +StringUtils.checkString(description)+  ", "
							+ StringUtils.checkString(StringUtils.b2s(active))+ ", " +  StringUtils.checkString(StringUtils.b2s(visible))+ ")";
					result = 2;
				}

				logger.info(sql);
				db.update(sql);
			} catch (Exception e) {
				result = -1;
				logger.error("Error in savePCStatus() method" + e.getMessage());
			}

			return result;
		}
		
		/**
		 * gets the Plan Check Status Data on Lookup or on single select
		 * 
		 * @param PCStatusForm
		 * @return
		 */
		public List getPlanCheckStatus(PCStatusForm pcStatusForm) {
			List pcStatusList = new ArrayList();
			RowSet rs = null;
			try {
				String sql = "";
				if(pcStatusForm != null){
				sql = ((pcStatusForm.getCode()<=0) ? sql : (sql + " PC_CODE = " + pcStatusForm.getCode() + " AND"));

				sql = ((pcStatusForm.getDescription() == null) || pcStatusForm.getDescription().equals("")) ? sql : (sql + " UPPER(PC_DESC) LIKE '%" + pcStatusForm.getDescription().toUpperCase() + "%' AND");
				
				//sql = ((pcStatusForm.getModuleId()<=0) ? sql : (sql + " MODULE_ID = " + pcStatusForm.getModuleId() + " AND"));

				if (pcStatusForm.isActive() == true)
					sql = sql + " ACTIVE = 'Y' AND";

				if (pcStatusForm.isVisible() == true)
					sql = sql + " SHOW_PC_TAB = 'Y' AND";
				
				if ((sql != null) && !("").equalsIgnoreCase(sql)) {
					sql = sql.substring(0, sql.length() - 4); // to remove the trailing AND
					sql = " WHERE " + sql;
				}
				}
				sql = "SELECT PC_CODE,PC_DESC,ACTIVE,SHOW_PC_TAB FROM LKUP_PC_STATUS LPS " + sql + " ORDER BY PC_DESC";

				logger.info("getPlanCheckStatusData() Sql is " + sql);

				rs = new Wrapper().select(sql);

				while (rs!=null && rs.next()) {			
					DisplayItem items = new DisplayItem();
					items.setFieldOne(StringUtils.i2s(rs.getInt("PC_CODE")));
					items.setFieldTwo(StringUtils.nullReplaceWithEmpty(rs.getString("PC_DESC")).trim());
					//items.setFieldThree(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));
					items.setFieldFour(StringUtils.nullReplaceWithEmpty(rs.getString("ACTIVE")));
					items.setFieldFive(StringUtils.nullReplaceWithEmpty(rs.getString("SHOW_PC_TAB")));
					items.setFieldSix("Y");
					pcStatusList.add(items);
				}
				if (rs != null) rs.close();
			} catch (Exception e) {
				logger.error(e.getMessage());
			}finally {
				try{
					if(rs!= null){
						rs.close();
					}
					}catch (Exception e) {
						logger.error(""+e);
					}
				}

			return pcStatusList;
		}	

				
		/**
		 * To Delete the PC status list selected in the screen
		 * @param id
		 * @return
		 */
		
		public int deletePCStatusList(String id) {
			logger.debug("Entered into deletePCStatusList..."+id);
			int result = 0;
			String sql = "";

			String[] idArray = {};

			idArray = id.split(",");
			logger.debug("id List " + idArray.length);
			for (int i = 0; i < idArray.length; i++) {
				sql = "DELETE FROM LKUP_PC_STATUS WHERE PC_CODE=" + idArray[i];
				logger.info(sql);
				result = 3;
				try {
					new Wrapper().update(sql);
				} catch (Exception e) {
					result = -1;
					logger.error("General error is deleteAddressType() " + e.getMessage());
				}
			}
			return result;
		}
		
		// Admin for Plan check status -- End

		/**
		 * Gets the user object for the given user id.
		 * 
		 * @param userId
		 * @return
		 * @throws Exception
		 */
		public User getOnlineUser(int userId) throws Exception {
            User user = new User();

 

            try {
                logger.debug("entered into getOnlineUser with ID" + userId);

 

                Wrapper db = new Wrapper();
                RowSet userRs = db.select("select * from ext_user where EXT_USER_ID = " + userId);

 

                if (userRs!=null && userRs.next()) {
                    // user = new User();

 

                    user.setUserId(StringUtils.s2i(StringUtils.d2s(userRs.getDouble("EXT_USER_ID"))));
                    user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
                    logger.debug(" userid  is set to  " + user.getUserId());
                    user.setUsername(userRs.getString("EXT_USERNAME"));
                    logger.debug("  Username  is set to  " + user.getUsername());
                    user.setFirstName(StringUtils.nullReplaceWithEmpty(userRs.getString("FIRSTNAME")));
                    logger.debug("  FirstName  is set to  " + user.getFirstName());
                    user.setLastName(StringUtils.nullReplaceWithEmpty(userRs.getString("LASTNAME")));
                    logger.debug(" LastName  is set to  " + user.getLastName());
                    user.setMiddleInitial("");
                    logger.debug(" MiddleInitial  is set to  " + user.getMiddleInitial());
                    user.setTitle(userRs.getString("EXT_USER_TYPE"));
                    user.setPassword(userRs.getString("ext_password"));
                    
                    user.setAccountNbr(userRs.getString("EXT_ACCT_NBR"));
                    
                    user.setIsObc(userRs.getString("OBC"));
                    if ((user.getIsObc().equals("Y") && userRs.getString("ACTIVEOBC").equals("Y")) || (user.getIsDot().equals("Y") && userRs.getString("ACTIVEDOT").equals("Y"))) {
                        user.setActive("Y");
                    } else {
                        user.setActive("N");
                    }
                }
                            
                if (userRs != null)
                    userRs.close();
        
            } catch (Exception e) {
                logger.error(" Exception in adminAgent -- getOnlineUser() method " + e.getMessage());
                throw e;
            }

 

            if (user == null)
                user = new User();

 

            return user;
        }
		
		
		public String getActivityTypes(int moduleId) throws Exception{
			logger.info("getActivityTypes(" + moduleId + ")");

			JSONObject json = new JSONObject();
			JSONArray array = new JSONArray();
			
			RowSet rs = null;

			try {
				String sql = "select TYPE_ID,DESCRIPTION from lkup_act_type where MODULE_ID ="+moduleId+" order by DESCRIPTION asc";;
				logger.debug(sql);
				rs = new Wrapper().select(sql);

				while (rs.next()) {				
					JSONObject item = new JSONObject();
					item.put("typeId", rs.getInt("TYPE_ID"));
					item.put("description", rs.getString("DESCRIPTION"));				
					array.put(item);
				}
				json.put("activityTypesData", array);
				
			} catch (Exception e) {
				logger.error("while getting activity types error :"+e.getMessage());
				throw e;
			}finally {
				try {
					if(rs != null) {
						rs.close();
					}
				}catch(Exception e) {
					
				}
			}
			
			logger.info("json Data = "+json.toString());
			return json.toString();
		}

		public String getAttachmentTypes(int actTypeId) {
			logger.info("getAttachmentTypes(" + actTypeId + ")");

			JSONObject json = new JSONObject();
			JSONArray array = new JSONArray();
			
			RowSet rs = null;

			try {
				String sql = "select a.TYPE_ID,a.DESCRIPTION from LKUP_ATTACHMENT_TYPE a, REF_ACT_ATTACHMENT_TYPE b, LKUP_ACT_TYPE c where a.TYPE_ID = b.LKUP_ATTACHMENT_TYPE_ID and b.LKUP_ACT_TYPE_ID = c.TYPE_ID and c.TYPE_ID = "+actTypeId+" order by a.DESCRIPTION asc";
				logger.debug(sql);
				rs = new Wrapper().select(sql);

				while (rs.next()) {				
					JSONObject item = new JSONObject();
					item.put("typeId", rs.getInt("TYPE_ID"));
					item.put("description", rs.getString("DESCRIPTION"));				
					array.put(item);
				}
				json.put("attachmentTypesData", array);
				
			} catch (Exception e) {
				logger.error("while getting attachment types error :"+e.getMessage());				
			}finally {
				try {
					if(rs != null) {
						rs.close();
					}
				}catch(Exception e) {
					
				}
			}
			
			logger.info("json Data = "+json.toString());
			return json.toString();
		}

		private String getAttachmentTypeDesc(String attachmentTypeId) {		 
			
			RowSet rs = null;
			String typeId = "";
			try {
				String	sql = "select DESCRIPTION from LKUP_ATTACHMENT_TYPE where TYPE_ID ="+attachmentTypeId;
			
				logger.debug(sql);
				 rs = new Wrapper().select(sql);

				if (rs.next()) {
					typeId = rs.getString("DESCRIPTION");
				}

				return typeId;
			} catch (Exception e) {
				logger.error("", e);				
			}finally {
				try{
					if(rs!= null){
						rs.close();
					}
				}catch (Exception e) {
					logger.error(""+e);
				}
			}
			return typeId;
		}
		
		public int getAttachmentTypeId() throws AgentException {
			logger.debug("getAttachmentTypeId()");
			
			int typeId = 0;
			RowSet rs = null;
			try {
				String	sql = "select max(type_id) as typeId from LKUP_ATTACHMENT_TYPE";
			
				logger.debug(sql);
				 rs = new Wrapper().select(sql);

				if (rs.next()) {
					typeId = rs.getInt("typeId");
				}

				return typeId;
			} catch (Exception e) {
				logger.error("", e);
				throw new AgentException("", e);
			}finally {
				try{
					if(rs!= null){
						rs.close();
					}
				}catch (Exception e) {
					logger.error(""+e);
				}
			}
		}
		
		public String getActivityTypesData() throws Exception{
			logger.info("getActivityTypesData()");

			JSONObject json = new JSONObject();
			JSONArray array = new JSONArray();
			
			RowSet rs = null;

			try {
				String sql = "select TYPE_ID,DESCRIPTION from lkup_act_type order by DESCRIPTION asc";
				logger.debug(sql);
				rs = new Wrapper().select(sql);

				while (rs.next()) {				
					JSONObject item = new JSONObject();
					item.put("typeId", rs.getInt("TYPE_ID"));
					item.put("description", rs.getString("DESCRIPTION"));				
					array.put(item);
				}
				json.put("activityTypesData", array);
				
			} catch (Exception e) {
				logger.error("while getting activity types error :"+e.getMessage());
				throw e;
			}finally {
				try {
					if(rs != null) {
						rs.close();
					}
				}catch(Exception e) {
					
				}
			}
			
			logger.info("json Data = "+json.toString());
			return json.toString();
		}
		public void updateInspectionCodes(InspectionCode[] inspectionCodeArray) throws AgentException {
			if (inspectionCodeArray != null && inspectionCodeArray.length > 0) { 
				// Wrapper
				Wrapper wrapper = new Wrapper();
				String updateSql = "";  
				String insertSql = "";
				wrapper.beginTransaction();
				try {
					for (int i = 0; i < inspectionCodeArray.length; i++) {
						if (inspectionCodeArray[i].getCode() == 0) {
							int inspCode = wrapper.getNextId("INSP_ACTN_CODE");
							/*insertSql = "insert into LKUP_INSPCT_CODE (ACTN_CODE,DESCRIPTION, MODULE_ID, DEPT_ID, ACTIVE ) values ("
									+ inspCode + ", '" + inspectionCodeArray[i].getDescription() + "', "
									+ inspectionCodeArray[i].getModuleId() + ", " + inspectionCodeArray[i].getDeptId()
									+ ", '" + inspectionCodeArray[i].getActive() + "')";
							wrapper.addBatch(insertSql);*/
							logger.debug("insertSql : "+insertSql);
						} else {
							/*updateSql = "update LKUP_INSPCT_CODE set DESCRIPTION = '"
									+ inspectionCodeArray[i].getDescription() + "',MODULE_ID= "
									+ inspectionCodeArray[i].getModuleId() + ", DEPT_ID = "
									+ inspectionCodeArray[i].getDeptId() + ", ACTIVE = '"
									+ inspectionCodeArray[i].getActive() + "' where ACTN_CODE = "
									+ inspectionCodeArray[i].getCode();
							wrapper.addBatch(updateSql);
							logger.debug("updateSql : "+updateSql);*/
						}
					}
					wrapper.executeBatch();
				} catch (Exception e) {
					logger.error("", e);
					throw new AgentException("", e);
				}
			}
		}
		public List<InspectionCode> getInspectionCodes() throws AgentException {
			List<InspectionCode> inspCodes = new ArrayList<InspectionCode>();

			String sql = "select lic.*,lm.DESCRIPTION as module_desc from LKUP_INSPCT_CODE lic "
					+ "left join lkup_module lm on lic.MODULE_ID = lm.ID "
					+ "order by lic.DESCRIPTION ";
			logger.debug("query to get inspection codes::" + sql);
			RowSet rs = null;
			try {
				rs = new Wrapper().select(sql);
				InspectionCode inspCode = null;
				if (rs != null) {
					while (rs.next()) {
						inspCode = new InspectionCode();
						inspCode.setCode(rs.getInt("ACTN_CODE"));
						inspCode.setDescription(rs.getString("DESCRIPTION"));
					/*	inspCode.setModuleId(rs.getString("MODULE_ID"));
						inspCode.setDeptId(rs.getString("DEPT_ID"));
						
						inspCode.setActive(rs.getString("ACTIVE"));*/
						inspCodes.add(inspCode);
					}
				}
			} catch (Exception e) {
				logger.error("", e);
				throw new AgentException("", e);
			} // Closing RowSet added by Arjun
			finally {
				if (rs != null) {
					try {
						rs.close();
					} catch (SQLException Ignore) {
						logger.warn("Not able to close the Rowset");
					}
				}
			}

			return inspCodes;
		}
		public List getInspCodes(InspectionCodeForm inspectionCodeForm) {
			logger.info("getInspCodes(inspectionCodeForm)");
			List inspCodes = new ArrayList();

			try {
				String sql = "";


				if (inspectionCodeForm.isActive() == true)
					sql = sql + " LIC.ACTIVE = 'Y' AND";

				sql = ((inspectionCodeForm.getModuleId() == null) || inspectionCodeForm.getModuleId().equals("-1")) ? sql : (sql + " LIC.MODULE_ID = " + StringUtils.nullReplaceWithEmpty(inspectionCodeForm.getModuleId()) + " AND");

				sql = ((inspectionCodeForm.getDeptId() == null) || inspectionCodeForm.getDeptId().equals("-1")) ? sql : (sql + " LIC.DEPT_ID = " + StringUtils.nullReplaceWithEmpty(inspectionCodeForm.getDeptId() ) + " AND");

				sql = ((inspectionCodeForm.getDescription() == null) || inspectionCodeForm.getDescription().equals("")) ? sql : (sql + " UPPER(LIC.DESCRIPTION) LIKE '%" + inspectionCodeForm.getDescription().toUpperCase() + "%' AND");

				if ((sql != null) && !("").equalsIgnoreCase(sql)) {
					sql = sql.substring(0, sql.length() - 4); // to remove the trailing
					sql = " WHERE " + sql;
				}

				sql = " select LIC.ACTN_CODE,LIC.DESCRIPTION, LM.DESCRIPTION AS MODULE_DESC, D.DESCRIPTION AS DEPARTMENT,LIC.ACTIVE from LKUP_INSPCT_CODE LIC JOIN LKUP_MODULE LM ON LM.ID=LIC.MODULE_ID JOIN DEPARTMENT D ON D.DEPT_ID=LIC.DEPT_ID " + sql + " ";
				logger.info(sql);

				RowSet rs = new Wrapper().select(sql);

				while (rs.next()) {
					DisplayItem items = new DisplayItem();
					items.setFieldOne(StringUtils.nullReplaceWithEmpty(rs.getString("ACTN_CODE")));
					items.setFieldTwo(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));

					if (inspStatusRemovable(rs.getString("ACTN_CODE"))) {
						items.setFieldSix("Y");
					} else {
						items.setFieldSix("N");
					}

					items.setFieldThree(StringUtils.nullReplaceWithEmpty(rs.getString("MODULE_DESC")));
					items.setFieldFour(StringUtils.nullReplaceWithEmpty(rs.getString("DEPARTMENT")));
					items.setFieldFive(StringUtils.nullReplaceWithEmpty(rs.getString("ACTIVE")));
					inspCodes.add(items);
				} // end of while loop

				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return inspCodes;
		}
		public boolean inspStatusRemovable(String statusid) {
			String sql = "select COUNT(*) as COUNT from INSPECTION where ACTN_CODE=" + statusid + "";
			boolean removable = false;

			try {
				RowSet rs = new Wrapper().select(sql);

				if (rs.next()) {
					if (rs.getInt("COUNT") == 0) {
						removable = true;
					} else {
						removable = false;
					}
				}

				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Sql error is inspStatusRemovable() " + e.getMessage());
			} catch (Exception e) {
				logger.error("General error is inspStatusRemovable() " + e.getMessage());
			}

			return removable;
		}
		
		public InspectionCodeForm getInspectionCode(int actnCode, InspectionCodeForm inspectionCodeForm) {
			logger.info("getInspectionCode(" + actnCode + ")");

			try {
				String sql = "SELECT * FROM LKUP_INSPCT_CODE WHERE ACTN_CODE=" + actnCode;
				logger.info(sql);

				RowSet rs = new Wrapper().select(sql);

				if (rs.next()) {
					inspectionCodeForm.setCode(rs.getInt("ACTN_CODE"));
					inspectionCodeForm.setDescription(rs.getString("DESCRIPTION"));
					inspectionCodeForm.setDeptId(rs.getString("DEPT_ID"));
					inspectionCodeForm.setModuleId(rs.getString("MODULE_ID"));
					inspectionCodeForm.setActive(StringUtils.s2b(rs.getString("ACTIVE")));
				}

				rs.close();
			} catch (Exception e) {
				logger.error("Error in get activity type " + e.getMessage());
			}

			return inspectionCodeForm;
		}
		
		public int saveInspCode(int code, String description, boolean active, String deptId, String moduleId) {
			logger.info("saveInspCode(" + code + "," + description + ", " + active + "," + deptId + "," + moduleId + ")");

			Wrapper db = new Wrapper();
			int result = 0;
			String sql = "";

			try {
				sql = "SELECT * FROM LKUP_INSPCT_CODE WHERE ACTN_CODE=" + code;
				logger.info(sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					sql = "UPDATE LKUP_INSPCT_CODE SET MODULE_ID=" + moduleId + ",DESCRIPTION=" + StringUtils.checkString(description) + ", ACTIVE=" + StringUtils.checkString(StringUtils.b2s(active)) + ",  DEPT_ID=" +deptId + " WHERE ACTN_CODE=" + code + " ";
					
					logger.info(sql);
					result = 2;
					db.update(sql);
				} else {

					sql = "INSERT INTO LKUP_INSPCT_CODE (ACTN_CODE,DESCRIPTION,MODULE_ID,DEPT_ID,ACTIVE) VALUES ((select MAX(ACTN_CODE)+1 AS ACTN_CODE from LKUP_INSPCT_CODE) ";
					sql = sql + ",";
					sql = sql + StringUtils.checkString(description);
					sql = sql + ",";
					sql = sql + moduleId;
					sql = sql + ",";
					sql = sql + deptId;
					sql = sql + ",";
					sql = sql + StringUtils.checkString(StringUtils.b2s(active));
					sql = sql + ")";
					result = 1;
					db.insert(sql);
					logger.info("sql :: " + sql);

				}
			} catch (Exception e) {
				result = -1;
				logger.error("Error in saveActivityStatus() method" + e.getMessage());
			}
			return result;
		}
		public int deleteInspCodes(String codeId) {
			logger.debug("Entered into deleteInspCodes...");
			int result = 0;
			String sql = "";

			String[] idArray = {};

			idArray = codeId.split(",");
			logger.debug("id List " + idArray.length);
			for (int i = 0; i < idArray.length; i++) {
				sql = "DELETE FROM LKUP_INSPCT_CODE WHERE ACTN_CODE=" + idArray[i];
				logger.info(sql);
				result = 3;
				try {
					new Wrapper().update(sql);
				} catch (Exception e) {
					result = -1;
					logger.error("General error is deleteInspCodes() " + e.getMessage());
				}
			}

			return result;
		}
		
		public List getInspItems(InspectionItemForm inspectionItemForm) {
			logger.info("getInspItems(inspectionItemForm)");
			List inspCodes = new ArrayList();

			try {
				String sql = "";


				sql = ((inspectionItemForm.getCode()==0 ) || inspectionItemForm.getCode()==-1) ? sql : (sql + " LII.INSPECTION_ITEM_CODE = " + inspectionItemForm.getCode() + " AND");

				sql = ((inspectionItemForm.getActType() == null) || inspectionItemForm.getActType().equals("-1")) ? sql : (sql + " LII.ACT_TYPE = " + StringUtils.checkString(StringUtils.nullReplaceWithEmpty(inspectionItemForm.getActType()) ) + " AND");

				sql = ((inspectionItemForm.getDescription() == null) || inspectionItemForm.getDescription().equals("")) ? sql : (sql + " UPPER(LII.DESCRIPTION) LIKE '%" + inspectionItemForm.getDescription().toUpperCase() + "%' AND");

				if ((sql != null) && !("").equalsIgnoreCase(sql)) {
					sql = sql.substring(0, sql.length() - 4); // to remove the trailing
					sql = " WHERE " + sql;
				}

				sql = " select LII.INSPCT_ITEM_ID,LII.DESCRIPTION,LAT.DESCRIPTION AS ACT_TYPE,LII.INSPECTION_ITEM_CODE from LKUP_INSPCT_ITEM LII JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE=LII.ACT_TYPE  " + sql + " ";
				logger.info(sql);

				RowSet rs = new Wrapper().select(sql);

				while (rs.next()) {
					DisplayItem items = new DisplayItem();
					items.setFieldOne(StringUtils.nullReplaceWithEmpty(rs.getString("INSPCT_ITEM_ID")));
					items.setFieldTwo(StringUtils.nullReplaceWithEmpty(rs.getString("DESCRIPTION")));

					if (inspItemRemovable(rs.getString("INSPCT_ITEM_ID"))) {
						items.setFieldSix("Y");
					} else {
						items.setFieldSix("N");
					}

					items.setFieldThree(StringUtils.nullReplaceWithEmpty(rs.getString("ACT_TYPE")));
					items.setFieldFour(StringUtils.nullReplaceWithEmpty(rs.getString("INSPECTION_ITEM_CODE")));
					inspCodes.add(items);
				} // end of while loop

				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
			}

			return inspCodes;
		}
		
		public boolean inspItemRemovable(String itemid) {
			String sql = "select COUNT(*) as COUNT from INSPECTION where INSPCT_ITEM_ID=" + itemid + "";
			boolean removable = false;

			try {
				RowSet rs = new Wrapper().select(sql);

				if (rs.next()) {
					if (rs.getInt("COUNT") == 0) {
						removable = true;
					} else {
						removable = false;
					}
				}

				if (rs != null) {
					rs.close();
				}
			} catch (SQLException e) {
				logger.error("Sql error is inspItemRemovable() " + e.getMessage());
			} catch (Exception e) {
				logger.error("General error is inspItemRemovable() " + e.getMessage());
			}

			return removable;
		}
		public InspectionItemForm getInspectionItem(int id, InspectionItemForm inspectionItemForm) {
			logger.info("getInspectionItem(" + id + ")");
			RowSet rs = null;
			try {
				String sql = "SELECT * FROM LKUP_INSPCT_ITEM WHERE INSPCT_ITEM_ID=" + id;
				logger.info(sql);

				 rs = new Wrapper().select(sql);

				if (rs.next()) {
					inspectionItemForm.setId(rs.getInt("INSPCT_ITEM_ID"));
					inspectionItemForm.setDescription(rs.getString("DESCRIPTION"));
					inspectionItemForm.setActType(rs.getString("ACT_TYPE"));
					inspectionItemForm.setCode(rs.getInt("INSPECTION_ITEM_CODE"));
					
				}

			} catch (Exception e) {
				logger.error("Error in get activity type " + e.getMessage());
			}finally {
				try{
				if(rs!=null){
					rs.close();
				}
				}catch (Exception e) {
					logger.error("",e);
				}
			}

			return inspectionItemForm;
		}
		public int saveInspItem(int id, String description, String actType, int code) {
			logger.info("saveInspItem(" + id + "," + description + ", " + actType + "," + code+")");

			Wrapper db = new Wrapper();
			int result = 0;
			String sql = "";

			try {
				sql = "SELECT * FROM LKUP_INSPCT_ITEM WHERE INSPCT_ITEM_ID=" + id;
				logger.info(sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					sql = "UPDATE LKUP_INSPCT_ITEM SET ACT_TYPE=" + StringUtils.checkString(actType) + ",DESCRIPTION=" + StringUtils.checkString(description) + ", INSPECTION_ITEM_CODE=" + code + " WHERE INSPCT_ITEM_ID=" + id + " ";
					
					logger.info(sql);
					result = 2;
					db.update(sql);
				} else {

					sql = "INSERT INTO LKUP_INSPCT_ITEM (INSPCT_ITEM_ID,DESCRIPTION,ACT_TYPE,INSPECTION_ITEM_CODE) VALUES ((select MAX(INSPCT_ITEM_ID)+1 AS INSPCT_ITEM_ID from LKUP_INSPCT_ITEM) ";
					sql = sql + ",";
					sql = sql + StringUtils.checkString(description);
					sql = sql + ",";
					sql = sql + StringUtils.checkString(actType);
					sql = sql + ",";
					sql = sql + code;
					sql = sql + ")";
					result = 1;
					db.insert(sql);
					logger.info("sql :: " + sql);

				}
			} catch (Exception e) {
				result = -1;
				logger.error("Error in saveInspItem() method" + e.getMessage());
			}
			return result;
		}
		public int deleteInspItems(String id) {
			logger.debug("Entered into deleteInspItems...");
			int result = 0;
			String sql = "";

			String[] idArray = {};

			idArray = id.split(",");
			logger.debug("id List " + idArray.length);
			for (int i = 0; i < idArray.length; i++) {
				sql = "DELETE FROM LKUP_INSPCT_ITEM WHERE INSPCT_ITEM_ID=" + idArray[i];
				logger.info(sql);
				result = 3;
				try {
					new Wrapper().update(sql);
				} catch (Exception e) {
					result = -1;
					logger.error("General error is deleteInspItems() " + e.getMessage());
				}
			}

			return result;
		}
		
		public List<Transaction> getTransactionList(String permitNumber) throws AgentException{
			logger.debug("entered getTransactionList");
			List<Transaction> transactionList = new ArrayList<Transaction>();
			Wrapper wrapper = new Wrapper();
			
			String sql = " SELECT * FROM ONLINE_PAYMENT_PERMITS "
						+ "WHERE ACT_ID = (SELECT ACT_ID FROM ACTIVITY WHERE ACT_NBR="+StringUtils.checkString(permitNumber) +")";
			
			logger.debug(sql);
			javax.sql.RowSet  rs = null;
			try {
				rs = wrapper.select(sql);
				
				if(rs != null) {
					while(rs.next()) {
						Transaction transaction = new Transaction();
						transaction.setActId(rs.getInt("ACT_ID"));
						transaction.setAmount(rs.getDouble("AMOUNT"));
						transaction.setAuthCode(rs.getString("AUTH_CODE"));
						transaction.setCreated(rs.getString("CREATED"));
						transaction.setCreatedBy(rs.getString("CREATED_BY"));
						transaction.setOnlinePayId(rs.getInt("ONLINE_PAY_ID"));
						transaction.setPaymentId(rs.getInt("PAYMENT_ID"));
						transaction.setPermitNumber(permitNumber);
						transaction.setResCode(rs.getString("RESPONSE_CODE"));
						transaction.setTxnDesc(rs.getString("STATUS_DESC"));
						transaction.setTxnNumber(rs.getString("ONLINETXN_ID"));
						transaction.setTxnStatus(rs.getString("TRANSACTION_STATUS"));
						transaction.setTxnType(rs.getString("TRANSACTION_TYPE"));
						transactionList.add(transaction);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			logger.debug("fetched getTransactionList as "+transactionList.size());
			return transactionList;
		}
}
    
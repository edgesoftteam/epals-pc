package elms.agent;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.FeeEdit;
import elms.app.admin.PayType;
import elms.app.finance.ActivityFee;
import elms.app.finance.ActivityFeeEdit;
import elms.app.finance.Fee;
import elms.app.finance.Finance;
import elms.app.finance.FinanceSummary;
import elms.app.finance.LedgerEdit;
import elms.app.finance.LookupFee;
import elms.app.finance.Payment;
import elms.app.finance.PaymentDetail;
import elms.app.finance.PaymentDetailEdit;
import elms.app.finance.RevenueEdit;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.beans.LookupFeeEdit;
import elms.control.beans.LookupFeesForm;
import elms.control.beans.finance.PostRevenueForm;
import elms.exception.AgentException;
import elms.exception.PaymentException;
import elms.exception.PaymentNotFoundException;
import elms.security.User;
import elms.util.Operator;
import elms.util.StringUtils;
import elms.util.db.Wrapper;
import sun.jdbc.rowset.CachedRowSet;

public class FinanceAgent implements Serializable {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(FinanceAgent.class.getName());
	Wrapper db = new Wrapper();

	/**
	 * Constructor for FinanceAgent
	 */
	public FinanceAgent() {
		super();

	}

	/**
	 * Populates the finance object from the rowset.
	 * 
	 * @param rs
	 * @return
	 */
	private Finance populateFinance(RowSet rs) throws Exception {
		Finance finance = new Finance();

		try {
			String type = rs.getString("fee_fl_4");

			if (type.equals("2")) {
				finance.setType('P');
			} else if (type.equals("1")) {
				finance.setType('B');
			} else if (type.equals("5")) {
				finance.setType('X');
			} else if (type.equals("3")) {
				finance.setType('D');
			} else if (type.equals("4")) {
				finance.setType('F');
			} else {
				return finance;
			}

			finance.setFeeAmt(rs.getDouble("FeeAmt"));
			finance.setPaidAmt(rs.getDouble("PaidAmt"));
			finance.setCreditAmt(rs.getDouble("CreditAmt"));
			finance.setAdjustmentAmt(rs.getDouble("AdjustmentAmt"));
			finance.setBouncedAmt(rs.getDouble("BouncedAmt"));
			finance.setAmountDue();

			logger.info("Exiting FinanceAgent getFinance()" + finance.getType() + "::" + finance.getFeeAmt());

			return finance;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent populateFinance() ");
			throw e;
		}
	}

	/**
	 * This method returns an ArrayList of Finance objects based on the sql query that is passed to it. It is used by the getActivityFinance, getSubProjectFinance and getProjectFinance methods
	 */
	public FinanceSummary getFinance(String sql) throws Exception {
		logger.info("Entering FinanceAgent getFinance() : " + sql);
		FinanceSummary financeSummary = new FinanceSummary();

		try {
			RowSet rs = db.select(sql);

			Finance total = new Finance('T');

			while (rs.next()) {
				Finance finance = populateFinance(rs);
				total.add(finance);
				financeSummary.add(finance);
			}

			financeSummary.add(total);
			logger.info("Exiting FinanceAgent getFinance()");

			return financeSummary;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFinance() " + e.getMessage());
			throw e;
		}
	}

	/**
	 * This method gets the deposit history
	 * 
	 * @param peopleId
	 * @return
	 * @throws Exception
	 */
	public List getDepositHistory(int peopleId) throws Exception {
		List depHistory = new ArrayList();
		try {
			String sql = "select distinct p.pymnt_id,p.pymnt_dt,p.pymnt_type,p.pymnt_amnt,a.act_nbr,p.pymnt_method,p.accnt_no,p.enter_by_id,p.comnt,p.post_date " + " from (payment p left outer join payment_detail pd on p.pymnt_id = pd.pymnt_id) left outer join activity a on pd.act_id = a.act_id where (p.people_id = " + peopleId + " and p.pymnt_method ='deposit') or (p.people_id = " + peopleId + " and p.pymnt_type in (7,8)) " + " order by p.pymnt_dt desc,p.pymnt_id desc";

			double depositBalance = 0;
			depositBalance = getDepositBalance(peopleId);

			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				PayType payType = new PayType();
				User user = new User();
				Payment payment = new Payment();
				payment.setPeopleId(peopleId);
				payment.setStrPaymentDate(rs.getString("PYMNT_DT"));
				payment.setStrAmnt(StringUtils.dbl2$(rs.getDouble("PYMNT_AMNT")));
				payment.setAccountNbr(rs.getString("ACCNT_NO"));
				payType = LookupAgent.getPayTypeDescription(rs.getInt("PYMNT_TYPE"));
				payment.setType(payType);

				String method = "";

				try {
					method = rs.getString("PYMNT_METHOD");

					if (method.equalsIgnoreCase("cash")) {
						method = "Cash";
					}

					if (method.equalsIgnoreCase("check")) {
						method = "Check";
					}

					if (method.equalsIgnoreCase("card")) {
						method = "Card";
					}

					if (method.equalsIgnoreCase("corretion")) {
						method = "Correction";
					}

					if (method.equalsIgnoreCase("transferIn")) {
						method = "Transfer In";
					}

					if (method.equalsIgnoreCase("nonRevenue")) {
						method = "Non-Revenue";
					}
				} catch (Exception e) {
					logger.debug("Error on payment method");
				}

				payment.setMethod(method);
				user = LookupAgent.getUserDetails(StringUtils.i2s(rs.getInt("ENTER_BY_ID")));

				payment.setEnteredBy(user);
				payment.setStrPostDate(rs.getString("POST_DATE"));
				payment.setActNbr(rs.getString("ACT_NBR"));
				payment.setPaymentId(StringUtils.s2i(rs.getString("PYMNT_ID")));
				payment.setComment(rs.getString("COMNT"));
				payment.setDepositBalance(StringUtils.dbl2$(depositBalance));
				depositBalance = rs.getDouble("PYMNT_AMNT");
				depHistory.add(payment);
			}

			return depHistory;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getDepositHistory() " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Actvity Finance
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public FinanceSummary getActivityFinance(int activityId) throws Exception {
		logger.info("Entering getActivityFinance() with id " + activityId);

		FinanceSummary finance = new FinanceSummary();

		try {
			String sql = "select fee_fl_4,sum(fee_amnt) as FeeAmt,sum(fee_paid) as PaidAmt,sum(fee_credit) as CreditAmt,sum(fee_adjustment) as AdjustmentAmt,sum(bounced_amnt) as BouncedAmt,0 As AmountDue  from activity_fee a join fee f on f.fee_id=a.fee_id  where fee_fl_4 in ('1','2','3','4','5') and activity_id= " + activityId + " group by fee_fl_4";
			logger.debug(sql);
			finance = getFinance(sql);
			logger.info("Exiting FinanceAgent getActivityFinance()");

			return finance;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getActivityFinance() " + e.getMessage());
			throw e;
		}
	}

	public FinanceSummary getSubProjectFinance(int subProjectId) throws Exception {
		logger.info("Entering getSubProjectFinance() with id " + subProjectId);

		FinanceSummary finance = new FinanceSummary();

		try {
			String sql = "select fee_fl_4,sum(fee_amnt) as FeeAmt,sum(fee_paid) as PaidAmt,sum(fee_credit) as CreditAmt,sum(fee_adjustment) as AdjustmentAmt,sum(bounced_amnt) as BouncedAmt,0 As AmountDue from activity a,activity_fee af join fee f on f.fee_id=af.fee_id where fee_fl_4 in ('1','2','3','4','5') and a.act_id=af.activity_id and a.sproj_id=" + subProjectId + " group by fee_fl_4";
			finance = getFinance(sql);
			logger.info("Exiting FinanceAgent getSubProjectFinance()");

			return finance;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getSubProjectFinance() " + e.getMessage());
			throw e;
		}
	}

	public FinanceSummary getProjectFinance(int projectId) throws Exception {
		logger.info("Entering getProjectFinance() with id " + projectId);

		FinanceSummary finance = new FinanceSummary();

		try {
			String sql = "select fee_fl_4,sum(fee_amnt) as FeeAmt,sum(fee_paid) as PaidAmt,sum(fee_credit) as CreditAmt,sum(fee_adjustment) as AdjustmentAmt,sum(bounced_amnt) as BouncedAmt,0 As AmountDue " + "from sub_project s,activity a,activity_fee af join fee f on f.fee_id=af.fee_id " + "where fee_fl_4 in ('1','2','3','4','5') and a.act_id=af.activity_id " + "and s.sproj_id=a.sproj_id " + "and s.proj_id=" + projectId + " group by fee_fl_4";
			finance = getFinance(sql);
			logger.info("Exiting FinanceAgent getProjectFinance()");

			return finance;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getProjectFinance() " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Payment history
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public RowSet getPaymentHistory(int activityId) throws Exception {
		logger.info("getPaymentHistory(" + activityId + ")");

		RowSet rs = null;

		try {
			int onlineUserId = new ActivityAgent().getActivityCreatedBy(activityId);
			if(activityId > 0 && onlineUserId >0){
				String sql = "select p.*,lpt.description as pay_type,e.firstname || ' ' || e.lastname as entered_by,p.AUTHORIZATION_ID as authorized_by,v.first_name || ' ' || v.last_name as voided_by,e.firstname || ' ' || e.lastname as people_name from ((((payment p left outer join ext_user e on e.EXT_USER_ID=p.enter_by_id) left outer join users v on v.userid=p.void_by) left outer join lkup_pay_type lpt on p.pymnt_type=lpt.pay_type_id) left outer join people pp on p.people_id = pp.people_id)  where p.pymnt_id in (select distinct pymnt_id from payment_detail where act_id = " + activityId + ") order by p.pymnt_id desc";
				logger.info(sql);
				rs = db.select(sql);
				logger.info("Exiting getPaymentHistory()");
				
			} else {
				String sql = "select p.*,lpt.description as pay_type,e.first_name || ' ' || e.last_name as entered_by,p.AUTHORIZATION_ID as authorized_by,v.first_name || ' ' || v.last_name as voided_by,pp.name || ' ' || pp.last_name as people_name from ((((payment p left outer join users e on e.userid=p.enter_by_id) left outer join users v on v.userid=p.void_by) left outer join lkup_pay_type lpt on p.pymnt_type=lpt.pay_type_id) left outer join people pp on p.people_id = pp.people_id)  where p.pymnt_id in (select distinct pymnt_id from payment_detail where act_id = " + activityId + ") order by p.pymnt_id desc";
				logger.info(sql);
				rs = db.select(sql);
				logger.info("Exiting getPaymentHistory()");
			}
			return rs;
		} catch (Exception e) {
			logger.error("Error:" + e.getMessage());
			throw e;
		}
	}

	public RowSet getPaymentDetailHistory(int paymentId) throws Exception {
		logger.info("getPaymentDetailHistory(" + paymentId + ")");

		RowSet rs = null;

		try {
			String sql = "select pd.trans_id,pd.amnt,f.fee_desc,f.fee_account,f.fee_fl_4,pd.comments,pd.fee_account " + " from payment_detail pd left outer join fee f on f.fee_id=pd.fee_id where pd.pymnt_id = " + paymentId + " order by f.fee_sequence";
			logger.info(sql);
			rs = db.select(sql);
			logger.info("Exiting getPaymentHistory()");

			return rs;
		} catch (Exception e) {
			logger.error("Error:" + e.getMessage());
			throw e;
		}
	}

	/*
	 * Update Activity Permit Fee & Plan Check Fee Dates
	 */
	public void setActivityDates(int activityId, String planCheckFeeDate, String developmentFeeDate, String permitFeeDate) throws Exception {
		logger.info("setActivityDates(" + activityId + ", " + planCheckFeeDate + ", " + developmentFeeDate + ", " + permitFeeDate + ")");

		Connection connection = db.getConnectionForPreparedStatementOnly();
		Statement statement = connection.createStatement();
		connection.setAutoCommit(false);
		String sql = "";

		try {
			// Get the list of fees that are attached to the activity. The fee_ids need to be updated to match the new fee dates.
			sql = "select a.activity_id,a.fee_id,f.act_type,f.fee_fl_4,f.fee_sequence,f.FEE_DESC from activity_fee a join fee f on a.fee_id=f.fee_id and activity_id=" + activityId + " and fee_fl_4 in ('0','1','2','3')";
			logger.info(sql);

			RowSet rsFees = db.select(sql.toString());

			// Update the dates in the activity
			sql = "update activity set UPDATED=CURRENT_TIMESTAMP,plan_chk_fee_date=" + StringUtils.toOracleDate(planCheckFeeDate) + ",development_fee_date=" + StringUtils.toOracleDate(developmentFeeDate) + ",permit_fee_date=" + StringUtils.toOracleDate(permitFeeDate) + " where act_id=" + activityId;
			logger.info(sql);
			statement.addBatch(sql);

			while (rsFees.next()) {
				//sql = "update activity_fee set fee_id = (select max(fee_id) from fee where act_type = " + StringUtils.checkString(rsFees.getString("ACT_TYPE")) + "	and fee_sequence = " + StringUtils.checkNumber(rsFees.getString("FEE_SEQUENCE")) + "	and ((fee_fl_4 in ('0','2') and (fee_creation_dt <= " + StringUtils.toOracleDate(permitFeeDate) + ")) or (fee_fl_4 = '1' and (fee_creation_dt <= " + StringUtils.toOracleDate(planCheckFeeDate) + ")) or (fee_fl_4 = '3' and (fee_creation_dt <= " + StringUtils.toOracleDate(developmentFeeDate) + ")))" + "	) where activity_id = " + StringUtils.checkNumber(rsFees.getString("activity_id")) + "	and fee_id = " + StringUtils.checkNumber(rsFees.getString("fee_id"));
				sql = "update activity_fee set fee_id = (select max(fee_id) from fee where act_type = " + StringUtils.checkString(rsFees.getString("ACT_TYPE")) + "	and FEE_DESC = " + StringUtils.checkString(rsFees.getString("FEE_DESC")) + " and fee_fl_4="+rsFees.getString("fee_fl_4")+"	and ((fee_fl_4 in ('0','2') and (fee_creation_dt <= " + StringUtils.toOracleDate(permitFeeDate) + ")) or (fee_fl_4 = '1' and (fee_creation_dt <= " + StringUtils.toOracleDate(planCheckFeeDate) + ")) or (fee_fl_4 = '3' and (fee_creation_dt <= " + StringUtils.toOracleDate(developmentFeeDate) + ")))" + "	) where activity_id = " + StringUtils.checkNumber(rsFees.getString("activity_id")) + "	and fee_id = " + StringUtils.checkNumber(rsFees.getString("fee_id"));
				logger.info(sql);
				statement.addBatch(sql);
			}

			logger.info("Updating fees");
			statement.executeBatch();
			connection.commit();
			logger.info("Exiting FinanceAgent setActivityDates()");
		} catch (Exception e) {
			connection.rollback();
			logger.error("Error in FinanceAgent setActivityDates() " + e.getMessage());
			throw e;
		} finally {
			try {
				if (statement != null)
					statement.close();
				if (connection != null)
					connection.close();
			} catch (Exception ignored) {
				// ignored
			}
		}
	}

	public ActivityDetail getActivityDetail(int activityId) throws Exception {
		logger.info("Entering getActivity() with id " + activityId);

		ActivityDetail activityDetail = new ActivityDetail();
		RowSet rs = null;
		try {

			String sql = "select * from activity where act_id=" + activityId;
			rs = db.select(sql);

			if (rs.next()) {
				activityDetail.setValuation(rs.getDouble("valuation"));
				activityDetail.setActivityFeeDate(StringUtils.dbDate2cal(rs.getString("permit_fee_date")));
				activityDetail.setPlanCheckFeeDate(StringUtils.dbDate2cal(rs.getString("plan_chk_fee_date")));
				activityDetail.setDevelopmentFeeDate(StringUtils.dbDate2cal(rs.getString("development_fee_date")));
				activityDetail.setPlanCheckRequired(rs.getString("plan_chk_req"));
				activityDetail.setDevelopmentFeeRequired(rs.getString("dev_fee_req"));
			}

			logger.info("Exiting FinanceAgent setActivityDates()");

			return activityDetail;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent setActivityDates() " + e.getMessage());
			throw e;
		}
	}

	public RowSet getCurrentFeeList(int activityId) throws Exception {
		logger.info("Entering getFeeList() with id " + activityId);

		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			sql = "SELECT U.LAND_ID FROM (LAND_USAGE U JOIN V_LSO_LAND L ON U.LSO_USE_ID=" + commercialUse + " AND L.LAND_ID=U.LAND_ID) JOIN V_PSA_LIST P ON P.LSO_ID=L.LSO_ID WHERE P.ACT_ID=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {
				useSql = "((f.lso_use is null) or (f.lso_use = " + commercialUse + "))";
			} else {
				useSql = "((f.lso_use is null) or (f.lso_use != " + commercialUse + "))";
			}

			rs = new CachedRowSet();
			sql = "select a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked,f.fee_desc,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment,0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit,af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit from (fee f join activity a on  a.act_type=f.act_type  and fee_calc_1 is not null  and ((fee_fl_4 in ('0','2','4') and (f.fee_creation_dt <= a.permit_fee_date )) or (fee_fl_4 in '1' and (f.fee_creation_dt <= a.plan_chk_fee_date )) or (fee_fl_4 in '3' and (f.fee_creation_dt <= a.development_fee_date ))) and ((fee_fl_4 in ('0','2','4') and (f.fee_expiration_dt >= a.permit_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '1' and (f.fee_expiration_dt >= a.plan_chk_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '3' and (f.fee_expiration_dt >= a.development_fee_date or f.fee_expiration_dt is null))) and a.act_id =" + activityId + ")" + " left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id and af.fee_amnt > 0.0" + " where (f.fee_pc in ('P','C','B','X','D'))" + " and " + useSql + " and f.FEE_EXPIRATION_DT is null order by f.fee_sequence";
			logger.info(sql);
			rs = db.select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get All Permit & Plan Check Fees that may be applicable for an activity .
	 * 
	 * feeChecked 0 - fee not selected for this activity feeChecked 1 - fee has been selected for this activity
	 * 
	 * fee_pc flag P - Permit Fee C - Plan Check Fees B - Both Plan Check and Permit
	 * 
	 * feeAmount is the calculated fee amount based on the number of units (feeUnits)
	 */
	public RowSet getFeeList(int activityId) throws Exception {
		logger.info("Entering getFeeList() with id " + activityId);

		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			sql = "SELECT U.LAND_ID FROM (LAND_USAGE U JOIN V_LSO_LAND L ON U.LSO_USE_ID=" + commercialUse + " AND L.LAND_ID=U.LAND_ID) JOIN V_PSA_LIST P ON P.LSO_ID=L.LSO_ID WHERE P.ACT_ID=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {
				useSql = "((f.lso_use is null) or (f.lso_use = " + commercialUse + "))";
			} else {
				useSql = "((f.lso_use is null) or (f.lso_use != " + commercialUse + "))";
			}

			rs = new CachedRowSet();
			sql = "select a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked,f.fee_desc,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment,0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit,af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit from (fee f join activity a on  a.act_type=f.act_type  and fee_calc_1 is not null  and ((fee_fl_4 in ('0','2','4') and (f.fee_creation_dt <= a.permit_fee_date )) or (fee_fl_4 in '1' and (f.fee_creation_dt <= a.plan_chk_fee_date )) or (fee_fl_4 in '3' and (f.fee_creation_dt <= a.development_fee_date ))) and ((fee_fl_4 in ('0','2','4') and (f.fee_expiration_dt >= a.permit_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '1' and (f.fee_expiration_dt >= a.plan_chk_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '3' and (f.fee_expiration_dt >= a.development_fee_date or f.fee_expiration_dt is null))) and a.act_id =" + activityId + ")" + " left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id and af.fee_amnt > 0.0" + " where (f.fee_pc in ('P','C','B','X','D'))" + " and " + useSql + " order by f.fee_sequence";
			logger.info(sql);
			rs = db.select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Gets the business tax fees
	 * 
	 * @param activityId
	 * @return
	 */
	public RowSet getBusinessTaxList(int activityId) {
		logger.info("Entering getBusinessTaxList() with id " + activityId);

		RowSet rs = null;
		String sql = "";
		String BLAccount = "001.CD000.41002.1001"; // this is the business license account number

		try {
			rs = new CachedRowSet();

			sql = "(select af.activity_id,af.fee_id, af.people_id, af.fee_valuation,af.comments,af.fee_amnt,f.fee_account,fee_paid,fee_credit,af.fee_adjustment,p.name,p.lic_no from activity_fee af join fee f on af.fee_id=f.fee_id,people p  where p.people_id=af.people_id and af.fee_id=0 and af.activity_id = " + activityId + ")  UNION  (select ap.act_id as activity_id,0 as fee_id, ap.people_id,0 as fee_valuation,'' as comments, 0 as fee_amnt," + StringUtils.checkString(BLAccount) + " as fee_account,0 as fee_paid,0 as fee_credit,0 as fee_adjustment,p.name,p.lic_no  from activity_people ap,people p  where ap.people_id = p.people_id and act_id = " + activityId + " and p.people_type_id = 1 and ap.people_id not in(select people_id  from activity_fee  where fee_id=0 and activity_id = " + activityId + ")) ";

			logger.info(sql);
			rs = db.select(sql);
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getBusinessTaxList() : sql : " + sql + ":" + e.getMessage());
		}

		logger.info("Exiting FinanceAgent getBusinessTaxList()");

		return rs;
	}

	public void calculateFeeAmount(Wrapper db, List activityFees) {
		double feeAmount = 0.0d;

		// create an array of subtotals and initialise all to zero
		double[] subTotals = new double[6];
		subTotals[0] = 0.0;
		subTotals[1] = 0.0;
		subTotals[2] = 0.0;
		subTotals[3] = 0.0;
		subTotals[4] = 0.0;
		subTotals[5] = 0.0;

		int aFeeSTFlg;

		for (int i = 0; i < activityFees.size(); i++) {
			ActivityFeeEdit activityFee = (ActivityFeeEdit) activityFees.get(i);

			feeAmount = calculateFeeAmount(db, activityFee, subTotals);

			if (!(activityFee.getSubtotalLevel() == null)) {
				aFeeSTFlg = StringUtils.s2i(activityFee.getSubtotalLevel());
				subTotals[aFeeSTFlg] += feeAmount;
				subTotals[aFeeSTFlg] = Double.parseDouble(new DecimalFormat("0.00").format(subTotals[aFeeSTFlg]));
			}
		}
	}

	public double calculateFeeAmount(Wrapper db, ActivityFeeEdit activityFeeEdit, double[] subTotals) {
		ActivityFee activityFee = new ActivityFee();

		activityFee.setActivityId(StringUtils.s2i(activityFeeEdit.getActivityId()));
		activityFee.setFeeId(StringUtils.s2i(activityFeeEdit.getFeeId()));
		activityFee.setFeeUnits(StringUtils.s2d(activityFeeEdit.getFeeUnits()));
		activityFee.setFeeAmount(StringUtils.s2d(activityFeeEdit.getFeeAmount()));
		activityFee.setFeePaid(StringUtils.s2d(activityFeeEdit.getFeePaid()));
		activityFee.setAdjustmentAmount(StringUtils.s2d(activityFeeEdit.getAdjustmentAmount()));
		activityFee.setCreditAmount(StringUtils.s2d(activityFeeEdit.getCreditAmount()));
		activityFee.setBouncedAmount(StringUtils.s2d(activityFeeEdit.getBouncedAmount()));
		activityFee.setFeeInit(StringUtils.s2i(activityFeeEdit.getFeeInit()));
		activityFee.setFeeFactor(StringUtils.s2bd(activityFeeEdit.getFeeFactor()));
		activityFee.setFactor(StringUtils.s2bd(activityFeeEdit.getFactor()));
		activityFee.setSubtotalLevel(StringUtils.s2i(activityFeeEdit.getSubtotalLevel()));

		calculateFeeAmount(db, activityFee);
		activityFeeEdit.setFeeAmount(StringUtils.dbl2$(activityFee.getFeeAmount()));

		return activityFee.getFeeAmount();
	}

	/**
	 * calculates the fee amount.
	 * 
	 * @param db
	 * @param activityFee
	 */
	public void calculateFeeAmount(Wrapper db, ActivityFee activityFee) {
		logger.info("calculateFeeAmount(Wrapper, " + activityFee + ")");

		double feeAmount = 0.00;
		double[] subTotals = {};
		int levelId = 0;
		double BUSINESS_TAX_MIN = 0;
		double BUSINESS_TAX_MAX = 0;
		double BUSINESS_TAX_PERCENT = 0;
		String sql = "";

		try {

			Map busTaxList = new HashMap();
			busTaxList = getBusTaxList();

			BUSINESS_TAX_MIN = StringUtils.s2d((String) busTaxList.get("busTaxMin"));
			BUSINESS_TAX_MAX = StringUtils.s2d((String) busTaxList.get("busTaxMax"));
			BUSINESS_TAX_PERCENT = StringUtils.s2d((String) busTaxList.get("busTax"));

			if (activityFee.getType() == 'B') { // Business Tax
				logger.debug("fee type is business tax");
				feeAmount = activityFee.getFeeValuation() * BUSINESS_TAX_PERCENT;

				if (feeAmount < BUSINESS_TAX_MIN) {
					feeAmount = BUSINESS_TAX_MIN;
					logger.debug("buiness tax set to minimum cap");
				} else if (feeAmount > BUSINESS_TAX_MAX) {
					feeAmount = BUSINESS_TAX_MAX;
					logger.debug("buiness tax set to maximum cap");
				} else {
					logger.debug("business tax set to the calculation");
				}

				logger.debug("Fee amount before calculation is " + feeAmount);
			} else {
				RowSet rs = new CachedRowSet();
				boolean calculateFee = true;

				sql = "select (fee_amnt-fee_paid) as balance from activity_fee where activity_id = " + activityFee.getActivityId() + " and fee_id = " + activityFee.getFeeId();
				logger.debug(sql);
				rs = db.select(sql);

				if (rs.next()) {
					if (rs.getDouble("BALANCE") != 0.00d) {
						logger.debug("balance is greater than 0.00d ,calculate fee = true");
						calculateFee = true;
					} else {
						logger.debug("balance is 0.00d,calculate fee = false");
						calculateFee = false;
					}
				} else {
					logger.debug("Resultset does not have any values, calculate fee = true");
					calculateFee = true;
				}

				sql = "select * from fee f,activity a where act_id = " + activityFee.getActivityId() + " and fee_id = " + activityFee.getFeeId();
				logger.debug(sql);
				rs = new CachedRowSet();
				rs = db.select(sql);

				if (rs.next()) {
					logger.info("Fee Calculate : " + activityFee.getFeeId() + ":" + rs.getString("fee_calc_1") + ": Fee Amount : " + activityFee.getFeeAmount() + ": Paid :" + activityFee.getFeePaid());

					// if ((rs.getInt("STATUS") == 2) || (rs.getInt("STATUS") == 3) || (rs.getInt("STATUS") == 4) || (rs.getInt("STATUS") == 30) || (rs.getInt("STATUS") == 31)) {
					if ((rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_CE_INACTIVE) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_CE_SUSPENSE) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BT_CALENDAR_YEAR_EXPIRED) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BT_OUT_OF_BUSINESS) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BL_OUT_OF_BUSINESS) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BL_FISCAL_YEAR_EXPIRED)) {
						// If the fee has already been paid and Activity Status=Issued then do not recalculate it.
						logger.debug("fee has already been paid and Activity Status=Issued then do not recalculate it");
						feeAmount = Double.parseDouble(new DecimalFormat("0.00").format(activityFee.getFeeAmount()));
						feeAmount = 0.0;
						activityFee.setFeeAmount(0.0);
						logger.debug("FeeAmount is : " + feeAmount);
					} else {
						switch (rs.getString("fee_calc_1").charAt(0)) {
						case 'A': // This is for fees that are based on number of units
							logger.debug("fee_calc_1=A,This is for fees that are based on number of units");

							if (rs.getDouble("fee_factor") > 0.00) {
								logger.debug("Fee factor is > 0.00, calculating fee amount");
								if (activityFee.getFeeUnits() <= 0) {
									feeAmount = rs.getDouble("fee_factor") * Math.max(0, activityFee.getFeeUnits());

								} else {
									feeAmount = rs.getDouble("fee_factor") * Math.max(1, activityFee.getFeeUnits());
								}
								logger.debug("calculated fee amount is " + feeAmount);
							}

							break;

						case 'B': // This is for fees that are based on number of units
							logger.debug("fee_calc_1=B,This is for fees that are based on number of units");
							feeAmount = rs.getDouble("factor") + (rs.getDouble("fee_factor") * activityFee.getFeeUnits());

							break;

						case 'C': // this is for a flat fee
							logger.debug("fee_calc_1=C,this is for a flat fee");
							feeAmount = rs.getDouble("factor");

							break;

						case 'D': // Max of factor and fee_factor*units
							logger.debug("fee_calc_1=D");
							feeAmount = Math.max(rs.getDouble("factor"), rs.getDouble("fee_factor") * activityFee.getFeeUnits());

							break;

						case 'E': // Min of factor and fee_factor*units
							logger.debug("fee_calc_1=E");
							feeAmount = Math.min(rs.getDouble("factor"), rs.getDouble("fee_factor") * activityFee.getFeeUnits());

							break;

						case 'F': // This is for fees that are based on number of units
							logger.debug("fee_calc_1=F");
							feeAmount = rs.getDouble("factor") * activityFee.getFeeUnits();

							break;

						case 'G': // User Entering the Fee Amount
							logger.debug("fee_calc_1=G");
							feeAmount = activityFee.getFeeAmount();

							break;

						case 'H': // this is for fee depending on valuation
							logger.debug("fee_calc_1=H, valuation * fee units");
							logger.debug("Fee units are " + activityFee.getFeeUnits());
							feeAmount = rs.getDouble("valuation") * rs.getDouble("fee_factor");
							
							double minFeeAmount = rs.getDouble("FACTOR");
							feeAmount = feeAmount>minFeeAmount?feeAmount:minFeeAmount;
							
							logger.debug("Calculated fee amount under formula H is " + feeAmount);

							break;

						case 'I': //
							logger.debug("fee_calc_1=I");
							feeAmount = rs.getDouble("valuation") * rs.getDouble("factor");

							break;

						case 'J':
							logger.debug("fee_calc_1=J");

							/**
							 * If feeInit > 0 then Mutilply the subtotal[feeInit] value by fee/unit Else Mutilply the subtotal[subTotalLevel] value by fee/unit
							 **/
							logger.debug("feeInit is : " + activityFee.getFeeInit());
							logger.debug("subTotalLevel is : " + activityFee.getSubtotalLevel());

							subTotals = activityFee.getSubTotals();
							logger.debug("sub totals are " + subTotals);

							if (activityFee.getFeeInit() > 0) {
								feeAmount = subTotals[activityFee.getFeeInit()];
							} else {
								logger.debug("in the else, subtotal level is " + activityFee.getSubtotalLevel());
								feeAmount = subTotals[activityFee.getSubtotalLevel()];
							}

							logger.debug("fee amount is :" + feeAmount);
							feeAmount *= activityFee.getFactor().doubleValue();

							break;

						case 'Q': // this is for fees based on the fee lookup table
							logger.debug("fee_calc_1=Q,this is for fees based on the fee lookup table");
							sql = "select * from v_fee_lookup_value where " + " fee_id = " + activityFee.getFeeId() + " order by low_range";
							logger.info(sql);

							RowSet frs = db.select(sql);
							feeAmount = 0.0;
							find: while (frs.next()) {
								if (frs.getDouble("HIGH_RANGE") >= rs.getDouble("valuation")) {
									feeAmount = frs.getDouble("Result");
									logger.debug("high range is great than or equal to valuation--feeAmount got is " + feeAmount);

									if (frs.getDouble("Over") > 0) {
										logger.debug("over is great than 0, so adding the formula");
										feeAmount += (StringUtils.divideCeiling(rs.getDouble("Valuation") - frs.getDouble("Low_Range"), frs.getDouble("Over")) * frs.getDouble("Plus"));
										logger.debug("fee amount after calculating for over is " + feeAmount);
									}

									if (rs.getDouble("factor") > 0.0) {
										logger.debug("factor is greater than 0, so multipler formaula applied");
										feeAmount *= rs.getDouble("factor");
										logger.debug("the new fee amount after incorporating factor is " + feeAmount);
									}

									break find;
								}
							}

							frs.close();

							break;

						case 'R':
							logger.debug("fee_calc_1=R, Max (Factor & fee factor * valuation)");
							feeAmount = Math.max(rs.getDouble("factor"), rs.getDouble("fee_factor") * rs.getDouble("valuation"));

							break;

						case 'S':
							logger.debug("fee_calc_1=S, not defined");

						case 'T':
							logger.debug("fee_calc_1=T");
							sql = "select *  from lkup_fee lf join lkup_fee_ref lfr on lf.lkup_fee=lfr.lkup_fee " + " where lfr.fee_id = " + activityFee.getFeeId() + " order by low_range";
							logger.debug(sql);

							CachedRowSet urs = new CachedRowSet();
							urs.populate(db.select(sql));
							feeAmount = 0.0;

							double increment = 0.0;
							double over = 0;
							double plus = 0;

							find: while (urs.next()) {
								if ((urs.getDouble("low_range") <= activityFee.getFeeUnits()) && (urs.getDouble("high_range") >= activityFee.getFeeUnits())) {
									feeAmount = urs.getDouble("Result");
									increment = activityFee.getFeeUnits() - urs.getDouble("low_range");
									plus = urs.getDouble("Plus");
									over = urs.getDouble("Over");

									break find;
								}
							}

							if (over > 0.0) {
								switch (rs.getString("fee_calc_1").charAt(0)) {
								case 'R':
									feeAmount += (increment / over * plus);

									break;

								case 'S': // Ceiling
									feeAmount += (StringUtils.divideCeiling(increment, over) * plus);

									break;

								case 'T': // Base
									feeAmount += (StringUtils.divideBase(increment, over) * plus);

									break;
								}
							}

							if (rs.getDouble("factor") > 0.0) {
								feeAmount *= rs.getDouble("factor");
							}

							break;

						case 'U':
							logger.debug("fee_calc_1=U");

							if (activityFee.getFeeInit() > 0) {
								levelId = activityFee.getFeeInit();
							} else {
								levelId = activityFee.getSubtotalLevel();
							}

							subTotals = activityFee.getSubTotals();
							logger.debug("LevelId : " + levelId);
							logger.debug("SubTotal : " + subTotals[levelId]);
							logger.debug("Factor : " + activityFee.getFactor().doubleValue());
							feeAmount = Math.max(subTotals[levelId], activityFee.getFactor().doubleValue());

							break;

						case 'V':
							logger.debug("fee_calc_1=V");

							if (activityFee.getFeeAmount() > activityFee.getFactor().doubleValue()) {
								feeAmount = activityFee.getFactor().doubleValue();
							} else {
								feeAmount = activityFee.getFeeAmount();
							}

							break;

						case 'W': // this is for fees based on the fee lookup table
							logger.debug("fee_calc_1=W");

							// float permitFeeAmount = 0.0;
							double totalAmount = 0.0;
							subTotals = activityFee.getSubTotals();

							if (activityFee.getFeeInit() > 0) {
								levelId = activityFee.getFeeInit();
							} else {
								levelId = activityFee.getSubtotalLevel();
							}

							totalAmount = subTotals[levelId];

							sql = "select * from v_fee_lookup_value where " + " fee_id = " + activityFee.getFeeId() + " and high_range >= " + totalAmount + " order by low_range";
							logger.info(sql);

							RowSet wrs = db.select(sql);

							feeAmount = 0.0;

							if (wrs.next()) {
								feeAmount = wrs.getDouble("Result");

								if (wrs.getDouble("Over") > 0.0) {
									feeAmount += ((totalAmount - wrs.getDouble("Low_Range")) / wrs.getDouble("Over") * wrs.getDouble("Plus"));
								}

								if (rs.getDouble("factor") > 0.0) {
									feeAmount *= rs.getDouble("factor");
								}
							}

							wrs.close();

							break;

						case 'Y':
							logger.debug("fee_calc_1=Y");

							/**
							 * Mutilply the subtotal value by Subtotallevel and compare the result to Factor Return the highest
							 */

							// add code here to select the subtotal from the arrey object based on feeInit of activityFee obj
							logger.debug("before setting up amount for calc Y : " + feeAmount);
							logger.debug("subtotallevel is : " + activityFee.getSubtotalLevel());
							subTotals = activityFee.getSubTotals();
							feeAmount = subTotals[activityFee.getSubtotalLevel()];
							logger.debug("after setting up amount for calc Y : " + feeAmount);
							logger.debug("fee factor " + activityFee.getFactor().doubleValue());

							feeAmount *= activityFee.getFeeFactor().doubleValue();

							if (feeAmount < activityFee.getFactor().doubleValue()) {
								feeAmount = activityFee.getFactor().doubleValue();
							}

							break;

						case 'y':
							logger.debug("fee_calc_1=y");

							/**
							 * Mutilply the subtotal value by fee/unit and compare the result to Factor Return the highest
							 */

							// add code here to select the subtotal from the arrey object based on feeInit of activityFee obj
							logger.debug("before setting up amount for calc y : " + feeAmount);
							logger.debug("feeInit is : " + activityFee.getFeeInit());
							subTotals = activityFee.getSubTotals();
							feeAmount = subTotals[activityFee.getFeeInit()];
							logger.debug("after setting up amount for calc y : " + feeAmount);
							logger.debug("fee factor " + activityFee.getFactor().doubleValue());

							feeAmount *= activityFee.getFeeFactor().doubleValue();

							if (feeAmount < activityFee.getFactor().doubleValue()) {
								feeAmount = activityFee.getFactor().doubleValue();
							}

							break;

						case 'a':
							logger.debug("fee_calc_1=a");

							/**
							 * Mutilply the subtotal value by units and compare the result to Factor Return the highest
							 */

							// add code here to select the subtotal from the arrey object based on feeInit of activityFee obj
							logger.debug("before setting up amount for calc y : " + feeAmount);
							logger.debug("feeInit is : " + activityFee.getFeeInit());
							subTotals = activityFee.getSubTotals();
							feeAmount = subTotals[activityFee.getFeeInit()];
							logger.debug("subtotal amount is : " + feeAmount);
							logger.debug("after setting up amount for calc y : " + feeAmount);
							feeAmount *= activityFee.getFeeUnits();

							if (feeAmount < activityFee.getFeeFactor().doubleValue()) {
								feeAmount = activityFee.getFeeFactor().doubleValue();
							}

							break;

						case 'b': //
							logger.debug("fee_calc_1=b");
							feeAmount = rs.getDouble("valuation") * rs.getDouble("fee_factor");

							break;
						case 'c': // Added For BT
							logger.debug("fee_calc_1=c");
							feeAmount = (rs.getDouble("fee_factor") * ((activityFee.getFeeUnits()) / 100)) + rs.getDouble("factor");
							break;
						case 'd': // Added For BT
							logger.debug("fee_calc_1=d");
							feeAmount = ((activityFee.getFeeUnits() * rs.getDouble("fee_factor")) + rs.getDouble("factor")) * 0.75;
							break;
						case 'e': // Added For BT
							logger.debug("fee_calc_1=e");
							feeAmount = ((activityFee.getFeeUnits() * rs.getDouble("fee_factor")) + rs.getDouble("factor")) * 0.50;
							break;
						case 'f': // Added For BT
							logger.debug("fee_calc_1=f");
							feeAmount = ((activityFee.getFeeUnits() * rs.getDouble("fee_factor")) + rs.getDouble("factor")) * 0.25;
							break;
						case 'g': // Added For BT
							logger.debug("fee_calc_1=g");
							double total1 = activityFee.getPermitLicenseFeeTotal();
							double feeUnit = rs.getDouble("fee_factor");
							if ((total1 * (rs.getDouble("factor")) / 100) < feeUnit)
								feeAmount = feeUnit;
							else
								feeAmount = total1 * (rs.getDouble("factor") / 100);

							double myDouble = feeAmount;
							double myDoubleMultiplied = myDouble * 100;
							double myDoubleRounded = Math.round(myDoubleMultiplied);
							feeAmount = myDoubleRounded / 100.0;

							logger.debug("Penalty fees  " + feeAmount);

							break;
						default:
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeAmount() : sql : " + sql + ":" + e.getMessage());
		}

		feeAmount = Double.parseDouble(new DecimalFormat("0.00").format(feeAmount));
		activityFee.setFeeAmount(feeAmount);
		logger.info("Exiting getFeeAmount() " + feeAmount);

		return;
	}

	/**
	 * process activity fee
	 * 
	 * @param db
	 * @param activityFee
	 * @return
	 * @throws Exception
	 */
	public String processActivityFee(Wrapper db, ActivityFee activityFee) throws Exception {
		logger.info("processActivityFee(Wrapper db, " + activityFee.getFeeAmount() + ")");

		String sql = "select count(*) as counter from activity_fee where activity_id=" + activityFee.getActivityId();

		if (activityFee.getType() == 'B') { // Business Tax
			sql += (" and people_id =" + activityFee.getPeopleId());
		} else {
			sql += (" and fee_id = " + activityFee.getFeeId());
		}

		try {
			logger.info(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				if (rs.getInt("counter") == 0) {
					if (activityFee.getType() == 'B') {// business tax
						sql = "insert into activity_fee(activity_id,people_id,fee_valuation,fee_amnt,comments,fee_credit,fee_adjustment) values (" + activityFee.getActivityId() + "," + activityFee.getPeopleId() + "," + activityFee.getFeeValuation() + "," + activityFee.getFeeAmount() + ",'" + activityFee.getComments() + "',0,0)";
					} else {
						sql = "insert into activity_fee(activity_id,fee_id,fee_units,fee_amnt,fee_credit,fee_adjustment,people_id) values (" + activityFee.getActivityId() + "," + activityFee.getFeeId() + "," + activityFee.getFeeUnits() + "," + activityFee.getFeeAmount() + ",0,0,-1)";
					}
				} else {
					if (activityFee.getType() == 'B') {// business tax
						sql = "update activity_fee set fee_valuation = " + activityFee.getFeeValuation() + ",fee_amnt = " + activityFee.getFeeAmount() + ",comments = '" + activityFee.getComments() + "' where activity_id = " + activityFee.getActivityId() + " and people_id = " + activityFee.getPeopleId();
					} else {
						sql = "update activity_fee set fee_units = " + activityFee.getFeeUnits() + ",fee_amnt = " + activityFee.getFeeAmount() + " where activity_id = " + activityFee.getActivityId() + " and fee_id = " + activityFee.getFeeId();
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent processActivityFee() " + e.getMessage());
		}

		logger.info("Exiting processActivityFee()");

		return sql;
	}

	public void saveFeesList(int activityId, List activityFeeList) throws Exception {
		logger.info("saveFeesList(" + activityId + ", ActivityFee List of " + activityFeeList.size() + " Elements)");

		ActivityFee activityFee = new ActivityFee();
		List sqlList = new ArrayList();
		String sql = "";
		String feeIds = "";
		int i = 0;

		try {
			logger.debug("~~~~~~~~~~~~~~~~~~`` :: " + activityFeeList.size());
			for (i = 0; i < activityFeeList.size(); i++) {
				activityFee = (ActivityFee) activityFeeList.get(i);
				logger.debug("~~~~~~~~~~~~~~~~~~`` :: " + activityFee.getFeeAmount() + ":: i :: " + i);
				logger.debug("~~~~~~~~~~~~~~~~~~`` :: " + activityFee.getFeeFlagFour() + ":: i :: " + i);
				sql = processActivityFee(db, activityFee);
				logger.info(sql);
				sqlList.add(sql);
				feeIds += (activityFee.getFeeId() + ",");
			}

			// Update fees with a payment to be set to amount 0 as they have been unselected
			sql = "update activity_fee set fee_amnt=0,fee_units=0 where activity_id=" + activityId + " and fee_id not in (" + feeIds + "0)";
			logger.info(sql);
			sqlList.add(sql);
			
			// Delete all the fees that have not been checked. Make sure
			// not to delete the Business Tax entries (fee_id 0) or fees that have a payment (fee_paid != 0)
			sql = "delete from activity_fee where activity_id=" + activityId + " and fee_id not in (" + feeIds + "0) and fee_paid=0.0";
			logger.info(sql);
			sqlList.add(sql);

			db.beginTransaction();

			for (i = 0; i < sqlList.size(); i++) {
				db.addBatch((String) sqlList.get(i));
			}

			// Update fees with a payment to be set to amount 0 as they have been unselected
			sql = "update activity_fee set fee_amnt=0,fee_units=0 where activity_id=" + activityId + " and fee_id not in (" + feeIds + "0)";
			logger.info(sql);
			db.addBatch(sql);

			// Delete all the fees that have not been checked. Make sure
			// not to delete the Business Tax entries (fee_id 0) or fees that have a payment (fee_paid != 0)
			sql = "delete from activity_fee where activity_id=" + activityId + " and fee_id not in (" + feeIds + "0) and fee_paid=0.0";
			logger.info(sql);
			db.addBatch(sql);
			db.executeBatch();
			 new PeopleAgent().updateActivity(activityId, 0);
		} catch (Exception e) {
			logger.error("Error in FinanceAgent saveFeeList() " + e.getMessage());
			throw e;
		}
	}

	/**
	 * Saves the business tax fees
	 * 
	 * @param activityFeeList
	 */
	public void saveBusinessTaxList(List activityFeeList) throws Exception {
		logger.info("Entering saveBusinessTaxList() with " + activityFeeList.size() + " elements");

		ActivityFee activityFee = new ActivityFee();
		List sqlList = new ArrayList();
		String sql = "";
		String peopleList = "";
		int i = 0;
		String activityId = "";

		try {
			db.beginTransaction();

			for (i = 0; i < activityFeeList.size(); i++) {
				activityFee = (ActivityFee) activityFeeList.get(i);
				activityId = activityFee.getActivityId() + "";

				if (activityFee.getFeeValuation() > 0) {
					logger.debug("obtained activity id is :" + activityId);
					activityFee.setType('B');

					// Calculate the fee amount for Business Tax
					logger.debug("Starting to calculate business tax amount");
					calculateFeeAmount(db, activityFee);
					sql = processActivityFee(db, activityFee);
					logger.info(sql);
					sqlList.add(sql);
					peopleList += (activityFee.getPeopleId() + ",");
					sql = "select count(*) as count from activity_people where act_id=" + activityId + " and psa_type='A' and people_id=" + activityFee.getPeopleId();
					logger.info(sql);
					RowSet apRs = db.select(sql);

					if (apRs.next()) {
						if (apRs.getInt("count") == 0) {
							sql = "insert into activity_people (act_id,people_id,psa_type) values(" + activityId + "," + activityFee.getPeopleId() + "," + "'A')";
							db.addBatch(sql);
							logger.debug("Activity_people sql in save business tax" + sql);
						}
					}

					apRs.close();
				}
			}

			logger.debug("activity id obtained is " + activityId);
			logger.debug("The people list is " + peopleList);

			sql = "delete from activity_fee where activity_id=" + activityId + " and fee_id=0 and fee_paid=0 and fee_credit=0 and people_id not in (" + peopleList + "0)";
			logger.info(sql);
			db.addBatch(sql);

			for (i = 0; i < sqlList.size(); i++) {
				db.addBatch((String) sqlList.get(i));
				logger.info("Batch sql (" + i + ") " + sqlList.get(i));
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error("Error in FinanceAgent saveBusinessTaxList() " + e.getMessage());
		}

		logger.info("Exiting FinanceAgent saveBusinessTaxList()");

		return;
	}

	/**
	 * delete the Business Tax Fee in Fee Mgr
	 * 
	 * @param activityId
	 *            , peopleId
	 * @author Gayathri
	 * @throws Exception
	 */
	public boolean deleteBusinessTax(int activityId, int peopleId) throws Exception {
		logger.info("deleteBusinessTax(" + activityId + "," + peopleId + ")");
		try {
			String PayFeeSql = "select fee_paid from activity_fee where activity_id = " + activityId + " and  people_id = " + peopleId;

			RowSet rs = db.select(PayFeeSql);
			double feePaidAmt = 0;
			String sql = "";

			if (rs != null && rs.next()) {
				feePaidAmt = rs.getDouble("fee_paid");
			}

			if (feePaidAmt == 0.0) {
				sql = "delete from activity_fee where activity_id =" + activityId + " and  people_id =" + peopleId;
				logger.info(sql);
				new Wrapper().update(sql);
				return true;
			} else {
				sql = "update activity_fee set fee_amnt=0,fee_units=0 where activity_id=" + activityId + " and  people_id =" + peopleId + "  and fee_id not in (0)";
				logger.info(sql);
				new Wrapper().update(sql);
				return false;
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 ** Updates the deposit amount in the people table and enters the payment in the Payment Table
	 ** 
	 */
	public boolean saveDeposit(Payment payment) {
		logger.info("Entering saveDeposit()");

		String sql = "";
		boolean success = false;

		try {
			db.beginTransaction();

			if (createPayment(db, payment)) {
				debitDeposit(db, payment);
				db.executeBatch();
				success = true;
			}
		} catch (Exception e) {
			logger.error("Error in saveDeposit() " + e.getMessage());
		}
		return success;
	}

	/*
	 * Get All Current Fees that have been selected for an activity
	 */
	public RowSet getActivityFeeList(int activityId) {
		logger.info("Entering getActivityFeeList() with id " + activityId);

		RowSet rs = null;

		try {
			rs = new CachedRowSet();

			String sql = "select af.activity_id,f.FEE_ID,f.fee_fl_4,f.fee_pc,f.fee_desc,f.lso_use,af.fee_amnt,fee_paid,fee_credit,af.fee_adjustment,af.bounced_amnt,f.fee_account from fee f join activity_fee af on f.FEE_ID=af.fee_id and fee_fl_4 in ('1','2','3','4') " + " and af.activity_id = " + activityId + " order by f.fee_sequence";
			logger.info(sql);
			rs = db.select(sql);
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getActivityFeeList() " + e.getMessage());
		}

		logger.info("Exiting FinanceAgent getActivityFeeList()");

		return rs;
	}

	/*
	 * Pay the balance due for an activity based on feeType. If the feeType is set to ' ' that implies all the fees are being paid
	 */
	public void payBalanceDue(int activityId, Payment payment, String feeType) throws Exception {
		logger.info("Entering payBalanceDue() with id " + activityId + " and type " + feeType);

		String sql;

		try {
			if ((payment.getMethod().equals("refund")) || (payment.getMethod().equals("transferOut"))) {
				sql = "select af.activity_id,f.fee_sequence,(af.fee_paid - af.fee_amnt) as amnt, af.fee_id,af.people_id,f.fee_pc from activity_fee af join fee f on f.fee_id = af.fee_id where af.activity_id = " + activityId + " and (af.fee_paid - af.fee_amnt) > 0 ";
			} else {
				// get all the unpaid fees for the specified activity
				sql = "select activity_id,fee_id,people_id,unpaid_amnt,bounced_amnt,fee_account " + " from v_activity_fee_unpaid where activity_id = " + activityId;
			}

			if (feeType.length() > 0) {
				sql += (" and fee_fl_4 in (" + feeType + ")");
			}

			logger.debug(sql);

			db.beginTransaction();

			// Add the Insert Payment Statement
			if (createPayment(db, payment)) {
				logger.debug(sql);

				RowSet rs = db.select(sql);
				PaymentDetail detail = new PaymentDetail();
				detail.setActivityId(activityId);
				detail.setPaymentId(payment.getPaymentId());

				while (rs.next()) {
					detail.setFeeId(rs.getInt("fee_id"));
					detail.setAmount(rs.getDouble("unpaid_amnt"));
					detail.setPeopleId(rs.getInt("people_id"));
					detail.setBouncedAmt(0 - rs.getDouble("bounced_amnt"));
					detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));
					logger.debug("payment detail is " + detail.toString());

					// Build sql for creating the payment detail
					sql = createPaymentDetail(detail);
					db.addBatch(sql);

					// Build sql for updating the Activity Fee
					sql = updateActivityFee(payment.getType(), detail);
					db.addBatch(sql);
				}

				logger.debug("end tran and going for execuation");
				db.executeBatch();
			}

			logger.info("Exiting FinanceAgent payBalanceDue()");
		} catch (Exception e) {
			logger.error("Error in FinanceAgent payBalanceDue() " + e.getMessage());
			throw e;
		}
	}

	/*
	 * Pay all the fees due for an activity
	 */
	public void payBalanceDue(int activityId, Payment payment) throws Exception {
		payBalanceDue(activityId, payment, "");
	}

	/*
	 * Pay all the fees due for a SubProject
	 */
	public void paySubProjectBalance(int subProjectId, Payment payment) throws Exception {
		logger.info("paySubProjectBalance(" + subProjectId + ", Payment)");

		try {
			String sql = "";

			// get all the unpaid fees for the specified activity
			sql = "select activity_id,fee_id,people_id,unpaid_amnt,bounced_amnt,fee_account " + " from v_activity_fee_unpaid " + " where activity_id in ( select act_id from activity where sproj_id = " + subProjectId + " ) and fee_fl_4 in ('1','2','3','4','5')";

			logger.info(sql);

			db.beginTransaction();

			// Add the Insert Payment Statement
			if (createPayment(db, payment)) {
				logger.debug(sql);

				RowSet rs = db.select(sql);
				PaymentDetail detail = new PaymentDetail();
				detail.setPaymentId(payment.getPaymentId());
				logger.debug("paymentID in payment Detail " + detail.getPaymentId());

				// Iterate thru the Resultset and create the PaymentDetail
				while (rs.next()) {
					detail.setActivityId(rs.getInt("activity_id"));
					logger.debug("actID in payment Detail " + detail.getActivityId());
					detail.setFeeId(rs.getInt("fee_id"));
					detail.setAmount(rs.getDouble("unpaid_amnt"));
					detail.setPeopleId(rs.getInt("people_id"));
					detail.setBouncedAmt(0 - rs.getDouble("bounced_amnt"));
					detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));

					logger.debug("payment detail is " + detail.toString());

					// Build sql for creating the payment detail
					sql = createPaymentDetail(detail);
					db.addBatch(sql);

					// Build sql for updating the Activity Fee
					sql = updateActivityFee(payment.getType(), detail);
					db.addBatch(sql);
				}

				logger.debug("end tran and going for execuation");
				db.executeBatch();
			}

			logger.info("Exiting FinanceAgent payBalanceDue()");
		} catch (Exception e) {
			logger.error("Error in FinanceAgent payBalanceDue() " + e.getMessage());
			throw e;
		}
	}

	/*
	 * Make Partial Payment for the activity and specific fee types
	 */
	public void payPartialAmount(int activityId, Payment payment, String feeType) throws Exception {
		logger.info("payPartialAmount(" + activityId + ", Payment, " + feeType + ")");

		try {
			Wrapper ppaDb = new Wrapper();
			ppaDb.beginTransaction();
			ActivityAgent activityAgent = new ActivityAgent();
			
			if (createPayment(ppaDb, payment)) {
				// get all the unpaid fees for the specified activity
				String sql = "";

				if (payment.getAmount() < 0.0) {
					sql = "SELECT af.activity_id,f.fee_sequence,af.fee_paid as amnt, af.fee_id,af.people_id,f.fee_pc,f.fee_account from activity_fee af join fee f on f.fee_id = af.fee_id where af.activity_id = " + activityId + " and (af.fee_paid) > 0 ";
				} else {
					sql = "select activity_id,fee_id,people_id,fee_sequence,unpaid_amnt as amnt,bounced_amnt,fee_account from v_activity_fee_unpaid where activity_id = " + activityId;
				}

				if (feeType != "") {
					sql += (" and fee_fl_4 in (" + feeType + ")");
				}

				sql += " order by fee_sequence";

				// For -ve amounts unpay fees from the last to first
				if (payment.getAmount() < 0.0) {
					sql += " desc";
				}

				logger.info(sql);

				RowSet rs = ppaDb.select(sql);

				PaymentDetail detail = new PaymentDetail();
				detail.setActivityId(activityId);
				detail.setPaymentId(payment.getPaymentId());
				detail.setActNbr(activityAgent.getActivtiyNumber(activityId));
				
				double balanceAmt = payment.getAmount();

				if (balanceAmt < 0.0) {
					logger.debug("balance amount is less than zero");

					// Iterate thru the Resultset and create the PaymentDetail
					balanceAmt *= -1;

					while (rs.next() && (balanceAmt > 0.0)) {
						detail.setFeeId(rs.getInt("fee_id"));

						detail.setPeopleId(rs.getInt("people_id"));
						detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));

						if (balanceAmt > rs.getDouble("amnt")) {
							balanceAmt -= rs.getDouble("amnt");
							detail.setAmount(0 - rs.getDouble("amnt"));
						} else {
							detail.setAmount(0.0 - balanceAmt);
							balanceAmt = 0.0;
						}

						// Build sql for creating the payment detail
						sql = createPaymentDetail(detail);
						logger.info(sql);
						ppaDb.addBatch(sql);

						// Build sql for updating the Activity Fee
						sql = updateActivityFee(payment.getType(), detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}
					}

					logger.debug("amount after payment detail#### " + balanceAmt);
					ppaDb.executeBatch();
				} else {
					logger.debug("balance amount is greater than zero");

					// PAYMENT :Iterate thru the Resultset and create the PaymentDetail for PAYMENT
					// First Pay off any Bounced Amt
					logger.debug("First paying off any bounced amount");

					while (rs.next() && (balanceAmt > 0.0)) {
						detail.setFeeId(rs.getInt("fee_id"));
						detail.setPeopleId(rs.getInt("people_id"));
						detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));
						detail.setAmount(0.0);

						if (balanceAmt > rs.getDouble("bounced_amnt")) {
							balanceAmt -= rs.getDouble("bounced_amnt");
							balanceAmt = Double.parseDouble(new DecimalFormat("0.00").format(balanceAmt));
							detail.setBouncedAmt(0 - rs.getDouble("bounced_amnt"));
						} else {
							detail.setBouncedAmt(0 - balanceAmt);
							balanceAmt = 0.0;
						}

						// Build sql for creating the payment detail
						logger.debug("Building sql for creating payment detail");
						sql = createPaymentDetail(detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}

						// Build sql for updating the Activity Fee
						logger.debug("Building sql for updating activity fee payment detail");
						sql = updateActivityFee(payment.getType(), detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}
						
						// Build sql for updating the Activity for RENEWAL_ONLINE flag
						if(detail.getActNbr()!=null && detail.getActNbr().startsWith("BL")){
						sql = updateActivity(detail);
						if (!(sql.equals(""))) { // add the sql only if it is not blank
						logger.info(sql);
						ppaDb.addBatch(sql);
						} else {
						logger.debug("SQL is null , so not added to the batch");
						}
						
						}
					}

					// next pay off any amount dues
					rs.beforeFirst();
					logger.debug("payment off any amount dues");

					while (rs.next() && (balanceAmt > 0.0)) {
						detail.setFeeId(rs.getInt("fee_id"));
						detail.setPeopleId(rs.getInt("people_id"));
						detail.setBouncedAmt(0.0d);
						detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));

						if (balanceAmt > rs.getDouble("amnt")) {
							balanceAmt -= rs.getDouble("amnt");
							balanceAmt = Double.parseDouble(new DecimalFormat("0.00").format(balanceAmt));
							detail.setAmount(rs.getDouble("amnt"));
						} else {
							detail.setAmount(balanceAmt);
							balanceAmt = 0.0;
						}

						// Build sql for creating the payment detail
						sql = createPaymentDetail(detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}

						// Build sql for updating the Activity Fee
						sql = updateActivityFee(payment.getType(), detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}
					}

					logger.debug("amount after payment detail " + balanceAmt);
					ppaDb.executeBatch();

					logger.debug("excecuted the previous transaction, starting another transaction");
					ppaDb.beginTransaction();

					if (balanceAmt > 0.0) {
						logger.debug("Balance amount is > 0");
						detail.setAmount(balanceAmt);
						sql = updateCreditToPaymentDetail(feeType, detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}

						sql = updateCreditToActivityFee(payment.getType(), feeType, detail);

						if (!(sql.equals(""))) { // add the sql only if it is not blank
							logger.info(sql);
							ppaDb.addBatch(sql);
						} else {
							logger.debug("SQL is null , so not added to the batch");
						}
					}

					logger.debug("Payment detail : " + detail);

					ppaDb.executeBatch();
				}
			}

			logger.info("Exiting FinanceAgent payPartialAmount()");
		} catch (Exception e) {
			logger.error("Error in FinanceAgent payPartialAmount() " + e.getMessage());
			throw e;
		}
	}

	public void payPartialAmount(int activityId, Payment payment) throws Exception {
		payPartialAmount(activityId, payment, "");
	}

	public void paySpecificAmount(int activityId, Payment payment, List paymentDetailList) throws Exception {
		logger.info("paySpecificAmount(" + activityId + ", Payment , " + paymentDetailList.size() + ")");

		try {
			int i = 0;
			String sql = "";
			RowSet rs = null;
			Wrapper dba = new Wrapper();
			
			// get all the unpaid fees for the specified activity
			
			//sql = "update payment set void = 'Y' WHERE pymnt_id=" + payment.getPaymentId();
			
			sql = "select distinct p.pymnt_id,p.pymnt_amnt" + " from payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id " + " where void = 'N' and pd.act_id = " + activityId;
			logger.info(sql);
			rs = db.select(sql);
			while (rs.next()) {
				Payment voidPayment = new Payment();
				voidPayment.setPaymentId(rs.getInt("pymnt_id"));
				voidPayment.setAmount(rs.getDouble("pymnt_amnt"));
				if(payment.getAmount() < 0.0){
					voidPayment.setVoided('Y');
					updateVoidDetails(voidPayment);
				}
			}
			dba.beginTransaction();
			logger.info(sql);
			db.addBatch(sql);
			// Add the Insert Payment Statement
			if (createPayment(dba, payment)) {
				if(payment.getType().getPayTypeId() == 4 && (payment.getMethod().equalsIgnoreCase(Constants.PAYMENT_METHOD_CORRECTION)||payment.getMethod().equalsIgnoreCase(Constants.PAYMENT_METHOD_NON_REVENUE))) {
					//sql = "update payment set reversal = 'N' WHERE pymnt_id=" + payment.getPaymentId();
					sql = "update payment set void = 'Y',void_by = "+ payment.getEnteredBy().getUserId() + ",void_dt = CURRENT_DATE WHERE pymnt_id=" + payment.getPaymentId();
					logger.info(sql);
					dba.addBatch(sql);
				}
				PaymentDetail detail = new PaymentDetail();
				double balanceAmt = payment.getAmount();
				logger.debug("balanceAmt is : " + balanceAmt + " for detail list size :" + paymentDetailList.size());

				// Iterate thru the list of PaymentDetails
				for (i = 0; i < paymentDetailList.size(); i++) {
					detail = (PaymentDetail) paymentDetailList.get(i);

					detail.setActivityId(activityId);
					detail.setPaymentId(payment.getPaymentId());
					logger.debug(i + ":" + detail);

					sql = createPaymentDetail(detail);
					logger.info(sql);

					dba.addBatch(sql);

					sql = updateActivityFee(payment.getType(), detail);
					logger.info(sql);

					dba.addBatch(sql);
				}

				dba.executeBatch();
			}
			
			logger.info("Exiting FinanceAgent paySpecificAmount()");
		} catch (Exception e) {
			logger.error("Error in FinanceAgent paySpecificAmount() " + e.getMessage());
			throw e;
		}
	}

	/*
	 * REVERSE plan check credit for Permit payment : method : pc credit
	 */
	public void reverseCredit(int activityId, Payment payment, String feeType) throws Exception {
		PaymentDetail detail = new PaymentDetail();
		String sql = "";
		detail.setActivityId(activityId);
		detail.setPaymentId(payment.getPaymentId());

		double balanceAmt = (0 - payment.getAmount());
		detail.setAmount(balanceAmt);

		db.beginTransaction();
		sql = updateCreditToPaymentDetail(feeType, detail);
		logger.debug("final SQL for payment_detail update @@@@@@@@ : " + sql);
		db.addBatch(sql);

		sql = updateCreditToActivityFee(payment.getType(), feeType, detail);
		logger.debug("final SQL for activity_fee update @@@@@@@@ : " + sql);
		db.addBatch(sql);

		logger.debug("Payment detail #### : " + detail);
		db.executeBatch();

		return;
	}

	public boolean voidPayment(int activityId, double amount) throws Exception {
		logger.info("Entering voidPayment() with :" + activityId + ":" + amount);

		boolean success = false;

		try {
			CachedRowSet rs = new CachedRowSet();

			// Select the payment that matches the amount and activity specified
			// In case of multiple matching payments use the most recent payment
			String sql = "select pymnt_id from payment where void='N' and paymnt_amnt=" + amount + " and pymnt_id in (select paymnt_id from payment_detail where act_id = " + activityId + " order by pymnt_id desc";

			rs.populate(db.select(sql));

			if (rs.next()) {
				Payment payment = new Payment();
				payment.setPaymentId(rs.getInt("pymnt_id"));
				voidPayment(payment, activityId);
				success = true;
			} else {
				throw new PaymentNotFoundException();
			}

			return success;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent voidPayment() " + e.getMessage());
			throw new PaymentException();
		}
	}

	private void voidPayment(Payment payment, int activityId) throws Exception {
		logger.info("voidPayment(Payment, " + activityId + ")");

		try {
			CachedRowSet rs = new CachedRowSet();
			String sql;
			sql = "update payment set " + " void = 'Y' WHERE pymnt_id=" + payment.getPaymentId();

			db.beginTransaction();
			logger.info(sql);
			db.addBatch(sql);
			/*db.beginTransaction();
			sql = "update payment set void = 'Y',reversal ='"+payment.getReversal()+"' WHERE pymnt_id=" + payment.getPaymentId();
			logger.info(sql);
			db.addBatch(sql);*/
			sql = "select act_id,fee_id,amnt,people_id,fee_account from payment_detail " + " where pymnt_id = " + payment.getPaymentId();

			rs.populate(db.select(sql));

			int paymentId = 0;

			if (rs != null) {
				payment.setAmount(0 - payment.getAmount());
				payment.setMethod("reverse");
				payment.setVoided('Y');
				//payment.setReversal("N");
				if (createPayment(db, payment)) {
					logger.debug("Reverse payment for : " + activityId + " : amount : " + payment.getAmount());
				}

				// paymentId = db.getNextId("PAYMENT_ID");
				paymentId = payment.getPaymentId();
				logger.debug("new paymentID is " + paymentId);
				sql = "update payment set " + " void = 'Y',void_dt = CURRENT_DATE, void_by = " + payment.getEnteredBy().getUserId() + " WHERE pymnt_id=" + payment.getPaymentId();

				
				logger.info(sql);
				db.addBatch(sql);
			}

			PaymentDetail detail = new PaymentDetail();
			detail.setActivityId(activityId);
			detail.setPaymentId(paymentId);

			while (rs.next()) {
				detail.setFeeId(rs.getInt("fee_id"));
				detail.setPeopleId(rs.getInt("people_Id"));
				detail.setAmount(0 - rs.getDouble("amnt"));
				detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));

				sql = updateActivityFee(payment.getType(), detail);
				logger.info(sql);
				db.addBatch(sql);
				sql = createPaymentDetail(detail);
				logger.info(sql);
				db.addBatch(sql);
			}

			rs.close();
			db.executeBatch();
			logger.info("Exiting FinanceAgent voidPayment()");
		} catch (Exception e) {
			logger.error("Error in FinanceAgent voidPayment() " + e.getMessage());
			throw e;
		}
	}

	// VOID ALL PAYMENT
	public void voidAllPayments(int activityId, Payment payment) throws Exception {
		logger.info("voidAllPayments(" + activityId + ", Payment)");

		try {
			CachedRowSet rs = new CachedRowSet();

			String sql = "select distinct p.pymnt_id,p.pymnt_amnt" + " from payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id " + " where void = 'N' and pd.act_id = " + activityId;
			logger.info(sql);
			rs.populate(db.select(sql));

			while (rs.next()) {
				payment.setPaymentId(rs.getInt("pymnt_id"));
				payment.setAmount(rs.getDouble("pymnt_amnt"));
				voidPayment(payment, activityId);
			}

			rs.close();
			logger.info("Exiting FinanceAgent voidAllPayments()");
		} catch (Exception e) {
			logger.error("Error in FinanceAgent voidAllPayments() " + e.getMessage());
			throw e;
		}
	}

	/*
	 * Make Extended Credit Payment for the activity (no method)
	 */
	public void extendCredit(int activityId, Payment payment, String feeType) throws Exception {
		logger.info("Entering extendCredit() with id " + activityId);

		db.beginTransaction();

		if (createPayment(db, payment)) {
			// get all the unpaid fees for the specified activity
			String sql = "";

			if (payment.getAmount() < 0.0) {
				sql = "SELECT af.activity_id,f.fee_sequence,af.fee_paid as amnt, af.fee_id,af.people_id,f.fee_pc,af.fee_account from activity_fee af join fee f on f.fee_id = af.fee_id where af.activity_id = " + activityId + " and (af.fee_paid) > 0 ";
			} else {
				sql = "select activity_id,fee_id,people_id,fee_sequence,unpaid_amnt,fee_account as amnt from v_activity_fee_unpaid where activity_id = " + activityId;
			}

			if (feeType != "") {
				sql += (" and fee_fl_4 in (" + feeType + ")");
			}

			sql += " order by fee_sequence";

			// For -ve amounts unpay fees from the last to first
			// if (payment.getAmount() < 0.0) sql+= " description";
			logger.debug("extendCredit SQL : " + sql);

			RowSet rs = db.select(sql);

			PaymentDetail detail = new PaymentDetail();
			detail.setActivityId(activityId);
			detail.setPaymentId(payment.getPaymentId());

			double balanceAmt = payment.getAmount();

			// int feeId = 0;
			// int peopleId = 0;
			// PAYMENT :Iterate thru the Resultset and create the PaymentDetail for PAYMENT
			while (rs.next() && (balanceAmt > 0.0)) {
				detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));
				detail.setFeeId(rs.getInt("fee_id"));
				detail.setPeopleId(rs.getInt("people_id"));

				if (balanceAmt > rs.getDouble("amnt")) {
					balanceAmt -= rs.getDouble("amnt");
					balanceAmt = Double.parseDouble(new DecimalFormat("0.00").format(balanceAmt));
					detail.setAmount(rs.getDouble("amnt"));
				} else {
					detail.setAmount(balanceAmt);
					balanceAmt = 0.0;
				}

				// Build sql for creating the payment detail
				sql = createPaymentDetail(detail);
				db.addBatch(sql);

				// Build sql for updating the Activity Fee
				sql = updateActivityFee(payment.getType(), detail);
				db.addBatch(sql);
			}

			logger.debug("amount after payment detail#### " + balanceAmt);
			db.executeBatch();
			db.beginTransaction();

			if (balanceAmt > 0.0) {
				detail.setAmount(balanceAmt);
				sql = updateCreditToPaymentDetail(feeType, detail);
				logger.debug("final SQL for payment_detail update @@@@@@@@ : " + sql);
				db.addBatch(sql);

				sql = updateCreditToActivityFee(payment.getType(), feeType, detail);
				logger.debug("final SQL for activity_fee update @@@@@@@@ : " + sql);
				db.addBatch(sql);
			}

			logger.debug("Payment detail #### : " + detail);

			db.executeBatch();
		}
	}

	public boolean nsfPayments(int activityId, String checkNo, Payment payment) throws Exception {
		logger.info("Entering nsfPayments() with id " + checkNo);

		boolean isCheckNo = false;

		try {
			CachedRowSet rs = new CachedRowSet();

			String sql = "select distinct p.pymnt_id,p.pymnt_amnt from payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id  where upper(accnt_no) =upper('" + checkNo + "') and pd.act_id = " + activityId;

			logger.info(sql);
			rs.populate(db.select(sql));

			if (rs.next()) {
				isCheckNo = true;
				payment.setPaymentId(rs.getInt("pymnt_id"));
				payment.setAmount(0 - rs.getDouble("pymnt_amnt"));
				payment.setMethod("nsf");

				sql = "select act_id,fee_id,amnt,people_id,fee_account from payment_detail " + " where pymnt_id = " + payment.getPaymentId();

				RowSet pd = db.select(sql);

				if (pd != null) {
					db.beginTransaction();

					if (createPayment(db, payment)) {
						int paymentId = payment.getPaymentId();
						logger.debug("new paymentID is " + paymentId);

						PaymentDetail detail = new PaymentDetail();
						detail.setActivityId(activityId);
						detail.setPaymentId(paymentId);

						while (pd.next()) {
							detail.setFeeId(pd.getInt("fee_id"));
							detail.setPeopleId(pd.getInt("people_Id"));
							detail.setBouncedAmt(pd.getDouble("amnt"));
							sql = "UPDATE ACTIVITY_FEE SET BOUNCED_AMNT = BOUNCED_AMNT + " + detail.getBouncedAmt() + " WHERE ACTIVITY_ID= " + detail.getActivityId() + " AND PEOPLE_ID = " + detail.getPeopleId() + " AND fee_id = " + detail.getFeeId();
							logger.debug(sql);
							db.addBatch(sql);
							sql = createPaymentDetail(detail);
							logger.debug(sql);
							db.addBatch(sql);
						}
					}

					db.executeBatch();
					rs.close();
				}
			}

			logger.info("Exiting FinanceAgent voidAllPayments()");

			return isCheckNo;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent voidAllPayments() " + e.getMessage());
			throw e;
		}
	}

	/**
	 * createPayment method generates the sql to add a new payment from the Payment object passed. It uses the getNextId function to generate the payment id.
	 * 
	 */
	private boolean createPayment(Wrapper dba, Payment payment) throws Exception {
		logger.debug("Entering createPayment() with  " + payment);

		boolean success = false;

		try {
			int paymentId = dba.getNextId("PAYMENT_ID");
			payment.setPaymentId(paymentId);
			logger.debug("payment Id in payment is " + payment.getPaymentId());

			logger.debug(" " + payment.getPaymentId());
			logger.debug(" " + StringUtils.checkString(payment.getMethod()));
			logger.debug(" " + payment.getType().getPayTypeId());
			logger.debug(" " + payment.getPeopleId());
			logger.debug(" " + StringUtils.checkString(payment.getPayee()));
			logger.debug(" " + payment.getAmount());
			logger.debug(" " + StringUtils.checkString(payment.getAccountNbr()));
			logger.debug(" " + StringUtils.checkString(payment.getComment()));
			logger.debug(" " + payment.getEnteredBy().getUserId());
			logger.debug(" " + payment.getAuthorizedBy());
			logger.debug(" " + StringUtils.checkString(payment.getDepartmentCode()));
			logger.debug(" " + payment.getVoided());

			String sql = "INSERT into PAYMENT (PYMNT_ID,PYMNT_METHOD,PYMNT_TYPE,PYMNT_DT,PEOPLE_ID,PAYEE,PYMNT_AMNT,ACCNT_NO,COMNT,ENTER_BY_ID,AUTHORIZATION_ID,DEPT_CODE,VOID) VALUES ( " + payment.getPaymentId() + "," + StringUtils.checkString(payment.getMethod()) + "," + payment.getType().getPayTypeId() + "," + "CURRENT_DATE," + payment.getPeopleId() + "," + StringUtils.checkString(payment.getPayee()) + "," + payment.getAmount() + "," + StringUtils.checkString(payment.getAccountNbr()) + "," + StringUtils.checkString(payment.getComment()) + "," + payment.getEnteredBy().getUserId() + "," + StringUtils.checkString(payment.getAuthorizedBy()) + "," + StringUtils.checkString(payment.getDepartmentCode()) + ",'" + payment.getVoided() + "')";
			logger.info(sql);
			dba.addBatch(sql);

			if (payment.getMethod().equalsIgnoreCase("deposit")) {
				payment.setAmount(payment.getAmount() * -1);
				debitDeposit(dba, payment);

				payment.setAmount(payment.getAmount() * -1);
			}
			
			if(!payment.getOnlineTxnId().equals("")){
			String sql1 = " UPDATE PAYMENT SET ONLINETXNID = " + StringUtils.checkString(payment.getOnlineTxnId()) + " where PYMNT_ID = "+payment.getPaymentId();
			logger.info(sql1);
			dba.addBatch(sql1);
			}

			success = true;
			logger.debug("success is :" + success);

			return success;
		} catch (Exception e) {
			logger.error("Error createPayment() " + e.getMessage());
			throw e;
		}
	}

	private boolean debitDeposit(Wrapper dba, Payment payment) throws Exception {
		String sql;

		sql = "update people set deposit_balance = deposit_balance + " + payment.getAmount() + " where people_id=" + payment.getPeopleId();
		dba.addBatch(sql);

		sql = "INSERT INTO PAYMENT_DETAIL (TRANS_ID,ACT_ID,PYMNT_ID,AMNT,FEE_ACCOUNT) VALUES (" + dba.getNextId("PAYMENT_DETAIL_ID") + "," + payment.getLevelId() + "," + payment.getPaymentId() + "," + payment.getAmount() + "," + 406 + ")";
		dba.addBatch(sql);

		return true;
	}

	private String createPaymentDetail(PaymentDetail detail) throws Exception {
		logger.info("Entering createPaymentDetail() with  " + detail);

		String sql = "";
		double amount = detail.getAmount() - detail.getBouncedAmt();

		if (amount != 0.0) {
			detail.setTransactionId(db.getNextId("PAYMENT_DETAIL_ID"));
			sql = "INSERT INTO PAYMENT_DETAIL (TRANS_ID,PYMNT_ID,ACT_ID,FEE_ID,PEOPLE_ID,AMNT,FEE_ACCOUNT) VALUES (" + detail.getTransactionId() + "," + detail.getPaymentId() + "," + detail.getActivityId() + "," + detail.getFeeId() + "," + detail.getPeopleId() + "," + amount + "," + StringUtils.checkString(detail.getFeeAccount()) + ")";
		}

		logger.debug("sql for createPaymentDetail  is " + sql);
		logger.info("Exiting createPaymentDetail()");

		return sql;
	}

	private String updateActivityFee(PayType paymentType, PaymentDetail detail) throws Exception {
		logger.debug("updateActivityFee(" + paymentType + ", " + detail + ")");
		 new PeopleAgent().updateActivity(detail.getActivityId(), 0);
		String sql = "UPDATE ACTIVITY_FEE SET ";

		switch (paymentType.getPayTypeId()) {
		case 1:
		case 2:
		case 3:
		case 4:
			sql += (" FEE_PAID = FEE_PAID + " + detail.getAmount() + ",BOUNCED_AMNT = BOUNCED_AMNT + " + detail.getBouncedAmt());
			logger.info(sql);

			break;

		case 5:
		case 6:
			sql += (" FEE_CREDIT = FEE_CREDIT + " + detail.getAmount());
			logger.info(sql);

			break;
		case 9:
			sql += (" FEE_PAID = FEE_PAID + " + detail.getAmount());
			logger.info(sql);

			break;
		}

		if (detail.getPeopleId() > 0) {
			logger.debug("payment of business tax");
			sql += (" WHERE activity_id=" + detail.getActivityId() + " AND FEE_ID= "+detail.getFeeId()+" AND people_id = " + detail.getPeopleId());
		} else {
			logger.debug("payment of plan check or permit fees");
			sql += (" WHERE activity_id=" + detail.getActivityId() + " AND fee_id = " + detail.getFeeId());
		}

		logger.debug("Exiting updateActivityFee()");
		logger.info(sql);

		return sql;
	}

	private String updateActivity(PaymentDetail detail) throws Exception {
		logger.debug("updateActivityFee(" + detail + ")");

		 String sql = "UPDATE ACTIVITY SET UPDATED=CURRENT_TIMESTAMP,RENEWAL_ONLINE= 'Y' WHERE ACT_ID=" + detail.getActivityId();
		logger.info(sql);
		logger.debug("Exiting updateActivity()");
		return sql;
	}
	
	/* ACTIVITY_FEE : To update credit amount for specifiec fee_id and specific feeType */
	private String updateCreditToActivityFee(PayType paymentType, String feeType, PaymentDetail detail) throws Exception {
		logger.debug("updateCreditToActivityFee(" + paymentType + ", " + feeType + ", " + detail + ")");

		int feeId = 0;
		int peopleId = 0;

		try {
			String feeIdsql = "SELECT af.activity_id,af.fee_id, af.people_id,f.fee_sequence,f.fee_pc from activity_fee af join fee f on f.fee_id = af.fee_id  where activity_id =" + detail.getActivityId();

			if (detail.getAmount() < 0) {
				feeIdsql += " and fee_amnt < fee_paid ";
			}

			feeIdsql += (" and fee_fl_4 in (" + feeType + ") and rownum=1 order by f.fee_sequence desc ");
			logger.debug("feeIdsql [1] : " + feeIdsql);

			RowSet rs = db.select(feeIdsql);

			while (rs.next()) {
				feeId = rs.getInt("FEE_ID");
				peopleId = rs.getInt("PEOPLE_ID");
			}

			if (rs != null) {
				rs.close();
				logger.debug("After closing rs ");
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent updateCreditToActivityFee() " + e.getMessage());
			throw e;
		}

		String sql = "UPDATE ACTIVITY_FEE SET ";

		switch (paymentType.getPayTypeId()) {
		case 1:
		case 2:
		case 3:
		case 4:
			sql += (" FEE_PAID = FEE_PAID + " + detail.getAmount());

			break;

		case 5:
		case 6:
			sql += (" FEE_CREDIT = FEE_CREDIT + " + detail.getAmount());

			break;
		}

		if (peopleId != 0) {
			logger.debug("payment of business tax");
			sql += (" WHERE activity_id=" + detail.getActivityId() + " AND people_id = " + peopleId);
		} else {
			logger.debug("payment of plan check or permit fees");
			sql += (" WHERE activity_id=" + detail.getActivityId() + " AND fee_id = " + feeId);
		}

		logger.debug("Exiting updateActivityFee()");
		logger.info(sql);

		return sql;
	}

	/* PLAN CHECK - PERMIT CREDIT : plan check OR permit credit for activity */
	public String pcpCreditForActivity(char feeType, int activityId) {
		logger.debug("Entering pcpCrditForActivity");

		String creditAmt = "";

		try {
			String creditsql = "SELECT sum(af.fee_paid +af.fee_credit + af.fee_adjustment - af.fee_amnt) as credit_amt from activity_fee af join fee f on f.fee_id = af.fee_id where af.activity_id =" + activityId + " and (af.fee_paid +af.fee_credit + af.fee_adjustment - af.fee_amnt) > 0 and f.fee_fl_4 = '" + feeType + "'";

			logger.debug("pcpCredit SQL : " + creditsql);

			RowSet rs = db.select(creditsql);

			while (rs.next()) {
				creditAmt = rs.getString("credit_amt");

				if (creditAmt == null) {
					creditAmt = "0";
				}
			}

			if (rs != null) {
				rs.close();
				logger.debug("After closing rs ");
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent pcpCreditForActivity() " + e.getMessage());
		}

		return creditAmt;
	}

	/* PAYMENT_DETAIL : To update credit amount for specifiec fee_id and specific feeType */
	private String updateCreditToPaymentDetail(String feeType, PaymentDetail detail) throws Exception {
		logger.debug("updateCreditToPaymentDetail(" + feeType + ", " + detail + ")");

		int feeId = 0;
		int peopleId = 0;
		String feeAccount = "0";
		String sql;

		try {
			sql = "SELECT af.activity_id,af.fee_id, af.people_id,f.fee_sequence,f.fee_pc,f.fee_account from activity_fee af join fee f on f.fee_id = af.fee_id " + " where activity_id =" + detail.getActivityId() + " and fee_fl_4 in (" + feeType + ")";

			if (detail.getAmount() < 0) {
				sql += " and fee_paid > fee_amnt";
			} else {
				sql += " and rownum=1 order by f.fee_sequence desc ";
			}

			logger.info(sql);

			RowSet rs = db.select(sql);

			if (rs.next()) {
				feeId = rs.getInt("FEE_ID");
				peopleId = rs.getInt("PEOPLE_ID");
				feeAccount = rs.getString("FEE_ACCOUNT");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent updateCreditToPaymentDetail() " + e.getMessage());
		}

		detail.setFeeId(feeId);
		detail.setPeopleId(peopleId);
		detail.setFeeAccount(feeAccount);

		sql = createPaymentDetail(detail);

		logger.debug("Exiting updateActivityFee()");

		return sql;
	}

	/*
	 * Add deposit/Withdrawl
	 */
	public CachedRowSet depositDetails(int peopleId) {
		logger.debug("Before Retrieving Deposit Details with peopleid " + peopleId);

		CachedRowSet rs = null;

		// max(pymnt_dt) from payment where people_id
		try {
			rs = new CachedRowSet();

			// Payment payment = new Payment();
			String sql = "select p.name,p.lic_no,p.deposit_balance, max(pt.pymnt_dt) as pymnt_dt  " + " from people p join payment pt on p.people_id = pt.people_id  " + " where p.people_id = " + peopleId + " group by p.name,p.lic_no,p.deposit_balance ";
			logger.debug(sql);
			rs.populate(db.select(sql));

			// rs.close();
		} catch (Exception e) {
			logger.error("Error in FinanceAgent depositDetails() " + e.getMessage());
		}

		logger.debug("Exiting FinanceAgent depositDetails()");

		return rs;
	}

	/*
	 * to get current deposit balance from payment
	 */
	public double getDepositBalance(int peopleId) {
		logger.debug("Before Retrieving Deposit balance with peopleid " + peopleId);

		CachedRowSet rs = null;
		double depositBalance = 0;

		// max(pymnt_dt) from payment where people_id
		try {
			rs = new CachedRowSet();

			// Payment payment = new Payment();
			String sql = "select deposit_balance from people " + " where people_id = " + peopleId;
			logger.debug(sql);
			rs.populate(db.select(sql));

			while (rs.next()) {
				depositBalance = rs.getDouble("deposit_balance");
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Error in FinanceAgent depositDetails() " + e.getMessage());
		}

		logger.debug("Exiting FinanceAgent depositDetails()");

		return depositBalance;
	}

	public void savePaymentComments(Wrapper db, LedgerEdit[] payments, PaymentDetailEdit[] details) throws Exception {
		try {
			db.beginTransaction();

			String sql;
			String hide;

			try {
				for (int i = 0; i < payments.length; i++) {
					hide = payments[i].getHide().equals("") ? "N" : "Y";
					sql = "update payment set comnt=" + StringUtils.checkString(payments[i].getComments()) + ",hide = " + StringUtils.checkString(hide) + " where pymnt_id = " + payments[i].getTransactionId();
					logger.info(sql);
					db.addBatch(sql);
				}
			} catch (Exception e) {
				logger.warn("Empty payments list");
			}

			try {
				for (int i = 0; i < details.length; i++) {
					sql = "update payment_detail set comments=" + StringUtils.checkString(details[i].getComment()) + " where trans_id = " + details[i].getTransactionId();
					logger.info(sql);
					db.addBatch(sql);
				}
			} catch (Exception e) {
				logger.warn("Empty payments detail list");
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.warn("");
		}

		return;
	}

	public String paymentIdList(int activityId) {
		String paymentIdStr = "";

		try {

			String sql = "select distinct pymnt_id from payment_detail where act_id=" + activityId;
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				if (paymentIdStr.equalsIgnoreCase("")) {
					paymentIdStr = rs.getString("PYMNT_ID").trim();
				}

				paymentIdStr = paymentIdStr + "," + rs.getString("PYMNT_ID").trim();
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception thrown while trying to delete Project :" + e.getMessage());
		}

		return paymentIdStr;
	}

	// code added for development fees
	public RowSet getDevelopmentFeeList(int activityId) throws Exception {
		logger.info("Entering getDevelopmentFeeList() with id " + activityId);

		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			sql = "SELECT U.LAND_ID FROM (LAND_USAGE U JOIN V_LSO_LAND L ON U.LSO_USE_ID=" + commercialUse + " AND L.LAND_ID=U.LAND_ID) JOIN V_PSA_LIST P ON P.LSO_ID=L.LSO_ID WHERE P.ACT_ID=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {
				useSql = "((f.lso_use is null) or (f.lso_use = " + commercialUse + "))";
			} else {
				useSql = "((f.lso_use is null) or (f.lso_use != " + commercialUse + "))";
			}

			rs = new CachedRowSet();
			sql = "select a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked,f.fee_desc,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment,0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit,af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit from (fee f join activity a on a.act_type=f.act_type and fee_calc_1 is not null  and ((fee_fl_4 in ('0','2','4') and (f.fee_creation_dt <= a.permit_fee_date )) or (fee_fl_4 in '1' and (f.fee_creation_dt <= a.plan_chk_fee_date )) or (fee_fl_4 in '3' and (f.fee_creation_dt <= a.development_fee_date ))) and ((fee_fl_4 in ('0','2','4') and (f.fee_expiration_dt >= a.permit_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '1' and (f.fee_expiration_dt >= a.plan_chk_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '3' and (f.fee_expiration_dt >= a.development_fee_date or f.fee_expiration_dt is null))) and a.act_id =" + activityId + ") left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id and af.fee_amnt > 0.0 where (f.fee_pc in ('P','C','B','X','D')) and " + useSql + " order by f.fee_sequence";
			logger.debug("developmentfees");
			logger.info("sql " + sql);
			rs = db.select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						logger.debug("inside rs.next() developmentfees");
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getDevelopmentFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getDevelopmentFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getDevelopmentFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	/**
	 * calculates the fee amount.
	 * 
	 * @param db
	 * @param activityFee
	 */
	public void calculateFeeAmountPftotal(Wrapper db, ActivityFee activityFee, double pfTotal) {
		logger.info("calculateFeeAmountPftotal(Wrapper, " + activityFee + ")");

		double feeAmount = 0.00;
		double[] subTotals = {};
		int levelId = 0;
		double BUSINESS_TAX_MIN = 10.00d;
		double BUSINESS_TAX_MAX = 460.00d;
		String sql = "";

		try {
			if (activityFee.getType() == 'B') { // Business Tax
				logger.debug("fee type is business tax");
				feeAmount = activityFee.getFeeValuation() * 0.00080;// it was 77 cents before july 1,2008--changed by AB

				if (feeAmount < BUSINESS_TAX_MIN) {
					feeAmount = BUSINESS_TAX_MIN;
					logger.debug("buiness tax set to minimum cap");
				} else if (feeAmount > BUSINESS_TAX_MAX) {
					feeAmount = BUSINESS_TAX_MAX;
					logger.debug("buiness tax set to maximum cap");
				} else {
					logger.debug("business tax set to the calculation");
				}

				logger.debug("Fee amount before calculation is " + feeAmount);
			} else {
				RowSet rs = new CachedRowSet();
				boolean calculateFee = true;

				sql = "select (fee_amnt-fee_paid) as balance from activity_fee where activity_id = " + activityFee.getActivityId() + " and fee_id = " + activityFee.getFeeId();
				logger.debug(sql);
				rs = db.select(sql);

				if (rs.next()) {
					if (rs.getDouble("BALANCE") != 0.00d) {
						logger.debug("balance is greater than 0.00d ,calculate fee = true");
						calculateFee = true;
					} else {
						logger.debug("balance is 0.00d,calculate fee = false");
						calculateFee = false;
					}
				} else {
					logger.debug("Resultset does not have any values, calculate fee = true");
					calculateFee = true;
				}

				sql = "select * from fee f,activity a where act_id = " + activityFee.getActivityId() + " and fee_id = " + activityFee.getFeeId();
				logger.debug(sql);
				rs = new CachedRowSet();
				rs = db.select(sql);

				if (rs.next()) {
					logger.info("Fee Calculate : " + activityFee.getFeeId() + ":" + rs.getString("fee_calc_1") + ": Fee Amount : " + activityFee.getFeeAmount() + ": Paid :" + activityFee.getFeePaid());

					if ((rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_CE_INACTIVE) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_CE_SUSPENSE) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BT_CALENDAR_YEAR_EXPIRED) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BT_OUT_OF_BUSINESS) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BL_OUT_OF_BUSINESS) || (rs.getInt("STATUS") == Constants.ACTIVITY_STATUS_BL_FISCAL_YEAR_EXPIRED)) {
						// If the fee has already been paid and Activity Status=Issued then do not recalculate it.
						logger.debug("fee has already been paid and Activity Status=Issued then do not recalculate it");
						feeAmount = Double.parseDouble(new DecimalFormat("0.00").format(activityFee.getFeeAmount()));
						feeAmount = 0.0;
						activityFee.setFeeAmount(0.0);
						logger.debug("FeeAmount is : " + feeAmount);
					} else {
						switch (rs.getString("fee_calc_1").charAt(0)) {
						case 'A': // This is for fees that are based on number of units
							logger.debug("fee_calc_1=A,This is for fees that are based on number of units");

							if (rs.getDouble("fee_factor") > 0.00) {
								logger.debug("Fee factor is > 0.00, calculating fee amount");
								feeAmount = rs.getDouble("fee_factor") * Math.max(1, activityFee.getFeeUnits());
								// DecimalFormat df = new DecimalFormat("0.00");

								logger.debug("calculated fee amount is " + feeAmount);
							}

							break;

						case 'B': // This is for fees that are based on number of units
							logger.debug("fee_calc_1=B,This is for fees that are based on number of units");
							feeAmount = rs.getDouble("factor") + (rs.getDouble("fee_factor") * activityFee.getFeeUnits());

							break;

						case 'C': // this is for a flat fee
							logger.debug("fee_calc_1=C,this is for a flat fee");
							feeAmount = rs.getDouble("factor");

							break;

						case 'D': // Max of factor and fee_factor*units
							logger.debug("fee_calc_1=D");
							feeAmount = Math.max(rs.getDouble("factor"), rs.getDouble("fee_factor") * activityFee.getFeeUnits());

							break;

						case 'E': // Min of factor and fee_factor*units
							logger.debug("fee_calc_1=E");
							feeAmount = Math.min(rs.getDouble("factor"), rs.getDouble("fee_factor") * activityFee.getFeeUnits());

							break;

						case 'F': // This is for fees that are based on number of units
							logger.debug("fee_calc_1=F");
							feeAmount = rs.getDouble("factor") * activityFee.getFeeUnits();

							break;

						case 'G': // User Entering the Fee Amount
							logger.debug("fee_calc_1=G");
							feeAmount = activityFee.getFeeAmount();

							break;

						case 'H': // this is for fee depending on valuation
							logger.debug("fee_calc_1=H, valuation * fee units");
							logger.debug("Fee units are " + activityFee.getFeeUnits());
							feeAmount = rs.getDouble("valuation") * rs.getDouble("fee_factor");
							
							double minFeeAmount = rs.getDouble("FACTOR");
							feeAmount = feeAmount>minFeeAmount?feeAmount:minFeeAmount;
							
							logger.debug("Calculated fee amount under formula H is " + feeAmount);

							break;

						case 'I': //
							logger.debug("fee_calc_1=I");
							feeAmount = rs.getDouble("valuation") * rs.getDouble("factor");

							break;

						case 'J':
							logger.debug("fee_calc_1=J");

							/**
							 * If feeInit > 0 then Mutilply the subtotal[feeInit] value by fee/unit Else Mutilply the subtotal[subTotalLevel] value by fee/unit
							 **/
							logger.debug("feeInit is : " + activityFee.getFeeInit());
							logger.debug("subTotalLevel is : " + activityFee.getSubtotalLevel());

							subTotals = activityFee.getSubTotals();
							logger.debug("sub totals are " + subTotals);

							if (activityFee.getFeeInit() > 0) {
								feeAmount = subTotals[activityFee.getFeeInit()];
							} else {
								logger.debug("in the else, subtotal level is " + activityFee.getSubtotalLevel());
								feeAmount = subTotals[activityFee.getSubtotalLevel()];
							}

							logger.debug("fee amount is :" + feeAmount);
							feeAmount *= activityFee.getFactor().doubleValue();

							break;

						case 'Q': // this is for fees based on the fee lookup table
							logger.debug("fee_calc_1=Q,this is for fees based on the fee lookup table");
							sql = "select * from v_fee_lookup_value where " + " fee_id = " + activityFee.getFeeId() + " order by low_range";
							logger.info(sql);

							RowSet frs = db.select(sql);
							feeAmount = 0.0;
							find: while (frs.next()) {
								if (frs.getDouble("HIGH_RANGE") >= rs.getDouble("valuation")) {
									feeAmount = frs.getDouble("Result");
									logger.debug("high range is great than or equal to valuation--feeAmount got is " + feeAmount);

									if (frs.getDouble("Over") > 0) {
										logger.debug("over is great than 0, so adding the formula");
										feeAmount += (StringUtils.divideCeiling(rs.getDouble("Valuation") - frs.getDouble("Low_Range"), frs.getDouble("Over")) * frs.getDouble("Plus"));
										logger.debug("fee amount after calculating for over is " + feeAmount);
									}

									if (rs.getDouble("factor") > 0.0) {
										logger.debug("factor is greater than 0, so multipler formaula applied");
										feeAmount *= rs.getDouble("factor");
										logger.debug("the new fee amount after incorporating factor is " + feeAmount);
									}

									break find;
								}
							}

							frs.close();

							break;

						case 'R':
							logger.debug("fee_calc_1=R, Max (Factor & fee factor * valuation)");
							feeAmount = Math.max(rs.getDouble("factor"), rs.getDouble("fee_factor") * rs.getDouble("valuation"));

							break;

						case 'S':
							logger.debug("fee_calc_1=S, not defined");

						case 'T':
							logger.debug("fee_calc_1=T");
							sql = "select *  from lkup_fee lf join lkup_fee_ref lfr on lf.lkup_fee=lfr.lkup_fee " + " where lfr.fee_id = " + activityFee.getFeeId() + " order by low_range";
							logger.debug(sql);

							CachedRowSet urs = new CachedRowSet();
							urs.populate(db.select(sql));
							feeAmount = 0.0;

							double increment = 0.0;
							double over = 0;
							double plus = 0;

							find: while (urs.next()) {
								if ((urs.getDouble("low_range") <= activityFee.getFeeUnits()) && (urs.getDouble("high_range") >= activityFee.getFeeUnits())) {
									feeAmount = urs.getDouble("Result");
									increment = activityFee.getFeeUnits() - urs.getDouble("low_range");
									plus = urs.getDouble("Plus");
									over = urs.getDouble("Over");

									break find;
								}
							}

							if (over > 0.0) {
								switch (rs.getString("fee_calc_1").charAt(0)) {
								case 'R':
									feeAmount += (increment / over * plus);

									break;

								case 'S': // Ceiling
									feeAmount += (StringUtils.divideCeiling(increment, over) * plus);

									break;

								case 'T': // Base
									feeAmount += (StringUtils.divideBase(increment, over) * plus);

									break;
								}
							}

							if (rs.getDouble("factor") > 0.0) {
								feeAmount *= rs.getDouble("factor");
							}

							break;

						case 'U':
							logger.debug("fee_calc_1=U");

							if (activityFee.getFeeInit() > 0) {
								levelId = activityFee.getFeeInit();
							} else {
								levelId = activityFee.getSubtotalLevel();
							}

							subTotals = activityFee.getSubTotals();
							logger.debug("LevelId : " + levelId);
							logger.debug("SubTotal : " + subTotals[levelId]);
							logger.debug("Factor : " + activityFee.getFactor().doubleValue());
							feeAmount = Math.max(subTotals[levelId], activityFee.getFactor().doubleValue());

							break;

						case 'V':
							logger.debug("fee_calc_1=V");

							if (activityFee.getFeeAmount() > activityFee.getFactor().doubleValue()) {
								feeAmount = activityFee.getFactor().doubleValue();
							} else {
								feeAmount = activityFee.getFeeAmount();
							}

							break;

						case 'W': // this is for fees based on the fee lookup table
							logger.debug("fee_calc_1=W");

							// float permitFeeAmount = 0.0;
							double totalAmount = 0.0;
							subTotals = activityFee.getSubTotals();

							if (activityFee.getFeeInit() > 0) {
								levelId = activityFee.getFeeInit();
							} else {
								levelId = activityFee.getSubtotalLevel();
							}

							totalAmount = subTotals[levelId];

							sql = "select * from v_fee_lookup_value where " + " fee_id = " + activityFee.getFeeId() + " and high_range >= " + totalAmount + " order by low_range";
							logger.info(sql);

							RowSet wrs = db.select(sql);

							feeAmount = 0.0;

							if (wrs.next()) {
								feeAmount = wrs.getDouble("Result");

								if (wrs.getDouble("Over") > 0.0) {
									feeAmount += ((totalAmount - wrs.getDouble("Low_Range")) / wrs.getDouble("Over") * wrs.getDouble("Plus"));
								}

								if (rs.getDouble("factor") > 0.0) {
									feeAmount *= rs.getDouble("factor");
								}
							}

							wrs.close();

							break;

						case 'Y':
							logger.debug("fee_calc_1=Y");

							/**
							 * Mutilply the subtotal value by Subtotallevel and compare the result to Factor Return the highest
							 */

							// add code here to select the subtotal from the arrey object based on feeInit of activityFee obj
							logger.debug("before setting up amount for calc Y : " + feeAmount);
							logger.debug("subtotallevel is : " + activityFee.getSubtotalLevel());
							subTotals = activityFee.getSubTotals();
							feeAmount = subTotals[activityFee.getSubtotalLevel()];
							logger.debug("after setting up amount for calc Y : " + feeAmount);
							logger.debug("fee factor " + activityFee.getFactor().doubleValue());

							feeAmount *= activityFee.getFeeFactor().doubleValue();

							if (feeAmount < activityFee.getFactor().doubleValue()) {
								feeAmount = activityFee.getFactor().doubleValue();
							}

							break;

						case 'y':
							logger.debug("fee_calc_1=y");

							/**
							 * Mutilply the subtotal value by fee/unit and compare the result to Factor Return the highest
							 */

							// add code here to select the subtotal from the arrey object based on feeInit of activityFee obj
							logger.debug("before setting up amount for calc y : " + feeAmount);
							logger.debug("feeInit is : " + activityFee.getFeeInit());
							subTotals = activityFee.getSubTotals();
							feeAmount = subTotals[activityFee.getFeeInit()];
							logger.debug("after setting up amount for calc y : " + feeAmount);
							logger.debug("fee factor " + activityFee.getFactor().doubleValue());

							feeAmount *= activityFee.getFeeFactor().doubleValue();

							if (feeAmount < activityFee.getFactor().doubleValue()) {
								feeAmount = activityFee.getFactor().doubleValue();
							}

							break;

						case 'a':
							logger.debug("fee_calc_1=a");

							/**
							 * Mutilply the subtotal value by units and compare the result to Factor Return the highest
							 */

							// add code here to select the subtotal from the arrey object based on feeInit of activityFee obj
							logger.debug("before setting up amount for calc y : " + feeAmount);
							logger.debug("feeInit is : " + activityFee.getFeeInit());
							subTotals = activityFee.getSubTotals();
							feeAmount = subTotals[activityFee.getFeeInit()];
							logger.debug("subtotal amount is : " + feeAmount);
							logger.debug("after setting up amount for calc y : " + feeAmount);
							feeAmount *= activityFee.getFeeUnits();

							if (feeAmount < activityFee.getFeeFactor().doubleValue()) {
								feeAmount = activityFee.getFeeFactor().doubleValue();
							}

							break;

						case 'b': //
							logger.debug("fee_calc_1=b");
							feeAmount = rs.getDouble("valuation") * rs.getDouble("fee_factor");

							break;
						case 'c': // Added For BT
							logger.debug("fee_calc_1=c");
							feeAmount = (rs.getDouble("fee_factor") * ((activityFee.getFeeUnits()) / 100)) + rs.getDouble("factor");
							break;
						case 'd': // Added For BT
							logger.debug("fee_calc_1=d");
							feeAmount = ((activityFee.getFeeUnits() * rs.getDouble("fee_factor")) + rs.getDouble("factor")) * 0.75;
							break;
						case 'e': // Added For BT
							logger.debug("fee_calc_1=e");
							feeAmount = ((activityFee.getFeeUnits() * rs.getDouble("fee_factor")) + rs.getDouble("factor")) * 0.50;
							break;
						case 'f': // Added For BT
							logger.debug("fee_calc_1=f");
							feeAmount = ((activityFee.getFeeUnits() * rs.getDouble("fee_factor")) + rs.getDouble("factor")) * 0.25;
							break;
						case 'g': // Added For BT
							logger.debug("fee_calc_1=g");
							double total1 = pfTotal;
							double feeUnit = rs.getDouble("fee_factor");
							if ((total1 * (rs.getDouble("factor")) / 100) < feeUnit)
								feeAmount = feeUnit;
							else
								feeAmount = total1 * (rs.getDouble("factor") / 100);

							double myDouble = feeAmount;
							double myDoubleMultiplied = myDouble * 100;
							double myDoubleRounded = Math.round(myDoubleMultiplied);
							feeAmount = myDoubleRounded / 100.0;

							logger.debug("Penalty fees  " + feeAmount);

							break;
						default:
							break;
						}
					}
				}
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeAmount() : sql : " + sql + ":" + e.getMessage());
		}

		feeAmount = Double.parseDouble(new DecimalFormat("0.00").format(feeAmount));
		activityFee.setFeeAmount(feeAmount);
		logger.info("Exiting getFeeAmount() " + feeAmount);

		return;
	}

	/**
	 * Get All Permit & Plan Check Fees that may be applicable for an activity .
	 * 
	 * feeChecked 0 - fee not selected for this activity feeChecked 1 - fee has been selected for this activity
	 * 
	 * fee_pc flag P - Permit Fee C - Plan Check Fees B - Both Plan Check and Permit
	 * 
	 * feeAmount is the calculated fee amount based on the number of units (feeUnits)
	 */
	public RowSet getOnlineFeeList(int activityId, String sTypeId) throws Exception {
		logger.info("Entering getFeeList() with id " + activityId);

		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			sql = "SELECT U.LAND_ID FROM (LAND_USAGE U JOIN V_LSO_LAND L ON U.LSO_USE_ID=" + commercialUse + " AND L.LAND_ID=U.LAND_ID) JOIN V_PSA_LIST P ON P.LSO_ID=L.LSO_ID WHERE P.ACT_ID=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {
				useSql = "((f.lso_use is null) or (f.lso_use = " + commercialUse + "))";
			} else {
				useSql = "((f.lso_use is null) or (f.lso_use != " + commercialUse + "))";
			}

			rs = new CachedRowSet();
			sql = "select a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked,f.fee_desc,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment,0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit,af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit from (fee f join activity a on  a.act_type=f.act_type  and fee_calc_1 is not null  and ((fee_fl_4 in ('0','2','4') and (f.fee_creation_dt <= a.permit_fee_date )) or (fee_fl_4 in '1' and (f.fee_creation_dt <= a.plan_chk_fee_date )) or (fee_fl_4 in '3' and (f.fee_creation_dt <= a.development_fee_date ))) and ((fee_fl_4 in ('0','2','4') and (f.fee_expiration_dt >= a.permit_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '1' and (f.fee_expiration_dt >= a.plan_chk_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '3' and (f.fee_expiration_dt >= a.development_fee_date or f.fee_expiration_dt is null))) and a.act_id =" + activityId + ")" + " left outer join ACT_SUBTYPE ast on a.act_id = ast.act_id join FEE_ACTSUBTYPE fa on f.fee_id = fa.fee_id and fa.ACT_SUBTYPE_ID IN " + sTypeId + " left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id and af.fee_amnt > 0.0" + " where (f.fee_pc in ('P','C','B','X','D'))" + " and " + useSql + " order by f.fee_sequence";
			logger.info(sql);
			rs = db.select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}

	
	public RowSet getNewOnlineFeeList(int actId) throws Exception {
		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;

		try {
			// First get the LAND_USE (Commercial or Residential)
			rs = new CachedRowSet();
			sql = "select  a.act_id,f.fee_id,f.fee_pc,fee_factor,fee_calc_1,f.fee_calc_2,'of' as feeChecked, f.fee_desc || ' - ' || LAS.ACT_SUBTYPE as fee_desc  ,f.lso_use,f.fee_factor,f.fee_input_fl as input,f.fee_fl_1 as required,f.tax_flag,0.0 as feeUnits,0.0 as feeAmount,0.0 as feePaid,0.0 as feeCredit,0.0 as feeAdjustment, 0.0 as bouncedAmt,af.fee_id as activityFeeId,af.fee_units,af.fee_amnt,af.fee_paid,af.fee_credit, af.fee_adjustment,af.bounced_amnt,f.factor, f.fee_subt_Level as subtotalLevel, f.fee_fl_4 as feeFlagFour , f.fee_init  as feeInit  from fee f  left outer join activity a on  a.act_type=f.act_type  left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id     left outer join LKUP_ACT_SUBTYPE LAS on  f.act_subtype_ID=las.ACT_SUBTYPE_ID  where  a.act_id ="+actId;//+"f.ACT_SUBTYPE_ID in "+feesubType;
			//sql = "select * from fee f 	left outer join activity a on  a.act_type=f.act_type and a.act_id ="+actId+" left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id where f.ACT_SUBTYPE_ID ="+subTypeId;
			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				if (rs.getString("activityFeeId") != null) {
					try {
						rs.updateString("feeChecked", "on");
						rs.updateDouble("feeUnits", rs.getDouble("fee_units"));
						rs.updateBigDecimal("feeAmount", rs.getBigDecimal("fee_amnt"));
						rs.updateBigDecimal("feePaid", rs.getBigDecimal("fee_paid"));
						rs.updateBigDecimal("feeCredit", rs.getBigDecimal("fee_credit"));
						rs.updateBigDecimal("feeAdjustment", rs.getBigDecimal("fee_adjustment"));
						rs.updateBigDecimal("bouncedAmt", rs.getBigDecimal("bounced_amnt"));
						rs.updateRow();
					} catch (Exception rsException) {
						logger.error("Error in FinanceAgent getFeeList() :" + rs.getInt("act_id") + ":" + rs.getInt("fee_id") + ":" + rsException.getMessage());
						throw rsException;
					}
				}
			}

			logger.info("Exiting FinanceAgent getFeeList()");

			return rs;
		} catch (Exception e) {
			logger.error("Error in FinanceAgent getFeeList() : sql : " + sql + ":" + e.getMessage());
			throw e;
		}
	}
	/*
	 * Pay all the fees due for a SubProject for specfic activities
	 */
	public void payComboSubProjectBalance(int subProjectId, Payment payment, String activityIds) {
		try {
			String sql = "";

			// get all the unpaid fees for the specified activity
			sql = "select activity_id,fee_id,people_id,unpaid_amnt,bounced_amnt,fee_account " + " from v_activity_fee_unpaid " + " where activity_id in (" + activityIds + ") and fee_fl_4 in ('1','2','5')";

			logger.debug(sql);

			db.beginTransaction();

			// Add the Insert Payment Statement
			if (createPayment(db, payment)) {
				logger.debug(sql);
				RowSet rs = new Wrapper().select(sql);
				PaymentDetail detail = new PaymentDetail();
				detail.setPaymentId(payment.getPaymentId());
				logger.debug("paymentID in payment Detail " + detail.getPaymentId());

				// Iterate thru the Resultset and create the PaymentDetail
				while (rs.next()) {
					detail.setActivityId(rs.getInt("activity_id"));
					logger.debug("actID in payment Detail " + detail.getActivityId());
					detail.setFeeId(rs.getInt("fee_id"));
					
					String activityType = new ActivityAgent().getActivityType(rs.getInt("activity_id"));
					if(activityType.equals("PKNCOM")){
						detail.setAmount(payment.getAmount());
						}else{
						detail.setAmount(rs.getDouble("unpaid_amnt"));
					 }
					
					detail.setPeopleId(rs.getInt("people_id"));
					detail.setBouncedAmt(0 - rs.getDouble("bounced_amnt"));
					detail.setFeeAccount(rs.getString("FEE_ACCOUNT"));
					logger.debug("payment rs detail " + rs.getDouble("unpaid_amnt"));
					logger.debug("payment detail detail " + detail.getAmount());
					logger.debug("payment detail is " + detail.toString());

					// Build sql for creating the payment detail
					sql = createPaymentDetail(detail);
					db.addBatch(sql);

					// Build sql for updating the Activity Fee
					sql = updateActivityFee(payment.getType(), detail);
					db.addBatch(sql);
				}

				logger.debug("end tran and going for execuation");
				db.executeBatch();
			}
		} catch (Exception e) {
			logger.error("Error in FinanceAgent payBalanceDue() " + e.getMessage());
		}

		logger.info("Exiting FinanceAgent payBalanceDue()");

		return;
	}

	public CachedRowSet getFeeList() {
		return getFeeList("");
	}

	public CachedRowSet getFeeList(String actType) {
		logger.info("Entering getFeeList()");

		CachedRowSet rs = null;
		String sql = "";

		try {

			rs = new CachedRowSet();
			sql = "select fee_id as feeId ,act_type as actType,fee_type as feeType,fee_desc as description,fee_creation_dt as creationDate,fee_factor as feeFactor,factor,fee_sequence as sequence from fee where fee_expiration_dt is null ";

			if (!(actType.equals("") || actType.equalsIgnoreCase("All"))) {
				sql += (" and actType = '" + actType + "' ");
			}

			sql += " order by actType,fee_desc,fee_sequence";
			logger.info(sql);
			rs.populate(db.select(sql));

		} catch (Exception e) {
			logger.error("Error in getFeeList() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		}
		return rs;
	}

	/**
	 * Obtain a List of fees for the corresponding activity type
	 * 
	 * @returns java.util.List
	 * @author Anand Belaguly
	 * @date Aug 04,2002
	 */
	public List getFees(String actType) {
		List fees = new ArrayList();
		String sql = "";

		try {
			sql = "select f.*,lf.lkup_fee from fee f left outer join lkup_fee_ref lf on lf.fee_id=f.fee_id  where fee_expiration_dt is null ";

			if (!(actType.equals("") || actType.equalsIgnoreCase("All"))) {
				sql += (" and act_type = '" + actType + "' ");
			}

			sql += " order by act_type,fee_sequence,fee_desc";
			logger.info(sql);
			logger.debug("Sql query is ******* " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				FeeEdit fee = new FeeEdit();
				fee.setFeeId(rs.getString("fee_id"));
				fee.setActType(rs.getString("act_type"));
				fee.setFeeType(rs.getString("fee_type"));

				fee.setDescription(rs.getString("fee_desc"));
				fee.setCreationDate(rs.getDate("fee_creation_dt"));
				fee.setExpirationDate(rs.getDate("fee_expiration_dt"));
				fee.setFeeFactor(rs.getString("fee_factor"));

				fee.setFactor(rs.getString("factor"));

				fee.setSequence(rs.getString("fee_sequence"));

				fee.setInputFlag(rs.getString("fee_input_fl"));
				fee.setFlagOne(rs.getString("fee_fl_1"));
				fee.setFlagTwo(rs.getString("fee_fl_2"));
				fee.setFlagThree(rs.getString("fee_fl_3"));
				fee.setFlagFour(rs.getString("fee_fl_4"));
				fee.setFeeCalcOne(rs.getString("fee_calc_1"));
				fee.setFeeCalcTwo(rs.getString("fee_calc_2"));
				fee.setFeeCalcThree(rs.getString("fee_calc_3"));
				fee.setFeeCalcFour(rs.getString("fee_calc_4"));
				fee.setDescription(rs.getString("fee_desc"));
				fee.setInitial(rs.getString("fee_init"));
				fee.setSubtotalLevel(rs.getString("fee_subt_level"));
				fee.setAccount(rs.getString("fee_account"));
				fee.setMiscellaneous(rs.getString("misc"));
				fee.setType(rs.getString("fee_pc"));
				fee.setFeeLookup(rs.getString("lkup_fee"));
				fee.setTaxFlag(rs.getString("tax_flag"));
				fee.setRenewable(StringUtils.s2b(rs.getString("renewable")));
				fee.setOnlineRenewalbleFlag(StringUtils.s2b(rs.getString("ONLINE_RENEWABLE")));
				
				fee.setAccountCode(rs.getString("account_code"));
				fee.setActivitySubtypeId(rs.getString("ACT_SUBTYPE_ID"));
				fee.setOnlineInput(rs.getString("online_input"));
				fee.setFeeClass(rs.getString("fee_class"));
				
				
//				logger.debug("$$$$$ isOnlineRenewalbleFlag :: " + fee.isOnlineRenewalbleFlag());

				fees.add(fee);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getFeeList() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		} finally {
			logger.info("Exiting getFees()");
		}

		return fees;
	}

	public List getFees(String actType, String flag) {
		List fees = new ArrayList();
		String sql = "";
		try {
			sql = "select f.*,lf.lkup_fee from fee f left outer join lkup_fee_ref lf on lf.fee_id=f.fee_id  where fee_expiration_dt is null ";

			if (!(actType.equals("") || actType.equalsIgnoreCase("All"))) {
				sql += (" and act_type = '" + actType + "' ");
			}
			if (flag != null && !(flag.equals(""))) {

				sql += (" and f.fee_calc_1 is not null and f.fee_fl_4 in('0','2') and f.FEE_FL_1  in ('2','3') and f.FEE_INPUT_FL  in ('0','C','X') and  f.fee_creation_dt <=current_date ");
			}

			sql += " order by act_type,fee_sequence,fee_desc";
			logger.info(sql);
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				FeeEdit fee = new FeeEdit();
				fee.setFeeId(rs.getString("fee_id"));
				fee.setActType(rs.getString("act_type"));
				fee.setFeeType(rs.getString("fee_type"));
				fee.setDescription(rs.getString("fee_desc"));
				fee.setCreationDate(rs.getDate("fee_creation_dt"));
				fee.setExpirationDate(rs.getDate("fee_expiration_dt"));
				fee.setFeeFactor(rs.getString("fee_factor"));
				fee.setFactor(rs.getString("factor"));
				fee.setSequence(rs.getString("fee_sequence"));

				fee.setInputFlag(rs.getString("fee_input_fl"));
				fee.setFlagOne(rs.getString("fee_fl_1"));
				fee.setFlagTwo(rs.getString("fee_fl_2"));
				fee.setFlagThree(rs.getString("fee_fl_3"));
				fee.setFlagFour(rs.getString("fee_fl_4"));
				fee.setFeeCalcOne(rs.getString("fee_calc_1"));
				fee.setFeeCalcTwo(rs.getString("fee_calc_2"));
				fee.setFeeCalcThree(rs.getString("fee_calc_3"));
				fee.setFeeCalcFour(rs.getString("fee_calc_4"));
				fee.setDescription(rs.getString("fee_desc"));
				fee.setInitial(rs.getString("fee_init"));
				fee.setSubtotalLevel(rs.getString("fee_subt_level"));
				fee.setAccount(rs.getString("fee_account"));
				fee.setMiscellaneous(rs.getString("misc"));
				fee.setType(rs.getString("fee_pc"));
				fee.setFeeLookup(rs.getString("lkup_fee"));
				fee.setTaxFlag(rs.getString("tax_flag"));

				fees.add(fee);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getFeeList() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		} finally {
			logger.info("Exiting getFees()");
		}

		return fees;
	}

	public Fee getFee(int feeId) {
		logger.info("Entering getFee(" + feeId + ")");

		String sql = "";
		Fee fee = new Fee();

		try {

			CachedRowSet rs = new CachedRowSet();
			sql = "select * from fee where fee_id = " + feeId;
			rs.populate(db.select(sql));

			if (rs.next()) {
				fee.setFeeId(rs.getInt("fee_id"));
				fee.setActType(rs.getString("act_type"));
				fee.setFeeType(rs.getString("fee_type"));
				fee.setCreationDate(rs.getDate("fee_creation_dt"));
				fee.setExpirationDate(rs.getDate("fee_expiration_dt"));
				fee.setSequence(rs.getInt("fee_sequence"));
				fee.setInputFlag(rs.getString("fee_input_fl"));
				fee.setFlagOne(rs.getString("fee_fl_1"));
				fee.setFlagTwo(rs.getString("fee_fl_2"));
				fee.setFlagThree(rs.getString("fee_fl_3"));
				fee.setFlagFour(rs.getString("fee_fl_4"));
				fee.setFeeCalcOne(rs.getString("fee_calc_1"));
				fee.setFeeCalcTwo(rs.getString("fee_calc_2"));
				fee.setFeeCalcThree(rs.getString("fee_calc_3"));
				fee.setFeeCalcFour(rs.getString("fee_calc_4"));
				fee.setDescription(rs.getString("fee_desc"));
				fee.setInitial(rs.getInt("fee_init"));
				fee.setSubtotalLevel(rs.getInt("fee_subt_level"));
				fee.setFactor(rs.getBigDecimal("factor"));
				fee.setFeeFactor(rs.getBigDecimal("fee_factor"));
				fee.setAccount(rs.getInt("fee_account"));
				fee.setMiscellaneous(rs.getString("misc"));
				fee.setType(rs.getString("fee_pc"));
				fee.setTaxFlag(rs.getInt("tax_flag"));
			}
		} catch (Exception e) {
			logger.error("Error in getFee() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		}

		return fee;
	}

	public boolean saveFeeFactors(List feeList) {
		String sql = "";
		boolean success = false;

		try {

			db.beginTransaction();

			for (int i = 0; i < feeList.size(); i++) {
				FeeEdit fee = (FeeEdit) feeList.get(i);
				sql = "update fee set factor=" + fee.getFactor() + ",fee_factor=" + fee.getFeeFactor() + " where fee_id=" + fee.getFeeId();
				db.addBatch(sql);
			}

			db.executeBatch();
			success = true;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return success;
	}

	public boolean saveFee(FeeEdit[] feeList) {
		logger.info("Entering saveFee(" + feeList.length + ")");

		String sql = "";
		int feeId;
		boolean success = false;

		try {

			db.beginTransaction();

			for (int i = 0; i < feeList.length; i++) {
				int actSubtypeId = Operator.toInt(feeList[i].getActivitySubtypeId());
				if (feeList[i].getFeeId().equals("0")) {
					String tmpFeeType = "' '";// StringUtils.checkString(feeList[i].getFeeType());
					logger.debug("Print tmpFeeType " + tmpFeeType);

					sql = "insert into fee (fee_id,act_type,fee_type,fee_creation_dt,fee_expiration_dt,fee_sequence,fee_input_fl,fee_fl_1,fee_fl_2,fee_fl_3,fee_fl_4,fee_calc_1,fee_calc_2,fee_calc_3,fee_calc_4,fee_desc,fee_init,fee_subt_level,fee_factor,factor,fee_account,misc,tax_flag,fee_pc,renewable,account_code,ACT_SUBTYPE_ID,ONLINE_INPUT,FEE_CLASS,ONLINE_RENEWABLE) values (";
					feeId = db.getNextId("FEE_ID");
					sql += (feeId + ",");
					sql += (StringUtils.checkString(feeList[i].getActType()) + "," + tmpFeeType + "," + StringUtils.toOracleDate(feeList[i].getCreationDate()) + "," + StringUtils.toOracleDate(feeList[i].getExpirationDate()) + "," + StringUtils.checkNumber(feeList[i].getSequence()) + "," + StringUtils.checkString(feeList[i].getInputFlag()) + "," + StringUtils.checkString(feeList[i].getFlagOne()) + "," + StringUtils.checkString(feeList[i].getFlagTwo()) + "," + StringUtils.checkString(feeList[i].getFlagThree()) + "," + StringUtils.checkString(feeList[i].getFlagFour()) + "," + StringUtils.checkString(feeList[i].getFeeCalcOne()) + "," + StringUtils.checkString(feeList[i].getFeeCalcTwo()) + "," + StringUtils.checkString(feeList[i].getFeeCalcThree()) + "," + StringUtils.checkString(feeList[i].getFeeCalcFour()) + "," + StringUtils.checkString(feeList[i].getDescription()) + "," + StringUtils.checkNumber(feeList[i].getInitial()) + "," + StringUtils.checkNumber(feeList[i].getSubtotalLevel()) + "," + StringUtils.checkNumber(feeList[i].getFeeFactor()) + "," + StringUtils.checkNumber(feeList[i].getFactor()) + "," + StringUtils.checkString(feeList[i].getAccount()) + "," + StringUtils.checkString(feeList[i].getMiscellaneous()) + "," + StringUtils.checkNumber(feeList[i].getTaxFlag()) + "," + StringUtils.checkString(feeList[i].getType()) + "," + StringUtils.checkString(StringUtils.b2s(feeList[i].isRenewable())) + "," + StringUtils.checkString(feeList[i].getAccountCode()) + "," + actSubtypeId + "," + feeList[i].getOnlineInput()+ "," + StringUtils.checkString(feeList[i].getFeeClass()) + "," + StringUtils.checkString(StringUtils.b2s(feeList[i].isOnlineRenewalbleFlag())) + ")");
				} else {
					feeId = StringUtils.s2i(feeList[i].getFeeId());
					sql = "update fee set " + "act_type=" + StringUtils.checkString(feeList[i].getActType()) + "," + "fee_type=" + StringUtils.checkString(feeList[i].getFeeType()) + "," + "fee_creation_dt=" + StringUtils.toOracleDate(feeList[i].getCreationDate()) + "," + "fee_expiration_dt=" + StringUtils.toOracleDate(feeList[i].getExpirationDate()) + "," + "fee_sequence=" + StringUtils.checkNumber(feeList[i].getSequence()) + "," + "fee_input_fl=" + StringUtils.checkString(feeList[i].getInputFlag()) + "," + "fee_fl_1=" + StringUtils.checkString(feeList[i].getFlagOne()) + "," + "fee_fl_2=" + StringUtils.checkString(feeList[i].getFlagTwo()) + "," + "fee_fl_3=" + StringUtils.checkString(feeList[i].getFlagThree()) + "," + "fee_fl_4=" + StringUtils.checkString(feeList[i].getFlagFour()) + "," + "fee_calc_1=" + StringUtils.checkString(feeList[i].getFeeCalcOne()) + "," + "fee_calc_2=" + StringUtils.checkString(feeList[i].getFeeCalcTwo()) + "," + "fee_calc_3=" + StringUtils.checkString(feeList[i].getFeeCalcThree()) + "," + "fee_calc_4=" + StringUtils.checkString(feeList[i].getFeeCalcFour()) + "," + "fee_desc=" + StringUtils.checkString(feeList[i].getDescription()) + "," + "fee_init=" + StringUtils.checkNumber(feeList[i].getInitial()) + "," + "fee_subt_level=" + StringUtils.checkNumber(feeList[i].getSubtotalLevel()) + "," + "fee_factor=" + StringUtils.checkNumber(feeList[i].getFeeFactor()) + "," + "factor=" + StringUtils.checkNumber(feeList[i].getFactor()) + "," + "fee_account=" + StringUtils.checkString(feeList[i].getAccount()) + "," + "misc=" + StringUtils.checkString(feeList[i].getMiscellaneous()) + "," + "tax_flag=" + StringUtils.checkNumber(feeList[i].getTaxFlag()) + "," + "fee_pc= " + StringUtils.checkString(feeList[i].getType()) + "," + "renewable= " + StringUtils.checkString(StringUtils.b2s(feeList[i].isRenewable())) + "," + "account_code=" + StringUtils.checkString(feeList[i].getAccountCode()) + "," + "ACT_SUBTYPE_ID=" + actSubtypeId + "," + "ONLINE_INPUT=" + feeList[i].getOnlineInput() + "," + "fee_class=" + StringUtils.checkString(feeList[i].getFeeClass()) + "," + "ONLINE_RENEWABLE=" + StringUtils.checkString(StringUtils.b2s(feeList[i].isOnlineRenewalbleFlag())) + " where fee_id =" + feeList[i].getFeeId();
					logger.debug("RRRRRR " + feeList[i].isRenewable());
				}

				logger.info(sql);
				db.addBatch(sql);

				sql = "delete from lkup_fee_ref where fee_id = " + feeList[i].getFeeId();
				logger.info(sql);
				db.addBatch(sql);

				if (feeList[i].getFeeLookup().length() > 0) {
					sql = "insert into lkup_fee_ref values (" + feeId + "," + feeList[i].getFeeLookup() + ")";
					logger.info(sql);
					db.addBatch(sql);
				}
			}

			db.executeBatch();
			success = true;
		} catch (Exception e) {
			db.rollbackTransaction();
			logger.error("Error in saveFee() :" + e.getMessage());
		}

		return success;
	}

	public boolean saveFee(Fee fee) {
		logger.info("Entering saveFee()");

		String sql = "";
		boolean success = false;

		try {

			CachedRowSet rs = new CachedRowSet();

			int actSubtypeId = Operator.toInt(fee.getActivitySubtypeId());
			if (fee.getFeeId() == 0) {
				sql = "insert into fee (fee_id,act_type,fee_type,fee_creation_dt,fee_expiration_dt,fee_sequence,fee_input_fl,fee_fl_1,fee_fl_2,fee_fl_3,fee_fl_4,fee_calc_1,fee_calc_2,fee_calc_3,fee_calc_4,fee_desc,fee_init,fee_subt_level,fee_factor,factor,fee_account,misc,tax_flag,fee_pc,renewable,account_code,ACT_SUBTYPE_ID,ONLINE_INPUT,FEE_CLASS) values (";
				sql += (db.getNextId("FEE_ID") + ",");
			} else {
				sql = "update fee set (act_type,fee_type,fee_creation_dt,fee_expiration_dt,fee_sequence,fee_input_fl,fee_fl_1,fee_fl_2,fee_fl_3,fee_fl_4,fee_calc_1,fee_calc_2,fee_calc_3,fee_calc_4,fee_desc,fee_init,fee_subt_level,fee_factor,factor,fee_account,misc,tax_flag,fee_pc,renewable,account_code,ACT_SUBTYPE_ID,ONLINE_INPUT,FEE_CLASS) = (";
			}

			sql += (StringUtils.checkString(fee.getActType()) + "," + StringUtils.checkString(fee.getFeeType()) + "," + StringUtils.toOracleDate(StringUtils.cal2str(fee.getCreationDate())) + "," + StringUtils.toOracleDate(StringUtils.cal2str(fee.getExpirationDate())) + "," + fee.getSequence() + "," + StringUtils.checkString(fee.getInputFlag()) + "," + StringUtils.checkString(fee.getFlagOne()) + "," + StringUtils.checkString(fee.getFlagTwo()) + "," + StringUtils.checkString(fee.getFlagThree()) + "," + StringUtils.checkString(fee.getFlagFour()) + "," + StringUtils.checkString(fee.getFeeCalcOne()) + "," + StringUtils.checkString(fee.getFeeCalcTwo()) + "," + StringUtils.checkString(fee.getFeeCalcThree()) + "," + StringUtils.checkString(fee.getFeeCalcFour()) + "," + StringUtils.checkString(fee.getDescription()) + "," + fee.getInitial() + "," + fee.getSubtotalLevel() + "," + fee.getFeeFactor() + "," + fee.getFactor() + "," + fee.getAccount() + "," + StringUtils.checkString(fee.getMiscellaneous()) + "," + StringUtils.checkNumber(fee.getTaxFlag()) + "," + StringUtils.checkString(fee.getType()) + "," + StringUtils.checkString(StringUtils.b2s(fee.isRenewable())) + "," + StringUtils.checkString(fee.getAccountCode()) + "," + actSubtypeId + "," + fee.getOnlineInput() + "," + StringUtils.checkString(fee.getFeeClass()) + ")");

			if (fee.getFeeId() != 0) {
				sql += (" where fee_id =" + fee.getFeeId());
			}

			logger.debug(sql);
			db.update(sql);
			success = true;
		} catch (Exception e) {
			logger.error("Error in saveFee() : sql : " + sql + ":" + e.getMessage());
		}
		return success;
	}

	public boolean deleteFees(List feeList) {
		boolean success = false;
		FeeEdit fee = null;

		logger.info("Entering deleteFees() with " + feeList.size() + " elements");

		try {
			String sql = "delete from fee where fee_id in (";

			for (int i = 0; i < feeList.size(); i++) {
				fee = (FeeEdit) feeList.get(i);
				sql += (fee.getFeeId() + ",");
			}

			sql += "-1)";
			logger.debug(sql);
			new Wrapper().update(sql);
			success = true;
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return success;
	}

	/**
	 * This function is used to initialize the fees before making modification to the next fiscal year fees.
	 * 
	 * @param iDate
	 * @return
	 * @throws Exception
	 */
	public boolean initializeFees(Calendar iDate) throws Exception {
		logger.info("initializeFees(" + iDate + ")");

		boolean success = false;
		RowSet rs = null;
		String sql = "";
		int minFeeKey = 0;
		int maxFeeKey = 0;
		int feeKeyIncrement = 0;

		int minLkupKey = 0;
		int maxLkupKey = 0;
		int lkupKeyIncrement = 0;

		logger.info("Entering initializeFees()");

		try {

			sql = "select min(fee_id) as min_fee_id,max(fee_id) as max_fee_id from fee where fee_id>0 and fee_expiration_dt is null";
			logger.debug(sql);

			rs = db.select(sql);

			if (rs.next()) {
				minFeeKey = rs.getInt("min_fee_id");
				logger.debug("the min fee key is " + minFeeKey);
				maxFeeKey = rs.getInt("max_fee_id");
				logger.debug("the max fee key is " + maxFeeKey);
				feeKeyIncrement = maxFeeKey - minFeeKey + 1;
				logger.debug("the fee key increment is " + feeKeyIncrement);
			}

			sql = "select min(lkup_fee) as min_fee_lkup,max(lkup_fee) as max_fee_lkup from lkup_fee where fee_expiration_dt is null";
			logger.debug(sql);

			rs = db.select(sql);

			if (rs.next()) {
				minLkupKey = rs.getInt("min_fee_lkup");
				logger.debug("the min lkup key is " + minLkupKey);
				maxLkupKey = rs.getInt("max_fee_lkup");
				logger.debug("the min lkup key is " + maxLkupKey);
				lkupKeyIncrement = maxLkupKey - minLkupKey + 1;
				logger.debug("the lkup key increment is " + lkupKeyIncrement);
			}

			db.beginTransaction();

			sql = "insert into fee select fee_id+" + feeKeyIncrement + " as fee_id,act_type,fee_type," + "to_date(" + StringUtils.checkString(StringUtils.cal2str(iDate)) + ",'MM/DD/YYYY')" + ",fee_expiration_dt,fee_sequence,fee_input_fl,fee_fl_1,fee_fl_2,fee_fl_3,fee_fl_4,fee_calc_1,fee_calc_2,fee_calc_3,fee_calc_4,fee_desc,fee_init,fee_subt_level,fee_factor,factor,fee_account,misc,fee_pc,tax_flag,lso_use,renewable,act_subtype_id,online_input,fee_class,account_code, ONLINE_RENEWABLE, FEE_ONLINE_DESC, ONLINE_FLAG, READY_TO_PAY,DEPENDENT_FEE  from fee where fee_id>0 and fee_expiration_dt is null";
			logger.debug(sql);
			db.addBatch(sql);

			sql = "insert into lkup_fee select lkup_fee+" + lkupKeyIncrement + " as lkup_fee,lkup_fee_name," + StringUtils.toOracleDate(StringUtils.cal2str(iDate)) + ",fee_expiration_dt,low_range,high_range,result,plus,over,update_flag  from lkup_fee where fee_expiration_dt is null";
			logger.debug(sql);
			db.addBatch(sql);

			// Set iDate to previous day. This will be the expiry day for the old fees
			iDate.add(Calendar.DATE, -1);
			sql = "update fee set fee_expiration_dt = " + StringUtils.toOracleDateTime(StringUtils.cal2str(iDate)) + " where fee_id > 0 and fee_id <= " + maxFeeKey + " and fee_expiration_dt is null";
			logger.debug(sql);
			db.addBatch(sql);

			sql = "update lkup_fee set fee_expiration_dt = " + StringUtils.toOracleDateTime(StringUtils.cal2str(iDate)) + " where lkup_fee <= " + maxLkupKey + " and fee_expiration_dt is null";
			logger.debug(sql);
			db.addBatch(sql);

			// Add relevant entries to the LKUP_FEE_REF table
			sql = "insert into lkup_fee_ref select fee_id + " + feeKeyIncrement + ",lkup_fee + " + lkupKeyIncrement + " from lkup_fee_ref  where fee_id >= " + minFeeKey + " and fee_id <= " + maxFeeKey;
			logger.debug(sql);
			db.addBatch(sql);

			sql = "update nextid set IDVALUE=(select max(fee_id) from fee) where IDNAME='FEE_ID'";
			logger.debug(sql);
			db.addBatch(sql);

			sql = "update nextid set IDVALUE=(select max(lkup_fee) from lkup_fee) where IDNAME='LOOKUP_FEE'";
			logger.debug(sql);
			db.addBatch(sql);
			
			sql = "UPDATE REF_FEE_FIELD SET FEE_ID = FEE_ID + "+ feeKeyIncrement ;
			logger.debug(sql);
			db.addBatch(sql);

			db.executeBatch();
			updateDependentFeeToInitializeFees(feeKeyIncrement);
			success = true;

			return success;
		} catch (Exception e) {
			logger.error(e.getMessage(),e);
			throw new Exception("Exception in initializeFees, message is " + e.getMessage());
		}
	}
	
	private void updateDependentFeeToInitializeFees(int feeKeyIncrement) {
		String sql = "select * from FEE where DEPENDENT_FEE is not null and FEE_EXPIRATION_DT is null";
		
		RowSet rs = null;
		
		String[] dependentFees;
		
		String newDependentFee = "";
		try {
			rs= db.select(sql);
			
			if(rs != null) {
				while(rs.next()) {
					String dependentFee = rs.getString("DEPENDENT_FEE");
					logger.debug(dependentFee);
					if(dependentFee != null && !dependentFee.equalsIgnoreCase("")) {
						dependentFees = dependentFee.split(",");
						newDependentFee = "";
						for(String fee: dependentFees) {
							int newFeeId = StringUtils.s2i(fee)+ feeKeyIncrement;
							newDependentFee = newDependentFee + newFeeId +",";
						}
						if(newDependentFee.endsWith(",")) {
							newDependentFee = newDependentFee.substring(0,newDependentFee.lastIndexOf(","));
						}
						String updateSql = "update fee set DEPENDENT_FEE = '"+newDependentFee+"' where FEE_ID = "+ rs.getInt("FEE_ID");
						logger.debug(updateSql);
						db.update(updateSql);
					}
					
					
				}
			}
			logger.debug(newDependentFee);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	
	

	public List getLookupFeeList() {
		logger.info("Entering getLookupFeeList()");

		List feeList = new ArrayList();
		String sql = "";

		try {
			sql = "select lkup_fee, lkup_fee_name,fee_creation_dt  from lkup_fee where fee_expiration_dt is null  group by lkup_fee,lkup_fee_name,fee_creation_dt  order by lkup_fee_name asc,lkup_fee desc";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LookupFee lkupFee = new LookupFee();
				lkupFee.setLookupFeeId(rs.getInt("lkup_fee"));
				lkupFee.setLookupFee(rs.getString("lkup_fee_name"));
				lkupFee.setCreationDate(rs.getDate("fee_creation_dt"));
				feeList.add(lkupFee);
			}
		} catch (Exception e) {
			logger.error("Error in getLookupFeeList() : " + "/n" + "sql : " + sql + ":" + e.getMessage());
		}
		return feeList;
	}

	public List getLookupFee(int lookupFeeId) {
		logger.info("Entering getLookupFee()");

		List feeList = new ArrayList();
		String sql = "";

		try {
			sql = "select * from lkup_fee ";

			if (lookupFeeId > 0) {
				sql += (" where lkup_fee =" + lookupFeeId);
			} else {
				sql += " where fee_expiration_dt is null";
			}

			sql += " order by lkup_fee_name,low_range";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LookupFeeEdit lookupFee = new LookupFeeEdit();
				lookupFee.setLookupFeeId(rs.getString("lkup_fee"));
				lookupFee.setLookupFee(rs.getString("lkup_fee_name"));
				lookupFee.setCreationDate(rs.getDate("fee_creation_dt"));
				lookupFee.setLowRange(rs.getString("low_range"));
				lookupFee.setHighRange(rs.getString("high_range"));
				lookupFee.setOver(rs.getString("over"));
				lookupFee.setPlus(rs.getString("plus"));
				lookupFee.setResult(rs.getString("result"));
				feeList.add(lookupFee);
			}
		} catch (Exception e) {
			logger.error("Error in getLookupFee() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		}
		return feeList;
	}

	/**
	 * This method deletes all the old entries from the lkup_fee table and inserts the entries submitted for the user. If the lookupFeeId is zero then we are inserting a new set of lookup fees
	 */
	public boolean setLookupFee(int lookupFeeId, List feeList) {
		logger.info("Entering setLookupFee(" + lookupFeeId + ") " + feeList.size() + " elements");

		RowSet rs = null;
		boolean success = false;
		String sql = "";
		Calendar lookupCreationDt = Calendar.getInstance();
		Calendar lookupExpirationDt = Calendar.getInstance();
		String lookupFeeName = "";
		String lookupUpdateFlag = "";

		try {

			LookupFee lookupFee = null;

			if (lookupFeeId == 0) {
				lookupFeeId = db.getNextId("LOOKUP_FEE");
				lookupFee = (LookupFee) feeList.get(0);
				lookupCreationDt = lookupFee.getCreationDate();
				lookupExpirationDt = lookupFee.getExpirationDate();
				lookupFeeName = lookupFee.getLookupFee();
				lookupUpdateFlag = "Y";
				db.beginTransaction();
			} else {
				sql = "select * from lkup_fee where lkup_fee = " + lookupFeeId;
				rs = db.select(sql);

				if (rs.next()) {
					lookupCreationDt.setTime(rs.getDate("fee_creation_dt"));

					if (rs.getDate("fee_expiration_dt") != null) {
						lookupExpirationDt.setTime(rs.getDate("fee_expiration_dt"));
					} else {
						lookupExpirationDt = null;
					}

					lookupFeeName = rs.getString("lkup_fee_name");
					lookupUpdateFlag = rs.getString("update_flag");
				}

				logger.info(lookupFeeName + "..." + lookupUpdateFlag);
				db.beginTransaction();
				sql = "delete from lkup_fee where lkup_fee = " + lookupFeeId;
				logger.info(sql);
				db.addBatch(sql);
			}

			for (int i = 0; i < feeList.size(); i++) {
				lookupFee = (LookupFee) feeList.get(i);
				lookupFee.setLookupFeeId(lookupFeeId);
				logger.info("Processing (" + i + ") " + lookupFee);
				sql = "insert into lkup_fee (lkup_fee,lkup_fee_name,fee_creation_dt,fee_expiration_dt,low_range,high_range,result,plus,over,update_flag) values (";
				sql += (lookupFee.getLookupFeeId() + ",'");
				sql += (lookupFeeName + "',");
				sql += StringUtils.toOracleDate(StringUtils.cal2str(lookupCreationDt)) + ",";

				if (lookupExpirationDt == null) {
					sql += "null,";
				} else {
					sql += StringUtils.toOracleDate(StringUtils.cal2str(lookupExpirationDt)) + ",";
				}

				sql += (lookupFee.getLowRange() + ",");
				sql += (lookupFee.getHighRange() + ",");
				sql += (lookupFee.getResult() + ",");
				sql += (lookupFee.getPlus() + ",");
				sql += (lookupFee.getOver() + ",");
				sql += (StringUtils.checkString(lookupUpdateFlag) + ")");
				db.addBatch(sql);
			}

			db.executeBatch();
		} catch (Exception e) {
			logger.error("Error in FeeAgent setLookupFee()() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		}
		return success;
	}

	/**
	 * gets the list of fee id mapped for activity subtype RONPER and updates the new fee id generated for the old ones.
	 */
	public void updateFeeActSubType() {
		logger.info("Entered into updateFeeActSubType...");
		String sql = "";
		String newSql = "";
		String updatesSql = "";

		RowSet rs = null;
		RowSet rsFee = null;

		try {
			sql = "select FEE_ID, ACT_TYPE, FEE_DESC from FEE where FEE_ID IN (select distinct FEE_ID from FEE_ACTSUBTYPE)";
			rs = db.select(sql);
			while (rs.next()) {
				int oldFeeId = rs.getInt("FEE_ID");
				int newFeeId = 0;
				newSql = "select FEE_ID from FEE where ACT_TYPE = '" + rs.getString("ACT_TYPE") + "' and FEE_DESC = '" + rs.getString("FEE_DESC") + "' and FEE_EXPIRATION_DT is NULL";
				// logger.debug("New fee sql : "+newSql);
				rsFee = db.select(newSql);
				if (rsFee.next()) {
					newFeeId = rsFee.getInt("FEE_ID");
					updatesSql = "update FEE_ACTSUBTYPE set FEE_ID = " + newFeeId + " where FEE_ID = " + oldFeeId;
					// logger.debug("updating FeeActSubType : "+updatesSql);
					db.update(updatesSql);
				}
				rsFee.close();
			}
			rs.close();
		} catch (Exception e) {
			logger.error("error while updating FeeActSubType ..." + e.getMessage());
		}
	}

	/**
	 * Save Contractor Business Tax Fees Lookup data
	 * 
	 * @param lookupFeesForm
	 * @param user
	 * @throws Exception
	 */
	public void saveBusTax(LookupFeesForm lookupFeesForm, User user) throws Exception {
		logger.debug("Entered saveBusTax()..");

		try {

			String sql = "UPDATE LKUP_FEE_BUS_TAX SET BUS_TAX_MIN = " + lookupFeesForm.getBusTaxMin() + ",BUS_TAX_MAX = " + lookupFeesForm.getBusTaxMax() + ",BUS_TAX = " + StringUtils.checkString(lookupFeesForm.getBusTax()) + ",UPDATED_BY=" + user.getUserId() + "";
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Exception while trying to insert " + e.getMessage());
			throw new Exception("saveBusTax  UPDATE FAILED " + e.getMessage());
		}
	}

	/**
	 * Get Contractor Business Tax Fee Lookup Data
	 * 
	 * @param lpFeesForm
	 * @return
	 */
	public List getBusTaxList(LookupFeesForm lpFeesForm) {
		logger.info("Entering getBusTaxList()");

		List feeList = new ArrayList();
		String sql = "";

		try {
			sql = "select * from LKUP_FEE_BUS_TAX order by updated desc";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				LookupFeesForm lookupFeesForm = new LookupFeesForm();
				lookupFeesForm.setBusTaxMin(rs.getInt("BUS_TAX_MIN"));
				lookupFeesForm.setBusTaxMax(rs.getInt("BUS_TAX_MAX"));
				lookupFeesForm.setBusTax(rs.getString("BUS_TAX"));
				feeList.add(lookupFeesForm);
			}
		} catch (Exception e) {
			logger.error("Error in getBusTaxList() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		}
		return feeList;
	}

	public Map getBusTaxList() {
		logger.info("Entering getBusTaxList()");

		Map feeList = new HashMap();
		String sql = "";

		try {
			sql = "select * from LKUP_FEE_BUS_TAX  order by updated desc";
			logger.debug(sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				LookupFeesForm lookupFeesForm = new LookupFeesForm();
				feeList.put("busTaxMin", StringUtils.i2s(rs.getInt("BUS_TAX_MIN")));
				feeList.put("busTaxMax", StringUtils.i2s(rs.getInt("BUS_TAX_MAX")));
				feeList.put("busTax", rs.getString("BUS_TAX"));
				break;
			}
		} catch (Exception e) {
			logger.error("Error in getBusTaxList() : " + "\n" + "sql : " + sql + ":" + e.getMessage());
		}
		return feeList;
	}

	public boolean getRevenue(PostRevenueForm form) {
		logger.info("Entering getRevenue()");
		RevenueEdit[] revenueArray = null;
		List accountList = new LinkedList();
		double totalAmount = 0.0d;
		double postedAmount = 0.0d;
		String sql;

		try {
			/**
			 * First check if there is any entry in the revenue table for the Date and Department that has been entered
			 */

			sql = "select count(*) from revenue where " + "dept_code = '" + form.getInputDepartment() + "' and " + "transaction_date= '" + form.getInputDate() + "'";
			logger.debug(sql);
			RowSet rs = db.select(sql);
			int revenueCount = 0;
			if (rs.next()) {
				revenueCount = rs.getInt(1);
			}

			// Form the SQl Query based on whether there is ny entry in the revenue table
			if (revenueCount > 0)
				sql = "select r.batch_nbr,r.batch_date,r.account_nbr as fee_account,sum(pd.amnt) as amount,r.batch_amt,r.comments" + " from revenue r left outer join (payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id and p.pymnt_method not in ('cityJob','billtoFA','sierra','refund','bounced','nsf','nonRevenue') and p.void_dt is null) " + " on r.account_nbr=pd.fee_account and r.dept_code=p.dept_code and r.transaction_date=p.pymnt_dt " + " where r.transaction_date='" + form.getInputDate() + "' and " + " r.dept_code = '" + form.getInputDepartment() + "'" + " group by r.account_nbr,r.batch_nbr,r.batch_date,r.batch_amt,r.comments";
			else
				sql = "select r.batch_nbr,r.batch_date,pd.fee_account,sum(pd.amnt) as amount,sum(pd.amnt) as batch_amt,r.comments from " + " (payment p join payment_detail pd on p.pymnt_id=pd.pymnt_id) " + " left outer join revenue r on r.account_nbr=pd.fee_account and r.dept_code=p.dept_code and r.transaction_date=p.pymnt_dt " + " where p.pymnt_dt='" + form.getInputDate() + "' and " + " p.dept_code = '" + form.getInputDepartment() + "' and " + " p.pymnt_method not in ('cityJob','billtoFA','sierra','refund','bounced','nsf','nonRevenue') " + " group by pd.fee_account,r.batch_nbr,r.batch_date,r.batch_amt,r.comments";
			logger.debug(sql);
			rs = db.select(sql);
			while (rs.next()) {
				form.setBatchNbr(rs.getString("batch_nbr"));
				form.setBatchDate(StringUtils.date2str(rs.getDate("batch_date")));
				RevenueEdit re = new RevenueEdit(rs.getString("fee_account"), rs.getDouble("amount"), rs.getDouble("batch_amt"), rs.getString("comments"));
				totalAmount += rs.getDouble("amount");
				postedAmount += rs.getDouble("batch_amt");
				accountList.add(re);
			}

			// Assign a new batch number if one does not currently exist
			if (form.getBatchNbr() == null)
				form.setBatchNbr("" + db.getNextId("BATCH_NBR"));

			revenueArray = new RevenueEdit[accountList.size()];
			for (int i = 0; i < revenueArray.length; i++) {
				revenueArray[i] = (RevenueEdit) accountList.get(i);
			}

			form.setDeptCode(form.getInputDepartment());
			form.setTransactionDate(form.getInputDate());
			form.setTotalRevenue(StringUtils.dbl2$(totalAmount));
			form.setTotalRevenuePosted(StringUtils.dbl2$(postedAmount));
			form.setAccountList(revenueArray);

		} catch (Exception e) {
			logger.error("Error in FinanceAgent getActivityFeeList() " + e.getMessage());
		}

		logger.info("Exiting getRevenue(" + accountList.size() + ")");
		return true;

	}

	public boolean saveRevenue(User user, PostRevenueForm form) {
		logger.info("Entering saveRevenue()");
		String sql;
		try {
			sql = "delete from revenue where dept_code = '" + form.getDeptCode() + "' and transaction_date='" + form.getTransactionDate() + "'";
			db.update(sql);

			db.beginTransaction();
			RevenueEdit[] accountList = form.getAccountList();
			for (int i = 0; i < accountList.length; i++) {
				if ((StringUtils.$2dbl(accountList[i].getPostedAmt()) != 0.00d) || (StringUtils.$2dbl(accountList[i].getRevenueAmt()) != 0.0d)) {
					sql = "insert into revenue (batch_nbr,batch_date,dept_code,account_nbr,batch_amt,transaction_date,revenue_amt,comments,updated_by,updated) values (" + form.getBatchNbr() + ", null,'" + form.getDeptCode() + "'," + accountList[i].getAccountNbr() + "," + StringUtils.$2dbl(accountList[i].getPostedAmt()) + ",'" + form.getTransactionDate() + "'," + StringUtils.$2dbl(accountList[i].getRevenueAmt()) + ",'" + accountList[i].getComments() + "'," + user.getUserId() + ",current_timestamp)";
					db.addBatch(sql);
				}
			}
			db.executeBatch();

		} catch (Exception e) {
			logger.error("Error in saveRevenue() " + e.getMessage());
		}

		logger.info("Exiting saveRevenue()");
		return true;

	}
	public double getActivityBalance(String actId) throws AgentException {
		double activityBalance = 0.0;

		RowSet rs = null;
		Wrapper db = new Wrapper();
		String sql = "select (sum(FEE_AMNT) - sum(FEE_PAID) - sum(FEE_CREDIT) - sum(FEE_ADJUSTMENT) + sum(BOUNCED_AMNT)) as balance from activity_fee where activity_id  = " + actId;
		try {
			rs = db.select(sql);
			if (rs != null && rs.next()) {
				activityBalance = rs.getDouble("balance");
			}
		} catch (Exception e) {
			logger.error("error while getting balance for actiivty", e);
			throw new AgentException("", e);
		}
		return activityBalance;
	}

	public Payment getLastPayment() {
		Payment payment = new Payment();
		String sql = "select * from payment where PYMNT_ID in  (select PYMNT_ID from PAYMENT_DETAIL where act_id = 879947) order by PYMNT_DT desc";

		RowSet rs = null;
		Wrapper db = new Wrapper();

		try {
			rs = db.select(sql);

			if (rs != null && rs.next()) {

				payment.setPaymentDate(rs.getDate("PYMNT_DT"));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return payment;
	}
	public String getReadyToPayFlag(int activityId){
		String sql = "SELECT READY_TO_PAY FROM ONLINE_PAYMENT_FLAG WHERE ACT_ID = "+activityId;
		logger.debug(sql);
		RowSet rs = null;
		Wrapper db = new Wrapper();
		String readyToPay = "N";
		try {
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				readyToPay = rs.getString("READY_TO_PAY");
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {

			}
		}
		return readyToPay;
	}
	public void updateReadyToPayFlag(int activityId, String readyToPay,int userId) {
		String sql = "SELECT ACT_ID FROM ONLINE_PAYMENT_FLAG WHERE ACT_ID = "+activityId;
		logger.debug(sql);
		RowSet rs = null;
		Wrapper db = new Wrapper();
		int actId = 0;
		try {
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				actId = rs.getInt("ACT_ID");
			}
			if(actId>0){
				sql = "UPDATE ONLINE_PAYMENT_FLAG SET READY_TO_PAY = '"+readyToPay+"',UPDATED_BY = "+userId+",UPDATED = CURRENT_DATE WHERE ACT_ID = "+actId;
			}else{
				sql = "INSERT INTO ONLINE_PAYMENT_FLAG(ACT_ID,READY_TO_PAY,CREATED_BY,CREATED,UPDATED_BY,UPDATED) VALUES("+activityId+",'"+readyToPay+"',"+userId+",CURRENT_DATE,"+userId+",CURRENT_DATE)";
			}
			logger.debug(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}finally{
			try{
				if(rs != null)
				{
					rs.close();
				}
			}catch(Exception e){
				
			}
		}
		
	}

public List<Integer> getReadyToPayFlagEnabledFeeList(int activityId) throws Exception {
		
		int commercialUse = 2;
		RowSet rs = null;
		String sql = "";
		String useSql;
		Wrapper db = new Wrapper();
		List<Integer> readyToPayFalgEnabledFeeList = new ArrayList<Integer>();
		try {
			// First get the LAND_USE (Commercial or Residential)
			sql = "SELECT U.LAND_ID FROM (LAND_USAGE U JOIN V_LSO_LAND L ON U.LSO_USE_ID=" + commercialUse
					+ " AND L.LAND_ID=U.LAND_ID) JOIN V_PSA_LIST P ON P.LSO_ID=L.LSO_ID WHERE P.ACT_ID=" + activityId;
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {
				useSql = "((f.lso_use is null) or (f.lso_use = " + commercialUse + "))";
			} else {
				useSql = "((f.lso_use is null) or (f.lso_use != " + commercialUse + "))";
			}
			rs = new CachedRowSet();
			sql = "select f.fee_id, f.fee_init  as feeInit from (fee f join activity a on  a.act_type=f.act_type  and fee_calc_1 is not null  and "
					+ "((fee_fl_4 in ('0','2','4') and (f.fee_creation_dt <= a.permit_fee_date ))"
					+ " or (fee_fl_4 in '1' and (f.fee_creation_dt <= a.plan_chk_fee_date ))"
					+ " or (fee_fl_4 in '3' and (f.fee_creation_dt <= a.development_fee_date ))) "
					+ "and ((fee_fl_4 in ('0','2','4') and (f.fee_expiration_dt >= a.permit_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '1' and (f.fee_expiration_dt >= a.plan_chk_fee_date or f.fee_expiration_dt is null)) or (fee_fl_4 in '3' and (f.fee_expiration_dt >= a.development_fee_date or f.fee_expiration_dt is null))) and a.act_id ="
					+ activityId + ")"
					+ " left outer join activity_fee af on af.activity_id = a.act_id and af.fee_id=f.fee_id and af.fee_amnt > 0.0"
					+ " where (f.fee_pc in ('P','C','B','X','D')) and f.READY_TO_PAY = 'Y'" + " and " + useSql + " order by f.fee_sequence";

			logger.debug(sql);

			rs = db.select(sql);

			while (rs != null && rs.next()) {
				readyToPayFalgEnabledFeeList.add(rs.getInt("FEE_ID"));
				logger.debug(readyToPayFalgEnabledFeeList.toString());
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e) {

			}
		}
		
		logger.debug("readyToPayFalgEnabledFeeList" + readyToPayFalgEnabledFeeList.toString());
		return readyToPayFalgEnabledFeeList;
	}
public boolean getActivityFeeBalanceAmount(Wrapper db, ActivityFee activityFee) {
	logger.info("getActivityFeeBalanceAmount(Wrapper, " + activityFee + ")");

	double feeAmount = 0.00;
	boolean feeBalance = true;
	RowSet rs = null;
	try {
		rs = new CachedRowSet();
			String sql = "select (fee_amnt-fee_paid) as balance,fee_paid from activity_fee where activity_id = " + activityFee.getActivityId() + " and fee_id = " + activityFee.getFeeId();
			logger.debug(sql);
			rs = db.select(sql);

			if (rs.next()) {
				if (rs.getDouble("BALANCE") != 0.00d || (activityFee.getFeeAmount() > rs.getDouble("FEE_PAID"))) {
					logger.debug("balance is greater than 0.00d ,calculate fee = true");
					feeBalance = true;
				}
				else {
					logger.debug("balance is 0.00d,calculate fee = false");
					feeBalance = false;
				}
			} else {
				logger.debug("Resultset does not have any values, calculate fee = true");
				feeBalance = true;
			}
	}catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {

		}
	}
	
	return feeBalance;
	}

public void reverseSpecificTransaction(int activityId, int paymentId, User user) {
	logger.debug("reverseSpecificTransaction("+ activityId+","+  paymentId+","+user+")");
	RowSet rs = null;
	Payment payment = new Payment();
	PayType payType= new PayType();
	
	StringBuilder GET_PAYMENT_SQL = new StringBuilder();
	GET_PAYMENT_SQL.append("SELECT PYMNT_AMNT,PAYEE FROM PAYMENT WHERE PYMNT_ID =");
	GET_PAYMENT_SQL.append(paymentId);
	
	logger.debug(GET_PAYMENT_SQL.toString());
	
	try {
		rs = new Wrapper().select(GET_PAYMENT_SQL.toString());
		if(rs != null && rs.next()){
			payment.setAmount(rs.getDouble("PYMNT_AMNT"));
			logger.debug("Transaction Amount::"+payment.getAmount());
			payment.setPayee(rs.getString("PAYEE"));
			logger.debug("PAYEE::"+payment.getPayee());
		}
		
		payment.setEnteredBy(user);
		payment.setPeopleId(user.getUserId());
		payment.setDepartmentCode(user.getDepartment().getDepartmentCode());
		payment.setPaymentId(paymentId);
		payType.setPayTypeId(9);
		payment.setType(payType);
		//payment.setReversal("N");
		
		voidPayment(payment, activityId);
		
	} catch (Exception e) {
		logger.error(e);
		e.printStackTrace();
	} finally {
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {

		}
	}
	
}

	public void updateVoidDetails(Payment payment) {
		String sql = "update payment set void = '"+payment.getVoided()+"' WHERE pymnt_id=" + payment.getPaymentId();//,void_by = "+ payment.getEnteredBy().getUserId() + ",void_dt = CURRENT_DATE
		
			logger.info(sql);
			try {
				db.insert(sql);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			logger.info(sql);
		

	}

	
}

package elms.agent;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivityType;
import elms.app.admin.ProjectStatus;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.common.Agent;
import elms.app.common.AttachmentResult;
import elms.app.lso.Lso;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.control.beans.DocumentSearchForm;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * The search database connector and agent.
 * 
 * @author Venkata Kastala (venkata@edgesoftinc.com) updated - Jan 22, 2004, 5:00:00 PM - Venkata Kastala
 */

public class SearchAgent extends Agent {

	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(SearchAgent.class.getName());

	/**
	 * The default constructor
	 */
	public SearchAgent() {
		// default constructor
	}

	/**
	 * This returns the search results for the document search.
	 * 
	 * @param documentSearchForm
	 * @return
	 * @throws Exception
	 */
	public List getDocumentSearchResults(DocumentSearchForm documentSearchForm, String contextRoot) throws Exception {
		logger.debug("getDocumentSearchResults(" + documentSearchForm + "," + contextRoot + ")");

		ResourceBundle obcProperties = Wrapper.getResourceBundle();
		String DESTINATION_SERVER_URL = "/attachments/";
		String CONTEXT_ROOT = contextRoot;
		DESTINATION_SERVER_URL = CONTEXT_ROOT + DESTINATION_SERVER_URL;

		List documentList = new ArrayList();
		String sql = "";
		String whereString = "";
		documentSearchForm.setAddressSearch(false);
		whereString = getSqlByKeyWords(documentSearchForm) + getSqlByAddressAndPSA(documentSearchForm);
		sql = "select * from attachments";
		if (!whereString.equals("")) {
			sql = sql + " where " + whereString;
			sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
		}
		logger.debug("" + documentSearchForm.isAddressSearch());
		logger.debug(documentSearchForm.getLsoList());

		if (documentSearchForm.isAddressSearch()) {
			sql += " UNION SELECT * FROM ATTACHMENTS WHERE ATTACH_LEVEL ='A' AND LEVEL_ID IN (SELECT ACT_ID FROM V_PSA_LIST WHERE LSO_ID IN (" + documentSearchForm.getLsoList() + "))";
			sql += " UNION SELECT * FROM ATTACHMENTS WHERE ATTACH_LEVEL ='P' AND LEVEL_ID IN (SELECT PROJ_ID FROM V_PSA_LIST WHERE LSO_ID IN (" + documentSearchForm.getLsoList() + "))";
			sql += " UNION SELECT * FROM ATTACHMENTS WHERE ATTACH_LEVEL ='Q' AND LEVEL_ID IN (SELECT SPROJ_ID FROM V_PSA_LIST WHERE LSO_ID IN (" + documentSearchForm.getLsoList() + "))";
		}

		logger.debug("The sql string in the main method is " + sql);
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				AttachmentResult attachmentResult = new AttachmentResult();
				attachmentResult.setAttachmentId(rs.getString("ATTACH_ID"));
				String level = rs.getString("ATTACH_LEVEL");
				if (level == null)
					level = "";
				else if (level.equals("L"))
					level = "Land";
				else if (level.equals("S"))
					level = "Structure";
				else if (level.equals("O"))
					level = "Occupancy";
				else if (level.equals("P"))
					level = "Project";
				else if (level.equals("Q"))
					level = "Sub Project";
				else if (level.equals("A"))
					level = "Activity";
				attachmentResult.setAttachmentLevel(level);
				attachmentResult.setDescription(rs.getString("description"));
				String fileName = rs.getString("FILE_NAME");
				if (fileName == null)
					fileName = "";
				attachmentResult.setFileName(fileName);
				attachmentResult.setLevelId(rs.getString("LEVEL_ID"));
				attachmentResult.setLocation(rs.getString("LOCATION"));
				attachmentResult.setStatus(rs.getString("STATUS"));
				String fileUrl = DESTINATION_SERVER_URL + fileName;
				attachmentResult.setFileUrl(fileUrl);
				documentList.add(attachmentResult);
			}
			if (rs != null)
				rs.close();
			logger.debug("returning list of attachment results of size " + documentList.size());
			return documentList;
		} catch (Exception e) {
			logger.error("Error occured while searching for documents " + e.getMessage());
			throw e;
		}
	}

	public String getSqlByKeyWords(DocumentSearchForm documentSearchForm) {
		String sql = "";
		sql = documentSearchForm.getKeyword1() == null || documentSearchForm.getKeyword1().equals("") ? sql : sql + " upper(keyword1) like '%" + documentSearchForm.getKeyword1().toUpperCase() + "%' or" + " upper(keyword2) like '%" + documentSearchForm.getKeyword1().toUpperCase() + "%' or" + " upper(keyword3) like '%" + documentSearchForm.getKeyword1().toUpperCase() + "%' or" + " upper(keyword4) like '%" + documentSearchForm.getKeyword1().toUpperCase() + "%' or";
		sql = documentSearchForm.getKeyword2() == null || documentSearchForm.getKeyword2().equals("") ? sql : sql + " upper(keyword1) like '%" + documentSearchForm.getKeyword2().toUpperCase() + "%' or" + " upper(keyword2) like '%" + documentSearchForm.getKeyword2().toUpperCase() + "%' or" + " upper(keyword3) like '%" + documentSearchForm.getKeyword2().toUpperCase() + "%' or" + " upper(keyword4) like '%" + documentSearchForm.getKeyword2().toUpperCase() + "%' or";

		sql = documentSearchForm.getKeyword3() == null || documentSearchForm.getKeyword3().equals("") ? sql : sql + " upper(keyword1) like '%" + documentSearchForm.getKeyword3().toUpperCase() + "%' or" + " upper(keyword2) like '%" + documentSearchForm.getKeyword3().toUpperCase() + "%' or" + " upper(keyword3) like '%" + documentSearchForm.getKeyword3().toUpperCase() + "%' or" + " upper(keyword4) like '%" + documentSearchForm.getKeyword3().toUpperCase() + "%' or";

		sql = documentSearchForm.getKeyword4() == null || documentSearchForm.getKeyword4().equals("") ? sql : sql + " upper(keyword1) like '%" + documentSearchForm.getKeyword4().toUpperCase() + "%' or" + " upper(keyword2) like '%" + documentSearchForm.getKeyword4().toUpperCase() + "%' or" + " upper(keyword3) like '%" + documentSearchForm.getKeyword4().toUpperCase() + "%' or" + " upper(keyword4) like '%" + documentSearchForm.getKeyword4().toUpperCase() + "%' or";

		if (!sql.equals("")) {
			sql = sql.substring(0, sql.length() - 3); // to remove the trailing and
			sql = "(" + sql + ") and";
		}
		return sql;
	}

	public String getSqlByAddress(String streetNumber, String fraction, String streetname, String unit) {
		String sql = "";
		String tempStreetNumber = "";

		if (streetname != null && !streetname.equals("-1")) {
			// format the query string in streetNum to make it suitable for SQL
			if (streetNumber.equals("") || streetNumber.length() == 0) {
				tempStreetNumber = "%";
			}
			for (int i = 0; i < streetNumber.length(); i++) {
				if (Character.isDigit(streetNumber.charAt(i))) {
					tempStreetNumber = tempStreetNumber + streetNumber.charAt(i);
				} else {
					tempStreetNumber = tempStreetNumber + '%';
					break;
				}
			}
			sql = "select lso_id from lso_address where rtrim(cast(str_no as char(10))) like '" + tempStreetNumber + "' and";
			sql = fraction == null || fraction.equals("") ? sql : sql + " str_mod ='" + fraction + "' and";
			sql = sql + " street_id=" + streetname + " and";
			sql = unit == null || unit.equals("") ? sql : sql + " unit ='" + unit + "' and";
			sql = sql.substring(0, sql.length() - 4); // to remove the trailing and
		}
		return sql;
	}

	public String getSqlByAddressAndPSA(DocumentSearchForm documentSearchForm) {
		String activitySql = "";
		String sql = "";
		String levelType = "";

		activitySql = documentSearchForm.getActivityNumber() == null || documentSearchForm.getActivityNumber().equals("") ? activitySql : activitySql + " upper(act_nbr) like '" + documentSearchForm.getActivityNumber().toUpperCase() + "%' and";
		activitySql = documentSearchForm.getActivityDescription() == null || documentSearchForm.getActivityDescription().equals("") ? activitySql : activitySql + " upper(description) like '%" + documentSearchForm.getActivityDescription().toUpperCase() + "%' and";
		activitySql = documentSearchForm.getAppliedDate() == null || documentSearchForm.getAppliedDate().equals("") ? activitySql : activitySql + " applied_date = " + "to_date ('" + documentSearchForm.getAppliedDate() + "','MM/DD/YYYY')" + " and";
		activitySql = documentSearchForm.getIssuedDate() == null || documentSearchForm.getIssuedDate().equals("") ? activitySql : activitySql + " issued_date = " + "to_date ('" + documentSearchForm.getIssuedDate() + "','MM/DD/YYYY')" + " and";
		activitySql = documentSearchForm.getFinaledDate() == null || documentSearchForm.getFinaledDate().equals("") ? activitySql : activitySql + " final_date = " + "to_date ('" + documentSearchForm.getFinaledDate() + "','MM/DD/YYYY')" + " and";

		if (!activitySql.equals(""))
			activitySql = activitySql.substring(0, activitySql.length() - 4); // to remove the trailing and

		if (documentSearchForm.getStreetName() == null || documentSearchForm.getStreetName().equals("-1") || documentSearchForm.getStreetName().equals("")) { // address = NO
			if (documentSearchForm.getProjectNumber() == null || documentSearchForm.getProjectNumber().equals("")) { // address = NO, project = NO
				if (documentSearchForm.getSubProjectNumber() == null || documentSearchForm.getSubProjectNumber().equals("")) { // address = NO, project = NO, sub project = NO
					if (!activitySql.equals("")) { // address = NO, project = NO, sub project = NO, activity = YES
						sql = "select act_id from activity where " + activitySql;
						levelType = "'A'";
					}
				} else { // // address = NO, project = NO, sub project = YES
					if (activitySql.equals("")) { // address = NO, project = NO, sub project = YES, activity = NO
						sql = "select sproj_id from sub_project where upper(sproj_nbr) like '%" + documentSearchForm.getSubProjectNumber() + "%'";
						levelType = "'Q'";
					} else { // address = NO, project = NO, sub project = YES, activity = YES
						sql = "select act_id from v_activity where upper(sproj_nbr) like '" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%' and" + activitySql;
						levelType = "'A'";
					}
				}
			} else { // address = NO, project = YES
				if (documentSearchForm.getSubProjectNumber() == null || documentSearchForm.getSubProjectNumber().equals("")) { // address = NO, project = YES, sub project = NO
					if (activitySql.equals("")) { // address = NO, project = YES, sub project=NO, activity = NO
						sql = "select proj_id from project where upper(project_nbr) like '" + documentSearchForm.getProjectNumber().toUpperCase() + "%'";
						levelType = "'P'";
					} else { // address = NO, project = YES, sub-project = NO, activity = YES
						sql = "select act_id from v_activity where upper(project_nbr) like '" + documentSearchForm.getProjectNumber().toUpperCase() + "%' and" + activitySql;
						levelType = "'A'";
					}
				} else { // address = NO, project = YES, sub project = YES
					if (activitySql.equals("")) { // address = NO, project = YES, sub-project = YES, activity = NO
						sql = "select sproj_id from sub_project where upper(sproj_nbr) like '" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%' and proj_id in(select proj_id from project where upper(project_nbr) like '%" + documentSearchForm.getProjectNumber().toUpperCase() + "%')";
						levelType = "'Q'";
					} else { // address = NO, project = YES, sub-project = NO, activity = YES
						sql = "select act_id from v_activity where upper(project_nbr) like '%" + documentSearchForm.getProjectNumber().toUpperCase() + "%' and upper(sproj_nbr) like '%" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%' and" + activitySql;
						levelType = "'A'";
					}
				}
			}
		} else { // address = YES

			String addressSql = getSqlByAddress(documentSearchForm.getStreetNumber(), documentSearchForm.getStreetFraction(), documentSearchForm.getStreetName(), documentSearchForm.getUnit());
			documentSearchForm.setLsoList(addressSql);
			logger.debug(addressSql);
			if (documentSearchForm.getProjectNumber() == null || documentSearchForm.getProjectNumber().equals("")) { // address = YES, project = NO
				if (documentSearchForm.getSubProjectNumber() == null || documentSearchForm.getSubProjectNumber().equals("")) { // address = YES, project = NO, sub project = NO
					if (activitySql.equals("")) { // address = YES, project = NO, sub project = NO, activity = NO
						logger.debug("Address Search");
						sql = addressSql;
						levelType = "'L','S','O'";
						documentSearchForm.setAddressSearch(true);
					} else { // address = YES, project = NO, sub project = NO, activity = YES
						sql = "select act_id from v_activity where lso_id in(" + addressSql + ")";
						levelType = "'A'";
					}
				} else { // address = YES, project = NO, sub project = YES
					if (activitySql.equals("")) { // address = YES, project = NO, sub project = YES, activity = NO
						sql = "select sproj_id from sub_project where upper(sproj_nbr) like '%" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%' and" + " proj_id in (select proj_id from project where lso_id in(" + addressSql + ")";
						levelType = "'Q')";
					} else { // address = YES, project = NO, sub project = YES, activity = YES
						sql = "select act_id from v_activity where lso_id in(" + addressSql + ") and upper(sproj_nbr) like '%" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%'";
						levelType = "'A'";
					}
				}
			} else { // address = YES, project = YES
				if (documentSearchForm.getSubProjectNumber() == null || documentSearchForm.getSubProjectNumber().equals("")) { // sub project # is not entered
					if (activitySql.equals("")) { // address = YES, project = YES, sub project = NO, activity = NO
						sql = "select proj_id from project where upper(project_nbr) like '" + documentSearchForm.getProjectNumber().toUpperCase() + "%' and lso_id in(" + addressSql + ")";
						levelType = "'P'";
					} else { // address = YES, project = YES, sub project = NO, activity = YES
						sql = "select act_id from v_activity where " + activitySql + " and upper(project_nbr) like '" + documentSearchForm.getProjectNumber().toUpperCase() + "%' and lso_id in(" + addressSql + ")";
						levelType = "'A'";
					}
				} else { // address = YES, project = YES, sub project = YES
					if (activitySql.equals("")) { // address = YES, project = YES, sub project = YES, activity = NO
						sql = "select sproj_id from sub_project where upper(sproj_nbr) like '%" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%' and" + " proj_id in(select proj_id from project where upper(project_nbr) like '%" + documentSearchForm.getProjectNumber().toUpperCase() + "%' and lso_id in(" + addressSql + "))";
						levelType = "'Q'";
					} else { // address = YES, project = YES, sub project = YES, activity = YES
						sql = "select act_id from v_activity where " + activitySql + " and upper(project_nbr) like '" + documentSearchForm.getProjectNumber().toUpperCase() + "%' and upper(sproj_nbr) like '" + documentSearchForm.getSubProjectNumber().toUpperCase() + "%' and lso_id in(" + addressSql + ")";
						levelType = "'A'";
					}
				}
			}
		}
		if (!sql.equals("")) {
			// checking the comments

			logger.debug("The comments are: " + documentSearchForm.getComment());

			sql = documentSearchForm.getComment() == null || documentSearchForm.getComment().equals("") ? " level_id in(" + sql + ") and attach_level in(" + levelType + ") and" : " level_id in(" + sql + ") and attach_level in(" + levelType + ") and" + " level_id in(select level_id from comments where upper(comnt) like '%" + documentSearchForm.getComment().toUpperCase() + "%' and level_id in(" + sql + ") and comnt_level in(" + levelType + ")) and";
		}
		return sql;
	}

	public List searchProjects(String val) {
		List prjList = new ArrayList();
		if (!val.equals("")) {
			String query = "";
			String where = " WHERE ";
			String fullQuery = "SELECT PROJECT.*,LPS.description AS PROJ_STS_DESC,V.ADDRESS FROM (PROJECT LEFT OUTER JOIN LKUP_PROJ_ST LPS ON PROJECT.STATUS_ID=LPS.STATUS_ID) LEFT OUTER JOIN V_ADDRESS_LIST V ON V.LSO_ID=PROJECT.LSO_ID ";

			List list = new ArrayList();

			query = ((val == null) || val.equals("")) ? query : (query + " UPPER(PROJECT_NBR) LIKE '" + (StringUtils.formatQueryForString(val).toUpperCase()) + "%' ");

			if ((query != null) && !query.equals("")) {
				// query += " PROJECT.NAME != 'Public Works' ";
				fullQuery = fullQuery + where + query + " ORDER BY APPLIED_DT DESC";
			}

			logger.debug("SQL is - " + fullQuery);
			try {

				Wrapper db = new Wrapper();
				sun.jdbc.rowset.CachedRowSet rs = (sun.jdbc.rowset.CachedRowSet) new Wrapper().select(fullQuery);

				rs.beforeFirst();
				while (rs.next()) {
					ProjectDetail projectDetail = new ProjectDetail();
					projectDetail.setName(rs.getString("NAME"));
					projectDetail.setDescription(rs.getString("DESCRIPTION"));
					projectDetail.setProjectStatus(new ProjectStatus(rs.getInt("STATUS_ID"), rs.getString("PROJ_STS_DESC")));
					projectDetail.setProjectNumber(rs.getString("PROJECT_NBR"));
					projectDetail.setAddress(rs.getString("ADDRESS"));

					Project project = new Project(rs.getInt("PROJ_ID"), projectDetail);
					project.setLso(new Lso(rs.getInt("LSO_ID")));

					prjList.add(project);
				}
				rs.close();
			} catch (Exception e) {
				logger.debug("Exception in method searchProjects - " + e.getMessage());
			}
		}
		return prjList;
	}

	public List searchSubProjects(String val) {
		List sprjList = new ArrayList();
		String query = "LKUP_SPROJ_TYPE.SPROJ_TYPE_ID=SUB_PROJECT.STYPE_ID AND LKUP_SPROJ_STYPE.SPROJ_STYPE_ID=SUB_PROJECT.SSTYPE_ID AND";
		String where = " WHERE ";
		String fullQuery = "SELECT V.ADDRESS,LKUP_SPROJ_TYPE.SPROJ_TYPE,LKUP_SPROJ_STYPE.TYPE,PROJECT.PROJ_ID,PROJECT.LSO_ID,SUB_PROJECT.* FROM ((SUB_PROJECT JOIN PROJECT ON PROJECT.PROJ_ID=SUB_PROJECT.PROJ_ID) JOIN V_ADDRESS_LIST V ON V.LSO_ID=PROJECT.LSO_ID) JOIN V_STREET_LIST VSL ON VSL.STREET_ID=V.STREET_ID,LKUP_SPROJ_TYPE,LKUP_SPROJ_STYPE  ";
		SubProjectType subProjectType;
		SubProjectSubType subProjectSubType;
		List list = new ArrayList();

		query = ((val == null) || val.equals("")) ? query : (query + " UPPER(SUB_PROJECT.SPROJ_NBR) LIKE '" + (StringUtils.formatQueryForString(val).toUpperCase()) + "%' ");

		if ((query != null) && !query.equals("")) {
			fullQuery = fullQuery + where + query + " ORDER BY VSL.STREET_NAME,V.STR_NO,PROJECT.APPLIED_DT DESC";
		}

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();
			RowSet rs = new Wrapper().select(fullQuery);

			rs.beforeFirst();

			while (rs.next()) {
				SubProjectDetail subProjectDetail = new SubProjectDetail();

				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(rs.getInt("STYPE_ID"));
				subProjectType.setSubProjectType(rs.getString("SPROJ_TYPE"));
				subProjectDetail.setSubProjectType(subProjectType);

				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("SSTYPE_ID"));
				subProjectSubType.setType(rs.getString("TYPE"));
				subProjectDetail.setSubProjectSubType(subProjectSubType);
				subProjectDetail.setSubProjectNumber(rs.getString("SPROJ_NBR"));
				subProjectDetail.setDescription(rs.getString("DESC"));
				subProjectDetail.setLsoId(rs.getInt("LSO_ID"));
				subProjectDetail.setAddress(rs.getString("ADDRESS"));

				SubProject subProject = new SubProject(rs.getInt("SPROJ_ID"), subProjectDetail);
				sprjList.add(subProject);
			}
			rs.close();
		} catch (Exception e) {
			logger.debug("Exception in method searchSubProjects - " + e.getMessage());
		}
		return sprjList;
	}

	public List searchActivities(String val) {
		List actList = new ArrayList();
		String query = "";
		String where = " WHERE ";

		String fullQuery = "select Distinct SP.PROJ_ID,V.LSO_ID,A.*,V.ADDRESS,LAT.DESCRIPTION AS ACT_TYPE_DESC,LAS.STAT_CODE AS ACT_STS_CODE,LAS.DESCRIPTION as ACT_STS_DESC from activity A LEFT OUTER  JOIN SUB_PROJECT SP ON SP.SPROJ_ID=A.SPROJ_ID LEFT OUTER JOIN LKUP_ACT_TYPE LAT ON LAT.TYPE=A.ACT_TYPE LEFT OUTER JOIN V_ADDRESS_LIST V ON V.ADDR_ID=A.ADDR_ID LEFT OUTER JOIN LKUP_ACT_ST LAS ON LAS.STATUS_ID=A.STATUS";

		List list = new ArrayList();
		query = query + " ACT_NBR LIKE '" + val.toUpperCase() + "%'";

		if ((query != null) && !query.equals("")) {
			fullQuery = fullQuery + where + query + " order by a.applied_date desc,a.act_id desc";
		}

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();

			RowSet rs = new Wrapper().select(fullQuery);

			rs.beforeFirst();

			while (rs.next()) {
				ActivityDetail activityDetail = new ActivityDetail();

				activityDetail.setActivityType(new ActivityType(rs.getString("ACT_TYPE"), rs.getString("ACT_TYPE_DESC")));
				activityDetail.setStatus(new ActivityStatus(rs.getInt("STATUS"), rs.getString("ACT_STS_CODE"), rs.getString("ACT_STS_DESC")));
				activityDetail.setDescription(rs.getString("DESCRIPTION"));
				activityDetail.setActivityNumber(rs.getString("ACT_NBR"));
				activityDetail.setLsoId(rs.getInt("LSO_ID"));
				activityDetail.setAppliedDate(rs.getDate("APPLIED_DATE"));
				activityDetail.setExpirationDate(rs.getDate("exp_date"));
				activityDetail.setStartDate(rs.getDate("start_date"));
				activityDetail.setFinaledDate(rs.getDate("final_date"));
				activityDetail.setIssueDate(rs.getDate("ISSUED_DATE"));
				activityDetail.setAddressId(rs.getInt("addr_id"));
				activityDetail.setAddress(rs.getString("address"));

				Activity activity = new Activity(rs.getInt("ACT_ID"), activityDetail);

				actList.add(activity);
			}
			rs.close();
			logger.debug("actList in method searchActivities - " + actList.size());
		} catch (SQLException sql) {
			logger.debug("SQLException in method searchActivities - " + sql.getMessage());
		} catch (Exception e) {
			logger.debug("Exception in method searchActivities - " + e.getMessage());
		}
		return actList;
	}
}
package elms.agent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.ActivityType;
import elms.app.admin.CountByHoldType;
import elms.app.admin.ProjectStatus;
import elms.app.admin.ProjectType;
import elms.app.admin.SubProjectStatus;
import elms.app.admin.SubProjectSubType;
import elms.app.admin.SubProjectType;
import elms.app.common.Agent;
import elms.app.common.DisplayItem;
import elms.app.finance.ProjectFinance;
import elms.app.finance.SubProjectFinance;
import elms.app.lso.Lso;
import elms.app.lso.LsoAddress;
import elms.app.lso.Owner;
import elms.app.lso.PsaTree;
import elms.app.lso.Street;
import elms.app.lso.Use;
import elms.app.people.People;
import elms.app.planning.LinkActivityEdit;
import elms.app.planning.LinkSubProjectEdit;
import elms.app.planning.ResolutionEdit;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.app.project.Project;
import elms.app.project.ProjectDetail;
import elms.app.project.SubProject;
import elms.app.project.SubProjectDetail;
import elms.common.Constants;
import elms.control.beans.admin.StreetListAdminForm;
import elms.exception.AgentException;
import elms.security.Department;
import elms.security.User;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class ProjectAgent extends Agent {

	static Logger logger = Logger.getLogger(ProjectAgent.class.getName());
	protected Project project = null;
	protected int projectId;
	protected int lsoId = -1;
	protected Lso lso = null;
	protected SubProject subProject = null;
	protected ProjectFinance projcetFinance = null;
	protected List peopleList = null;
	protected People peopleObj = null;
	protected List processTeamList = null;
	protected List psaAttachmentList = null;
	protected List pasConditionList = null;
	protected List psaHoldList = null;
	protected List psaCommentList = null;
	protected ProjectDetail projectDetail = null;
	protected Use use = null;
	protected ProjectType projectType = null;
	protected SubProjectType subProjectType = null;
	protected SubProjectSubType subProjectSubType = null;
	protected List subProjects = null;
	protected SubProjectDetail subProjectDetail = null;
	protected List activities = null;
	protected Activity activity = null;
	protected SubProjectFinance subProjectFinance = null;
	protected User user = null;
	protected Department department = null;
	protected int subProjectId = -1;
	protected Street street = null;

	/**
	 * The SQL Queries
	 * 
	 */
	public static final String GET_LSO_TYPE = "select lso_type from v_lso_type where lso_id=?";
	public static final String GET_STRUCTURE = "select structure_id from land_structure where land_id=?";
	public static final String GET_OCCUPANCY = "select occupancy_id from structure_occupant where structure_id = ?";
	public static final String GET_SUB_PROJECTS = "select sp.sproj_id,sp.sproj_nbr,sp.description,sp.label,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where proj_id = ? and lsps.psa_active = 'Y' order by created desc,sproj_id desc";
	public static final String GET_ALL_SUB_PROJECTS = "select sp.sproj_id,sp.sproj_nbr,sp.description,sp.label,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where proj_id = ?  order by created desc,sproj_id desc";
	public static final String GET_ACTIVITIES = "select a.act_id,a.act_nbr,replace(replace(a.description,chr(10),''),chr(13),'') as description,a.label,a.status as actStatus,las.description as status,lat.description as act_type,las.psa_active as psa_active from activity a,lkup_act_st las,lkup_act_type lat where a.sproj_id = ? and a.status = las.status_id and a.act_type = lat.type and las.psa_active = 'Y' order by a.created desc,a.act_id desc";
	public static final String GET_ALL_ACTIVITIES = "select a.act_id,a.act_nbr,replace(replace(a.description,chr(10),''),chr(13),'') as description,a.label,a.status as actStatus,las.description as status,lat.description as act_type,las.psa_active as psa_active from activity a,lkup_act_st las,lkup_act_type lat where a.sproj_id = ? and a.status = las.status_id and a.act_type = lat.type order by a.created desc,a.act_id desc";

	public static final String GET_SUB_PROJECTS_NONLOCATIONAL = "select sp.sproj_id,sp.sproj_nbr,sp.description,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where proj_id in (?) and sp.sproj_id in (?) and lsps.psa_active = 'Y' order by created desc,sproj_id desc";
	public static final String GET_ALL_SUB_PROJECTS_NONLOCATIONAL = "select sp.sproj_id,sp.sproj_nbr,sp.description,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where proj_id in (?) and sp.sproj_id in (?) order by created desc,sproj_id desc";

	/**
	 * The PSA Tree List
	 */
	public List psaTreeList = null;

	/**
	 * The PSA Tree
	 */
	public PsaTree psaTree = null;

	/**
	 * 3CF6993601D3
	 */
	public ProjectAgent() {

		// default constructor
	}

	/**
	 * gets the project object for a project id.
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public Project getProject(int projectId) throws Exception {

		try {

			Wrapper db = new Wrapper();

			String sql = "select p.*,val.address from (project p join v_address_list val on val.lso_id=p.lso_id) where p.proj_id=" + projectId;

			logger.debug("The select Sql of Project is " + sql);
			RowSet rs = db.select(sql);
			projectDetail = new ProjectDetail();
			if (rs.next()) {

				projectDetail.setAddress(rs.getString("address"));
				projectDetail.setProjectNumber(rs.getString("project_nbr"));
				logger.debug("Project Number is .. " + projectDetail.getProjectNumber());
				projectDetail.setName(rs.getString("name"));
				logger.debug("Name is .. " + projectDetail.getName());
				projectDetail.setDescription(rs.getString("description"));
				logger.debug("Description  is .. " + projectDetail.getDescription());
				projectDetail.setStartDate(rs.getDate("applied_dt"));
				logger.debug("Start Date is .. " + StringUtils.cal2str(projectDetail.getStartDate()));
				projectDetail.setCompletionDate(rs.getDate("completion_dt"));
				logger.debug("Completion Date  is .. " + StringUtils.cal2str(projectDetail.getCompletionDate()));
				projectDetail.setExpriationDate(rs.getDate("expired_dt"));
				logger.debug("Expiration Date  is .. " + StringUtils.cal2str(projectDetail.getExpriationDate()));
				projectDetail.setProjectStatus(LookupAgent.getProjectStatus(rs.getInt("status_id")));
				if (projectDetail.getProjectStatus() == null) {
					projectDetail.setProjectStatus(new ProjectStatus());
				}
				logger.debug("Status is .. " + projectDetail.getProjectStatus().getDescription());
				projectDetail.setValuation(getValuation(projectId));
				logger.debug("Valuation is .. " + projectDetail.getValuation());
				projectDetail.setMicrofilm(rs.getString("microfilm"));
				logger.debug("Microfilm is .. " + projectDetail.getMicrofilm());
				projectDetail.setLabel(rs.getString("label"));
				logger.debug("Label is .. " + projectDetail.getLabel());

				projectDetail.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				if (projectDetail.getCreatedBy() != null) {
					logger.debug(" created user is set to  " + projectDetail.getCreatedBy().getUsername());
				}

				projectDetail.setCreated(rs.getDate("created_dt"));
				if (projectDetail.getCreated() != null) {
					logger.debug(" created date is set to  " + StringUtils.cal2str(projectDetail.getCreated()));
				}

				projectDetail.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				if (projectDetail.getUpdatedBy() != null) {
					logger.debug(" updated user is set to  " + projectDetail.getUpdatedBy().getUsername());
				}

				projectDetail.setUpdated(rs.getDate("update_dt"));
				if (projectDetail.getUpdated() != null) {
					logger.debug("updated date is set to  " + StringUtils.cal2str(projectDetail.getUpdated()));
				}

				// setting the project is finaled or not
				projectDetail.setOpenActivitiesCount(openActivitiesCount(projectId));
				logger.debug("Count  is set to  " + projectDetail.getOpenActivitiesCount());

				// setting the lso Object Of Project Object
				lsoId = rs.getInt("lso_Id");
				String lsoType = new AddressAgent().getLsoType(lsoId);

				// setting the Use Object Of Project Object
				sql = "select * from lso_use where lso_use_id=" + rs.getInt("lso_use_id");
				logger.debug(sql);
				RowSet rsUse = db.select(sql);
				use = new Use();
				if (rsUse.next()) {

					use.setId(rsUse.getInt("lso_use_id"));
					logger.debug("Use .. Id  is .. " + use.getId());
					use.setDescription(rsUse.getString("description"));
					logger.debug("Use .. Description is .. " + use.getDescription());
					use.setType(rsUse.getString("lso_type"));
					logger.debug("Use .. Lso_type  is .. " + use.getType());
				}

				rsUse.close();

				// setting all required Objects
				lso = new Lso();
				lso.setLsoId(lsoId);
				lso.setLsoType(lsoType);

				List subProjectList = new ArrayList();
				ProjectFinance projectFinance = new ProjectFinance();
				List projectHoldList = new CommonAgent().getHolds(projectId, "P", 3);
				logger.debug("obtained hold list of size " + projectHoldList.size());
				List projectConditionList = new ArrayList();
				List projectAttachmentList = new ArrayList();
				List projectCommentList = new ArrayList();
				List processTeamList = new ArrayList();
				List peopleList = new ArrayList();
				String projectType = "";
				project = new Project(projectId, lso, projectDetail, subProjectList, projectFinance, projectHoldList, projectConditionList, projectAttachmentList, projectCommentList, processTeamList, peopleList, projectType, use);
				logger.debug("Project Object Created .." + project.toString());
				rs.close();
			}
			// get project from the database for corresponding project id.
			logger.debug("returning project back to action  .." + project.toString());
			return project;
		} catch (Exception e) {

			logger.error("Exception in getproject method in ProjectAgent " + e.getMessage());
			throw e;
		}

	}

	/**
	 * saves a project to the database.
	 * 
	 * @param aProject
	 * @throws Exception
	 */
	public void setProject(Project aProject) throws Exception {

		project = aProject;
	}

	/**
	 * adds a project to the database.
	 * 
	 * @param project
	 * @return
	 * @throws Exception
	 */
	public int addProject(Project project) throws Exception {

		try {
			// Don't Allow duplicate projects for Housing
			if (project.getProjectDetail().getName().equalsIgnoreCase(Constants.PROJECT_NAME_HOUSING)) {
				projectId = getProjectId(project.getLso().getLsoId(), project.getProjectDetail().getName());
				logger.debug("projectId      ::::: " + projectId);
				if (projectId > 0)
					return projectId;
			}

			// add the project to the database
			Wrapper db = new Wrapper();
			String sql = "";
			projectId = db.getNextId("PROJECT_ID");
			int projNum = db.getNextId("PR_NUM");
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String projectNbr = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);
			db.beginTransaction();
			sql = "Insert into project(proj_id,lso_id,project_nbr,name,description,status_id,applied_dt,completion_dt,created_by,created_dt,updated_by,update_dt,lso_use_id,dept_id) values(" + projectId + "," + project.getLso().getLsoId() + "," + StringUtils.checkString(projectNbr) + "," + StringUtils.checkString(project.getProjectDetail().getName()) + "," + StringUtils.checkString(project.getProjectDetail().getDescription()) + "," + project.getProjectDetail().getProjectStatus().getStatusId() + ",to_date(" + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getStartDate())) + ",'MM/DD/YYYY'),to_date(" + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getCompletionDate())) + ",'MM/DD/YYYY')," + project.getProjectDetail().getCreatedBy().getUserId() + "," + "current_timestamp" + "," + project.getProjectDetail().getUpdatedBy().getUserId() + "," + "current_timestamp" + "," + project.getUse().getId() + ",8" + ")";
			logger.debug(sql);
			db.insert(sql);
			return projectId;
		} catch (Exception e) {

			projectId = -1;
			logger.error("Error in Adding Project.." + e.getMessage());
			throw e;

		}

	}

	/**
	 * saves project details to the database.
	 * 
	 * @param project
	 * @return
	 * @throws Exception
	 */
	public int saveProjectDetails(Project project) throws Exception {

		try {

			Wrapper db = new Wrapper();
			String sql = "";
			db.beginTransaction();

			sql = "update project set name=" + StringUtils.checkString(project.getProjectDetail().getName()) + "," + "description=" + StringUtils.checkString(project.getProjectDetail().getDescription()) + "," + "microfilm=" + StringUtils.checkString(project.getProjectDetail().getMicrofilm()) + ",label=" + StringUtils.checkString(project.getProjectDetail().getLabel()) + ",status_id=" + project.getProjectDetail().getProjectStatus().getStatusId() + "," + "applied_dt=to_date(" + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getStartDate())) + ",'mm/dd/yyyy')," + "completion_dt=to_date(" + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getCompletionDate())) + ",'mm/dd/yyyy')," + "expired_dt=to_date(" + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getExpriationDate())) + ",'mm/dd/yyyy')," + "update_dt=" + "current_timestamp" + ",updated_by=" + project.getProjectDetail().getUpdatedBy().getUserId() + " where proj_id=" + project.getProjectId();
			logger.info(sql);
			db.addBatch(sql);
			sql = "update activity set microFilm=" + StringUtils.checkString(project.getProjectDetail().getMicrofilm()) + " where sproj_id in (select sproj_id from sub_project where proj_id=" + project.getProjectId() + ")";
			logger.debug("update microfilm sql in activity table is " + sql);
			db.addBatch(sql);
			db.executeBatch();
			return project.getProjectId();
		} catch (Exception e) {

			logger.error("Exception thrown while trying to update ProjectDetails  :" + e.getMessage());
			throw e;

		}

	}

	/**
	 * deletes a project for a given project id.
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int deleteProject(int projectId) throws Exception {

		int result = -1;
		try {

			if (projectId != 0) {

				Wrapper db = new Wrapper();
				db.beginTransaction();
				String sql = "";
				sql = "delete from project where proj_id=" + projectId;
				logger.debug(" Delete Project Sql is " + sql);
				db.addBatch(sql);
				db.executeBatch();
				result = 1;
			}
			return result;
		} catch (Exception e) {

			result = -1;
			logger.error("Exception thrown while trying to delete Project :" + e.getMessage());
			throw e;

		}

	}

	/**
	 * gets the valuation for a project.
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public double getValuation(int projectId) throws Exception {

		double valuation = 0.00;
		try {

			Wrapper db = new Wrapper();
			String sql = "select sum(valuation) as totalvaluation from activity where sproj_id in(select sproj_id from sub_project where proj_id=" + projectId + ")";
			RowSet rs = db.select(sql);
			if (rs.next()) {
				valuation = rs.getDouble("totalvaluation");
			}
			return valuation;
		} catch (Exception e) {

			logger.error("Exception in getValuation method");
			throw e;
		}

	}

	/**
	 * searches for projects.
	 * 
	 * @param projectNumber
	 * @param projectName
	 * @param name
	 * @param peopleName
	 * @param peopleLic
	 * @param personType
	 * @param personInfoType
	 * @param streetNum
	 * @param streetID
	 * @param apn
	 * @return
	 * @throws Exception
	 */
	public List searchProjects(String projectNumber, String projectName, String name, String peopleName, String peopleLic, String personType, String personInfoType, String streetNum, String streetID, String apn) throws Exception {

		String query = "";
		String where = " WHERE ";
		String fullQuery = "SELECT PROJECT.*,LPS.description AS PROJ_STS_DESC,V.DL_ADDRESS FROM (PROJECT LEFT OUTER JOIN LKUP_PROJ_ST LPS ON PROJECT.STATUS_ID=LPS.STATUS_ID) LEFT OUTER JOIN V_ADDRESS_LIST V ON V.LSO_ID=PROJECT.LSO_ID ";

		List list = new ArrayList();

		query = ((projectNumber == null) || projectNumber.equals("")) ? query : (query + " UPPER(PROJECT_NBR) LIKE '" + (StringUtils.formatQueryForString(projectNumber).toUpperCase()) + "' AND");

		query = ((projectName == null) || projectName.equals("")) ? query : (query + " UPPER(NAME) LIKE '%" + StringUtils.formatQueryForString(projectName).toUpperCase() + "%' AND");

		// user name
		if ((name != null) && !name.equals("")) {

			query = query + " PROJ_ID IN (SELECT PSA_ID AS PROJ_ID FROM PROCESS_TEAM WHERE PSA_TYPE = 'P' AND USERID = " + name + ") AND ";
		}

		// people licenseNumber
		query = ((peopleLic == null) || peopleLic.equals("")) ? query : (query + " PROJ_ID IN (SELECT PROJ_ID FROM SUB_PROJECT WHERE SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(LIC_NO) LIKE '" + StringUtils.formatQueryForString(peopleLic).toUpperCase() + "'))))  AND");

		// people name
		query = ((peopleName == null) || peopleName.equals("")) ? query : (query + " PROJ_ID IN (SELECT PROJ_ID FROM SUB_PROJECT WHERE SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PSA_TYPE='A' AND PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(NAME) LIKE '%" + StringUtils.formatQueryForString(peopleName).toUpperCase() + "%'))) UNION SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PSA_TYPE='P' AND  PEOPLE_ID IN ( SELECT PEOPLE_ID FROM PEOPLE WHERE UPPER(NAME) LIKE '%" + StringUtils.formatQueryForString(peopleName).toUpperCase() + "%'))  AND");

		String formattedApn = StringUtils.formatQueryForString(apn);
		if (!formattedApn.equals("%")) {

			query = ((formattedApn == null) || (formattedApn.indexOf("%") < 0)) ? (query + " LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN = '" + formattedApn + "')  AND") : (query + " LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN LIKE '" + formattedApn + "')  AND");
		}

		query = ((streetID == null) || streetID.equals("-1")) ? query : (query + " LSO_ID IN (SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE RTRIM (CAST (STR_NO AS CHARACTER(40))) like '" + StringUtils.formatQueryForNumber(streetNum) + "' AND STREET_ID = " + streetID + ")  AND");

		// form the full query string
		if ((query != null) && !query.equals("")) {

			// query = query.substring(0, query.length() - 4); //to remove the trailing AND
			query += " PROJECT.NAME != 'Public Works' ";
			fullQuery = fullQuery + where + query + " ORDER BY APPLIED_DT desc";
		}

		logger.debug("SQL is - " + fullQuery);
		try {

			Wrapper db = new Wrapper();
			sun.jdbc.rowset.CachedRowSet rs = (sun.jdbc.rowset.CachedRowSet) db.select(fullQuery);

			rs.beforeFirst();
			while (rs.next()) {
				ProjectDetail projectDetail = new ProjectDetail();
				projectDetail.setName(rs.getString("NAME"));
				projectDetail.setDescription(rs.getString("description"));
				projectDetail.setProjectStatus(new ProjectStatus(rs.getInt("STATUS_ID"), rs.getString("PROJ_STS_DESC")));
				projectDetail.setProjectNumber(rs.getString("PROJECT_NBR"));
				projectDetail.setAddress(rs.getString("DL_ADDRESS"));

				Project project = new Project(rs.getInt("PROJ_ID"), projectDetail);
				project.setLso(new Lso(rs.getInt("LSO_ID")));

				list.add(project);
			}
			return list;
		} catch (Exception e) {
			logger.debug("Exception in method searchProjects - " + e.getMessage());
			throw e;
		}
	}

	/**
	 * searches for projects taking the sql.
	 * 
	 * @param sql
	 * @return
	 * @throws Exception
	 */
	public List searchProjects(String sql) throws Exception {

		List list = new ArrayList();
		try {

			Wrapper db = new Wrapper();
			logger.debug(sql);
			RowSet rs = db.select(sql);

			while (rs.next()) {

				ProjectDetail projectDetail = new ProjectDetail();
				projectDetail.setName(rs.getString("NAME"));
				projectDetail.setDescription(rs.getString("description"));
				projectDetail.setAppliedDate(rs.getDate("APPLIED_DT"));
				projectDetail.setProjectStatus(LookupAgent.getProjectStatus(rs.getInt("STATUS_ID")));

				projectDetail.setProjectNumber(rs.getString("PROJECT_NBR"));
				Project project = new Project(rs.getInt("PROJ_ID"), projectDetail);

				Lso lso = new Lso(rs.getInt("LSO_ID"));
				project.setLso(lso);

				list.add(project);
			}

			logger.debug("Project Count is : " + list.size());
			return list;
		} catch (Exception e) {

			logger.error("Exception in method searchProjects (advanced search)- " + e.getMessage());
			throw e;
		}

	}

	/**
	 * counts the open activities for a project id.
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int openActivitiesCount(int projectId) throws Exception {

		int count = 0;
		try {

			Wrapper db = new Wrapper();

			// sql for geetiing the count of activities which are not closed.
			String sql = "select count(*) as count from activity where sproj_id in (select sproj_id from sub_project where proj_id=" + projectId + ") and status not in ( 2,3,4,29,30,31)";
			RowSet rs = db.select(sql);
			if (rs.next()) {
				count = rs.getInt("count");
			}

			rs.close();
			return count;
		} catch (Exception e) {

			logger.error("Exception in method openActivitiesCount ");
			throw e;
		}

	}

	/**
	 * gets the land use id for a project id.
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public int getLandUseIdForProject(int projectId) throws Exception {

		int landUseId = -1;
		String sql = "select lso_use_id from land_usage where land_id in " + "(select land_id from v_lso_land where lso_id =" + "(select lso_id from project where proj_id=" + projectId + "))";
		logger.debug("Land Use Id SQL is " + sql);

		try {

			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				landUseId = rs.getInt("lso_use_id");
			}

			if (rs != null) {
				rs.close();
			}
			return landUseId;
		} catch (Exception e) {

			logger.error("Exception in method getLandUseId ");
			throw e;
		}

	}

	public int addNewProject(int lsoId, Project project) throws Exception {

		try {

			// add the project to the database
			Wrapper db = new Wrapper();
			String sql = "";
			projectId = db.getNextId("PROJECT_ID");
			int projNum = db.getNextId("PR_NUM");
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String projectNbr = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);
			db.beginTransaction();
			sql = "Insert into project(proj_id,lso_id,project_nbr,name,description,status_id,applied_dt,completion_dt,created_by,created_dt,updated_by,update_dt,lso_use_id,dept_id) values(" + projectId + "," + lsoId + "," + StringUtils.checkString(projectNbr) + "," + StringUtils.checkString(project.getProjectDetail().getName()) + "," + StringUtils.checkString(project.getProjectDetail().getDescription()) + "," + project.getProjectDetail().getProjectStatus().getStatusId() + ",convert(datetime," + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getStartDate())) + ",101),convert(datetime," + StringUtils.checkString(StringUtils.cal2str(project.getProjectDetail().getCompletionDate())) + ",101)," + project.getProjectDetail().getCreatedBy().getUserId() + "," + "current_timestamp" + "," + project.getProjectDetail().getUpdatedBy().getUserId() + "," + "current_timestamp" + "," + project.getUse().getId() + ",8" + ")";
			logger.debug(sql);
			db.insert(sql);
			return projectId;
		} catch (Exception e) {

			projectId = -1;
			logger.error("Error in Adding Project.." + e.getMessage());
			throw e;

		}

	}

	/**
	 * @param lsoId
	 *            , pName
	 * @return prjId
	 * @throws Exception
	 *             returns ProjectId if there are any Project in the given Lsoid having the same Project Name
	 */
	public int getProjectId(int lsoId, String pName) throws Exception {
		logger.info("Entered getProjectId(" + lsoId + "," + pName + ")");
		int prjId = -1;
		String sql = "select proj_id from project where lso_id = " + lsoId + " and upper(name)='" + pName.trim().toUpperCase() + "'";
		logger.debug(sql);
		try {
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				prjId = rs.getInt("proj_id");
			}
			return prjId;
		} catch (Exception e) {
			logger.error("Exception in method getProjectId ");
			throw e;
		}
	}

	/**
	 * creates a project in the database.
	 * 
	 * @param project
	 * @return
	 * @throws Exception
	 */
	public int createProject(String projectName, int departmentId, String lsoId, int userId) throws Exception {
		logger.info("createProject(" + projectName + "," + departmentId + ", " + lsoId + ", " + userId + ")");

		try {

			Wrapper db = new Wrapper();
			String sql = "";
			projectId = db.getNextId("PROJECT_ID");
			int projNum = db.getNextId("PR_NUM");
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String projectNbr = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(projNum), 7);

			sql = "Insert into project(proj_id,lso_id,project_nbr,name,dept_id,status_id,created_by,created_dt) values(" + projectId + "," + lsoId + "," + StringUtils.checkString(projectNbr) + "," + StringUtils.checkString(projectName) + "," + departmentId + "," + "1" + "," + userId + "," + "current_timestamp" + ")";
			logger.debug(sql);
			db.insert(sql);
			return projectId;
		} catch (Exception e) {

			projectId = -1;
			logger.error("Error in Adding Project.." + e.getMessage());
			throw e;

		}

	}

	/**
	 * Get PSA List
	 * 
	 * @param lso
	 * @param active
	 * @return List
	 */
	public List getPsaTreeList(long lso, boolean active, String contextRoot) throws Exception {
		logger.info("getPsaTreeList(" + lso + "," + active + "," + contextRoot + ")");

		String activeStr = active ? "Y" : "N";
		// the database wrapper
		Wrapper db = new Wrapper();

		if (lso <= 0) {
			return new ArrayList();
		}

		String sql = "";

		// the connections
		Connection connection = null;

		// the statements
		Statement stmtProject = null;
		PreparedStatement pstmtSubProject = null;
		PreparedStatement pstmtActivity = null;

		// the result sets
		ResultSet rsProject = null;
		ResultSet rsSubProject = null;
		ResultSet rsActivity = null;

		try {
			// get the connections
			connection = db.getConnectionForPreparedStatementOnly();

			// statement for the project
			stmtProject = connection.createStatement();

			// statement to get all activities
			if (active == true) {
				pstmtSubProject = connection.prepareStatement(GET_SUB_PROJECTS);
				pstmtActivity = connection.prepareStatement(GET_ACTIVITIES);
			} else {
				pstmtSubProject = connection.prepareStatement(GET_ALL_SUB_PROJECTS);
				pstmtActivity = connection.prepareStatement(GET_ALL_ACTIVITIES);
			}

			// used to return result
			psaTreeList = new ArrayList();

			long sequence = 1;
			long projectId = 0;
			long subProjectId = 0;
			long activityId = 0;
			long currentProject = 0;
			long currentSubProject = 0;
			String description = "";
			String alt = "";
			String label = "";

			// get the list of lsoIds descending from this lsoId and iterate over them
			// When the user clicks on land, it needs to display all the projects in structure and occupancy also.
			List list = getLsoIdList(lso);
			logger.debug("The obtained lso id list is " + list);

			String listValues = "";

			for (int i = 0; i < list.size(); i++) {
				if (i == 0) {
					listValues += list.get(i);
				} else {
					listValues += ("," + list.get(i));
				}
			}

			logger.debug("String list is " + listValues);

			// loop through the list to populate through the tree.

			// get the Project Ids and iterate over them
			if (active == true) {
				sql = "select p.proj_id,p.status_id,p.label,vlt.lso_type,p.project_nbr,p.name,p.description,lps.description as status from project p join v_lso_type vlt on p.lso_id = vlt.lso_id,lkup_proj_st lps where p.lso_id in (" + listValues + ") and p.status_id in (1,6,7) and p.status_id = lps.status_id order by p.applied_dt desc,p.proj_id desc";
				logger.debug("get only Active Projects, SubProjects and Activities " + sql);
			} else {
				// get all the Projects, SubProjects and Activities
				sql = "select p.proj_id,p.status_id,p.label,vlt.lso_type, p.project_nbr,p.name,p.description,lps.description as status  from project p join v_lso_type vlt on p.lso_id=vlt.lso_id,lkup_proj_st lps  where p.lso_id in (" + listValues + ") and p.status_id = lps.status_id order by applied_dt desc,p.proj_id desc";
				logger.debug("get all the Projects, SubProjects and Activities " + sql);
			}

			logger.debug(sql);

			rsProject = stmtProject.executeQuery(sql);

			while (rsProject.next()) {
				projectId = rsProject.getLong(1);
				logger.debug("This is project id - " + projectId);

				// find the project is closed or not
				String projStatus = "";

				if ((rsProject.getInt(2) == 1) || (rsProject.getInt(2) == 6) || (rsProject.getInt(2) == 7)) {
					projStatus = "O";
				} else {
					projStatus = "C";
				}

				// get Project Description
				description = "";
				description = rsProject.getString("NAME");

				if (description == null) {
					description = "";
				}

				if (description.trim().length() == 0) {
					description = "Project -- no name ";
				}

				// previous trace
				description = StringUtils.replaceDoubleQuot(description);

				if (description.length() > 75) {
					description = description.substring(0, 75);
				}

				label = (rsProject.getString("label")) != null ? rsProject.getString("label") : "";
				logger.debug("Label is " + label);

				String name = StringUtils.replaceDoubleQuot(rsProject.getString("NAME")) + " " + label;
				logger.debug("name+label " + name);

				alt = "";
				alt = ((rsProject.getString("PROJECT_NBR") == null) || rsProject.getString("PROJECT_NBR").equals("")) ? (alt + "Project#:&#10;&#13;") : (alt + "Project#:" + rsProject.getString("PROJECT_NBR").trim() + "&#10;&#13;");

				alt = ((rsProject.getString("description") == null) || rsProject.getString("description").equals("")) ? (alt + "description:&#10;&#13;") : (alt + "description:" + StringUtils.replaceDoubleQuot(rsProject.getString("description").trim()) + "&#10;&#13;");
				alt = ((rsProject.getString("STATUS") == null) || rsProject.getString("STATUS").equals("")) ? (alt + "Status:") : (alt + "Status:" + rsProject.getString("STATUS").trim());

				// create a node for this Project and add it to the tree
				psaTree = new PsaTree((new Long(sequence)).toString(), "0", name, contextRoot + "/viewProject.do?projectId=" + (new Long(projectId)).toString(), alt, rsProject.getString("LSO_TYPE"), projStatus, projectId, "P");

				psaTreeList.add(psaTree);

				// update the counters
				currentProject = sequence++;

				// get the SubProjects and iterate over them
				pstmtSubProject.setLong(1, projectId);
				logger.debug(GET_SUB_PROJECTS);
				rsSubProject = pstmtSubProject.executeQuery();

				while (rsSubProject.next()) {
					String tmp = "";

					// get the SubProject Id
					subProjectId = rsSubProject.getLong(1);
					logger.debug("Sub project id is " + subProjectId);

					description = StringUtils.replaceDoubleQuot(rsSubProject.getString("PROJECT_TYPE"));

					if (description != null && description.startsWith("BL") || description.startsWith("BT")) {
						description = StringUtils.replaceDoubleQuotWithSplChar(rsSubProject.getString("PROJECT_TYPE")).toUpperCase();
						logger.debug("subproject Name is " + description);
					} else {
						description = StringUtils.replaceDoubleQuotWithSplChar(rsSubProject.getString("PROJECT_TYPE"));
					}

					if (description == null) {
						description = "";
					}

					if (description.trim().length() == 0) {
						description = "Sub Project -- no type ";
					}

					if (description.length() > 75) {
						description = description.substring(0, 75);
					}

					String subProjectLabel = (rsSubProject.getString("label")) != null ? rsSubProject.getString("label") : "";
					logger.debug("subProjectLabel = " + subProjectLabel);

					// update description
					description = description + " " + subProjectLabel;
					logger.debug("updated description with label= " + description);

					String subProjStatus = "";

					if ((rsSubProject.getInt("status") == 1) || (rsSubProject.getInt("status") == 6) || (rsSubProject.getInt("status") == 7) || (rsSubProject.getInt("status") == 4)) {
						subProjStatus = "O";
					} else {
						subProjStatus = "C";
					}

					alt = "";
					alt = ((rsSubProject.getString("SPROJ_NBR") == null) || rsSubProject.getString("SPROJ_NBR").equals("")) ? (alt + "SProject#:&#10;&#13;") : (alt + "SProject#:" + rsSubProject.getString("SPROJ_NBR").trim() + "&#10;&#13;");
					tmp = rsSubProject.getString("LST_DESC");
					alt += ("Type:" + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					tmp = rsSubProject.getString("SPROJ_STYPE");
					alt += ("Sub Type:" + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					tmp = rsSubProject.getString("project_type");
					alt += ("Name: " + ((tmp == null) ? "" : StringUtils.replaceDoubleQuot(tmp.trim())) + "&#10;&#13;");
					tmp = rsSubProject.getString("status_desc");
					alt += ("Status: " + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					alt = (rsSubProject.getString("description") == null) ? (alt + "description:") : (alt + "description:" + StringUtils.replaceDoubleQuot(rsSubProject.getString("description").trim()));

					// set the tree
					psaTree = new PsaTree((new Long(sequence)).toString(), (new Long(currentProject)).toString(), description, contextRoot + "/viewSubProject.do?subProjectId=" + (new Long(subProjectId)).toString() + "&active=" + activeStr, alt, "", subProjStatus, subProjectId, "Q");

					psaTreeList.add(psaTree);

					// update the counters
					currentSubProject = sequence++;

					// get the Activities and iterate over them
					pstmtActivity.setLong(1, subProjectId);
					logger.debug("subProjectId" + subProjectId);

					if (active == true) {
						logger.debug(GET_ACTIVITIES);
					} else {
						logger.debug(GET_ALL_ACTIVITIES);
					}

					rsActivity = pstmtActivity.executeQuery();

					while (rsActivity.next()) {

						// get the Activity Id
						activityId = rsActivity.getLong(1);
						logger.debug("This is activity id " + activityId);

						// get Activity Description
						description = StringUtils.replaceDoubleQuot(rsActivity.getString("ACT_TYPE"));

						if (description == null) {
							description = "";
						}

						if (description.trim().length() == 0) {
							description = "Activity -- no type ";
						}

						if (description.length() > 75) {
							description = description.substring(0, 75);
						}

						String activityLabel = (rsActivity.getString("label")) != null ? rsActivity.getString("label") : "";
						logger.debug("Activity label is " + activityLabel);

						// updated description
						description = description + " " + activityLabel;
						logger.debug("description + label = " + description);

						String actStatus = "";

						if (rsActivity.getString("psa_active").equalsIgnoreCase("N")) {
							actStatus = "C";
						} else {
							actStatus = "O";
						}

						int intActivityId = new Long(activityId).intValue();

						// Done on 29-Jun-2009 for getting 1 city wide records in less time.
						List activitySubTypes = (List) new ActivityAgent().getSubTypes(intActivityId);

						String activitySubTypesString = StringUtils.listToString(activitySubTypes);

						alt = "";
						alt = (rsActivity.getString("ACT_NBR") == null) ? (alt + "Activity#:&#10;&#13;") : (alt + "Activity#:" + rsActivity.getString("ACT_NBR").trim() + "&#10;&#13;");
						alt = (rsActivity.getString("description") == null) ? (alt + "Description:&#10;&#13;") : (alt + "description:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(rsActivity.getString("description").trim(), "\r\n", " "))) + "&#10;&#13;");
						alt = (rsActivity.getString("act_type") == null) ? (alt + "Type:&#10;&#13;") : (alt + "Type:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(rsActivity.getString("act_type").trim(), "\r\n", " "))) + "&#10;&#13;");
						alt = (activitySubTypesString == null) ? (alt + "Sub Type:&#10;&#13;") : (alt + "Sub Type:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(activitySubTypesString.trim(), "\r\n", " "))) + "&#10;&#13;");
						alt = (rsActivity.getString("STATUS") == null) ? (alt + "Status:") : (alt + "Status:" + rsActivity.getString("STATUS").trim());

						// set the tree
						psaTree = new PsaTree((new Long(sequence++)).toString(), (new Long(currentSubProject)).toString(), description, contextRoot + "/viewActivity.do?activityId=" + (new Long(activityId)).toString(), alt, "", actStatus, activityId, "A");

						psaTreeList.add(psaTree);
					}

					// end while Activity
					rsActivity.close();
				}

				// end while SubProject
				rsSubProject.close();
			}

			// end while Project
			rsProject.close();

			logger.debug("List - " + psaTreeList.size());
			logger.info("Getting hold status");
			getHoldInfo(psaTreeList);
			logger.info("After hold status");

			return psaTreeList;
		} catch (Exception e) {
			logger.error("getPsaTreeList : Error while creating the List of tree elements: " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rsActivity != null) {
					rsActivity.close();
				}

				if (rsSubProject != null) {
					rsSubProject.close();
				}

				if (rsProject != null) {
					rsProject.close();
				}

				if (pstmtActivity != null) {
					pstmtActivity.close();
				}

				if (pstmtSubProject != null) {
					pstmtSubProject.close();
				}

				if (stmtProject != null) {
					stmtProject.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Error while closeing database handles .." + e.getMessage());
			}
		}
	}

	/**
	 * Function to find all descending lsoIds from any lsoId
	 * 
	 */
	public List getLsoIdList(long lsoId) throws Exception {
		logger.info("getLsoIdList(" + lsoId + ")");

		// the database handles.
		Wrapper wrapper = new Wrapper();

		// the connection
		Connection connection = null;

		// the prepared statements
		PreparedStatement getLsoTypeStatement = null;
		PreparedStatement getStructureStatement = null;
		PreparedStatement getOccupancyStatement = null;

		// the result sets
		ResultSet rsStructure = null;
		ResultSet rsOccupancyL = null;
		ResultSet rsOccupancyS = null;
		ResultSet rsType = null;
		List list = new ArrayList();

		if (lsoId <= 0) {
			return list;
		}

		try {
			connection = wrapper.getConnectionForPreparedStatementOnly();

			char type = ' ';
			String query = "";

			getLsoTypeStatement = connection.prepareStatement(GET_LSO_TYPE);
			logger.info(GET_LSO_TYPE);
			getLsoTypeStatement.setLong(1, lsoId);
			logger.debug("parameter 1 = " + lsoId);
			rsType = getLsoTypeStatement.executeQuery();

			if (rsType.next()) {
				type = ((rsType.getString("LSO_TYPE")).trim()).charAt(0);
				logger.debug("obtained LSO type as " + type);
			}

			switch (type) {
			case 'L':

				// first look for NON-LOCATION LSO Ids around this given LSO ID

				// lsoId is the land id needs loops for structure ids and occupancy ids add the Land id to the list
				list.add(new Long(lsoId));

				long str = 0;
				long occL = 0;

				getStructureStatement = connection.prepareStatement(GET_STRUCTURE);
				logger.info(GET_STRUCTURE);
				logger.debug("parameter 1 = " + lsoId);
				getStructureStatement.setLong(1, lsoId);
				rsStructure = getStructureStatement.executeQuery();

				getOccupancyStatement = connection.prepareStatement(GET_OCCUPANCY);

				// iterate over structure ids for this Land
				while (rsStructure.next()) {
					// set parameters to find occupancies for this structure
					str = rsStructure.getLong(1);

					// first add the structure id
					list.add(new Long(str));
					getOccupancyStatement.setLong(1, str);
					logger.info(GET_OCCUPANCY);
					rsOccupancyL = getOccupancyStatement.executeQuery();

					// iterate over occupancies for this structure
					while (rsOccupancyL.next()) {
						// add the occupancy id to the list
						occL = rsOccupancyL.getLong(1);
						list.add(new Long(occL));
					}
				}

				break;

			case 'S':

				// lsoId in this case is the structure id needs only one loop for occupancies
				getOccupancyStatement = connection.prepareStatement(GET_OCCUPANCY);
				getOccupancyStatement.setLong(1, lsoId);

				long occS = 0;

				// first add the structure id
				list.add(new Long(lsoId));

				// execute query to find occupancies for this structure
				logger.debug("GET_OCCUPANCY:" + GET_OCCUPANCY);
				rsOccupancyS = getOccupancyStatement.executeQuery();

				// iterate over occpancies for this structure
				while (rsOccupancyS.next()) {
					// add the occupancy id to the list
					occS = rsOccupancyS.getLong(1);
					list.add(new Long(occS));
				}

				break;

			case 'O':

				// occupancy id does not have any child
				list.add(new Long(lsoId));

				break;
			}

			logger.debug("Returning list of lso ids of size " + list.size());

			return list;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		} finally {
			try {
				if (rsStructure != null) {
					rsStructure.close();
				}

				if (rsOccupancyL != null) {
					rsOccupancyL.close();
				}

				if (rsOccupancyS != null) {
					rsOccupancyS.close();
				}

				if (rsType != null) {
					rsType.close();
				}

				if (getStructureStatement != null) {
					getStructureStatement.close();
				}

				if (getOccupancyStatement != null) {
					getOccupancyStatement.close();
				}

				if (getOccupancyStatement != null) {
					getOccupancyStatement.close();
				}

				if (getLsoTypeStatement != null) {
					getLsoTypeStatement.close();
				}

				if (connection != null) {
					connection.close();
				}

			} catch (Exception e) {
				logger.error("Exception occred while closing database handles " + e.getMessage());
			}
		}
	}

	public List getOnlyOnePsaTreeList(long lso, boolean active, String psaNodeId, String contextRoot) throws Exception {
		logger.info("getOnlyOnePsaTreeList(" + lso + "," + active + "," + psaNodeId + "," + contextRoot + ")");

		// the database wrapper
		Wrapper db = new Wrapper();

		if (lso <= 0) {
			return new ArrayList();
		}

		String sql = "";

		// the connections
		Connection connection = null;

		// the statements
		Statement stmtProject = null;
		PreparedStatement pstmtSubProject = null;
		PreparedStatement pstmtActivity = null;

		// the result sets
		ResultSet rsProject = null;
		ResultSet rsSubProject = null;
		ResultSet rsActivity = null;

		try {
			// get the connections
			connection = db.getConnectionForPreparedStatementOnly();

			// statement for the project
			stmtProject = connection.createStatement();

			String projId = new ActivityAgent().getProjectId(psaNodeId);
			String subProjId = getSubProjectId(psaNodeId);
			// statement to get all sub projects

			String GET_ACTIVITIES1 = "select a.act_id,a.act_nbr,replace(replace(a.description,chr(10),''),chr(13),'') as description,a.label,a.status as actStatus,las.description as status,lat.description as act_type,las.psa_active as psa_active from activity a,lkup_act_st las,lkup_act_type lat where a.act_id=" + psaNodeId + " and a.sproj_id =" + subProjId + " and a.status = las.status_id and a.act_type = lat.type and las.psa_active = 'Y' order by a.created desc,a.act_id desc";
			String GET_ALL_ACTIVITIES1 = "select a.act_id,a.act_nbr,replace(replace(a.description,chr(10),''),chr(13),'') as description,a.label,a.status as actStatus,las.description as status,lat.description as act_type from activity a,lkup_act_st las,lkup_act_type lat where a.act_id=" + psaNodeId + " and a.sproj_id =" + subProjId + " and a.status = las.status_id and a.act_type = lat.type order by a.created desc,a.act_id desc";
			String GET_SUB_PROJECTS1 = "select sp.sproj_id,sp.sproj_nbr,sp.description,sp.label,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where sproj_id=" + subProjId + " and proj_id =" + projId + " and lsps.psa_active = 'Y' order by created desc,sproj_id desc";
			String GET_ALL_SUB_PROJECTS1 = "select sp.sproj_id,sp.sproj_nbr,sp.description,sp.label,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where sproj_id=" + subProjId + " and proj_id =" + projId + "  order by created desc,sproj_id desc";

			// statement to get all activities
			if (active == true) {
				pstmtSubProject = connection.prepareStatement(GET_SUB_PROJECTS1);
				pstmtActivity = connection.prepareStatement(GET_ACTIVITIES1);
			} else {
				pstmtSubProject = connection.prepareStatement(GET_ALL_SUB_PROJECTS1);
				pstmtActivity = connection.prepareStatement(GET_ALL_ACTIVITIES1);
			}

			// used to return result
			psaTreeList = new ArrayList();

			long sequence = 1;
			long projectId = 0;
			long subProjectId = 0;
			long activityId = 0;
			long currentProject = 0;
			long currentSubProject = 0;
			String description = "";
			String alt = "";

			// get the list of lsoIds descending from this lsoId and iterate over them
			List list = getLsoIdList(lso);
			logger.debug("The obtained lso id list is " + list);

			String listValues = "";

			for (int i = 0; i < list.size(); i++) {
				if (i == 0) {
					listValues += list.get(i);
				} else {
					listValues += ("," + list.get(i));
				}
			}

			logger.debug("String list is " + listValues);

			if (active == true) {
				// get only Active Projects, SubProjects and Activities
				sql = "select p.proj_id,p.status_id,p.label,vlt.lso_type,p.project_nbr,p.name,p.description,lps.description as status from project p join v_lso_type vlt on p.lso_id = vlt.lso_id,lkup_proj_st lps where p.proj_id=" + projId + " and p.lso_id in (" + listValues + ") and p.status_id in (1,6,7) and p.status_id = lps.status_id  order by p.applied_dt desc,p.proj_id desc";
				logger.debug("get only Active Projects, SubProjects and Activities " + sql);
			} else {
				// get all the Projects, SubProjects and Activities
				sql = "select p.proj_id,p.status_id,p.label,vlt.lso_type, p.project_nbr,p.name,p.description,lps.description as status  from project p join v_lso_type vlt on p.lso_id=vlt.lso_id,lkup_proj_st lps  where p.proj_id=" + projId + " and p.lso_id in (" + listValues + ") and p.status_id = lps.status_id order by applied_dt desc,p.proj_id desc";
				logger.debug("get all the Projects, SubProjects and Activities " + sql);
			}

			logger.debug(sql);

			rsProject = stmtProject.executeQuery(sql);

			while (rsProject.next()) {
				projectId = rsProject.getLong(1);
				logger.debug("This is project id - " + projectId);

				// find the project is closed or not
				String projStatus = "";

				if ((rsProject.getInt(2) == 1) || (rsProject.getInt(2) == 6) || (rsProject.getInt(2) == 7)) {
					projStatus = "O";
				} else {
					projStatus = "C";
				}

				// get Project Description
				description = "";
				description = rsProject.getString("NAME");

				if (description == null) {
					description = "";
				}

				if (description.trim().length() == 0) {
					description = "Project -- no name ";
				}

				description = StringUtils.replaceDoubleQuot(description);

				if (description.length() > 75) {
					description = description.substring(0, 75);
				}

				String label = (rsProject.getString("label")) != null ? rsProject.getString("label") : "";
				logger.debug("Label is " + label);

				String name = StringUtils.replaceDoubleQuot(rsProject.getString("NAME")) + " " + label;

				logger.debug("name+label " + name);

				alt = "";
				alt = ((rsProject.getString("PROJECT_NBR") == null) || rsProject.getString("PROJECT_NBR").equals("")) ? (alt + "Project#:&#10;&#13;") : (alt + "Project#:" + rsProject.getString("PROJECT_NBR").trim() + "&#10;&#13;");

				alt = ((rsProject.getString("description") == null) || rsProject.getString("description").equals("")) ? (alt + "description:&#10;&#13;") : (alt + "description:" + StringUtils.replaceDoubleQuot(rsProject.getString("description").trim()) + "&#10;&#13;");
				alt = ((rsProject.getString("STATUS") == null) || rsProject.getString("STATUS").equals("")) ? (alt + "Status:") : (alt + "Status:" + rsProject.getString("STATUS").trim());

				// create a node for this Project and add it to the tree
				psaTree = new PsaTree((new Long(sequence)).toString(), "0", name, contextRoot + "/viewProject.do?projectId=" + (new Long(projectId)).toString(), alt, rsProject.getString("LSO_TYPE"), projStatus, projectId, "P");

				// check and update the hold status.
				psaTreeList.add(psaTree);

				// update the counters
				currentProject = sequence++;

				// get the SubProjects and iterate over them
				logger.debug(GET_SUB_PROJECTS1);
				rsSubProject = pstmtSubProject.executeQuery();

				while (rsSubProject.next()) {
					String tmp = "";

					// get the SubProject Id
					subProjectId = rsSubProject.getLong(1);
					logger.debug("Sub project id is " + subProjectId);
					description = StringUtils.replaceDoubleQuot(rsSubProject.getString("PROJECT_TYPE"));
					logger.debug("Description before is " + description);
					if (description == null) {
						description = "";
					}

					if (description.trim().length() == 0) {
						description = "Sub Project -- no type ";
					}

					if (description.length() > 75) {
						description = description.substring(0, 75);
					}

					logger.debug("Description after is " + description);
					String activityLabel = (rsSubProject.getString("label")) != null ? rsSubProject.getString("label") : "";
					logger.debug("Activity label is " + activityLabel);

					description = description + " " + activityLabel;
					logger.debug("description + label = " + description);

					String subProjectLabel = rsSubProject.getString("label");
					logger.debug("subProjectLabel " + subProjectLabel);

					description = description + " " + subProjectLabel;
					logger.debug("updated description+label = " + description);

					String subProjStatus = "";

					if ((rsSubProject.getInt("status") == 1) || (rsSubProject.getInt("status") == 6) || (rsSubProject.getInt("status") == 7) || (rsSubProject.getInt("status") == 4)) {
						subProjStatus = "O";
					} else {
						subProjStatus = "C";
					}

					logger.debug("Sub project status " + subProjStatus);
					alt = "";
					alt = ((rsSubProject.getString("SPROJ_NBR") == null) || rsSubProject.getString("SPROJ_NBR").equals("")) ? (alt + "SProject#:&#10;&#13;") : (alt + "SProject#:" + rsSubProject.getString("SPROJ_NBR").trim() + "&#10;&#13;");
					tmp = rsSubProject.getString("LST_DESC");
					alt += ("Type:" + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					tmp = rsSubProject.getString("SPROJ_STYPE");
					alt += ("Sub Type:" + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					tmp = rsSubProject.getString("project_type");
					alt += ("Name: " + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					tmp = rsSubProject.getString("status_desc");
					alt += ("Status: " + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
					alt = (rsSubProject.getString("description") == null) ? (alt + "description:") : (alt + "description:" + StringUtils.replaceDoubleQuot(rsSubProject.getString("description").trim()));

					logger.debug("alt = " + alt);
					// set the tree
					psaTree = new PsaTree((new Long(sequence)).toString(), (new Long(currentProject)).toString(), description, contextRoot + "/viewSubProject.do?subProjectId=" + (new Long(subProjectId)).toString(), alt, "", subProjStatus, subProjectId, "Q");

					// check and update the hold status.
					psaTreeList.add(psaTree);

					// update the counters
					currentSubProject = sequence++;

					if (active == true) {
						logger.debug(GET_ACTIVITIES1);
					} else {
						logger.debug(GET_ALL_ACTIVITIES1);
					}

					rsActivity = pstmtActivity.executeQuery();

					while (rsActivity.next()) {

						// get the Activity Id
						activityId = rsActivity.getLong(1);
						logger.debug("This is activity id " + activityId);

						// get Activity Description
						description = StringUtils.replaceDoubleQuot(rsActivity.getString("ACT_TYPE"));

						if (description == null) {
							description = "";
						}

						if (description.trim().length() == 0) {
							description = "Activity -- no type ";
						}

						if (description.length() > 75) {
							description = description.substring(0, 75);
						}

						String actStatus = "";

						if ((rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_CE_INACTIVE) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_CE_SUSPENSE) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_PC_EXPIRED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_PC_CANCELLED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_ADMIN_COMPLETED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_CANCELLED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_EXPIRED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_PLANNING_DENIED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_PLANNING_DENIED_WITHOUT_PREJUDICE) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_PLANNING_EXPIRED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_LC_PERMIT_EXPIRED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_LC_PERMIT_CANCELLED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_LC_PERMIT_DENIED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BL_DISAPPROVED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BL_OUT_OF_BUSINESS) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BL_DENIED) || (rsActivity.getInt("actStatus") == Constants.ACTIVITY_STATUS_BUILDING_CERTIFICATE_OF_OCCUPANCY)) {
							actStatus = "C";
						} else {
							actStatus = "O";
						}

						int intActivityId = new Long(activityId).intValue();
						List activitySubTypes = (List) LookupAgent.getSubTypes(new ActivityAgent().getMultipleActivitySubTypes(intActivityId));
						logger.debug("new ActivityAgent().getMultipleActivitySubTypes(intActivityId) " + new ActivityAgent().getMultipleActivitySubTypes(intActivityId));
						String activitySubTypesString = StringUtils.listToString(activitySubTypes);

						alt = "";
						alt = (rsActivity.getString("ACT_NBR") == null) ? (alt + "Activity#:&#10;&#13;") : (alt + "Activity#:" + rsActivity.getString("ACT_NBR").trim() + "&#10;&#13;");
						alt = (rsActivity.getString("description") == null) ? (alt + "Description:&#10;&#13;") : (alt + "description:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(rsActivity.getString("description").trim(), "\r\n", " "))) + "&#10;&#13;");
						alt = (rsActivity.getString("act_type") == null) ? (alt + "Type:&#10;&#13;") : (alt + "Type:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(rsActivity.getString("act_type").trim(), "\r\n", " "))) + "&#10;&#13;");
						alt = (activitySubTypesString == null) ? (alt + "Sub Type:&#10;&#13;") : (alt + "Sub Type:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(activitySubTypesString.trim(), "\r\n", " "))) + "&#10;&#13;");
						alt = (rsActivity.getString("STATUS") == null) ? (alt + "Status:") : (alt + "Status:" + rsActivity.getString("STATUS").trim());

						// set the tree
						psaTree = new PsaTree((new Long(sequence++)).toString(), (new Long(currentSubProject)).toString(), description, contextRoot + "/viewActivity.do?activityId=" + (new Long(activityId)).toString(), alt, "", actStatus, activityId, "A");

						psaTreeList.add(psaTree);
					}

					// end while Activity
					rsActivity.close();
				}

				// end while SubProject
				rsSubProject.close();
			}

			// end while Project
			rsProject.close();

			// } //end for loop
			logger.debug("List - " + psaTreeList.size());
			logger.info("Getting hold status");
			getHoldInfo(psaTreeList);
			logger.info("After hold status");

			return psaTreeList;
		} catch (Exception e) {
			logger.error("getOnlyOnePsaTreeList : Error while creating the List of tree elements:  " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rsActivity != null) {
					rsActivity.close();
				}

				if (rsSubProject != null) {
					rsSubProject.close();
				}

				if (rsProject != null) {
					rsProject.close();
				}

				if (pstmtActivity != null) {
					pstmtActivity.close();
				}

				if (pstmtSubProject != null) {
					pstmtSubProject.close();
				}

				if (stmtProject != null) {
					stmtProject.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Error while closeing database handles .." + e.getMessage());
			}
		}
	}

	/**
	 * check for out of town address
	 * 
	 * @param lsoId
	 * @return outOfTown
	 * @author Manjuprasad
	 */

	public String checkForCityWide(long lsoId) {
		logger.info("checkForCityWide(" + lsoId + ")");
		String outOfTown = "";

		try {
			Wrapper db = new Wrapper();
			// For bt
			String sql = "select * from v_address_list where lso_id=" + lsoId;
			logger.info(sql);
			RowSet rs = db.select(sql);
			String streetName = "";
			int streetNumber = 0;

			if (rs.next()) {
				streetName = rs.getString("STR_NAME");
				streetNumber = rs.getInt("STR_NO");
			}

			if (streetName.equalsIgnoreCase("CITYWIDE") && (streetNumber == 1 || streetNumber == 2 || streetNumber == 3 || streetNumber == 4 || streetNumber == 5)) {
				outOfTown = "yes";
			} else {
				outOfTown = "no";
			}

			logger.debug("checkForCityWide() method   is ..." + outOfTown);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to get actId  :" + e.getMessage());
		}

		return outOfTown;
	}

	/**
	 * check for out of town address
	 * 
	 * @param activityId
	 * @return activityId
	 * @author Hemavathi
	 */

	public String checkForOutOfTownAddress(String activityId) {
		logger.info("checkForOutOfTownAddress(" + activityId + ")");
		String actId = "";

		try {
			Wrapper db = new Wrapper();
			String sql = "select bt.act_id from bt_activity bt ,v_activity_address vaa where bt.act_id=vaa.act_id and vaa.STREET_ID=" + Constants.CITYWIDE_STREET_ID + " and bt.act_id=" + activityId;
			logger.info(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				actId = StringUtils.i2s(rs.getInt("act_id"));
			} else {
				String sql1 = "select bl.act_id from bl_activity bl ,v_activity_address vaa where bl.act_id=vaa.act_id  and vaa.STREET_ID=" + Constants.CITYWIDE_STREET_ID + " and bl.act_id=" + activityId;
				RowSet rs1 = db.select(sql1);
				if (rs1.next()) {
					actId = StringUtils.i2s(rs1.getInt("act_id"));
				} else {
					// For Other than BL / BT Activities
					String otherActivitysql = "select a.act_id from activity a ,v_activity_address vaa where a.act_id=vaa.act_id  and vaa.STREET_ID=" + Constants.CITYWIDE_STREET_ID + " and a.act_id=" + activityId;
					RowSet otherActivityrs = db.select(otherActivitysql);
					if (otherActivityrs.next()) {
						actId = StringUtils.i2s(otherActivityrs.getInt("act_id"));
					}
				}

			}

			logger.debug("checkForOutOfTownAddress() method   is ..." + actId);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to get actId  :" + e.getMessage());
		}

		return actId;
	}

	public String getNonLocationalForAddressRange(String listValues) {
		logger.info("getNonLocationalForAddressRange(" + listValues + ")");

		int streetId = 0;
		int streetNo = 0;
		int actId = 0;
		String actIds = "";
		listValues = listValues.replaceAll(",,", "0");
		try {
			Wrapper db = new Wrapper();
			String sql = "select * from LSO_ADDRESS where lso_id in (" + listValues + ") and lso_type='L'";
			logger.debug(sql);
			RowSet rs = db.select(sql);
			while (rs.next()) {
				streetNo = Integer.parseInt(StringUtils.nullReplaceWithZero(rs.getString("STR_NO")));
				streetId = Integer.parseInt(StringUtils.nullReplaceWithZero(rs.getString("STREET_ID")));

			}
			int fromStreetNo = 0;
			int toStreetNo = 0;

			String sql1 = "select * from act_address_range where STREET_ID1=" + streetId + " or STREET_ID2=" + streetId + " or STREET_ID3=" + streetId + " or STREET_ID4=" + streetId + " or STREET_ID5=" + streetId;
			logger.debug(sql1);
			RowSet rs1 = db.select(sql1);
			Set<Integer> s = new HashSet<Integer>();
			while (rs1.next()) {

				if (streetId == rs1.getInt("STREET_ID1")) {
					fromStreetNo = rs1.getInt("FROM_STR_NO1");
					toStreetNo = rs1.getInt("TO_STR_NO1");
					if ((streetNo >= fromStreetNo) && (streetNo <= toStreetNo)) {
						actId = (rs1.getInt("ACT_ID"));
						s.add(actId);
					}
				}

				if (streetId == rs1.getInt("STREET_ID2")) {
					fromStreetNo = rs1.getInt("FROM_STR_NO2");
					toStreetNo = rs1.getInt("TO_STR_NO2");
					if ((streetNo >= fromStreetNo) && (streetNo <= toStreetNo)) {
						actId = (rs1.getInt("ACT_ID"));
						s.add(actId);
					}
				}

				if (streetId == rs1.getInt("STREET_ID3")) {
					fromStreetNo = rs1.getInt("FROM_STR_NO3");
					toStreetNo = rs1.getInt("TO_STR_NO3");
					if ((streetNo >= fromStreetNo) && (streetNo <= toStreetNo)) {
						actId = (rs1.getInt("ACT_ID"));
						s.add(actId);
					}
				}

				if (streetId == rs1.getInt("STREET_ID4")) {
					fromStreetNo = rs1.getInt("FROM_STR_NO4");
					toStreetNo = rs1.getInt("TO_STR_NO4");
					if ((streetNo >= fromStreetNo) && (streetNo <= toStreetNo)) {
						actId = (rs1.getInt("ACT_ID"));
						s.add(actId);
					}
				}

				if (streetId == rs1.getInt("STREET_ID5")) {
					fromStreetNo = rs1.getInt("FROM_STR_NO5");
					toStreetNo = rs1.getInt("TO_STR_NO5");
					if ((streetNo >= fromStreetNo) && (streetNo <= toStreetNo)) {
						actId = (rs1.getInt("ACT_ID"));
						s.add(actId);
					}
				}

			}

			logger.debug("created set is of size " + s.size());
			Iterator<Integer> itr = s.iterator();
			while (itr.hasNext())
				actIds += itr.next() + ",";

			if (!actIds.equalsIgnoreCase("")) {
				actIds = actIds.substring(0, actIds.length() - 1);
				logger.debug("Activity Ids are " + actIds);
			}

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception thrown while trying to get Non LOCATIONAL   :" + e.getMessage());
		}
		return actIds;

	}

	public List getNonLocationProjects(List psaTreeList, long lso, boolean active, String contextRoot) throws Exception {
		logger.info("getNonLocationProjects(" + lso + "," + active + "," + contextRoot + ")");

		Wrapper db = new Wrapper();

		String sql = "";    

		// the connections
		Connection connection = null;

		// the statements
		Statement stmtProject = null;
		Statement stmtSubProject = null;
		Statement stmtActivity = null;

		// the result sets
		ResultSet rsProject = null;
		ResultSet rsSubProject = null;
		ResultSet rsActivity = null;

		try {

			List list = getLsoIdList(lso);
			logger.debug("The obtained lso id list is " + list);

			String listValues = "";

			for (int i = 0; i < list.size(); i++) {
				if (i == 0) {
					listValues += list.get(i);
				} else {
					listValues += ("," + list.get(i));
				}
			}
			listValues = listValues.replaceAll(",,", "0");
			logger.debug("String list is " + listValues);
			String actIds = "";
			actIds = getNonLocationalForAddressRange(listValues);

			// Non Locational stuff
			if (!actIds.equalsIgnoreCase("")) {

				// get the connections
				connection = db.getConnectionForPreparedStatementOnly();

				// statement for the project
				stmtProject = connection.createStatement();

				long sequence = 100;
				long projectId = 0;
				long subProjectId = 0;
				long activityId = 0;
				long currentProject = 100;
				long currentSubProject = 100;
				String description = "";
				String alt = "";

				String projectNo = "";
				String sql2 = "select distinct(sp.proj_id),a.sproj_id from activity a left outer join sub_project sp on a.sproj_id=sp.sproj_id where a.act_id in (" + actIds + ")";
				logger.debug(sql2);
				RowSet rs2 = db.select(sql2);
				while (rs2.next()) {
					projectNo += rs2.getInt("PROJ_ID") + ",";
				}
				if (!projectNo.equalsIgnoreCase("")) {
					projectNo = projectNo.substring(0, projectNo.length() - 1);
					logger.debug("Project No: " + projectNo);

					if (active == true) {
						sql = "select p.proj_id,p.status_id,p.lso_id,vlt.lso_type,p.project_nbr,p.name,p.description,lps.description as status from project p join v_lso_type vlt on p.lso_id = vlt.lso_id,lkup_proj_st lps where p.proj_id in (" + projectNo + ") and p.status_id in (1,6,7) and p.status_id = lps.status_id order by p.applied_dt desc,p.proj_id desc";
						logger.debug("Active Projects:" + sql);
					} else {
						sql = "select p.proj_id,p.status_id,vlt.lso_type, p.project_nbr,p.name,p.description,lps.description as status  from project p join v_lso_type vlt on p.lso_id=vlt.lso_id,lkup_proj_st lps  where p.proj_id in (" + projectNo + ") and p.status_id = lps.status_id order by applied_dt desc,p.proj_id desc";
						logger.debug("All Projects:" + sql);
					}

					logger.debug(sql);

					rsProject = stmtProject.executeQuery(sql);

					while (rsProject.next()) {
						projectId = rsProject.getLong(1);
						logger.debug("This is project id - " + projectId);

						// find the project is closed or not
						String projStatus = "";

						if ((rsProject.getInt(2) == 1) || (rsProject.getInt(2) == 6) || (rsProject.getInt(2) == 7)) {
							projStatus = "O";
						} else {
							projStatus = "C";
						}

						// get Project Description
						description = "";
						description = rsProject.getString("NAME");

						if (description == null) {
							description = "";
						}

						if (description.trim().length() == 0) {
							description = "Project -- no name ";
						}

						// previous trace
						description = StringUtils.replaceDoubleQuot(description);

						if (description.length() > 75) {
							description = description.substring(0, 75);
						}

						alt = "";
						alt = ((rsProject.getString("PROJECT_NBR") == null) || rsProject.getString("PROJECT_NBR").equals("")) ? (alt + "Project#:&#10;&#13;") : (alt + "Project#:" + rsProject.getString("PROJECT_NBR").trim() + "&#10;&#13;");

						alt = ((rsProject.getString("description") == null) || rsProject.getString("description").equals("")) ? (alt + "description:&#10;&#13;") : (alt + "description:" + StringUtils.replaceDoubleQuot(rsProject.getString("description").trim()) + "&#10;&#13;");
						alt = ((rsProject.getString("STATUS") == null) || rsProject.getString("STATUS").equals("")) ? (alt + "Status:") : (alt + "Status:" + rsProject.getString("STATUS").trim());

						// create a node for this Project and add it to the tree
						psaTree = new PsaTree((new Long(sequence)).toString(), "0", "NonLocational-" + StringUtils.replaceDoubleQuot(rsProject.getString("NAME")), contextRoot + "/viewProject.do?projectId=" + (new Long(projectId)).toString(), alt, rsProject.getString("LSO_TYPE"), projStatus, projectId, "P");

						psaTreeList.add(psaTree);

						// update the counters
						currentProject = sequence++;

						// String sprjNo = "";
						String sql3 = "select distinct a.sproj_id from activity a where a.act_id in (" + actIds + ")";
						logger.debug(sql3);

						stmtSubProject = connection.createStatement();
						String subProjectSql = "";

						if (active == true) {
							subProjectSql = "select sp.sproj_id,sp.sproj_nbr,sp.description,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where proj_id =" + projectId + " and sp.sproj_id in (" + sql3 + ") and lsps.psa_active = 'Y' order by created desc,sproj_id desc";
						} else {
							subProjectSql = "select sp.sproj_id,sp.sproj_nbr,sp.description,lp.description as project_type,lst.description as lst_desc,lss.description as sproj_stype,stype_id,lsps.description as status_desc,sstype_id,sp.status as status from ((sub_project sp left outer join lkup_ptype lp on sp.sproj_type = lp.ptype_id) left outer join lkup_sproj_type lst on sp.stype_id = lst.sproj_type_id ) left outer join lkup_sproj_stype lss on sp.sstype_id = lss. sproj_stype_id left outer join lkup_sproj_st lsps on lsps.status_id = sp.status where proj_id =" + projectId + " and sp.sproj_id in (" + sql3 + ") order by created desc,sproj_id desc";
						}
						logger.info("subProjectSql:" + subProjectSql);

						rsSubProject = stmtSubProject.executeQuery(subProjectSql);

						while (rsSubProject.next()) {
							String tmp = "";

							// get the SubProject Id
							subProjectId = rsSubProject.getLong(1);
							logger.debug("Sub project id is " + subProjectId);

							description = StringUtils.replaceDoubleQuot(rsSubProject.getString("PROJECT_TYPE"));

							if (description != null && description.startsWith("BL") || description.startsWith("BT")) {
								description = StringUtils.replaceDoubleQuotWithSplChar(rsSubProject.getString("PROJECT_TYPE")).toUpperCase();
								logger.debug("subproject Name is " + description);
							} else {
								description = StringUtils.replaceDoubleQuotWithSplChar(rsSubProject.getString("PROJECT_TYPE"));
							}

							if (description == null) {
								description = "";
							}

							if (description.trim().length() == 0) {
								description = "Sub Project -- no type ";
							}

							if (description.length() > 75) {
								description = description.substring(0, 75);
							}

							String subProjStatus = "";

							if ((rsSubProject.getInt("status") == 1) || (rsSubProject.getInt("status") == 6) || (rsSubProject.getInt("status") == 7) || (rsSubProject.getInt("status") == 4)) {
								subProjStatus = "O";
							} else {
								subProjStatus = "C";
							}

							alt = "";
							alt = ((rsSubProject.getString("SPROJ_NBR") == null) || rsSubProject.getString("SPROJ_NBR").equals("")) ? (alt + "SProject#:&#10;&#13;") : (alt + "SProject#:" + rsSubProject.getString("SPROJ_NBR").trim() + "&#10;&#13;");
							tmp = rsSubProject.getString("LST_DESC");
							alt += ("Type:" + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
							tmp = rsSubProject.getString("SPROJ_STYPE");
							alt += ("Sub Type:" + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
							tmp = rsSubProject.getString("project_type");
							alt += ("Name: " + ((tmp == null) ? "" : StringUtils.replaceDoubleQuot(tmp.trim())) + "&#10;&#13;");
							tmp = rsSubProject.getString("status_desc");
							alt += ("Status: " + ((tmp == null) ? "" : tmp.trim()) + "&#10;&#13;");
							alt = (rsSubProject.getString("description") == null) ? (alt + "description:") : (alt + "description:" + StringUtils.replaceDoubleQuot(rsSubProject.getString("description").trim()));

							// set the tree
							psaTree = new PsaTree((new Long(sequence)).toString(), (new Long(currentProject)).toString(), description, contextRoot + "/viewSubProject.do?subProjectId=" + (new Long(subProjectId)).toString(), alt, "", subProjStatus, subProjectId, "Q");

							psaTreeList.add(psaTree);

							// update the counters
							currentSubProject = sequence++;

							stmtActivity = connection.createStatement();

							if (active == true) {
								sql = "select a.act_id,a.act_nbr,a.description,a.status as actStatus,las.description as status,lat.description as act_type,las.psa_active as psa_active from activity a,lkup_act_st las,lkup_act_type lat where a.sproj_id=" + subProjectId + " and a.act_id IN (" + actIds + ") and a.status = las.status_id and a.act_type = lat.type and las.active = 'Y' order by a.applied_date desc,a.act_id desc";
								logger.debug("GET_ACTIVITIES_NONLOCATIONAL:" + sql);
							} else {
								sql = "select a.act_id,a.act_nbr,a.description,a.status as actStatus,las.description as status,lat.description as act_type,las.psa_active as psa_active from activity a,lkup_act_st las,lkup_act_type lat where a.sproj_id=" + subProjectId + " and a.act_id IN (" + actIds + ") and a.status = las.status_id and a.act_type = lat.type order by a.applied_date desc,a.act_id desc";
								logger.debug("GET_ALL_ACTIVITIES_NONLOCATIONAL:" + sql);
							}

							// rsActivity = pstmtActivity.executeQuery();
							rsActivity = stmtActivity.executeQuery(sql);

							while (rsActivity.next()) {

								// get the Activity Id
								activityId = rsActivity.getLong(1);
								logger.debug("This is activity id " + activityId);

								// get Activity Description
								description = StringUtils.replaceDoubleQuot(rsActivity.getString("ACT_TYPE"));

								if (description == null) {
									description = "";
								}

								if (description.trim().length() == 0) {
									description = "Activity -- no type ";
								}

								if (description.length() > 75) {
									description = description.substring(0, 75);
								}

								String actStatus = "";

								if (rsActivity.getString("psa_active").equalsIgnoreCase("N")) {
									actStatus = "C";
								} else {
									actStatus = "O";
								}

								int intActivityId = new Long(activityId).intValue();

								List activitySubTypes = (List) new ActivityAgent().getSubTypes(intActivityId);

								String activitySubTypesString = StringUtils.listToString(activitySubTypes);

								alt = "";
								alt = (rsActivity.getString("ACT_NBR") == null) ? (alt + "Activity#:&#10;&#13;") : (alt + "Activity#:" + rsActivity.getString("ACT_NBR").trim() + "&#10;&#13;");
								alt = (rsActivity.getString("description") == null) ? (alt + "Description:&#10;&#13;") : (alt + "description:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(rsActivity.getString("description").trim(), "\r\n", " "))) + "&#10;&#13;");
								alt = (rsActivity.getString("act_type") == null) ? (alt + "Type:&#10;&#13;") : (alt + "Type:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(rsActivity.getString("act_type").trim(), "\r\n", " "))) + "&#10;&#13;");
								alt = (activitySubTypesString == null) ? (alt + "Sub Type:&#10;&#13;") : (alt + "Sub Type:" + StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(StringUtils.replaceString(activitySubTypesString.trim(), "\r\n", " "))) + "&#10;&#13;");
								alt = (rsActivity.getString("STATUS") == null) ? (alt + "Status:") : (alt + "Status:" + rsActivity.getString("STATUS").trim());

								// set the tree
								psaTree = new PsaTree((new Long(sequence++)).toString(), (new Long(currentSubProject)).toString(), description, contextRoot + "/viewActivity.do?activityId=" + (new Long(activityId)).toString(), alt, "", actStatus, activityId, "A");

								psaTreeList.add(psaTree);
							}

							// end while Activity
							rsActivity.close();
						}

						// end while SubProject
						rsSubProject.close();
					}

					// end while Project
					rsProject.close();

					logger.debug("List - " + psaTreeList.size());
					logger.info("Getting hold status");
					getHoldInfo(psaTreeList);
					logger.info("After hold status");
				}
			}
			return psaTreeList;
		} catch (Exception e) {
			logger.error("Error while creating the List of tree for NonLocationProjects elements: " + e.getMessage());
			throw e;
		} finally {
			try {
				if (rsActivity != null) {
					rsActivity.close();
				}

				if (rsSubProject != null) {
					rsSubProject.close();
				}

				if (rsProject != null) {
					rsProject.close();
				}

				if (stmtSubProject != null) {
					stmtSubProject.close();
				}

				if (stmtProject != null) {
					stmtProject.close();
				}

				if (stmtActivity != null) {
					stmtActivity.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception e) {
				logger.error("Error while getNonLocationProjects closeing database handles .." + e.getMessage());
			}
		}
	}

	/**
	 * Access method for the subProject property.
	 * 
	 * @return the current value of the subProject property
	 */
	public SubProject getSubProject(int subProjId, String active) throws Exception {
		try {
			Wrapper db = new Wrapper();

			// getting subproject Details;
			String sql = "select sp.sproj_id,sp.label,val.address,sp.sproj_nbr,sp.proj_id,sp.caselog_id,sp.sproj_type,sp.stype_id,sp.sstype_id,sp.status,sp.description,sp.created,sp.created_by,sp.updated,sp.updated_by,lpt.description as lpt_desc,lspt.description as lspt_desc,lspt.dept_code,lspt.sproj_type as lspt_sproj_type,lspst.type,lspst.description as lspst_desc from ((((sub_project sp left outer join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id) left outer join lkup_sproj_type lspt on sp.stype_id = lspt.sproj_type_id) left outer join lkup_sproj_stype lspst on sp.sstype_id = lspst.sproj_stype_id) left outer join v_psa_list vpl on vpl.sproj_id=sp.sproj_id) left outer join v_address_list val on vpl.lso_id=val.lso_id where sp.sproj_id=" + subProjId;
			logger.debug(" subProjectAgent -- getSubProject() -- select Sql  is " + sql);

			RowSet rs = db.select(sql);
			subProject = new SubProject();
			subProject.setSubProjectId(subProjId);
			logger.debug(" subProjectAgent -- getSubProject() -- subProject -- subprojectId is set to  " + subProject.getSubProjectId());
			subProjectDetail = new SubProjectDetail();

			if (rs.next()) {
				subProject.setProjectId(rs.getInt("proj_id"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProject -- projectId is set to  " + subProject.getProjectId());

				// getting the project Type Object
				projectType = new ProjectType();
				projectType.setProjectTypeId(rs.getInt("sproj_type"));
				logger.debug(" subProjectAgent -- getSubProject() -- projectType -- ProjectTypeId is set to  " + projectType.getProjectTypeId());

				if (rs.getString("lpt_desc") != null && (rs.getString("lpt_desc").startsWith("BL") || rs.getString("lpt_desc").startsWith("BT"))) {
					projectType.setDescription(rs.getString("lpt_desc").toUpperCase());
				} else {
					projectType.setDescription(rs.getString("lpt_desc"));
				}
				logger.debug(" subProjectAgent -- getSubProject() -- projectType -- Description is set to  " + projectType.getDescription());

				// getting the subproject type Object
				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(rs.getInt("stype_id"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- SubProjectTypeId is set to  " + subProjectType.getSubProjectTypeId());
				subProjectType.setDescription(rs.getString("lspt_desc"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- Description is set to  " + subProjectType.getDescription());

				subProjectType.setDepartmentId(rs.getString("dept_code"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- DepartmentCode is set to  " + subProjectType.getDepartmentId());
				subProjectType.setSubProjectType(rs.getString("lspt_sproj_type"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- SubProjectType is set to  " + subProjectType.getSubProjectType());

				// getting sub project sub type Object
				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("sstype_id"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectSubType -- SubProjectSubTypeId is set to  " + subProjectSubType.getSubProjectSubTypeId());
				subProjectSubType.setType(rs.getString("type"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectSubType -- SubProjectType is set to  " + subProjectSubType.getType());
				subProjectSubType.setDescription(rs.getString("lspst_desc"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectSubType -- SubProjectType is set to  " + subProjectSubType.getDescription());
				subProjectDetail.setSubProjectNumber(rs.getString("sproj_nbr"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- SubProjectType is set to  " + subProjectDetail.getSubProjectNumber());

				subProjectDetail.setCaseLogId(rs.getInt("caselog_id"));
				logger.debug("subProjectAgent - caselogId: " + subProjectDetail.getCaseLogId());
				String sql1 = "select description from lkup_caselog where caselog_id = " + subProjectDetail.getCaseLogId() + "";
				logger.debug(sql1);
				RowSet rs1 = db.select(sql1);
				while (rs1.next()) {
					subProjectDetail.setCaseLogDesc(rs1.getString("description"));
				}

				subProjectDetail.setAddress(rs.getString("address"));
				logger.debug("subProjectDetail -- Address is set to  " + subProjectDetail.getAddress());

				subProjectDetail.setProjectType(projectType);
				logger.debug("subProjectDetail -- SubProjectType is set to  " + subProjectDetail.getProjectType().toString());
				subProjectDetail.setSubProjectType(subProjectType);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail-- SubProjectType is set to  " + subProjectDetail.getSubProjectType().toString());
				subProjectDetail.setSubProjectSubType(subProjectSubType);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- SubProjectSubType is set to  " + subProjectDetail.getSubProjectSubType().toString());

				subProjectDetail.setSubProjectStatus(LookupAgent.getSubProjectStatus(rs.getInt("status")));

				if (subProjectDetail.getSubProjectStatus() == null) {
					subProjectDetail.setSubProjectStatus(new SubProjectStatus());
				}

				logger.debug("getSubProject -- Sub Project Details ..Status is .. " + subProjectDetail.getSubProjectStatus().getDescription());

				subProjectDetail.setDescription(rs.getString("description"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Description is set to  " + subProjectDetail.getDescription());

				subProjectDetail.setLabel(rs.getString("label"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Label is set to  " + subProjectDetail.getLabel());

				subProjectDetail.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));

				if (subProjectDetail.getCreatedBy() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- created user is set to  " + subProjectDetail.getCreatedBy().getUsername());
				}

				subProjectDetail.setCreated(rs.getDate("created"));

				if (subProjectDetail.getCreated() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- created date is set to  " + subProjectDetail.getCreated().getTime());
				}

				subProjectDetail.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));

				if (subProjectDetail.getUpdatedBy() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- updated user is set to  " + subProjectDetail.getUpdatedBy().getUsername());
				}

				subProjectDetail.setUpdated(rs.getDate("updated"));

				if (subProjectDetail.getUpdated() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- updateddate is set to  " + subProjectDetail.getUpdated().getTime());
				}

				List countByHoldTypes = new CommonAgent().getCountByHoldTypes(subProject.getSubProjectId(), "Q");

				if ((countByHoldTypes == null) || (countByHoldTypes.size() == 0)) {
					countByHoldTypes = new ArrayList();
				}

				Iterator iter = countByHoldTypes.iterator();

				while (iter.hasNext()) {
					CountByHoldType countByHoldType = (CountByHoldType) iter.next();
					logger.debug(" countByHoldType  " + subProjectDetail.getHardHoldCount());

					if (countByHoldType.getHoldType().equals("H")) {
						subProjectDetail.setHardHoldCount(countByHoldType.getCount());
					}

					logger.debug("subProjectAgent -- getSubProject() -- subProjectDetail -- Hard hold count is set to  " + subProjectDetail.getHardHoldCount());

					if (countByHoldType.getHoldType().equals("S")) {
						subProjectDetail.setSoftHoldCount(countByHoldType.getCount());
					}

					logger.debug("subProjectAgent -- getSubProject() -- subProjectDetail -- Soft hold count is set to  " + subProjectDetail.getSoftHoldCount());

					if (countByHoldType.getHoldType().equals("W")) {
						subProjectDetail.setWarnHoldCount(countByHoldType.getCount());
					}

					logger.debug("subProjectAgent -- getSubProject() -- subProjectDetail -- Warn hold count is set to  " + subProjectDetail.getWarnHoldCount());
				}

				int attachmentCount = new CommonAgent().getAttachmentCount(subProject.getSubProjectId(), "Q");
				subProjectDetail.setAttachmentCount(attachmentCount);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Attachment Count  is set to  " + subProjectDetail.getAttachmentCount());

				int conditionCount = new CommonAgent().getConditionCount(subProject.getSubProjectId(), "Q");
				subProjectDetail.setConditionCount(conditionCount);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Condition Count  is set to  " + subProjectDetail.getConditionCount());
			}

			rs.close();
			subProject.setSubProjectDetail(subProjectDetail);
			logger.debug(" subProjectAgent -- getSubProject() --subProject  -- SubProjectDetail -- is set to  " + subProject.getSubProjectDetail().toString());

			// Get all Activities for the Sub-Project
			activities = new ArrayList();
			activities = new ActivityAgent().getActivitiesLite(subProjId, active);
			subProject.setActivity(activities);
			logger.debug("Activity List is set to  " + subProject.getActivity().size());

			// Get the Financial History for the Sub-Project
			subProjectFinance = new SubProjectFinance();
			subProject.setSubProjectFinance(subProjectFinance);
			logger.debug(" subProjectAgent -- getSubProject() --subProject  -- SubProjectFinance -- is set to  " + subProject.getSubProjectFinance().toString());

			// Get the Holds count for the Sub-Project
			subProject.setPsaHold(new CommonAgent().getHolds(subProjId, "Q", 3));
			logger.debug(" subProjectAgent -- getSubProject() --subProject  -- List of Holds Set with size " + subProject.getPsaHold().size());

			if (LookupAgent.getProjectNameId("Q", subProjId) == Constants.PROJECT_NAME_PLANNING_ID) { // Planning SubProject
				subProject.setPlanningDetails(ActivityAgent.getPlanningDetails("Q", subProjId));
			}
			return subProject;
		} catch (Exception e) {
			logger.error("Exception in -- ProjectAgent -- getSubProject() -- " + e.getMessage(), e);
			throw e;
		}

	}

	/**
	 * Access method for the subProject property.
	 * 
	 * @return the current value of the subProject property
	 */
	public SubProject getSubProjectLite(int subProjId) {
		try {
			Wrapper db = new Wrapper();

			// getting subproject Details;
			String sql = "select sp.sproj_id,val.address,sp.sproj_nbr,sp.proj_id,sp.sproj_type,sp.stype_id,sp.sstype_id,sp.status,sp.description,sp.created,sp.created_by,sp.updated,sp.updated_by," + "lpt.description as lpt_desc,d.dept_code," + "lspt.description as lspt_desc,lspt.dept_code as dept_id,lspt.sproj_type as lspt_sproj_type," + "lspst.type,lspst.description as lspst_desc" + " from (((((sub_project sp left outer join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id)" + " left outer join lkup_sproj_type lspt on sp.stype_id = lspt.sproj_type_id) " + " left outer join lkup_sproj_stype lspst on sp.sstype_id = lspst.sproj_stype_id)" + " left outer join v_psa_list vpl on vpl.sproj_id=sp.sproj_id)" + " left outer join v_address_list val on vpl.lso_id=val.lso_id)" + " left outer join department d on cast(lspt.dept_code as int) = d.dept_id" + " where sp.sproj_id=" + subProjId;
			logger.debug(" subProjectAgent -- getSubProject() -- select Sql  is " + sql);

			RowSet rs = db.select(sql);
			subProject = new SubProject();
			subProject.setSubProjectId(subProjId);
			subProjectDetail = new SubProjectDetail();

			if (rs.next()) {
				subProject.setProjectId(rs.getInt("proj_id"));
				subProjectDetail.setSubProjectNumber(rs.getString("sproj_nbr"));
				subProjectDetail.setAddress(rs.getString("address"));
				subProjectDetail.setDescription(rs.getString("description"));

				projectType = new ProjectType();
				projectType.setProjectTypeId(rs.getInt("sproj_type"));
				projectType.setDescription(rs.getString("lpt_desc"));
				subProjectDetail.setProjectType(projectType);

				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(rs.getInt("stype_id"));
				subProjectType.setDescription(rs.getString("lspt_desc"));
				subProjectType.setDepartmentId(rs.getString("dept_id"));
				subProjectType.setDepartmentCode(rs.getString("dept_code"));
				subProjectType.setSubProjectType(rs.getString("lspt_sproj_type"));
				subProjectDetail.setSubProjectType(subProjectType);

				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("sstype_id"));
				subProjectSubType.setType(rs.getString("type"));
				subProjectSubType.setDescription(rs.getString("lspst_desc"));
				subProjectDetail.setSubProjectSubType(subProjectSubType);

				subProjectDetail.setSubProjectStatus(LookupAgent.getSubProjectStatus(rs.getInt("status")));

				if (subProjectDetail.getSubProjectStatus() == null) {
					subProjectDetail.setSubProjectStatus(new SubProjectStatus());
				}

				subProjectDetail.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				subProjectDetail.setCreated(rs.getDate("created"));
				subProjectDetail.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				subProjectDetail.setUpdated(rs.getDate("updated"));
			}

			rs.close();
			subProject.setSubProjectDetail(subProjectDetail);
		} catch (Exception e) {
			logger.debug("Exception in -- ProjectAgent -- getSubProject() -- " + e.getMessage());
		}

		return subProject;
	}

	public int addSubProject(SubProject subProject) {
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			subProjectId = db.getNextId("SUB_PROJECT_ID");

			int sprojNum = db.getNextId("SP_NUM");
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);

			sql = "insert into sub_project(sproj_id,proj_id,sproj_type,stype_id,sstype_id,status,description,created_by,created,updated_by,updated,sproj_nbr) " + " values(" + subProjectId + "," + subProject.getProjectId() + "," + subProject.getSubProjectDetail().getProjectType().getProjectTypeId() + "," + subProject.getSubProjectDetail().getSubProjectType().getSubProjectTypeId() + "," + subProject.getSubProjectDetail().getSubProjectSubType().getSubProjectSubTypeId() + "," + subProject.getSubProjectDetail().getStatus() + "," + StringUtils.checkString(subProject.getSubProjectDetail().getDescription()) + "," + subProject.getSubProjectDetail().getUpdatedBy().getUserId() + "," + "current_date" + "," + subProject.getSubProjectDetail().getUpdatedBy().getUserId() + "," + "current_date" + "," + StringUtils.checkString(subProjectNumber) + ")";

			db.insert(sql);
			logger.debug(" subProjectAgent -- addSubProject() --insert Sql is " + sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update ProjectDetails  :" + e.getMessage());
		}

		return subProjectId;
	}

	public int deleteSubProject(int subProjectId) {
		int projectId = -1;

		try {
			if (subProjectId != 0) {
				Wrapper db = new Wrapper();
				db.beginTransaction();

				String sql = "select * from sub_project where sproj_id=" + subProjectId;
				logger.debug(" deleteSubProject() --Select  Sql is " + sql);

				RowSet rs = db.select(sql);

				if (rs.next()) {
					projectId = rs.getInt("proj_id");
				}

				logger.debug(" projectId for the SubProject is " + projectId);
				rs.close();
				rs = null;
				String deleteCalSql = "delete from sp_calendar where sproj_id=" + subProjectId;
				logger.debug(" deleteCalSql() --delete Sql is " + deleteCalSql);
				db.addBatch(deleteCalSql);

				String testSql = "delete from sub_project where sproj_id=" + subProjectId;
				logger.debug(" deleteSubProject() --delete Sql is " + testSql);
				db.addBatch(testSql);
				db.executeBatch();
			}
		} catch (Exception e) {
			logger.error("Exception thrown while trying to delete SubProject :" + e.getMessage());
		}

		return projectId;
	}

	public int getProjectId(int subProjectId) {
		int projectId = -1;

		try {
			Wrapper db = new Wrapper();
			String sql = "select proj_id from sub_project where sproj_id=" + subProjectId;
			logger.debug(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				projectId = rs.getInt("proj_id");
			}

			logger.debug("getProjectId() method projectId  is ..." + projectId);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update getProjectId  :" + e.getMessage());
		}

		return projectId;
	}

	public String getSubProjectId(String activityId) {
		logger.info("getSubProjectId(" + activityId + ")");
		String subProjectId = "";

		try {
			Wrapper db = new Wrapper();
			String sql = "select sproj_id from activity where act_id=" + activityId;
			logger.info(sql);
			RowSet rs = db.select(sql);

			if (rs.next()) {
				subProjectId = rs.getString("sproj_id");
			}

			logger.debug("getSubProjectId() method SubProjectId  is ..." + subProjectId);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to get subProjectId  :" + e.getMessage());
		}

		return subProjectId;
	}

	public int saveSubProjectDetail(SubProject subProject) {
		try {
			Wrapper db = new Wrapper();
			String sql = "update sub_project set  stype_id=" + subProject.getSubProjectDetail().getSubProjectType().getSubProjectTypeId() + "," + "sstype_id=" + subProject.getSubProjectDetail().getSubProjectSubType().getSubProjectSubTypeId() + "," + "description=" + StringUtils.checkString(subProject.getSubProjectDetail().getDescription()) + ",label=" + StringUtils.checkString(subProject.getSubProjectDetail().getLabel()) + ",caselog_id=" + subProject.getSubProjectDetail().getCaseLogId() + "," + "status=" + subProject.getSubProjectDetail().getSubProjectStatus().getStatusId() + "," + "updated_by=" + subProject.getSubProjectDetail().getUpdatedBy().getUserId() + "," + "updated=current_date" + "   where sproj_id =" + subProject.getSubProjectId();
			logger.info(sql);
			db.update(sql);
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update SubProjectDetails  :" + e.getMessage());
		}

		return subProject.getSubProjectId();
	}

	public List getSubProjects(int projectId, String active) {
		try {
			Wrapper db = new Wrapper();
			String sql = "select sproj_id from sub_project where proj_id=" + projectId;
			if (active.equalsIgnoreCase("Y")) {
				sql = sql + " and  status !=2 ";
			}
			RowSet rs = db.select(sql);
			subProjects = new ArrayList();

			while (rs.next()) {
				subProject = this.getSubProject(rs.getInt("sproj_id"), active);
				subProjects.add(subProject);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception in -- subProjectAgent --  getSubProjectsList  -- " + e.getMessage());
		}

		return subProjects;
	}

	public LsoAddress getPrimaryAddress(String subProjectId) {
		LsoAddress address = new LsoAddress();
		String sql = "";
		sql = "select la.addr_id as id,rtrim(cast(la.str_no as char(8))) || coalesce(' ' || la.str_mod,'') || " + " coalesce(' ' || sl.str_name,'') || coalesce(' ' || sl.str_type,'') || coalesce(' ' || sl.suf_dir,'') ||" + " coalesce(', ' || sl.pre_dir,'') || coalesce(' ' || la.unit,'') as address from lso_address la,street_list sl" + " where la.street_id = sl.street_id and la.primary='Y' and la.lso_id = (select distinct lso_id from v_psa_list where sproj_id=" + subProjectId + ")";
		logger.debug(sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				address.setAddressId(rs.getInt("id"));
				address.setDescription(rs.getString("address"));
			}
		} catch (SQLException e) {
			logger.debug("Sql Exception in getting activity address list " + e.getMessage());
		} catch (Exception e) {
			logger.debug("General Exception in getting activity address list " + e.getMessage());
		}

		return address;
	}

	/**
	 * Search for activities matching search criteria
	 * 
	 * @return a List of ActivityDetail objects
	 */
	public List searchActivities(String activityNumber, String activityType, String projectNumber, String projectName, String streetNum, String streetID, String apn, String projectTeam, String personType, String personInfoType, String projectTypeId, String subProjectTypeId, String subProjectSubTypeId) {
		String query = "";
		String where = " WHERE ";
		String fullQuery = "SELECT * FROM ACTIVITY ";

		List list = new ArrayList();

		try {
			query = ((activityNumber == null) || activityNumber.equals("")) ? query : (query + " ACT_ID = " + (Integer.parseInt(activityNumber)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
		}

		query = ((activityType == null) || activityType.equals("")) ? query : (query + " ACT_TYPE LIKE '" + StringUtils.formatQueryForString(activityType) + "' AND");

		try {
			query = ((projectNumber == null) || projectNumber.equals("")) ? query : (query + " SPROJ_ID IN (SELECT SPROJ_ID FROM SUB_PROJECT WHERE PROJ_ID = " + (Integer.parseInt(projectNumber)) + ") AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
		}

		query = ((projectName == null) || projectName.equals("")) ? query : (query + " SPROJ_ID IN (SELECT SPROJ_ID FROM SUB_PROJECT WHERE PROJ_ID IN (SELECT PROJ_ID FROM PROJECT WHERE NAME LIKE '" + StringUtils.formatQueryForString(projectName) + "')) AND");

		query = ((apn == null) || apn.equals("")) ? query : (query + " SPROJ_ID IN ( SELECT SPROJ_ID FROM SUB_PROJECT WHERE PROJ_ID IN (SELECT PROJ_ID FROM PROJECT WHERE LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN LIKE '" + StringUtils.formatQueryForString(apn) + "')))  AND");

		// projectTeam holds the user_id
		if ((personInfoType != null) && personInfoType.equals("name") && (personType != null) && personType.equals("I")) {
			try {
				query = ((projectTeam == null) || projectTeam.equals("")) ? query : (query + " SPROJ_ID IN ( SELECT SPROJ_ID FROM SUB_PROJECT WHERE PROJ_ID IN (SELECT PSA_ID AS PROJ_ID FROM PSA_USER WHERE USER_ID = " + Integer.parseInt(projectTeam) + " AND PSA_TYPE = 'A'))  AND");
			} catch (NumberFormatException num) {
				logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
			}
		}

		// projectTeam holds the user_id
		if ((personInfoType != null) && personInfoType.equals("name") && (personType != null) && personType.equals("E")) {
			try {
				query = ((projectTeam == null) || projectTeam.equals("")) ? query : (query + " SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID = " + Integer.parseInt(projectTeam) + "))  AND");
			} catch (NumberFormatException num) {
				logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
			}
		}

		// projectTeam has the license number
		if ((personInfoType != null) && personInfoType.equals("licenceNumber")) {
			query = ((projectTeam == null) || projectTeam.equals("")) ? query : (query + " SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID = ( SELECT PEOPLE_ID FROM PEOPLE WHERE LIC_NO ='" + projectTeam + "'))  AND");
		}

		query = ((streetID == null) || streetID.equals("") || (streetNum == null) || streetNum.equals("")) ? query : (query + " SPROJ_ID IN ( SELECT SPROJ_ID FROM SUB_PROJECT WHERE PROJ_ID IN (SELECT PROJ_ID FROM PROJECT WHERE LSO_ID IN (SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE RTRIM (CAST (STR_NO AS CHARACTER(40))) like '" + StringUtils.formatQueryForNumber(streetNum) + "' AND STREET_ID = " + streetID + ")))  AND");

		try {
			query = ((projectType == null) || projectType.equals("")) ? query : (query + " SPROJ_ID IN ( SELECT SPROJ_ID FROM SUB_PROJECT WHERE SPROJ_TYPE = " + (Integer.parseInt(projectTypeId)) + ") AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
		}

		try {
			query = ((subProjectType == null) || subProjectType.equals("")) ? query : (query + " SPROJ_ID IN ( SELECT SPROJ_ID FROM SUB_PROJECT WHERE STYPE_ID = " + (Integer.parseInt(subProjectTypeId)) + ") AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
		}

		try {
			query = ((subProjectSubType == null) || subProjectSubType.equals("")) ? query : (query + " SPROJ_ID IN ( SELECT SPROJ_ID FROM SUB_PROJECT WHERE SSTYPE_ID = " + (Integer.parseInt(subProjectSubTypeId)) + ") AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchActivities - " + num.getMessage());
		}

		// form the full query string
		if ((query != null) && !query.equals("")) {
			query = query.substring(0, query.length() - 4); // to remove the trailing AND
			fullQuery = fullQuery + where + query;
		}

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();
			sun.jdbc.rowset.CachedRowSet rs = (sun.jdbc.rowset.CachedRowSet) db.select(fullQuery);

			rs.beforeFirst();

			while (rs.next()) {
				ActivityDetail activityDetail = new ActivityDetail();
				activityDetail.setActivityType(LookupAgent.getActivityType(rs.getString("ACT_TYPE")));
				activityDetail.setStatus(LookupAgent.getActivityStatus(rs.getInt("STATUS")));
				activityDetail.setDescription(rs.getString("description"));

				Activity activity = new Activity(rs.getInt("ACT_ID"), activityDetail);

				list.add(activity);
			}
		} catch (SQLException sql) {
			logger.debug("SQLException in method searchActivities - " + sql.getMessage());
		} catch (Exception e) {
			logger.debug("Exception in method searchActivities - " + e.getMessage());
		}

		return list;
	}

	/**
	 * Searches for SubProjects matching search criteria
	 * 
	 * @return a List of SubProject records
	 */
	public List searchSubProjects(String projectNumber, String projectName, String projectTeam, String personType, String personInfoType, String streetNum, String streetID, String apn, String projectTypeId, String subProjectTypeId, String subProjectSubTypeId) {
		String query = "";
		String where = " WHERE ";
		String fullQuery = "SELECT * FROM SUB_PROJECT  ";

		List list = new ArrayList();

		try {
			query = ((projectNumber == null) || projectNumber.equals("")) ? query : (query + " PROJ_ID = " + (Integer.parseInt(projectNumber)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		query = ((projectName == null) || projectName.equals("")) ? query : (query + " PROJ_ID IN ( SELECT PROJ_ID FROM PROJECT WHERE NAME LIKE '" + StringUtils.formatQueryForString(projectName) + "') AND");

		// projectTeam holds the user_id
		if ((personInfoType != null) && personInfoType.equals("name") && (personType != null) && personType.equals("I")) {
			try {
				query = ((projectTeam == null) || projectTeam.equals("")) ? query : (query + " PROJ_ID IN (SELECT PSA_ID AS PROJ_ID FROM PSA_USER WHERE USER_ID = " + Integer.parseInt(projectTeam) + " AND PSA_TYPE = 'P')  AND");
			} catch (NumberFormatException num) {
				logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
			}
		}

		// projectTeam holds the user_id
		if ((personInfoType != null) && personInfoType.equals("name") && (personType != null) && personType.equals("E")) {
			try {
				query = ((projectTeam == null) || projectTeam.equals("")) ? query : (query + " SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID = " + Integer.parseInt(projectTeam) + "))  AND");
			} catch (NumberFormatException num) {
				logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
			}
		}

		// projectTeam holds the licenseNumber
		if ((personInfoType != null) && personInfoType.equals("licenceNumber")) {
			query = ((projectTeam == null) || projectTeam.equals("")) ? query : (query + " SPROJ_ID IN (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID IN ( SELECT ACT_ID FROM ACTIVITY_PEOPLE WHERE PEOPLE_ID = ( SELECT PEOPLE_ID FROM PEOPLE WHERE LIC_NO ='" + projectTeam + "')))  AND");
		}

		query = ((apn == null) || apn.equals("")) ? query : (query + " PROJ_ID IN (SELECT PROJ_ID FROM PROJECT WHERE LSO_ID IN (SELECT LSO_ID FROM LSO_APN WHERE APN LIKE '" + StringUtils.formatQueryForString(apn) + "'))  AND");

		query = ((streetID == null) || streetID.equals("") || (streetNum == null) || streetNum.equals("")) ? query : (query + " PROJ_ID IN ( SELECT PROJ_ID FROM PROJECT WHERE LSO_ID IN (SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE RTRIM (CAST (STR_NO AS CHARACTER(40))) like '" + StringUtils.formatQueryForNumber(streetNum) + "' AND STREET_ID = " + streetID + "))  AND");

		try {
			query = ((projectType == null) || projectType.equals("")) ? query : (query + " SPROJ_TYPE = " + (Integer.parseInt(projectTypeId)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		try {
			query = ((subProjectType == null) || subProjectType.equals("")) ? query : (query + " STYPE_ID = " + (Integer.parseInt(subProjectTypeId)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		try {
			query = ((subProjectSubType == null) || subProjectSubType.equals("")) ? query : (query + " SSTYPE_ID = " + (Integer.parseInt(subProjectSubTypeId)) + " AND");
		} catch (NumberFormatException num) {
			logger.debug("NumberFormatException in searchSubProjects - " + num.getMessage());
		}

		// form the full query string
		if ((query != null) && !query.equals("")) {
			query = query.substring(0, query.length() - 4); // to remove the trailing AND
			fullQuery = fullQuery + where + query;
		}

		logger.debug("SQL is - " + fullQuery);

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(fullQuery);

			while (rs.next()) {
				SubProjectDetail subProjectDetail = new SubProjectDetail();
				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(rs.getInt("STYPE_ID"));
				subProjectDetail.setSubProjectType(subProjectType);
				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("SSTYPE_ID"));
				subProjectDetail.setSubProjectSubType(subProjectSubType);
				subProjectDetail.setDescription(rs.getString("description"));
				list.add(subProjectDetail);
			}
		} catch (Exception e) {
			logger.debug("Exception in method searchSubProjects - " + e.getMessage());
		}

		return list;
	}

	public int getLandUseIdForSubProject(int subProjectId) {
		int landUseId = -1;
		String sql = "select lso_use_id from land_usage where land_id in " + "(select land_id from v_lso_land where lso_id =" + "(select lso_id from project where proj_id=(select proj_id from sub_project where sproj_id=" + subProjectId + ")))";
		logger.debug("Land Use Id SQL is " + sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				landUseId = rs.getInt("lso_use_id");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("SQLException in method getLandUseId ");
		} catch (Exception e) {
			logger.error("Exception in method getLandUseId ");
		}

		return landUseId;
	}

	/**
	 * @param prjId
	 *            , spType
	 * @return subPrjId
	 * @throws Exception
	 *             returns SubProjectId if there are any SubProject in the given prjId having the same SubProject description
	 */
	public int getSubProjectId(int prjId, int spType) throws Exception {
		logger.info("Entered getSubProjectId(" + prjId + "," + spType + ")");
		int subPrjId = -1;
		String sql = "select * from sub_project where proj_id = " + prjId + " and sproj_type=" + spType;
		logger.debug("SQL is " + sql);
		try {
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				subPrjId = rs.getInt("sproj_id");
			}
			return subPrjId;
		} catch (Exception e) {
			logger.error("Exception in method getSubProjectId ");
			throw e;
		}
	}

	/**
	 * Creates a new sub project
	 * 
	 * @param subProject
	 * @return
	 */
	public int createSubProject(int projectId, String subProjectNameId, String subProjectTypeId, int userId) throws Exception {
		logger.info("createSubProject(" + projectId + "," + subProjectNameId + ", " + subProjectTypeId + ", " + userId + ")");
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			subProjectId = db.getNextId("SUB_PROJECT_ID");

			int sprojNum = db.getNextId("SP_NUM");
			Calendar calendar = Calendar.getInstance();
			SimpleDateFormat formatter = new SimpleDateFormat("yy");
			String subProjectNumber = formatter.format(calendar.getTime()) + StringUtils.fixedLengthString(StringUtils.i2s(sprojNum), 7);

			sql = "insert into sub_project(sproj_id,proj_id,sproj_type,stype_id,created_by,created,sproj_nbr,status) values(" + subProjectId + "," + projectId + "," + subProjectNameId + "," + subProjectTypeId + "," + userId + "," + "current_date" + "," + StringUtils.checkString(subProjectNumber) + ",1)";
			logger.debug(sql);
			db.insert(sql);
			return subProjectId;
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update ProjectDetails  :" + e.getMessage());
			throw e;
		}

	}

	/**
	 * Gets the list of linked sub projects for the given sub project id
	 * 
	 * @param subProjectId
	 * @return
	 */
	public static List getSubProjectList(int subProjectId) {
		List subProjects = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select s.sproj_id,s.sproj_nbr,p.name as projectName,s.description,st.description as status,l.sproj_id_to as linked,lp.description as sprojname from ((sub_project s left outer join lkup_sproj_st st on st.status_id=s.status) join lkup_ptype lp on  s.sproj_type = lp.ptype_id  join project p on p.proj_id=s.proj_id) left outer join sproj_links l on l.sproj_id_to = s.sproj_id and l.sproj_id_from = " + subProjectId + " where  p.lso_id = (select p.lso_id from project p join sub_project s on p.proj_id=s.proj_id and s.sproj_id=" + subProjectId + ") order by projectName";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				LinkSubProjectEdit linkSubProjectEdit = new LinkSubProjectEdit();

				linkSubProjectEdit.setLinked((rs.getString("linked") == null) ? "off" : "on");
				linkSubProjectEdit.setSubProjectId(rs.getString("sproj_id"));
				linkSubProjectEdit.setSubProjectNbr(rs.getString("sproj_nbr"));
				linkSubProjectEdit.setDescription(rs.getString("description"));
				linkSubProjectEdit.setStatus(rs.getString("status"));
				linkSubProjectEdit.setSubProjectName(rs.getString("sprojname"));
				linkSubProjectEdit.setProjectName(rs.getString("projectName"));
				subProjects.add(linkSubProjectEdit);
			}

			rs.close();
		} catch (Exception e) {
			logger.error("Exception in --  getSubProjectList  -- " + e.getMessage());
		}

		return subProjects;
	}

	/**
	 * Gets the list of all linked activities for the given activity id
	 * 
	 * @param activityId
	 * @return
	 */
	public static List getAllLinkedActivitiesList(int activityId, String projectId, String active) {
		logger.info("getAllLinkedActivitiesList(" + activityId + ")");
		List tmpLinkedActivityList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			long lsoId = -1;
			long psaLandId = 0;
			ResultSet psaRs = null;

			// the database handles.
			Wrapper wrapper = new Wrapper();

			// getting lsoId from
			String lsoId_sql = "select lso_Id from activity a, sub_project sp, project p where a.act_id = " + activityId + " and sp.sproj_id = a.sproj_id and sp.proj_Id = p.Proj_Id";
			logger.debug("lsoId_sql is " + lsoId_sql);
			RowSet lsoId_rs = db.select(lsoId_sql);
			while (lsoId_rs.next()) {
				lsoId = lsoId_rs.getInt("lso_Id");
				logger.debug(" LAND LSO ID IS " + lsoId);
			}

			// getting landId from V_LSO_LAND by passing lsoId
			String psaLandIdSql = "select land_Id from v_lso_land where lso_Id=" + lsoId;
			logger.debug("Getting Land_Id " + psaLandIdSql);
			psaRs = wrapper.select(psaLandIdSql);

			while (psaRs.next()) {
				psaLandId = psaRs.getInt("LAND_ID");
			}

			ProjectAgent psaTreeAgent = new ProjectAgent();
			List list = psaTreeAgent.getLsoIdList(psaLandId);
			logger.debug("The lso id from psaTreeList in LinkSubProject is " + list.size());

			String listValues = "";

			for (int i = 0; i < list.size(); i++) {
				if (i == 0) {
					listValues += list.get(i);
				} else {
					listValues += ("," + list.get(i));
				}
			}

			logger.debug("String list is " + listValues);

			String projId_sql = "select p.proj_id from project p left outer join LKUP_PNAME lpn on p.NAME=lpn.NAME join v_lso_type vlt on p.lso_id=vlt.lso_id,lkup_proj_st lps  where p.lso_id in (" + listValues + ") and p.status_id = lps.status_id ";
			if (!projectId.equalsIgnoreCase("0")) {
				projId_sql += " and lpn.PNAME_ID in (" + projectId + ") ";
			}
			projId_sql += "order by applied_dt desc,p.proj_id desc";
			logger.debug("projId_sql is " + projId_sql);
			RowSet projId_rs = db.select(projId_sql);
			List projList = new ArrayList();
			int projId = -1;

			while (projId_rs.next()) {
				projId = projId_rs.getInt("proj_Id");
				projList.add(new Long(projId));
			}
			logger.debug(" Project Id List is " + projList.size());
			String projectListValues = "";
			for (int i = 0; i < projList.size(); i++) {
				if (i == 0) {
					projectListValues += projList.get(i);
				} else {
					projectListValues += ("," + projList.get(i));
				}
			}
			logger.debug("projectListValues is " + projectListValues);

			String subprojId_sql = "select sproj_Id from sub_project sp where sp.proj_Id in(" + projectListValues + ")";
			logger.debug("subprojId_sql is " + subprojId_sql);
			RowSet subprojId_rs = db.select(subprojId_sql);
			List sprojList = new ArrayList();
			int sprojId = -1;
			while (subprojId_rs.next()) {
				sprojId = subprojId_rs.getInt("sproj_Id");
				sprojList.add(new Long(sprojId));
			}
			logger.debug(" subProject Id List is " + sprojList.size());
			String subProjListValues = "";
			for (int i = 0; i < sprojList.size(); i++) {
				if (i == 0) {
					subProjListValues += sprojList.get(i);
				} else {
					subProjListValues += ("," + sprojList.get(i));
				}
			}
			logger.debug("Sub project List Values is " + subProjListValues);

			String landId = "";
			String getLandIdSqlForActivityId = "select land_id from v_lso_land where lso_id in (select lso_id from project where proj_id in (select proj_id from sub_project where sproj_id in (select sproj_id from activity where act_id=" + activityId + ")))";
			logger.info(getLandIdSqlForActivityId);
			RowSet getLandIdSqlForActivityIdResult = db.select(getLandIdSqlForActivityId);
			if (getLandIdSqlForActivityIdResult.next()) {
				landId = getLandIdSqlForActivityIdResult.getString(1);
			}
			logger.debug("obtained land id is " + landId);

			// Getting all the activities under given lso_Id
			String sql = "select a.act_id,a.act_nbr,a.description,a.act_type,st.description as status,l.activity_id_to as linked from ((activity a left outer join lkup_act_st st on st.status_id = a.status) join sub_project sp on sp.sproj_id = a.sproj_id) left outer join activity_links l on l.activity_id_to = a.act_id and l.activity_id_from =  " + activityId + "  where a.act_id!= " + activityId + " and a.sproj_Id in(" + subProjListValues + ")";
			if (active.equalsIgnoreCase("Y")) {
				sql = sql + " and st.psa_active='Y' ";
			}
			logger.info(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				LinkActivityEdit activity = new LinkActivityEdit();
				activity.setLinked((rs.getString("linked") == null) ? "off" : "on");
				activity.setActivityId(rs.getString("act_id"));
				activity.setActivityId(rs.getString("act_id"));
				activity.setActivityType(LookupAgent.getActivityDesc(rs.getString("act_type")));
				activity.setActivityNbr(rs.getString("act_nbr"));
				activity.setDescription(rs.getString("description"));
				activity.setStatus(rs.getString("status"));
				tmpLinkedActivityList.add(activity);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception in --getAllLinkedActivitiesList:  " + e.getMessage());
		}

		return tmpLinkedActivityList;
	}

	public static List getLinkedSubProjects(int subProjectId) {
		List subProjects = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select s.sproj_id,s.sproj_nbr,s.description,st.description as status,l.sproj_id_to as linked" + " from ((sub_project s left outer join lkup_sproj_st st on st.status_id=s.status) join project p on p.proj_id=s.proj_id) join sproj_links l on l.sproj_id_to = s.sproj_id and l.sproj_id_from = " + subProjectId + " order by l.sproj_id_from,s.sproj_id desc";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				LinkSubProjectEdit subProject = new LinkSubProjectEdit();
				subProject.setLinked((rs.getString("linked") == null) ? "off" : "on");
				subProject.setSubProjectId(rs.getString("sproj_id"));
				subProject.setSubProjectNbr(rs.getString("sproj_nbr"));
				subProject.setDescription(rs.getString("description"));
				subProject.setStatus(rs.getString("status"));
				subProjects.add(subProject);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception in --  getActivity List  -- " + e.getMessage());
		}

		return subProjects;
	}

	public static List getLinkedActivityList(int activityId) {
		List tmpLinkedActivityList = new ArrayList();

		try {
			Wrapper db = new Wrapper();
			String sql = "select a.act_id,a.act_nbr,a.description,st.description as status,l.activity_id_to as linked from ((activity a left outer join lkup_act_st st on st.status_id = a.status) join sub_project sp on sp.sproj_id = a.sproj_id ) join activity_links l on l.activity_id_to = a.act_id and l.activity_id_from = " + activityId + " order by l.activity_id_from,a.act_id desc";
			logger.debug(sql);

			RowSet rs = db.select(sql);

			while (rs.next()) {
				LinkActivityEdit activity = new LinkActivityEdit();
				activity.setLinked((rs.getString("linked") == null) ? "off" : "on");
				activity.setActivityId(rs.getString("act_id"));
				activity.setActivityNbr(rs.getString("act_nbr"));
				activity.setDescription(rs.getString("description"));
				activity.setStatus(rs.getString("status"));
				tmpLinkedActivityList.add(activity);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception in --getLinkedActivityList:  " + e.getMessage());
		}

		return tmpLinkedActivityList;
	}

	public static List getResolutionSqls(String levelType, int levelId, int userId, List resolutionEdits) throws Exception {
		List sqls = new ArrayList();
		Wrapper db = new Wrapper();

		String sql = "";
		Iterator iter = resolutionEdits.iterator();

		while (iter.hasNext()) {
			ResolutionEdit resolution = (ResolutionEdit) iter.next();

			if (!(resolution.getDelete() == null) && resolution.getDelete().equals("on")) {
				sql = "delete from resolution where resolution_id = " + resolution.getResolutionId();
				sqls.add(sql);
			} else if (resolution.getResolutionId().equals("0")) {
				int resolutionId = db.getNextId("RESOLUTION_ID");
				sql = "insert into resolution(resolution_id,act_id,sproj_id,resolution_nbr,description,date_approved,updated_by,updated)" + " values(" + resolutionId + ",";

				if (levelType.equals("A")) {
					sql += (levelId + ",0,");
				} else {
					sql += ("0," + levelId + ",");
				}

				sql += (StringUtils.checkString(resolution.getResolutionNbr()) + "," + StringUtils.checkString(resolution.getDescription()) + "," + StringUtils.checkString(resolution.getApprovedDate()) + "," + userId + "," + "CURRENT_TIMESTAMP)");
				sqls.add(sql);
			} else {
				sql = "update resolution set " + " resolution_nbr = " + StringUtils.checkString(resolution.getResolutionNbr()) + ",description = " + StringUtils.checkString(resolution.getDescription()) + ",date_approved = " + StringUtils.checkString(resolution.getApprovedDate()) + ",updated_by = " + userId + ",updated = CURRENT_TIMESTAMP " + "  where resolution_id = " + resolution.getResolutionId();
				sqls.add(sql);
			}
		}

		return sqls;
	}

	public int addProject(String projName, int lsoId, User user) throws Exception {

		Lso lso = new Lso();
		lso.setLsoId(lsoId);
		Use useObj = new Use(2, "");
		// Project Details
		ProjectDetail projectDetail = new ProjectDetail();
		projectDetail.setName(projName);
		projectDetail.setDescription(projName);
		projectDetail.setProjectStatus(LookupAgent.getProjectStatus(Constants.PROJECT_STATUS));
		projectDetail.setCreatedBy(user);
		projectDetail.setUpdatedBy(user);

		// not really required
		List subProjects = new ArrayList();
		ProjectFinance projectFinance = new ProjectFinance();
		List projectHolds = new ArrayList();
		List projectConditions = new ArrayList();
		List projectAttachments = new ArrayList();
		List projectComments = new ArrayList();
		List processTeams = new ArrayList();
		List peoples = new ArrayList();
		String project_Type = "";
		Project project = new Project(0, lso, projectDetail, subProjects, projectFinance, projectHolds, projectConditions, projectAttachments, projectComments, processTeams, peoples, project_Type, useObj);
		logger.debug("created the project object as " + project);
		int projectId = addProject(project);
		return projectId;
	}

	/**
	 * Gets the street
	 * 
	 * @return Returns a Street
	 */
	public Street getStreet(int streetId) {
		logger.info("getStreet(" + streetId + ")");

		// String sql = "select street_id,street_name from v_street_list where street_id=" + streetId;
		String sql = "select * from street_list where street_id=" + streetId;

		try {
			RowSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				String prefixDirection = rs.getString("pre_dir") != null ? rs.getString("pre_dir") : "";
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = rs.getString("str_type") != null ? rs.getString("str_type") : "";
				String suffixDirection = rs.getString("suf_dir") != null ? rs.getString("suf_dir") : "";
				String fullStreetName = prefixDirection + " " + streetName + " " + streetType;
				street = new Street(streetId, fullStreetName);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return street;
	}

	public List getStreets(String lsoId) {
		String sql = "select street_id,street_name from v_street_list where street_id in " + "(select street_id from lso_addr_range where lso_id=" + lsoId + ") order by street_name";
		List streetList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				street = new Street(rs.getInt("street_id"), StringUtils.properCase(rs.getString("street_name")));
				streetList.add(street);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public List getStreetList() {
		String sql = "select street_id,street_name from v_street_list order by street_name";
		List streetList = new ArrayList();

		try {
			RowSet rs = new Wrapper().select(sql);
			while (rs.next()) {
				streetList.add(new elms.util.DDList(new Integer(rs.getString("street_id")), rs.getString("street_name")));
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public List getStreetArrayList() {
		String sql = "select * from street_list where street_id > 0 order by str_name";
		List streetList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				String prefixDirection = rs.getString("pre_dir") != null ? rs.getString("pre_dir") : "";
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = rs.getString("str_type") != null ? rs.getString("str_type") : "";
				String suffixDirection = rs.getString("suf_dir") != null ? rs.getString("suf_dir") : "";
				String fullStreetName = prefixDirection + " " + streetName + " " + streetType;

				street = new Street(rs.getInt("street_id"), StringUtils.properCase(fullStreetName));
				streetList.add(street);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public List getStreetNameArrayList() {
		String sql = "select unique(STR_NAME),STR_TYPE,STREET_ID  from street_list where street_id > 0 order by str_name";
		List streetList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = rs.getString("str_type") != null ? rs.getString("str_type") : "";
				String fullStreetName = streetName + " " + streetType;
				int streetId = rs.getInt("STREET_ID");

				street = new Street(streetId, StringUtils.properCase(streetName), StringUtils.properCase(streetType));
				streetList.add(street);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public List getUniqueStreetNameArrayList() {
		String sql = "select Unique(STR_NAME || ' ' ||STR_TYPE) as STR_NAME  from street_list where street_id > 0 order by str_name";
		List streetList = new ArrayList();
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				String streetName = rs.getString("str_name") != null ? rs.getString("str_name") : "";
				String streetType = "";

				street = new Street(StringUtils.properCase(streetName), StringUtils.properCase(streetName));

				// street.setStreetId(rs.getInt("STREET_ID"));
				streetList.add(street);
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return streetList;
	}

	public String getCrossStreet(int street1, int street2) {
		String crossStreet = "";
		String sql = "select * from v_street_list where street_id in (" + street1 + "," + street2 + ") order by street_name";
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			while (rs.next()) {
				crossStreet = crossStreet + rs.getString("street_name") + " x ";
			}
			if (rs != null)
				rs.close();
			if (crossStreet != null && !crossStreet.equals(""))
				crossStreet = crossStreet.substring(0, crossStreet.length() - 3);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return crossStreet;
	}

	/**
	 * gets the Street List .
	 * 
	 * @return
	 * @author Hemavathi
	 */
	public List getStreetListAdmin() throws Exception {
		String sql = "";

		// sql = "select sl.*, from street_list sl join lso_address la on  la.street_id = sl.street_id where   sl.street_id > 0  order by str_name";

		sql = " select DISTINCT sl.*,la.STREET_ID  AS checkStreetId from street_list sl left join lso_address la on la.street_id = sl.street_id where   sl.street_id > 0  order by str_name";

		List displayList = new ArrayList();

		try {
			ResultSet rs = new Wrapper().select(sql);

			while (rs != null && rs.next()) {
				DisplayItem displayItem = new DisplayItem();
				displayItem.setFieldOne(rs.getString("street_id"));
				displayItem.setFieldTwo(rs.getString("str_name"));
				displayItem.setFieldThree(rs.getString("pre_dir"));
				displayItem.setFieldFour(rs.getString("str_type"));
				displayItem.setFieldFive(rs.getString("suf_dir"));
				if (rs.getString("checkStreetId") != null) {
					displayItem.setFieldSix("N");
				} else
					displayItem.setFieldSix("Y");

				displayList.add(displayItem);
			}
			if (rs != null) {
				rs.close();
			}
			return displayList;
		}

		catch (Exception e) {
			logger.error("General Error in getusers " + e.getMessage());
			throw e;
		}

	}

	/**
	 * gets the street list for a perticular street name.
	 * 
	 * @param streetname
	 * @return form object * @author Hemavathi
	 */

	public StreetListAdminForm getstreetIdList(String streetid, StreetListAdminForm streetListAdminForm) throws Exception {
		logger.info("getActivityStatus(" + streetid + ")");
		int result = 0;

		// StreetListAdminForm streetListAdminForm1 = new StreetListAdminForm();
		// begining of try block
		try {

			String sql = "select * from street_list where STREET_ID=" + streetid;
			logger.debug(sql);
			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {

				streetListAdminForm.setStreetName(rs.getString("str_name"));
				streetListAdminForm.setStreetPrefix(rs.getString("pre_dir"));
				streetListAdminForm.setStreetType(rs.getString("str_type"));
				streetListAdminForm.setStreetSuffix(rs.getString("suf_dir"));
				streetListAdminForm.setStreetListId(rs.getInt("STREET_ID"));

			}
			rs.close();
			return streetListAdminForm;

		}// end of try block

		catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in getting street list" + e.getMessage());
			throw new Exception("Problem in Agent during getting the list");

		}

	}

	/**
	 * Function to Update an Save the street details.
	 * 
	 * @param streetid
	 *            ,streetName,streetPrefix,streetType,streetSuffix
	 * @return * @author Hemavathi
	 */

	public int saveStreetList(int streetid, String streetName, String streetPrefix, String streetType, String streetSuffix) {

		logger.info("saveStreetList(" + streetid + "," + streetName + ", " + streetPrefix + ", " + streetType + "," + streetSuffix + ")");
		Wrapper db = new Wrapper();
		int result = 0;
		String sql = "";
		String sql1 = "";

		// begining of try block
		try {

			sql = "SELECT * FROM street_list WHERE STREET_ID='" + streetid + "'";
			RowSet rs = db.select(sql);

			// Values are updated
			if (rs.next()) {
				sql = "update street_list set str_name=" + StringUtils.checkString(streetName).toUpperCase() + "," + " pre_dir=" + StringUtils.checkString(streetPrefix).toUpperCase() + "," + " str_type=" + StringUtils.checkString(streetType).toUpperCase() + "," + " suf_dir=" + StringUtils.checkString(streetSuffix).toUpperCase() + " WHERE STREET_ID='" + streetid + "'";
				logger.debug(sql);
				db.update(sql);
				result = 2;
			}

			// values are inserted
			else {
				String streetlistid = "";
				sql1 = "select max(street_id)+1 as id from street_list";
				RowSet rs1 = db.select(sql1);
				while (rs1.next()) {
					streetlistid = rs1.getString("id");
				}

				sql = "insert into street_list(STREET_ID,str_name,pre_dir,str_type,suf_dir) values (";
				sql = sql + streetlistid;
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetName).toUpperCase();
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetPrefix).toUpperCase();
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetType).toUpperCase();
				sql = sql + ",";
				sql = sql + StringUtils.checkString(streetSuffix).toUpperCase();
				sql = sql + ")";
				result = 1;
				db.insert(sql);

			}
		} // end of try block
		catch (Exception e) {
			e.printStackTrace();
			result = -1;
			logger.error("Error in saveStreetList() method" + e.getMessage());
		}
		return result;
	}

	/**
	 * Function to delete the street list.
	 * 
	 * @param streetid
	 * @return * @author Hemavathi
	 */

	public int deleteStreetList(String streetId) {
		int result = 0;
		String sql = "delete from street_list where street_id=" + streetId;
		logger.debug(sql);

		// begining of try block
		try {
			new Wrapper().update(sql);
		} catch (SQLException e) {
			result = -1;
			logger.error("Sql error is deleteStreetList() " + e.getMessage());
		} catch (Exception e) {
			result = -1;
			logger.error("General error is deleteStreetList() " + e.getMessage());
		}

		return result;
	}

	public String getStreetName(String StreetNameId) throws AgentException {
		String streetName = "";

		String getStreetNameQuery = "Select STR_NAME from STREET_LIST where STREET_ID = " + StreetNameId;

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();

			pstmt = connection.prepareStatement(getStreetNameQuery);

			rs = pstmt.executeQuery();

			if (rs != null && rs.next()) {
				streetName = rs.getString("STR_NAME");
			}
		} catch (Exception ex) {
			logger.error("Exception in getStreetName", ex);
			throw new AgentException();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception Ignore) {
			}
		}
		return streetName;
	}

	public Map getStreetNameMap() throws AgentException {
		Map streetMap = new HashMap();

		String getStreetQuery = "select STREET_ID,PRE_DIR,STR_NAME,STR_TYPE from STREET_LIST ";

		String streetName = "";

		logger.debug(getStreetQuery);

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();

			pstmt = connection.prepareStatement(getStreetQuery);

			rs = pstmt.executeQuery();

			if (rs != null) {
				String name = "";
				String id = "";
				String preDir = "";
				String strType = "";
				while (rs.next()) {
					streetName = "";
					name = rs.getString("STR_NAME");
					id = rs.getString("STREET_ID");
					preDir = rs.getString("PRE_DIR");
					strType = rs.getString("STR_TYPE");

					if (preDir != null) {
						streetName = streetName + preDir + " ";
					}

					if (name != null) {
						streetName = streetName + name + " ";
					}

					if (strType != null) {
						streetName = streetName + strType + " ";
					}
					streetMap.put(id, streetName);
				}
			}
		} catch (Exception ex) {
			logger.error("Exception in getStreetName", ex);
			throw new AgentException();
		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
				if (pstmt != null) {
					pstmt.close();
				}
				if (rs != null) {
					rs.close();
				}
			} catch (Exception Ignore) {
			}
		}

		logger.debug("Map Size::" + streetMap.size());
		return streetMap;
	}

	public int getStreetIDForLSO(String lsoId) {
		String sql = "select STREET_ID from v_address_list where lso_id=" + lsoId;
		int id = -1;
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			if (rs.next()) {
				id = rs.getInt("street_id");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return id;
	}

	public int getStreetNOForLSO(String lsoId) {
		String sql = "select STR_NO from v_address_list where lso_id=" + lsoId;
		int id = -1;
		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql);
			if (rs.next()) {
				id = rs.getInt("STR_NO");
			}
			if (rs != null)
				rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return id;
	}

	public int getStreetId(String streetDir, String streetName) throws AgentException {
		int streetId = 0;
		String sql = " SELECT * FROM STREET_LIST where (" + " UPPER(STR_NAME || ' ' || STR_TYPE) = UPPER('" + streetName + "')" + " or" + " UPPER(STR_NAME) = UPPER('" + streetName + "'))";

		if (streetDir != null && !streetDir.equalsIgnoreCase("")) {
			sql = sql + " and PRE_DIR = '" + streetDir + "'";
		} else {
			sql = sql + " and PRE_DIR is null";

		}

		logger.debug("sql for streetID::" + sql);
		Wrapper db = new Wrapper();

		RowSet rs = null;

		try {
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				streetId = rs.getInt("STREET_ID");
			}

		} catch (Exception e) {
			logger.debug("", e);
			throw new AgentException("", e);
		}

		return streetId;
	}

	/**
	 * Access method for the subProject property.
	 * 
	 * @return the current value of the subProject property
	 */
	public SubProject getSubProject(int subProjId) throws Exception {
		try {
			Wrapper db = new Wrapper();

			// getting subproject Details; //commented by Raj for CSi PSA structure
			// String sql = "select sp.sproj_id,val.address,sp.sproj_nbr,sp.proj_id,sp.caselog_id,sp.sproj_type,sp.stype_id,sp.sstype_id,sp.status,sp.description,sp.created,sp.created_by,sp.updated,sp.updated_by," + "lpt.description as lpt_desc," + "lspt.description as lspt_desc,lspt.dept_code,lspt.sproj_type as lspt_sproj_type," + "lspst.type,lspst.description as lspst_desc" + " from ((((sub_project sp left outer join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id)" + " left outer join lkup_sproj_type lspt on sp.stype_id = lspt.sproj_type_id) " + " left outer join lkup_sproj_stype lspst on sp.sstype_id = lspst.sproj_stype_id)" + " left outer join v_psa_list vpl on vpl.sproj_id=sp.sproj_id)" + " left outer join v_address_list val on vpl.lso_id=val.lso_id" + " where sp.sproj_id=" + subProjId;
			String sql = "select sp.sproj_id,val.address,sp.sproj_nbr,sp.proj_id,sp.caselog_id,sp.sproj_type,sp.stype_id,sp.sstype_id,sp.status,sp.description,sp.created,sp.created_by,sp.updated,sp.updated_by," + "lpt.description as lpt_desc," + "lspt.description as lspt_desc,lspt.dept_code," + "lspst.type,lspst.description as lspst_desc" + " from ((((sub_project sp left outer join lkup_ptype lpt on sp.sproj_type=lpt.ptype_id)" + " left outer join lkup_sproj_type lspt on sp.SPROJ_TYPE = lspt.sproj_type_id) " + " left outer join lkup_sproj_stype lspst on sp.sstype_id = lspst.sproj_stype_id)" + " left outer join v_psa_list vpl on vpl.sproj_id=sp.sproj_id)" + " left outer join v_address_list val on vpl.lso_id=val.lso_id" + " where sp.sproj_id=" + subProjId;
			logger.debug(" subProjectAgent -- getSubProject() -- select Sql  is " + sql);

			RowSet rs = db.select(sql);
			subProject = new SubProject();
			subProject.setSubProjectId(subProjId);
			logger.debug(" subProjectAgent -- getSubProject() -- subProject -- subprojectId is set to  " + subProject.getSubProjectId());
			subProjectDetail = new SubProjectDetail();

			if (rs.next()) {
				subProject.setProjectId(rs.getInt("proj_id"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProject -- projectId is set to  " + subProject.getProjectId());

				// getting the project Type Object
				projectType = new ProjectType();
				projectType.setProjectTypeId(rs.getInt("sproj_type"));
				logger.debug(" subProjectAgent -- getSubProject() -- projectType -- ProjectTypeId is set to  " + projectType.getProjectTypeId());
				projectType.setDescription(rs.getString("lspt_desc"));
				logger.debug(" subProjectAgent -- getSubProject() -- projectType -- Description is set to  " + projectType.getDescription());

				// getting the subproject type Object
				subProjectType = new SubProjectType();
				subProjectType.setSubProjectTypeId(rs.getInt("stype_id"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- SubProjectTypeId is set to  " + subProjectType.getSubProjectTypeId());
				subProjectType.setDescription(rs.getString("lspt_desc"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- Description is set to  " + subProjectType.getDescription());

				// subProjectType.setLsoUseId(rs.getInt("lso_use_id"));
				// logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- lso_use_id is set to  " + subProjectType.getLsoUseId());
				subProjectType.setDepartmentId(rs.getString("dept_code"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- DepartmentCode is set to  " + subProjectType.getDepartmentId());
				// subProjectType.setSubProjectType(rs.getString("lspt_sproj_type"));
				subProjectType.setSubProjectType("");
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectType -- SubProjectType is set to  " + subProjectType.getSubProjectType());

				// getting sub project sub type Object
				subProjectSubType = new SubProjectSubType();
				subProjectSubType.setSubProjectSubTypeId(rs.getInt("sstype_id"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectSubType -- SubProjectSubTypeId is set to  " + subProjectSubType.getSubProjectSubTypeId());
				subProjectSubType.setType(rs.getString("type"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectSubType -- SubProjectType is set to  " + subProjectSubType.getType());
				subProjectSubType.setDescription(rs.getString("lspst_desc"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectSubType -- SubProjectType is set to  " + subProjectSubType.getDescription());
				subProjectDetail.setSubProjectNumber(rs.getString("sproj_nbr"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- SubProjectType is set to  " + subProjectDetail.getSubProjectNumber());

				subProjectDetail.setCaseLogId(rs.getInt("caselog_id"));
				logger.debug("subProjectAgent - caselogId: " + subProjectDetail.getCaseLogId());
				String sql1 = "select description from lkup_caselog where caselog_id = " + subProjectDetail.getCaseLogId() + "";
				logger.debug(sql1);
				RowSet rs1 = db.select(sql1);
				while (rs1.next()) {
					subProjectDetail.setCaseLogDesc(rs1.getString("description"));
				}

				subProjectDetail.setAddress(rs.getString("address"));
				logger.debug("subProjectDetail -- Address is set to  " + subProjectDetail.getAddress());

				subProjectDetail.setProjectType(projectType);
				logger.debug("subProjectDetail -- SubProjectType is set to  " + subProjectDetail.getProjectType().toString());
				subProjectDetail.setSubProjectType(subProjectType);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail-- SubProjectType is set to  " + subProjectDetail.getSubProjectType().toString());
				subProjectDetail.setSubProjectSubType(subProjectSubType);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- SubProjectSubType is set to  " + subProjectDetail.getSubProjectSubType().toString());

				subProjectDetail.setSubProjectStatus(LookupAgent.getSubProjectStatus(rs.getInt("status")));

				if (subProjectDetail.getSubProjectStatus() == null) {
					subProjectDetail.setSubProjectStatus(new SubProjectStatus());
				}

				logger.debug("getSubProject -- Sub Project Details ..Status is .. " + subProjectDetail.getSubProjectStatus().getDescription());

				subProjectDetail.setDescription(rs.getString("description"));
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Description is set to  " + subProjectDetail.getDescription());
				subProjectDetail.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));

				if (subProjectDetail.getCreatedBy() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- created user is set to  " + subProjectDetail.getCreatedBy().getUsername());
				}

				subProjectDetail.setCreated(rs.getDate("created"));

				if (subProjectDetail.getCreated() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- created date is set to  " + subProjectDetail.getCreated().getTime());
				}

				subProjectDetail.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));

				if (subProjectDetail.getUpdatedBy() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- updated user is set to  " + subProjectDetail.getUpdatedBy().getUsername());
				}

				subProjectDetail.setUpdated(rs.getDate("updated"));

				if (subProjectDetail.getUpdated() != null) {
					logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- updateddate is set to  " + subProjectDetail.getUpdated().getTime());
				}

				List countByHoldTypes = new CommonAgent().getCountByHoldTypes(subProject.getSubProjectId(), "Q");

				if ((countByHoldTypes == null) || (countByHoldTypes.size() == 0)) {
					countByHoldTypes = new ArrayList();
				}

				Iterator iter = countByHoldTypes.iterator();

				while (iter.hasNext()) {
					CountByHoldType countByHoldType = (CountByHoldType) iter.next();
					logger.debug(" countByHoldType  " + subProjectDetail.getHardHoldCount());

					if (countByHoldType.getHoldType().equals("H")) {
						subProjectDetail.setHardHoldCount(countByHoldType.getCount());
					}

					logger.debug("subProjectAgent -- getSubProject() -- subProjectDetail -- Hard hold count is set to  " + subProjectDetail.getHardHoldCount());

					if (countByHoldType.getHoldType().equals("S")) {
						subProjectDetail.setSoftHoldCount(countByHoldType.getCount());
					}

					logger.debug("subProjectAgent -- getSubProject() -- subProjectDetail -- Soft hold count is set to  " + subProjectDetail.getSoftHoldCount());

					if (countByHoldType.getHoldType().equals("W")) {
						subProjectDetail.setWarnHoldCount(countByHoldType.getCount());
					}

					logger.debug("subProjectAgent -- getSubProject() -- subProjectDetail -- Warn hold count is set to  " + subProjectDetail.getWarnHoldCount());
				}

				// int attachmentCount = new AttachmentAgent().getAttachmentCount(subProject.getSubProjectId(), "Q");
				// subProjectDetail.setAttachmentCount(attachmentCount);

				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Attachment Count  is set to  " + subProjectDetail.getAttachmentCount());

				int conditionCount = new CommonAgent().getConditionCount(subProject.getSubProjectId(), "Q");
				subProjectDetail.setConditionCount(conditionCount);
				logger.debug(" subProjectAgent -- getSubProject() -- subProjectDetail -- Condition Count  is set to  " + subProjectDetail.getConditionCount());
			}

			rs.close();
			rs = null;
			subProject.setSubProjectDetail(subProjectDetail);
			logger.debug(" subProjectAgent -- getSubProject() --subProject  -- SubProjectDetail -- is set to  " + subProject.getSubProjectDetail().toString());

			// Get all Activities for the Sub-Project
			activities = new ArrayList();
			activities = new ActivityAgent().getActivitiesLite(subProjId);
			subProject.setActivity(activities);
			logger.debug("Activity List is set to  " + subProject.getActivity().size());

			// Get the Financial History for the Sub-Project
			subProjectFinance = new SubProjectFinance();
			subProject.setSubProjectFinance(subProjectFinance);
			logger.debug(" subProjectAgent -- getSubProject() --subProject  -- SubProjectFinance -- is set to  " + subProject.getSubProjectFinance().toString());

			// Get the Holds count for the Sub-Project
			subProject.setPsaHold(new CommonAgent().getHolds(subProjId, "Q", 3));
			logger.debug(" subProjectAgent -- getSubProject() --subProject  -- List of Holds Set with size " + subProject.getPsaHold().size());

			if (LookupAgent.getProjectNameId("Q", subProjId) == Constants.PROJECT_NAME_PLANNING_ID) { // Planning SubProject
				subProject.setPlanningDetails(new ActivityAgent().getPlanningDetails("Q", subProjId));
			}
			return subProject;
		} catch (Exception e) {
			logger.error("Exception in -- ProjectAgent -- getSubProject() -- " + e.getMessage());
			throw e;
		}

	}

	public int addSubProject(int projectId, User user, ActivityType actType) throws Exception {
		int subProjectId = 0;
		List list = user.getModules();
		int subProjTypeId = actType.getSubProjectType();
		subProjectId = addSubProject(projectId, user, subProjTypeId);
		return subProjectId;
	}

	public int addSubProject(int projectId, User user, int subProjTypeId) throws Exception {
		int subProjectId = 0;
		SubProjectDetail subProjectDetail = new SubProjectDetail();

		ProjectType projectType = new ProjectType();
		projectType.setProjectTypeId(subProjTypeId);
		SubProjectType subProjectType = new SubProjectType();
		subProjectType.setSubProjectTypeId(subProjTypeId);
		SubProjectSubType subProjectSubType = new SubProjectSubType();
		// subProjectSubType.setSubProjectSubTypeId(deptId);

		subProjectDetail.setProjectType(projectType);
		subProjectDetail.setSubProjectType(subProjectType);
		subProjectDetail.setSubProjectSubType(subProjectSubType);
		subProjectDetail.setDescription("");
		subProjectDetail.setCreatedBy(user);
		subProjectDetail.setUpdatedBy(user);
		int projectNameId = -1;
		projectNameId = LookupAgent.getProjectNameId("P", projectId);
		subProjectDetail.setStatus("1");

		// Not really using the below objects...but required
		SubProjectFinance subProjectFinance = new SubProjectFinance();
		List subProjectActivityList = new ArrayList();
		List subProjectHoldList = new ArrayList();
		List subProjectConditionList = new ArrayList();
		List subProjectAttachmentList = new ArrayList();
		List subProjectCommentList = new ArrayList();

		SubProject subProject = new SubProject(0, projectId, subProjectDetail, subProjectFinance, subProjectActivityList, subProjectHoldList, subProjectConditionList, subProjectAttachmentList, subProjectCommentList);

		// adding the sub project to database
		subProjectId = addSubProject(subProject);
		return subProjectId;
	}

	public List getSubProjects(int projectId) {
		try {
			Wrapper db = new Wrapper();
			String sql = "select sproj_id from sub_project where proj_id=" + projectId;
			RowSet rs = db.select(sql);
			subProjects = new ArrayList();

			while (rs.next()) {
				subProject = this.getSubProject(rs.getInt("sproj_id"));
				subProjects.add(subProject);
			}

			rs.close();
			rs = null;
		} catch (Exception e) {
			logger.error("Exception in -- subProjectAgent --  getSubProjectsList  -- " + e.getMessage());
		}

		return subProjects;
	}

	public int getLandUseId(int subProjectId) {
		int landUseId = -1;
		String sql = "select lso_use_id from land_usage where land_id in " + "(select land_id from v_lso_land where lso_id =" + "(select lso_id from project where proj_id=(select proj_id from sub_project where sproj_id=" + subProjectId + ")))";
		logger.debug("Land Use Id SQL is " + sql);

		try {
			RowSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				landUseId = rs.getInt("lso_use_id");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error("SQLException in method getLandUseId ");
		} catch (Exception e) {
			logger.error("Exception in method getLandUseId ");
		}

		return landUseId;
	}

	public Owner getOwnerByAssessorData(String subProjectId) throws Exception {
		logger.debug("getOwnerByAssessorData(" + subProjectId + ")");

		Owner owner = new Owner();

		StringBuffer sql = new StringBuffer();
		sql.append("SELECT DISTINCT O.OWNER_ID,O.NAME,O.LAST_UPDATED,VLL.LSO_TYPE");
		sql.append(" FROM (((V_LSO_LAND VLL JOIN V_PSA_LIST VPL ON VLL.LSO_ID = VPL.LSO_ID AND VPL.SPROJ_ID = " + subProjectId + ")");
		sql.append(" LEFT OUTER JOIN LSO_APN LA ON VPL.LSO_ID = LA.LSO_ID)");
		sql.append(" LEFT OUTER JOIN APN_OWNER AO ON LA.APN = AO.APN )");
		sql.append(" LEFT OUTER JOIN OWNER O ON AO.OWNER_ID = O.OWNER_ID");
		logger.debug("SQL : " + sql.toString());

		try {
			Wrapper db = new Wrapper();
			RowSet rs = db.select(sql.toString());
			SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
			// Getting the latest owner info of activity in that project
			if (rs.next()) {
				if (rs.getInt("OWNER_ID") == 0) {
					if ((rs.getString("LSO_TYPE") != null) && !rs.getString("LSO_TYPE").equals("L")) { // LSO_TYPE equals S or O
						sql = null;
						sql = new StringBuffer();
						sql.append(" SELECT O.OWNER_ID,O.NAME,O.LAST_UPDATED");
						sql.append(" FROM (LSO_APN LA JOIN APN_OWNER AO ON LA.APN = AO.APN)");
						sql.append(" JOIN OWNER O ON AO.OWNER_ID = O.OWNER_ID");
						sql.append(" WHERE LA.LSO_ID =(SELECT LAND_ID FROM V_LSO_LAND");
						sql.append(" WHERE LSO_ID = (SELECT DISTINCT LSO_ID FROM V_PSA_LIST WHERE SPROJ_ID = " + subProjectId + "))");
						logger.debug("SQL2 : " + sql);

						RowSet rs1 = db.select(sql.toString());

						if (rs1.next()) {
							owner.setOwnerId(rs1.getInt("OWNER_ID"));
							owner.setName(rs1.getString("NAME"));
							if (rs1.getString("LAST_UPDATED") != null) {
								owner.setLastUpdated(rs1.getDate("LAST_UPDATED"));
							}
						}

						if (rs1 != null) {
							rs1.close();
						}
					} else {
						owner.setOwnerId(rs.getInt("OWNER_ID"));
						owner.setName(rs.getString("NAME"));
						if (rs.getString("LAST_UPDATED") != null) {
							owner.setLastUpdated(rs.getDate("LAST_UPDATED"));
						}
					}
				} else {
					owner.setOwnerId(rs.getInt("OWNER_ID"));
					owner.setName(rs.getString("NAME"));
					if (rs.getString("LAST_UPDATED") != null) {
						owner.setLastUpdated(rs.getDate("LAST_UPDATED"));
					}
				}
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Error in getOwnerByAssessorData() " + e.getMessage());
			throw e;
		}

		return owner;
	}

	// Getting active or not based on sub project id
	public static String getSubProjectActiveOrNot(int subPrjtId) throws Exception {
		logger.info("Entered getSubProjectActiveOrNot...");
		String active = null;
		RowSet rs = null;
		String sql = "SELECT LSS.PSA_ACTIVE FROM SUB_PROJECT SP , LKUP_SPROJ_ST LSS WHERE SP.STATUS = LSS.STATUS_ID AND SP.SPROJ_ID = " + subPrjtId;
		logger.debug("SQL is " + sql);
		try {
			rs = new Wrapper().select(sql);
			if (rs != null && rs.next()) {
				active = rs.getString("PSA_ACTIVE");
			}

			return active;
		} catch (Exception e) {
			logger.error("Exception in method getSubProjectId ");
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
		}
	}
	
	public String getProjectTypeOfSubProject(int subPrjtId) throws Exception {
		logger.info("Entered getProjectTypeOfSubProject...");
		String ptypeId = null;
		RowSet rs = null;
		String sql = "SELECT DESCRIPTION FROM LKUP_PTYPE WHERE PTYPE_ID = (SELECT SPROJ_TYPE FROM SUB_PROJECT WHERE SPROJ_ID="+subPrjtId+") ";
		logger.debug("SQL is " + sql);
		try {
			rs = new Wrapper().select(sql);
			if (rs != null && rs.next()) {
				ptypeId = rs.getString("DESCRIPTION");
			}

			return ptypeId;
		} catch (Exception e) {
			logger.error("Exception in method getProjectTypeOfSubProject ");
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
		}
	}
	
	public String get311SubProjectTypeName(int subPrjtTypeId) throws Exception {
		logger.info("Entered get311SubProjectTypeName...");
		String description311 = null;
		RowSet rs = null;
		String sql = "SELECT DESCRIPTION_311 FROM LKUP_SPROJ_TYPE WHERE SPROJ_TYPE_ID="+subPrjtTypeId;
		logger.debug("SQL is " + sql);
		try {
			rs = new Wrapper().select(sql);
			if (rs != null && rs.next()) {
				description311 = rs.getString("DESCRIPTION_311");
			}

			return description311;
		} catch (Exception e) {
			logger.error("Exception in method get311SubProjectTypeName ");
			throw e;
		} finally {
			if (rs != null) {
				rs.close();
			}
		}
	}
}

package elms.agent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import sun.jdbc.rowset.CachedRowSet;
import elms.app.admin.ActivityStatus;
import elms.app.admin.ActivityType;
import elms.app.admin.InspectionCategory;
import elms.app.admin.InspectionCode;
import elms.app.admin.InspectionItem;
import elms.app.admin.InspectionSubCategory;
import elms.app.admin.InspectorUser;
import elms.app.inspection.Inspection;
import elms.app.inspection.InspectionLibraryRecord;
import elms.app.inspection.Inspector;
import elms.app.inspection.InspectorRecord;
import elms.app.people.People;
import elms.app.project.Activity;
import elms.app.project.ActivityDetail;
import elms.common.Constants;
import elms.control.beans.InspectionForm;
import elms.control.beans.SearchLibForm;
import elms.control.beans.ViewAllInspectionForm;
import elms.control.beans.online.ApplyDotOnlinePermitForm;
import elms.control.beans.online.ApplyOnlinePermitForm;
import elms.control.beans.online.HolidayEditorForm;
import elms.control.beans.online.MyPermitForm;
import elms.exception.AgentException;
import elms.exception.DuplicateInspectionRouteException;
import elms.util.MessageUtils;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

public class InspectionAgent {
	/**
	 * The logger
	 */
	static Logger logger = Logger.getLogger(InspectionAgent.class.getName());

	/**
	 * The database queries
	 * 
	 */

	public InspectionAgent() {
	}

	/**
	 * Function to get a item code for add Inspection record. Used to set item code in add inspection.
	 */
	public int getInspectionItemCode(String activityId) {
		int inspectionItemCode = 0;

		try {
			String sql = "select * from inspection where act_id = " + activityId + " and actn_code = " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION + " and rownum=1" + " order by timestamp asc ";
			logger.debug("Inspection Item code SQL : " + sql);

			ResultSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				inspectionItemCode = rs.getInt("inspct_item_id");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionItemCode;
	}

	/**
	 * Function to get a list of all Inspections for an Activity. Used to create a list
	 */
	public CachedRowSet getInspectionList(String activityId) {
		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			rs = new CachedRowSet();

			String sql = "select i.inspection_id as id,i.inspection_dt,i.inspector_id inspector_id,i.comments as comments, lii.description as item,lic.description as actn_code, COALESCE(' ' || i.request_source,'') AS request_source from ( inspection i left outer join lkup_inspct_item lii  on i.inspct_item_id = lii.inspct_item_id ) left outer join lkup_inspct_code lic on i.actn_code = lic.actn_code where i.act_id = " + activityId + " order by inspection_id desc";
			logger.debug("inspection List " + sql);
			rs = (CachedRowSet) db.select(sql);
		} catch (Exception e) {
			logger.error("Exception occured in getInspectionList method of InspectionAgent. Message-" + e.getMessage());
		}

		return rs;
	}

	/**
	 * Function to get a list of all Inspections for an Activity. Used to create a list
	 */
	public List getInspectionList(String activityId, int numberOfRows) throws Exception {
		List inspectionList = null;
		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			inspectionList = new ArrayList();

			String sql = "select i.inspection_id ,i.inspection_dt,i.inspector_id ,i.comments , lii.description ,i.actn_code,i.request_source from inspection i left outer join lkup_inspct_item lii on i.inspct_item_id = lii.inspct_item_id  where i.act_id = " + activityId + "and rownum <= " + 3 + " order by inspection_id desc";

			logger.info(sql);
			rs = db.select(sql);

			while (rs.next()) {
				Inspection inspection = new Inspection();
				inspection.setInspectionId(rs.getString("inspection_id"));
				inspection.setDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("inspection_dt"))));
				inspection.setInspectorId(rs.getString("inspector_id"));
				inspection.setComments(rs.getString("comments"));
				inspection.setInspectionItem(rs.getString("description"));
				inspection.setActnCode(rs.getString("actn_code"));
				inspection.setActionCodeDescription(LookupAgent.getActionCodeDesc(rs.getString("actn_code")));
				inspection.setSource(rs.getString("request_source"));
				inspectionList.add(inspection);
			}

			rs.close();

			return inspectionList;
		} catch (SQLException sql) {
			logger.error("SQLException in getInspectionList method of InspectionAgent. Message-" + sql.getMessage());
			throw sql;
		} catch (Exception e) {
			logger.error("Exception occured in getInspectionList method of InspectionAgent. Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to get all data for a specific Inspection
	 */
	public CachedRowSet getInspectionData(String inspectionId) {
		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			rs = new CachedRowSet();

			String sql = "select i.inspection_id,i.act_id,i.inspector_id, i.inspection_dt, i.actn_code, i.route_seq, i.request_source,i.comments, i.inspct_item_id, lii.description from inspection i left outer join lkup_inspct_item lii  on i.inspct_item_id = lii.inspct_item_id where i.inspection_id = " + inspectionId;
			logger.debug("Inspection  " + sql);
			rs = (CachedRowSet) db.select(sql);
		} catch (SQLException sql) {
			logger.error("SQLException in getInspectionData method of InspectionAgent. Message-" + sql.getMessage());
		} catch (Exception e) {
			logger.error("Exception occured in getInspectionData method of InspectionAgent. Message-" + e.getMessage());
		}

		return rs;
	}

	/**
	 * Function to save edited inspection data. Called from SaveEditedInspectionAction.
	 */
	public void saveEditedInspection(InspectionForm form, String outstandingFees) {
		ActivityAgent activityAgent = new ActivityAgent();
		Wrapper db = new Wrapper();

		try {
			String inspectionId = form.getInspectionId();
			String actionCode = form.getActionCode();
			String activityId = form.getActivityId();
			logger.debug("action code ### : " + actionCode);
			logger.debug("outstanding Fees ### : " + outstandingFees);

			if ((actionCode.equals(StringUtils.i2s(Constants.INSPECTION_CODES_FINALED))) && (outstandingFees.equals("N"))) {
				int finalUpdate = activityAgent.finalActivity(activityId);
				logger.debug("do final" + finalUpdate);
			}

			String sql = "UPDATE INSPECTION SET INSPECTION_DT = " + StringUtils.toOracleDate(form.getInspectionDate()) + ", ACTN_CODE = " + form.getActionCode() + ", COMMENTS = '" + StringUtils.replaceQuot(form.getComments()) + "' WHERE INSPECTION_ID = " + inspectionId;
			logger.debug(sql);
			db.beginTransaction();
			db.addBatch(sql);

			// Update activity with expiration date 180 days after inspection date
			Calendar cal = StringUtils.str2cal(form.getInspectionDate());
			cal.add(Calendar.DAY_OF_YEAR, 180); // add 180 days to today's date.
			sql = "update activity set exp_date=" + StringUtils.toOracleDate(StringUtils.cal2str(cal)) + " where act_id=" + activityId;
			logger.debug(sql);
			db.addBatch(sql);
			db.executeBatch();
		} catch (Exception e) {
			db.rollbackTransaction();
			logger.error("Transaction rolled back.Exception occured in saveEditedInspection. Message-" + e.getMessage());
		}
	}

	/**
	 * Function to save newly created inspection data. Called from SaveNewInspectionAction.
	 */
	public void saveNewInspection(InspectionForm form, String outstandingFees, String reQSrc) {
		logger.info("saveNewInspection(InspectionForm , " + outstandingFees + ", " + reQSrc + ")");

		try {
			Wrapper db = new Wrapper();
			String inspectionId = StringUtils.i2s(db.getNextId("INSPECTION_ID"));
			String activityId = form.getActivityId();

			form.setInspectionId(inspectionId);

			String sql = "INSERT INTO INSPECTION(INSPECTION_ID,ACT_ID,INSPECTOR_ID,INSPCT_ITEM_ID,INSPECTION_DT,ACTN_CODE,ROUTE_SEQ,TIMESTAMP,REQUEST_SOURCE,COMMENTS) VALUES (" + form.getInspectionId() + ", " + form.getActivityId() + ", " + form.getInspectorId() + ", " + form.getInspectionItem() + ", to_date('" + form.getInspectionDate() + "','MM/DD/YYYY'), " + form.getActionCode() + ", " + "0" + ", current_timestamp" + "," + StringUtils.checkString(reQSrc) + ", " + StringUtils.checkString(StringUtils.replaceQuot((form.getComments()))) + ")";
			logger.debug(sql);
			db.insert(sql);

			if ((form.getActionCode().equals(StringUtils.i2s(Constants.INSPECTION_CODES_FINALED))) && (outstandingFees.equals("N"))) {
				int finalUpdate = new ActivityAgent().finalActivity(activityId);
				logger.debug("do final" + finalUpdate);
			} else {
				// Update activity with expiration date 180 days after inspection date
				Calendar cal = StringUtils.str2cal(form.getInspectionDate());
				cal.add(Calendar.DAY_OF_YEAR, 180); // add 180 days to today's date.
				sql = "update activity set exp_date=" + StringUtils.toOracleDate(StringUtils.cal2str(cal)) + " where act_id=" + activityId;
				logger.debug(sql);
				db.insert(sql);
			}

			if (!form.getActionCode().equals(StringUtils.i2s(Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION))) {
				sql = "update inspection set actn_code = " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION_HISTORY + " where actn_code= " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION + " and act_id = " + form.getActivityId() + " and inspct_item_id = " + form.getInspectionItem();
				logger.debug(sql);
				db.update(sql);
			}
		} catch (Exception e) {
			logger.error("Exception: " + e.getMessage());
		}
	}

	/**
	 * Get the inspection item codes
	 * 
	 * @param activityType
	 * @return
	 */
	public static List getInspectionItemCodes(String activityType) {
		List inspectionItemCodeList = new ArrayList();
		InspectionItem inspectionItem = null;

		inspectionItemCodeList.add(new InspectionItem(-1, "Please Select"));

		try {
			String sql = "select inspct_item_id, inspection_item_code  || ' - ' || description as description from lkup_inspct_item where act_type='" + activityType + "' order by inspection_item_code";
			logger.info(sql);
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectionItem = new InspectionItem(rs.getInt("inspct_item_id"), rs.getString("description"));
				inspectionItemCodeList.add(inspectionItem);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionItemCodeList;
	}

	/**
	 * Gets the inspections action codes
	 * 
	 * @return
	 */
	public static List getInspectionActionCodes(int deptId) {
		List inspectionActionCodeList = new ArrayList();
		InspectionCode inspectionCode = null;

		inspectionActionCodeList.add(new InspectionCode(-1, "Please Select"));

		try {
			String sql = "select * from lkup_inspct_code where dept_id=" + deptId + " order by description";
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectionCode = new InspectionCode(rs.getInt("ACTN_CODE"), rs.getString("description"));
				inspectionActionCodeList.add(inspectionCode);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionActionCodeList;
	}

	/**
	 * gets the inspection categories
	 * 
	 * @return
	 */
	public static List getInspectionCategories() {
		List inspectionCategories = new ArrayList();
		InspectionCategory inspectionCategory = null;

		inspectionCategories.add(new InspectionCategory(-1, "Please Select"));

		try {
			String sql = "SELECT * FROM LKUP_INSPCT_CAT ORDER BY description";
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectionCategory = new InspectionCategory(rs.getInt("CAT_ID"), (rs.getString("description")).trim());
				inspectionCategories.add(inspectionCategory);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionCategories;
	}

	/**
	 * gets the inspection sub categories
	 * 
	 * @param catDesc
	 * @return
	 */
	public static List getInspectionSubCategories(String catDesc) {
		List inspectionSubCategories = new ArrayList();
		InspectionSubCategory inspectionSubCategory = null;

		inspectionSubCategories.add(new InspectionSubCategory(-1, "Please Select"));

		try {
			String sql = "SELECT SCAT.SCAT_ID, SCAT.description FROM LKUP_INSPCT_SCAT SCAT, LKUP_INSPCT_CAT CAT WHERE SCAT.CAT_ID = CAT.CAT_ID AND ltrim(rtrim(CAT.description)) = '" + catDesc + "' ORDER BY SCAT.description";
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectionSubCategory = new InspectionSubCategory(rs.getInt("SCAT_ID"), rs.getString("description"));
				inspectionSubCategories.add(inspectionSubCategory);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionSubCategories;
	}

	/**
	 * gets the inspection sub categories for a inspection category
	 * 
	 * @param categoryId
	 * @return
	 */
	public static List getInspectionSubCategories(int categoryId) {
		List inspectionSubCategories = new ArrayList();
		InspectionSubCategory inspectionSubCategory = null;
		inspectionSubCategories.add(new InspectionSubCategory(-1, "Please Select"));

		try {
			String sql = "select scat_id,description from lkup_inspct_scat where cat_id  = " + categoryId + "  order by description";
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectionSubCategory = new InspectionSubCategory(rs.getInt("scat_id"), rs.getString("description"));
				inspectionSubCategories.add(inspectionSubCategory);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionSubCategories;
	}

	/**
	 * Function to get a list of users who are inspectors
	 */
	public static List getInspectorUsers(int deptId) {
		List inspectorUsers = new ArrayList();
		InspectorUser inspectorUser = null;

		try {
			String sql = "SELECT DISTINCT USER_ID, FIRST_NAME, LAST_NAME FROM USER_GROUPS, GROUPS, USERS WHERE USERS.USERID = USER_GROUPS.USER_ID AND USER_GROUPS.GROUP_ID in (SELECT GROUP_ID FROM GROUPS WHERE GROUP_ID= " + Constants.GROUPS_INSPECTOR + ") and users.active='Y' and users.dept_id=" + deptId + " ORDER BY FIRST_NAME";

			logger.debug("inspection users " + sql);
			inspectorUser = new InspectorUser(-1, "Unassigned");
			inspectorUsers.add(inspectorUser);

			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectorUser = new InspectorUser(rs.getInt("USER_ID"), rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
				inspectorUsers.add(inspectorUser);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectorUsers;
	}

	/**
	 * Function to get the name of one specific inspector
	 */
	public String getInspectorUser(String inspectorId) {
		String name = "Unassigned";

		try {
			String sql = "SELECT FIRST_NAME, LAST_NAME FROM USERS WHERE USERID = " + inspectorId;

			ResultSet rs = new Wrapper().select(sql);

			if (rs.next()) {
				name = rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());

			return name;
		}

		return name;
	}

	/**
	 * Function to get items from library for a specific sub category
	 */
	public SearchLibForm getLibraryItems(SearchLibForm frm) {
		logger.debug("Inside getLibraryItems");

		SearchLibForm form = new SearchLibForm();

		String category = frm.getCategory();
		String subCategory = frm.getSubCategory();
		String categoryDesc = "";
		String subCategoryDesc = "";
		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			rs = new CachedRowSet();

			String sql = "SELECT * FROM LKUP_INSPCT_LIB WHERE SCAT_ID = " + subCategory;
			logger.debug(sql);
			rs = (CachedRowSet) db.select(sql);

			ArrayList list = new ArrayList();

			if (rs != null) {
				rs.beforeFirst();

				while (rs.next()) {
					InspectionLibraryRecord rec = new InspectionLibraryRecord("false", rs.getString("description"), rs.getString("COMNT_STR"));
					list.add(rec);
				}
			}

			sql = "SELECT description FROM LKUP_INSPCT_CAT WHERE CAT_ID = " + category;
			logger.debug(sql);
			rs = (CachedRowSet) db.select(sql);

			if (rs != null) {
				rs.beforeFirst();

				if (rs.next()) {
					categoryDesc = rs.getString("description");
				}
			}

			sql = "SELECT description FROM LKUP_INSPCT_SCAT WHERE SCAT_ID = " + subCategory;
			logger.debug(sql);
			rs = (CachedRowSet) db.select(sql);

			if (rs != null) {
				rs.beforeFirst();

				if (rs.next()) {
					subCategoryDesc = rs.getString("description");
				}
			}

			rs.close();

			form.setCategory(category);
			form.setSubCategory(subCategory);
			form.setCategoryDesc(categoryDesc);
			form.setSubCategoryDesc(subCategoryDesc);

			InspectionLibraryRecord[] recArray = new InspectionLibraryRecord[list.size()];
			list.toArray(recArray);

			form.setLibRecordList(recArray);
			logger.debug("# of elements set in List - " + recArray.length);
		} catch (Exception e) {
			logger.error("Exception occured in getLibraryItems method of InspectionAgent. Message-" + e.getMessage());
		}

		return form;
	}

	/**
	 * Function to get all inspection items
	 */
	public ViewAllInspectionForm getAllInspections(String id, String date, int deptId) throws Exception {
		logger.info("getAllInspections(" + id + ", " + date + ", " + deptId + ")");

		ViewAllInspectionForm form = new ViewAllInspectionForm();

		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();

			rs = new CachedRowSet();

			StringBuffer sql = new StringBuffer();

			sql.append("SELECT I.INSPECTION_ID,I.ACTN_CODE,I.INSPECTION_DT,I.INSPECTOR_ID,I.INSPCT_ITEM_ID, I.COMMENTS, I.ROUTE_SEQ,A.ACT_ID,A.ACT_NBR, U.FIRST_NAME, U.LAST_NAME,SP.SPROJ_ID,SP.SPROJ_NBR,LA.STR_NO,V.STREET_NAME,LI.DESCRIPTION,P.PROJ_ID,P.PROJECT_NBR FROM ");
			sql.append("INSPECTION I LEFT OUTER JOIN USERS U ON U.USERID=I.INSPECTOR_ID JOIN ACTIVITY A ON A.ACT_ID=I.ACT_ID JOIN LKUP_INSPCT_ITEM LI ON I.INSPCT_ITEM_ID=LI.INSPCT_ITEM_ID JOIN SUB_PROJECT SP ON A.SPROJ_ID = SP.SPROJ_ID JOIN PROJECT P ON P.PROJ_ID=SP.PROJ_ID JOIN LSO_ADDRESS LA ON LA.ADDR_ID=A.ADDR_ID JOIN V_STREET_LIST V ON V.STREET_ID=LA.STREET_ID ");
			sql.append("WHERE I.INSPECTION_DT <= TO_DATE('" + date + "','MM/DD/YYYY') ");

			if (deptId == Constants.DEPARTMENT_DEPARTMENT_BUILDING_SAFETY) {
				logger.debug(":: Getting Building Inspections ::");
				sql.append(" AND I.ACTN_CODE = " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION + " AND A.ACT_NBR LIKE 'BS%'");
			}

			if (deptId == Constants.DEPARTMENT_PUBLIC_WORKS) {
				logger.debug(":: Getting Public works Inspections ::");
				sql.append(" AND I.ACTN_CODE = " + Constants.INSPECTION_CODES_PW_REQUEST_FOR_INSPECTION + " AND ACT_NBR like 'PW%'");
			}

			if ((id != null) && !id.equals("")) {
				sql.append(" AND I.INSPECTOR_ID = " + id);
			}

			sql.append(" ORDER BY U.LAST_NAME,U.FIRST_NAME,I.INSPECTOR_ID,I.ROUTE_SEQ,I.INSPECTION_DT DESC");

			logger.debug(sql.toString());

			rs = (CachedRowSet) db.select(sql.toString());

			List inspectorList = new ArrayList();
			Inspector insp = null;
			String strNo;
			String strName;
			String inspectorId;
			String lastInspectorId = "";
			String name = "";
			InspectorRecord rec;
			List list = new ArrayList();
			String inspctItem;

			while (rs.next()) {
				inspectorId = rs.getString("INSPECTOR_ID");

				if ((inspectorId == null) || inspectorId.equals("") || inspectorId.equals("0")) {
					inspectorId = "0";
				}

				if (!inspectorId.equals(lastInspectorId)) {
					if (!lastInspectorId.equals("")) {
						insp.setInspectorRecord(list);
						inspectorList.add(insp);
					}

					if (inspectorId.equals("0")) {
						name = "Unassigned";
					} else {
						name = "";

						if (rs.getString("FIRST_NAME") != null) {
							name = rs.getString("FIRST_NAME") + " ";
						}

						if (rs.getString("LAST_NAME") != null) {
							name += (rs.getString("LAST_NAME") + " ");
						}
					}

					insp = new Inspector();
					insp.setInspectorId(inspectorId);
					insp.setName(name);

					list = new ArrayList();
					lastInspectorId = inspectorId;
				}

				inspctItem = rs.getString("description");
				strName = rs.getString("STREET_NAME");
				strNo = rs.getString("STR_NO");

				rec = new InspectorRecord("off", rs.getString("PROJ_ID"), rs.getString("PROJECT_NBR"), rs.getString("SPROJ_ID"), rs.getString("SPROJ_NBR"), strNo + " " + strName, rs.getString("ACT_ID"), inspctItem, rs.getString("INSPECTION_ID"), rs.getString("ACTN_CODE"), rs.getString("ACT_NBR"), rs.getString("ROUTE_SEQ"));
				rec.setInspectionDate(StringUtils.sqlDateToString(rs.getDate("INSPECTION_DT")));
				
				list.add(rec);
			}

			if (!lastInspectorId.equals("")) {
				insp.setInspectorRecord(list);
				inspectorList.add(insp);
			}

			Inspector[] inspArray = new Inspector[inspectorList.size()];
			inspectorList.toArray(inspArray);

			form.setInspector(inspArray);
			form.setInspectionDate(date);
			form.setRoutingId(id);
			form.setChanged("no");

			rs.close();

			return form;
		} catch (Exception e) {
			logger.error("Exception occured in getAllInspections method of InspectionAgent. Message-" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Function to change inspection assignments
	 */
	public void assignInspections(ViewAllInspectionForm frm) {
		logger.info("assignInspections(ViewAllInspectionForm frm)");
		String reAssign = "";
		String inspectionId = "";
		List inspectionList = new ArrayList();
		Inspector[] inspArray;
		InspectorRecord[] inspRecArray;

		try {
			// loop over the for and collect the inspection_ids to be modified
			if (frm != null) {
				reAssign = frm.getReAssign();
				inspArray = frm.getInspector();

				if ((reAssign == null) || reAssign.equals("") || (inspArray == null) || (inspArray.length <= 0)) {
					return;
				}

				for (int i = 0; i < inspArray.length; i++) {
					// this part for checking for reAssign
					inspRecArray = inspArray[i].getInspectorRecord();

					for (int j = 0; j < inspRecArray.length; j++) {
						inspectionId = inspRecArray[j].getInspectionId();

						if (!inspectionList.contains(inspectionId) && inspRecArray[j].getCheck().equals("on")) {
							inspectionList.add(inspectionId);
						}
					}

					// end for inspRecArray.length
					// this part for checking for selectAll
					if (inspArray[i].getSelectValue().equals("all")) {
						for (int j = 0; j < inspRecArray.length; j++) {
							inspRecArray[j].setCheck("on");
						}
					}

					// end if "all"
					inspArray[i].setSelectValue("off");
				}

			}

			if (inspectionList.size() > 0) {
				String sql = "UPDATE INSPECTION SET INSPECTOR_ID =" + reAssign + " WHERE INSPECTION_ID IN (";

				for (int i = 0; i < inspectionList.size(); i++) {
					sql = sql + inspectionList.get(i) + ", ";
				}

				// remove the trailing comma
				sql = sql.substring(0, sql.length() - 2);

				// add the closing bracket
				sql = sql + ")";

				Wrapper db = new Wrapper();

				// logger.debug("sql query is: " + sql);
				db.update(sql);
			}
		} catch (Exception e) {
			logger.error("Exception occured in assignInspections method of InspectionAgent. Message-" + e.getMessage());
		}
	}

	public String changeRouting(ViewAllInspectionForm form) throws Exception {
		logger.info("changeRouting(ViewAllInspectionForm)");

		String inspectorId = "";
		Inspector[] inspectorArray;
		InspectorRecord[] inspectorRecordArray;
		Wrapper db = new Wrapper();
		Connection connection = db.getConnectionForPreparedStatementOnly();

		PreparedStatement statement = connection.prepareStatement("UPDATE INSPECTION SET ROUTE_SEQ = ? WHERE INSPECTION_ID = ?");

		try {
			// there needs to be at least one inspector record in the form
			if (form != null) {
				inspectorArray = form.getInspector();

				if ((inspectorArray == null) || !(inspectorArray.length > 0)) {
					logger.error("no inspectors found in the form ");
					throw new Exception("No inspectors found");
				} else {
					inspectorId = inspectorArray[0].getInspectorId();
					logger.debug("The inspector id is " + inspectorId);
					inspectorRecordArray = inspectorArray[0].getInspectorRecord();
					logger.debug("The number of inspection records found is " + inspectorRecordArray.length);

					// check for any duplicate elements and return without saving if there are any
					Set set = new HashSet();

					for (int i = 0; i < inspectorRecordArray.length; i++) {
						if (inspectorRecordArray[i].getRoute().equals("")) {
							logger.debug("No route defined, throwing exception");
							throw new DuplicateInspectionRouteException("No route defined");
						}

						set.add(inspectorRecordArray[i].getRoute());
					}

					if (set.size() != inspectorRecordArray.length) {
						logger.debug("Duplicate route defined, throwing exception");
						throw new DuplicateInspectionRouteException("Duplicate route defined");
					}

					// loop over all the records and update the new route sequence
					logger.debug("updating the new route sequence");

					for (int i = 0; i < inspectorRecordArray.length; i++) {
						statement.setInt(1, Integer.parseInt(inspectorRecordArray[i].getRoute()));
						statement.setInt(2, Integer.parseInt(inspectorRecordArray[i].getInspectionId()));
						statement.executeUpdate();
					}

					logger.debug("The new inspection route sequence udpated successfully");
				}
			}

			return inspectorId;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in changeRouting method of InspectionAgent. Message-" + e.getMessage());
			throw e;
		} finally {
			try {
				if (statement != null) {
					statement.close();
				}

				if (connection != null) {
					connection.close();
				}
			} catch (Exception ignored) {
				logger.error("unable to close connection " + ignored.getMessage());
			}
		}
	}

	/**
	 * Function to return search results for Inspection
	 */
	public List searchInspections(String query) {
		List inspList = new ArrayList();
		Inspection inspection = null;
		String actnCode = "";
		String date = "";
		String actNumber = "";

		try {
			logger.debug(query);

			ResultSet rs = new Wrapper().select(query);

			while (rs.next()) {
				actnCode = LookupAgent.getActionCodeDesc(rs.getString("ACTN_CODE"));
				date = StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("INSPECTION_DT")));
				actNumber = LookupAgent.getActivityNumberForActivityId(rs.getString("ACT_ID"));
				inspection = new Inspection(rs.getString("INSPECTION_ID"), actNumber, rs.getString("INSPCT_ITEM_ID"), date, rs.getString("REQUEST_SOURCE"), actnCode);
				inspList.add(inspection);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error("Exception occured in method searchInspections of InspectionAgent." + e.getMessage());
		}

		return inspList;
	}

	/**
	 * Function to find a user attached to an activity
	 */
	public String getInspectorFromTerritoryMapping(String subProjectTypeId, String activityType, String apn) {
		logger.info("getInspectorFromTerritoryMapping(" + subProjectTypeId + "," + activityType + "," + apn + ")");

		Wrapper db = new Wrapper();
		RowSet rs = null;
		RowSet rs2 = null;
		String inspectorId = "-1";
		String sql = "";

		try {
			// first check the all addresses (0 territory) list to see if we can get results
			sql = "select * from lkup_ter_insp where sp_type_id=" + subProjectTypeId + " and act_type='" + activityType + "' and territory=0";
			logger.info(sql);
			rs = db.select(sql);

			if (rs.next()) {
				inspectorId = rs.getString("INSPECTOR_ID");
			} else {
				// this means that we didn't get any results in the 0 territory, we need to lookup using apn and get the inspector id from the territory mapping
				logger.debug("looking up the terrotory apn");
				sql = "select * from lkup_ter_insp where sp_type_id=" + subProjectTypeId + " and act_type='" + activityType + "' and territory=(select territory from lkup_ter_apn where apn='" + apn + "')";
				logger.info(sql);
				rs2 = db.select(sql);

				if (rs2.next()) {
					inspectorId = rs2.getString("INSPECTOR_ID");
				} else {
					logger.warn("inspector id not found in both the tables,setting to unassigned ");
				}
			}
		} catch (Exception e) {
			logger.error("Exception occured in getInspectorFromTerritoryMapping method of InspectionAgent. Message-" + e.getMessage());
		} finally {
			try {
				if (rs != null) {
					rs.close();
				}

				if (rs2 != null) {
					rs2.close();
				}
			} catch (Exception ignored) {
				// ignored
			}
		}

		return inspectorId;
	}

	public int getInspectionItemCodeCancel(String activityId) {
		int inspectionItemCode = 0;
		try {
			String sql = "select * from inspection where act_id = " + activityId + " and actn_code = " + Constants.INSPECTION_CODES_CANCELLED + " and ROWNUM = 1 order by timestamp desc";
			logger.debug("Inspection Item code SQL : " + sql);
			ResultSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				inspectionItemCode = rs.getInt("inspct_item_id");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionItemCode;
	}

	public String getInspectionCommentsForRequest(String activityId) {
		String inspectionItemCode = "";
		try {
			String sql = "select * from inspection where act_id = " + activityId + " and actn_code = " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION + " and ROWNUM = 1 order by timestamp desc";
			logger.debug("Inspection Item code SQL : " + sql);
			ResultSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				inspectionItemCode = rs.getString("COMMENTS");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionItemCode;
	}

	public String getInspectionCommentsForCancel(String activityId) {
		String inspectionItemCode = "";
		try {
			String sql = "select * from inspection where act_id = " + activityId + " and actn_code = " + Constants.INSPECTION_CODES_CANCELLED + " and ROWNUM = 1 order by timestamp desc ";
			logger.debug("Inspection Item code SQL : " + sql);
			ResultSet rs = new Wrapper().select(sql);
			if (rs.next()) {
				inspectionItemCode = rs.getString("COMMENTS");
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionItemCode;
	}

	public RowSet getHolidayList() throws Exception {
		String sql = "select * from lkup_insp_cal order by insp_cal_date desc";
		RowSet rs = new Wrapper().select(sql);

		return rs;
	}


	public RowSet getHolidayListBasedOnYear(String year) throws Exception {
		String sql = "SELECT * from lkup_insp_cal where insp_cal_date >=  to_date('01-01-"+year+"','DD-MM-YYYY') AND insp_cal_date <=  to_date('31-12-"+year+"','DD-MM-YYYY') order by insp_cal_date desc";
		logger.debug("sql.."+sql);
		RowSet rs = new Wrapper().select(sql);

		return rs;
	}

	public String getYearOnId(String holidayId) throws Exception {
		String sql = "select insp_cal_date from lkup_insp_cal where insp_cal_id="+holidayId+" order by insp_cal_date desc";
		logger.debug("sql.. "+sql);
		String year = "";
		RowSet rs = null;
		try {
			rs = new Wrapper().select(sql);
			if(rs.next()) {
				Date date = rs.getDate("insp_cal_date");
				SimpleDateFormat formatNowYear = new SimpleDateFormat("yyyy");
				year = formatNowYear.format(date);
			}
			logger.debug("year.."+year);
		}catch (Exception e) {
			logger.error(e);
		}finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
		return year;
	}

	public static HolidayEditorForm getHolidayList(String holidayId) throws Exception {
		String sql = "select * from lkup_insp_cal where insp_cal_id="+holidayId+" order by insp_cal_date desc";
		logger.debug("sql.. "+sql);
		HolidayEditorForm holidayEditorForm = new HolidayEditorForm();
		RowSet rs = null;
		
		try{
			rs=new Wrapper().select(sql);
		
			if(rs.next()) {
				holidayEditorForm.setDate(StringUtils.date2str(rs.getDate("insp_cal_date")));
				holidayEditorForm.setDescription(rs.getString("description"));
				holidayEditorForm.setId(rs.getString("insp_cal_id"));
			}
		}catch (Exception e) {
			logger.error(e);
		}finally {
			try {
				if (rs != null) {
					rs.close();
				}
			} catch (Exception e1) {
				logger.error("Exception occured while closing rs " + e1.getMessage());
			}
		}
		return holidayEditorForm;
	}


	public boolean findInspectionForEmail(int insId) throws Exception {

		boolean res = true;
		List inspectionList = null;

		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			inspectionList = new ArrayList();

			String sql = "select * from inspection where schedule_flag=1 and email_sent is null and INSPECTOR_ID =" + insId;

			logger.info(sql);
			rs = new Wrapper().select(sql);

			while (rs.next()) {
				Inspection inspection = new Inspection();
				inspection.setInspectionId(rs.getString("inspection_id"));
				inspection.setDate(StringUtils.cal2str(StringUtils.dbDate2cal(rs.getString("inspection_dt"))));
				inspection.setInspectorId(rs.getString("inspector_id"));
				inspection.setComments(rs.getString("comments"));
				// inspection.setInspectionItem(rs.getString("desc"));
				inspection.setActnCode(rs.getString("actn_code"));
				inspection.setActionCodeDescription(LookupAgent.getActionCodeDesc(rs.getString("actn_code")));
				inspection.setSource(rs.getString("request_source"));
				inspection.setInspectionItem(LookupAgent.getInspctItemDescFromId(rs.getString("INSPCT_ITEM_ID")));

				String start = "";
				String end = "";
				switch (StringUtils.s2i(rs.getString("SCHEDULE_START"))) {
				case 1:
					start = "8.00 a.m.";
					break;
				case 2:
					start = "9.00 a.m.";
					break;
				case 3:
					start = "10.00 a.m.";
					break;
				case 4:
					start = "11.00 a.m.";
					break;
				case 5:
					start = "12.00 a.m.";
					break;
				case 6:
					start = "1.00 p.m.";
					break;
				case 7:
					start = "2.00 p.m.";
					break;
				case 8:
					start = "3.00 p.m.";
					break;
				case 9:
					start = "4.00 p.m.";
					break;
				case 10:
					start = "5.00 p.m.";
					break;
				case 11:
					start = "6.00 p.m.";
					break;
				default:
					start = "";
				}

				switch (StringUtils.s2i(rs.getString("SCHEDULE_END"))) {
				case 1:
					end = "8.00 a.m.";
					break;
				case 2:
					end = "9.00 a.m.";
					break;
				case 3:
					end = "10.00 a.m.";
					break;
				case 4:
					end = "11.00 a.m.";
					break;
				case 5:
					end = "12.00 a.m.";
					break;
				case 6:
					end = "1.00 p.m.";
					break;
				case 7:
					end = "2.00 p.m.";
					break;
				case 8:
					end = "3.00 p.m.";
					break;
				case 9:
					end = "4.00 p.m.";
					break;
				case 10:
					end = "5.00 p.m.";
					break;
				case 11:
					end = "6.00 p.m.";
					break;
				default:
					end = "";
				}
				inspection.setSource(start + " - " + end);
				inspectionList.add(inspection);
			}

			rs.close();

			if (!inspectionList.isEmpty()) {
				for (int i = 0; i < inspectionList.size(); i++) {
					Inspection insp = (Inspection) inspectionList.get(i);
					sendEmail(insp);
					updateEmailSent(StringUtils.s2i(insp.getInspectionId()));
				}

			}

		} catch (SQLException sql) {
			logger.error("SQLException in getInspectionList method of InspectionAgent. Message-" + sql.getMessage());
			throw sql;
		} catch (Exception e) {
			logger.error("Exception occured in getInspectionList method of InspectionAgent. Message-" + e.getMessage());
			throw e;
		}
		logger.debug("returning");
		return res;
	}

	public void sendEmail(Inspection insp) throws Exception {
		String fromEmail = Wrapper.getResourceBundle().getString("EMAIL_FROM");
		try {
			// Send Email
			StringBuffer emailTo = new StringBuffer();

			StringBuffer nullEmail = new StringBuffer();

			boolean nullable = false;

			nullEmail.append("\n\n The following person email address are null.please update these Emails. \n \n");

			// send email of plan check status change here...

			List emailList = getEmailBlastforInspection(StringUtils.s2i(insp.getInspectionId()));

			if (!emailList.isEmpty()) {

				for (int i = 0; i < emailList.size(); i++) {

					People people = (People) emailList.get(i);

					// Check if email is null means send mail to webmaster saying people does not have email address
					// else send email to the concertned person

					if (people.getEmailAddress().equalsIgnoreCase(fromEmail)) {

						nullEmail.append("People ID: " + people.getPeopleId() + " \n Name: " + people.getName() + "  \n  Address:  " + people.getAddress() + "\n\n");

						nullable = true;
					} else {
						emailTo.append(people.getEmailAddress() + ";");
					}
				}
			}

			ResourceBundle obcProperties = Wrapper.getResourceBundle();

			String emailFlag = obcProperties.getString("EMAIL_FLAG");

			if (emailFlag.equalsIgnoreCase("off")) {
				emailTo = new StringBuffer();
				emailTo.append("aromero@beverlyhills.org;jdeanda@beverlyhills.org;");
			}

			// String subject = "Inspection Scheduled";
			String subject = new AdminAgent().getEmailSubject("INSPECTION_SCHEDULED");

			int actId = LookupAgent.getActivityIdFromInspectionId(StringUtils.s2i(insp.getInspectionId()));
			logger.debug("actId is" + actId);
			HashMap hm = new HashMap();

			hm.put("PERMITNUMBER", new ActivityAgent().getActivtiyNumber(actId));
			hm.put("PERMITTYPE", LookupAgent.getActivityType(LookupAgent.getActivityTypeForActId(actId + "")).getDescription());
			hm.put("ADDRESS", LookupAgent.getActivityAddress(actId)[0]);
			hm.put("STATUS", insp.getActionCodeDescription());
			hm.put("TYPE", insp.getInspectionItem());
			hm.put("DATE", insp.getDate());
			hm.put("TIME", insp.getSource());

			// Permit Number: "+new ActivityAgent().getActivtiyNumber(StringUtils.s2i(insp.getInspectionId()))+"\n Inspection Status : "+insp.getActionCodeDescription()+"\n Inspection Type :"+insp.getInspectionItem()+"\n");

			logger.debug("&&&&&&&&&" + new AdminAgent().getEmailMessage("INSPECTION_SCHEDULED"));

			String message = StringUtils.parseString(new AdminAgent().getEmailMessage("INSPECTION_SCHEDULED"), hm);
			// StringBuffer message = new StringBuffer("\n\n"+new AdminAgent().getEmailMessage("INSPECTION_SCHEDULED")+"\n\n");

			// message.append("\n Permit Number: "+new ActivityAgent().getActivtiyNumber(StringUtils.s2i(insp.getInspectionId()))+"\n Inspection Status :  "+insp.getActionCodeDescription()+"\n Inspection Type :"+insp.getInspectionItem()+"\n");

			// "\n Inspection Date: " +insp.getDate()
			new MessageUtils().sendEmail(emailTo.toString(), subject, message.toString());

			String subject1 = "Email not sent because Email address is null";

		}

		catch (Exception e) {

			logger.debug("Exception Occured while sending Email" + e.getMessage());
			throw e;
		}
	}

	public void updateEmailSent(int insId) throws Exception {

		List inspectionList = null;

		RowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			inspectionList = new ArrayList();

			String sql = "update Inspection set EMAIL_SENT = 'Y' where inspection_id = " + insId;

			logger.info(sql);

			new Wrapper().update(sql);

		} catch (Exception e) {
			logger.debug("Exception occured" + e.getMessage());
			throw e;
		}
	}

	public List getEmailBlastforInspection(int inspId) throws Exception {
		String fromEmail = Wrapper.getResourceBundle().getString("EMAIL_FROM");
		List emailAddressList = new ArrayList();

		try {
			People people = null;

			Wrapper db = new Wrapper();

			String sql = "select email_addr ,people_id , people_type_id ,name ,addr ,city, state ,zip  from people where people_id in (select people_id from activity_people where act_id = ( select act_id from Inspection where INSPECTION_ID=" + inspId + "))  and EMAIL_NOTIFICATION='Y'";

			logger.debug("The getEmailBlastforInspection is " + sql);

			RowSet rs = new Wrapper().select(sql);

			while (rs.next()) {

				people = new People();
				if (rs.getString("email_addr") != null) {
					logger.debug("Email address not  null");
					people.setEmailAddress(rs.getString("email_addr"));
				} else {
					logger.debug("Email address null");
					people.setEmailAddress(fromEmail);
				}
				people.setPeopleId(rs.getInt("people_id"));
				people.setAddress(rs.getString("addr") + ", " + rs.getString("city") + ", " + rs.getString("state") + "-" + rs.getString("zip"));
				people.setPeopleId(rs.getInt("people_type_id"));
				people.setName(rs.getString("name"));

				emailAddressList.add(people);
			}

			rs.close();

		} catch (Exception e) {
			logger.error("Exception thrown while trying to update PlanCheck  :" + e.getMessage());
		}

		return emailAddressList;
	}

	public String[] getActivityId(String activityNumber) throws Exception {
		logger.debug("Entering  getActivityId with activityNumber" + activityNumber);

		String[] actitvity = new String[3];

		String actId = "";

		try {
			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select * from activity where act_nbr= upper(" + StringUtils.checkString(activityNumber) + ")");

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			if ((rs != null) && rs.next()) {
				actitvity[0] = rs.getString("act_id");
				actitvity[1] = rs.getString("ACT_TYPE");

				String status = this.getStatus(rs.getInt("status"));
				actitvity[2] = status;
			}

			logger.debug("The act  id is " + actitvity[0]);
			logger.debug("The act  type is " + actitvity[1]);
			logger.debug("The act  Status is " + actitvity[2]);
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting activity ID" + e.getMessage());
		}

		return actitvity;
	}

	private String getStatus(int statusId) throws Exception {
		logger.debug("Entering  getStatus with statusId" + statusId);

		String status = "";

		try {
			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select * from lkup_act_st where status_id=" + statusId);

			logger.debug(sql.toString());

			RowSet rs1 = db.select(sql.toString());

			if ((rs1 != null) && rs1.next()) {
				status = rs1.getString("description");
			}
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting getStatus " + e.getMessage());
		}

		return status;
	}

	public List fetchInspectionResults(String activityNbr, String inspDate, String inspectionCode) throws Exception {
		logger.debug("Entering  fetchInspectionResults with activityNumber: " + activityNbr + "inspDate" + inspDate + "inspectionCode " + inspectionCode);

		try {
			List inspectionResults = new ArrayList();

			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select i.inspection_dt, i.inspection_id ,i.act_id ,i.Inspector_id,i.request_source,i.comments,i.schedule_start," + "i.schedule_end, lic.actn_code ,lic.description ,lii.INSPECTION_ITEM_CODE , lii.description as description1 ,i.TIMESTAMP  from   inspection i ,  lkup_inspct_code lic , LKUP_INSPCT_ITEM lii " + " where i.actn_code =lic.actn_code and  i.INSPCT_ITEM_ID = lii.INSPCT_ITEM_ID");

			if ((activityNbr != null) && (!activityNbr.equals(""))) {
				sql.append(" and  act_id =" + getActivityId(activityNbr)[0]);
			}

			if ((inspDate != null) && (!inspDate.equals(""))) {
				sql.append(" and i.inspection_dt= to_date(" + StringUtils.checkString(inspDate) + ",'MM/DD/YYYY')");
			}

			if ((inspectionCode != null) && (!inspectionCode.equals(""))) {

				// sql.append(" and i.INSPCT_ITEM_ID= "+inspectionCode );
				// sql.append(" and i.INSPCT_ITEM_id= (select  INSPCT_ITEM_CODE  from  LKUP_INSPCT_ITEM  where INSPECTION_ITEM_ID =" +
				// inspectionCode + ")");

			}

			sql.append("order by i.inspection_dt desc,i.TIMESTAMP desc");

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			// logger.debug("The size is "+rs.isFirst());
			if ((rs != null) && (rs.isFirst())) {
				return null;
			}

			while (rs.next()) {
				Inspection insp = new Inspection();
				String permitNumber = new ActivityAgent().getActivtiyNumber(rs.getInt("act_id"));
				logger.debug("!!!!1permitNumber" + permitNumber);
				insp.setActivityId(permitNumber);
				insp.setSource(permitNumber);
				insp.setActnCode(rs.getString("actn_code"));
				insp.setActionCodeDescription(rs.getString("description"));
				// insp.setActivityId(rs.getString("act_id"));
				insp.setInspectionId(rs.getString("inspection_id"));
				insp.setInspectionItem(rs.getString("INSPECTION_ITEM_CODE") + "-" + rs.getString("description1"));
				insp.setInspectionType("");
				insp.setInspectorId(rs.getString("Inspector_id"));
				insp.setSource(rs.getString("request_source"));
				insp.setDate(StringUtils.changeDateFormate(rs.getString("inspection_dt")));
				insp.setComments(rs.getString("comments"));

				inspectionResults.add(insp);
			}

			rs.close();

			// logger.debug("Size "+inspectionResults.size());
			return inspectionResults;
		} catch (Exception e) {
			logger.debug("Exception ocuured while fetching Inspection resluts" + e.getMessage());
			throw e;
		}
	}

	public boolean cancelInspectionResults(String activityNbr, String inspDate, String inspectionCode) throws Exception {
		logger.debug("Entering  cancelInspectionResults with activityNumber: " + activityNbr + "inspDate" + inspDate);

		boolean result = false;
		
		int deptId = LookupAgent.getDepartmentId((activityNbr).substring(0,2 ));
        

		result = checkOneInspectionPerPermitPerDay(getActivityId(activityNbr)[0], inspDate);

		if (result == false) {
			return result;
		}
		

		try {
			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("update inspection i set ACTN_CODE = (select  ACTN_CODE  from LKUP_INSPCT_CODE where dept_id="+deptId+" and upper(description) = upper('" + Constants.CANCELLED + "'))  where ");

			if ((activityNbr != null) && (!activityNbr.equals(""))) {
				sql.append("  act_id =" + getActivityId(activityNbr)[0]);
			}

			if ((inspDate != null) && (!inspDate.equals(""))) {
				sql.append(" and i.inspection_dt= to_date(" + StringUtils.checkString(inspDate) + ",'YYYY-MM-DD')");
			}

			if ((inspectionCode != null) && (!inspectionCode.equals(""))) {
				sql.append(" and i.INSPCT_ITEM_ID=(select  INSPCT_ITEM_ID  from  LKUP_INSPCT_ITEM  where INSPECTION_ITEM_CODE =" + inspectionCode + ")");
			}

			logger.debug(sql.toString());

			int i = db.update(sql.toString());

			logger.debug("The inspection is Canceled" + i);

			if (i > 0) {
				result = true;
			}

			logger.debug("The inspection is Canceled" + result);

			return result;
		} catch (Exception e) {
			logger.debug("Exception ocuured while cancelInspectionResults" + e.getMessage());
			throw e;
		}
	}

	public boolean checkOneInspectionPerPermitPerDay(String actId, String inspectionDate) throws Exception {
		boolean result = false;

		logger.debug("Entering  checkOneInspectionPerPermitPerDay with actId: " + actId + "inspectionDate " + inspectionDate);
		int deptId = LookupAgent.getDepartmentId((LookupAgent.getActivityNumberForActivityId(actId)).substring(0,2 ));
         
		try {
			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select  count(*)  as count  from  inspection  where INSPECTION_DT= to_date(" + StringUtils.checkString(inspectionDate) + ",'YYYY-MM-DD')" + " and actn_code =(select  ACTN_CODE  from LKUP_INSPCT_CODE where dept_id="+deptId+" and upper(description) = upper('" + Constants.REQUEST_FOR_INSPECTION + "')) and act_id =" + actId);

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			if ((rs != null) && rs.next()) {
				int count = rs.getInt("count");

				if (count >= 1) {
					result = true;
				}
			}

			logger.debug("The checkOneInspectionPerPermitPerDay is " + result);

			rs.close();
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting checkOneInspectionPerPermitPerDay" + e.getMessage());
		}

		return result;
	}

	public boolean checkOneInspectionCodePerDay(String inspectionCode, String inspectionDate, String actId) throws Exception {
		boolean result = false;

		logger.debug("Entering  checkOneInspectionCodePerDay with inspectionCode: " + StringUtils.checkString(inspectionCode) + "inspectionDate " + StringUtils.checkString(inspectionDate));
             int deptId = LookupAgent.getDepartmentId((LookupAgent.getActivityNumberForActivityId(actId)).substring(0,2 ));
             
		try {
			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select  count ( * ) as count from  inspection i  , LKUP_INSPCT_ITEM  lii  where lii. INSPCT_ITEM_ID  = i.INSPCT_ITEM_ID  and actn_code =(select  ACTN_CODE  from LKUP_INSPCT_CODE where dept_id="+deptId+ " and upper(description) = upper('" + Constants.REQUEST_FOR_INSPECTION + "') ) and INSPECTION_DT= to_date(" + StringUtils.checkString(inspectionDate) + ",'YYYY-MM-DD')" + " and i.act_id =" + actId);
			sql.append(" and i.INSPCT_ITEM_ID =" + inspectionCode);

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			if ((rs != null) && rs.next()) {
				int count = rs.getInt("count");

				if (count >= 1) {
					result = true;
				}
			}

			logger.debug("The checkOneInspectionCodePerDay is " + result);

			rs.close();
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting checkOneInspectionCodePerDay" + e.getMessage());
		}

		return result;
	}

	public String[] isNoScheduleWindow(Calendar cal) throws Exception {
		logger.debug("Entering  isNoScheduleWindow with calendar: ");

		String[] result = new String[3];

		result[0] = "false";

		String timeStart = null;

		String timeEnd = null;

		try {
			int day = cal.get(cal.DAY_OF_WEEK);

			// gives 1-7 means sunday through saturday
			if ((day == 1) || (day == 7)) {
				// trying to schedule some Inspection on Saturday or sunday
				return result;
			}

			logger.debug("Day is " + day);

			String[] Days = { "", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY" };

			String today = Days[day];

			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select *  from lkup_insp_time where insp_time_day='SCHEDULE_" + today + "_START' union select *  from lkup_insp_time where insp_time_day='SCHEDULE_" + today + "_END'");

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			while (rs.next()) {
				if (("SCHEDULE_" + today + "_START").equalsIgnoreCase(rs.getString("INSP_TIME_DAY"))) {
					timeStart = rs.getString("INSP_TIME_VALUE");
				} else {
					timeEnd = rs.getString("INSP_TIME_VALUE");
				}
			}

			logger.debug("The timeStart is " + timeStart);

			logger.debug("The timeEnd is " + timeEnd);

			rs.close();

			int timeStartInt = timeStart != null ? StringUtils.s2i(timeStart.replace(':', ' ')) : 0;
			int timeEndInt = timeEnd != null ? StringUtils.s2i(timeEnd.replace(':', ' ')) : 0;

			int[] dateTime = getCurrentDateAndTime(cal);

			int date = 0;
			int time = 0;

			if (dateTime != null) {
				date = dateTime[0];
				time = dateTime[1];
			}

			logger.debug("date is " + date);

			logger.debug("time is " + time);

			logger.debug("time" + time + " > " + timeStartInt + "&& time" + time + "< end" + timeStartInt);

			if ((time > timeStartInt) && (time < timeEndInt)) {
				result[0] = "" + true;
				result[1] = "" + timeEndInt;
			}
		} catch (Exception e) {
			logger.debug("Exception ocuured in isNoScheduleWindow" + e.getMessage());
		}

		return result;
	}

	public static int[] getCurrentDateAndTime(Calendar c1) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd-HH-mm-SS");

		String currentDateTime = new String(formatter1.format(c1.getTime()));

		logger.debug(" Entering getCurrentDateAndTime with calendar " + currentDateTime);

		String currentTime = currentDateTime.substring(11, 16);

		String currentDate = currentDateTime.substring(0, 10);

		int currentDateInt = StringUtils.s2i(currentDate.replace('-', ' '));

		int currentTimeInt = StringUtils.s2i(currentTime.replace('-', ' '));
		logger.debug("currentTimeInt" + currentTimeInt);

		int[] dateTime = new int[4];
		dateTime[0] = currentDateInt;
		dateTime[1] = currentTimeInt;

		return dateTime;
	}

	public String getNextInspectionDate(Calendar cal) throws Exception {
		try {
			boolean holiday = false;
			String nextInspectionDate = null;
			int[] dateTime = getCurrentDateAndTime(cal);

			int date = dateTime[0];

			int time = dateTime[1];

			logger.debug("date is " + date);

			logger.debug("time is " + time);

			int schecduleCutoffInt = isScheduleCutoff(cal);

			logger.debug("schecduleCutoff is " + schecduleCutoffInt);

			int day = cal.get(cal.DAY_OF_WEEK);

			logger.debug("Compare" + time + " > " + schecduleCutoffInt);

			if (time > schecduleCutoffInt) {
				// check whether time exceeded for scheduling today.
				// true tomorrow else today
				// calendar gives 1-7 means sunday - saturday
				if (day == 6) { // means friday add 3 more days to make monday
					cal.add(cal.DATE, 3);
				} else if (day == 7) {
					// means Saturday add 2 more days to make monday
					cal.add(cal.DATE, 2);
				} else {
					cal.add(cal.DATE, 1);
				}
			} else {
				// calendar gives 1-7 means sunday - saturday
				if (day == 7) {
					// means Saturday add 2 more days to make monday
					cal.add(cal.DATE, 2);
				}
			}

			logger.debug("Day is " + cal.get(cal.DAY_OF_WEEK));

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			nextInspectionDate = new String(formatter.format(cal.getTime()));

			logger.debug(nextInspectionDate);

			RowSet rowset = null;// new InspectionAgent().getHolidayList();

			int nextInspectionDateInt = StringUtils.s2i(nextInspectionDate.replace('-', ' '));

			while ((rowset != null) && rowset.next()) {
				// Gets the holiday list from the holiday Editor
				String holidayDate = rowset.getString("INSP_CAL_DATE");

				// Converts into Int from String for comparison
				int holidayDateInt = StringUtils.s2i(holidayDate.replace('-', ' '));

				logger.debug("holidayDateInt : " + holidayDateInt);

				logger.debug("nextInspectionDateInt : " + nextInspectionDateInt);

				if (holidayDateInt == nextInspectionDateInt) {
					holiday = true;
					rowset.close();
				}
			}
			if (rowset != null)
				rowset.close();

			if (holiday) {
				// cal.add(cal.DATE, 1);
				getNextInspectionDate(cal);

				// If(holiday) calls the same method and iterate through it to fine the next business day
			}

			logger.debug("The Date is " + nextInspectionDate);

			return nextInspectionDate;
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting getNextInspectionDate" + e.getMessage());
			throw e;
		}
	}

	public int isScheduleCutoff(Calendar c1) throws Exception {
		// logger.debug("Entering  isScheduleCutoff with inspectionDate: "+StringUtils.checkString(inspectionDate));
		String scheduleCutoff = null;

		int scheduleCutoffInt = -2;

		try {
			// Find out current time and Date for Schedule
			int currentDateInt = getCurrentDateAndTime(c1)[0];

			int currentTimeInt = getCurrentDateAndTime(c1)[0];

			int day = c1.get(c1.DAY_OF_WEEK);

			// gives 1-7 means sunday through saturday
			logger.debug("currentTime is: " + currentTimeInt);

			logger.debug("currentTime is: " + currentDateInt);

			if ((day == 1) || (day == 7)) {
				// trying to schedule some Inspection on Saturday or sunday
				return -1;
			}

			logger.debug("Day is " + day);

			String[] Days = { "", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY" };

			String today = Days[day];

			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select *  from lkup_insp_time where insp_time_day='CUTOFF_" + today + "_SCHEDULE'");

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			while (rs.next()) {
				scheduleCutoff = rs.getString("INSP_TIME_VALUE");
			}

			logger.debug("The scheduleCutoff is " + scheduleCutoff);

			rs.close();
			if (scheduleCutoff != null)
				scheduleCutoffInt = StringUtils.s2i(scheduleCutoff.replace(':', ' '));

			logger.debug("scheduleCutoffInt is: " + scheduleCutoffInt);
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting isScheduleCutoff" + e.getMessage());
		}

		return scheduleCutoffInt;
	}

	public int isCancelCutoff(Calendar c1) throws Exception {
		// logger.debug("Entering  isCancelCutooff with inspectionDate: "+StringUtils.checkString(inspectionDate));
		String cancelCutoff = null;

		int cancelCutoffInt = -2;

		try {
			// Find out current time and Date for Schedule
			int currentDateInt = getCurrentDateAndTime(c1)[0];

			int currentTimeInt = getCurrentDateAndTime(c1)[0];

			int day = c1.get(c1.DAY_OF_WEEK);

			// gives 1-7 means sunday through saturday
			logger.debug("currentTime is: " + currentTimeInt);

			logger.debug("currentTime is: " + currentDateInt);

			if ((day == 1) || (day == 7)) {
				// trying to schedule some Inspection on Saturday or sunday
				return -1;
			}

			logger.debug("Day is " + day);

			String[] Days = { "", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY" };

			String today = Days[day];

			Wrapper db = new Wrapper();

			StringBuffer sql = new StringBuffer();

			sql.append("select *  from lkup_insp_time where insp_time_day='CUTOFF_" + today + "_CANCEL'");

			logger.debug(sql.toString());

			ResultSet rs = db.select(sql.toString());

			while (rs.next()) {
				cancelCutoff = rs.getString("INSP_TIME_VALUE");
			}

			logger.debug("The scheduleCutoff is " + cancelCutoff);

			rs.close();
			if (cancelCutoff != null)
				cancelCutoffInt = StringUtils.s2i(cancelCutoff.replace(':', ' '));

			logger.debug("scheduleCutoffInt is: " + cancelCutoffInt);
		} catch (Exception e) {
			logger.debug("Exception ocuured while getting isScheduleCutoff" + e.getMessage());
		}

		return cancelCutoffInt;
	}

	public String getNextInspectionDateForCancel(Calendar cal) throws Exception {
		try {
			boolean holiday = false;
			String nextInspectionDate = null;
			int[] dateTime = getCurrentDateAndTime(cal);

			int date = dateTime[0];

			int time = dateTime[1];

			logger.debug("date is " + date);

			logger.debug("time is " + time);

			int cancelCutoffInt = isCancelCutoff(cal);

			logger.debug("schecduleCutoff is " + cancelCutoffInt);

			int day = cal.get(cal.DAY_OF_WEEK);

			logger.debug("Compare" + time + " > " + cancelCutoffInt);

			if (time > cancelCutoffInt) {
				// check whether time exceeded for scheduling today.
				// true tomorrow else today
				// calendar gives 1-7 means sunday - saturday
				if (day == 6) { // means friday add 3 more days to make monday
					cal.add(cal.DATE, 3);
				} else if (day == 7) {
					// means Saturday add 2 more days to make monday
					cal.add(cal.DATE, 2);
				} else {
					cal.add(cal.DATE, 1);
				}
			} else {
				// calendar gives 1-7 means sunday - saturday
				if (day == 7) {
					// means Saturday add 2 more days to make monday
					cal.add(cal.DATE, 2);
				}
			}

			logger.debug("Day is " + cal.get(cal.DAY_OF_WEEK));

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

			nextInspectionDate = new String(formatter.format(cal.getTime()));

			logger.debug(nextInspectionDate);

			RowSet rowset = null;// new InspectionAgent().getHolidayList();

			int nextInspectionDateInt = StringUtils.s2i(nextInspectionDate.replace('-', ' '));

			while ((rowset != null) && rowset.next()) {
				// Gets the holiday list from the holiday Editor
				String holidayDate = rowset.getString("INSP_CAL_DATE");

				// Converts into Int from String for comparison
				int holidayDateInt = StringUtils.s2i(holidayDate.replace('-', ' '));

				logger.debug("holidayDateInt : " + holidayDateInt);

				logger.debug("nextInspectionDateInt : " + nextInspectionDateInt);

				if (holidayDateInt == nextInspectionDateInt) {
					holiday = true;
					rowset.close();
				}
			}
			if (rowset != null)
				rowset.close();

			if (holiday) {
				// cal.add(cal.DATE, 1);
				getNextInspectionDateForCancel(cal);

				// If(holiday) calls the same method and iterate through it to fine the next business day
			}

			logger.debug("The Date is " + nextInspectionDate);

			return nextInspectionDate;
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Exception ocuured while getting getNextInspectionDate" + e.getMessage());
			throw e;
		}
	}

	public List getInspectionItemCodeForInspectionCode(String inspectionCode) {
		List inspectionItemCodeList = new ArrayList();
		InspectionItem inspectionItem = null;

		try {
			String sql = "select inspct_item_id, char(inspection_item_code)  || ' - ' || description as description from lkup_inspct_item where INSPCT_ITEM_ID=" + inspectionCode + " order by inspection_item_code";
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				inspectionItem = new InspectionItem(rs.getInt("inspct_item_id"), rs.getString("description"));
				inspectionItemCodeList.add(inspectionItem);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return inspectionItemCodeList;
	}

	// Required for Online Inspection, - Request, Result and cancellation of Inspection
	public List getMyPermits(String emailAddr) {
		List actList = new ArrayList();
		Activity activity = null;

		// inspectionItemCodeList.add(new InspectionItem(-1, "Please Select"));
		try {
			String sql =

			"select distinct(ac.act_id), lat.description as type,pp.proj_id,  pp.project_nbr,  ac.description,sp.sproj_nbr,sp.sproj_id , ac.ISSUED_DATE , vaa.ADDRESS   ,ac.act_nbr, p.email_addr, las.STAT_CODE    from  people p   join  ACTIVITY_PEOPLE  a  on    a.people_id=p.people_id    join activity ac on ac.act_id = a.act_id join  LKUP_ACT_ST  las  on las.status_id  =ac.status join sub_project sp on sp.sproj_id = ac.sproj_id  join project pp on pp.proj_id = sp.proj_id  join v_activity_address vaa on  vaa.act_id = ac.act_id  join lkup_act_type lat on lat.type = ac.act_type  and  lower(p.email_addr) = lower('" + emailAddr + "')  AND NOT AC.ACT_TYPE LIKE 'PW%' AND NOT AC.ACT_TYPE LIKE 'DOT%' and a.psa_type in ('A','C') order by  pp.proj_id,   vaa.ADDRESS  asc,ac.act_nbr asc, ac.ISSUED_DATE desc";

			// "select distinct sp.sproj_id ,pp.proj_id,  p.name,  vaa.ADDRESS  ,p.city, p.state, p.zip, p.email_addr, p.comnts, ac.*,las.STAT_CODE    from  people p   join  ACTIVITY_PEOPLE  a  on    a.people_id=p.people_id    join activity ac on ac.act_id = a.act_id join  LKUP_ACT_ST  las  on las.status_id  =ac.status "+
			// "join sub_project sp on sp.sproj_id = ac.sproj_id  join project pp on pp.proj_id = sp.proj_id "+
			// " join v_activity_address vaa on  vaa.act_id = ac.act_id "+
			// "and  p.email_addr='"+emailAddr+"' AND NOT AC.ACT_TYPE LIKE 'PW%' AND NOT AC.ACT_TYPE LIKE 'DOT%' and a.psa_type in ('A','C') order by pp. proj_id asc,   vaa.ADDRESS  asc,ac.act_nbr asc, ac.ISSUED_DATE desc ";
			logger.debug(sql);

			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				activity = new Activity();
				activity.setActivityId(rs.getInt("act_id"));

				activity.setSubProjectId(StringUtils.s2i(rs.getString("SPROJ_NBR")));

				ActivityDetail actDetail = new ActivityDetail();
				if (rs.getString("ISSUED_DATE") != null) {

					String date = rs.getString("ISSUED_DATE");

					StringBuffer date12 = new StringBuffer(date);
					logger.debug("The date is " + date12);

					String date1 = date12.substring(5, 7) + "/" + date12.substring(8, 10) + "/" + date12.substring(0, 4);
					logger.debug("The date is " + date1);
					actDetail.setInspectionRequired(date1);

				} else {
					actDetail.setInspectionRequired("");
				}
				actDetail.setActivityNumber(rs.getString("ACT_NBR"));
				actDetail.setStatus(new ActivityStatus(0, "", rs.getString("stat_code")));
				actDetail.setActivityType(new ActivityType(rs.getString("type"), rs.getString("description")));
				activity.setActivityDetail(actDetail);
				// actDetail.setAddress(rs.getString("addr")+" "+rs.getString("city")+" "+rs.getString("state")+" "+rs.getString("zip"));
				String add = (rs.getString("ADDRESS") != null) ? rs.getString("ADDRESS").replace(',', ' ') : "";
				actDetail.setAddress(add);
				actDetail.setMicrofilm(rs.getString("PROJECT_NBR"));
				actList.add(activity);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return actList;
	}

	// added by Sunil
	public List getMyPermits(String emailAddr, String action) {
		List actList = new ArrayList();
		logger.debug("getMyPermits( Entered");
		logger.debug("action " + action);
		MyPermitForm myPermitForm = null;
		Activity activity = null;
		// inspectionItemCodeList.add(new InspectionItem(-1, "Please Select"));
		try {
			String sql =

			"select A.SPROJ_ID,A.ACT_ID,A.ACT_NBR,P.Agent_name,LAT.description as Type,LAS.description as Status,A.APPLIED_DATE,LA.STR_NO ,LA.STR_MOD,SL.PRE_DIR,SL.STR_NAME,SL.STR_TYPE,LA.CITY,LA.STATE,LA.ZIP from Activity A " + " Left outer join ACTIVITY_PEOPLE AP on A.ACT_ID=AP.ACT_ID " + " Left outer join People P on P.People_ID= Ap.PEople_ID " + " Left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE " + " Left outer join Lkup_ACT_ST LAS on LAS.STATUS_ID=A.status " + " LEFT outer join LSO_ADDRESS LA on LA.ADDR_ID=A.ADDR_ID " + " Left outer join Street_list SL on SL.STREET_ID=LA.Street_ID " + " where P.email_addr = " + StringUtils.checkString(emailAddr);
			logger.debug(sql);

			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {

				activity = new Activity();
				activity.setActivityId(rs.getInt("act_id"));

				// activity.setSubProjectId(StringUtils.s2i(rs.getString("SPROJ_NBR")));

				ActivityDetail actDetail = new ActivityDetail();
				if (rs.getString("APPLIED_DATE") != null) {

					String date = rs.getString("APPLIED_DATE");

					StringBuffer date12 = new StringBuffer(date);
					logger.debug("The date is " + date12);

					String date1 = date12.substring(5, 7) + "/" + date12.substring(8, 10) + "/" + date12.substring(0, 4);
					logger.debug("The date is " + date1);
					actDetail.setInspectionRequired(date1);

				} else {
					actDetail.setInspectionRequired("");
				}
				actDetail.setActivityNumber(rs.getString("ACT_NBR"));
				actDetail.setStatus(new ActivityStatus(0, "", rs.getString("STATUS")));
				// actDetail.setActivityType(new ActivityType(rs.getString("type"),rs.getString("desc")));
				// activity.setActivityDetail(actDetail);
				// actDetail.setAddress(rs.getString("addr")+" "+rs.getString("city")+" "+rs.getString("state")+" "+rs.getString("zip"));
				// String add = (rs.getString("ADDRESS")!=null)? rs.getString("ADDRESS").replace(',',' '):"";
				// actDetail.setAddress(add);
				// actDetail.setMicrofilm(rs.getString("PROJECT_NBR"));

				// myPermitForm = new MyPermitForm();

				// myPermitForm.setActivityNo(rs.getString("act_nbr"));

				actDetail.setActivityType(new ActivityType(rs.getString("TYPE"), rs.getString("TYPE")));
				activity.setActivityDetail(actDetail);

				actDetail.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("PRE_DIR")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_NAME")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_Type")));
				// myPermitForm.setFullAddress(rs.getString("SPROJ_NBR"));
				myPermitForm = new MyPermitForm();
				myPermitForm.setActivityId(rs.getString("act_id"));
				myPermitForm.setSprojId(rs.getString("sproj_id"));
				myPermitForm.setActivityNo(rs.getString("act_nbr"));

				myPermitForm.setActivityType(rs.getString("TYPE"));

				myPermitForm.setActivityStatus(rs.getString("STATUS"));
				myPermitForm.setActivityDate(rs.getString("APPLIED_DATE"));

				myPermitForm.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("PRE_DIR")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_NAME")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_Type")));

				if (action.equalsIgnoreCase("view")) {
					actList.add(myPermitForm);
				} else {
					actList.add(activity);
				}
				//
			}

			if (rs != null) {
				rs.close();
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return actList;
	}

	/**
	 * 
	 * @param emailAddr
	 * @param comboNumber
	 * @return List of Activities mapped to a particular Combo Number
	 */
	public List getComboActList(String emailAddr,String inActive,int lsoId) {
		List actList = new ArrayList();
		logger.debug("getMyPermits( Entered");
		MyPermitForm myPermitForm = null;
		ApplyOnlinePermitForm applyOnlinePermitForm = null;
		Activity activity = null;
		try {
			/*String sql = "select distinct(A.ACT_ID),A.SPROJ_ID,A.ACT_TYPE,A.ACT_NBR,A.STATUS as act_stat,P.Agent_name,LAT.description as Type,LAS.description as Status,A.APPLIED_DATE,LA.STR_NO ,LA.STR_MOD,SL.PRE_DIR,SL.STR_NAME,SL.STR_TYPE,LA.CITY,LA.STATE,LA.ZIP,AST.ACT_SUBTYPE_ID from Activity A " + " Left outer join ACT_SUBTYPE AST on A.ACT_ID=AST.ACT_ID and AST.ACT_SUBTYPE_ID > 0 " + " Left outer join ACTIVITY_PEOPLE AP on A.ACT_ID=AP.ACT_ID " + " Left outer join People P on P.People_ID= Ap.PEople_ID " + " Left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE " + " Left outer join Lkup_ACT_ST LAS on LAS.STATUS_ID=A.status " + " LEFT outer join LSO_ADDRESS LA on LA.ADDR_ID=A.ADDR_ID " + " Left outer join Street_list SL on SL.STREET_ID=LA.Street_ID " + " Left outer join SUB_PROJECT SP  on A.SPROJ_ID = SP.SPROJ_ID where ";
			// if(comboNumber!=null){
			if (stNo != null)
				sql += " ( LA.STR_NO = " + StringUtils.checkString(stNo);
			if (pre != null)
				sql += " and SL.PRE_DIR = " + StringUtils.checkString(pre);
			if (stName != null)
				sql += " and SL.STR_NAME = " + StringUtils.checkString(stName);
			if (typ != null)
				sql += " and SL.STR_TYPE = " + StringUtils.checkString(typ);
			sql += " ) and ";
			// }
			
			
			
			if (inActive.endsWith("No")) {
				sql += " a.status not in " + actStatus + " and  ";
			} else {
				sql += " a.status in " + actStatus + " and  ";
			}
			sql += " lower(P.email_addr) = " + StringUtils.checkString(emailAddr.toLowerCase()) + "  order by A.ACT_ID desc";*/
			
			StringBuilder sb = new StringBuilder();
			sb.append(" select distinct(A.ACT_ID),A.SPROJ_ID,A.ACT_TYPE,A.ACT_NBR,A.STATUS as act_stat, ");
			sb.append(" P.Agent_name,LAT.description as Type,LAS.description as Status,A.APPLIED_DATE,LA.STR_NO ,LA.STR_MOD,SL.PRE_DIR,SL.STR_NAME,SL.STR_TYPE,LA.CITY,LA.STATE,  ");
			sb.append(" LA.ZIP,AST.ACT_SUBTYPE_ID, LAS.ONLINE_PRINT from Activity A  Left outer join ACT_SUBTYPE AST on A.ACT_ID=AST.ACT_ID and AST.ACT_SUBTYPE_ID > 0   ");
			sb.append(" Left outer join ACTIVITY_PEOPLE AP on A.ACT_ID=AP.ACT_ID   ");
			sb.append(" Left outer join People P on P.People_ID= Ap.PEople_ID   ");
			sb.append(" Left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE  ");
			sb.append(" Left outer join Lkup_ACT_ST LAS on LAS.STATUS_ID=A.status  ");
			sb.append(" LEFT outer join LSO_ADDRESS LA on LA.ADDR_ID=A.ADDR_ID   ");
			sb.append(" Left outer join Street_list SL on SL.STREET_ID=LA.Street_ID  ");
			sb.append(" Left outer join SUB_PROJECT SP  on A.SPROJ_ID = SP.SPROJ_ID where  LA.LSO_ID=").append(lsoId).append("   ");
			sb.append(" and   lower(P.email_addr) = ").append(StringUtils.checkString(emailAddr.toLowerCase())).append("     ");
			
			if (inActive.endsWith("No")) {
				sb.append(" and LAS.PSA_ACTIVE='N'  ");
			}else{
				sb.append(" and LAS.PSA_ACTIVE='Y'  ");	
			}
			sb.append(" order by A.ACT_ID desc  ");	
			logger.debug(sb.toString());
			
			
			
			

			ResultSet rs = new Wrapper().select(sb.toString());
			while (rs.next()) {
				myPermitForm = new MyPermitForm();
				myPermitForm.setActivityId(rs.getString("act_id"));
				myPermitForm.setSprojId(rs.getString("sproj_id"));
				myPermitForm.setActivityNo(rs.getString("act_nbr"));
				myPermitForm.setActivityType(rs.getString("TYPE"));
				myPermitForm.setActivityStatus(rs.getString("STATUS"));
				myPermitForm.setActivityDate(rs.getString("APPLIED_DATE"));
				myPermitForm.setActivityTypeCode(rs.getString("ACT_TYPE"));
				myPermitForm.setAddress(StringUtils.nullReplaceWithEmpty(rs.getString("STR_NO")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_MOD")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("PRE_DIR")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_NAME")) + " " + StringUtils.nullReplaceWithEmpty(rs.getString("STR_Type")));
				myPermitForm.setPrint(rs.getString("act_stat").equals(Constants.ACTIVITY_ISSUED_STATUS) ? "Y" : "N");

				// logger.debug("**********" + myPermitForm.getPrint());
				logger.debug("**********" + rs.getString("act_stat"));
				myPermitForm.setActSubType(rs.getString("ACT_SUBTYPE_ID"));
				myPermitForm.setAmount(getAmount(rs.getString("act_id")));
				logger.debug("***	myPermitForm.getAmount****" + myPermitForm.getAmount());
				myPermitForm.setPrint(rs.getString("ONLINE_PRINT"));

				logger.debug("**********" + myPermitForm.getPrint());
				actList.add(myPermitForm);
			}

			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {

			logger.error(e.getMessage());
		}
		return actList;
	}

	/**
	 * 
	 * @param emailAddr
	 * @param action
	 * @return List of Combo Number for a Logged in User, and its respective Activities Mapped to particular combo
	 */
	public List getComboList(String emailAddr, String inActive) {
		List comboList = new ArrayList();
		logger.debug("getMyPermits( Entered");
		logger.debug("inActive " + inActive);
		MyPermitForm myPermitForm = null;
		Activity activity = null;
		// inspectionItemCodeList.add(new InspectionItem(-1, "Please Select"));
		//String actStatus = "";
		//actStatus = "(" + Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL + "," + Constants.ACTIVITY_STATUS_BUILDING_PERMIT_CANCELLED + "," + Constants.ACTIVITY_STATUS_BUILDING_PERMIT_EXPIRED + ")";
		try {
			/*String sql = "select DISTINCT(P.email_addr),LA.STR_NO AS C0 ,LA.STR_MOD,SL.PRE_DIR AS C1 ,SL.STR_NAME AS C2,SL.STR_TYPE AS C3,LA.CITY,LA.STATE,LA.ZIP from Activity A  Left outer join ACTIVITY_PEOPLE AP on A.ACT_ID=AP.ACT_ID  Left outer join People P on P.People_ID= Ap.PEople_ID  Left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE  Left outer join Lkup_ACT_ST LAS on LAS.STATUS_ID=A.status  LEFT outer join LSO_ADDRESS LA on LA.ADDR_ID=A.ADDR_ID  Left outer join Street_list SL on SL.STREET_ID=LA.Street_ID Left outer join SUB_PROJECT SP  on A.SPROJ_ID = SP.SPROJ_ID Left outer join inspection I on A.act_id = I.act_id " + " where lower(P.email_addr) = " + StringUtils.checkString(emailAddr.toLowerCase()) + " ";
			if (inActive.endsWith("No")) {

				sql = sql + " and a.status not in " + actStatus + " and (((add_months(A.EXP_DATE , " + Constants.ACTIVITY_EXP_Month + ") >= current_date or add_months(A.PLAN_CHK_FEE_DATE , " + Constants.ACTIVITY_EXP_Month + ") >= current_date or add_months(I.inspection_dt , " + Constants.ACTIVITY_EXP_Month + ")  >= current_date) and not A.act_nbr like 'PW%') or (A.act_nbr like 'PW%' and A.EXP_DATE >= current_date)) ";
			} else {

				sql = sql + " and a.status in " + actStatus + " and SP.SPROJ_NBR IS NOT NULL  and not A.act_nbr like 'PW%' ";
			}*/
			
			StringBuilder sb = new StringBuilder();
			sb.append(" select DISTINCT(P.email_addr),LA.STR_NO AS C0 ,LA.STR_MOD,SL.PRE_DIR AS C1 ,SL.STR_NAME AS C2,SL.STR_TYPE AS C3,LA.CITY,LA.STATE,LA.ZIP,LA.LSO_ID ");
			sb.append(" from Activity A   ");
			sb.append(" Left outer join ACTIVITY_PEOPLE AP on A.ACT_ID=AP.ACT_ID  ");
			sb.append("  Left outer join People P on P.People_ID= Ap.PEople_ID ");
			sb.append(" Left outer join LKUP_ACT_TYPE LAT on LAT.TYPE=A.ACT_TYPE ");
			sb.append(" Left outer join Lkup_ACT_ST LAS on LAS.STATUS_ID=A.status ");
			sb.append(" LEFT outer join LSO_ADDRESS LA on LA.ADDR_ID=A.ADDR_ID ");
			sb.append(" Left outer join Street_list SL on SL.STREET_ID=LA.Street_ID ");
			sb.append(" Left outer join SUB_PROJECT SP  on A.SPROJ_ID = SP.SPROJ_ID ");
		//	sb.append(" Left outer join inspection I on A.act_id = I.act_id ");
			sb.append(" where lower(P.email_addr) = ").append(StringUtils.checkString(emailAddr.toLowerCase())).append(" ");
			
			if (inActive.endsWith("No")) {
				sb.append(" and LAS.PSA_ACTIVE='N'  ");
			}else{
				sb.append(" and LAS.PSA_ACTIVE='Y'  ");	
			}
			
			
			logger.debug(sb.toString());
			ResultSet rs = new Wrapper().select(sb.toString());

			while (rs.next()) {
				ApplyOnlinePermitForm applyOnlinePermitObj = new ApplyOnlinePermitForm();
				applyOnlinePermitObj.setComboNumber("");
				// applyOnlinePermitObj.setLsoId(Integer.parseInt(rs.getString("LSO_ID")));
				// applyOnlinePermitObj.setComboName(rs.getString("DESCription")!=null?rs.getString("DESCription"):rs.getString("DESCription"));
				applyOnlinePermitObj.setStreetName((rs.getString("C0") != null ? rs.getString("C0") : "") + " " + (rs.getString("C1") != null ? rs.getString("C1") : "") + " " + (rs.getString("C2") != null ? rs.getString("C2") : "") + " " + (rs.getString("C3") != null ? rs.getString("C3") : ""));
				// logger.debug("------------"+applyOnlinePermitObj.getStreetName());
				// String number="";
				// if(rs.getString("SPROJ_NBR")!=null)number = rs.getString("SPROJ_NBR");
				// else if(rs.getString("SPROJ_NBR")!=null)number = rs.getString("SPROJ_NBR");
				// applyOnlinePermitObj.setComboNumber("1");
				// applyOnlinePermitObj.setSubProjectId(StringUtils.s2i(rs.getString("SPROJ_ID")));
				// applyOnlinePermitObj.setPrint(new OnlinePermitAgent().checkPrintPermit(rs.getString("SPROJ_ID")));
				List actList = getComboActList(emailAddr, inActive,rs.getInt("LSO_ID"));

				applyOnlinePermitObj.setComboActSize(actList.size());
				applyOnlinePermitObj.setComboActList(actList);
				comboList.add(applyOnlinePermitObj);
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return comboList;
	}

	// MY Permits DOTLIST
	public List getDotList(String accntNbr, String action) {
		List dotList = new ArrayList();
		logger.debug("getMyPermits( Entered");
		logger.debug("action " + action);
		MyPermitForm myPermitForm = null;
		Activity activity = null;
		// inspectionItemCodeList.add(new InspectionItem(-1, "Please Select"));
		try {
			String sql = "select a.*,d.status as dot_status,d.license_plate,lat.description as PTYPE from activity a,dot_activity d,lkup_act_type lat " + " where a.act_id = d.act_id and d.status ='A' and exp_date > current_date and a.sproj_id= " + accntNbr + " and lat.type= a.act_type  order by a.act_type asc,created desc ";
			logger.debug(sql);
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				ApplyDotOnlinePermitForm applyDotOnlinePermitObj = new ApplyDotOnlinePermitForm();
				applyDotOnlinePermitObj.setActivityId(rs.getInt("ACT_ID"));
				applyDotOnlinePermitObj.setActivityNbr(rs.getString("ACT_NBR"));
				applyDotOnlinePermitObj.setActType(rs.getString("PTYPE"));
				applyDotOnlinePermitObj.setStartDate(rs.getString("START_DATE"));
				applyDotOnlinePermitObj.setExpiryDate(rs.getString("EXP_DATE"));
				if (rs.getString("DOT_STATUS").equalsIgnoreCase("A")) {

					applyDotOnlinePermitObj.setActivityStatus("Active");
				} else {
					applyDotOnlinePermitObj.setActivityStatus(rs.getString("DOT_STATUS"));
				}

				dotList.add(applyDotOnlinePermitObj);
			}
			rs.close();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return dotList;
	}

	// get Amount
	public String getAmount(String actId) {

		String amt = "0";
		// inspectionItemCodeList.add(new InspectionItem(-1, "Please Select"));
		try {
			String sql = "select sum(fee_amnt- fee_paid) as FeeAmt from activity_fee where activity_id=" + actId;
			// logger.debug(sql);
			ResultSet rs = new Wrapper().select(sql);

			while (rs.next()) {
				if (rs.getString("FeeAmt") != null && !(rs.getString("FeeAmt").equals("-1"))) {
					amt = StringUtils.i2s(StringUtils.s2i((rs.getString("FeeAmt"))));
				}

			}

			amt = StringUtils.str2$(amt);

			rs.close();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		return amt;
	}

	public Inspection getLatestInspection(String actId) throws AgentException {
		Inspection inspection = null;
		String inspectorQuery = "select * from (select * from inspection where ACT_ID = " + actId + " order by INSPECTION_DT desc) where rownum = 1";
		RowSet rs = null;
		Wrapper db = new Wrapper();
		logger.debug("Inspector Query::" + inspectorQuery);

		try {
			rs = db.select(inspectorQuery);
			if (rs != null && rs.next()) {
				inspection = new Inspection();
				inspection.setInspectionId(rs.getString("INSPECTION_ID"));
				inspection.setActivityId(rs.getString("ACT_ID"));
				inspection.setActnCode(rs.getString("ACTN_CODE"));
				inspection.setComments("COMMENTS");
				inspection.setInspectionItem(rs.getString("INSPCT_ITEM_ID"));
				inspection.setInspectorId(rs.getString("INSPECTOR_ID"));
			}

		} catch (Exception e) {
			logger.error("error in getLatestInspection", e);
			throw new AgentException("", e);
		}

		return inspection;
	}

	public void updateInspectionDate(int inspId, String date) throws AgentException {

		Wrapper db = new Wrapper();

		String sql = "UPDATE INSPECTION SET INSPECTION_DT  = to_date('" + date + "','MM/DD/YYYY') WHERE INSPECTION_ID =  " + inspId;

		logger.debug("sql::" + sql);

		try {
			db.update(sql);
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}
	}

	public CachedRowSet getInspectionNoticeList(String activityId) {
		CachedRowSet rs = null;

		try {
			Wrapper db = new Wrapper();
			rs = new CachedRowSet();

			String sql = "select i.inspection_id as id,i.inspection_dt,i.inspector_id inspector_id,i.comments as comments,i.timeunit,i.NO_OF_CLASS1_VIOLATION,i.NO_OF_CLASS2_VIOLATION,NO_OF_MINOR_VIOLATION, lii.description as item,lic.description as actn_code, COALESCE(' ' || i.request_source,'') AS request_source from ( inspection i left outer join lkup_inspct_item lii  on i.inspct_item_id = lii.inspct_item_id ) left outer join lkup_inspct_code lic on i.actn_code = lic.actn_code where I.ACTN_CODE not in (" + Constants.SYSTEMATIC_INSP_STAT_CANCELLED + ") and i.act_id = " + activityId + " order by inspection_id desc";
			logger.debug("inspection List " + sql);
			rs = (CachedRowSet) db.select(sql);
		} catch (Exception e) {
			logger.error("Exception occured in getInspectionList method of InspectionAgent. Message-" + e.getMessage());
		}

		return rs;
	}

	public Inspection getLastClosedInspection(String actId) throws AgentException {
		Inspection inspection = null;
		String inspectorQuery = "select * from (select * from inspection where ACTN_CODE not in (select ACTN_CODE from LKUP_INSPCT_CODE where ACTIVE = 'Y') and ACT_ID = " + actId + " order by INSPECTION_DT desc) where rownum = 1";
		RowSet rs = null;
		Wrapper db = new Wrapper();
		logger.debug("Inspector Query::" + inspectorQuery);

		try {
			rs = db.select(inspectorQuery);
			if (rs != null && rs.next()) {
				inspection = new Inspection();
				inspection.setInspectionId(rs.getString("INSPECTION_ID"));
				inspection.setActivityId(rs.getString("ACT_ID"));
				inspection.setActnCode(rs.getString("ACTN_CODE"));
				inspection.setComments("COMMENTS");
				inspection.setInspectionItem(rs.getString("INSPCT_ITEM_ID"));
				inspection.setInspectorId(rs.getString("INSPECTOR_ID"));
				inspection.setDate(StringUtils.sqlDateToString(rs.getDate("INSPECTION_DT")));
			}

		} catch (Exception e) {
			logger.error("error in getLatestInspection", e);
			throw new AgentException("", e);
		}

		return inspection;
	}

	/**
	 * Function to save newly created inspection data. Called from SaveNewInspectionAction.
	 */
	public void saveNewInspection(InspectionForm form, String outstandingFees) throws AgentException {
		ActivityAgent activityAgent = new ActivityAgent();

		try {

			Wrapper db = new Wrapper();
			String actionCode = form.getActionCode();
			String activityId = form.getActivityId();
			logger.debug("action code ### : " + actionCode);
			logger.debug("outstanding Fees ### : " + outstandingFees);

			String sql = "";
			String[] inspectionItems = form.getInspectionItems();
			String inspectionItem = "";
			if (inspectionItems == null || inspectionItems.length < 1 || inspectionItems[0].equalsIgnoreCase("")) {
				inspectionItems = new String[] { form.getInspectionItem() };
			}
			if (inspectionItems.length > 0) {
				for (int i = 0; i < inspectionItems.length; i++) {
					inspectionItem = inspectionItems[i];
					String inspectionId = StringUtils.i2s(db.getNextId("INSPECTION_ID"));
					form.setInspectionId(inspectionId);
					sql = "INSERT INTO INSPECTION(INSPECTION_ID,ACT_ID,INSPECTOR_ID,INSPCT_ITEM_ID,INSPECTION_DT,ACTN_CODE,ROUTE_SEQ,TIMESTAMP,REQUEST_SOURCE,COMMENTS,TIMEUNIT,NO_OF_CLASS1_VIOLATION,NO_OF_CLASS2_VIOLATION,NO_OF_MINOR_VIOLATION,UPDATED_BY,PROGRAM_ELEMENT) VALUES (" + form.getInspectionId() + ", " + form.getActivityId() + ", " + form.getInspectorId() + ", " + inspectionItem + ", to_date('" + form.getInspectionDate() + "','MM/DD/YYYY'), " + form.getActionCode() + ", " + "0" + ", current_timestamp" + ",'MAN'" + ", " + StringUtils.checkString(StringUtils.replaceQuot(StringUtils.replaceDoubleQuot(form.getComments()))) + "," + form.getTimeUnit() + "," + StringUtils.checkString(form.getNoOfViolation1()) + "," + StringUtils.checkString(form.getNoOfViolation2()) + "," + StringUtils.checkString(form.getNoOfMinorViolation()) + "," + form.getUpdatedBy() + "," + StringUtils.checkString(form.getProgElement()) + ")";
					logger.debug(sql);
					db.insert(sql);

					if (!form.getActionCode().equals(StringUtils.i2s(Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION))) {
						sql = "update inspection set actn_code = " + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION_HISTORY + " where actn_code=" + Constants.INSPECTION_CODES_REQUEST_FOR_INSPECTION + " and act_id = " + form.getActivityId() + " and inspct_item_id = " + inspectionItem;
						logger.debug(sql);
						db.update(sql);
					}
				}
			}

			String projectName = LookupAgent.getProjectName("A", Integer.parseInt(form.getActivityId()));
			// if(!(projectName.equalsIgnoreCase("Systematic Inspection")))
			// {
			// to insert newly created record in inspection history table
			// int historyid=db.getNextId("INSP_HISTORY_ID");
			// String sql1 = "INSERT INTO INSPECTION_HISTORY(INSPECTION_HISTORY_ID,INSPECTION_ID,ACT_ID,INSPECTOR_ID,INSPCT_ITEM_ID,INSPECTION_DT,ACTN_CODE,TIMESTAMP) VALUES (" +historyid + ", " + form.getInspectionId()+ ", " +form.getActivityId() + ", " + form.getInspectorId() + ", " + form.getInspectionItem() + ", to_date('" + form.getInspectionDate() + "','MM/DD/YYYY'), " + form.getActionCode() + ", " + " current_timestamp" + ")";
			// logger.debug("query to insert record"+sql1);
			// db.insert(sql1);
			// }

			String sql1 = "";
			String sql2 = "";
			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CERTIFIED1)) {

				sql1 = "update activity set status = " + Constants.ACTIVITY_STATUS_NS_SYS_CLOSED + " where act_id=" + activityId;

				logger.debug(sql1);

				String actStatId = "" + db.getNextId("ACT_STAT_ID");

				sql2 = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + Constants.ACTIVITY_STATUS_NS_SYS_CERTIFIED1 + ",current_date," + form.getInspectorId() + ",current_timestamp,current_timestamp,'',current_timestamp)";
				db.insert(sql2);
			}

			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CERTIFIED2)) {

				sql1 = "update activity set status = " + Constants.ACTIVITY_STATUS_NS_SYS_CLOSED + " where act_id=" + activityId;

				logger.debug(sql1);

				String actStatId = "" + db.getNextId("ACT_STAT_ID");

				sql2 = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + Constants.ACTIVITY_STATUS_NS_SYS_CERTIFIED2 + ",current_date," + form.getInspectorId() + ",current_timestamp,current_timestamp,'',current_timestamp)";
				db.insert(sql2);
			}

			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CERTIFIED3)) {

				sql1 = "update activity set status = " + Constants.ACTIVITY_STATUS_NS_SYS_CLOSED + " where act_id=" + activityId;

				logger.debug(sql1);

				String actStatId = "" + db.getNextId("ACT_STAT_ID");

				sql2 = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + Constants.ACTIVITY_STATUS_NS_SYS_CERTIFIED3 + ",current_date," + form.getInspectorId() + ",current_timestamp,current_timestamp,'',current_timestamp)";
				db.insert(sql2);
			}
			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CORRECTION)) {
				sql1 = "update activity set status = " + Constants.ACTIVITY_STATUS_NS_SYS_OPEN + " where act_id=" + activityId;
				logger.debug(sql1);

				String actStatId = "" + db.getNextId("ACT_STAT_ID");

				sql2 = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + Constants.ACTIVITY_STATUS_NS_SYS_OPEN + ",current_date," + form.getInspectorId() + ",current_timestamp,current_timestamp,'',current_timestamp)";
				db.insert(sql2);
			}
			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CLOSED)) {
				sql1 = "update activity set status = " + Constants.ACTIVITY_STATUS_NS_SYS_CLOSED + " where act_id=" + activityId;
				logger.debug(sql1);

				String actStatId = "" + db.getNextId("ACT_STAT_ID");

				sql2 = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + Constants.ACTIVITY_STATUS_NS_SYS_CLOSED + ",current_date," + form.getInspectorId() + ",current_timestamp,current_timestamp,'',current_timestamp)";
				db.insert(sql2);
			}
			/*
			 * if((form.getActionCode()).endsWith("22")) { sql = "update activity set status = "+Constants.ACTIVITY_STATUS_NSD_OPEN+" where act_id=" + activityId; logger.debug(sql); }
			 */
			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_REFER_TO_CE)) {
				sql1 = "update activity set status = " + Constants.ACTIVITY_STATUS_NSD_REFER_TO_CE + " where act_id=" + activityId;
				logger.debug(sql1);

				String actStatId = "" + db.getNextId("ACT_STAT_ID");

				sql2 = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values(" + actStatId + "," + activityId + "," + Constants.ACTIVITY_STATUS_NSD_REFER_TO_CE + ",current_date," + form.getInspectorId() + ",current_timestamp,current_timestamp,'',current_timestamp)";
				db.insert(sql2);
			}
			if ((form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CERTIFIED1) || (form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CERTIFIED2) || (form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CERTIFIED3) || (form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CORRECTION) || (form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_CLOSED) || (form.getActionCode()).equalsIgnoreCase(Constants.SYSTEMATIC_INSP_STAT_REFER_TO_CE)) {
				logger.debug("hai problem with update");
				db.update(sql1);

			}

			// Update activity with expiration date
			if (form.getInspectionDate() != null) {
				new ActivityAgent().updateActivityExpirationDate(form.getInspectionDate(), Integer.parseInt(activityId), actionCode);
			}

		} catch (Exception ex) {
			logger.error("Exception in saveNewInspection", ex);
			throw new AgentException();
		}
	}

	public boolean canHaveOpenViolation(String actionCodeId) throws AgentException {
		boolean canHaveOpenViolation = true;

		Wrapper db = new Wrapper();

		RowSet rs = null;

		String sql = "select CAN_HAVE_OPEN_VIOLATION from LKUP_INSPCT_CODE where ACTN_CODE = " + actionCodeId;

		logger.debug("sql::" + sql);

		try {
			rs = db.select(sql);

			if (rs != null && rs.next()) {
				if (rs.getString("CAN_HAVE_OPEN_VIOLATION") != null && rs.getString("CAN_HAVE_OPEN_VIOLATION").equalsIgnoreCase("N")) {
					canHaveOpenViolation = false;
				}
			}
			return canHaveOpenViolation;
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
	}

	public String getdayAdd(String inspctItemId) throws Exception {
		String addDayQuery = "SELECT DAYS_ADD FROM LKUP_INSPCT_ITEM where INSPCT_ITEM_ID = ?";
		int addDays = 0;
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(addDayQuery);
			logger.debug("addDayQuery::" + addDayQuery);
			pstmt.setString(1, inspctItemId);
			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				if (rs.getString("DAYS_ADD") != null) {
					addDays = rs.getInt("DAYS_ADD");
				} else {
					return null;
				}
			}
		} catch (Exception ex) {
			logger.error("", ex);
			throw new Exception("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
		return "" + addDays;
	}

	public String isInspectorAvailable(String inspectorId, String selectedDate, int timeUnit, String inspectionId, int maxInspectionCount) throws Exception {

		String inspectorAvailableQuery = "SELECT SUM(TIMEUNIT) as timeUnits,INSPECTION_DT FROM INSPECTION WHERE INSPECTOR_ID = ? AND " + " INSPECTION_DT >= ? GROUP BY INSPECTION_DT ORDER BY INSPECTION_DT ASC";

		String finalDateString = "";

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat sdfForDb = new SimpleDateFormat("dd-MMM-yyyy");

		Date dateFromUI = sdf.parse(selectedDate);
		selectedDate = sdfForDb.format(dateFromUI);

		int addDays = 0;

		int timeAvailable = 0;

		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		ResultSet rs = null;

		int inspectionCount = 0;

		try {
			connection = wrapper.getConnection();
			pstmt = connection.prepareStatement(inspectorAvailableQuery);
			logger.debug("inspectorAvailableQuery::" + inspectorAvailableQuery);
			logger.debug("inspectorId::" + inspectorId);
			logger.debug("selectedDate::" + selectedDate);
			pstmt.setString(1, inspectorId);
			pstmt.setString(2, selectedDate);
			rs = pstmt.executeQuery();
			Map map = new HashMap();
			if (rs != null) {
				Calendar cl = Calendar.getInstance();
				while (rs.next()) {
					Date date = rs.getDate("INSPECTION_DT", cl);
					map.put(sdf.format(date), rs.getString("timeUnits"));
				}
			}
			Calendar clUI = Calendar.getInstance();
			clUI.setTime(dateFromUI);
			boolean getDate = false;

			String inspectionTimeQuery = "SELECT TIMEUNIT FROM INSPECTION WHERE INSPECTION_ID = ?";
			pstmt = null;
			rs = null;

			pstmt = connection.prepareStatement(inspectionTimeQuery);
			pstmt.setString(1, inspectionId);
			rs = pstmt.executeQuery();
			boolean resetTimeAvailable = false;

			while (!getDate) {
				Date finalDate = clUI.getTime();
				finalDateString = sdf.format(finalDate);
				timeAvailable = 0;
				if (!resetTimeAvailable) {
					if (rs != null && rs.next()) {
						timeAvailable = rs.getInt("TIMEUNIT");
					}
					resetTimeAvailable = true;
				}

				if (clUI.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					clUI.add(Calendar.DATE, 1);
				} else if (clUI.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
					clUI.add(Calendar.DATE, 2);
				} else {
					String finalDateStrForDb = sdfForDb.format(finalDate);
					String checkVacationQuery = "select * from vacation where End_Date >= '" + finalDateStrForDb + "' and '" + finalDateStrForDb + "' >= Start_Date and userId in (-1," + inspectorId + ") order by end_date desc";
					logger.debug("checkVacationQuery::" + checkVacationQuery);
					pstmt = null;
					rs = null;
					pstmt = connection.prepareStatement(checkVacationQuery);
					rs = pstmt.executeQuery();
					if (rs != null) {
						if (rs.next()) {
							Date vacationEndDate = rs.getDate("END_DATE", clUI);
							if (rs.getString("LEAVE_HOUR") != null && rs.getInt("LEAVE_HOUR") == Constants.MAX_TIME_HOUR) {
								clUI.add(Calendar.DATE, 1);
							} else {
								timeAvailable = timeAvailable + (Constants.MAX_TIME_HOUR - rs.getInt("LEAVE_HOUR")) * 60;
								if (map.get(finalDateString) != null) {
									timeAvailable = timeAvailable - Integer.parseInt(map.get(finalDateString).toString());
								}

								if (timeAvailable >= timeUnit) {
									inspectionCount = getInspectionCount(inspectorId, finalDateStrForDb);
									if (inspectionCount >= maxInspectionCount) {
										clUI.add(Calendar.DATE, 1);
									} else {
										logger.debug("finalDateString::" + finalDateString);
										return finalDateString;
									}
								} else {
									clUI.add(Calendar.DATE, 1);
								}
							}
						} else {
							timeAvailable = timeAvailable + Constants.MAX_TIME_HOUR * 60;
							if (map.get(finalDateString) != null) {
								timeAvailable = timeAvailable - Integer.parseInt(map.get(finalDateString).toString());
							}
							if (timeAvailable >= timeUnit) {
								inspectionCount = getInspectionCount(inspectorId, finalDateStrForDb);
								if (inspectionCount >= maxInspectionCount) {
									clUI.add(Calendar.DATE, 1);
								} else {
									logger.debug("finalDateString::" + finalDateString);
									return finalDateString;
								}
							} else {
								clUI.add(Calendar.DATE, 1);
							}
						}
					} else {
						inspectionCount = getInspectionCount(inspectorId, finalDateStrForDb);
						if (inspectionCount >= maxInspectionCount) {
							clUI.add(Calendar.DATE, 1);
						} else {
							logger.debug("finalDateString3::" + finalDateString);
							return finalDateString;
						}
					}
				}
			}
			// }

		} catch (Exception ex) {
			logger.error("", ex);
			throw new Exception("", ex);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}
			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException Ignore) {
				}
			}
		}
		return finalDateString;
	}

	private int getInspectionCount(String inspectorId, String date) throws AgentException {
		int inspectionCount = 0;
		String query = "select count(INSPECTOR_ID) as INSPECTION_COUNT from inspection where Inspector_id = " + inspectorId + " and INSPECTION_DT = '" + date + "' ";
		logger.debug("inspection Count Query::::" + query);

		Wrapper wrapper = new Wrapper();
		try {
			RowSet rs = wrapper.select(query);
			if (rs != null && rs.next()) {
				inspectionCount = rs.getInt("INSPECTION_COUNT");
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}

		return inspectionCount;
	}

}

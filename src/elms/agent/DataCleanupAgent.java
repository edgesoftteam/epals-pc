package elms.agent;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.common.Agent;
import elms.util.db.Wrapper;

public class DataCleanupAgent extends Agent {
	static Logger logger = Logger.getLogger(DataCleanupAgent.class.getName());
	
	public void removeSpecialCharactersFromLicNumber() {
		logger.debug("inside removeSpecialCharactersFromLicNumber");
		
		RowSet rs = null;
		Wrapper db = new Wrapper();
		String licNumber = "";
		int peopleId = 0;
		
		String getAllContractors = "SELECT * FROM PEOPLE WHERE PEOPLE_TYPE_ID = 4 and REGEXP_LIKE(LIC_NO,  '[^a-zA-Z0-9]' )";
		String updatePeople = "";
		try {
			rs = db.select(getAllContractors);
		
				if(rs!=null) {
					while(rs.next()) {
						licNumber = rs.getString("LIC_NO");
						peopleId = rs.getInt("PEOPLE_ID");
						licNumber = licNumber.replaceAll("[^a-zA-Z0-9]", "");  
						updatePeople = "UPDATE PEOPLE SET LIC_NO = '"+licNumber+"' where PEOPLE_ID = " + peopleId;
						logger.debug(updatePeople);
						db.update(updatePeople);
					}
				}
		} catch (Exception e) {
			logger.error("",e);
		}
		
		convertContractorsToAgent();
	}
	
	// Convert those contractors to agents who has 0 activity attached to them
	public void convertContractorsToAgent() {
		
		logger.debug("inside convertContractorsToAgent");
		
		RowSet rs = null;
		RowSet rsDetail = null;
		
		
		Wrapper db = new Wrapper();
		String licNumber = "";
		String sql = "select * from (select count(People_ID) as contcount,LIC_NO  from people where LIC_NO is not null AND PEOPLE_TYPE_ID = 4 group by LIC_NO)temp where contcount>1 AND LIC_NO != '0' ORDER BY contcount DESC";
		String updateContractor = "";
		try {
			rs = db.select(sql);
			
			if(rs != null) {
				while(rs.next()){
					licNumber = rs.getString("LIC_NO");	
					rsDetail = null;
					String detailSql = "select P.*,(Select COUNT(*) from EPALS.ACTIVITY_PEOPLE where ACTIVITY_PEOPLE.PEOPLE_ID = P.PEOPLE_ID) as permitCount from people P where PEOPLE_TYPE_ID = 4 AND P.LIC_NO = '"+licNumber+"' ORDER BY permitCount DESC";
					
					rsDetail = db.select(detailSql);
					
					if(rsDetail != null) {
						while(rsDetail.next()) {
							if(rsDetail.getInt("permitCount") == 0) {
								updateContractor = "UPDATE PEOPLE SET PEOPLE.PEOPLE_TYPE_ID = 1 where PEOPLE_ID = " + rsDetail.getInt("PEOPLE_ID");
								logger.debug(updateContractor);
								db.update(updateContractor);
							}
						}
						
					}
				}
			}
			}catch (Exception e) {
				logger.error("",e);
			}
	}
	
	// Convert those contractors to agents who has 1 activity attached to them
	public void convertContractorsToAgent1() {
		
		logger.debug("inside convertContractorsToAgent");
		
		RowSet rs = null;
		RowSet rsDetail = null;
		
		
		Wrapper db = new Wrapper();
		String licNumber = "";
		String sql = "select * from (select count(People_ID) as contcount,LIC_NO  from people where LIC_NO is not null AND PEOPLE_TYPE_ID = 4 group by LIC_NO)temp where contcount>1 AND LIC_NO != '0' ORDER BY contcount DESC";
		String updateContractor = "";
		String assignActivity ="";
		try {
			rs = db.select(sql);
			
			if(rs != null) {
				while(rs.next()){
					licNumber = rs.getString("LIC_NO");	
					rsDetail = null;
					String detailSql = "select P.*,(Select COUNT(*) from EPALS.ACTIVITY_PEOPLE where ACTIVITY_PEOPLE.PEOPLE_ID = P.PEOPLE_ID) as permitCount from people P where PEOPLE_TYPE_ID = 4 AND P.LIC_NO = '"+licNumber+"' ORDER BY permitCount DESC";
					
					rsDetail = db.select(detailSql);
					
					int mainContractor = 0;
					
					if(rsDetail != null) {
						while(rsDetail.next()) {
							if(mainContractor == 0) {
								mainContractor = rsDetail.getInt("PEOPLE_ID");
								continue;
							}
							if(rsDetail.getInt("permitCount") == 1) {
								assignActivity = "update ACTIVITY_PEOPLE SET PEOPLE_ID = "+mainContractor+" where PEOPLE_ID=" + rsDetail.getInt("PEOPLE_ID");
								logger.debug(assignActivity);
								try {
								db.update(assignActivity);
								}catch (Exception e) {
									logger.error("",e);
									continue;
								}
								updateContractor = "UPDATE PEOPLE SET PEOPLE.PEOPLE_TYPE_ID = 1 where PEOPLE_ID = " + rsDetail.getInt("PEOPLE_ID");
								logger.debug(updateContractor);
								db.update(updateContractor);
							}
						}
						
					}
				}
			}
			}catch (Exception e) {
				logger.error("",e);
			}
	}
	
}

package elms.agent;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.sql.RowSet;

import org.apache.log4j.Logger;

import elms.app.admin.EngineerUser;
import elms.app.admin.PlanCheckStatus;
import elms.app.common.Agent;
import elms.app.project.ActivityAttributes;
import elms.app.project.ActivityHistory;
import elms.app.project.PlanCheck;
import elms.app.project.PlanCheckHistory;
import elms.common.Constants;
import elms.control.actions.ApplicationScope;
import elms.control.beans.PlanCheckForm;
import elms.exception.AgentException;
import elms.util.StringUtils;
import elms.util.db.Wrapper;

/**
 * @author Shekhar Jain
 */
public class PlanCheckAgent extends Agent {
	/**
	 * The Logger
	 */
	static Logger logger = Logger.getLogger(PlanCheckAgent.class.getName());
	protected List planChecks;
	protected PlanCheck planCheck;
	protected int planCheckId;
	protected int activityId;



	/**
	 * Get Plan checks
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public PlanCheck[] getPlanChecks(int activityId,List sorting) throws Exception {
		logger.info("getPlanChecks(" + activityId + ")");

		List list = new ArrayList();
		RowSet rs = null;
		try {
			String date;
			String trgdate;
			String compdate;
			Wrapper db = new Wrapper();

			String sql = null;
			
				sql = "select PC.plan_chk_id, PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.plan_chk_date, PC.comnt, U.first_name || ' ' || U.last_name as name, S.pc_desc,PC.target_date, PC.completion_date,PC.department  from plan_check PC left outer join LKUP_PC_PROCESS_TYPE LPT ON PC.PROCESS_TYPE = LPT.PC_PROCESS_TYPE_ID left outer join users U on U.userid = PC.engineer, lkup_pc_status S where PC.act_id=" + activityId + " AND PC.stat_code = S.pc_code ";
		
			if(sorting != null && sorting.size() >0){
				//List sorting = cipSearch.getSorting();
				sql = sql + " Order by ";
				for(int i=0;i<sorting.size();i++){
					sql = sql + sorting.get(i).toString() + ",";
				}
				sql = sql.substring(0,sql.lastIndexOf(","));
			}else{
				sql = sql + " order by PC.target_date";
			}
				

			logger.debug(sql);

			rs = db.select(sql);

			while (rs!=null && rs.next()) {
				// create new planCheck objects and add them to list
				planCheck = new PlanCheck();
				planCheck.setPlanCheckId(rs.getInt("plan_chk_id"));
				//System.out.println("plan_chk_id"+rs.getInt("plan_chk_id"));
				planCheck.setStatusDesc(rs.getString("pc_desc"));
				
				planCheck.setDeptDesc(LookupAgent.getDepartmentbasedOnId(rs.getString("department")).getDescription());
				
				planCheck.setProcessTypeDesc(rs.getString("PC_PROCESS_TYPE"));

				date = rs.getString("plan_chk_date");
				logger.debug("date is " + date);

				Calendar c = StringUtils.dbDate2cal(date);
				logger.debug("calendar is " + c);
				date = StringUtils.cal2str(c);
				logger.debug("after date " + date);
				planCheck.setDate(date);
				planCheck.setPlanCheckDate(c);
				logger.debug("plancheck date form DB  ### " + c);

				trgdate = rs.getString("target_date");
				logger.debug("Target Date is " + trgdate);

				Calendar c1 = StringUtils.dbDate2cal(trgdate);
				logger.debug("calendar is " + c1);
				trgdate = StringUtils.cal2str(c1);
				logger.debug("after date " + trgdate);
				planCheck.setTrgdate(trgdate);
				planCheck.setTargetDate(c1);
				logger.debug("target date form DB  ### " + c1);

				compdate = rs.getString("completion_date");
				logger.debug("Completion Date is " + compdate);

				Calendar c2 = StringUtils.dbDate2cal(compdate);
				logger.debug("calendar is " + c2);
				compdate = StringUtils.cal2str(c2);
				logger.debug("after date " + compdate);
				planCheck.setCompdate(compdate);
				planCheck.setCompletionDate(c2);
				logger.debug("completion date form DB  ### " + c2);

				planCheck.setEngineerName(rs.getString("name"));

				if ((rs.getString("name") == null) || (rs.getString("name").trim().equalsIgnoreCase(""))) {
					planCheck.setEngineerName("Unassigned");
				}

				if (rs.getString("comnt") == null) {
					planCheck.setComments("");
				} else {
					if((rs.getString("comnt")).length()>120) {
						planCheck.setComments((rs.getString("comnt")).substring(0, 120));	
					}else {
						planCheck.setComments(rs.getString("comnt"));	
					}
				}
				
				planCheck.setPlanCkeckHistories(getPlanChecksHistory(rs.getInt("plan_chk_id")));

				logger.debug("befor add list");
				list.add(planCheck);
				logger.debug("after add list");
			}

			rs.close();
			logger.debug("returning getPlanChecks back to action  ..");

			PlanCheck[] tmpArray = new PlanCheck[list.size()];
			list.toArray(tmpArray);

			return tmpArray;
		} catch (Exception e) {
			logger.debug("Exception in getPlanChecks method in PlanCheckAgent " + e.getMessage()+e);
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	/**
	 * Get Plan checks
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public PlanCheckHistory[] getPlanChecksHistory(int planCheckId) throws Exception {
		logger.info("getPlanChecks(" + planCheckId + ")");

		List list = new ArrayList();
		RowSet rs =  null;
		PlanCheckHistory planCheck = null;
		try {
			String date;
			String trgdate;
			String compdate;
			Wrapper db = new Wrapper();

			String sql = null;
			
				sql = "select PCH.PLAN_CHK_HISTORY_ID,PCH.plan_chk_id, PCH.plan_chk_date, PCH.comnt, U.first_name || ' ' || U.last_name as name,S.pc_desc,PCH.target_date, PCH.completion_date,PCH.department from PLAN_CHECK_HISTORY PCH left outer join users U on U.userid = PCH.engineer, lkup_pc_status S where PCH.plan_chk_id=" + planCheckId + " AND PCH.stat_code = S.pc_code order by updaetd desc,plan_chk_id desc";
				
		
			logger.debug(sql);

			rs = db.select(sql);

			while (rs!=null && rs.next()) {
				// create new planCheck objects and add them to list
				planCheck = new PlanCheckHistory();
				planCheck.setPcHistoryId(rs.getInt("PLAN_CHK_HISTORY_ID"));
				planCheck.setPlanCheckId(rs.getInt("plan_chk_id"));
				planCheck.setStatusDesc(StringUtils.nullReplaceWithEmpty(rs.getString("pc_desc")));
				planCheck.setDeptDesc(LookupAgent.getDepartmentbasedOnId(rs.getString("department")).getDescription());

				date = rs.getString("plan_chk_date");
				logger.debug("date is " + date);

				Calendar c = StringUtils.dbDate2cal(date);
				logger.debug("calendar is " + c);
				date = StringUtils.cal2str(c);
				logger.debug("after date " + date);
				planCheck.setDate(date);
				planCheck.setPlanCheckDate(c);
				logger.debug("plancheck date form DB  ### " + c);

				trgdate = rs.getString("target_date");
				logger.debug("Target Date is " + trgdate);

				Calendar c1 = StringUtils.dbDate2cal(trgdate);
				logger.debug("calendar is " + c1);
				trgdate = StringUtils.cal2str(c1);
				logger.debug("after date " + trgdate);
				planCheck.setTrgdate(trgdate);
				planCheck.setTargetDate(c1);
				logger.debug("target date form DB  ### " + c1);

				compdate = rs.getString("completion_date");
				logger.debug("Completion Date is " + compdate);

				Calendar c2 = StringUtils.dbDate2cal(compdate);
				logger.debug("calendar is " + c2);
				compdate = StringUtils.cal2str(c2);
				logger.debug("after date " + compdate);
				planCheck.setCompdate(compdate);
				planCheck.setCompletionDate(c2);
				logger.debug("completion date form DB  ### " + c2);

				planCheck.setEngineerName(rs.getString("name"));

				if ((rs.getString("name") == null) || (rs.getString("name").trim().equalsIgnoreCase(""))) {
					planCheck.setEngineerName("Unassigned");
				}

				if (rs.getString("comnt") == null) {
					planCheck.setComments("");
				} else {
					if((rs.getString("comnt")).length()>120) {
						planCheck.setComments((rs.getString("comnt")).substring(0, 120));	
					}else {
						planCheck.setComments(rs.getString("comnt"));							
					}
				}

				logger.debug("befor add list");
				list.add(planCheck);
				logger.debug("after add list");
			}

			rs.close();
			logger.debug("returning getPlanChecks back to action  ..");

			PlanCheckHistory[] tmpArray = new PlanCheckHistory[list.size()];
			list.toArray(tmpArray);

			return tmpArray;
		} catch (Exception e) {
			logger.debug("Exception in getPlanChecks method in PlanCheckAgent " + e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	public String getDefaultPlanCheck(String actType) throws Exception {
		logger.info("getDefaultPlanCheck(" + actType + ")");
		String sql = "select default_pc from lkup_act_type where type = '" + actType + "'";
		String defaults = "";
		logger.info("sql " + sql );
		RowSet rs = null;
		try {
			Wrapper db = new Wrapper();
			rs = db.select(sql);

			if (rs!=null && rs.next()) {
				defaults = rs.getString("default_pc");
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}//Closing RowSet added by Manjuprasad
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }

		return defaults;
	}

	/**
	 * Delete plan check
	 * 
	 * @param selectedPlanCheck
	 * @param psaId
	 * @throws Exception
	 */
	public void deletePlanCheck(String[] selectedPlanCheck, String psaId) throws Exception {
		logger.info("deletePlanCheck(" + selectedPlanCheck + ", " + psaId + ")");

		String sql = "";
		logger.debug("inside plan check delete");

		try {
			Wrapper db = new Wrapper();
			db.beginTransaction();
			logger.debug("inside try with plancheck list " + selectedPlanCheck);
			logger.debug("size of arrey is " + selectedPlanCheck.length);

			for (int i = 0; i < selectedPlanCheck.length; i++) {
				logger.debug("inside for");
				sql = "DELETE FROM PLAN_CHECK WHERE ACT_ID = " + psaId + " AND PLAN_CHK_ID =" + selectedPlanCheck[i];
				logger.debug("sql for delete plancheck " + sql);
				db.addBatch(sql);
			}

			db.executeBatch();
			db.commitTransaction();
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}

	/**
	 * Get Plan Check
	 * 
	 * @param planCheckId
	 * @return
	 * @throws Exception
	 */
	public PlanCheck getPlanCheck(int planCheckId) throws Exception {
		logger.info("getPlanCheck(" + planCheckId + ")");

		planCheck = new PlanCheck();
		RowSet rs = null;
		try {
			String date;
			String trgdate;
			String compdate;
			Wrapper db = new Wrapper();
			String sql = "select ACT.act_nbr,PC.plan_chk_date, PC.target_date,PC.completion_date,PC.ACT_ID,PC.units, PC.STAT_CODE, PC.ENGINEER, PC.COMNT,PC.cat_code,PC.title_code,PC.created_by,PC.updated_by,pc.department,PC.PROCESS_TYPE,PC.CREATED,U.FIRST_NAME,U.LAST_NAME ,U.email_id,ACT.ACT_NBR,(LA.STR_NO ||' '|| V.STREET_NAME) as ADDRESS_FORMAT, LAT.DESCRIPTION AS ACTTYPE ,ACT.DESCRIPTION AS ACTDESC from plan_check pc left join USERS U  on PC.ENGINEER = U.USERID " + " LEFT JOIN ACTIVITY ACT ON PC.ACT_ID= ACT.ACT_ID JOIN LSO_ADDRESS LA ON LA.ADDR_ID=ACT.ADDR_ID JOIN V_STREET_LIST V ON V.STREET_ID=LA.STREET_ID LEFT JOIN LKUP_ACT_TYPE LAT ON ACT.ACT_TYPE = LAT.TYPE where plan_chk_id= " + planCheckId;

			logger.debug("The select Sql of plan Check  is " + sql);

			rs = db.select(sql);

			if (rs!=null && rs.next()) {
				date = rs.getString("plan_chk_date");

				Calendar c = StringUtils.dbDate2cal(date);
				planCheck.setPlanCheckDate(c);
				date = StringUtils.cal2str(c);
				planCheck.setDate(date);
				planCheck.setActNbr(rs.getString("act_nbr"));
				trgdate = rs.getString("target_date");

				Calendar c1 = StringUtils.dbDate2cal(trgdate);
				planCheck.setTargetDate(c1);
				trgdate = StringUtils.cal2str(c1);
				planCheck.setTrgdate(trgdate);

				compdate = rs.getString("completion_date");

				Calendar c2 = StringUtils.dbDate2cal(compdate);
				planCheck.setCompletionDate(c2);
				compdate = StringUtils.cal2str(c2);
				planCheck.setCompdate(compdate);

				planCheck.setActivityId(StringUtils.s2i(rs.getString("act_id")));
				//planCheck.setUnit(StringUtils.d2s(rs.getDouble("units")));
				planCheck.setUnitMinutes(StringUtils.d2s(rs.getDouble("units")));
				planCheck.setStatusCode(StringUtils.s2i(rs.getString("stat_code")));
				planCheck.setEngineer(StringUtils.s2i(rs.getString("engineer")));
				planCheck.setComments((rs.getString("comnt") != null) ? rs.getString("comnt") : "");
				planCheck.setCategoryCode(StringUtils.s2i(rs.getString("cat_code")));
				planCheck.setTitleCode(StringUtils.s2i(rs.getString("title_code")));
				planCheck.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				planCheck.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
//				planCheck.setDeptDesc(StringUtils.nullReplaceWithEmpty(rs.getString("department")));
				planCheck.setDeptDesc(LookupAgent.getDepartmentbasedOnId(rs.getString("department")).getDescription());
				
				planCheck.setProcessType(rs.getInt("PROCESS_TYPE"));
				planCheck.setCreated(rs.getDate("CREATED"));
				planCheck.setAddress(rs.getString("ADDRESS_FORMAT"));
				planCheck.setActType(rs.getString("ACTTYPE"));
				planCheck.setActDesc(rs.getString("ACTDESC"));
				planCheck.setEngineerMailId(rs.getString("email_id"));
			}

			rs.close();
			logger.debug("returning getPlanCheck back to action  ..");

			return planCheck;
		} catch (Exception e) {
			logger.debug("Exception in getPlanCheck" + e.getMessage()+e);
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	/**
	 * Get Engineer from project
	 * 
	 * @param projectId
	 * @return
	 * @throws Exception
	 */
	public String getEngFromProj(String projectId) throws Exception {
		ResultSet rs = null;

		try {
			Wrapper db = new Wrapper();

			String sql = "SELECT USERID FROM PROCESS_TEAM WHERE GROUP_ID = (SELECT GROUP_ID FROM GROUPS WHERE upper(GROUPS.NAME) = upper('Plan Review Engineer')) AND PSA_ID = " + projectId;
			logger.debug("inspector sql : " + sql);

			rs = db.select(sql);

			if (rs!=null && rs.next()) {
				logger.debug("returning inspectorid: " + rs.getString("USERID"));
				return rs.getString("USERID");
			}

			if (rs != null) {
				rs.close();
			}

			logger.debug("returning inspectorid: -1");

			return "-1";
		} catch (Exception e) {
			logger.debug("Exception occured in getUserForActivity method of InspectionAgent. Message-" + e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	/**
	 * Get latest plan check
	 * 
	 * @param activityId
	 * @return
	 * @throws Exception
	 */
	public PlanCheck getLatestPlanCheck(int activityId) throws Exception {
		planCheck = new PlanCheck();
		RowSet rs = null;
		try {
			Wrapper db = new Wrapper();
			String sql = null;
			
				sql = "select PC.*, U.first_name || ' ' || U.last_name as name, S.pc_desc  from plan_check PC left outer join users U on U.userid = PC.engineer, lkup_pc_status S where PC.act_id=" + activityId + " AND PC.stat_code = S.pc_code order by plan_chk_date desc,plan_chk_id desc";
		
			logger.debug(sql);

			rs = db.select(sql);

			if (rs!=null && rs.next()) {
				planCheck.setPlanCheckId(rs.getInt("PLAN_CHK_ID"));
				planCheck.setPlanCheckDate(rs.getDate("plan_chk_date"));
				logger.debug("getLatestPlanCheck() .. Plan Check date is set to  .. " + StringUtils.cal2str(planCheck.getPlanCheckDate()));
				planCheck.setDate(StringUtils.date2str(rs.getDate("plan_chk_date")));
				logger.debug("getLatestPlanCheck() ..Plan Check String date .. " + planCheck.getDate());
				planCheck.setTrgdate(StringUtils.date2str(rs.getDate("target_date")));
				logger.debug("getLatestPlanCheck() ..Plan Check String target date .. " + planCheck.getTrgdate());
				planCheck.setCompdate(StringUtils.date2str(rs.getDate("completion_date")));
				logger.debug("getLatestPlanCheck() ..Plan Check String completion date .. " + planCheck.getCompdate());
				planCheck.setActivityId(StringUtils.s2i(rs.getString("act_id")));
				logger.debug("getLatestPlanCheck() ..Plan Check activity Id .. " + planCheck.getActivityId());
				planCheck.setStatusCode(StringUtils.s2i(rs.getString("stat_code")));
				logger.debug("getLatestPlanCheck() ..Plan Check Status .. " + planCheck.getStatusCode());

				PlanCheckStatus planCheckStatus = LookupAgent.getPlanCheckStatus(StringUtils.s2i(rs.getString("stat_code")));

				if (planCheckStatus == null) {
					planCheckStatus = new PlanCheckStatus();
				}

				planCheck.setStatusDesc(planCheckStatus.getDescription());
				logger.debug("getLatestPlanCheck() ..Plan Check Status Description is .. " + planCheck.getStatusDesc());
				planCheck.setEngineerName(rs.getString("name"));
				logger.debug("getLatestPlanCheck() ..Plan Check Enginner Name is ..  " + planCheck.getEngineerName());
				planCheck.setEngineer(StringUtils.s2i(rs.getString("engineer")));
				logger.debug("getLatestPlanCheck() ..Plan Check Enginner ..  " + planCheck.getEngineer());
				planCheck.setComments(rs.getString("comnt"));
				logger.debug("getLatestPlanCheck() ..Plan Check comnt  .. " + planCheck.getComments());
				planCheck.setCategoryCode(StringUtils.s2i(rs.getString("cat_code")));
				logger.debug("getLatestPlanCheck() ..Plan Check CategoryCode .. " + planCheck.getCategoryCode());
				planCheck.setTitleCode(StringUtils.s2i(rs.getString("title_code")));
				logger.debug("getLatestPlanCheck() .. Plan Check Title code .. " + planCheck.getTitleCode());
				planCheck.setCreatedBy(new AdminAgent().getUser(rs.getInt("created_by")));
				logger.debug("getLatestPlanCheck() .. Plan Check Created by .. " + planCheck.getCreatedBy());
				planCheck.setUpdatedBy(new AdminAgent().getUser(rs.getInt("updated_by")));
				logger.debug("getLatestPlanCheck() .. Plan Check Updated By " + planCheck.getUpdatedBy());
				logger.debug("getLatestPlanCheck() ..Plan Check Department ..  " + planCheck.getDeptDesc());
//				planCheck.setDeptDesc(rs.getString("department"));
				planCheck.setDeptDesc(LookupAgent.getDepartmentbasedOnId(rs.getString("department")).getDescription());
				
			}

			if (rs != null) {
				rs.close();
			}

			logger.debug("returning PlanCheck  ..");

			return planCheck;
		} catch (Exception e) {
			logger.error("Exception in getLatestPlanCheck method in PlanCheckAgent " + e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	/**
	 * Edit plan check
	 * 
	 * @param planCheck
	 * @return
	 * @throws Exception
	 */
	public int editPlanCheck(PlanCheck planCheck) throws AgentException {
		String sql = "";
		RowSet pcHistory =null;
		try {
			this.activityId = planCheck.getActivityId();
			Wrapper db = new Wrapper();

			db.beginTransaction();

			int oldPlanCheckProcessType = 0;
			int newPlanCheckProcessType = planCheck.getProcessType();

			int pcStatusCode = planCheck.getStatusCode();

			 pcHistory = db.select("SELECT * FROM PLAN_CHECK WHERE PLAN_CHK_ID = " + planCheck.getPlanCheckId());

		
				sql = "UPDATE PLAN_CHECK SET PLAN_CHK_DATE = to_date(" + StringUtils.checkString(StringUtils.cal2str(planCheck.getPlanCheckDate())) + ",'MM/DD/YYYY') ," + " TARGET_DATE = to_date(" + StringUtils.checkString(StringUtils.cal2str(planCheck.getTargetDate())) + ",'MM/DD/YYYY') ," + "COMPLETION_DATE = to_date(" + StringUtils.checkString(StringUtils.cal2str(planCheck.getCompletionDate())) + ",'MM/DD/YYYY') ," + "UNITS=" + planCheck.getUnitMinutes() + ",ENGINEER = " + planCheck.getEngineer() + " , STAT_CODE = " + planCheck.getStatusCode() + " , " + "COMNT = " + StringUtils.checkString(planCheck.getComments()) + ", UPDATED_BY = " + planCheck.getUpdatedBy().getUserId() + " , UPDAETD = current_timestamp , PROCESS_TYPE = "+planCheck.getProcessType() + "where plan_chk_id = " + planCheck.getPlanCheckId();
		
			logger.debug(sql);

			db.addBatch(sql);

			// Activity status is getting changed according to plan check status

			/*
			 * Code commented (as requested by Suresh)
			 * 
			 * /
			 * 
			 * /* if (pcStatusCode > 0) { sql = "update activity set status = (select act_status_code from lkup_pc_status where pc_code=" + pcStatusCode + ")  where act_id=" + activityId; logger.debug(sql); db.addBatch(sql); }
			 */
			/*
			 * Code to make an entry for plan check history
			 */

			int pcHistoryId = db.getNextId("PC_HISTORY_ID");
			if (pcHistory != null && pcHistory.next()) {

				/*if (pcHistory.getString("PROCESS_TYPE") != null) {
					oldPlanCheckProcessType = pcHistory.getInt("PROCESS_TYPE");
				}

				int daysToAdd = daysToAdd(newPlanCheckProcessType);
				if (newPlanCheckProcessType != Constants.PC_PROCESS_EXPRESS) {
					if (oldPlanCheckProcessType != newPlanCheckProcessType) {
						sql = "UPDATE PLAN_CHECK SET  PROCESS_TYPE    = '" + newPlanCheckProcessType + "', TARGET_DATE = (CREATED +" + daysToAdd + ") WHERE ACT_ID = " + activityId+ " and plan_chk_id = " + planCheck.getPlanCheckId();
						db.addBatch(sql);
					}
				} else {
					if (oldPlanCheckProcessType != newPlanCheckProcessType) {
						sql = "UPDATE PLAN_CHECK SET  PROCESS_TYPE    = '" + newPlanCheckProcessType + "'  WHERE ACT_ID = " + activityId + " and plan_chk_id = " + planCheck.getPlanCheckId();
						db.addBatch(sql);
					}
				}

				logger.debug(sql);*/

				sql = "INSERT INTO PLAN_CHECK_HISTORY (PLAN_CHK_HISTORY_ID,PLAN_CHK_ID,ACT_ID,PLAN_CHK_DATE,STAT_CODE,ENGINEER,COMNT,CAT_CODE,TITLE_CODE,UPDATED_BY,UPDAETD,TARGET_DATE,COMPLETION_DATE,UNITS,PROCESS_TYPE,DEPARTMENT)" + "    VALUES (" + pcHistoryId + "," + pcHistory.getInt("PLAN_CHK_ID") + "," + pcHistory.getInt("ACT_ID") + ",";

				if (pcHistory.getDate("PLAN_CHK_DATE") != null) {
					
						sql = sql + "to_date('" + pcHistory.getDate("PLAN_CHK_DATE") + "','YYYY-MM-DD'),";
						
					
				} else {
					sql = sql + "null,";
				}
				sql = sql + pcHistory.getInt("STAT_CODE") + "," + pcHistory.getInt("ENGINEER") + ",";

				if (pcHistory.getString("COMNT") != null) {
					sql = sql + StringUtils.checkString(pcHistory.getString("COMNT")) + ",";
				} else {
					sql = sql + "null,";
				}
				sql = sql + pcHistory.getInt("CAT_CODE") + "," + pcHistory.getInt("TITLE_CODE") + "," + pcHistory.getInt("UPDATED_BY") + ",current_timestamp,";

				if (pcHistory.getDate("TARGET_DATE") != null) {
				
						sql = sql + "to_date('" + pcHistory.getDate("TARGET_DATE") + "','YYYY-MM-DD'),";
						
					
				} else {
					sql = sql + "null,";
				}

				if (pcHistory.getDate("COMPLETION_DATE") != null) {
					
						sql = sql + "to_date('" + pcHistory.getDate("COMPLETION_DATE") + "','YYYY-MM-DD'),";
						
					
				} else {
					sql = sql + "null,";
				}

				sql = sql + pcHistory.getDouble("UNITS") + ",";

				if (pcHistory.getString("PROCESS_TYPE") != null) {
					sql = sql + "'" + pcHistory.getString("PROCESS_TYPE") + "',";
				} else {
					sql = sql + "null,";
				}

				if (pcHistory.getString("DEPARTMENT") != null) {
					sql = sql + "'" + pcHistory.getString("DEPARTMENT") + "')";
				} else {
					sql = sql + "null)";
				}

				logger.debug("pcHistory SQL::" + sql);

				db.addBatch(sql);

			}
			db.executeBatch();
			db.commitTransaction();
			return planCheck.getPlanCheckId();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			//System.out.println(sql);
			throw new AgentException("", e);
		}//Closing RowSet added by Manjuprasad
		 finally{
	            if(pcHistory != null){
	                try {
	                	pcHistory.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }

	}

	/*private int daysToAdd(int processType) {
		int days = 0;
		if (processType == Constants.PC_PROCESS_REGULAR) {
			days = Constants.PC_DAYS_TO_ADD_REGULAR;
		} else if (processType == Constants.PC_PROCESS_FAST_TRACK) {
			days = Constants.PC_DAYS_TO_ADD_FAST_TRACK;
		} else if (processType == Constants.PC_PROCESS_OTC) {
			days = Constants.PC_DAYS_TO_ADD_OTC;
		} else {
			return 0;
		}
		return days;
	}*/

	/**
	 * Add Plan check
	 * 
	 * @param planCheck
	 * @return
	 * @throws Exception
	 */
	public int addPlanCheck(PlanCheck planCheck) throws Exception {
		logger.info("addPlanCheck(planCheck)");
		Wrapper db = new Wrapper();
		ActivityHistory status = new ActivityHistory();
		int actStatId = db.getNextId("act_stat_id");
		try {
			// add the PlanCheck to the database

			String sql = "";
			activityId = planCheck.getActivityId();

			int pcStatusCode = planCheck.getStatusCode();

			logger.debug("*****************" + planCheck.getProcessType());
		/*	logger.debug("*****Constants.PC_PROCESS_EXPRESS)******" + Constants.PC_PROCESS_EXPRESS);
			if (planCheck.getProcessType() == Constants.PC_PROCESS_EXPRESS) {
				logger.debug("*****************" + planCheck.getProcessType());
			}

			if (planCheck.getProcessType() == Constants.PC_PROCESS_EXPRESS) {
				int daysToAdd = daysToAdd(planCheck.getProcessType());
				sql = "UPDATE PLAN_CHECK SET  PROCESS_TYPE    = '" + planCheck.getProcessType() + "' WHERE ACT_ID = " + activityId;
			} else {
				int daysToAdd = daysToAdd(planCheck.getProcessType());
				sql = "UPDATE PLAN_CHECK SET  PROCESS_TYPE    = '" + planCheck.getProcessType() + "', TARGET_DATE = (CREATED +" + daysToAdd + ") WHERE ACT_ID = " + activityId;
			}

			logger.debug("update Plan Check query ::" + sql);
			db.update(sql);*/

			planCheckId = db.getNextId("PLANCHECK_ID");
			db.beginTransaction();
			
				sql = "Insert into plan_check(plan_chk_id,plan_chk_date,act_id,engineer, stat_code,cat_code,title_code,comnt,created_by,created,updated_by,updaetd,target_date,completion_date,units,department,PROCESS_TYPE) " + " values(" + planCheckId + ",to_date('" + (StringUtils.cal2str(planCheck.getPlanCheckDate())) + "','MM/DD/YYYY')," + activityId + "," + planCheck.getEngineer() + "," + pcStatusCode + "," + planCheck.getCategoryCode() + "," + planCheck.getTitleCode() + "," + StringUtils.checkString(planCheck.getComments()) + "," + planCheck.getCreatedBy().getUserId() + "," + "current_timestamp" + "," + planCheck.getUpdatedBy().getUserId() + "," + "current_timestamp" + ",to_date('" + (StringUtils.cal2str(planCheck.getTargetDate())) + "','MM/DD/YYYY ')," + "to_date('" + (StringUtils.cal2str(planCheck.getCompletionDate())) + "','MM/DD/YYYY')," + StringUtils.checkString(planCheck.getUnitMinutes()) + "," + StringUtils.checkString(planCheck.getDeptDesc()) + "," + planCheck.getProcessType() + " )";
				
			
			logger.debug(sql);
			db.addBatch(sql);

			// Activity status is getting changed according to plan check status
			/*
			 * if (pcStatusCode > 0) { sql = "update activity set status = (select act_status_code from lkup_pc_status where pc_code=" + pcStatusCode + ")  where act_id=" + activityId; logger.debug(sql); db.addBatch(sql); }
			 */

			// logger.debug("addPlanCheck() -- update activity Status .. " + sql);

			/*
			 * sql = "insert into act_status_history (act_stat_id,act_id,status,changed_dt,updated_by,stat_exp_dt,permit_exp_dt,comments,updated)values("+actStatId+","+activityId + ","+pcStatusCode+",current_date," + planCheck.getCreatedBy().getUserId()+",current_date,"+"current_date,"+StringUtils.checkString("Status while adding plancheck") + ",current_timestamp)"; db.addBatch(sql); logger.debug(sql); sql = "update act_status_history set status = (select act_status_code from lkup_pc_status where pc_code=" + pcStatusCode + ")  where act_stat_id=" +actStatId ; logger.debug(sql); db.addBatch(sql);
			 */

			db.executeBatch();
			db.commitTransaction();

			return planCheckId;
		} catch (Exception e) {
			planCheckId = -1;
			logger.error("Exception When Adding Plan Check....." + e.getMessage());
			throw e;
		}
	}

	public void addBasicPlanChecks(List<PlanCheck> pcList) throws AgentException {

		logger.debug("Entering addBasicPlanChecks...");
		String sql = null;
	
			sql = "INSERT INTO PLAN_CHECK (PLAN_CHK_ID, PLAN_CHK_DATE, ACT_ID, STAT_CODE, ENGINEER, COMNT, CAT_CODE, TITLE_CODE, CREATED_BY, CREATED, UPDATED_BY, UPDAETD, " + " TARGET_DATE, COMPLETION_DATE,UNITS, DEPARTMENT,PROCESS_TYPE) VALUES (?, CURRENT_DATE, ?, ?,0,?, 0, 0, ?, CURRENT_DATE, ?, CURRENT_DATE, ?, CURRENT_DATE, 0, ?,?)";
			
		
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		try {
			connection = wrapper.getConnection();

			PlanCheck pc = null;

			pstmt = connection.prepareStatement(sql);
			logger.debug("sql for insert "+sql);
			for (int i = 0; i < pcList.size(); i++) {

				pc = pcList.get(i);

				int plancheckId = wrapper.getNextId("PLANCHECK_ID");

				pstmt.setInt(1, plancheckId);

				pstmt.setInt(2, pc.getActivityId());

				if (pc.getStatusCode() == 0) {
					pstmt.setInt(3, Constants.BUILDING_PLAN_CHECK_STATUS_PC_SUBMITTED);
				} else {
					pstmt.setInt(3, pc.getStatusCode());
				}

				pstmt.setString(4, pc.getComments());

				pstmt.setInt(5, pc.getCreatedBy().getUserId());

				pstmt.setInt(6, pc.getCreatedBy().getUserId());

				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.DATE, Constants.PC_DAYS_TO_ADD_REGULAR);

				Date date = new Date(cal.getTimeInMillis());

				pstmt.setDate(7, date);

				pstmt.setString(8, pc.getDeptDesc());

				pstmt.setInt(9, Constants.PLANCHECK_DEFAULT_PROCESS_TYPE);

				pstmt.addBatch();
			}

			pstmt.executeBatch();
		} catch (Exception e) {
			logger.error("error while inserting Plan check", e);
			throw new AgentException("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}

			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
		}

	}

	/**
	 * Save Plan check
	 * 
	 * @param planCheck
	 * @return
	 * @throws Exception
	 */
	public int savePlanCheck(PlanCheck planCheck) throws Exception {
		try {
			Wrapper db = new Wrapper();
			String sql = "";
			db.beginTransaction();
			
				sql = "update plan_check set plan_check_date= to_date('" + StringUtils.checkString(StringUtils.cal2str(planCheck.getPlanCheckDate())) + ",'MM/DD/YYYY')," + "stat_code=" + planCheck.getStatusCode() + ",cat_code=" + planCheck.getCategoryCode() + ",title_code=" + planCheck.getTitleCode() + "," + "comnt=" + StringUtils.checkString(planCheck.getComments()) + "," + "update_by=" + planCheck.getUpdatedBy().getUserId() + "," + "updated=" + "current_timestamp" + "," + " where plan_chk_id=" + planCheck.getPlanCheckId();
				
			
			db.addBatch(sql);
			logger.debug("save Plan Check Sql in savePlanCheck Method is " + sql);

			// Activity status is getting changed according to plan check status
			if (planCheck.getStatusCode() > 0) {
				sql = "update activity set status = (select act_status_code from lkup_pc_status where pc_code=" + planCheck.getStatusCode() + ")  where act_id=" + activityId;
				db.addBatch(sql);
			}

			logger.debug("savePlanCheck() -- update activity Status .. " + sql);
			db.executeBatch();
			db.commitTransaction();

			return planCheck.getPlanCheckId();
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update PlanCheck  :" + e.getMessage());
			throw e;
		}
	}

	/**
	 * Get engineers
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getEngineerUsers() throws Exception {
		logger.info("getEngineerUsers()");

		List engineerUsers = new ArrayList();
		EngineerUser engineerUser = null;
		ResultSet rs = null;
		try {
			// 19 - Plan Review Engineer
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT U.USERID,U.FIRST_NAME,U.LAST_NAME");
			sql.append(" FROM USERS U JOIN USER_GROUPS UG ON U.USERID = UG.USER_ID");
			sql.append(" WHERE UG.GROUP_ID IN (" + Constants.GROUPS_PLAN_REVIEW_ENGINEER + ")  AND U.ACTIVE='Y' ORDER BY U.FIRST_NAME");

			logger.debug("inspection users ########### " + sql);
			engineerUser = new EngineerUser(0, "Unassigned");
			engineerUsers.add(engineerUser);

			rs = new Wrapper().select(sql.toString());

			while (rs!=null && rs.next()) {
				engineerUser = new EngineerUser(rs.getInt("USERID"), rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
				engineerUsers.add(engineerUser);
			}

			if (rs != null) {
				rs.close();
			}

			return engineerUsers;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	/**
	 * Get engineers
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List getEngineerUsers(int deptId) throws Exception {
		logger.info("getEngineerUsers()");

		if (deptId == 0) {
			return getEngineerUsers();
		}

		List engineerUsers = new ArrayList();
		EngineerUser engineerUser = null;
		ResultSet rs = null;
		try {
			// 19 - Plan Review Engineer
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT U.USERID,U.FIRST_NAME,U.LAST_NAME");
			sql.append(" FROM USERS U JOIN USER_GROUPS UG ON U.USERID = UG.USER_ID");
			sql.append(" WHERE UG.GROUP_ID IN (" + Constants.GROUPS_PLAN_REVIEW_ENGINEER + ","+ Constants.GROUPS_PLAN_CHECK_MANAGER+") and U.DEPT_ID = " + deptId + " and U.ACTIVE='Y' ORDER BY U.FIRST_NAME");

			logger.debug("plan review Engineer ::" + sql);
			engineerUser = new EngineerUser(0, "Unassigned");
			engineerUsers.add(engineerUser);

			rs = new Wrapper().select(sql.toString());

			while (rs!=null && rs.next()) {
				engineerUser = new EngineerUser(rs.getInt("USERID"), rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
				engineerUsers.add(engineerUser);
			}

			if (rs != null) {
				rs.close();
			}

			return engineerUsers;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	// get the dafault Plan check Engineer From Project Level Process team ( Engineer , Supervisor or Inspector ) of one plan check
	public int getPlanReviewEngineer(String activityId) throws Exception {
		int userId = 0;
		RowSet rs = null;
		try {
			Wrapper db = new Wrapper();
			String sql = "select userid  from process_team where psa_type='P' and group_id = 19 and psa_id = (select proj_id from v_psa_list where act_id=" + activityId + ") order by created";
			logger.debug("The select Sql of plan Check  is " + sql);

			rs = db.select(sql);

			if (rs!=null && rs.next()) {
				userId = rs.getInt("userId");
			}

			rs.close();

			return userId;
		} catch (Exception e) {
			logger.error("Exception thrown while trying to update PlanCheck  :" + e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	public static String getDepartments(int userId) throws Exception {
		logger.debug("getDepartments(" + userId + ")");

		String deptType = "";
		RowSet rs = null;
		try {
			String sql = "select DESCRIPTION from department where DEPT_ID= (select DEPT_ID from users where USERID=" + userId + " )";
			logger.debug(" getDepartments  -- " + sql);

			rs = new Wrapper().select(sql);

			while (rs!=null && rs.next()) {

				deptType = rs.getString("DESCRIPTION");
				logger.debug("departments" + rs.getString("DESCRIPTION"));

			}
			rs.close();

			return deptType;
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}
	}

	public List getLatestPlanCheckList(int activityId) throws Exception {

		List planCheckList = new ArrayList();
		RowSet rs = null;
		try {
			Wrapper db = new Wrapper();
			String sql = null;
			
				sql = "select * From (Select PC.plan_chk_id,PC.department,PC.stat_code,PC.plan_chk_date,PC.target_date,PC.completion_date,PC.comnt,U.first_name || ' ' || U.last_name as name, S.pc_desc  from plan_check PC left outer join users U on U.userid = PC.engineer, lkup_pc_status S  where PC.act_id=" + activityId + " AND PC.stat_code = S.pc_code order by plan_chk_id desc) where rownum < 4";
				
		
			logger.debug(sql);

			rs = db.select(sql);

			while (rs!=null && rs.next()) {
				PlanCheckForm planCheckForm = new PlanCheckForm();
				String name = rs.getString("name");
				if (name == null || name.trim().equalsIgnoreCase("")) {
					name = Constants.USER_UNASSIGNED;
				}
				planCheckForm.setEnginner(name);
				// logger.debug("getLatestPlanCheck() ..Plan Check Enginner Name is ..  " + planCheck.getEngineerName());
//				planCheckForm.setDepartment(rs.getString("department"));
				planCheckForm.setDepartment(LookupAgent.getDepartmentbasedOnId(rs.getString("department")).getDescription());
				
				// logger.debug("getLatestPlanCheck() ..Plan Check Department ..  " + planCheck.getDeptDesc());
				PlanCheckStatus planCheckStatus = LookupAgent.getPlanCheckStatus(StringUtils.s2i(rs.getString("stat_code")));

				if (planCheckStatus == null) {
					planCheckStatus = new PlanCheckStatus();
				}
				planCheckForm.setStatusDesc(planCheckStatus.getDescription());
				// logger.debug("getLatestPlanCheck() ..Plan Check Status Description is .. " + planCheck.getStatusDesc());
				planCheckForm.setPlanCheckDate(StringUtils.date2str(rs.getDate("plan_chk_date")));
				// logger.debug("getLatestPlanCheck() .. Plan Check date is set to  .. " + StringUtils.cal2str(planCheck.getPlanCheckDate()));

				planCheckForm.setTargetDate(StringUtils.date2str(rs.getDate("target_date")));
				// logger.debug("getLatestPlanCheck() ..Plan Check String target date .. " + planCheck.getTrgdate());
				planCheckForm.setCompletionDate(StringUtils.date2str(rs.getDate("completion_date")));
				// logger.debug("getLatestPlanCheck() ..Plan Check String completion date .. " + planCheck.getCompdate());

				planCheckForm.setComments(rs.getString("comnt"));
				// logger.debug("getLatestPlanCheck() ..Plan Check comnt  .. " + planCheck.getComments());
				planCheckList.add(planCheckForm);

			}
			rs.close();
			logger.debug("returning requestList of size " + planCheckList.size());
			return planCheckList;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Exception occured in planCheckList method . Message-" + e.getMessage());
			throw e;
		}		//Closing RowSet added by Siva kumari
		finally {
		       if(rs != null){
		           try {
		                 rs.close();
		           } catch (SQLException Ignore) {
		                 logger.warn("Not able to close the Rowset");
		           }
		      }
			}

	}

	public List<PlanCheck> getAllPlanChecks(String engineerId, int moduleId, String deptDesc, String fromDate, String toDate, int deptId) throws AgentException {

		/*
		 * engineerId is null for all the plan checks in system.
		 */

		String sql = "";
		if (engineerId != null && deptId <= 0) {
			sql = "SELECT PC.PLAN_CHK_ID, PC.PLAN_CHK_DATE,PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.TARGET_DATE,PC.ACT_ID, PC.STAT_CODE, PC.ENGINEER, PC.COMNT,PC.DEPARTMENT,LPC.PC_DESC,U.FIRST_NAME,U.LAST_NAME ,ACT.ACT_NBR,(LA.STR_NO ||' '|| V.STREET_NAME) as ADDRESS_FORMAT, LAT.DESCRIPTION AS ACTTYPE ,ACT.DESCRIPTION AS ACTDESC, LAS.DESCRIPTION AS ACTSTATUS, D.DEPT_CODE  FROM PLAN_CHECK PC  LEFT JOIN LKUP_PC_STATUS LPC ON PC.STAT_CODE = LPC.PC_CODE left join USERS U  on PC.ENGINEER = U.USERID LEFT JOIN ACTIVITY ACT ON PC.ACT_ID= ACT.ACT_ID JOIN LSO_ADDRESS LA ON LA.ADDR_ID=ACT.ADDR_ID JOIN V_STREET_LIST V ON V.STREET_ID=LA.STREET_ID LEFT JOIN LKUP_ACT_ST las on ACT.STATUS = LAS.STATUS_ID LEFT JOIN LKUP_ACT_TYPE LAT ON ACT.ACT_TYPE = LAT.TYPE LEFT JOIN LKUP_PC_PROCESS_TYPE LPT ON LPT.PC_PROCESS_TYPE_ID = PC.PROCESS_TYPE LEFT JOIN DEPARTMENT D ON LAT.DEPT_ID=D.DEPT_ID where ENGINEER = " + engineerId + " AND LPC.ACTIVE = 'Y' AND LPC.SHOW_PC_TAB = 'Y' AND (PC.PLAN_CHK_DATE >= TO_DATE("+StringUtils.checkString(fromDate)+",'MM/DD/YYYY'))"+" AND (PC.PLAN_CHK_DATE <= TO_DATE("+StringUtils.checkString(toDate)+",'MM/DD/YYYY')) ORDER BY   U.FIRST_NAME,U.LAST_NAME,pc.PLAN_CHK_DATE, DEPARTMENT ASC, ACT_NBR asc, TARGET_DATE ASC"; 
		} else if(engineerId != null && deptId > 0 ){
			sql = "SELECT PC.PLAN_CHK_ID, PC.PLAN_CHK_DATE,PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.TARGET_DATE,PC.ACT_ID, PC.STAT_CODE, PC.ENGINEER, PC.COMNT,PC.DEPARTMENT,LPC.PC_DESC,U.FIRST_NAME,U.LAST_NAME ,ACT.ACT_NBR,(LA.STR_NO ||' '|| V.STREET_NAME) as ADDRESS_FORMAT, LAT.DESCRIPTION AS ACTTYPE ,ACT.DESCRIPTION AS ACTDESC, LAS.DESCRIPTION AS ACTSTATUS, D.DEPT_CODE  FROM PLAN_CHECK PC  LEFT JOIN LKUP_PC_STATUS LPC ON PC.STAT_CODE = LPC.PC_CODE left join USERS U  on PC.ENGINEER = U.USERID LEFT JOIN ACTIVITY ACT ON PC.ACT_ID= ACT.ACT_ID JOIN LSO_ADDRESS LA ON LA.ADDR_ID=ACT.ADDR_ID JOIN V_STREET_LIST V ON V.STREET_ID=LA.STREET_ID LEFT JOIN LKUP_ACT_ST las on ACT.STATUS = LAS.STATUS_ID LEFT JOIN LKUP_ACT_TYPE LAT ON ACT.ACT_TYPE = LAT.TYPE LEFT JOIN LKUP_PC_PROCESS_TYPE LPT ON LPT.PC_PROCESS_TYPE_ID = PC.PROCESS_TYPE LEFT JOIN DEPARTMENT D ON LAT.DEPT_ID=D.DEPT_ID where ENGINEER = " + engineerId + " AND LPC.ACTIVE = 'Y' AND LPC.SHOW_PC_TAB = 'Y' AND (PC.PLAN_CHK_DATE >= TO_DATE("+StringUtils.checkString(fromDate)+",'MM/DD/YYYY'))"+" AND (PC.PLAN_CHK_DATE <= TO_DATE("+StringUtils.checkString(toDate)+",'MM/DD/YYYY')) AND (PC.DEPARTMENT = '"+deptId+"' OR LAT.DEPT_ID = "+deptId+") ORDER BY   U.FIRST_NAME,U.LAST_NAME,pc.PLAN_CHK_DATE, DEPARTMENT ASC, ACT_NBR asc, TARGET_DATE ASC"; 
		}else {
			sql = "SELECT PC.PLAN_CHK_ID, PC.PLAN_CHK_DATE,PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.TARGET_DATE,PC.ACT_ID, PC.STAT_CODE, PC.ENGINEER, PC.COMNT,PC.DEPARTMENT,LPC.PC_DESC,U.FIRST_NAME,U.LAST_NAME ,ACT.ACT_NBR,(LA.STR_NO ||' '|| V.STREET_NAME) as ADDRESS_FORMAT, LAT.DESCRIPTION AS ACTTYPE ,ACT.DESCRIPTION AS ACTDESC, LAS.DESCRIPTION AS ACTSTATUS, D.DEPT_CODE  FROM PLAN_CHECK PC  LEFT JOIN LKUP_PC_STATUS LPC ON PC.STAT_CODE = LPC.PC_CODE left join USERS U  on PC.ENGINEER = U.USERID LEFT JOIN ACTIVITY ACT ON PC.ACT_ID= ACT.ACT_ID JOIN LSO_ADDRESS LA ON LA.ADDR_ID=ACT.ADDR_ID JOIN V_STREET_LIST V ON V.STREET_ID=LA.STREET_ID LEFT JOIN LKUP_ACT_ST las on ACT.STATUS = LAS.STATUS_ID LEFT JOIN LKUP_ACT_TYPE LAT ON ACT.ACT_TYPE = LAT.TYPE LEFT JOIN LKUP_PC_PROCESS_TYPE LPT ON LPT.PC_PROCESS_TYPE_ID = PC.PROCESS_TYPE LEFT JOIN DEPARTMENT D ON LAT.DEPT_ID=D.DEPT_ID WHERE LPC.ACTIVE = 'Y' AND LPC.SHOW_PC_TAB = 'Y' AND (PC.PLAN_CHK_DATE >= TO_DATE("+StringUtils.checkString(fromDate)+",'MM/DD/YYYY'))"+" AND (PC.PLAN_CHK_DATE <= TO_DATE("+StringUtils.checkString(toDate)+",'MM/DD/YYYY')) AND (PC.DEPARTMENT = '"+deptId+"' OR LAT.DEPT_ID = "+deptId+") ORDER BY   U.FIRST_NAME,U.LAST_NAME,pc.PLAN_CHK_DATE, DEPARTMENT ASC, ACT_NBR asc, TARGET_DATE ASC"; 
		}

		Wrapper db = new Wrapper();
		RowSet rs = null;
		logger.debug(sql);
		PlanCheck pc = null;

		List<PlanCheck> pcList = new ArrayList<PlanCheck>();

		try {
			rs = db.select(sql);
			if (rs != null) {
				while (rs.next()) {
					pc = new PlanCheck();

					pc.setActivityId(rs.getInt("ACT_ID"));

					pc.setComments(rs.getString("COMNT"));

					Calendar c = StringUtils.dbDate2cal(rs.getString("PLAN_CHK_DATE"));

					pc.setPlanCheckDate(c);

					pc.setDate(StringUtils.cal2str(c));

					Calendar targetDateCal = StringUtils.dbDate2cal(rs.getString("TARGET_DATE"));

					pc.setTargetDate(targetDateCal);

					pc.setTrgdate(StringUtils.cal2str(targetDateCal));

					if (rs.getInt("ENGINEER") != 0 || rs.getInt("ENGINEER") == -1) {
						pc.setEngineerName(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
					} else {
						pc.setEngineerName("Unassigned");
					}

					pc.setPlanCheckId(rs.getInt("PLAN_CHK_ID"));

					pc.setStatusDesc(rs.getString("PC_DESC"));

					pc.setEngineer(rs.getInt("ENGINEER"));

					pc.setActNbr(rs.getString("ACT_NBR"));

					pc.setAddress(rs.getString("ADDRESS_FORMAT"));

					pc.setProcessTypeDesc(rs.getString("PC_PROCESS_TYPE"));
					pc.setActDesc(rs.getString("ACTDESC"));
					pc.setActStatus(rs.getString("ACTSTATUS"));
					pc.setActType(rs.getString("ACTTYPE"));
//					pc.setDepartment(rs.getString("DEPARTMENT"));
					pc.setDepartment(LookupAgent.getDepartmentbasedOnId(rs.getString("DEPARTMENT")).getDescription());
					
					pc.setActDept(rs.getString("DEPT_CODE"));
					pcList.add(pc);

				}
			}
			return pcList;
		} catch (Exception e) {
			logger.error("error while getting Plan checks:", e);
			throw new AgentException("", e);
		}//Closing RowSet added by Manjuprasad
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }

	}

	public void reAssignPLanCheck(List<PlanCheck> pcList, int engineerId,int deptId) throws AgentException {

		String sql = "UPDATE PLAN_CHECK SET ENGINEER = ? ,DEPARTMENT = ? WHERE PLAN_CHK_ID = ?";

		logger.debug("update sql... "+sql);
		// Wrapper
		Wrapper wrapper = new Wrapper();

		// Connection
		Connection connection = null;

		PreparedStatement pstmt = null;

		try {
			connection = wrapper.getConnection();

			PlanCheck pc = null;

			pstmt = connection.prepareStatement(sql);

			for (int i = 0; i < pcList.size(); i++) {

				pc = pcList.get(i);

				pstmt.setInt(1, engineerId);
				pstmt.setInt(2, deptId);
				pstmt.setInt(3, pc.getPlanCheckId());

				pstmt.addBatch();
			}
			pstmt.executeBatch();

		} catch (Exception e) {
			logger.error("error while reAssigning plan check", e);
			throw new AgentException("", e);
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException Ignore) {
				}

			}
			if (pstmt != null) {
				try {
					pstmt.close();
				} catch (SQLException Ignore) {
				}
			}
		}

	}

	public void deletePlanChecks(List<PlanCheck> pcToDelete) throws AgentException {
		if (pcToDelete != null && pcToDelete.size() > 0) {

			String planCheckIds = "";
			for (int i = 0; i < pcToDelete.size(); i++) {
				planCheckIds = planCheckIds + pcToDelete.get(i).getPlanCheckId() + ",";
			}
			if (planCheckIds.indexOf(",") != -1) {
				planCheckIds = planCheckIds.substring(0, planCheckIds.lastIndexOf(","));
			}
			Wrapper db = new Wrapper();
			db.beginTransaction();
			String sql = "DELETE FROM PLAN_CHECK_HISTORY where PLAN_CHK_ID in (" + planCheckIds + ")";

			logger.debug(sql);

			db.addBatch(sql);

			sql = "DELETE FROM PLAN_CHECK where PLAN_CHK_ID in (" + planCheckIds + ")";

			logger.debug(sql);

			db.addBatch(sql);

			try {
				db.executeBatch();
				db.commitTransaction();
			} catch (Exception e) {
				db.rollbackTransaction();
				logger.error("", e);
				throw new AgentException("", e);
			}
		}
		return;
	}

	public ActivityAttributes getPlanCheckCounts(int activityId) throws AgentException {
		Wrapper db = new Wrapper();
		ActivityAttributes attr = new ActivityAttributes();
		String sql = null;
		
			sql = "SELECT (SELECT  COUNT (*)  FROM activity a, plan_check pc WHERE a.act_id = pc.act_id AND a.act_id=" + activityId + ") PLANCHECK_TOTAL, " + "(SELECT  COUNT (*)  FROM activity a, plan_check pc WHERE a.act_id = pc.act_id AND pc.STAT_CODE in (select PC_CODE from lkup_PC_status where ACTIVE = 'Y')  AND a.act_id=" + activityId + ") PLANCHECK_OPEN " + "FROM DUAL";
			
	
		logger.debug("plan check count sql::" + sql);
		RowSet rs = null;
		try {
			rs = db.select(sql);
			if (rs != null && rs.next()) {
				attr.setTotalPlanCheck(rs.getInt("PLANCHECK_TOTAL"));
				attr.setOpenPlanCheck(rs.getInt("PLANCHECK_OPEN"));
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}//Closing RowSet added by Manjuprasad
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }

		return attr;
	}
	
	public List<Integer> getPCId(String activityId,List sorting)  throws AgentException {
		List<Integer> PcId = new ArrayList<Integer>();
		Wrapper db = new Wrapper();
		//db.beginTransaction();

//		String sql = "SELECT * FROM PLAN_CHECK PC WHERE ACT_ID in (" + activityId + ") ";
		String sql =null;
	
			sql ="select PC.plan_chk_id, PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.plan_chk_date, PC.comnt, U.first_name || ' ' || U.last_name as name, S.pc_desc,PC.target_date, PC.completion_date,PC.department  from plan_check PC left outer join LKUP_PC_PROCESS_TYPE LPT ON PC.PROCESS_TYPE = LPT.PC_PROCESS_TYPE_ID left outer join users U on U.userid = PC.engineer, lkup_pc_status S where PC.act_id=" + activityId + " AND PC.stat_code = S.pc_code"; 
			
		
		if(sorting != null && sorting.size() >0){
			//List sorting = cipSearch.getSorting();
			sql = sql + " Order by ";
			for(int i=0;i<sorting.size();i++){
				sql = sql + sorting.get(i).toString() + ",";
			}
			sql = sql.substring(0,sql.lastIndexOf(","));
		}else{
			sql = sql + " order by PC.target_date";
		}

		logger.debug("getPCId sql::" + sql);
		RowSet rs = null;
		try {
			rs = db.select(sql);
			if (rs != null ) {
				while (rs.next()) {
				PcId.add(rs.getInt("PLAN_CHK_ID"));
			}}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}//Closing RowSet added by Arjun
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }
		
		return PcId;
		
	}
	
	
	public List<Integer> getPCHistoryId(String activityId)  throws AgentException {
		List<Integer> PcId = new ArrayList<Integer>();
		Wrapper db = new Wrapper();
		//db.beginTransaction();
		String sql = "SELECT DISTINCT PLAN_CHK_ID FROM PLAN_CHECK_HISTORY WHERE ACT_ID in (" + activityId + ")";
		
		logger.debug("getPCHistoryId sql::" + sql);
		
		RowSet rs = null;
		try {
			rs = db.select(sql);
			if (rs != null ) {
			while (rs.next()) {
				PcId.add(rs.getInt("PLAN_CHK_ID"));
				
			}
				
			}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}//Closing RowSet added by Arjun
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }
		
		return PcId;
		
	}
	
	public List<Integer> getPCIdBasedOnEngineerID(int engineerId,String activityId,List sorting)  throws AgentException {
		List<Integer> PcId = new ArrayList<Integer>();
		Wrapper db = new Wrapper();
		//db.beginTransaction();

//		String sql = "SELECT * FROM PLAN_CHECK PC WHERE ACT_ID in (" + activityId + ") ";
		String sql =null;
	
			sql ="select PC.plan_chk_id, PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.plan_chk_date, PC.comnt, U.first_name || ' ' || U.last_name as name, S.pc_desc,PC.target_date, PC.completion_date,PC.department  from plan_check PC left outer join LKUP_PC_PROCESS_TYPE LPT ON PC.PROCESS_TYPE = LPT.PC_PROCESS_TYPE_ID left outer join users U on U.userid = PC.engineer, lkup_pc_status S where PC.act_id=" + activityId + " AND PC.stat_code = S.pc_code AND PC.engineer = "+engineerId; 
			
		
		if(sorting != null && sorting.size() >0){
			//List sorting = cipSearch.getSorting();
			sql = sql + " Order by ";
			for(int i=0;i<sorting.size();i++){
				sql = sql + sorting.get(i).toString() + ",";
			}
			sql = sql.substring(0,sql.lastIndexOf(","));
		}else{
			sql = sql + " order by PC.target_date";
		}

		logger.debug("getPCId sql::" + sql);
		RowSet rs = null;
		try {
			rs = db.select(sql);
			if (rs != null ) {
				while (rs.next()) {
				PcId.add(rs.getInt("PLAN_CHK_ID"));
			}}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}//Closing RowSet added by Arjun
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }
		
		return PcId;
		
	}

	public List<Integer> getPCIdBasedOnDept(int deptId, String strActivityId, List sorting) throws AgentException {
		List<Integer> PcId = new ArrayList<Integer>();
		Wrapper db = new Wrapper();
		//db.beginTransaction();

//		String sql = "SELECT * FROM PLAN_CHECK PC WHERE ACT_ID in (" + activityId + ") ";
		String sql =null;
	
			sql ="select PC.PLAN_CHK_ID, PC.PROCESS_TYPE,LPT.PC_PROCESS_TYPE,PC.plan_chk_date, PC.comnt, U.first_name || ' ' || U.last_name as name, S.pc_desc,PC.target_date, PC.completion_date,PC.department  from plan_check PC left outer join LKUP_PC_PROCESS_TYPE LPT ON PC.PROCESS_TYPE = LPT.PC_PROCESS_TYPE_ID left outer join users U on U.userid = PC.engineer, lkup_pc_status S where PC.act_id=" + strActivityId + " AND PC.stat_code = S.pc_code AND UPPER(PC.DEPARTMENT) = '"+deptId+"'"; 
			
		
		if(sorting != null && sorting.size() >0){
			//List sorting = cipSearch.getSorting();
			sql = sql + " Order by ";
			for(int i=0;i<sorting.size();i++){
				sql = sql + sorting.get(i).toString() + ",";
			}
			sql = sql.substring(0,sql.lastIndexOf(","));
		}else{
			sql = sql + " order by PC.target_date";
		}

		logger.debug("getPCId sql::" + sql);
		RowSet rs = null;
		try {
			rs = db.select(sql);
			if (rs != null ) {
				while (rs.next()) {
				PcId.add(rs.getInt("PLAN_CHK_ID"));
			}}
		} catch (Exception e) {
			logger.error("", e);
			throw new AgentException("", e);
		}//Closing RowSet added by Arjun
		 finally{
	            if(rs != null){
	                try {
	                      rs.close();
	                } catch (SQLException Ignore) {
	                      logger.warn("Not able to close the Rowset");
	                }
	          }
	    }
		
		return PcId;

	}
	
}

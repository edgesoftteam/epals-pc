﻿/**
 * Copyright (c) 2013, CKSource - Sunil. All rights reserved.
 * For contacting, http://www.edgesoftinc.com or sunvoyage
 */

/**
 * @fileOverview The "eslink" plugin. It wraps the selected block level elements with a 'eslink' element with specified styles and attributes.
 *
 */

(function () {
	var field ='';
	
	//var id = getUrlVars()['FORM_ID'];
	
	
	CKEDITOR.plugins.add('eslink',
    {
        //requires: ['eslink.jsp'],
    
    	    init: function (editor) {
        	
            var me = this;
         
            CKEDITOR.dialog.add('eslinkDialog', function (targetEditor) {
            	return {
                    title: 'Fields',
                    minWidth: 695,
                    minHeight: 300,
                    contents:
                       [
                       {
                           id: 'iframeGloss',
                           label: 'eslink',
                           expand: true,
                           elements:
                        [
                            {
                                type: 'html',
                                id: 'Glossary',
                                label: 'Glossary',
                                style: 'width : 100%;',
                                html: '<iframe src="../admin/formmgr/plugins/noticeFields.jsp" frameborder="0" name="custom_text" id="custom_text" allowtransparency="1" style="width:100%;height:490px;margin:0;padding:0;"></iframe>'
                            }
                        ]
                       }
                       ] 
                      , buttons:[{
                    	   type:'button',
                    	   id:'okBtn',
                    	   label: 'Save',
                    	   style: 'background-color:#666666; padding:6px;',
                    	   onClick: function(){
                    		   window.frames['custom_text'].save();
                    	   }
                    	  }, CKEDITOR.dialog.cancelButton],
                    onOk: function () {
                    	
                    },
                    onShow:function () {
                    	var link = document.getElementById('custom_text');
                    	link.src =  "../admin/formmgr/plugins/noticeFields.jsp";
                    	field ="";
                    }
                   
                
                    
                };


            });

            editor.ui.addButton('eslink',
            {
                label: 'Fields',
                command: 'eslink',
                icon: this.path + 'icon-text.png'
            });
            
            
            var iconPath = this.path + 'images/icon.png';
           
            editor.addCommand('eslinkDialog', new CKEDITOR.dialogCommand('eslinkDialog'));

            editor.ui.addButton('eslink',
            {
                label: 'Fields',
                command: 'eslinkDialog',
                icon: this.path + 'icon-text.png'
            });
            
             
            if (editor.contextMenu)
            {
            	// Code creating context menu items goes here.
            	editor.contextMenu.addListener( function( element )
            	{
            		
            		if ( element )
            			
            			
            			if (element.getAttribute('special') == 'text'){
            				 var t = element.getAttribute( 'name' );
            				 var src = element.getAttribute( 'id' );
							 field = src;
            		         var text1 = 'Edit Fields - '+t;
            		         editor.addMenuItem( 'abbrItem',
            		             	{
            		             		label : text1,
            		             		icon : iconPath,
            		             		command : 'eslinkDialog',
            		             		group : 'myGroup'
            		             	});
            				
            			}else {
            				 editor.removeMenuItem( 'abbrItem');
            			}
            		
            		if ( element && !element.isReadOnly() && !element.data( 'cke-realelement' ) )
             			return { abbrItem : CKEDITOR.TRISTATE_OFF };
            		return null;
            	});
            }
            
            
            if ( editor.contextMenu ){
            	editor.addMenuGroup( 'myGroup' );
            }
            
        }
   
    
    });
})();

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}




<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.*,elms.app.common.*,elms.agent.*"%>
<html:html>
<head>
<html:base/>
<title>City of Beverly Hills : Online Business Center : Planners Explorer Frame</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath(); %>

<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="10" height="8"><font class="black2b">Planning
      Explorer</td>
  </tr>
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td width="1%"><img src="images/spacer.gif" width="5" height="1"><img src="images/site_icon_inspector.gif" width="27" height="22"></td>
                <td valign="top"><img src="images/spacer.gif" width="1" height="5"><br>
                  <font class="treetext"><a href="<%=contextRoot%>/viewPlanners.do" class="tree1" target="_top">All Planners</a></td>
              </tr>
              <%
               List planners = (List)ActivityAgent.getPlanners();
               for(int i=0;i<planners.size();i++){
               		ProcessTeamNameRecord processTeamRecord = (ProcessTeamNameRecord)planners.get(i);
               		String userId = processTeamRecord.getNameValue();
              %>
              <tr>
                <td width="1%"><img src="images/spacer.gif" width="5" height="1"><img src="images/site_icon_inspector.gif" width="27" height="22"></td>
                <td valign="top"><img src="images/spacer.gif" width="1" height="5"><br>
                  <font class="treetext"><a href="<%=contextRoot%>/planningRoutingAction.do?userId=<%=userId%>&userName=<%=processTeamRecord.getNameLabel()%>" class="tree1" target="f_content"><%=processTeamRecord.getNameLabel()%></a>(<%=new ActivityAgent().getPlannerUpdateCount(userId)%>)</td>
              </tr>
			 <%}%>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html:html>

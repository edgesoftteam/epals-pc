<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page
	import="elms.security.*,java.util.*,java.util.*,elms.common.*"%>
<%
   boolean general=false, businessLicenseUser = false, businessLicenseApproval = false, businessTaxUser = false, businessTaxApproval = false, codeInspector = false;
   User userBL = (User)session.getAttribute("user");
   List groups = (java.util.List) userBL.getGroups();
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
		if(group.groupId == Constants.GROUPS_GENERAL) general = true;
  		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
		if(group.groupId == Constants.GROUPS_CODE_INSPECTOR) codeInspector = true;
   }
%>
<app:checkLogon/>

<html:html>
<HEAD>
<html:base/>
<TITLE>City of Burbank : Online Business Center : Add Project</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
</HEAD>
<SCRIPT language="JavaScript" src="../script/calendar.js"></SCRIPT>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	strValue=false;
    strValue=validateData('req',document.forms['projectForm'].elements['projectName'],'Project Name is a required field');
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms['projectForm'].elements['description'],'Description is a required field');
    }
    if (strValue == true)
    {

    	if (document.forms['projectForm'].elements['use'].value == '-1')
    	{
    		alert('Select Use');
    		document.forms['projectForm'].elements['use'].focus();
    		strValue=false;
    	}
    	else
    	{
    		strValue=true;
    	}
    }
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms['projectForm'].elements['startDate'],'Applied Date is a required field');
    }
    if (strValue == true)
    {
    	//alert(document.forms['projectForm'].elements['expirationDate'].value);
		if (document.forms['projectForm'].elements['startDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['projectForm'].elements['startDate'],'Invalid date format');
    	}
    }
     if (strValue == true)
    {
    	//alert(document.forms['projectForm'].elements['expirationDate'].value);
		if (document.forms['projectForm'].elements['completionDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['projectForm'].elements['completionDate'],'Invalid date format');
    	}
    }
    return strValue;
}
</SCRIPT>
<BODY class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<%
  	String lsoId = (String)request.getAttribute("lsoId");
  	String ownerName = (String)request.getAttribute("ownerName");
  	String streetNumber = (String)request.getAttribute("streetNumber");
  	String streetName = (String)request.getAttribute("streetName");
 	String unit = (String)request.getAttribute("unit");
  	String useDesc = (String)request.getAttribute("useDesc");
  	if(useDesc == null) useDesc ="";
  	String useId = (String)request.getAttribute("useId");

%>
<html:form action="/saveProject" onsubmit="return validateFunction();"> <html:hidden property="lsoId" value="<%=lsoId%>"/>
<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
    <TR valign="top">
        <TD width="99%">
        <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
            <TR>
                <TD><FONT class="con_hdr_3b">Add Project<br><br></TD>
            </TR>
            <TR>
                <TD>
                <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TR>
                        <TD>
                        <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                            <TR>
                                <TD width="99%" class="tabletitle">Project Details</TD>
                                <TD width="1%" class="tablebutton"><NOBR> <html:reset value="Cancel" styleClass="button" onclick="history.back();"/> <html:submit value="Save" styleClass="button"/> &nbsp;</NOBR></TD>
                            </TR>
                        </TABLE>
                        </TD>
                    </TR>
                    <TR>
                        <TD background="../images/site_bg_B7C1CB.jpg">
                        <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                            <TR valign="top">
                                <TD class="tablelabel">Project #</TD>
                                <TD class="tabletext">Not Yet Created</TD>
                                <TD class="tablelabel">Owner</TD>
                                <TD class="tabletext">
                                 <% if(ownerName!=null){
                                       out.println(ownerName);
                                      } else {
                                          out.println("");
                                      }
                                 %>&nbsp;
                                <html:hidden property="ownerName" value="<%=ownerName%>"/> </TD>
                            </TR>
                            <TR valign="top">
                                <TD class="tablelabel">Project Name</TD>
								<% if(((businessLicenseUser) || (businessTaxUser)) && (codeInspector)){%>
								<TD class="tabletext">
									<html:text property="projectName" value="Code Enforcement" size="50" maxlength="256" styleClass="textbox" readonly="true" />
								</TD>
								<%}else{%>
								<TD class="tabletext">
                                	<html:select property="projectName" styleClass="textbox">
                             		  <html:options collection="projectNames"  property="name"  labelProperty="description" />
                          		   </html:select>
								</TD>
								<%}%>
                                <TD class="tablelabel">Address</TD>
                                <TD class="tabletext"> <%=streetNumber%>&nbsp;<%=streetName%> &nbsp;<%=unit%>&nbsp;
                                <html:hidden property="streetNumber" value="<%=streetNumber%>"/>
                                <html:hidden property="streetName" value="<%=streetName%>"/>
								<html:hidden property="unit" value="<%=unit%>"/> </TD>
                            </TR>
                            <TR valign="top">
                                <TD class="tablelabel">Description</TD>
                                <TD class="tabletext"><html:text property="description" size="50" maxlength="256" styleClass="textbox" /></TD>
                                <TD class="tablelabel"> Use</TD>
                                <TD class="tabletext"><%=useDesc%>&nbsp;
                                <html:hidden property ="use" value="<%=useId%>"/></TD>
                            </TR>
                            <TR valign="top">
                                <TD class="tablelabel">Applied Date</TD>
                                <TD class="tabletext" valign="top"><html:text property="startDate"  maxlength="10" styleClass="textboxd" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <A href="javascript:show_calendar('projectForm.startDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <IMG src="../images/calendar.gif" width="16" height="15" border="0"></A></TD>
                                <TD class="tablelabel">Completion Date</TD>
                                <TD class="tabletext"><html:text property="completionDate" maxlength="10"  styleClass="textboxd" onkeypress="return validateDate();" onblur="DateValidate(this);"></html:text> <A href="javascript:show_calendar('projectForm.completionDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <IMG src="../images/calendar.gif" width="16" height="15" border="0"></A></TD>
                            </TR>
                        </TABLE>
                        </TD>
                    </TR>
                </TABLE>
                </TD>
            </TR>
            <TR>
                <TD>&nbsp;</TD>
            </TR>
        </TABLE>
        </TD>
        <TD width="1%"></TD>
    </TR>
</TABLE>
</html:form>
</BODY>
</html:html>


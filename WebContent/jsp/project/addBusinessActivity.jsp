<%@ page import='elms.agent.*,java.util.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>


<html:html>
<head>
<html:base />
<title>City of Burbank : Permitting And Licensing System : Add Business Activity</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="http://localhost:8080/epals/jsp/css/elms.css" type="text/css">
<script src="../script/yCodelib.js">
</script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"> </script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>

<%!

List streetList = new ArrayList();
List sptList = new ArrayList();
List spstList = new ArrayList();
List ptList = new ArrayList();
List atList = new ArrayList();
List atsList = new ArrayList();
List userList = new ArrayList();

 %>
<%

String contextRoot = request.getContextPath();
try{
List streetList = new AddressAgent().getStreetArrayList();
pageContext.setAttribute("streetList", streetList);
List sptList = LookupAgent.getSubProjectTypes();
pageContext.setAttribute("sptList", sptList);
List spstList = LookupAgent.getSubProjectSubTypes();
pageContext.setAttribute("spstList", spstList);
List ptList = LookupAgent.getSubProjectNames("ALL");
pageContext.setAttribute("ptList", ptList);
List atList = LookupAgent.getSearchActivityTypes();
pageContext.setAttribute("atList", atList);
List atsList = LookupAgent.getActivitySubTypes();
pageContext.setAttribute("atsList", atsList);
List userList = new AdminAgent().getNameList();
}
catch(Exception e){
	//ignored
}
pageContext.setAttribute("userList", userList);

%>


<script language="JavaScript">
function searchValidate() {

var i=0;

if(document.forms['basicSearchForm'].apn.value!="" && document.forms['basicSearchForm'].apn.value.length<10){
alert("Please enter 10 digits for APN");
document.forms['basicSearchForm'].apn.focus();
i++;
}
else if(document.forms['basicSearchForm'].projectNumber.value!="" && document.forms['basicSearchForm'].projectNumber.value.length<9){
alert("Please enter 9 digits for Project No");
document.forms['basicSearchForm'].projectNumber.focus();
i++;
}

else if(document.forms['basicSearchForm'].subProjectNumber.value!="" && document.forms['basicSearchForm'].subProjectNumber.value.length<9){
alert("Please enter 9 digits for SubProject No");
document.forms['basicSearchForm'].subProjectNumber.focus();
i++;
}

else if(document.forms['basicSearchForm'].ownerName.value!="" && document.forms['basicSearchForm'].ownerName.value.length<3){
alert("Please enter a minimum of 3 Characters for the Owner Name");
document.forms['basicSearchForm'].ownerName.focus();
i++;
}

else if(document.forms['basicSearchForm'].activityNumber.value!="" && document.forms['basicSearchForm'].activityNumber.value.length<9){
alert("Please enter a minimum of 9 Characters for the Activity Number");
document.forms['basicSearchForm'].activityNumber.focus();
i++;
}

//alert(i);
if (i>0 ) {
document.forms['basicSearchForm'].action="";
}
var b=0;
/*if (true) {

alert(document.forms['basicSearchForm'].length);

for(k=0;k<document.forms['basicSearchForm'].length;k++) {
alert(document.forms.object[i]);
}
document.forms['basicSearchForm'].action="";
}*/


}

function dohref() {
  parent.f_content.location.href='<%=contextRoot%>/viewAdvancedSearch.do';
}


function doDocumentHref() {
  parent.f_content.location.href='<%=contextRoot%>/viewDocumentSearch.do';
}

function evalType() {
	var size= document.forms[0].elements['activityType'].length;
	var str = "";
	var delimiter = "";
	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['activityType'].options[i].selected) {
			str += delimiter + document.forms[0].elements['activityType'].options[i].text;
			delimiter = "\r";
		}
	}
document.forms[0].elements['selectedActType'].value=str;
}

function evalSubType() {
	var size= document.forms[0].elements['activitySubType'].length;
	var str = "";
	var delimiter = "";
	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['activitySubType'].options[i].selected) {
			str += delimiter + document.forms[0].elements['activitySubType'].options[i].text;
			delimiter = "\r";
		}
	}
document.forms[0].elements['selectedActSubType'].value=str;
}

function disEvalType(){
document.forms[0].elements['selectedActType'].value="";
}

function disEvalSubType(){
document.forms[0].elements['selectedActSubType'].value="";
}

</script>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0" onload="initComboBox(); fixTable(); AdjustComboBoxes();">
<html:form name="basicSearchForm" type="elms.control.beans.BasicSearchForm" action="viewSearchResults.do">

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Add Activity<br>
					<br>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="40%" background="../images/site_bg_B7C1CB.jpg"
										align="left">Add Activity</td>
									<td width="40%" background="../images/site_bg_B7C1CB.jpg"
										align="right">
									<%--
                                   	<html:button value="Advanced Search"  property="advanced search" styleClass="button" onclick="javascript:dohref()"/> &nbsp;
                                   	--%>
                                 <html:button  property="Cancel"  value="Cancel" styleClass="button" onclick="cancel();"/>

                          <html:button property="Save" value="Save" styleClass="button" onclick="return save();"/>
									&nbsp;</td>

									</tr>
							</table>
							</td>
						</tr>

						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" cellpadding="2" cellspacing="1">
												<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Is Business located in the City of Burbank?</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Application Type</td>
									<td colspan="2" class="tabletext">

									<html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select>		<BR>
										</td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Address</td>
									<td colspan="5" class="tabletext"> <html:text
										size="20" maxlength="10" property="apn" styleClass="textbox"
										onkeypress="return validateNumeric();"/>
									<html:text
										size="20" maxlength="10" property="apn" styleClass="textbox"
										onkeypress="return validateNumeric();"/>
									<html:text
										size="20" maxlength="10" property="apn" styleClass="textbox"
										onkeypress="return validateNumeric();"/>
									<html:text
										size="20" maxlength="10" property="apn" styleClass="textbox"
										onkeypress="return validateNumeric();"/>
                                   </td>

								</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Out of Town Address</td>
									<td colspan="5" class="tabletext"><html:text
										size="20" property="ownerName" styleClass="textbox" />
									<html:text size="20" property="ownerName" styleClass="textbox" />
									<html:text size="20" property="ownerName" styleClass="textbox" />
									<html:text size="20" property="ownerName" styleClass="textbox" />
									<html:text size="20" property="ownerName" styleClass="textbox" />
                                     </td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Activity Number</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Activity Status</td>
									<td colspan="2" class="tabletext">

									<html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select>		<BR>
										</td>
								</tr>
								<tr>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Creation Date</td>

									<td colspan="1" class="tabletext">


											<html:text size="20" property="foreignAddressState" styleClass="textbox" />&nbsp;
										<BR>
									</td>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Renewal Date</td>
									<td colspan="2" class="tabletext">

				<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />		<BR>
										</td>
								</tr>

								<tr>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Business Name</td>

									<td colspan="1" class="tabletext">


											<html:text size="20" property="foreignAddressState" styleClass="textbox" />&nbsp;
										<BR>
									</td>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Business Account Number</td>
									<td colspan="2" class="tabletext">

				<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />		<BR>
										</td>
								</tr>
								<%--
                            <tr>
                                <td colspan="3" class="tablelabel">Project/Activity Team</td>
                            </tr>
                            <tr>
                                                <td class="tablelabel" width="20%" height="23">Name</td>
                                                <td colspan="2" class="tabletext" valign="top"><html:text property="name" size="20" styleClass="textbox"/>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td ><html:select property="name"  styleClass="textbox" style="position:absolute" onchange="if (SearchName !=null) {SearchName.value.processComboBoxSelect();}">
                                             <html:option value=""></html:option>
                                             <html:options collection="userList" property="userId" labelProperty="name" />
                                             </html:select></td>
                                    </tr>
                                </table>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" class="tablelabel">People</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Name</td>
                                <td colspan="2" class="tabletext"><html:text property="peopleName" size="20" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">License No</td>
                                <td colspan="2" class="tabletext"><html:text property="peopleLic" size="20" styleClass="textbox"/></td>
                            </tr>
                            --%>

							<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Corporate Name</td>
									<td colspan="5" class="tabletext"><html:text
										size="20" property="ownerName" styleClass="textbox" /> </td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Business Phone ,Ext</td>

									<td colspan="1" class="tabletext">


											<html:text size="20" property="foreignAddressState" styleClass="textbox" />&nbsp;
                                            <html:text size="20" property="foreignAddressState" styleClass="textbox" />&nbsp;
										<BR>
									</td>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Business Fax</td>
									<td colspan="2" class="tabletext">

				<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />		<BR>
										</td>
									</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Activity Type</td>

									<td colspan="1" class="tabletext">


										<html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select>	<BR>
									</td>
							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Activity Sub-Type</td>
									<td colspan="2" class="tabletext">

				           <html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select>				</td>
						</tr>
								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Muncipal Code</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">SIC Code</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

							<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Description of Business</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Class Code</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

                                <tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Home Occupation</td>

									<td colspan="1" class="tabletext">

										<html:checkbox property="foreignAddressLine2" styleClass="textbox" value="Yes">Yes</html:checkbox>
									<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Decal Code</td>
									<td colspan="2" class="tabletext">


									<html:checkbox property="foreignAddressLine2" styleClass="textbox" value="Yes">Yes</html:checkbox>

									</td>
								</tr>

									<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Application Date</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Issue Date</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Starting Date</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Out of Business Date</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Ownership Type</td>

									<td colspan="1" class="tabletext">

										 <html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select><BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Federal ID Number</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">E-mail Address</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Social Security Number</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

						       <tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Mailing Address</td>

									<td colspan="5" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										    <html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										    <html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

										<BR>

									</td>


								</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Previous Address</td>

									<td colspan="5" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										    <html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										    <html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

										<BR>

									</td>


								</tr>


                                <tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Insurance Expiration Date</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Bond Expiration Date</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

                                 <tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Department of Justice Expiration Date</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Federal Firearms License Expiration Date</td>
									<td colspan="2" class="tabletext">


									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>

								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Square Footage(Area Occupied)</td>

									<td colspan="1" class="tabletext">

											<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />
										<BR>

									</td>

							<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Quantity</td>
									<td colspan="2" class="tabletext">

				                     <html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select>
									<html:text size="20" property="foreignAddressLine1" styleClass="textbox" />

									</td>
								</tr>


								<tr>
									<td class="tablelabel" width="20%"><font
										class="con_hdr_1">Type of Exemptions</td>

									<td colspan="5" class="tabletext">
										<html:select
										property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId"
											labelProperty="description" />
									</html:select>


									</td>


								</tr>

									</table>
									</td>

								</tr>


							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td width="1%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="32">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>

</html:form>

<SCRIPT>
//==================================================================
//initComboBox() creates a combo box object
//------------------------------------------------------------------
var SearchStreetName;
var SearchStreetName1;
function initComboBox()
{
	SearchStreetName= new ComboBoxObject("streetName", "SearchStreetName");
	SearchStreetName.setDoLookup(true);
	SearchStreetName.setCaseInsensitive(true);
	SearchStreetName.setDefaultIndex(0);
	SearchStreetName.focus();

	SearchStreetName1 = new ComboBoxObject("streetName1", "SearchStreetName1");
	SearchStreetName1.setDoLookup(true);
	SearchStreetName1.setCaseInsensitive(true);
	SearchStreetName1.setDefaultIndex(0);
	SearchStreetName1.focus();

}
function fixTable() {
	if (SearchStreetName!=null)
	{
		if (tdid.offsetWidth < SearchStreetName.ComboWidth)
		{
			var temp = tableAutoComboBox.style.tableLayout;
			tableAutoComboBox.style.tableLayout = "fixed";
			tdid.style.posWidth = SearchStreetName.ComboWidth;
			tableAutoComboBox.style.tableLayout = temp;
		}
	}
	if (SearchStreetName1 != null)
	{
		if (tdid1.offsetWidth < SearchStreetName1.ComboWidth)
		{
			var temp = tableAutoComboBox1.style.tableLayout;
			tableAutoComboBox1.style.tableLayout = "fixed";
			tdid1.style.posWidth = SearchStreetName1.ComboWidth;
			tableAutoComboBox1.style.tableLayout = temp;
		}
	}
}
</SCRIPT>
</body>
</html:html>

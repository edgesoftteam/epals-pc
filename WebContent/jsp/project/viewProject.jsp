<%@ page import="elms.common.*,java.util.*,elms.security.*,elms.app.common.*,elms.agent.*,elms.util.*" %>
<%
   boolean general=false, parkingUser=false,publicWorksUser=false, conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false, businessLicenseUser = false, businessLicenseApproval = false, businessTaxUser = false, businessTaxApproval = false, codeInspector = false, regulatoryPermit = false, hAdmin = false;
   User user = (User)session.getAttribute("user");
   List groups = (java.util.List) user.getGroups();
   if(user.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_PUBLIC_WORKS_CODE)){
	   publicWorksUser = true;
   }
   if(user.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_PARKING_CODE)){
	   parkingUser = true;
   }
   if(user.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_HOUSING_CODE)){
	   hAdmin = true;
   }
   Iterator itr = groups.iterator();
  
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_CODE_INSPECTOR) codeInspector = true;
		if(group.groupId == Constants.GROUPS_REGULATORY_PERMIT) regulatoryPermit = true;
		if(group.groupId == Constants.GROUPS_HOUSING_USER) hAdmin = true;
   }
%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<app:checkLogon/>


<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
String editable = (String) request.getAttribute("editable");
if(editable == null)  editable = "";

//Getting Project Details From Request
String lsoId = (String)session.getAttribute("lsoId");
if(lsoId == null) lsoId = "";
String projectId = (String)request.getAttribute("projectId");
if(projectId == null) projectId = "";
String ownerName = (String)request.getAttribute("ownerName");
if(ownerName == null) ownerName = "";
String streetNumber = (String)request.getAttribute("streetNumber");
if(streetNumber == null) streetNumber = "";
String streetName = (String)request.getAttribute("streetName");
if(streetName == null) streetName = "";
String unit = (String)request.getAttribute("unit");
if(unit == null) unit = "";
String projectNumber = (String)request.getAttribute("projectNumber");
if(projectNumber == null) projectNumber = "";
String projectName = (String)request.getAttribute("projectName");
if(projectName == null) projectName = "";
String projectDescription = (String)request.getAttribute("projectDescription");
if(projectDescription == null) projectDescription = "";
String landUse = (String)request.getAttribute("use");
if(landUse == null) landUse = "";
String startDate = (String)request.getAttribute("startDate");
if(startDate == null) startDate = "";
String completionDate = (String)request.getAttribute("completionDate");
if(completionDate == null) completionDate = "";
String expirationDate = (String)request.getAttribute("expirationDate");
if(expirationDate == null) expirationDate = "";
String valuation = (String)request.getAttribute("valuation");
if(valuation == null) valuation = "";
String status = (String)request.getAttribute("status");
if (status == null) status = "";
String level = "P";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String createdBy = (String)request.getAttribute("createdBy");
if (createdBy == null) createdBy = "";
String microfilm = (String)request.getAttribute("microfilm");

String active = (String)request.getAttribute("active");
String label = (String) request.getAttribute("label");



//Getting SubProject Details From Request
java.util.List subProjects = (java.util.List) request.getAttribute("subProjects");
java.util.Iterator subProjectsIter = subProjects.iterator();

java.util.List holds = (java.util.List) request.getAttribute("holds");
if(holds == null) holds = new java.util.ArrayList();
pageContext.setAttribute("holds",holds);

java.util.List condList = (java.util.List)request.getAttribute("condList");
if(condList == null) condList = new java.util.ArrayList();
pageContext.setAttribute("condList",condList);

// conditionally  doing the tree refresh
String onloadAction = (String)session.getAttribute("onloadAction");
if (onloadAction == null ) onloadAction = "";

String projectNameType = LookupAgent.getProjectNameForProjectId(projectId);

%>
<script language="JavaScript">

function deleteProject() {
   userInput = confirm("Are you sure you want to delete this Project?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/deleteProject.do?projectId=<%=projectId%>'
   }
}
function deleteSubProject(subProjectId) {
    userInput = confirm("Are you sure you want to delete this Sub Project?");
  if (userInput==true) {
    document.location.href='<%=contextRoot%>/deleteSubProject.do?subProjectId='+subProjectId;
  }
}

function getActive(val){
	document.location.href= "<%=contextRoot%>/viewProject.do?projectId=<%=projectId%>&active="+val;

	}
	
function getSubProject(subProjectId){
	var activeStatus;
	if(document.getElementById("activeOne").checked == true) {
		activeStatus = document.getElementById("activeOne").value;
	} else if(document.getElementById("activeTwo").checked == true) {
		activeStatus = document.getElementById("activeTwo").value;
	}
	document.location.href = "<%=contextRoot%>/viewSubProject.do?subProjectId=" + subProjectId +  "&active=" + activeStatus;
}

</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="<%=onloadAction%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">&nbsp;Project Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Project Detail</td>
                                <%if(editable.equals("true")){%>

	                                <security:editable levelId="<%=projectId%>" levelType="P" editProperty="allProjectAndDept">
	                                <security:editable levelId="<%=projectId%>" levelType="P" editProperty="checkUser">
	                                <td width="1%" class="tablebutton"> <a href="<%=contextRoot%>/editProjectDetails.do?levelId=<%=projectId%>&level=<%=level%>" class="tablebutton">Edit</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
	                                </security:editable>
                                	</security:editable>

                                <%}%>
                                <% if(subProjects.isEmpty()) { %>
                                <% if(projectTech){%>

                                 <security:editable levelId="<%=projectId%>" levelType="P" editProperty="allProjectAndDept">
                                <td width="1%" class="tablebutton"><nobr><a href="javascript:deleteProject();" class="tablebutton">Delete<img src="../images/site_blt_x_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                                </security:editable>

                                <%}}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Project #</td>
                                <td class="tabletext"><%=projectNumber%></td>
                                <td class="tablelabel">Owner</td>
                                <td class="tabletext"><%=ownerName%>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Project Name</td>
                                <td class="tabletext"><%=projectName%></td>
                                <td class="tablelabel">Address</td>
                                <td class="tabletext"> <%=streetNumber%>&nbsp;<%=streetName%>&nbsp;<%=unit%>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" wrap>Description</td>
                                <td class="tabletext"> <%=projectDescription%> </td>
                                <td class="tablelabel">Land Use</td>
                                <td class="tabletext"><%=landUse%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Status</td>
                                <td class="tabletext"><%=status%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=createdBy%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Label</td>
                                <td class="tabletext" colspan="3"><%=label%></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Sub-Projects
                        	 [ Active<nobr><input type="radio" id="activeOne" name="active"  value="Y" onclick="getActive(this.value);" /> &nbsp;
                        	 All<input type="radio" id="activeTwo" name="active" value="N" onclick="getActive(this.value);" />  ]
                        </td>

                        <%if(editable.equals("true")){%>
						 <security:editable levelId="<%=projectId%>" levelType="P" editProperty="allProjectAndDept">

                        <td width="1%" class="tablebutton">
                        	<nobr>
                        	
                        		<security:editable levelId="<%=projectId%>" levelType="P" editProperty="checkUser">
                        			<a href="<%=contextRoot%>/addSubProject.do?projectId=<%=projectId%>&projectName=<%=projectName%>" class="tablebutton">
                        				Add SubProject<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0">
                        			</a>
                        		</security:editable>
                        	</nobr>
                        </td>
                        </security:editable>
                        <%}%>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>

                    <%
			 int columnCount = 0;
			 while (subProjectsIter.hasNext()) {
	             columnCount = columnCount + 1;
	             elms.app.project.SubProject subProject = (elms.app.project.SubProject) subProjectsIter.next();
	             // Getting SubProject Id
	             String subProjectId = elms.util.StringUtils.i2s(subProject.getSubProjectId());
	              //Getting projectType
	              elms.app.project.SubProjectDetail subProjectDetail = subProject.getSubProjectDetail();
	              String subProjectNumber = subProjectDetail.getSubProjectNumber();
	              elms.app.admin.ProjectType projectType = subProjectDetail.getProjectType();
	             String projectTypeDesc = "";
	             if (projectType != null) projectTypeDesc = projectType.getDescription();
	             //Getting subProjectType description
	             elms.app.admin.SubProjectType subProjectType = subProjectDetail.getSubProjectType();
	             String subProjectTypeDesc = "";
	             if (subProjectType != null)
					{
					 subProjectTypeDesc = subProjectType.getDescription();
					if(subProjectTypeDesc==null)	{subProjectTypeDesc="";}
					}

	             String subProjectDescription = "";
	             if (subProjectType != null)
					{
	            	 subProjectDescription = subProjectDetail.getDescription();
					if(subProjectDescription==null)	{subProjectDescription="";}
					}


	             //Getting subProjectSubType description
	             elms.app.admin.SubProjectSubType subProjectSubType = subProjectDetail.getSubProjectSubType();
	             String subProjectSubTypeDesc = "";
	             if (subProjectSubType != null) {
						subProjectSubTypeDesc = subProjectSubType.getDescription();
						if(subProjectSubTypeDesc==null){subProjectSubTypeDesc="";}
					}

	             String hardHoldCount = elms.util.StringUtils.i2s(subProjectDetail.getHardHoldCount());
	             String softHoldCount = elms.util.StringUtils.i2s(subProjectDetail.getSoftHoldCount());
	             String warnHoldCount = elms.util.StringUtils.i2s(subProjectDetail.getWarnHoldCount());
	             String attachmentCount = elms.util.StringUtils.i2s(subProjectDetail.getAttachmentCount());
	             int conditionCount = subProjectDetail.getConditionCount();
	             // getting the list of activities of subprojects
	             java.util.List activities = subProject.getActivity();
	             java.util.Iterator activitiesIter = activities.iterator();
	             if (columnCount % 2 == 1) out.println("<tr>");
 			%>


                    <td width="50%" background="../images/site_bg_B7C1CB.jpg" valign="top">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                        <tr>
                            <td class="tablebutton" height="15"><a href="javascript:getSubProject(<%=subProjectId%>);" class="tablebutton"><%=projectTypeDesc%></a>&nbsp;(<%=subProjectNumber%>)</td>

					<%  if(editable.equals("true")){
                                 if (activities.isEmpty()) {
									 %>

									  <security:editable levelId="<%=projectId%>" levelType="P" editProperty="allProjectAndDept">
                                    <td class="tablebutton" align="right"><nobr><a href="javascript:deleteSubProject(<%=subProjectId%>);" class="hdrs">Delete <img src="../images/site_blt_minus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                                    </security:editable>
                          <%}}%>
                        </tr>
                        <tr valign="top">
                            <td class="tabletext" colspan="2">
                            <table width="100%" border="0" cellspacing="1" cellpadding="0">
                                <tr align="center">
                                    <td class="tablelabel">Type</td>
                                    <td class="tablelabel">Sub-Type</td>
                                </tr>
                                <tr align="center">
                                    <td class="tabletext"><%=subProjectTypeDesc%></td>
                                    <td class="tabletext"><%=subProjectSubTypeDesc%></td>
                                </tr>

                            </table>
                            </td>
                        </tr>

                        <tr align="center">
                             <td class="tablelabel">Description</td>
                        </tr>
                        <tr align="center">
                             <td class="tabletext"><%=subProjectDescription%></td>
                        </tr>


                        <tr valign="top">
                            <td class="tabletext" colspan="2">
                            <table width="100%" border="0" cellspacing="1" cellpadding="0">
                                <tr align="center">
                                    <td class="tablelabel" colspan="6">Holds</td>
                                    <td class="tablelabel">Conditions</td>
                                    <td class="tablelabel">Attachments</td>
                                </tr>
                                <tr align="center">
                                    <td class="tabletext">(<%=warnHoldCount%>)</td>
                                    <td><img src="../images/site_icon_hold_warning.gif" width="21" height="20"></td>
                                    <td class="tabletext">(<%=hardHoldCount%>)</td>
                                    <td><img src="../images/site_icon_hold_hard.gif" width="21" height="20"></td>
                                    <td class="tabletext">(<%=softHoldCount%>)</td>
                                    <td><img src="../images/site_icon_hold_soft.gif" width="21" height="20"></td>
                                    <% if (conditionCount == 0) { %>
                                    <td class="tabletext">None</td>
                                    <%}else{%>
                                    <td class="tabletext"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=subProjectId%>&conditionLevel=Q&ed=<%=editable%>">View</a></td>
                                    <%}%>
                                    <td class="tabletext">(<%=attachmentCount%>)</td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td class="tabletext" colspan="2">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr align="center">
                                    <td colspan="2" class="tabletitle">Activities</td>
                                </tr>
                                <tr>
                                    <td colspan="2" valign="top"><img src="../images/spacer.gif" width="1" height="7"></td>
                                </tr>
                                <%
 		while (activitiesIter.hasNext()) {
             String activityId = "";
             String activityNumber = "";
             String activityDesc = "";
              String activityStatus = "";
             String activityTypeCode = "";
             String activityTypeDesc = "";
             elms.app.project.Activity activity = (elms.app.project.Activity) activitiesIter.next();
             activityId = elms.util.StringUtils.i2s(activity.getActivityId());
             if(activity.getActivityDetail() != null) {
                     activityNumber = activity.getActivityDetail().getActivityNumber();
                     if (activityNumber == null) activityNumber = "";
                     activityDesc = activity.getActivityDetail().getDescription();
                     if(activityDesc == null) activityDesc = "";
                     if (activity.getActivityDetail().getActivityType()  != null)  {
                             activityTypeCode = activity.getActivityDetail().getActivityType().getType();
                             if ( activityTypeCode == null) activityTypeCode = "";
                             activityTypeDesc = activity.getActivityDetail().getActivityType().getDescription();
                             if (activityTypeDesc == null)  activityTypeDesc = "";
                             if(activity.getActivityDetail().getStatus() == null) activityStatus = "" ;
                             else activityStatus = activity.getActivityDetail().getStatus().getDescription();
                     }
              }
              String activityDetails =  "  " + activityTypeDesc + ", " + activityDesc   +    "  ( "   +  activityStatus + ")" ;
 %>
                                <tr>
                                    <td width="1%" valign="top"><img src="../images/site_blt_dot_000000.gif" width="21" height="10"></td>
                                    <td width="99%" valign="top" class="tabletext"> <a href="javascript:parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=lsoId%>&psaNodeId=<%=activityId%>';"><%=activityNumber%></a> <%=activityDetails%></td>
                                </tr>
                                <%}%>
                                <tr>
                                    <td colspan="2" valign="top"><img src="../images/spacer.gif" width="1" height="7"></td>
                                </tr>


                                <tr>
                                    <td colspan="2" valign="top"><img src="../images/spacer.gif" width="1" height="7"></td>
                                </tr>
                            </table>
                            </td>
                        </tr>
                    </table>
                    </td>
                    <%
                          if (columnCount % 2 == 0) out.println("</tr>");
                       }
                       if (columnCount % 2  == 1){
                    %>
                        <td width="50%">&nbsp;</td>
                        </tr>
                    <%}%>
                </table>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=projectId%>&conditionLevel=<%=level%>&ed=<%=editable%>" class="tabletitle">Conditions</a></td>
                                <%if(editable.equals("true")){%>
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addCondition.do?levelId=<%=projectId%>&conditionLevel=<%=level%>" class="tablebutton">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                                <%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="cond" name="condList" type="elms.app.common.Condition">
                            <tr valign="top">
                                <td class="tablelabel"><bean:write name="cond" property="conditionCode"/></td>
                                <td class="tabletext"><bean:write name="cond" property="shortText"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

			<jsp:include page="../includes/comments.jsp" flush="true">
				<jsp:param name="id" value="<%=projectId%>" />
				<jsp:param name="level" value="<%=level%>" />
				<jsp:param name="editable" value="<%=editable%>" />
			</jsp:include>

            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=projectId%>&level=<%=level%>" class="tabletitle">Attachments</a></td>
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=projectId%>&level=<%=level%>" class="tablebutton">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>

                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">ID</td>
                                <td class="tablelabel">Description</td>
                                <td class="tablelabel">Size</td>
                                <td class="tablelabel">Date</td>
                            </tr>
							<%int i=0;%>
                        <logic:iterate id="attachment" name="attachmentList" type="elms.app.common.Attachment" scope="request">
							<%
							List attachmentsList = (List)request.getAttribute("attachmentList");
							Attachment attachmentObj = new Attachment();
							String fileNameStr = "";

							try{
								attachmentObj = (Attachment)attachmentsList.get(i);
								fileNameStr = attachmentObj.file.getFileName();
								fileNameStr = java.net.URLDecoder.decode(fileNameStr, "UTF-8");
							}catch(Exception e){}
						 %>
                        <tr valign="top" >
                            <td class="tabletext"> <%=fileNameStr%> </td>
                            <td class="tabletext"> <bean:write name="attachment" property="description"/> </td>
                            <td class="tabletext"> <bean:write name="attachment" property="file.fileSize"/> </td>
                            <td class="tabletext"> <bean:write name="attachment" property="file.strCreateDate"/> </td>
                        </tr>
							<%i++; %>
                        </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="200" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="85%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=<%=projectId%>&level=<%=level%>&ed=<%=editable%>" class="tabletitle">Holds</a></td>
                                <%if(editable.equals("true")){%>
                                <td width="14%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addHold.do?levelId=<%=projectId%>&level=<%=level%>" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></nobr></td>
                                <%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="hold" name="holds">
                            <tr valign="top">
                                <td class="tabletext"><bean:write name="hold" property="type"/></td>
                                <td class="tabletext"><bean:write name="hold" property="title"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Site Data</td>
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=<%=projectId%>&levelType=P" class="tablebutton">View<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</body>
<script>
var radio = '<%=active%>';
  if(radio == 'Y'){
    document.all.activeOne.checked = true;
   }else {
	document.all.activeTwo.checked = true;
   }
</script>

</html:html>

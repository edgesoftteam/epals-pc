<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.util.StringUtils"%>
<%@page import="java.util.*, elms.control.beans.*, elms.app.inspection.*"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="java.util.*,elms.app.common.*,elms.agent.*"%>
<%@page import="elms.control.actions.ApplicationScope"%>

<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<%@page import="elms.common.Constants"%>
<%@page import="elms.security.User"%>
<app:checkLogon/>

<html:html>
<head> 
<html:base/>
<title>City of Burbank : ePALS : PlanCheck</title>

<% String contextRoot = request.getContextPath(); %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>

<link rel="stylesheet" href="../css/kendo.default-v2.min.css"/>
<script language="javascript" type="text/javascript" src="../script/kendo.all.min.js"></script>

</head>
<%


User user = (User)session.getAttribute(Constants.USER_KEY);
String deptCode = user.getDepartment().getDepartmentCode();

String dateFrom=(String)session.getAttribute("fromDate");
String dateTo=(String)session.getAttribute("toDate");
System.out.println("toDate..."+dateTo);

//dateFrom=StringUtils.dateslashes2datehypen(dateFrom);
//dateTo=StringUtils.dateslashes2datehypen(dateTo);
%> 
<script>
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");
</script>

<script>
	function showActivity(actId){
		parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId='+actId;
	}

	function showPlanCheck(pcId,department){
		//var deptDesc = document.forms[0].deptDesc.value;
		var fromDate=document.forms[0].elements['fromDate'].value;
		var toDate=document.forms[0].elements['toDate'].value;
		parent.f_content.location.href='<%=contextRoot%>/editPlanCheck.do?fromWhere=viewAllPC&deptDesc='+department+'&planCheckId='+pcId+'&fromDate='+fromDate+'&toDate='+toDate;
		}

	function reAssignPlanCheck(){
		if(document.forms[0].engineerId.value != -1){
		document.forms[0].action = document.forms[0].action + "?action=reAssign";
		document.forms[0].submit();
		}
	}

	function deptChanged(){
		/* if(document.forms[0].deptDesc.value != -1){
			document.forms[0].action = document.forms[0].action + "?action=deptChanged";
			document.forms[0].submit();
		} */
	}
	
	function selectAllCheckBox(obj){
		/* var name = obj.name;
		var value = obj.checked;
		var lastIndex = name.lastIndexOf("selectAllCheckBox");
		name = name.substring(0,lastIndex);
		var count = 1;
		var checkBox = name + 'planCheckArray[0].check';
		while(document.getElementById(checkBox)){
			document.getElementById(checkBox).checked = value;
			checkBox = name + 'planCheckArray['+ count  +'].check';
			count++;
			} */
		document.forms[0].action='<%=contextRoot%>/planCheckSelectAll.do';
		document.forms[0].submit();
		}

	function printPlanCheck(engId){
		window.open("<%=contextRoot%>/reAssignPlanCheck.do?engineerId=" + engId +"&action=print&deptDesc="+ document.forms[0].deptDesc.value);
		}

	function onclickprocess(action)
	{
	    var refresh="true";
	    if(action != 'reset'){
		    if(document.forms[0].elements['fromDate'].value != '' && !isDate(document.forms[0].elements['fromDate'].value)){
		    	document.forms[0].elements['fromDate'].focus();
				return false;
			}
			if(document.forms[0].elements['toDate'].value != '' && !isDate(document.forms[0].elements['toDate'].value)){
				document.forms[0].elements['toDate'].focus();
				return false;
			}
	    }	
	    
		document.forms[0].action = document.forms[0].action + "?action="+action;
		document.forms[0].submit();
	}
	
	
	function getUsers(dept){
		//alert(dept);
		if(dept == ""){
			var box = document.forms[0].elements['engineerId'];
			box.length = 0;
	        var no = new Option();
	        no.value = 0;
	        no.text = "Unassigned";
	        box[0] = no;
			return true;
		}	
		var url = "<%=request.getContextPath()%>/viewAllPlanCheck.do?action=getUsers&deptDesc="+dept;
		//alert(url);
		xmlhttp.open('POST', url,false);
		xmlhttp.onreadystatechange = getUsersHandler;
	    xmlhttp.send(null);
	}
	function getUsersHandler(){
	    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
				    var result=xmlhttp.responseText;
				   	var box = document.forms[0].elements['engineerId'];
				   	engineerUsers = result.split("$@$");
				    box.length = 1;
					c=0;
					for(c=1; c<engineerUsers.length; c++) {
				          var no = new Option();
				          var engineerUserDetail = engineerUsers[c].split("#@#");
				          no.value = engineerUserDetail[0];
				          no.text = engineerUserDetail[1];
				          box[c] = no;
				     }
	 	}
	}

	
</script>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="javascript:parent.f_reports.location.reload();">
<html:form name="planCheckReassignForm" type="elms.control.beans.project.PlanCheckReassignForm" action="reAssignPlanCheck">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="40" colspan="5"><font class="con_hdr_3b"><a name="top"><bean:write name="planCheckReassignForm" property="deptDesc"></bean:write>&nbsp;PC/Approvals</font>
                </td>
            </tr>
            <tr> 
            <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
            	<td colspan="5">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                   		<tr>
                        	<td width="98%" class="tabletitle"><font class="con_hdr_2b">Date Range Filter</font></td>
                        </tr>
                    </table>
            	</td>
			</tr>
            		<tr>
            <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="0">
                            <tr>
            	<td class="tablelabel" width="5%"><nobr>Date From</nobr></td>
	            <td class="tabletext" width="10%"><nobr> 
	            	 <nested:text property="fromDate" size="10" styleId="fromdatedatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13"/>
	            </nobr></td>
				<td class="tablelabel" width="5%"><nobr>Date To</nobr></td>
	            <td class="tabletext" width="25%" colspan="5"><nobr> 
	            	<nested:text property="toDate" size="10" styleId="todatedatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13"/> </nobr> 
	        	<html:button value="Refresh" property="refresh1" styleClass="button" onclick="onclickprocess('refresh')"/> 
	        	&nbsp; <html:button value="Today" property="reset" styleClass="button" onclick="onclickprocess('reset')"/> </nobr>
	        	</td>
            	<%-- <td height="30" class="tablelabel" width="50%" colspan="3">
            		Select Department: &nbsp;
                	<html:select property="deptDesc"	styleClass="textbox" onchange="getUsers(this.value)">
                    	<html:option value="-1">Please Select</html:option>
						<html:options collection="deptList" property="description" labelProperty="description" />
					</html:select> 
            	</td> --%>
            </tr>
             <tr>
                                <td class="tabletext" colspan="8"><img src="../images/spacer.gif" width="1" height="5"></td>
             </tr>
            <tr>
                <td colspan="8">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
              	   <tr>
                  	   <td>
                      	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                               	 <td width="40%"  class="tablelabel"><b>Staff</b></td>
<%--                                	<logic:equal name="planCheckReassignForm" property="manager" value="Y">  --%>
<!--                                	 <td width="40%"  class="tablelabel"> -->
<!--                                	 Select Department: &nbsp; -->
<%--                 					<html:select property="deptDesc"	styleClass="textbox" onchange="getUsers(this.value)"> --%>
<%--                     				<html:option value="-1">Please Select</html:option> --%>
<%-- 									<html:options collection="deptList" property="description" labelProperty="description" /> --%>
<%-- 									</html:select>  --%>
<!--                                	 </td> -->
<%-- 								</logic:equal> --%>
                               	 <td width="20%"  class="tablelabel"><nobr>
                               	 <logic:equal name="planCheckReassignForm" property="manager" value="Y"> 
								
                               	 Re-Assign To &nbsp;
                               	 <html:select property="engineerId"	styleClass="textbox" onchange="reAssignPlanCheck();">
                               	 <html:option value="-1">Please Select</html:option>
											<html:options collection="engineerUsers" property="userId"
												labelProperty="name" />
										    </html:select> 
									 </logic:equal>	    
                               	 &nbsp;&nbsp;</nobr>
                               	 </td>
                               	
                              </tr>
                           </table>
                       </td>
           		   </tr>
           		   </table>
           		   </td>
           		   </tr>
           		   <tr>
                                <td class="tabletext" colspan="8"><img src="../images/spacer.gif" width="1" height="5"></td>
             </tr>
             <tr>
                      <td background="../images/site_bg_B7C1CB.jpg">
                         <!-- <table width="100%" border="0" cellspacing="1" cellpadding="2"> -->
                  		   
                  		   <nested:iterate indexId="indexId" scope="session" name="planCheckReassignForm" property="pcEngineerArray" id="pcEngineer" >
                  		   <logic:present name="pcEngineer" property="planCheckArray">
							<tr>
                                <td class = "tablelabel" colspan="8"><b><nested:write property="userName"/></b>
                                <a name='<nested:write property="userId"/>'></a> &nbsp;
<%--                                 <a href="javascript:printPlanCheck(<nested:write property='userId'/>);" class="hdrs"><img src="../images/print.gif" alt="Print List" data-toggle="tooltip" data-placement="top" title="Print List" border="0"></a></font> --%>
								<!-- <a target="_blank"  href="" data-toggle="tooltip" data-placement="top" title="Print"><img src="../images/printer.png" border='0'></a> -->
                                </td>
                            </tr>
                            <tr>
                            <logic:equal name="planCheckReassignForm" property="manager" value="Y">
                             <td width="4%" class="tablelabel"><font class="con_text_1">&nbsp;                             
	                             <nested:checkbox styleId="allTypeId"  property="selectAllCheckBox" value="true" onclick="selectAllCheckBox(this)" />
	                             <nested:hidden property="selectAllCheckBox" value="false" />
                             </font></td>
                             </logic:equal>
                              <td width="16%" class="tablelabel">Status Date</td>
                              <td width="16%" class="tablelabel">Target Completion Date</td>
                              <td width="16%" class="tablelabel">Activity #</td>
                              <td width="16%" class="tablelabel">Address</td>
                              <td width="16%" class="tablelabel">Plan Check Status</td>
                              <td width="16%" class="tablelabel" nowrap="nowrap">Process Type</td>
                              <td width="16%" class="tablelabel" nowrap="nowrap">Assigned Department</td>
                            </tr>
                            <nested:iterate id="testId"  property="planCheckArray" indexId="lindexId">
                            <tr>
                           <logic:equal name="planCheckReassignForm" property="manager" value="Y">
                           <td class="tabletext" width="10%" bgcolor="#FFFFFF">&nbsp;&nbsp;                          
	                             <nested:checkbox styleId="allTypeId"  property="check" value="true" onclick="selectCheckBox()" />
	                             <nested:hidden property="check" value="false" />
                           </td>
                           </logic:equal>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<nested:write  property="date"/></td>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<nested:write  property="trgdate"/></td>
<%--                              <logic:equal name="planCheckReassignForm" property="manager" value="Y"> --%>
<%--                             <nested:equal property = "actDept" value = "<%= deptCode%>"> --%>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<a href="javascript:showActivity(<nested:write  property="activityId"/>);" data-toggle="tooltip" data-placement="top"
                             title="ACTIVITY#:<nested:write  property="activityId"/>&#13;&#13;DESCRIPTION:<nested:write  property="actDesc"/>&#13;&#13;TYPE:<nested:write  property="actType"/>&#13;&#13;STATUS:<nested:write  property="actStatus"/>"><nested:write  property="actNbr"/></a></font></td>
<%--                             </nested:equal> --%>
<%--                             <nested:notEqual property = "actDept" value = "<%= deptCode%>"> --%>
<%--                             <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<nested:write  property="actNbr"/></td> --%>
<%--                             </nested:notEqual> --%>
<%--                             </logic:equal> --%>
<%--                             <logic:notEqual name="planCheckReassignForm" property="manager" value="Y"> --%>
<%--                             <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<nested:write  property="actNbr"/></td> --%>
<%--                             </logic:notEqual> --%>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<nested:write  property="address"/></td>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<a href="javascript:showPlanCheck(<nested:write  property="planCheckId"/>,'<nested:write  property="department"/>');"><nested:write  property="statusDesc"/></a></td>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1">&nbsp;<nested:write  property="processTypeDesc"/></td>
                            <td class="tabletext" width="16%" bgcolor="#FFFFFF" colspan="1" nowrap="nowrap">&nbsp;<nested:write  property="department"/></td>
                            </tr>
						   	</nested:iterate>
						   	</logic:present>
						   	</nested:iterate>
                        <!-- </table> -->
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            </table>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
</html:form>
<script type="text/javascript">
		 $("#fromdatedatepicker").kendoDatePicker({
                });
		 $("#todatedatepicker").kendoDatePicker({
         });
		 
		 </script>
</body>

</html:html>
<%@ page import='elms.agent.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Modify
Certificate</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<SCRIPT language="JavaScript">
//function textCounter(field,maxlimit) {
//alert(field.value);
//if (field.value.length > maxlimit) // if too long...trim it!
//field.value = field.value.substring(0, maxlimit);
// otherwise, update 'characters left' counter
//else
//countfield.value = maxlimit - field.value.length;
//}

function textCounter(field,maxlimit) {
//alert(field.value);
if (field.value.substring(0, maxlimit)) // if too long...trim it!
field.value = field.value.substring(0, maxlimit);
// otherwise, update 'characters left' counter
//else
//countfield.value = maxlimit - field.value.length;
}
</script>
</head>
<%
	String contextRoot = request.getContextPath();
	String actNBR = (String)request.getAttribute("actNBR");

	String actid = (String)request.getAttribute("actid");



    //String certificateURL=LookupAgent.getReportsUrl("CERTIFICATE_URL");
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction(){
	if (document.forms['certificateForm'].elements['certificate.ISSUED_DATE'].value != ''){
   		strValue=validateData('date',document.forms['certificateForm'].elements['certificate.ISSUED_DATE'],'Invalid date format');
   	    return strValue;
   	}else if (document.forms['certificateForm'].elements['certificate.INSPECTOR'].value == '-1'){
		alert("Please select inspector");
		document.forms['certificateForm'].elements['certificate.INSPECTOR'].focus();
		return false;
   	}else{
		strValue = true;
   	}
    return strValue;
}

function processPrint()
{
  actnbr=document.forms['certificateForm'].elements['certificate.actNbr'].value;
 // window.open('<%--=certificateURL--%>'+actnbr);

}

function onclickprocess()
{

	document.location.href ='<%=request.getContextPath()%>/viewActivity.do?activityId='+<%=actid%>;

}

function NumericEntry(){
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
	return false;
	}
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:form action="/saveCertificate"
	onsubmit="return validateFunction();">


	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Certificate Of Occupancy/Completion&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<br>
					<br>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="95%" background="../images/site_bg_B7C1CB.jpg"><font
										class="con_hdr_2b">Certificate </td>
									<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
										width="46" height="25"></td>
									<td width="10%" class="tablelabel"><nobr>
									<a target="_blank" href="<%=contextRoot%>/printCOO.do?actNbr=<%=actNBR%>"><font class="con_hdr_link">Print Certificate</a>
									 <html:button  property="back"  value="Back" styleClass="button" onclick="onclickprocess();" />
										<html:submit value="Update" styleClass="button" /> &nbsp;</nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Permit No: </td>
									<td class="tabletext"><html:text property="certificate.ACT_NBR" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Date Issued : </td>
									<td class="tabletext"><nobr> <html:text property="certificate.ISSUED_DATE" maxlength="10"
										styleClass="textboxd" onkeypress="return validateDate();" /> <html:link
										href="javascript:show_calendar('certificateForm.elements[3]');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;"><img src="../images/calendar.gif" width="16" height="15" border=0 />
									</html:link></nobr></td>
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Address :</td>
									<td class="tabletext"><html:text property="certificate.ACT_ADDR" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Owner Name :</td>
									<td class="tabletext"><html:text property="certificate.ACT_PEOPLE" size="30" maxlength="30" styleClass="textbox" /></td>

								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">A.P.N </td>
									<td class="tabletext"><html:text property="certificate.ACT_APN" maxlength="55" styleClass="textbox" /></td>
									<td class="tablelabel" colspan="1"><font class="con_hdr_1"> Occupancy Group </font> </td>
									<td class="tabletext" colspan="1"><html:text property="certificate.OCC_GRP" size="15" maxlength="10" styleClass="textbox" /></td>

    							</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Construction Type </td>
									<td class="tabletext"><html:text property="certificate.CONSTR_TYPE" size="25" maxlength="10" styleClass="textbox" /></td>

    								<td class="tablelabel"><font	class="con_hdr_1">Stories</td>
									<td class="tabletext"><html:text property="certificate.STORIES" size="25" maxlength="10" styleClass="textbox" onkeypress="return NumericEntry()"/></td>
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Building Area </td>
									<td class="tabletext"><html:text property="certificate.BLDG_AREA" size="25" maxlength="10" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1">Fire Sprinklers </td>
									<td class="tabletext"><html:text property="certificate.SPRINKLE" size="25" maxlength="10" styleClass="textbox" /></td>
								</tr>

							<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Garage Area</td>
									<td class="tabletext"><html:text property="certificate.GARAGE_AREA" size="25" maxlength="10" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1">Basement Area</td>
									<td class="tabletext"><html:text property="certificate.BSMT_AREA" size="25" maxlength="30" styleClass="textbox" /></td>
								</tr>

						<tr>
									<td class="tablelabel"><font	class="con_hdr_1">No of Dwellings</td>
								<td class="tabletext"><html:text property="certificate.DWELLING" size="25" maxlength="10" styleClass="textbox" onkeypress="return NumericEntry()"/></td>

							<td class="tablelabel"><font	class="con_hdr_1">Parking Spaces</td>
									<td class="tabletext"><html:text property="certificate.PARKING" size="25" maxlength="10" styleClass="textbox"/></td>

								</tr>

								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Inspector</td>
									<td class="tabletext">
										<html:select property="certificate.INSPECTOR" styleClass="textbox">
											<html:options collection="inspectorUsers" property="userId" labelProperty="inspectorId"/>
                                  		</html:select>
                                  	</td>
									<td class="tablelabel"><font	class="con_hdr_1">Building Offical</td>
									<td class="tabletext"><html:text property="certificate.BLDG_OFFICIAL" size="25" maxlength="30" styleClass="textbox" /></td>
								</tr>

						<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Building Code</td>
								<td class="tabletext" colspan="3"><html:textarea property="certificate.COND_CODE" rows="6" cols="60"/>
								</td>
                                      <!-- input type="hidden" name=remLen size="3" maxlength="3" value="450" -->
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Project Description

								<td class="tabletext"  colspan="3"><html:textarea property="certificate.ACT_DESCRIPTION" rows= "6" cols="60" onkeydown="textCounter(this,450);"  onkeyup="textCounter(this,450);"/></td>
								</tr>
							<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Special Conditions

								<td class="tabletext"  colspan="3"><html:textarea property="certificate.SPEC_CONDITIONS" rows= "6" cols="60" onkeydown="textCounter(this,450);"  onkeyup="textCounter(this,450);"/></td>
								</tr>


							</table>
			</td>
			<td width="1%"></td>
		</tr>
	</table>
</html:form>
</body>
</html:html>

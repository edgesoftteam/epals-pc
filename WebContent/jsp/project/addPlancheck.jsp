<%@page import="java.util.ArrayList"%>
<%@page import="elms.app.admin.EngineerUser"%>
<%@page import="elms.util.StringUtils"%>
<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<%@ page import='elms.common.Constants'%>
<%@ page import='java.util.List'%>
<app:checkLogon />
<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : PC/Approval</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>

<link rel="stylesheet" href="../css/kendo.default-v2.min.css"/>
<script language="javascript" type="text/javascript" src="../script/kendo.all.min.js"></script>
</head>
<script>
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");
</script>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" onload="tagetdateToBlank();"
	marginwidth="0" marginheight="0">

<%
   // Getting the employees as plan check engineers from the groups  1-Inspector, 6-Supervisor, 7-Engineer , 11- Permit Tech
   java.util.List engineerUsers = new ArrayList();
EngineerUser engineerUser = null;
engineerUser = new EngineerUser(0, "Unassigned");
engineerUsers.add(engineerUser);
 pageContext.setAttribute("engineerUsers", engineerUsers);
   
   List<Integer> plancheckId =(List)request.getAttribute("plancheckId");
   System.out.println("plancheckId ::"+plancheckId);
   List<Integer> pcId =(List)request.getAttribute("pcId");
   System.out.println("pcId ::"+pcId);
   List<Integer> pcIdBasedOnEngineer =(List)request.getAttribute("pcIdBasedOnEngineer");
   System.out.println("pcIdBasedOnEngineer ::"+pcIdBasedOnEngineer);
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   String activityId = (String)request.getAttribute("activityId");
  
   if(activityId ==null) activityId = "";
   if (lsoAddress == null ) lsoAddress = "";
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";
   String psaId = (String) request.getAttribute("psaId");
   //System.out.print("psaId::"+psaId);
   String psaType = (String) request.getAttribute("psaType");
   if (psaType ==null) psaType = "A";
   if (psaId ==null) psaId = "";
   int moduleId = 0;
   if(request.getAttribute("moduleId") != null){
	   moduleId = Integer.parseInt(request.getAttribute("moduleId").toString());
   }
   //String showAddButton = (String)request.getParameter("showAddButton");
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/elms.css" type="text/css">
<script language="JavaScript" src="../script/editActivity.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/util.js"></script>
<script language="JavaScript">
var strValue;
function validateFunction(button) {
	if (document.forms[0].elements['planCheckStatus'].value == '-1')  {
	    	alert('Please select status');
			document.forms[0].elements['planCheckStatus'].focus();
	    	return false;
	}else if (document.forms[0].elements['planCheckDate'].value == '')  {
	    	alert('Please select date');
            document.forms[0].elements['planCheckDate'].focus();
	    	return false;
// 	}else if (document.forms[0].elements['targetDate'].value == '')  {
// 	    	alert('Please select target date');
// 	        document.forms[0].elements['targetDate'].focus();
// 	    	return false;
	}else if(!validateData('date',document.forms[0].elements['planCheckDate'],'Invalid date format')){
			document.forms[0].elements['planCheckDate'].focus();
			return false;
// 	}else if(!validateData('date',document.forms[0].elements['targetDate'],'Invalid date format')){
// 			document.forms[0].elements['targetDate'].focus();
// 			return false;
	}else if(document.forms[0].elements['department'].value == ''){
	    	alert('Please select department');
	        document.forms[0].elements['department'].focus();
	    	return false;
	}else {
			var targetDate=document.forms[0].elements['targetDate'].value;
		//	var completionDate=document.forms[0].elements['completionDate'].value;
			var planCheckDate=document.forms[0].elements['planCheckDate'].value;
			
			var str=compareDate(targetDate,planCheckDate);
			if(str==true){
			alert("Target completion date cannot be less than Status Date");
			return false;
			}
	}
	button.disabled=true;
   	document.forms[0].action = document.forms[0].action + "?action=save";
	document.forms[0].submit();
}

function goBack(button)
{
	//button.value='Please Wait..';
	button.disabled=true;
	document.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
}

function checkDate (field) {
	return validateData('date',field,'Date is not Valid');
}

function addPC()
{
	document.location.href='<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>&showAdd=yes';
}
function sort(column){
	
	parent.f_content.location.href='<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>&action=sorting&sortingField='+column;
	
}

function tagetdateToBlank(){
	document.forms[0].elements['targetDate'].value = "";
	return;
}
function updateTargetDate(processType){
	var daysToAdd = 0;
<%-- 	if(processType == <%= Constants.PC_PROCESS_REGULAR%>){ --%>
<%-- 		daysToAdd = <%= Constants.PC_DAYS_TO_ADD_REGULAR%>; --%>
<%-- 	}else if(processType == <%= Constants.PC_PROCESS_FAST_TRACK%>){ --%>
<%-- 		daysToAdd = <%= Constants.PC_DAYS_TO_ADD_FAST_TRACK%>; --%>
<%-- 	}else if(processType == <%= Constants.PC_PROCESS_EXPRESS%>){ --%>
// 		document.forms[0].elements['targetDate'].value = "";
// 		return;
// 	}else{
// 		daysToAdd = 0;
// 	}
	
	
	if(processType == <%= Constants.PC_PROCESS_ADU%>){
		daysToAdd = <%= Constants.PC_DAYS_TO_ADD_ADU%>;
	}else if(processType == <%= Constants.PC_PROCESS_REGULAR%> || processType == <%= Constants.PC_PROCESS_FAST_TRACK%> || processType == <%= Constants.PC_PROCESS_FAST_TRACK%> || processType == <%= Constants.PC_PROCESS_OTC%> ){
		document.forms[0].elements['targetDate'].value = "";
		return;
	}else{
		daysToAdd = 0;
	}
	dateStr = document.forms[0].elements['created'].value;
	dateStr = addDays(dateStr,daysToAdd);
	document.forms[0].elements['targetDate'].value = dateStr;
}
function addDays(pcDate,noofdays) {
	var trgDate = new Date(pcDate);
	trgDate.setDate(trgDate.getDate() + noofdays);
	var dd = trgDate.getDate();
	var mm = trgDate.getMonth() + 1;
	var y = trgDate.getFullYear();
	var formattedDate = mm + '/'+ dd + '/'+ y;
	return formattedDate;
	}

/* function actstatus(act){
if(act.value!=''){

	checkStatus(act);
}
}
 */
function getUsers(dept){
	if(dept == ""){
		var box = document.forms[0].elements['engineerId'];
		box.length = 0;
        var no = new Option();
        no.value = 0;
        no.text = "Unassigned";
        box[0] = no;
		return true;
	}	
	var url = "<%=request.getContextPath()%>/addPlanCheck.do?action=getUsers&deptDesc="+dept;
	xmlhttp.open('POST', url,false);
	xmlhttp.onreadystatechange = getUsersHandler;
    xmlhttp.send(null);
}
function getUsersHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
			   	var box = document.forms[0].elements['engineerId'];
			   	engineerUsers = result.split("$@$");
			    box.length = 1;
				c=0;
				for(c=1; c<engineerUsers.length; c++) {
			          var no = new Option();
			          var engineerUserDetail = engineerUsers[c].split("#@#");
			          no.value = engineerUserDetail[0];
			          no.text = engineerUserDetail[1];
			          box[c] = no;
			     }
 	}
}

function getStatusList(dept){
	if(dept == ""){
		var box = document.forms[0].elements['engineerId'];
		box.length = 0;
        var no = new Option();
        no.value = 0;
        no.text = "Please Select";
        box[0] = no;
		return true;
	}	
	var url = "<%=request.getContextPath()%>/addPlanCheck.do?action=getStatusList&deptDesc="+dept;
	xmlhttp.open('POST', url,false);
	xmlhttp.onreadystatechange = getStatusListHandler;
    xmlhttp.send(null);
}
function getStatusListHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
			   	var box = document.forms[0].elements['planCheckStatus'];
			   	statuses = result.split("$@$");
			    box.length = 1;
				c=0;
				for(c=1; c<statuses.length; c++) {
			          var no = new Option();
			          var statusDetail = statuses[c].split("#@#");
			          no.value = statusDetail[0];
			          no.text = statusDetail[1];
			          box[c] = no;
			     }
 	}
}

function selectAllCheckBoxFunc(obj){
	var count = 1;
	var value = obj.checked;
	var checkBox = 'planChecks[0].check';
	while(document.forms[0].elements[checkBox]){
		document.forms[0].elements[checkBox].checked = value;
		checkBox = 'planChecks['+ count  +'].check';
		count++;
		}
	}

function deletePlanChecks(){
	var i = 0;
	var count =0;
	var name = "planChecks["+i+"].check";
	
	while(document.forms[0].elements[name] != null){
		if(document.forms[0].elements[name].checked){
			count = count + 1;
		}
		i++;
		name = "planChecks["+i+"].check";
	''	}
	if(count == 0){
		alert("Please select a record to delete.");
	}else{
		document.forms[0].action = document.forms[0].action + "?action=delete";
		document.forms[0].submit();
	}
}

</script>

<html:form action="/savePlanCheck">
	<html:hidden property="activityId" />
    <html:hidden property="pcApproved"/>
    <html:hidden property="permitFinal"/>
	<html:hidden property="todaysDate" />
	<html:hidden property="created"/>
	
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">PC/Approval</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<font class="con_hdr_blue_3b"><%=lsoAddress%> <%=psaInfo%></font><br></td>
				
			</tr>
			<html:errors/>
			<%if(request.getAttribute("showAdd") != null &&  request.getAttribute("showAdd").toString().equalsIgnoreCase("yes")){ %>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" class="tabletitle"><font
											class="con_hdr_2b">Add</font></td>
										<!-- <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
											width="26" height="25"></td> -->
										<td width="1%" class="tablelabel">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="goBack(this);" />&nbsp;
												 <security:editable levelId="<%=psaId%>" levelType="<%=psaType%>" editProperty="allProjectAndDept">
											 <security:editable levelId="<%=psaId%>" levelType="<%=psaType%>" editProperty="editable">
												<html:button property="save" value="Save" styleClass="button" onclick="return validateFunction(this);"/>
												</security:editable>
												</security:editable>
												 &nbsp;
											</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td class="tablelabel" width="116">Plan Check Status<font class="red1"><strong>*</strong></font></td>
                                        <td class="tablelabel" width="162">Department<font class="red1"><strong>*</strong></font></td> 
                                        <td class="tablelabel" width="191">Staff</td>
										<td class="tablelabel" width="165">Status Date<font class="red1"><strong>*</strong></font></td>
										<td class="tablelabel" width="162">Target Completion Date
<!--                                        	<font class="red1"><strong>*</strong></font> -->
                                       	</td> 
										<td class="tablelabel" width="165">Process Type</font></td>
                                       	
										<td class="tablelabel" width="117">Time Units</td>
									</tr>
									<tr valign="top">
										<td class="tabletext" bgcolor="#FFFFFF" width="116"><html:select property="planCheckStatus"
											styleClass="requiredtext" style="font-size:10px;">
											<html:options collection="planCheckStatuses" property="code"
												labelProperty="description" />
										    </html:select>
										</td>
										  <td class="tabletext" bgcolor="#FFFFFF"  valign="top" width="162"><nobr>
                                          <html:select property="department" styleClass="requiredtext" onchange="getUsers(this.value)" style="font-size:10px;">
                                          <html:option value="">Please Select</html:option>
											<html:options collection="deptList" property="departmentId"
												labelProperty="description" />
										    </html:select> 
                                        </td>
                                        
                           			    <td class="tabletext" bgcolor="#FFFFFF" width="117"><html:select property="engineerId" styleClass="textbox">
                           			                             			    
                           			    <html:options collection="engineerUsers" property="userId"
												labelProperty="name" />
                           			    </html:select> 
										</td>
                                        <td class="tabletext" bgcolor="#FFFFFF"  valign="top" width="165">
                                         
                                            <html:text property="planCheckDate" size="10" styleId="plancheckdatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13"/>
					                    </td>	
										<td class="tabletext" bgcolor="#FFFFFF"  valign="top" width="193">
                                          
                                            <html:text property="targetDate" size="10" styleId="targetdatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13"/>
                                            
                                        </td>
                                        	
										<td class="tabletext" bgcolor="#FFFFFF" width="116"><html:select property="processType" onchange="updateTargetDate(this.value)"
											styleClass="textbox">
											<html:options collection="planCheckProcessTypeList" property="processTypeId" labelProperty="processType" />
										    </html:select>	
										</td>
                                         
                                     <td class="tabletext" bgcolor="#FFFFFF" valign="top" width="191">
                                               <nobr><%-- <nested:text  property="unit"  maxlength="10"  styleClass="textbox" onkeypress="return validateDecimal();"  onblur=""/> --%>  
                                                <html:select property="unitHours" styleClass="textbox">
                                             <html:option value=''>HH</html:option>
                                             <% 
                                             for( int i=0;i<24;i++){
                                            	if(i<10){	 %>
                                            	<html:option value='<%= StringUtils.i2s(i)  %>'> <%="0"+i %></html:option>
                                            	<%}else{ %>
                                             <html:option value='<%= StringUtils.i2s(i)  %>'> <%=i %></html:option>
                                             <%} }%>
                                             </html:select>
                                             <html:select property="unitMinutes" styleClass="textbox">
                                             <html:option value=''>MM</html:option>
                                             <% 
                                             for( int j=0;j<60;j++){ 
                                             if(j<10){
                                             %>
                                             <html:option value='<%=StringUtils.i2s(j)%>'> <%="0"+j %></html:option>
                                             <%}else{ %>
                                             <html:option value='<%=StringUtils.i2s(j)%>'> <%=j %></html:option>
                                             <%}} %>
                                             </html:select>
                                        </td>
									</tr>
									<tr>
										<td class="tablelabel">Comments</td>
										<td class="tabletext" bgcolor="#FFFFFF" colspan="3" ><html:textarea
											property="comments" cols="75" rows="7" styleClass="textbox" /></td>
											
										<td class="tablelabel">Email</td>
										<td class="tabletext" colspan="2"><html:checkbox property = "emailflag"></html:checkbox></td>	
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="32">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
	</table>
	<%}else{ %>
	
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<TBODY>
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
										  <td width="99%" class="tabletitle"><font class="con_hdr_2b">PC/Approval</font></td>
										  <!-- <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="21" height="20"></td> -->
										  <td width="1%" class="tablelabel"><nobr>
										  <html:button property="back" value="Back" styleClass="button" onclick="goBack(this);" />&nbsp; 
										 
										  <security:editable levelId="<%=psaId%>" levelType="<%=psaType%>" editProperty="allProjectAndDept">
										  <security:editable levelId="<%=psaId%>" levelType="<%=psaType%>" editProperty="editable">
										  <%if(request.getAttribute("showAdd") == null || !request.getAttribute("showAdd").toString().equalsIgnoreCase("yes")){ %>
										 <%--  <%if(request.getAttribute("showAddButton") != null && request.getAttribute("showAddButton").toString().equalsIgnoreCase("yes")){ %> --%>
										 <html:button property="add" value="Add" styleClass="button" onclick="addPC();" />&nbsp;
										   
										  <html:button property="delete" value="Delete" styleClass="button" onclick="deletePlanChecks();" />&nbsp;
										   <%} //}%>
										   </security:editable>
										   </security:editable>
										
										  
										 
										 </nobr></td>
										 </tr>
										 
									</table>
									</td>
								</tr>
								<tr>
									<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2" style='table-layout:fixed'>
										<TBODY>
											<tr>
									<td class="tablelabel" width = "5%" align="center">&nbsp;                             
			                             <nested:checkbox styleId="allTypeId"  property="selectAllCheckBox" value="true" onclick="javascript:selectAllCheckBoxFunc(this)" />
		                             </td>
		                             
												<td class="tablelabel"><nobr><a href=javascript:sort('S.pc_desc')>Plan Check Status</a></nobr></td>
                                                <td class="tablelabel"><a href=javascript:sort('PC.department')>Department</a></td>
												<td class="tablelabel"><a href=javascript:sort('name')>Staff</a></td>
												<td class="tablelabel"><a href=javascript:sort('PC.plan_chk_date')>Status Date</a></td>
												<td class="tablelabel"><a href=javascript:sort('PC.target_date')>Target Date</a></td>
                                                <td class="tablelabel"><a href=javascript:sort('PC.PROCESS_TYPE')>Process Type</td>
												<td class="tablelabel"><a href=javascript:sort('PC.comnt')>Comments</a></td>
                                                <td class="tablelabel" width = "5%"> Edit</td>
											</tr>
												 <%   int count =0;
												 int count1 =0;
												 %>
												 <tr>
												
											<nested:iterate name="planCheckForm" scope="session" property="planChecks" id = "plancheckobj">
											
											
												<tr valign="top" onmouseover="changeColorParentCss(<nested:write property="planCheckId"/>,'#b2bbcf');" onmouseout="changeColorParentCss(<nested:write property="planCheckId"/>,'#FFFFFF');">	
																
						                        <%						                         
						                          boolean value=true;
						                         for(Integer i : plancheckId){
						                        	 System.out.println("element--"+count+"  --  "+pcId.get(count));
						                        	 if(pcId.get(count).equals(i)){
						                        		 //System.out.println("element"+pcId.get(count));
						                        		 value = false;
						                        	 }
						                         }
						                         count++;  
						                       if(value){
						                        	 %>
						                        	
						                        		<td class="tabletext" width = "5%" align="center">&nbsp;					                        								                             
										                 <nested:checkbox styleId="allTypeId"  property="check" value="true"  > 
										                 </nested:checkbox>
										                </td>
						                       		 <%	
						                         }else{%>
													<td class="tabletext" width = "5%" align="center">&nbsp;
														<nested:checkbox styleId="allTypeId"  property="check" disabled="true" /> 				                             									                  
						                             </td>						
			                     		 <%   } %> 
			                     		 			<td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;"><b><nested:write property="statusDesc" /></b></td>
                                                    <td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;" width = "5%"><b><nested:write property="deptDesc" /></b></td>
													<td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;"><nested:write property="engineerName" /></td>
			                     					<td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;"><nested:write property="date" /></td>
													<td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;"><nested:write property="trgdate" /></td>
                                                    <td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;"><nested:write property="processTypeDesc" /></td>
													<td class="tabletext changecolorParent_<nested:write property="planCheckId"/> showPlanCheckHistory title_<nested:write property="planCheckId"/>" data-title2="Hide Plan Check History" title="Show Plan Check History" id="<nested:write property="planCheckId"/>" style="cursor:pointer;word-wrap: break-word;"><nested:write property="comments" /></td>
                                                    
			                     		  <%						                         
						                          boolean value1=true;
			                     		  		  if(pcIdBasedOnEngineer != null && pcIdBasedOnEngineer.size()>0){
						                          for(Integer i : pcIdBasedOnEngineer){
						                        	 //System.out.println("element--"+count1+"  --  "+pcId.get(count1));
						                        	 if(pcId.get(count1).equals(i)){
						                        		 //System.out.println("element"+pcId.get(count1));
						                        		 value1 = false;
						                        	 }
						                          }
						                         count1++;  
						                       if(!value1){
						                        	 %>
													<td class="tabletext changecolorParent_<nested:write property="planCheckId"/>" align="center">
														<a href='<%=contextRoot%>/editPlanCheck.do?planCheckId=<nested:write property="planCheckId"/>'>
															<img src="../images/pencil.gif" alt="Edit" border="0"></a></td>
															<%	
						                         }else{%>
													<td class="tabletext changecolorParent_<nested:write property="planCheckId"/>" align="center" ></td>
													 <%   }}else{%> 
													 <td class="tabletext changecolorParent_<nested:write property="planCheckId"/>" align="center" >
														<a href='<%=contextRoot%>/editPlanCheck.do?planCheckId=<nested:write property="planCheckId"/>'>
															<img src="../images/pencil.gif" alt="Edit" border="0"></a></td>
													 <%} %>
													
												</tr>
												<tr id="show_details_<nested:write property="planCheckId"/>" style="display:none;" >
												<td class="tablelabel"> <b>History</b></td> 
						
												<td background="../images/site_bg_B7C1CB.jpg" colspan="8">
												<table width="100%" border="0" cellspacing="1" cellpadding="2"  style='table-layout:fixed'>
												<TBODY>
												<tr>
												
												<td class="tablelabel"><nobr><b>Plan Check Status</b></nobr></td>
                                                <td class="tablelabel"><b>Department</b></td>
												<td class="tablelabel"><b>Staff</b></td>
												<td class="tablelabel"><b>Status Date</b></td>
												<td class="tablelabel"><b>Target Date</b></td>
												<td class="tablelabel"><b>Comments</b></td>
											</tr>
											<logic:present name="plancheckobj" property="planCkeckHistories">
											<nested:iterate name = "plancheckobj" property="planCkeckHistories">
												<tr valign="top" class= "detail_<nested:write property="pcHistoryId"/> changecolor_<nested:write property="pcHistoryId"/> changecolorParent_<nested:write property="planCheckId"/>">
													
													<td class="tabletext changecolor_<nested:write property="planCheckId"/>"><nested:write
														property="statusDesc" /></td>
                                                    <td class="tabletext changecolor_<nested:write property="planCheckId"/>"><nested:write
														property="deptDesc" /></td>
													<td class="tabletext changecolor_<nested:write property="planCheckId"/>"><nested:write
														property="engineerName"/></td>
													<td class="tabletext changecolor_<nested:write property="planCheckId"/>"><nested:write
														property="date" /></td>
													<td class="tabletext changecolor_<nested:write property="planCheckId"/>"><nested:write
														property="trgdate" /></td>
													<td class="tabletext changecolor_<nested:write property="planCheckId"/>" style="word-wrap: break-word;"><nested:write
														property="comments" /></td>
												</tr>
												
											</nested:iterate>
											</logic:present>
										</TBODY>
									</table> 
									</td>
						
												</tr>
											</nested:iterate>
											<%-- <%} %> --%>
											<!--  to be added -->
						
											</TBODY>
									</table> 
									</td>
								</tr>
							</TBODY>
						</table>
						</td>
					</tr>
					</TBODY>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<TBODY>
						<tr>
							<td height="32">&nbsp;</td>
						</tr>
					</TBODY>
				</table>
				</td>
			</tr>
		</TBODY>
	</table>


<%} %>
</html:form>
<script type="text/javascript">
		 $("#plancheckdatepicker").kendoDatePicker({
                });
		 $("#targetdatepicker").kendoDatePicker({
         });
		
			$(function(){
				$(document).ready(function() {
			
			//show PC History details
			 $(".showPlanCheckHistory").click(function(){
				  var id = $(this).attr("id");
				  var t1 = $(this).attr("title");
				  var t2 = $(this).data("title2")
				  changecolorCss(id,"#f4f0f0");
				  $("#show_details_"+id).toggle();
				  if(t2){
					  $(".title_"+id).attr('title', t2).data('title2', t1);
				  };
			});
				});
				
			});
			 function changecolorCss(id,color){
				 	$('.changecolor_'+id).css({backgroundColor: color});
				}
			function changeColorParentCss(id,color){
				//alert(id+":"+color);
 				$('.changecolorParent_'+id).css({backgroundColor: color});
			}
		 </script>

</body>
</html:html>


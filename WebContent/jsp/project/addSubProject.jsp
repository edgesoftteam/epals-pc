<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
String projectName = (String)session.getAttribute("projectName")!=null?(String)session.getAttribute("projectName"):"";
%>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	strValue=false;
	if (document.forms['subProjectForm'].elements['subProject'].value == '')
	{
		alert("Select Sub-Project name");
		document.forms['subProjectForm'].elements['subProject'].focus();
		strValue=false;
	}
	else if (document.forms['subProjectForm'].elements['type'].value == '')
	{
		alert("Select Sub-Project Type");
		document.forms['subProjectForm'].elements['type'].focus();
		strValue=false;
	}
	else if (document.forms['subProjectForm'].elements['subType'].value == '')
	{
		alert("Select Sub-Project Sub Type");
		document.forms['subProjectForm'].elements['subType'].focus();
		strValue=false;
	}
	else if (document.forms['subProjectForm'].elements['subProject'].value != '-1')
	{
    	strValue=validateData('req',document.forms['subProjectForm'].elements['description'],'Description is a required field');
    }
	else
	{
		strValue=true;
	}
	return strValue;
}
function fromptList()
{
    document.forms[0].action='<%=contextRoot%>/addSubProject.do?from=ptList&projectName=<%=projectName%>';
    document.forms[0].elements['type'].disabled=true;
    document.forms[0].elements['subType'].disabled=true;
    document.forms[0].submit();
    return true;
}
function fromsptList()
{
   document.forms[0].action='<%=contextRoot%>/addSubProject.do?projectName=<%=projectName%>';
    document.forms[0].elements['subType'].disabled=true;
   document.forms[0].submit();
   return true;

}

</SCRIPT>
<%
 	String projectId = request.getParameter("projectId");

 	String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";
    String psaInfo = (String)session.getAttribute("psaInfo");
    if (psaInfo == null ) psaInfo = "";

%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form name="subProjectForm" type="elms.control.beans.SubProjectForm" action="/saveSubProject" onsubmit="return validateFunction();">
<html:hidden property="projectId" value="<%=projectId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Sub-Project&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Add Sub-Project </td>
                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
                          <html:submit property="Save" value="Save" styleClass="button" onclick="return validateFunction();"/>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Sub-Project Name</td>
                      <td class="tablelabel">Type</td>
                      <td class="tablelabel">Sub-Type</td>
                      <td class="tablelabel">Description</td>
                    </tr>
                    <tr>
                      <td class="tabletext">
                        <html:select property="subProject" styleClass="textbox" onchange="return fromptList()">
                        	 <html:option value="">Please Select</html:option>
                             <html:options collection="ptList"  property="projectTypeId"  labelProperty="description" />
                        </html:select>
                      </td>
                      <td class="tabletext">
                        <html:select property="type" styleClass="textbox"  onchange="return fromsptList()">
                        	 <html:option value="">Please Select</html:option>
                             <html:options collection="sptList" property="subProjectTypeId" labelProperty="description" />
                        </html:select>
                      </td>
                      <td class="tabletext">
                        <html:select property="subType" styleClass="textbox">
                              <html:options collection="spstList" property="subProjectSubTypeId" labelProperty="description" />
                        </html:select>
                      </td>
                      <td class="tabletext">
                        <html:text  property="description" size="25" maxlength="2000" styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

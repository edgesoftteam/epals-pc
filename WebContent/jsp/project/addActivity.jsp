<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="elms.control.beans.ActivityForm,elms.util.*,java.text.SimpleDateFormat,java.util.*"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<%@ page import='elms.app.admin.CustomField' %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>


</head>

<%

int count = 0;
String contextRoot = request.getContextPath();
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";

 ActivityForm activityForm = (ActivityForm)request.getAttribute("activityForm");

if(activityForm == null){
  activityForm =  (ActivityForm)session.getAttribute("activityForm");
}

String levelId = "";
String levelType = "";
if(activityForm != null){
  levelId = activityForm.getActivityId();
  levelType = "A";

  if(levelId == null || levelId == "0"){
  System.out.println("activityForm.getSubProjectId(): "+activityForm.getSubProjectId());
    levelId = activityForm.getSubProjectId();
    levelType = "Q";
  }
}
Calendar cal = Calendar.getInstance();
 //String a=cal.getTime()+"";
 String currentDate= StringUtils.cal2str(Calendar.getInstance());
  SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
 String strCalendar = formatter.format(cal.getTime());
 
 String wmFlag = (String)request.getAttribute("wmFlag");
%>

<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>	
<script language="JavaScript">

function reSubmit(){
  document.forms['activityForm'].action='<%=contextRoot%>/addActivity.do';
  document.forms['activityForm'].submit();
  return true;
}

function save() {
if (validateFunction()) {
	 document.forms[0].action='<%=contextRoot%>/saveActivity.do';
	  if(document.forms[0].elements['planCheckRequired'] != null){
		  document.forms[0].elements['tempPlanCheckRequired'].value = document.forms[0].elements['planCheckRequired'].checked;
		  }
	 document.forms[0].submit();
	 return true;
} else {
	  return false;
	}
}
function cancel(){
   document.forms['activityForm'].action='<%=contextRoot%>/cancelAddActivity.do';
   document.forms['activityForm'].submit();
   return true;
}

$( document ).ready(function() {
	<% CustomField[] customFields =  activityForm.getCustomFieldList(); 
		for(int i = 0; i < customFields.length; i++){
			if(i % 2==0){
	%>
	$("#customFields<%=i%>").css("display","none");    
    
     $("#wmRequired").click(function() {
        if (this.checked) {
        	$("#customFields<%=i%>").css("display","table-row");
        }else{
        	$("#customFields<%=i%>").css("display","none");
        }
    });
    
    <% }} %>
});

</script>


<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form   name="activityForm" type="elms.control.beans.ActivityForm" action="">
<html:hidden property="subProjectId"/>
<html:hidden property="origin"/>
<html:hidden property="displayContent"/>
<html:hidden property="completionDateLabel"/>
<html:hidden property="expirationDateLabel"/>
<html:hidden property="tempPlanCheckRequired"></html:hidden>


<input type="hidden" name="dateChange" value="<%=strCalendar%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Activity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Add Activity</td>
                      <td width="1%" class="tablebutton"><nobr>
                          <html:button  property="Cancel"  value="Cancel" styleClass="button" onclick="cancel();"/>
                          <html:button property="Save" value="Save" styleClass="button" onclick="return save();"/>
                       &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                      <jsp:include page="../includes/addActivityFields.jsp" flush="true">
                         <jsp:param name="thisForm" value="activityForm"/>
                      </jsp:include>
                        <%  int i=0 ;   %>
                    <nested:iterate  property="customFieldList">
                 
                              <nested:hidden property="fieldId" />
                              <nested:hidden property="fieldType" />
                              <nested:hidden property="fieldLabel" />
                             <nested:hidden property="fieldRequired" />
                     <%
                     	CustomField[] customFieldsArray =  activityForm.getCustomFieldList();
                     	String style = "";
                     	if(customFieldsArray[i].getFieldRequired().equals("Y")) {
                     		style = "requiredtext";
                     	}
                     	else {
                     		style = "textbox";
                     	}
                     %>
                     
                          <%if(i % 2==0) {  %>   <tr bgcolor="#FFFFFF" id="customFields<%=i%>">
                                                                    <td class="tablelabel"><nested:write property="fieldLabel" /></td>
                                                                    <td  class="tabletext" valign="top"> 
                                                                    <%if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DROPDOWN")){
                                                                    String attributeName = "dropdownOptionList" + customFieldsArray[i].getFieldId();
                                                                    %>
                                                                    <nested:select styleClass="<%=style%>" property="fieldValue">
                                                                    		<html:option value="">Please Select</html:option>	
																			<html:options collection="<%= attributeName %>" property="id" labelProperty="label" />
                                                                    </nested:select>
                                                                    <%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("CHECKBOX")){%>
                                                                    	<nested:checkbox styleClass="<%=style%>"  property="fieldValue" value="Y"></nested:checkbox>
                                                                    <%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DATE")){%>
                                                                    <nested:text onfocus="showCalendarControl(this);" styleClass="<%=style%>" property="fieldValue" />
                                                                    </td>
                                                                    <%}else{%>
                                                                    <nested:text styleClass="<%=style%>" property="fieldValue" />
                                                                    <%}%>
                                                                    </td>
                                                                   
                          <% } else  {%>                           
                                                                    <td class="tablelabel"><nested:write property="fieldLabel" /></td>
                                                                    <td  class="tabletext" valign="top"> 
                                                                    <%if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DROPDOWN")){
                                                                    String attributeName = "dropdownOptionList" + customFieldsArray[i].getFieldId();
                                                                    %>
                                                                    <nested:select styleClass="<%=style%>" property="fieldValue">
                                                                    		<html:option value="">Please Select</html:option>
																			<html:options collection="<%= attributeName %>" property="id" labelProperty="label" />
                                                                    </nested:select>
                                                                    <%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("CHECKBOX")){%>
                                                                    	<nested:checkbox styleClass="<%=style%>" property="fieldValue" value="Y"></nested:checkbox>
                                                                    <%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DATE")){%>
                                                                    <nested:text onfocus="showCalendarControl(this);" styleClass="<%=style%>" property="fieldValue" />
                                                                    </td>
                                                                    <%} else{%>
                                                                    	<nested:text styleClass="<%=style%>" property="fieldValue" maxlength="256" />
                                                                    <%}%>
                                                                    
                                                                    </td>
                                                                     <% }%>

			             <%if(i % 2==1) {  %>     </tr>    <% } i++ ;%>  
                    
                    </nested:iterate>
                    <% if(count % 2==1){%>   <td bgcolor="#FFFFFF" colspan="6" > </td> </tr>     <% } %> 
                 
                   </table>
                </td>
              </tr>
            </table>
           </td>
        </tr>
      </table>
    </td>
  </tr>

</table>
</html:form>
</body>
</html:html>

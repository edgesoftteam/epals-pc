<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*, elms.util.*,elms.agent.*,elms.common.*,elms.app.bl.*,elms.app.bt.*,elms.app.common.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<app:checkLogon/>

<%
	String contextRoot = request.getContextPath();
	elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);
	String editable = (String) request.getAttribute("editable");
	String lsoAddress = (String) session.getAttribute("lsoAddress");
	String totalCondCount = (String) request.getAttribute("totalConditionCount");
	String warnCondCount = (String) request.getAttribute("warnConditionCount");
	
	String deletable = (String)request.getAttribute("deletable");

	String one="one";
   	request.setAttribute("one",one);
	String onloadAction = "";
	boolean  statusEdit;
	try {
		onloadAction = (String)session.getAttribute("onloadAction");
	}
	catch (Exception e) {
	}

    String psaId = (String)session.getAttribute(Constants.PSA_ID);
    String lsoId1 = (String)session.getAttribute(Constants.LSO_ID);

    String lsoType = (String) session.getAttribute(Constants.LSO_TYPE);

    String psaType = (String)session.getAttribute(Constants.PSA_TYPE);
	String activityId =   (String)request.getAttribute("activityId");

	String activityType= (String)request.getAttribute("activityType");

	if (activityType == null) activityType = "";

	String level = "A";
	session.setAttribute("levelId",activityId);
	session.setAttribute("level",level);
	session.setAttribute("onloadAction","");

	String activityTypeDesc= (String)request.getAttribute("activityTypeDesc");
	if (activityTypeDesc == null) activityTypeDesc = "";
	List activitySubTypes=(ArrayList)request.getAttribute("activitySubTypes");
	if (activitySubTypes == null) activitySubTypes = new ArrayList();
	String activityNumber = (String)request.getAttribute("activityNumber");
	if (activityNumber == null) activityNumber = "";
	String description = (String)request.getAttribute("description");
	if (description == null) description = "";
	String address = (String)request.getAttribute("address");
	if (address == null) address = "";
	String valuation = (String)request.getAttribute("valuation");
	if (valuation == null) valuation = "";
	String planCheckRequired = (String)request.getAttribute("planCheckRequired");
	if (planCheckRequired == null) planCheckRequired = "N";
	String developmentFeeRequired = (String)request.getAttribute("developmentFeeRequired");
	if (developmentFeeRequired == null) developmentFeeRequired = "N";
	String inspectionRequired = (String)request.getAttribute("inspectionRequired");
	if (inspectionRequired == null) inspectionRequired = "N";
	String issueDate = (String)request.getAttribute("issueDate");
	if (issueDate == null) issueDate = "";
	String startDate = (String)request.getAttribute("startDate");
	if (startDate == null) startDate = "";
	String completionDate = (String)request.getAttribute("completionDate");
	if (completionDate == null) completionDate = "";
	String expirationDate = (String)request.getAttribute("expirationDate");
	if (expirationDate == null) expirationDate = "";
	String createdBy = (String)request.getAttribute("createdBy");	
	if (createdBy == null) createdBy = "";
	
	String onlineApplication = (String)request.getAttribute("onlineApplication");
	if (onlineApplication != null && onlineApplication.equalsIgnoreCase("Y")){
		createdBy = "Online User";
	} 
	
	String label = (String)request.getAttribute("label");
	if (label == null) label = "";
	String greenHalo = ("on".equalsIgnoreCase((String)request.getAttribute("greenHalo")))?"Yes":"No";

	String parkingZoneDescription = (String)request.getAttribute("parkingZoneDescription");
	if (parkingZoneDescription == null) parkingZoneDescription = "";

	String completionDateLabel = (String)request.getAttribute("completionDateLabel");
	if (completionDateLabel == null) completionDateLabel = "";
	String expirationDateLabel = (String)request.getAttribute("expirationDateLabel");
	if (expirationDateLabel == null) expirationDateLabel = "";

	String newUnits = (String)request.getAttribute("newUnits");
	if (newUnits == null) newUnits = "";

	String existingUnits = (String)request.getAttribute("existingUnits");
	if (existingUnits == null) existingUnits = "";

	String demolishedUnits = (String)request.getAttribute("demolishedUnits");
	if (demolishedUnits == null) demolishedUnits = "";

	String microfilm = (String)request.getAttribute("microfilm");

	List customFieldsList= new ArrayList();
	customFieldsList= (List) request.getAttribute("customFieldsList");

	List lncvList = new ArrayList();
	lncvList = (List) (request.getAttribute("lncvList"));
	
	int lncvUsed = 0;
	lncvUsed = lncvList.size();
	int lncvAvail = 0;
	lncvAvail = 96 - lncvUsed;


	int lsoId= StringUtils.s2i((String)request.getAttribute("lsoId"));

    boolean businessTaxProject=false,businessLicenseProject=false,lcUser=false,pdUser = false,businessLicenseUser = false, businessLicenseApproval = false, businessTaxUser = false, businessTaxApproval = false, codeInspector=false;
    /* boolean isPlanCheckManager = false;
	boolean isPlanCheckReviewEngineer = false; */
    String username = user.getUsername()!=null ? user.getUsername():"";
   	if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("PD")) pdUser = true;
   	if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("LC")) lcUser = true;
   	String statusCheckForInactivity =  (String)request.getAttribute("status");
	if(statusCheckForInactivity.equals("Suspense")){statusCheckForInactivity="Active";}
	String status=statusCheckForInactivity;


	
	request.setAttribute("status1",status);
    boolean   InactiveEditPermission=false;
    if(status.equals("Active") || status.equals("Suspense")||((Boolean)request.getAttribute("activityInactiveEditPermission")).booleanValue()==true ) {
         InactiveEditPermission=true;
    }

   	java.util.List groups = (java.util.List) user.getGroups();
   	java.util.Iterator itr = groups.iterator();
   	while(itr.hasNext()){
    	elms.security.Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_CODE_INSPECTOR) codeInspector = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
  		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
		/* if (group.getGroupId() == Constants.GROUPS_PLAN_REVIEW_ENGINEER) {
			isPlanCheckReviewEngineer = true;
		}
		if (group.getGroupId() == Constants.GROUPS_PLAN_CHECK_MANAGER) {
			isPlanCheckManager = true;
	    } */
	
   	}

   	String projectName = LookupAgent.getProjectNameForActivityNumber(activityNumber);
   	String subProjectName = LookupAgent.getSubProjectNameForActivityId(activityId);
	if(subProjectName.startsWith(Constants.SUB_PROJECT_NAME_STARTS_WITH)) businessLicenseProject = true;
	if(subProjectName.startsWith(Constants.BT_SUB_PROJECT_NAME_STARTS_WITH)) businessTaxProject = true;
	
	
	java.util.List questionList = new java.util.ArrayList();
 	questionList = ActivityAgent.getQuestionAnswers(activityNumber); 
 	pageContext.setAttribute("questionList", questionList);
 	
	
%>
<html:html>

<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript">

function deleteActivity() {
   userInput = confirm("Are you sure you want to delete this Activity?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/deleteActivity.do?activityId=<%=activityId%>';
   }
}
</script>
</head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="<%=onloadAction%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="3"><font class="con_hdr_3b">&nbsp;Activity Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b">

                <bean:write name="lsoAddress"/></font>
                <br>
                <br>
                </td>
            </tr>
<%!
String outOfTownStreetNumber = "";
String outOfTownStreetName = "";
String outOfTownUnitNumber = "";
String outOfTownCity = "";
String outOfTownState = "";
String outOfTownZip = "";
String outOfTownZip4 = "";
String addressStreetNumber = "";
String addressStreetFraction = "";
String addressStreetName = "";
String addressUnitNumber = "";
String addressCity = "";
String addressState = "";
String addressZip = "";
String addressZip4 = "";
String businessAddressStreetNumber = "";
String businessAddressStreetFraction = "";
String businessAddressStreetName = "";
String businessAddressUnitNumber = "";
String businessAddressCity = "";
String businessAddressState = "";
String businessAddressZip = "";
String businessAddressZip4 = "";

%>
<%
if (businessLicenseProject && activityNumber.trim().startsWith("BL")) {

	BusinessLicenseActivity businessLicenseActivity= (BusinessLicenseActivity) request.getAttribute("businessLicenseActivity");

	String activityStatus = businessLicenseActivity.getActivityStatus().getDescription()!= null ? businessLicenseActivity.getActivityStatus().getDescription(): "";
	if (activityStatus == null || activityStatus == "") activityStatus = "";

	String applicationType = businessLicenseActivity.getApplicationType() != null ? businessLicenseActivity.getApplicationType().getDescription(): "";
	if (applicationType == null || applicationType == "") applicationType = "";

	activityType = businessLicenseActivity.getActivityType() != null ? businessLicenseActivity.getActivityType().getDescription(): "";
	if (activityType == null) activityType = "";

	boolean businessLocation = businessLicenseActivity.isBusinessLocation();
	if(businessLocation == true){

	addressStreetNumber = (String) businessLicenseActivity.getAddressStreetNumber();
	if (addressStreetNumber == null)addressStreetNumber = "";

	addressStreetFraction = businessLicenseActivity.getAddressStreetFraction();
	if (addressStreetFraction == null) addressStreetFraction = "";

	addressStreetName = businessLicenseActivity.getAddressStreetName();
	if (addressStreetName == null) addressStreetName = "";

	addressUnitNumber = businessLicenseActivity.getAddressUnitNumber();
	if (addressUnitNumber == null) addressUnitNumber = "";

	addressCity = businessLicenseActivity.getAddressCity();
	if (addressCity == null) addressCity = "";

	addressState = businessLicenseActivity.getAddressState();
	if (addressState == null) addressState = "";

	addressZip = businessLicenseActivity.getAddressZip();
	if (addressZip == null) addressZip = "";

	addressZip4 = businessLicenseActivity.getAddressZip4();
		if (addressZip4 == null) addressZip4 = "";
		}else{

		outOfTownStreetNumber = (String) businessLicenseActivity.getOutOfTownStreetNumber();
		if (outOfTownStreetNumber == null)outOfTownStreetNumber = "";

		outOfTownStreetName = businessLicenseActivity.getOutOfTownStreetName();
		if (outOfTownStreetName == null) outOfTownStreetName = "";

		outOfTownUnitNumber = businessLicenseActivity.getOutOfTownUnitNumber();
		if (outOfTownUnitNumber == null) outOfTownUnitNumber = "";

		outOfTownCity = businessLicenseActivity.getOutOfTownCity();
		if (outOfTownCity == null) outOfTownCity = "";

		outOfTownState = businessLicenseActivity.getOutOfTownState();
		if (outOfTownState == null) outOfTownState = "";

		outOfTownZip = businessLicenseActivity.getOutOfTownZip();
		if (outOfTownZip == null) outOfTownZip = "";

		outOfTownZip4 = businessLicenseActivity.getOutOfTownZip4();
		if (outOfTownZip4 == null) outOfTownZip4 = "";
	}

	String businessName = businessLicenseActivity.getBusinessName();
	if (businessName == null) businessName = "";

	String businessAccountNumber = businessLicenseActivity.getBusinessAccountNumber();
	if (businessAccountNumber == null) businessAccountNumber = "";

	String corporateName = businessLicenseActivity.getCorporateName();
	if (corporateName == null) corporateName = "";

	String renewalDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getRenewalDate());
	if (renewalDate == null) renewalDate = "";

	String businessPhone = businessLicenseActivity.getBusinessPhone();
	if (businessPhone == null) businessPhone = "";

	String businessExtension = businessLicenseActivity.getBusinessExtension();
	if (businessExtension == null) businessExtension = "";

	String businessFax = businessLicenseActivity.getBusinessFax();
	if (businessFax == null) businessFax = "";

	String sicCode = businessLicenseActivity.getSicCode();
	if (sicCode == null) sicCode = "";

	String muncipalCode = businessLicenseActivity.getMuncipalCode();
	if (muncipalCode == null) muncipalCode = "";

	String descOfBusiness = businessLicenseActivity.getDescOfBusiness();
	if (descOfBusiness == null) descOfBusiness = "";

	String classCode = businessLicenseActivity.getClassCode();
	if (classCode == null) classCode = "";

	boolean homeOccupation = businessLicenseActivity.isHomeOccupation();
	String homeOccupationYesNo = "";
	if (homeOccupation == true){homeOccupationYesNo = "Yes";}else{homeOccupationYesNo = "No";}

	boolean decalCode = businessLicenseActivity.isDecalCode();
	String decalCodeYesNo = "";
	if (decalCode == true){decalCodeYesNo = "Yes";}else{decalCodeYesNo = "No";}

	String creationDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getCreationDate());
	if (creationDate == null) creationDate = "";

	String applicationDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getApplicationDate());
	if (applicationDate == null) applicationDate = "";

	String startingDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getStartingDate());
	if (startingDate == null) startingDate = "";

	String outOfBusinessDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate());
	if (outOfBusinessDate == null) outOfBusinessDate = "";

	String ownershipType = businessLicenseActivity.getOwnershipType() != null ? businessLicenseActivity.getOwnershipType().getDescription(): "";
	if (ownershipType == null || ownershipType == "") ownershipType = "";

	String federalIdNumber = businessLicenseActivity.getFederalIdNumber();
	if (federalIdNumber == null) federalIdNumber = "";

	String emailAddress = businessLicenseActivity.getEmailAddress();
	if (emailAddress == null) emailAddress = "";

	String socialSecurityNumber = businessLicenseActivity.getSocialSecurityNumber();
	if (socialSecurityNumber == null) socialSecurityNumber = "";

	String mailStreetNumber = businessLicenseActivity.getMailStreetNumber();
	if (mailStreetNumber == null) mailStreetNumber = "";

	String mailStreetName = businessLicenseActivity.getMailStreetName();
	if (mailStreetName == null) mailStreetName = "";

	String mailUnitNumber = businessLicenseActivity.getMailUnitNumber();
	if (mailUnitNumber == null) mailUnitNumber = "";

	String mailCity = businessLicenseActivity.getMailCity();
	if (mailCity == null) mailCity = "";

	String mailState = businessLicenseActivity.getMailState();
	if (mailState == null) mailState = "";

	String mailZip = businessLicenseActivity.getMailZip();
	if (mailZip == null) mailZip = "";
	String mailZip4 = businessLicenseActivity.getMailZip4();
	if (mailZip4 == null) mailZip4 = "";

	String prevStreetNumber = businessLicenseActivity.getPrevStreetNumber();
	if (prevStreetNumber == null) prevStreetNumber = "";

	String prevStreetName = businessLicenseActivity.getPrevStreetName();
	if (prevStreetName == null) prevStreetName = "";

	String prevUnitNumber = businessLicenseActivity.getPrevUnitNumber();
	if (prevUnitNumber == null) prevUnitNumber = "";

	String prevCity = businessLicenseActivity.getPrevCity();
	if (prevCity == null) prevCity = "";

	String prevState = businessLicenseActivity.getPrevState();
	if (prevState == null) prevState = "";

	String prevZip = businessLicenseActivity.getPrevZip();
	if (prevZip == null) prevZip = "";

	String prevZip4 = businessLicenseActivity.getPrevZip4();
	if (prevZip4 == null) prevZip4 = "";

	String insuranceExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate());
	if (insuranceExpDate == null) insuranceExpDate = "";

	String bondExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getBondExpDate());
	if (bondExpDate == null) bondExpDate = "";

	String deptOfJusticeExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate());
	if (deptOfJusticeExpDate == null)  deptOfJusticeExpDate = "";

	String federalFirearmsLiscExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate());
	if (federalFirearmsLiscExpDate == null) federalFirearmsLiscExpDate = "";

	String squareFootage = businessLicenseActivity.getSquareFootage();
	if (squareFootage == null) squareFootage = "";

	String quantity =businessLicenseActivity.getQuantity() != null ? businessLicenseActivity.getQuantity().getDescription(): "";
	if (quantity == null || quantity == "") quantity = "";

	String quantityNum = businessLicenseActivity.getQuantityNum();
	if (quantityNum == null) quantityNum = "";

	String typeOfExemptions = businessLicenseActivity.getTypeOfExemptions() != null ? businessLicenseActivity.getTypeOfExemptions().getDescription(): "";
	if (typeOfExemptions == null || typeOfExemptions == "") typeOfExemptions = "";

	String renewalOnline = businessLicenseActivity.getRenewalOnline();
	if (renewalOnline == null) renewalOnline = "";
	if(!renewalOnline.equals("") && renewalOnline.equalsIgnoreCase("Y")){
		renewalOnline = "Yes";
	}else{
		renewalOnline = "No";
	}
	
	int icreatedBy = businessLicenseActivity.getCreatedBy();
	String name = "";
	try{
	name = LookupAgent.getUserDetails(StringUtils.i2s(icreatedBy)).getUsername();
	if (name == null) name = "";
	}catch(Exception e){}

			 %>
			<%--business license activity detail start--%>
			<tr>
				<td colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td width="100%" class="tabletitle">Business Activity Detail</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="tablelabel">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tabletitle"></td>
								<% if(businessLicenseUser){%>
								<td  class="tablebutton" align="right">
									<a href="<%=contextRoot%>/editBusinessLicenseActivity.do?activityId=<%=activityId%>&lsoId=<%=lsoId%>&businessLocation1=<%=businessLocation%>&addressStreetNumber1=<%=addressStreetNumber%>&addressStreetFraction1=<%=addressStreetFraction%>&addressStreetName1=<%=addressStreetName%>&addressUnitNumber1=<%=addressUnitNumber%>&addressCity1=<%=addressCity%>&addressState1=<%=addressState%>&addressZip1=<%=addressZip%>&addressZip41=<%=addressZip4%>" class="tablebutton">Edit</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
									<a href="javascript:deleteActivity();" class="hdrs">Delete</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                       		<% String blcert = LookupAgent.getBusinessLicenseCertificateURL(); %>
		                       		<%if(blcert.equalsIgnoreCase("XMLPDF")){ %>
			                       		<a target="_blank" href="<%=contextRoot%>/printBusinessLicense.do?activityNumber=<%=activityNumber%>"  class="tablebutton">Print Business License Certificate</a>
		                       		<%}else{ %>
		                       			<a target="_blank" href="<%=blcert %><%=activityNumber%>"  class="tablebutton">Print Business License Certificate</a>
		                       		<%} %>
		                    	</td>
							<%}%>
							</tr>
						</table>
						</td>
					</tr>
	                <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
                                <td width="20%" class="tablelabel">Activity Number</td>
                                <td width="20%" class="tabletext"><%=activityNumber%></td>
                                <td width="20%" class="tablelabel">Activity Status</td>
                                <td width="20%" class="tabletext"><%=activityStatus%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Application Type</td>
                                <td class="tabletext"><%=applicationType%></td>
                                <td class="tablelabel">Activity Type</td>
                                <td class="tabletext"><%=activityType%></td>
                            </tr>
							<%if(businessLocation == true){%>
                            <tr valign="top">
                                <td class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="3"><%=addressStreetNumber%> <%=addressStreetFraction%> <%=addressStreetName%> <%=addressUnitNumber%> <%=addressCity%> <%=addressState%> <%=addressZip%> <%=addressZip4%></td>
                            </tr>
							<%}else{%>
							<tr valign="top">
                                <td class="tablelabel">Out of Town Address</td>
                                <td class="tabletext" colspan="3"><%=outOfTownStreetNumber%> <%=outOfTownStreetName%> <%=outOfTownUnitNumber%> <%=outOfTownCity%> <%=outOfTownState%> <%=outOfTownZip%> <%=outOfTownZip4%><%}%></td>
                            </tr>

                            <tr valign="top">
                                <td class="tablelabel">Business Name</td>
                                <td class="tabletext"><%=businessName%></td>
                                <td class="tablelabel">Business Account Number</td>
                                <td class="tabletext"><%=businessAccountNumber%></td>
                            </tr>
                            <tr>
                            	<td class="tablelabel">Renewal Code</td>
								<td class="tabletext"><%=activityId%></td>
								<td class="tablelabel"> Online Renewed</td>
								<td class="tabletext"><%=renewalOnline%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Corporate Name</td>
                                <td class="tabletext" colspan="3"><%=corporateName%></td>
							</tr>
                            <tr valign="top">
                                <td class="tablelabel">Business Phone,Ext</td>
                                <td class="tabletext"><%=businessPhone%> <%=businessExtension%></td>
                                <td class="tablelabel">Business Fax</td>
                                <td class="tabletext"><%=businessFax%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Class Code</td>
                                <td class="tabletext"><%=classCode%></td>
                                <td class="tablelabel">Municipal Code</td>
                                <td class="tabletext"><%=muncipalCode%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">SIC</td>
                                <td class="tabletext"><%=sicCode%></td>
                                <td class="tablelabel">Decal Code</td>
                                <td class="tabletext"><%=decalCodeYesNo%></td>
                            </tr>

                             <tr valign="top">
                                <td class="tablelabel">Description of Business</td>
                                <td class="tabletext"  colspan="3"><%=descOfBusiness%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Home Occupation</td>
                                <td class="tabletext" colspan="3"><%=homeOccupationYesNo%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Creation Date</td>
                                <td class="tabletext"><%=creationDate%></td>
								<td class="tablelabel">Renewal Date</td>
                                <td class="tabletext"><%=renewalDate%></td>
                           	</tr>
							<tr valign="top">
                                <td class="tablelabel">Application Date</td>
                                <td class="tabletext"><%=applicationDate%></td>
                                <td class="tablelabel">Issue Date</td>
                                <td class="tabletext"><%=issueDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Starting Date</td>
                                <td class="tabletext"><%=startingDate%></td>
                                <td class="tablelabel">Out of Business Date</td>
                                <td class="tabletext"><%=outOfBusinessDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Ownership Type</td>
                                <td class="tabletext"><%=ownershipType%></td>
                                <td class="tablelabel">Federal ID Number</td>
                                <td class="tabletext"><%=federalIdNumber%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">E-mail Address</td>
                                <td class="tabletext"><%=emailAddress%></td>
							<% if((lcUser)||(pdUser)){%>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext"><%=socialSecurityNumber%></td>
                            <%}else if(socialSecurityNumber.equals("")){%>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext"> </td>
                            <%}else {%>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext">XXX-XX-XXXX</td>
                            <%}%>
							</tr>
							<%-- <tr valign="top">
                                <td class="tablelabel">Mailing Address</td>
                                <td class="tabletext" colspan="3"><%=mailStreetNumber%> <%=mailStreetName%> <%=mailUnitNumber%> <%=mailCity%> <%=mailState%> <%=mailZip%> <%=mailZip4%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Previous Address</td>
                                <td class="tabletext" colspan="3"><%=prevStreetNumber%> <%=prevStreetName%> <%=prevUnitNumber%> <%=prevCity%> <%=prevState%> <%=prevZip%> <%=prevZip4%></td>
							</tr> --%>
							<tr valign="top">
                                <td class="tablelabel">Insurance Expiration Date</td>
                                <td class="tabletext"><%=insuranceExpDate%></td>
                                <td class="tablelabel">Bond Expiration Date</td>
                                <td class="tabletext"><%=bondExpDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Department of Justice Expiration Date</td>
                                <td class="tabletext"><%=deptOfJusticeExpDate%></td>
                                <td class="tablelabel">Federal Firearms License Expiration Date</td>
                                <td class="tabletext"><%=federalFirearmsLiscExpDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Square Footage (Area Occupied)</td>
                                <td class="tabletext"><%=squareFootage%></td>
                                <td class="tablelabel">Quantity</td>
                                <td class="tabletext"><%=quantity%> <%=quantityNum%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Type Of Exemptions</td>
                                <td class="tabletext"><%=typeOfExemptions%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=name%></td>
                            </tr>
						</table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>

<%--             <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
               
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">	Business Address	</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
            
            	<%if(businessLocation == true){%>
                            <tr valign="top">
                                <td  width="25%"  class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="3"><%=addressStreetNumber%> <%=addressStreetFraction%> <%=addressStreetName%> <%=addressUnitNumber%> <%=addressCity%> <%=addressState%> <%=addressZip%> <%=addressZip4%></td>
                            </tr>
							<%}else{%>
							<tr valign="top">
                                <td width="25%"  class="tablelabel">Out of Town Address</td>
                                <td class="tabletext" colspan="3"><%=outOfTownStreetNumber%> <%=outOfTownStreetName%> <%=outOfTownUnitNumber%> <%=outOfTownCity%> <%=outOfTownState%> <%=outOfTownZip%> <%=outOfTownZip4%><%}%></td>
                            </tr>
                       </table></td></tr>  
                         </table></td></tr>  --%>    
                  <tr>
                <td colspan="3">&nbsp;</td>
          		  </tr>
               
            
			<%
			if(businessLicenseActivity.getMultiAddress() != null){
			for(int i=0; i< businessLicenseActivity.getMultiAddress().length; i++){ %>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">	<%=	businessLicenseActivity.getMultiAddress()[i].getAddressType()%>	</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
                                <td width="20%" class="tablelabel">Street Number , Street Name1</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivity.getMultiAddress()[i].getStreetNumber()%>&nbsp; <%=businessLicenseActivity.getMultiAddress()[i].getStreetName1()%> </td>
                                <td width="20%" class="tablelabel">Street Name2</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivity.getMultiAddress()[i].getStreetName2()%></td>
                            </tr>
                            <tr valign="top">
                                <td width="20%" class="tablelabel"> Unit,City,State,Zip,Zip4</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivity.getMultiAddress()[i].getUnit()%>&nbsp;<%=businessLicenseActivity.getMultiAddress()[i].getCity()%>&nbsp; <%=businessLicenseActivity.getMultiAddress()[i].getState()%>&nbsp;<%=businessLicenseActivity.getMultiAddress()[i].getZip()%>&nbsp;<%=businessLicenseActivity.getMultiAddress()[i].getZip4()%> </td>
                                <td width="20%" class="tablelabel">Attn</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivity.getMultiAddress()[i].getAttn()%></td>
                            </tr>
                   </table>
                        </td>
                    </tr>
                    
                    </table>
                        </td>
                    </tr>
                    
                     <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
					<%} }%>
			<%--busiiness license activity detail end--%>

<% }else if (businessTaxProject && activityNumber.trim().startsWith("BT")) {

	//business Tax specific fields -- start
	BusinessTaxActivity businessTaxActivity= (BusinessTaxActivity) request.getAttribute("businessTaxActivity");
	
	String activityStatus = businessTaxActivity.getActivityStatus().getDescription()!= null ? businessTaxActivity.getActivityStatus().getDescription(): "";
	if (activityStatus == null || activityStatus == "") activityStatus = "";

	String applicationType = businessTaxActivity.getApplicationType() != null ? businessTaxActivity.getApplicationType().getDescription(): "";
	if (applicationType == null || applicationType == "") applicationType = "";

	activityType = businessTaxActivity.getActivityType() != null ? businessTaxActivity.getActivityType().getDescription(): "";
	if (activityType == null) activityType = "";

	boolean businessLocation = businessTaxActivity.isBusinessLocation();
	if(businessLocation == true){

		businessAddressStreetNumber = (String) businessTaxActivity.getBusinessAddressStreetNumber();
		if (businessAddressStreetNumber == null) businessAddressStreetNumber = "";

		businessAddressStreetFraction = businessTaxActivity.getBusinessAddressStreetFraction();
		if (businessAddressStreetFraction == null) businessAddressStreetFraction = "";

		businessAddressStreetName = businessTaxActivity.getBusinessAddressStreetName();
		if (businessAddressStreetName == null) businessAddressStreetName = "";

		businessAddressUnitNumber = businessTaxActivity.getBusinessAddressUnitNumber();
		if (businessAddressUnitNumber == null) businessAddressUnitNumber = "";

		businessAddressCity = businessTaxActivity.getBusinessAddressCity();
		if (businessAddressCity == null) businessAddressCity = "";

		businessAddressState = businessTaxActivity.getBusinessAddressState();
		if (businessAddressState == null) businessAddressState = "";

		businessAddressZip = businessTaxActivity.getBusinessAddressZip();
		if (businessAddressZip == null) businessAddressZip = "";

		businessAddressZip4 = businessTaxActivity.getBusinessAddressZip4();
			if (businessAddressZip4 == null) businessAddressZip4 = "";
	}else{

		outOfTownStreetNumber = (String) businessTaxActivity.getOutOfTownStreetNumber();
		if (outOfTownStreetNumber == null)outOfTownStreetNumber = "";

		outOfTownStreetName = businessTaxActivity.getOutOfTownStreetName();
		if (outOfTownStreetName == null) outOfTownStreetName = "";

		outOfTownUnitNumber = businessTaxActivity.getOutOfTownUnitNumber();
		if (outOfTownUnitNumber == null) outOfTownUnitNumber = "";

		outOfTownCity = businessTaxActivity.getOutOfTownCity();
		if (outOfTownCity == null) outOfTownCity = "";

		outOfTownState = businessTaxActivity.getOutOfTownState();
		if (outOfTownState == null) outOfTownState = "";

		outOfTownZip = businessTaxActivity.getOutOfTownZip();
		if (outOfTownZip == null) outOfTownZip = "";

		outOfTownZip4 = businessTaxActivity.getOutOfTownZip4();
		if (outOfTownZip4 == null) outOfTownZip4 = "";
	}

	String businessName = businessTaxActivity.getBusinessName();
	if (businessName == null) businessName = "";

	String businessAccountNumber = businessTaxActivity.getBusinessAccountNumber();
	if (businessAccountNumber == null) businessAccountNumber = "";

	String corporateName = businessTaxActivity.getCorporateName();
	if (corporateName == null) corporateName = "";

	String attn = businessTaxActivity.getAttn();
	if (attn == null) attn = "";

	String businessPhone = businessTaxActivity.getBusinessPhone();
	if (businessPhone == null) businessPhone = "";

	String businessExtension = businessTaxActivity.getBusinessExtension();
	if (businessExtension == null) businessExtension = "";

	String businessFax = businessTaxActivity.getBusinessFax();
	if (businessFax == null) businessFax = "";

	String sicCode = businessTaxActivity.getSicCode();
	if (sicCode == null) sicCode = "";

	String muncipalCode = businessTaxActivity.getMuncipalCode();
	if (muncipalCode == null) muncipalCode = "";

	String descOfBusiness = businessTaxActivity.getDescOfBusiness();
	if (descOfBusiness == null) descOfBusiness = "";

	String classCode = businessTaxActivity.getClassCode();
	if (classCode == null) classCode = "";

	boolean homeOccupation = businessTaxActivity.isHomeOccupation();
	String homeOccupationYesNo = "";
	if (homeOccupation == true){homeOccupationYesNo = "Yes";}else{homeOccupationYesNo = "No";}

	boolean decalCode = businessTaxActivity.isDecalCode();
	String decalCodeYesNo = "";
	if (decalCode == true){decalCodeYesNo = "Yes";}else{decalCodeYesNo = "No";}

	boolean otherBusinessOccupancy = businessTaxActivity.isOtherBusinessOccupancy();
	String otherBusinessOccupancyYesNo = "";
	if (otherBusinessOccupancy == true){otherBusinessOccupancyYesNo = "Yes";}else{otherBusinessOccupancyYesNo = "No";}

	String creationDate = elms.util.StringUtils.cal2str(businessTaxActivity.getCreationDate());
	if (creationDate == null) creationDate = "";

	String applicationDate = elms.util.StringUtils.cal2str(businessTaxActivity.getApplicationDate());
	if (applicationDate == null) applicationDate = "";

	String startingDate = elms.util.StringUtils.cal2str(businessTaxActivity.getStartingDate());
	if (startingDate == null) startingDate = "";

	String outOfBusinessDate = elms.util.StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate());
	if (outOfBusinessDate == null) outOfBusinessDate = "";

	String ownershipType = businessTaxActivity.getOwnershipType() != null ? businessTaxActivity.getOwnershipType().getDescription(): "";
	if (ownershipType == null || ownershipType == "") ownershipType = "";

	String federalIdNumber = businessTaxActivity.getFederalIdNumber();
	if (federalIdNumber == null) federalIdNumber = "";

	String emailAddress = businessTaxActivity.getEmailAddress();
	if (emailAddress == null) emailAddress = "";

	String socialSecurityNumber = businessTaxActivity.getSocialSecurityNumber();
	if (socialSecurityNumber == null) socialSecurityNumber = "";

	String mailStreetNumber = businessTaxActivity.getMailStreetNumber();
	if (mailStreetNumber == null) mailStreetNumber = "";

	String mailStreetName = businessTaxActivity.getMailStreetName();
	if (mailStreetName == null) mailStreetName = "";

	String mailUnitNumber = businessTaxActivity.getMailUnitNumber();
	if (mailUnitNumber == null) mailUnitNumber = "";

	String mailCity = businessTaxActivity.getMailCity();
	if (mailCity == null) mailCity = "";

	String mailState = businessTaxActivity.getMailState();
	if (mailState == null) mailState = "";

	String mailZip = businessTaxActivity.getMailZip();
	if (mailZip == null) mailZip = "";

	String mailZip4 = businessTaxActivity.getMailZip4();
	if (mailZip4 == null) mailZip4 = "";

	String prevStreetNumber = businessTaxActivity.getPrevStreetNumber();
	if (prevStreetNumber == null) prevStreetNumber = "";

	String prevStreetName = businessTaxActivity.getPrevStreetName();
	if (prevStreetName == null) prevStreetName = "";

	String prevUnitNumber = businessTaxActivity.getPrevUnitNumber();
	if (prevUnitNumber == null) prevUnitNumber = "";

	String prevCity = businessTaxActivity.getPrevCity();
	if (prevCity == null) prevCity = "";

	String prevState = businessTaxActivity.getPrevState();
	if (prevState == null) prevState = "";

	String prevZip = businessTaxActivity.getPrevZip();
	if (prevZip == null) prevZip = "";

	String prevZip4 = businessTaxActivity.getPrevZip4();
	if (prevZip4 == null) prevZip4 = "";

	String name1 = businessTaxActivity.getName1();
	if (name1 == null) name1 = "";

	String title1 = businessTaxActivity.getTitle1();
	if (title1 == null) title1 = "";

	String owner1StreetNumber = businessTaxActivity.getOwner1StreetNumber();
	if (owner1StreetNumber == null) owner1StreetNumber = "";

	String owner1StreetName = businessTaxActivity.getOwner1StreetName();
	if (owner1StreetName == null)owner1StreetName = "";

	String owner1UnitNumber = businessTaxActivity.getOwner1UnitNumber();
	if (owner1UnitNumber == null) owner1UnitNumber = "";

	String owner1City = businessTaxActivity.getOwner1City();
	if (owner1City == null) owner1City = "";

	String owner1State = businessTaxActivity.getOwner1State();
	if (owner1State == null) owner1State = "";

	String owner1Zip = businessTaxActivity.getOwner1Zip();
	if (owner1Zip == null) owner1Zip = "";

	String owner1Zip4 = businessTaxActivity.getOwner1Zip4();
	if (owner1Zip4 == null)owner1Zip4 = "";

	String name2 = businessTaxActivity.getName2();
	if (name2 == null) name2 = "";

	String title2 = businessTaxActivity.getTitle2();
	if (title2 == null) title2 = "";

	String owner2StreetNumber = businessTaxActivity.getOwner2StreetNumber();
	if (owner2StreetNumber == null) owner2StreetNumber = "";

	String owner2StreetName = businessTaxActivity.getOwner2StreetName();
	if (owner2StreetName == null)owner2StreetName = "";

	String owner2UnitNumber = businessTaxActivity.getOwner2UnitNumber();
	if (owner2UnitNumber == null) owner2UnitNumber = "";

	String owner2City = businessTaxActivity.getOwner2City();
	if (owner2City == null) owner2City = "";

	String owner2State = businessTaxActivity.getOwner2State();
	if (owner2State == null) owner2State = "";

	String owner2Zip = businessTaxActivity.getOwner2Zip();
	if (owner2Zip == null) owner2Zip = "";

	String owner2Zip4 = businessTaxActivity.getOwner2Zip4();
	if (owner2Zip4 == null)owner2Zip4 = "";

	String squareFootage = businessTaxActivity.getSquareFootage();
	if (squareFootage == null) squareFootage = "";

	String quantity =businessTaxActivity.getQuantity() != null ? businessTaxActivity.getQuantity().getDescription(): "";
	if (quantity == null || quantity == "") quantity = "";

	String quantityNum = businessTaxActivity.getQuantityNum();
	if (quantityNum == null) quantityNum = "";

	String driverLicense = businessTaxActivity.getDriverLicense();
	if (driverLicense == null) driverLicense = "";
	
	String mailAttn = businessTaxActivity.getMailAttn();
	if (mailAttn == null) mailAttn = "";

	String preAttn = businessTaxActivity.getPreAttn();
	if (preAttn == null) preAttn = "";
	
	String owner1Attn = businessTaxActivity.getOwner1Attn();
	if (owner1Attn == null) owner1Attn = "";
	
	String owner2Attn = businessTaxActivity.getOwner2Attn();
	if (owner2Attn == null) owner2Attn = "";
	
	String renewalOnline = businessTaxActivity.getRenewalOnline();
	if (renewalOnline == null) renewalOnline = "";
	if(!renewalOnline.equals("") && renewalOnline.equalsIgnoreCase("Y")){
		renewalOnline = "Yes";
	}else{
		renewalOnline = "No";
	}
	
	int icreatedBy = businessTaxActivity.getCreatedBy();
	String name = "";
	try{
	name = LookupAgent.getUserDetails(StringUtils.i2s(icreatedBy)).getUsername();
	if (name == null) name = "";
	}catch(Exception e){}

			 %>
			<%-- business Tax activity detail start--%>
			<tr>
				<td colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td width="100%" class="tabletitle">Business Activity Detail</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="tablelabel">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tabletitle"></td>
								<% if(businessTaxUser){%>
        							<td  class="tablebutton" align="right">
        							 <!-- Edit Business Tax Activity -->
                            <a href="<%=contextRoot%>/editBusinessTaxActivity.do?activityId=<%=activityId%>&lsoId=<%=lsoId%>&businessLocation1=<%=businessLocation%>&businessAddressStreetNumber1=<%=businessAddressStreetNumber%>&businessAddressStreetFraction1=<%=businessAddressStreetFraction%>&businessAddressStreetName1=<%=businessAddressStreetName%>&businessAddressUnitNumber1=<%=businessAddressUnitNumber%>&businessAddressCity1=<%=businessAddressCity%>&businessAddressState1=<%=businessAddressState%>&businessAddressZip1=<%=businessAddressZip%>&businessAddressZip41=<%=businessAddressZip4%>" class="tablebutton">Edit</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
                            <!-- delete Business License Activity -->
         					<a href="javascript:deleteActivity();" class="tablebutton">Delete</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
         					<!--Print Business Tax Certificate -->
                     		<% String btcert = LookupAgent.getBusinessTaxCertificateURL(); %>
                     		<%if(btcert.equalsIgnoreCase("XMLPDF")){ %>
                      		<a target="_blank" href="<%=contextRoot%>/printBusinessLicense.do?activityNumber=<%=activityNumber%>"  class="tablebutton">Print Business Tax Certificate</a>
                     		<%}else{ %>
                     			<a target="_blank" href="<%=btcert %><%=activityNumber%>"  class="tablebutton">Print Business License Certificate</a>
                     		<%} %>

                             </td>
      							 <%}%>
							</tr>
						</table>
						</td>
					</tr>
	                <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
                                <td width="20%" class="tablelabel">Activity Number</td>
                                <td width="20%" class="tabletext"><%=activityNumber%></td>
                                <td width="20%" class="tablelabel">Activity Status</td>
                                <td width="20%" class="tabletext"><%=activityStatus%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Application Type</td>
                                <td class="tabletext"><%=applicationType%></td>
                                <td class="tablelabel">Activity Type</td>
                                <td class="tabletext"><%=activityType%></td>
                            </tr>
                            <tr>
                            	<td class="tablelabel">Renewal Code</td>
								<td class="tabletext"><%=activityId%></td>								
                                <td class="tablelabel">Business Account Number</td>
                                <td class="tabletext"><%=businessAccountNumber%></td>
                            </tr>
							<%if(businessLocation == true){%>
                            <tr valign="top">
                                <td class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="3"><%=businessAddressStreetNumber%> <%=businessAddressStreetFraction%> <%=businessAddressStreetName%> <%=businessAddressUnitNumber%> <%=businessAddressCity%> <%=businessAddressState%> <%=businessAddressZip%> <%=businessAddressZip4%></td>
                            </tr>
							<%}else{%>
							<tr valign="top">
                                <td class="tablelabel">Out of Town Address</td>
                                <td class="tabletext" colspan="3"><%=outOfTownStreetNumber%> <%=outOfTownStreetName%> <%=outOfTownUnitNumber%> <%=outOfTownCity%> <%=outOfTownState%> <%=outOfTownZip%> <%=outOfTownZip4%><%}%></td>
                            </tr>

                            <tr valign="top">
                                <td class="tablelabel">Business Name</td>
                                <td class="tabletext"><%=businessName%></td>                                
								<td class="tablelabel"> Online Renewed</td>
								<td class="tabletext"><%=renewalOnline%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Corporate Name</td>
                                <td class="tabletext"><%=corporateName%></td>
								<td class="tablelabel">Attn.</td>
                                <td class="tabletext"><%=attn%></td>
							</tr>

                            <tr valign="top">
                                <td class="tablelabel">Business Phone,Ext</td>
                                <td class="tabletext"><%=businessPhone%> <%=businessExtension%></td>
                                <td class="tablelabel">Business Fax</td>
                                <td class="tabletext"><%=businessFax%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">NAICS Code</td>
                                <td class="tabletext"><%=sicCode%></td>
								<td class="tablelabel">Municipal Code</td>
                                <td class="tabletext"><%=muncipalCode%></td>

                            </tr>
                            <tr valign="top">
								<td class="tablelabel">Description of Business</td>
                                <td class="tabletext"><%=descOfBusiness%></td>
								<td class="tablelabel">Class Code</td>
                                <td class="tabletext"><%=classCode%></td>
                            </tr>
							<tr valign="top">
								<td class="tablelabel">Decal Code</td>
                                <td class="tabletext"><%=decalCodeYesNo%></td>
                                <td class="tablelabel">Home Occupation</td>
                                <td class="tabletext" colspan="1"><%=homeOccupationYesNo%></td>
							</tr>
							<tr valign="top">
								<td class="tablelabel">Are there any other businesses occupying this premise?</td>
                                <td class="tabletext"><%=otherBusinessOccupancyYesNo%></td>
                                <td class="tablelabel"> Online Renewed</td>
                                <td class="tabletext"><%=renewalOnline%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Application Date</td>
                                <td class="tabletext"><%=applicationDate%></td>
                                <td class="tablelabel">Issue Date</td>
                                <td class="tabletext"><%=issueDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Starting Date</td>
                                <td class="tabletext"><%=startingDate%></td>
                                <td class="tablelabel">Out of Business Date</td>
                                <td class="tabletext"><%=outOfBusinessDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Ownership Type</td>
                                <td class="tabletext"><%=ownershipType%></td>
							<%
								if((lcUser)  || (pdUser) ){%>
                                <td class="tablelabel">Federal ID Number</td>
                                <td class="tabletext"><%=federalIdNumber%></td>
                           <%}else if(federalIdNumber.equals("") ){%>
								<td class="tablelabel">Federal ID Number</td>
                                <td class="tabletext"> </td>
                            <%}else {%>
								<td class="tablelabel">Federal ID Number</td>
                                <td class="tabletext">XXXXXXXXX</td>
                            <%}%>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">E-mail Address</td>
                                <td class="tabletext"><%=emailAddress%></td>
							<%if((lcUser)  || (pdUser)){%>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext"><%=socialSecurityNumber%></td>
                            <%}else if(socialSecurityNumber.equals("")){%>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext"> </td>
                            <%}else {%>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext">XXX-XX-XXXX</td>
                            <%}%>
							</tr>
							<%-- <tr valign="top">
                                <td class="tablelabel">Mailing Address</td>
                                <td class="tabletext" ><%=mailStreetNumber%> <%=mailStreetName%> <%=mailUnitNumber%> <%=mailCity%> <%=mailState%> <%=mailZip%> <%=mailZip4%></td>
                                <td class="tablelabel">Mail Attn </td>
                                <td class="tabletext"><%=mailAttn %> </td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Previous Address</td>
                                <td class="tabletext"><%=prevStreetNumber%> <%=prevStreetName%> <%=prevUnitNumber%> <%=prevCity%> <%=prevState%> <%=prevZip%> <%=prevZip4%></td>
							    <td class="tablelabel">Pre Attn</td>
                                <td class="tabletext"> <%=preAttn %></td> 
							</tr> --%>
						<%--	<tr valign="top">
                                <td class="tablelabel">Owner1 Name</td>
                                <td class="tabletext"><%=name1%></td>
                                <td class="tablelabel">Title</td>
                                <td class="tabletext"><%=title1%></td>
                            </tr>
							 <%if((lcUser)  || (pdUser)){%>
							<tr valign="top">
                                <td class="tablelabel">Owner1 Address</td>
                                <td class="tabletext" colspan="3"><%=owner1StreetNumber%> <%=owner1StreetName%> <%=owner1UnitNumber%> <%=owner1City%> <%=owner1State%> <%=owner1Zip%> <%=owner1Zip4%></td>
                            </tr>
							<%}else if(owner1StreetNumber.equals("") &&owner1StreetName.equals("") && owner1UnitNumber.equals("") && owner1City.equals("") && owner1State.equals("") && owner1Zip.equals("") && owner1Zip4.equals("")){%>
								<tr valign="top">
                                <td class="tablelabel">Owner1 Address</td>
                                <td class="tabletext" ></td>
                                <td class="tablelabel">Owner1 Attn</td>
                                <td class="tabletext"><%=owner1Attn %> </td>
                            </tr>
							<%}else {%>
								<tr valign="top">
                                <td class="tablelabel">Owner1 Address</td>
                              <!--   <td class="tabletext" colspan="3">XXXXXXX</td> -->
                                <td class="tabletext"><%=owner1StreetNumber%> <%=owner1StreetName%> <%=owner1UnitNumber%> <%=owner1City%> <%=owner1State%> <%=owner1Zip%> <%=owner1Zip4%></td>
                                <td class="tablelabel">Owner1 Attn</td>
                                <td class="tabletext"><%=owner1Attn %> </td>
                            </tr>

							<%}%> --%>
							<%--<tr valign="top">
                                <td class="tablelabel">Owner2 Name</td>
                                <td class="tabletext"><%=name2%></td>
                                <td class="tablelabel">Title</td>
                                <td class="tabletext"><%=title2%></td>
                            </tr>
							 <%if((lcUser)  || (pdUser)){%>
							<tr valign="top">
                                <td class="tablelabel">Owner2 Address</td>
                                <td class="tabletext" colspan="3"><%=owner2StreetNumber%> <%=owner2StreetName%> <%=owner2UnitNumber%> <%=owner2City%> <%=owner2State%> <%=owner2Zip%> <%=owner2Zip4%></td>
                            </tr>
							<%}else if(owner2StreetNumber.equals("") &&owner2StreetName.equals("") && owner2UnitNumber.equals("") && owner2City.equals("") && owner2State.equals("") && owner2Zip.equals("") && owner2Zip4.equals("")){%>
								<tr valign="top">
                                <td class="tablelabel">Owner2 Address</td>
                                <td class="tabletext" ></td>
                                <td class="tablelabel">Owner2 Attn</td>
                                 <td class="tabletext"> <%=owner2Attn %></td>
                            </tr>
							<%}else{%>
								<tr valign="top">
                                <td class="tablelabel">Owner2 Address</td>
                              <!--   <td class="tabletext" colspan="3">XXXXXXX</td> -->
                                 <td class="tabletext" ><%=owner2StreetNumber%> <%=owner2StreetName%> <%=owner2UnitNumber%> <%=owner2City%> <%=owner2State%> <%=owner2Zip%> <%=owner2Zip4%></td>
                                 <td class="tablelabel">Owner2 Attn</td>
                                 <td class="tabletext"> <%=owner2Attn %></td>
                            </tr> 

							<%}%>--%>
							<tr valign="top">
                                <td class="tablelabel">Square Footage (Area Occupied)</td>
                                <td class="tabletext"><%=squareFootage%></td>
                                <td class="tablelabel">Quantity</td>
                                <td class="tabletext"><%=quantity%> <%=quantityNum%></td>
                            </tr>

							<tr valign="top">
                                <td class="tablelabel">Creation Date</td>
                                <td class="tabletext"><%=creationDate%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=name%></td>
                            </tr>
							<%if((lcUser)  || (pdUser)){%>
							<tr valign="top">
                                <td class="tablelabel">Driver's License</td>
                                <td class="tabletext" colspan="3"><%=driverLicense%></td>
                            </tr>
							<%}else if(driverLicense.equals("")){%>
								<tr valign="top">
                                <td class="tablelabel">Driver's License</td>
                                <td class="tabletext" colspan="3"></td>
                            </tr>
							<%}else{%>
								<tr valign="top">
                                <td class="tablelabel">Driver's License</td>
                               <!--  <td class="tabletext" colspan="3">XXXXXXX</td> -->
                                 <td class="tabletext" colspan="3"><%=driverLicense%></td>
                            </tr>

							<%}%>

						</table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>

<%--             <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">	Business Address	</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

					<%if(businessLocation == true){%>
                            <tr valign="top">
                                <td width="25%"  class="tablelabel">Business Address</td>
                                <td width="75%"  class="tabletext"><%=businessAddressStreetNumber%> <%=businessAddressStreetFraction%> <%=businessAddressStreetName%> <%=businessAddressUnitNumber%> <%=businessAddressCity%> <%=businessAddressState%> <%=businessAddressZip%> <%=businessAddressZip4%></td>
                            </tr>
							<%}else{%>
							<tr valign="top">
                                <td class="tablelabel">Out of Town Address</td>
                                <td class="tabletext" colspan="3"><%=outOfTownStreetNumber%> <%=outOfTownStreetName%> <%=outOfTownUnitNumber%> <%=outOfTownCity%> <%=outOfTownState%> <%=outOfTownZip%> <%=outOfTownZip4%><%}%></td>
                            </tr>
					
					</table></td></tr></table></td></tr> --%>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			<%
			if(businessTaxActivity.getMultiAddress() != null){
			for(int i=0; i< businessTaxActivity.getMultiAddress().length; i++){ %>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">	<%=	businessTaxActivity.getMultiAddress()[i].getAddressType()%>	</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                        
                       		 <tr valign="top">
                                <td width="20%" class="tablelabel">Name</td>
                                <td width="20%" class="tabletext"><%=businessTaxActivity.getMultiAddress()[i].getName()%> </td>
                                <td width="20%" class="tablelabel">Title</td>
                                <td width="20%" class="tabletext"><%=businessTaxActivity.getMultiAddress()[i].getTitle()%></td>
                            </tr>
                        
							<tr valign="top">
                                <td width="20%" class="tablelabel">Street Number , Street Name1</td>
                                <td width="20%" class="tabletext"><%=businessTaxActivity.getMultiAddress()[i].getStreetNumber()%>&nbsp; <%=businessTaxActivity.getMultiAddress()[i].getStreetName1()%> </td>
                                <td width="20%" class="tablelabel">Street Name2</td>
                                <td width="20%" class="tabletext"><%=businessTaxActivity.getMultiAddress()[i].getStreetName2()%></td>
                            </tr>
                            <tr valign="top">
                                <td width="20%" class="tablelabel"> Unit,City,State,Zip,Zip4</td>
                                <td width="20%" class="tabletext"><%=businessTaxActivity.getMultiAddress()[i].getUnit()%>&nbsp;<%=businessTaxActivity.getMultiAddress()[i].getCity()%>&nbsp; <%=businessTaxActivity.getMultiAddress()[i].getState()%>&nbsp;<%=businessTaxActivity.getMultiAddress()[i].getZip()%>&nbsp;<%=businessTaxActivity.getMultiAddress()[i].getZip4()%> </td>
                                <td width="20%" class="tablelabel">Attn</td>
                                <td width="20%" class="tabletext"><%=businessTaxActivity.getMultiAddress()[i].getAttn()%></td>
                            </tr>
                   </table>
                        </td>
                    </tr>
                    
                    </table>
                        </td>
                    </tr>
                    
                     <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
					<%} }%>

			<%--business Tax activity detail end--%>



			<%}else{ %>
			<%--generic activity detail start--%>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="2">
                            <tr>
                               <td width="66%" class="tabletitle">Activity Detail</td>
                              <tr align=right> <td width="1%" class="tablebutton">
									<%
										if(
											(
												(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING))
													&&
												(user.getDepartment().getDepartmentCode().equalsIgnoreCase("BS"))
											)
										){
									%>
                                    <!-- Copy Activity -->
                                    <a href="<%=contextRoot%>/viewCopyActivity.do?activityId=<%=activityId%>" class="tablebutton">Copy</a>
	                                <%
	                                	}
	                                %>

	                                 <!-- Edit Activity -->

<%-- 	                                 <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept"> --%>
<%-- 	                                 	<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser"> --%>
	                                 		 |
	                                 		<a href="<%=contextRoot%>/editActivityDetails.do?activityId=<%=activityId%>&type=<%=activityType%>" class="tablebutton">Edit</a>
<%-- 	                                 	</security:editable> --%>
<%-- 	                                </security:editable> --%>

									<!-- delete Activity -->
									<security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
	                          			<logic:equal name="activity" property="deletable" value="true" scope="request">
	                          				<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
		                                 		 |
	                                 			<a href="javascript:deleteActivity();" class="tablebutton">Delete</a>
	                                   		</security:editable>
                                    	</logic:equal>
                                    </security:editable>

	                               <!--Print permit -->
	                               <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">

											<%

											if(

													(projectName.equals(elms.common.Constants.PROJECT_NAME_PARKING))
														&&
													(user.getDepartment().getDepartmentCode().equalsIgnoreCase("PK"))

												){

												 if(activityType.equalsIgnoreCase("PKNCOM")){
											%>
											| <a target="_blank" href="<%=LookupAgent.getLncvPermitURL() %><%=activityNumber%>" class="tablebutton">Print Permit</a>
											 <% } else { %>
											| <a target="_blank" href="<%=LookupAgent.getParkingPermitURL() %><%=activityNumber%>" class="tablebutton">Print Permit</a>
											
											<% } } else if(

												(projectName.equals(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS))
													&&
												(user.getDepartment().getDepartmentCode().equalsIgnoreCase("PW"))

												){ 

											%>
											| <a target="_blank" href="<%=LookupAgent.getPublicWorksPermitURL() %><%=activityNumber%>" class="tablebutton">Print Permit</a>
											
											
											<!-- pl -->
											
											<% } else if(

												(projectName.equals(elms.common.Constants.PROJECT_NAME_PLANNING))
													&&
												(user.getDepartment().getDepartmentCode().equalsIgnoreCase("PL"))

												){ 

											%>
											| <a target="_blank" href="<%=LookupAgent.getPlaningPermitReportURL() %><%=activityNumber%>" class="tablebutton">Print Permit</a>
											
											<!-- LC -->
											<% } else if(

												(projectName.equals(elms.common.Constants.PROJECT_NAME_LICENSE_AND_CODE_SERVICES))
													

												){ 

											%>
											| <a target="_blank" href="<%=LookupAgent.getCodeEnforcementPermitReportURL() %><%=activityNumber%>" class="tablebutton">Print Permit</a>
											
											
											
											
											
											
											
											<% } else { 
											
												String URL = LookupAgent.getBuildingPermitReportURL();
												
												if(URL == null || URL.equalsIgnoreCase("") || URL.equalsIgnoreCase("XMLPDF")){
													URL = contextRoot + "/printPermit.do?actNbr="+ activityNumber + "&actType="+activityType;
												}
												
												
											%>
											
											| <a target="_blank" href="<%=URL%>" class="tablebutton">Print Permit</a>

											 <% } %>


											
	                                </security:editable>

									<%
										if(
											(
												(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING))
													&&
												(user.getDepartment().getDepartmentCode().equalsIgnoreCase("BS"))
											)
										){
									%>
	                                 	<!-- Print COO -->
	                                 	| <a href="<%=contextRoot%>/editCertificate.do?actId=<%=activityId%>&actNBR=<%=activityNumber%>"  class="tablebutton">Certificate of Occupancy</a>
									
									<%
										if(greenHalo.equalsIgnoreCase("Yes") | greenHalo.equalsIgnoreCase("Y")){
									%>
									 <!-- Green Halo -->
										| <a href="<%=contextRoot%>/editGreenHalo.do?activityId=<%=activityId%>" class="tablebutton">Green Halo</a>&nbsp;
		                                <%
										}
									%>
		                                <!-- print FIR -->
		                                | <a target="_blank" href="<%=contextRoot%>/printFIR.do?actNbr=<%=activityNumber%>"  class="tablebutton">Print FIR</a>&nbsp;
									<%
										}
									%>
								</td>
                           </tr>

                        </table>
                        </td>
                    </tr>


                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                             	<td class="tablelabel">Activity #</td>
                                <td class="tabletext"><%=activityNumber%></td>
                                <td class="tablelabel">Activity Status</td>
                                <td class="tabletext"><bean:write name="status"/> <%--=status--%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Activity Type</td>
                                <td class="tabletext"><%=activityTypeDesc%></td>
                                <td class="tablelabel">Sub Type</td>
                                <td class="tabletext">

                                <%
                                	Iterator it =activitySubTypes.iterator();
	                                while(it.hasNext()){
    		                            out.println(""+it.next()+"<br/>"+"<dt>");
            	                    }
                                 %>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Description</td>
                                <td class="tabletext" colspan="3"><%=description%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Applied Date</td>
                                <td class="tabletext"><%=startDate%></td>
                                <td class="tablelabel">Issue Date</td>
                                <td class="tabletext"><%=issueDate%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel"><%=completionDateLabel%></td>
                                <td class="tabletext"><%=completionDate%></td>
                                <td class="tablelabel"><%=expirationDateLabel%></td>
                                <td class="tabletext"><%=expirationDate%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Address</td>
                                <td class="tabletext"><%=address%></td>
                                <td class="tablelabel">Valuation</td>
                                <td class="tabletext"><%=valuation%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Microfilm</td>
                                <td class="tabletext"><%=microfilm%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=createdBy%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Label</td>
                                <td class="tabletext"><%=label%></td>
                                <td class="tablelabel">Parking Zone</td>
                                <td class="tabletext"><%=parkingZoneDescription%></td>
                            </tr>
                             <tr valign="top">
                                <td class="tablelabel">Green Halo</td>
                                <td class="tabletext" colspan="3"><%=greenHalo%></td>
                            </tr>

                             <security:readable redLevelId="<%=activityId%>" redLevelType="A" readProperty="field">
                             <tr valign="top">
                                <td class="tablelabel">New Units</td>
                                <td class="tabletext"><%=newUnits%></td>
                                <td class="tablelabel">Existing Units</td>
                                <td class="tabletext"><%=existingUnits%></td>
                            </tr>

                             <tr valign="top">
                                <td class="tablelabel">Demolished Units</td>
                                <td class="tabletext" colspan="3"><%=demolishedUnits%></td>
                            </tr>
                            </security:readable>



                          <%  int i=1 ;   %>
			              <logic:iterate id="cfL" name="customFieldsList" type="elms.app.admin.CustomField" scope="request" >
			                               <%if(i % 2==1) { %>     <tr valign="top">
			                                                       <td class="tablelabel"><bean:write name="cfL" property="fieldLabel"/></td>
			                                                       <td class="tabletext"><bean:write name="cfL" property="fieldValue"/></td>


			                                   <% } else  {  %>    <td class="tablelabel"><bean:write name="cfL" property="fieldLabel"/></td>
			                                                       <td class="tabletext"><bean:write name="cfL" property="fieldValue"/></td> <% }%>


			                               <%if(i % 2==0) { %>      </tr>    <% } i++ ;%>



			               </logic:iterate>

                          <%   if(customFieldsList.size() % 2==1) { %>   <td class="tabletext" colspan="4"></td> </tr>  <% } %>





                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			<%--generic activity detail end--%>
			<%} %>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listPeople.do?id=<%=activityId%>&type=A&ed=<%=editable%>&ed=<%=editable%>" class="tabletitle">

									<%
						            if ((businessLicenseProject && activityNumber.trim().startsWith("BL")) || (businessTaxProject && activityNumber.trim().startsWith("BT"))) {
									 %>
									Business Contact Manager
									<%
									} else {
									 %>
									People Manager
									<%
									}
									 %>
									</a>
								</td>
                                  <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
	                                  <logic:equal name="editable" value="true">
	                                   <td width="1%" class="tablebutton"><nobr>
	                                   		<a href="<%=contextRoot%>/searchPeople.do?level=<%=level%>" class="tablebutton">Add</a>&nbsp;
	                                   </td>
	                                  </logic:equal>
	                              </security:editable>
                           </tr>
                        </table>
                        </td>
                    </tr>

               	    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">People Type</td>
                                <td class="tablelabel">Title</td>
                                <td class="tablelabel">Name</td>
                                <td class="tablelabel">Phone</td>
                                <td class="tablelabel">Hold</td>
                            </tr>

                            <logic:iterate id="people" name="peopleList" type="elms.app.people.People" scope="request" >
                            <tr valign="top"
	                          <logic:equal name="people" property="anyDateExpired" value="Y">
	    	                    bgcolor="#F48080"
		                      </logic:equal>
	                          <logic:notEqual name="people" property="anyDateExpired" value="Y">
	    	                    class="tabletext"
		                      </logic:notEqual>
		                     >

                                <td class="tabletext"><bean:write name="people" property="peopleType.type"/></td>
                                <td class="tabletext"><bean:write name="people" property="licenseNbr"/></td>
                                <td class="tabletext"><a href='<%=contextRoot%>/editPeople.do?peopleId=<bean:write name="people" property="peopleId"/>&psaId=<%=activityId%>&psaType=A&levelType=A'><bean:write name="people" property="name"/></a></td>
                                <td class="tabletext"><bean:write name="people" property="phoneNbr"/></td>
                                <td class="tabletext"><bean:write name="people" property="onHold"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>

				<%
					if((projectName.equals(elms.common.Constants.PROJECT_NAME_LICENSE_AND_CODE))){
				%>
                    <tr>
                		<td colspan="3">&nbsp;</td>
            		</tr>
                    <tr>
                        <td colspan="3">
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                            <tr>
	                                <td width="99%" class="tabletitle">Code Enforcement</td>
	                                <td width="1%" class="tablebutton"><nobr>
	                                	<a href="<%=contextRoot%>/viewCodeEnforcement.do?activityId=<%=activityId%>&editable=<%=editable%>&status=<%=status%>&first=<%=one%>" class="tablebutton">
	                                		View
	                                	</a></nobr>&nbsp;
	                                </td>
	                                </tr>
	                        </table>
                        </td>
                    </tr>
            	 <%}%>

               	 </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
           <%--   <%
                if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING) || projectName.equals(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS)){
            %>
            <logic:equal name="planCheckRequired" value="Y">
             <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                               
                                   
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>" class="tabletitle">PC/Approval</a></td>
                               
                                <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
                                <logic:equal name="editable" value="true">
                                	<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>" class="tablebutton">Add</a>&nbsp;</nobr>
                                </logic:equal>
                                </security:editable>
			 			       
                               <logic:notEqual name="editable" value="true">
                                 <td width="100%" class="tabletitle">PC/Approval</td>
                               </logic:notEqual>
                           </tr>
                        </table>
                        </td>
                    </tr>

               	    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Staff</td>
                                <td class="tablelabel">Department</td>
                                <td class="tablelabel">Status</td>
                                <td class="tablelabel">Status Date</td>
                                <td class="tablelabel">Target Completion Date</td>
                            </tr>

                            <logic:iterate id="planCheck" name="planCheckList" type="elms.app.project.PlanCheck" scope="request" >
                            <logic:iterate name="planCheckForm" property="planCheckList" id="latestPlanCheck" scope="request">
                            <tr valign="top">
								<td class="tabletext"><bean:write name="latestPlanCheck" property="enginner"/></td>
                                <td class="tabletext"><bean:write name="latestPlanCheck" property="department"/></td>
                                <td class="tabletext"><bean:write name="latestPlanCheck" property="statusDesc"/></td>
                                <td class="tabletext"><bean:write name="latestPlanCheck" property="planCheckDate"/></td>
                                <td class="tabletext"><bean:write name="latestPlanCheck" property="targetDate"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                        
                    </tr>
                   </table>
                   </td>
                   </tr>
                    </logic:equal>
                    <% } %>
 --%>                    <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="98%" class="tabletitle">
                                	Finance Manager
                                </td>
                                <logic:equal scope="request" name="disableFees" value="Y">
                                	<font color="red">&nbsp;&nbsp;(Plan Check Entry Required before adding Fees)
                                </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel">
                                <div align="right">Activity</div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tablelabel">
                                <div align="right">Plan Check</div>
                                </td>
								<td class="tablelabel">
									<div align="right">Development Fee</div>
								</td>
								<td class="tablelabel">
                                <div align="right">Contractor Tax</div>
                                </td>
								<% } %>
								<td class="tablelabel">
									<div align="right">Penalty Fee</div>
								</td>
                                <td class="tablelabel">
                                <div align="right">Total</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">

						     	  <%
					                if(!activityType.equalsIgnoreCase("PKNCOM")){
					              %>
									<logic:equal scope="request" name="disableFees" value="N">
                                      <%if(editable.equals("true")){%>
                                      	<a href="<%=contextRoot%>/viewFeesMgr.do?id=<%=activityId%>" class="tablelabel">
                                      <%}%>
                                   </logic:equal>
                                    <%}%>Fees</a>
									 

                                </td>

                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitFeeAmt"/></div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
								<div align="right"><bean:write	scope="request" name="financeSummary"	property="developmentFeeAmt" /></div>
								</td>
								<td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxFeeAmt"/></div>
                                </td>
								<% } %>
								<td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="penaltyFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalFeeAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">

                                	<logic:equal scope="request" name="disableFees" value="N">
                                      <%if(editable.equals("true")){%>
                                      	<a href="<%=contextRoot%>/viewPayment.do?levelId=<%=activityId%>&level=<%=level%>" class="tablelabel" id="pay">
                                      <%}%>
                                   </logic:equal>Payment</a>
                                </td>

                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitPaidAmt"/></div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="developmentFeePaidAmt" /></div>
								</td>
								<td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxPaidAmt"/></div>
                                </td>
								<% } %>
                                <td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="penaltyPaidAmt" /></div>
								</td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalPaidAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Balance Due </td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="permitAmountDue"/></div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="planCheckAmountDue"/></div>
                                </td>
                                <td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="developmentFeeAmountDue" /></div>
								</td>
								<td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxAmountDue"/></div>
                                </td>
								<% } %>
                                <td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="penaltyAmountDue" /></div>
								</td>
                                <td class="tabletext">
                                <div align="right"><bean:write scope="request" name="financeSummary" property="totalAmountDue"/></div>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>

			<logic:equal name="activity" scope="request" property="activityDetail.activityType.departmentCode" value="PL">
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">
                                	<a href="<%=contextRoot%>/listStatus.do?levelId=<%=activityId%>&levelType=<%=level%>&editable=<%=editable%>&status=<%=status%>">
                                		Status

                                	</a>
                                </td>
                                <logic:equal name="editable" value="true">
                                <td width="1%" class="tablebutton"><nobr>
                                	<a href="<%=contextRoot%>/listStatus.do?levelId=<%=activityId%>&levelType=<%=level%>&editable=true" style="tablebutton">
                                		Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0">
                                	</a></nobr>
                                </td>
                                </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
						    <tr>
								<td class="tablelabel" width="15%">Date</td>
								<td class="tablelabel" width="30%">Status</td>
								<td class="tablelabel" width="55%">Comment</td>
                            </tr>
                            <logic:iterate id="statuss" name="activity" length="3" property="planningDetails.statusList" type="elms.app.planning.StatusEdit">
                            <tr valign="top">
                                <td class="tabletext"><bean:write name="statuss" property="statusDate"/></td>
                                <td class="tabletext"><bean:write name="statuss" property="status"/></td>
                                <td class="tabletext"><bean:write name="statuss" property="comment"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listResolution.do?levelId=<%=activityId%>&levelType=<%=level%>&ed=<%=editable%>" style="tabletitle">Resolutions/Ordinances</a></td>
                                <logic:equal name="editable" value="true">
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/listResolution.do?levelId=<%=activityId%>&levelType=<%=level%>" style="tablebutton">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                                </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
							<td class="tablelabel" width="20%">Resolution #</td>
							<td class="tablelabel" width="60%">Description</td>
							<td class="tablelabel" width="20%">Approved Date</td>
                            </tr>
                            <logic:iterate id="resolution" name="activity" length="3" property="planningDetails.resolutionList" type="elms.app.planning.ResolutionEdit">
                            <tr valign="top">
                                <td class="tabletext"><bean:write name="resolution" property="resolutionNbr"/></td>
                                <td class="tabletext"><bean:write name="resolution" property="description"/></td>
                                <td class="tabletext"><bean:write name="resolution" property="approvedDate"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listEnvReview.do?levelId=<%=activityId%>&level=<%=level%>&editable=<%=editable%>" style="tabletitle">Environmental Review</a></td>
                                <logic:equal name="editable" value="true">
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/listEnvReview.do?levelId=<%=activityId%>&level=<%=level%>&editable=true" style="tablebutton">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                                </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
						    <tr>
								<td class="tablelabel" width="20%">Date</td>
								<td class="tablelabel" width="40%">Env. Determination</td>
								<td class="tablelabel" width="40%">Action</td>
                            </tr>
                            <logic:iterate id="envReview" name="activity" length="3" property="planningDetails.envReviewList" type="elms.app.planning.EnvReviewEdit">
			    			<logic:notEqual name="envReview" property="envDeterminationId" value="0">
                            <tr valign="top">
                                <td class="tabletext"><bean:write name="envReview" property="actionDate"/></td>
                                <td class="tabletext"><bean:write name="envReview" property="envDetermination"/></td>
                                <td class="tabletext"><bean:write name="envReview" property="envDeterminationAction"/></td>
                            </tr>
                            </logic:notEqual>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
		</logic:equal>
	    <tr valign="top">

			<%
            if (businessLicenseProject && activityNumber.trim().startsWith("BL")) {
			 %>
				<%-- BL activity team start--%>
				<td width="49%" height="55">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="99%" class="tabletitle">
                                  <logic:equal name="editable" value="true">
                                  <a href="<%=contextRoot%>/listActivityTeam.do?psaId=<%=activityId%>" class="tabletitle">BL Activity Team</a>
								  </logic:equal>
                                  <logic:notEqual name="editable" value="true">
                                  Activity Team
                                  </logic:notEqual>
                               </td>
                                <td width="1%" class="tablebutton">

                                  <logic:equal name="editable" value="true">
                                  	<a href="<%=contextRoot%>/addActivityTeam.do?psaId=<%=activityId%>" class="tablebutton">Add</a>&nbsp;
                                  </logic:equal>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="activityTeam" name="activityTeamList" type="elms.control.beans.BusinessLicenseApprovalForm">
			                    <tr>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="departmentName" />
								  </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="userName" />
			                      </td>
			                    </tr>
			                      
							</logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
				<%-- BL Activity team end--%>
			<%}else if (businessTaxProject && activityNumber.trim().startsWith("BT")) {%>

				<%-- BT activity team start--%>
				<td width="49%" height="55">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="99%" class="tabletitle">
                                  <logic:equal name="editable" value="true">
                                  <a href="<%=contextRoot%>/listActivityTeam.do?psaId=<%=activityId%>" class="tabletitle">BT Activity Team</a>
								  </logic:equal>
                                  <logic:notEqual name="editable" value="true">
                                  Activity Team
                                  </logic:notEqual>
                               </td>
                                <td width="1%" class="tablebutton">

                                  <logic:equal name="editable" value="true">
                                  	<a href="<%=contextRoot%>/addActivityTeam.do?psaId=<%=activityId%>" class="tablebutton">Add</a>&nbsp;
                                  </logic:equal>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="activityTeam" name="activityTeamList" type="elms.control.beans.BusinessLicenseApprovalForm">
			                    <tr>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="departmentName" />
								  </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="userName" />
			                      </td>
			                     </tr>
							</logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
				<%-- BT Activity team end--%>
				<%}else{%>
				<%-- Common Activity team start--%>

				<td width="49%" height="55">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="99%" class="tabletitle">
                                  <logic:equal name="editable" value="true">
                                  <a href="<%=contextRoot%>/editProcessTeam.do?psaId=<%=activityId%>" class="tabletitle">Activity Team</a>
								  </logic:equal>
                                  <logic:notEqual name="editable" value="true">
                                  Activity Team
                                  </logic:notEqual>
                               </td>
                                <td width="1%" class="tablebutton">
								<security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
                                  <logic:equal name="editable" value="true">
                                  	<a href="<%=contextRoot%>/addProcessTeam.do?psaId=<%=activityId%>" class="tablebutton">Add</a>&nbsp;
                                  </logic:equal>
                                 </security:editable>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="processTeamRecord" name="processList" type="elms.app.common.ProcessTeamRecord" scope="request">
                            <tr class="tabletext">
                                <td class="tablelabel"><bean:write name="processTeamRecord" property="title"/></td>
                                <td class="tabletext"><bean:write name="processTeamRecord" property="name"/></td>
                                <logic:equal name="processTeamRecord" property="lead" value="on">
                                <td class="tabletext"><img src="../images/site_icon_mgmt.gif" width="21" height="20"></td>
                                </logic:equal>
                                <logic:notEqual name="processTeamRecord" property="lead" value="on">
                                <td class="tabletext"></td>
                                </logic:notEqual>
                            </tr>
                           </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
				<%-- common Activity team end--%>
				<%} %>
                <td width="2%" height="55">&nbsp;</td>
                <td width="49%" height="55">

                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                               <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=activityId%>&conditionLevel=<%=level%>&ed=<%=editable%>&status=<%=status%>" class="tabletitle">Conditions</a></td>

	                               <logic:equal name="editable" value="true">
	                                 <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addCondition.do?levelId=<%=activityId%>&conditionLevel=<%=level%>" class="tablebutton">Add</a></nobr>&nbsp;</td>
	                               </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr valign="top">
                                <td width="50%" class="tablelabel">Total ( <%=totalCondCount%> ) </td>
                                <td width="50%" class="tablelabel">Warning ( <%=warnCondCount%> ) </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="cond" name="condList" type="elms.app.common.Condition" scope="request">
                            <tr valign="top">
                                <td class="tabletext"><bean:write name="cond" property="conditionCode"/></td>
                                <td class="tabletext"><bean:write name="cond" property="shortText"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>



			<jsp:include page="../includes/comments.jsp" flush="true">
				<jsp:param name="id" value="<%=activityId%>" />
				<jsp:param name="level" value="<%=level%>" />
				<jsp:param name="editable" value="<%=editable%>" />
			</jsp:include>


            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=activityId%>&level=<%=level%>" class="tabletitle">Attachments</a></nobr></td>
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=activityId%>&level=<%=level%>" class="tablebutton">Add</a>&nbsp;</nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">ID</td>
                                <td class="tablelabel" >Description</td>
                                <td class="tablelabel">Size</td>
                                <td class="tablelabel" >Date</td>
                            </tr>
							<%int i=0;%>
                            <logic:iterate id="attachment" name="attachmentList" type="elms.app.common.Attachment" scope="request">
						<%
							List attachmentsList = (List)request.getAttribute("attachmentList");
							Attachment attachmentObj = new Attachment();
							String fileNameStr = "";

							try{
								attachmentObj = (Attachment)attachmentsList.get(i);
								fileNameStr = attachmentObj.file.getFileName();
								fileNameStr = java.net.URLDecoder.decode(fileNameStr, "UTF-8");
							}catch(Exception e){}
						 %>
                            <tr valign="top" >
                                <td class="tabletext"> <%=fileNameStr%> </td>
                                <td class="tabletext"> <bean:write name="attachment" property="description"/> </td>
                                <td class="tabletext"> <bean:write name="attachment" property="file.fileSize"/> </td>
                                <td class="tabletext"> <bean:write name="attachment" property="file.strCreateDate"/> </td>
                            </tr>
						<%i++; %>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            
            
            <!-- start online questionaire-->
            <%if(questionList.size()>0){ %>
             <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td class="tabletitle"><nobr>Online Questionaire</nobr></td>
                                
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">ID</td>
                                <td class="tablelabel" >Question</td>
                                <td class="tablelabel">Answer</td>
                               
                            </tr>
							<% i=0;%>
                            
						<%
							for(int q=0;q<questionList.size();q++){
							elms.control.beans.online.ApplyOnlinePermitForm a = (elms.control.beans.online.ApplyOnlinePermitForm) questionList.get(q);
							String imgsrc ="../images/unchecked.png";
							if(a.getTempQuestionId().equals("Y")){
								imgsrc ="../images/check.png";
							}
							
						 %>
                            <tr valign="top" >
                                <td class="tabletext"> <%=a.getTempOnlineID()%> </td>
                                <td class="tabletext"> <%=a.getQuestionaireDescription()%> </td>
                                <td class="tabletext"> <img src="<%=imgsrc%>"> </td>
                               
                            </tr>
						<%} %>
                           
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
             <%}%>
            <!-- end -->
            

        </table>
        </td>
        <td width="1%">
        <table width="200" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=<%=activityId%>&level=<%=level%>&ed=<%=editable%>&status=<%=status%>" class="tabletitle">Holds</a></td>
                                   <logic:equal name="editable" value="true">
                                     <td width="1%" class="tablebutton">
                                     <nobr><a href="<%=contextRoot%>/addHold.do?levelId=<%=activityId%>&level=<%=level%>" class="tablebutton">Add</a>&nbsp;</nobr></td>
                                   </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="hold" name="holds" type="elms.app.common.Hold" scope="request">
                            <tr valign="top" >
                                <td class="tabletext"> <bean:write name="hold" property="type"/></td>
                                <td class="tabletext"><bean:write name="hold" property="title"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Site Data</td>
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=<%=activityId%>&levelType=A" class="tablebutton">View</a>&nbsp;</nobr></td>
                    </tr>
                </table>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

			<%
            if (businessLicenseProject) {
			 %>

            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Approvals</td>
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewBusinessLicenseApproval.do?activityId=<%=activityId%>" class="tablebutton">View</a>&nbsp;</nobr></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

			<%
            }
			 %>
			<%
            if (businessTaxProject) {
			 %>

            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">BT Approvals</td>
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewBusinessLicenseApproval.do?activityId=<%=activityId%>" class="tablebutton">View</a>&nbsp;</nobr></td>
                    </tr>
                </table>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>

			<%
            }
			 %>

            <%
                if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING) || projectName.equals(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS)){
            %>


            <logic:equal name="planCheckRequired" value="Y">
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                               <logic:equal name="editable" value="true">
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>" class="tabletitle">Plan Check</a></td>
                                <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
                                <%-- <% 		
                     				if(user.getRole().description == Constants.ROLES_ADMINISTRATOR || isPlanCheckManager) {
                     		 %> --%>
                                	<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>&showAdd=yes" class="tablebutton">Add</a>&nbsp;</nobr>
                                	<%-- <%} %> --%>
                                </security:editable>
			 			       </logic:equal>
                               <logic:notEqual name="editable" value="true">
                                 <td width="100%" class="tabletitle">Plan Check</td>
                               </logic:notEqual>
                            </tr>
                        </table>
                        </td>
                    </tr>
<!--                     <tr> -->
<!--                         <td background="../images/site_bg_B7C1CB.jpg"> -->
<!--                         <table width="100%" border="0" cellspacing="1" cellpadding="2"> -->
                        
<!--                             <tr valign="top"> -->
<%--                                 <td class="tablelabel" width="1%"><bean:write scope="request" name="latestPlanCheck" property="date"/></td> --%>
<%--                                 <td class="tabletext" width="99%"><bean:write scope="request" name="latestPlanCheck" property="statusDesc"/> </td> --%>
<!--                             </tr> -->
<!--                         </table> -->
<!--                         </td> -->
<!--                     </tr> -->
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
			</logic:equal>

			<logic:equal name="inspectionRequired" value="Y">
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listInspection.do?activityId=<%=activityId%>&type=<%=activityType%>&ed=<%=editable%>&insp=<bean:write name='activity' property='inspectable' scope='request'/>" class="tabletitle">Inspections</a></td>
                                <logic:equal name="editable" value="true">
	                                <logic:equal name="activity" property="inspectable" value="true" scope="request">
		                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addInspection.do?activityId=<%=activityId%>&type=<%=activityType%>" class="tablebutton">Add</a>&nbsp;</nobr></td>
	                                </logic:equal> 
                                </logic:equal>

                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
			  			<logic:iterate id="inspection" name="inspectionList" type="elms.app.inspection.Inspection" scope="request" >
                            <tr valign="top">
                                <td class="tabletext" width="1%"><bean:write name="inspection" property="date"/></td>
                                <td class="tabletext" width="99%"><bean:write name="inspection" property="inspectionItem"/></td>
                                <td class="tabletext" width="99%"><bean:write name="inspection" property="actionCodeDescription"/></td>
                            </tr>
                        </logic:iterate>
                        </table>
                        </td>
                    </tr>
				</logic:equal>

            <tr>
                <td>&nbsp;</td>
            </tr>
            <%
                }
            %>

		            <tr>
		                <td>
		                <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr>
		                        <td>
		                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr>
		                                <td width="99%" class="tabletitle">
				                            <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
		        	                        	<a href="<%=contextRoot%>/listLinkActivity.do?activityId=<%=activityId%>&editable=<%=editable%>&active=Y" class="tabletitle">
		                    	            </security:editable>
		            	                    		Linked Activities

				                            <security:editable levelId="<%= activityId%>" levelType="A" editProperty="allProjectAndDept">
		                    	            	</a>
		                    	            </security:editable>
		                        	    </td>

		                            </tr>
		                        </table>
		                        </td>
		                    </tr>
		                    
		                    
		                    <tr>
		                        <td background="../images/site_bg_B7C1CB.jpg">
		                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

								<logic:iterate id="linkedActivity" name="linkedActivityList" type="elms.app.planning.LinkActivityEdit" scope="request" >
		                            <tr valign="top">
		                                <td class="tabletext"><bean:write name="linkedActivity" property="activityNbr"/></td>
		                                <td class="tabletext"><bean:write name="linkedActivity" property="description"/></td>
		                            </tr>
		                        </logic:iterate>

		                        </table>
		                        </td>
		                    </tr>
		                </table>
		                </td>
		            </tr>
					<tr>
		                <td>&nbsp;</td>
		            </tr>
		     <%
		     int count=LookupAgent.getDepartmentForActType(activityType);
		     if(count >=1 && (user.getDepartment().getDepartmentId()==Constants.DEPARTMENT_HOUSING))
		     {
		     %>       
    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/viewCodeEnforcement.do?activityId=<%=activityId%>&editable=<%=editable%>&status=<%=status%>&first=<%=one%>&show=NoticeType&deletable=<%=deletable%>" class="tabletitle">
                                Notices</a>
<%--                                  <font class="con_hdr_1">[<bean:write name="activityAttributes" property="totalNotice"/>]</font> --%>
                                 </td>
                                   <logic:equal name="editable" value="true">
                                     <td width="1%" class="tablebutton">
                                     <nobr><a href="<%=contextRoot%>/viewCodeEnforcement.do?activityId=<%=activityId%>&editable=<%=editable%>&status=<%=status%>&first=<%=one%>&show=NoticeType&deletable=<%=deletable%>&action=addNotice" class="tablebutton">Add</a>&nbsp;</nobr></td>
                                   </logic:equal>
                            </tr>
                             <tr>
				                <td>
				                <div style=" height: 70px; width: 100px; font-size: 5px; overflow-y: auto;"> 
				                  <table width="220%" border="0" cellspacing="1" cellpadding="2">
				                     <logic:iterate id="notice" name="notices">
				                           <tr valign="top" class="tabletext">
				                               <td><bean:write name="notice" property="ceType"/></td>
				                          </tr>
				                    </logic:iterate>
				                  </table>
				                  </div>
				                </td>
				            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
		                <td>&nbsp;</td>
		            </tr>
		            
		      <%}%>      
                     <tr>
				          <td>
				            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				              <tr>
				                <td>
				                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				                    <tr>
				                      <td width="99%" class="tabletitle">
				                    	<a href="<%=contextRoot%>/listPeople.do?id=<%=lsoId1%>&type=<%=lsoType %>&ed=<%=editable%>&levelId=<%=activityId%>&levelType=A" class="tabletitle">Tenants</a>        
				                      </td>                 
				                    </tr>
				                  </table>
				                </td>
				              </tr>
				               <tr>
					               <td>
<!-- 					               <div style=" height: 100px; width: 100px; font-size: 5px; overflow-y: auto;">  -->
					                 <table width="220%" border="0" cellspacing="1" cellpadding="2">
					                    <logic:iterate id="tenant" name="tenants">
					                          <tr valign="top" class="tabletext">
					                              <td><a href='<%=contextRoot%>/editPeople.do?peopleId=<bean:write name="tenant" property="tenantId"/>&lsoId=<%=lsoId1 %>' ><bean:write name="tenant" property="name"/></a></td>
					                         </tr>
					                   </logic:iterate>
					                 </table>
<!-- 					                 </div> -->
					               </td>
					          </tr>
				              <tr>
				                <td background="../images/site_bg_B7C1CB.jpg">
				                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
				                  
				                  </table>
				                </td>
				              </tr>
				            </table>
				          </td>
				      </tr>
		              
		                </table>
		                </td>
		            </tr>
					<tr>
		                <td>&nbsp;</td>
		            </tr>

            <%
            	
                if(activityType.equalsIgnoreCase("PKNCOM")){
            %>

		             <tr>
		                <td>
		                <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr>
		                        <td>
		                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                            <tr>
		                                <td width="99%" class="tabletitle">
				                            <security:editable levelId="<%=activityId%>" levelType="A" editProperty="allProjectAndDept">
		        	                        	<a href="<%=contextRoot%>/listLncvActivity.do?activityId=<%=activityId%>&editable=<%=editable%>&active=Y" class="tabletitle">
		                    	            </security:editable>
		            	                    		LNCV Permits - Used (<%=lncvUsed%>) &nbsp; Avail(<%=lncvAvail%>)

				                            <security:editable levelId="<%= activityId%>" levelType="A" editProperty="allProjectAndDept">
		                    	            	</a>
		                    	            </security:editable>
		                        	    </td>

		                            </tr>
		                        </table>
		                        </td>
		                    </tr>
		                    <tr>
		                        <td background="../images/site_bg_B7C1CB.jpg">
		                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
								<logic:iterate id="lncvActivity" name="lncvList" type="elms.app.people.People" scope="request" length="3">
		                            <tr valign="top">
		                                <td class="tabletext"><bean:write name="lncvActivity" property="date"/></td>
		                            </tr>
		                        </logic:iterate>

		                        </table>
		                        </td>
		                    </tr>
		                </table>
		                </td>
		            </tr>
            <%
                }
            %>
		            
					<tr>
		                <td>&nbsp;</td>
		            </tr>


			<jsp:include page="../includes/mapPanel.jsp" flush="true">
				<jsp:param name="addressString" value="<%=address %>" />
			</jsp:include>


               </table>
                </td>
            </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html:html>
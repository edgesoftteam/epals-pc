<%@ page import='elms.agent.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ page import='elms.app.admin.CustomField' %>
<%@ page import='elms.control.beans.GreenHaloForm' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Modify
Green Halo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
//function textCounter(field,maxlimit) {
//alert(field.value);
//if (field.value.length > maxlimit) // if too long...trim it!
//field.value = field.value.substring(0, maxlimit);
// otherwise, update 'characters left' counter
//else
//countfield.value = maxlimit - field.value.length;
//}

function textCounter(field,maxlimit) {
//alert(field.value);
if (field.value.substring(0, maxlimit)) // if too long...trim it!
field.value = field.value.substring(0, maxlimit);
// otherwise, update 'characters left' counter
//else
//countfield.value = maxlimit - field.value.length;
}
</script>
</head>
<%
	String contextRoot = request.getContextPath();

	String actid = (String)request.getAttribute("actid");



    //String certificateURL=LookupAgent.getReportsUrl("CERTIFICATE_URL");
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction(){
	if (document.forms['greenHaloForm'].elements['greenHalo.projectStartDate'].value != ''){
   		strValue=validateData('date',document.forms['greenHaloForm'].elements['greenHalo.projectStartDate'],'Project Start Date is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.projectEndDate'].value != ''){
   		strValue=validateData('date',document.forms['greenHaloForm'].elements['greenHalo.projectEndDate'],'Project End Date is a required field');
   	    return strValue;
   	}
		strValue = true;
    return strValue;
}
function onclickprocess()
{
	document.location.href ='<%=request.getContextPath()%>/viewActivity.do?activityId='+<%=actid%>;
}

function onchangeprocess()
{
	if (document.forms['greenHaloForm'].elements['greenHalo.companyName'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.companyName'],'Company Name is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.projectName'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.projectName'],'Project Name is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.phone'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.phone'],'Phone is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.email'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.email'],'Email is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.street'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.street'],'Street is a required field');
   	    return strValue;
   	}
	/*if (document.forms['greenHaloForm'].elements['greenHalo.aptSuite'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.aptSuite'],'Apt Suite is a required field');
   	    return strValue;
   	}*/
	if (document.forms['greenHaloForm'].elements['greenHalo.zipcode'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.zipcode'],'Zip code is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.squareFootage'].value == '' && document.forms['greenHaloForm'].elements['greenHalo.squareFootage'].value==0){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.squareFootage'],'Square footage is a required field and must be greater then zero.');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.projectStartDate'].value == ''){
   		strValue=validateData('date',document.forms['greenHaloForm'].elements['greenHalo.projectStartDate'],'Project Start Date is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.projectEndDate'].value == ''){
   		strValue=validateData('date',document.forms['greenHaloForm'].elements['greenHalo.projectEndDate'],'Project End Date is a required field');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.value'].value == '' && document.forms['greenHaloForm'].elements['greenHalo.value'].value == 0){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.value'],'Value is a required field and must be greater then zero.');
   	    return strValue;
   	}
	if (document.forms['greenHaloForm'].elements['greenHalo.buildingType'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.buildingType'],'Building type is a required field');
   	    return strValue;
   	}
   	if (document.forms['greenHaloForm'].elements['greenHalo.projectType'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.projectType'],'Project type is a required field');
   	    return strValue;
   	}
   	if (document.forms['greenHaloForm'].elements['greenHalo.permitSqFootage'].value == ''&& document.forms['greenHaloForm'].elements['greenHalo.permitSqFootage'].value == 0 ){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.permitSqFootage'],'Permit sq footage is a required field and must be greater then zero.');
   	    return strValue;
   	}
   	if (document.forms['greenHaloForm'].elements['greenHalo.permit'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.permit'],'Permit is a required field');
   	    return strValue;
   	}
   	/*if (document.forms['greenHaloForm'].elements['greenHalo.permitProjectType'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.permitProjectType'],'Permit project type is a required field');
   	    return strValue;
   	}
   	if (document.forms['greenHaloForm'].elements['greenHalo.permitValue'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.permitValue'],'Permit value is a required field');
   	    return strValue;
   	}*/
   	
	if (document.forms['greenHaloForm'].elements['greenHalo.description'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.description'],'Description is a required field');
   	    return strValue;
   	}
	/*if (document.forms['greenHaloForm'].elements['greenHalo.permitNote'].value == ''){
   		strValue=validateData('req',document.forms['greenHaloForm'].elements['greenHalo.permitNote'],'Permit note is a required field');
   	    return strValue;
   	}*/
		strValue = true;
	if(strValue){
		document.forms['greenHaloForm'].action='<%=contextRoot%>/saveGreenHalo.do';
		document.forms['greenHaloForm'].submit();
    	return true;
	}
}
window.onload = function(){
	if(document.forms['greenHaloForm'].elements['greenHalo.companyName'].value !=""){
		document.getElementById('updateGreen').style.display='block';
		document.getElementById('saveGreen').style.display='none';
	}else{
		document.getElementById('updateGreen').style.display='none';
		document.getElementById('saveGreen').style.display='block';
	}
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:form name="greenHaloForm" type="elms.control.beans.GreenHaloForm" action="" >


	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Green Halo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<br>
					<br>
					</td>
				</tr>
				<tr>
					<td><html:errors /></td>
				</tr>
				<tr>
					<td><font color='green'><%= (String)request.getAttribute("greenHaloSaveStatus") %></font></td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="95%" background="../images/site_bg_B7C1CB.jpg"><font
										class="con_hdr_2b">Green Halo</td>
									<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
										width="46" height="25"></td>
									<td width="100%" class="tablelabel" id="updateGreen" style="display: block;height:25;"><nobr>
									 <html:button  property="back"  value="Back" styleClass="button" onclick="onclickprocess();" />
									 <html:button  property="Update"  value="Update" styleClass="button" onclick="onchangeprocess();" />
									</td>
									<td width="100%" class="tablelabel" id="saveGreen" style="display: block;height:25;"><nobr>
									 <html:button  property="back"  value="Back" styleClass="button" onclick="onclickprocess();" />
									  <html:button property="Save"  value="Save" styleClass="button" onclick="onchangeprocess();" />
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Company Name: </td>
									<td class="tabletext"><html:text property="greenHalo.companyName" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Project Name:</td>
									<td class="tabletext"><html:text property="greenHalo.projectName" size="30" maxlength="30" styleClass="textbox" /></td>
									
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Phone: </td>
									<td class="tabletext"><html:text property="greenHalo.phone" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Email:</td>
									<td class="tabletext"><html:text property="greenHalo.email" size="30" maxlength="30" styleClass="textbox" /></td>
									
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Street: </td>
									<td class="tabletext"><html:text property="greenHalo.street" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Apt Suite:</td>
									<td class="tabletext"><html:text property="greenHalo.aptSuite" size="30" maxlength="30" styleClass="textbox" /></td>
									
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Zip Code: </td>
									<td class="tabletext"><html:text property="greenHalo.zipcode" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Square Footage:</td>
									<td class="tabletext"><html:text property="greenHalo.squareFootage" size="30" maxlength="30" styleClass="textbox" /></td>
									
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Project Start Date : </td>
									<td class="tabletext"><nobr> <html:text property="greenHalo.projectStartDate"  size="30" maxlength="30"
										styleClass="textboxd" onkeypress="return validateDate();" /> <html:link
										href="javascript:show_calendar('greenHaloForm.elements[12]');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;"><img src="../images/calendar.gif" width="16" height="15" border=0 />
									</html:link></nobr></td>									
									
									<td class="tablelabel"><font	class="con_hdr_1"> Project End Date : </td>
									<td class="tabletext"><nobr> <html:text property="greenHalo.projectEndDate" maxlength="10"
										styleClass="textboxd" onkeypress="return validateDate();" /> <html:link
										href="javascript:show_calendar('greenHaloForm.elements[13]');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;"><img src="../images/calendar.gif" width="16" height="15" border=0 />
									</html:link></nobr></td>
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1"> Value: </td>
									<td class="tabletext"><html:text property="greenHalo.value" size="30" maxlength="30" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1">Building Type </td>
									
									<td class="tabletext">
									
									
									<nested:select  property="greenHalo.buildingType"  styleClass="textbox">
										<option value="">Please Select</option>
                    					<html:options collection="buildingTypes" property="id" labelProperty="label" />
                    				</nested:select>
									</td>
									
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Project Type </td>
									
									<td class="tabletext">
									
									
									<nested:select  property="greenHalo.projectType"  styleClass="textbox">
										<option value="">Please Select</option>
                    					<html:options collection="projectTypes" property="id" labelProperty="label" />
                    				</nested:select>
									</td>
									
									<td class="tablelabel"><font	class="con_hdr_1"> Permit Square Footage:</td>
									<td class="tabletext"><html:text property="greenHalo.permitSqFootage" size="30" maxlength="30" styleClass="textbox" /></td>
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Permit: </td>
									<td class="tabletext"><html:text property="greenHalo.permit" size="25" maxlength="10" styleClass="textbox"/></td>
									<td class="tablelabel"><font	class="con_hdr_1"> Permit Project Type:</td>
									<td class="tabletext"><html:text property="greenHalo.permitProjectType" size="30" maxlength="30" styleClass="textbox" /></td>
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Permit Value(Valuation): </td>
									<td class="tabletext"><html:text property="greenHalo.permitValue" size="25" maxlength="10" styleClass="textbox" /></td>
									<td class="tablelabel"><font	class="con_hdr_1"> </td>
									<td class="tabletext"></td>
    								
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Description:

								<td class="tabletext"  colspan="3"><html:textarea property="greenHalo.description" rows= "6" cols="60" onkeydown="textCounter(this,450);"  onkeyup="textCounter(this,450);"/></td>
								</tr>
								<tr>
									<td class="tablelabel"><font	class="con_hdr_1">Permit Note:

								<td class="tabletext"  colspan="3"><html:textarea property="greenHalo.permitNote" rows= "6" cols="60" onkeydown="textCounter(this,450);"  onkeyup="textCounter(this,450);"/></td>
								</tr>
							</table>
			</td>
			<td width="1%"></td>
		</tr>
	</table>
</html:form>
</body>
</html:html>

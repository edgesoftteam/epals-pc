<%@page import="elms.security.User"%>
<%@page import="elms.util.StringUtils"%>
<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<%@ page import='elms.common.Constants'%>

<app:checkLogon />
<%     String contextRoot = request.getContextPath();
	   User user =(User) session.getAttribute(Constants.USER_KEY);
	   String department=(String)request.getAttribute("deptDesc");
	   String fromDate="";
	   String toDate="";
	   if(request.getAttribute("fromDate")!= null){
		   fromDate=(String)request.getAttribute("fromDate");
	   }
	   if(request.getAttribute("toDate")!= null){
		   toDate=(String)request.getAttribute("toDate");
	   }
	   System.out.println("fromDate.. "+fromDate+" todate.."+toDate);

		request.setAttribute("fromDate", fromDate);
		request.setAttribute("toDate", toDate);
%>

<%@page import="elms.util.OBCTimekeeper"%><html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Edit PC/Approval</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/elms.css" type="text/css">
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>

<link rel="stylesheet" href="../css/kendo.default-v2.min.css"/>
<script language="javascript" type="text/javascript" src="../script/kendo.all.min.js"></script>
</head>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<% String fromWhere = (String)request.getAttribute("fromWhere"); 
System.out.println("fromewhere.. "+fromWhere);%>
<%
int deptId = 0;
if(request.getAttribute("deptId") != null){
	deptId = Integer.parseInt(request.getAttribute("deptId").toString());
}

// Getting the employees as engineers from the groups  1-Inspector, 6-Supervisor, 7-Engineer , 11- Permit Tech
java.util.List engineerUsers = PlanCheckAgent.getEngineerUsers(deptId);
pageContext.setAttribute("engineerUsers", engineerUsers);
String activityId = (String)request.getAttribute("activityId");
if(activityId == null) activityId ="";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
String planCheckId =(String)request.getAttribute("planCheckId");
if(planCheckId ==null) planCheckId = "";


int moduleId = 0;
if(request.getAttribute("moduleId") != null){
	   moduleId = Integer.parseInt(request.getAttribute("moduleId").toString());
}

OBCTimekeeper r = new OBCTimekeeper();
r.addDay(15);
OBCTimekeeper f = new OBCTimekeeper();
f.addDay(7);
OBCTimekeeper curr = new OBCTimekeeper();


%>
<script language="JavaScript" src="../script/calendar.js"></script>
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">

<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/util.js"></script>
<script>
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");
</script>
<script>
var strValue;
function validateFunction(button) {
	    
   	    if (document.forms['planCheckForm'].elements['planCheckStatus'].value == '-1')  {
	    	alert('Please select status');
	    	document.forms['planCheckForm'].elements['planCheckStatus'].focus();
	    	strValue=false;
	    }else if (document.forms[0].elements['planCheckDate'].value == '')  {
	    	alert('Please select date');
            document.forms[0].elements['planCheckDate'].focus();
	    	return false;
// 		}else if (document.forms[0].elements['targetDate'].value == '')  {
// 	    	alert('Please select target date');
// 	        document.forms[0].elements['targetDate'].focus();
// 	    	return false;
		}else if(!validateData('date',document.forms[0].elements['planCheckDate'],'Invalid date format')){
			document.forms[0].elements['planCheckDate'].focus();
			return false;
// 		}else if(!validateData('date',document.forms[0].elements['targetDate'],'Invalid date format')){
// 			document.forms[0].elements['targetDate'].focus();
// 			return false;
		}else if (document.forms[0].elements['unitHours'].value == ''&&document.forms[0].elements['unitMinutes'].value == '')  {
	    	alert('Select Time Unit');
	    	document.forms[0].elements['unit'].focus();
	    	 return false;
		}else {
			var targetDate=document.forms[0].elements['targetDate'].value;
			var planCheckDate=document.forms[0].elements['planCheckDate'].value;
				
			//var str=compareDate(targetDate,planCheckDate);
			//if(str==true){
			//alert("Target completion date can not be less than Status Date");
			//return false;
			//}
		}
		<%-- <%if(request.getAttribute("fromWhere") != null){ %>
			document.forms[0].action = document.forms[0].action + "?fromWhere=<%=request.getAttribute("fromWhere")%>"
			<%if(request.getAttribute("deptDesc") != null){%>
			document.forms[0].action = document.forms[0].action + "&deptDesc=<%=request.getAttribute("deptDesc")%>";
		<%}}%> --%>
		

		document.forms[0].action = document.forms[0].action + "?fromWhere=<%=fromWhere%>&fromDate=<%=fromDate%>&toDate=<%=toDate%>";
		document.forms[0].submit();
}



function onclickprocess(strval)
{
    if (strval == 'dp') {
      document.forms['planCheckForm'].action='<%=contextRoot%>/deletePlanCheck.do?psaId=<%=activityId%>';
      document.forms['planCheckForm'].submit();
    }  
    
}    
function goBack(button)
{
	var fromWhere='<%=fromWhere%>';
	var str= <%=activityId%>;
	//alert(str);
	//button.value='Please Wait..';
	button.disabled=true;
	if(fromWhere == 'viewAllPC'){
<%-- 		window.location.href='<%=contextRoot%>/viewAllPlanCheck.do?action=initialOne'; --%>
		top.location='<%=contextRoot%>/viewAllPlanCheck.do?action=refresh';
	}else{
		document.location.href='<%=contextRoot%>/addPlanCheck.do?activityId=<%=activityId%>';
	}
}  
function checkDate (field) {
	return validateData('date',field,'Date is not Valid');
}

function addDays(pcDate,noofdays) {
	var trgDate = new Date(pcDate);
	trgDate.setDate(trgDate.getDate() + noofdays);
	var dd = trgDate.getDate();
	var mm = trgDate.getMonth() + 1;
	var y = trgDate.getFullYear();
	var formattedDate = mm + '/'+ dd + '/'+ y;
	return formattedDate;
	}


function updateTargetDate(processType){
	var daysToAdd = 0;
<%-- 	if(processType == <%= Constants.PC_PROCESS_REGULAR%>){ --%>
<%-- 		daysToAdd = <%= Constants.PC_DAYS_TO_ADD_REGULAR%>; --%>
<%-- 	}else if(processType == <%= Constants.PC_PROCESS_FAST_TRACK%>){ --%>
<%-- 		daysToAdd = <%= Constants.PC_DAYS_TO_ADD_FAST_TRACK%>; --%>
<%-- 	}else if(processType == <%= Constants.PC_PROCESS_EXPRESS%>){ --%>
// 		document.forms[0].elements['targetDate'].value = "";
// 		return;
// 	}else{
// 		daysToAdd = 0;
// 	}


	if(processType == <%= Constants.PC_PROCESS_ADU%>){
		daysToAdd = <%= Constants.PC_DAYS_TO_ADD_ADU%>;
	}else if(processType == <%= Constants.PC_PROCESS_REGULAR%> || processType == <%= Constants.PC_PROCESS_FAST_TRACK%> || processType == <%= Constants.PC_PROCESS_FAST_TRACK%>  || processType == <%= Constants.PC_PROCESS_OTC%>){
		document.forms[0].elements['targetDate'].value = "";
		return;
	}else{
		daysToAdd = 0;
	}
	dateStr = document.forms[0].elements['created'].value;
	dateStr = addDays(dateStr,daysToAdd);
	document.forms[0].elements['targetDate'].value = dateStr;
}

<%-- function changeStatus(field) {

	if(field == <%= Constants.PI_STATUS_RESUBMITTED%>){
		if(document.forms[0].processType.value ==  '<%= Constants.PC_PROCESS_REGULAR%>'){
			document.forms[0].targetDate.value = '<%=r.getString("MM/DD/YYYY")%>';
		}
		if(document.forms[0].processType.value ==  '<%= Constants.PC_PROCESS_FAST_TRACK%>'){
			document.forms[0].targetDate.value = '<%=f.getString("MM/DD/YYYY")%>';
		}
		
	}

	document.forms[0].planCheckDate.value = '<%=curr.getString("MM/DD/YYYY")%>';
	document.forms[0].comments.value = '';		
	
	
} --%>

</script>
<html:form action="/savePlanCheck">
	<html:hidden property="activityId" />
    <html:hidden property="todaysDate" />
	<html:hidden property="planCheckId" value="<%=planCheckId%>" />
	<html:hidden property="created"/>
	<html:hidden property="department" />

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Edit PC/Approval</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br></td>
			</tr>
			<html:errors/>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" class="tabletitle">EDIT</td>
										
										<td width="1%" class="tablelabel">
											<nobr>
<%-- 												<%if(fromWhere != null && fromWhere.equalsIgnoreCase("viewAllPC")){ %> --%>
<%-- 												<%System.out.println(user.getDepartment().getDescription().equalsIgnoreCase(department)); --%>
<!-- // 												System.out.println(user.getDepartment().getDescription()); -->
<!-- // 												System.out.println(department); -->
<%-- 												if(user.getDepartment().getDescription().equalsIgnoreCase(department)){ %> --%>
<%-- 												<html:button property="back" value="Back" styleClass="button" onclick="javascript:history.back()" />&nbsp; --%>
<%-- 												<html:button property="Save" value="Save" styleClass="button" onclick="validateFunction(this);" /> --%>
<%-- 												<%}%> --%>
<%-- 												<%}else{ %> --%>
													<html:button property="back" value="Back" styleClass="button" onclick="goBack(this);" />&nbsp;
													<html:button property="Save" value="Save" styleClass="button" onclick="validateFunction(this);" />
<%-- 												<%} %> --%>
												 &nbsp;
											</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td class="tablelabel">Plan Check Status<font class="red1"><strong>*</strong></font></td>
                                         <td class="tablelabel">Department<font class="red1"><strong>*</strong></font></td> 
										 <td class="tablelabel">Staff</td> 
										<td class="tablelabel">Status Date<font class="red1"><strong>*</strong></font></td>
										 <td class="tablelabel">Target Completion Date
<!--                                          <font class="red1"><strong>*</strong></font> -->
                                         </td>
                                        <td class="tablelabel">Process Type</td>
                                        <td class="tablelabel">Time Units</td>
									</tr>
									<tr valign="top">
										<td class="tabletext"><html:select property="planCheckStatus"
											styleClass="requiredtext" style="font-size:10px;">
											<html:options collection="planCheckStatuses" property="code"
												labelProperty="description" />
										</html:select></td>
										
										<td class="tabletext"><!-- <font	class="con_text_1" style="vertical-align: middle"> --><bean:write name="deptDesc" /> <!-- </font> --></td>
										 
										<td bgcolor="#FFFFFF"><html:select property="engineerId" styleClass="textbox">
											<html:options collection="engineerUsers" property="userId" labelProperty="name" />
										</html:select></td>
										<td class="tabletext"  valign="top"><nobr>
                                           
                                            <html:text property="planCheckDate" size="10" styleId="plancheckdatepicker1" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13"/>
                                        </td>
										<td class="tabletext"  valign="top"><nobr>
                                          
                                            <html:text property="targetDate" size="10" styleId="targetdatepicker1" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13"/>
                                        </td> 
                                        <td class="tabletext" width="116"><html:select property="processType" styleClass="textbox" onchange="updateTargetDate(this.value)">
											<html:options collection="planCheckProcessTypeList" property="processTypeId" labelProperty="processType" />
										    </html:select>
										</td>
                                         
                                       <td class="tabletext" valign="top">
                                              <nobr><%-- <html:text  property="unit"  maxlength="10"  styleClass="textbox" onkeypress="return validateDecimal();"/> --%>  
                                             
                                             
                                             <html:select property="unitHours" styleClass="textbox">
                                             <html:option value=''>HH</html:option>
                                             <% 
                                             for( int i=0;i<24;i++){
                                            	if(i<10){	 %>
                                            	<html:option value='<%= StringUtils.i2s(i)  %>'> <%="0"+i %></html:option>
                                            	<%}else{ %>
                                             <html:option value='<%= StringUtils.i2s(i)  %>'> <%=i %></html:option>
                                             <%} }%>
                                             </html:select>
                                             <html:select property="unitMinutes" styleClass="textbox">
                                             <html:option value=''>MM</html:option>
                                             <% 
                                             for( int j=0;j<60;j++){ 
                                             if(j<10){
                                             %>
                                             <html:option value='<%=StringUtils.i2s(j)%>'> <%="0"+j %></html:option>
                                             <%}else{ %>
                                             <html:option value='<%=StringUtils.i2s(j)%>'> <%=j %></html:option>
                                             <%}} %>
                                             </html:select>
                                        </td>
									</tr>
									<tr>	
										<td class="tablelabel">Comments</td>
										<td class="tabletext" colspan="3"><html:textarea property="comments" cols="75" rows="7" styleClass="textbox" /></td>
										<td class="tablelabel">Email</td>
										<td class="tabletext" colspan="2"><html:checkbox property = "emailflag"></html:checkbox></td>
										
									</tr>
								</table>
								</td>
							</tr>
							<tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="199%" class="tabletitle"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=planCheckId%>&actId=<%=activityId%>&level=F&fromWhere=<%=fromWhere%>" class="hdrs"><font class="con_hdr_2b">Attachments</font></a></nobr></td>
                                <td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=planCheckId%>&actId=<%=activityId%>&level=F&fromWhere=<%=fromWhere%>" class="hdrs"><font class="con_hdr_link">Add</font><img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td bgcolor="#FFFFFF" class="tablelabel">ID</td>
                                <td bgcolor="#FFFFFF" class="tablelabel">Description</td>
                                <td bgcolor="#FFFFFF" class="tablelabel">Size</td>
                                <td bgcolor="#FFFFFF" class="tablelabel">Date</td>
                            </tr>
                            <%
        		            	String DESTINATION_SERVER_URL = "/attachments/";
		                    %>
                            <logic:iterate id="attachment" name="attachmentList" type="elms.app.common.Attachment" scope="request">
                            <tr valign="top" bgcolor="#FFFFFF">
	                            <td bgcolor="#FFFFFF" class="tabletext"><a target="_blank" href="<%=request.getContextPath()%><%=DESTINATION_SERVER_URL%><bean:write name="attachment" property="file.fileName"/>"> <bean:write name="attachment" property="file.fileName"/> </a></td>
                                <td bgcolor="#FFFFFF" class="tabletext"> <bean:write name="attachment" property="description"/> </td>
                                <td bgcolor="#FFFFFF" class="tabletext"> <bean:write name="attachment" property="file.fileSize"/> </td>
                                <td bgcolor="#FFFFFF" class="tabletext"> <bean:write name="attachment" property="file.strCreateDate"/></td>
                             
                            </tr>
                            </logic:iterate>
                        </table>
							</td>
							</tr>
							
							<tr>
											<td width="1%">
												<table width="100%" border="0" cellspacing="0"
													cellpadding="0">
													<TBODY>
														<tr>
															<td height="32">&nbsp;</td>
														</tr>
													</TBODY>
												</table>
											</td>
										</tr>
							<tr>
							
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<TBODY>
			<tr valign="top">
				<td width="100%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<TBODY>
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
										  <td width="100%" class="tabletitle"><font class="con_hdr_2b">History</font></td>
										  <!-- <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="21" height="20"></td> -->
										  <!-- <td width="1%" class="tablelabel"><nobr> -->
										 </tr>
									</table>
									</td>
								</tr>
								<tr>
									<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2"  style='table-layout:fixed'>
										<TBODY>
											<tr>
												
												<td class="tablelabel"><nobr>Plan Check Status</nobr></td>
												<td class="tablelabel">Status Date</td>
												<td class="tablelabel">Target Date</td><!--
												<td background="../images/site_bg_D7DCE2.jpg"><font
													class="con_hdr_1">Completion Date</font></td>
                                                	--><td class="tablelabel">Staff</td>
												<td class="tablelabel">Comments</td>
                                                <td class="tablelabel">Department</td>
											</tr>
											<nested:present property="planChecksHistory">
											<nested:iterate property="planChecksHistory">
												<tr valign="top">
													
													<!--<td bgcolor="#FFFFFF"><font class="con_text_1"><nested:write property="statusDesc" /></font></td>-->
													<td class="tabletext" bgcolor="#FFFFFF"><nested:write
														property="statusDesc" /></td>
													<td class="tabletext" bgcolor="#FFFFFF"><nested:write
														property="date" /></td>
													<td class="tabletext" bgcolor="#FFFFFF"><nested:write
														property="trgdate" /></td><!--
													<td bgcolor="#FFFFFF"><font class="con_text_1"><nested:write
														property="compdate" /></font></td>
													--><td class="tabletext" bgcolor="#FFFFFF"><nested:write
														property="engineerName" /></td>
													<td class="tabletext" bgcolor="#FFFFFF" style="word-wrap: break-word;"><nested:write
														property="comments" /></td>
                                                    <td class="tabletext" bgcolor="#FFFFFF"><nested:write
														property="deptDesc" /></td>
												</tr>
											</nested:iterate>
											</nested:present>
										</TBODY>
									</table> 
									</td>
								</tr>
							</TBODY>
						</table>
						</td>
					</tr>
					</TBODY>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<TBODY>
						<tr>
							<td height="32">&nbsp;</td>
						</tr>
					</TBODY>
				</table>
				</td>
			</tr>
		</TBODY>
	</table>
							</td>
							</tr>			
							
							
						</table>
						
					</tr>
				</table>
			</tr>
	</table>
		
</html:form>
<script type="text/javascript">
$("#plancheckdatepicker1").kendoDatePicker({
});
$("#targetdatepicker1").kendoDatePicker({
});
		 </script>
</body>
</html:html>


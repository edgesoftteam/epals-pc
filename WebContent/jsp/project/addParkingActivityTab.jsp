<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="elms.control.beans.ActivityForm,elms.util.*,java.text.SimpleDateFormat,java.util.*,elms.agent.*,elms.common.Constants"%>
<%@ page import="elms.util.db.*"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security"%>

<%
String contextRoot = request.getContextPath();
int count = (Integer) request.getAttribute("count");
pageContext.setAttribute("count", count);

String lncv = (String) (request.getAttribute("lncv")!=null? request.getAttribute("lncv"):"N");
request.setAttribute("lncv",lncv);

String email = (String) (request.getAttribute("email")!=null? request.getAttribute("email"):"");
String dlno = (String) (request.getAttribute("dlno")!=null? request.getAttribute("dlno"):"");
String phno = (String) (request.getAttribute("phno")!=null? request.getAttribute("phno"):"");
String name = (String) (request.getAttribute("fname")!=null? request.getAttribute("fname"):"");
String lname = (String) (request.getAttribute("lname")!=null? request.getAttribute("lname"):"");
String phone = (String) (request.getAttribute("phone")!=null? request.getAttribute("phone"):"");
String peopleId = (String) (request.getAttribute("peopleId")!=null? request.getAttribute("peopleId"):"");
String actId = (String) (request.getAttribute("actId")!=null? request.getAttribute("actId"):"");
String label = (String) (request.getAttribute("label")!=null? request.getAttribute("label"):"");
String exisitingDt = (String) (request.getAttribute("exisitingDt")!=null? request.getAttribute("exisitingDt"):"");

List peopleList = new ArrayList();
peopleList = (List) (request.getAttribute("peopleList"));

int dtcount = Integer.parseInt((String) (request.getAttribute("dtcount")!=null? request.getAttribute("dtcount"):"1"));
int dtsize =96;
System.out.println(exisitingDt);

%>
<app:checkLogon />
<html:html>
<head>
<html:base />


<title><%=Wrapper.getCityName()%> : <%=Wrapper.getPrivateLabel()%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/themes/base/jquery.ui.all.css">
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/js/jquery.alerts.js" type="text/javascript"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.core.js"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.widget.js"></script>
<script src="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/ui/jquery.ui.datepicker.js" type="text/javascript"></script>
<link rel="stylesheet" href="<%=contextRoot%>/jsp/script/jquery-ui-1.8.2.custom/development-bundle/demos/demos.css">



<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/addActivity.js"></script>

<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>




<script language="javascript">
$(function() {
	var dtc = <%=dtcount%>;
	var $mexisitingDates = new Array(<%=exisitingDt%>);


function checkAvailability(mydate){
	var $return=true;
	var $returnclass ="available";
	$checkdate = $.datepicker.formatDate('mm/dd/yy', mydate);
	for(var i = 0; i < $mexisitingDates.length; i++)	{ 
	 if($mexisitingDates[i] == $checkdate){
		$return = false;
		$returnclass= "unavailable";
		
		}
	}
	
	return [$return,$returnclass];
}


	$('.datepickerclass').live('click', function() {
    	$(this).datepicker({ minDate: -0,beforeShowDay: checkAvailability,showOn:'focus' }).focus();
	});


});

</script>
<script language="javascript">
function disableForm() {
	if (document.all || document.getElementById) {
		for (i = 0; i < document.forms[0].length; i++) {
		var formElement = document.forms[0].elements[i];
			if (true) {
				formElement.disabled = true;
			}
		}
	}
}

function fromSubProjectName()
{
    document.forms[0].action='<%=contextRoot%>/addParkingActivityTab.do?from=subProjectName';
    document.forms[0].submit();
    disableForm();
    return true;
}
function fromActivityType()
{
    document.forms[0].action='<%=contextRoot%>/addParkingActivityTab.do?from=activityType';
    document.forms[0].submit();
    disableForm();
    return true;

}

function checkDate(label,val,place){
	var strValue;
	if(val!=''){
	var dtStr=val;
	//alert(val);
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strMonth=dtStr.substring(0,pos1);
	var strDay=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
		}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	//alert(val);
	if (pos1==-1 || pos2==-1){
		alert("The date format for "+label +" should be : mm/dd/yyyy");
		place.focus();
		strValue = false;
		return strValue;
	}
	if (month<1 || month>12){
		alert("Please enter a valid month for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (strYear.length != 4 || year==0 ){
		alert("Please enter a valid 4 digit year for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	}
	strValue= true;
	return strValue;
}
function checkInt(label,val,place){
	var strValue;
		if(isNaN(val))	{
		       alert("Please enter "+label+" with Numeric value");
		       place.focus();
		       strValue= false;

	     }else {strValue= true; }  return strValue;
}
function checkString(label,val,place){
	var strValue;
	   var words = val.length;
	    if(words>254)	{
		       alert("Please enter "+label+" with less then 255 characters value");
		       place.focus();
		       strValue= false;

	     }else {strValue= true; }	    return strValue;
}



function saveCustom(count){

	 if (count>0) {

			for (i=0;i<count;i++) {

				var fieldType = document.forms[0].elements['customFieldList[' + i + '].fieldType'].value ;
				var fieldValue = document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value ;
				var fieldLabel = document.forms[0].elements['customFieldList[' + i + '].fieldLabel'].value ;
				var fieldReq = document.forms[0].elements['customFieldList[' + i + '].fieldRequired'].value ;
				var place= document.forms[0].elements['customFieldList[' + i + '].fieldValue'];

				if(fieldType == 'INTEGER'){         if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false; }
					  if(checkInt(fieldLabel,fieldValue,place)){
					 			}else { return false; }
				}

				if(fieldType == 'STRING'){ 			if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false;  }
						 	if(checkString(fieldLabel,fieldValue,place)){
				                   }else { return false; }
				}

				if(fieldType == 'DATE'){ 	         if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false;  }
				   		if(checkDate(fieldLabel,fieldValue,place)){
									}else { return false; }
				}

			} //for
			
			document.forms[0].action='<%=contextRoot%>/saveParkingActivityTab.do';
	        document.forms[0].elements['Save'].disabled=true;
	        document.forms[0].submit();
	        return true;

	 } // end count
			else {
			         return false;
				}
}
function save(){

	//lncv validations
	var lncv = '<%=lncv%>';
	if(lncv == 'Y'){
		if(document.getElementById('fname').value ==''){
			alert("Please enter the First Name.");
			document.getElementById('fname').focus();
			return false;
		}

		
		if(document.getElementById('label').value ==''){
			alert("Please enter the Vehicle Number.");
			document.getElementById('label').focus();
			return false;
		}
		if(document.getElementById('dlno').value ==''){
			alert("Please enter the DL Number.");
			document.getElementById('dlno').focus();
			return false;
		}
		if(document.getElementById('email').value !=''){
			var x=document.getElementById('email').value;
			var atpos=x.indexOf("@");
			var dotpos=x.lastIndexOf(".");
			if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
			  alert("Not a valid e-mail address");
			  document.getElementById('email').focus();
			  return false;
			}
		}
		
		
		 var pcount =0;
		 var dcount = 0;
		 var ucount =0;
		 var newlist = new Array();
		 var hash = new Object();
		 for(var i=<%=dtcount%>;i<=<%=dtsize%>;i++){
			 if(document.getElementById('lncvDate'+i).value != ''){
				 pcount++;
				 if (hash[document.getElementById('lncvDate'+i).value] != 1){
						newlist = newlist.concat(document.getElementById('lncvDate'+i).value);
						hash[document.getElementById('lncvDate'+i).value] = 1;
					}
					else { dcount++; }
			 }
		 }
		
		 
		 if(dcount >0){
			 alert("You cannot choose the same dates.Please enter a different date.");
			 return false;
		 }else if(pcount == 0){
			 alert("Please choose a date in order to proceed.");
			 return false;
		 }//else if(pcount%3 !=0){
			// alert("Please choose an entire permit block in order to proceed.");
		 //}
		
	}
	
	
	
	// lncv validation end


	   count = <%=count%>;
      if(count>0){
		saveCustom(count);
      }else{
    	   document.forms[0].action='<%=contextRoot%>/saveParkingActivityTab.do';
           document.forms[0].submit();
           disableForm();
           return true;
	  }
}

function checkAddress(){
	if(document.forms[0].elements['subProjectName'].value==255002){//hardcoded LNCV value
		document.forms[0].elements['streetNumber'].disabled=true;
		document.forms[0].elements['streetName'].disabled=true;
		var d = new Date();
		var curr_year = d.getFullYear();
		var lastDay = '12/31/'+curr_year;
		document.forms[0].elements['expirationDate'].value=lastDay;
	}
}

//lncv dates

function dispMin(){
var lncv = '<%=lncv%>';
	if(lncv == 'Y'){
		for(var i=<%=dtcount%>;i<=6;i++){
			document.getElementById('lncvdt'+i).style.display = '' ;
			document.getElementById('spcount').value = i;
		}

		for(var i=<%=dtcount%>+6;i<=<%=dtsize%>;i++){
			document.getElementById('lncvdt'+i).style.display = 'none' ;
			
		}
		
		if(<%=dtcount%>  > 6){
			var modsp = parseInt(<%=dtcount%>) +5  ;
			 $("#spcount").val(modsp);
		}
		
		
		
		
	}
}
function dispMore(){
	var spcount = document.getElementById('spcount').value;
	
	var indst = parseInt(spcount) + 1;
	var inded = parseInt(spcount) +6 ;
	var spval = parseInt(inded);
	if(parseInt(spval) > <%=dtsize%>){
		spval	=  parseInt(inded) - <%=dtsize%>;
		inded = parseInt(inded) -  parseInt(spval);
	}
	for(var i=indst;i<=inded;i++){
		document.getElementById('lncvdt'+i).style.display = '' ;
		document.getElementById('spcount').value = i;
	}
}

function searchPeople(){
	var email = document.getElementById('email').value;
	var dlno = document.getElementById('dlno').value;
	var fname = document.getElementById('lname').value;
	var lname = document.getElementById('fname').value;
	if(email != '' || dlno != '' || fname !='' || lname !=''){
		document.forms[0].action='<%=contextRoot%>/addParkingActivityTab.do?from=lncvSearch';
	    document.forms[0].submit();
	}else{
		alert("Please enter any one search criteria");
		return false;
	}
}

function check(){
	var carCode= event.keyCode;
	event.returnValue = false;
}
</script>


</head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="checkAddress();dispMin();">

<html:form name="activityForm" type="elms.control.beans.ActivityForm" action="" >
<input type="hidden" name="spcount" id="spcount"  />
<input type="hidden" name="peopleId" value="<%=peopleId%>" />
<input type="hidden" name="actId" value="<%=actId%>"  />
<input type="hidden" name="lncv" value="<%=lncv%>" />

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Add Permit<br>
					<br>
					</td>
				</tr>
				<tr>
					<td><font class="con_text_red1"><html:errors/></td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="99%" background="../images/site_bg_B7C1CB.jpg" class="tabletitle">ADD PERMIT</td>
									<td width="1%" background="../images/site_bg_B7C1CB.jpg" class="tablebutton"><nobr>
										<html:button property="Save" value=" Save " styleClass="button" onclick="return save();" />&nbsp;</nobr>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">

								<tr>

									<td class="tablelabel">Address<font class="con_text_red1">*</font></td>
									<td class="tabletext" >
										<html:text size="10" property="streetNumber" styleClass="textbox" />
										<html:select property="streetName" styleClass="textbox">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="streetList" property="streetId" labelProperty="streetName" />
										</html:select>
									</td>

									<td class="tablelabel">Sub-Project <font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top">
										<html:select styleClass="textbox" property="subProjectName" onchange="return fromSubProjectName()">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="subProjectNames" property="projectTypeId" labelProperty="description" />
										</html:select>
									</td>


								</tr>
								<tr>

									<td class="tablelabel">Activity Type <font class="con_text_red1">*</font></td>
									<td class="tabletext" valign="top">
										<html:select styleClass="textbox" property="activityType" onchange="return fromActivityType()">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="activityTypes" property="type" labelProperty="description" />
										</html:select>
									</td>

									<td class="tablelabel">Parking Zone </td>
									<td class="tabletext"  valign="top">
										<html:select property="parkingZone" styleClass="textbox">
											<html:option value="-1">Please select</html:option>
											<html:options collection="parkingZones" property="pzoneId" labelProperty="name"/>
										</html:select>
									</td>
								</tr>
								<tr>

									<td class="tablelabel">Activity Status<font class="con_text_red1">*</font></td>
									<td class="tabletext">
										<html:select property="activityStatus" styleClass="textbox">
											<html:options collection="activityStatuses" property="status" labelProperty="code" />
										</html:select>
									</td>
									<td class="tablelabel"><nobr>Expiration Date</nobr></td>
									<td class="tabletext" valign="top"><nobr> <html:text property="expirationDate" maxlength="10" styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);" /> </nobr></td>
          						</tr>


          						 <%  if(lncv.equals("Y")){   %>

	          						 <tr>
		          						 <td class="tabletext" colspan="4"> &nbsp; <br> <br>
		          						 </td>
	          						 </tr>
	          						 <tr>
										  <td class="tablelabel">Vehicle No</td>
										  <td class="tabletext" valign="top" colspan="1">
										      <html:text styleClass="textbox" property="label" styleId="label" value="<%=label%>" maxlength="25"/>
										  </td>
										  <td class="tablelabel" ><nobr>DL No</td>
										<td class="tabletext" valign="top" colspan="1"> <input type="text" name="dlno"  id="dlno" value="<%=dlno%>" class="textboxd"  /> </td>
	          						</tr>
	          						<tr>
										<td class="tablelabel" >Email</td>
										<td class="tabletext" valign="top" colspan="4" ><nobr> <input type="text" name="email" id="email" value="<%=email%>"   size="40"/> &nbsp; 
										<input type="button" name="Search" value="Search People" onclick="searchPeople();" >
										</td>

	          						</tr>
	          						<tr>
										<td class="tablelabel">Name<font class="con_text_red1">*</font></td>
										<td class="tabletext" valign="top" colspan="1"><nobr> <input type="text" name="fname"  id="fname" value="<%=name%>"  class="textboxd"  /> &nbsp; <input type="text" name="lname"  value="<%=lname%>" class="textboxd"  /></nobr></td>
										 <td class="tablelabel" ><nobr>Phone Number</td>
										<td class="tabletext" valign="top" colspan="1"> <input type="text" name="phno" id="phno" value="<%=phno%>" maxlength="10" class="textboxd"  /> </td>
								    </tr>

								    <tr>
										<td class="tablelabel">Dates</td>
										<td class="tabletext" valign="top" colspan="3"><nobr><img src="../images/add.png" alt="Add More Dates" title="Add More Dates" onclick="dispMore();"/> </nobr></td>
									  </tr>
										<% for (int i=dtcount;i<=dtsize;i++){ %>
			 									<tr id="lncvdt<%=i%>">
													<td id="lncvdt<%=i%>" class="tabletext" colspan="4"> LNCV Date <%=i%>
														<input type="text" name="lncvDate<%=i%>"  id="lncvDate<%=i%>" class="datepickerclass" onclick="check();" >
													</td>
												</tr>

										<%  } %>



          						  <%  } else {%>
	          						   <tr>
										  <td class="tablelabel">Label</td>
										  <td class="tabletext" valign="top" colspan="3">
										      <html:text styleClass="textbox" property="label" maxlength="25"/>
										  </td>
	          							</tr>

          						   <%  }%>

						  			    <%  int i=0 ;   %>
		                    <nested:iterate  property="customFieldList" >

		                              <nested:hidden property="fieldId" />
		                              <nested:hidden property="fieldType" />
		                              <nested:hidden property="fieldLabel" />
		                              <nested:hidden property="fieldRequired" />

		                          <%if(i % 2==0) {  %>   <tr class="tabletext">
									<nested:equal property="fieldType" value="DATE">
		                                  	<td class="tablelabel"><nested:write property="fieldLabel" /></td>
		                                  	<td class="tabletext" valign="top">
		                                  		<nested:text  styleClass="textbox" property="fieldValue" onfocus="showCalendarControl(this);" />
		                                  	</td>
		                                  </nested:equal>
		                                  <nested:notEqual property="fieldType" value="DATE">
		                                  	<td class="tablelabel"><nested:write property="fieldLabel" /></td>
		                                  	<td class="tabletext" valign="top"> <nested:text  styleClass="textbox" property="fieldValue" /></td>
		                                  </nested:notEqual>

		                          <% } else  {%>
		                          		<nested:equal property="fieldType" value="DATE">
		                          		<td class="tablelabel"><nested:write property="fieldLabel" /></td>
		                                  <td class="tabletext" valign="top">
		                                  	<nested:text  styleClass="textbox" property="fieldValue" onfocus="showCalendarControl(this);"/>

		                                  </td> </nested:equal>
		                                  <nested:notEqual property="fieldType" value="DATE">
		                                  <td class="tablelabel"><nested:write property="fieldLabel" /></td>
		                                  <td class="tabletext" valign="top"> <nested:text  styleClass="textbox" property="fieldValue" /></td>
		                                  </nested:notEqual> <% }%>

					             <%if(i % 2==1) {  %>     </tr>    <% } i++ ;%>

		                    </nested:iterate>
		                    <% if(count % 2==1){%>   <td class="tabletext" colspan="6" > </td> </tr>     <% } %>

							</table>
							</td>
						</tr>


						<!--  People start-->

						 <% if(peopleList.size()>1) {%>
						 <tr valign="top">
		                               <td class="tabletext" height="3px">&nbsp;</td>
		                  </tr>

						 <tr>
                        	<td background="../images/site_bg_B7C1CB.jpg">
		                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
		                            <tr valign="top">
		                                <td class="tablelabel">People Type</td>
		                                <td class="tablelabel">Name</td>
		                                <td class="tablelabel">E-Mail</td>
		                                <td class="tablelabel">Lic No</td>
		                                <td class="tablelabel">Phone</td>
		                            </tr>

		                            <logic:iterate id="people" name="peopleList" type="elms.app.people.People" scope="request" >
		                            <tr valign="top">
		                            	<td class="tabletext"><bean:write name="people" property="comments"/></td>
		                            	 <td class="tabletext"><a href='<%=contextRoot%>/addParkingActivityTab.do?from=lncvSearched&peopleId=<bean:write name="people" property="peopleId"/>&psaId=<bean:write name="people" property="activityId"/>&psaType=A&levelType=A'><bean:write name="people" property="name"/></a></td>
		                            	 <td class="tabletext"><bean:write name="people" property="emailAddress"/></td>
		                                 <td class="tabletext"><bean:write name="people" property="licenseNbr"/></td>
		                                 <td class="tabletext"><bean:write name="people" property="phoneNbr"/></td>

		                            </tr>
		                            </logic:iterate>
		                        </table>
                        </td>
                    </tr>
                    <% }%>

						<!-- People end -->
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*,elms.app.common.*,elms.util.*,elms.agent.*,elms.common.*,elms.security.*"%>

<%
	boolean general = false, parkingUser = false, publicWorksUser = false, businessLicenseUser = false, businessLicenseApproval = false, businessTaxUser = false, businessTaxApproval = false, codeInspector = false, regulatoryPermit = false;
	User userBL = (User) session.getAttribute("user");
	if (userBL.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_PUBLIC_WORKS_CODE)) {
		publicWorksUser = true;
	}
	if (userBL.getDepartment().getDepartmentCode().equalsIgnoreCase(Constants.DEPARTMENT_PARKING_CODE)) {
		parkingUser = true;
	}
	List groups = (java.util.List) userBL.getGroups();
	Iterator itr = groups.iterator();
	while (itr.hasNext()) {
		Group group = (elms.security.Group) itr.next();
		if (group.groupId == Constants.GROUPS_GENERAL)
			general = true;
		if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER)
			businessLicenseUser = true;
		if (group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL)
			businessLicenseApproval = true;
		if (group.groupId == Constants.GROUPS_BUSINESS_TAX_USER)
			businessTaxUser = true;
		if (group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL)
			businessTaxApproval = true;
		if (group.groupId == Constants.GROUPS_CODE_INSPECTOR)
			codeInspector = true;
		if (group.groupId == Constants.GROUPS_REGULATORY_PERMIT)
			regulatoryPermit = true;
	}
%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>

<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security"%>
<app:checkLogon />
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Sub-Project Manager</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%
	String contextRoot = request.getContextPath();
		String editable = (String) request.getAttribute("editable");
		if (editable == null)
			editable = "true";

		String lsoId = (String) session.getAttribute("lsoId");
		if (lsoId == null)
			lsoId = "";
		String projectId = (String) request.getAttribute("projectId");
		if (projectId == null)
			projectId = "";

		String subProjectId = (String) request.getAttribute("subProjectId");
		if (subProjectId == null)
			subProjectId = "";

		String level = "Q";

		session.setAttribute("levelId", subProjectId);
		session.setAttribute("level", level);

		java.util.List activities = (java.util.List) request.getAttribute("activities");
		java.util.Iterator activitiesIter = activities.iterator();

		java.util.List holds = (java.util.List) request.getAttribute("holds");
		if (holds == null)
			holds = new java.util.ArrayList();
		pageContext.setAttribute("holds", holds);

		java.util.List condList = (java.util.List) request.getAttribute("condList");
		if (condList == null)
			condList = new java.util.ArrayList();
		pageContext.setAttribute("condList", condList);

		String lsoAddress = (String) session.getAttribute("lsoAddress");
		if (lsoAddress == null)
			lsoAddress = "";

		String onloadAction = (String) session.getAttribute("onloadAction");
		if (onloadAction == null)
			onloadAction = "";

		int projectNameId = LookupAgent.getProjectNameId(level, StringUtils.s2i(subProjectId));
		String projectNameType = LookupAgent.getProjectNameForProjectId(projectId);
%>

</head>
<bean:define id="user" name="user" type="elms.security.User" />

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="<%=onloadAction%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><font class="con_hdr_3b">&nbsp;Sub Project Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><br>
				<br>
				</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle">Sub Project Detail</td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablebutton"><security:editable levelId="<%=subProjectId%>" levelType="Q" editProperty="allProjectAndDept">
									<security:editable levelId="<%=subProjectId%>" levelType="Q" editProperty="checkUser">
										<a href="<%=contextRoot%>/editSubProjectDetails.do?subProjectId=<%=subProjectId%>" class="tablebutton">Edit</a>
									</security:editable>
								</security:editable></td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
								<td class="tablelabel">Sub Project #</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.subProjectNumber" /></td>
								<td class="tablelabel">Sub Project Name</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.projectType.description" /></td>
							</tr>
							<tr valign="top">
								<td class="tablelabel">Type</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.subProjectType.description" /></td>
								<td class="tablelabel">Sub-Type</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.subProjectSubType.description" /></td>
							</tr>
							<tr valign="top">
								<td class="tablelabel">Description</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.description" /></td>
								<td class="tablelabel">Created By</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.createdBy.firstName" /> <bean:write name="subProject" scope="request" property="subProjectDetail.createdBy.lastName" /></td>
							</tr>
							<tr valign="top">
								<td class="tablelabel">Status</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.subProjectStatus.description" /></td>
								<td class="tablelabel">CaseLog</td>
								<td class="tabletext"><bean:write name="subProject" scope="request" property="subProjectDetail.caseLogDesc" /></td>
							</tr>
							<tr valign="top">
								<td class="tablelabel">Label</td>
								<td class="tabletext" colspan="3"><bean:write name="subProject" scope="request" property="subProjectDetail.label" /></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle">Activities</td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablebutton">
								<%
									if (LookupAgent.isPsaActive(subProjectId)) {
								%> 
								<security:editable levelId="<%=subProjectId%>" levelType="Q" editProperty="allProjectAndDept">
									<security:editable levelId="<%=subProjectId%>" levelType="Q" editProperty="checkUser">
										<nobr><a href="<%=contextRoot%>/addActivity.do?subProjectId=<%=subProjectId%>&from=Q" class="tablebutton" > Add Activity</a></nobr>
									</security:editable>
								</security:editable> <%
 	} else {
 %> <nobr>Add Activity </nobr>
								<%
									}
								%>
								</td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
								<td class="tabletext">
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td colspan="2" valign="top"><img src="../images/spacer.gif" width="1" height="7"></td>
									</tr>
									<%
										while (activitiesIter.hasNext()) {
												String activityId = "";
												String activityNumber = "";
												String activityDesc = "";
												String activityStatus = "";
												String activityTypeCode = "";
												String activityTypeDesc = "";
												String label = "";
												elms.app.project.Activity activity = (elms.app.project.Activity) activitiesIter.next();
												activityId = elms.util.StringUtils.i2s(activity.getActivityId());
												if (activity.getActivityDetail() != null) {
													activityNumber = activity.getActivityDetail().getActivityNumber();
													if (activityNumber == null)
														activityNumber = "";
													activityDesc = activity.getActivityDetail().getDescription();
													if (activityDesc == null)
														activityDesc = "";
													if (activity.getActivityDetail().getActivityType() != null) {
														activityTypeCode = activity.getActivityDetail().getActivityType().getType();
														if (activityTypeCode == null)
															activityTypeCode = "";
														activityTypeDesc = activity.getActivityDetail().getActivityType().getDescription();
														if (activityTypeDesc == null)
															activityTypeDesc = "";
														if (activity.getActivityDetail().getStatus() != null)
															activityStatus = activity.getActivityDetail().getStatus().getDescription();
														if (activityStatus == null)
															activityStatus = "";
													}
													
													label  = activity.getActivityDetail().getLabel();
													if (label == null)
														label = "";
												}
												String activityDetails = "  " + activityTypeDesc + ", " +label+ ""+ activityDesc + " ( " + activityStatus + ")";
									%>

									<tr>
										<td width="1%" valign="top"><img src="../images/site_blt_dot_000000.gif" width="21" height="10"></td>
										<td width="99%" valign="top" class="tabletext"><a href="javascript:parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=lsoId%>&psaNodeId=<%=activityId%>';"> <%=activityNumber%> </a> <%=activityDetails%></td>
									</tr>
									
									
									
									
									<%
										}
									%>
									<tr>
										<td colspan="2" valign="top"><img src="../images/spacer.gif" width="1" height="7"></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td class="tabletitle">Finance</td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
								<td class="tablelabel">
								<div align="right">Activity</div>
								</td>
								<td class="tablelabel">
								<div align="right">Plan Check</div>
								</td>
								<td class="tablelabel">
								<div align="right">Development Fee</div>
								</td>
								<td class="tablelabel">
								<div align="right">Contractor Tax</div>
								</td>
								<td class="tablelabel">
								<div align="right">Total</div>
								</td>
							</tr>
							<tr>
								<td class="tablelabel">Fees</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="permitFeeAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="planCheckFeeAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="developmentFeeAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxFeeAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="totalFeeAmt" /></div>
								</td>
							</tr>
							<tr>
								<td class="tablelabel"> Payment</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="permitPaidAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="planCheckPaidAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="developmentFeePaidAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxPaidAmt" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="totalPaidAmt" /></div>
								</td>
							</tr>
							<tr>
								<td class="tablelabel">Balance Due </td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="permitAmountDue" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="planCheckAmountDue" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="developmentFeeAmountDue" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="businessTaxAmountDue" /></div>
								</td>
								<td class="tabletext">
								<div align="right"><bean:write scope="request" name="financeSummary" property="totalAmountDue" /></div>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>

			<%
				if (projectNameId == elms.common.Constants.PROJECT_NAME_PLANNING_ID) {
			%>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listStatus.do?levelId=<%=subProjectId%>&levelType=<%=level%>&editable=<%=editable%>" class="hdrs">Status</a></td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/listStatus.do?levelId=<%=subProjectId%>&levelType=<%=level%>&editable=true" class="hdrs">Add</a></nobr></td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tablelabel" width="15%">Date</td>
								<td class="tablelabel" width="30%">Status</td>
								<td class="tablelabel" width="55%">Comment</td>
							</tr>
							<logic:iterate id="status" name="subProject" length="3" property="planningDetails.statusList" type="elms.app.planning.StatusEdit">
								<tr valign="top">
									<td class="tabletext"><bean:write name="status" property="statusDate" /></td>
									<td class="tabletext"><bean:write name="status" property="status" /></td>
									<td class="tabletext"><bean:write name="status" property="comment" /></td>
								</tr>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listResolution.do?levelId=<%=subProjectId%>&levelType=<%=level%>&ed=<%=editable%>" class="hdrs">Resolutions/Ordinances</a></td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/listResolution.do?levelId=<%=subProjectId%>&levelType=<%=level%>" class="hdrs">Add</a></nobr></td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tablelabel" width="20%">Resolution #</td>
								<td class="tablelabel" width="60%">Description</td>
								<td class="tablelabel" width="20%">Approved Date</td>
							</tr>
							<logic:iterate id="resolution" name="subProject" length="3" property="planningDetails.resolutionList" type="elms.app.planning.ResolutionEdit">
								<tr valign="top">
									<td class="tabletext"><bean:write name="resolution" property="resolutionNbr" /></td>
									<td class="tabletext"><bean:write name="resolution" property="description" /></td>
									<td class="tabletext"><bean:write name="resolution" property="approvedDate" /></td>
								</tr>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listEnvReview.do?levelId=<%=subProjectId%>&level=<%=level%>&editable=<%=editable%>" class="hdrs">Environmental Review</a></td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/listEnvReview.do?levelId=<%=subProjectId%>&level=<%=level%>&editable=true" class="hdrs">Add</a></nobr></td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tablelabel" width="20%">Date</td>
								<td class="tablelabel" width="40%">Env. Determination</td>
								<td class="tablelabel" width="40%">Action</td>
							</tr>
							<logic:iterate id="envReview" name="subProject" length="3" property="planningDetails.envReviewList" type="elms.app.planning.EnvReviewEdit">
								<tr valign="top">
									<td class="tabletext"><bean:write name="envReview" property="actionDate" /></td>
									<td class="tabletext"><bean:write name="envReview" property="envDetermination" /></td>
									<td class="tabletext"><bean:write name="envReview" property="envDeterminationAction" /></td>
								</tr>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<%
				}
			%>

			<tr>
				<td>&nbsp;</td>
			</tr>
			<jsp:include page="../includes/comments.jsp" flush="true">
				<jsp:param name="id" value="<%=subProjectId%>" />
				<jsp:param name="level" value="<%=level%>" />
				<jsp:param name="editable" value="<%=editable%>" />
			</jsp:include>

			<%
				if ((projectNameType.equals(elms.common.Constants.PROJECT_NAME_PLANNING)) && (user.getDepartment().getDepartmentCode().equalsIgnoreCase("PL"))) {
			%>
			<tr>
				<td>&nbsp;</td>
			</tr>

			<jsp:include page="../includes/calendarMiniPanel.jsp" flush="true">
				<jsp:param name="id" value="<%=subProjectId%>" />
				<jsp:param name="level" value="<%=level%>" />
				<jsp:param name="editable" value="<%=editable%>" />
			</jsp:include>

			<%
				}
			%>

			<tr>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>

								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=subProjectId%>&level=<%=level%>" class="tabletitle">Attachments</a></td>
								<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=subProjectId%>&level=<%=level%>" class="tablebutton">Add</a></nobr></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
								<td class="tablelabel">File Name</td>
								<td class="tablelabel">Description</td>
								<td class="tablelabel">Size</td>
								<td class="tablelabel">Date</td>
							</tr>
							<%
								int i = 0;
							%>
							<logic:iterate id="attachment" name="attachmentList" type="elms.app.common.Attachment" scope="request">
								<%
									List attachmentsList = (List) request.getAttribute("attachmentList");
											Attachment attachmentObj = new Attachment();
											String fileNameStr = "";

											try {
												attachmentObj = (Attachment) attachmentsList.get(i);
												fileNameStr = attachmentObj.file.getFileName();
												fileNameStr = java.net.URLDecoder.decode(fileNameStr, "UTF-8");
											} catch (Exception e) {
											}
								%>
								<tr valign="top" class="tabletext">
									<td><%=fileNameStr%></td>
									<td><bean:write name="attachment" property="description" /></td>
									<td><bean:write name="attachment" property="file.fileSize" /></td>
									<td><bean:write name="attachment" property="file.strCreateDate" /></td>
								</tr>
								<%
									i++;
								%>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>

		</table>
		</td>
		<td width="1%">
		<table width="200" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="32">&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=<%=subProjectId%>&level=<%=level%>&ed=<%=editable%>" class="tabletitle">Holds</a></td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addHold.do?levelId=<%=subProjectId%>&level=<%=level%>" class="tablebutton">Add</a></nobr></td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<logic:iterate id="hold" name="holds">
								<tr valign="top" class="tabletext">
									<td><bean:write name="hold" property="type" /></td>
									<td><bean:write name="hold" property="title" /></td>
								</tr>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=subProjectId%>&conditionLevel=<%=level%>&ed=<%=editable%>" class="tabletitle">Conditions</a></td>
								<%
									if (editable.equals("true")) {
								%>
								<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addCondition.do?levelId=<%=subProjectId%>&conditionLevel=<%=level%>" class="tablebutton">Add</a></nobr></td>
								<%
									}
								%>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<logic:iterate id="cond" name="condList" type="elms.app.common.Condition">
								<tr valign="top">
									<td class="tablelabel"><bean:write name="cond" property="conditionId" /></td>
									<td class="tablebutton"><bean:write name="cond" property="shortText" /></td>
								</tr>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle">Site Data</td>
								<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=<%=subProjectId%>&levelType=Q&lsoId=<%=lsoId%>" class="tablebutton">View</a></nobr></td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>

			<%
				if (projectNameId == elms.common.Constants.PROJECT_NAME_PLANNING_ID) {
			%>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listLinkSubProject.do?subProjectId=<%=subProjectId%>&editable=<%=editable%>" class="tabletitle">Linked Sub-Projects</a></td>
							</tr>
						</table>
						</td>
					</tr>
					<tr>
						<td background="../images/site_bg_B7C1CB.jpg">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<logic:iterate id="subProject" name="subProject" property="planningDetails.linkedSubProjects" type="elms.app.planning.LinkSubProjectEdit">
								<tr valign="top">
									<td class="tabletext"><bean:write name="subProject" property="subProjectNbr" /></td>
									<td class="tabletext"><bean:write name="subProject" property="description" /></td>
								</tr>
							</logic:iterate>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<%
				}
			%>

			<tr>
				<td>&nbsp;</td>
			</tr>
			<jsp:include page="../includes/activityTeamList.jsp" flush="true">
				<jsp:param name="editable" value="<%=editable%>" />
				<jsp:param name="id" value="<%=subProjectId%>" />
			</jsp:include>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<jsp:include page="../includes/viewPeopleManagerMini.jsp" flush="true">
				<jsp:param name="editable" value="<%=editable%>" />
				<jsp:param name="id" value="<%=subProjectId%>" />
			</jsp:include>
			</td>
			</tr>
		</table>
</body>
</html:html>



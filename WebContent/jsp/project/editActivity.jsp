<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ page import="elms.control.beans.ActivityForm,elms.util.*,java.text.SimpleDateFormat,java.util.*"%>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>


<%@ page import='elms.app.admin.CustomField' %>
<script language="JavaScript" src="../script/editActivity.js"></script>


<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>

</head>
<%
String contextRoot = request.getContextPath();
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";


 ActivityForm activityForm = (ActivityForm)request.getAttribute("activityForm");

if(activityForm == null){
  activityForm =  (ActivityForm)session.getAttribute("activityForm");
}

String levelId = "";
String levelType = "";
if(activityForm != null){
  levelId = activityForm.getActivityId();
  levelType = "A";

  if(levelId == null || levelId == "0"){
  System.out.println("activityForm.getSubProjectId(): "+activityForm.getSubProjectId());
    levelId = activityForm.getSubProjectId();
    levelType = "Q";
  }
}

Calendar cal = Calendar.getInstance();
 //String a=cal.getTime()+"";
 String currentDate= StringUtils.cal2str(Calendar.getInstance());
  SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
 String strCalendar = formatter.format(cal.getTime());

%>

<SCRIPT language="javascript" type="text/javascript">


var count = 0;
function save()
{
if(document.forms[0].address.value == ""){
	alert("Please select address");
	document.forms[0].address.focus();
	return false;
	}
if(document.forms[0].activityStatus.value == ""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	return false;
	}
}
function checkDate(label,val,place){
	var strValue;
	if(val!=''){
	var dtStr=val;
	//alert(val);
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strMonth=dtStr.substring(0,pos1);
	var strDay=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
		}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	//alert(objValue);
	if (pos1==-1 || pos2==-1){
		alert("The date format for "+label +" should be : mm/dd/yyyy");
		place.focus();
		strValue = false;
		return strValue;
	}
	if (month<1 || month>12){
		alert("Please enter a valid month for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (strYear.length != 4 || year==0 ){
		alert("Please enter a valid 4 digit year for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date for "+label +"");
		place.focus();
		strValue= false;
		return strValue;
	}
	}
	strValue= true;
	return strValue;
}
function checkInt(label,val,place){
	var strValue;
		if(isNaN(val))	{
		       alert("Please enter "+label+" with Numeric value");
		       place.focus();
		       strValue= false;

	     }else {strValue= true; }   return strValue;
}
function checkString(label,val,place){
	var strValue;
	   var words = val.length;

	    if(words>254)	{
		       alert("Please enter "+label+" with less then 255 characters value");
		       place.focus();
		       strValue= false;

	     }else {strValue= true; }   return strValue;
}
function customValidation(){

	for (i=0;i<count;i++) {
			var fieldType = document.getElementById('customFieldList[' + i + '].fieldType').value ;
			var fieldValue = document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value ;
			var fieldLabel = document.forms[0].elements['customFieldList[' + i + '].fieldLabel'].value ;
			var fieldReq = document.forms[0].elements['customFieldList[' + i + '].fieldRequired'].value ;
			var place= document.forms[0].elements['customFieldList[' + i + '].fieldValue'];

			if(fieldType == 'INTEGER'){         
	 			if(fieldReq=='Y'  && fieldValue=='')
	 	 			{   
	 	 				alert(fieldLabel+" is a requred field"); place.focus(); 
	 	 					return false; 
	 	 			}
				if(!checkInt(fieldLabel,fieldValue,place)){
	 				return false; 
	 				}
			} 

			if(fieldType == 'STRING'){
	 			if(fieldReq=='Y'  && fieldValue==''){   
	 	 				alert(fieldLabel+" is a requred field"); 
	 	 				place.focus(); 
	 	 				return false;  
	 	 		}
				if(!checkString(fieldLabel,fieldValue,place))
					return false; 
			}

			if(fieldType == 'DATE'){
	 			if(fieldReq=='Y'  && fieldValue==''){   
	 	 				alert(fieldLabel+" is a requred field"); 
	 	 				place.focus(); 
	 	 				return false;  
	 	 		}
			   	if(fieldValue != '' && !checkDate(fieldLabel,fieldValue,place)){ 
	 			   	return false; 
	 			}
			}
			if(fieldType == 'CHECKBOX'){
 			if(document.forms[0].elements['customFieldList[' + i + '].fieldValue'].checked){
 			document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value = 'Y';
	 		}else{
	 		document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value = 'N';
		 	}
 		}
	}  
			return true;
	}
	

function saveCustom(count){

  if (validateFunction()) {
		for (i=0;i<count;i++) {

			var fieldType = document.forms[0].elements['customFieldList[' + i + '].fieldType'].value ;
			var fieldValue = document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value ;
			var fieldLabel = document.forms[0].elements['customFieldList[' + i + '].fieldLabel'].value ;
			var fieldReq = document.forms[0].elements['customFieldList[' + i + '].fieldRequired'].value ;
			var place= document.forms[0].elements['customFieldList[' + i + '].fieldValue'];

			if(fieldType == 'INTEGER'){         if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false; }
				  if(checkInt(fieldLabel,fieldValue,place)){
				 			}else { return false; }
			}

			if(fieldType == 'STRING'){ 			if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false;  }
					 	if(checkString(fieldLabel,fieldValue,place)){
			                   }else { return false; }
			}

			if(fieldType == 'DATE'){ 	         if(fieldReq=='Y'  && fieldValue==''){   alert(fieldLabel+" is a required field"); place.focus(); return false;  }
			   		if(checkDate(fieldLabel,fieldValue,place)){
								}else { return false; }
			}
			if(fieldType == 'CHECKBOX'){
		 		if(document.forms[0].elements['customFieldList[' + i + '].fieldValue'].checked){
		 			document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value = 'Y';
			 		}	else{
			 		document.forms[0].elements['customFieldList[' + i + '].fieldValue'].value = '';
				 }
		 		}

		} //for

		 document.forms[0].action='<%=contextRoot%>/saveActivityDetails.do';
		 document.forms[0].submit();
		 return true;

 } // end validate
	   	else {
			  return false;
			}

}

function update() {

	count = <%=activityForm.getCustomFieldList().length%>;
	if(count>0){
		saveCustom(count);
	} else  if (validateFunction()) {
				 document.forms[0].action='<%=contextRoot%>/saveActivityDetails.do';
		         document.forms[0].submit();
		         return true;
		    } else {
		         return false;
		}
}
</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form action="/saveActivityDetails" >
<html:hidden property="activityId"/>
<html:hidden property="activityType" />
<html:hidden property="oldActivityStatus" />
<html:hidden property="sixMonthsLaterDate" />
<html:hidden property="oldExpirationDate" />
<html:hidden property="outStandingFees" />
<html:hidden property="oldIssueDate" />
<html:hidden property="sixMonthsOfIssueDate" />
<html:hidden property="displayContent"/>
<html:hidden property="completionDateLabel"/>
<html:hidden property="expirationDateLabel"/>
<input type="hidden" name="dateChange" value="<%=strCalendar%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <font class="con_hdr_3b">Edit Activity&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Activity Manager </td>
                      <td width="1%" class="tablebutton"><nobr>
                          <html:reset  value="Back" styleClass="button" onclick="history.back();" />
                           <security:editable levelId="<%=levelId%>" levelType="<%=levelType%>" editProperty="checkUser">
                        <html:button property="Update" value="Update" styleClass="button" onclick="return update();"/>
                           </security:editable>
                        &nbsp;</nobr></td>

                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                     <jsp:include page="../includes/editActivityFields.jsp" flush="true">
                        <jsp:param name="thisForm" value="activityForm"/>
                     </jsp:include>

                    <%  int i=0 ;   %>
                    <nested:iterate  property="customFieldList" >

						<nested:hidden property="fieldId" />
						<nested:hidden property="fieldType" />
						<nested:hidden property="fieldLabel" />
						<nested:hidden property="fieldRequired" />
						<%	
							CustomField[] customFieldsArray =  activityForm.getCustomFieldList(); 
							String style = "";
							if(customFieldsArray[i].getFieldRequired().equals("Y")) {
								style = "requiredtext";
							}
							else {
								style = "textbox";
							}
						%>
                        <%if(i % 2==0) {  %>
                        <tr>
                        	<td class="tablelabel" background="../images/site_bg_D7DCE2.jpg"><nested:write property="fieldLabel" /></td>
						    <td  class="tabletext" bgcolor="#FFFFFF" valign="top"> 
						    <%if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DROPDOWN")){
						     String attributeName = "dropdownOptionList" + customFieldsArray[i].getFieldId();
						     %>
						     <nested:select styleClass="<%=style%>" property="fieldValue">
								<html:option value="">Please Select</html:option>
								<html:options collection="<%= attributeName %>" property="id" labelProperty="label" />
							</nested:select> <%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("CHECKBOX")){%>
							<nested:checkbox styleClass="<%=style%>" property="fieldValue" value="Y"></nested:checkbox>
							<%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DATE")){%>
							<nested:text onfocus="showCalendarControl(this);" styleClass="<%=style%>" property="fieldValue" />
							</td>
							<%}else{%>
							<nested:text styleClass="<%=style%>" property="fieldValue" />
							<%}%>
							</td>
	
                         <% } else  {%>
                          <td class="tablelabel" background="../images/site_bg_D7DCE2.jpg"><nested:write property="fieldLabel" /></td>
						  <td class="tabletext" bgcolor="#FFFFFF" valign="top">						
								<%if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DROPDOWN")){
								String attributeName = "dropdownOptionList" + customFieldsArray[i].getFieldId();
								%>
								<nested:select styleClass="<%=style%>" property="fieldValue">
								<html:option value="">Please Select</html:option>
								<html:options collection="<%= attributeName %>" property="id" labelProperty="label" />
								</nested:select>
								<%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("CHECKBOX")){%>
								<nested:checkbox styleClass="<%=style%>" property="fieldValue" value="Y"></nested:checkbox>
								<%}else if(customFieldsArray[i].getFieldType() != null && customFieldsArray[i].getFieldType().equalsIgnoreCase("DATE")){%>
								<nested:text onfocus="showCalendarControl(this);" styleClass="<%=style%>" property="fieldValue" />
								</td>
								<%} else{%>
								<nested:text styleClass="<%=style%>" property="fieldValue" maxlength="256" />
								<%}%>
								
								</td>
                         <% }%>
                         <%if(i % 2==1) {  %>     </tr>    <% } i++ ;%>
                    </nested:iterate>
                    <% if(activityForm.getCustomFieldList().length % 2==1){%>
                    		<td class="tabletitle" colspan="6" > </td> </tr>
                    <% } %>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
    <td width="1%">
    </td>
  </tr>
</table>

<script>count = <%= i %>;</script>
</html:form>
</html:html> 





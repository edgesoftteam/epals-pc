<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath(); %>
<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg">

<table width="108%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td class="frametitle" style="padding-left: 15px">Inspections Explorer</td>
  </tr>
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table border="0" cellspacing="10" cellpadding="0">
              <tr>
                <td valign="top"><a href="<%=contextRoot%>/viewAllInspection.do" class="treeuser-multi" target="_top">All Inspectors</a></td>
                <td>&nbsp;</td>
              </tr>
            
            <logic:iterate id="insp" name="viewAllInspectionForm" property="inspector" type="elms.app.inspection.Inspector">
              <tr>
                <td valign="top"><a href="<%=contextRoot%>/inspectionRoutingAction.do?inspectorId=<%=insp.getInspectorId()%>" class="treeuser" target="f_content"><%=insp.getName()%></a></td>
                <td class="treelabel"><%=insp.getCount()%></td>
              </tr>
              </logic:iterate>

            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html:html>

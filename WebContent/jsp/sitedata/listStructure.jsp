<%@ page import="elms.agent.*,elms.control.beans.sitedata.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon />

<html:html>
	<head>
		<html:base />
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="../css/elms.css" type="text/css">
	</head>
	<bean:define id="user" name="user" type="elms.security.User"/>


<%
	String contextRoot = request.getContextPath();
	int rows = 0;
   	String psaInfo = "",lsoAddress = "";

   try {
		lsoAddress = (String)session.getAttribute("lsoAddress");
   		if (lsoAddress == null ) lsoAddress = "";

   		psaInfo = (String) session.getAttribute("psaInfo");
   		if (psaInfo == null ) psaInfo = "";
   	} catch (Exception e) {
   	}

   	SiteStructureForm siteStructureForm = (SiteStructureForm)session.getAttribute("siteStructureForm");

	String levelType = siteStructureForm.getLevelType()!=null?siteStructureForm.getLevelType():"";
	String levelId = ""+siteStructureForm.getLevelId();

%>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="JavaScript">
var strValue;

function setbackChange() {
	document.forms['siteStructureForm'].elements['setback.changed'].value = true;
}

function validateFunction()
{
    return true;
}

function addPage() {
	document.forms['siteStructureForm'].action='<%=contextRoot%>/viewSiteStructure.do?action=add';
	document.forms['siteStructureForm'].submit();
}

function getActiveList(val){

	   document.forms[0].action= "<%=contextRoot%>/viewSiteStructure.do?levelType=<%=levelType%>&levelId=<%=levelId%>&lsoId=<%=levelId%>&activeSite="+val;
	   document.forms[0].submit();
	}

function getUrl(levelType,levelId,lsoId,structureId){

	var val="";
	for (var i=0; i < document.siteStructureForm.activeSite.length; i++){
	   if (document.siteStructureForm.activeSite[i].checked){
		   val= document.siteStructureForm.activeSite[i].value;
	      }
	  }
	  document.forms[0].action= "<%=contextRoot%>/viewSiteStructure.do?action=edit&levelType="+levelType+"&levelId="+levelId+"&lsoId="+lsoId+"&structureId="+structureId+"&activeSite="+val;
	  document.forms[0].submit();
}

</script>


<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/saveSiteStructure.do?action=Add">
<html:hidden property="lsoId" />
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		    <tr>
		        <td><font class="con_hdr_3b">Site Structure List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
		       </td>
		    </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Site Data Structure
									[
										Active<html:radio property="activeSite" value="Y" onclick="getActiveList(this.value);" /> &nbsp;
										 All<html:radio property="activeSite" value="N" onclick="getActiveList(this.value);" />
									]
                                </td>
                                <td width="1%" class="tablebutton"><nobr>
			                    	<html:button property="Cancel" value=" Back " styleClass="button" onclick="history.back();" />
			                    	<html:button property="Add" value=" Add " styleClass="button" onclick="return addPage();" />
                               	</td>
                            </tr>
                        </table>
                        </td>
                    </tr>

                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
	                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
								<logic:iterate id="structure" name="siteStructureForm" property="structureList" scope="session">
									<% rows++; %>

	                             <tr>
	                                <td class="tablelabel" width="50"><a href="javascript:getUrl('<bean:write name="siteStructureForm" property="levelType" />',<bean:write name="siteStructureForm" property="levelId" />,<bean:write name="structure" property="lsoId" />,<bean:write name="structure" property="structureId" />); "><%= rows %></a></td>
	                                <td class="tabletext">
										<a href="javascript:getUrl('<bean:write name="siteStructureForm" property="levelType" />',<bean:write name="siteStructureForm" property="levelId" />,<bean:write name="structure" property="lsoId" />,<bean:write name="structure" property="structureId" />); "><bean:write name="structure" property="buildingName" /></a>
	                                </td>
	                            </tr>
			                    </logic:iterate>

	                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>

<%@ page import="elms.agent.*,elms.control.beans.sitedata.*,elms.app.sitedata.*,java.util.*,elms.util.*,org.apache.commons.validator.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon />
<html:html>
	<head>
		<html:base />
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="../css/elms.css" type="text/css">
	</head>
	<bean:define id="user" name="user" type="elms.security.User"/>

<%
	String contextRoot = request.getContextPath();
	String flag = (String) request.getAttribute("flag");
	int rows = 0;
   	String psaInfo = "",lsoAddress = "";
   	try {
		lsoAddress = (String)session.getAttribute("lsoAddress");
   		if (lsoAddress == null ) lsoAddress = "";

   		psaInfo = (String) session.getAttribute("psaInfo");
   		if (psaInfo == null ) psaInfo = "";
   	}
   	catch (Exception e) {
   	}

   	SiteStructureForm siteStructureForm = (SiteStructureForm)session.getAttribute("siteStructureForm");

	String projectName="";
	String levelType = siteStructureForm.getLevelType()!=null?siteStructureForm.getLevelType():"";
	String levelId = ""+siteStructureForm.getLevelId();
	if(levelType.equalsIgnoreCase("A")){
		projectName = LookupAgent.getProjectNameForActivityNumber(LookupAgent.getActivityNumberForActivityId(levelId));
	}
	List siteZoneList = (List) LookupAgent.getSiteZoneList();
	request.setAttribute("siteZoneList",siteZoneList);
	List siteUseList = (List) LookupAgent.getSiteUseList();
	request.setAttribute("siteUseList",siteUseList);
	List siteLocationList = (List) LookupAgent.getSiteLocationList();
	request.setAttribute("siteLocationList",siteLocationList);
	List siteRoofList = (List) LookupAgent.getSiteRoofList();
	request.setAttribute("siteRoofList",siteRoofList);
	List siteConstTypeList = (List) LookupAgent.getSiteConstTypeList();
	request.setAttribute("siteConstTypeList",siteConstTypeList);
	List parkingZones = (List) LookupAgent.getParkingZones();
	request.setAttribute("parkingZones",parkingZones);
%>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="javascript" src="../script/actb.js"></script>
<script language="javascript" src="../script/common.js"></script>
<script language="JavaScript">
var strValue;

function setbackChange() {
	document.forms['siteStructureForm'].elements['setback.changed'].value = true;
}

function validateFunction()
{
    return true;
}

function savePage(){

	var active="";
	if(document.forms[0].elements('siteStructure.active').checked == true){ 	active="Y"; }
	if(document.forms[0].elements('siteStructure.active').checked == false){    active="N"; }

	if (validateFunction()){
		document.forms['siteStructureForm'].action='<%=contextRoot%>/saveSiteStructure.do?action=Save&active='+active;
		document.forms['siteStructureForm'].submit();
		document.forms['siteStructureForm'].elements['Save'].value='Updating...';
		document.forms['siteStructureForm'].elements['Save'].disabled=true;
 	}else
 		return false;
}


function cancelPage() {
	document.forms[0].action='<%=contextRoot%>/saveSiteStructure.do?action=Cancel';
 	document.forms[0].submit();
}

function validateDecimal(evt){
	var charCode = (evt.which) ? evt.which : evt.keyCode
         if (charCode > 31 && ( charCode < 48 || charCode > 57))
            return false;

         return true;

}

</script>

<html:form action="/saveSiteStructure.do?action=Save">
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        		<tr>
		        	<td>
		        		<font class="con_hdr_3b">View Site Structure History&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
       				</td>
    			</tr>
    			<tr>
					<td>
						<table cellpadding="0" cellspacing="0" border="0" width="100%">
						    <tr>
						        <td colspan="2">
			            			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			                			<tr>
						                    <td class="tabletitle" width="70%">Site Data Structure</td>
			                			</tr>
			            			</table>
			        			</td>
			    			</tr>

						    <tr>
						        <td background="../images/site_bg_B7C1CB.jpg" valign="top" width="50%" >
						            <table cellpadding="2" cellspacing="1" border="0" width="100%">
						                <tr>
						                    <td height="30" width="25%" nowrap align="right" class="tablelabel" >Structure Name</td>
						                    <td class="tabletext"><html:text property="siteStructure.buildingName" styleClass="textbox" size="30" maxlength="40" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Building Use</td>
						                    <td class="tabletext" height="30" nowrap ><html:text property="siteStructure.buildingUse" styleClass="textbox" size="30" maxlength="30" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Const. Type</td>
						                    <td height="30" nowrap class="tabletext">
						                    	<html:select property="siteStructure.constructionType" styleClass="textbox">
												  <option value=""></option>
												  <html:options collection="siteConstTypeList" property="description" labelProperty="description" />
												</html:select>
											</td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed Height Ft/ In</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.heightFeet" styleClass="textbox" size="4" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						               </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual Height Ft / In</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.addHeightFeet" styleClass="textbox" size="4" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						               </tr>
						               <tr>
						                    <td height="30" nowrap align="right" class="tablelabel"># of Horses</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.heightInches" styleClass="textbox" size="2" maxlength="2" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Sprinklered</td>
						                    <td height="30" nowrap class="tabletext">
						                    	<html:select property="siteStructure.sprinklered" styleClass="textbox">
												  <option value="Y">Y</option>
												  <option value="N">N</option>
												</html:select>
						                    </td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Lot Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.lotArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Pad Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.padArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed Garage Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.garageArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual Garage Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualGarageArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Slope Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.slopeArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed Basement Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.basementArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual Basement Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualBasementArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed Number of Dwellings</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.dwellingUnits" styleClass="textbox" size="4" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual Number of Dwellings</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualDwellingUnits" styleClass="textbox" size="4" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed Bedrooms</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.bedrooms" styleClass="textbox" size="3" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual Bedrooms</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualBedrooms" styleClass="textbox" size="3" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Module Number</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.moduleNumber" styleClass="textbox full-size" size="3" maxlength="30" /></td>
						                </tr> <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Solar kw Size (DC)</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.kwSize" styleClass="textbox full-size" size="3" maxlength="30"/></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Battery Included</td>
						                    <td height="30" nowrap class="tabletext">
						                    	<html:select  property="siteStructure.batteryIncluded" styleClass="textbox full-size">
								  					<html:option value=""></html:option>
								                	<html:option value="Y">Yes</html:option>
								                	<html:option value="N">No</html:option>
								                </html:select>
						                   </td>
						                </tr>
						            </table>
						        </td>
						        <td background="../images/site_bg_B7C1CB.jpg" valign="top" width="50%">
						            <table cellpadding="2" cellspacing="1" border="0">
						                <tr>
						                    <td height="30" nowrap width="25%" align="right" class="tablelabel">Building Type</td>
						                    <td height="30" nowrap width="75%" class="tabletext">
						                    	<html:select property="siteStructure.buildingType" styleClass="textbox">
						                    	  <option value=""></option>
												  <html:options collection="buildingTypeList" property="id" labelProperty="description" />
						                    	</html:select>
						                    </td>

						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">OCC Group</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.occupancyGroup" styleClass="textbox" size="30" maxlength="30" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed # of Stories</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.stories" styleClass="textbox" size="3" maxlength="3" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual # of Stories</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualStories" styleClass="textbox" size="3" maxlength="3" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed # of Basement Levels</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.basementLevels" styleClass="textbox" size="2" maxlength="2" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual # of Basement Levels</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualBasementLevels" styleClass="textbox" size="2" maxlength="2" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Roofing</td>
						                    <td height="30" nowrap class="tabletext">
						                    	<html:select property="siteStructure.roofing" styleClass="textbox">
												  <option value=""></option>
												  <html:options collection="siteRoofList" property="description" labelProperty="description" />
						                    	</html:select>
						                    </td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Zoning</td>
						                    <td height="30" nowrap class="tabletext">
						                        <html:select property="siteStructure.zoning" styleClass="textbox">
												  <option value="">Please Select</option>
												  <html:options collection="siteZoneList" property="id" labelProperty="description" />
						                    	</html:select>
						                    </td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Location</td>
						                    <td height="30" nowrap class="tabletext">
						                    	<html:select property="siteStructure.location" styleClass="textbox">
												  <option value=""></option>
												  <html:options collection="siteLocationList" property="code" labelProperty="description" />
						                    	</html:select>
						                    </td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed F.A.R.</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.allowedFar" styleClass="textbox" size="5" maxlength="5" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual F.A.R.</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualFar" styleClass="textbox" size="5" maxlength="5" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Exist Bldg Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.buildingArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Allowed Addl or New Bldg Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.newBuildingArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Actual Addl or New Bldg Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.actualNewBuildingArea" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Demo Bldg Area</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.floorAreaAdded" styleClass="textbox" size="6" maxlength="6" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Required Parking</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.requiredParking" styleClass="textbox" size="4" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Provided Parking</td>
						                    <td height="30" nowrap class="tabletext"><html:text property="siteStructure.providedParking" styleClass="textbox" size="4" maxlength="4" onkeypress="return validateDecimal(event);" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Active</td>
						                    <td height="30"  class="tabletext">

						                   <html:checkbox property="siteStructure.active" /></td>
						                </tr>
						                <tr>
						                    <td height="30" nowrap align="right" class="tablelabel">Conf Number</td>
						                    <td height="30"  class="tabletext">
											<html:text property="siteStructure.confNumber" styleClass="textbox full-size" size="4" maxlength="30"/>
											</td>
						                </tr>
						                <tr>
						                     <td height="30" nowrap align="right" class="tablelabel">Meter Spot Date</td>
						                    <td height="30"  class="tabletext">
											<html:text  property="siteStructure.meterSpotDate" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
											</td>
						                </tr>
						                <tr>
						                     <td height="30" nowrap align="right" class="tabletext" colspan="2">&nbsp;</td>
						                </tr>
						            </table>
						        </td>
						    </tr>

						    <tr>
						        <td background="../images/site_bg_B7C1CB.jpg" width="50%">
									<table cellpadding="2" cellspacing="1" border="0" width="100%">
							        	<html:hidden  property="setback.changed" />
							            <tr>
							                <td class="tablelabel" valign="top" >Setback</td>
							                <td class="tablelabel" valign="top" >Feet</td>
							                <td class="tablelabel" valign="top" >Inches</td>
							                <td class="tablelabel" valign="top" >Dir</td>
							                <td class="tablelabel" valign="top" >Comments</td>
							            </tr>
										<tr>
											<td class="tablelabel" valign="top">Front Setback</td>
											<td class="tabletext" valign="top"><html:text  property="setback.frontFt" onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="6"  maxlength="6" styleClass="textbox"/></td>
											<td class="tabletext" valign="top"><html:text  property="setback.frontIn" onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="2" maxlength="2" styleClass="textbox"/></td>
											<td class="tabletext" valign="top"></td>
											<td class="tabletext" valign="top"><html:text  property="setback.frontComment" onchange="setbackChange();" size="25" maxlength="50" styleClass="textbox"/></td>
										</tr>
										<tr>
											<td class="tablelabel" valign="top">Rear Setback</td>
											<td class="tabletext" valign="top"><html:text  property="setback.rearFt" onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="6" maxlength="6" styleClass="textbox"/></td>
											<td class="tabletext" valign="top"><html:text  property="setback.rearIn" onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="2" maxlength="2" styleClass="textbox"/></td>
											<td class="tabletext" valign="top"></td>
											<td class="tabletext" valign="top"><html:text  property="setback.rearComment" onchange="setbackChange();" size="25" maxlength="50" styleClass="textbox"/></td>
										</tr>
										<tr>
											<td class="tablelabel" valign="top">Side 1 Setback</td>
											<td class="tabletext" valign="top"><html:text  property="setback.side1Ft"  onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="6" maxlength="6" styleClass="textbox"/></td>
											<td class="tabletext" valign="top"><html:text  property="setback.side1In"  onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="2" maxlength="2" styleClass="textbox"/></td>
											<td class="tabletext" valign="top">
												<html:select  property="setback.side1Dir" onchange="setbackChange();" styleClass="textbox">
								  					<html:option value=""></html:option>
								                	<html:option value="N">N</html:option>
								                	<html:option value="S">S</html:option>
								                	<html:option value="E">E</html:option>
								                	<html:option value="W">W</html:option>
								                </html:select>
								                </td>
								            <td class="tabletext" valign="top"><html:text  property="setback.side1Comment"  onchange="setbackChange();" size="25" maxlength="50" styleClass="textbox" /></td>
										</tr>
										<tr>
											<td class="tablelabel" valign="top">Side 2 Setback</td>
											<td class="tabletext" valign="top"><html:text  property="setback.side2Ft"  onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="6" maxlength="6" styleClass="textbox"/></td>
											<td class="tabletext" valign="top"><html:text  property="setback.side2In"  onkeypress="return validateDecimal(event);" onchange="setbackChange();" size="2" maxlength="2" styleClass="textbox"/></td>
											<td class="tabletext" valign="top">
												<html:select  property="setback.side2Dir" onchange="setbackChange();" styleClass="textbox">
								  					<html:option value=""></html:option>
								                	<html:option value="N">N</html:option>
								                	<html:option value="S">S</html:option>
								                	<html:option value="E">E</html:option>
								                	<html:option value="W">W</html:option>
								                </html:select>
											</td>
											<td class="tabletext" valign="top"><html:text  property="setback.side2Comment" onchange="setbackChange();" size="25" maxlength="50" styleClass="textbox"/></td>
										</tr>

									</table>


						        </td>
						        <td background="../images/site_bg_B7C1CB.jpg" width="50%">
							        <table cellpadding="2" cellspacing="1" border="0" width="100%">
						                <tr>
						                    <td class="tablelabel" colspan="2">
						                    	Comments
						                    </td>
						                </tr>
						                <tr>
						                    <td class="tabletext">
						                    	<html:textarea property="siteStructure.comment" cols="30" rows="5" />
						                    </td>
						                </tr>
						            </table>
						        </td>

						    </tr>
						</table>

						<table>
				            <tr>
				                <td colspan="2">&nbsp;</td>
				            </tr>
						</table>


					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</body>
<html:hidden property="structureId" />
<html:hidden property="lsoId" />
<html:hidden property="levelId" />
<html:hidden property="levelType" />

</html:form>
</html:html>

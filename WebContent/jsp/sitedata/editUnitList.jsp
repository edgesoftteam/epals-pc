<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<app:checkLogon />

<%@ page import="elms.app.sitedata.*"%>
<%@ page import="java.util.*"%>
<%@ page import="elms.util.*"%>
<%
	String action = (String) request.getAttribute("action");
%>
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Fees Maintenance: Edit Fee Schedule</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String contextRoot = request.getContextPath();
		int rows = 0;
		String psaInfo = "", lsoAddress = "";

		try {
			lsoAddress = (String) session.getAttribute("lsoAddress");
			if (lsoAddress == null)
				lsoAddress = "";

			psaInfo = (String) session.getAttribute("psaInfo");
			if (psaInfo == null)
				psaInfo = "";
		} catch (Exception e) {
		}
%>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
    return true;
}

function savePage() {
	if (validateFunction()) {
		document.forms[0].action='<%=contextRoot%>/saveSiteUnit.do?action=Save';
 		document.forms[0].submit();
 	} else
 		return false;
}

function addPage() {
	if (validateFunction()) {
		document.forms[0].action='<%=contextRoot%>/saveSiteUnit.do?action=Add';
 		document.forms[0].submit();
 	} else
 		return false;
}

function cancelPage() {
	document.forms[0].action='<%=contextRoot%>/saveSiteUnit.do?action=Cancel';
 	document.forms[0].submit();
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<html:form action="/saveSiteUnit.do">
	<table width="100%" border="0" cellspacing="10" cellpadding="10">
		<tr>
			<td><font class="con_hdr_3b">Edit Structure Units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
			<br>
			</td>
		</tr>
	</table>

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Unit List <br>
					<br>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="1%" class="tablelabel" align="right"><nobr> <html:reset value="Reset" styleClass="button" /> <html:button property="Cancel" value="Back" styleClass="button" onclick="return cancelPage();" /> <html:button property="Add" value="Add" styleClass="button" onclick="return addPage();" /> <html:button property="Save" value="Update" styleClass="button" onclick="return savePage();" /> </nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr valign="top">
							<td background="../images/site_bg_B7C1CB.jpg"><nested:iterate property="unitArray">
								<%
									rows++;
								%>
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td colspan="3" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
									</tr>
								</table>
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr class="tabletext">
										<td class="tablelabel">Unit</td>
										<td><nested:text property="unit" styleClass="textboxm" size="4" /></td>
										<td class="tablelabel">Floor</td>
										<td><nested:text property="floor" styleClass="textbox" size="3" maxlength="2" onkeypress="return validateDecimal();" /></td>
										<td class="tablelabel">Address</td>
										<td colspan="3"><nobr><nested:text property="streetNbr" styleClass="textbox" size="5" maxlength="5" onkeypress="return validateDecimal();" /> <nested:select property="streetId" styleClass="textbox">
											<option value=""></option>
											<html:options collection="streets" property="streetId" labelProperty="streetName" />
										</nested:select></nobr></td>
										<td class="tablelabel">Use</td>
										<td><nested:select property="useType" styleClass="textbox">
											<option value=""></option>
											<html:options collection="siteUseList" property="siteUseId" labelProperty="description" />
										</nested:select></td>

									</tr>
									<tr class="tabletext">
										<td class="tablelabel">Permit #</td>
										<td><nested:text property="actNbr" styleClass="textboxm" size="9" maxlength="9" /></td>
										<td class="tablelabel">Est Sq. Ft.</td>
										<td><nested:text property="sqFootage" styleClass="textboxm" size="5" maxlength="9" onkeypress="return validateDecimal();" /></td>
										<td class="tablelabel">Mezzanine</td>
										<td><nested:checkbox property="mezzFlag" /></td>
										<td class="tablelabel">Mezzanine Area</td>
										<td><nested:text property="mezzArea" styleClass="textboxm" size="6" onkeypress="return validateDecimal();" /></td>
										<td class="tablelabel">Use Type</td>
										<td><nested:select property="mezzUseType" styleClass="textbox">
											<option value=""></option>
											<html:options collection="siteUseList" property="siteUseId" labelProperty="description" />
										</nested:select></td>
									</tr>
								</table>
							</nested:iterate></td>
						</tr>
					</table>
					<%
						String rowCounter = "" + rows;
					%> <html:hidden property="rowCount" value="<%=rowCounter%>" /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>

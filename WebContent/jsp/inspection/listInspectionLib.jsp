<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	  String contextRoot = request.getContextPath();
      elms.control.beans.SearchLibForm libSet = (elms.control.beans.SearchLibForm)request.getAttribute("libSet");
      pageContext.setAttribute("libSet", libSet);

	String editable = request.getParameter("ed");
    if(editable == null) editable = "";
    String inspectable = request.getParameter("insp");
 	if(inspectable == null) inspectable = "";
%>
<script>
function onclickprocess()
{
  document.forms[0].action='<%=contextRoot%>/addToInspection.do?ed=<%=editable%>&insp=<%=inspectable%>';
  document.forms[0].submit();
  return true;
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/addToInspection">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Inspection Library</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Inspection Library List</td>

                      <td width="1%" class="tablebutton"><nobr>
                      <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                      <html:submit  value="Add to Inspection" styleClass="button" onclick="return onclickprocess();"/>
                        &nbsp;</nobr></td></tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Category</td>
                      <td class="tabletext"><bean:write scope="request" name="libSet" property="categoryDesc"/></td>
                      <td class="tablelabel">Sub Category</td>
                      <td class="tabletext"><bean:write scope="request" name="libSet" property="subCategoryDesc"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
       <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td  background="../images/site_bg_B7C1CB.jpg">
                        List</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Select</td>
                      <td class="tablelabel"> Title</td>
                      <td class="tablelabel"> Comment Text</td>
                    </tr>
            <nested:iterate  property="libRecordList" type="elms.app.inspection.InspectionLibraryRecord">
            <tr valign="top">
			<td class="tabletext"><nested:checkbox  styleId="checkbox2" property="check"/></td>
            <td class="tabletext"><nested:write property="title"/></td>
            <td class="tabletext"><nested:write property="comment"/></td>
            </tr>
             </nested:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
       </table>
	   </html:form>

</body>
</html:html>

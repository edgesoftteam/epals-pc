<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>


<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%

java.util.List inspectionActionCodeList = InspectionAgent.getInspectionActionCodes(((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getDepartment().getDepartmentId());
pageContext.setAttribute("inspectionActionCodeList", inspectionActionCodeList);

java.util.List inspectionItemCodeList = (java.util.List) request.getAttribute("inspectionItemCodeList");
pageContext.setAttribute("inspectionItemCodeList", inspectionItemCodeList);


java.util.List inspectorUsers = InspectionAgent.getInspectorUsers(((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getDepartment().getDepartmentId());
pageContext.setAttribute("inspectorUsers", inspectorUsers);

elms.control.beans.InspectionForm inspectionForm = (elms.control.beans.InspectionForm)session.getAttribute("inspectionForm");
String inspectorId = inspectionForm.getInspectorId();
pageContext.setAttribute("inspectorId",inspectorId);

    String psaInfo = (String)session.getAttribute("psaInfo");
    if (psaInfo == null ) psaInfo = "";

    String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	//strValue=false;
	
	
	
	
	if (document.forms['inspectionForm'].elements['inspectionDate'].value == '')
   	{
 		strValue=validateData('req',document.forms['inspectionForm'].elements['inspectionDate'],'Date is a required field');
 		return;
    }

	if (document.forms['inspectionForm'].elements['inspectionDate'].value != '')
   	{
 		strValue=validateData('date',document.forms['inspectionForm'].elements['inspectionDate'],'Invalid date format');
    }
    else
    {
    	strValue=true;
    }

	if (strValue == true)
	{

    	if (document.forms['inspectionForm'].elements['inspectionItem'].value == '-1')
     	{
	 	               alert('Inspection Item is a required field');
	 	               strValue=false;
        }
	}

	if (strValue == true)
	{

    	if (document.forms['inspectionForm'].elements['actionCode'].value == '-1')
     	{
	 	               alert('Action Code is a required field');
	 	               strValue=false;
        }
     	else if (document.forms['inspectionForm'].elements['actionCode'].value != '99')
    	{
    		strValue=validateData('req',document.forms['inspectionForm'].elements['comments'],'Comments is a required field');
	    }
	}
	 if (strValue == true)
	{

		 document.forms[0].elements['save'].disabled=true;
		document.inspectionForm.libOrSave.value='save';
		document.forms[0].submit();
		
		
	} 
 
	
}

function goBack(button) {
  button.disabled=true;
  document.location.href="<%=request.getContextPath()%>/viewActivity.do?activityId=<%=inspectionForm.getActivityId()%>";
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<html:form action="/saveNewInspection">
<html:hidden property="activityId" />
<html:hidden property="libOrSave"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><tr valign="top">
    <td width="99%">
      <table width="99%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><tr>
          <td><font class="con_hdr_3b">Add Inspection</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
         </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><tr>
                      <td width="99%" class="tabletitle">Add</td>
                      <td width="1%" class="tablebutton"><nobr>
                          <html:button  value="Back" property="Back" styleClass="button" onclick="goBack(this)"/>
                          <html:button value="Library" property="library" styleClass="button" onclick="javascript:document.inspectionForm.libOrSave.value='lib';document.forms[0].submit();"/>
                          <html:button value="Save" property="save" styleClass="button" onclick="validateFunction();"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </TBODY></table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><tr>
                      <td class="tablelabel"> Inspection Date</td>
                      <td class="tablelabel"> Inspection Item</td>
                      <td class="tablelabel"> Action Code</td>
                      <td class="tablelabel">Inspector</td>
                    </tr>
                    <tr valign="top">
                      <td class="tabletext"><nobr>
                          <html:text  property="inspectionDate" size="10" styleClass="textbox" onkeypress="return validateDate();"></html:text>
					    <a href="javascript:show_calendar('inspectionForm.inspectionDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border="0"/></a></nobr>
                      </td>
                      <td class="tabletext">
                          <html:select property="inspectionItem" styleClass="textbox">
                            <html:options collection="inspectionItemCodeList" property="code" labelProperty="description"/>
                        </html:select>
                      </td>
                      <td class="tabletext">
                        <html:select property="actionCode" styleClass="textbox">
                            <html:options collection="inspectionActionCodeList" property="code" labelProperty="description"/>
                        </html:select>
                      </td>
                      <td class="tabletext">
                        <html:select property="inspectorId" styleClass="textbox">
                            <html:options collection="inspectorUsers" property="userId" labelProperty="inspectorId"/>
                        </html:select>
                      </td>
                    </tr>
					<tr>
                      <td class="tablelabel"> Comments</td>
                      <td class="tabletext" colspan="3">
                          <html:textarea rows="25"  cols="73" property="comments" value="<%=inspectionForm.getComments()%>">
						</html:textarea>
                      </td>
					</tr>
                  </TBODY></table>
                </td>
              </tr>
            </TBODY></table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </TBODY></table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </TBODY></table>
    </td>
  </tr>
</TBODY></table>
</html:form>
</body>
</html:html>

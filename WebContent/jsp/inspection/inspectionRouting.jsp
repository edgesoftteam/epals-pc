<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>

</head>
<%!
String projectIdStr = "";
String subProjectIdStr = "";
String activityIdStr = "";
String inspectionIdStr = "";
String status = "";
String holdInfo = "";
String activityInspectable = "";
String hardHoldActive ="";
elms.app.finance.FinanceSummary financeSummary = null;
%>
<%
	String contextRoot = request.getContextPath();
	String error = (String) request.getAttribute("error")!=null ? (String) request.getAttribute("error") : "" ;
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form   action="/refreshRouting">
<html:hidden property="routingId"/>
<html:hidden property="changed"/>


<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Inspections</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<font class="red3" ><%=error%></font>
						</td>
					</tr>

                    <tr>
                      <td width="99%" class="tabletitle">Inspection Routing </td>
                        <td width="1%" class="tablebutton"><nobr><a href="javascript:document.viewAllInspectionForm.changed.value='yes';document.forms[0].submit();" class="hdrs">Save<img src="../images/site_blt_x_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"><nobr>Inspection Date</nobr></td>
                      <td class="tabletext" colspan="5" valign="top"><nobr>
                          <nested:text  property="inspectionDate" size="10" styleClass="textbox" onfocus="showCalendarControl(this);"/>
					    </nobr>
                        <html:button value="Refresh" property="refresh" styleClass="button" onclick="document.forms[0].submit();" />
                      </td>
                    </tr>
                    <nested:iterate  property="inspector" type="elms.app.inspection.Inspector">
                    <tr>
                      <td class="tablelabel"><nobr>Inspector</nobr></td>
                      <td class="tabletext" colspan="6"> <nested:write property="name" /></td>
                    </tr>
                    <tr>
                      <td  class="tabletext" colspan="6"><img src="../images/spacer.gif" width="1" height="5"></td>
                    </tr>
                    <tr>
                      <td class="tablelabel"><nobr>Sub Project
                        No. </nobr></td>
                      <td class="tablelabel"> Activity
                        No. </td>
                       <td class="tablelabel"> Inspection
                        Date </td>
                      <td class="tablelabel"><nobr>Address/Activity
                        Info</nobr></td>
                      <td class="tablelabel"> Inspection
                        Type </td>
                      <td class="tablelabel"> Routing</td>
                    </tr>
                    <nested:iterate id="rec" name="inspector" property="inspectorRecord" type="elms.app.inspection.InspectorRecord">
                    <%
						elms.app.inspection.InspectorRecord insp = (elms.app.inspection.InspectorRecord)pageContext.getAttribute("rec");
                    	projectIdStr = insp.getProjectId();
                    	subProjectIdStr = insp.getSubProjectId();
                    	activityIdStr = insp.getActivityId();
                        String activityNo = insp.getActivityNbr();
						inspectionIdStr = insp.getInspectionId();
						//finance for this activity
						try{
 					   		financeSummary = new elms.agent.FinanceAgent().getActivityFinance(Integer.parseInt(activityIdStr));
						}catch(Exception e){}
   					   		request.setAttribute("financeSummary", financeSummary);

						int actStatusId=0;
						 try{
							 status = LookupAgent.getMostRecentHoldStatus(activityIdStr);
							actStatusId = LookupAgent.getActivityStatusId(activityIdStr);

							if (!(new CommonAgent().editable(elms.util.StringUtils.s2i(activityIdStr), "A"))) {
						                hardHoldActive = "false";
						            }else{
						           hardHoldActive="true";
									}
						}catch(Exception e){}

//***************************************End for inspectable check***********************************
               // Checking Inspectable and deletable
                if(activityNo.startsWith("PW")){
                	if ((financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))){ // Status = Issued and balance due is zero
						activityInspectable = "true";
                     }else{
						activityInspectable = "false";
                     }
                 }else if (activityNo.startsWith("CE")){
                	if ((financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))) { // Status = Issued and balance due is zero
						activityInspectable = "true";
					}else{
						activityInspectable = "false";
                    }
                 }else if ((
                (actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_PERMIT_ISSUED)||//permit issued
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_ADMIN_PENDING)||//admin pending
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_PERMIT_REISSUED)||//permit reissued
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_PERMIT_EXTENDED)||//permit extended
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_TCO)) &&
				(financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))) { // Status = Issued and balance due is zero
						activityInspectable = "true";
                }else{
						activityInspectable = "false";
                }
//***************************************End for inspectable check***********************************
		%>
                    <tr>
                      <td class="tabletext" valign="center"><a href="<%=contextRoot%>/viewSubProject.do?subProjectId=<%=subProjectIdStr%>&address=<nested:write  property="address"/>"><nested:write property="subProjectNbr"/></a></td>
	        			<%if(status.equalsIgnoreCase("S") || status.equalsIgnoreCase("A")){%>
							<td class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<%=activityIdStr%>"><font class="red1"><nested:write property="activityNbr"/></a></td>
						<%}else{%>
							<td class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<%=activityIdStr%>"><nested:write property="activityNbr"/></a></td>
						<%}%>
					  <td class="tabletext" valign="center"> <nested:write property="inspectionDate"/></td>	
                      <td class="tabletext" valign="center"> <nested:write property="address"/></td>
					  <td class="tabletext" valign="center"> <a href="<%=contextRoot%>/editInspection.do?inspectId=<%=inspectionIdStr%>&ed=<%=hardHoldActive%>&insp=<%=activityInspectable%>"><nested:write property="inspectionType"/></a></td>
                      <td class="tabletext" valign="center"> <nested:text  property="route" size="2" styleClass="textbox" onkeypress="return validateDate();"/></td>
                    </tr>
					</nested:iterate>
					</nested:iterate>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

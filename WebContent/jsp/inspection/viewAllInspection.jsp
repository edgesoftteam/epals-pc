<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="java.util.*, elms.control.beans.*, elms.app.inspection.*"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>


<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>

</head>
<%

String projectIdStr = "";
String subProjectIdStr = "";
String activityIdStr = "";
String inspectionIdStr = "";
String status = "";
String holdInfo = "";
String activityInspectable = "";
String hardHoldActive ="";
elms.app.finance.FinanceSummary financeSummary = null;
String contextRoot = request.getContextPath();

%>

<script language="JavaScript" src="../script/formValidations.js"></script>
<script>
function onclickprocess()
{
   document.forms[0].action='<%=contextRoot%>/assignInspectionAction.do';
   document.forms[0].submit();
}
function onchangeprocess()
{
   document.forms[0].action='<%=contextRoot%>/assignInspectionAction.do';
   document.forms[0].submit();
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="javascript:parent.f_reports.location.reload();">
<html:form name="viewAllInspectionForm" type="elms.control.beans.ViewAllInspectionForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Inspections</font><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Inspections</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><nobr>Date</nobr></td>
                                <td class="tabletext" valign="top" colspan="3"><nobr> <nested:text property="inspectionDate" size="10" styleClass="textbox" onfocus="showCalendarControl(this);" onblur="DateValidate(this);"  onkeypress="return event.keyCode!=13"/> </nobr>&nbsp;<html:button value="Refresh" property="refresh" styleClass="button" onclick="onclickprocess()"/></td>
                                <logic:equal name="viewAllInspectionForm" property="manager" value="Y">
	                                <td class="tablelabel" valign="top">Re-Assign</td>
	                                <td class="tabletext" valign="top" colspan="3"><nested:select property="reAssign" styleClass="textbox" onchange="onchangeprocess()">
	                                	<nested:options collection="inspectorUsers" property="userId" labelProperty="inspectorId"/>
	                                	</nested:select>
	                                </td>
	                            </logic:equal>
                                <logic:notEqual name="viewAllInspectionForm" property="manager" value="Y">
	                                <td class="tabletext" valign="top" colspan="4">&nbsp;</td>
	                            </logic:notEqual>
                             </tr>

                            <nested:iterate indexId="indexId" property="inspector" type="elms.app.inspection.Inspector"> <nested:hidden property="count"/> <nested:hidden property="selectValue"/>
                            <tr>
                                <td class="tabletext" colspan="8"><img src="../images/spacer.gif" width="1" height="5"></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" colspan="8"><nobr><b><nested:write property="name"/></b></nobr></td>
                            </tr>
                            <tr>
			                     <%
			                     Integer index = (Integer)pageContext.getAttribute("indexId");
			                     %>
                                <td class="tablelabel"><logic:equal name="viewAllInspectionForm" property="manager" value="Y"><nobr><input type="button" value="Select All" property="selectAll" class="button" onclick="document.viewAllInspectionForm.elements['inspector[<%=index%>].selectValue'].value='all';document.forms[0].action='<%=contextRoot%>/inspectionSelectAllAction.do';document.forms[0].submit();"/></nobr></logic:equal></td>
                                <td class="tablelabel"><nobr>Sub Project No. </nobr></td>
                                <td class="tablelabel"><nobr>Activity No. </nobr></td>
                                <td class="tablelabel">Inspection Date</td>
                                <td class="tablelabel">Address/Activity Info</td>
                                <td class="tablelabel">Inspection Item </td>
                                <td class="tablelabel">Route </td>
                                <td class="tablelabel">FIR</td>
		                     </tr>
		                            <nested:iterate id="rec" name="inspector" property="inspectorRecord" type="elms.app.inspection.InspectorRecord">
            	<%
						elms.app.inspection.InspectorRecord insp = (elms.app.inspection.InspectorRecord)pageContext.getAttribute("rec");
                    	projectIdStr = insp.getProjectId();
                    	subProjectIdStr = insp.getSubProjectId();
                    	activityIdStr = insp.getActivityId();
                        String activityNo = insp.getActivityNbr();
						inspectionIdStr = insp.getInspectionId();

						//finance for this activity
						try{
 					   		financeSummary = new elms.agent.FinanceAgent().getActivityFinance(Integer.parseInt(activityIdStr));
						}catch(Exception e){}
   					   		request.setAttribute("financeSummary", financeSummary);

						int actStatusId=0;
						 try{
							 status = LookupAgent.getMostRecentHoldStatus(activityIdStr);
							actStatusId = LookupAgent.getActivityStatusId(activityIdStr);

							if (!(new CommonAgent().editable(elms.util.StringUtils.s2i(activityIdStr), "A"))) {
						                hardHoldActive = "false";
						            }else{
						           hardHoldActive="true";
									}
							}catch(Exception e){}

//***************************************End for inspectable check***********************************
               // Checking Inspectable and deletable
                if(activityNo.startsWith("PW")){
                	if ((financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))){ // Status = Issued and balance due is zero
						activityInspectable = "true";
                     }else{
						activityInspectable = "false";
                     }
                 }else if (activityNo.startsWith("CE")){
                	if ((financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))) { // Status = Issued and balance due is zero
						activityInspectable = "true";
					}else{
						activityInspectable = "false";
                    }
                 }else if ((
                (actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_PERMIT_ISSUED)||//permit issued
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_ADMIN_PENDING)||//admin pending
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_PERMIT_REISSUED)||//permit reissued
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_PERMIT_EXTENDED)||//permit extended
				(actStatusId == elms.common.Constants.ACTIVITY_STATUS_BUILDING_TCO)) &&
				(financeSummary.getTotalAmountDue().equals("$0.00") || financeSummary.getTotalAmountDue().startsWith("("))) { // Status = Issued and balance due is zero
						activityInspectable = "true";
                }else{
						activityInspectable = "false";
                }
//***************************************End for inspectable check***********************************
inspectionIdStr = insp.getInspectionId();
		%>
                            <tr>
                                <td class="tabletext" valign="top" align="center">
                                    <nested:equal property="actionCode" value="98">
	                                	<img src="../images/correct.jpg">
	                                </nested:equal>
	                                <logic:equal name="viewAllInspectionForm" property="manager" value="Y">
	                                <nested:notEqual property="actionCode" value="98">
	                                	<nested:checkbox styleId="checkbox2" property="check"/>
	                                </nested:notEqual>
	                                </logic:equal>

                                </td>
                                <td class="tabletext" valign="top"><a href="<%=contextRoot%>/viewSubProject.do?subProjectId=<%=subProjectIdStr%>&address=<nested:write  property="address"/>"><nested:write property="subProjectNbr"/></a></td>
                                <%if(status.equalsIgnoreCase("S") || status.equalsIgnoreCase("A")){%>
									<td class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<%=activityIdStr%>"><font class="red1"><nested:write property="activityNbr"/></a></td>
								<%}else{%>
									<td class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<%=activityIdStr%>"><nested:write property="activityNbr"/></a></td>
								<%}%>
                                <td class="tabletext" valign="top"><nested:write property="inspectionDate"/></td>
                                <td class="tabletext" valign="top"><nested:write property="address"/></td>
								<td class="tabletext" valign="top"><a href="<%=contextRoot%>/editInspection.do?inspectId=<%=inspectionIdStr%>&lsoAddress=<nested:write property="address"/>&ed=<%=hardHoldActive%>&insp=<%=activityInspectable%>"><nested:write property="inspectionType"/></a></td>
								<td class="tabletext" valign="top"><nested:write property="route" /></td>
                                <td class="tabletext" valign="top"><a href="<%=contextRoot%>/printFIR.do?actNbr=<%=LookupAgent.getActivityNumberForActivityId(activityIdStr)%>" target="_blank">FIR</a></td>
                            </tr>
                            </nested:iterate> </nested:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
<%
ViewAllInspectionForm frm = (ViewAllInspectionForm) session.getAttribute("viewAllInspectionForm");

			if (frm == null)
				System.out.println("frm is null");

			Inspector[] inspectors = null;
			InspectorRecord[] records;

			inspectors = frm.getInspector();

				for (int i = 0; i < inspectors.length; i++) {
						records = inspectors[i].getInspectorRecord();

						for (int j = 0; j < records.length; j++) {
							records[j].setCheck("of");
						}
					inspectors[i].setSelectValue("off");

				}


			frm.setInspector(inspectors);

			session.setAttribute("viewAllInspectionForm", frm);


%> </html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<%
    String contextRoot = request.getContextPath();
    String editable = request.getParameter("ed");
    if(editable == null) editable = "";
    String inspectable = request.getParameter("insp");
    if(inspectable == null) inspectable = "";
    String activityId = (String)request.getAttribute("activityId");
    String activityType = (String) request.getAttribute("activityType");
    if  (activityType == null ) activityType = "";
    else activityType =  "&type=" + activityType;
    String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";
    String psaInfo = (String)session.getAttribute("psaInfo");
    if (psaInfo == null ) psaInfo = "";
%>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><font class="con_hdr_3b">Inspection List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Inspections</td>
                      <td width="1%" class="tablebutton"><nobr>

                      <a href="<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>" class="hdrs"><font class="con_hdr_link">Back<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a>&nbsp;
                      <% if(editable.equals("true") && inspectable.equals("true")){ %>
                        <a href="<%=contextRoot%>/addInspection.do?activityId=<%=activityId%><%=activityType%>" styleClass="hdrs"><font class="con_hdr_link">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                      <%}%>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Inspection Date</td>
                      <td class="tablelabel">Inspection Item</td>
                      <td class="tablelabel">Action Code</td>
                      <td class="tablelabel">Source</td>
                    </tr>
					<logic:iterate id="inspection" name="inspectionList" scope="request" type="elms.app.inspection.InspectionList">
	                    <tr valign="top">
	                      <td class="tabletext">
	                      <a href="<%=contextRoot%>/editInspection.do?inspectId=<bean:write name="inspection" property="inspectionId"/>&ed=<%=editable%>&insp=<%=inspectable%>">
	                        <bean:write name="inspection" property="inspectionDate"/>
	                      </a>
	                      </td>
	                      <td class="tabletext"><bean:write name="inspection" property="inspectionItem"/></td>
	                      <td class="tabletext"><bean:write name="inspection" property="actionCode"/></td>
	                      <td class="tabletext"><bean:write name="inspection" property="requestSource"/></td>
	                    </tr>
					</logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html:html>


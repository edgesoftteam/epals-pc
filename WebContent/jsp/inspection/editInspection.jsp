<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ page import="elms.control.beans.*" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%
	String contextRoot = request.getContextPath();

	String editable = request.getParameter("ed");
    if(editable == null) editable = "";
    String inspectable = request.getParameter("insp");
 	if(inspectable == null) inspectable = "";

    String inspURL=LookupAgent.getReportsURL();
%>
<script src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	//strValue=false;
	strValue=validateData('req',document.forms['inspectionForm'].elements['inspectionDate'],'Inspection Date is a required field');
	if (strValue == true)
	{
		if (document.forms['inspectionForm'].elements['actionCode'].value == '-1')
   		{
 			alert("Action Code is a required field");
 			strValue=false;
    	}
    	else
    	{
    		strValue=true;
    	}
    }
	if (strValue == true)
	{
    	if (document.forms['inspectionForm'].elements['actionCode'].value != '99')
    	{
    		strValue=validateData('req',document.forms['inspectionForm'].elements['comments'],'Comments is a required field');
	    }
	}
	return strValue;
}
function onclickprocess(strval)
{
  if (strval = 'vl') document.location='<%=contextRoot%>/viewLibraryPage.do?ed=<%=editable%>&insp=<%=inspectable%>';

}
function processPrint()
{
  inspId=document.forms['inspectionForm'].elements['inspectionId'].value;
  window.open('<%=inspURL%>'+inspId);
}
</SCRIPT>
</head>

<%
java.util.List inspectionActionCodeList = InspectionAgent.getInspectionActionCodes(((elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY)).getDepartment().getDepartmentId());
pageContext.setAttribute("inspectionActionCodeList", inspectionActionCodeList);
elms.control.beans.InspectionForm inspectionForm = (elms.control.beans.InspectionForm)session.getAttribute("inspectionForm");

String breadcrump = elms.agent.LookupAgent.getBreadCrump(inspectionForm.getActivityId());

%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<html:form  action="/saveEditedInspection" onsubmit="return validateFunction();">
<html:hidden property="inspectionId" value="<%=inspectionForm.getInspectionId()%>"/>
<html:hidden property="activityId" value="<%=inspectionForm.getActivityId()%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">

      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
          <td><font class="con_hdr_3b">Modify Inspection</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=breadcrump%><br><br>
         </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
             <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                      <td width="99%" class="tabletitle">Modify</td>


                      <td width="1%" class="tablebutton"><nobr>
						   <html:reset  value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
					      <%
					      	if(!(inspectionForm.getActionCode().equalsIgnoreCase("1"))){
					      %>
					      <html:button value="Library" property="library" styleClass="button" onclick="onclickprocess('vl')"/>
							<% if(editable.equals("true") && inspectable.equals("true")){ %>
                          <html:submit value="Update" property="update" styleClass="button"/>
						  <%
						  	}}
						  %>
                       </nobr>
                        </td>

                    </tr>
                 </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">

                   <tr>
                      <td class="tablelabel"> Inspection&nbsp;Date</td>
                      <td class="tablelabel"> Inspection Item</td>
                      <td class="tablelabel"> Action Code</td>
                      <td class="tablelabel">Inspector</td>
                    </tr>

                    <tr valign="top">

                  <td class="tabletext"> <nobr>
                   <html:text  property="inspectionDate" size="10" styleClass="textbox" onkeypress="return validateDate();"> </html:text>
                   <a href="javascript:show_calendar('inspectionForm.inspectionDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                    <img src="../images/calendar.gif" width="16" height="15" border="0"/>
                   </a>
                  	</nobr> </td>
                      <td class="tabletext"><%= inspectionForm.getInspectionItemDesc()%></td>
                      <td class="tabletext">
                         <html:select property="actionCode" styleClass="textbox">
                            <html:options collection="inspectionActionCodeList" property="code" labelProperty="description"/>
                       	  </html:select>


                      </td>
                       <td class="tabletext">
						<%= inspectionForm.getInspectorName()%>
                      </td>

                    </tr>
					<tr>
                      <td class="tablelabel"> Comments</td>
                      <td class="tabletext" colspan="3">
                          <html:textarea rows="25" cols="80"  property="comments">
						</html:textarea>
				
                  		</td>
					</tr>


                 </table>
                </td>
              </tr>
          </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
     </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

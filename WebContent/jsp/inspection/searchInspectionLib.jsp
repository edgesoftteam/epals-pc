<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base />
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"> </script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<%
String contextRoot = request.getContextPath();
java.util.List inspectionCategoryList = InspectionAgent.getInspectionCategories();
pageContext.setAttribute("inspectionCategoryList", inspectionCategoryList);
java.util.List  inspectionSubCategoryList = (java.util.List) request.getAttribute("inspectionSubCategoryList");
pageContext.setAttribute("inspectionSubCategoryList", inspectionSubCategoryList);

	String editable = request.getParameter("ed");
    if(editable == null) editable = "";
    String inspectable = request.getParameter("insp");
 	if(inspectable == null) inspectable = "";
%>
<script>
function onchangeprocess()
{
  document.forms[0].action='<%=contextRoot%>/searchLibrary.do?flag=onChange&ed=<%=editable%>&insp=<%=inspectable%>';
  document.forms[0].submit();
  return true;
}
function onclickprocess()
{
  document.forms[0].action='<%=contextRoot%>/searchLibrary.do?ed=<%=editable%>&insp=<%=inspectable%>';
  document.forms[0].submit();
  return true;

}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onresize="AdjustComboBoxes();" >

<html:form action="/searchLibrary">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Inspection Library Maintenance</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle">Search</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Category</td>
					  <td class="tabletext" width="74%">
					    <html:select property="category" styleClass="textbox" style="position:absolute" onchange="return onchangeprocess()">
                          <html:options collection="inspectionCategoryList" property="categoryId" labelProperty="description"/>
                        </html:select>
                        <br>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Sub Category</td>
                      <td class="tabletext" width="74%">
                        <html:select  property="subCategory" styleClass="textbox" style="position:absolute" onchange="if(SubCategoryCombo != null) {SubCategoryCombo.processComboBoxSelect();}">
                          <html:options collection="inspectionSubCategoryList" property="subCategoryId" labelProperty="description"/>
						</html:select>
						<br>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" colspan="2"><img src="../images/spacer.gif" width="1" height="10"></td>
                    </tr>
                    <tr>
                      <td class="tabletext" colspan="2"> <nobr>
                        <html:submit  value="Search" property="search" styleClass="button" onclick="return onclickprocess();"/>
                        <html:reset  value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
                      	</nobr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<SCRIPT>
var CategoryCombo;
var SubCategoryCombo;

function initComboBox(strCategoryId)
{
	CategoryCombo = new ComboBoxObject("category", "CategoryCombo");
	CategoryCombo.setDoLookup(true);
	CategoryCombo.setCaseInsensitive(true);
	CategoryCombo.setDefaultByText(strCategoryId);

	SubCategoryCombo = new ComboBoxObject("subCategory", "SubCategoryCombo");
	SubCategoryCombo.setDoLookup(true);
	SubCategoryCombo.setCaseInsensitive(true);
}
</SCRIPT>
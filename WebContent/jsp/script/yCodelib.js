/* 
 * Copyright Notice. 
 * This code was originally created by yCode: http://www.yCode.com  
 * Any modifications to this code can only be made upon purchase from
 * yCode provided that this notice remains in the source code. 
 *
 */

//ycodelib.js

//==========================================================
//calculate_absolute_X() gets the X position on the screen
//----------------------------------------------------------
function calculate_absolute_X( theElement )
{
	var xPosition = 0;

	while ( theElement != null )
	{
		xPosition += theElement.offsetLeft;
		theElement = theElement.offsetParent;
	}

	return xPosition;
}

//==========================================================
//calculate_absolute_Y() gets the Y position on the screen
//----------------------------------------------------------
function calculate_absolute_Y( theElement )
{
	var yPosition = 0;

	while ( theElement != null )
	{
		yPosition += theElement.offsetTop;
		theElement = theElement.offsetParent;
	}

	return yPosition;
}

//make sure that we're running IE5
function checkBrowser()
{
	var bDisplayMessage=true;
	var av=navigator.appVersion;

	// Check if IE and if so, check version
	var iMSIE=parseInt(av.indexOf("MSIE"));

	if (iMSIE>=1)
	{
		// Get major version and write appropriate style
		var iVer=parseInt(av.charAt(iMSIE+5));
		if (iVer>=5)
		{
			bDisplayMessage=false;
		}
	}  
}
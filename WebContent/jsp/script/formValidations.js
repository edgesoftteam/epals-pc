
// Declaring valid date character, minimum year and maximum year
function checkNum(data)
{
   var validNum = "0123456789.";
   var i = count = 0;
   var dec = ".";
   var space = " ";
   var val = "";

   for (i = 0; i < data.length; i++)
      if (validNum.indexOf(data.substring(i, i+1)) != "-1")
         val += data.substring(i, i+1);

   return val;
}

function DisplayPhoneHyphen(str){
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}else{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 )){
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
	if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
}

function validateInteger(){

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
	return false;	
	}
}

function dollarFormatted(amount) {
	return '$' + commaFormatted(amount);
}

function commaFormatted(amount) {
		var delimiter = ","; // replace comma if desired
		var a = amount.split('.',2)
		var d = a[1];
		var i = parseInt(a[0]);
		if(isNaN(i)) { return ''; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		var n = new String(i);
		var a = [];
		while(n.length > 3)
		{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
		}
		if(n.length > 0) { a.unshift(n); }
		n = a.join(delimiter);
		if(d.length < 1) { amount = n; }
		else { amount = n + '.' + d; }
		amount = minus + amount;
		return amount;
}
// end of function CommaFormatted()

function formatCurrency(field) {
		var i = parseFloat(checkNum(field.value));
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = parseInt((i + .005) * 100);
		i = i / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		field.value = dollarFormatted(s);
		return true;
}
function displayPeriod() {
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}
}





function DateValidate(objVar)
{
 if (objVar.value !="")
 {
 	if (isDate(objVar.value)==false){
			objVar.focus()
		}
 }
}

function isDate(dtStr){
	//alert(dtStr);
	var daysInMonth = DaysArray(12);
	var pos1=dtStr.indexOf(dtCh);
	var pos2=dtStr.indexOf(dtCh,pos1+1);
	var strMonth=dtStr.substring(0,pos1);
	var strDay=dtStr.substring(pos1+1,pos2);
	var strYear=dtStr.substring(pos2+1);
	strYr=strYear;
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
	}
	month=parseInt(strMonth);
	day=parseInt(strDay);
	year=parseInt(strYr);
	//alert(month);
	//alert(day);
	//alert(year);
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy");
		return false;
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month");
		return false;
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day");
		return false;
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid year in YYYY format");
		return false;
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date");
		return false;
	}


	today=new Date();
	//var christmas = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
	//alert(Date.parse(dt));
	var christmas= new Date(year,month-1,day); //Month is 0-11 in JavaScript
	//alert(christmas);
	//Set 1 day in milliseconds
	var one_day=1000*60*60*24;

	//Calculate difference btw the two dates, and convert to days
	var no_of_days =Math.ceil((christmas.getTime()-today.getTime())/(one_day));
	//alert(no_of_days);


	
return true;
}


var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31;
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30};
		if (i==2) {this[i] = 29};
   }
   return this;
}

function validateEmail(email)
{
//  a very simple email validation checking.
//  you can add more complex email checking if it helps
    var splitted = email.match("^(.+)@(.+)$");    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null)
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if
      return true;
    }
return false;
}

/* function validateData
*  Checks each field in a form
*  Called from validateForm function
*/
function validateData(strValidateStr,objValue,strError)
{
    var epos = strValidateStr.search("=");
    var  command  = "";
    var  cmdvalue = "";
    if(epos >= 0)
    {
     command  = strValidateStr.substring(0,epos);
     cmdvalue = strValidateStr.substr(epos+1);
    }
    else
    {
     command = strValidateStr;
    }
    switch(command)
    {
        case "req":
        case "required":
         {
           if((objValue.value == 'null') || eval((objValue.value.length)) == 0)
           {
              if(!strError || strError.length ==0)
              {
                strError = objValue.name + " : Required Field";
              }//if
              alert(strError);
              objValue.focus();
              return false;
           }//if
           break;
         }//case required
        case "maxlength":
        case "maxlen":
          {
             if(eval(objValue.value.length) >  eval(cmdvalue))
             {
               if(!strError || strError.length ==0)
               {
                 strError = objValue.name + " : "+cmdvalue+" characters maximum ";
               }//if
               alert(strError + "\n[Current length = " + objValue.value.length + " ]");
               objValue.focus();
               return false;
             }//if
             break;
          }//case maxlen
        case "minlength":
        case "minlen":
           {
             if(eval(objValue.value.length) <  eval(cmdvalue))
             {
               if(!strError || strError.length ==0)
               {
                 strError = objValue.name + " : " + cmdvalue + " characters minimum  ";
               }//if
               alert(strError + "\n[Current length = " + objValue.value.length + " ]");
               objValue.focus();
               return false;
             }//if
             break;
            }//case minlen
        case "alnum":
        case "alphanumeric":
           {
              var charpos = objValue.value.search("[^A-Za-z0-9]");
              if(objValue.value.length > 0 &&  charpos >= 0)
              {
               if(!strError || strError.length ==0)
                {
                  strError = objValue.name+": Only alpha-numeric characters allowed ";
                }//if
                alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
                objValue.focus();
                return false;
              }//if
              break;
           }//case alphanumeric
        case "num":
        case "numeric":
           {
              var charpos = objValue.value.search("[^0-9]");
              if(objValue.value.length > 0 &&  charpos >= 0)
              {
                if(!strError || strError.length ==0)
                {
                  strError = objValue.name+": Only digits allowed ";
                }//if
                alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
                objValue.focus();
                return false;
              }//if
              break;
           }//numeric
        case "alphabetic":
        case "alpha":
           {
              var charpos = objValue.value.search("[^A-Za-z]");
              if(objValue.value.length > 0 &&  charpos >= 0)
              {
                  if(!strError || strError.length ==0)
                {
                  strError = objValue.name+": Only alphabetic characters allowed ";
                }//if
                alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
                objValue.focus();
                return false;
              }//if
              break;
           }//alpha
        case "currency":
        case "curr":
           {
              var charpos = objValue.value.search("[^0-9,.]");
              if(objValue.value.length > 0 &&  charpos >= 0)
              {
                  if(!strError || strError.length ==0)
                {
                  strError = objValue.name+": Only alphabetic characters allowed ";
                }//if
                alert(strError);
                objValue.focus();
                return false;
              }//if
              break;
           }//alpha
        case "date":
        	{
        		dtStr=objValue.value;
        		var daysInMonth = DaysArray(12);
				var pos1=dtStr.indexOf(dtCh);
				var pos2=dtStr.indexOf(dtCh,pos1+1);
				var strMonth=dtStr.substring(0,pos1);
				var strDay=dtStr.substring(pos1+1,pos2);
				var strYear=dtStr.substring(pos2+1);
				strYr=strYear;
				if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1);
				if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1);
				for (var i = 1; i <= 3; i++) {
					if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1);
					}
				month=parseInt(strMonth);
				day=parseInt(strDay);
				year=parseInt(strYr);
				if (pos1==-1 || pos2==-1){
					alert("The date format should be : mm/dd/yyyy");
					objValue.focus();
					return false;
				}
				if (month<1 || month>12){
					alert("Please enter a valid month");
					objValue.focus();
					return false;
				}
				if (day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
					alert("Please enter a valid day");
					objValue.focus();
					return false;
				}
				if (strYear.length != 4 || year==0 ){
					alert("Please enter a valid 4 digit year");
					objValue.focus();
					return false;
				}
				if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
					alert("Please enter a valid date");
					objValue.focus();
					return false;
				}
				return true;
        	}

        case "email":
          {
               if(!validateEmail(objValue.value))
               {
                 if(!strError || strError.length ==0)
                 {
                    strError = objValue.name+": Enter a valid Email address ";
                 }//if
                 alert(strError);
                 objValue.focus();
                 return false;
               }//if
           break;
          }//case email
        case "lt":
        case "lessthan":
         {
            if(isNaN(objValue.value))
            {
              alert(objValue.name+": Should be a number ");
              return false;
            }//if
            if(eval(objValue.value) >=  eval(cmdvalue))
            {
              if(!strError || strError.length ==0)
              {
                strError = objValue.name + " : value should be less than "+ cmdvalue;
              }//if
              alert(strError);
              objValue.focus();
              return false;
             }//if
            break;
         }//case lessthan
        case "gt":
        case "greaterthan":
         {
            if(isNaN(objValue.value))
            {
              alert(objValue.name+": Should be a number ");
              return false;
            }//if
             if(eval(objValue.value) <=  eval(cmdvalue))
             {
               if(!strError || strError.length ==0)
               {
                 strError = objValue.name + " : value should be greater than "+ cmdvalue;
               }//if
               alert(strError);
               objValue.focus();
               return false;
             }//if
            break;
         }//case greaterthan
        case "regexp":
         {
            if(!objValue.value.match(cmdvalue))
            {
              if(!strError || strError.length ==0)
              {
                strError = objValue.name+": Invalid characters found ";
              }//if
              alert(strError);
              objValue.focus();
              return false;
            }//if
           break;
         }//case regexp
         case "pho":
         case "phone":
	    {
          var charpos = objValue.value.search("[^0-9-]");
          if(objValue.value.indexOf('-') != 3 || objValue.value.lastIndexOf('-') != 7 )
          {
          	alert(strError);
            objValue.focus();
            return false;
          }
    
          
          if(objValue.value.length > 0 &&  charpos >= 0)
          {
            if(!strError || strError.length ==0)
            {
              strError = objValue.name+": Only digits allowed ";
            }//if
            alert(strError + "\n [Error character position " + eval(charpos+1)+"]");
            objValue.focus();
            return false;
          }//if
          break;
       }//phone
        case "dontselect":
         {
            if(objValue.selectedIndex == null)
            {
              alert("BUG: dontselect command for non-select Item");
              return false;
            }
            if(objValue.selectedIndex == eval(cmdvalue))
            {
             if(!strError || strError.length ==0)
              {
              strError = objValue.name+": Please Select one option ";
              }//if
              alert(strError);
              objValue.focus();
              return false;
             }
             break;
         }//case dontselect
    }//switch
    return true;
}

/*
* function validateForm
* the function that can be used to validate any form
* returns false if the validation fails; true if success
* arguments  :
* objFrm     : the form object
* arrObjDesc : an array of objects describing the validations to conduct on each
*              input item.
*          The array should consist of one object per input item in the order the input
*          elements are present in the form. Each object consist of zero or more validation
*          objects. Each of these validation object is a pair consisting of the validation
*          descriptor string and an optional Error message.
*/

function validateForm(objFrm,arrObjDesc)
{
 for(var itrobj=0; itrobj < arrObjDesc.length; itrobj++)
 {
   if(objFrm.elements.length <= itrobj)
   {
        alert("BUG: Obj descriptor for a non existent form element");
        return false;
   }//if
   for(var itrdesc=0; itrdesc < arrObjDesc[itrobj].length ;itrdesc++)
   {
      if(validateData(arrObjDesc[itrobj][itrdesc][0],
                 objFrm[itrobj],arrObjDesc[itrobj][itrdesc][1]) == false)
       {
	     objFrm[itrobj].focus();
	     return false;
       }//if
   }//for
 }//for
}

function validateNumeric()
{
//&& (event.keyCode != 44)
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	event.returnValue = false;
}

function validateCurrency()
{
//&& (event.keyCode != 44)
	if (event.keyCode<45 || event.keyCode>57)
	event.returnValue = false;
}


function validateDate()
{
//&& (event.keyCode != 44)
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 47))
	event.returnValue = false;
}

function validateDecimal()
{
//&& (event.keyCode != 44)
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	event.returnValue = false;
}

	function compareDate(stStr,endStr)
	{
	    if((stStr != "" && endStr != ""))
	    {
		    var stArray = stStr.split("/");
		    var endArray = endStr.split("/");
		    if(endArray[0].length<=1)
		    {
		      endArray[0]="0"+endArray[0];
		    }
		    
		    if(endArray[1].length<=1)
		    {
		      endArray[1]="0"+endArray[1];
		    }
		    
		    if(stArray[0].length<=1)
		    {
		      stArray[0]="0"+stArray[0];
		    }
		    
		    if(stArray[1].length<=1)
		    {
		      stArray[1]="0"+stArray[1];
		    }
	    
		    
		    if(endArray[2] < stArray[2])
		    {
		    	return false;   	
		    }
	
	    	if(endArray[0] < stArray[0] && endArray[2] <= stArray[2])
		    {
	    		return false;
		    }
		    else
		    {
			    if(endArray[1] < stArray[1] && endArray[2]<=stArray[2] && endArray[0]<=stArray[0])
	            {
		    		return false;
			    }
	    	    else(endArray[1] > stArray[1])
	            {  
		  			return true;
	
	            }  
		    }
		}
	}
	
	function replaceAll(Source,stringToFind,stringToReplace){

  var temp = Source;

    var index = temp.indexOf(stringToFind);

        while(index != -1){

            temp = temp.replace(stringToFind,stringToReplace);

            index = temp.indexOf(stringToFind);

        }

        return temp;

}
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
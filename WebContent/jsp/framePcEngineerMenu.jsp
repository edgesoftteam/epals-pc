<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.*,elms.app.common.*,elms.agent.*"%>
<%@page import="elms.control.actions.ApplicationScope"%>

<html:html>
<head>
<html:base/>
<title>City of Burbank : ePALS : PlanCheck</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
<% String contextRoot = request.getContextPath(); %>

<script language='javascript'>
contentUrl='<%=contextRoot%>/jsp/project/viewPlanChecks.jsp';
function scrollToPlanner(uid) {
	parent.f_content.location = contentUrl+'#'+uid;
}
</script>
</head>

<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg">

<table width="108%" border="0" cellspacing="0" cellpadding="0">
  <tr>
   <!--  <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="10" height="8"><font class="black2b">PC/Approvals Explorer</font></td> -->
    <td class="frametitle" style="padding-left: 15px"><b>PC/Approvals Explorer</b></td>
  </tr>
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table border="0" cellspacing="10" cellpadding="0">
              <!-- <tr>
                <td width="1%"><img src="images/spacer.gif" width="5" height="1"><img src="images/site_icon_inspector.gif" width="27" height="22"></td>
                <td valign="top"><img src="images/spacer.gif" width="1" height="5"><br>
                  <font class="treetext"><a href="<=contextRoot>/viewPlanners.do" class="tree1" target="_top">All Assigned</a></font></td>
              </tr>
               -->
                <tr>
                <td valign="top"><a href="<%=contextRoot%>/viewAllPlanCheck.do" class="treeuser-multi" target="_top">All PC/Approvers</a></td>
                <td>&nbsp;</td>
              </tr>
              <%
              List engineerList = (List) session.getAttribute("engineerList");
              System.out.println(engineerList);
              if(engineerList != null){
               for(int i=0;i<engineerList.size();i++){
            	   Map map = (Map) engineerList.get(i);
            	   String name = map.get("name").toString();
            	   String count = map.get("count").toString();
            	   String userId = map.get("id").toString();
 			{
            %>
              <tr>
                <td valign="top">
                  <%-- <a href='javascript:scrollToPlanner(<%=userId%>)' class="treeuser"><%=name%></a></td> --%>
                  <a href="<%=contextRoot%>/viewAllPlanCheck.do?engineerId=<%=userId%>" class="treeuser" target="_top"><%=name%></a></td>
                <td class="treelabel"><%=count%></td>
              </tr>
			 <%}}}%>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.gsearch.GlobalSearch"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="elms.util.StringUtils,java.util.*,elms.common.*,java.text.SimpleDateFormat"%>
<app:checkLogon/>
<html:html>
<head>
<script>
function popup(url)
{
 params  = 'width='+screen.width;
 params += ', height='+screen.height;
 params += ', top=0, left=0';
 params += ', fullscreen=no';

 newwin=window.open(url,'GEMS', params);
 if (window.focus) {newwin.focus()}
 return false;
}
function loadsq(){

	if(sq!=""){
	changeColor('sq');
	}
}

function gisbtn(){
	var browserName=navigator.appName;
	var gis = document.getElementById("gisbtn");
	if(gis==null)
	return;
	if (browserName=="Netscape"){
	gis.style.padding = "0 10px 7px 0";
	}else{
	if (browserName=="Microsoft Internet Explorer"){
	gis.style.padding = "0 10px 0 0";
	}else{
	gis.style.padding = "0 10px 7px 0";
	}
	}
	}

</script>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
<style>

#glsearch { }
input.glsearch { border: 0px; color: #000000; font-family: Arial, Helvetica, sans-serif; font-weight: bold; font-size: 16px; outline: none; background-color: #ccc; border-radius: 10px; box-shadow: inset 0px 0px 5px 0px #000000; padding: 10px;  background-repeat: no-repeat; background-position: right 5px center }
input.glsearch_on { color: #000000 !important; background-color: #ffffff !important }
</style>
</head>
<body text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/frames/top/background.gif" onload="gisbtn();loadsq();">
<%
	//get the context root.
    String contextRoot = request.getContextPath();
    boolean parking=false, addressTech=false,general=false,inspector = false,conditionTech=false, pentamationTech=false, userTech=false, peopleTech=false, projectTech=false, moveProjectTech=false, lsoTech=false, feesTech = false, addLandTech = false ,superAdmin=false, planner=false, admin=false ,inspectionManager=false, businessLicenseApproval=false, businessLicenseUser=false, businessTaxApproval=false, businessTaxUser=false, businessLicenseTaxUser=false;
    boolean planCheckManager = false;
    boolean planCheckReviewEngineer = false;
    boolean assessor = false;
    //get user from session.
    elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
    //get full name of the user
    if(user.getUsername().equalsIgnoreCase("edge")) superAdmin = true;
   	String fullName = user.getFullName()!=null?user.getFullName():"";
   
   	if(fullName!=null && fullName.length()>15){
   	fullName = fullName.substring(0, 15);
   	}
	//get the menu highlight parameter
	String hl = request.getParameter("hl")!=null ? request.getParameter("hl") :"home" ;
	//get role
	String roleDescription = "";
	roleDescription = user.getRole().getDescription();
	if(roleDescription == null) roleDescription = "";
	if(roleDescription.equals(Constants.ROLES_ADMINISTRATOR)) admin=true;
	//get department.
	String departmentCode = user.getDepartment().getDepartmentCode();
	int departmentId = user.getDepartment().getDepartmentId();
	//get security groups.
    java.util.List groups = (java.util.List) user.getGroups();
    java.util.Iterator itr = groups.iterator();
    while(itr.hasNext()){
        elms.security.Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true;
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
        if(group.groupId == Constants.GROUPS_GENERAL) general = true;
        if(group.groupId == Constants.GROUPS_INSPECTOR) inspector = true;
        if(group.groupId == Constants.GROUPS_INSPECTION_MANAGER) inspectionManager = true;
        if(group.groupId == Constants.GROUPS_PLANNER) planner = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_PARKING) parking = true;
		if((group.groupId == Constants.GROUPS_PLAN_REVIEW_ENGINEER)) {
			planCheckReviewEngineer = true;
		}
		if((group.groupId == Constants.GROUPS_PLAN_CHECK_MANAGER)){
			planCheckManager = true;
		}
		 if(group.groupId == Constants.GROUPS_ASSESSOR_DATA_MAINTENANCE) assessor = true;
	}
    
    String sq = request.getParameter("sq")!=null?request.getParameter("sq"):"";
    String deltaIndex= StringUtils.nullReplaceWithEmpty(GlobalSearch.getKeyValue("SOLR_DELTA_INDEX"));
 %>

<script>
var sq ="<%=sq%>";
function bookMarkDisplay(){ 
	parent.location.href="<%=contextRoot%>/jsp/gsearch/frameBookmark.jsp";
}


function changeColor(id){
	document.getElementById(id).style.backgroundColor = "#ffffff";
}


function changeColorBlur(id){
	document.getElementById(id).style.backgroundColor = "#ccc"; 
}



function funSearch(){ 
	var val = document.all.sq.value;
	parent.location.href="<%=contextRoot%>/jsp/gsearch/bigdata.jsp?sq="+val;
	}
	
function funQuickSearch(){	
	var val = document.all.textfield2.value;
	parent.f_content.location.href='<%=contextRoot%>/viewQuickSearch.do?searchVal='+val;
}

function displayKeyCode(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 13){
    	funSearch();		
    } 
}
function displayQuickKeyCode(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 13){
    	funQuickSearch();		
    } 
}
</script>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
 <%if(!deltaIndex.equals("") && deltaIndex.equalsIgnoreCase("YES")){ %>
    <tr>
        <td valign="top" width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              
                <td valign="top">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    <td  bgcolor="#A9A9A9"><img src="images/spacer.gif" width="15" height="1"></td>
                    <td bgcolor="#A9A9A9" ><img src="images/site_logo_shield.png" width="33" height="35"></td>
                    <td  bgcolor="#A9A9A9"><img src="images/spacer.gif" width="10" height="1"></td>
                        <td bgcolor="#A9A9A9"> <img src="images/citylogo.png"  width="170" height="43"></td>
                    </tr>
                    <tr>
                    
<%--                         <td bgcolor="#808080" colspan="3" width="99%" align="center" ><font class="white2b">   <%=fullName%></font></td> --%>
                        <!-- <td bgcolor="#808080" width="1" colspan="1"><img src="images/site_bg_cccccc.jpg"  width="1" height="20"></td> -->
                        <td bgcolor="#808080" colspan="4" width="99%" align="center" ><font class="white2b"><%=fullName%> | 



                  <script>


function makeArray() {
  var args = makeArray.arguments;
  for (var i = 0; i < args.length; i++) {
this[i] = args[i];
  }
  this.length = args.length;
}

function fixDate(date) {
  var base = new Date(0);
  var skew = base.getTime();
  if (skew > 0)
date.setTime(date.getTime() - skew);
}

function getString(date) {
  var months = new makeArray("January", "February", "March",
"April",   "May",      "June",
"July",    "August",   "September",
"October", "November", "December");
  return months[date.getMonth()] + " " +
date.getDate() + ", " + ( date.getFullYear());
}

function changeColor(id)
{
	/* alert("hi"); */
	document.getElementById(id).style.backgroundColor = "#ffffff";
 
}


function changeColorBlur(id)
{
	 /* alert("changeColorBlur");  */
	document.getElementById(id).style.backgroundColor = "#ccc";
 
}

var cur = new Date();
fixDate(cur);
var str = getString(cur);
document.write(str);
</script> </font></td>
                        <td bgcolor="#555555" width="1%"><img src="images/spacer.gif" width="1" height="29"></td>
                    </tr>
                </table>
                </td>

            </tr>
        </table>
        </td>
        <td valign="top" width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td colspan="2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
              
              
                
             
                
                
                <!-- Quick Search  -->  
                 <!-- <td background="images/site_top_bg.jpg"  nowrap="nowrap"> -->
                 <td  bgcolor="#555555" nowrap="nowrap">
              <!-- <div align="left" nowrap><img src="images/spacer.gif" width="1" height="10"><br>-->
              <!-- <font style="font-family:arial;font-size:12px;font-weight:bold;color:#7393A8">Quick-Search</font> -->
              <!-- <font class="white1"><%=fullName%> |</font> -->
              <%//if(!miFireHosted.equalsIgnoreCase("Y")){ %> 
              <!--
              <a href="<%=request.getContextPath()%>/docs/release_notes.txt" target="_blank" title="<%=Version.getDate()%>"><font class="white1">v <%=Version.getNumber()%>&nbsp;</font></a>
              <input type="text" id="textfield2"  size="16" style="background-color:#778ca1;border:1px solid #67819A; color:#FFFFFF; font-family: Arial; font-size: 11px; font-weight:bold" 
              title="Search for an activity number, apn" onkeypress="displayKeyCode(event);"> 
             
        <input type="button" name="search" value="Find" class="button" style="border:1px solid #7393A8; background-color:#7393A8; cursor: hand" onclick="funSearch();">&nbsp;&nbsp;&nbsp;&nbsp; -->
        <!--  &nbsp; -->
        <!-- <input type="text" id="sq" class="glsearch"   name="sq" size="16" style="width: calc(95%)" onblur="changeColorBlur('sq');" onclick="changeColor('sq');"  onkeypress="displayKeyCode(event);">  -->
     	
       <span class="glyphicon glyphicon-search form-control-feedback">
<!-- 	    <font class="white" color="#FFFFFF" size=6> &emsp;SAIRA&emsp;</font> -->
       <img id="view_bookmark" src="<%=contextRoot%>/jsp/images/icons/glsearch/saira.png" align="center" width="149" height="35" title="ASK SAIRA" style="cursor: pointer;">
        <input type="text" id="sq" class="glsearch" value="<%=sq %>"   name="sq" size="10" style="width: calc(80%);height: 35px; background-image: url(<%=request.getContextPath()%>/jsp/images/icons/dark/search.png); background-repeat: no-repeat;"  onblur="changeColorBlur('sq');" onclick="changeColor('sq');"  onkeypress="displayKeyCode(event);" >     
	
	<i class="fa fa-search"></i>
	</span>

									
        <%//}else{
        	%>
        <font class="white1">v <%=Version.getNumber()%>&nbsp;</font>
        <img width="0" height="27">
          <img id="view_bookmark" src="<%=contextRoot%>/jsp/images/icons/controls/white/bookmark.png" align="center" width="30" height="27" title="Manage Bookmark" style="cursor: pointer;"  onclick="bookMarkDisplay();">
     
     		
     		
        <%//} %>
        </div>
        
        </td>
         <td  bgcolor="#555555" width="20" nowrap="nowrap"></td>

            </tr>
    
    
     <tr> <td  bgcolor="#555555" nowrap="nowrap" colspan="2" height="3">  </td></tr>
    <tr>
    <td valign="middle" colspan="2"  bgcolor="#808080" background="images/site_nav_bg.jpg"  style="border-bottom: 1px solid #c9c9c9">
        <!-- <td valign="middle" colspan="2" bgcolor="#808080" style="border-bottom: 1px solid #c9c9c9"> -->
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td bgcolor="#808080" >
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="middle"> 
                        
                        <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="58" align="center">
                        	<% if(hl.equals("home")) { %>
                        	 <font class="blue2b">   <a href=javascript:parent.location.href='<%=contextRoot%>/viewHome.do' class="navon"><font class="blue1b"><b> Home</b></font></a></font>
                        	<% } else { %>
                        	<font class="blue2b"><a href=javascript:parent.location.href='<%=contextRoot%>/viewHome.do' class="nav"><font class="white1"><b> Home</b></font></a></font>
                        	<% } %>
                        </td>
                        
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="69" align="center">
                        	<% if(hl.equals("search")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/search.jsp'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=search'" class="navon"><font class="blue1b"><b>Search</b></font> </a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/search.jsp'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=search'" class="nav"><font class="white1"><b>Search</b></font></a>
                        	<% } %>
                        </td>
						<%if(assessor){ %>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="79" align="center">
                        	<% if(hl.equals("assessor")) { %>
                        	    <a href="javascript:parent.f_content.location.href='<%=contextRoot%>/assessorData.do?action=search'; parent.f_top.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=assessor'"  class="navon"><font class="blue1b"><b>Assessor</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.f_content.location.href='<%=contextRoot%>/assessorData.do?action=search'; parent.f_top.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=assessor'"  class="nav"><font class="white1"><b> Assessor</b></font></a>
                        	<% } %>
                        </td>
                          <% } %>
                        <% if(departmentId==Constants.DEPARTMENT_PUBLIC_WORKS){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="40" align="center">
                        	<% if(hl.equals("activity")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=activity'"  class="navon"><font class="blue1b"><b>PW</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=activity'"  class="nav"><font class="white1"> <b>PW</b></font></a>
                        	<% } %>
                        </td>
                        <% } %>

						<% if(parking){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="70" align="center">
                        	<% if(hl.equals("parking")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewParkingActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=parking'"  class="navon"><font class="blue1b"><b>Parking</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewParkingActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=parking'"  class="nav"><font class="white1"><b>Parking</b></font></a>
                        	<% } %>
                        </td>
                  <% } %>
                  
                  
                   <% if(planCheckManager || planCheckReviewEngineer){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="89" align="center">
                        	<% if(hl.equals("planCheck")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllPlanCheck.do?action=initialOne'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planCheck'"  class="navon"><font class="blue1b"><b>Plan Check</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllPlanCheck.do?action=initialOne'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planCheck'"  class="nav"><font class="white1"><b>Plan Check</b></font></a>
                        	<% } %>
                        </td>
                       
 					<% } %>
 					
 					
                        <% if(departmentCode.equalsIgnoreCase("BS") || departmentCode.equalsIgnoreCase("PW")){%>
                        <% if(inspectionManager || inspector){ %>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="89" align="center">
                        	<% if(hl.equals("inspections")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllInspection.do'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=inspections'"  class="navon"><font class="blue1b"><b>Inspections</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllInspection.do'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=inspections'"  class="nav"><font class="white1"><b>Inspections</b></font></a>
                        	<% } %>
                        </td>
                        <% }} %>
 						
 						<% if(departmentCode.equalsIgnoreCase("PL")){%>
 						<%if(planner){ %>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="89" align="center">
                        	<% if(hl.equals("planners")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewPlanners.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planners'" class="navon"><font class="blue1b"><b>Planners</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewPlanners.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planners'" class="nav"><font class="white1"><b>Planners</b></font></a>
                        	<% } %>
                        </td>
 						<% }}%>
 						
 						<% if(businessLicenseApproval){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="90" align="center">
                        	<% if(hl.equals("blapproval")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/blApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=blapproval'" class="navon"><font class="blue1b"><b>BL Approvals</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/blApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=blapproval'" class="nav"><font class="white1"><b>BL Approvals</b></font></a>
                        	<% } %>
                        </td>
 						<% }%>
						
 						<% if(businessTaxApproval){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="90" align="center">
                        	<% if(hl.equals("btapproval")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/btApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=btapproval'" class="navon"><font class="blue1b"><b>BT Approvals</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/btApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=btapproval'" class="nav"><font class="white1"><b>BT Approvals</b></font></a>
                        	<% } %>
                        </td>
 						<% }%>

 						<% if(businessLicenseUser && !businessTaxUser){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="120" align="center">
                        	<% if(hl.equals("businesslicense")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessLicense.jsp'" class="navon"><font class="blue1b"><b>Business License</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessLicense.jsp'" class="nav"><font class="white1"><b>Business License</b></font></a>
                        	<% } %>
                        </td>
 						<% }%>

 						<% if(businessTaxUser && !businessLicenseUser){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="120" align="center">
                        	<% if(hl.equals("businesslicense")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="navon"><font class="blue1b"><b>Business Tax</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="nav"><font class="white1"><b>Business Tax</b></font></a>
                        	<% } %>
                        </td>
 						<% }%>

 						<% if(businessLicenseUser && businessTaxUser ){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="95" align="center">
                        	<% if(hl.equals("businesslicense")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="navon"><font class="blue1b"><b>Add BT / BL</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="nav"><font class="white1"><b>Add BT / BL</b></font></a>
                        	<% } %>
                        </td>
 						<% }%>
 						<%if(admin){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="60" align="center">
                        	<html:link href="<%=elms.agent.LookupAgent.getReportsURL()%>" target="_new" styleClass="nav"><font class="white1"><b>Reports</b></font></html:link>
                        </td>

                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="40" align="center">
                        	<a href="<%=contextRoot%>/jsp/gis/gems.jsp" target="_new" class="nav"><font class="white1"><b>GIS</b></font></a>
                        </td>
						
		    			<td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="55" align="center">
                        	<% if(hl.equals("admin")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAdmin.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=admin'" class="navon"><font class="blue1b"><b>Admin</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAdmin.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=admin'" class="nav"><font class="white1"><b>Admin</b></font></a>
                        	<% } %>
                        </td>
                        <% } %>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="65" align="center">
                        	<a href="http://support.edgesoftinc.com" target="_new" class="nav"><font class="white1"><b>Support</b></font></a>
                        </td>
                     
                        <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="60" align="center">
                        	<% if(hl.equals("logout")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/logout.do'" class="navon"><font class="blue1b"><b>Logout</b></font></a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/logout.do'" class="nav"><font class="white1"><b>Logout</b></font></a>
                        	<% } %>
                        </td>
                        
                        <td width="1" align="center" ><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
    <td valign="middle"  colspan="2" bgcolor="#808080"><br><br><br><br><br><br></td>
        <!-- <td valign="middle"  colspan="2" bgcolor="#e5e5e5"><br><br><br><br><br><br></td> -->
    </tr>
    </table></td></tr></table></td></tr>
    
    <%}else{ %>
     <tr>
        <td valign="top" colspan="2" background="images/frames/top/ShadowBottom.png"><img src="images/spacer.gif" width="1" height="5"></td>
    </tr>
    <tr>
        <td valign="top">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><img src="images/spacer.gif" width="10" height="1"></td>
                <td><img src="images/site_logo_shield.png" width="33" height="35"></td>
                <td><img src="images/spacer.gif" width="10" height="1"></td>
                <td><img src="images/citylogo.png" width="413" height="35"></td>
            </tr>
        </table>
        </td>
        <td align="right">
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td width="250" align="right"><font class="white1" ><%=fullName.toUpperCase() %></font> | <a href="<%=request.getContextPath()%>/docs/release_notes.txt" target="_blank" title="<%=Version.getDate()%>"><font class="white1">v <%=Version.getNumber()%>&nbsp;&nbsp;</font></a>&nbsp;</td>
                    <td width="11"><img src="images/frames/top/QuickSearchLeft.png" width="11" height="20"></td>
                    <td width="100" background="images/frames/top/QuickSearchBackground.png"><input type="text" id="textfield2" size="16" style="background-color:transparent; color:#808080; border: 0px; font-family: Arial; font-size: 11px; font-weight:bold" title="Search for activities by typing an activity number in part or full" onFocus="this.select()" onkeypress="displayQuickKeyCode(event);"></td>
                    <td width="18"><img src="images/frames/top/QuickSearchRight.png" width="18" height="20" border="0" onclick="funQuickSearch();"></td>
                    <td width="10"><img src="images/spacer.gif" width="10" height="1"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="middle" colspan="2" background="images/frames/top/ShadowTop.png"><img src="images/spacer.gif" width="1" height="5"></td>
    </tr>
    <tr>
        <td valign="middle" colspan="2" bgcolor="#e5e5e5"  style="border-bottom: 1px solid #c9c9c9">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr valign="middle"> 
                        
                        <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="58" align="center">
                        	<% if(hl.equals("home")) { %>
                        	    <a href=javascript:parent.location.href='<%=contextRoot%>/viewHome.do' class="navon">Home</a>
                        	<% } else { %>
                        	    <a href=javascript:parent.location.href='<%=contextRoot%>/viewHome.do' class="nav">Home</a>
                        	<% } %>
                        </td>
                        
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="69" align="center">
                        	<% if(hl.equals("search")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/search.jsp'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=search'" class="navon">Search</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/search.jsp'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=search'" class="nav">Search</a>
                        	<% } %>
                        </td>

                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="79" align="center">
                        	<% if(hl.equals("assessor")) { %>
                        	    <a href="javascript:parent.f_content.location.href='<%=contextRoot%>/assessorData.do?action=search'; parent.f_top.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=assessor'"  class="navon">Assessor</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.f_content.location.href='<%=contextRoot%>/assessorData.do?action=search'; parent.f_top.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=assessor'"  class="nav">Assessor</a>
                        	<% } %>
                        </td>
                        
                        <% if(departmentId==Constants.DEPARTMENT_PUBLIC_WORKS){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="40" align="center">
                        	<% if(hl.equals("activity")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=activity'"  class="navon">PW</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=activity'"  class="nav">PW</a>
                        	<% } %>
                        </td>
                        <% } %>

						<% if(parking){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="70" align="center">
                        	<% if(hl.equals("parking")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewParkingActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=parking'"  class="navon">Parking</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewParkingActivityTab.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=parking'"  class="nav">Parking</a>
                        	<% } %>
                        </td>
      	            <% } %>
 					<% if(planCheckManager || planCheckReviewEngineer){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="89" align="center">
                        	<% if(hl.equals("planCheck")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllPlanCheck.do?action=initialOne'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planCheck'"  class="navon">Plan Check</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllPlanCheck.do?action=initialOne'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planCheck'"  class="nav">Plan Check</a>
                        	<% } %>
                        </td>
                       
 					<% } %>
                        <% if(departmentCode.equalsIgnoreCase("BS") || departmentCode.equalsIgnoreCase("PW")){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="89" align="center">
                        	<% if(hl.equals("inspections")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllInspection.do'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=inspections'"  class="navon">Inspections</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAllInspection.do'; self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=inspections'"  class="nav">Inspections</a>
                        	<% } %>
                        </td>
                        <% } %>
 						
 						<% if(departmentCode.equalsIgnoreCase("PL")){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="89" align="center">
                        	<% if(hl.equals("planners")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewPlanners.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planners'" class="navon">Planners</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewPlanners.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=planners'" class="nav">Planners</a>
                        	<% } %>
                        </td>
 						<% }%>
 						
 						<% if(businessLicenseApproval){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="90" align="center">
                        	<% if(hl.equals("blapproval")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/blApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=blapproval'" class="navon">BL Approvals</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/blApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=blapproval'" class="nav">BL Approvals</a>
                        	<% } %>
                        </td>
 						<% }%>
						
 						<% if(businessTaxApproval){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="90" align="center">
                        	<% if(hl.equals("btapproval")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/btApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=btapproval'" class="navon">BT Approvals</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/btApproval.jsp';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=btapproval'" class="nav">BT Approvals</a>
                        	<% } %>
                        </td>
 						<% }%>

 						<% if(businessLicenseUser && !businessTaxUser){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="120" align="center">
                        	<% if(hl.equals("businesslicense")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessLicense.jsp'" class="navon">Business License</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessLicense.jsp'" class="nav">Business License</a>
                        	<% } %>
                        </td>
 						<% }%>

 						<% if(businessTaxUser && !businessLicenseUser){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="120" align="center">
                        	<% if(hl.equals("businesslicense")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="navon">Business Tax</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="nav">Business Tax</a>
                        	<% } %>
                        </td>
 						<% }%>

 						<% if(businessLicenseUser && businessTaxUser ){%>
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="95" align="center">
                        	<% if(hl.equals("businesslicense")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="navon">Add BT / BL</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/jsp/viewBusinessTax.jsp'" class="nav">Add BT / BL</a>
                        	<% } %>
                        </td>
 						<% }%>

                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="60" align="center">
                        	<html:link href="<%=elms.agent.LookupAgent.getReportsURL()%>" target="_new" styleClass="nav">Reports</html:link>
                        </td>

                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="40" align="center">
                        	<a href="<%=contextRoot%>/jsp/gis/gems.jsp" target="_new" class="nav">GIS</a>
                        </td>

						<%if(admin){%>
		    			<td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="55" align="center">
                        	<% if(hl.equals("admin")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAdmin.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=admin'" class="navon">Admin</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/viewAdmin.do';self.location.href='<%=contextRoot%>/jsp/frameTop.jsp?hl=admin'" class="nav">Admin</a>
                        	<% } %>
                        </td>
                        <% } %>
                        
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="65" align="center">
                        	<a href="http://support.edgesoftinc.com" target="_new" class="nav">Support</a>
                        </td>
                        
                     	
                        <td width="1"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        <td width="60" align="center">
                        	<% if(hl.equals("logout")) { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/logout.do'" class="navon">Logout</a>
                        	<% } else { %>
                        	    <a href="javascript:parent.location.href='<%=contextRoot%>/logout.do'" class="nav">Logout</a>
                        	<% } %>
                        </td>
                        
                        <td width="1" align="center"><img src="images/site_bg_cccccc.jpg" width="1" height="20"></td>
                        
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td valign="middle" colspan="2" bgcolor="#e5e5e5"><br><br><br><br><br><br></td>
    </tr>
    <%} %>
    
</table>
</body>
</html:html>

 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
String contextRoot = request.getContextPath();
CachedRowSet crset = (CachedRowSet) request.getAttribute("dlLogs");
int size = crset.size();
if(crset !=null) crset.beforeFirst();
String message = (String)request.getAttribute("message");
if(message==null) message = "";
%>
<html:html>
<HEAD>
<html:base/>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="javascript">

function deleteRecord(recordId) {
   userInput = confirm("Are you sure you want to delete record number "+recordId+" ?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/digitalLibraryLogs.do?action=delete&recordId='+recordId;
   }
}

function deleteAllRecords() {
   userInput = confirm("Are you sure you want to delete all Records?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/digitalLibraryLogs.do?action=delete';
   }
}

</script>


</HEAD>

<BODY >

<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">Digital Library Exception Log</FONT><BR>
            <BR>
            </TD>
        </TR>
        <TR>
        <td>
        <font color='green'><b><%=message%></</b>
        </td>
        </tr>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Exception messages (<%=size%> Messages found)</TD>
                      <TD width="1%" class="tablebutton"><NOBR>

                      <a href="javascript:deleteAllRecords();">Clear Logs</a>

                      </NOBR></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Record ID</TD>
                      <TD class="tablelabel">Date</TD>
                      <TD class="tablelabel">File Name</TD>
                      <TD class="tablelabel">Error Message</TD>
                      <TD class="tablelabel">Delete</TD>
                    </TR>
 <%
   					while (crset.next()) {
   						String fileName = crset.getString("FILE_NAME");
   						String exceptionMessage = crset.getString("EXCEPTION_MESSAGE");
   						String created = crset.getString("CREATED");
   						String recordId = crset.getString("ID");
 %>
            	    <TR valign="top">
	 					<TD class="tabletext">
             			<%=recordId %>

             			</TD>

	 					<TD class="tabletext">
             			<%=created %>

             			</TD>

 	 					<TD class="tabletext">
             			<%=fileName %>

             			</TD>

 	 					<TD class="tabletext">
             			<%=exceptionMessage %>

             			</TD>

 	 					<TD class="tabletext">
             			<a href="javascript:deleteRecord(<%=recordId%>);"><img src="../images/delete.gif" alt="Delete" border="0"></a>

             			</TD>


					</TR>
 <%
 					}
 					if(crset!=null) crset.close();
 %>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
    <TD width="1%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD height="32">&nbsp;</TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>

</BODY>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script> <script>checkBrowser();</script> <script src="../script/yCode_combo_box.js"> </script>
<%String contextRoot = request.getContextPath();%>
<script language="JavaScript">
var strValue;
function validateFunction() {
	strValue=false;
	if (document.forms['conditionsLibraryForm'].elements['levelType'].value == '') {
		alert("Select Level type");
		document.forms['conditionsLibraryForm'].elements['levelType'].focus();
		strValue=false;
	}else if (document.forms['conditionsLibraryForm'].elements['conditionType'].value == '') {
		alert("Enter Condition Type");
		document.forms['conditionsLibraryForm'].elements['conditionType'].focus();
		strValue=false;
	}else if (document.forms['conditionsLibraryForm'].elements['conditionCode'].value == '') {
		alert("Enter Condition Code");
		document.forms['conditionsLibraryForm'].elements['conditionCode'].focus();
		strValue=false;
	}else if (document.forms['conditionsLibraryForm'].elements['conditionDesc'].value == '') {
		alert("Select Description");
		document.forms['conditionsLibraryForm'].elements['conditionDesc'].focus();
		strValue=false;
	}else{
	   	document.forms[0].action='<%=contextRoot%>/conditionsLibAdmin.do?save=Yes';
		document.forms[0].submit();
    }
}
function getConditionType() {
       document.forms[0].action='<%=contextRoot%>/conditionsLibAdmin.do?ctype=Yes';
       document.forms[0].submit();
}
function getLibCondition(id) {
       document.forms[0].action='<%=contextRoot%>/conditionsLibAdmin.do?id='+id;
       document.forms[0].submit();
}
function lookup() {
       document.forms[0].action='<%=contextRoot%>/conditionsLibAdmin.do?lookup=Yes';
       document.forms[0].submit();
}
function refresh() {
     document.forms[0].action='<%=contextRoot%>/conditionsLibAdmin.do';
     document.forms[0].submit();
}
function doDelete() {
     document.forms[0].action='<%=contextRoot%>/conditionsLibAdmin.do?delete=Yes';
     document.forms[0].submit();
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form focus="levelType" name="conditionsLibraryForm" type="elms.control.beans.admin.ConditionsLibraryForm" action="/conditionsLibAdmin">
<html:hidden property="conditionId"/>
<logic:notEqual value="YES" property="displayConditions" name="conditionsLibraryForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Conditions Library Administration</font><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <html:errors/>
		                    <tr>
		                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

		                        <td width="1%" class="tablebutton"><nobr>
		                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
		                          <html:reset value="Reset" styleClass="button" onclick="return refresh()"/>
		                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="lookup()"></html:button>
		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">
                            <tr>
                                <td class="tablelabel"><nobr>Level Type</nobr></td>
                                <td class="tabletext">
                                   <html:select property="levelType" styleClass="textbox" onchange="getConditionType()">
                        	          <html:option value="">Please Select</html:option>
                                      <html:option value="L">Land</html:option>
                                      <html:option value="S">Structure</html:option>
                                      <html:option value="O">Occupancy</html:option>
                                      <html:option value="P">Project</html:option>
                                      <html:option value="Q">Sub Project</html:option>
                                      <html:option value="A">Activity</html:option>
                                   </html:select>
                                </td>
                                <td class="tablelabel">Condition Type</td>
                                <td class="tabletext">
                                   <html:select property="conditionType" styleClass="textbox">
                                      <html:option value="">Please Select</html:option>
                                      <logic:equal value="L" property="levelType" name="conditionsLibraryForm">
                        	            <html:options collection="lsoUses" property="id" labelProperty="description" />
                        	          </logic:equal>
                                      <logic:equal value="S" property="levelType" name="conditionsLibraryForm">
                        	            <html:options collection="lsoUses" property="id" labelProperty="description" />
                        	          </logic:equal>
                                      <logic:equal value="O" property="levelType" name="conditionsLibraryForm">
                        	            <html:options collection="lsoUses" property="id" labelProperty="description" />
                        	          </logic:equal>
                                      <logic:equal value="P" property="levelType" name="conditionsLibraryForm">
                        	            <html:options collection="projectTypes" property="projectNameId" labelProperty="description" />
                        	          </logic:equal>
                                      <logic:equal value="Q" property="levelType" name="conditionsLibraryForm">
                        	            <html:options collection="subProjectTypes" property="subProjectTypeId" labelProperty="description" />
                        	          </logic:equal>
                                      <logic:equal value="A" property="levelType" name="conditionsLibraryForm">
                        	            <html:options collection="activityTypes" property="type" labelProperty="departmentDescription" />
                        	          </logic:equal>
                                   </html:select>
                                </td>
                                <td class="tablelabel"><nobr>Condition Code</nobr></td>
                                <td class="tabletext"> <html:text size="20" property="conditionCode" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/> &nbsp;
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Inspectable ?</td>
                                <td class="tabletext" valign="top">
                                  <html:select property="inspectability" styleClass="textbox">
				    <html:option value=""></html:option>
				    <html:option value="Y">Yes</html:option>
				    <html:option value="N">No</html:option>
				  </html:select>
                                </td>
                                <td class="tablelabel" valign="top"><nobr>Display Warning ?</nobr></td>
                                <td class="tabletext" valign="top">
                                   <html:select property="warning" styleClass="textbox">
				    <html:option value=""></html:option>
				    <html:option value="Y">Yes</html:option>
				    <html:option value="N">No</html:option>
				  </html:select>
				</td>
                                <td class="tablelabel" valign="top">Standard ?</td>
                                <td class="tabletext" valign="top">
                                  <html:select property="required" styleClass="textbox">
				    <html:option value=""></html:option>
				    <html:option value="Y">Yes</html:option>
				    <html:option value="N">No</html:option>
				  </html:select>
				</td>
                            </tr>
                            <tr>
                                <td class="tablelabel"><nobr>Description</nobr></td>
                                <td class="tabletext" colspan="5">
                                   <html:select property="conditionDesc" styleClass="textbox">
                        	      <html:option value="">Please Select</html:option>
                                      <html:option value="C">Construction</html:option>
                                      <html:option value="O">Operational</html:option>
                                      <html:option value="N">Other</html:option>
                                   </html:select>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel"><nobr>Title</nobr></td>
                                <td class="tabletext" colspan="5"><html:text size="125" property="shortText" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" colspan="6">Text</td>
                            </tr>
                            <tr>
                                <td class="tabletext" colspan="6"><html:textarea property="conditionText" cols="90" rows="20"/></td>
                            </tr>

                       </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</logic:notEqual>

<logic:equal value="YES" property="displayConditions" name="conditionsLibraryForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Conditions Library List</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="95%" height="25" class="tabletitle">Conditions Library</td>

                      <td width="1%" class="tablebutton"><nobr>
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                       <html:button value="Delete" property="delete" styleClass="button" onclick="doDelete()"/>
                       <html:button value="Add" property="add" styleClass="button" onclick="refresh()"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td width="5%" class="tablelabel">Delete</td>
                      <td width="40%" class="tablelabel">Title</td>
                      <td width="15%" class="tablelabel">Condition Level</td>
                      <td width="15%" class="tablelabel"><nobr>Condition Type</nobr></td>
                      <td width="15%" class="tablelabel">Condition Code</td>
                    </tr>
                    <logic:iterate id="condition" property="conditions" name="conditionsLibraryForm">
                      <tr class="tabletext">
                        <logic:equal value="YES" property="removable" name="condition">
                          <td class="tabletext" align="right"><html:multibox property="selectedItems"><bean:write name="condition" property="libraryId"/></html:multibox></td>
                        </logic:equal>
                        <logic:equal value="NO" property="removable" name="condition">
                          <td class="tabletext">&nbsp;</td>
                        </logic:equal>
                        <td class="tabletext" >

                            <a  href="<%=contextRoot%>/conditionsLibAdmin.do?id=<bean:write name="condition" property="libraryId"/>" >
                               <bean:write name="condition" property="short_text" />
                            </a>

                        </td>
                        <td class="tabletext"><bean:write name="condition" property="levelType" /></td>
                        <td class="tabletext"><bean:write name="condition" property="conditionType" /></td>
                        <td class="tabletext"><bean:write name="condition" property="conditionCode" /></td>
                      </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:equal>
</html:form>
</body>
</html:html>


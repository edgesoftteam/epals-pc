<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>

<app:checkLogon/>


<%@page import="java.util.ArrayList"%>
<%@page import="elms.control.beans.admin.OnlinePermitForm"%>

<%@page import="elms.util.StringUtils"%><html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"> </script>
<%
	//the context path
   	String contextRoot = request.getContextPath();
	elms.control.beans.admin.OnlinePermitForm onlinePermitForm = (elms.control.beans.admin.OnlinePermitForm)request.getAttribute("onlinePermitForm");
	ArrayList a = new ArrayList();
	a = (ArrayList)request.getAttribute("lkupQuestionaireList");
%>
<script language="JavaScript">

var actType = "<%=onlinePermitForm.getActType()%>";
var stypeId = "<%=onlinePermitForm.getStypeId()%>";
var feeId = "<%=onlinePermitForm.getFeeId()%>";
var planCheck = "<%=onlinePermitForm.getPlanCheck()%>";


var str = "";
var strCode = "";
var strDesc = "";
var delimiter = "";
var delimiter1 = "";
function selectActType() {
	var size= document.forms[0].elements['feeId'].length;
	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['feeId'].options[i].selected) {
			str += delimiter + document.forms[0].elements['feeId'].options[i].text;
			var text = document.forms[0].elements['feeId'].options[i].text;
			var value = document.forms[0].elements['feeId'].options[i].value;
			var op = new Option(text,value);
			document.forms[0].elements['selectedFeeId'].add(op,document.forms[0].elements['selectedFeeId'].options.length);
			delimiter = "\r";
			document.forms[0].elements['feeId'].remove(i);
			i--;
			size--;
		}
	}
}

function deselectActType() {
	var size= document.forms[0].elements['selectedFeeId'].length;
	for (var i=0;i<size;i++) {
		if (document.forms[0].elements['selectedFeeId'].options[i].selected) {
			var text = document.forms[0].elements['selectedFeeId'].options[i].text;
			var value = document.forms[0].elements['selectedFeeId'].options[i].value;
			var op = new Option(text,value);
			document.forms[0].elements['feeId'].add(op,document.forms[0].elements['feeId'].options.length);
			document.forms[0].elements['selectedFeeId'].remove(i);
			i--;
			size--;
		}
	}
}

function loadActFees() {
    document.forms[0].action='<%=contextRoot%>/onlineQuestionaire.do?action=edit';
    document.forms[0].submit();
}

function clearFields(){
	str = "";
	strDesc = "";
	strCode = "";
	document.forms[0].stypeId.value=-1;
	document.forms[0].feeId.value="";
	document.forms[0].questionId.value="";
	document.forms[0].actType.value="";
    document.forms[0].action='<%=contextRoot%>/onlineQuestionaire.do';
    document.forms[0].submit();

}

function validateFunction() {

	var checked = document.forms[0].planCheck.checked;
	  if (checked){
		  document.forms[0].planCheck.value ="Y";
	  }else {
		  document.forms[0].planCheck.value ="N";
	  }
	
	if(document.forms[0].actType.value ==""){
		alert("Please select a Activity Type");
		document.forms[0].stypeId.focus();
		return false;
	}
	
	if(document.forms[0].stypeId.value ==""){
		alert("Please select a Activity SubType");
		document.forms[0].stypeId.focus();
		return false;
	}
	if(document.forms[0].feeId.value ==""){
		alert("Please select a Fee");
		document.forms[0].stypeId.focus();
		return false;
	}

	if(document.forms[0].questionId.value ==""){
		alert("Please select a Question");
		document.forms[0].stypeId.focus();
		return false;
	}
	
	document.forms[0].action='<%=contextRoot%>/onlineQuestionaire.do?action=save';
    document.forms[0].submit();
}

function lkup() {
	document.forms[0].action='<%=contextRoot%>/onlineQuestionaire.do?action=lkup';
    document.forms[0].submit();
}

function del() {
	document.forms[0].action='<%=contextRoot%>/onlineQuestionaire.do?action=del';
    document.forms[0].submit();
}
function getSubTypes() {
	if(document.forms[0].actType.value !=""){
		document.forms[0].action='<%=contextRoot%>/onlineQuestionaire.do?action=dropdown';
    	document.forms[0].submit();
	}
}

function frmLoad(){
	
	

	document.forms[0].actType.value = actType;
	document.forms[0].stypeId.value = stypeId;
	document.forms[0].feeId.value = feeId;

	if(planCheck=='Y'){
		document.forms[0].planCheck.checked = true;
	}
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="frmLoad()">
<html:form action="/onlineQuestionaire">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	                <td><font class="con_hdr_3b">Online Questionaire Mapping</font><br><br></td>
	            </tr>
	            <tr>
	            <td>
		            <html:errors/>
		            <logic:present name="message" scope="request" >
			            <font color='green'><bean:write name="message"/></font>
		            </logic:present>
	            </td>
	            </tr>
				<tr>
	                <td>
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                        <td>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    <html:errors property="error"/>
			                    <tr>
			                      <td width="99%" class="tabletitle">Add/Update</td>

			                        <td width="1%" class="tablebutton"><nobr>
			                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="return clearFields();"></html:button>
			                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
			                          &nbsp;</nobr></td>
			                      </tr>
	                        </table>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td background="../images/site_bg_B7C1CB.jpg">
	                          <table width="100%" cellpadding="2" cellspacing="1">
								<tr>
									<td class="tablelabel"><nobr>Activity Type</nobr></td>
                                	<td class="tabletext" colspan="3">
                                	<html:select property="actType" styleClass="textbox" onchange="getSubTypes();" >
                                			<option value="" > Please Select</option>
											<option value="200200" > Single-Family Residential</option>
											<option value="200201" > Duplex, Apartment, Condomimium, Townhouse</option>
											<option value="200202" > Commercial/ Industrial</option>
									</html:select>
                                	<!-- <bean:write name="onlinePermitForm" property="actType"/> -->
                                	</td>
									<td class="tablelabel">Activity Sub-Type</td>
		                            <td class="tabletext" colspan="2">
		                               <html:select property="stypeId" styleClass="textbox" onchange="loadActFees()">
		                    	          <html:option value="-1">Please Select</html:option>
		                                  <html:options collection="actSubtypeList" property="stypeId" labelProperty="actSubName" />
		                               </html:select>
		                            </td>
		                        </tr>
	                            <tr>
	                                <td class="tablelabel">Fee</td>
	                                <td class="tabletext" colspan="3">
										<html:select  property="feeId"  styleClass="textbox" onchange="loadActFees()">
											<option value="" > Please Select</option>
											<html:options  collection="actSubFeeMap"  property="feeId"  labelProperty="description" />
									    </html:select>
									</td>
									<td class="tablelabel">Plan Check Required</td>
	                                <td class="tabletext" colspan="3">
	                                	<html:checkbox property="planCheck" />
									</td>
							    </tr>
	                            <tr>
	                               <td class="tablelabel">Questions</td>
	                                <td class="tabletext" colspan="6">
										<html:select  property="questionId" styleClass="textbox" onchange="loadActFees()">
											<option value="" > Please Select</option>
											<html:options  collection="questionaireList"  property="questionId"  labelProperty="description" />
									    </html:select>
									</td>
	                            </tr>
	                              <!-- End of Displaying list Land Use with multibox  -->
	                          </table>
							</td>
						</tr>
						
						<tr>
				<td> <br>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="100%" height="25"  class="tabletitle">Lookup </td>
                       <td width="1"><img src="/obc/jsp/images/site_bg_cccccc.jpg" width="1" height="19"></td>
                        <td width="1%" background="../images/site_bg_D7DCE2.jpg"> 
                       		<html:button  value="Delete" property="delete" styleClass="button" onclick="del()"/>
                    	</td>
                      
                    </tr>  
                  </table>
					<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="#b7c1cb">
						<tr> 
							<td class="tablelabel">&nbsp;</td>
							<td class="tablelabel">  ACTIVITY TYPE </td>
							<td class="tablelabel">  ACTIVITY SUBTYPE </td>
							<td class="tablelabel">  FEE </td>
							<td class="tablelabel" nowrap="nowrap">  PC REQ </td>
							<td class="tablelabel">  QUESTION </td>
							
						</tr>
							
						<!-- add -->
						<%for(int i=0;i<a.size();i++){ 
							int lkId = ((OnlinePermitForm)a.get(i)).getLkId();
							String activityType = ((OnlinePermitForm)a.get(i)).getActType();
							String activtitySubType = ((OnlinePermitForm)a.get(i)).getActSubName();
							String fee = ((OnlinePermitForm)a.get(i)).getActCode();
							String pc = ((OnlinePermitForm)a.get(i)).getPlanCheck();
							String question = ((OnlinePermitForm)a.get(i)).getDescription();
							
							
	
						%>
						<tr valign="top"> 
							<td class="tabletext">
								<html:multibox property="deleteSelectedId" value="<%=StringUtils.i2s(lkId) %>"></html:multibox></td>
							<td class="tabletext">  <%=activityType %> </td>
							<td class="tabletext">   <%=activtitySubType %> </td>
							<td class="tabletext">  <%=fee %> </td>
							<td class="tabletext">  <%=pc %> </td>
							<td class="tabletext">  <%=question %> </td>
													
						</tr>
					
					<%} %>
					</table>
				
				
				</td>
			</tr>		
						
	                </table>
	                
	                
	                
	                </td>
	            </tr>
	        </table>
	    </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>


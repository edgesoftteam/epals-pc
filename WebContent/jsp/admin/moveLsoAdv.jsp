<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*,elms.agent.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<app:checkLogon/>

<html:html>

<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String contextRoot = request.getContextPath();
	String moveStreetId="-1";
	String toStreetId="-1";
	try {
		moveStreetId = (String) request.getAttribute("moveStreetId");
		if(moveStreetId==null) moveStreetId = "-1";

		toStreetId = (String) request.getAttribute("toStreetId");
		if(toStreetId==null) toStreetId = "-1";

	} catch (Exception e) {
		moveStreetId = "-1";
		toStreetId = "-1";
	}

	 List  streets = (List) AddressAgent.getStreetArrayList();
	 request.setAttribute("streets",streets);
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>

<script language="JavaScript">

var strValue;
function validateFunction() {
	strValue=false;
	if (document.forms['advancedMoveLsoForm'].elements['moveNumber'].value == '') {
		alert('Enter From Street Number');
		document.forms['advancedMoveLsoForm'].elements['moveNumber'].focus();
		strValue=false;
	}else if (document.forms['advancedMoveLsoForm'].elements['moveName'].value == '-1') {
		alert('Enter From Street Name');
		document.forms[0].elements['moveName'].focus();
		strValue=false;
	}else if (document.forms['advancedMoveLsoForm'].elements['toNumber'].value == '') {
		alert('Enter To Street Number');
		document.forms['advancedMoveLsoForm'].elements['toNumber'].focus();
		strValue=false;
	}else if (document.forms['advancedMoveLsoForm'].elements['toName'].value == '-1') {
		alert('Enter To Street Name');
		document.forms['advancedMoveLsoForm'].elements['toName'].focus();
		strValue=false;
	}else{
		document.forms[0].action='<%=contextRoot%>/verifyAdvancedMoveLso.do';
		document.forms[0].submit();
	}
}

function resetFields() {
document.forms['advancedMoveLsoForm'].elements['moveNumber'].value='';
document.forms['advancedMoveLsoForm'].elements['toNumber'].value='';
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="resetFields();">

<html:form focus="moveNumber" action="/verifyAdvancedMoveLso.do"  type="advancedMoveLsoForm">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Move LSO</font><br>
            <br>
            </td>
        </tr>
	<html:errors/>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="97%" class="tabletitle">Move Structure or Occupancy</td>
                        <td width="2%" class="tablebutton">
						<html:button property="verifyAdvancedMoveLso" value="Verify" styleClass="button" onclick="return validateFunction();"></html:button></td>
                    </tr>
                </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
						<td width="20%" class="tablelabel"> Move</td>
                        <td width="20%" class="tabletext">
                             <html:text size="8" property="moveNumber" styleClass="textbox"/>
                        </td>
                        <td width="60%" class="tabletext">
                           <table>
                                <tr>
                                    <td><html:select property="moveName" styleClass="textbox">
                                        <html:option value="-1">Please Select</html:option>
                                        <html:options collection="streets" property="streetId" labelProperty="streetName"/>
                                        </html:select></td>
                                </tr>
                            </table>
                        </td>
                      </tr>
                    <tr valign="top">
                    	<td width="20%" class="tablelabel"> To</td>
                        <td width="20%" class="tabletext">
                             <html:text size="8" property="toNumber" styleClass="textbox"/>
                        </td>
                        <td width="60%" class="tabletext" colspan="2">
                           <table>
                                    <tr>
                                        <td>
	                                        <html:select property="toName" styleClass="textbox">
	                                        	<html:option value="-1">Please Select</html:option>
	                                        	<html:options collection="streets" property="streetId" labelProperty="streetName"/>
	                                        </html:select>
                                        </td>
                                    </tr>
                                </table>
                        </td>
                      </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td colspan="2"></td>
  </tr>
</table>

</html:form>
</body>
</html:html>
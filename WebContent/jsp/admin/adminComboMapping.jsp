<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ page import="elms.app.lso.LsoUse.*"%>

<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="javascript">
/// Start of Check version People History
var no_array = new Array();
function checkLandUseId(val)
{

    var valNew=val.value;
    var useIdArray = document.forms[0].useIdArray.value;
    no_array=useIdArray.split(",");

    if(val.checked == false )
    {

        document.forms[0].useIdArray.value = removeFromArray(valNew,no_array).join(",");
         useIdArray = removeFromArray(valNew,no_array).join(",");
        no_array=useIdArray.split(",");
        valNew="";
    }

    if(valNew != "")
    {
     useIdArray += valNew+",";

    }


    document.forms[0].useIdArray.value = useIdArray;


    return true;

}


function removeFromArray(val, ar){
s = String(ar)
// remove if not first item (global search)
reRemove = new RegExp(","+val,"g")
s = s.replace(reRemove,"")
// remove if at start of array
reRemove = new RegExp("^"+val+",")
s = s.replace(reRemove,"")
// remove if only item
reRemove = new RegExp("^"+val+"$")
s = s.replace(reRemove,"")
return new Array(s)
}

</script>
</head>

<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"> </script>

<%
	//the context path
   	String contextRoot = request.getContextPath();
	String selectedActType = (String)request.getAttribute("selectedActType");
	String selectedActTypeCode = (String)request.getAttribute("selectedActTypeCode");
	java.util.List useIdMaps= new java.util.ArrayList();
	//System.out.println("The size of the lso list is " +lsoUseMap.get(0));
	elms.app.lso.LsoUse lsoUseMap = (elms.app.lso.LsoUse)request.getAttribute("lsoUseMap");
	elms.control.beans.admin.AdminComboMappingForm adminComboMappingForm= (elms.control.beans.admin.AdminComboMappingForm)request.getAttribute("adminComboMappingForm");


%>

<script language="JavaScript">
<logic:notEqual value="YES" property="displayComboList" name="AdminComboMappingForm">
	var str = "";
	var strCode = "";
	var strDesc = "";
	var delimiter = "";
	var delimiter1 = "";

function selectActType() {
	var size= document.forms[0].elements['actType'].length;

	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['actType'].options[i].selected) {
			str += delimiter + document.forms[0].elements['actType'].options[i].text;
			var text = document.forms[0].elements['actType'].options[i].text;
			var value = document.forms[0].elements['actType'].options[i].value;
			var op = new Option(text,value);
			document.forms[0].elements['selectedActivityType'].add(op,document.forms[0].elements['selectedActivityType'].options.length);
			delimiter = "\r";
			document.forms[0].elements['actType'].options[i].selected = false;
		}
	}
}








//added for button plan check
//add plan check to individual activity -added by Manjuprasad
function addPlanCheck() {

var pcReq=document.forms[0].pcReq.value;
if (document.forms[0].elements['selectedActivityType'].length == 0) {
		alert("Select atleast one Activity");
		document.forms[0].elements['actType'].focus();
	}
		else  {




		strCode = "";
		strDesc = "";
		delimiter1 ="";

		     var theWidth=700;

     var theHeight=300;

     var theTop=(screen.height/2)-(theHeight/2);

     var theLeft=(screen.width/2)-(theWidth/2);

     var features=

     'scrollbars=1,height='+theHeight+',width='+theWidth+',top='+theTop+',left='+theLeft+',resizable=1';


		var size= document.forms[0].elements['selectedActivityType'].length;
		for (var i=0; i<size;i++) {
				strCode += delimiter1 + document.forms[0].elements['selectedActivityType'].options[i].value;
				strDesc += delimiter1 + document.forms[0].elements['selectedActivityType'].options[i].text;
				delimiter1 = "@";
		}

		document.forms[0].elements['actCode'].value=strCode;
		document.forms[0].elements['actName'].value=strDesc;

	var mywindow = window.open('<%=contextRoot%>/comboMappingAdmin.do?pCheck=Yes&strDesc='+strDesc+'&strCode='+strCode+'&pcReq='+pcReq,'mywindow', features,'');
	if (window.focus) {mywindow.focus()}
	return false;


}

}


function deselectActType() {
	for (var i=0;i<document.forms[0].elements['selectedActivityType'].length;i++) {
		if (document.forms[0].elements['selectedActivityType'].options[i].selected) {
			document.forms[0].elements['selectedActivityType'].remove(i);
		}
	}
}
function populateActType(){
	if(document.forms[0].elements['selectedActType'].value != ""){
		var arrActType = new Array();
		var arrActTypeCode = new Array();
		var strActType = document.forms[0].elements['selectedActType'].value;
		var strActTypeCode = document.forms[0].elements['selectedActTypeCode'].value;
		arrActType = strActType.split(",");
		arrActTypeCode = strActTypeCode.split(",");
		for(var i=0;i<arrActType.length-1;i++){
			var op = new Option(arrActType[i],arrActTypeCode[i]);
			document.forms[0].elements['selectedActivityType'].add(op,document.forms[0].elements['selectedActivityType'].options.length);
		}
		str = document.forms[0].elements['selectedActType'].value;
		strDesc = document.forms[0].elements['selectedActType'].value;
		document.forms[0].elements['actName'].value = document.forms[0].elements['selectedActType'].value;
		strCode = document.forms[0].elements['selectedActTypeCode'].value;
		document.forms[0].elements['actCode'].value = document.forms[0].elements['selectedActTypeCode'].value;
	}
}
function validateFunction() {
	if (document.forms['comboMappingForm'].elements['deptCode'].value == -1) {
		alert("Select Department");
		document.forms['comboMappingForm'].elements['deptCode'].focus();
	}else if (document.forms['comboMappingForm'].elements['subProjName'].value == '') {
		alert("Enter Sub Project Name");
		document.forms['comboMappingForm'].elements['subProjName'].focus();
	}else if (document.forms['comboMappingForm'].elements['selectedActivityType'].length == 0) {
		alert("Select atleast one Activity");
		document.forms['comboMappingForm'].elements['actType'].focus();
	}else if (document.forms['comboMappingForm'].elements['stypeId'].value == -1) {
		alert("Select Sub Project Type");
		document.forms['comboMappingForm'].elements['stypeId'].focus();
	}else if (document.forms['comboMappingForm'].elements['stypeSubId'].value == -1) {
		alert("Select Sub Project Sub Type");
		document.forms['comboMappingForm'].elements['stypeSubId'].focus();
	}else if(isNaN(document.forms['comboMappingForm'].elements['startsAfter'].value))
	{
		alert("Please enter numeric value for Starts After");
		document.forms['comboMappingForm'].elements['startsAfter'].focus();
	}
	else{
		strCode = "";
		strDesc = "";
		delimiter1 ="";

		if(document.forms[0].elements['forcePCTemp'].checked == true)
		{
		document.forms[0].elements['forcePC'].value='Y';
		}else{
		document.forms[0].elements['forcePC'].value='N';
		}

	    if(document.forms[0].elements['approvalTemp'].checked == true)
		{
		document.forms[0].elements['approval'].value='Y';
		}else{
		document.forms[0].elements['approval'].value='N';
		}

	   if(document.forms[0].elements['onlineTemp'].checked == true)
		{
		document.forms[0].elements['online'].value='Y';
		}else{
		document.forms[0].elements['online'].value='N';
		}
		var size= document.forms[0].elements['selectedActivityType'].length;
		for (var i=0; i<size;i++) {
				strCode += delimiter1 + document.forms[0].elements['selectedActivityType'].options[i].value;
				strDesc += delimiter1 + document.forms[0].elements['selectedActivityType'].options[i].text;
				delimiter1 = ",";
		}
		document.forms[0].elements['actCode'].value=strCode;
		document.forms[0].elements['actName'].value=strDesc;

	   	document.forms[0].action='<%=contextRoot%>/comboMappingAdmin.do?save=Yes';
		document.forms[0].submit();
    }
}
</logic:notEqual>
function lookup() {
    document.forms[0].action='<%=contextRoot%>/comboMappingAdmin.do?lookup=Yes';
    document.forms[0].submit();
}

function clearFields(){
	document.forms['comboMappingForm'].elements['deptCode'].value = -1;
	document.forms['comboMappingForm'].elements['subProjName'].value = "";
	document.forms[0].elements['selectedActivityType'].length = 0;
	document.forms[0].elements['actType'].value = "";
	document.forms['comboMappingForm'].elements['stypeId'].value = -1;
	document.forms['comboMappingForm'].elements['stypeSubId'].value = -1;
	//document.forms[0].elements['isPcReq'].checked = false;
	str = "";
	strDesc = "";
	strCode = "";
}

function remove(){
	document.forms[0].action='<%=contextRoot%>/comboMappingAdmin.do?active=Yes';
    document.forms[0].submit();
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
<logic:notEqual value="YES" property="displayComboList" name="AdminComboMappingForm">
onload="selectActType();populateActType();"
</logic:notEqual>
>

<html:form name="comboMappingForm" type="elms.control.beans.admin.AdminComboMappingForm" action="/comboMappingAdmin">
<html:hidden property="mapId"/>
<html:hidden property="actName"/>
<html:hidden property="useIdArray" />
<html:hidden property="forcePC" />
<html:hidden property="approval" />
<html:hidden property="online" />


<html:hidden property="selectedActType" value='<%=selectedActType%>'/>
<html:hidden property="selectedActTypeCode" value='<%=selectedActTypeCode%>'/>
<html:hidden property="actCode"/>
<!-- added hidden variable for plan check for individual activity added by Manjuprasad-->
<html:hidden property="pcReq" />
<logic:notEqual value="YES" property="displayComboList" name="AdminComboMappingForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Combo Permit Mapping</font><br>
                <br>
                </td>
            </tr>
            <tr>
            <td>
	            <html:errors/>
	            <logic:present name="message" scope="request" >
		            <font color='green'><bean:write name="message"/></font>
	            </logic:present>
            </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <html:errors property="error"/>
		                    <tr>
		                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>
		                        <td width="1%" class="tablebutton"><nobr>
		                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="return clearFields();"></html:button>
		                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return lookup();"></html:button>
		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">

                            <tr>
								<td class="tablelabel">Department</td>
                                <td class="tabletext" colspan="3">
                                   <html:select property="deptCode" styleClass="textbox">
                        	          <html:option value="-1">Please Select</html:option>
                                      <html:options collection="departments" property="departmentCode" labelProperty="description" />
                                   </html:select>
                                </td>
								<td class="tablelabel"><nobr>Combo Name</nobr></td>
                                <td class="tabletext"><html:text size="25" property="subProjName" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Activity Type</td>
                                <td colspan="2" class="tabletext"><html:select property="actType" size="25" styleClass="textbox" multiple="multiple"> <html:options collection="atList" property="type" labelProperty="description"/> </html:select></td>
								<td width="2%" class="tabletext"><html:button style="width:25" property ="select" value=">" styleClass="button" onclick="selectActType()"></html:button><br>
								<html:button style="width:25" property ="deselect" value="<" styleClass="button" onclick="deselectActType()"></html:button></td>
								<td class="tabletext" colspan="4"><select name="selectedActivityType" multiple size="20" style="width:250"></select></td>
                            </tr>
							<tr>
								<td class="tablelabel">Sub Project Type</td>
                                <td class="tabletext" colspan="3">
                                   <html:select property="stypeId" styleClass="textbox">
                        	          <html:option value="-1">Please Select</html:option>
                                      <html:options collection="subProjectTypes" property="subProjectTypeId" labelProperty="description" />
                                   </html:select>
                                </td>
								<td class="tablelabel">Sub Project Sub Type</td>
                                <td class="tabletext" colspan="3">
                                   <html:select property="stypeSubId" styleClass="textbox">
                        	          <html:option value="-1">Please Select</html:option>
                                      <html:options collection="subProjectSubTypes" property="subProjectSubTypeId" labelProperty="description" />
                                   </html:select>
                                </td>
                            </tr>
							<tr>
									<td class="tablelabel">Plan Check Required</td>
	                             <td class="tabletext" colspan="3"><html:button property="addPC" value="Add PlanCheck" styleClass="button" onclick="return addPlanCheck();"></html:button></td>

	                              <td class="tablelabel">Force Plan Check</td>
	                                <td class="tabletext" colspan="3">
	                                <%
	                                if((adminComboMappingForm.getForcePC()).equalsIgnoreCase("Y"))
	                                {
	                                %>
	                                <input type="checkbox" name="forcePCTemp" checked="checked">
	                                <%}else{%>
	                                <input type="checkbox" name="forcePCTemp">
	                                <%}%>
	                                </td>
	                       </tr>

	                       <tr>
									<td class="tablelabel">Online</td>
	                               <td class="tabletext" colspan="3">
	                                <%
	                                if((adminComboMappingForm.getOnline()).equalsIgnoreCase("Y"))
	                                {
	                                %>
	                               <input type="checkbox" name="onlineTemp" checked="checked">
	                               <%
	                               }else{
	                               %>
	                               <input type="checkbox" name="onlineTemp">
	                               <%}%>
	                               </td>


	                             <td class="tablelabel">Approval</td>
	                                <td class="tabletext" colspan="3" >
	                                	                                <%
	                                if((adminComboMappingForm.getApproval()).equalsIgnoreCase("Y"))
	                                {
	                                %>
	                                <input type="checkbox" name="approvalTemp" checked="checked">
	                                <%}else{%>
	                                <input type="checkbox" name="approvalTemp">
	                                <%}%>
	                                </td>
	                       </tr>
	                       <!-- Displaying list Land Use with multibox  -->
	                     <%
	                     int i=0;
	                     %>
	                     <tr>

	                            <td class="tablelabel" ><nobr>Starts After</nobr></td>
                                <td class="tabletext" colspan="6"><html:text size="25" property="startsAfter" styleClass="textbox"/></td>

                          </tr>
                            <table width="100%" border="0" cellspacing="1" cellpadding="2">

	                        <tr>
	                        	<td class="tablelabel">Land USE</td>

	                        	 <%
	                        	 boolean checkval=false;
	                        	 elms.agent.ActivityAgent comboAgent = new elms.agent.ActivityAgent();
	                        	 useIdMaps = (java.util.List)request.getAttribute("useIdMaps");
	                        	 for(int j=0;j<lsoUseMap.getLsoUseId().size();j++)
	                        	 {

	                        	 checkval=comboAgent.compareListWithString((String)lsoUseMap.getLsoUseId().get(j), useIdMaps);

	                        	 if(i%2==0)
	                        	 {
	                        	 %>
	                        	 <tr>
	                        	 <%}%>

							    <td class="tabletext">

                                <td  colspan="3" class="tabletext">
                                <%
                                if(checkval)
                                {
                                %>
                                <input type="checkbox" name="lsoUseMap" onclick="return checkLandUseId(this)" value=<%=lsoUseMap.getLsoUseId().get(j)%> checked>
                                <%}else{%>
                                <input type="checkbox" name="lsoUseMap" onclick="return checkLandUseId(this)" value=<%=lsoUseMap.getLsoUseId().get(j)%>>
                                <%}%>
                                 <%=lsoUseMap.getLsoUseDescription().get(j)%>
                               </td>
                              	 <%
                              	 if(i%2==0)
                              	 {%>
                              	 </tr>
                              	 <%
                              	 }
	                        	 i++;
	                        	 }
	                        	 %>

                            </tr>
                              <!-- End of Displaying list Land Use with multibox  -->
                            </table>


                       </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</logic:notEqual>
<logic:equal value="YES" property="displayComboList" name="AdminComboMappingForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Combo Mapping List</font><br>
            <br>
            </td>
        </tr>
		<tr>
			<td>
	            <html:errors/>
	            <logic:present name="message" scope="request" >
		            <font color="green" ><bean:write name="message"/></font>
	            </logic:present>
            </td>
		</tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="95%" height="25" class="tabletitle">Combo Permit Mapping </td>

                      <td width="1%" class="tablebutton"><nobr>
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
					   <html:button  value="Delete" property="delete" styleClass="button" onclick="remove()"/>&nbsp;
                       <html:submit property="addmapping" value="Add Mapping" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                    <!--Sunil-->
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
					  <td width="5%" class="tablelabel">Delete</td>
                      <td width="25%" class="tablelabel">Sub Project Name</td>
                      <td width="25%" class="tablelabel">Department</td>
					  <td width="45%" class="tablelabel">Activity</td>
                    </tr>
					 <logic:iterate id="combomapp" property="comboList" name="AdminComboMappingForm">
                      <tr>

						<logic:equal value="Y" property="isActive" name="combomapp">

                        <td class="tabletext"><html:multibox property="selectedId"><bean:write name="combomapp" property="mapId"/></html:multibox></td>
						<td class="tabletext"><a  href="<%=contextRoot%>/comboMappingAdmin.do?mapId=<bean:write name="combomapp" property="mapId" />&subProjName=<bean:write name="combomapp" property="subProjName"/>&deptCode=<bean:write name="combomapp" property="deptCode"/>"><bean:write name="combomapp" property="subProjName" /></a></td>
                        <td class="tabletext"><bean:write name="combomapp" property="deptCode" /></td>
                        <td class="tabletext"><bean:write name="combomapp" property="actType" /></td>
						</logic:equal>
                      </tr>
                    </logic:iterate>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:equal>
</html:form>
</body>
</html:html>


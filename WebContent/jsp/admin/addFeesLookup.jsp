<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%!int lstLength = 0;%>
<% String contextRoot = request.getContextPath();
lstLength = elms.util.StringUtils.s2i((String)request.getAttribute("lst.length"));

boolean insertRowTrue = elms.util.StringUtils.s2b((String)request.getAttribute("insertRowTrue"));
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript1.2">

function insertRow() {
	document.addLookupFeesForm.action='<%=contextRoot%>/insertAddFeesLookup.do?insertRowTrue=false';
	document.addLookupFeesForm.submit();
}

function validateFunction() {
    strValue=validateData('req',document.addLookupFeesForm.elements['name'],'FeeLookup Name is a required field');
    if (strValue == true)
    {
    	strValue=validateData('req',document.addLookupFeesForm.elements['creationDate'],'Effective Date is a required field');
    }
    if (strValue == true)
    {
		if (document.addLookupFeesForm.elements['creationDate'].value != '')
    	{
    		strValue=validateData('date',document.addLookupFeesForm.elements['creationDate'],'Invalid date format');
    	}
    }

    if (strValue == true)
    {
 		document.addLookupFeesForm.action='<%=contextRoot%>/saveAddFeesLookup.do';
		document.addLookupFeesForm.submit();
    }
}

function highRange(str)
{

var insertRowTrue = <%=insertRowTrue%>;

	if(insertRowTrue == false){
			ctr = 1;
			if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
				event.returnValue = false;
			}else{
					for(i=1;i<=ctr;i++){
							if (document.forms[0].elements[i*6].value.length == 12 ){
								document.forms[0].elements[i*6].value = document.forms[0].elements[i*6].value +'.';
			 				}if (document.forms[0].elements[i*6].value.length > 15 )  event.returnValue = false;
						}
				}
	}else{
		ctr = '<%=lstLength%>';
		var j=0;
			if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
			event.returnValue = false;
			}else{
				for(i=1;i<=ctr;i++){
					if (document.forms[0].elements[(i*6)-j].value.length == 12 ){
						document.forms[0].elements[(i*6)-j].value = document.forms[0].elements[(i*6)-j].value +'.';
	 				}if (document.forms[0].elements[(i*6)-j].value.length > 15 )  event.returnValue = false;
					j=j+1;
					}
			}
	}
}


function lowRange(str)
{
var insertRowTrue = <%=insertRowTrue%>;

	if(insertRowTrue == false){
			ctr = 1;
			if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
				event.returnValue = false;
			}else{
					for(i=1;i<=ctr;i++){
							if (document.forms[0].elements[i*5].value.length == 12 ){
								document.forms[0].elements[i*5].value = document.forms[0].elements[i*5].value +'.';
			 				}if (document.forms[0].elements[i*5].value.length > 15 )  event.returnValue = false;
						}
				}
	}else{
		ctr = '<%=lstLength%>';
			if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
			event.returnValue = false;
			}else{
				for(i=1;i<=ctr;i++){
					if (document.forms[0].elements[(i*5)].value.length == 12 ){
						document.forms[0].elements[(i*5)].value = document.forms[0].elements[(i*5)].value +'.';
	 				}if (document.forms[0].elements[(i*5)].value.length > 15 )  event.returnValue = false;

					}
			}
	}
}


</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="addLookupFeesForm" type="elms.control.beans.AddLookupFeesForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Lookup</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="60%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%">Add Lookup</td>

                        <td width="1%" class="tablebutton">
                          <nobr>
                          		<html:button property="cancel" value="Cancel" styleClass="button" onclick="history.back();return true;" /> &nbsp;
                                <html:button property="change" value="Save" styleClass="button" onclick="validateFunction();"/>
                          &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td colspan="3" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"><nobr>Lookup Name</nobr></td>
                      <td class="tablelabel">Effective</td>
                    </tr>
                    <tr>
						<td class="tabletext"> <html:text property="name" styleClass="textbox" size="20" /> </td>
						<td class="tabletext"><nobr> <html:text property="creationDate" styleClass="textbox" size="12" onkeypress="return validateDate();"/>
						<a href="javascript:show_calendar('addLookupFeesForm.creationDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0></a></nobr></td>
					</tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td><img src="../images/spacer.gif" width="1" height="7"></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%">Add Rows To Lookup (Optional)</td>

                      <td width="1%" class="tablebutton" align="right"><nobr>
                          <html:button property="insert" value="Insert Row" styleClass="button" onclick="insertRow();" />
                          </nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Low Range</td>
                      <td class="tablelabel">High Range</td>
                      <td class="tablelabel">Result</td>
                      <td class="tablelabel">Over</td>
                      <td class="tablelabel">Plus</td>
                    </tr>
						<nested:iterate property="lkupFeeEditList">
                            <tr>
                                <td class="tabletext"> <nested:text property="lowRange" styleClass="textboxm" maxlength="15" size="12" onkeypress="return lowRange('lowRange');"/> </td>
                                <td class="tabletext"> <nested:text property="highRange" styleClass="textboxm" maxlength="15" size="17" onkeypress="return highRange('highRange');"/> </td>
                                <td class="tabletext"> <nested:text property="result" styleClass="textboxm" size="12" onkeypress="return validateDecimal();"/> </td>
                                <td class="tabletext"> <nested:text property="over" styleClass="textboxm" size="12" onkeypress="return validateDecimal();"/> </td>
                                <td class="tabletext"> <nested:text property="plus" styleClass="textboxm" size="7" onkeypress="return validateDecimal();"/></td>
                    		</tr>
						</nested:iterate>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
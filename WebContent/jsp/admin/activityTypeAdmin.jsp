<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath();%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="javascript">
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");


function test(){
    document.getElementById('addressDiv').innerHTML = '';
	document.getElementById("addressDiv").style.display =  'none';
	document.getElementById('loader').innerHTML = '';
}

function checkTable(){

	document.forms['activityTypeForm'].elements['Lookup'].disabled = true;
	document.getElementById('loader').innerHTML = '<img src="../images/loader.gif" style="float:right;">';

	var url = "<%=contextRoot%>/activityTypeAdmin.do?lookup=Yes";

	if(document.forms['activityTypeForm'].elements['activityType'].value != ''){
		url = url + "&activityTypeStr=" + document.forms['activityTypeForm'].elements['activityType'].value;
	}
	if(document.forms['activityTypeForm'].elements['description'].value != ''){
		url = url + "&description=" + document.forms['activityTypeForm'].elements['description'].value;
	}
	if(document.forms['activityTypeForm'].elements['subProjectType'].value != '-1'){
		url = url + "&subProjectType=" + document.forms['activityTypeForm'].elements['subProjectType'].value;
	}
	if(document.forms['activityTypeForm'].elements['department'].value != '-1'){
		url = url + "&department=" + document.forms['activityTypeForm'].elements['department'].value;
	}
	if(document.forms['activityTypeForm'].elements['moduleId'].value != '-1'){
		url = url + "&moduleId=" + document.forms['activityTypeForm'].elements['moduleId'].value;
	}
	if(document.forms['activityTypeForm'].elements['muncipalCode'].value != ''){
		url = url + "&muncipalCode=" + document.forms['activityTypeForm'].elements['muncipalCode'].value;
	}
	if(document.forms['activityTypeForm'].elements['sicCode'].value != ''){
		url = url + "&sicCode=" + document.forms['activityTypeForm'].elements['sicCode'].value;
	}
	if(document.forms['activityTypeForm'].elements['classCode'].value != ''){
		url = url + "&classCode=" + document.forms['activityTypeForm'].elements['classCode'].value;
	}
	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);
}

function checkTableHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
				if(result == 'Activity Type Saved Successfully'){
					document.getElementById('saveDiv').innerHTML = result;
				}else{
					document.forms['activityTypeForm'].elements['Lookup'].disabled = false;
					document.getElementById('loader').innerHTML = '';
					if(result == 'No Records Found'){
						document.getElementById('saveDiv').innerHTML = result;
					}else{
						document.getElementById('addressDiv').innerHTML = result;
					}
				}
			    document.getElementById("addressDiv").style.display =  '';
 	}
}


if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}

</script>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction() {
	strValue=false;
	if (document.forms['activityTypeForm'].elements['activityType'].value == '') {
		alert("Enter Activity Type");
		document.forms['activityTypeForm'].elements['activityType'].focus();
		strValue=false;
	}else if (document.forms['activityTypeForm'].elements['description'].value == ''){
		alert("Enter Description");
		document.forms['activityTypeForm'].elements['description'].focus();
		strValue=false;
	}else if (document.forms['activityTypeForm'].elements['department'].value == '-1') {
		alert("Select Department");
		document.forms['activityTypeForm'].elements['department'].focus();
		strValue=false;
	}else{
		var url = "<%=contextRoot%>/activityTypeAdmin.do?save=Yes";

		if(document.forms['activityTypeForm'].elements['activityType'].value != ''){
			url = url + "&activityTypeStr=" + document.forms['activityTypeForm'].elements['activityType'].value;
		}
		if(document.forms['activityTypeForm'].elements['description'].value != ''){
			url = url + "&description=" + document.forms['activityTypeForm'].elements['description'].value;
		}
		if(document.forms['activityTypeForm'].elements['subProjectType'].value != '-1'){
			url = url + "&subProjectType=" + document.forms['activityTypeForm'].elements['subProjectType'].value;
		}
		if(document.forms['activityTypeForm'].elements['department'].value != '-1'){
			url = url + "&department=" + document.forms['activityTypeForm'].elements['department'].value;
		}
		if(document.forms['activityTypeForm'].elements['moduleId'].value != '-1'){
			url = url + "&moduleId=" + document.forms['activityTypeForm'].elements['moduleId'].value;
		}
		if(document.forms['activityTypeForm'].elements['muncipalCode'].value != ''){
			url = url + "&muncipalCode=" + document.forms['activityTypeForm'].elements['muncipalCode'].value;
		}
		if(document.forms['activityTypeForm'].elements['sicCode'].value != ''){
			url = url + "&sicCode=" + document.forms['activityTypeForm'].elements['sicCode'].value;
		}
		if(document.forms['activityTypeForm'].elements['classCode'].value != ''){
			url = url + "&classCode=" + document.forms['activityTypeForm'].elements['classCode'].value;
		}
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
	    xmlhttp.send(null);
	}
}


function refresh(){
     document.forms[0].action='<%=contextRoot%>/activityTypeAdmin.do';
     document.forms[0].submit();
}

var no_array = new Array();
function checkIds(val){
	var valNew=val;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false ){
		document.forms[0].checkboxArray.value = removeFromArray(valNew.value,no_array).join(",");
     	checkboxArray = removeFromArray(valNew.value,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}
	if(valNew != ""){
	 checkboxArray += valNew.value+",";
	}
	document.forms[0].checkboxArray.value = checkboxArray;
	return true;
}

function removeFromArray(val, ar){
	s = String(ar)
	// remove if not first item (global search)
	reRemove = new RegExp(","+val,"g")
	s = s.replace(reRemove,"")
	// remove if at start of array
	reRemove = new RegExp("^"+val+",")
	s = s.replace(reRemove,"")
	// remove if only item
	reRemove = new RegExp("^"+val+"$")
	s = s.replace(reRemove,"")
	return new Array(s)
}

	function removeTable(){

	var url = "<%=contextRoot%>/activityTypeAdmin.do?delete=Yes";

	url = url + "&checkboxArray=" + document.forms['activityTypeForm'].elements['checkboxArray'].value;

	if(document.forms['activityTypeForm'].elements['activityType'].value != ''){
		url = url + "&activityTypeStr=" + document.forms['activityTypeForm'].elements['activityType'].value;
	}
	if(document.forms['activityTypeForm'].elements['description'].value != ''){
		url = url + "&description=" + document.forms['activityTypeForm'].elements['description'].value;
	}
	if(document.forms['activityTypeForm'].elements['subProjectType'].value != '-1'){
		url = url + "&subProjectTypeText=" + document.forms['activityTypeForm'].elements['subProjectType'].value;
	}
	if(document.forms['activityTypeForm'].elements['department'].value != '-1'){
		url = url + "&department=" + document.forms['activityTypeForm'].elements['department'].value;
	}
	if(document.forms['activityTypeForm'].elements['moduleId'].value != '-1'){
		url = url + "&moduleId=" + document.forms['activityTypeForm'].elements['moduleId'].value;
	}
	if(document.forms['activityTypeForm'].elements['muncipalCode'].value != ''){
		url = url + "&muncipalCode=" + document.forms['activityTypeForm'].elements['muncipalCode'].value;
	}
	if(document.forms['activityTypeForm'].elements['sicCode'].value != ''){
		url = url + "&sicCode=" + document.forms['activityTypeForm'].elements['sicCode'].value;
	}
	if(document.forms['activityTypeForm'].elements['classCode'].value != ''){
		url = url + "&classCode=" + document.forms['activityTypeForm'].elements['classCode'].value;
	}
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
    	xmlhttp.send(null);
}

//Start Ajax functionality for Activity Type
var xmlhttp;

	function CreateXmlHttp(str, editFlag){

	//Assigning Activity Id to the hidden variable
	document.forms['activityTypeForm'].elements['activityTypeId'].value = str;
	document.forms['activityTypeForm'].elements['editFlg'].value = editFlag;

	//Creating object of XMLHTTP in IE
		var url="<%=contextRoot%>/activityTypeAdmin.do?id="+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		  }else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 if(xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ActivityType;
			xmlhttp.send(null);
		  }else{
		  alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ActivityType(){

	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){

		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){

		    var text=xmlhttp.responseText;
			text=text.split(',');

			document.forms['activityTypeForm'].elements['activityType'].value = text[0];

			if(document.forms['activityTypeForm'].elements['editFlg'].value == 'N'){
				document.forms['activityTypeForm'].elements['activityType'].disabled=true;
			}else{
				document.forms['activityTypeForm'].elements['activityType'].disabled=false;
			}

			document.forms['activityTypeForm'].elements['description'].value = text[1];
			document.forms['activityTypeForm'].elements['subProjectType'].value = text[2];
			document.forms['activityTypeForm'].elements['department'].value = text[3];
			document.forms['activityTypeForm'].elements['moduleId'].value = text[4];
			document.forms['activityTypeForm'].elements['muncipalCode'].value = text[5];
			document.forms['activityTypeForm'].elements['sicCode'].value = text[6];
			document.forms['activityTypeForm'].elements['classCode'].value = text[7];

			if(document.forms['activityTypeForm'].elements['activityType'].value == ""){
				document.forms['activityTypeForm'].elements['description'].value = "";
				document.forms['activityTypeForm'].elements['subProjectType'].value = "";
				document.forms['activityTypeForm'].elements['department'].value = "";
				document.forms['activityTypeForm'].elements['moduleId'].value = "";
				document.forms['activityTypeForm'].elements['muncipalCode'].value = "";
				document.forms['activityTypeForm'].elements['sicCode'].value = "";
				document.forms['activityTypeForm'].elements['classCode'].value = "";
			}
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}
//End Ajax functionality for Activity Type
</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="test()">
<html:form focus="activityType" name="activityTypeForm" type="elms.control.beans.admin.ActivityTypeForm" action="">
<html:hidden property="activityTypeId"/>
<html:hidden property="checkboxArray" value=""/>
<html:hidden property="editFlg" value=""/>

<div id="saveDiv" align="justify" style="color:#088A08"></div>

<logic:notEqual value="YES" property="displayActTypes" name="activityTypeForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="100%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="36px"><font class="con_hdr_3b">Activity Type Administration</font></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="Reset" value="Reset" styleClass="button" onclick="refresh();"/>
                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return checkTable();"></html:button>
                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Activity Type</td>
                      <td class="tabletext">
                      	<html:text property="activityType" size="25" maxlength="6" styleClass="textbox"/>&nbsp;
                      </td>
                      <td class="tablelabel">Description</td>
                      <td class="tabletext">
                        <html:text  property="description" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Sub Project Type</td>


                      <td class="tabletext">
							<html:select property="subProjectType" styleClass="textbox">
								<html:option value="-1"> Please Select</html:option>
								<html:options collection="subProjectTypes" property="projectTypeId" labelProperty="departmentDescription" />
							</html:select>
                      </td>
                      <td class="tablelabel">Department</td>
                      <td class="tabletext">
                        <html:select property="department" styleClass="textbox"> <%--onchange="return onChangeAction()"--%>
                        	  <html:option value="-1">Please Select</html:option>
                              <html:options collection="departments" property="departmentId" labelProperty="description" />
                        </html:select>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Module</td>
                      <td class="tabletext">
                      
                        <html:select property="moduleId" styleClass="textbox">
                        	  <html:option value="-1">Please Select</html:option>
                              <html:options collection="modules" property="id" labelProperty="description" />
                        </html:select>
            <%-- 
                        <html:select property="moduleId" styleClass="textbox">
                        	  <html:option value="-1">Please Select</html:option>
                        	  <html:option value="1">Building</html:option>
                        	  <html:option value="2">Planning</html:option>
                        	  <html:option value="3">Code Enforcement</html:option>
                        	  <html:option value="4">Business License</html:option>
                        	  <html:option value="5">Service Request</html:option>
                        	  <html:option value="6">Transporation</html:option>
							  <html:option value="8">Business Tax</html:option>
							  <html:option value="9">Public Works</html:option>
							  <html:option value="10">Parking</html:option>
                        </html:select>--%>
                      </td>
					  <td class="tablelabel">Municipal Code</td>
                      <td class="tabletext">
                     	<html:text property="muncipalCode" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
					<tr>
                      <td class="tablelabel">SIC/NAICS Code</td>
                      <td class="tabletext">
                     	<html:text property="sicCode" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
                      </td>
                      <td class="tablelabel">Class Code</td>
                      <td class="tabletext">
                        <html:text property="classCode" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
                </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
    </td>

  </tr>
</table>
</logic:notEqual>
<div id="loader" style="padding-right:50%;"><span><img src="../images/loader.gif"></span></div>
<div id="addressDiv"></div>

</html:form>
</body>
</html:html>

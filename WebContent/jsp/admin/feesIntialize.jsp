<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
		strValue=validateData('req',document.forms['feeInitializeForm'].elements['feeDate'],'Fee Date is a required field');
		if (strValue == true)
		{
			if (document.forms['feeInitializeForm'].elements['feeDate'].value != '')
	    	{
	    		strValue=validateData('date',document.forms['feeInitializeForm'].elements['feeDate'],'Invalid date format');
	    	}
	    }
	    return strValue;
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/feesInitialize" onsubmit="return validateFunction();">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Fees Maintenance</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle">Initialize<nobr></nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr class="tabletext">
                      <td colspan="2"><font class="red3">Proceed With Caution</font></td>
                    </tr>
                    <tr class="tabletext">
                      <td colspan="2"><font class="red2">Initialization process
                        will expire all current fees at the Fee Date and create
                        the new set of current fees!</font></td>
                    </tr>
                    <tr>
                      <td class="tablelabel" width="1%"><nobr>Fee
                        Date </nobr></td>
                      <td class="tabletext" width="99%"><nobr>&nbsp;


                       <html:text  property="feeDate" styleClass="textbox" onkeypress="return validateDate();"/>
					    <html:link href="javascript:show_calendar('feeInitializeForm.feeDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link>
                        </nobr>
                        <html:submit  property="Submit" value="Save" styleClass="button"/>
                      </td>
                    </tr>
                    <tr class="tabletext"><TD colspan="2"><font class="red3"><bean:write name="feeInitializeForm" property="strMessage" /></font></TD></TR>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

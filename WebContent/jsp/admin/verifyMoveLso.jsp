<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<app:checkLogon />

<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%String contextRoot = request.getContextPath();
%>
<SCRIPT language="JavaScript1.2">

function checkRadios() {
 //alert(document.forms[0].elements['moveId'].checked);
 var el = document.forms[0].elements;
 for(var i = 0 ; i < el.length ; ++i) {
  if(el[i].type == "radio") {
   var radiogroup = el[el[i].name]; // get the whole set of radio buttons.
   //alert(radiogroup.length);
   var itemchecked = false;
   for(var j = 0 ; j < radiogroup.length ; ++j) {
    if(radiogroup[j].checked) {
     //alert("in if "+radiogroup);
	 itemchecked = true;
	 break;
	}
   }

   if(!itemchecked) {
       if(document.forms[0].elements['moveId'].checked == true){
        itemchecked = true;
        }
        else{
        alert("Please select an address on the left to move.");
        if(el[i].focus)
         el[i].focus();
        return false;
       }
    }
   }
   }
	document.forms[0].action='<%=contextRoot%>/saveMoveLso.do';
 	document.forms[0].submit();
}


function cancelPage() {
	document.forms[0].action='<%=contextRoot%>/moveLso.do';
 	document.forms[0].submit();
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">

<html:form name="moveVerifyForm" type="elms.control.beans.MoveVerifyForm" action="">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="50%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Move LSO Verify</font><br>
					<br>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td colspan="3">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="98%" class="tabletitle">Verify you wish to move this LSO</td>
									<td width="1%" class="tablebutton"><nobr>
									<logic:notPresent name="moveVerifyForm" property="message">
										<html:button property="ok" value="  OK  " styleClass="button" onclick="return checkRadios()" />
									</logic:notPresent>
										<html:button property="cancel" value="Cancel" styleClass="button" onclick="return cancelPage();" /> &nbsp;</nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="tabletext">&nbsp;</td>
						</tr>
						<logic:present name="moveVerifyForm" property="message">
							<tr>
								<td><bean:write name="moveVerifyForm"
									property="message" /></td>
							</tr>
						</logic:present>
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="tabletitle">Move Details</td>
										</tr>
									</table>
									</td>
								</tr>
								<bean:define id="details" name="moveVerifyForm"
									property="moveStructureDetails" type="java.util.Collection" />
								<logic:iterate id="moveStructureDetail" name="details"
									type="elms.app.admin.lso.MoveStructureDetail">
									<tr>
										<td width="45%" background="../images/site_bg_B7C1CB.jpg">
										<table width="100%" border="0" cellspacing="1" cellpadding="2">
											<tr valign="top">
												<td class="tablelabel">LSO ID<html:radio
													property="moveId"
													value="<%=moveStructureDetail.getMoveId()%>" /></td>
												<td class="tabletext"><bean:write
													name="moveStructureDetail" property="moveId" /></td>
											</tr>
											<tr valign="top">
												<td class="tablelabel">Address</td>
												<td class="tabletext"><bean:write
													name="moveStructureDetail" property="moveAddressOne" /></td>
											</tr>

											<tr valign="top">
												<td class="tablelabel"></td>
												<td class="tabletext"><bean:write
													name="moveStructureDetail" property="moveAddressTwo" />&nbsp;</td>
											</tr>

											<tr valign="top">
												<td class="tablelabel">Alias</td>
												<td class="tabletext"><bean:write
													name="moveStructureDetail" property="moveAlias" />&nbsp;</td>
											</tr>
											<tr valign="top">
												<td class="tablelabel">Description</td>
												<td class="tabletext"><bean:write
													name="moveStructureDetail" property="moveDescription" />&nbsp;</td>
											</tr>
										</table>
										</td>
									</tr>
									<tr>
										<td>&nbsp;</td>
									</tr>

								</logic:iterate>
							</table>
							</td>

							<td width="5%">&nbsp; TO</td>

							<td width="50%" >
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td class="tabletitle">Details</td>
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td width="45%" class="tablebutton">
									<table width="100%" border="0" cellspacing="1" cellpadding="1">
										<tr valign="top">
											<td class="tablelabel">LSO ID</td>
											<td class="tabletext"><bean:write
												name="moveVerifyForm" property="toId" /></td>
										</tr>
										<tr valign="top">
											<td class="tablelabel">Address</td>
											<td class="tabletext"><bean:write
												name="moveVerifyForm" property="toAddressOne" /> </td>
										</tr>
										<tr valign="top">
											<td class="tablelabel"></td>
											<td class="tabletext"><bean:write
												name="moveVerifyForm" property="toAddressTwo" /></td>
										</tr>
										<tr valign="top">
											<td class="tablelabel">Alias</td>
											<td class="tabletext"><bean:write
												name="moveVerifyForm" property="toAlias" /></td>
										</tr>
										<tr valign="top">
											<td class="tablelabel">Description</td>
											<td class="tabletext"><bean:write
												name="moveVerifyForm" property="toDescription" /></td>
										</tr>
									</table>
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
	<input type="hidden" name="toId"
		value="<bean:write name="moveVerifyForm" property="toId" />">
	<input type="hidden" name="moveType"
		value="<bean:write name="moveVerifyForm" property="moveType" />">
</html:form>
</body>
</html:html>

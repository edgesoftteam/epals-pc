 <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
String contextRoot = request.getContextPath();
CachedRowSet crset = (CachedRowSet) request.getAttribute("importStatus");
int size = crset.size();
if(crset !=null) crset.beforeFirst();
String message = (String)request.getAttribute("message");
if(message==null) message = "";
%>
<html:html>
<HEAD>
<html:base/>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<logic:present name="enableAutoRefresh" scope="session">
	<META HTTP-EQUIV="refresh" CONTENT="5">
</logic:present>
<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="javascript">

function startImport() {
	  if(document.assessorImportForm.fileName.checked){
	  	userInput = confirm("Are you sure you want to start the import process ?");
	  	if (userInput==true) {
       		return true;
   		}
   		else{
   			return false;
   		}
	  }
	  else{
	  	alert("Please choose the file to import");
	  	return false;
	  }
}
function deleteAllRecords() {
   userInput = confirm("Are you sure you want to delete all Records?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/assessorImport.do?action=delete';
   }
}


</script>


</HEAD>

<BODY >
<form name="assessorImportForm" action="<%=request.getContextPath()%>/assessorImport.do?action=startImport" method="post" >

<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">Assessor Import</FONT>
          <logic:present name="showImportInProgress" >
          	(Script run in progress..)
          </logic:present>
          <BR>
            <BR>

            <font class = "red2">Note : This function is for EPALS Administrators only, please don't use this if you don't know what this function does.</font>
            </TD>
        </TR>
        <TR>
        <td>
        <font color='green'><b><%=message%></b></font>
        </td>
        </tr>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Choose file to import</TD>
                      <TD width="1%" class="tablebutton"><NOBR>

					  <logic:equal name="showStartImport" value="yes">
	                      <input type="submit" value="Start Import" onclick="return startImport()"/>
    				  </logic:equal>

                      </NOBR></TD>

                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <%
					java.util.ResourceBundle obcProperties = elms.util.db.Wrapper.getResourceBundle();
					String cfString = obcProperties.getString("ASSESSOR_CHECKIN_FOLDER");
              %>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Files in the CHECK-IN Folder (<%=cfString%>)</TD>
                    </TR>
            	    <TR valign="top">
	 					<TD class="tabletext"><FONT class="con_text_1_ni">
					<%
	 					java.io.File checkInFolder = new java.io.File(cfString);
	 					String[] files = checkInFolder.list();
						for (int c = 0; c < files.length; c++){
							String filename = files[c];
					%>
					<input type = "radio" name = "fileName" value="<%=filename%>"><%=filename%><br>
					<%
						}
					%>

             			</TD>


					</TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>
</form>
<logic:present name="showStatusLogs">
<logic:equal name="showStatusLogs" value="yes">


<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">Assessor Import Status</FONT>
          (<a href="<%=request.getContextPath()%>/assessorImport.do?action=showStatus">REFRESH</a> this page)
		<logic:notPresent name="enableAutoRefresh" >
	          (<a href="<%=request.getContextPath()%>/assessorImport.do?action=showStatus&enableAutoRefresh=true">Enable Auto Refresh</a>)
        </logic:notPresent>
		<logic:present name="enableAutoRefresh" >
    		 (This page will refresh every 5 seconds automatically)
        </logic:present>
          <BR>
            <BR>
            </TD>
        </TR>
        <TR>
        </tr>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Assessor Import Log (<%=size%> Messages found)</TD>
                      <TD width="1%" class="tablebutton"><NOBR>

					  <logic:equal name="showClearLogs" value="yes">
                      	<a href="javascript:deleteAllRecords();">CLEAR LOGS</a>
                      </logic:equal>

                      </NOBR></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Date</TD>
                      <TD class="tablelabel">Status</TD>
                      <TD class="tablelabel">Category</TD>
                      <TD class="tablelabel">Message</TD>
                    </TR>
 					<%
	   					while (crset.next()) {
	   						Object dateObject = crset.getObject("UPDATED");
	   						oracle.sql.TIMESTAMP oracleTS = (oracle.sql.TIMESTAMP) dateObject;
	   						String status = crset.getString("STATUS");
	   						String category = crset.getString("TABLE_NAME");
	   						String logMessage = crset.getString("MESSAGE");
 					%>
            	    <TR valign="top">
	 					<TD class="tabletext">
             			<%=elms.util.StringUtils.truncateMilliseconds(oracleTS.timestampValue().toString()) %>

             			</TD>

	 					<TD class="tabletext">
             			<%=status %>

             			</TD>

 	 					<TD class="tabletext">
             			<%=category %>

             			</TD>

 	 					<TD class="tabletext">
             			<%=logMessage %>

             			</TD>

					</TR>
 					<%
	 					}
	 					if(crset!=null) crset.close();
 					%>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>
</logic:equal>
</logic:present>
</BODY>
</html:html>

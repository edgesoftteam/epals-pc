<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath();%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="javascript">
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");


function test(){
    document.getElementById('addressDiv').innerHTML = '';
	document.getElementById("addressDiv").style.display =  'none';
	document.getElementById('loader').innerHTML = '';
}

function checkTable(){

	document.forms['pcStatusForm'].elements['Lookup'].disabled = true;
	document.getElementById('loader').innerHTML = '<img src="../images/loader.gif" style="float:right;">';

	var url = "<%=contextRoot%>/pcStatusAdmin.do?lookup=Yes";

	
	if(document.forms['pcStatusForm'].elements['description'].value != ''){
		url = url + "&description=" + document.forms['pcStatusForm'].elements['description'].value;
	}
	
	/* if(document.forms['pcStatusForm'].elements['moduleId'].value != '-1'){
		url = url + "&moduleId=" + document.forms['pcStatusForm'].elements['moduleId'].value;
	} */
	
	if(document.forms['pcStatusForm'].elements['active'].checked == true){
		url = url + "&active=Y";
	}
	else{
		url = url + "&active=N";
	}
	if(document.forms['pcStatusForm'].elements['visible'].checked == true){
		url = url + "&visible=Y" ;
	}else{
		url = url + "&visible=N";
	}
	
	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);
}

function checkTableHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
				if(result == 'Plan Check Status Saved Successfully'){
					document.getElementById('saveDiv').innerHTML = result;
				}else{
					document.forms['pcStatusForm'].elements['Lookup'].disabled = false;
					document.getElementById('loader').innerHTML = '';
					if(result == 'No Records Found'){
						document.getElementById('saveDiv').innerHTML = result;
					}else{
						document.getElementById('addressDiv').innerHTML = result;
					}
				}
			    document.getElementById("addressDiv").style.display =  '';
 	}
}


if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}

</script>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction() {
	strValue=false;
	
	if (document.forms['pcStatusForm'].elements['description'].value == ''){
		alert("Enter Description");
		document.forms['pcStatusForm'].elements['description'].focus();
		strValue=false;
	}/* else if (document.forms['pcStatusForm'].elements['moduleId'].value == '-1') {
		alert("Select Module");
		document.forms['pcStatusForm'].elements['moduleId'].focus();
		strValue=false;
	} */else{
		var url = "<%=contextRoot%>/pcStatusAdmin.do?save=Yes";
		if(document.forms['pcStatusForm'].elements['code'].value != '-1'){
			url = url + "&code=" + document.forms['pcStatusForm'].elements['code'].value;
		}
		if(document.forms['pcStatusForm'].elements['description'].value != ''){
			url = url + "&description=" + document.forms['pcStatusForm'].elements['description'].value;
		}
		/* if(document.forms['pcStatusForm'].elements['moduleId'].value != '-1'){
			url = url + "&moduleId=" + document.forms['pcStatusForm'].elements['moduleId'].value;
		} */
		if(document.forms['pcStatusForm'].elements['active'].checked == true){
			url = url + "&active=Y";
		}
		else{
			url = url + "&active=N";
		}
		if(document.forms['pcStatusForm'].elements['visible'].checked == true){
			url = url + "&visible=Y" ;
		}else{
			url = url + "&visible=N";
		}
		//alert("url"+url)
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
	    xmlhttp.send(null);
	}
}


function refresh(){
     document.forms[0].action='<%=contextRoot%>/pcStatusAdmin.do';
     document.forms[0].submit();
}

var no_array = new Array();
function checkIds(val){
	var valNew=val;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false ){
		document.forms[0].checkboxArray.value = removeFromArray(valNew.value,no_array).join(",");
     	checkboxArray = removeFromArray(valNew.value,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}
	if(valNew != ""){
	 checkboxArray += valNew.value+",";
	}
	document.forms[0].checkboxArray.value = checkboxArray;
	return true;
}

function removeFromArray(val, ar){
	s = String(ar)
	// remove if not first item (global search)
	reRemove = new RegExp(","+val,"g")
	s = s.replace(reRemove,"")
	// remove if at start of array
	reRemove = new RegExp("^"+val+",")
	s = s.replace(reRemove,"")
	// remove if only item
	reRemove = new RegExp("^"+val+"$")
	s = s.replace(reRemove,"")
	return new Array(s)
}

	function removeTable(){

	var url = "<%=contextRoot%>/pcStatusAdmin.do?delete=Yes";

	url = url + "&checkboxArray=" + document.forms['pcStatusForm'].elements['checkboxArray'].value;
	
	if(document.forms['pcStatusForm'].elements['code'].value != ''){
		url = url + "&code=" + document.forms['pcStatusForm'].elements['code'].value;
	}
	if(document.forms['pcStatusForm'].elements['description'].value != ''){
		url = url + "&description=" + document.forms['pcStatusForm'].elements['description'].value;
	}
	/* if(document.forms['pcStatusForm'].elements['moduleId'].value != '-1'){
		url = url + "&moduleId=" + document.forms['pcStatusForm'].elements['moduleId'].value;
	} */
	if(document.forms['pcStatusForm'].elements['active'].checked == true){
		url = url + "&active=Y";
	}
	else{
		url = url + "&active=N";
	}
	if(document.forms['pcStatusForm'].elements['visible'].checked == true){
		url = url + "&visible=Y" ;
	}else{
		url = url + "&visible=N";
	}
	//alert("URL :: " +url);
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
    	xmlhttp.send(null);
}

//Start Ajax functionality for Activity Type
var xmlhttp;

	function CreateXmlHttp(str, editFlag){

	//Assigning Activity Id to the hidden variable
	document.forms['pcStatusForm'].elements['code'].value = str;
	document.forms['pcStatusForm'].elements['editFlg'].value = editFlag;

	//Creating object of XMLHTTP in IE
		var url="<%=contextRoot%>/pcStatusAdmin.do?id="+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		  }else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 if(xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ActivityType;
			xmlhttp.send(null);
		  }else{
		  alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ActivityType(){

	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){

		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){

		    var text=xmlhttp.responseText;
			text=text.split(',');
			document.forms['pcStatusForm'].elements['code'].value = text[0];

			/* if(document.forms['pcStatusForm'].elements['editFlg'].value == 'N'){
				document.forms['pcStatusForm'].elements['code'].disabled=true;
			}else{
				document.forms['pcStatusForm'].elements['code'].disabled=false;
			}
 */
			document.forms['pcStatusForm'].elements['description'].value = text[1];
			
			/* document.forms['pcStatusForm'].elements['moduleId'].value = text[2]; */
			
			if(text[2] == 'true'){
				document.forms['pcStatusForm'].elements['active'].checked =true;
			}else{
				document.forms['pcStatusForm'].elements['active'].checked =false;
			}

			if(text[3] == 'true'){
				document.forms['pcStatusForm'].elements['visible'].checked =true;
			}else{
				document.forms['pcStatusForm'].elements['visible'].checked =false;
			}

			/* if(document.forms['pcStatusForm'].elements['code'].value == ""){
				document.forms['pcStatusForm'].elements['description'].value = "";
				document.forms['pcStatusForm'].elements['moduleId'].value = "";
				document.forms['pcStatusForm'].elements['active'].checked =false;
				document.forms['pcStatusForm'].elements['visible'].checked =false;
			
			} */
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}
//End Ajax functionality for Activity Type
</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="test()">
<html:form focus="description" name="pcStatusForm" type="elms.control.beans.admin.PCStatusForm" action="">
<html:hidden property="code"/>
<html:hidden property="checkboxArray" value=""/>
<html:hidden property="editFlg" value=""/>

<div id="saveDiv" align="justify" style="color:#088A08"></div>

<logic:notEqual value="YES" property="displayPCStatus" name="pcStatusForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="100%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="36px"><font class="con_hdr_3b">Plan Check Status Administration</font></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="Reset" value="Reset" styleClass="button" onclick="refresh();"/>
                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return checkTable();"></html:button>
                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <%-- <td class="tablelabel">Code</td>
                      <td class="tabletext">
                      	<html:text property="code" size="25" maxlength="6" styleClass="textbox"/>&nbsp;
                      </td> --%>
                      <td class="tablelabel">Description</td>
                      <td class="tabletext" colspan="3">
                        <html:text  property="description" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
                      </td>
                      <%--  <td class="tablelabel">Module</td>
                      <td class="tabletext">
                      
                        <html:select property="moduleId" styleClass="textbox">
                        	  <html:option value="-1">Please Select</html:option>
                              <html:options collection="modules" property="id" labelProperty="description" />
                        </html:select>
          
                      </td> --%>
                    </tr>
                    
                    <tr>
                     
					  <td class="tablelabel">Active</td>
                      <td class="tabletext">
                     	<html:checkbox  property="active" styleClass="textbox"></html:checkbox>
                      </td>
                       <td class="tablelabel">Visible</td>
                      <td class="tabletext">
                     	<html:checkbox  property="visible" styleClass="textbox"></html:checkbox>
                      </td>
                    </tr>
					
                </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
    </td>

  </tr>
</table>
</logic:notEqual>
<div id="loader" style="padding-right:50%;"><span><img src="../images/loader.gif"></span></div>
<div id="addressDiv"></div>

</html:form>
</body>
</html:html>

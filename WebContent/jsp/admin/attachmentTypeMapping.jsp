
<%-- <%@page import="alain.core.utils.Operator"%> --%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="elms.control.beans.admin.AttachmentTypeMappingForm"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>
<% 
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
AttachmentTypeMappingForm f = (AttachmentTypeMappingForm)request.getAttribute("attachmentTypeMappingForm");

String addFlag = (String) request.getAttribute("addFlag");

%>

<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="x-ua-compatible" content="IE=9"/> 
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%-- <script language="JavaScript" src="<%=contextRoot%>/tools/ckeditor/jquery/jquery.min.js"></script>
<script language="JavaScript" src="<%=contextRoot%>/tools/ckeditor/jquery/jquery-migrate-1.2.1.js"></script> --%>
<%-- <script language="javascript" type="text/javascript" src="<%=contextRoot%>/tools/ckeditor/ckeditor_4.1/ckeditor.js"></script> --%>

<script type="text/javascript">
/* $(function(){
	
	$(document).ready(function() {});
	
	set();
	$('#backnav').click(function() {
		window.location = "index.jsp";	
	});
	
	$(window).on('beforeunload', function(){
		//return 'Are you sure you want to leave without saving?';
	});	
});
 */

/* function clean(){} */

function dosave() {
	
	try{
		
	   if(document.forms[0].description.value == -1){
		   alert("Please enter attachment type");
		   return false;
	   }
	   
	   if(document.forms[0].actType.value == -1){
		   alert("Please enter activity type");
		   return false;
	   }
	}catch(Exception){}
	   return true;
	  
} 
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/attachmentTypeMapping?action=save" onsubmit="return dosave();">
 <%-- <html:hidden property="refId"/> --%>
<%--<html:hidden property="emailTempId"/>
<html:hidden property="emailTempTypeId"/> --%>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Attachment Types Administration<br>
            <br>
            </font></td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b></font>
        </TD>
        </TR>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_2b">Manage Attachment Types
                        </font></td>
                      <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../images/site_bg_D7DCE2.jpg"><nobr> 
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                      
					  <html:submit value="Save" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td background="../images/site_bg_B7C1CB.jpg"> 
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  
                    <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Attachment Type</font></td>
	                     <td bgcolor="#FFFFFF"> 
	                      <%if(addFlag != null && addFlag.equalsIgnoreCase("Y")){ %>
	                      	<html:select property="description" styleClass="textbox">
                        	  <html:option value="-1">Please Select</html:option> 
                              <html:options collection="attachmentTypeList" property="attachmentTypeId" labelProperty="attachmentTypeDesc" />
                        	</html:select>
                        <%}else{ %>
                        	<html:select property="description" styleClass="textbox">
                        	  <%-- <html:option value="-1">Please Select</html:option>  --%>
                              <html:options collection="attachmentTypeList" property="attachmentTypeId" labelProperty="attachmentTypeDesc"/>
                        	</html:select>
                        <%}%>
	                     </td>
	                </tr>
	                 <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Activity Type</font></td>
	                     <td bgcolor="#FFFFFF">	                      	
	                      	<html:select property="actType" styleClass="textbox">
                        	  <%-- <html:option value="-1">Please Select</html:option>  --%>
                              <html:options collection="activityTypesList" property="actTypeId" labelProperty="actTypeDesc" />
                        	</html:select>
	                     </td>
	                </tr>    	
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
 
</html:form>
</body>
</html:html>

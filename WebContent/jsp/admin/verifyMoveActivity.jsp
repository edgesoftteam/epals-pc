<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
      String contextRoot = request.getContextPath();
%>
<SCRIPT language="JavaScript1.2">

function processPage() {
	document.forms[0].action='<%=contextRoot%>/saveMoveActivity.do';
 	document.forms[0].submit();}

function cancelPage() {
	document.forms[0].action='<%=contextRoot%>/moveProject.do';
 	document.forms[0].submit();
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="moveVerifyForm" type="elms.control.beans.MoveVerifyForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="50%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Move Activity Verify</font><br>
            <br>
            </td>
        </tr>
		<html:errors/>
		<tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="3">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98%" class="tabletitle">Move
                        Activity </td>

                      <td width="1%" class="tablebutton"><nobr>
<logic:notPresent name="moveVerifyForm" property="message" >
			          <html:button property="ok" value="OK" styleClass="button" onclick="return processPage();"/>
</logic:notPresent>
					  <html:button property="cancel" value="Cancel" styleClass="button" onclick="return cancelPage();"/>
                       &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
		<tr>
			<td colspan="2" class="tabletext">&nbsp;</td>
		</tr>
<logic:present name="moveVerifyForm" property="message">
		<tr>
			<td><bean:write name="moveVerifyForm" property="message"/></td>
		</tr>
</logic:present>
        <tr>
          <td>
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                            <td class="tabletitle">Activity Details</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width= "45%" class="tablebutton">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">

                            <td class="tablelabel">Activity #</td>
                            <td class="tabletext"><bean:write name="moveVerifyForm" property="activityNbr" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Type</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="activityType" /></td>
                    </tr>

                    <tr valign="top">
                      <td class="tablelabel">Description</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="activityDescription" />&nbsp;</td>
                    </tr>

                    <tr valign="top">
                      <td class="tablelabel">Amount Due</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="amtDue" />&nbsp;</td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Amount Paid </td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="amtPaid" />&nbsp;</td>
                    </tr>

                  </table>
                </td>
              </tr>
            </table>
          </td>
          <td width="10%"><font class="con_text_1" align="center">to</font></td>
          <td width= "45%" background="../images/site_bg_B7C1CB.jpg">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                            <td class="tabletitle">Sub
                              Project Details</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td width= "45%" class="tablebutton">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                            <td class="tablelabel">Sub Project #</td>
                            <td class="tabletext"><bean:write name="moveVerifyForm" property="subProjectNbr" /></td>
                    </tr>
                    <tr valign="top">
                            <td class="tablelabel">Sub Project Name</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="subProjectName" /> </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Description</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="subProjectDescription" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel">Type</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="subProjectType" /></td>
                    </tr>
                   <tr valign="top">
                      <td class="tablelabel">Sub-Type</td>
                      <td class="tabletext"><bean:write name="moveVerifyForm" property="subProjectSubType" /></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
                </td>
              </tr>
            </table>
		  </td>
        </tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type="hidden" name="subProjectId" value="<bean:write name="moveVerifyForm" property="subProjectId" />">
<input type="hidden" name="activityId" value="<bean:write name="moveVerifyForm" property="activityId" />">
</html:form>
</body>
</html:html>

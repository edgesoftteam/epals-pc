<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>


<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="50%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Move LSO Success</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="3">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98%" class="tabletitle">&nbsp;</td>

                      <td width="1%" class="tablebutton"><nobr>
                       &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
		<tr>
			<td colspan="2" class="tabletext">&nbsp;</td>
		</tr>
		<tr>
			<td>Move LSO Completed Successfully</td>
		</tr>
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>

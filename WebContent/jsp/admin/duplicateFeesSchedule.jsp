<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/maintainFeesSchedule">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Duplicate Selected Fee Schedule</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%">Duplicate
                        Fee Schedule</td>

                        <td width="1%" class="tablebutton" align="right"><nobr>
                          <html:submit  value="Cancel"  styleClass="button"/>
			  <html:submit  value="Update"  styleClass="button"/>
                          </nobr>
    		      </td>
 	            </tr>
                   </table>
                </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td colspan="3" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Activity
                        Type</td>
                      <td class="tablelabel">Fee
                        Description</td>
                      <td class="tablelabel">Effective</td>
                      <td class="tablelabel">Fee/Unit</td>
                      <td class="tablelabel">Factor</td>
                      <td class="tablelabel">Up</td>
                      <td class="tablelabel">Seq</td>
                    </tr>
                    <tr class="tabletext">
                      <td>
                          <html:select property="activityType" styleClass="textbox">
                               <html:option value="1">Type1</html:option>
                          </html:select>
                      </td>
                      <td>
			  <html:text property="feeDescription" styleClass="textbox" size="20" />
                      </td>
                      <td><nobr>
                        <html:text  property="effectiveDate" value="" styleClass="textbox"/>

			<html:link href="javascript:show_calendar('feeScheduleForm.effectiveDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link>
			  </nobr>
                      </td>
                      <td>
                        <html:text  property="feeUnit" value="" size="10" styleClass="textbox"/>
                      </td>
                      <td>
                        <html:text  property="factor" value="" size="10" styleClass="textbox"/>
                      </td>
                      <td>
                          <html:select property="up" styleClass="textbox">
                               <html:option value="1">Factor</html:option>
                          </html:select>
                      </td>
                      <td>
                        <html:text  property="seq" value="" size="5" styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

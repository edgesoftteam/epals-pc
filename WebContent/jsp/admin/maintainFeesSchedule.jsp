<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<app:checkLogon/>

<%@ page import="elms.app.finance.*" %>
<%@ page import="elms.app.project.*" %>
<%@ page import="java.util.*"%>
<%@ page import="elms.util.*"%>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
String contextRoot = request.getContextPath();
List activityTypes = (List) session.getAttribute("activityTypes");
pageContext.setAttribute("activityTypes", activityTypes);

String activityType = (String) session.getAttribute("activityType");
String error = (String) request.getAttribute("error")!=null ? (String) request.getAttribute("error") : "" ;
%>
<script>
function onclickprocess(strval)
{
    if (strval == 'cfs') {
       document.feeScheduleForm.action='<%=contextRoot%>/cancelFeesSchedule.do';
       document.feeScheduleForm.submit();
       return true;
    }else if (strval == 'efsedit') {
       document.feeScheduleForm.action='<%=contextRoot%>/editFeesSchedule.do?action=Edit';
       document.feeScheduleForm.submit();
       return true;
    }else if (strval == 'dfs') {
       document.feeScheduleForm.action='<%=contextRoot%>/deleteFeesSchedule.do';
       document.feeScheduleForm.submit();
       return true;
    }else if (strval == 'efsdup') {
       document.feeScheduleForm.action='<%=contextRoot%>/editFeesSchedule.do?action=Duplicate';
       document.feeScheduleForm.submit();
       return true;
    }else if (strval == 'pfs') {
       document.feeScheduleForm.action='<%=contextRoot%>/previewFeesSchedule.do';
       document.feeScheduleForm.submit();
       return true;
    }else if (strval == 'chfs') {
       document.feeScheduleForm.action='<%=contextRoot%>/changeFeesSchedule.do';
       document.feeScheduleForm.submit();
       return true;
     }else if (strval == 'efsadd') {
       document.feeScheduleForm.action='<%=contextRoot%>/editFeesSchedule.do?action=Add';
       document.feeScheduleForm.submit();
       return true;
    }
}
function onchangeprocess()
{
    document.feeScheduleForm.action='<%=contextRoot%>/maintainFeesSchedule.do';
		if(document.feeScheduleForm.activityType.value=="dummy"){
			alert("Choose the proper option");
		document.feeScheduleForm.activityType.value="";
		return false;
	}
    document.feeScheduleForm.submit();
    return true;
}
</script>
<html:form focus="activityType" name="feeScheduleForm" type="elms.control.beans.FeeScheduleForm" action="">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Fees Maintenance</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%">Fee Schedule</td>
                      <td width="1%" class="tablebutton" align="right"><nobr>
                        <html:button  property="cancel" value="Cancel" styleClass="button" onclick="return onclickprocess('cfs')" />
                      </nobr>
					  </td>
                   </tr>
                  </table>
                </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Find
                        / Browse</td>
                      <td class="tablelabel" colspan="5"><nobr>Name:
                      <html:select property="activityType" value="<%=activityType%>" styleClass="textbox" onchange="return onchangeprocess()">
                      	<html:option value="">Please Select</html:option>
                      	<html:option value="ALL">All</html:option>
						<html:options collection="activityTypes" property="type" labelProperty="description"/>
					  </html:select>
                        </nobr></td>
                    </tr>
                    <tr>
                      <td class="tablelabel"><nobr>Check
                        Functions ></nobr></td>
                      <td class="tablelabel">
                        <html:button  property="edit" value="Edit" styleClass="button" onclick="return onclickprocess('efsedit')" />
                      </td>
                      <td class="tablelabel">
                        <html:button  property="delete" value="Delete" styleClass="button" onclick="return onclickprocess('dfs')" />
                      </td>
                      <td class="tablelabel">
                        <html:button  property="duplicate" value="Duplicate" styleClass="button" onclick="return onclickprocess('efsdup')" />
                      </td>
                      <td class="tablelabel"> <nobr>Change
                        &quot;%&quot;
                        <html:text property="percent" styleClass="textbox" size="5" value="0" onkeypress="return validateNumeric();"/>
                        <html:button  property="preview" value="Preview" styleClass="button" onclick="return onclickprocess('pfs')" />
                        <html:button  property="change" value="Change" styleClass="button" onclick="return onclickprocess('chfs')" />
                        </nobr> </td>
                      <td class="tablelabel">
                        <html:button  property="addFee" value="Add Fee" styleClass="button" onclick="return onclickprocess('efsadd')" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" colspan="6"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>

                </td>
              </tr>
 	            	<tr>
 	            	 <td colspan="3"><font class="red3"><%= error %></font></td>
 	            	</tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Check</td>
                      <td class="tablelabel">Seq</td>
                      <td class="tablelabel">Activity Type</td>
                      <td class="tablelabel">Fee Description</td>
                      <td class="tablelabel">Effective</td>
                      <td class="tablelabel">Fee/Unit</td>
                      <td class="tablelabel">Factor</td>
                      <td class="tablelabel">Up</td>
                    </tr>
<%
						if (!activityType.equals("")) {
%>
						<nested:iterate property="feeEditList">
                            <tr class="tabletext">
                                <td class="tabletext" align="center"><nested:checkbox property="check" ></nested:checkbox></td>
                                <td class="tabletext"> <nested:text property="sequence" readonly="true" styleClass="textboxm" size="4" /> </td>
                                <td class="tabletext"> <nested:text property="actType" readonly="true" styleClass="textbox" size="12" /> </td>
                                <td class="tabletext"> <nested:text property="description" readonly="true" styleClass="textbox" size="40" /> </td>
                                <td class="tabletext"> <nested:text property="creationDate" readonly="true" styleClass="textboxm" size="10" /> </td>
                                <td class="tabletext"> <nested:text property="feeFactor" readonly="true" styleClass="textboxm" size="10" /> </td>
                                <td class="tabletext"> <nested:text property="factor" readonly="true" styleClass="textboxm" size="10" /> </td>
                                <td class="tabletext" align="center"><nested:checkbox property="up" ></nested:checkbox></td>
                            </tr>
						</nested:iterate>
<%
						}
%>
                  </table>
                </td>
              </tr>

        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>

</body>
</html:html>

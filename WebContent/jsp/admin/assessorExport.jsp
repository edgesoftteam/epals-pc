<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>



<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/calendar_assessor.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script>

function validateFunction()
{

       var strValue=false;
       strValue=validateData('req',document.forms[0].elements['date'],'Date is a required field');
       if (strValue == true)
       {
 		   strValue=validateData('req',document.forms[0].elements['description'],'Description is a required field');
  	   }
  	   return strValue;
}


</script>

</head>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";

String path =  (String)request.getAttribute("path");
if(path==null ) path ="";


%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/assessorExport.do?type=import">

<font color='green'><b><%=message%></b></font>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b"></font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Please enter the Month For Assessor Export
                        </td>

                      <td width="1%" class="tablebutton"><nobr>
					  <html:submit value=" Fetch " styleClass="button" />&nbsp;
					  <%if(!(path.equals(""))){
							path= "pdf/"+path; %>
						<input type ="button" class="button" value="View File" onclick ="javascript:window.open('<%=path%>')"/>
					  <%} %>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Date (mm/yyyy)</TD>
                    </TR>
            	    <TR valign="top">
	 					<TD class="tabletext">
             		<html:text  property="date" size="7" styleClass="textbox" maxlength="7" onkeypress="return validateDate();"/>
             		<html:link href="javascript:show_calendar('forms[0].date');"
						    onmouseover="window.status='Calendar';return true;"
						    onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link>

             			</TD>
             		</TR>

                  </TBODY></TABLE>
                </TD>
              </TR>
           </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>


<%@page import="elms.util.Operator"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="elms.control.beans.admin.NoticeTemplateAdminForm"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>
<% 
String contextRoot = request.getContextPath();
String noticeTemplate = (String)request.getAttribute("noticeTemplate");
String message = (String)request.getAttribute("message");
if(message==null) message="";
if(noticeTemplate==null) noticeTemplate="";
NoticeTemplateAdminForm f = (NoticeTemplateAdminForm)request.getAttribute("noticeTemplateAdminForm");
ArrayList actionList = new ArrayList();
actionList =  (ArrayList) request.getAttribute("actionList");
ArrayList noticeTypeList = new ArrayList();
noticeTypeList =  (ArrayList) request.getAttribute("noticeTypeList");

String content = Operator.javascriptFriendly(f.getMessage());
if(content==null) content="";



%>


<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="x-ua-compatible" content="IE=9"/> 
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
<script language="JavaScript" src="<%=contextRoot%>/tools/jquery/jquery-migrate-1.2.1.js"></script>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/tools/ckeditor_4.1/ckeditor.js"></script>


<script type="text/javascript">
$(function(){
	
	$(document).ready(function() {
	
		
	
	});
	
	set();
	$('#backnav').click(function() {
		window.location = "index.jsp";	
	});
	
	$(window).on('beforeunload', function(){
		//return 'Are you sure you want to leave without saving?';
	});

	
});

function set(){
	try{
	CKEDITOR.instances.editor1.setData( "<%=content%>" );
	}catch(Exception){}
}

function clean(){
	
}

function dosave() {

   if(document.forms[0].noticeTemplateName.value ==''){
	   alert("Please enter a notice template name");
	   return false;
   }
	   
   if(document.forms[0].noticeTypeId.value ==''){
		   alert("Please select notice type");
		   return false;
	 } 
  //alert(document.forms[0].CONTENT.value);
    //document.forms[0].CONTENT.value;
	document.forms[0].submit();
	 
	
} 
</script>
</head>


<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/noticeTemplateAdmin?action=save" onsubmit="return dosave();">
 <html:hidden property="id"/>&nbsp;
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Notice Templates Administration<br>
            <br>
            </font></td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b></font>
        </TD>
        </TR>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_2b">Manage Templates
                        </font></td>
                      <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../images/site_bg_D7DCE2.jpg"><nobr> 
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                      
					  <html:submit value="Save" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td background="../images/site_bg_B7C1CB.jpg"> 
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  
                    <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Notices Template Name</font></td>
	                     <td bgcolor="#FFFFFF"> 
	                      <html:text property="noticeTemplateName" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
	                     </td>
	                </tr>
	                
	                  <html:hidden property="actionId" value="1" />   
	               <!--  <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Notices Template Action</font></td>
	                     <td bgcolor="#FFFFFF"> 
	                         
	                           <html:select  styleClass="textbox"  property="actionId" onchange="reSubmit();" >
						      	   <html:option value="">Please Select</html:option>
						           <html:options  collection="actionList"  property="fieldOne"  labelProperty="fieldTwo" />
						      </html:select>
	                      &nbsp;
	                     </td>
	                </tr> -->
	                
	                <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Notices Type</font></td>
	                     <td bgcolor="#FFFFFF"> 
	                           <html:select  styleClass="textbox"  property="noticeTypeId" onchange="reSubmit();" >
						      	   <html:option value="">Please Select</html:option>
						           <html:options  collection="noticeTypeList"  property="ceTypeId"  labelProperty="name" />
						      </html:select>
	                      &nbsp;
	                     </td>
	                </tr>
	                  
                  	<tr>
						<td bgcolor="#FFFFFF" width="100%" colspan="2">
							<textarea name="CONTENT" id="editor1" ></textarea>
						</td>
					</tr>
					
<!-- ui -->                   
                   
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
 <script>
								
	CKEDITOR.timestamp = null;      
	CKEDITOR.config.toolbar_MA=[
	                           
	                        	{ name: 'styles', items : [ 'FontSize','Font','Styles','Heading1' ] },
	                        	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ] },
	                        	'/',
	                        	
	                        	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	                        	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
	                        	'/',
	                        	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll' ] },
	                        	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	                        	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	                        	{ name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak','Iframe' ] },
	                        	'/',
	                        	{ name: 'tools', items : [ 'Source','-','Maximize', 'ShowBlocks','RemoveFormat','-','SpellChecker', 'Scayt'] },
	                        	{ name: 'es', items : [ 'eslink'] },
	                        	
	                        ];
	
	CKEDITOR.replace( 'editor1', {
		fullPage: true,
		toolbar:'MA',
		extraPlugins: 'wysiwygarea',
		//extraPlugins: 'eslink',
		extraPlugins: "imagebrowser,eslink",
	    imageBrowser_listUrl: "images_list.json",    
		removePlugins : 'forms',
		enterMode : CKEDITOR.ENTER_BR,
		shiftEnterMode : CKEDITOR.ENTER_BR
		//uiColor:'#666666'
		
		
		
	});

	

</script>	
</html:form>
</body>
</html:html>

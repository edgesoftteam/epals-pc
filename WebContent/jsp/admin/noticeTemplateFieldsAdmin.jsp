<%@page import="java.util.List"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="elms.control.beans.admin.NoticeTemplateAdminForm"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>

<html:html>
<app:checkLogon/>
<% 
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
List<DisplayItem> noticeTemplateFieldsList = new ArrayList<DisplayItem>();
noticeTemplateFieldsList =  (List<DisplayItem>) request.getAttribute("noticeTemplateFieldsList");
%>



<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="x-ua-compatible" content="IE=9"/> 
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
<script language="JavaScript" src="<%=contextRoot%>/tools/jquery/jquery-migrate-1.2.1.js"></script>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/tools/ckeditor_4.1/ckeditor.js"></script>

<%
int j=0;
String fieldDescription=""; 
for(int i=0;i<noticeTemplateFieldsList.size();i++)
{
	fieldDescription=fieldDescription + '"'+((DisplayItem)noticeTemplateFieldsList.get(i)).getFieldThree()+'"';
	
	if(i<noticeTemplateFieldsList.size()-1)
	{
		fieldDescription= fieldDescription + ",";	
	}
}
%>
<script type="text/javascript">

var colArray = new Array(<%=fieldDescription%>);   

function fieldNameSelection()
{
	var idVal= document.getElementById('fieldId').value -1;
	document.getElementById('fieldDescription').value=colArray[idVal];
	
	
	return true;
}

function saveDate()
{
	var idVal= document.getElementById('fieldId').value;
	var desc= document.getElementById('fieldDescription').value;
	
	document.forms[0].action="<%=contextRoot%>/noticeTemplateFieldsAdmin.do?action=save&fieldId="+idVal+"&fieldDescription="+desc;
	document.forms[0].submit();  
}
</script>
</head>
     

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<form name="noticeTemplateAdminForm" action="<%=contextRoot%>/noticeTemplateFieldsAdmin.do?action=save" method="post">
 &nbsp;
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Notice Fields Administration<br>
            <br>
            </font></td>
        </tr>
       
        <TR>
        <TD>
        <font color='green'><b><%=message%></b></font>
        </TD>
        </TR>
        
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_2b">Manage Notice Fields
                        </font></td>
                      <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../images/site_bg_D7DCE2.jpg"><nobr> 
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                      
					  <html:button value="Save" property="Save" onclick="saveDate()" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td background="../images/site_bg_B7C1CB.jpg"> 
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  
                  <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Field Names</font></td>
	                     <td bgcolor="#FFFFFF"> 
	                         <select  class="textbox"  id="fieldId" property="fieldId" onchange="fieldNameSelection()" >
						      	   <option value="">Please Select</option>
						      	   <%
						      	   for(int i=0;i<noticeTemplateFieldsList.size();i++)
						      	    {
						      		 
						      		
						      	   %>
						      	   <option value="<%=((DisplayItem)noticeTemplateFieldsList.get(i)).getFieldOne() %>"><%=((DisplayItem)noticeTemplateFieldsList.get(i)).getFieldTwo()%></option>
						      	   <%
						      	    }
						      	   %>
						       </select>
	                      &nbsp;
	                     </td>
	                </tr>
	                    
                    
                    <tr valign="top"> 
	                     <td background="../images/site_bg_D7DCE2.jpg" ><font class="con_hdr_1">Notices Template Field Description</font></td>
	                     <td bgcolor="#FFFFFF"> 
	                      <input type="text" name="fieldDescription" id="fieldDescription" size="50" maxlength="100" class="textbox"/>&nbsp;
	                     </td>
	                </tr>
	                
	                  
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</form>
</body>
</html:html>

<%@ page import="java.sql.*" %>

<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<title>SQL Search Page</title>
</head>
<body bgcolor="#EAEAFF">
<%
boolean firstTime = true;
boolean moreResults = false;
String param = request.getParameter("sql");
if(param==null) {
	param="select count(*) from lso_address";
	firstTime=false;
}
java.sql.ResultSet rs = null;

%>
<form action="sql.jsp" method=post>
<p><font size="4"><u>Database Query</u></p>

<table border="0" width="80%">
	<tr>
		<td ><textarea cols="80" rows="10" name=sql ><%=param%></textarea></td>

		<td><input type=submit name=submit value="Query" size="20%"></td>

	</tr>

</table>
</form>
<%
if(firstTime){

	try{
  		java.sql.Connection conn = new elms.util.db.Wrapper().getConnectionForPreparedStatementOnly();
  		java.sql.Statement stmt = conn.createStatement();
  		moreResults = stmt.execute(param);
  		rs = stmt.getResultSet();

  		ResultSetMetaData meta = rs.getMetaData();

		int num_cols = meta.getColumnCount();

	%>
	<body>
	<p><font size="3"><u>Query Results </u></p>
	<table border="0" bgcolor="#000000" >
		<tr bgcolor="#0066cc" >
			<%
				for (int i = 1; i <= num_cols; i++) {
			%>
				<td ><b><font color="#FFFFFF"><%=meta.getColumnName(i)%></b></td>
			<%
			}
			%>
		</tr>
			<%
			while(rs.next()){
			%>
		<tr class="tabletext">
		<%
				for (int i = 1; i <= num_cols; i++) {
		%>

				<td><font color="#000000"><%=rs.getString(i)%></td>
		<%
				}
		%>
		</tr>

		<%
			}
		%>
<%
	if(rs!=null) rs.close();
	if(stmt!=null) stmt.close();
	if(conn!=null) conn.close();
	%>
	<%
	}
	catch(Exception e){
		out.println("The error is:"+e.getMessage());
		//e.printStackTrace(response.getWriter());
	}
%>
</table>
<%
}
%>

</body>

</html>

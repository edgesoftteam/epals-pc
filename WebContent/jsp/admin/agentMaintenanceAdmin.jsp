<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="java.util.List"%>
<%@page import="elms.app.people.People"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<% 
	String contextRoot = request.getContextPath();
	List<People> agentContractorList  = (List)request.getAttribute("agentContractorList");
	String filter = (String)request.getAttribute("filter");
%>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
	font: normal 8pt/9pt verdana,arial,helvetica;
	color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<html:form action="/agentMaintenanceAdmin.do" name="peopleForm" type="elms.control.beans.PeopleForm" >
		<table width="100%" border="0" cellspacing="10" cellpadding="0" >
			<tr valign="top">
				<td width="100%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr><td><font class="con_hdr_3b">Agent Maintenance List
						<div style="float: right;">
							<input type="text" class="textboxd" id="filter" name="filter" placeholder="Search.." value="<%=filter%>"/>
							<input type="submit" id="search" onclick="filterData();" style="display: none;"/>
						</div>
						<br><br></font></td></tr>
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="100%" height="25" bgcolor="#B7C1CB"><font class="con_hdr_2b">Agent Maintenance</font></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
					<td background="../images/site_bg_B7C1CB.jpg">
						<table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td width="20%" class="tablelabel"><font class="con_hdr_1">Agent Name</font></td>
								<td width="20%" class="tablelabel"><font class="con_hdr_1">Contractor Name</font></td>
								<td width="20%" class="tablelabel"><font class="con_hdr_1">License Number</font></td>
								<td width="10%" class="tablelabel"><font class="con_hdr_1"><nobr>Status</nobr></font></td>
								<td width="10%" class="tablelabel"><font class="con_hdr_1"><nobr>Start Date</nobr></font></td>
								<td width="10%" class="tablelabel"><font class="con_hdr_1"><nobr>Expiration Date</nobr></font></td>
								<td width="10%" class="tablelabel"><font class="con_hdr_1"><nobr>&nbsp;</nobr></font></td>
							</tr>
							<%
							for (int i = 0; i < agentContractorList.size(); i++) {
							People p = (People) agentContractorList.get(i);
							%>
							<tr>
								<td class="tabletext" valign="top">
									<font class="con_text_1">
									<%= p.getAgentName()%>
									</font>
								</td>
								<td class="tabletext" valign="top">
									<font class="con_text_1">
									<%= p.getName()%>
									</font>
								</td>
								<td class="tabletext" valign="top">
									<font class="con_text_1">
									<%=p.getLicenseNbr() %>
									</font>
								</td>
								<td class="tabletext" valign="top">
									<font class="con_text_1">
									<%=p.getStatus() %>
									</font>
								</td>
								<td class="tabletext" valign="top">
									<input type="text" id="startDate_<%=i %>" class="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								</td>
								<td class="tabletext" valign="top">
									<input type="text" id="expirationDate_<%=i %>" class="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								</td>
								<td class="tabletext" valign="top">
									<input type="button" name="verifyContractorName"  value="Verify"  class="button" onclick="javascript:verifyContractor(<%=i%>,<%=p.getCaId()%>)" />
									<input type="button" name="deleteContractorName"  value="Delete"  class="button" onclick="javascript:deleteContractor(<%=i%>,<%=p.getCaId()%>)" />
								</td>
							</tr>
					<%} %>
					</table>
					</td></tr></table></td></tr>
					<tr><td>&nbsp;</td>
			</tr>
		</table>
	</html:form>
	<script type="text/javascript">
		function verifyContractor(id,caId)
		{
			var startDate = document.getElementById('startDate_'+id).value;
			var expDate = document.getElementById('expirationDate_'+id).value;
			
			if(startDate == ''){
				alert('Please select start date.');
				return false;
			} else if(expDate == ''){
				alert('Please select expiration date.');
				return false;
			}
			
			document.location='<%=contextRoot%>/agentMaintenanceAdmin.do?caId='+caId+'&action=verify&startDate='+startDate+'&expDate='+expDate;
		}
		
		function deleteContractor(id,caId)
		{
			
			document.location='<%=contextRoot%>/agentMaintenanceAdmin.do?caId='+caId+'&action=delete';
		}
		
		function filterData(){
			var filterData = document.getElementById('filter').value;
			document.forms[0].action='<%=contextRoot%>/agentMaintenanceAdmin.do?action=filter&filterData='+filterData;
			document.forms[0].submit();
		}
	</script>
</body>
</html:html>

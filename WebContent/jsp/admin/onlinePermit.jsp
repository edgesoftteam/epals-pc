<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>

<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"> </script>
<%
	//the context path
   	String contextRoot = request.getContextPath();
	elms.control.beans.admin.OnlinePermitForm onlinePermitForm = (elms.control.beans.admin.OnlinePermitForm)request.getAttribute("onlinePermitForm");
%>
<script language="JavaScript">

var actType = "";
var stypeId = "";


var str = "";
var strCode = "";
var strDesc = "";
var delimiter = "";
var delimiter1 = "";
function selectActType() {
	var size= document.forms[0].elements['feeId'].length;
	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['feeId'].options[i].selected) {
			str += delimiter + document.forms[0].elements['feeId'].options[i].text;
			var text = document.forms[0].elements['feeId'].options[i].text;
			var value = document.forms[0].elements['feeId'].options[i].value;
			var op = new Option(text,value);
			document.forms[0].elements['selectedFeeId'].add(op,document.forms[0].elements['selectedFeeId'].options.length);
			delimiter = "\r";
			document.forms[0].elements['feeId'].remove(i);
			i--;
			size--;
		}
	}
}

function deselectActType() {
	var size= document.forms[0].elements['selectedFeeId'].length;
	for (var i=0;i<size;i++) {
		if (document.forms[0].elements['selectedFeeId'].options[i].selected) {
			var text = document.forms[0].elements['selectedFeeId'].options[i].text;
			var value = document.forms[0].elements['selectedFeeId'].options[i].value;
			var op = new Option(text,value);
			document.forms[0].elements['feeId'].add(op,document.forms[0].elements['feeId'].options.length);
			document.forms[0].elements['selectedFeeId'].remove(i);
			i--;
			size--;
		}
	}
}

function loadActFees() {
    document.forms[0].action='<%=contextRoot%>/onlinePermit.do?action=edit';
    document.forms[0].submit();
}

function clearFields(){
	str = "";
	strDesc = "";
	strCode = "";
	document.forms[0].stypeId.value=-1;
    document.forms[0].action='<%=contextRoot%>/onlinePermit.do';
    document.forms[0].submit();

}

function validateFunction() {
	
	if(document.forms[0].stypeId.value ==""){
		alert("Please select a Activity SubType");
		document.forms[0].stypeId.focus();
		return false;
	}
	if(document.forms[0].elements['selectedFeeId'].length ==0){
		alert("Please select some fees in order to save");
		return false;
	}
	for(var i=0;i<document.forms[0].elements['selectedFeeId'].length;i++){
		document.forms[0].elements['selectedFeeId'].options[i].selected=true;
	}
	document.forms[0].action='<%=contextRoot%>/onlinePermit.do?action=save';
    document.forms[0].submit();
}

function lkup() {
	document.forms[0].action='<%=contextRoot%>/onlinePermit.do?action=lkup';
    document.forms[0].submit();
}
function getSubTypes() {
	if(document.forms[0].actType.value !=""){
		document.forms[0].action='<%=contextRoot%>/onlinePermit.do?action=dropdown';
    	document.forms[0].submit();
	}
}

function frmLoad(){
	for(var i=0;i<document.forms[0].elements['selectedFeeId'].length;i++){
		document.forms[0].elements['selectedFeeId'].options[i].selected=false;
	}

	document.forms[0].actType.value = actType;
	document.forms[0].stypeId.value = stypeId;
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="frmLoad()">
<html:form action="/onlinePermit">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	                <td><font class="con_hdr_3b">Online Permit Fee Mapping</font><br><br></td>
	            </tr>
	            <tr>
	            <td>
		            <html:errors/>
		            <logic:present name="message" scope="request" >
			            <font color='green'><bean:write name="message"/></font>
		            </logic:present>
	            </td>
	            </tr>
				<tr>
	                <td>
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                        <td>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    <html:errors property="error"/>
			                    <tr>
			                      <td width="99%" class="tabletitle">Add/Update</td>

			                        <td width="1%" class="tablebutton"><nobr>
			                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="return clearFields();"></html:button>
			                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
			                          &nbsp;</nobr></td>
			                      </tr>
	                        </table>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td background="../images/site_bg_B7C1CB.jpg">
	                          <table width="100%" cellpadding="2" cellspacing="1">
								<tr>
									<td class="tablelabel"><nobr>Activity Type</nobr></td>
                                	<td class="tabletext" colspan="3">
                                	<html:select property="actType" styleClass="textbox" onchange="getSubTypes();" >
                                			<option value="" > Please Select</option>
											<option value="200200" > Single-Family Residential</option>
											<option value="200201" > Duplex, Apartment, Condomimium, Townhouse</option>
											<option value="200202" > Commercial/ Industrial</option>
									</html:select>
                                	<!-- <bean:write name="onlinePermitForm" property="actType"/> -->
                                	</td>
									<td class="tablelabel">Activity Sub-Type</td>
		                            <td class="tabletext">
		                               <html:select property="stypeId" styleClass="textbox" onchange="loadActFees()">
		                    	          <html:option value="-1">Please Select</html:option>
		                                  <html:options collection="actSubtypeList" property="stypeId" labelProperty="actSubName" />
		                               </html:select>
		                            </td>
		                        </tr>
	                            <tr>
	                                <td class="tablelabel">Fee List</td>
	                                <td colspan="2" class="tabletext">
										<html:select property="feeId" size="25" style="width:250" styleClass="textbox" multiple="multiple" ondblclick="selectActType()">
											<html:options collection="actFeeList" property="feeId" labelProperty="description"/>
										</html:select>
									</td>
									<td width="2%" class="tabletext">
										<html:button style="width:25" property ="select" value=">" styleClass="button" onclick="selectActType()"></html:button><br>
										<html:button style="width:25" property ="deselect" value="<" styleClass="button" onclick="deselectActType()"></html:button>
									</td>
									<td class="tabletext" colspan="4">
										<html:select  property="selectedFeeId" size="25" style="width:250" styleClass="textbox" multiple="multiple" ondblclick="deselectActType()">
											<html:options  collection="actSubFeeMap"  property="feeId"  labelProperty="description" />
									    </html:select>
									</td>
	                            </tr>
	                              <!-- End of Displaying list Land Use with multibox  -->
	                          </table>
							</td>
						</tr>
	                </table>
	                </td>
	            </tr>
	        </table>
	    </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>


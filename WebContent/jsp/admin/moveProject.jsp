<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import='elms.common.Version'%>
<%@ page import="elms.util.db.*"%>
<app:checkLogon/>


<html:html>
<head>
<html:base/>
<title><%=Wrapper.getCityName()%> : <%=Wrapper.getPrivateLabel()%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script>
/*All the scripts should be here in one place and not scattered all over the file*/
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");
</script>
<% String contextRoot = request.getContextPath();%>

<script>

/*For Move Project Start*/

function validateProjAndLsoHandler(){
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
			    var resultset = result.split("@&@");
			    projId = resultset[0];
			    addrId = resultset[1];
			    if(projId == 0 && addrId==0){
			    alert("Project # and LSO # is not valid");
			    document.forms['moveProjectForm'].elements['pprojectno'].focus();
			    return false;
			    }else if(projId != 0 && addrId==0){
			    alert("LSO # is not valid");
			    document.forms['moveProjectForm'].elements['plsono'].focus();
			    return false;
			    }else if(projId == 0 && addrId!=0){
			    alert("Project # is not valid");
			    document.forms['moveProjectForm'].elements['pprojectno'].focus();
			    return false;
			    }else{
	    	    document.forms[0].action='<%=contextRoot%>/verifyMoveProject.do';
	    	    document.forms[0].submit();
    	    	return true;
			    }
		}
}
/*For Move Project End*/


/*For Move Sub Proj Start*/
function validateSProjAndProj(){
	
   var ssubprojectno= document.forms['moveProjectForm'].elements['ssubprojectno'].value ;
    
   var sprojectno = document.forms['moveProjectForm'].elements['sprojectno'].value;
	
	var url = '<%=request.getContextPath()%>/movePSAValidation.do?action=sProjAndProj&sProjectNbr='+ssubprojectno+'&projectNbr='+sprojectno+'';
	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = validateSProjAndProjHandler;
    xmlhttp.send(null);
}
function validateSProjAndProjHandler(){
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
			    var resultset = result.split("@&@");
			    projId = resultset[0];
			    subProjId = resultset[1];
			    if(projId == 0 && subProjId==0){
			    alert("Sub Project #  and Project # is not valid");
			    document.forms['moveProjectForm'].elements['ssubprojectno'].focus();
			    return false;
			    }else if(projId != 0 && subProjId==0){
			    alert("Sub Project # is not valid");
			    document.forms['moveProjectForm'].elements['ssubprojectno'].focus();
			    return false;
			    }else if(projId == 0 && subProjId!=0){
			    alert("Project # is not valid");
			    document.forms['moveProjectForm'].elements['sprojectno'].focus();
			    return false;
			    }else{
	    	    document.forms[0].action='<%=contextRoot%>/verifyMoveSubProject.do';
	    	    document.forms[0].submit();
    	    	return true;
			    }
		}
}
/*For Move Sub Proj End */

/*For Move Activity Start */
function validateActivityAndSProj(){
	
	   var aactivityno = document.forms['moveProjectForm'].elements['aactivityno'].value;
	    
	       var asubprojectno = document.forms['moveProjectForm'].elements['asubprojectno'].value ;
	
	var url = '<%=request.getContextPath()%>/movePSAValidation.do?action=activityAndSProj&activityNbr='+aactivityno+'&sProjectNbr='+asubprojectno+'';
	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = validateActivityAndSProjHandler;
    xmlhttp.send(null);
}
function validateActivityAndSProjHandler(){
    	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var result=xmlhttp.responseText;
			    var resultset = result.split("@&@");
			    actId = resultset[0];
			    subProjId = resultset[1];
			    if(actId == 0 && subProjId==0){
			    alert("Activity # and Sub Project # is not valid");
			    document.forms['moveProjectForm'].elements['aactivityno'].focus();
			    return false;
			    }else if(actId != 0 && subProjId==0){
			    alert("Sub Project # is not valid");
			    document.forms['moveProjectForm'].elements['asubprojectno'].focus();
			    return false;
			    }else if(actId == 0 && subProjId!=0){
			    alert("Activity # is not valid");
			    document.forms['moveProjectForm'].elements['aactivityno'].focus();
			    return false;
			    }else{
	    	    document.forms[0].action = '<%=contextRoot%>/verifyMoveActivity.do';
	    	    document.forms[0].submit();
    	    	return true;
			    }
		}
}
/*For Move Activity End */


</script>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
 <SCRIPT language="JavaScript">
 var strValue;
 function validateMoveProject()
 {
	 
 	//strValue=false;
     if (document.forms['moveProjectForm'].elements['pprojectno'].value == '')
     {
     	alert('Enter Project Number');
     	document.forms['moveProjectForm'].elements['pprojectno'].focus();
     	strValue=false;
     }
     else
     {
     	strValue=true;
     }
    if (strValue==true) {
	    if (document.forms['moveProjectForm'].elements['plsono'].value == '') {
	     	alert('Enter LSOID ');
	     	document.forms['moveProjectForm'].elements['plsono'].focus();
	     	strValue=false;
	    }
	 }
     if (strValue == true) {
    	 
     	validateProjAndLso();
     	return false;
     }else {
        return false;
     }
 }
 function validateProjAndLso(){
		
		var pprojectNo = document.forms['moveProjectForm'].elements['pprojectno'].value;
		var plsono = document.forms['moveProjectForm'].elements['plsono'].value;
		var url = '<%=request.getContextPath()%>/movePSAValidation.do?action=projAndLSO&projectNbr='+pprojectNo+'&lsoId='+plsono+'';
		
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = validateProjAndLsoHandler;
	    xmlhttp.send(null);
	}
  function validateMoveSubProject()
  {
  	//strValue=false;
      if (document.forms['moveProjectForm'].elements['ssubprojectno'].value == '')
      {
      	alert('Enter Sub Project Number');
      	document.forms['moveProjectForm'].elements['ssubprojectno'].focus();
      	strValue=false;
      }
      else
      {
      	strValue=true;
      }
     if (strValue==true) {
     if (document.forms['moveProjectForm'].elements['sprojectno'].value == '')
      {
      	alert('Enter Project Number ');
      	document.forms['moveProjectForm'].elements['sprojectno'].focus();
      	strValue=false;
      } }
      if ( strValue == true) {
           validateSProjAndProj();
           return false;
      }else {
           return false;
      }

  }
 
 function validateMoveActivity()
 {
 	//strValue=false;
     if (document.forms['moveProjectForm'].elements['aactivityno'].value == '')
     {
     	alert('Enter Activity Number');
     	document.forms['moveProjectForm'].elements['aactivityno'].focus();
     	strValue=false;
     }
     else
     {
     	strValue=true;
     }
    if (strValue==true) {
       if (document.forms['moveProjectForm'].elements['asubprojectno'].value == '')
       {
     	   alert('Enter Sub Project Number ');
     	   document.forms['moveProjectForm'].elements['asubprojectno'].focus();
     	   strValue=false;
       }
    }
    if ( strValue == true ) {
        validateActivityAndSProj();
        return false;
    }else {
        return false;
    }
}

 </SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form focus="pprojectno" name="moveProjectForm" type="elms.control.beans.MoveProjectForm" action="">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Move Project<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Move Project
                        </td>

                      <td width="1%" class="tablebutton"><nobr>
					  <html:submit property="verifyMoveProject"  value="Verify"  onclick= "return validateMoveProject()"   styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> Project
                        #</td>
                      <td class="tablelabel">&nbsp;
                        </td>
                      <td class="tablelabel"> LSO
                        #</td>
                    </tr>
                    <tr valign="top">
                      <td class="tabletext">
                        <html:text  name = "moveProjectForm"   property="pprojectno" size="25"  styleClass="textbox"/>
                      </td>
                      <td class="tabletext"> to </td>
                      <td class="tabletext">
						<html:text  name = "moveProjectForm"   property="plsono" size="25"  styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Move
                        Sub Project </td>

                      <td width="1%" class="tablebutton"><nobr>
					  <html:submit property="verifyMoveSubProject"  value="Verify"    onclick= "return validateMoveSubProject()" styleClass="button"/>

                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> Sub Project
                        #</td>
                      <td class="tablelabel">&nbsp;
                        </td>
                      <td class="tablelabel"> Project #</td>
                    </tr>
                    <tr valign="top">
                      <td class="tabletext">
						<html:text  name = "moveProjectForm"   property="ssubprojectno" size="25"  styleClass="textbox"/>
                      </td>
                      <td class="tabletext"> to </td>
                      <td class="tabletext">
                      <html:text  name = "moveProjectForm"   property="sprojectno" size="25"  styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Move Activity
                        </td>

                      <td width="1%" class="tablebutton"><nobr>
					  <html:submit property="verifyMoveActivity"  value="Verify"   onclick= "return validateMoveActivity()"  styleClass="button"/> &nbsp;</nobr>
					  </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> Activity
                        #</td>
                      <td class="tablelabel">&nbsp;
                        </td>
                      <td class="tablelabel"> Sub
                        Project #</td>
                    </tr>
                    <tr valign="top">
                      <td class="tabletext">
					  <html:text  name = "moveProjectForm"   property="aactivityno" size="25"  styleClass="textbox"/>
					  </td>
                      <td class="tabletext"> to </td>
                      <td class="tabletext">
					  <html:text  name = "moveProjectForm"   property="asubprojectno" size="25"  styleClass="textbox"/>
					  </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>

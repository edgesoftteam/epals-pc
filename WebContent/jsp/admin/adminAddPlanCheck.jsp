<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>



<app:checkLogon/>
<%
	//the context path
   	String contextRoot = request.getContextPath();
   	System.out.println("actname   " +request.getAttribute("actname"));
 	elms.control.beans.admin.AdminComboMappingForm adminComboMappingForm = (elms.control.beans.admin.AdminComboMappingForm)request.getAttribute("adminComboMappingForm");
  	String selectedActivityType[] = elms.util.StringUtils.stringtoArray((String)request.getParameter("strDesc"),"@");
	String selectedActivityCode[] = elms.util.StringUtils.stringtoArray((String)request.getParameter("strCode"),"@");

%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script language="javascript">
/// Start of Check version People History
var no_array = new Array();
function checkIsPcReq(val)
{

    var valNew=val.value;
    var pcReq = document.forms[0].pcReq.value;
    no_array=pcReq.split(",");

    if(val.checked == false )
    {

        document.forms[0].pcReq.value = removeFromArray(valNew,no_array).join(",");
         pcReq = removeFromArray(valNew,no_array).join(",");
        no_array=pcReq.split(",");
        valNew="";
    }

    if(valNew != "")
    {
     pcReq += valNew+",";

    }


    document.forms[0].pcReq.value = pcReq;


    return true;

}


function removeFromArray(val, ar){
s = String(ar)
// remove if not first item (global search)
reRemove = new RegExp(","+val,"g")
s = s.replace(reRemove,"")
// remove if at start of array
reRemove = new RegExp("^"+val+",")
s = s.replace(reRemove,"")
// remove if only item
reRemove = new RegExp("^"+val+"$")
s = s.replace(reRemove,"")
return new Array(s)
}

//This function is to get list of activities for which plan check is checked and submit to combo mapping page
function recordPlanCheck()
{
var pcReq = document.forms[0].pcReq.value;
window.close();
document.forms[0].action='<%=contextRoot%>/comboMappingAdmin.do?pcReq = '+ pcReq + '&pCheck=No';
document.forms[0].submit();

}
</script>

<script src="../script/yCode_combo_box.js"> </script>




<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form name="comboMappingForm" type="elms.control.beans.admin.AdminComboMappingForm" action="/comboMappingAdmin">
<html:hidden property="pcReq" />
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                     </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>


<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">

        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="25%" height="25" class="tabletitle">Map Plan Check Fees to Activities  </td>
                      <td width="1%" class="tablebutton"><html:button property="Save" value="Finish" styleClass="button" onclick="return recordPlanCheck();"></html:button></td>
                     </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>

                      <td width="25%" class="tablelabel">Activity Types</td>
                        <td width="8%" class="tablelabel">Plan Check Fees</td>
                    </tr>


				     <%
				     //This part contains Activity list followed by check boxes
				     //this variable is used to get the last position of character in the string
                      int lastIndx;
                      //pcReqs is used to store the list of activities code for which plan check is requested
                      String[] pcReqs=null;

				     //arrIndex is used to store the indexes list for which plan check is selected
				     String arrIndex="";
				     //arrIndex contains the array of indexes for which plan check is selected
		             String[] arrIndexs=null;

		             //getting the list of activities for which plan check is selected through session
		             String pcReq=(String)session.getAttribute("pcReq");

		            //initially it checks for plan check is selected for the activity list

		            if((!(pcReq.equals(""))) && (pcReq !=null))

				     {

				     //getting last index of string for ex:- String a= 1,2, ; in previous examples the "," is extra . to remove that we are getting last index of string
				     lastIndx= ((String)session.getAttribute("pcReq")).lastIndexOf(",");
				     //get the list of activities for which plan check is selected
				     pcReqs=elms.util.StringUtils.stringtoArray(( ((String)session.getAttribute("pcReq")).substring(0,lastIndx)),",");



		            //this block is used to map the list of activities with plan check with list of all the activities and get the indexes of mapped one
		            	for(int i=0;i<pcReqs.length;i++){

						   for(int j=0;j<selectedActivityCode.length;j++)
					   		{
								if(pcReqs[i].equalsIgnoreCase(selectedActivityCode[j])){
								//get list of indexes when mapped and store it in arrIndex with delimiter as ","
								arrIndex = arrIndex + j + ",";
								}

							}

						}

					//get last index of "," in the arrIndex string
					lastIndx= arrIndex.lastIndexOf(",");

					arrIndex = arrIndex.substring(0,lastIndx);

					//split the arrIndex into array and store it in arrIndexes
					arrIndexs =elms.util.StringUtils.stringtoArray(arrIndex,",");
		            }

		            for(int i=0;i<selectedActivityType.length;i++){
		          %>
		       			 <!-- Display of Activities list-->
		            	<tr><td class="tabletext"><%=selectedActivityType[i]%></select></td>
				            <%
					             if((!(pcReq.equals(""))) && (pcReq !=null))
							     {

				            %>


							<%
								// match index flag is set N when index is matched
								String matchIndexFlag="Y";
							    for(int j=0;j<arrIndexs.length;j++){
							    //Mapping the index with plan check selected with list of the activity indexes
		    					if(arrIndexs[j].equalsIgnoreCase(elms.util.StringUtils.i2s(i)))
	        						{

   							%>
			   						<td class="tabletext">
			   						<!-- The check box checked state display when mapped -->
			   						<input type="checkbox" name="isPcReq" value='<%=selectedActivityCode[i]%>' onclick="return checkIsPcReq(this)" checked></td>
   							<%

	   							matchIndexFlag="N";
	   				    	     break;

	   							}

	   							}

	   							for(int j=0;j<arrIndexs.length;j++){
	   							//The list of activities with check box unchecked when not mapped
	   							if(matchIndexFlag.equalsIgnoreCase("Y"))
	        					{

   							%>
   						<td class="tabletext">
	   						<input type="checkbox" name="isPcReq" value='<%=selectedActivityCode[i]%>' onclick="return checkIsPcReq(this)"></td>

	   						<%

		   						}
		   						break;
		   						}
	   						%>
   						 </tr>
						<%

						}
						//This block is used  initially when the when the new is to be inserted
						else
						{
						System.out.println("Hi Hello Finally came out of Loop");

						%>
						<td class="tabletext">
	   						<input type="checkbox" name="isPcReq" value='<%=selectedActivityCode[i]%>' onclick="return checkIsPcReq(this)"></td>

						<%


						}
					}
						%>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>


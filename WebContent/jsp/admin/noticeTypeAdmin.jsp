<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page import='java.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
List noticeTypes = (List)request.getAttribute("noticeTypes");
%>
<script language="javascript" type="text/javascript">
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");


function test(){
    document.getElementById('tableDiv').innerHTML = '';
	document.getElementById("tableDiv").style.display =  'none';
	document.getElementById('loader').innerHTML = '';
}

function checkTable(){
	document.forms['noticeTypeAdminForm'].elements['Lookup'].disabled = true;
	document.getElementById('loader').innerHTML = '<img src="../images/loader.gif" style="float:right;">';

	var url = "<%=contextRoot%>/noticeTypeAdmin.do?lookup=Yes";

	if(document.forms['noticeTypeAdminForm'].elements['description'].value != ''){
		url = url + "&noticeType=" + document.forms['noticeTypeAdminForm'].elements['description'].value;
	}

	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);
}

function checkTableHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
	    var result=xmlhttp.responseText;
		if(result == 'Notice Type Saved Successfully'){
			document.getElementById('saveDiv').innerHTML = result;
		}else{
			document.forms['noticeTypeAdminForm'].elements['Lookup'].disabled = false;
			document.getElementById('loader').innerHTML = '';
			if(result == 'No Records Found'){
				document.getElementById('saveDiv').innerHTML = result;
			}else{
				document.getElementById('tableDiv').innerHTML = result;
			}
		}
	    document.getElementById("tableDiv").style.display =  '';
 	}
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
save();
}

}

var strValue;
function save(){
	strValue=false;
	 if(document.forms['noticeTypeAdminForm'].elements['description'].value == ''){
		alert("Enter Notice Type");
		document.forms['noticeTypeAdminForm'].elements['description'].focus();
		return false;
	}else{
	  var url = '<%=contextRoot%>/noticeTypeAdmin.do?save=Yes';

		if(document.forms['noticeTypeAdminForm'].elements['noticeTypeId'].value != ''){
			url = url + "&noticeTypeId=" + document.forms['noticeTypeAdminForm'].elements['noticeTypeId'].value;
		}
		
		if(document.forms['noticeTypeAdminForm'].elements['description'].value != ''){
		url = url + "&description=" + document.forms['noticeTypeAdminForm'].elements['description'].value;
		}
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
	    xmlhttp.send(null);
	}
}

function refresh(){
     document.forms[0].action='<%=contextRoot%>/noticeTypeAdmin.do';
     document.forms[0].submit();
}

var no_array = new Array();
function checkIds(val){
	var valNew=val;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false ){
		document.forms[0].checkboxArray.value = removeFromArray(valNew.value,no_array).join(",");
     	checkboxArray = removeFromArray(valNew.value,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}
	if(valNew != ""){
	 checkboxArray += valNew.value+",";
	}
	document.forms[0].checkboxArray.value = checkboxArray;
	return true;
}

function removeFromArray(val, ar){
	s = String(ar)
	// remove if not first item (global search)
	reRemove = new RegExp(","+val,"g")
	s = s.replace(reRemove,"")
	// remove if at start of array
	reRemove = new RegExp("^"+val+",")
	s = s.replace(reRemove,"")
	// remove if only item
	reRemove = new RegExp("^"+val+"$")
	s = s.replace(reRemove,"")
	return new Array(s)
}

	function removeTable() {
     url = '<%=contextRoot%>/noticeTypeAdmin.do?delete=Yes';

	 url = url + "&checkboxArray=" + document.forms['noticeTypeAdminForm'].elements['checkboxArray'].value;

	
	 if(document.forms['noticeTypeAdminForm'].elements['description'].value != ''){
		url = url + "&description=" + document.forms['noticeTypeAdminForm'].elements['description'].value;
	 }

	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);

}

//Start Ajax functionality for Notice Type
	var xmlhttp;
	function CreateXmlHttp(str, editFlag){

	//Assigning Activity Id to the hidden variable
	document.forms['noticeTypeAdminForm'].elements['noticeTypeId'].value = str;
	document.forms['noticeTypeAdminForm'].elements['editFlg'].value = editFlag;

	//Creating object of XMLHTTP in IE
		var url="<%=contextRoot%>/noticeTypeAdmin.do?id="+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		  }else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		 if(xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_NoticeType;
			xmlhttp.send(null);
		  }else{
		  alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_NoticeType(){
	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){
		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){
		    var text=xmlhttp.responseText;
			text=text.split(',');
			document.forms['noticeTypeAdminForm'].elements['description'].value = text[0];


			if(document.forms['noticeTypeAdminForm'].elements['description'].value == ""){
				document.forms['noticeTypeAdminForm'].elements['description'].value = "";
			}
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}
//End Ajax functionality for Activity Type
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="test()">
<html:form focus="description" name="noticeTypeAdminForm" type="elms.control.beans.admin.NoticeTypeAdminForm" action="/noticeTypeAdmin">
<html:hidden property="noticeTypeId"/>
<html:hidden property="checkboxArray" value=""/>
<html:hidden property="editFlg" value=""/>

<div id="saveDiv" align="justify" style="color:#088A08"></div>

<logic:notEqual value="YES" property="displayNoticeTypes" name="noticeTypeAdminForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="36px"><font class="con_hdr_3b">Notice Type Administration</font></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			 <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="Reset" value="Reset" styleClass="button" onclick="refresh();"/>
                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return checkTable();"></html:button>
                          <html:button property="Save" value="Save" styleClass="button" onclick="return save();"></html:button>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                    
                      <td class="tablelabel">Notice Type</td>
                      <td class="tabletext">
                        <html:text property="description" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
                </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:notEqual>
<div id="loader" style="padding-right:50%;"><span><img src="../images/loader.gif"></span></div>
<div id="tableDiv"></div>

</html:form>
</body>
</html:html>

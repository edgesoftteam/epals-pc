<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ page import="elms.control.beans.admin.AttachmentTypeForm"%>
<%@ page import="java.util.List"%>
<%@ page import="elms.app.common.DisplayItem"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="org.json.JSONArray"%>
<%@ page import="org.json.JSONObject"%>

<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>epals</title>  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script src="../script/yCodelib.js"> </script> <script>checkBrowser();</script> <script src="../script/yCode_combo_box.js"> </script>

<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";

AttachmentTypeForm attachmentTypeForm = null;
attachmentTypeForm = (AttachmentTypeForm) session.getAttribute("attachmentTypeForm");

if(attachmentTypeForm.getOnlineFlag()!=null && attachmentTypeForm.getOnlineFlag().equalsIgnoreCase("Y")){
	System.out.println("#$#$#$#$#$#$ inside if " +attachmentTypeForm.getOnlineFlag());
	attachmentTypeForm.setOnlineFlag("on");
}



 String activityTypes = (String) request.getAttribute("activityTypes"); 
System.out.println("activityTypes = "+activityTypes);
List<DisplayItem> activityTypesList = new ArrayList<DisplayItem>();
if(activityTypes != null){
	//System.out.println("activityTypes not null");
	JSONObject obj = new JSONObject(activityTypes);
	
	JSONArray activityTypesArray = obj.getJSONArray("activityTypesData");
	//System.out.println("activityTypesArray = "+activityTypesArray);
	for(int i = 0 ; i < activityTypesArray.length() ; i++){
		DisplayItem di = new DisplayItem();
		/* System.out.println("typeid = "+activityTypesArray.getJSONObject(i).getString("typeId"));
		System.out.println("description = "+activityTypesArray.getJSONObject(i).getString("description")); */
		di.setFieldOne(String.valueOf(activityTypesArray.getJSONObject(i).getInt("typeId")));
		di.setFieldTwo(activityTypesArray.getJSONObject(i).getString("description"));
		activityTypesList.add(di);
	}
} 


String attachmentTypes = (String) request.getAttribute("attachmentTypes"); 

List<DisplayItem> attachmentTypesList = new ArrayList<DisplayItem>();
if(attachmentTypes != null){	
	JSONObject obj = new JSONObject(attachmentTypes);	
	JSONArray activityTypesArray = obj.getJSONArray("attachmentTypesData");	
	for(int i = 0 ; i < activityTypesArray.length() ; i++){
		DisplayItem di = new DisplayItem();		
		di.setFieldOne(String.valueOf(activityTypesArray.getJSONObject(i).getInt("typeId")));
		di.setFieldTwo(activityTypesArray.getJSONObject(i).getString("description"));
		attachmentTypesList.add(di);
	}
} 

String flag = (String) request.getAttribute("update");
/* List<DisplayItem> attachmentTypes = (List<DisplayItem>) request.getAttribute("attachmentTypes"); 
System.out.println("attachmentTypes = "+attachmentTypes); */

//List departmentsList = attachmentTypeForm.getDepartmentsList();
%>
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
<script language="JavaScript">

function validateFunction() {
	
	//var strValue=false;
	
	if (document.forms['attachmentTypeForm'].elements['moduleId'].value == '-1') {
		alert("Please select a module");
		document.forms['attachmentTypeForm'].elements['moduleId'].focus();
		//strValue=false;
	}else  if (document.forms['attachmentTypeForm'].elements['activityType'].value == '-1') {
		alert("Please enter activity type");
		document.forms['attachmentTypeForm'].elements['activityType'].focus();
		//strValue=false;
	}else  if (document.forms['attachmentTypeForm'].elements['attachmentTypeDesc'].value == ' ') {		
		
		if($("#otherCheck").prop('checked') == true){
			if(document.forms['attachmentTypeForm'].elements['attachmentDesc'].value == ''){
				alert("Please enter attachment description");
				document.forms['attachmentTypeForm'].elements['attachmentDesc'].focus();
			}
		}else{
			alert("Please enter attachment type");
			document.forms['attachmentTypeForm'].elements['attachmentType'].focus();
		}	
		//document.forms['attachmentTypeForm'].elements['attachmentType'].focus();
		//strValue=false;
	}else{
	   	document.forms[0].action='<%=contextRoot%>/attachmentTypeAdmin.do?action=save';
		document.forms[0].submit();
    }      
}
  
function lookup() {
       document.forms[0].action='<%=contextRoot%>/attachmentTypeAdmin.do?action=lookup';
       document.forms[0].submit();     	
}
function refresh() {
     document.forms[0].action='<%=contextRoot%>/attachmentTypeAdmin.do';
     document.forms[0].submit();
} 
function doDelete() {
     document.forms[0].action='<%=contextRoot%>/attachmentTypeAdmin.do?action=delete';
     document.forms[0].submit();
}

function getActivityTypes(element)	{			
	
	var moduleIdVal = element.value;
	
		$.ajax({
            type: "GET",
            url:"<%=contextRoot%>/attachmentTypeAdmin.do?action=actTypes",
            dataType: "json",
            cache: false,
            data: {moduleId:moduleIdVal},
            success: function (data) {	            	
            	
            	$('#activityTypes option:not(:first)').remove();	
            	
	            if(data.activityTypesData != ''){           	
	            
	                $.each(data.activityTypesData,function(i,obj)
	                {
	                 
	                 var div_data="<option value="+obj.typeId+">"+obj.description+"</option>";
	                
	                $(div_data).appendTo('#activityTypes'); 
	                }); 
	            }else{     	
	            	 
	            	$('#activityTypes option:not(:first)').remove();	            	
	            }
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback 				
				alert('Error: ' + errorMessage);
			} 
          });
	}	
	


function getAttachmentTypes(element)	{			
	
	var actTypeId = element.value;
		
		$.ajax({
            type: "GET",
            url:"<%=contextRoot%>/attachmentTypeAdmin.do?action=attachmentTypes",
            dataType: "json",
            cache: false,
            data: {actTypeId:actTypeId},
            success: function (data) { 
            	
            	$('#attachmentTypes option:not(:first)').remove();
            	
	            if(data.attachmentTypesData != ''){           	
	            
	                $.each(data.attachmentTypesData,function(i,obj)
	                {
	                	
	                 var div_data="<option value="+obj.typeId+">"+obj.description+"</option>";
	                
	                $(div_data).appendTo('#attachmentTypes'); 
	                $('#attachmentTypes').show();
	            	$('#other').show();
	            	$('#attachmentDesc').hide();
	            	
	                }); 
	            }else{     	
	            	
	            	$('#attachmentTypes option:not(:first)').remove();	     
	            	$('#attachmentTypes').hide();
	            	$('#other').hide();
	            	$('#attachmentDesc').show();
	            }
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback 				
				alert('Error: ' + errorMessage);
			} 
          });
	}	

$( document ).ready(function() {
	$('#attachmentDesc').hide();
	
	$('#otherCheck').click(function(){
        if($(this).prop("checked") == true){
        	$('#attachmentDesc').show();
        	$('#attachmentTypes').prop('disabled',true)
        }
        else if($(this).prop("checked") == false){
        	$('#attachmentDesc').hide();
        	$('#attachmentTypes').prop('disabled',false)
        }
    });
	
	<% if(flag != null && flag.equals("Y")){%>
		$('#other').hide();
		$('#activityTypes').prop('disabled',true);
		$('#moduleRow').hide();		
		document.forms['attachmentTypeForm'].elements['moduleId'].value = " ";
	<%}%>
});




</script>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form name="attachmentTypeForm" type="elms.control.beans.admin.AttachmentTypeForm" action="/attachmentTypeAdmin">
<html:hidden property="attachmentTypeId"/> 
<logic:notEqual value="YES" property="displayAttachmentType" name="attachmentTypeForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Attachment Type Admin Maintenance<br>
                <br>
                </font></td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <html:errors property="error"/>
		                    <tr>
		                      <td width="99%" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_2b">Lookup/Add/Update</font></td>
		                      <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
		                        <td width="1%" background="../images/site_bg_D7DCE2.jpg"><nobr>
		                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
		                          <html:reset value="Reset" styleClass="button" onclick="return refresh()"/>
		                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="lookup()"></html:button>
		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">
							<%-- <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Email Template Type</nobr></font></td>
		                        <td bgcolor="#FFFFFF"><font class="con_text_1"> <html:text size="20" property="emailTemplateType" styleClass="textbox"/> </font>&nbsp;</td>
		                    </tr> --%>
		                    <tr id="moduleRow">
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Department/Module<span style="color:red;">*</span></nobr></font></td>		                        
		                    	<td bgcolor="#FFFFFF">
			                         <html:select property="moduleId" styleClass="textbox" onchange="getActivityTypes(this)">
			                        	  <html:option value="-1">Please Select</html:option>
			                              <html:options collection="modules" property="id" labelProperty="description" />
			                        </html:select>
			                    </td>
		                    </tr>
		                   <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Activity Type<span style="color:red;">*</span></nobr></font></td>
		                        <td bgcolor="#FFFFFF">
			                        <html:select property="activityType" styleClass="textbox" styleId="activityTypes" onchange="getAttachmentTypes(this)">
		                        	<html:option value="-1">Please Select</html:option>
									<%-- <html:options collection="activityTypes" property="type" labelProperty="description" /> --%>
									 <%if(activityTypesList != null && activityTypesList.size() >0){ 
											for(int i=0;i<activityTypesList.size();i++){
									 %>
										<html:option  value="<%=((DisplayItem)(activityTypesList.get(i))).getFieldOne()%>"><%=((DisplayItem)(activityTypesList.get(i))).getFieldTwo()%></html:option>
									<%}} %>   
		                        </html:select>
		                       
			                    </td>			                   	
		                    </tr>
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Attachment Type<span style="color:red;">*</span></nobr></font></td>
		                        <td bgcolor="#FFFFFF">		                        	
		                        	<html:select property="attachmentTypeDesc" styleId="attachmentTypes" styleClass="textbox"> 
			                        	  <html:option value=" ">Please Select</html:option>	
			                         <%if(attachmentTypesList != null && attachmentTypesList.size() >0){ 
											for(int i=0;i<attachmentTypesList.size();i++){
									 %>
										<html:option  value="<%=((DisplayItem)(attachmentTypesList.get(i))).getFieldOne()%>"><%=((DisplayItem)(attachmentTypesList.get(i))).getFieldTwo()%></html:option>
									<%}} %>	                            
			                        </html:select>
		                        
		                    		<span id="other"><font class="con_hdr_1">Other&nbsp;&nbsp;</font><html:checkbox property="other" styleId="otherCheck"></html:checkbox></span>
		                        	<span id="attachmentDesc"><html:text size="20" property="attachmentDesc" styleClass="textbox"/></span>
		                    	</td>
		                    </tr> 
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Document URL</nobr></font></td>
		                        <td bgcolor="#FFFFFF"><font class="con_text_1"> <html:text size="20" property="documentUrl" styleClass="textbox"/> </font>&nbsp;</td>
		                    </tr>
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>BMC Description</nobr></font></td>
		                        <td bgcolor="#FFFFFF"><font class="con_text_1"> <html:text size="20" property="bmcPopupWindowUrl" styleClass="textbox"/> </font>&nbsp;</td>
		                    </tr>
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>BMC PopUp Window URL</nobr></font></td>		                        
		                    	<td bgcolor="#FFFFFF"><font class="con_text_1"> <html:text size="20" property="bmcDescription" styleClass="textbox"/> </font>&nbsp;</td>
		                    </tr>		                    
		                    <tr>
		                    	<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Online Flag</font></td>
								<td bgcolor="#FFFFFF"><html:checkbox property="onlineFlag" ></html:checkbox></td>								
							</tr>									                    
		                     <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Upload or Download Type</nobr></font></td>		                        
		                     	<td bgcolor="#FFFFFF">
                                   <html:select property="uploadOrDownloadType" styleClass="textbox">
                        	          <html:option value="">Please Select</html:option> 
                        	          <html:option value="Download">Download</html:option> 
                        	          <html:option value="Upload">Upload</html:option> 
                                      <html:option value="Both Upload and Download">Both Upload and Download</html:option> 
                                      <html:option value="None">None</html:option> 
                                   </html:select>
                                </td>
		                    </tr>
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Level</nobr></font></td>		                        
		                     	<td bgcolor="#FFFFFF">
                                   <html:select property="level" styleClass="textbox" multiple="multiple">
                        	          <%-- <html:option value="-1">Please Select</html:option>  --%>
                        	          <html:option value="L">Land</html:option> 
                        	          <html:option value="S">Structure</html:option> 
                                      <html:option value="O">Occupancy</html:option> 
                                      <html:option value="P">Project</html:option>
                                      <html:option value="Q">Sub Project</html:option> 
                                   </html:select>
                                </td>
		                    </tr>
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Department</font></td>
                                <td bgcolor="#FFFFFF">
                                   <html:select property="department" styleClass="textbox" multiple="multiple">
                                   <html:option value="-1">Please Select</html:option> 
                                   <html:options collection="departments" property="departmentId" labelProperty="description"/></html:select>                                   
                                   
                                </td>
		                     </tr>
		                    <tr>
								<td background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1"><nobr>Comments</nobr></font></td>
		                        <td bgcolor="#FFFFFF"><font class="con_text_1"> <html:textarea rows="4" cols="50" property="comments" styleClass="textbox"/> </font>&nbsp;</td>
		                    </tr>
                       	 </table>
                    	</td>
                  </tr>
                </table>
               </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</logic:notEqual>

<logic:equal value="YES" property="displayAttachmentType" name="attachmentTypeForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Attachment Type List<br>
            <br>
            </font></td>
        </tr>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                 <html:errors property="error"/>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="95%" height="25" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_2b">Attachment Type Library</font></td>
                      <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../images/site_bg_D7DCE2.jpg"><nobr> 
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                       <html:button value="Delete" property="delete" styleClass="button" onclick="doDelete()"/>
                       <html:button value="Add" property="add" styleClass="button" onclick="refresh()"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td background="../images/site_bg_B7C1CB.jpg"> 
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr> 
                      <td width="6%" background="../images/site_bg_D7DCE2.jpg" style="text-align:center;"><font class="con_hdr_1">Delete</font></td>                                         
                      <td width="25%" background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Attachment Type</font></td>    
                      <td width="25%" background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Activity Type</font></td>                   
                      <td width="20%" background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">BMC Description</font></td> 
                      <td width="5%" background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Upload or Download Type</font></td>  
                      <td width="34%" background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Departments</font></td> 
                      <td width="10%" background="../images/site_bg_D7DCE2.jpg"><font class="con_hdr_1">Levels</font></td>                     
                    </tr>
                    <logic:iterate id="attachmentTypeAdmin" property="attachmentTypeList" name="attachmentTypeForm">
                      <tr bgcolor="#FFFFFF">
                         <td align="center"><html:multibox property="selectedItems"><bean:write name="attachmentTypeAdmin" property="refId"/></html:multibox></td>                          
                         <td bgcolor="#FFFFFF" >
                          <font class="con_text_1">
                          	<a href="<%=contextRoot%>/attachmentTypeAdmin.do?id=<bean:write name="attachmentTypeAdmin" property="refId"/>&department=<bean:write name="attachmentTypeAdmin" property="departmentIds"/>&level=<bean:write name="attachmentTypeAdmin" property="levelIds" />" >
                               <bean:write name="attachmentTypeAdmin" property="attachmentTypeDesc" />
                            </a>
                           </font>
                         </td>
                        <td bgcolor="#FFFFFF"><font class="con_text_1"><bean:write name="attachmentTypeAdmin" property="actTypeDesc" /></font></td>
                        <td bgcolor="#FFFFFF"><font class="con_text_1"><bean:write name="attachmentTypeAdmin" property="bmcdescription" /></font></td>                       
                        <td bgcolor="#FFFFFF"><font class="con_text_1"><bean:write name="attachmentTypeAdmin" property="uploadOrDownloadType" /></font></td>                     	
                        <td bgcolor="#FFFFFF"><font class="con_text_1"><bean:write name="attachmentTypeAdmin" property="department" /></font></td>                       
                        <td bgcolor="#FFFFFF"><font class="con_text_1"><bean:write name="attachmentTypeAdmin" property="level" /></font></td>
                      </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="32">&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:equal>

</html:form>
</body>
</html:html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*,java.util.*'%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<%
	String contextRoot = request.getContextPath();
	
%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="javascript">
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");


function test(){
    document.getElementById('tableDiv').innerHTML = '';
	document.getElementById("tableDiv").style.display =  'none';
	document.getElementById('loader').innerHTML = '';
}

function checkTable(){
	document.forms['addressTypeAdminForm'].elements['Lookup'].disabled = true;
	document.getElementById('loader').innerHTML = '<img src="../images/loader.gif" style="float:right;">';

	var url = "<%=contextRoot%>/addressTypeAdmin.do?lookup=Yes";

	if(document.forms['addressTypeAdminForm'].elements['id'].value != ''){
		url = url + "&id=" + document.forms['addressTypeAdminForm'].elements['id'].value;
	}
	if(document.forms['addressTypeAdminForm'].elements['addressType'].value != ''){
		url = url + "&addressType=" + document.forms['addressTypeAdminForm'].elements['addressType'].value;
	}
	
	if(document.forms['addressTypeAdminForm'].elements['btFlag'].checked == true){
		url = url + "&btFlag=Y" ;
	}else{
		url = url + "&btFlag=N" ;
	}
	if(document.forms['addressTypeAdminForm'].elements['blFlag'].checked == true){
		url = url + "&blFlag=Y";
	}else{
		url = url + "&blFlag=N";
	}
	
	
	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);
}

function checkTableHandler(){
	//alert("save");
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var statusResult=xmlhttp.responseText;
			    if(statusResult == 'Activity Status Saved Successfully'){
					document.getElementById('saveDiv').innerHTML = statusResult;
				}else if(statusResult == 'Address Type Already Avilable'){
					document.getElementById('saveDiv').innerHTML = statusResult;
				}else{
					document.forms['addressTypeAdminForm'].elements['Lookup'].disabled = false;
					document.getElementById('loader').innerHTML = '';
					if(statusResult == 'No Records Found'){
						document.getElementById('saveDiv').innerHTML = statusResult;
					}else{
						document.getElementById('tableDiv').innerHTML = statusResult;
					}

				}
			    document.getElementById("tableDiv").style.display =  '';
 	}
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
	checkTable();
	event.preventDefault ? event.preventDefault() : (event.returnValue = false);
//validateFunction();
}

}

</script>
<script language="JavaScript">
var strValue;
function validateFunction() {
		strValue=false;
	 	if (document.forms['addressTypeAdminForm'].elements['addressType'].value == '') {
			alert("Enter Address Type");
			document.forms['addressTypeAdminForm'].elements['addressType'].focus();
			strValue=false;
			event.preventDefault ? event.preventDefault() : (event.returnValue = false);
			return false;
		}else{ 
			var url = '<%=contextRoot%>/addressTypeAdmin.do?save=Yes';

			if(document.forms['addressTypeAdminForm'].elements['id'].value != ''){
				url = url + "&id=" + document.forms['addressTypeAdminForm'].elements['id'].value;
			}
			if(document.forms['addressTypeAdminForm'].elements['addressType'].value != ''){
				url = url + "&addressType=" + document.forms['addressTypeAdminForm'].elements['addressType'].value;
			}
			
			if(document.forms['addressTypeAdminForm'].elements['btFlag'].checked == true){
				url = url + "&btFlag=Y" ;
			}else{
				url = url + "&btFlag=N" ;
			}
			if(document.forms['addressTypeAdminForm'].elements['blFlag'].checked == true){
				url = url + "&blFlag=Y";
			}else{
				url = url + "&blFlag=N";
			}

			//alert(url);
			xmlhttp.open('POST', url,true);
			/* alert("postafter"); */
			xmlhttp.onreadystatechange = checkTableHandler;
			xmlhttp.onreadystatechange = checkTableHandler;
		    xmlhttp.send(null);
		}
	}

function refresh(){
     document.forms['addressTypeAdminForm'].action='<%=contextRoot%>/addressTypeAdmin.do';
     document.forms['addressTypeAdminForm'].submit();
}

var no_array = new Array();
function checkIds(val){
	var valNew=val;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false ){
		document.forms[0].checkboxArray.value = removeFromArray(valNew.value,no_array).join(",");
     	checkboxArray = removeFromArray(valNew.value,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}
	if(valNew != ""){
	 checkboxArray += valNew.value+",";
	}
	document.forms[0].checkboxArray.value = checkboxArray;
	return true;

}

function removeFromArray(val, ar){
	s = String(ar)
	// remove if not first item (global search)
	reRemove = new RegExp(","+val,"g")
	s = s.replace(reRemove,"")
	// remove if at start of array
	reRemove = new RegExp("^"+val+",")
	s = s.replace(reRemove,"")
	// remove if only item
	reRemove = new RegExp("^"+val+"$")
	s = s.replace(reRemove,"")
	return new Array(s)
}

function removeTable(statusId) {

		url = '<%=contextRoot%>/addressTypeAdmin.do?delete=Yes';

		url = url + "&checkboxArray=" + document.forms['addressTypeAdminForm'].elements['checkboxArray'].value;


		if(document.forms['addressTypeAdminForm'].elements['id'].value != ''){
			url = url + "&id=" + document.forms['addressTypeAdminForm'].elements['id'].value;
		}
		if(document.forms['addressTypeAdminForm'].elements['addressType'].value != ''){
			url = url + "&addressType=" + document.forms['addressTypeAdminForm'].elements['addressType'].value;
		}
		
		if(document.forms['addressTypeAdminForm'].elements['btFlag'].checked == true){
			url = url + "&btFlag=Y" ;
		}else{
			url = url + "&btFlag=N" ;
		}
		if(document.forms['addressTypeAdminForm'].elements['blFlag'].checked == true){
			url = url + "&blFlag=Y";
		}else{
			url = url + "&blFlag=N";
		}
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
    	xmlhttp.send(null);
	}
</script>
<SCRIPT language="javascript" type="text/javascript">
//Start of Ajax functionality for Activity Status populating values to the fields

var xmlhttp;

	function CreateXmlHttp(str){

	//Assigning Status Id to the hidden variable
	document.forms['addressTypeAdminForm'].elements['id'].value = str;
	//document.forms['activityStatusAdminForm'].elements['editFlg'].value = editFlag;

	//Creating object of XMLHTTP in IE
		var url="<%=contextRoot%>/addressTypeAdmin.do?typeId="+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		  }else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		if (xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ActivityStatus;
			xmlhttp.send(null);
		  }else{
		  alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ActivityStatus(){
	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){
		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){
		    var text=xmlhttp.responseText;
			text=text.split(',');
			document.forms['addressTypeAdminForm'].elements['addressType'].value = text[0];

			/* if(document.forms['activityStatusAdminForm'].elements['editFlg'].value == 'N'){
				document.forms['activityStatusAdminForm'].elements['statusCode'].disabled=true;
			}else{
				document.forms['activityStatusAdminForm'].elements['statusCode'].disabled=false;
			}

			document.forms['activityStatusAdminForm'].elements['descriptionType'].value = text[1];
 */
			if(text[1] == 'true'){
				document.forms['addressTypeAdminForm'].elements['btFlag'].checked =true;
			}else{
				document.forms['addressTypeAdminForm'].elements['btFlag'].checked =false;
			}

			if(text[2] == 'true'){
				document.forms['addressTypeAdminForm'].elements['blFlag'].checked =true;
			}else{
				document.forms['addressTypeAdminForm'].elements['blFlag'].checked =false;
			}

			/* if(document.forms['activityStatusAdminForm'].elements['statusCode'].value == ""){
				document.forms['activityStatusAdminForm'].elements['descriptionType'].value = "";
				document.forms['activityStatusAdminForm'].elements['active'].checked = false;
				document.forms['activityStatusAdminForm'].elements['psaActive'].checked = false;
				document.forms['activityStatusAdminForm'].elements['projectNameId'].value = "";
				document.forms['activityStatusAdminForm'].elements['moduleId'].value = "";
			} */
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}
//End Ajax functionality for Activity Status populating values to the fields
</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="test();">
<html:form  action="" name="addressTypeAdminForm" type="elms.control.beans.admin.AddressTypeAdminForm" >
<html:hidden property="id"/>
<html:hidden property="checkboxArray" value=""/>
<html:hidden property="editFlg" value=""/>

<div id="saveDiv" align="justify" style="color:#088A08"></div>

<!-- Start of logic tag not to display /iterate values   -->

<logic:notEqual value="YES" property="displayAddressType" name="addressTypeAdminForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="40%" height="36px"><font class="con_hdr_3b">Address Type Administration</font></td>
        </tr>
         <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <html:errors/>
                    <tr>
                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="Reset" value="Reset" styleClass="button" onclick="refresh();"/>
                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return checkTable();" />
						  <html:button property="Save/Update" value="Save/Update" styleClass="button" onclick="return validateFunction();"/>                          
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" width="320">Address Type</td>
                      <td class="tabletext" width="168" colspan="3">
                        <html:text property="addressType" size="50" maxlength="25" styleClass="textbox"/>
                      </td>
                      
                    </tr>
                    <tr>
						<td class="tablelabel" width="20%">BT Active</td>
						<td class="tabletext">
					    	<html:checkbox  property="btFlag" styleClass="textbox">Yes</html:checkbox>
						</td>
						<td class="tablelabel" width="20%">BL Active</td>
						<td class="tabletext">
					    	<html:checkbox  property="blFlag"  styleClass="textbox">Yes</html:checkbox>
						</td>
					</tr>
					
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:notEqual>
<!-- end of logic tag -->

<div id="loader" style="padding-right:50%;"><span><img src="../images/loader.gif"></span></div>
<div id="tableDiv"></div>

</html:form>
</body>
</html:html>

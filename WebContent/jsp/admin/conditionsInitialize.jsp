<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*,java.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%!
List moduleList = new ArrayList();

 %>
<%
	String contextRoot = request.getContextPath();
	boolean act=false;
	if(request.getQueryString()!=null) {act=true;}

	try{
	//Function to get list of modules
	moduleList =  CommonAgent.getModuleList();
	pageContext.setAttribute("moduleList", moduleList);
	}
	catch(Exception e){}
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">

		var strValue;

	function removeFromArray(val, ar){
			s = String(ar)
			// remove if not first item (global search)
			reRemove = new RegExp(","+val,"g")
			s = s.replace(reRemove,"")
			// remove if at start of array
			reRemove = new RegExp("^"+val+",")
			s = s.replace(reRemove,"")
			// remove if only item
			reRemove = new RegExp("^"+val+"$")
			s = s.replace(reRemove,"")
			return new Array(s)
		}

	//Function to get different module id's seperated  by a comma
	function getModuleIds(val)
	  {
       var dummy = document.forms['conditionsInitializeForm'].moduleIdString.value;
		dummy += val.value + ",";
		var no_array = new Array();

         no_array=dummy.split(",");
		 if(val.checked == false)
			{
        		document.forms[0].moduleIdString.value = removeFromArray(val.value,no_array).join(",");
		     	dummy = removeFromArray(val.value,no_array).join(",");
			    no_array=dummy.split(",");
				val.value="";
			}

        document.forms['conditionsInitializeForm'].moduleIdString.value = dummy;

	  }

	function validateFunction()
	{

		strValue=validateData('req',document.forms['conditionsInitializeForm'].elements['conditionDate'],'Condition Date is a required field');
		if (strValue == true)
		{
			if (document.forms['conditionsInitializeForm'].elements['conditionDate'].value != '')
	    	{
	    		strValue=validateData('date',document.forms['conditionsInitializeForm'].elements['conditionDate'],'Invalid date format');
	    	}
			if(document.forms['conditionsInitializeForm'].moduleIdString.value=='')
			{
				alert('Please select any department');
				return false;
			}
	    }
	return strValue;
	}


</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/conditionsInitialize"  onsubmit="return validateFunction();">
<html:hidden property="moduleIdString" />
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Conditions Maintenance</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle">Initialize<nobr></nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">

                    <tr class="tabletext">
                      <td colspan="2"><font class="red3">Proceed With Caution</font></td>
                    </tr>
                    <tr class="tabletext">
                      <td colspan="2"><font class="red2">Initialization process
                        will expire all current conditions at the condition Date and create
                        the new set of current conditions!</font>
					  </td>
                    </tr>

                    <tr>
                      <td class="tablelabel" width="1%"><nobr>Condition
                        Date </nobr></td>

                      <td class="tabletext" width="99%"><nobr>&nbsp;


                       <html:text  property="conditionDate" styleClass="textbox" onkeypress="return validateDate();"/>
					    <html:link href="javascript:show_calendar('conditionsInitializeForm.conditionDate');"
                       		     onmouseover="window.status='Calendar';return true;"
                        		 onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
						</html:link>
                        </nobr>
                       <html:submit  property="Submit" value="Save" styleClass="button"/>

                      </td>
                    </tr>

                    <tr class="tabletext"><TD colspan="2"><font class="red3"><bean:write name="conditionsInitializeForm" property="strMessage" /></font></TD></TR>

				 </table>
                </td>
              </tr>
				<html:errors/>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

      </table>
    </td>

				<tr>
					<td>
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<%
						for(int i=0;i<moduleList.size();i++)
						{
						elms.app.common.Module module = (elms.app.common.Module)moduleList.get(i);
					%>

					<tr>
						<td class="tablelabel" width="20%"><%=module.getDescription()%></td>
						<td class="tabletext">
					    	<html:multibox  name="conditionsInitializeForm" property="modules" styleClass="textbox" onclick ="getModuleIds(this)"><%=module.getId()%></html:multibox>
						</td>

					</tr>
				<% }%>

					</table>
				</td>


    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

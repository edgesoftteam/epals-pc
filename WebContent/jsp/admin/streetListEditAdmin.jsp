<%@page import="elms.agent.*,elms.control.beans.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<jsp:useBean id="useAgent" scope="request" class="elms.agent.AddressAgent"/>
<jsp:useBean id="zoneAgent" scope="request" class="elms.agent.AddressAgent"/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

	<%

	String contextRoot = request.getContextPath();
	String streetid = (String)request.getAttribute("id");

 	%>
<SCRIPT language="JavaScript" src="../script/util.js"></SCRIPT>
	<SCRIPT language="JavaScript">

	var strValue;
	function validateFunction() {
		if (document.forms['streetListAdminForm'].elements['streetName'].value == '') {
			alert("Enter Street Name");
			document.forms['streetListAdminForm'].elements['streetName'].focus();
			strValue=false;
		}else{
		   	document.forms[0].action='<%=contextRoot%>/streetListAdmin.do?update=Yes&id=<%=streetid%>';
			document.forms[0].submit();
	    }
	}

	function refresh(){
    		document.forms[0].action='<%=contextRoot%>/streetListAdmin.do';
	   		document.forms[0].submit();
		}

	</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="" name="streetListAdminForm" type="elms.control.beans.admin.StreetListAdminForm">
<html:hidden property="streetListId"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr>
                <td>
	 <html:errors/>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					 <tr>
          <td><font class="con_hdr_3b">Add Street</font><br>
            <br>
            </td>
        </tr>


                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td width="99%" class="tabletitle">Street List</td>
								<td width="" class="tablebutton"><nobr><html:button value="Save" property="save" styleClass="button" onclick="return validateFunction();"/>
								<html:button property ="cancel" value="Cancel" styleClass="button" onclick="refresh();"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>

                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Prefix Dir</td>
                                <td class="tabletext" valign="top"><html:text property="streetPrefix" size="15" maxlength="2" styleClass="textbox" onkeypress="return validateAlphaNumeric();"/></td>
                                <td class="tablelabel" valign="top">Street Name</td>
								<td class="tabletext" valign="top"><html:text property="streetName" size="15" maxlength="20"  styleClass="textbox" onkeypress="return validateAlphaNumeric();" /></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Street Type</td>
                                <td class="tabletext" valign="top"><html:text property="streetType" maxlength="10" size="15" styleClass="textbox" onkeypress="return validateAlphaNumeric();"/></td>
                                <td class="tablelabel" valign="top">Suffix Dir</td>
                                <td class="tabletext" valign="top"><html:text property="streetSuffix" size="15" maxlength="2" styleClass="textbox" onkeypress="return validateAlphaNumeric();"/></td>

                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
</table>
</html:form>
</body>
</html:html>


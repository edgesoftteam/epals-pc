<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.admin.*" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<%
	String contextRoot = request.getContextPath();

%>
<script>
function validateFunction() {
	if (document.forms['lookupFeesForm'].elements['busTaxMin'].value == 0) {
		alert("Enter ContractTax Minimum Value");
		document.forms['lookupFeesForm'].elements['busTaxMin'].focus();
		return false;
	}else if (document.forms['lookupFeesForm'].elements['busTaxMax'].value == 0) {
		alert("Enter ContractTax Maximum Value");
		document.forms['lookupFeesForm'].elements['busTaxMax'].focus();
		return false;
	} else if (document.forms['lookupFeesForm'].elements['busTax'].value == '') {
		alert("Enter Tax %");
		document.forms['lookupFeesForm'].elements['busTax'].focus();
		return false;
	}
	document.forms[0].action='<%=contextRoot%>/maintainContractorTax.do?save=Yes';
	document.forms[0].submit();
}

function check(){
var carCode= event.keyCode;
  if ( (carCode < 45) || (carCode > 57) || (carCode == 47)){
   event.returnValue = false;
   }
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form name="lookupFeesForm" type="elms.control.beans.LookupFeesForm" action="/maintainContractorTax">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Contractor Tax Maintenance</font><br>
                <br>
                </td>
            </tr>
            <tr>
            <td>
	            <html:errors/>
	         <logic:present name="message" scope="request" >
		            <font color='green'><bean:write name="message"/></font>
	            </logic:present>
	              <logic:present name="error" scope="request" >
		            <font color='red'><bean:write name="error"/></font>
	            </logic:present>
            </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

		                    <tr>
		                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

		                        <td width="1%" class="tablebutton"><nobr>
		                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="return clearFields();"></html:button>

		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">

                            <tr>
								<td class="tablelabel">Contractor Tax Min</td>
                                <td class="tabletext" colspan="2"><html:text size="20" property="busTaxMin" styleClass="textbox" maxlength="12" onkeypress="check();" />

                                </td>

                                <td class="tablelabel">Contractor Tax Max</td>
                                <td class="tabletext" colspan="2"><html:text size="20" property="busTaxMax" styleClass="textbox" maxlength="12" onkeypress="check();" />

                                </td>

                                  <td class="tablelabel">Tax %</td>
                                <td class="tabletext" colspan="2"><html:text size="30" property="busTax" styleClass="textbox" maxlength="12" onkeypress="check();"/>

                                </td>
                            </tr>


                            </table>
	                       </td>
	                       </tr>

                       </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

        </table>



<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Contractor Tax </font><br>
            <br>
            </td>
        </tr>
		<tr>
			<td>
	            <html:errors/>

		            <font color="green" >

            </td>
		</tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="95%" height="25" background="../images/site_bg_B7C1CB.jpg">Contractor Tax </td>

                      <td width="1%" class="tablelabel"><nobr>
                      &nbsp;</nobr></td>
                    </tr>
                    <!--Sunil-->
                  </table>
                </td>
              </tr>

             <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="5">
                    <tr>
					  <td width="5%" class="tablelabel">&nbsp;</td>
                      <td class="tablelabel">Contractor Tax Min</td>
              		 <td class="tablelabel">Contractor Tax Max</td>
              		 <td class="tablelabel">Tax</td>
              	    </tr>

					<logic:iterate id="combomapp" property="taxList" name="lookupFeesForm" >
                      <tr>
                        <td class="tabletext">  &nbsp;

				     	</td>

                    <td class="tabletext"><bean:write name="combomapp" property="busTaxMin" /></td>
					<td class="tabletext"><bean:write name="combomapp" property="busTaxMax" /></td>
					<td class="tabletext"><bean:write name="combomapp" property="busTax" /></td>

				   </tr>
                    </logic:iterate>

                  </table>
                </td>
              </tr>



            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>

</body>
</html:html>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ page import="java.util.*"%>
<%@ page import="elms.control.beans.online.EmailTemplateAdminForm"%>

<html:html>
<app:checkLogon/>
<% 
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
List emailTempList = new ArrayList();
emailTempList = (ArrayList) request.getAttribute("emailTempList");
%>
<head>
<html:base/>
<title>City of San Fernando : Activity Information Management System : Email Template Administration</title>  
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" href="../css/elms_new.css" type="text/css">

<script language="JavaScript" src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
<script language="JavaScript" src="<%=contextRoot%>/tools/jquery/jquery-migrate-1.2.1.js"></script>
</head>


<script language="JavaScript">
function doDelete() {
    document.forms[0].action='<%=contextRoot%>/emailTemplateAdmin.do?action=delete';
    document.forms[0].submit();
} 

function doAdd() {
    document.forms[0].action='<%=contextRoot%>/emailTemplateAdmin.do?action=new';
    document.forms[0].submit();
} 
</script>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form action="/emailTemplateAdmin?action=new">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Email Templates Administration<br>
            <br>
            </font></td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b></font>
        </TD>
        </TR>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="99%" height="25" class="tabletitle"><font class="con_hdr_2b">Email Templates</font></td>
                      <!-- <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td> -->
                      <td width="1%" class="tablelabel"><nobr> 
                       <html:button value="Delete" property="delete" styleClass="button" onclick="doDelete()"/>
                       <html:button value="Add" property="add" styleClass="button" onclick="doAdd()"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
				<td background="../images/site_bg_B7C1CB.jpg">  
					<table width="100%" border="0" cellspacing="1" cellpadding="2">
						 <tr> 
		                     <td width="20%" class="tablelabel">Delete</td>	
		                     <td width="40%" class="tablelabel">Title</td>	                      
		                     <td width="40%" class="tablelabel">Subject</td>
		                    	
		                     					
                    	</tr>
							
						<%for(int i=0;i<emailTempList.size();i++){ 
							EmailTemplateAdminForm emailTemplateAdminForm = (EmailTemplateAdminForm) emailTempList.get(i);
							int id = emailTemplateAdminForm.getRefEmailTempId();
							String title = emailTemplateAdminForm.getTitle();
							String subject = emailTemplateAdminForm.getEmailSubject();
							int action = emailTemplateAdminForm.getAction();
							String msg = emailTemplateAdminForm.getEmailMessage();
	
						%>
						<tr bgcolor="#FFFFFF" valign="top"> 
							
							<td class="tabletext" align="right"><html:multibox property="selectedItems"><%=id%></html:multibox> </td>
							<td class="tabletext"> <a href="<%=contextRoot%>/emailTemplateAdmin.do?action=edit&id=<%=id%>"> <%=title %> </a></td>
							<td class="tabletext">  <%=subject %></td>
							
						</tr>
					
					<%} %>
					</table>
				
				
				</td>
			</tr>		
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
 
</html:form>
</body>
</html:html>
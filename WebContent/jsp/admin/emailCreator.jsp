
<%@page import="alain.core.utils.Operator"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="elms.control.beans.online.EmailTemplateAdminForm"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>
<% 
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
EmailTemplateAdminForm f = (EmailTemplateAdminForm)request.getAttribute("emailTemplateAdminForm");

String addFlag = (String) request.getAttribute("addFlag");

String content = Operator.javascriptFriendly(f.getEmailMessage());
if(content==null) content=" ";
%>

<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta http-equiv="x-ua-compatible" content="IE=9"/> 
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="<%=contextRoot%>/tools/ckeditor_4.1/jquery/jquery.min.js"></script>
<script language="JavaScript" src="<%=contextRoot%>/tools/ckeditor_4.1/jquery/jquery-migrate-1.2.1.js"></script>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/tools/ckeditor_4.1/ckeditor.js"></script>

<script type="text/javascript">
$(function(){
	
	$(document).ready(function() {});
	
	set();
	$('#backnav').click(function() {
		window.location = "index.jsp";	
	});
	
	$(window).on('beforeunload', function(){
		//return 'Are you sure you want to leave without saving?';
	});	
});

function set(){
	try{
	CKEDITOR.instances.editor1.setData( "<%=content%>" );
	}catch(Exception){}
}

function clean(){}

function dosave() {
	
	try{
	
	   if(document.forms[0].title.value ==''){
		   alert("Please enter a title");
		   return false;
	   }
	   
	   if(document.forms[0].subject.value ==''){
		   alert("Please enter a subject");
		   return false;
	   }
	}catch(Exception){}
	   return true;
	  
} 
</script>
</head>

<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/emailTemplateAdmin?action=save" onsubmit="return dosave();">
<html:hidden property="refEmailTempId"/>
<html:hidden property="emailTempId"/>
<html:hidden property="emailTempTypeId"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Email Templates Administration<br>
            <br>
            </font></td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b></font>
        </TD>
        </TR>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="99%" class="tabletitle"><font class="con_hdr_2b">Manage Email Templates
                        </font></td>
                      <!-- <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td> -->
                      <td width="1%" class="tablelabel"><nobr> 
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                      
					  <html:submit value="Save" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td background="../images/site_bg_B7C1CB.jpg"> 
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  
                    <tr valign="top"> 
	                     <td class="tablelabel" >Email Template Type Name</td>
	                     <td class="tabletext"> 
	                      <%if(addFlag != null && addFlag.equalsIgnoreCase("Y")){ %>
	                      	<html:select property="title" styleClass="textbox">
                        	  <html:option value="-1">Please Select</html:option> 
                              <html:options collection="emailTempTypeList" property="emailTemplateTypeId" labelProperty="emailTemplateType" />
                        	</html:select>
                        <%}else{ %>
                        	<html:select property="title" styleClass="textbox" disabled="true">
                        	  <html:option value="-1">Please Select</html:option> 
                              <html:options collection="emailTempTypeList" property="emailTemplateTypeId" labelProperty="emailTemplateType"/>
                        	</html:select>
                        <%}%>
	                     </td>
	                </tr>
	                <tr valign="top"> 
	                     <td class="tablelabel" >Email Subject</td>
	                     <td class="tabletext"> 
	                      <html:text property="emailSubject" size="50" maxlength="100" styleClass="textbox"/>&nbsp;
	                     </td>
	                </tr>
                  	<tr>
						<td class="tabletext" width="100%" colspan="2">
							<textarea name="CONTENT" id="editor1" ></textarea>
						</td>
					</tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
 <script>
								
	CKEDITOR.timestamp = null;      
	CKEDITOR.config.toolbar_MA=[	                           
	                        	{ name: 'styles', items : [ 'FontSize','Font','Styles','Heading1' ] },
	                        	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript' ] },
	                        	'/',
	                        	
	                        	{ name: 'colors', items : [ 'TextColor','BGColor' ] },
	                        	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
	                        	'/',
	                        	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll' ] },
	                        	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	                        	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	                        	{ name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak','Iframe' ] },
	                        	'/',
	                        	{ name: 'tools', items : [ 'Source','-','Maximize', 'ShowBlocks','RemoveFormat','-','SpellChecker', 'Scayt'] },
	                        	{ name: 'es', items : [ 'eslink'] },	                        	
	                        ];
	
	CKEDITOR.replace( 'editor1', {
		fullPage: true,
		toolbar:'MA',
		extraPlugins: 'wysiwygarea',
		extraPlugins: 'eslink',
		removePlugins : 'forms',
		enterMode : CKEDITOR.ENTER_BR,
		shiftEnterMode : CKEDITOR.ENTER_BR
		//uiColor:'#666666'
	});

</script>	
</html:form>
</body>
</html:html>

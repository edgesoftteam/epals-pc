<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.app.common.DropdownValue"%>
<%@page import="elms.common.Dropdown"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.admin.*" %>


<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">

</head>

<script language="JavaScript" src="../script/calendar.js"></script>

<%
	//the context path
   	String contextRoot = request.getContextPath();
    String edit="";
  	edit = (String)request.getAttribute("edit");

  	elms.control.beans.admin.CustomLabelsForm clForm = (elms.control.beans.admin.CustomLabelsForm)request.getAttribute("customLabelsForm");
   	String selectedActStat="";
  	selectedActStat = clForm.getLevelCode();
	String selectedActStatCode = clForm.getLevelStat();

	Boolean disable= false;
	if(edit!=null){disable= true;}

%>
<script language="JavaScript">

	var str = "";
	var strCode = "";
	var strDesc = "";
	var delimiter = "";
	var delimiter1 = "";


function selectLevelType() {
	var size= document.forms[0].elements['levelStat'].length;

	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['levelStat'].options[i].selected) {
			str += delimiter + document.forms[0].elements['levelStat'].options[i].text;
			var text = document.forms[0].elements['levelStat'].options[i].text;
			var value = document.forms[0].elements['levelStat'].options[i].value;
			var op = new Option(text,value);
			document.forms[0].elements['selectedLevelTypes'].add(op,document.forms[0].elements['selectedLevelTypes'].options.length);
			delimiter = "\r";
			document.forms[0].elements['levelStat'].options[i].selected = false;
		}
	}
}


function deselectLevelType() {
	for (var i=0;i<document.forms[0].elements['selectedLevelTypes'].length;i++) {
		if (document.forms[0].elements['selectedLevelTypes'].options[i].selected) {
			document.forms[0].elements['selectedLevelTypes'].remove(i);
		}
	}
}
function processFieldType(){
	if(document.getElementById('fieldType').value == 'DROPDOWN'){
		document.getElementById("dropdownRow").style.display =  '';
	}else{
		document.getElementById("dropdownRow").style.display =  'none';
		}
}


function validateFunction() {

 if (document.forms['customLabelsForm'].elements['fieldLabel'].value == '') {
		alert("Enter Field Label");
		document.forms['customLabelsForm'].elements['fieldLabel'].focus();
	}else if (document.forms['customLabelsForm'].elements['fieldDesc'].value == '') {
		alert("Enter Field Description");
		document.forms['customLabelsForm'].elements['fieldDesc'].focus();
	}else if (document.forms['customLabelsForm'].elements['fieldType'].value == '') {
		alert("Enter Field Type");
		document.forms['customLabelsForm'].elements['fieldType'].focus();
	}
// 	else if(document.forms['customLabelsForm'].elements['fieldType'].value == 'DROPDOWN'){
// 		if(document.forms['customLabelsForm'].elements['dropDownOption'].value == ''){
// 			alert("Please enter dropdown option.");
// 			document.forms['customLabelsForm'].elements['dropDownOption'].focus();
// 			return false;
// 			}
// 	}
	else {
		var size= document.forms[0].elements['selectedLevelTypes'].length;
		strCode="";
		for (var i=0; i<size;i++) {
				strCode += delimiter1 + document.forms[0].elements['selectedLevelTypes'].options[i].value;
				delimiter1 = ",";
		}
		document.forms[0].elements['levelCode'].value=strCode;


	var edit ='<%=edit%>';
	if(edit != 'null'){

				document.forms[0].action='<%=contextRoot%>/createCustomLabels.do?editSave=Yes';
				document.forms[0].submit();
				}
			else{

			  	document.forms[0].action='<%=contextRoot%>/createCustomLabels.do?save=Yes';
				document.forms[0].submit();
		}
	}
}




function clearFields(){

	document.forms['customLabelsForm'].elements['fieldLabel'].value = "";
	document.forms['customLabelsForm'].elements['fieldDesc'].value = "";
	document.forms['customLabelsForm'].elements['fieldType'].value = "";
	document.forms['customLabelsForm'].elements['levelType'].value = "0";

	document.forms['customLabelsForm'].elements['fieldRequired'].checked = false;


	document.forms[0].elements['selectedLevelTypes'].length = 0;
	document.forms[0].elements['levelStat'].value = "";
	document.forms[0].elements['levelCode'].value = "";
	document.forms[0].elements['levelName'].value = "";



	str = "";
	strDesc = "";
	strCode = "";

}

function remove(){
  userInput = confirm("Are you sure you want to delete this CustomLabel?");
   if (userInput==true) {
	document.forms[0].action='<%=contextRoot%>/createCustomLabels.do?active=Yes';
    document.forms[0].submit();
    }
}

function getAtList(){
	var levelType="";
	levelType= document.forms['customLabelsForm'].elements['levelType'].value;
	document.forms[0].action='<%=contextRoot%>/createCustomLabels.do?getList=Yes&levelType='+levelType;
	document.forms[0].submit();

}

function lookUp(){

	document.forms[0].elements['Lookup'].disabled = true;

	var size= document.forms[0].elements['selectedLevelTypes'].length;
	strCode="";

	if(size!=0){
	     for (var i=0; i<size;i++) {

			 strCode += delimiter1 + document.forms[0].elements['selectedLevelTypes'].options[i].value;

			delimiter1 = ",";
	     }
		document.forms[0].elements['levelName'].value=strCode;
	}


	document.forms[0].action='<%=contextRoot%>/createCustomLabels.do?lookUp=Yes';
	document.forms[0].submit();

}
// edit
function populateLevelType(){
	if('<%=selectedActStat%>' != "null"){
		var arrActType = new Array();
		var arrActTypeCode = new Array();
		var strActType = '<%=selectedActStat%>';
		var strActTypeCode = '<%=selectedActStatCode%>';
		arrActType = strActType.split(",");
		arrActTypeCode = strActTypeCode.split(",");

		for(var i=0;i<arrActType.length-1;i++){

			var op = new Option(arrActType[i],arrActTypeCode[i]);
			document.forms[0].elements['selectedLevelTypes'].add(op,document.forms[0].elements['selectedLevelTypes'].options.length);

		}
		str = '<%=selectedActStat%>';
		strDesc = '<%=selectedActStatCode%>';
		document.forms[0].elements['levelCode'].value = '<%=selectedActStat%>';
		strCode = '<%=selectedActStatCode%>';
		document.forms[0].elements['levelName'].value = '<%=selectedActStatCode%>';




	}
}


</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="populateLevelType();">

<html:form name="customLabelsForm" type="elms.control.beans.admin.CustomLabelsForm" action="/createCustomLabels">
<html:hidden property="fieldId"/>
<html:hidden property="levelCode"/>
<html:hidden property="levelName"/>



<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Custom Fields Maintenance</font><br>
                <br>
                </td>
            </tr>
            <tr>
            <td>
	            <html:errors/>
	         <logic:present name="message" scope="request" >
		            <font color='green'><bean:write name="message"/></font>
	            </logic:present>
	              <logic:present name="error" scope="request" >
		            <font color='red'><bean:write name="error"/></font>
	            </logic:present>
            </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

		                    <tr>
		                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

		                        <td width="1%" class="tablebutton"><nobr>
		                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="return clearFields();"></html:button>
		                          <html:button property ="Lookup" value="Lookup" styleClass="button" onclick="return lookUp();"></html:button>
		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">
                            
                            <tr>
								<td class="tablelabel">Field Label</td>
                                <td class="tabletext" colspan="2"><html:text size="25" property="fieldLabel" styleClass="textbox" maxlength="30"/>

                                </td>

                                <td class="tablelabel">Field Description</td>
                                <td class="tabletext" colspan="2"><html:text size="40" property="fieldDesc" styleClass="textbox"/>

                                </td>
                            </tr>

                             <tr>
								<td class="tablelabel">Field Type</td>
                                <td class="tabletext" colspan="2">
                                <html:select property="fieldType" styleClass="textbox" onchange="processFieldType();">
                                   <html:option value="">Please Select</html:option>
                                   <html:option value="STRING">Text</html:option>
                                   <html:option value="INTEGER">Number</html:option>
                                   <html:option value="DATE">Date</html:option>
                                   <html:option value="DROPDOWN">DropDown</html:option>
                                   <html:option value="CHECKBOX">Checkbox</html:option>
                              </html:select>
                                </td>
                                <td class="tablelabel">Required</td>
                                <td class="tabletext" colspan="2">
                                <html:checkbox property="fieldRequired"> </html:checkbox>
                                </td>
							</tr>
							<tr id="dropdownRow" style=display:none;>
							<td class="tablelabel">Drop down Options <br> &nbsp; (comma separated)</font></td>
								<td class="tabletext" colspan="2"><html:text size="40" maxlength="6000" property="dropDownOption" styleClass="textbox"/></td>
							
							</tr>  
							<tr>
						
                                <td class="tablelabel">Level</td>
                                <td class="tabletext" colspan="2">
                             	 <html:select property="levelType" styleClass="textbox" onchange="getAtList();" >
                                   <html:option value="0">Please Select</html:option>
                                 <html:option value="ACTIVITY">Activity</html:option> </html:select>

                                </td>

								<td class="tablelabel">Sequence</td>
                                <td class="tabletext" colspan="2"><html:text size="25" property="sequence" styleClass="textbox" maxlength="30"/></td>
							
							</tr>


						 <tr>
                                <td class="tablelabel">Level Type</td>
                                
                                </td>
                                
                                <td colspan="2" class="tabletext">
                                <html:select property="levelStat" size="17" styleClass="textbox" multiple="multiple">
                                <html:options collection="atList" property="typeId" labelProperty="description"/> </html:select></td>
								<td width="2%" class="tabletext">
								<html:button style="width:25" property ="select" value=">" styleClass="button" onclick="selectLevelType()"></html:button><br>
								<html:button style="width:25" property ="deselect" value="<" styleClass="button" onclick="deselectLevelType()" ></html:button></td>
								<td class="tabletext" colspan="4"><select name="selectedLevelTypes" multiple size="13" style="width:250"></select></td>
                            </tr>



                            </table>
	                       </td>
	                       </tr>

                       </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

        </table>


<logic:equal value="YES" property="displayComboList" name="customLabelsForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Custom Fields Maintenance List</font><br>
            <br>
            </td>
        </tr>
		<tr>
			<td>
	            <html:errors/>

		            <font color="green" ></font>

            </td>
		</tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="95%" height="25" class="tabletitle">Custom Fields </td>

                      <td width="1%" class="tablebutton"><nobr>

					   <html:button  value="Delete" property="delete" styleClass="button" onclick="remove()"/>&nbsp;

                        &nbsp;</nobr></td>
                    </tr>
                    <!--Sunil-->
                  </table>
                </td>
              </tr>

             <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="5">
                    <tr>
					  <td width="5%" class="tablelabel">Delete</td>
                      <td width="19%" class="tablelabel">Field Label</td>
              		 <td width="19%" class="tablelabel">Field Description</td>
              		 <td width="19%" class="tablelabel">Field Type</td>
              		 <td width="19%" class="tablelabel">Level </td>
              		 <td width="19%" class="tablelabel">Level Type</td>


                    </tr>

					 <logic:iterate id="combomapp" property="comboList" name="customLabelsForm">
                      <tr>
                        <td class="tabletext">
				            	<logic:equal name="combomapp" value="Y" property="deleteDisplay">
					            <html:multibox property="selectedActItem"><bean:write name="combomapp" property="fieldId"/></html:multibox>
					            </logic:equal>
				     	</td>

                    <td class="tabletext"><a  href="<%=contextRoot%>/createCustomLabels.do?edit=Yes&fieldId=<bean:write name="combomapp" property="fieldId" />"><bean:write name="combomapp" property="fieldLabel" /></a></td>
				    <td class="tabletext"><bean:write name="combomapp" property="fieldDesc" /></td>
					<td class="tabletext"><bean:write name="combomapp" property="fieldType" /></td>
					<td class="tabletext"><bean:write name="combomapp" property="levelType" /></td>
					<td class="tabletext" nowrap><bean:write name="combomapp" property="levelTypeDesc" /></td>
				   </tr>
                    </logic:iterate>

                  </table>
                </td>
              </tr>



            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:equal>
</html:form>
</body>
<script>
if(document.getElementById('fieldType').value == 'DROPDOWN'){
	document.getElementById("dropdownRow").style.display =  '';
}
</script>
</html:html>
<%@ page import='elms.agent.*,java.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

	<app:checkLogon/>
	<html:html>
	<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

	<link rel="stylesheet" href="../css/elms.css" type="text/css">
	</head>
	<%String contextRoot = request.getContextPath(); %>

	<SCRIPT language="JavaScript">

	  	function validateFunction() {
		   	document.forms[0].action='<%=contextRoot%>/streetListAdmin.do?action=add';
			document.forms[0].submit();
	    }
		function remove() {
	     document.forms[0].action='<%=contextRoot%>/streetListAdmin.do?delete=Yes';
	     document.forms[0].submit();
		}
		function refresh(){
	     document.forms[0].action='<%=contextRoot%>/streetListAdmin.do';
	     document.forms[0].submit();
		}



</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/streetListAdmin" name="streetListAdminForm" type="elms.control.beans.admin.StreetListAdminForm" >

<html:errors/>
<!-- Start of logic tag to display /iterate values   -->


<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Street List Maintenance</font><br>
            <br>
            </td>
        </tr>

		<tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>  <td width="95%" height="25" class="tabletitle">Street List</td>

                      <td width="1%" class="tablebutton"><nobr>
						<html:button  value="Cancel" property="cancel" styleClass="button" onclick="return refresh()"/>
						<html:button property="delete" value="Delete" styleClass="button" onclick="return remove();"/>
                       <html:button property="add" value="Add" styleClass="button" onclick="return validateFunction();"/>

                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td width="10%" class="tablelabel">Delete</td>
                      <td width="15%" class="tablelabel">Street Name</td>
                      <td width="30%" class="tablelabel">Prefix Dir</td>
                      <td width="25%" class="tablelabel">Street Type </td>
           			  <td  class="tablelabel" width="22%">Suffix Dir</td>
                    </tr>
                    <logic:iterate id="streetListId" name="streetList">
                      <tr class="tabletext">
						<logic:equal value="Y" property="fieldSix" name="streetListId">
                          <td><html:multibox property="selectedMaping"><bean:write name="streetListId" property="fieldOne"/></html:multibox></td>
                        </logic:equal>
                        <logic:equal value="N" property="fieldSix" name="streetListId"><td>&nbsp;</td></logic:equal>
						<td class="tabletext" >

                            <a href ="<%=contextRoot%>/streetListAdmin.do?id=<bean:write name="streetListId" property="fieldOne"/>" >
                               <bean:write name="streetListId" property="fieldTwo" />
                            </a>

                        </td>
                        <td class="tabletext"><bean:write name="streetListId" property="fieldThree" /></td>
                        <td class="tabletext" width="254"><bean:write name="streetListId" property="fieldFour" /></td>
                        <td class="tabletext"><bean:write name="streetListId" property="fieldFive" /></td>
                      </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>


</html:form>
</body>
</html:html>
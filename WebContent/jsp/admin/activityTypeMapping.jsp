<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page import='java.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%
String contextRoot = request.getContextPath();
List subProjectSubTypes = (List)request.getAttribute("subProjectSubTypes");
List subProjectSubSubTypes = (List)request.getAttribute("subProjectSubSubTypes");
List activityTypes = (List)request.getAttribute("activityTypes");
String id = (String)request.getAttribute("mappingId");

 %>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="javascript" type="text/javascript" src="../script/actb.js"></script>
<script language="javascript" type="text/javascript" src="../script/common.js"></script>
<SCRIPT language="JavaScript">

var strValue;
function validateFunction() {
	/*strValue=false;
	if (document.forms['activityTypeForm'].elements['subProjectSubType'].value == '-1'){
		alert("Select Sub Project Type");
		document.forms['activityTypeForm'].elements['type'].focus();
		strValue=false;
	}else if (document.forms['activityTypeForm'].elements['subProjectSubSubType'].value == '-1') {
		alert("Select Sub-Project Sub Type");
		document.forms['activityTypeForm'].elements['subType'].focus();
		strValue=false;
	}else{
	   	document.forms[0].action='<%//=contextRoot%>/activityTypeMapping.do?save=Yes';
		document.forms[0].submit();
    }*/
	document.forms[0].action='<%=contextRoot%>/activityTypeMapping.do?save=Yes&id=<%=id%>';
	document.forms[0].submit();
}
function lookup() {
	strValue=false;
	if (document.forms['activityTypeForm'].elements['subProjectSubType'].value == '-1' &&
	    document.forms['activityTypeForm'].elements['subProjectSubSubType'].value == '-1'  &&
	    document.forms['activityTypeForm'].elements['activityType'].value == '' ) {
	    strValue=false;
	}else{
       document.forms[0].action='<%=contextRoot%>/activityTypeMapping.do?lookup=Yes';
       document.forms[0].submit();
	}
}
</SCRIPT>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form focus="landUse" name="activityTypeForm" type="elms.control.beans.admin.ActivityTypeForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Activity Type Mapping</font><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <html:errors property="error"/>
                    <tr>
                      <td width="99%" class="tabletitle">Mapping</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return lookup();"></html:button>
                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Delete</td>
                      <td class="tablelabel">Activity Type</td>
                      <td class="tablelabel">Sub Project Type</td>
                      <td class="tablelabel">Sub-Type</td>
                    </tr>
                    <tr>
                      <td class="tabletext" >
                      </td>
                      <td class="tabletext" >
						<html:select property="activityType" styleClass="textbox">
                    	  <html:option value="">Please Select</html:option>
                          <html:options collection="activityTypes" property="type" labelProperty="departmentDescription" />
                    	</html:select>
                      </td>
                      <td class="tabletext">
                        <html:select property="subProjectSubType" styleClass="textbox">
							<html:option value=""> Please select</html:option>
							<html:options collection="subProjectSubTypes" property="subProjectTypeId" labelProperty="description" />
						</html:select>
                      </td>
                      <td class="tabletext">
                        <html:select property="subProjectSubSubType" styleClass="textbox">
							<html:option value=""> Please select</html:option>
							<html:options collection="subProjectSubSubTypes" property="subProjectSubTypeId" labelProperty="description" />
						</html:select>
                      </td>
                    </tr>

                    <tr valign="top">
                      <td class="tablelabel">Delete</td>
                      <td class="tablelabel">Activity Type</td>
                      <td class="tablelabel">Sub Project Type</td>
                      <td class="tablelabel">Sub Project Sub Type</td>
                    </tr>

                    <logic:iterate id="maping" name="mapings">
                    <tr class="tabletext">
                      <td class="tabletext" ><html:multibox property="selectedMaping"><bean:write name="maping" property="fieldOne"/></html:multibox></td>
                      <td class="tabletext" ><a href="<%=contextRoot%>/activityTypeMapping.do?id=<bean:write name="maping" property="fieldOne" />&actType=<bean:write name="maping" property="fieldSeven" />&subType=<bean:write name="maping" property="fieldTwo" />&spst=<bean:write name="maping" property="fieldSix" />"><bean:write name="maping" property="fieldFive" /></a></td>
                      <td class="tabletext" ><bean:write name="maping" property="fieldThree" /></td>
                      <td class="tabletext" ><bean:write name="maping" property="fieldFour" /></td>
                    </tr>
                    </logic:iterate>
                </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

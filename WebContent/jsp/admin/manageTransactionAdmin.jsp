<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="elms.util.StringUtils"%>
<%@page import="elms.app.admin.Transaction"%>
<%@page import="java.util.List"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<% 
	String contextRoot = request.getContextPath();
	List<Transaction> transactionList  = (List)request.getAttribute("transactionList");
	String permitNumber = (String)request.getAttribute("permitNumber");
%>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>	
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
	font: normal 8pt/9pt verdana,arial,helvetica;
	color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
	<html:form action="/manageTransactions.do" name="nullForm" type="elms.control.beans.NullForm" >
		<table width="100%" border="0" cellspacing="10" cellpadding="0" >
			<tr valign="top">
				<td width="100%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<font class="con_hdr_3b">Manage Transactions
									<div style="float: right;">
										<input type="text" class="textboxd" id="permitNumber" name="permitNumber" placeholder="Permit Number" value="<%=permitNumber%>"/>
										<input type="submit" value="Search" onclick="return filterData();"/>
									</div>
									<br><br>
								</font>
							</td>
						</tr>
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="100%" height="25" bgcolor="#B7C1CB"><font class="con_hdr_2b">Transactions List</font></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
								<table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Permit Number</font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Transaction #</font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Response Code</font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Amount</font></td>
										<td width="20%" class="tablelabel"><font class="con_hdr_1">Payment Date Time</font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Status</font></td>
										<td width="20%" class="tablelabel"><font class="con_hdr_1">Description</font></td>
									</tr>
										<%
											for (int i = 0; i < transactionList.size(); i++) {
												Transaction t = (Transaction) transactionList.get(i);
										%>
										<tr>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<%= t.getPermitNumber() %>
												</font>
											</td>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<a href="javascript:getTransactionDetails(<%= t.getTxnNumber() %>,<%=t.getActId()%>)"><%= t.getTxnNumber() %></a>
												</font>
											</td>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<%= StringUtils.nullReplaceWithBlank(t.getResCode()) %>
												</font>
											</td>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<%= t.getAmount() %>
												</font>
											</td>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<%= StringUtils.nullReplaceWithBlank(t.getCreated()) %>
												</font>
											</td>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<%= StringUtils.nullReplaceWithBlank(t.getTxnStatus()) %>
												</font>
											</td>
											<td class="tabletext" valign="top">
												<font class="con_text_1">
												<%= StringUtils.nullReplaceWithBlank(t.getTxnDesc()) %>
												</font>
											</td>
										</tr>
									<%} %>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td>&nbsp;</td></tr>
			<tr valign="top" id="txnRow" style="display: none;">
				<td width="100%">
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="100%" height="25" bgcolor="#B7C1CB"><font class="con_hdr_2b">Transaction Summary</font></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Transaction #</font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Permit Number</font></td>
										<td width="15%" class="tablelabel"><font class="con_hdr_1">Transaction Status</font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1">Settled Amount</font></td>
										<td width="20%" class="tablelabel"><font class="con_hdr_1"><nobr>Submit Time Local</nobr></font></td>
										<td width="10%" class="tablelabel"><font class="con_hdr_1"><nobr>Response Code</nobr></font></td>
										<td width="25%" class="tablelabel"><font class="con_hdr_1"><nobr>Response Reason Description</nobr></font></td>
									</tr>
									<tr id="txnData"></tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr id="loader" style="display: none;"><td><img src="<%=contextRoot%>/jsp/images/loaders/loading.gif" width="10%" height="10%"></td></td></tr>
		</table>
	</html:form>
	<script type="text/javascript">
		function filterData(){
			var permitNumber = document.getElementById('permitNumber').value;
			if(permitNumber == ''){
				alert('Enter a permit number');
				return false;
			} else {
				document.forms[0].action='<%=contextRoot%>/manageTransactions.do?action=filter&permitNumber='+permitNumber;
				document.forms[0].submit();
			}
		}
		 
		function calAPI(tid,aid){
			$.ajax({
				type: 'POST',
				url: "<%=contextRoot%>/manageTransactions.do?action=transaction&id="+tid+"&aid="+aid, 
				success: function(result){
					console.log(result);
					$('#txnRow').show();
					$('#txnData').html(result);	
					$('#loader').hide();
				}
		    });
			
		} 
		
		function getTransactionDetails(tid,aid){
			$('#txnData').html('');
			$('#loader').show();
			$('#txnId').empty();
			$('#txnId').append(tid);
			
			window.setTimeout( calAPI(tid,aid), 5000 ); 
		}
	</script>
</body>
</html:html>

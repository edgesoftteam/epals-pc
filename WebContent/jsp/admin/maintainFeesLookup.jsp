<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@page import="elms.control.beans.LookupFeeEdit"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%@ page import="elms.app.finance.*" %>
<%@ page import="elms.app.project.*" %>
<%@ page import="java.util.*"%>
<%@ page import="elms.util.*"%>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<%! int lookupFeeListSize = 0;%>
<%
	String contextRoot = request.getContextPath();
	//Get parameters from the session
    String lookupId = (String) session.getAttribute("lookupId");
    List lookupList = (List) session.getAttribute("lookupList");

	lookupFeeListSize = ((List) session.getAttribute("lookupFeeList")).size();

	boolean insertRowTrue = elms.util.StringUtils.s2b((String)request.getAttribute("insertRowTrue"));

	int lookupFeeListSize4AddRow = elms.util.StringUtils.s2i((String) request.getAttribute("lookupFeeListSize4AddRow"));

	pageContext.setAttribute("lookupList", lookupList);
%>
<script>
function onclickprocess(strval)
{
    if (strval == 'cfl') {
        document.lookupFeesForm.action='<%=contextRoot%>/cancelFeesLookup.do';
        document.lookupFeesForm.submit();
        return true;
    }else if (strval == 'sfl') {
       document.lookupFeesForm.action='<%=contextRoot%>/saveFeesLookup.do';
       document.lookupFeesForm.submit();
       return true;
    }else if (strval == 'dfl') {
       document.lookupFeesForm.action='<%=contextRoot%>/deleteFeesLookup.do';
       document.lookupFeesForm.submit();
       return true;
    }else if (strval == 'pfl') {
       document.lookupFeesForm.action='<%=contextRoot%>/previewFeesLookup.do';
       document.lookupFeesForm.submit();
       return true;
    }else if (strval == 'afl') {
      document.lookupFeesForm.action='<%=contextRoot%>/addFeesLookup.do';
      document.lookupFeesForm.submit();
      return true;
    }else if (strval == 'arfl') {
      document.lookupFeesForm.action='<%=contextRoot%>/addRowFeesLookup.do?insertRowTrue=false';
      document.lookupFeesForm.submit();
      return true;
    }
}

function onchangeprocess()
{
     document.lookupFeesForm.action='<%=contextRoot%>/maintainFeesLookup.do?flCount=0';
     document.lookupFeesForm.submit();
     return true;
}

function lowRange(str)
{
var insertRowTrue = <%=insertRowTrue%>;

var j=0;
	if(insertRowTrue == false){
		ctr = '<%=lookupFeeListSize%>';
		if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
			event.returnValue = false;
		}else{
			for(i=1;i<=ctr;i++){
				if (document.forms[0].elements[(i*12)-(j*3)].value.length == 12 ){
						document.forms[0].elements[(i*12)-(j*3)].value = document.forms[0].elements[(i*12)-(j*3)].value +'.';
	 			}if (document.forms[0].elements[(i*12)-(j*3)].value.length > 15 )  event.returnValue = false;
				j=j+1;
				}//for loop end
		}//inner else end
	}else{
	ctr = '<%=lookupFeeListSize4AddRow%>';
		if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
			event.returnValue = false;
		}else{
			for(i=1;i<=ctr;i++){
				if (document.forms[0].elements[(i*12)-(j*3)].value.length == 12 ){
						document.forms[0].elements[(i*12)-(j*3)].value = document.forms[0].elements[(i*12)-(j*3)].value +'.';
	 			}if (document.forms[0].elements[(i*12)-(j*3)].value.length > 15 )  event.returnValue = false;
				j=j+1;
				}//for loop end
		}//inner if's else end
	 }//outer if's else end
}//method end


function check()
{
var insertRowTrue = <%=insertRowTrue%>;
var j=0;
	if(insertRowTrue == false){
		ctr = '<%=lookupFeeListSize%>';
	}else{
		ctr = '<%=lookupFeeListSize4AddRow%>';
	}//outer if's else end

	var enteredValue = document.forms[0].elements[(ctr*12)-((ctr-1)*3)].value;

	for(i=1;i<=ctr-1 ;i++){
		compare = document.forms[0].elements[(i*13)-(j*4)].value;
		if(enteredValue == compare){
			alert("Please choose other Low range value");
			document.forms[0].elements[(ctr*12)-((ctr-1)*3)].focus;
			event.returnValue = false;
		}
		j=j+1;
		}//for loop end
		
}//method end


function addValue()
{
	ctr = '<%=lookupFeeListSize%>';
	var enteredValue = document.forms[0].elements[(ctr*12)-((ctr-1)*3)].value;
	document.forms[0].elements[(ctr*12)-((ctr-1)*3)].value = document.forms[0].elements[(ctr*13)-((ctr-1)*4)].value + 1;	
}

function highRange(str)
{
var insertRowTrue = <%=insertRowTrue%>;

var j=0;
	if(insertRowTrue == false){
		ctr = '<%=lookupFeeListSize%>';
		if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
			event.returnValue = false;
		}else{
			for(i=1;i<=ctr;i++){
				if (document.forms[0].elements[(i*13)-(j*4)].value.length == 12 ){
					document.forms[0].elements[(i*13)-(j*4)].value = document.forms[0].elements[(i*13)-(j*4)].value +'.';
 				}if (document.forms[0].elements[(i*13)-(j*4)].value.length > 15 )  event.returnValue = false;
			j=j+1;
				}//for loop end
		}//inner else end
	}else{
	ctr = '<%=lookupFeeListSize4AddRow%>';
		if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
			event.returnValue = false;
		}else{
			for(i=1;i<=ctr;i++){
				if (document.forms[0].elements[(i*13)-(j*4)].value.length == 12 ){
					document.forms[0].elements[(i*13)-(j*4)].value = document.forms[0].elements[(i*13)-(j*4)].value +'.';
 				}if (document.forms[0].elements[(i*13)-(j*4)].value.length > 15 )  event.returnValue = false;
			j=j+1;
				}//for loop end
		}//inner if's else end
	 }//outer if's else end
}//method end

</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form focus="lookupId" name="lookupFeesForm" type="elms.control.beans.LookupFeesForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Fees Maintenance</font><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Fee Lookup Schedule </td>

                                <td width="1%" class="tablebutton"><nobr> <html:button property="cancel" value="Cancel" styleClass="button" onclick="return onclickprocess('cfl')" /> &nbsp;</nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><nobr>Find / Browse ></nobr></td>
                                <td class="tablelabel" colspan="2"><nobr>Name:


                     <html:select property="lookupId" value="<%=lookupId%>" styleClass="textbox" style="position:absolute" onchange="return onchangeprocess()">
 					 <html:option value="">Please Select</html:option>
 					 <html:option value="ALL">All</html:option>
					 <html:options collection="lookupList" property="lookupFeeId" labelProperty="lookupFee"/>
					</html:select> </nobr></td>
                                <td class="tablelabel" colspan="4" align="right"><nobr>&nbsp;</nobr></td>
                            </tr>
                            <tr>
                                <td class="tablelabel"><nobr>Check Functions ></nobr></td>
                                <td class="tablelabel" align="center"><html:button property="delete" value="Delete" styleClass="button" onclick="return onclickprocess('dfl')"> </html:button></td>
                                <td class="tablelabel" align="center"><html:button property="edit" value="Save" styleClass="button" onclick="return onclickprocess('sfl')"></html:button></td>
                                <td class="tablelabel" align="center">Change &quot;%&quot; <html:text property="percent" styleClass="textbox" size="5" value="0" onkeypress="return validateNumeric();"/>

                                 <html:button property="preview" value="Preview" styleClass="button" onclick="return onclickprocess('pfl')"> </html:button>
                                  <html:button property="change" value="Change" styleClass="button" onclick="return onclickprocess('sfl')"></html:button></td>

                                <td class="tablelabel" align="center"><html:button property="addLookup" value="Add Lookup" styleClass="button" onclick="return onclickprocess('afl')"></html:button></td>
                                <td class="tablelabel" colspan="2" align="center">
                                 <html:button property="addRow" value="Add" styleClass="button" onclick="return onclickprocess('arfl')"></html:button></td>
                            </tr>
                            <tr>
                                <td colspan="7" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel">Check</td>
                                <td class="tablelabel"><nobr> LookupName</nobr></td>
                                <td class="tablelabel">Effective</td>
                                <td class="tablelabel">Low Range</td>
                                <td class="tablelabel">High Range</td>
                                <td class="tablelabel">Result</td>
                                <td class="tablelabel" colspan="2">Over</td>
                                <td class="tablelabel">Plus</td>
                                <td class="tablelabel">Up</td>
                            </tr>
<%
						if (lookupId !="0") {
%>
						<nested:iterate property="lkupFeeEditList">
                            <tr>
                                <td class="tabletext" align="center"><nested:checkbox property="check" ></nested:checkbox></td>
                                <td class="tabletext"> <nested:text property="lookupFee" readonly="true" styleClass="textbox" size="20" /> </td>
                                <td class="tabletext"> <nested:text property="creationDate" readonly="true" styleClass="textbox" size="12" /> </td>
                                <td class="tabletext"> <nested:text property="lowRange" styleClass="textboxm" maxlength="15" size="12" onblur ="return check();"  onkeypress="return lowRange(this);" /> </td>
                                <td class="tabletext"> <nested:text property="highRange" styleClass="textboxm" maxlength="15" size="17" onkeypress="return highRange('highRange');" /> </td>
                                <td class="tabletext"> <nested:text property="result" styleClass="textboxm" size="12" onkeypress="return validateNumeric();" /> </td>
                                <td class="tabletext"> <nested:text property="over" styleClass="textboxm" size="12" onkeypress="return validateNumeric();" /> </td>
                                <td class="tabletext"> </td>
                                <td class="tabletext"><nested:text property="plus" styleClass="textboxm" size="7" onkeypress="return validateNumeric();"/></td>
                                <td align="center"><nested:checkbox property="up" /></td>
                            </tr>
						</nested:iterate>
<%
						}
%>
                        </table>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2"">
                            <tr class="tabletext">
                                <td colspan="7"><img src="../images/spacer.gif" width="1" height="7"></td>
                            </tr>
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" align="center"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" align="center"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" align="center"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" align="center"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" colspan="2" align="center"> </td>
                            </tr>
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" colspan="2"></td>
                                <td background="../images/site_bg_B7C1CB.jpg" colspan="4" align="right"></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>



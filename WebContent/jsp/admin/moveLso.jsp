<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<app:checkLogon/>

<html:html>

<head>
	<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String moveStreetId="-1";
	String toStreetId="-1";
	try {
		moveStreetId = (String) request.getAttribute("moveStreetId");
		toStreetId = (String) request.getAttribute("toStreetId");
	} catch (Exception e) {
		toStreetId = "-1";
	}
%>

<script src="../script/yCodelib.js"> </script>
<script src="../script/yCode_combo_box.js"> </script>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>

<script language="JavaScript">
var strValue;
function validateFunction() {
    strValue = validateData('req',document.forms['moveLsoForm'].elements['moveNumber'],'Enter From Street Number');
    if (strValue == true) {
    	strValue=validateData('req',document.forms['moveLsoForm'].elements['toNumber'],'Enter To Street Number');
    }else return false;
    return strValue ;
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onresize="fixTable();fixTable1();AdjustComboBoxes(); " onload="initComboBox(<%=moveStreetId%>,<%=toStreetId%>); resetFields();fixTable();fixTable1();AdjustComboBoxes();">
<html:form action="/verifyMoveLso" onsubmit="return validateFunction();">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Move LSO</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Move Structure or Occupancy</td>

                      <td width="1%" class="tablebutton"><nobr>
					  <html:submit property="verifyMoveLso"  value="Verify"  styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                    	<td width="20%" class="tablelabel" > Move</td>
                    	<td width="20%" class="tabletext"><html:select property="moveType" styleClass="textbox" > <html:option value="S">Structure</html:option> <html:option value="O">Occupancy</html:option></html:select></td>
                        <td width="20%" class="tabletext">
                             <html:text size="8" property="moveNumber" styleClass="textbox" />
                             <html:select property="moveFraction" styleClass="textbox" onblur="MoveStreetName.focus();"> <html:option value=""></html:option> <html:option value="1/4">1/4</html:option> <html:option value="1/2">1/2</html:option> <html:option value="3/4">3/4</html:option> </html:select>
                        </td>
                        <td width="20%" class="tabletext">
                           <table border="0" id="tableAutoComboBox" onresize="fixTable();" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td id=tdid><html:select property="moveName" styleClass="textbox" style="position:absolute" onchange="if (MoveStreetName!=null) {MoveStreetName.processComboBoxSelect();}">
                                        <html:option value="-1">Please Select</html:option>
                                        <html:options collection="streets" property="streetId" labelProperty="streetName"/>
                                        </html:select></td>
                                </tr>
                            </table>
                        </td>
                        <td width="20%" class="tabletext">
                        	Unit: <html:text size="8" property="moveUnit" styleClass="textbox" />
                        </td>
                      </tr>
                    <tr valign="top">
                    	<td width="20%" class="tablelabel"> To</td>
                        <td width="20%" class="tabletext">&nbsp;</td>
                        <td width="20%" class="tabletext">
                             <html:text size="8" property="toNumber" styleClass="textbox" />
                             <html:select property="toFraction" styleClass="textbox" onblur="ToStreetName.focus();"> <html:option value=""></html:option> <html:option value="1/4">1/4</html:option> <html:option value="1/2">1/2</html:option> <html:option value="3/4">3/4</html:option> </html:select>
                        </td>
                        <td width="40%" class="tabletext" colspan="2">
                           <table border="0" id="tableAutoComboBox1" onresize="fixTable1();" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td id=tdid1><html:select property="toName" styleClass="textbox" style="position:absolute" onchange="if (ToStreetName!=null) {ToStreetName.processComboBoxSelect();}">
                                        <html:option value="-1">Please Select</html:option>
                                        <html:options collection="streets" property="streetId" labelProperty="streetName"/>
                                        </html:select></td>
                                </tr>
                            </table>
                        </td>
                      </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
  	<td colspan="2"><html:errors/></td>
  </tr>
</table>

</html:form>
</body>
</html:html>
<SCRIPT>
//==================================================================
//initComboBox() creates a combo box object
//------------------------------------------------------------------
var MoveStreetName;
var ToStreetName;

function initComboBox(moveStreetId,toStreetId)
{
	MoveStreetName= new ComboBoxObject("moveName", "MoveStreetName");
	MoveStreetName.setDoLookup(true);
	MoveStreetName.setCaseInsensitive(true);
	MoveStreetName.setDefaultByValue(moveStreetId);
	//MoveStreetName.focus();

	ToStreetName = new ComboBoxObject("toName", "ToStreetName");
	ToStreetName.setDoLookup(true);
	ToStreetName.setCaseInsensitive(true);
	ToStreetName.setDefaultByValue(toStreetId);
	//ToStreetName.focus();

}
function fixTable() {
	if (MoveStreetName!=null)
	{
		if (tdid.offsetWidth < MoveStreetName.ComboWidth)
		{
			var temp = tableAutoComboBox.style.tableLayout;
			tableAutoComboBox.style.tableLayout = "fixed";
			tdid.style.posWidth = MoveStreetName.ComboWidth;
			tableAutoComboBox.style.tableLayout = temp;
		}
	}

}
function fixTable1() {
	if (ToStreetName != null)
	{
		if (tdid1.offsetWidth < ToStreetName.ComboWidth)
		{
			var temp = tableAutoComboBox1.style.tableLayout;
			tableAutoComboBox1.style.tableLayout = "fixed";
			tdid1.style.posWidth = ToStreetName.ComboWidth;
			tableAutoComboBox1.style.tableLayout = temp;
		}
	}
}
</SCRIPT>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>

<app:checkLogon/>


<%@page import="java.util.ArrayList"%>
<%@page import="elms.control.beans.admin.OnlinePermitForm"%>

<%@page import="elms.util.StringUtils"%><html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"> </script>
<%
	//the context path
   	String contextRoot = request.getContextPath();
	elms.control.beans.admin.OnlinePermitForm onlinePermitForm = (elms.control.beans.admin.OnlinePermitForm)request.getAttribute("onlinePermitForm");
	ArrayList a = new ArrayList();
	a = (ArrayList)request.getAttribute("lkupQuestionaireList");
%>
<script language="JavaScript">

var actType = "<%=onlinePermitForm.getActType()%>";



var str = "";
var strCode = "";
var strDesc = "";
var delimiter = "";
var delimiter1 = "";


function loadActFees() {
    document.forms[0].action='<%=contextRoot%>/onlineloadqa.do?action=edit';
    document.forms[0].submit();
}

function clearFields(){
	str = "";
	strDesc = "";
	strCode = "";
	
	document.forms[0].description.value="";
	document.forms[0].actType.value="";
    document.forms[0].action='<%=contextRoot%>/onlineloadqa.do';
    document.forms[0].submit();

}

function validateFunction() {

	
	
	if(document.forms[0].actType.value ==""){
		alert("Please select a Table");
		document.forms[0].stypeId.focus();
		return false;
	}
	
	document.forms[0].action='<%=contextRoot%>/onlineloadqa.do?action=save';
    document.forms[0].submit();
}

function lkup() {
	document.forms[0].action='<%=contextRoot%>/onlineloadqa.do?action=lkup';
    document.forms[0].submit();
}

function del() {
	document.forms[0].action='<%=contextRoot%>/onlineloadqa.do?action=del';
    document.forms[0].submit();
}
function getSubTypes() {
	if(document.forms[0].actType.value !=""){
		document.forms[0].action='<%=contextRoot%>/onlineloadqa.do?action=dropdown';
    	document.forms[0].submit();
	}
}

function frmLoad(){
	document.forms[0].actType.value = actType;
	
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="frmLoad()">
<html:form action="/onlineloadqa">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
	            <tr>
	                <td><font class="con_hdr_3b">Online Load Questions/Acknowledgment</font><br><br></td>
	            </tr>
	            <tr>
	            <td>
		            <html:errors/>
		            <logic:present name="message" scope="request" >
			            <font color='green'><bean:write name="message"/></font>
		            </logic:present>
	            </td>
	            </tr>
				<tr>
	                <td>
	                <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                        <td>
	                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
			                    <html:errors property="error"/>
			                    <tr>
			                      <td width="99%" class="tabletitle">Add/Update</td>

			                        <td width="1%" class="tablebutton"><nobr>
			                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="return clearFields();"></html:button>
			                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
			                          &nbsp;</nobr></td>
			                      </tr>
	                        </table>
	                        </td>
	                    </tr>
	                    <tr>
	                        <td background="../images/site_bg_B7C1CB.jpg">
	                          <table width="100%" cellpadding="2" cellspacing="1">
								<tr>
									<td class="tablelabel"><nobr>Activity Type</nobr></td>
                                	<td class="tabletext" colspan="6">
                                	<html:select property="actType" styleClass="textbox" onchange="getSubTypes();" >
                                			<option value="" > Please Select</option>
											<option value="LKUP_QUESTIONS" > QUESTIONS</option>
											<option value="LKUP_ACKNOWLEDGMENTS" > ACKNOWLEDGMENTS</option>
									<!-- <bean:write name="onlinePermitForm" property="actType"/> -->
									</html:select>
                                	</td>
								</tr>
								<tr>
									<td class="tablelabel"><nobr>Description</nobr></td>
                                	<td class="tabletext" colspan="6">
                                	<html:textarea rows="5" cols="60" property="description"/>
                                	</td>
								</tr>
	                              <!-- End of Displaying list Land Use with multibox  -->
	                          </table>
							</td>
						</tr>
						
						<tr>
				<td> <br>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="100%" height="25"  class="tabletitle">Lookup </td>
                       <td width="1"><img src="/obc/jsp/images/site_bg_cccccc.jpg" width="1" height="19"></td>
                        <td width="1%" background="../images/site_bg_D7DCE2.jpg"> 
                       		<html:button  value="Delete" property="delete" styleClass="button" onclick="del()"/>
                    	</td>
                      
                    </tr>  
                  </table>
					<table width="100%" border="0" cellspacing="1" cellpadding="2" bgcolor="#b7c1cb">
						<tr> 
							<td class="tablelabel">&nbsp;</td>
							<td class="tablelabel">  DESCRIPTION </td>
							
						</tr>
							
						<!-- add -->
						<%for(int i=0;i<a.size();i++){ 
							int lkId = ((OnlinePermitForm)a.get(i)).getLkId();
							String question = ((OnlinePermitForm)a.get(i)).getDescription();
							
							
	
						%>
						<tr valign="top"> 
							<td class="tabletext">
							<html:multibox property="deleteSelectedId" value="<%=StringUtils.i2s(lkId) %>"></html:multibox></td>
							<td class="tabletext">  <%=question %> </td>
													
						</tr>
					
					<%} %>
					</table>
				
				
				</td>
			</tr>		
						
	                </table>
	                
	                
	                
	                </td>
	            </tr>
	        </table>
	    </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>


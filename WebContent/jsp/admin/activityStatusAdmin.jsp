<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*,java.util.*'%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%!
List moduleList = new ArrayList();
List pNameList = new ArrayList();
 %>
<%
	String contextRoot = request.getContextPath();
	try{
	//Function to get list of module
	moduleList = LookupAgent.getModules();
	pageContext.setAttribute("moduleList", moduleList);

    //Function to get list of Project Name
	pNameList = LookupAgent.getProjectNames();
	pageContext.setAttribute("pNameList", pNameList);
	}catch(Exception e){}
%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="javascript">
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");


function test(){
    document.getElementById('tableDiv').innerHTML = '';
	document.getElementById("tableDiv").style.display =  'none';
	document.getElementById('loader').innerHTML = '';
}

function checkTable(){
	document.forms['activityStatusAdminForm'].elements['Lookup'].disabled = true;
	document.getElementById('loader').innerHTML = '<img src="../images/loader.gif" style="float:right;">';

	var url = "<%=contextRoot%>/activityStatusAdmin.do?lookup=Yes";

	if(document.forms['activityStatusAdminForm'].elements['statusCode'].value != ''){
		url = url + "&statusCodeStr=" + document.forms['activityStatusAdminForm'].elements['statusCode'].value;
	}
	if(document.forms['activityStatusAdminForm'].elements['descriptionType'].value != ''){
		url = url + "&descriptionType=" + document.forms['activityStatusAdminForm'].elements['descriptionType'].value;
	}
	if(document.forms['activityStatusAdminForm'].elements['active'].checked == true){
		url = url + "&active=Y" ;
	}else{
		url = url + "&active=N" ;
	}
	if(document.forms['activityStatusAdminForm'].elements['psaActive'].checked == true){
		url = url + "&psaActive=Y";
	}else{
		url = url + "&psaActive=N";
	}
	if(document.forms['activityStatusAdminForm'].elements['projectNameId'].value != '-1'){
		url = url + "&projectNameId=" + document.forms['activityStatusAdminForm'].elements['projectNameId'].value;
	}
	if(document.forms['activityStatusAdminForm'].elements['moduleId'].value != '-1'){
		url = url + "&moduleId=" + document.forms['activityStatusAdminForm'].elements['moduleId'].value;
	}
	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);
}

function checkTableHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var statusResult=xmlhttp.responseText;
				if(statusResult == 'Activity Status Saved Successfully'){
					document.getElementById('saveDiv').innerHTML = statusResult;
				}else{
					document.forms['activityStatusAdminForm'].elements['Lookup'].disabled = false;
					document.getElementById('loader').innerHTML = '';
					if(statusResult == 'No Records Found'){
						document.getElementById('saveDiv').innerHTML = statusResult;
					}else{
						document.getElementById('tableDiv').innerHTML = statusResult;
					}

				}
			    document.getElementById("tableDiv").style.display =  '';
 	}
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}

</script>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction() {
		strValue=false;
		if (document.forms['activityStatusAdminForm'].elements['statusCode'].value == '') {
			alert("Enter Activity Status");
			document.forms['activityStatusAdminForm'].elements['statusCode'].focus();
			strValue=false;
		}else if (document.forms['activityStatusAdminForm'].elements['descriptionType'].value == ''){
			alert("Enter Description");
			document.forms['activityStatusAdminForm'].elements['descriptionType'].focus();
			strValue=false;
		}else if (document.forms['activityStatusAdminForm'].elements['projectNameId'].value == '-1') {
			alert("Select Project Name");
			document.forms['activityStatusAdminForm'].elements['projectNameId'].focus();
			strValue=false;
		}else if (document.forms['activityStatusAdminForm'].elements['moduleId'].value == '-1') {
			alert("Select Module");
			document.forms['activityStatusAdminForm'].elements['moduleId'].focus();
			strValue=false;
		}else{
			var url = '<%=contextRoot%>/activityStatusAdmin.do?save=Yes';

			if(document.forms['activityStatusAdminForm'].elements['activityStatusId'].value != ''){
				url = url + "&statusId=" + document.forms['activityStatusAdminForm'].elements['activityStatusId'].value;
			}
			if(document.forms['activityStatusAdminForm'].elements['statusCode'].value != ''){
				url = url + "&statusCodeStr=" + document.forms['activityStatusAdminForm'].elements['statusCode'].value;
			}
			if(document.forms['activityStatusAdminForm'].elements['descriptionType'].value != ''){
				url = url + "&descriptionType=" + document.forms['activityStatusAdminForm'].elements['descriptionType'].value;
			}
			if(document.forms['activityStatusAdminForm'].elements['active'].checked == true){
				url = url + "&active=Y" ;
			}else{
				url = url + "&active=N" ;
			}
			if(document.forms['activityStatusAdminForm'].elements['psaActive'].checked == true){
				url = url + "&psaActive=Y";
			}else{
				url = url + "&psaActive=N";
			}
			if(document.forms['activityStatusAdminForm'].elements['projectNameId'].value != '-1'){
				url = url + "&projectNameId=" + document.forms['activityStatusAdminForm'].elements['projectNameId'].value;
			}
			if(document.forms['activityStatusAdminForm'].elements['moduleId'].value != '-1'){
				url = url + "&moduleId=" + document.forms['activityStatusAdminForm'].elements['moduleId'].value;
			}
			xmlhttp.open('POST', url,true);
			xmlhttp.onreadystatechange = checkTableHandler;
		    xmlhttp.send(null);
		}
	}

function refresh(){
     document.forms['activityStatusAdminForm'].action='<%=contextRoot%>/activityStatusAdmin.do';
     document.forms['activityStatusAdminForm'].submit();
}

var no_array = new Array();
function checkIds(val){
	var valNew=val;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false ){
		document.forms[0].checkboxArray.value = removeFromArray(valNew.value,no_array).join(",");
     	checkboxArray = removeFromArray(valNew.value,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}
	if(valNew != ""){
	 checkboxArray += valNew.value+",";
	}
	document.forms[0].checkboxArray.value = checkboxArray;
	return true;

}

function removeFromArray(val, ar){
	s = String(ar)
	// remove if not first item (global search)
	reRemove = new RegExp(","+val,"g")
	s = s.replace(reRemove,"")
	// remove if at start of array
	reRemove = new RegExp("^"+val+",")
	s = s.replace(reRemove,"")
	// remove if only item
	reRemove = new RegExp("^"+val+"$")
	s = s.replace(reRemove,"")
	return new Array(s)
}

function removeTable(statusId) {

		url = '<%=contextRoot%>/activityStatusAdmin.do?delete=Yes';

		url = url + "&checkboxArray=" + document.forms['activityStatusAdminForm'].elements['checkboxArray'].value;


		if(document.forms['activityStatusAdminForm'].elements['statusCode'].value != ''){
			url = url + "&statusCodeStr=" + document.forms['activityStatusAdminForm'].elements['statusCode'].value;
		}
		if(document.forms['activityStatusAdminForm'].elements['descriptionType'].value != ''){
			url = url + "&descriptionType=" + document.forms['activityStatusAdminForm'].elements['descriptionType'].value;
		}
		if(document.forms['activityStatusAdminForm'].elements['active'].checked == true){
			url = url + "&active=Y" ;
		}else{
			url = url + "&active=N" ;
		}
		if(document.forms['activityStatusAdminForm'].elements['psaActive'].checked == true){
			url = url + "&psaActive=Y";
		}else{
			url = url + "&psaActive=N";
		}
		if(document.forms['activityStatusAdminForm'].elements['projectNameId'].value != '-1'){
			url = url + "&projectNameId=" + document.forms['activityStatusAdminForm'].elements['projectNameId'].value;
		}
		if(document.forms['activityStatusAdminForm'].elements['moduleId'].value != '-1'){
			url = url + "&moduleId=" + document.forms['activityStatusAdminForm'].elements['moduleId'].value;
		}
		xmlhttp.open('POST', url,true);
		xmlhttp.onreadystatechange = checkTableHandler;
    	xmlhttp.send(null);
	}
</SCRIPT>
<SCRIPT language="javascript" type="text/javascript">
//Start of Ajax functionality for Activity Status populating values to the fields

var xmlhttp;

	function CreateXmlHttp(str, editFlag){

	//Assigning Status Id to the hidden variable
	document.forms['activityStatusAdminForm'].elements['activityStatusId'].value = str;
	document.forms['activityStatusAdminForm'].elements['editFlg'].value = editFlag;

	//Creating object of XMLHTTP in IE
		var url="<%=contextRoot%>/activityStatusAdmin.do?id="+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		  }else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		if (xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ActivityStatus;
			xmlhttp.send(null);
		  }else{
		  alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ActivityStatus(){
	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){
		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){
		    var text=xmlhttp.responseText;
			text=text.split(',');
			document.forms['activityStatusAdminForm'].elements['statusCode'].value = text[0];

			if(document.forms['activityStatusAdminForm'].elements['editFlg'].value == 'N'){
				document.forms['activityStatusAdminForm'].elements['statusCode'].disabled=true;
			}else{
				document.forms['activityStatusAdminForm'].elements['statusCode'].disabled=false;
			}

			document.forms['activityStatusAdminForm'].elements['descriptionType'].value = text[1];

			if(text[2] == 'true'){
				document.forms['activityStatusAdminForm'].elements['active'].checked =true;
			}else{
				document.forms['activityStatusAdminForm'].elements['active'].checked =false;
			}

			if(text[3] == 'true'){
				document.forms['activityStatusAdminForm'].elements['psaActive'].checked =true;
			}else{
				document.forms['activityStatusAdminForm'].elements['psaActive'].checked =false;
			}

			document.forms['activityStatusAdminForm'].elements['projectNameId'].value = text[4];
			document.forms['activityStatusAdminForm'].elements['moduleId'].value = text[5];

			if(document.forms['activityStatusAdminForm'].elements['statusCode'].value == ""){
				document.forms['activityStatusAdminForm'].elements['descriptionType'].value = "";
				document.forms['activityStatusAdminForm'].elements['active'].checked = false;
				document.forms['activityStatusAdminForm'].elements['psaActive'].checked = false;
				document.forms['activityStatusAdminForm'].elements['projectNameId'].value = "";
				document.forms['activityStatusAdminForm'].elements['moduleId'].value = "";
			}
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}
//End Ajax functionality for Activity Status populating values to the fields
</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="test()">
<html:form focus="statusCode" action="" name="activityStatusAdminForm" type="elms.control.beans.admin.ActivityStatusAdminForm" >
<html:hidden property="activityStatusId"/>
<html:hidden property="checkboxArray" value=""/>
<html:hidden property="editFlg" value=""/>

<div id="saveDiv" align="justify" style="color:#088A08"></div>

<!-- Start of logic tag not to display /iterate values   -->

<logic:notEqual value="YES" property="displayActStatus" name="activityStatusAdminForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="40%" height="36px"><font class="con_hdr_3b">Activity Status Administration</font></td>
        </tr>
         <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <html:errors/>
                    <tr>
                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="Reset" value="Reset" styleClass="button" onclick="refresh();"/>
                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="return checkTable();" />
                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" width="320">Activity Status Code</td>
                      <td class="tabletext" width="168">
                        <html:text property="statusCode" size="50" maxlength="25" styleClass="textbox"/>&nbsp;
                      </td>
                      <td class="tablelabel" width="350">Description</td>
                      <td class="tabletext" width="258">
                        <html:text  property="descriptionType" size="50" maxlength="50" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
                    <tr>
						<td class="tablelabel" width="20%">Active</td>
						<td class="tabletext">
					    	<html:checkbox  property="active" styleClass="textbox">Yes</html:checkbox>
						</td>
						<td class="tablelabel" width="20%">PSA Active</td>
						<td class="tabletext">
					    	<html:checkbox  property="psaActive"  styleClass="textbox">Yes</html:checkbox>
						</td>
					</tr>
					<tr>
						<td class="tablelabel">Project Name</td>
                      	<td class="tabletext" colspan="1">
                        	<html:select property="projectNameId" styleClass="textbox">
                        	 	<html:option value="-1">Please Select</html:option>
								<html:options collection="pNameList" property="projectNameId" labelProperty="description"/>
                        	</html:select>
                        </td>
					  	<td class="tablelabel">Module</td>
							<td class="tabletext" colspan="3">
                      	<html:select property="moduleId" styleClass="textbox">
							<html:option value="-1">Please Select</html:option>
							<html:options collection="moduleList" property="id" labelProperty="description"/>
						</html:select>
						    </td>
				    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:notEqual>
<!-- end of logic tag -->

<div id="loader" style="padding-right:50%;"><span><img src="../images/loader.gif"></span></div>
<div id="tableDiv"></div>

</html:form>
</body>
</html:html>

<%@page import="elms.common.Constants"%>
<%@page import="elms.app.common.DisplayItem"%>
<%@page import="java.util.ArrayList,java.util.List"%>
<%@page import="elms.control.actions.ApplicationScope"%>

<%
int formId = 0;
String contextRoot = request.getContextPath();
List a = new ArrayList();
a = elms.agent.AdminAgent.getNoticeFieldList();
%>

<html>
<head>
	<title>Text</title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
	<meta http-equiv="x-ua-compatible" content="IE=Edge"/> 
	<link rel="stylesheet" href="<%=contextRoot%>/tools/jquery/jquery-ui.css" />
	<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
	<script src="<%=contextRoot%>/tools/jquery/jquery-migrate-1.2.1.js"></script>
	<script src="<%=contextRoot%>/tools/jquery/jquery-ui.js"></script>
	
<script type="text/javascript" >
$(function() {
	$(document).ready(function() {
		$( "#tabs" ).tabs();
	});
    
});

function save(){
	var $form = $('#editform');
    var $inputs = $form.find("input, select, button, textarea");
    var fieldname = $("#fname").val();
    
    if(fieldname == ""){
    	alert("Field Name is required");
    	return false;
    }
    
   // var f = feeconfig(feeamt,feebudget,feeaccount);
    $('#ui').val($.trim(fieldname));
    suc();
    //var serializedData = $form.serialize();
   
    
 
}

function suc(){
	var CKEDITOR   = window.parent.CKEDITOR;
	var oEditor   = CKEDITOR.instances.editorName;
	var c = $('#ui').val();
	var myEditor = CKEDITOR.instances.editor1;
	myEditor.insertHtml(c);
	CKEDITOR.dialog.getCurrent().hide();
}

</script>


</head>
<body>
<form action="#" id="editform" name="editform" >
	<div id="tabs">
	  <ul>
	    <li><a href="#tabs-1" >Fields</a></li>
	  </ul>
		<div id="tabs-1">
			<table cellpadding="4" cellspacing="8" border="0" width="100%">
					<tr>
						<td style="width:1%;">Type</td>
						<td>
							<select name="FIELD_NAME"  class="default" style="width:99%;" id="fname">
								<option value="">Please Select</option>
								<% 	for (int i = 0; i < a.size();  i++) {  
									String name =  ((DisplayItem)a.get(i)).getFieldOne();
									String value =  "#"+((DisplayItem)a.get(i)).getFieldOne()+"#"; 
								%>
									<option value="<%=value%>"><%=name%></option>
								<%} %>
							</select>
						</td>
					</tr>
					
			</table>
			
		</div>
			<input  type="hidden" id="ui" name="ui" value="0"/>
		
	</div>
</form>
</body>
</html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='java.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>

<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="../script/calendar.js"></script>
<script src="../script/yCodelib.js"> </script>
<script src="../script/yCode_combo_box.js"> </script>
<%!
List departments = new ArrayList(); // for the Department list
String flag = "";
%>

<%
String contextRoot = request.getContextPath();
String superAdminControl="";
javax.servlet.http.HttpSession obcSession = request.getSession();
elms.security.User test =(elms.security.User)obcSession.getAttribute(elms.common.Constants.USER_KEY);
String testUser = test.getUsername()!=null ? test.getUsername():"";
if(testUser.equalsIgnoreCase("edge"))  superAdminControl = "edge";
else superAdminControl = "";
String queryString="";
if(request.getQueryString()!=null){queryString=request.getQueryString();}
else{queryString="";}

departments = (List) request.getAttribute("departments");
%>
<script language="JavaScript">
<logic:notEqual value="YES" property="displayUserList" name="userForm">

function check() {
if(queryString!='lookup=Yes')
evalType();
}
var strValue;
var rights='edge';
function evalType() {
	document.forms['userForm'].elements['userName'].focus();
	var size= document.forms['userForm'].elements['userGroups'].length;
	var str = "";
	var delimiter = "";
	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['userGroups'].options[i].selected) {
			str += delimiter + document.forms[0].elements['userGroups'].options[i].text;
			delimiter = "\r";
		}
	}
	document.forms[0].elements['selectedUserGroups'].value=str;
}


function validateFunction() {

	strValue=false;
	var control= document.forms['userForm'].elements['superAdminControl'].value;

	 var txtUserName=document.forms['userForm'].elements['userName'].value;
	if(txtUserName.indexOf(' ')>-1){
		alert('Space is not allowed for UserName');
		document.forms['userForm'].elements['userName'].focus();
		strValue=false;
	}else if (document.forms['userForm'].elements['userName'].value == '') {
		alert("Enter User Name");
		document.forms['userForm'].elements['userName'].focus();
		strValue=false;
	}else if(document.forms['userForm'].elements['userName'].value == rights && control!=rights){
		document.forms['userForm'].elements['password'].disabled = 'true';
		strValue=false;
	}else if (document.forms['userForm'].elements['password'].value == ''){
		alert("Enter Password");
		document.forms['userForm'].elements['password'].focus();
		strValue=false;
	}else if (document.forms['userForm'].elements['firstName'].value == '') {
		alert("Enter First Name");
		document.forms['userForm'].elements['firstName'].focus();
		strValue=false;
	}else if (document.forms['userForm'].elements['lastName'].value == '') {
		alert("Enter Last Name");
		document.forms['userForm'].elements['lastName'].focus();
		strValue=false;
	}else if (document.forms['userForm'].elements['departmentId'].value == '') {
		alert("Select Department");
		document.forms['userForm'].elements['departmentId'].focus();
		strValue=false;
	}else{
	   	document.forms[0].action='<%=contextRoot%>/userAdmin.do?save=Yes';
		document.forms[0].submit();
    }
}
</logic:notEqual>

function lookup() {

    document.forms[0].action='<%=contextRoot%>/userAdmin.do?lookup=Yes';
    document.forms[0].submit();
}
function disableGroups() {
	var selectedRoleId = document.forms[0].role.value;
	//alert(selectedRoleId);
	if(selectedRoleId == 4){// view only user
		document.forms[0].userGroups.disabled=true;
		document.forms[0].selectedUserGroups.disabled=true;
	}
	else{
		document.forms[0].userGroups.disabled=false;
		document.forms[0].selectedUserGroups.disabled=true;
	}

}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
document.forms[0].action='<%=contextRoot%>/userAdmin.do?lookup=Yes';
document.forms[0].submit();
}

}

</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"
<logic:notEqual value="YES" property="displayUserList" name="userForm">
onload="evalType();"
</logic:notEqual>
>

<html:form name="userForm" type="elms.control.beans.admin.UserForm" action="/userAdmin">
<html:hidden property="userId"/>
<html:hidden property="superAdminControl" value="<%=superAdminControl%>"/>
<logic:notEqual value="YES" property="displayUserList" name="userForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">User Maintenance</font><br>
                <br>
                </td>
            </tr>
            <tr>
            <td>
	            <html:errors/>
	            <logic:present name="message" scope="request" >
		            <font class="green2b"><bean:write name="message"/></font><br>
	            </logic:present>
            </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <html:errors property="error"/>
		                    <tr>
		                      <td width="99%" class="tabletitle">Lookup / Add / Update </td>

		                        <td width="1%" class="tablebutton"><nobr>
								  <html:reset value="Reset" styleClass="button"/>
								  <html:submit property="Lookup" value="Lookup" styleClass="button" onclick="return lookup();"></html:submit>
		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">
                            <tr>
                                <td class="tablelabel">User Name</td>
                                <td class="tabletext" > <html:text size="20" property="userName" styleClass="textbox"/> </td>
                               <td class="tablelabel">Password</td>
                                <td class="tabletext" colspan="6"> <html:password size="20" property="password" styleClass="textbox"/> &nbsp;
                            </tr>

                            <tr>
                                <td class="tablelabel"><nobr>First Name</nobr></td>
                                <td class="tabletext"><html:text size="25" property="firstName" styleClass="textbox"/></td>
                                <td class="tablelabel">MI</td>
                                <td class="tabletext"><html:text size="10" maxlength="1" property="mi" styleClass="textbox"/></td>
                                <td class="tablelabel">Last Name</td>
                                <td class="tabletext"><html:text size="25" property="lastName" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel"><nobr>Employee Number</nobr></td>
                                <td class="tabletext"><html:text size="25" property="employeeNumber" styleClass="textbox"/></td>
                                <td class="tablelabel">Department</td>
                                <td class="tabletext" colspan="3">
                                   <html:select property="departmentId" styleClass="textbox">
										<html:option value=""> Please select</html:option>
										<html:options collection="departments" property="departmentId" labelProperty="description" />
									</html:select>
                                </td>
                            </tr>

                            <tr>
                                <td class="tablelabel">Title</td>
                                <td class="tabletext"><html:text size="25" property="title" styleClass="textbox"/></td>
                                <td class="tablelabel"> Role</td>
                                <td class="tabletext">
                                   <html:select property="role" styleClass="textbox" onchange="disableGroups();">
                        	          <html:option value="-1">Please Select</html:option>
                                      <html:options collection="roles" property="roleId" labelProperty="description" />
                                   </html:select>
                                </td>
                                <td class="tablelabel">Active</td>
                                <td class="tabletext"><html:checkbox property="active"/></td>
                            </tr>
							<tr>
								<td class="tablelabel" valign="top" height="25">Email</td>
                     			 <td class="tabletext" valign="top" height="25" colspan="5">
                     			 <html:text  property="userEmail" size="25" maxlength="50" styleClass="textbox"/>
                    		  </td>
							</tr>
                            <tr>
                                <td rowspan="2" class="tablelabel">Groups</td>
                                <td class="tabletext"><html:select property="userGroups" size="25" styleClass="textbox" multiple="multiple" onchange="evalType()">
                                <html:option value="">Administrative Staff</html:option>
                                <html:options collection="groups" property="groupId" labelProperty="name"/></html:select></td>
                                <td class="tabletext" colspan="4" valign="top"><textarea  name="selectedUserGroups" cols="40" rows="20"></textarea></td>
                            </tr>
                       </table>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</logic:notEqual>
<logic:equal value="YES" property="displayUserList" name="userForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">User List</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="95%" height="25" class="tabletitle">Users</td>

                      <td width="1%" class="tablebutton"><nobr>
                       <html:button  value="Back" property="Back" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                       <html:submit property="adduser" value="Add User" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td width="15%" class="tablelabel">User name</td>
                      <td width="25%" class="tablelabel">Name</td>
                      <td width="25%" class="tablelabel">Title</td>
                      <td width="25%" class="tablelabel">Department</td>
                      <td width="10%" class="tablelabel">Active</td>
                    </tr>
                    <logic:iterate id="user" property="users" name="userForm">
                      <tr>
                        <td class="tabletext" >

                            <a  href="<%=contextRoot%>/userAdmin.do?id=<bean:write name="user" property="fieldOne"/>" >
                               <bean:write name="user" property="fieldTwo" />
                            </a>

                        </td>
                        <td class="tabletext"><bean:write name="user" property="fieldThree" /></td>
                        <td class="tabletext"><bean:write name="user" property="fieldFour" /></td>
                        <td class="tabletext"><bean:write name="user" property="fieldFive" /></td>
                        <td class="tabletext"><bean:write name="user" property="fieldSix" /></td>
                      </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:equal>

</html:form>
</body>
</html:html>


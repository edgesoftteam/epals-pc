<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.agent.LookupAgent"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%@ page import="elms.app.finance.*,java.util.*" %>
<%
String action = (String) request.getAttribute("action");
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" type="text/javascript" src="../../tools/jquery/jquery.min.js"></script>
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
		String contextRoot = request.getContextPath();
		List activityTypes = (List) session.getAttribute("activityTypes");
		pageContext.setAttribute("activityTypes", activityTypes);

		List feeLookups = (List) request.getAttribute("feeLookups");
		pageContext.setAttribute("feeLookups", feeLookups);

		elms.control.beans.FeeScheduleForm feeFrm = (elms.control.beans.FeeScheduleForm)session.getAttribute("feeScheduleForm");
		elms.app.admin.FeeEdit[] feeEdit = feeFrm.getFeeEditList();
		int rows = 0;
		
		List activitySubTypes = new ArrayList();
		
		
		if(feeFrm.getFeeEditList().length>0){
			activitySubTypes = LookupAgent.getActivitySubTypes(feeFrm.getFeeEditList()[0].getActType());
			
		}
		
		
		pageContext.setAttribute("activitySubTypes", activitySubTypes);
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	ctr = document.feeScheduleForm.rowCount.value;
	
	strValue = true;
	for(i=1;i<=ctr;i++) {
		if (document.forms['feeScheduleForm'].elements[(i*24)-15].value != '') {
			strValue=validateData('date',document.forms['feeScheduleForm'].elements[(i*24)-15],'Invalid date format');
		}
    	if (strValue == true) {
			if (document.forms['feeScheduleForm'].elements[(i*24)-14].value != '') {
   				strValue=validateData('date',document.forms['feeScheduleForm'].elements[(i*24)-14],'Invalid date format');
   			}
   		}
    	if (strValue == false) {
    		return strValue;
    	}
    }
    return true;
}

function updatePage() {
	selectAllCheckBox();
	if (save() && validateFunction()) {
		document.forms[0].action='<%=contextRoot%>/saveFeesSchedule.do?action=Save';
 		document.forms[0].submit();
 	} else
 		return false;
}

function selectAllCheckBox(){
ctr = document.feeScheduleForm.rowCount.value;
var j=0;
for(i=1;i<=ctr;i++){
	
		if(document.forms['feeScheduleForm'].elements[(i*23)+0+j].checked) {
			document.forms['feeScheduleForm'].elements[(i*23)+1+j].value = "true";
		}else {
			document.forms['feeScheduleForm'].elements[(i*23)+1+j].value = "false";
		}
		
		document.forms['feeScheduleForm'].elements[(i*23)+1+j].value = document.forms['feeScheduleForm'].elements[(i*23)+0+j].checked;
		j=j+1;
	}
}

function selectAllCheckBoxForOnlineRenewal(){
	ctr = document.feeScheduleForm.rowCount.value;
	var j=0;
	for(i=1;i<=ctr;i++){
		
			if(document.forms['feeScheduleForm'].elements[(i*25)+0+j].checked) {
				document.forms['feeScheduleForm'].elements[(i*25)+1+j].value = "true";
			}else {
				document.forms['feeScheduleForm'].elements[(i*25)+1+j].value = "false";
			}
			
			document.forms['feeScheduleForm'].elements[(i*25)+1+j].value = document.forms['feeScheduleForm'].elements[(i*25)+0+j].checked;
			j=j+1;
		}
	}

function cancelPage() {
	document.forms[0].action='<%=contextRoot%>/saveFeesSchedule.do?action=Cancel';
 	document.forms[0].submit();
}

function visibleFalse(){
ctr = document.feeScheduleForm.rowCount.value;
 var j=0;
for(i=1;i<=ctr;i++){
		document.forms['feeScheduleForm'].elements[(i*20)+3+j].style.display="none";
		j=j+1;
	}
}

function save()
{
ctr = document.feeScheduleForm.rowCount.value;

for(i=1;i<=ctr;i++) {
		
	
	
		if (document.forms['feeScheduleForm'].elements[(i*24)-21].value == "") {
			alert("Please enter Sequence");
			document.forms['feeScheduleForm'].elements[(i*24)-21].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-20].value == "") {
			alert("Please enter Fee Description");
			document.forms['feeScheduleForm'].elements[(i*24)-20].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-17].value == "") {
			alert("Please Select Activity Type");
			document.forms['feeScheduleForm'].elements[(i*24)-17].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-15].value == "") {
			alert("Please Select Creation Date");
			document.forms['feeScheduleForm'].elements[(i*24)-15].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-11].value == "") {
			alert("Please Select Input");
			document.forms['feeScheduleForm'].elements[(i*24)-11].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-9].value == "") {
			alert("Please Select Formula");
			document.forms['feeScheduleForm'].elements[(i*24)-9].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-6].value == "") {
			alert("Please Select Payment");
			document.forms['feeScheduleForm'].elements[(i*24)-6].focus();
			return false;
		}else if (document.forms['feeScheduleForm'].elements[(i*24)-4].value == "") {
			alert("Please Select Account Code");
			document.forms['feeScheduleForm'].elements[(i*24)-4].focus();
			return false;
		}
    }

	return true;
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}

// draw back of ajax is in edit/duplicate for online TODO 
function onchangeprocess()
{
	
var a = "<%=action%>";	
ctr = document.feeScheduleForm.rowCount.value;

for(i=1;i<=ctr;i++) {
    if(document.forms['feeScheduleForm'].elements[(i*24)-17].value == "dummy"){
			alert("Choose the proper activity type");
			document.forms['feeScheduleForm'].elements[(i*24)-17].value="";
			return false;
	}
    
    if(a=="Add"){
      var acttype = document.forms['feeScheduleForm'].elements[(i*24)-17].value;
	    $.post("<%=contextRoot%>/editFeesSchedule.do?method=subtypes&actType="+acttype, function(data){
		  	if(data.length>0) {
	      	     $('#activitySubTypes').empty();
	      	 	  $('#activitySubTypes').append(data);
	     		}
		});
    }
    
}
    return true;
}


function onchangeprocesssubtype()
{
document.forms['feeScheduleForm'].elements[25].value = $('#activitySubTypes').val();
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<html:form action="/saveFeesSchedule.do">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Fee Schedule Maintenance ( <%=action%> )</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%"><%=action%> Fee Schedule</td>

                        <td width="1%" class="tablebutton" align="right"><nobr>
                          <html:reset value="Reset" styleClass="button"/>
                          <html:button property="Cancel" value="Cancel" styleClass="button" onclick="return cancelPage();" />
			  			  <html:button property="Update" value="Update" styleClass="button" onclick="return updatePage();" />
                          </nobr>
    		      		</td>
 	            	</tr>
                   </table>
                </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
 				 <nested:iterate property="feeEditList">
<%
	rows++;

%>

 				 <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td colspan="3" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                 </table>
                 <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr class="tabletext">
                      <td class="tablelabel">Sequence <font color="red">*</font>&nbsp;</td>
                      <td class="tabletext"><nested:text property="sequence" styleClass="textboxm" size="4" onkeypress="return validateDecimal();" /></td>
                      <td class="tablelabel">Fee Description <font color="red">*</font>&nbsp;</td>
                      <td class="tabletext"><nested:text property="description" styleClass="textbox" size="30" maxlength="100" onblur="return ValidateBlankSpace(this);"/></td>
                      <td class="tablelabel">Fee/Unit</td>
                      <td class="tabletext"><nested:text property="feeFactor" styleClass="textboxm" size="10" onkeypress="return validateDecimal();" /></td>
                    </tr>
                    <tr class="tabletext">
                      <td class="tablelabel">SubTotal Level</td>
                      <td class="tabletext"><nested:select property="subtotalLevel" styleClass="textbox">
                      		<html:option value=""></html:option>
                      		<html:option value="0">0</html:option>
                      		<html:option value="1">1</html:option>
                      		<html:option value="2">2</html:option>
                      		<html:option value="3">3</html:option>
                      		<html:option value="4">4</html:option>
                      		<html:option value="5">5</html:option>
                          </nested:select>
                      </td>
                      <td class="tablelabel">Activity Type <font color="red">*</font>&nbsp;</td>
                      <td class="tabletext">
                      	  <nested:select property="actType" styleClass="textbox" onchange="return onchangeprocess()">
                      	  <html:option value="">Please Select</html:option>
			              <html:options collection="activityTypes" property="type" labelProperty="description"/>
			              </nested:select>
			          </td>
			          <td class="tablelabel">Factor</td>
                      <td class="tabletext"><nested:text property="factor" styleClass="textboxm" size="10" onkeypress="return validateDecimal();"/></td>
                    </tr>
                    <tr class="tabletext">
                      <td class="tablelabel">Creation Date <font color="red">*</font>&nbsp;</td>
                      <td class="tabletext"><nested:text property="creationDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();"/>
                       <A href="javascript:show_calendar('feeScheduleForm.elements[(<%=rows%>*21)-12]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
                        <IMG src="../images/calendar.gif" width="16" height="15" border="0">
                       </A>
                      </td>
                      <td class="tablelabel">Expiration Date</td>
                      <td class="tabletext"><nested:text property="expirationDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" readonly="true"/>

                      </td>
                      <td class="tablelabel">Print</td>
                      <td class="tabletext">
                          <nested:select property="flagTwo" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="N">Do Not Print</html:option>
                            <html:option value="1">Print When > 0</html:option>
                            <html:option value="Y">Always Print</html:option>
                          </nested:select>
                      </td>
					</tr>
                    <tr class="tabletext">
                      <td class="tablelabel">Required</td>
                      <td class="tabletext">
                          <nested:select property="flagOne" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="1">For Plan Check</html:option>
                            <html:option value="2">For Permit</html:option>
                            <html:option value="3">For Permit and PC</html:option>
                            <html:option value="4">For Permit, if PC Required</html:option>
							<html:option value="5">For Development Fee</html:option>
                            <html:option value="6">For Penalty</html:option>
                            <html:option value="0">Not Required</html:option>
                            <html:option value="7">For Permit Online</html:option>
                          </nested:select>
                      </td>
                      <td class="tablelabel">Input <font color="red">*</font>&nbsp;</td>
                      <td class="tabletext"><nested:select property="inputFlag" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="X">Invisible Supplemental Fee</html:option>
                            <html:option value="0">No Input Allowed</html:option>
                            <html:option value="I">Integer Units</html:option>
                            <html:option value="C">Check Mark</html:option>
                            <html:option value="D">Decimal Units</html:option>
                            <html:option value="M">Dollar Amount</html:option>
                            <html:option value="N">Number Dwellings</html:option>
                           </nested:select>
                      </td>
                      <td class="tablelabel">Enabled</td>
                      <td class="tabletext">
                          <nested:select property="type" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="B">Always</html:option>
                            <html:option value="P">Permit Mode Only</html:option>
                            <html:option value="X">Never</html:option>
                          </nested:select>
                      </td>
                    </tr>
                    <tr class="tabletext">
                      <td class="tablelabel">Formula <font color="red">*</font>&nbsp;</td>
                      <td  class="tabletext" colspan="3">
                          <nested:select property="feeCalcOne" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="A">Units * Fee/Unit</html:option>
                            <html:option value="B">Units * Fee/Unit + Factor</html:option>
                            <html:option value="C">Factor (Flat Fee)</html:option>
                            <html:option value="D">Max(Units * Fee/Unit, Factor)</html:option>
                            <html:option value="E">Min(Units * Fee/Unit, Factor)</html:option>
                            <html:option value="F">Units * (If(factor>0,factor,1))</html:option>
                            <html:option value="G">$ Amount</html:option>
                            <html:option value="H">Valuation * Fee/Unit</html:option>
                            <html:option value="I">Valuation * (If(factor>0,factor,1))</html:option>
                            <html:option value="J">If(Init>0,Subtotal(Init),SubTotal(subt)*Factor)</html:option>
                            <html:option value="Q">Lookup Result(Valuation)</html:option>
							<html:option value="R">Max (Factor , fee factor * valuation)</html:option>
                            <html:option value="S">RoundUp(Lookup Result(Units)*If(Factor>0,Factor,1)</html:option>
                            <html:option value="T">RoundDown(Lookup Result(Units)*If(Factor>0,Factor,1)</html:option>
                            <html:option value="U">Max(If(Init>0,subtotal(Init),subtotal(Subt)),factor)</html:option>
                            <html:option value="V">Min(if(init>0,subtotal[init]subtotal[subt]),factor)</html:option>
                            <html:option value="W">Lookup Result(if(init > 0,subtotal[init],subtotal[subtotal]) * factor</html:option>
                            <html:option value="Y">Max(Subtotal[subt] * fee/unit,factor)</html:option>
                            <html:option value="y">Max(Subtotal[init] * fee/unit,factor)</html:option>
                            <html:option value="a">Max(Subtotal[init] * units,factor)</html:option>
                            <html:option value="b">Valuation * Fee</html:option>
							<html:option value="c">(Fee/Unit * (Units/100)) + Factor</html:option>
							<html:option value="d">((Units * Fee/Unit) + Factor) * 0.75</html:option>
							<html:option value="e">((Units * Fee/Unit) + Factor) * 0.50</html:option>
							<html:option value="f">((Units * Fee/Unit) + Factor) * 0.25</html:option>
							<html:option value="g">Total1 = Sum of all Permit/License Fees</html:option>
							<html:option value="h">RoundUp(Lookup Result(Units*Fee/Unit))</html:option>
							<html:option value="i">Lookup Result(Units)</html:option>
                          </nested:select>
                      </td>
                      <td class="tablelabel">Calculate</td>
                      <td class="tabletext">
                          <nested:select property="feeCalcTwo" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="1">No Restrictions</html:option>
                            <html:option value="3">Unpaid Only</html:option>
                            <html:option value="4">PC Status Not Final</html:option>
                          </nested:select>
                      </td>
                    </tr>
                    <tr class="tabletext">
                      <td class="tablelabel">Lookup</td>
                      <td class="tabletext">
                          <nested:select property="feeLookup" styleClass="textbox">
                            <option value=""></option>
                            <html:options collection="feeLookups" property="lookupFeeId" labelProperty="lookupFee"/>
                          </nested:select>
                      </td>
                      <td class="tablelabel">Payment <font color="red">*</font>&nbsp;</td>
                      <td class="tabletext" colspan="4">
                          <nested:select property="flagFour" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="0">No Payment</html:option>
                            <html:option value="1">Apply to PC</html:option>
                            <html:option value="2">Apply to Permit</html:option>
							<html:option value="3">Apply to Development Fee</html:option>
							 <html:option value="4">Apply to Penalty</html:option>
                          </nested:select>
                      </td>
                     
                   
                    </tr>
                    
                    
                     <tr class="tabletext">
                      <td class="tablelabel">Account</td>
                    	<td class="tabletext">
                          <nested:text property="account" styleClass="textboxm" size="18"/>
                      	</td>
                      <td class="tablelabel">Account Code <font color="red">*</font>&nbsp;</td>
                     <td class="tabletext">
                          <nested:text property="accountCode" styleClass="textboxm" size="18"/>
                      	</td>
                      <td class="tablelabel">Tax</td>
                      <td class="tabletext">
                          <nested:select property="taxFlag" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="0">Regular Fee</html:option>
                            <html:option value="1">Tax</html:option>
                          </nested:select>
                      </td>
                    </tr>
                    
                    
                    <tr class="tabletext">
                      <td class="tablelabel">Comments</td>
                      <td  class="tabletext" colspan=3>
                          <nested:text property="miscellaneous" styleClass="textbox" size="80" maxlength="80"/>
                      </td>
                      <td class="tablelabel">Renewable</td>
                      <td  class="tabletext" >
						  <nested:checkbox  property="renewable" onclick="selectAllCheckBox()"/>
						  <nested:hidden property="renewableFlag"/>
                      </td>
                     </tr>
                     
                     <tr class="tabletext">
                      <td class="tablelabel">Online Renewable</td>
                      <td  class="tabletext" >
						  <nested:checkbox  property="onlineRenewalbleFlag" onclick="selectAllCheckBoxForOnlineRenewal()"/>
						  <nested:hidden property="onlineRenewalbleFlag"/>
                      </td>
                     </tr>
                     
                   
                     <tr class="tabletext">
                      <td class="tabletitle" colspan="8"><br>ONLINE</td>
                     </tr>
                     
                     
                    <tr class="tabletext">
                     	<td class="tablelabel">Activity Subtype</td>
	                      <td class="tabletext">
		                  
		                  <%if(action.equalsIgnoreCase("edit")){ %>
		                       <nested:select property="activitySubtypeId"  styleClass="textbox" >
	                      	  <html:option value="">Please Select</html:option>
				              <html:options collection="activitySubTypes" property="id" labelProperty="description"/>
				              </nested:select>
				           <%} else { %>   
				               <nested:hidden property="activitySubtypeId" />
				               <select id="activitySubTypes" name="activitySubTypes" class="textbox" onchange="return onchangeprocesssubtype();">
				                 <option value="">Please Select</option>
				               </select>
				            <%}  %>
	                      </td>
	                      
	                      <td class="tablelabel">Online Input</td>
	                      <td class="tabletext" colspan="4">
	                          <nested:select property="onlineInput" styleClass="textbox">
	                            <html:option value="0">Please Select</html:option>
	                            <html:option value="1">Square Footage</html:option>
	                            <html:option value="2">No of Dwelling Units</html:option>
	                            <html:option value="3">No of Floors</html:option>
	                            <html:option value="4">Valuation</html:option>
	                          </nested:select>
	                      </td>
	                      
	                   
                     </tr>
                  </table>
				</nested:iterate>
                </td>
              </tr>
            </table>
<%
	String rowCounter = "" + rows;
%>
            <html:hidden property = "rowCount" value="<%=rowCounter%>" />
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
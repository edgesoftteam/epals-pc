<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<%@ page import="elms.common.*,java.util.*,elms.security.*"%>

<html:html>
<style type="text/css">
#menu1 {cursor: hand}
#menu1a {display: none}
#menu1a1 {display: none}
#menu1a11 {display: none}
#menu1a12 {display: none}
#menu1a2 {display: none}
#menu1a3 {display: none}
#menu1a4 {display: none}
#menu1b {cursor: hand}
#menu1b1 {display: none}
#menu1b2 {display: none}
#menu1b3 {display: none}
#menu1c {display: none}
#menu1c1 {display: none}
#menu1c2 {display: none}
#menu1c3 {display: none}
#menu2 {cursor: hand}
#menu2a {display: none}
#menu2a1 {display: none}
#menu2a2 {display: none}
#menu1d1 {display: none}
#menu1e1 {display: none}
#menu1f1 {display: none}
#menu1g1 {display: none}
#menu1h9a{display: none}
#menu1h1 {display: none}
#menu1i1 {display: none}
#menu1j1 {display: none}
#menu1k1 {display: none}
#menu1h11a{display: none}
</style>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">

</head>

<%
   boolean superAdmin= false;
   String contextRoot = request.getContextPath();
   User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
   String username = user.getUsername()!=null ? user.getUsername():"";
   if(username.equalsIgnoreCase("edge")) superAdmin = true;
%>

<%
   boolean businessLicenseUser = false, businessLicenseApproval = false,assessorDataMaintenance=false, conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false,streetListTech=false,inspectionsOnline =false,onlinePermit =false,customLabels=false;
   List groups = (java.util.List) session.getAttribute("groups");
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true;
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;  customLabels = true;
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
        if(group.groupId == Constants.GROUPS_ASSESSOR_DATA_MAINTENANCE ) assessorDataMaintenance = true;
        if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
        if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
    	if(group.groupId == Constants.GROUPS_INSPECTION_MANAGER ){ inspectionsOnline = true;   } onlinePermit = true;
   }
%>

<script>
function dohref(strHref)
{
	//Fee Maintenance
   if (strHref == 'feeLookup')      parent.f_content.location.href="<%=contextRoot%>/maintainFeesLookup.do";
   else if (strHref == 'feeSchedule') parent.f_content.location.href="<%=contextRoot%>/maintainFeesSchedule.do";
   else if (strHref == 'feeInitialize')  parent.f_content.location.href="<%=contextRoot%>/feesInitialize.do";
   else if (strHref == 'contractorTax')  parent.f_content.location.href="<%=contextRoot%>/maintainContractorTax.do";
   else if (strHref == 'postRevenue') parent.f_content.location.href="<%=contextRoot%>/postRevenue.do";
	//Address Maintenance
   else if (strHref == 'addLand') parent.f_content.location.href="<%=contextRoot%>/addLand.do";
   else if (strHref == 'moveAddress')  parent.f_content.location.href="<%=contextRoot%>/moveLsoAdv.do";
   else if (strHref == 'streetList') parent.f_content.location.href="<%=contextRoot%>/streetListAdmin.do";
   else if (strHref == 'assessorImport') parent.f_content.location.href="<%=contextRoot%>/assessorImport.do?action=showStatus";
   else if (strHref == 'assessorExport') parent.f_content.location.href="<%=contextRoot%>/jsp/admin/assessorExport.jsp";
	//people maintenance
   else if (strHref == 'people')  parent.f_content.location.href="<%=contextRoot%>/searchPeople.do?fromAdmin=true";
	//Project Maintenance
   else if (strHref == 'subProjectName') parent.f_content.location.href="<%=contextRoot%>/subProjectNameAdmin.do";
   else if (strHref == 'subProjectType') parent.f_content.location.href="<%=contextRoot%>/subProjectTypeAdmin.do";
   else if (strHref == 'subProjectSubType') parent.f_content.location.href="<%=contextRoot%>/subProjectSubTypeAdmin.do";
   else if (strHref == 'activityType') parent.f_content.location.href="<%=contextRoot%>/activityTypeAdmin.do";
   else if (strHref == 'activitySubType') parent.f_content.location.href="<%=contextRoot%>/activitySubTypeAdmin.do";
   else if (strHref == 'activityStatus') parent.f_content.location.href="<%=contextRoot%>/activityStatusAdmin.do";
   else if (strHref == 'activityMapping') parent.f_content.location.href="<%=contextRoot%>/activityTypeMapping.do";
   else if (strHref == 'activityStatus') parent.f_content.location.href="<%=contextRoot%>/activityStatusAdmin.do";
   else if (strHref == 'moveProject')  parent.f_content.location.href="<%=contextRoot%>/moveProject.do";
   else if (strHref == 'addressType')  parent.f_content.location.href="<%=contextRoot%>/addressTypeAdmin.do";
   else if (strHref == 'noticeType')  parent.f_content.location.href="<%=contextRoot%>/noticeTypeAdmin.do";
   else if (strHref == 'system') parent.f_content.location.href="<%=contextRoot%>/lookupSystemAdmin.do?firstTime=yes";
   else if (strHref == 'pcs') parent.f_content.location.href="<%=contextRoot%>/pcStatusAdmin.do";
   else if (strHref == 'inspi') parent.f_content.location.href="<%=contextRoot%>/inspectionItemAdmin.do?firstTime=yes";
   else if (strHref == 'inspc') parent.f_content.location.href="<%=contextRoot%>/inspectionCodeAdmin.do?firstTime=yes";
   
	//Conditions Maintenance
   else if (strHref == 'conditionsLibrary') parent.f_content.location.href="<%=contextRoot%>/conditionsLibAdmin.do";
   else if (strHref == 'conditionsInitialize') parent.f_content.location.href="<%=contextRoot%>/conditionsInitialize.do";
   // Finance tasks
   else if (strHref == 'postRevenue') parent.f_content.location.href="<%=contextRoot%>/postRevenue.do";
	//user Maintenance
   else if (strHref == 'addUser')  parent.f_content.location.href="<%=contextRoot%>/userAdmin.do";
	//Online Permit
   else if (strHref == 'onlinePermit')  parent.f_content.location.href="<%=contextRoot%>/onlinePermit.do";
   else if (strHref == 'onlineQuestionaire')  parent.f_content.location.href="<%=contextRoot%>/onlineQuestionaire.do";
   else if (strHref == 'onlineAcknowledgement')  parent.f_content.location.href="<%=contextRoot%>/onlineAcknowledgement.do";
   else if (strHref == 'onlineloadqa')  parent.f_content.location.href="<%=contextRoot%>/onlineloadqa.do";
	//Online Inspections
   else if (strHref == 'holidayEditor') parent.f_content.location.href="<%=contextRoot%>/holidayEditor.do?occurance=Y";
   else if (strHref == 'limitPerDay') parent.f_content.location.href="<%=contextRoot%>/limitPerDay.do";
   else if (strHref == 'cutoffTimes') parent.f_content.location.href="<%=contextRoot%>/cutoffTimes.do";
   
   //Email Templates
   else if (strHref == 'emailType') parent.f_content.location.href="<%=contextRoot%>/emailTemplateTypeAdmin.do";
   else if (strHref == 'emailAdmin') parent.f_content.location.href="<%=contextRoot%>/emailTemplateAdmin.do";
	//System Maintenance
   else if (strHref == 'reloadApplication') parent.f_content.location.href="<%=contextRoot%>/reloadApplication.do";
   else if (strHref == 'yearEnd') parent.f_content.location.href="<%=contextRoot%>/yearEndMaintenance.do?from=menu";
   else if (strHref == 'digitalLibraryLogs') parent.f_content.location.href="<%=contextRoot%>/digitalLibraryLogs.do";
   // super admin tasks
   else if (strHref == 'addressImport') parent.f_content.location.href="<%=contextRoot%>/lsoImport.do?action=showStatus";
   else if (strHref == 'fsb') parent.f_content.location.href="<%=contextRoot%>/docs/fsb.jsp";
   else if (strHref == 'sql') parent.f_content.location.href="<%=contextRoot%>/docs/sql.jsp";
   else if (strHref == 'changePassword') parent.f_content.location.href="<%=contextRoot%>/jsp/common/changePassword.jsp"; //sunil
   else if (strHref == 'customLabels') parent.f_content.location.href="<%=contextRoot%>/createCustomLabels.do"; //sunil
   else if (strHref == 'reloadApplication') parent.f_content.location.href="<%=contextRoot%>/reloadApplication.do";
   //Notice templates admin
   else if (strHref == 'noticeTemplateAdmin') parent.f_content.location.href="<%=contextRoot%>/noticeTemplateAdmin.do?action=display";//NoticeTemplate added by Manju
   else if (strHref == 'noticeTemplateFieldsAdmin') parent.f_content.location.href="<%=contextRoot%>/noticeTemplateFieldsAdmin.do?action=display";//NoticeTemplateFields added by Manju
   //attachment types
   else if (strHref == 'attachmentTypeAdmin') parent.f_content.location.href="<%=contextRoot%>/attachmentTypeAdmin.do";
   else if (strHref == 'attachmentTypeMapping') parent.f_content.location.href="<%=contextRoot%>/attachmentTypeMapping.do";
   
   else if (strHref == 'sairaAttachment') parent.f_content.location.href="<%=contextRoot%>/jsp/gsearch/admin/sairaAttachment.jsp";
   else if (strHref == 'agentMaintenance') parent.f_content.location.href="<%=contextRoot%>/agentMaintenanceAdmin.do";
   else if (strHref == 'transactions') parent.f_content.location.href="<%=contextRoot%>/manageTransactions.do";
}
</script>

<body bgcolor="#e5e5e5" text="#000000" background="images/site_bg_e5e5e5.jpg" style="margin:0; overflow-x:auto; overflow-y:auto;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="10" height="8"><font class="black2b">Admin Explorer</td>
  </tr>
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
			<td class="treetext">
				<div class="adminMenuContainer">

						<%if(projectTech) {%>
							<div class="sub_menu" onclick="if(document.all.menu1a1.style.display =='block'){document.all.menu1a1.style.display='none'; document['plusminus1'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1a1.style.display='block'; document['plusminus1'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus1" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Project Maintenance">Project Maintenance</html:link></div>
								<ul id="menu1a1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('subProjectName')"  styleClass="tree1" title="Sub Project Name" >Sub Project Name</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('subProjectType')"  styleClass="tree1" title="Sub Project Type" >Sub Project Type</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('subProjectSubType')"  styleClass="tree1" title="Sub Project Sub Type" >Sub Project Sub Type</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('activityType')"  styleClass="tree1" title="Activity Type">Activity Type</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('activitySubType')"  styleClass="tree1" title="Activity Sub Type">Activity Sub Type</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('activityMapping')"  styleClass="tree1" title="Activity Mapping">Activity Mapping</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('activityStatus')"  styleClass="tree1" title="Activity Status">Activity Status</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('moveProject')"  styleClass="tree1" title="Move Project">Move Project</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('addressType')"  styleClass="tree1" title="Address Type">Address Type</html:link></li>
								    <li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('system')"  styleClass="tree1" title="Application Configuration">Application Configuration</html:link></li>
								    <li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('pcs')"  styleClass="tree1" title="PC Status Maintenance">Plan Check Status </html:link></li>
								    <li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('inspi')"  styleClass="tree1" title="Inspection Item">Inspection Item </html:link></li>
								    <li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('inspc')"  styleClass="tree1" title="Inspection Code">Inspection Code </html:link></li>
					  
								</ul>
						<%}%>
						<%if(peopleTech) {%>
							<div class="sub_menu" onclick="if(document.all.menu1a11.style.display =='block'){document.all.menu1a11.style.display='none'; document['plusminus11'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1a11.style.display='block'; document['plusminus11'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus11" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Project Maintenance">People Maintenance</html:link></div>
								<ul id="menu1a11">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('people')"  styleClass="tree1" title="People Manager">Add / Edit People</html:link></li>
								</ul>
						<%}%>
						<%if(conditionTech) {%>
							<div class="sub_menu" onclick="if(document.all.menu1a12.style.display =='block'){document.all.menu1a12.style.display='none'; document['plusminus12'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1a12.style.display='block'; document['plusminus12'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus12" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Project Maintenance">Conditions Maintenance</html:link></div>
								<ul id="menu1a12">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('conditionsLibrary')"  styleClass="tree1" title="Condition Library">Condition Library</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('conditionsInitialize')"  styleClass="tree1" title="Condition Initialize">Condition Initialize</html:link></li>
								</ul>
						<%}%>
						<%if(feesTech) {%>
							<div class="sub_menu" onclick="if(document.all.menu1b1.style.display =='block'){document.all.menu1b1.style.display='none'; document['plusminus2'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1b1.style.display='block'; document['plusminus2'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus2" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Fees Maintenance">Fees Maintenance</html:link></div>
								<ul id="menu1b1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('feeLookup')"  styleClass="tree1" title="Fee Lookup">Fee Lookup</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('feeSchedule')"  styleClass="tree1" title="Fee Maintenance">Fee Schedule</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('feeInitialize')" styleClass="tree1" title="Fee Intialize">Fee Initialize</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('contractorTax')"  styleClass="tree1" title="Contractor Tax">Contractor Tax</html:link></li>
								</ul>
						<%}%>
						<%if(addressTech) {%>
							<div class="sub_menu" onclick="if(document.all.menu1c1.style.display =='block'){document.all.menu1c1.style.display='none'; document['plusminus3'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1c1.style.display='block'; document['plusminus3'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus3" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Address Maintenance">Address Maintenance</html:link></div>
								<ul id="menu1c1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('addLand')"  styleClass="tree1" title="Add Land">Add Land</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('moveAddress')"  styleClass="tree1" title="Move Address">Move Address</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('streetList')"  styleClass="tree1" title="Street List">Streets</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('reloadApplication')"  styleClass="tree1" title="Street List">Reload Streets</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('assessorImport')"  styleClass="tree1" title="Assessor Import">Assessor Import</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('assessorExport')"  styleClass="tree1" title="Assessor Export">Assessor Export</html:link></li>
								</ul>

						<%}%>
						
							<div class="sub_menu" onclick="if(document.all.menu1d1.style.display =='block'){document.all.menu1d1.style.display='none'; document['plusminus4'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1d1.style.display='block'; document['plusminus4'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus4" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="User Maintenance">User Maintenance</html:link></div>
							<ul id="menu1d1">
							<%if(userTech){%>
								<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('addUser')"  styleClass="tree1" title="Add User">Add / Edit Users</html:link></li>
							<%}%>
								<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('agentMaintenance')"  styleClass="tree1" title="Add User">Agent Maintenance</html:link></li>
							</ul>
						<%if(projectTech) {%>
							<div class="sub_menu" onclick="if(document.all.menu1f1.style.display =='block'){document.all.menu1f1.style.display='none'; document['plusminus6'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1f1.style.display='block'; document['plusminus6'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus6" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Online Inspections">Online Inspections</html:link></div>
								<ul id="menu1f1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('holidayEditor')"  styleClass="tree1" title="Holiday Editor">Holiday Editor</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('limitPerDay')"  styleClass="tree1" title="Limit Per Day">Limit Per Day</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('cutoffTimes')"  styleClass="tree1" title="Cut Off Times">Cut Off Times</html:link></li>
								</ul>

							<div class="sub_menu" onclick="if(document.all.menu1g1.style.display =='block'){document.all.menu1g1.style.display='none'; document['plusminus7'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1g1.style.display='block'; document['plusminus7'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus7" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Online Permit">Online Permit</html:link></div>
								<ul id="menu1g1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('onlinePermit')"  styleClass="tree1" title="Online Fee Mapping">Online Fee Mapping</html:link></li>
									
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('onlineQuestionaire')"  styleClass="tree1" title="Online Questionaire Mapping">Online Questionaire Mapping</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('onlineAcknowledgement')"  styleClass="tree1" title="Online Acknowledgement Mapping">Online Acknowledgement</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('onlineloadqa')"  styleClass="tree1" title="Load Questions/Acknowledgment">Load Q /A </html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('transactions')"  styleClass="tree1" title="Transactions"> Transactions</html:link></li>
									
								</ul>

							<%if(customLabels){%>
							<div class="sub_menu" onclick="if(document.all.menu1h1.style.display =='block'){document.all.menu1h1.style.display='none'; document['plusminus8'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1h1.style.display='block'; document['plusminus8'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus8" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Online Permit">Custom Fields</html:link></div>
								<ul id="menu1h1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('customLabels')"  styleClass="tree1" title="Custom Fields">Fields</html:link></li>
								</ul>
							<%}%>

							<div class="sub_menu" onclick="if(document.all.menu1i1.style.display =='block'){document.all.menu1i1.style.display='none'; document['plusminus9'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1i1.style.display='block'; document['plusminus9'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus9" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Online Permit">Email Templates</html:link></div>
								<ul id="menu1i1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('emailType')"  styleClass="tree1" title="Types"> Types</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('emailAdmin')"  styleClass="tree1" title="Email Templates"> Templates</html:link></li>
								</ul>
						<% } %>
                              <!--Notices template admin creation menu -->
	                        <div class="sub_menu" onclick="if(document.all.menu1h9a.style.display =='block'){document.all.menu1h9a.style.display='none'; document['plusminus9a'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1h9a.style.display='block'; document['plusminus9a'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus9a" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Notice Template">Notices</html:link></div>
									<ul id="menu1h9a">
										<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('noticeType')"  styleClass="tree1" title="Notice Type"> Types</html:link></li>
									   <li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('noticeTemplateAdmin')"  styleClass="tree1" title="Notices Template"> Templates</html:link></li>
									</ul>
									
							<div class="sub_menu" onclick="if(document.all.menu1j1.style.display =='block'){document.all.menu1j1.style.display='none'; document['plusminus10'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1j1.style.display='block'; document['plusminus10'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus10" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Online Permit">My Account</html:link></div>
								<ul id="menu1j1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('changePassword')"  styleClass="tree1" title="Change Password">Change Password</html:link></li>
								</ul>

						<%if(superAdmin){%>
							<div class="sub_menu" onclick="if(document.all.menu1e1.style.display =='block'){document.all.menu1e1.style.display='none'; document['plusminus5'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1e1.style.display='block'; document['plusminus5'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus5" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Super Admin">Super Admin</html:link></div>
								<ul id="menu1e1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('addressImport')"  styleClass="tree1" title="Address Import">Address Import</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('fsb')"  styleClass="tree1" title="FSB">FSB</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('sql')"  styleClass="tree1" title="SQL Editor">SQL Editor</html:link></li>

								</ul>

						<%}%>
						<div class="sub_menu" onclick="if(document.all.menu1k1.style.display =='block'){document.all.menu1k1.style.display='none'; document['plusminus10'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1k1.style.display='block'; document['plusminus10'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus10" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Attachment Types">Attachments</html:link></div>
								<ul id="menu1k1">
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('attachmentTypeAdmin')"  styleClass="tree1" title="Attachment Types"> Types</html:link></li>
									<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('attachmentTypeMapping')"  styleClass="tree1" title="Attachment Reference Mapping"> Types Mapping</html:link></li>
								</ul>
						
						<div class="sub_menu" onclick="if(document.all.menu1h11a.style.display =='block'){document.all.menu1h11a.style.display='none'; document['plusminus11a'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1h11a.style.display='block'; document['plusminus11a'].src='images/site_blt_minus_003366.gif'}"><img name="plusminus11a" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#"  styleClass="tree1" title="Saira Attachments">SAIRA Attachments</html:link></div>
									<ul id="menu1h11a">
										<li><img src="images/right_arrow.gif">&nbsp;<html:link href="javascript:dohref('sairaAttachment')"  styleClass="tree1" title="Import Attachments"> Import Attachments</html:link></li>
									   
									</ul>
					</div>
				</td>
			</tr>
		</table>
	</td>
</tr>
</table>
</body>
</html:html>
<%@ page import="elms.agent.*" %>

<%@page import="java.util.*,elms.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<app:checkLogon/>
<%@ page import="sun.jdbc.rowset.*"%>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Enterprise Permitting and Licensing System : Add Deposit</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>

<% String contextRoot = request.getContextPath();
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	//strValue=false;
    if (document.forms['paymentMgrForm'].elements['amount'].value == '')
    {
    	alert('Enter Amount');
    	document.forms['paymentMgrForm'].elements['amount'].focus();
    	strValue=false;
    }
    else
    {
    	strValue=true;
    }
	   if (strValue==true) {
   if (document.forms['peopleForm'].elements['people.name'].value == '')
    {
    	alert('Name is a required field');
    	document.forms['peopleForm'].elements['people.name'].focus();
    	strValue=false;
    } }
   if (strValue==true){
   if (document.forms['peopleForm'].elements['people.address'].value == '')
    {
    	alert('Address is a required field');
    	document.forms['peopleForm'].elements['people.address'].focus();
    	strValue=false;
    } }
    if (strValue==true){
   if (document.forms['peopleForm'].elements['people.phoneNbr'].value == '')
    {
    	alert('Phone # is a required field');
    	document.forms['peopleForm'].elements['people.phoneNbr'].focus();
    	strValue=false;
    } }
    return strValue;
}
function valCurrency()
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46) && (event.keyCode != 44) && (event.keyCode !=45))
	event.returnValue = false;
}
function checkNum(data)
{
   var validNum = "0123456789.";
   var i = count = 0;
   var dec = ".";
   var space = " ";
   var val = "";

   for (i = 0; i < data.length; i++)
      if (validNum.indexOf(data.substring(i, i+1)) != "-1")
         val += data.substring(i, i+1);

   return val;
}

function dollarFormatted(amount) {
	return '$' + commaFormatted(amount);
}

function commaFormatted(amount) {
		var delimiter = ","; // replace comma if desired
		var a = amount.split('.',2)
		var d = a[1];
		var i = parseInt(a[0]);
		if(isNaN(i)) { return ''; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		var n = new String(i);
		var a = [];
		while(n.length > 3)
		{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
		}
		if(n.length > 0) { a.unshift(n); }
		n = a.join(delimiter);
		if(d.length < 1) { amount = n; }
		else { amount = n + '.' + d; }
		amount = minus + amount;
		return amount;
}
// end of function CommaFormatted()
function formatCurrency(field) {
    var i = parseFloat(checkNum(field.value));
    if(isNaN(i)) { i = 0.00; }
    var minus = '';
    if(i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if(s.indexOf('.') < 0) { s += '.00'; }
    if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    field.value = dollarFormatted(s);
    return true;
}
function DisplayPeriod() {
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}else{
		if (document.forms[0].amount.value.length == 8 )	{
			document.forms[0].amount.value = document.forms[0].amount.value +'.';
 		}
 		if (document.forms[0].amount.value.length > 10 )  event.returnValue = false;
	}
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
 String peopleId = (String) request.getAttribute("peopleId");
 if(peopleId ==null) peopleId ="";
	String psaId = (String)request.getAttribute("psaId");
	if(psaId ==null) psaId ="";
	String psaType = (String)request.getAttribute("psaType");
	if(psaType ==null) psaType = "";

	session.setAttribute("peopleId",peopleId);
	session.setAttribute(elms.common.Constants.PSA_ID,psaId);
	session.setAttribute(elms.common.Constants.PSA_TYPE,psaType);

	CachedRowSet rs = new CachedRowSet();
    rs = (CachedRowSet)request.getAttribute("rs");

  	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";

	String psaInfo = (String)session.getAttribute("psaInfo");
	if (psaInfo == null ) psaInfo = "";
	String projectName="";
	if(psaType.equalsIgnoreCase("A")){
		projectName = LookupAgent.getProjectNameForActivityNumber(LookupAgent.getActivityNumberForActivityId(psaId));
	}

%>
<html:form action="/saveDeposit" onsubmit="return validateFunction();">
<html:hidden  value="<%=peopleId%>" property="peopleId"/>
<html:hidden  value="<%=psaId%>" property="psaId"/>
<html:hidden  value="<%=psaType%>"  property ="psaType"/>

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Deposit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="100%" background="../images/site_bg_B7C1CB.jpg">Depositor Details</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
<%
			while (rs.next()) {
               String balance = elms.util.StringUtils.dbl2$(rs.getDouble("deposit_balance"));
               if(balance == null) balance = "$0.00";
%>
                    <tr>
                      <td class="tablelabel">Name</td>
                      <td class="tabletext"><%= rs.getString("name") %> </td>
                      <td class="tablelabel">Balance</td>
                      <td class="tabletext"><%= balance %></td>
                    </tr>
                    <tr>
                      <td class="tablelabel">License No</td>
                      <td class="tabletext"><%= StringUtils.nullReplaceWithEmpty(rs.getString("lic_no")) %></td>
                      <td class="tablelabel">Modified</td>
                      <td class="tabletext"><%= elms.util.StringUtils.date2str(rs.getDate("pymnt_dt")) %></td>
                    </tr>
<%
			   }
%>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98%" background="../images/site_bg_B7C1CB.jpg">Add Deposit</td>

	                        <td width="1%" class="tablelabel"><nobr>
	                        <html:reset  value="Cancel"  styleClass="button" onclick="history.back();"/>
	                        <input type="button" value="Show Transactions"  onclick= "document.location='<%=contextRoot%>/viewTransaction.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">
	                        <html:submit  value="Save"  styleClass="button" />
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel" >Date</td>
                      <td class="tabletext" colspan="3"><%=StringUtils.cal2str(Calendar.getInstance())%></td>

                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Transaction</td>
                      <td class="tabletext">
                        <html:select property="transactionType" styleClass="textbox">
                            <html:option value="deposit">Deposit</html:option>
                            <html:option value="withdrawal" >Withdrawal</html:option>
                        </html:select>
					  </td>
                      <td class="tablelabel" >Method</td>
                      <td class="tabletext">
                        <html:select property="method" styleClass="textbox">
                            <html:option value="cash">Cash</html:option>
                            <html:option value="check">Check</html:option>
                            <html:option value="card">Card</html:option>
                            <html:option value="correction">Correction</html:option>
                            <html:option value="transferIn">Transfer In</html:option>
                            <html:option value="nonRevenue">Non-Revenue</html:option>
                        </html:select>
					  </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Amount</td>
                      <td class="tabletext">
                        <html:text  property="amount" size="15" styleClass="textbox" onblur="formatCurrency(this);" onkeypress="return DisplayPeriod();"/>
					  </td>
                      <td class="tablelabel" >Check/Conf. No.</td>
                      <td class="tabletext">
                        <html:text  property="checkNoConfirmation" size="15" styleClass="textbox"/>
					  </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Comments</td>
                      <td class="tabletext" colspan="3">
                        <html:textarea rows="10" cols="80" property="comments">
						</html:textarea>
					  </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/spacer.gif" width="1" height="32"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

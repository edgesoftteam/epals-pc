<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="sun.jdbc.rowset.*"%>


<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Show Transactions</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath();
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<%
	java.util.List depositList = (java.util.List) request.getAttribute("depositList");
	if(depositList!=null) pageContext.setAttribute("depositList",depositList);

		String peopleId = (String) request.getAttribute("peopleId");
		String psaId = (String) request.getAttribute("psaId");
		String psaType = (String) request.getAttribute("psaType");

	CachedRowSet rs = new CachedRowSet();
	rs = (CachedRowSet)request.getAttribute("rs");
	String lic_no = "";
%>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Show Transactions<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                     <td width="98%" background="../images/site_bg_B7C1CB.jpg">Depositor Details</td>

	                        <td width="1%" class="tablelabel"><nobr>
	                        <html:reset  value="Cancel"  styleClass="button" onclick="history.back();"/>
                      <input type="button" value="Add Deposit"  onclick = "document.location='<%=contextRoot%>/addDeposit.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button"> &nbsp;
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
<%
			while (rs.next()) {
 				lic_no = rs.getString("lic_no");
 				if (lic_no ==null) { lic_no=""; }

%>
                    <tr>
                      <td class="tablelabel">Name</td>
                      <td class="tabletext"><%= rs.getString("name") %> </td>
                      <td class="tablelabel">Balance</td>
                      <td class="tabletext">$<%= rs.getBigDecimal("deposit_balance") %></td>
                    </tr>
                    <tr>
                      <td class="tablelabel">License No</td>
                      <td class="tabletext"><%= lic_no  %></td>
                      <td class="tablelabel">Modified</td>
                      <td class="tabletext"><%= rs.getDate("pymnt_dt") %></td>
                    </tr>
<%
					}
%>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg">Transactions</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top" >
                      <!--<td class="tablelabel"><nobr>Trans No.</nobr></td>-->
                      <td class="tablelabel"><nobr> Date of Tran. </nobr></td>
                      <!--<td class="tablelabel"><nobr>Post Date</nobr></td>-->
                      <td class="tablelabel"><nobr>Tr.Type</nobr></td>
                      <td class="tablelabel"><nobr>Activity #</nobr></td>
                      <td class="tablelabel"><nobr>Amount</nobr></td>
                      <td class="tablelabel"><nobr>Pay Type</nobr></td>
                      <td class="tablelabel"><nobr>Check #</nobr></td>
                      <!--<td class="tablelabel"><nobr>Balance</nobr></td>-->
                      <td class="tablelabel"><nobr>Comments</nobr></td>
                      <td class="tablelabel"><nobr>Entered By</nobr></td>
                      <!--<td class="tablelabel"><nobr>License No</nobr></td>-->

                    </tr>
                    <logic:iterate id="payment" name="depositList">
                    <tr valign="top" >
                      <td class="tabletext"><bean:write name="payment" property="strPaymentDate"/></td>
                      <td class="tabletext"><bean:write name="payment" property="type.description"/></td>
                      <td class="tabletext"><bean:write name="payment" property="actNbr"/></td>
                      <td class="tabletext" align="right"><bean:write name="payment" property="strAmnt"/></td>
                      <td class="tabletext"><bean:write name="payment" property="method"/></td>
                      <td class="tabletext"><bean:write name="payment" property="accountNbr"/></td>
                      <td class="tabletext"><bean:write name="payment" property="comment"/></td>
                      <td class="tabletext"><bean:write name="payment" property="enteredBy.username"/></td>
                    </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>

    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="../images/spacer.gif" width="1" height="32"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>

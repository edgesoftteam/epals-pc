<%@ page import="elms.agent.*,elms.common.*,elms.util.*,java.util.*,java.util.ResourceBundle" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<app:checkLogon/>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Add People</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<%!
List eyeColorList = new ArrayList();
%>


<%

	elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);
	String contextRoot = request.getContextPath();
 	String addEditFlag = (String) request.getAttribute("addEditFlag");

	elms.app.people.People people = (elms.app.people.People)request.getAttribute("people");
    elms.control.beans.PeopleForm peopleForm=(elms.control.beans.PeopleForm)request.getAttribute("peopleForm");

	if((peopleForm.getStrDlExpiryDate().equals("")) || (peopleForm.getStrDlExpiryDate().equals("null")))
	{
	people.setDlExpiryDateString("N");
	}

	if((people.getConfirmStrDlExpiryDate()!="") || (people.getConfirmStrDlExpiryDate()!=null))
	{
	peopleForm.setConfirmStrDlExpiryDate(people.getConfirmStrDlExpiryDate());
	}

    //if(people.getAnyDateExpired().equals("null"))
	//{
	//people.setAnyDateExpired("A");
	//}
 	String peopleId = (String) request.getAttribute("peopleId");

   	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";

	String psaInfo = (String)session.getAttribute("psaInfo");
	if (psaInfo == null ) psaInfo = "";

	String psaId = (String)request.getAttribute("psaId");
	if(psaId ==null) psaId ="";
	String psaType = (String)request.getAttribute("psaType");
	if(psaType ==null) psaType = "";

	String projectName="";
	if(psaType.equalsIgnoreCase("A")){
		try{
		projectName = LookupAgent.getProjectNameForActivityNumber(LookupAgent.getActivityNumberForActivityId(psaId));

		eyeColorList = LookupAgent.getEyeColors();
		pageContext.setAttribute("eyeColorList", eyeColorList);
		}catch(Exception e){}
	}

    boolean lcUser=false,pdUser = false;
   	if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("PD")) pdUser = true;
   	if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("LC")) lcUser = true;

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	strValue=true;

	   if (strValue==true) {
   if (document.forms['peopleForm'].elements['people.name'].value == '')
    {
    	alert('Name is a required field');
    	document.forms['peopleForm'].elements['people.name'].focus();
    	strValue=false;
    } }

   if (strValue==true){
	   if (document.forms['peopleForm'].elements['people.name'].value == '') {
	    	alert('Name is a required field');
	    	document.forms['peopleForm'].elements['people.name'].focus();
	    	strValue=false;
	    } else if ((document.forms['peopleForm'].elements['people.address'].value == '')) {
	    	alert('Address is a required field');
	    	document.forms['peopleForm'].elements['people.address'].focus();
	    	strValue=false;
		} else if (document.forms['peopleForm'].elements['people.city'].value == '') {
	    	alert('City is a required field');
	    	document.forms['peopleForm'].elements['people.city'].focus();
	    	strValue=false;
		} else if (document.forms['peopleForm'].elements['people.state'].value == '') {
			alert('State is a required field');
			document.forms['peopleForm'].elements['people.state'].focus();
			strValue=false;
	    } else if (document.forms['peopleForm'].elements['people.zipCode'].value == '') {
			alert('Zip Code is a required field');
			document.forms['peopleForm'].elements['people.zipCode'].focus();
			strValue=false;
	    }  else if ((document.forms['peopleForm'].elements['people.ssn'].value != '') && ((document.forms['peopleForm'].elements['people.ssn'].value) != (document.forms['peopleForm'].elements['people.confirmSsn'].value))) {
			alert("The SSN and Confirm SSN is not matching Please reconfirm");
			document.forms['peopleForm'].elements['people.confirmSsn'].focus();
			strValue=false;

	    }  else if ((document.forms['peopleForm'].elements['people.dlNumber'].value != '') && ((document.forms['peopleForm'].elements['people.dlNumber'].value) != (document.forms['peopleForm'].elements['people.confirmDlNumber'].value))) {

			alert("The DL/ID Number and Confirm DL/ID Number is not matching Please reconfirm");
			document.forms['peopleForm'].elements['people.confirmDlNumber'].focus();
			strValue=false;
		}else if ((document.forms['peopleForm'].elements['strDlExpiryDate'].value != '') && ((document.forms['peopleForm'].elements['strDlExpiryDate'].value) != (document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].value))){

			if (document.forms['peopleForm'].elements['people.dlNumber'].value == ''){
			alert("Please Enter the DL/ID Number ");
			document.forms['peopleForm'].elements['people.dlNumber'].focus();
			strValue=false;
			return strValue;
			}
			alert("The DL/ID Expiration Date and Confirm DL/ID Expiration Date is not matching Please reconfirm");
			document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].focus();
			strValue=false;
		   }
			else if ((document.forms['peopleForm'].elements['strDlExpiryDate'].value != '') && ((document.forms['peopleForm'].elements['strDlExpiryDate'].value) == (document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].value))){

			if (document.forms['peopleForm'].elements['people.dlNumber'].value == ''){
			alert("Please Enter the DL/ID Number ");
			document.forms['peopleForm'].elements['people.dlNumber'].focus();
			strValue=false;
			return strValue;
			}

		   }else if ((document.forms['peopleForm'].elements['people.confirmSsn'].value != '') && ((document.forms['peopleForm'].elements['people.confirmSsn'].value) != (document.forms['peopleForm'].elements['people.ssn'].value))) {
			alert("The SSN and Confirm SSN is not matching Please reconfirm");
			document.forms['peopleForm'].elements['people.ssn'].focus();
			strValue=false;

	    	}else if ((document.forms['peopleForm'].elements['people.confirmDlNumber'].value != '') && ((document.forms['peopleForm'].elements['people.confirmDlNumber'].value) != (document.forms['peopleForm'].elements['people.dlNumber'].value))) {


			alert("The DL/ID Number and Confirm DL/ID Number is not matching Please reconfirm");
			document.forms['peopleForm'].elements['people.dlNumber'].focus();
			strValue=false;
			}else if ((document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].value != '') && ((document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].value) != (document.forms['peopleForm'].elements['strDlExpiryDate'].value))){

			if (document.forms['peopleForm'].elements['people.dlNumber'].value == ''){
			alert("Please Enter the DL/ID Number ");
			document.forms['peopleForm'].elements['people.dlNumber'].focus();
			strValue=false;
			return strValue;
			}
			alert("The DL/ID Expiration Date and Confirm DL/ID Expiration Date is not matching Please reconfirm");
			document.forms['peopleForm'].elements['strDlExpiryDate'].focus();
			strValue=false;
		   }
			else if ((document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].value != '') && ((document.forms['peopleForm'].elements['confirmStrDlExpiryDate'].value) == (document.forms['peopleForm'].elements['strDlExpiryDate'].value))){

			if (document.forms['peopleForm'].elements['people.dlNumber'].value == ''){
			alert("Please Enter the DL/ID Number ");
			document.forms['peopleForm'].elements['people.dlNumber'].focus();
			strValue=false;
			return strValue;
			}

		   }
   }

    return strValue;
}
function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.peopleForm.elements[str].value.length == 3 ) || (document.peopleForm.elements[str].value.length == 7 ))
		{
			document.peopleForm.elements[str].value = document.peopleForm.elements[str].value +'-';
 		}
 		if (document.peopleForm.elements[str].value.length > 11 )  event.returnValue = false;
	}
}
function onclickprocess(strval)
{
   if (strval == 'ad') document.location='<%=contextRoot%>/addDeposit.do';
   else if (strval == 'st') document.location='<%=contextRoot%>/viewTransaction.do'
}
function dohref(strHref)
{
   if (strHref == 'admin') parent.f_content.location.href="<%=contextRoot%>/searchPeople.do";
}

// Start of Check version People History
var no_array = new Array();
function checkVersions(val)
{
	var valNew=val.value;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false )
	{

		document.forms[0].checkboxArray.value = removeFromArray(valNew,no_array).join(",");
     	checkboxArray = removeFromArray(valNew,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}

	if(valNew != "")
	{
	 checkboxArray += valNew+",";

	}

    if(no_array.length > 2)
	{
	val.checked=false;
    checkboxArray=document.forms[0].checkboxArray.value;
	alert("Only two versions can be compared");
	}

	document.forms[0].checkboxArray.value = checkboxArray;
	return true;

}

function removeFromArray(val, ar){
s = String(ar)
// remove if not first item (global search)
reRemove = new RegExp(","+val,"g")
s = s.replace(reRemove,"")
// remove if at start of array
reRemove = new RegExp("^"+val+",")
s = s.replace(reRemove,"")
// remove if only item
reRemove = new RegExp("^"+val+"$")
s = s.replace(reRemove,"")
return new Array(s)
}

function viewDifferences()
{
var checkboxArray = document.forms[0].checkboxArray.value;
no_array=checkboxArray.split(",");
//alert(checkboxArray);
if(no_array.length < 3)
	{
	alert("Please select two checkboxes to compare");
	}
   else{
    document.location='<%=contextRoot%>/versionCompare.do?peopleId=<%=peopleId%>&typeId=J&checkboxArray='+checkboxArray;
  }
}

function DisplaySSNHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}else{
		if (document.forms[0].elements[str].value.length == 3)
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';

 		}
		if ((document.forms[0].elements[str].value.length == 6))  document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		if (document.forms[0].elements[str].value.length > 10 )  event.returnValue = false;
	}
}


function openUrl(){
	window.open('<%=elms.agent.LookupAgent.getContractorsStateLicenseBoardURL()%>');
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/savePeople" onsubmit="return validateFunction();">
<html:hidden property="checkboxArray" value=""/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg" width="98%">
                      <%=addEditFlag%> Applicant </td>

                      <td width="1%" class="tablelabel"><nobr>
                         <% if(lsoAddress.equalsIgnoreCase("") & psaInfo.equalsIgnoreCase("")){  %>
                          <input type="button" value="Back"  onclick = "dohref('admin');" class="button">
                          <% }else{  %>
                          <input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/listPeople.do?id=<%=psaId%>&type=<%=psaType%>'" class="button">
                          <% }  %>


                          <% if(addEditFlag.equalsIgnoreCase("edit")){  %>
                          <input type="button" value="Hold"  onclick = "document.location='<%=contextRoot%>/listHold.do?levelId=<%=peopleId%>&level=Z'" class="button">
                          <input type="button" value="Add Deposit"  onclick = "document.location='<%=contextRoot%>/addDeposit.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">
                          <input type="button" value="Show Transactions"  onclick= "document.location='<%=contextRoot%>/viewTransaction.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">
                          <html:button property="checkState"  value="Check State"  styleClass="button" onclick="openUrl()"/>
                          <%}%>
                         <security:editable levelId="<%=psaId%>" levelType="A" editProperty="allProjectAndDept">
	                     <security:editable levelId="<%=psaId%>" levelType="A" editProperty="checkUser">
								<html:submit  value="Add/Update" styleClass="button" />
						</security:editable>
						</security:editable>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Type</td>
                      <td class="tabletext" >Applicant</td>
                      <td class="tablelabel">Title</td>
                      <td class="tabletext" >
						<html:text  name = "peopleForm"   property="people.title" size="40"  styleClass="textbox"/>
						</td>
                    </tr>
                    <tr>
                      <td class="tablelabel">First Name</td>
                      <td class="tabletext" >
                         <html:text  name = "peopleForm"   property="people.name" size="40"  styleClass="textbox"/>
                     	 <html:hidden name = "peopleForm"  property="people.peopleId" />
                     	 <html:hidden name = "peopleForm"  property="people.licenseNbr" value=""/>
                     	  <html:hidden name = "peopleForm"  property="psaId" />
                     	  <html:hidden name = "peopleForm"  property="psaType" />
                      </td>
                      <td class="tablelabel">Last Name</td>
                      <td class="tabletext" >
                         <html:text  name = "peopleForm"   property="people.firstName" size="40"  styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Business Name</td>
                      <td class="tabletext" >
                         <html:text  name = "peopleForm"   property="people.businessName" size="40"  styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Corporate Name</td>
                      <td class="tabletext" >
                         <html:text  name = "peopleForm"   property="people.corporateName" size="40"  styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Business Address</td>
                      <td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.address" size="45" maxlength="60" styleClass="textbox"/>
                          <html:text  name = "peopleForm"   property="people.city" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.state" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.zipCode" size="10" maxlength="9" styleClass="textbox" />

                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Home Address</td>
                      <td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.homeAddress" size="45" maxlength="60" styleClass="textbox"/>
                          <html:text  name = "peopleForm"   property="people.homeCity" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.homeState" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.homeZipCode" size="10" maxlength="9" styleClass="textbox" />

                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Mailing Address</td>
                      <td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.mailingAddress" size="45" maxlength="60" styleClass="textbox"/>
                          <html:text  name = "peopleForm"   property="people.mailingCity" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.mailingState" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.mailingZipCode" size="10" maxlength="9" styleClass="textbox" />

                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Business Phone, Ext</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm" maxlength="12"  property="people.phoneNbr" size="25" styleClass="textbox" onkeypress="return DisplayHyphen('people.phoneNbr');" />&nbsp;
                          <html:text  name = "peopleForm" maxlength="4"  property="people.ext" size="4" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Fax</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm" property="people.faxNbr" size="25" maxlength="12" styleClass="textbox" onkeypress="return DisplayHyphen('people.faxNbr');" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Home Phone</td>
                      <td class="tabletext" >
                          <html:text   name = "peopleForm"  property="people.homePhone" size="25" maxlength="12" styleClass="textbox" onkeypress="return DisplayHyphen('people.homePhone');"/>
                      </td>
                      <td class="tablelabel">Email</td>
                      <td class="tabletext" >
                          <html:text   name = "peopleForm"  property="people.emailAddress" size="25" maxlength="200" styleClass="textbox"/>
                      </td>
                    </tr>
					<% if((lcUser)||(pdUser)){%>
                    <tr>

                      <td class="tablelabel">Social Security Number</td>
                      <td class="tabletext">
                          <html:text name = "peopleForm" maxlength="11"  property="people.ssn" size="25" onkeypress="return DisplaySSNHyphen('people.ssn');" styleClass="textbox"/>&nbsp;
                      </td>

                      <td class="tablelabel">Confirm Social Security Number</td>
                      <td class="tabletext">
                          <html:text name = "peopleForm" maxlength="11"  property="people.confirmSsn" size="25" onkeypress="return DisplaySSNHyphen('people.confirmSsn');" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>
					<%}else{%>
                    <tr>
                      <td class="tablelabel">Social Security Number</td>
                      <td class="tabletext">
                          XXX-XX-XXXX
                      </td>
                    </tr>
					<%}%>

					<% if((lcUser)||(pdUser)){%>
					<tr>
                      <td class="tablelabel">Date of Birth</td>
                     <td class="tabletext" colspan=3>
                      <nobr>
                        <html:text  name = "peopleForm" property="dateOfBirthString" size="10" styleClass="textbox" />
					    <html:link href="javascript:show_calendar('peopleForm.dateOfBirthString');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
					  </td>

                    </tr>


					<tr>
						<td class="tablelabel">DL/ID Number</td>
                      	<td class="tabletext">
                          <html:text name="peopleForm" maxlength="15" property="people.dlNumber" size="25" styleClass="textbox"/>&nbsp;
                      	</td>
                      	<td class="tablelabel">Confirm DL/ID Number </td>
                    	<td class="tabletext">
                          <html:text name="peopleForm" maxlength="15" property="people.confirmDlNumber" size="25" styleClass="textbox"/>&nbsp;
                      	</td>

                    </tr>

                    <tr>
                      	<td class="tablelabel">DL/ID Expiration Date </td>

						<td <%if(people.getDlExpiryDateString().equals("Y")){%> bgcolor="#F48080" <%}else{%>  class="tabletext"  <%} %>  >

                      	<nobr>
                          <html:text  name = "peopleForm"   property="strDlExpiryDate" size="10" styleClass="textbox" />
					    <html:link href="javascript:show_calendar('peopleForm.strDlExpiryDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
					  </td>

                      	<td class="tablelabel">Confirm DL/ID Expiration Date </td>

						<td class="tabletext">

                      	<nobr>
                          <html:text  name = "peopleForm"   property="confirmStrDlExpiryDate" size="10" styleClass="textbox" />
					    <html:link href="javascript:show_calendar('peopleForm.confirmStrDlExpiryDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
					  </td>
                    </tr>
					<tr>
                      <td class="tablelabel">Height</td>
                      <td class="tabletext">
                          <html:text name="peopleForm" property="people.height" size="25" styleClass="textbox"/>&nbsp;
                      </td>
                      <td class="tablelabel">Weight</td>
                      <td class="tabletext">
                          <html:text name = "peopleForm" maxlength="12"  property="people.weight" size="25" styleClass="textbox"/>&nbsp;
                      </td>
                    </tr>

					<tr>
                      <td class="tablelabel">Hair Color</td>
                      <td class="tabletext">
						<html:select name="peopleForm" property="people.hairColor" size="1" styleClass="textbox">
							<html:option value="">Please Select</html:option>
							<html:option value="Black">Black</html:option>
							<html:option value="Blonde">Blonde</html:option>
							<html:option value="Brown">Brown</html:option>
							<html:option value="Gray">Gray</html:option>
							<html:option value="Red">Red</html:option>

						</html:select>
                      </td>
                      <td class="tablelabel">Eye color</td>
                      <td class="tabletext">
                         <html:select name="peopleForm" property="people.eyeColor" size="1" styleClass="textbox">
							<html:option value="">Please Select</html:option>
							<html:options collection="eyeColorList" property="id" labelProperty="description"/>
						</html:select>
                      </td>
                    </tr>
					<tr>
                      <td class="tablelabel">Gender</td>
                      <td colspan="3" class="tabletext">
                         <html:select name="peopleForm" property="people.gender" size="1" styleClass="textbox">
							<html:option value="">Please Select</html:option>
							<html:option value="Female">Female</html:option>
							<html:option value="Male">Male</html:option>

						</html:select>
                      </td>
                    </tr>
					<%}%>

                    <tr>
                      <td class="tablelabel">Additional
                        Comments </td>
                      <td class="tabletext" colspan="3">
                          <html:textarea  name = "peopleForm"  property="people.comments" cols="85" rows="3" styleClass="textbox" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Hold Information </td>
                      <td class="tabletext" colspan="3">
                          <html:textarea  name = "peopleForm" property="hasHold" cols="85" rows="3" disabled="true" styleClass="textbox" />
                      </td>
                    </tr>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
<!-- Change History Part -->
			<tr>
				<td>
				<!-- display of the People Version history list -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="99%" background="../images/site_bg_B7C1CB.jpg">
                                  People History
                               </td>
                               <td width="99%" background="../images/site_bg_B7C1CB.jpg">

                                  <input type="button" name="viewDifference"  value="View Difference"  style="button" onclick="javascript:viewDifferences()" />

                               </td>
                                <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="17" height="19"></td>
                                <td width="1%" class="tablelabel">
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                        <tr>
                                <td class="tablelabel">Select</td>
								<td class="tablelabel">Version Number</td>
		                        <td class="tablelabel">Updated</td>
		                        <td class="tablelabel"><nobr>Name </nobr></td>
		                        <td class="tablelabel"><nobr>Address</nobr></td>
		                        <td class="tablelabel">Phone</td>
		                     </tr>
							<%
							   PeopleAgent peopleAgent = new PeopleAgent();
								java.util.List peopleHistoryList=  new java.util.ArrayList();
									try{
							   peopleHistoryList = peopleAgent.getPeopleHistoryList(peopleId);
							    pageContext.setAttribute("peopleHistoryList",peopleHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<peopleHistoryList.size();i++){
									elms.control.beans.PeopleVersionForm peopleVersionForm = (elms.control.beans.PeopleVersionForm)peopleHistoryList.get(i);

							 %>

			                    <tr valign="top">
								  <td class="tabletext">
			                         <html:multibox property="versionDifference"  onclick="javascript:return checkVersions(this)">I</html:multibox>
								  </td>
			                      <td class="tabletext">
			                         Current Version
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getUpdated()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getAddress()%> <%=peopleVersionForm.getCity()%> <%=peopleVersionForm.getState()%> <%=peopleVersionForm.getZip()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getPhone()%>
			                      </td>

						    <%} %>
							<%
							   java.util.List contractHistoryList=  new java.util.ArrayList();
									try{
							   contractHistoryList = peopleAgent.getContractHistoryList(peopleId);
							    pageContext.setAttribute("contractHistoryList",contractHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<contractHistoryList.size();i++){
									elms.control.beans.PeopleVersionForm peopleVersionForm = (elms.control.beans.PeopleVersionForm)contractHistoryList.get(i);

							 %>

			                    <tr valign="top">
								  <td class="tabletext">
			                         <html:multibox property="versionDifference"  onclick="javascript:return checkVersions(this)"><%=peopleVersionForm.getVersionNumber()%></html:multibox>
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getVersionNumber()%>
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getUpdated()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getAddress()%> <%=peopleVersionForm.getCity()%> <%=peopleVersionForm.getState()%> <%=peopleVersionForm.getZip()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getPhone()%>
			                      </td>

						    <%} %>
                        </table>
                        </td>
                    </tr>
<!-- End Change History Part -->
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>

<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String contextRoot = request.getContextPath();
	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";

	String psaInfo = (String)session.getAttribute("psaInfo");
	if (psaInfo == null ) psaInfo = "";
	
	String level = (String)request.getAttribute("level");

	session.setAttribute("level", level);
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
    	strValue=true;
 		if ((document.forms['peopleForm'].elements['people.licenseNbr'].value == '') &&
	   		(document.forms['peopleForm'].elements['people.name'].value == '') &&
		    (document.forms['peopleForm'].elements['people.phoneNbr'].value == '')) {
			 		alert("At Least one search criteria is required");
			 		strValue = false;
	    }else if(document.forms['peopleForm'].elements['people.peopleType.code'].value == ''){
			alert("Select Type is required");
			strValue = false;
		}else{
			document.forms[0].action='<%=contextRoot%>/viewPeopleSearchResults.do';
  			document.forms[0].submit();
		}
    return strValue;
}
function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.peopleForm.elements[str].value.length == 3 ) || (document.peopleForm.elements[str].value.length == 7 ))
		{
			document.peopleForm.elements[str].value = document.peopleForm.elements[str].value +'-';
 		}
 		if (document.peopleForm.elements[str].value.length > 11 )  event.returnValue = false;
	}
}
if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
		validateFunction();
	}
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<html:form focus="people.peopleType.code" action="/viewPeopleSearchResults">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
<html:hidden property="psaType" value = '<%=level %>'></html:hidden>
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Maintenance&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle">People Search </td>
                        <td width="1%" class="tablebutton"><nobr>
     					<html:reset  value="Back" styleClass="button" onclick="javascript:history.back()"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" width="1%"><nobr>Select Type</nobr></td>
                      <td class="tabletext" width="99%">

					
					 	<%if(level.equalsIgnoreCase("O")){ %>
					 	<html:hidden name = "peopleForm" property="people.peopleType.code"  value = "T"></html:hidden>
					 	<html:text name = "peopleForm" property="people.peopleType.code" value = "Tenant" readonly = "true"></html:text>
<%-- 							<html:select  name = "peopleForm" property="people.peopleType.code" size="1" styleClass="textbox" value = "T"> --%>
<%-- 						<html:option value="">Please Select</html:option> --%>
						
						
<%-- 					 	<logic:equal scope="request" name="fromAdmin" value="true"> --%>
<%-- 			                <html:options collection="peopleTypeList" property="code" labelProperty="type" /> --%>
<%-- 					 	</logic:equal> --%>
<%-- 					 	<logic:notEqual scope="request" name="fromAdmin" value="true"> --%>
<%-- 			                <html:options collection="peopleTypeList" property="code" labelProperty="type" /> --%>
<%-- 					 	</logic:notEqual> --%>
<%-- 					  </html:select> --%>
						<%} else { %>
						
						<html:select  name = "peopleForm" property="people.peopleType.code" size="1" styleClass="textbox" >
						<html:option value="">Please Select</html:option>
						
						
					 	<logic:equal scope="request" name="fromAdmin" value="true">
			                <html:options collection="peopleTypeList" property="code" labelProperty="type" />
					 	</logic:equal>
					 	<logic:notEqual scope="request" name="fromAdmin" value="true">
			                <html:options collection="peopleTypeList" property="code" labelProperty="type" />
					 	</logic:notEqual>
					  </html:select>
					 	<%}%>
		           

 			</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" width="1%"><nobr>Enter
                        License No.</nobr></td>
                      <td class="tabletext" width="99%">
                        <html:text  name = "peopleForm"  property="people.licenseNbr" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" width="1%"><nobr>Enter
                        Name </nobr></td>
                      <td class="tabletext" width="99%">
                        <html:text  name = "peopleForm"   property="people.name" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" width="1%"><nobr>Phone,Ext</nobr></td>
                      <td class="tabletext" width="99%">
                        <html:text  name = "peopleForm" maxlength="12"  property="people.phoneNbr" size="14" styleClass="textbox" onkeypress="return DisplayHyphen('people.phoneNbr');" />
                        &nbsp;
                        <html:text  name = "peopleForm" maxlength="4"  property="people.ext" size="4" styleClass="textbox"/>
                         </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" width="1%">&nbsp;</td>
                      <td class="tabletext" width="99%"> &nbsp;
			<html:button  property="Find" value="Find" styleClass="button" onclick="return validateFunction();"/>
                        &nbsp;
			<html:reset  value="Clear" styleClass="button"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<%@page import="elms.util.StringUtils"%>
<%@page import="elms.common.Constants"%>
<%@ page import="elms.agent.*,java.util.ResourceBundle" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Edit Tenant</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>

<%
	String contextRoot = request.getContextPath();
	String addEditFlag = (String) request.getAttribute("addEditFlag");
 	String peopleId = (String) request.getAttribute("peopleId");

   	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";
	
 	String city = (String)session.getAttribute("city");
	if (city == null ) city = "";
	
 	String state = (String)session.getAttribute("state");
	if (state == null ) state = "";
	
	String lsoId = (String)session.getAttribute("lsoId");
	if(lsoId ==null) lsoId = "";
	
	boolean tenant = false;
	if(lsoId != null && peopleId == null){
		tenant = PeopleAgent.checkTenant(StringUtils.s2i(lsoId));
	}
	
 	String zip = (String)session.getAttribute("zip");
	if (zip == null ) zip = "";
	
	String unit = (String)session.getAttribute("unit");
	if (unit == null ) unit = "";

	String psaInfo = (String)session.getAttribute("psaInfo");
	if (psaInfo == null ) psaInfo = "";

	String psaId = (String)request.getAttribute("psaId");
	if(psaId ==null) psaId ="";
	String psaType = (String)request.getAttribute("psaType");
	if(psaType ==null) psaType = "";
	
	String level = (String)session.getAttribute("level");
	if(level == null) level = (String)request.getAttribute("level");
	if(level ==null) level = "";
	
	String levelType = (String)request.getParameter("levelType");
	if(levelType ==null) 
	{
		String levelTypeNew = (String)request.getAttribute("levelType");
		if(levelTypeNew ==null) 
			levelType = "";
		else
			levelType=levelTypeNew;
	}
	
	String projectName="";
	if(psaType.equalsIgnoreCase("A")){
	try{
		projectName = LookupAgent.getProjectNameForActivityNumber(LookupAgent.getActivityNumberForActivityId(psaId));;
	}catch(Exception e){}
	
	
	
}

%>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>

<SCRIPT>
var strValue;
function validateFunction()
{
	strValue=true;

   if (document.forms['peopleForm'].elements['people.name'].value == '') {
    	alert('Name is a required field');
    	document.forms['peopleForm'].elements['people.name'].focus();
    	strValue=false;
    } else if ((document.forms['peopleForm'].elements['people.address'].value == '')) {
    	alert('Address is a required field');
    	document.forms['peopleForm'].elements['people.address'].focus();
    	strValue=false;
	} else if (document.forms['peopleForm'].elements['people.city'].value == '') {
    	alert('City is a required field');
    	document.forms['peopleForm'].elements['people.city'].focus();
    	strValue=false;
	} else if (document.forms['peopleForm'].elements['people.state'].value == '') {
		alert('State is a required field');
		document.forms['peopleForm'].elements['people.state'].focus();
		strValue=false;
    } else if (document.forms['peopleForm'].elements['people.zipCode'].value == '') {
		alert('Zip Code is a required field');
		document.forms['peopleForm'].elements['people.zipCode'].focus();
		strValue=false;
    } else if(<%=tenant%>){
	 alert("Already Tenant is there");
	 strValue=false;
   }
    else if (document.forms['peopleForm'].elements['strMoveInDate'].value == '')
    {
    	alert("Move In Date is a required field");
   	 	strValue=false;
    }
    else if (document.forms['peopleForm'].elements['strMoveOutDate'].value != '')
    {
    	var moveInDate=document.forms['peopleForm'].elements['strMoveInDate'].value
    	var moveOutDate=document.forms['peopleForm'].elements['strMoveOutDate'].value
    	
    	if ((new Date(moveInDate).getTime()) > (new Date(moveOutDate).getTime())) {
    		alert("Move out date should be greater than Move in date ");
    		strValue=false;
    	} 
    }
   
   return strValue;
}

function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.peopleForm.elements[str].value.length == 3 ) || (document.peopleForm.elements[str].value.length == 7 ))
		{
			document.peopleForm.elements[str].value = document.peopleForm.elements[str].value +'-';
 		}
 		if (document.peopleForm.elements[str].value.length > 11 )  event.returnValue = false;
	}
}
function onclickprocess(strval)
{
  if (strval = 'ad') document.location='<%=contextRoot%>/addDeposit.do';
  if (strval = 'st') document.location='<%=contextRoot%>/viewTransaction.do';
}
function dohref(strHref)
{
   if (strHref == 'admin') parent.f_content.location.href="<%=contextRoot%>/searchPeople.do";
}

// Start of Check version People History
var no_array = new Array();
function checkVersions(val)
{
	var valNew=val.value;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false )
	{

		document.forms[0].checkboxArray.value = removeFromArray(valNew,no_array).join(",");
     	checkboxArray = removeFromArray(valNew,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}

	if(valNew != "")
	{
	 checkboxArray += valNew+",";

	}

    if(no_array.length > 2)
	{
	val.checked=false;
    checkboxArray=document.forms[0].checkboxArray.value;
	alert("Only two versions can be compared");
	}

	document.forms[0].checkboxArray.value = checkboxArray;
	return true;

}
function removeFromArray(val, ar){
s = String(ar)
// remove if not first item (global search)
reRemove = new RegExp(","+val,"g")
s = s.replace(reRemove,"")
// remove if at start of array
reRemove = new RegExp("^"+val+",")
s = s.replace(reRemove,"")
// remove if only item
reRemove = new RegExp("^"+val+"$")
s = s.replace(reRemove,"")
return new Array(s)
}

function viewDifferences()
{
var checkboxArray = document.forms[0].checkboxArray.value;
no_array=checkboxArray.split(",");
if(no_array.length < 3)
	{
	alert("Please select two checkboxes to compare");
	}
   else{
    document.location='<%=contextRoot%>/versionCompare.do?peopleId=<%=peopleId%>&typeId=T&checkboxArray='+checkboxArray;
  }
}
function openUrl(){
	window.open('<%=elms.agent.LookupAgent.getContractorsStateLicenseBoardURL()%>');
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/savePeople" onsubmit="return validateFunction();">
<html:hidden property="checkboxArray" value=""/>
<html:hidden property="people.isFromOccupancy" value='<%=level%>'></html:hidden>
<html:hidden property="people.lsoId" value='<%=lsoId%>'></html:hidden>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg" width="98%">
                      <%=addEditFlag%> Tenant </td>

                      <td width="1%" class="tablelabel"><nobr>
                         <% if(lsoAddress.equalsIgnoreCase("") & psaInfo.equalsIgnoreCase("")){  %>
                          <input type="button" value="Back"  onclick = "dohref('admin');" class="button">
                          <% }else{  %>
                          
                          <%
                          if(levelType.equalsIgnoreCase("O") && (psaType.equalsIgnoreCase("O") || psaType.equalsIgnoreCase("A"))){
                          %>
                          	<input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/listPeople.do?id=<%=lsoId%>&type=<%=levelType %>&levelId=<%=psaId%>&levelType=<%=psaType%>'" class="button">
                          <%
                          }else if(levelType.equals("S")){%>
          					<input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/listPeople.do?id=<%=lsoId%>&type=S&levelId=<%=psaId%>&levelType=<%=psaType%>'" class="button">
                          <%}else{
                          %>
                          	<input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/listPeople.do?id=<%=lsoId%>&type=L&levelId=<%=psaId%>&levelType=<%=psaType%>'" class="button">
                          <% }}  %>


                           <% if(projectName.equalsIgnoreCase(Constants.PROJECT_NAME_HOUSING) &&  (user.getDepartment().getDepartmentCode().equalsIgnoreCase("HD"))){ %>
                          <input type="button" value="Hold"  onclick = "document.location='<%=contextRoot%>/listHold.do?levelId=<%=peopleId%>&level=Z'" class="button">
                          <input type="button" value=" Activities "  onclick = "document.location='<%=contextRoot%>/viewAssociatedActivities.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>&name=<bean:write  name ="peopleForm"  property="people.name" />'" class="button">

                          <input type="button" value="Add Deposit"  onclick = "document.location='<%=contextRoot%>/addDeposit.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">
                          <input type="button" value="Show Transactions"  onclick= "document.location='<%=contextRoot%>/viewTransaction.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">
                          <html:button property="checkState"  value="Check State"  styleClass="button"  onclick="openUrl()"/>
                          <%}%>
                          <html:submit  value="Add/Update" styleClass="button" />

                         &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Type</td>
                      <td class="tabletext" colspan="3">Tenant</td>
                    </tr>
                       <html:hidden name = "peopleForm"  property="people.peopleId" />
                       <html:hidden name = "peopleForm"  property="people.licenseNbr" value=""/>
                       <html:hidden name = "peopleForm"  property="psaId" />
                       <html:hidden name = "peopleForm"  property="psaType" />
                    <tr>
                      <td class="tablelabel">Name</td>
                      <td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.name" size="45"  styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Address</td>
                     <% if(lsoAddress != ""){ %>
                     	<td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.address" value="<%= lsoAddress %>" size="45" maxlength="60" styleClass="textbox"/>
                      </td>
                     <% } else{%>
                      	<td class="tabletext" colspan="3">
                          <html:text name = "peopleForm" property="people.address" size="45" maxlength="60" styleClass="textbox"/>
                      </td>
                      <% }%> 
                      
                    </tr>
                    <tr>
                      <td class="tablelabel">City, State, Zip</td>
                      <td class="tabletext" colspan="3">
                      <%if(city!="" && state !="" && zip!=""){%>
                      	  <html:text  name = "peopleForm"   property="people.city" value="<%=city %>" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.state" value="<%=state %>" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.zipCode" value="<%=zip %>" size="10" maxlength="9" styleClass="textbox" />
                      <%} else{%>
                       	  <html:text  name = "peopleForm"   property="people.city" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.state" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.zipCode" size="10" maxlength="9" styleClass="textbox" />
                      <%}%>
                          
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Phone, Ext</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm" maxlength="12"  property="people.phoneNbr" size="25" styleClass="textbox" onkeypress="return DisplayHyphen('people.phoneNbr');"/>&nbsp;
                          <html:text  name = "peopleForm" maxlength="4"  property="people.ext" size="4" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Fax</td>
                      <td class="tabletext">
                           <html:text  name = "peopleForm" property="people.faxNbr" size="25" maxlength="12" styleClass="textbox" onkeypress="return DisplayHyphen('people.faxNbr');"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Email</td>
                      <td class="tabletext">
                          <html:text   name = "peopleForm"  property="people.emailAddress" size="25" maxlength="200" styleClass="textbox"/>
                      </td>
<!--                       <td class="tablelabel">Copy as Applicant</td> -->
<!--                       <td class="tabletext">Yes -->
<%--                           <html:radio name = "peopleForm"  property="people.copyApplicant" value="Y" /> --%>
<!--                         &nbsp;&nbsp;No -->
<%--                           <html:radio name = "peopleForm"  property="people.copyApplicant" value="N" /> --%>

<!--                         </td> -->
<!--                     </tr> -->
<!--                      <tr> -->
                      <td class="tablelabel">Household Income</td>
                      <td class="tabletext">
                          <html:text   name = "peopleForm"  property="people.householdIncome" size="25" maxlength="40" styleClass="button" onblur="formatCurrency(this);"/>
                      </td>
                     </tr>
                     <tr>
                      <td class="tablelabel">Net Rent</td>
                      <td class="tabletext">
                          <html:text   name = "peopleForm"  property="people.netRent" size="25" maxlength="40" styleClass="button" onblur="formatCurrency(this);"/>
                      </td>
                       <td class="tablelabel">Unit Designation</td>
                        <td class="tabletext">
                          <html:select name = "peopleForm"  property="people.unitDesignation" styleClass="textbox">
               	          	<html:option value="">Please Select</html:option>
               	          	<html:option value="EL">Extremely Low</html:option>
               	          	<html:option value="VL">Very Low</html:option>
               	          	<html:option value="L">Low</html:option>
               	          	<html:option value="M">Moderate</html:option>
               	          	<html:option value="LH">Low Home</html:option>
               	          	<html:option value="HH">High Home</html:option>
                          </html:select>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Move In Date</td>
                       <TD class="tabletext"><html:text name = "peopleForm"   property="strMoveInDate" maxlength="10"  styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"></html:text>
                       <A href="javascript:show_calendar('peopleForm.strMoveInDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> 
                       <IMG src="../images/calendar.gif" width="16" height="15" border="0"></A></TD>
                     
                      <td class="tablelabel">Move Out Date</td>
                      <TD class="tabletext"><html:text name = "peopleForm"  property="strMoveOutDate" maxlength="10"  styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"></html:text>
                       <A href="javascript:show_calendar('peopleForm.strMoveOutDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> 
                       <IMG src="../images/calendar.gif" width="16" height="15" border="0"></A></TD>
                      
                    </tr>
                    
                    <tr>
                      <td class="tablelabel">Additional Comments </td>
                      <td class="tabletext" colspan="3">
                          <html:textarea  name = "peopleForm"  property="people.comments" cols="85" rows="3" styleClass="textbox" />
                      </td>
                    </tr>
                                        <tr>
                      <td class="tablelabel">Hold Information </td>
                      <td class="tabletext" colspan="3">
                          <html:textarea  name = "peopleForm" property="hasHold" cols="85" rows="3" disabled="true" styleClass="textbox" />
                      </td>
                    </tr>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
<!-- Change History Part -->
			<tr>
				<td>
				<!-- display of the People Version history list -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="99%" background="../images/site_bg_B7C1CB.jpg">
                                  People History
                               </td>
                               <td width="99%" background="../images/site_bg_B7C1CB.jpg">

                                  <input type="button" name="viewDifference"  value="View Difference"  style="button" onclick="javascript:viewDifferences()" />

                               </td>
                                <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="17" height="19"></td>
                                <td width="1%" class="tablelabel">
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                        <tr>
                                <td class="tablelabel">Select</td>
								<td class="tablelabel">Version Number</td>
		                        <td class="tablelabel">Updated</td>
		                        <td class="tablelabel"><nobr>Name </nobr></td>
		                        <td class="tablelabel"><nobr>Address</nobr></td>
		                        <td class="tablelabel">Phone</td>
		                     </tr>
							<%
							   PeopleAgent peopleAgent = new PeopleAgent();
								java.util.List peopleHistoryList=  new java.util.ArrayList();
									try{
							   peopleHistoryList = peopleAgent.getPeopleHistoryList(peopleId);
							    pageContext.setAttribute("peopleHistoryList",peopleHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<peopleHistoryList.size();i++){
									elms.control.beans.PeopleVersionForm peopleVersionForm = (elms.control.beans.PeopleVersionForm)peopleHistoryList.get(i);

							 %>

			                    <tr valign="top">
								  <td class="tabletext">
			                         <html:multibox property="versionDifference"  onclick="javascript:return checkVersions(this)">I</html:multibox>
								  </td>
			                      <td class="tabletext">
			                         Current Version
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getUpdated()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getAddress()%> <%=peopleVersionForm.getCity()%> <%=peopleVersionForm.getState()%> <%=peopleVersionForm.getZip()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getPhone()%>
			                      </td>

						    <%} %>
							<%

								java.util.List contractHistoryList=  new java.util.ArrayList();
									try{
							   contractHistoryList = peopleAgent.getContractHistoryList(peopleId);
							    pageContext.setAttribute("contractHistoryList",contractHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<contractHistoryList.size();i++){
									elms.control.beans.PeopleVersionForm peopleVersionForm = (elms.control.beans.PeopleVersionForm)contractHistoryList.get(i);

							 %>

			                    <tr valign="top">
								  <td class="tabletext">
			                         <html:multibox property="versionDifference"  onclick="javascript:return checkVersions(this)"><%=peopleVersionForm.getVersionNumber()%></html:multibox>
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getVersionNumber()%>
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getUpdated()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getAddress()%> <%=peopleVersionForm.getCity()%> <%=peopleVersionForm.getState()%> <%=peopleVersionForm.getZip()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getPhone()%>
			                      </td>

						    <%} %>
                        </table>
                        </td>
                    </tr>
<!-- End Change History Part -->

<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><font class="con_hdr_3b">&nbsp;&nbsp;&nbsp;</td>
			</tr>
				<tr>
					<td><font class="con_hdr_3b">Tenant History&nbsp;&nbsp;&nbsp;<br>
					<br></td>

				</tr>
				<tr>
					<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel">Name</td>
									<td class="tablelabel">Address</td>
									<td class="tablelabel">Move In Date</td>
									<td class="tablelabel">Move Out Date</td>
								</tr>
									<%
								java.util.List getPeopleLsoIdHistoryList=  new java.util.ArrayList();
									try{
										getPeopleLsoIdHistoryList = peopleAgent.getPeopleLsoIdHistoryList(peopleId);
							    pageContext.setAttribute("getPeopleLsoIdHistoryList",getPeopleLsoIdHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<getPeopleLsoIdHistoryList.size();i++){
									elms.control.beans.PeopleForm peopleform =(elms.control.beans.PeopleForm)getPeopleLsoIdHistoryList.get(i);

							 %>

			                    <tr valign="top">
			                      <td class="tabletext">
			                         <%=peopleform.getPeople().getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleform.getPeople().getAddress()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=StringUtils.nullReplaceWithEmpty(peopleform.getPeople().getMoveInDate())%>
			                      </td>
			                      <td class="tabletext">
			                         <%=StringUtils.nullReplaceWithEmpty(peopleform.getPeople().getMoveOutDate())%>
			                      </td>

						    <%} %>
								</table>
								</td>
				</tr>
				</table>
				</td>
				</tr>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

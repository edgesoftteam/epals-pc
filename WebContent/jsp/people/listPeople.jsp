<%@page import="elms.util.StringUtils"%>
<%@page import="java.util.Date"%>
<%@ page import="elms.agent.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<app:checkLogon />
<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<bean:define id="user" name="user" type="elms.security.User"/>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0" onkeyup="onEnterKeyPress();">

<%!

String activityIdStr = "";
String status = "";
String hardHoldActive ="";
 %>
<%
String contextRoot = request.getContextPath();


String type = request.getParameter("type");
if(type == null) type = (String)request.getAttribute("type");
if(type ==null) type = "";

String id = request.getParameter("id");
request.setAttribute("lsoId", id);

String editable = request.getParameter("ed");
if(editable == null) editable = (String)request.getAttribute("ed");
if(editable ==null) editable = "";

String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";

String psaId = (String) request.getAttribute("psaId");
String psaType = (String) request.getAttribute("psaType");
if (psaType ==null) psaType = "";
if (psaId ==null) psaId = "";
int peopleCount = 0;
java.util.List peopleList = (java.util.List) request.getAttribute("peopleList");

java.util.Iterator iter = null;
if(peopleList !=null) {
	pageContext.setAttribute("peopleList",peopleList);
	peopleCount = peopleList.size();
	iter = peopleList.iterator();
}
String projectName="";
String activityNumber = "";
if(psaType.equalsIgnoreCase("A")){
	activityNumber = LookupAgent.getActivityNumberForActivityId(psaId);
	projectName = LookupAgent.getProjectNameForActivityNumber(activityNumber);
}



%>
<script>
function onclickprocess(strval)
{
    if (strval == 'sp') {
      document.location='<%=contextRoot%>/searchPeople.do?level=<%=psaType%>';
    }else if (strval == 'dp') {
      document.peopleForm.action='<%=contextRoot%>/deletePeople.do?psaId=<%=psaId%>&psaType=<%=psaType%>';
      document.peopleForm.submit();
    }else if (strval == 'va') {
      document.location='<%=contextRoot%>/viewActivity.do?activityId='+document.peopleForm.psaId.value;
    }else if (strval == 'vp') {
      document.location='<%=contextRoot%>/viewProject.do?projectId='+document.peopleForm.psaId.value;
    }

}

function goBack(button) {
  button.disabled=true;
  <logic:equal name="psaType" value="A">
	  document.location.href="<%=contextRoot%>/viewActivity.do?activityId=<%=psaId%>";
  </logic:equal>
  <logic:equal name="psaType" value="P">
	  document.location.href="<%=contextRoot%>/viewProject.do?projectId=<%=psaId%>";
  </logic:equal>
  <logic:equal name="psaType" value="Q">
  document.location.href="<%=contextRoot%>/viewSubProject.do?subProjectId=<%=psaId%>";
</logic:equal>
<logic:equal name="psaType" value="O">
document.location.href="<%=contextRoot%>/viewOccupancy.do?lsoId=<%=psaId%>";
</logic:equal>
<logic:equal name="psaType" value="L">
document.location.href="<%=contextRoot%>/viewLand.do?lsoId=<%=psaId%>";
</logic:equal>
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
	history.back();
}

}
</script>
<html:form name="peopleForm" type="elms.control.beans.PeopleForm" action="">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">People Manager List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br>
					<br></td>

				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="98%" class="tabletitle">People</td>
									<td width="1%" class="tablebutton"><nobr>

										<% if(editable.equals("true")){ %>
										 <security:editable levelId="<%=psaId%>" levelType="<%=psaType%>" editProperty="allProjectAndDept">
											 <security:editable levelId="<%=psaId%>" levelType="<%=psaType%>" editProperty="editable">
												<html:button property="new" value=" Add " onclick="onclickprocess('sp')" styleClass="button" /> &nbsp;&nbsp;
												<html:button property="delete" value=" Delete " styleClass="button" onclick="onclickprocess('dp')" /> &nbsp;&nbsp;
											</security:editable>
										</security:editable>
									    <%}%>
								    <html:button property="Back" value=" Back " onclick="goBack(this)" styleClass="button" />&nbsp;&nbsp; </nobr>
									</td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel">Select</td>
									<td class="tablelabel">People Type</td>
									<td class="tablelabel">Name</td>
									<td class="tablelabel">Phone</td>
								</tr>

								<% while(iter.hasNext()){
			                    	   elms.app.people.People people = (elms.app.people.People)iter.next();
			                           if(people.getMoveOutDate() == null || StringUtils.nullReplaceWithEmpty(people.getMoveOutDate()).compareTo(StringUtils.date2str(new Date())) >= 0){
				                    	
				                           String licenseNbr = people.getLicenseNbr().trim();
				                           if (licenseNbr == null || licenseNbr.equals("0") ) licenseNbr = "";
				                           String pcPeople = people.getLevelType().trim();
				                           if(pcPeople == null) pcPeople = "";
				                           String levelType = "";
										   String peopleId=elms.util.StringUtils.i2s(people.getPeopleId());
				                           if(pcPeople.equalsIgnoreCase("C")) levelType = "Plan Check";
				                           if(pcPeople.equalsIgnoreCase("A")) levelType = "Activity";
				                           if(pcPeople.equalsIgnoreCase("P")) levelType = "Project";
                    			 %>

	                            <tr valign="top"
		                          <% if((people.getAnyDateExpired()!=null ? people.getAnyDateExpired() : "").equals("Y")){%>
		    	                    bgcolor="#F48080"
			                      <%} else{%>
		    	                    class="tabletext"
			                      <%}%>
			                     >
								<%
								int actStatusId=0;
								 try{

									status = LookupAgent.getMostRecentHoldStatus(elms.util.StringUtils.nullReplaceWithEmpty(peopleId));
									actStatusId = LookupAgent.getActivityStatusId(elms.util.StringUtils.nullReplaceWithEmpty(peopleId));

									if (!(new CommonAgent().editable(elms.util.StringUtils.s2i(elms.util.StringUtils.nullReplaceWithEmpty(peopleId)), "A"))) {
								                hardHoldActive = "false";
								            }else{
						           hardHoldActive="true";
									}
								}catch(Exception e){}
								%>

									<td class="tabletext"><html:multibox property="selectedPeople"><%=psaId%>-<%=people.getPeopleId()%>-<%=pcPeople%></html:multibox></td>
									<td class="tabletext"><%=people.getPeopleType().getType()%></td>
									<td class="tabletext"><a href='<%=contextRoot%>/editPeople.do?peopleId=<%=people.getPeopleId()%>&psaId=<%=psaId%>&psaType=<%=psaType%>&levelType=<%=type%>'><%=people.getName()%><%=people.getLastName()%></a></td>
									<td class="tabletext"><%=people.getPhoneNbr()%></td>
									
								</tr>
								<% } } %>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td width="1%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="32">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
		<%if(type.equalsIgnoreCase("O")){ %>
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Tenant History&nbsp;&nbsp;&nbsp;<br>
					<br></td>

				</tr>
				<tr>
					<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel">Name</td>
									<td class="tablelabel">Address</td>
									<td class="tablelabel">Move In Date</td>
									<td class="tablelabel">Move Out Date</td>
								</tr>
							<%
							PeopleAgent peopleAgent = new PeopleAgent();
								java.util.List getPeopleLsoIdHistoryList=  new java.util.ArrayList();
									try{
										getPeopleLsoIdHistoryList = peopleAgent.getPeopleLsoIdHistoryList(id);
							    pageContext.setAttribute("getPeopleLsoIdHistoryList",getPeopleLsoIdHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<getPeopleLsoIdHistoryList.size();i++){
									elms.control.beans.PeopleForm peopleform =(elms.control.beans.PeopleForm)getPeopleLsoIdHistoryList.get(i);

							 %>

			                    <tr valign="top">
			                      <td class="tabletext">
			                         <%=peopleform.getPeople().getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleform.getPeople().getAddress()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleform.getPeople().getMoveInDate()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleform.getPeople().getMoveOutDate()%>
			                      </td>

						    <%} %>
								</table>
								</td>
				</tr>
				</table>
				</td>
				</tr>
				<%} %>
				
	</table>
	<html:hidden property="peopleCount" value="<%=elms.util.StringUtils.i2s(peopleCount)%>" />
	<html:hidden property="psaId" value="<%=psaId%>" />
</html:form>
</body>
</html:html>
<%@ page import="elms.app.people.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>
<%!
int versionNo = 0;
String name = "";
String address = "";
String city = "";
String state = "";
String zip = "";
String phone = "";
String ext = "";
String fax = "";
String email = "";
String copyApplicant = "";
String comments = "";
%>
<%
People peopleVer1= (People) request.getAttribute("peopleVer1");
People peopleVer2= (People) request.getAttribute("peopleVer2");
String contextRoot = request.getContextPath();
%>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Compare Versions</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<html:form action="/savePeople">
<html:hidden property="checkboxArray" />
<table width="100%" border="0" cellspacing="10" cellpadding="0"  >
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg" width="40%">Contact</td>
						<%
							versionNo = peopleVer1.getVersionNumber();
						%>
                      <td background="../images/site_bg_B7C1CB.jpg" width="40%"><%if(versionNo == 0){%>Current Version<%}else{%>Version <%=versionNo%><%}%></td>
						<%
							versionNo = peopleVer2.getVersionNumber();
						%>
                      <td background="../images/site_bg_B7C1CB.jpg" width="40%"><%if(versionNo == 0){%>Current Version<%}else{%>Version <%=versionNo%><%}%></td>

                      <td width="1%" class="tablelabel"><nobr>
						<input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/editPeople.do?peopleId=<%=peopleVer1.getPeopleId()%>&psaType=<%=peopleVer1.getPsaType()%>'" class="button">&nbsp;</nobr>
					</td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
						<td width="25%" class="tablelabel">Type</td>
						<td width="40%" class="tabletext" >Contact</td>
						<td class="tabletext">Contact</td>
                    </tr>
                    <tr>
						<td class="tablelabel">Name</td>
						<%
							name = peopleVer1.getName();
							if(name == null) name = "";
						%>
						<td <%if(peopleVer1.getName().equalsIgnoreCase(peopleVer2.getName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=name%></td>
						<%
							name = peopleVer2.getName();
							if(name == null) name = "";
						%>
						<td <%if(peopleVer1.getName().equalsIgnoreCase(peopleVer2.getName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=name%></td>
					</tr>
					<tr>
						<td class="tablelabel">Address</td>
						<%
							address = peopleVer1.getAddress();
							if(address == null) address = "";
						%>
						<td <%if(peopleVer1.getAddress().equalsIgnoreCase(peopleVer2.getAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=address%></td>
						<%
							address = peopleVer2.getAddress();
							if(address == null) address = "";
						%>
						<td <%if(peopleVer1.getAddress().equalsIgnoreCase(peopleVer2.getAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=address%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">City, State, Zip</td>
						<%
							city = peopleVer1.getCity();
							if(city == null) city = "";

							state = peopleVer1.getState();
							if(state == null) state = "";

							zip = peopleVer1.getZipCode();
							if(zip == null) zip = "";
						%>
						<td <%if((peopleVer1.getCity().equalsIgnoreCase(peopleVer2.getCity())) && (peopleVer1.getState().equalsIgnoreCase(peopleVer2.getState())) && (peopleVer1.getZipCode().equalsIgnoreCase(peopleVer2.getZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=city%> <%=state%> <%=zip%></td>
						<%
							city = peopleVer2.getCity();
							if(city == null) city = "";

							state = peopleVer2.getState();
							if(state == null) state = "";

							zip = peopleVer2.getZipCode();
							if(zip == null) zip = "";
						%>
						<td  <%if((peopleVer1.getCity().equalsIgnoreCase(peopleVer2.getCity())) && (peopleVer1.getState().equalsIgnoreCase(peopleVer2.getState())) && (peopleVer1.getZipCode().equalsIgnoreCase(peopleVer2.getZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=city%> <%=state%> <%=zip%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Phone, Ext</td>
						<%
							phone = peopleVer1.getPhoneNbr();
							if(phone == null) phone = "";

							ext = peopleVer1.getExt();
							if(ext == null) ext = "";
						%>
						<td  <%if((peopleVer1.getPhoneNbr().equalsIgnoreCase(peopleVer2.getPhoneNbr())) && (peopleVer1.getExt().equalsIgnoreCase(peopleVer2.getExt()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=phone%> <%=ext%></td>
						<%
							phone = peopleVer2.getPhoneNbr();
							if(phone == null) phone = "";

							ext = peopleVer2.getExt();
							if(ext == null) ext = "";
						%>
						<td <%if((peopleVer1.getPhoneNbr().equalsIgnoreCase(peopleVer2.getPhoneNbr())) && (peopleVer1.getExt().equalsIgnoreCase(peopleVer2.getExt()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=phone%> <%=ext%></td>
					</tr>
					<tr>
						<td class="tablelabel">Fax</td>
						<%
							fax = peopleVer1.getFaxNbr();
							if(fax == null) fax = "";
						%>
						<td <%if(peopleVer1.getFaxNbr().equalsIgnoreCase(peopleVer2.getFaxNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=fax%></td>
						<%
							fax = peopleVer2.getFaxNbr();
							if(fax == null) fax = "";
						%>
						<td <%if(peopleVer1.getFaxNbr().equalsIgnoreCase(peopleVer2.getFaxNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=fax%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Email</td>
						<%
							email = peopleVer1.getEmailAddress();
							if(email == null) email = "";
						%>
						<td <%if(peopleVer1.getEmailAddress().equalsIgnoreCase(peopleVer2.getEmailAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=email%></td>
						<%
							email = peopleVer2.getEmailAddress();
							if(email == null) email = "";
						%>
						<td <%if(peopleVer1.getEmailAddress().equalsIgnoreCase(peopleVer2.getEmailAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=email%></td>
					</tr>
					<tr>
						<td class="tablelabel">Copy as Applicant</td>
						<%
							copyApplicant = peopleVer1.getCopyApplicant();
							if(copyApplicant == null) copyApplicant = "";
						%>
						<td <%if(peopleVer1.getCopyApplicant().equalsIgnoreCase(peopleVer2.getCopyApplicant())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=copyApplicant%></td>
						<%
							copyApplicant = peopleVer2.getCopyApplicant();
							if(copyApplicant == null) copyApplicant = "";
						%>
						<td <%if(peopleVer1.getCopyApplicant().equalsIgnoreCase(peopleVer2.getCopyApplicant())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=copyApplicant%></td>
                   </tr>
                   <tr>
						<td class="tablelabel">Additional Comments </td>
						<%
							comments = peopleVer1.getComments();

							if(comments == null) comments = "";
						%>
						<td <%if(peopleVer1.getComments().equalsIgnoreCase(peopleVer2.getComments())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=comments%></td>
						<%
							comments = peopleVer2.getComments();

							if(comments == null) comments = "";
						%>
						<td <%if(peopleVer1.getComments().equalsIgnoreCase(peopleVer2.getComments())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=comments%></td>
                    </tr>

             </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

      </table>
    </td>

    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

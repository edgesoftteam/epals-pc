<%@ page import="elms.agent.*,java.util.ResourceBundle" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Add People</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>

<%
   String contextRoot = request.getContextPath();

   String addEditFlag = (String) request.getAttribute("addEditFlag");
   String peopleId = (String) request.getAttribute("peopleId");

   	String lsoAddress = (String)session.getAttribute("lsoAddress");
	if (lsoAddress == null ) lsoAddress = "";

	String psaInfo = (String)session.getAttribute("psaInfo");
	if (psaInfo == null ) psaInfo = "";

	String psaId = (String)request.getAttribute("psaId");
	if(psaId ==null) psaId ="";
	String psaType = (String)request.getAttribute("psaType");
	if(psaType ==null) psaType = "";

	String projectName="";
	if(psaType.equalsIgnoreCase("A")){
		try{
		projectName = LookupAgent.getProjectNameForActivityNumber(LookupAgent.getActivityNumberForActivityId(psaId));
		} catch(Exception e) { }
	}
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	//strValue=false;

	if (document.forms['peopleForm'].elements['strGeneralLiabilityDate'].value != '')
   	{
   		strValue=validateData('date',document.forms['peopleForm'].elements['strGeneralLiabilityDate'],'Invalid date format');
   	}

     if (strValue == true)
    {
    	if (document.forms['peopleForm'].elements['strAutoLiabilityDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['peopleForm'].elements['strAutoLiabilityDate'],'Invalid date format');
    	}
    }

   if (strValue==true){
	   if (document.forms['peopleForm'].elements['people.name'].value == '') {
	    	alert('Name is a required field');
	    	document.forms['peopleForm'].elements['people.name'].focus();
	    	strValue=false;
	    } else if ((document.forms['peopleForm'].elements['people.address'].value == '')) {
	    	alert('Address is a required field');
	    	document.forms['peopleForm'].elements['people.address'].focus();
	    	strValue=false;
		} else if (document.forms['peopleForm'].elements['people.city'].value == '') {
	    	alert('City is a required field');
	    	document.forms['peopleForm'].elements['people.city'].focus();
	    	strValue=false;
		} else if (document.forms['peopleForm'].elements['people.state'].value == '') {
			alert('State is a required field');
			document.forms['peopleForm'].elements['people.state'].focus();
			strValue=false;
	    } else if (document.forms['peopleForm'].elements['people.zipCode'].value == '') {
			alert('Zip Code is a required field');
			document.forms['peopleForm'].elements['people.zipCode'].focus();
			strValue=false;
	    }
   }

   if (strValue==true){
   if (document.forms['peopleForm'].elements['people.phoneNbr'].value == '')
    {
    	alert('Phone # is a required field');
    	document.forms['peopleForm'].elements['people.phoneNbr'].focus();
    	strValue=false;
    } }

    return strValue;
}
function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.peopleForm.elements[str].value.length == 3 ) || (document.peopleForm.elements[str].value.length == 7 ))
		{
			document.peopleForm.elements[str].value = document.peopleForm.elements[str].value +'-';
 		}
 		if (document.peopleForm.elements[str].value.length > 11 )  event.returnValue = false;
	}
}
function onclickprocess(strval)
{
  if (strval = 'ad') document.location='<%=contextRoot%>/addDeposit.do';
  if (strval = 'st') document.location='<%=contextRoot%>/viewTransaction.do';
}
function dohref(strHref){
   if (strHref == 'admin') parent.f_content.location.href="<%=contextRoot%>/searchPeople.do";
}

// Start of Check version People History
var no_array = new Array();
function checkVersions(val)
{
	var valNew=val.value;
	var checkboxArray = document.forms[0].checkboxArray.value;
	no_array=checkboxArray.split(",");

    if(val.checked == false )
	{

		document.forms[0].checkboxArray.value = removeFromArray(valNew,no_array).join(",");
     	checkboxArray = removeFromArray(valNew,no_array).join(",");
	    no_array=checkboxArray.split(",");
		valNew="";
	}

	if(valNew != "")
	{
	 checkboxArray += valNew+",";

	}

    if(no_array.length > 2)
	{
	val.checked=false;
    checkboxArray=document.forms[0].checkboxArray.value;
	alert("Only two versions can be compared");
	}

	document.forms[0].checkboxArray.value = checkboxArray;
	return true;

}

function removeFromArray(val, ar){
s = String(ar)
// remove if not first item (global search)
reRemove = new RegExp(","+val,"g")
s = s.replace(reRemove,"")
// remove if at start of array
reRemove = new RegExp("^"+val+",")
s = s.replace(reRemove,"")
// remove if only item
reRemove = new RegExp("^"+val+"$")
s = s.replace(reRemove,"")
return new Array(s)
}

function viewDifferences()
{
var checkboxArray = document.forms[0].checkboxArray.value;
no_array=checkboxArray.split(",");
if(no_array.length < 3)
	{
	alert("Please select two checkboxes to compare");
	}
   else{
    document.location='<%=contextRoot%>/versionCompare.do?peopleId=<%=peopleId%>&typeId=O&checkboxArray='+checkboxArray;
  }
}
function openUrl(){
	window.open('<%=elms.agent.LookupAgent.getContractorsStateLicenseBoardURL()%>');
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/savePeople" onsubmit="return validateFunction();">
<html:hidden property="checkboxArray" value=""/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg" width="98%">
                      <%=addEditFlag%> Other </td>

                      <td width="1%" class="tablelabel"><nobr>
                         <% if(lsoAddress.equalsIgnoreCase("") & psaInfo.equalsIgnoreCase("")){  %>
                          <input type="button" value="Back"  onclick = "dohref('admin');" class="button">
                          <% }else{  %>
                          <input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/listPeople.do?id=<%=psaId%>&type=<%=psaType%>'" class="button">
                          <% }  %>


                           <% if(addEditFlag.equalsIgnoreCase("edit")){  %>
                          <input type="button" value="Hold"  onclick = "document.location='<%=contextRoot%>/listHold.do?levelId=<%=peopleId%>&level=Z'" class="button">
                          <input type="button" value=" Activities "  onclick = "document.location='<%=contextRoot%>/viewAssociatedActivities.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>&name=<bean:write  name ="peopleForm"  property="people.name" />'" class="button">

                          <input type="button" value="Add Deposit"  onclick = "document.location='<%=contextRoot%>/addDeposit.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">
                          <input type="button" value="Show Transactions"  onclick= "document.location='<%=contextRoot%>/viewTransaction.do?peopleId=<%=peopleId%>&psaId=<%=psaId%>&psaType=<%=psaType%>'" class="button">

                          <html:button property="checkState"  value="Check State"  styleClass="button" onclick="openUrl()"/>
                          <%}%>
                          <html:submit  value="Add/Update" styleClass="button" />

                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Type</td>
                      <td class="tabletext" colspan="3">Other</td>
                   </tr>
                    <tr>
                      <td class="tablelabel">Name</td>
                      <td class="tabletext" colspan="3">
                         <html:text name = "peopleForm"   property="people.name" size="45" styleClass="textbox"/>
                     	 <html:hidden name = "peopleForm"  property="people.peopleId" />
                       	 <html:hidden name = "peopleForm"  property="people.licenseNbr" value="" />
                     	 <html:hidden name = "peopleForm"  property="psaId" />
                     	 <html:hidden name = "peopleForm"  property="psaType" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Address</td>
                      <td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.address" size="45" maxlength="60" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">City,
                        State, Zip</td>
                      <td class="tabletext" colspan="3">
                          <html:text  name = "peopleForm"   property="people.city" size="25" maxlength="20" styleClass="textbox" />
                          <html:text  name = "peopleForm"   property="people.state" size="2" maxlength="2" styleClass="textbox"  />
                          <html:text  name = "peopleForm"   property="people.zipCode" size="10" maxlength="9" styleClass="textbox" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Phone, Ext</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm" maxlength="12"  property="people.phoneNbr" size="25" styleClass="textbox" onkeypress="return DisplayHyphen('people.phoneNbr');" />&nbsp;
                          <html:text  name = "peopleForm" maxlength="4"  property="people.ext" size="4" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Fax</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm" property="people.faxNbr" size="25" maxlength="12" styleClass="textbox"
onkeypress="return DisplayHyphen('people.faxNbr');"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Email</td>
                      <td class="tabletext" colspan="3">
                          <html:text   name = "peopleForm"  property="people.emailAddress" size="25" maxlength="200" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Agent Name</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm"  property="people.agentName" size="25" maxlength="20" styleClass="textbox"/>&nbsp;
                      </td>
                      <td class="tablelabel">Agent Phone</td>
                      <td class="tabletext">
                          <html:text  name = "peopleForm" maxlength="12"  property="people.agentPhone" size="25" styleClass="textbox" onkeypress="return DisplayHyphen('people.agentPhone');" />&nbsp;
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">General
                        Liability Date</td>
                      <td class="tabletext"><nobr>
                          <html:text  name = "peopleForm"   property="strGeneralLiabilityDate" size="10" styleClass="textbox" />
					    <html:link href="javascript:show_calendar('peopleForm.strGeneralLiabilityDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
					  </td>
                      <td class="tablelabel">Auto
                        Liability Date</td>
                      <td class="tabletext"><nobr>
                        <html:text  name = "peopleForm"   property="strAutoLiabilityDate" size="10" styleClass="textbox" />
					    <html:link href="javascript:show_calendar('peopleForm.strAutoLiabilityDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
					  </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Additional
                        Comments </td>
                      <td class="tabletext" colspan="3">
                          <html:textarea  name = "peopleForm"  property="people.comments" cols="85" rows="3" styleClass="textbox" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
<!-- Change History Part -->
			<tr>
				<td>
				<!-- display of the People Version history list -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="99%" background="../images/site_bg_B7C1CB.jpg">
                                  People History
                               </td>
                               <td width="99%" background="../images/site_bg_B7C1CB.jpg">

                                  <input type="button" name="viewDifference"  value="View Difference"  style="button" onclick="javascript:viewDifferences()" />

                               </td>
                                <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="17" height="19"></td>
                                <td width="1%" class="tablelabel">
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                        <tr>
                                <td class="tablelabel">Select</td>
								<td class="tablelabel">Version Number</td>
		                        <td class="tablelabel">Updated</td>
		                        <td class="tablelabel"><nobr>Name </nobr></td>
		                        <td class="tablelabel"><nobr>Address</nobr></td>
		                        <td class="tablelabel">Phone</td>
		                     </tr>
							<%
							   PeopleAgent peopleAgent = new PeopleAgent();
								java.util.List peopleHistoryList=  new java.util.ArrayList();
									try{
							   peopleHistoryList = peopleAgent.getPeopleHistoryList(peopleId);
							    pageContext.setAttribute("peopleHistoryList",peopleHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<peopleHistoryList.size();i++){
									elms.control.beans.PeopleVersionForm peopleVersionForm = (elms.control.beans.PeopleVersionForm)peopleHistoryList.get(i);

							 %>

			                    <tr valign="top">
								  <td class="tabletext">
			                         <html:multibox property="versionDifference"  onclick="javascript:return checkVersions(this)">I</html:multibox>
								  </td>
			                      <td class="tabletext">
			                         Current Version
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getUpdated()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getAddress()%> <%=peopleVersionForm.getCity()%> <%=peopleVersionForm.getState()%> <%=peopleVersionForm.getZip()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getPhone()%>
			                      </td>

						    <%} %>
							<%

								java.util.List contractHistoryList=  new java.util.ArrayList();
									try{
							   contractHistoryList = peopleAgent.getContractHistoryList(peopleId);
							    pageContext.setAttribute("contractHistoryList",contractHistoryList);
								} catch(Exception e) { }


							   for(int i=0;i<contractHistoryList.size();i++){
									elms.control.beans.PeopleVersionForm peopleVersionForm = (elms.control.beans.PeopleVersionForm)contractHistoryList.get(i);

							 %>

			                    <tr valign="top">
								  <td class="tabletext">
			                         <html:multibox property="versionDifference"  onclick="javascript:return checkVersions(this)"><%=peopleVersionForm.getVersionNumber()%></html:multibox>
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getVersionNumber()%>
								  </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getUpdated()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getName()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getAddress()%> <%=peopleVersionForm.getCity()%> <%=peopleVersionForm.getState()%> <%=peopleVersionForm.getZip()%>
			                      </td>
			                      <td class="tabletext">
			                         <%=peopleVersionForm.getPhone()%>
			                      </td>

						    <%} %>
                        </table>
                        </td>
                    </tr>
<!-- End Change History Part -->

    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

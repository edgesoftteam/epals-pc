<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>

<%
  String levelId = (String)request.getAttribute("psaId");
  String level = (String)request.getAttribute("psaType");
%>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Add Contractor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath();%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script>
function addPeople()
{
    document.location='<%=contextRoot%>/addPeople.do'
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
		document.location='<%=contextRoot%>/addPeople.do'
	}

}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name ="peopleSearchForm" type="elms.control.beans.PeopleSearchForm" action="">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="peopleSearchForm" property="lsoAddress" /><bean:write name="peopleSearchForm" property="psaInfo" /><br><br>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98%" class="tabletitle">Search Results</td>
                      <td width="1%"class="tablebutton"><nobr>
                      	<security:editable levelId="<%=levelId%>" levelType="<%=level%>" editProperty="checkUser">
                      	<html:button  property = "new" value="New" onclick="addPeople()" styleClass="button"/>
                      	</security:editable>

                        &nbsp;&nbsp;
						<html:reset  value="Back" styleClass="button" onclick="history.back();"/>
		    			</nobr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel"></td>
                      <td class="tablelabel">Name</td>
                      <td class="tablelabel">License</td>
                      <td class="tablelabel">eMail</td>
                     <td class="tablelabel">Phone</td>
                      <td class="tablelabel">Type</td>
                      <td class="tablelabel">Activity Count</td>
                    </tr>
                    <% int peopleCount = 0; %>
		    <logic:iterate id="people" name="peopleSearchForm" property="peopleList">
                    <% peopleCount++; %>
                    <tr class="tabletext">
                      <td class="tabletext">
                        <%= peopleCount %>
                      </td>
                      <td class="tabletext">
                      	<a href='<%=contextRoot%>/editPeople.do?peopleId=<bean:write name="people" property="peopleId" />&psaId=<bean:write name="peopleSearchForm" property="psaId" />&psaType=<bean:write name="peopleSearchForm" property="psaType" />'>
                      	  <logic:equal name="people" property="onHold" value="Y">
                      	  	<font class="red1">
                      	  </logic:equal><bean:write name="people" property="name" />
                      	</a>
                      </td>
                      <td class="tabletext">
                        <bean:write name="people" property="licenseNbr" />
                      </td>
                      <td class="tabletext">
                        <bean:write name="people" property="emailAddress" />
                      </td>
                      <td class="tabletext">
                        <bean:write name="people" property="phoneNbr" />
                      </td>
                      <td class="tabletext">
                        <bean:write name="people" property="peopleType.type" />
                      </td>
                     <td class="tabletext">
                        <bean:write name="people" property="activityCount" />
                      </td>
                    </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>

    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<html:hidden name = "peopleForm" property="people.peopleType.code" />
</html:form>
</body>
</html:html>


<%@ page import="elms.app.people.*,elms.util.*,elms.common.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>
<%!
int versionNo = 0;
String name = "";
String hic = "";
String address = "";
String city = "";
String state = "";
String zip = "";
String homeAddress = "";
String homeCity = "";
String homeState = "";
String homeZip = "";
String mailingAddress = "";
String mailingCity = "";
String mailingState = "";
String mailingZip = "";
String phone = "";
String homePhone = "";
String ext = "";
String fax = "";
String email = "";
String copyApplicant = "";
String comments = "";
String title="";
String firstName ="";
String businessName="";
String corporateName="";
String agentName="";
String agentPhone="";
String generalLiabilityDate="";
String autoLiabilityDate="";
String dateOfBirth="";
String dlIdNumber="";
String dlIdExpDate="";
String hairColor="";
String eyeColor="";
String height="";
String weight="";
String gender="";
String ssn="";
boolean businessLicenseUser = false;
%>
<%
	String contextRoot = request.getContextPath();
	elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);
    boolean lcUser=false,pdUser = false;
   	if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("PD")) pdUser = true;
   	if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("LC")) lcUser = true;

%>
<%

People peopleVer1= (People) request.getAttribute("peopleVer1");
People peopleVer2= (People) request.getAttribute("peopleVer2");
%>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<html:form action="/savePeople">
<html:hidden property="checkboxArray" />
<table width="100%" border="0" cellspacing="10" cellpadding="0"  >
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="40%">Applicant</td>
						<%
							versionNo = peopleVer1.getVersionNumber();
						%>
                      <td class="tabletitle" width="40%"><%if(versionNo == 0){%>Current Version<%}else{%>Version <%=versionNo%><%}%></td>
						<%
							versionNo = peopleVer2.getVersionNumber();
						%>
                      <td class="tabletitle" width="40%"><%if(versionNo == 0){%>Current Version<%}else{%>Version <%=versionNo%><%}%></td>

                      <td width="1%" class="tablebutton"><nobr>
						<input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/editPeople.do?peopleId=<%=peopleVer1.getPeopleId()%>&psaType=<%=peopleVer1.getPsaType()%>'" class="button">&nbsp;</nobr>
					</td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
						<td width="25%" class="tablelabel">Type</td>
						<td width="40%" class="tabletext" >Applicant</td>
						<td class="tabletext">Applicant</td>
                    </tr>


                   <tr>
						<td class="tablelabel">Title</td>
						<%
							title = peopleVer1.getTitle();
							if(title == null) title = "";
						%>
						<td <%if(peopleVer1.getTitle().equalsIgnoreCase(peopleVer2.getTitle())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=title%></td>
						<%
							title = peopleVer2.getTitle();
							if(title == null) title = "";
						%>
						<td <%if(peopleVer1.getTitle().equalsIgnoreCase(peopleVer2.getTitle())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=title%></td>
					</tr>
					 <tr>
						<td class="tablelabel">First Name</td>
						<%
							name = peopleVer1.getName();
							if(name == null) name = "";
						%>
						<td <%if(peopleVer1.getName().equalsIgnoreCase(peopleVer2.getName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=name%></td>
						<%
							name = peopleVer2.getName();
							if(name == null) name = "";
						%>
						<td <%if(peopleVer1.getName().equalsIgnoreCase(peopleVer2.getName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=name%></td>
					</tr>
					<tr>
						<td class="tablelabel">Last Name</td>
						<%
							firstName = peopleVer1.getFirstName();
							if(firstName == null) firstName = "";
						%>
						<td <%if(peopleVer1.getFirstName().equalsIgnoreCase(peopleVer2.getFirstName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=firstName%></td>
						<%
							firstName = peopleVer2.getFirstName();
							if(firstName == null) firstName = "";
						%>
						<td <%if(peopleVer1.getFirstName().equalsIgnoreCase(peopleVer2.getFirstName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=firstName%></td>
					</tr>

					<% if((lcUser)||(pdUser)){%>
					 <tr>
						<td class="tablelabel">SSN</td>
						<%
							ssn = peopleVer1.getSsn();
							if(ssn == null) ssn = "";
						%>
						<td <%if(peopleVer1.getSsn().equalsIgnoreCase(peopleVer2.getSsn())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=ssn%></td>
						<%
							ssn = peopleVer2.getSsn();
							if(ssn == null) ssn = "";
						%>
						<td <%if(peopleVer1.getSsn().equalsIgnoreCase(peopleVer2.getSsn())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=ssn%></td>
					</tr>
					 <tr>
						<td class="tablelabel">Eye Color</td>
						<%
							eyeColor = peopleVer1.getEyeColor();
							if(eyeColor == null) eyeColor = "";
						%>
						<td <%if(peopleVer1.getEyeColor().equalsIgnoreCase(peopleVer2.getEyeColor())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=eyeColor%></td>
						<%
							eyeColor = peopleVer2.getEyeColor();
							if(eyeColor == null) eyeColor = "";
						%>
						<td <%if(peopleVer1.getEyeColor().equalsIgnoreCase(peopleVer2.getEyeColor())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=eyeColor%></td>
					</tr>

					 <tr>
						<td class="tablelabel">Hair Color</td>
						<%
							hairColor = peopleVer1.getHairColor();
							if(hairColor == null) hairColor = "";
						%>
						<td <%if(peopleVer1.getHairColor().equalsIgnoreCase(peopleVer2.getHairColor())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=hairColor%></td>
						<%
							hairColor = peopleVer2.getHairColor();
							if(hairColor == null) hairColor = "";
						%>
						<td <%if(peopleVer1.getHairColor().equalsIgnoreCase(peopleVer2.getHairColor())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=hairColor%></td>
					</tr>

					 <tr>
						<td class="tablelabel">Gender</td>
						<%
							gender = peopleVer1.getGender();
							if(gender == null) gender = "";
						%>
						<td <%if(peopleVer1.getGender().equalsIgnoreCase(peopleVer2.getGender())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=gender%></td>
						<%
							gender = peopleVer2.getGender();
							if(gender == null) gender = "";
						%>
						<td <%if(peopleVer1.getGender().equalsIgnoreCase(peopleVer2.getGender())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=gender%></td>
					</tr>
					 <tr>
						<td class="tablelabel">DL/ID Number</td>
						<%
							dlIdNumber = peopleVer1.getDlNumber();
							if(dlIdNumber == null) dlIdNumber = "";
						%>
						<td <%if(peopleVer1.getDlNumber().equalsIgnoreCase(peopleVer2.getDlNumber())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=dlIdNumber%></td>
						<%
							dlIdNumber = peopleVer2.getDlNumber();
							if(dlIdNumber == null) dlIdNumber = "";
						%>
						<td <%if(peopleVer1.getDlNumber().equalsIgnoreCase(peopleVer2.getDlNumber())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=dlIdNumber%></td>
					</tr>

					 <tr>
						<td class="tablelabel">DL/ID Expiration Date</td>
						<%
							dlIdExpDate = StringUtils.cal2str(peopleVer1.getDlExpiryDate());
							if(dlIdExpDate == null) dlIdExpDate = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getDlExpiryDate()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getDlExpiryDate()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=dlIdExpDate%></td>
						<%
							dlIdExpDate = StringUtils.cal2str(peopleVer2.getDlExpiryDate());
							if(dlIdExpDate == null) dlIdExpDate = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getDlExpiryDate()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getDlExpiryDate()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=dlIdExpDate%></td>
					</tr>
					<tr>
						<td class="tablelabel">Height</td>
						<%
							height = peopleVer1.getHeight();
							if(height == null) height = "";
						%>
						<td <%if(peopleVer1.getHeight().equalsIgnoreCase(peopleVer2.getHeight())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=height%></td>
						<%
							height = peopleVer2.getHeight();
							if(height == null) height = "";
						%>
						<td <%if(peopleVer1.getHeight().equalsIgnoreCase(peopleVer2.getHeight())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=height%></td>
					</tr>
                    <tr>
						<td class="tablelabel">Weight</td>
						<%
							weight = peopleVer1.getWeight();
							if(weight == null) weight = "";
						%>
						<td <%if(peopleVer1.getWeight().equalsIgnoreCase(peopleVer2.getWeight())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=weight%></td>
						<%
							weight = peopleVer2.getWeight();
							if(weight == null) weight = "";
						%>
						<td <%if(peopleVer1.getWeight().equalsIgnoreCase(peopleVer2.getWeight())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=weight%></td>
					</tr>

					<% }else{%>
					 <tr>
						<td class="tablelabel">SSN</td>
						<%
							ssn = peopleVer1.getSsn();
							if(ssn == null) ssn = "";
						%>
						<td <%if(peopleVer1.getSsn().equalsIgnoreCase(peopleVer2.getSsn())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>>XXX-XX-XXXX</td>
						<%
							ssn = peopleVer2.getSsn();
							if(ssn == null) ssn = "";
						%>
						<td <%if(peopleVer1.getSsn().equalsIgnoreCase(peopleVer2.getSsn())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>>XXX-XX-XXXX</td>
					</tr>
					<%}%>

					 <tr>
						<td class="tablelabel">Date Of Birth</td>
						<%
							dateOfBirth = StringUtils.cal2str(peopleVer1.getDateOfBirth());
							if(dateOfBirth == null) dateOfBirth = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getDateOfBirth()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getDateOfBirth()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=dateOfBirth%></td>
						<%
							dateOfBirth = StringUtils.cal2str(peopleVer2.getDateOfBirth());
							if(dateOfBirth == null) dateOfBirth = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getDateOfBirth()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getDateOfBirth()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=dateOfBirth%></td>
					</tr>




					 <tr>
						<td class="tablelabel">Business Name</td>
						<%
							businessName = peopleVer1.getBusinessName();
							if(businessName == null) businessName = "";
						%>
						<td <%if(peopleVer1.getBusinessName().equalsIgnoreCase(peopleVer2.getBusinessName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=businessName%></td>
						<%
							businessName = peopleVer2.getBusinessName();
							if(businessName == null) businessName = "";
						%>
						<td <%if(peopleVer1.getBusinessName().equalsIgnoreCase(peopleVer2.getBusinessName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=businessName%></td>
					</tr>
					 <tr>
						<td class="tablelabel">Corporate Name</td>
						<%
							corporateName = peopleVer1.getCorporateName();
							if(corporateName == null) corporateName = "";
						%>
						<td <%if(peopleVer1.getCorporateName().equalsIgnoreCase(peopleVer2.getCorporateName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=corporateName%></td>
						<%
							corporateName = peopleVer2.getCorporateName();
							if(corporateName == null) corporateName = "";
						%>
						<td <%if(peopleVer1.getCorporateName().equalsIgnoreCase(peopleVer2.getCorporateName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=corporateName%></td>
					</tr>
					<tr>
						<td class="tablelabel">Business Address</td>
						<%
							address = peopleVer1.getAddress();
							if(address == null) address = "";

							city = peopleVer1.getCity();
							if(city == null) city = "";

							state = peopleVer1.getState();
							if(state == null) state = "";

							zip = peopleVer1.getZipCode();
							if(zip == null) zip = "";


						%>
						<td <%if((peopleVer1.getAddress().equalsIgnoreCase(peopleVer2.getAddress())) && (peopleVer1.getCity().equalsIgnoreCase(peopleVer2.getCity())) && (peopleVer1.getState().equalsIgnoreCase(peopleVer2.getState())) && (peopleVer1.getZipCode().equalsIgnoreCase(peopleVer2.getZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=address%> <%=city%> <%=state%> <%=zip%></td>
						<%
							address = peopleVer2.getAddress();
							if(address == null) address = "";

							city = peopleVer2.getCity();
							if(city == null) city = "";

							state = peopleVer2.getState();
							if(state == null) state = "";

							zip = peopleVer2.getZipCode();
							if(zip == null) zip = "";


						%>
						<td  <%if((peopleVer1.getAddress().equalsIgnoreCase(peopleVer2.getAddress())) && (peopleVer1.getCity().equalsIgnoreCase(peopleVer2.getCity())) && (peopleVer1.getState().equalsIgnoreCase(peopleVer2.getState())) && (peopleVer1.getZipCode().equalsIgnoreCase(peopleVer2.getZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=address%> <%=city%> <%=state%> <%=zip%></td>
                    </tr>

					<tr>
						<td class="tablelabel">Home Address</td>
						<%
							homeAddress = peopleVer1.getHomeAddress();
							if(homeAddress == null) homeAddress = "";

							homeCity = peopleVer1.getHomeCity();
							if(homeCity == null) homeCity = "";

							homeZip = peopleVer1.getHomeZipCode();
							if(homeZip == null) homeZip = "";

							homeState = peopleVer1.getHomeState();
							if(homeState == null) homeState = "";


						%>
						<td <%if((peopleVer1.getHomeAddress().equalsIgnoreCase(peopleVer2.getHomeAddress())) && (peopleVer1.getHomeCity().equalsIgnoreCase(peopleVer2.getHomeCity())) && (peopleVer1.getHomeState().equalsIgnoreCase(peopleVer2.getHomeState())) && (peopleVer1.getHomeZipCode().equalsIgnoreCase(peopleVer2.getHomeZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=homeAddress%> <%=homeCity%> <%=homeState%> <%=homeZip%></td>
						<%
							homeAddress = peopleVer2.getHomeAddress();
							if(homeAddress == null) homeAddress = "";

							homeCity = peopleVer2.getHomeCity();
							if(homeCity == null) homeCity = "";

							homeZip = peopleVer2.getHomeZipCode();
							if(homeZip == null) homeZip = "";

							homeState = peopleVer2.getHomeState();
							if(homeState == null) homeState = "";


						%>
						<td <%if((peopleVer1.getHomeAddress().equalsIgnoreCase(peopleVer2.getHomeAddress())) && (peopleVer1.getHomeCity().equalsIgnoreCase(peopleVer2.getHomeCity())) && (peopleVer1.getHomeState().equalsIgnoreCase(peopleVer2.getHomeState())) && (peopleVer1.getHomeZipCode().equalsIgnoreCase(peopleVer2.getHomeZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=homeAddress%> <%=homeCity%> <%=homeState%> <%=homeZip%></td>
                    </tr>
					<tr>
						<td class="tablelabel">Mailing Address</td>
						<%
							mailingAddress = peopleVer1.getMailingAddress();
							if(mailingAddress == null) mailingAddress = "";

							mailingCity = peopleVer1.getMailingCity();
							if(mailingCity == null) mailingCity = "";

							mailingZip = peopleVer1.getMailingZipCode();
							if(mailingZip == null) mailingZip = "";

							mailingState = peopleVer1.getMailingState();
							if(mailingState == null) mailingState = "";


						%>
						<td <%if((peopleVer1.getMailingAddress().equalsIgnoreCase(peopleVer2.getMailingAddress())) && (peopleVer1.getMailingCity().equalsIgnoreCase(peopleVer2.getMailingCity())) && (peopleVer1.getMailingState().equalsIgnoreCase(peopleVer2.getMailingState())) && (peopleVer1.getMailingZipCode().equalsIgnoreCase(peopleVer2.getMailingZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=mailingAddress%> <%=mailingCity%> <%=mailingState%> <%=mailingZip%></td>
						<%
							mailingAddress = peopleVer2.getMailingAddress();
							if(mailingAddress == null) mailingAddress = "";

							mailingCity = peopleVer2.getMailingCity();
							if(mailingCity == null) mailingCity = "";

							mailingZip = peopleVer2.getMailingZipCode();
							if(mailingZip == null) mailingZip = "";

							mailingState = peopleVer2.getMailingState();
							if(mailingState == null) mailingState = "";


						%>
						<td <%if((peopleVer1.getMailingAddress().equalsIgnoreCase(peopleVer2.getMailingAddress())) && (peopleVer1.getMailingCity().equalsIgnoreCase(peopleVer2.getMailingCity())) && (peopleVer1.getMailingState().equalsIgnoreCase(peopleVer2.getMailingState())) && (peopleVer1.getMailingZipCode().equalsIgnoreCase(peopleVer2.getMailingZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=mailingAddress%> <%=mailingCity%> <%=mailingState%> <%=mailingZip%></td>
                    </tr>
                   <tr>
						<td class="tablelabel">Business Phone, Ext</td>
						<%
							phone = peopleVer1.getPhoneNbr();
							if(phone == null) phone = "";

							ext = peopleVer1.getExt();
							if(ext == null) ext = "";
						%>
						<td  <%if((peopleVer1.getPhoneNbr().equalsIgnoreCase(peopleVer2.getPhoneNbr())) && (peopleVer1.getExt().equalsIgnoreCase(peopleVer2.getExt()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=phone%> <%=ext%></td>
						<%
							phone = peopleVer2.getPhoneNbr();
							if(phone == null) phone = "";

							ext = peopleVer2.getExt();
							if(ext == null) ext = "";
						%>
						<td <%if((peopleVer1.getPhoneNbr().equalsIgnoreCase(peopleVer2.getPhoneNbr())) && (peopleVer1.getExt().equalsIgnoreCase(peopleVer2.getExt()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=phone%> <%=ext%></td>
					</tr>
					<tr>
						<td class="tablelabel">Fax</td>
						<%
							fax = peopleVer1.getFaxNbr();
							if(fax == null) fax = "";
						%>
						<td <%if(peopleVer1.getFaxNbr().equalsIgnoreCase(peopleVer2.getFaxNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=fax%></td>
						<%
							fax = peopleVer2.getFaxNbr();
							if(fax == null) fax = "";
						%>
						<td <%if(peopleVer1.getFaxNbr().equalsIgnoreCase(peopleVer2.getFaxNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=fax%></td>
                    </tr>
					<tr>
						<td class="tablelabel">Home Phone</td>
						<%
							homePhone = peopleVer1.getHomePhone();
							if(homePhone == null) homePhone = "";
						%>
						<td <%if(peopleVer1.getHomePhone().equalsIgnoreCase(peopleVer2.getHomePhone())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=homePhone%></td>
						<%
							homePhone = peopleVer2.getHomePhone();
							if(homePhone == null) homePhone = "";
						%>
						<td <%if(peopleVer1.getHomePhone().equalsIgnoreCase(peopleVer2.getHomePhone())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=homePhone%></td>
                   </tr>
                    <tr>
						<td class="tablelabel">Email</td>
						<%
							email = peopleVer1.getEmailAddress();
							if(email == null) email = "";
						%>
						<td <%if(peopleVer1.getEmailAddress().equalsIgnoreCase(peopleVer2.getEmailAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=email%></td>
						<%
							email = peopleVer2.getEmailAddress();
							if(email == null) email = "";
						%>
						<td <%if(peopleVer1.getEmailAddress().equalsIgnoreCase(peopleVer2.getEmailAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=email%></td>
					</tr>
                    <tr>
						<td class="tablelabel">Additional Comments </td>
						<%
							comments = peopleVer1.getComments();

							if(comments == null) comments = "";
						%>
						<td <%if(peopleVer1.getComments().equalsIgnoreCase(peopleVer2.getComments())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=comments%></td>
						<%
							comments = peopleVer2.getComments();

							if(comments == null) comments = "";
						%>
						<td <%if(peopleVer1.getComments().equalsIgnoreCase(peopleVer2.getComments())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=comments%></td>
                    </tr>

             </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

      </table>
    </td>

    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

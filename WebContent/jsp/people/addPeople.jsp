<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Add People</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/addPeople">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%">Add
                        People</td>

                      <td width="1%" class="tablebutton"><nobr>
                          <html:submit  value="Cancel"  styleClass="button" onclick="javascript:document.location='../psa/viewActivityMgr.html'" />
                          <html:submit  value="Check State"  styleClass="button"/>
                          <html:submit  value="Save" styleClass="button" />
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Type</td>
                      <td class="tabletext" colspan="3">Architect
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">License
                        No.</td>
                      <td class="tabletext">

                          <html:text  property="licenseNo" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">License
                        Expiration Date</td>
                      <td class="tabletext"><nobr>

                          <html:text  property="licenseExpirationDate" size="10" styleClass="textbox"/>

                        <img src="../images/calendar.gif" width="16" height="15" border=0/></nobr>
                    </tr>
                    <tr>
                      <td class="tablelabel">Name</td>
                      <td class="tabletext" colspan="3">
                          <html:text  property="name" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Address</td>
                      <td class="tabletext" colspan="3">
                          <html:text  property="address" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">City,
                        State, Zip</td>
                      <td class="tabletext" colspan="3">
                          <html:text  property="city" size="25" styleClass="textbox" />
                          <html:text  property="state" size="2" styleClass="textbox" value="CA" />
                          <html:text  property="zip" size="10" styleClass="textbox" value="90210-"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Phone</td>
                      <td class="tabletext">
                          <html:text  property="phone" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Fax</td>
                      <td class="tabletext">
                          <html:text  property="fax" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Email</td>
                      <td class="tabletext">
                          <html:text  property="email" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Copy
                        Applicant?</td>
                      <td class="tabletext">Yes


                          <html:radio  property="copyApplicant" value="" />
                        &nbsp;&nbsp;No
                          <html:radio  property="copyApplicant" value="" />

                        </td>
                    </tr>
                    <tr>
                      <td class="tablelabel">Additional
                        Comments </td>
                      <td class="tabletext" colspan="3">
                          <html:textarea property="additionalComments" cols="25" rows="3" styleClass="textbox" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

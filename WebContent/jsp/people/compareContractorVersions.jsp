<%@ page import="elms.app.people.*,elms.util.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>
<%!
int versionNo = 0;
String licenseNo = "";
String licenseExpDt = "";
String businessLicenseNbr = "";
String businessLicenseExpDt = "";
String name = "";
String hic = "";
String address = "";
String city = "";
String state = "";
String zip = "";
String phone = "";
String ext = "";
String fax = "";
String email = "";
String copyApplicant = "";
String autoLiabilityDate = "";
String generalLiabilityDate = "";
String workersCompExpires = "";
String workersCompensationWaive = "";
String comments = "";
%>
<%
People peopleVer1= (People) request.getAttribute("peopleVer1");
People peopleVer2= (People) request.getAttribute("peopleVer2");
String contextRoot = request.getContextPath();
%>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : People Manager : Compare Versions</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<html:form action="/savePeople">
<html:hidden property="checkboxArray" />
<table width="100%" border="0" cellspacing="10" cellpadding="0"  >
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">People Manager&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td background="../images/site_bg_B7C1CB.jpg" width="40%">Contractor</td>
						<%
							versionNo = peopleVer1.getVersionNumber();
						%>
                      <td background="../images/site_bg_B7C1CB.jpg" width="40%"><%if(versionNo == 0){%>Current Version<%}else{%>Version <%=versionNo%><%}%></td>
						<%
							versionNo = peopleVer2.getVersionNumber();
						%>
                      <td background="../images/site_bg_B7C1CB.jpg" width="40%"><%if(versionNo == 0){%>Current Version<%}else{%>Version <%=versionNo%><%}%></td>

                      <td width="1%" class="tablelabel"><nobr>
						<input type="button" value="Back"  onclick = "document.location='<%=contextRoot%>/editPeople.do?peopleId=<%=peopleVer1.getPeopleId()%>&psaType=<%=peopleVer1.getPsaType()%>'" class="button">&nbsp;</nobr>
					</td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
						<td width="25%" class="tablelabel">Type</td>
						<td width="40%" class="tabletext" >Contractor</td>
						<td class="tabletext">Contractor</td>
                    </tr>
                    <tr>
						<td class="tablelabel">License No.</td>
						<%
							licenseNo = peopleVer1.getLicenseNbr();
							if(licenseNo == null) licenseNo = "";
						%>
						<td <%if(peopleVer1.getLicenseNbr().equalsIgnoreCase(peopleVer2.getLicenseNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=licenseNo%></td>
						<%
							licenseNo = peopleVer2.getLicenseNbr();
							if(licenseNo == null) licenseNo = "";
						%>
						<td <%if(peopleVer1.getLicenseNbr().equalsIgnoreCase(peopleVer2.getLicenseNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=licenseNo%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">License Expiration Date</td>
						<%
							licenseExpDt = StringUtils.cal2str(peopleVer1.getLicenseExpires());
							if(licenseExpDt == null) licenseExpDt = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getLicenseExpires()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getLicenseExpires()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=licenseExpDt%></td>
						<%
							licenseExpDt = StringUtils.cal2str(peopleVer2.getLicenseExpires());
							if(licenseExpDt == null) licenseExpDt = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getLicenseExpires()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getLicenseExpires()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=licenseExpDt%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Business License No.</td>
						<%
							businessLicenseNbr = peopleVer1.getBusinessLicenseNbr();
							if(businessLicenseNbr == null) businessLicenseNbr = "";
						%>
						<td <%if(peopleVer1.getBusinessLicenseNbr().equalsIgnoreCase(peopleVer2.getBusinessLicenseNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=businessLicenseNbr%></td>
						<%
							businessLicenseNbr = peopleVer2.getBusinessLicenseNbr();
							if(businessLicenseNbr == null) businessLicenseNbr = "";
						%>
						<td <%if(peopleVer1.getBusinessLicenseNbr().equalsIgnoreCase(peopleVer2.getBusinessLicenseNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=businessLicenseNbr%></td>
					</tr>
					<tr>
						<td class="tablelabel">Business License Expiration Date</td>
						<%
							businessLicenseExpDt =StringUtils.cal2str(peopleVer1.getBusinessLicenseExpires());
							if(businessLicenseExpDt == null) businessLicenseExpDt = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getBusinessLicenseExpires()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getBusinessLicenseExpires()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=businessLicenseExpDt%></td>
						<%
							businessLicenseExpDt = StringUtils.cal2str(peopleVer2.getBusinessLicenseExpires());
							if(businessLicenseExpDt == null) businessLicenseExpDt = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getBusinessLicenseExpires()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getBusinessLicenseExpires()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=businessLicenseExpDt%></td>
					</tr>
                    <tr>
						<td class="tablelabel">Name</td>
						<%
							name = peopleVer1.getName();
							if(name == null) name = "";
						%>
						<td <%if(peopleVer1.getName().equalsIgnoreCase(peopleVer2.getName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=name%></td>
						<%
							name = peopleVer2.getName();
							if(name == null) name = "";
						%>
						<td <%if(peopleVer1.getName().equalsIgnoreCase(peopleVer2.getName())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=name%></td>
					</tr>
					<tr>
						<td class="tablelabel">HIC</td>
						<%
							hic = peopleVer1.getHicFlag();
							if(hic == null) hic = "";
						%>
						<td <%if(peopleVer1.getHicFlag().equalsIgnoreCase(peopleVer2.getHicFlag())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=hic%></td>
						<%
							hic = peopleVer2.getHicFlag();
							if(hic == null) hic = "";
						%>
						<td <%if(peopleVer1.getHicFlag().equalsIgnoreCase(peopleVer2.getHicFlag())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=hic%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Address</td>
						<%
							address = peopleVer1.getAddress();
							if(address == null) address = "";
						%>
						<td <%if(peopleVer1.getAddress().equalsIgnoreCase(peopleVer2.getAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=address%></td>
						<%
							address = peopleVer2.getAddress();
							if(address == null) address = "";
						%>
						<td <%if(peopleVer1.getAddress().equalsIgnoreCase(peopleVer2.getAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=address%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">City, State, Zip</td>
						<%
							city = peopleVer1.getCity();
							if(city == null) city = "";

							state = peopleVer1.getState();
							if(state == null) state = "";

							zip = peopleVer1.getZipCode();
							if(zip == null) zip = "";
						%>
						<td <%if((peopleVer1.getCity().equalsIgnoreCase(peopleVer2.getCity())) && (peopleVer1.getState().equalsIgnoreCase(peopleVer2.getState())) && (peopleVer1.getZipCode().equalsIgnoreCase(peopleVer2.getZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=city%> <%=state%> <%=zip%></td>
						<%
							city = peopleVer2.getCity();
							if(city == null) city = "";

							state = peopleVer2.getState();
							if(state == null) state = "";

							zip = peopleVer2.getZipCode();
							if(zip == null) zip = "";
						%>
						<td  <%if((peopleVer1.getCity().equalsIgnoreCase(peopleVer2.getCity())) && (peopleVer1.getState().equalsIgnoreCase(peopleVer2.getState())) && (peopleVer1.getZipCode().equalsIgnoreCase(peopleVer2.getZipCode()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=city%> <%=state%> <%=zip%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Phone, Ext</td>
						<%
							phone = peopleVer1.getPhoneNbr();
							if(phone == null) phone = "";

							ext = peopleVer1.getExt();
							if(ext == null) ext = "";
						%>
						<td  <%if((peopleVer1.getPhoneNbr().equalsIgnoreCase(peopleVer2.getPhoneNbr())) && (peopleVer1.getExt().equalsIgnoreCase(peopleVer2.getExt()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=phone%> <%=ext%></td>
						<%
							phone = peopleVer2.getPhoneNbr();
							if(phone == null) phone = "";

							ext = peopleVer2.getExt();
							if(ext == null) ext = "";
						%>
						<td <%if((peopleVer1.getPhoneNbr().equalsIgnoreCase(peopleVer2.getPhoneNbr())) && (peopleVer1.getExt().equalsIgnoreCase(peopleVer2.getExt()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=phone%> <%=ext%></td>
					</tr>
					<tr>
						<td class="tablelabel">Fax</td>
						<%
							fax = peopleVer1.getFaxNbr();
							if(fax == null) fax = "";
						%>
						<td <%if(peopleVer1.getFaxNbr().equalsIgnoreCase(peopleVer2.getFaxNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=fax%></td>
						<%
							fax = peopleVer2.getFaxNbr();
							if(fax == null) fax = "";
						%>
						<td <%if(peopleVer1.getFaxNbr().equalsIgnoreCase(peopleVer2.getFaxNbr())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=fax%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Email</td>
						<%
							email = peopleVer1.getEmailAddress();
							if(email == null) email = "";
						%>
						<td <%if(peopleVer1.getEmailAddress().equalsIgnoreCase(peopleVer2.getEmailAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=email%></td>
						<%
							email = peopleVer2.getEmailAddress();
							if(email == null) email = "";
						%>
						<td <%if(peopleVer1.getEmailAddress().equalsIgnoreCase(peopleVer2.getEmailAddress())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=email%></td>
					</tr>
					<tr>
						<td class="tablelabel">Copy as Applicant</td>
						<%
							copyApplicant = peopleVer1.getCopyApplicant();
							if(copyApplicant == null) copyApplicant = "";
						%>
						<td <%if(peopleVer1.getCopyApplicant().equalsIgnoreCase(peopleVer2.getCopyApplicant())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=copyApplicant%></td>
						<%
							copyApplicant = peopleVer2.getCopyApplicant();
							if(copyApplicant == null) copyApplicant = "";
						%>
						<td <%if(peopleVer1.getCopyApplicant().equalsIgnoreCase(peopleVer2.getCopyApplicant())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=copyApplicant%></td>
                   </tr>
                   <tr>
						<td class="tablelabel">General Liability Date</td>
						<%
							generalLiabilityDate = StringUtils.cal2str(peopleVer1.getGeneralLiabilityDate());
							if(generalLiabilityDate == null) generalLiabilityDate = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getGeneralLiabilityDate()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getGeneralLiabilityDate()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=generalLiabilityDate%></td>
						<%
							generalLiabilityDate = StringUtils.cal2str(peopleVer2.getGeneralLiabilityDate());
							if(generalLiabilityDate == null) generalLiabilityDate = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getGeneralLiabilityDate()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getGeneralLiabilityDate()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=generalLiabilityDate%></td>
					</tr>
					<tr>
						<td class="tablelabel">Auto Liability Date</td>
						<%
							autoLiabilityDate = StringUtils.cal2str(peopleVer1.getAutoLiabilityDate());
							if(autoLiabilityDate == null) autoLiabilityDate = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getAutoLiabilityDate()).equalsIgnoreCase(StringUtils.cal2str(peopleVer1.getAutoLiabilityDate()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=autoLiabilityDate%></td>
						<%
							autoLiabilityDate = StringUtils.cal2str(peopleVer2.getAutoLiabilityDate());
							if(autoLiabilityDate == null) autoLiabilityDate = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getAutoLiabilityDate()).equalsIgnoreCase(StringUtils.cal2str(peopleVer1.getAutoLiabilityDate()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=autoLiabilityDate%></td>
                    </tr>
                    <tr>
						<td class="tablelabel">Workers Comp Expiration Date</td>
						<%
							workersCompExpires = StringUtils.cal2str(peopleVer1.getWorkersCompExpires());
							if(workersCompExpires == null) workersCompExpires = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getWorkersCompExpires()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getWorkersCompExpires()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=workersCompExpires%></td>
						<%
							workersCompExpires = StringUtils.cal2str(peopleVer2.getWorkersCompExpires());
							if(workersCompExpires == null) workersCompExpires = "";
						%>
						<td <%if(StringUtils.cal2str(peopleVer1.getWorkersCompExpires()).equalsIgnoreCase(StringUtils.cal2str(peopleVer2.getWorkersCompExpires()))){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=workersCompExpires%></td>
					</tr>
					<tr>
						<td class="tablelabel">Workers  Comp Waiver Flag</td>
						<%
							workersCompensationWaive = peopleVer1.getWorkersCompensationWaive();
							if(workersCompensationWaive == null) workersCompensationWaive = "";
						%>
						<td <%if(peopleVer1.getWorkersCompensationWaive().equalsIgnoreCase(peopleVer2.getWorkersCompensationWaive())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=workersCompensationWaive%></td>
						<%
							workersCompensationWaive = peopleVer2.getWorkersCompensationWaive();
							if(workersCompensationWaive == null) workersCompensationWaive = "";
						%>
						<td <%if(peopleVer1.getWorkersCompensationWaive().equalsIgnoreCase(peopleVer2.getWorkersCompensationWaive())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=workersCompensationWaive%></td>
					</tr>
                    <tr>
						<td class="tablelabel">Additional Comments </td>
						<%
							comments = peopleVer1.getComments();

							if(comments == null) comments = "";
						%>
						<td <%if(peopleVer1.getComments().equalsIgnoreCase(peopleVer2.getComments())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=comments%></td>
						<%
							comments = peopleVer2.getComments();

							if(comments == null) comments = "";
						%>
						<td <%if(peopleVer1.getComments().equalsIgnoreCase(peopleVer2.getComments())){%>class="tabletext"<%}else {%>bgcolor="#CCFFCC" <%} %>><%=comments%></td>
                    </tr>

             </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

      </table>
    </td>

    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>

</html:html>

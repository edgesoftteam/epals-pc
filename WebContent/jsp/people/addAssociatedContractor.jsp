<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.app.people.People"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.project.*" %>
<%@ page import="elms.app.admin.*" %>
<%
	String contextRoot = request.getContextPath();
	String error = (String) request.getAttribute("error");
	if(error == null)
		error = "";
	
	String option = (String) request.getAttribute("option");
%>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/formValidations.js"></script>
<script type="text/javascript">
function validateFunction()
{
	if (document.forms['peopleForm'].elements['people.licenseNbr'].value == '') {
	     alert('Enter License number');
	     document.forms['peopleForm'].elements['people.licenseNbr'].focus();
	     return false
    }
	
   return true;
}

function addAssociatedContractor(contractorId,agentId)
{
    document.location='<%=contextRoot%>/addAssociatedContractor.do?agentId='+agentId+'&action=map&contractorId='+contractorId;
}

</script>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <html:form action="/addAssociatedContractor?action=save" onsubmit="return validateFunction();">
	  <table width="100%" border="0" cellspacing="10" cellpadding="0">
	    <tr valign="top">
	      <td width="99%">
	        <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <tr>
	            <td><font class="con_hdr_3b">ADD ASSOCIATED CONTRACTOR:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"> <%=session.getAttribute("name")%>
	                   <br>
	              <br>
	              </td>
	          </tr>
	          <tr>
	          <td>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	
	              <tr>
	                <td>
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                      <td class="tabletitle" width="98%">
	                      	Add Associated Contractor 
	                      </td>
	                      <td width="1%" class="tablebutton"><nobr>
	                        <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
							&nbsp;	                        
	                        <html:submit  value="Add" styleClass="button" />
	                        </td>
	                    </tr>
	                  </table>
	                </td>
	              </tr>
	
	              <tr>
	                <td background="../images/site_bg_B7C1CB.jpg">
	                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                    <tr>
	                      <td class="tablelabel">License No.</td>
	                      <td class="tablelabel">
	                          <html:text name = "peopleForm"  property="people.licenseNbr"  size="10" maxlength="30" styleClass="textbox" onkeypress="return validateInteger();"/>
	                          <html:hidden name = "peopleForm"  property="people.peopleId" />
	                          <html:hidden name = "peopleForm"  property="people.name" />
	                     	  <html:hidden name = "peopleForm"  property="psaId" />
    	                 	  <html:hidden name = "peopleForm"  property="psaType" />
	                      </td>
	                      </tr>
	                  </table>
	                </td>
	              </tr>
	              <tr>
			          <td>&nbsp;</td>
			      </tr>
	              <tr>
	              	<td class="red2"><%=error %></td>
	              </tr>
	              <%
	              	if(option !=null && option.equalsIgnoreCase("select")){
	              %>
	              	<tr>
                       <td background="../images/site_bg_B7C1CB.jpg">
	                       <table width="100%" border="0" cellspacing="1" cellpadding="2">
		                        <tr>
		                            <td class="tablelabel">Name</td>
			                        <td class="tablelabel">License Number</td>
			                        <td class="tablelabel">&nbsp;</td>
			                     </tr>
			                     <%
			                     	List<People> associatedContractorAgentList =  (List)request.getAttribute("contractorList");
									
			                     	if(associatedContractorAgentList != null && associatedContractorAgentList.size() > 0){
							   		
			                     		for(int ac=0; ac < associatedContractorAgentList.size(); ac++){
											People p =  associatedContractorAgentList.get(ac);

							 	%>
				                    <tr valign="top">
				                      <td class="tabletext">
				                         <%=p.getName() %>
									  </td>
				                      <td class="tabletext">
				                         <%=p.getLicenseNbr() %>
				                      </td>
				                      <td class="tabletext">
				                      <input type="button" name="addContractor"  value="Add"  class="button" onclick="javascript:addAssociatedContractor(<%=p.getPeopleId()%>,<bean:write  name ="peopleForm"  property="people.peopleId" />)" />
				                      </td>
								</tr>
						    <%} }%>
							</table>
                       </td>
                   </tr>
	              <% } %>
	            </table>
	          </td>
	        </tr>
	        </table>
	      </td>
	   </tr>
	 </table>
</html:form>
</body>
</html:html>


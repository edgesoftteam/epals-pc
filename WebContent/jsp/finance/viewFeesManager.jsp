<%@page import="elms.app.admin.ActivityType"%>
<%@page import="elms.util.StringUtils"%>
<%@ page import="elms.agent.*,elms.common.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon />

<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<bean:define id="user" name="user" type="elms.security.User"/>
<script>
<%!String activityId = "";
int actStatus = 0;
 %>

<%
String contextRoot = request.getContextPath();
activityId = request.getParameter("id");
			if (!(request.getParameter("id") == null)) {
                activityId = request.getParameter("id");
            }else {
                activityId = (String) request.getAttribute("id");
            }

String actNbr = (String) session.getAttribute("actNumber");
if (actNbr == null)	actNbr = "";
String lsoAddress = (String) session.getAttribute("lsoAddress");
if (lsoAddress == null)	lsoAddress = "";
String psaInfo = (String) session.getAttribute("psaInfo");
if (psaInfo == null) psaInfo = "";
String onloadScript = (String) request.getAttribute("onloadScript");


try{
actStatus = LookupAgent.getActivityStatusId(activityId);
if(activityId!=null && !activityId.equalsIgnoreCase("")){
lsoAddress =new ActivityAgent().getActivityAddressForId(StringUtils.s2i(activityId));
actNbr=LookupAgent.getActivityNumberForActivityId(activityId);
ActivityType activityType = LookupAgent.getActivityTypeForActId(StringUtils.s2i(activityId));
psaInfo=" -- "+actNbr+" - "+activityType.getDescription();
}
}catch(Exception e){}
String projectName = LookupAgent.getProjectNameForActivityNumber(actNbr);
if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){
	if (onloadScript == null) onloadScript = "frmLoad()";
}else{
	onloadScript = "";
}
String busTaxMin = (String) session.getAttribute("busTaxMin");
String busTaxMax = (String) session.getAttribute("busTaxMax");
String busTax = (String) session.getAttribute("busTax");

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="<%= contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
<link rel="stylesheet" href="../css/kendo.default-v2.min.css"/>
<script language="javascript" type="text/javascript" src="../script/kendo.all.min.js"></script>
<script language="JavaScript">
var busTaxMin = <%=busTaxMin%>;
var busTaxMax = <%=busTaxMax%>;
var busTax = <%=busTax%>;


function getBusinessTaxReport(actId) {
   window.open('<%=contextRoot%>/printBusTaxReceipt.do?actNbr=<%=actNbr%>&report=yes');
}
function validateUnits() {
	ii = parseInt(document.forms[1].elements['planCheckFeeCount'].value);
	for(i=0;i<ii;i++) {
		if ((document.forms[1].elements['planCheckFeeList[' + i + '].input'].value == 'I')&&  (document.forms[1].elements['planCheckFeeList[' + i + '].checked'].checked == true)&&  ((document.forms[1].elements['planCheckFeeList[' + i + '].feeUnits'].value == '') || (document.forms[1].elements['planCheckFeeList[' + i + '].feeUnits'].value == '0'))) {
			swal ("Please Enter # Units");
			document.forms[1].elements['planCheckFeeList[' + i + '].feeUnits'].focus();
			return false;
			}
	}

	//code added for development fees by Gayathri on 15thDec'08
    ii = parseInt(document.forms[1].elements['developmentFeeCount'].value);
	for(i=0;i<ii;i++) {
		if ((document.forms[1].elements['developmentFeeList[' + i + '].input'].value == 'I')&&  (document.forms[1].elements['developmentFeeList[' + i + '].checked'].checked == true)&&  ((document.forms[1].elements['developmentFeeList[' + i + '].feeUnits'].value == '') || (document.forms[1].elements['developmentFeeList[' + i + '].feeUnits'].value == '0'))) {
			swal ("Please Enter # Units");
			document.forms[1].elements['developmentFeeList[' + i + '].feeUnits'].focus();
			return false;
			}
	}

	ii = parseInt(document.forms[1].elements['activityFeeCount'].value);
	for(i=0;i<ii;i++) {
		if ((document.forms[1].elements['activityFeeList[' + i + '].input'].value == 'I')		&&  (document.forms[1].elements['activityFeeList[' + i + '].checked'].checked == true)		&&  ((document.forms[1].elements['activityFeeList[' + i + '].feeUnits'].value == '') || (document.forms[1].elements['activityFeeList[' + i + '].feeUnits'].value == '0'))) {
			swal ("Please Enter # Units");
			document.forms[1].elements['activityFeeList[' + i + '].feeUnits'].focus();
			return false;
			}
	}

	return true;
}

function calculateBusTax(name){


	if(busTaxMin == null || busTaxMax == null ||busTax == null)
	{
	     alert("Please configure Contractor Tax in Admin Screen In order to Calculate the Contractor Tax Fee Amount");
	     return false;
	}
	BUSINESS_TAX_MIN = busTaxMin;
    BUSINESS_TAX_MAX = busTaxMax;

	startIndex = name.indexOf('[')+1;
	endIndex = name.indexOf(']');
	index  = name.substring(startIndex,endIndex);
	if (document.forms[2].elements['businessTaxList[' + index + '].feeValuation'].value != "") {
		valuation = document.forms[2].elements['businessTaxList[' + index + '].feeValuation'].value;
		valuation = valuation.replace('$','');
		valuation = parseFloat(valuation.replace(',',''));
	//	tax = valuation * 0.00080;//changed by AB, was 77 cents before july 1,2008
	    tax = valuation * busTax;//changed by SV, july 15,2010
		if (tax < BUSINESS_TAX_MIN) {
            tax = BUSINESS_TAX_MIN;
        }
        else if (tax > BUSINESS_TAX_MAX) {
            tax = BUSINESS_TAX_MAX;
        }
		document.forms[2].elements['businessTaxList[' + index + '].feeAmount'].value = dollarFormatted(tax.toFixed(2));
	}
}

function resetForm() {
 		document.forms[1].action='<html:rewrite forward="viewFeesMgr"/>';
 		document.forms[1].submit();
}
// Add a new row to the business tax table. Make sure the last row has a peopleid in it
function addBusinessTaxRow() {
	i = parseInt(document.forms[2].elements['busTaxCount'].value);
	if (document.forms[2].elements['businessTaxList[' + i + '].peopleId'].value != "") {
		document.forms[2].action='<html:rewrite forward="addBusTaxFees" />';
		document.forms[2].submit();
		return true;
	}
}
function frmLoad(){
	ctr=parseInt(document.forms[2].elements['busTaxCount'].value);

	for(i=1;i<=ctr;i++)
		{
			b=(8*(i-1))+3;
			{
				document.forms[2].elements[b+3].disabled=true;
			}
		}

}
function copyValidate(indexValue){

	BUSINESS_TAX_MIN = busTaxMin;
    BUSINESS_TAX_MAX = busTaxMax;


     indexValue=indexValue.substring(16,17);
     if(document.forms[2].elements['businessTaxList[' + indexValue + '].checked'].checked == true)
     {
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].value  = document.forms[0].elements['valuation'].value;
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].readonly  = true;

			valuation = document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].value;
			valuation = valuation.replace('$','');
			re = /,/g;
			valuation = valuation.replace(re,'');
			valuation = parseFloat(valuation);

     	    tax = valuation * busTax;
			if (tax < BUSINESS_TAX_MIN) {
	            tax = BUSINESS_TAX_MIN;
	        }
	        else if (tax > BUSINESS_TAX_MAX) {
	            tax = BUSINESS_TAX_MAX;
	        }


			document.forms[2].elements['businessTaxList[' + indexValue + '].feeAmount'].value =   dollarFormatted(tax.toFixed(2));
	} else {
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeValuation'].value  = "";
            document.forms[2].elements['businessTaxList[' + indexValue + '].feeAmount'].value =   "";
     }

}

function checkUnit(indexValue,feeType){

		if (feeType == 1) {
			indexValue=indexValue.substring(17,19);
			indexValue=indexValue.replace(']','');
			//alert(indexValue);
			if(parseInt(document.forms[1].elements['planCheckFeeList[' + indexValue + '].feeUnits'].value) > 0) {
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked =true;
			}
			else{
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked =false;
			}

		}
		else if (feeType == 2) {

			indexValue=indexValue.substring(16,18);
			indexValue=indexValue.replace(']','');
			if(parseInt(document.forms[1].elements['activityFeeList[' + indexValue + '].feeUnits'].value) > 0) {
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked =true;
			}
			else{
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked =false;
			}
		}
	//code added for development fees by Gayathri on 15thDec'08
		else if (feeType == 3) {
			indexValue=indexValue.substring(19,21);
			indexValue=indexValue.replace(']','');
			if(parseInt(document.forms[1].elements['developmentFeeList[' + indexValue + '].feeUnits'].value) > 0) {
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked =true;
			}
			else{
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked =false;
			}
		}

}

function checkFee(indexValue,feeType){
		if (feeType == 1) {
			indexValue=indexValue.substring(17,19);
			indexValue=indexValue.replace(']','');
			//alert(indexValue);
			if(document.forms[1].elements['planCheckFeeList[' + indexValue + '].feeAmount'].value == '$0.00') {
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked = false;
			}
			else{
				document.forms[1].elements['planCheckFeeList[' + indexValue + '].checked'].checked =true;
			}

		}
		else if (feeType == 2) {

			indexValue=indexValue.substring(16,18);
			indexValue=indexValue.replace(']','');
			if(document.forms[1].elements['activityFeeList[' + indexValue + '].feeAmount'].value == '$0.00') {
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked = false;
			}
			else{
				document.forms[1].elements['activityFeeList[' + indexValue + '].checked'].checked = true;
			}
		}
		//code added for development fees by Gayathri on 15thDec'08
		else if (feeType == 3) {

			indexValue=indexValue.substring(19,21);
			indexValue=indexValue.replace(']','');
			if(document.forms[1].elements['developmentFeeList[' + indexValue + '].feeAmount'].value == '$0.00') {
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked = false;
			}
			else{
				document.forms[1].elements['developmentFeeList[' + indexValue + '].checked'].checked = true;
			}
		}

}

function prefillUnit(indexValue,feeType){
}

function newWindow(window) {

	 busTaxCount = document.forms[2].elements['busTaxCount'].value;

	 strValue=validateData('req',document.forms[2].elements['businessTaxList[' + busTaxCount + '].licenseNbr'],'License Number is a required field');

	if (strValue ==true)
	{
	msgWindow=open('',window,'resizable=yes,width=450,height=500,screenX=600,screenY=200,top=200,left=475,scrollbars');
     msgWindow.location.href = '<html:rewrite forward="listContractor" />' + '?licenseNumber=' + document.forms[2].elements['businessTaxList[' + busTaxCount + '].licenseNbr'].value + '&peopleId=' + document.forms[2].elements['businessTaxList[' + busTaxCount + '].peopleId'].value + '&name=' + document.forms[2].elements['businessTaxList[' + busTaxCount + '].name'].value + '&strFlag=fresh';
     if (msgWindow.opener == null) msgWindow.opener = self;
     }
}

function validateValuationFunction() {

	var pcFeeDate = $("#plancheckfeedatepicker").val();
	document.forms[0].planCheckFeeDate.value=pcFeeDate;
	
    strValue=validateData('req',document.forms[0].elements['planCheckFeeDate'],'Plan Check Fee Date is a required field');
    if (strValue == true)
    {
		if (document.forms[0].elements['planCheckFeeDate'].value != '')
    	{
    		strValue=validateData('date',document.forms[0].elements['planCheckFeeDate'],'Invalid date format');
    	}
    }
    var developmentFeeDate = $("#developmentfeedatepicker").val();
	document.forms[0].developmentFeeDate.value=developmentFeeDate;
	if (strValue == true)
    {
		if (document.forms[0].elements['developmentFeeDate'].value != '')
    	{
    		strValue=validateData('date',document.forms[0].elements['developmentFeeDate'],'Invalid date format');
    	}
    }
    
    var permitFeeDate = $("#permitfeedatepicker").val();
	document.forms[0].permitFeeDate.value=permitFeeDate;
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms[0].elements['permitFeeDate'],'Permit Fee Date is a required field');
    }
    if (strValue == true)
    {
		if (document.forms[0].elements['permitFeeDate'].value != '')
    	{
    		strValue=validateData('date',document.forms[0].elements['permitFeeDate'],'Invalid date format');
    	}
    }
    if (strValue == true)
    {
    	//alert("test");
 		document.forms[0].action='<%=contextRoot%>/saveValuationMgr.do';
 		document.forms[0].submit();
 		//return true;
    }
}

function validateBusinessTaxFunction() {
	ctr=parseInt(document.forms[2].elements['busTaxCount'].value)+1;
	for(i=1;i<=ctr;i++)	{
	    b=(8*(i-1))+3;
		if((eval(document.forms[2].elements[b+2].value.length) != 0) || (eval(document.forms[2].elements[b+6].value.length) != 0)) {
			strValue=validateData('req',document.forms[2].elements[b+2],'License Number is a required field');
			if (strValue == true) {
				strValue=validateData('req',document.forms[2].elements[b+4],'Name is a required field. Please click on Find Contractor button');
			}
			if (strValue == true) {
				strValue=validateData('req',document.forms[2].elements[b+5],'Valuation is a required field');
			}
			if ((document.forms[2].elements[b+1].checked != true) && (strValue == true)) {
				strValue=validateData('req',document.forms[2].elements[b+7],'Trade Description is a required field');
			}
			if(strValue == true){
				<%if((actStatus == Constants.ACTIVITY_STATUS_CE_INACTIVE) || (actStatus == Constants.ACTIVITY_STATUS_CE_SUSPENSE) || (actStatus == Constants.ACTIVITY_STATUS_BUILDING_PERMIT_FINAL) || (actStatus == Constants.ACTIVITY_STATUS_BT_CALENDAR_YEAR_EXPIRED) || (actStatus == Constants.ACTIVITY_STATUS_BT_OUT_OF_BUSINESS)  || (actStatus == Constants.ACTIVITY_STATUS_BL_OUT_OF_BUSINESS) || (actStatus == Constants.ACTIVITY_STATUS_BL_FISCAL_YEAR_EXPIRED)){%>
					alert('For the current Activity Status Business Tax fee can not be calculated');
				strValue = false;
			<%}%>
			}
			if (strValue == false) {
				return strValue;
			}
		}
	}

	document.forms[2].action='<%=contextRoot%>/saveBusTaxFees.do';
	document.forms[2].submit();
}
function checkPlanRequired() {
	// checking the plan check fees
	count =  parseInt(document.forms[1].elements['planCheckFeeCount'].value);
	for (i=0;i<=count;i++) {
		if ((document.forms[1].elements['planCheckFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['planCheckFeeList[' + i + '].required'].value != "")) {
				document.forms[1].elements['planCheckFeeList[' + i + '].checked'].checked = true;
		}
	}

}

//code added for development fees by Gayathri on 15thDec'08
function checkDevelopmentFeesRequired() {
	// checking the development fees
	count =  parseInt(document.forms[1].elements['developmentFeeCount'].value);

	for (i=0;i<=count;i++) {
		if ((document.forms[1].elements['developmentFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['developmentFeeList[' + i + '].required'].value != "")) {
				document.forms[1].elements['developmentFeeList[' + i + '].checked'].checked = true;
		}
	}
}

function checkPermitRequired() {
	// checking the activity fees
	count =  parseInt(document.forms[1].elements['activityFeeCount'].value);
	for (i=0;i<=count;i++) {
	//alert(i);
	//alert(document.forms[1].elements['activityFeeList[' + i + '].required'].value);
	if 	((document.forms[1].elements['activityFeeList[' + i + '].feePc'].value != "X") &&
			(document.forms[1].elements['activityFeeList[' + i + '].input'].value != "X") ) {

		if ((document.forms[1].elements['activityFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['activityFeeList[' + i + '].required'].value != ""))
			{
				document.forms[1].elements['activityFeeList[' + i + '].checked'].checked = true;
		}
	}
	}
}

function previewFees() {
	if (validateUnits() == true) {
			document.forms[1].action='<%=contextRoot%>/previewFees.do';
			document.forms[1].submit();
 		}
}

function saveFees() {
	if (validateUnits() == true) {
		document.forms[1].action='<%=contextRoot%>/saveFeesMgr.do';
 		document.forms[1].submit();
		}
}


function cancelPage() {
	document.forms[1].action='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
 	document.forms[1].submit();
}

//-----------------Added for penalty---------------
function checkPenaltyFeesRequired(){
count =  parseInt(document.forms[1].elements['penaltyFeeCount'].value);
for (i=0;i<=count;i++) {
if ((document.forms[1].elements['penaltyFeeList[' + i + '].required'].value != "0") &&
			(document.forms[1].elements['penaltyFeeList[' + i + '].required'].value != ""))
			{
				document.forms[1].elements['penaltyFeeList[' + i + '].checked'].checked = true;
		}
	}
}

function viewLedger() {
   	document.forms[1].action='<%=contextRoot%>/viewLedger.do?activityId=<%=activityId%>&activityNbr=<%=actNbr%>';
 	document.forms[1].submit();
}

function sendGreenHalo() {
   	document.forms[1].action='<%=contextRoot%>/sendGreenHalo.do?activityId=<%=activityId%>';
 	document.forms[1].submit();
}

function changeReadyToPayFlag(){
	var check = document.forms[1].elements['readyToPay'].checked;
	if(!check){
		if (validateUnits() == true) {
			document.forms[1].action='<%=contextRoot%>/saveFeesMgr.do';
	 		document.forms[1].submit();
		}
	}
}


</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0" onload="<%=onloadScript%>">


<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<font class="con_hdr_3b">Fee Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font>
						<br><br>
					</td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td><html:errors /></td>
							</tr>
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" class="tabletitle">
												Fees
											</td>
											<td class="tabletitle">
												<nobr>
													<%
													String green = (String)session.getAttribute("greenHalo");
													green = ("on".equalsIgnoreCase(green))?"Y":"N";
													
													String greenButton = (String)session.getAttribute("greenHaloButton");
													if(green.equalsIgnoreCase("Y") && greenButton!=""){ %>
														<html:button property="submitGreenHalo" value="<%=greenButton%>" styleClass="button" onclick="sendGreenHalo()"/>&nbsp;
													<%} %>
													<html:button property="PaymentHistory" value="Payment History" styleClass="button" onclick="viewLedger()"/>&nbsp;
                                     				<html:button property="Back" value="Back" styleClass="button" onclick="cancelPage();"></html:button> &nbsp;
												</nobr>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<html:hidden name="financeSummary"	property="planCheckFeeAmt" />
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
												<img src="../images/spacer.gif" width="1" height="1">
											</td>
											<td class="tablelabel" width="13%">
												<div align="right">
													Activity
												</div>
											</td>
					            			<% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
											<td class="tablelabel" width="13%">
												<div align="right">
													Plan Check
												</div>
											</td>
											<td class="tablelabel" width="15%">
												<div align="right">
													Development Fee
												</div>
											</td>
											<td class="tablelabel" width="20%">

												<div align="right">
													Contractor Tax
												</div>
											</td>
					            			<% } %>
											<td class="tablelabel" width="15%">
												<div align="right" >
													Penalty Fee
												</div>
											</td>
											<td class="tablelabel" width="10%">
												<div align="right">
													Total
												</div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel">
												Fees
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary" property="permitFeeAmt" />
												</div>
											</td>
											<% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary"	property="planCheckFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary"	property="developmentFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write	scope="session" name="financeSummary"	property="businessTaxFeeAmt" />
												</div>
											</td>
											<% } %>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary"	property="penaltyFeeAmt" />
												</div>
											</td>
											<td class="tabletext">
												<div align="right">
													<bean:write scope="session" name="financeSummary" property="totalFeeAmt" />
												</div>
											</td>
										</tr>
										<tr>
											<td class="tablelabel"><a
												href="<html:rewrite forward="viewPayment"/>?levelId=<%=activityId%>&level=A"
												class="hdrs">Payment</a></td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="permitPaidAmt" /></div>
											</td>
											<% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="planCheckPaidAmt" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="developmentFeePaidAmt" /></div>
											</td>
																						<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="businessTaxPaidAmt" /></div>
											</td>
											<% } %>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="penaltyPaidAmt"/></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="totalPaidAmt" /></div>
											</td>
										</tr>

										<tr>
											<td class="tablelabel">Balance Due </td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="permitAmountDue" /></div>
											</td>
											<% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="planCheckAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
													property="developmentFeeAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
												property="businessTaxAmountDue" /></div>
											</td>
											<% } %>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary"
													property="penaltyAmountDue" /></div>
											</td>
											<td class="tabletext">
											<div align="right"><bean:write
												scope="session" name="financeSummary" property="totalAmountDue" /></div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			<html:form name="feesMgrForm" type="elms.control.beans.FeesMgrForm"	action="">
			<input type="hidden" name="planCheckFeeDate" />
			<input type="hidden" name="developmentFeeDate" />
			<input type="hidden" name="permitFeeDate" />
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" class="tabletitle">Valuation</td>
											<td class="tabletitle"><nobr>
											<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="Save" value="Save Valuation" styleClass="button" onclick="validateValuationFunction();"/>
											</security:editable>
											&nbsp;</nobr></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr valign="top">
											<td class="tablelabel" width="1%">Valuation</td>
											<td class="tabletext" width="99%"><html:text
												property="valuation" size="18" maxlength="14"
												styleClass="textboxm" onblur="formatCurrency(this);"
												onkeypress="return displayPeriod();" /> </td>
										</tr>
										<tr valign="top">
											<td class="tablelabel" width="1%"><nobr>PLAN Check Fee Date</nobr></td>
											<td class="tabletext" width="99%">
												
												<html:text property="planCheckFeeDate" size="10" styleId="plancheckfeedatepicker" style="color:black;font-size:12px;" maxlength="10"
												styleClass="textboxd" onkeypress="return event.keyCode!=13"/>
												
										</tr>

											<tr valign="top">
											<td class="tablelabel" width="1%"><nobr>DEVELOPMENT Fee Date</nobr></td>
											<td class="tabletext" width="99%">
												<html:text property="developmentFeeDate" size="10" styleId="developmentfeedatepicker" style="color:black;font-size:12px;" maxlength="10"
												styleClass="textboxd"/>
										</tr>

										<tr valign="top">
											<td class="tablelabel" width="1%"><nobr>PERMIT Fee Date</nobr></td>
											<td class="tabletext" width="99%">
												<html:text property="permitFeeDate" size="10" styleId="permitfeedatepicker" style="color:black;font-size:12px;" maxlength="10"
												styleClass="textboxd"/>
										</tr>
									</table>
								</td>
								<html:hidden property="id" value="<%=activityId%>" />
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
				</tr>
			</html:form>


			<html:form name="feesMgrForm" type="elms.control.beans.FeesMgrForm" action="">
				<tr>
					<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="95%" class="tabletitle">Finance Manager
											<% if(projectName.equals(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS)){ %>
											&nbsp;&nbsp;<a href="<%=contextRoot%>/docs/<%=elms.agent.LookupAgent.getKeyValue("FEE_SPREADSHEET_LINK")%>" target="_blank">Fee Calculations</a>
											<% } %>
											
											</td>
											<td width="4%" class="tabletitle" style="text-transform:capitalize;white-space: nowrap;font-size: 12px;">
											<html:checkbox property="readyToPay" onclick="changeReadyToPayFlag();">Ready to Pay Online</html:checkbox>
											</td>
											<td width="1%" class="tabletitle"><nobr>
											<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											
											<html:button property="Reset" value="Reset" styleClass="button"
												onclick="resetForm();" /> <html:button property="Preview"
												value="Calculate" styleClass="button" onclick="previewFees();"></html:button>
												</security:editable>

											<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="Save" value="Save Fees" styleClass="button" onclick="saveFees()" />
											</security:editable>
											&nbsp;</nobr></td>
										</tr>
									</table>
								</td>
							</tr>
							<!-- <tr>
								<td><html:errors /></td>
							</tr>-->

							<%
								boolean rowFlag = false;
								int i = -1;
								String feeCount = "";
							%>

							<logic:equal name="feesMgrForm" property="planCheckFeesRequired" value="Y">
							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" class="tabletitle">Plan Check Fees</td>
											<td width="1%" class="tabletitle"><nobr>
											<html:button property="PlanRequired" value="Required"
												styleClass="button" onclick="checkPlanRequired();" /></td>

										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
											<div align="left"><img src="../images/spacer.gif" width="1"
												height="1"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
											<td class="tabletext">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
										</tr>

										<nested:iterate property="planCheckFeeList">
										<nested:hidden property="feeId" />
										<nested:hidden property="feeDescription"></nested:hidden>
										<%
											try{
													i++;
													if (rowFlag) {
														out.println("<TD></TD>");
														rowFlag = false;
													}
													else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}
												}catch(Exception e){}

										%>
											<td bgcolor="FFFFCC">
												<div align="left">
													<nested:hidden property="required" />
													<nested:checkbox property="checked" onclick="prefillUnit(this.name,1)"></nested:checkbox>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:write property="feeDescription" />

												</div>
											</td>
											<td class="tabletext">
												<div align="left">
													<nested:equal value="I" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();"	onblur="checkUnit(this.name,1)" />
													</nested:equal>
													<nested:equal value="D" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();" />
													</nested:equal>
												</div>
											</td>
											<td class="tabletext">

												<div align="right">

														<nested:equal value="M" property="input">
															<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,1)" />
														</nested:equal>
														<nested:notEqual property="input" value="M">
															<div align="right">
																<nested:write property="feeAmount" />
															</div>
														</nested:notEqual>

												</div>
											</td>
											<nested:hidden property="input" />

										</nested:iterate>
										<%
										feeCount = "" + i;
										%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
								<html:hidden property="planCheckFeeCount" value="<%=feeCount%>" />
							</logic:equal>

							<logic:notEqual name="feesMgrForm" property="planCheckFeesRequired"	value="Y">
								<html:hidden property="planCheckFeeCount" value="0" />
							</logic:notEqual>

						<logic:equal name="feesMgrForm" property="developmentFeesRequired" value="Y">

							<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" class="tabletitle">Development Fees</td>
											<td width="1%" class="tabletitle"><nobr>
											<html:button property="developmentRequired" value="Required" styleClass="button" onclick="checkDevelopmentFeesRequired();" /></td>
										</tr>
									</table>
								</td>
							</tr>

							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
											<div align="left"><img src="../images/spacer.gif" ></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
											<td class="tabletext">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
										</tr>
										<%
											rowFlag = false;
											i = -1;
										%>
										<nested:iterate property="developmentFeeList">
										<nested:hidden property="feeId" />
										<nested:hidden property="feeDescription"></nested:hidden>
										<%
											try{

													i++;
													if (rowFlag) {
														out.println("</TD><TD>");
														rowFlag = false;
													}
													else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}
												}catch(Exception e){}
										%>

											<td bgcolor="FFFFCC">
												<div align="left">
													<nested:hidden property="required" />
													<nested:checkbox property="checked" onclick="prefillUnit(this.name,3)"></nested:checkbox>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:write property="feeDescription" />

												</div>
											</td>
											<td class="tabletext">
												<div align="left">
													<nested:equal value="I" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();"	onblur="checkUnit(this.name,3)" />
													</nested:equal>
													<nested:equal value="D" property="input">
														<nested:text property="feeUnits" styleClass="textboxm" size="5" onkeypress="validateInteger();" />
													</nested:equal>
												</div>
											</td>
											<td class="tabletext">

												<div align="right">

														<nested:equal value="M" property="input">
															<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,3)" />
														</nested:equal>
														<nested:notEqual property="input" value="M">
															<div align="right">
																<nested:write property="feeAmount" />
															</div>
														</nested:notEqual>

												</div>
											</td>
											<nested:hidden property="input" />

										</nested:iterate>
										<%
										feeCount = "" + i;
										%>
									</table>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
								<html:hidden property="developmentFeeCount" value="<%=feeCount%>" />
							</logic:equal>

							<logic:notEqual name="feesMgrForm" property="developmentFeesRequired" value="Y">
								<html:hidden property="developmentFeeCount" value="0" />
							</logic:notEqual>

						<!-- Development of Fee -->
						<!-- End of the Code for development Fee -->


						<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" class="tabletitle">Permit/License/Business Tax Fees</td>
											<td width="1%" class="tabletitle"><nobr>
											<html:button property="PermitRequired" value="Required"	styleClass="button" onclick="checkPermitRequired();" /></td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
											<div align="left"><img src="../images/spacer.gif" width="1" height="1"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
											<td class="tabletext">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left"></div>
											</td>
											<td class="tablelabel">
											<div align="left">Fee Description</div>
											</td>
											<td class="tablelabel">
											<div align="left">Units</div>
											</td>
											<td class="tablelabel">
											<div align="left">Amount</div>
											</td>
										</tr>
										<%
											rowFlag = false;
											i = -1;
										%>
										<nested:iterate property="activityFeeList">
											<nested:hidden property="required" />
											<nested:hidden property="feePc" />
											<nested:hidden property="input" />
											<nested:notEqual value="X" property="feePc">
											<nested:notEqual value="X" property="input">
											<nested:hidden property="feeId" />
											<nested:hidden property="feeDescription"></nested:hidden>
												<%
												try{
													i++;
													if (rowFlag) {
														out.println("<TD></TD>");
														rowFlag = false;
													} else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}
												}catch(Exception e){}
										%>

												<nested:notEqual property="feeAmount" value="$0.00">
													<td bgcolor="FFFFCC">

														<div align="left">
															<nested:checkbox property="checked"	onclick="prefillUnit(this.name,2)"></nested:checkbox>
														</div>
													</td>
													<td class="tabletext" bgcolor="FFFFCC">
														<div align="left">

																<nested:write property="feeDescription" />

														</div>
													</td>
													<td class="tabletext" >
														<div align="left">
															<nested:equal value="I" property="input">
																<nested:text property="feeUnits" styleClass="textboxc"	size="5" onkeypress="validateInteger();" onblur="checkUnit(this.name,2)" />
															</nested:equal>
															<nested:equal value="D" property="input">
																<nested:text property="feeUnits" styleClass="textboxc"	size="5" />
															</nested:equal>
														</div>
													</td>
													<td class="tabletext">
														<div align="left">

																<nested:equal property="input" value="M">
																	<nested:text property="feeAmount" styleClass="textboxc"	size="12" onblur="formatCurrency(this);checkFee(this.name,2)" />
																</nested:equal>
																<nested:notEqual property="input" value="M">
																	<div align="right">
																		<nested:write property="feeAmount" />
																	</div>
																</nested:notEqual>

														</div>
													</td>
												</nested:notEqual>

											<nested:equal property="feeAmount" value="$0.00">
											<td class="tabletext">
												<div align="left">
													<nested:checkbox property="checked"	onclick="prefillUnit(this.name,2)"></nested:checkbox>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:write property="feeDescription" />

												</div>
											</td>
											<td class="tabletext">
												<div align="left">
													<nested:equal value="I" property="input">
														<nested:text property="feeUnits" styleClass="textboxm"	size="5" onkeypress="validateInteger();" onblur="checkUnit(this.name,2)" />
													</nested:equal>
													<nested:equal value="D" property="input">
														<nested:text property="feeUnits" styleClass="textboxm"	size="5" />
													</nested:equal>
												</div>
											</td>
											<td class="tabletext">
												<div align="left">

														<nested:equal property="input" value="M">
															<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,2)" />
														</nested:equal>
														<nested:notEqual property="input" value="M">
															<div align="right">
																<nested:write property="feeAmount" />
															</div>
														</nested:notEqual>

												</div>
											</td>
											</nested:equal>
										</nested:notEqual>
									</nested:notEqual>
										</nested:iterate>
										<%
										feeCount = "" + i;
										%>
										<html:hidden property="activityFeeCount" value="<%=feeCount%>" />
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>

				<html:hidden property="id" value="<%=activityId%>" />

				<!-- -------------------------Processing of penalty fees------------------------------ -->
						<tr>
								<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td width="98%" class="tabletitle">Penalty Fees</td>
											<td width="1%" class="tabletitle"><nobr>
											<html:button property="PenaltyFeesRequired" value="Required" styleClass="button" onclick="checkPenaltyFeesRequired();" /></td>
										</tr>
									</table>
								</td>
							</tr>
						<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td class="tablelabel">
												<div align="left">
													<img src="../images/spacer.gif" width="1" height="1">
												</div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Fee Description
												</div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Amount
												</div>
											</td>
											<td class="tabletext">
												<div align="left"></div>
											</td>
											<td class="tablelabel">
												<div align="left"></div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Fee Description
												</div>
											</td>
											<td class="tablelabel">
												<div align="left">
													Amount
												</div>
											</td>
										</tr>
										<%
										rowFlag = false;
										i= -1;

										%>
										<logic:present name="penaltyFeeList">
										<nested:iterate property="penaltyFeeList">
										<nested:hidden property="feeId" />
										<nested:hidden property="feeDescription"></nested:hidden>
												<%
												try{
													i++;
													if (rowFlag) {
														out.println("<TD></TD>");
														rowFlag = false;
													} else {
														if (i > 0) {
															out.println("</tr>");
															out.println("<tr>");
														}
														rowFlag = true;
													}

												}catch(Exception e){}

												%>

										<td class="tabletext">
														<div align="left">
															<nested:hidden property="required" />
															<nested:checkbox property="checked"	onclick="prefillUnit(this.name,4)"></nested:checkbox>
														</div>
													</td>
													<td class="tabletext">
														<div align="left">

																<nested:write property="feeDescription" />

														</div>
													</td>
										<td class="tabletext">
														<div align="left">

																<nested:equal property="input" value="M">
																	<nested:text property="feeAmount" styleClass="textboxm"	size="12" onblur="formatCurrency(this);checkFee(this.name,4)" />
																</nested:equal>
																<nested:notEqual property="input" value="M">
																	<div align="right">
																		<nested:write property="feeAmount" />
																	</div>
																</nested:notEqual>

														</div>
													</td>

										</nested:iterate>
										</logic:present>
										<%
										feeCount = "" + i;

										%>
										<html:hidden property="penaltyFeeCount" value="<%=feeCount%>" />

</table> </td> </tr><tr>
					<td>&nbsp;</td>
				</tr>
					<!-- -----------------------End of penalty fees-------------------------- -->

			</html:form>

			<% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
			<!-- contractor's business tax -->
			<html:form name="feesMgrForm" type="elms.control.beans.FeesMgrForm"	action="">
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="98%" class="tabletitle">Contractor Tax Fees</td>
									<td width="1%" class="tabletitle"><nobr>
										<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="printTax" value="Print Receipt" styleClass="button" onclick="getBusinessTaxReport()" />
											<html:button property="addTax" value="Add" styleClass="button" onclick="addBusinessTaxRow();"></html:button>
										</security:editable>
										<security:editable levelId="<%=activityId%>" levelType="A" editProperty="checkUser">
											<html:button property="save" value="Save Contractor Tax" styleClass="button" onclick="validateBusinessTaxFunction();"/>
										</security:editable>
									</nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">


								<tr>
									<td class="tablelabel">
									<div align="left"><img src="../images/spacer.gif" width="1"
										height="1">Primary</div>
									</td>
									<td class="tablelabel">
									<div align="left">License #</div>
									</td>
									<td class="tablelabel">
									<div align="left">Name</div>
									</td>
									<td class="tablelabel">
									<div align="left">Valuation</div>
									</td>
									<td class="tablelabel">Amount</td>
									<td class="tablelabel">Trade Description</td>
									<td class="tablelabel"><a
										target="_blank"
										href="<%=contextRoot%>/printBusTaxReceipt.do?actNbr=<%=actNbr%>&pId=all">
									<img src="../images/print-all.gif" border="0"> </a></td>
								</tr>
								<%int index = -1;%>
								<nested:iterate property="businessTaxList">
									<%index++;%>
									<tr>
										<nested:hidden property="peopleId" />
										<td class="tabletext">
										<div align="left"><nested:checkbox property="checked"
											styleId="checkbox2" onclick="copyValidate(this.name);" /></div>
										</td>

										<td class="tabletext">
										<div align="left"><nested:text property="licenseNbr" size="10"
											styleClass="textbox" /> &nbsp; <html:button
											property="findContractor" value="Find" styleClass="button"
											onclick="newWindow('window2')" /></div>
										</td>

										<td class="tabletext">
										<div align="left"><nested:text property="name" readonly="true"
											size="25" styleClass="textbox" /></div>
										</td>
										<td class="tabletext">
										<div align="left"><nested:text property="feeValuation"
											size="12" styleClass="textboxm"
											onkeypress="return validateCurrency();"
											onblur="calculateBusTax(this.name);formatCurrency(this);" /></div>
										</td>
										<td class="tabletext">
										<div align="left"><nested:text property="feeAmount"
											readonly="true" size="8" styleClass="textboxm" /></div>
										</td>
										<td class="tabletext"><nested:textarea rows="2" cols="20"
											property="comments" /></td>
										<td class="tabletext"><a target="_blank"
											href="<%=contextRoot%>/printBusTaxReceipt.do?actNbr=<%=actNbr%>&pId=<nested:write property="peopleId"/>">
										<img src="../images/print.gif" border="0"> </a></td>
										<td class="tabletext">
             							<a href="<%=contextRoot%>/deleteBusTaxFees.do?activityId=<%=activityId%>&pId=<nested:write property="peopleId"/>"><img src="../images/delete.gif" alt="Delete" border="0"></a>
             							</td>
									</tr>

								</nested:iterate>
								<%String sCount = "" + index;%>
								<html:hidden property="busTaxCount" value="<%=sCount%>" />
							</table>
							</td>

						</tr>
					</table>
					<html:hidden property="id" value="<%=activityId%>" />
					</html:form>
					<% } %>
		<script type="text/javascript">
		 $("#plancheckfeedatepicker").kendoDatePicker({
                });
		 $('#plancheckfeedatepicker').click(function(e){
             var obj = $(this).data('kendoDatePicker');
             if (typeof(obj.options.opened) == 'undefined' || !obj.options.opened){
                 obj.open();
             }
         });
		 $("#developmentfeedatepicker").kendoDatePicker({
         });
		 $('#developmentfeedatepicker').click(function(e){
             var obj = $(this).data('kendoDatePicker');
             if (typeof(obj.options.opened) == 'undefined' || !obj.options.opened){
                 obj.open();
             }
         });
		 $("#permitfeedatepicker").kendoDatePicker({
         });
		 $('#permitfeedatepicker').click(function(e){
             var obj = $(this).data('kendoDatePicker');
             if (typeof(obj.options.opened) == 'undefined' || !obj.options.opened){
                 obj.open();
             }
         });
		 </script>

		 </body>
</html:html>

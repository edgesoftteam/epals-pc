<%@ page import="javax.sql.*,sun.jdbc.rowset.*,java.util.*,elms.app.finance.*,elms.util.*,elms.agent.*,elms.common.*" %>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon />
<script>
var xmlhttp = false;
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");
</script>

 <%!String ledgerURL = ""; %>
<%
	String contextRoot = request.getContextPath();
	String levelId = (java.lang.String)session.getAttribute("levelId");
	String level = (java.lang.String)session.getAttribute("level");
	String activityType= (String)request.getAttribute("activityType");

	String label = "";
	double pcCredit = 0;
	double pmtCredit = 0;
	double developmentFeeCredit = 0;
	double penaltyCredit = 0;//added for penalty
	double businessTaxCredit = 0;
	double totalDeposit = 0;

	String activityNumber = (String)session.getAttribute("actNumber");
      if (activityNumber == null ) activityNumber = "";
		PeopleAgent peopleListAgent = new PeopleAgent();
		FinanceAgent financeAgent = new FinanceAgent();
	    java.util.List peopleList = peopleListAgent.getPeople(levelId,level);
		elms.util.StringUtils stringUtils = new elms.util.StringUtils();

	  	pageContext.setAttribute("peopleList", peopleList);

		if ((level == null) || level.equals("A"))
			{
			label = "Activity ";
			level = "A";
			}
		else
		   { label = "Sub Project ";
			  level = "Q";
			  request.setAttribute("level","Q");
	  }
      String lsoAddress = (String)session.getAttribute("lsoAddress");
      if (lsoAddress == null ) lsoAddress = "";
      String psaInfo = (String)session.getAttribute("psaInfo");
      if (psaInfo == null ) psaInfo = "";

	  pcCredit =elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('1', elms.util.StringUtils.s2i(levelId))).doubleValue();

      pmtCredit =elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('2', elms.util.StringUtils.s2i(levelId))).doubleValue();

	  developmentFeeCredit =elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('3', elms.util.StringUtils.s2i(levelId))).doubleValue();

	  penaltyCredit =elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('4', elms.util.StringUtils.s2i(levelId))).doubleValue();

	  businessTaxCredit = elms.util.StringUtils.s2bd(financeAgent.pcpCreditForActivity('5', elms.util.StringUtils.s2i(levelId))).doubleValue();

		try{
	    	  ledgerURL=LookupAgent.getReportsURL() +activityNumber;
		}catch(Exception e){}

	String projectName = LookupAgent.getProjectNameForActivityNumber(activityNumber);

%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/sweetalert/dist/sweetalert.css">
<script type="text/javascript" src="<%= contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
</head>

<script language="JavaScript" src="../script/calendar.js"></script> <script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript">
function viewLedger() {
	document.forms['paymentMgrForm'].elements['ledger'].disabled = true;
	<%-- parent.f_content.location.href='<%=contextRoot%>/viewLedger.do?activityId=<%=levelId%>&activityNbr=<%=psaInfo%>'; --%>
	document.location='<%=contextRoot%>/viewLedger.do?activityId=<%=levelId%>&activityNbr=<%=psaInfo%>';
}
function viewReceipt() {
	document.forms['paymentMgrForm'].elements['receipt'].disabled = true;
    window.open('<%=contextRoot%>/printReceipt.do?actNbr=<%=activityNumber%>&actType=<%=activityType%>','ledger', 'left=0, top=0, menubar=no, status=no, location=no, toolbar=no, scrollbars=yes, resizable=yes, width=800, height=600');
}
function printLedger() {
  window.open('<%=ledgerURL%>','ledger', 'left=0, top=0, menubar=no, status=no, location=no, toolbar=no, scrollbars=yes, resizable=yes, width=800, height=600');
  //parent.f_content.location.href="<%=contextRoot%>/jsp/project/printPermit.jsp?actNbr=<%=activityNumber%>&actType=LEDGER";

}
function goBackToActivity() {
   document.forms['paymentMgrForm'].elements['back'].disabled = true;
   document.location="<%=contextRoot%>/viewActivity.do?activityId=<%=levelId%>";
  <%--  parent.f_content.location.href="<%=contextRoot%>/viewActivity.do?activityId=<%=levelId%>"; --%>
}
function goBackToSubProject() {
   parent.f_content.location.href="<%=contextRoot%>/viewSubProject.do?subProjectId=<%=levelId%>";
}
function frmLoad() {
	if (document.paymentMgrForm.levelType.value =='A') {
		document.forms['paymentMgrForm'].elements['pay'].disabled = false;
		document.paymentMgrForm.planCheck.checked =false;
		document.paymentMgrForm.developmentFees.checked =false;
		document.paymentMgrForm.penaltyFees.checked =false;
		document.paymentMgrForm.permitFees.checked=false;
		document.paymentMgrForm.businessTax.checked=false;
		document.forms['paymentMgrForm'].elements['transactionType'].selectedIndex = 0;
		document.forms['paymentMgrForm'].elements['amount'].value="";
		document.forms['paymentMgrForm'].elements['method'].selectedIndex = 0;
		document.forms['paymentMgrForm'].elements['paidBy'].selectedIndex = 0;
		document.forms['paymentMgrForm'].elements['other'].checked = false;
		document.forms['paymentMgrForm'].elements['otherText'].value = "";
		document.forms['paymentMgrForm'].elements['checkNoConfirmation'].value = "";
		document.forms['paymentMgrForm'].elements['authorizedBy'].value = "";
		document.forms['paymentMgrForm'].elements['comments'].value = "";
	}

	if (document.paymentMgrForm.levelType.value =='A') {
		dspTable(1);

	}
	else {
		paymentTable.style.display="none";
	}
}

function formatAmount(strAmount) {
	  	strAmount = strAmount.replace('(','-');
      	strAmount = strAmount.replace('$','');
      	strAmount = strAmount.replace(')','');
      	strAmount = strAmount.replace(/,/g,"");
      	return strAmount;
}

var strValue;
function validateFunction() {
	document.forms['paymentMgrForm'].elements['pay'].disabled = true;
	payMethod=document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].selectedIndex].value;
	if (document.paymentMgrForm.levelType.value =='A') {
		  intValue=parseInt(document.forms['paymentMgrForm'].elements['transactionType'].options[document.forms['paymentMgrForm'].elements['transactionType'].selectedIndex].value);


		  if ((document.forms['paymentMgrForm'].elements['planCheck'].checked == false) &&  (document.forms['paymentMgrForm'].elements['developmentFees'].checked == false) && (document.forms['paymentMgrForm'].elements['permitFees'].checked == false) &&  (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == false) && (document.forms['paymentMgrForm'].elements['businessTax'].checked == false))
		  {
		  	    alert("At least one check box needs to be checked");
	      		strValue = false;
		  }
		  else
		  {
		  strValue=true;
		  }

		  if (strValue == true)
		  {
		  	strValue=validateData('req',document.paymentMgrForm.amount,'Payment Amount is a required field');
		  }
		  if (strValue ==  true)
		  {
		  	strValue1 = document.forms['paymentMgrForm'].elements['amount'].value;
		  	strValue1 = strValue1.replace('(','-');
	      	strValue1 = strValue1.replace('$','');
	      	strValue1 = strValue1.replace(')','');
	      	strValue1 = strValue1.replace(/,/g,"");
	      	if (parseFloat(strValue1) == 0)
	      	{
	      		alert("Payment Amount cannot be zero");
	      		strValue = false;
	      		if (strValue ==  false){
				document.forms['paymentMgrForm'].elements['pay'].disabled = false;
				document.forms['paymentMgrForm'].elements['amount'].focus();
			}else{
				strValue = true;
			}
	      	}else{
	      		strValue = true;
	      	}

		  }
		  if ((strValue == true) && (intValue != 5) && (intValue !=6))
		  {
		  	if (payMethod == "")
		  	{
		  		alert("Payment Method is a required field");
		  		strValue = false;
				if (strValue ==  false){
					document.forms['paymentMgrForm'].elements['pay'].disabled = false;
					document.forms['paymentMgrForm'].elements['method'].focus();
				}else{
					strValue = true;
				}
		  	}else{
		  		strValue =true;
		  	}	  }
		  if (strValue == true)
		  {
		  	if (payMethod == "Deposit") {
	  	     	strValue  = checkDeposit();
	  	     	if (!strValue) {
	  		     	alert("Payment amount exceeds available Deposit. Please verify");
	  	         	document.forms['paymentMgrForm'].elements['amount'].focus();
	  	     	}
	  	  	} else{
		  			strValue=true;
		  		}
		  }
		  if ( (strValue ==  true) && (intValue == 2) && (payMethod == "pcCredit"))
		  {
		  	strValue1 = document.forms['paymentMgrForm'].elements['amount'].value;
		  	strValue1 = strValue1.replace('(','-');
	      	strValue1 = strValue1.replace('$','');
	      	strValue1 = strValue1.replace(')','');
	      	strValue1 = strValue1.replace(/,/g,"");
	      	strValue2 = document.forms['paymentMgrForm'].elements['pcCreditAmt'].value;
	      	strValue2 = strValue2.replace('(','-');
	      	strValue2 = strValue2.replace('$','');
	      	strValue2 = strValue2.replace('(','');
	      	strValue2 = strValue2.replace(/,/g,"");
		  	if ( parseFloat(strValue1) > parseFloat(strValue2) )
		  	{
		  		alert("Payment Amount is greater than the plan check credit");
	 	   		strValue = false;
	      	}
	      	else
	      	{
	      		strValue=true;
	      	}
	      }
		  if ( (strValue ==  true) && (intValue == 2) && ((payMethod == "refund") || (payMethod == "transferOut")))
		  {
		  	strValue1 = document.forms['paymentMgrForm'].elements['amount'].value;
		  	strValue1 = strValue1.replace('(','-');
	      	strValue1 = strValue1.replace('$','');
	      	strValue1 = strValue1.replace(')','');
	      	strValue1 = strValue1.replace(/,/g,"");
	      	strValue2 = document.forms['paymentMgrForm'].elements['pcCreditAmt'].value;
	      	strValue2 = strValue2.replace('(','-');
	      	strValue2 = strValue2.replace('$','');
	      	strValue2 = strValue2.replace('(','');
	      	strValue2 = strValue2.replace(/,/g,"");
	      	strValue2 = "-" + strValue2;
	      	strValue3 = document.forms['paymentMgrForm'].elements['pmtCreditAmt'].value;
	      	strValue3 = strValue3.replace('(','-');
	      	strValue3 = strValue3.replace('$','');
	      	strValue3 = strValue3.replace('(','');
	      	strValue3 = strValue3.replace(/,/g,"");
	      	strValue3 = "-" + strValue3;
			strValue4 = document.forms['paymentMgrForm'].elements['deveFeeCreditAmt'].value;
	      	strValue4 = strValue4.replace('(','-');
	      	strValue4 = strValue4.replace('$','');
	      	strValue4 = strValue4.replace('(','');
	      	strValue4 = strValue4.replace(/,/g,"");
	      	strValue4 = "-" + strValue4;
			strValue5 = document.forms['paymentMgrForm'].elements['penaltyCreditAmt'].value;
	      	strValue5 = strValue5.replace('(','-');
	      	strValue5 = strValue5.replace('$','');
	      	strValue5 = strValue5.replace('(','');
	      	strValue5 = strValue5.replace(/,/g,"");
	      	strValue5 = "-" + strValue5;
	      	total =0;

			if (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true)
	      	{
	      		total = parseFloat(strValue5);
	      	}
			if (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true)
	      	{
	      		total = parseFloat(strValue4);
	      	}
			if (document.forms['paymentMgrForm'].elements['permitFees'].checked == true)
	      	{
	      		total = parseFloat(strValue3);
	      	}
	      	if (document.forms['paymentMgrForm'].elements['planCheck'].checked == true)
	      	{
	      		total = parseFloat(strValue2);
	      	}
	      	if ((document.forms['paymentMgrForm'].elements['planCheck'].checked == true)  && (document.forms['paymentMgrForm'].elements['permitFees'].checked == true) && (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) && (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true))
	      	{
	      		total = parseFloat(strValue2) + parseFloat(strValue3) + parseFloat(strValue4) + parseFloat(strValue5);
	      	}

	      	amount=parseFloat(strValue1);
	      	amount = amount.toFixed(2);
	      	total =total.toFixed(2);
		  	if ( amount > total ){
		  		alert("Payment Amount is greater than the available credit");
	 	   		strValue = false;
	      	}else{
	      		strValue=true;
	      	}
	      }
	     }
	     if ((strValue == true) && ((payMethod == "nsf") || (payMethod == "check")))
	     {
	     		if (document.forms['paymentMgrForm'].elements['checkNoConfirmation'].value == "" )
	     		{
	     			alert("Check No. Confirmation is a required field");
	 	   			strValue = false;
				if (strValue ==  false){
					document.forms['paymentMgrForm'].elements['pay'].disabled = false;
					document.forms['paymentMgrForm'].elements['checkNoConfirmation'].focus();
				}else{
					strValue = true;
				}
	 	   	}else{
	     			strValue = true;
	     		}
	     }
	     if ((strValue == true) && (intValue == 5)){
	     		if (document.forms['paymentMgrForm'].elements['authorizedBy'].value == "" )	{
	     			alert("Authorized By is a required field");
	 	   			strValue = false;
	 	   		}else{
	     			strValue = true;
	     		}
	     }
	     if ((strValue == true) && ((payMethod == "transferOut") || (payMethod == "transferIn"))){
	     		if (document.forms['paymentMgrForm'].elements['comments'].value == "" )	{
	     			alert("Comment is a required field");
	 	   			strValue = false;
	 	   		}else{
	     			strValue = true;
	     		}
	     }


	      if (document.paymentMgrForm.levelType.value =='Q'){
	      	if (payMethod == ""){
		  		alert("Payment Method is a required field");
		  		strValue = false;
		  	}else{
		  		strValue =true;
		  	}
		    if ((strValue == true) && (payMethod == "check")){
	     		if (document.forms['paymentMgrForm'].elements['checkNoConfirmation'].value == "" ){
	     			alert("Check No. Confirmation is a required field");
	 	   			strValue = false;
	 	   		}else{
	     			strValue = true;
	     		}
		    }

	      }

	      if (strValue == true){
	      	document.forms['paymentMgrForm'].action='<%=contextRoot%>/savePaymentMgr.do';
	 		document.forms['paymentMgrForm'].submit();
	 	 }
	}

function dspTable(intValue) {
	if (document.paymentMgrForm.levelType.value =='A') {
		if (intValue == 4) {
				paymentTable.style.display="";
				reverse.style.display ="";
		} else {
			paymentTable.style.display="none";
			reverse.style.display ="none";
		}

		if ((intValue == 1) || (intValue == 6)) {
				document.paymentMgrForm.planCheck.checked =true;
				document.paymentMgrForm.developmentFees.checked = true;
				document.paymentMgrForm.penaltyFees.checked = true;
				document.paymentMgrForm.permitFees.checked=true;
				document.paymentMgrForm.businessTax.checked=true;
		} else if (intValue == 4 ) {
				document.paymentMgrForm.planCheck.checked =true;
				document.paymentMgrForm.developmentFees.checked = true;
				document.paymentMgrForm.penaltyFees.checked = true;
				document.paymentMgrForm.permitFees.checked=true;
				document.paymentMgrForm.businessTax.checked=true;
		} else {
				document.paymentMgrForm.planCheck.checked =false;
				document.paymentMgrForm.developmentFees.checked = false;
				document.paymentMgrForm.penaltyFees.checked = false;
				document.paymentMgrForm.permitFees.checked=false;
				document.paymentMgrForm.businessTax.checked=false;
		}

		if ((intValue == 2) || (intValue == 4) || (intValue == 5)) {
			document.paymentMgrForm.amount.value =0;
		}

		if (document.paymentMgrForm.planCheckFeeAmt.value == '$0.00') {
			document.paymentMgrForm.planCheck.checked = false;
			document.paymentMgrForm.planCheck.disabled = true;
		}

		if (document.paymentMgrForm.developmentFeeAmt.value == '$0.00') {
			document.paymentMgrForm.developmentFees.checked = false;
			document.paymentMgrForm.developmentFees.disabled = true;
		}
		if (document.paymentMgrForm.permitFeeAmt.value == '$0.00') {
			document.paymentMgrForm.permitFees.checked = false;
			document.paymentMgrForm.permitFees.disabled = true;
		}
		if (document.paymentMgrForm.penaltyFeeAmt.value == '$0.00') {
			document.paymentMgrForm.penaltyFees.checked = false;
			document.paymentMgrForm.penaltyFees.disabled = true;
		}
		if (document.paymentMgrForm.businessTaxFeeAmt.value == '$0.00') {
			document.paymentMgrForm.businessTax.checked = false;
			document.paymentMgrForm.businessTax.disabled = true;
		}
	}

}

function calculatePayAmt() {
 strValue1='0';
 strValue2='0';
 strValue3='0';
 strValue4='0';
 total=0;

 if (document.paymentMgrForm.levelType.value =='A') {
	intValue=parseInt(document.forms['paymentMgrForm'].elements['transactionType'].options[document.forms['paymentMgrForm'].elements['transactionType'].selectedIndex].value);
	payMethod=document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].selectedIndex].value;

	if ((intValue == 1))  {
 		document.paymentMgrForm.planCheck.checked =true;
		document.paymentMgrForm.developmentFees.checked =true;
 		document.paymentMgrForm.penaltyFees.checked =true;
 		document.paymentMgrForm.permitFees.checked =true;
 		document.paymentMgrForm.businessTax.checked =true;

		if (document.forms['paymentMgrForm'].elements['planCheck'].checked == true) {
	      strValue1 = formatAmount(document.forms['paymentMgrForm'].elements['planCheckAmt'].value);
		}

		if (document.forms['paymentMgrForm'].elements['permitFees'].checked == true) {
		      strValue2 = formatAmount(document.forms['paymentMgrForm'].elements['permitAmt'].value);
		 }

		 if (document.forms['paymentMgrForm'].elements['businessTax'].checked == true) {
		      strValue3 = formatAmount(document.forms['paymentMgrForm'].elements['businessTaxAmt'].value);
		 }

		 if (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) {
	      	 strValue4 = formatAmount(document.forms['paymentMgrForm'].elements['developmentAmt'].value);
		 }

		 if (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true) {
	      	 strValue5 = formatAmount(document.forms['paymentMgrForm'].elements['penaltyAmt'].value);
		 }



		 total = parseFloat(strValue1) + parseFloat(strValue2) + parseFloat(strValue3) + parseFloat(strValue4) + parseFloat(strValue5);

		 if ((payMethod == "refund") || (payMethod == "transferOut")) {

		 	strValue1 = formatAmount(document.forms['paymentMgrForm'].elements['pcCreditAmt'].value);
		 	strValue2 = formatAmount(document.forms['paymentMgrForm'].elements['pmtCreditAmt'].value);
			strValue3 = formatAmount(document.forms['paymentMgrForm'].elements['deveFeeCreditAmt'].value);
			strValue4 = formatAmount(document.forms['paymentMgrForm'].elements['penaltyCreditAmt'].value);

		 	total = parseFloat(strValue1) + parseFloat(strValue2) + parseFloat(strValue3) + parseFloat(strValue4);
		 	document.forms['paymentMgrForm'].elements['amount'].value = total.toFixed(2);
		 } else {
		 	document.forms['paymentMgrForm'].elements['amount'].value = total.toFixed(2);
		 }
	}

	if (intValue == 2) {
		if ((payMethod == "pcCredit") || (payMethod == "transferOut")) {

			document.paymentMgrForm.planCheck.checked =false;
 			document.paymentMgrForm.permitFees.checked =true;
			document.paymentMgrForm.developmentFees.checked =false;
 			document.paymentMgrForm.penaltyFees.checked =false;
 			document.paymentMgrForm.businessTax.checked =false;
		}	

      		total =0;
      		if (document.forms['paymentMgrForm'].elements['permitFees'].checked == true) {
      			strValue1 = document.forms['paymentMgrForm'].elements['permitAmt'].value;
      			strValue1 = formatAmount(strValue1);
      			total = parseFloat(strValue1);
      		}

      		if (document.forms['paymentMgrForm'].elements['planCheck'].checked == true) {
      			strValue1 = document.forms['paymentMgrForm'].elements['planCheckAmt'].value;
      			strValue1 = formatAmount(strValue1);
     			total = total + parseFloat(strValue1);
      		}

      		if (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) {
      			strValue1 = document.forms['paymentMgrForm'].elements['developmentAmt'].value;
      			strValue1 = formatAmount(strValue1);
     			total = total + parseFloat(strValue1);
      		}

      		if (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true) {
      			strValue1 = document.forms['paymentMgrForm'].elements['penaltyAmt'].value;
      			strValue1 = formatAmount(strValue1);
     			total = total + parseFloat(strValue1);
      		}

      		if (document.forms['paymentMgrForm'].elements['businessTax'].checked == true) {
      			strValue1 = document.forms['paymentMgrForm'].elements['businessTaxAmt'].value;
      			strValue1 = formatAmount(strValue1);
				total = total + parseFloat(strValue1);
			}

      		document.forms['paymentMgrForm'].elements['amount'].value = total.toFixed(2);
	}

	if (intValue == 4) {
		//document.paymentMgrForm.businessTax.checked =true;
	}

	if (intValue == 3) {
		total = 0;
  		document.paymentMgrForm.planCheck.checked =true;
 		document.paymentMgrForm.permitFees.checked =true;
		document.paymentMgrForm.developmentFees.checked =true;
 		document.paymentMgrForm.penaltyFees.checked =true;
 		document.paymentMgrForm.businessTax.checked =true;

		if (document.forms['paymentMgrForm'].elements['planCheck'].checked == true) {
      		strValue1 = formatAmount(document.forms['paymentMgrForm'].elements['pmtPlanCheck'].value);
      		total = total + parseFloat(strValue1);
		}

 		if (document.forms['paymentMgrForm'].elements['permitFees'].checked == true) {
			strValue2 = formatAmount(document.forms['paymentMgrForm'].elements['pmtPermit'].value);
			total = total + parseFloat(strValue2);
		}

 		if (document.forms['paymentMgrForm'].elements['businessTax'].checked == true) {
			strValue3 = formatAmount(document.forms['paymentMgrForm'].elements['pmtBusTax'].value);
			total = total + parseFloat(strValue3);
		}

 		if (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) {
			strValue4 = formatAmount(document.forms['paymentMgrForm'].elements['pmtdevelopmentFee'].value);
			total = total + parseFloat(strValue4);
		}

 		if (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true) {
			strValue5 = formatAmount(document.forms['paymentMgrForm'].elements['pmtPenaltyFee'].value);
			total = total + parseFloat(strValue5);
		}

		document.forms['paymentMgrForm'].elements['amount'].value = total.toFixed(2);
	}

	if (intValue == 6) {
		total =0;

		if (document.forms['paymentMgrForm'].elements['planCheck'].checked == true) {
			strValue1 = formatAmount(document.forms['paymentMgrForm'].elements['pcECAmt'].value);
		    total = total + parseFloat(strValue1);
		}

		if (document.forms['paymentMgrForm'].elements['permitFees'].checked == true) {
			strValue2 = formatAmount(document.forms['paymentMgrForm'].elements['permitECAmt'].value);
			total = total + parseFloat(strValue2);
		}

		if (document.forms['paymentMgrForm'].elements['businessTax'].checked == true) {
			strValue3 = formatAmount(document.forms['paymentMgrForm'].elements['busECAmt'].value);
			total = total + parseFloat(strValue3);
		}

		if (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) {
			strValue4 = formatAmount(document.forms['paymentMgrForm'].elements['developmentFeeECAmt'].value);
			total = total + parseFloat(strValue4);
		}

		if (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true) {
			strValue5 = formatAmount(document.forms['paymentMgrForm'].elements['penaltyFeeECAmt'].value);
			total = total + parseFloat(strValue5);
		}

		document.forms['paymentMgrForm'].elements['amount'].value = total.toFixed(2);
	}

	if ((document.paymentMgrForm.planCheckFeeAmt.value == '$0.00') && (document.paymentMgrForm.planCheckAmt.value == '$0.00')) {
			document.paymentMgrForm.planCheck.checked = false;
			document.paymentMgrForm.planCheck.disabled = true;
	}

	if ((document.paymentMgrForm.developmentFeeAmt.value == '$0.00') && (document.paymentMgrForm.developmentAmt.value == '$0.00')) {
			document.paymentMgrForm.developmentFees.checked = false;
			document.paymentMgrForm.developmentFees.disabled = true;
	}

	if ((document.paymentMgrForm.penaltyFeeAmt.value == '$0.00') && (document.paymentMgrForm.penaltyAmt.value == '$0.00')) {
			document.paymentMgrForm.penaltyFees.checked = false;
			document.paymentMgrForm.penaltyFees.disabled = true;
	}

	if ((document.paymentMgrForm.permitFeeAmt.value == '$0.00') && (document.paymentMgrForm.permitAmt.value == '$0.00')) {
			document.paymentMgrForm.permitFees.checked = false;
			document.paymentMgrForm.permitFees.disabled = true;
	}

	if ((document.paymentMgrForm.businessTaxFeeAmt.value == '$0.00') && (document.paymentMgrForm.businessTaxAmt.value == '$0.00')) {
			document.paymentMgrForm.businessTax.checked = false;
			document.paymentMgrForm.businessTax.disabled = true;
	}
  }
}


function reverseFees() {

	feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);

	for (i=0;i<feeCount;i++) {
		if (document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked == true) {
			document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].paymentAmount'].value = parseFloat(document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feePaid'].value)*-1 ;
		}else document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].paymentAmount'].value = 0.00;
	}
	calculatePayFees();
	showMethod();
}

function reverseBTFees() {

	btFeeCount = parseInt(document.forms['paymentMgrForm'].elements['busindex'].value);

	for (i=0;i<btFeeCount;i++)
	{
		if (document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].checked'].checked == true){
			document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].paymentAmount'].value = parseFloat(document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].feePaid'].value)*-1 ;
		}else document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].paymentAmount'].value = 0.00;
	}
	calculatePayFees();
	showMethod();
	//document.forms['paymentMgrForm'].elements['amount'].focus();
}

function reverseAllFees() {
			intValue=parseInt(document.forms['paymentMgrForm'].elements['transactionType'].options[document.forms['paymentMgrForm'].elements['transactionType'].selectedIndex].value);
			if (intValue !=4) {
				return;
			}

			// If Both Plan Check and Permit Check Boxes are checked do nothing
      		if ((document.forms['paymentMgrForm'].elements['permitFees'].checked == true) &&
			    (document.forms['paymentMgrForm'].elements['planCheck'].checked == true) &&
				(document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) &&
				(document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true)) {
			   		return;
			   	}
			else if (document.forms['paymentMgrForm'].elements['planCheck'].checked == true) {

			feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);
			for (i=0;i<feeCount;i++) {

      			if ((document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feeFlagFour'].value == 1) &&
				   (parseFloat(document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feePaid'].value)>0)) {
					document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = true ;
				}else document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = false ;
			}
			}
			else if (document.forms['paymentMgrForm'].elements['permitFees'].checked == true) {

			feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);
			for (i=0;i<feeCount;i++) {
      			if ((document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feeFlagFour'].value == 2) &&
				   (parseFloat(document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feePaid'].value)>0)) {
					document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = true ;
				}else document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = false ;
			}
			}

			else if (document.forms['paymentMgrForm'].elements['developmentFees'].checked == true) {

			feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);
			for (i=0;i<feeCount;i++) {

      			if ((document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feeFlagFour'].value == 3) &&
				   (parseFloat(document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feePaid'].value)>0)) {
					document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = true ;
				}else document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = false ;
			   }
			}

			else if (document.forms['paymentMgrForm'].elements['penaltyFees'].checked == true) {

			feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);
			for (i=0;i<feeCount;i++) {

      			if ((document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feeFlagFour'].value == 4) &&
				   (parseFloat(document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feePaid'].value)>0)) {
					document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = true ;
				}else document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked = false ;
			}
			}
			reverseFees();
}


function calculatePayFees()
{
if (document.paymentMgrForm.levelType.value =='A') {
feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);
busCount = parseInt(document.forms['paymentMgrForm'].elements['busindex'].value);
total = 0;

	for (i=0;i<feeCount;i++)
	{
		//alert(total);
		if (document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].paymentAmount'].value != "")
		total = total + parseFloat(document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].paymentAmount'].value);
	}
	for (i=0;i<busCount;i++)
	{
		if (document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].paymentAmount'].value != "")
		total = total + parseFloat(document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].paymentAmount'].value);
	}
	strValue1 = document.forms['paymentMgrForm'].elements['totalAmt'].value;
    strValue1 = strValue1.replace('(','-');
    strValue1 = strValue1.replace('$','');
    strValue1 = strValue1.replace(')','');
	strValue1 = strValue1.replace(/,/g,"");
    //total = parseFloat(strValue1) + total;

	document.forms['paymentMgrForm'].elements['amount'].value = total.toFixed(2);
	showMethod();
  }
}

function valCurrency()
{

	intValue=parseInt(document.forms['paymentMgrForm'].elements['transactionType'].options[document.forms['paymentMgrForm'].elements['transactionType'].selectedIndex].value);
// 	alert(intValue);
	if ((intValue ==1) || (intValue == 3) || (intValue == 4))
	{
		payMethod=document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].selectedIndex].value;
		if (payMethod != "nsf")
		{
			event.returnValue = false;
		}
	}
}

// Populates the payment methods dynamically based on the transaction type and the amount field -ve or +ve

function showMethod() {
	if (document.paymentMgrForm.levelType.value =='A') {
		intValue=parseInt(document.forms['paymentMgrForm'].elements['transactionType'].options[document.forms['paymentMgrForm'].elements['transactionType'].selectedIndex].value);
	}
	else {
		intValue=1;
	}
	payMethod=document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].selectedIndex].value;
	amount=document.forms['paymentMgrForm'].elements['amount'].value;
	strNegative = amount.substring(0,1);
	document.forms['paymentMgrForm'].elements['method'].options.length=0;
	if (((intValue == 1) ||  (intValue == 2) || (intValue == 4)) && (strNegative != "-")) {
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('','');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Cash','cash');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Check','check');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Credit Card','creditcard');
	} else if ((intValue == 2) && (strNegative == "-")) {
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Correction','correction');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Non Revenue','nonRevenue');
	} else if (intValue == 3) {

			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('','');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Correction','correction');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Non Revenue','nonRevenue');
	} else if ((intValue == 4) && (strNegative == "-")) {
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('','');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Correction','correction');
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('Non Revenue','nonRevenue');
	} else if ((intValue = 5) || (intValue == 6)) {
			document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].options.length]=new Option('','');
	}
}
function getDeposit(){

<%
    Iterator itr = peopleList.iterator();
    int index = 0;
    while (itr.hasNext()) {
       elms.app.people.PeopleList people  = (elms.app.people.PeopleList) itr.next();
        totalDeposit = totalDeposit + Double.parseDouble(people.getDeposit());
%>
      if (document.forms['paymentMgrForm'].elements['paidBy'].selectedIndex == <%=index%> )
            if (document.forms['paymentMgrForm'].elements['method'].options[document.forms['paymentMgrForm'].elements['method'].selectedIndex].value == 'deposit' )
                    if (document.forms['paymentMgrForm'].elements['amount'].value  >  <%=people.getDeposit()%>)
                    	alert('Payment Amount is greater than the deposit amount');
<%
         index ++;
      }
      String strTolalDeposit = elms.util.StringUtils.dbl2$(totalDeposit);
 %>
}
function checkDeposit(){
<%
    Iterator itr1 = peopleList.iterator();
    index = 0;
    while (itr1.hasNext()) {
       elms.app.people.PeopleList people1 = (elms.app.people.PeopleList) itr1.next();
%>
      if (document.forms['paymentMgrForm'].elements['paidBy'].selectedIndex == <%=index%> )  {
            amount = document.forms['paymentMgrForm'].elements['amount'].value;
            amount = amount.replace('$','');
            amount = amount.replace(/,/g,'');
            if (parseFloat(amount)  <=  parseFloat(<%=people1.getDeposit()%>)) return true;
           else return false;
       }
<%index ++;}%>
}

function validateAlphaNumeric(){
	/*
	 * Key Codes
	 * 	a = 97
	 *  z = 122;
	 *	A = 65
	 *	Z = 90;
	 *	space = 32;
	 * 	0 = 48
	 * 	9 = 57
	 *
	 * */
	if((event.keyCode>96 && event.keyCode<123) || (event.keyCode>64 && event.keyCode<91) || (event.keyCode>47 && event.keyCode<58) || event.keyCode == 32){
		event.returnValue = true;
	}else{
		event.returnValue = false;
	}
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}


function getPayAmtForSpecificFees(){

	if (document.paymentMgrForm.levelType.value =='A') {
		feeCount = parseInt(document.forms['paymentMgrForm'].elements['feeindex'].value);
		busCount = parseInt(document.forms['paymentMgrForm'].elements['busindex'].value);

			for (i=0;i<feeCount;i++){
				 if (document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked == true){
				         document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].paymentAmount'].value = document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].feeAmountValue'].value;
			      }else  if (document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].checked'].checked == false){
				         document.forms['paymentMgrForm'].elements['activityFeeList[' + i + '].paymentAmount'].value = "0.00";
                    }
		      }

			for (i=0;i<busCount;i++){
				 if (document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].checked'].checked == true){
				    	document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].paymentAmount'].value = document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].feeAmountValue'].value;
			      }else  if (document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].checked'].checked == false){
				         document.forms['paymentMgrForm'].elements['businessTaxList[' + i + '].paymentAmount'].value = "0.00";
                   }
		      }


	}

}

function validateAdminFee(){
	
		var url = "<%=request.getContextPath()%>/viewPayment.do?action=getReadyToPayFlag&actId=<%=levelId%>";
		xmlhttp.open('POST', url,false);
		xmlhttp.onreadystatechange = validateAdminFeeHandler;
	    xmlhttp.send(null);
		
}
function validateAdminFeeHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
			    var flag=xmlhttp.responseText;
			    var method = document.forms['paymentMgrForm'].elements['method'].value;
			  
				if(flag == 'Y'){
					if(method == 'cash' || method == 'check'){
					swal("Please disable Ready to Pay flag and exclude Administration Fee from fee calculation.");
					document.forms['paymentMgrForm'].elements['method'].value='';
					return false;
					}
				}else{
					if(method == 'creditcard'){
						swal("Please enable Ready to Pay flag and calculate Administration Fee.");
						document.forms['paymentMgrForm'].elements['method'].value='';
						return false;
					}
				}
    }
}
</SCRIPT>
<bean:define id="user" name="user" type="elms.security.User"/>

<html:errors/>
<body  class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" Onload="frmLoad();calculatePayAmt();showMethod();">
<html:form name="paymentMgrForm" type="elms.control.beans.PaymentMgrForm" action="">
<html:hidden property="levelType" value="<%=level%>"/>
<html:hidden property="levelId" value="<%=levelId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b"><%=label%>Payment Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="95%" class="tabletitle">Payments</td>
                                <logic:equal name="paymentMgrForm" property="levelType" value="A">
                                   <td width="4%" class="tabletitle" nowrap>
                                     <html:button property="receipt" value="Print Receipt" styleClass="button" onclick="viewReceipt()"/>&nbsp;
                                     <html:button property="ledger" value="Ledger" styleClass="button" onclick="viewLedger()"/>&nbsp;
                                     <html:button property="back" value="Back" styleClass="button" onclick="goBackToActivity()"/>&nbsp;
                                   </td>
                                </logic:equal>
		     		            <logic:notEqual name="paymentMgrForm" property="levelType" value="A">
                                  <td width="4%" class="tabletitle" nowrap>
                                     <html:button property="back" value="Back" styleClass="button" onclick="goBackToSubProject()"/>&nbsp;
                                  </td>
                                </logic:notEqual>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel" width="13%">
                                <div align="right">Activity</div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tablelabel" width="13%">
                                <div align="right">Plan Check</div>
                                </td>
								<td class="tablelabel" width="15%">
                                <div align="right">Development Fee</div>
                                </td>
                                <td class="tablelabel" width="15%">
                                <div align="right">Contractor Tax</div>
                                </td>
					            <% } %>
								 <td class="tablelabel" width="15%">
                                <div align="right">Penalty Fee</div>
                                </td>
                                <td class="tablelabel" width="10%">
                                <div align="right">Total</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">
                                <logic:equal value="A" name="paymentMgrForm" property="levelType">
                                	<a href="<html:rewrite forward="viewFeesMgr"/>?id=<%=levelId%>" class="hdrs">
                                </logic:equal>
                                Fees
                                <logic:equal value="A" name="paymentMgrForm" property="levelType">
                                	</a>
                                </logic:equal></td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="permitFeeAmt"/></div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="planCheckFeeAmt"/></div>
                                </td>
								<td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="developmentFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="businessTaxFeeAmt"/></div>
                                </td>
					            <% } %>
								<td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="penaltyFeeAmt"/></div>
                                </td>
                                <td class="tabletext">
                                  <div align="right"><bean:write name="financeSummary" property="totalFeeAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Payment</td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="permitPaidAmt"/></div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="planCheckPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="developmentFeePaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="businessTaxPaidAmt"/></div>
                                </td>
					            <% } %>
								<td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="penaltyPaidAmt"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="totalPaidAmt"/></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Balance Due </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="permitAmountDue"/></div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="planCheckAmountDue"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="developmentFeeAmountDue"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="businessTaxAmountDue"/></div>
                                </td>
					            <% } %>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="penaltyAmountDue"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="right"><bean:write name="financeSummary" property="totalAmountDue"/></div>
                                </td>

                                <input type="hidden" name="planCheckFeeAmt" value="<bean:write name="financeSummary" property="planCheckFeeAmt"/>">
								<input type="hidden" name="developmentFeeAmt" value="<bean:write name="financeSummary" property="developmentFeeAmt"/>">
                                <input type="hidden" name="penaltyFeeAmt" value="<bean:write name="financeSummary" property="penaltyFeeAmt"/>">
                                <input type="hidden" name="permitFeeAmt" value="<bean:write name="financeSummary" property="permitFeeAmt"/>">
                                <input type="hidden" name="businessTaxFeeAmt" value="<bean:write name="financeSummary" property="businessTaxFeeAmt"/>">

                                <input type="hidden" name="pmtPermit" value="<bean:write name="financeSummary" property="permitPaidAmt"/>">
                                <input type="hidden" name="pmtPlanCheck" value="<bean:write name="financeSummary" property="planCheckPaidAmt"/>">
								<input type="hidden" name="pmtdevelopmentFee" value="<bean:write name="financeSummary" property="developmentFeePaidAmt"/>">
                                <input type="hidden" name="pmtPenaltyFee" value="<bean:write name="financeSummary" property="penaltyPaidAmt"/>">
                                <input type="hidden" name="pmtBusTax" value="<bean:write name="financeSummary" property="businessTaxPaidAmt"/>">
                                <input type="hidden" name="permitAmt" value="<bean:write name="financeSummary" property="permitAmountDue"/>">
                                <input type="hidden" name="planCheckAmt" value="<bean:write name="financeSummary" property="planCheckAmountDue"/>">
								<input type="hidden" name="developmentAmt" value="<bean:write name="financeSummary" property="developmentFeeAmountDue"/>">
                                <input type="hidden" name="penaltyAmt" value="<bean:write name="financeSummary" property="penaltyAmountDue"/>">
                                <input type="hidden" name="businessTaxAmt" value="<bean:write name="financeSummary" property="businessTaxAmountDue"/>">
                                <input type="hidden" name="permitBouncedAmt" value="<bean:write name="financeSummary" property="permitBouncedAmt"/>">
                                <input type="hidden" name="planCheckBouncedAmt" value="<bean:write name="financeSummary" property="planCheckBouncedAmt"/>">
								<input type="hidden" name="developmentFeeBouncedAmt" value="<bean:write name="financeSummary" property="developmentFeeBouncedAmt"/>">
                                <input type="hidden" name="penaltyFeeBouncedAmt" value="<bean:write name="financeSummary" property="penaltyBouncedAmt"/>">
                                <input type="hidden" name="businessTaxBouncedAmt" value="<bean:write name="financeSummary" property="businessTaxBouncedAmt"/>">
                                <input type="hidden" name="totalBouncedAmt" value="<bean:write name="financeSummary" property="totalBouncedAmt"/>">
                                <input type="hidden" name="totalAmt" value="<bean:write name="financeSummary" property="totalAmountDue"/>">
                                <input type="hidden" name="pcCreditAmt" value="<%=pcCredit%>">
                                <input type="hidden" name="pmtCreditAmt" value="<%=pmtCredit%>">
								<input type="hidden" name="deveFeeCreditAmt" value="<%=developmentFeeCredit%>">
                                <input type="hidden" name="penaltyCreditAmt" value="<%=penaltyCredit%>">

                                <input type="hidden" name="permitECAmt" value="<bean:write name="financeSummary" property="permitCreditAmt"/>">
                                <input type="hidden" name="pcECAmt" value="<bean:write name="financeSummary" property="planCheckCreditAmt"/>">
								<input type="hidden" name="developmentFeeECAmt" value="<bean:write name="financeSummary" property="developmentFeeCreditAmt"/>">
                                <input type="hidden" name="penaltyECAmt" value="<bean:write name="financeSummary" property="penaltyCreditAmt"/>">
                                <input type="hidden" name="busECAmt" value="<bean:write name="financeSummary" property="businessTaxCreditAmt"/>">
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
			<tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="95%" class="tabletitle">Credits</td>
                                <td width="4%" class="tabletitle" nowrap>&nbsp;</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel">
                                <div align="right">Activity</div>
                                </td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tablelabel">
                                <div align="right">Plan Check</div>
                                </td>
								<td class="tablelabel">
                                <div align="right">Development Fee</div>
                                </td>
								<td class="tablelabel">
                                <div align="right">Contractor Tax</div>
                                </td>
					            <% } %>
                                <td class="tablelabel">
                                <div align="right">Deposit</div>
                                </td>
                                <td class="tablelabel">
                                <div align="right">Total</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Amount</td>
                                <td class="tabletext"><div align="right"><%=StringUtils.dbl2$(pmtCredit)%></div></td>
					            <% if(projectName.equals(elms.common.Constants.PROJECT_NAME_BUILDING)){ %>
                                <td class="tabletext"><div align="right"><%=StringUtils.dbl2$(pcCredit)%></div></td>
								<td class="tabletext"><div align="right"><%=StringUtils.dbl2$(businessTaxCredit)%></div></td>
								<td class="tabletext"><div align="right"><%=StringUtils.dbl2$(developmentFeeCredit)%></div></td>
								<% } %>
                                <td class="tabletext"><div align="right"><%=strTolalDeposit%></div></td>
								<% double tot = (pcCredit + pmtCredit + developmentFeeCredit + businessTaxCredit + totalDeposit);%>
                                <td class="tabletext"><div align="right"><%=StringUtils.dbl2$(tot)%></div></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
<%if(totalDeposit > 0.00){ %>
			<tr>
                <td>&nbsp;</td>
            </tr>


			<tr>
				<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
 						<td width="95%" class="tabletitle">Deposits</td>

						<td width="4%" class="tablelabel" nowrap>&nbsp;</td>
					</tr>
				 </table>
				 </td>
			</tr>
            <tr>
				<td background="../images/site_bg_B7C1CB.jpg">
				<table width="100%" border="0" cellspacing="1" cellpadding="2">
					<tr>
						<td class="tablelabel"><div align="center">People Type</div></td>
                        <td class="tablelabel"><div align="center">People Name</div></td>
                        <td class="tablelabel"><div align="right">Actual Deposit</div></td>
						<td class="tablelabel"><div align="right">Fees Paid</div></td>
						<td class="tablelabel"><div align="right">Remaining Deposit</div></td>
					</tr>
<%
Iterator itr2 = peopleList.iterator();
    index = 0;
    while (itr2.hasNext()) {
       elms.app.people.PeopleList people2 = (elms.app.people.PeopleList) itr2.next();
 String name = people2.getName();
int nameIndex = name.indexOf('(');
name = name.substring(0,nameIndex);
%>
					<tr>
                        <td class="tabletext"><div align="left"><%=people2.getPeopleType()%></div></td>
                        <td class="tabletext"><div align="left"><%=name%></div></td>
                        <td class="tabletext"><div align="right"><a href="<%=contextRoot%>/addDeposit.do?peopleId=<%=people2.getPeopleId()%>&psaId=<%=levelId%>&psaType=<%=level%>" class="hdrs"><%=StringUtils.dbl2$(StringUtils.s2d(people2.getDeposit()))%></a></div></td>
						<td class="tabletext"><div align="right"><%=people2.getBalanceDeposit()%></div></td>
						<td class="tabletext"><div align="right"><%=StringUtils.dbl2$(StringUtils.s2d(people2.getDeposit()))%></div></td>
                    </tr>
<%}%>
                 </table>
                 </td>
               </tr>
              </table>
              </td>
            </tr>
<%} %>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="97%" class="tabletitle">Transaction Manager </td>
                                <td width="1%" class="tabletitle"><nobr>&nbsp;&nbsp;
                                <security:editable levelId="<%=levelId%>" levelType="<%=level%>" editProperty="checkUser">
                                <html:button property="pay" value=" Pay " styleClass="button" onclick="return validateFunction();"/>
                                </security:editable>
                                 &nbsp;</nobr></td>
                                <logic:equal name="paymentMgrForm" property="levelType" value="A">
                                <td id="reverse" width="1%" class="tabletitle"><nobr>&nbsp;
                                <html:button property="reverseAll" value="Reverse" styleClass="button" onclick="reverseAllFees();"/>&nbsp;</nobr></td>
                                <td width="1%" class="tabletitle"><nobr>
                                <html:button property="reset" value="Reset" styleClass="button" onclick="frmLoad();calculatePayAmt();"/> &nbsp;&nbsp;</nobr></td>
                                </logic:equal>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:equal name="paymentMgrForm" property="levelType" value="A">
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Payment Type </td>
                                <td class="tabletext" width="82%">
                                <html:checkbox property="permitFees" onclick="calculatePayAmt();"/> Permit Fees&nbsp;&nbsp;
                                <html:checkbox property="planCheck" onclick="calculatePayAmt();"/> Plan Check&nbsp;&nbsp;
                                <html:checkbox property="developmentFees" onclick="calculatePayAmt();"/> Development Fees&nbsp;&nbsp;
                                <html:checkbox property="businessTax" onclick="calculatePayAmt();"/> Contractor Tax&nbsp;&nbsp;
                                <html:checkbox property="penaltyFees" onclick="calculatePayAmt();"/> Penalty Fees </td>
                            </tr>
                            </logic:equal>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">
                                Transaction Type</td>
                                <td class="tabletext" width="82%">
                                <logic:equal name="paymentMgrForm" property="levelType" value="A">
                                <html:select property="transactionType" styleClass="textbox" onchange="dspTable(this.options[this.selectedIndex].value);calculatePayAmt();" onblur="showMethod();">
                                	<html:option value="1">Payment of Balance Due</html:option>
                                	<html:option value="3">Unpay All Fees</html:option>
                                	<html:option value="4">Specific Fee Payment</html:option>
                                </html:select>
                            </logic:equal>
                            <logic:equal name="paymentMgrForm" property="levelType" value="Q"><html:hidden property="transactionType" value="1"/> Payment of Balance Due</logic:equal></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Amount</td>
                                <td class="tabletext" width="82%"><logic:equal name="paymentMgrForm" property="levelType" value="A">
                                <html:text property="amount" size="10" styleClass="textboxm" onkeypress="return valCurrency();" onblur="showMethod();"/></logic:equal>
                                <logic:equal name="paymentMgrForm" property="levelType" value="Q"><html:hidden property="amount"/>
                                <bean:write name="financeSummary" property="totalAmountDue"/></logic:equal></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Method</td>
                                <td class="tabletext" width="82%"><html:select property="method" styleClass="textbox" onchange="validateAdminFee();">
                                <html:option value="">&nbsp;</html:option>
                                </html:select></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Paid By </td>
                                <td class="tabletext" width="82%">
                                	<html:select property="paidBy" styleClass="textbox" onchange="getDeposit();" >
                                		<html:option value="-1">Please Select</html:option>
                                		<html:options collection="peopleList" property="peopleId" labelProperty="name"/>
                                	</html:select>
                                	 &nbsp;&nbsp;&nbsp;Other<html:checkbox property="other"/> &nbsp;
                                		<html:text property="otherText" size="25" styleClass="textbox"/>
                                	</td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Check No. Confirmation</td>
                                <td class="tabletext" width="8%"><html:text property="checkNoConfirmation" size="20" styleClass="textbox" maxlength="10" onkeypress="return validateAlphaNumeric();"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Authorized By</td>
                                <td class="tabletext" width="82%"> <html:text property="authorizedBy" size="25" styleClass="textbox"/> </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="18%">Comments</td>
                                <td class="tabletext" width="82%"><html:textarea rows="3" cols="25" property="comments" styleClass="textbox"></html:textarea></td>
                            </tr>
                        </table>

                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table id="paymentTable" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Finance Manager</td>

                                <td width="1%" class="tablelabel"><nobr>&nbsp;<html:button property="calculate" value="Calculate Payment" styleClass="button" onclick="calculatePayFees();"/> &nbsp;<html:button property="reverse" value="Reverse" styleClass="button" onclick="reverseFees()"/>&nbsp;&nbsp;</nobr></td>
                            </tr>
                        </table>

                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

                            <tr>
                                <td colspan="9" class="tablelabel">`Development, Penalty and Permit Fees</td>
                            </tr>
                            <tr>
                                <td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">&nbsp;Fee Description</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Payment</div>
                                </td>
                                <td class="tabletext">
                                <div align="left"></div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Fee Description</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Payment</div>
                                </td>

                            </tr>
                            <tr>
                                <logic:equal name="paymentMgrForm" property="levelType" value="A"> <%
						boolean rowFlag = false;
						int i = 0;
%> <nested:iterate property="activityFeeList"> <%
						if (rowFlag) {
							out.println("<TD></TD>");
							rowFlag = false;
							} else {
							if (i>0) {
								out.println("</tr><tr>");
								}
							rowFlag = true;
						}
						i++;

%>
								<td class="tabletext">
                                <div align="left"><nested:checkbox property="checked" onclick="getPayAmtForSpecificFees();" /></div>
                                </td>
                                <td class="tabletext">
                                <div align="left"><nested:write property="feeDescription"/></div>
                                </td>
                                <td class="tabletext">
                                <div align="left"><nested:write property="feeAmount"/></div>
                                <nested:hidden property="feeAmount"></nested:hidden>
                                <nested:hidden property="feeAmountValue"></nested:hidden>
                                </td>
                                <td class="tabletext">
                                <div align="left"><nested:text property="paymentAmount" styleClass="textboxm" size="12" /></div>
                                </td>
                                <nested:hidden property="feePaid" /><nested:hidden property="feeAccount" /><nested:hidden property="bouncedAmount" /><nested:hidden property="feeFlagFour" />
                                </nested:iterate> <input type="hidden" name="feeindex" value="<%=i%>">
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

                            <tr class="tablelabel">
                                <td class="tabletext"><br>
                                </td>
                            </tr>
                        </table>

                        <!-- bussinessTax -->
 					<table width="100%" border="0" cellspacing="01" cellpadding="0">
					<tr>
						<td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Contractor Tax</td>
								<td width="1%" class="tablelabel"><nobr>&nbsp;<html:button property="reverse" value="Reverse" styleClass="button" onclick="reverseBTFees()"/>&nbsp;&nbsp;</nobr></td>

                            </tr>
						</table>
						</td>
					</tr>
					<tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
								<td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Name</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Payment</div>
                                </td>
                                <td class="tabletext">
                                <div align="left"></div>
                                </td>
								<td class="tablelabel">
                                <div align="left">&nbsp;</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Name</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Amount Due</div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Payment</div>
                                </td>
                            </tr>
                            <tr>

                            <%
								rowFlag = false;
								i = 0;
							%>
                            	<nested:iterate property="businessTaxList">
							<%
								if (rowFlag) {
										out.println("<TD></TD>");
										rowFlag = false;
									} else {
									if (i>0) {
										out.println("</tr><tr>");
									}
									rowFlag = true;
								}
								i++;
							%>
							<nested:hidden property="peopleId"/>
								<td class="tabletext">
                                <div align="left"><nested:checkbox property="checked"  onclick="getPayAmtForSpecificFees();" /></div>
                                </td>
                                <td class="tabletext">
                                <div align="left"><nested:write property="name"/></div>
                                </td>
                                <td class="tabletext"> <nested:write property="feeAmount"/>
                                <nested:hidden property="feeAmount"></nested:hidden>
                                <nested:hidden property="feeAmountValue"></nested:hidden>
                                </td>
                                <td class="tabletext"> <nested:text property="paymentAmount" styleClass="textboxm" size="12" onkeypress="return validateCurrency();"/></td>
                                 <nested:hidden property="feePaid" /><nested:hidden property="feeAccount" /><nested:hidden property="bouncedAmount" /><nested:hidden property="feeFlagFour" />

                                </nested:iterate>
                                <input type="hidden" name="busindex" value="<%=i%>">
                                </logic:equal>

                            </tr>
                        </table>
                </table>
                </td>
            </tr>
        </table>
        <html:hidden property="levelId" value="<%=levelId%>"/> <html:hidden property="level" value="<%=level%>"/> </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
</table>

<td width="1%">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td height="32">&nbsp;</td>
    </tr>
</table>
</td>
</tr>
</table>
</html:form>
</body>
</html:html>

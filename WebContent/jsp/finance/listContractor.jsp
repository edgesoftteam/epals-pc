<%@ page import='elms.agent.*' %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*" %>
<%@ page import="javax.sql.RowSet,java.sql.*" %>
<%@ page import="elms.app.people.*" %>
<%@ page import="elms.util.db.*"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%
	String licenseNbr = request.getParameter("licenseNumber");
	java.util.ArrayList peopleList = (java.util.ArrayList) new PeopleAgent().getSearchResults("C",licenseNbr,"","","");
	pageContext.setAttribute("peopleList", peopleList);
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<SCRIPT LANGUAGE="JavaScript"><!--
function sendToOpener(strValue) {

		//alert(strValue);
		var myArray = strValue.split('^');
		//alert(myArray[0]);
		busTaxCount = opener.document.forms[2].elements['busTaxCount'].value;
        opener.document.forms[2].elements['businessTaxList[' + busTaxCount + '].peopleId'].value = parseInt(myArray[0]);
        opener.document.forms[2].elements['businessTaxList[' + busTaxCount + '].licenseNbr'].value = myArray[1];
        opener.document.forms[2].elements['businessTaxList[' + busTaxCount + '].name'].value  = myArray[2];
}
//--></SCRIPT>



<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/listContractor">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Contractor List</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle">Contractors</td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel">Name</td>
                      <td class="tablelabel">License # </td>
                      <td class="tablelabel">&nbsp;</td>
                    </tr>

<logic:iterate id="people" name="peopleList">
                    <tr class="tabletext">
                      <td><bean:write name="people" property="name" /></td>
                      <td><bean:write name="people" property="licenseNbr" /></td>
                      <td><input type="button" value="Select" class="button" name="<bean:write name="people" property="peopleId"/>^<bean:write name="people" property="licenseNbr"/>^<bean:write name="people" property="name"/>" onclick="javascript:sendToOpener(this.name);window.close();">
                    </tr>
</logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>

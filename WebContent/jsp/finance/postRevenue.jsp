<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<%
      String contextRoot = request.getContextPath();
      java.util.List departmentList = (java.util.List) request.getAttribute("departmentList");
      pageContext.setAttribute("departmentList", departmentList);
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>

<script language="JavaScript">
function checkNum(data)
{
   var validNum = "-0123456789.";
   var i = count = 0;
   var dec = ".";
   var space = " ";
   var val = "";

   for (i = 0; i < data.length; i++) {
      if (validNum.indexOf(data.substring(i, i+1)) != "-1")
         val += data.substring(i, i+1);
      if (data.substring(i, i+1) == "(")
      	 val += "-";
      }
   return val;
}

function dollarFormatted(amount) {
	return '$' + commaFormatted(amount);
}

function commaFormatted(amount) {
		var delimiter = ","; // replace comma if desired
		var a = amount.split('.',2)
		var d = a[1];
		var i = parseInt(a[0]);
		if(isNaN(i)) { return ''; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		var n = new String(i);
		var a = [];
		while(n.length > 3)
		{
		var nn = n.substr(n.length-3);
		a.unshift(nn);
		n = n.substr(0,n.length-3);
		}
		if(n.length > 0) { a.unshift(n); }
		n = a.join(delimiter);
		if(d.length < 1) { amount = n; }
		else { amount = n + '.' + d; }
		amount = minus + amount;
		return amount;
}
// end of function CommaFormatted()

function formatCurrency(field) {
		var i = parseFloat(checkNum(field.value));
		if(isNaN(i)) { i = 0.00; }
		var minus = '';
		if(i < 0) { minus = '-'; }
		i = Math.abs(i);
		i = parseInt((i + .005) * 100);
		i = i / 100;
		s = new String(i);
		if(s.indexOf('.') < 0) { s += '.00'; }
		if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
		s = minus + s;
		field.value = dollarFormatted(s);
		return true;
}

function submitAction(act) {
	//alert(act);
	document.forms[0].action='<%=contextRoot%>/postRevenue.do?action='+act;
	document.forms[0].submit();
	return true;
}

</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="postRevenueForm" type="elms.control.beans.finance.PostRevenueForm" action="">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="5">
            <tr>
                <td width="1%" background="../images/site_bg_B7C1CB.jpg" colspan="3">
                    <img src="../images/spacer.gif" width="1" height="15">
                    <font class="con_hdr_3b">Interface to Fund Accounting</font><br>&nbsp;
                </td>
            </tr>
            <TR></tr>
            <tr>
		<td width="20%" class="tabletext"><div align="left"><nobr>Date: <html:text property="inputDate" maxlength="10"  styleClass="textboxd" onkeypress="return validateDate();"/> <A href="javascript:show_calendar('forms[0].inputDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border=0/></a></nobr></td>
		<td width="35%" class="tabletext"><div align="left"><nobr>Department: <html:select property="inputDepartment" styleClass="textbox"><html:options collection="departmentList" property="departmentCode" labelProperty="description"/> </html:select></nobr></td>
		<td width="45%" class="tabletext"><div align="left"><html:button property="Revenue" value="Get Revenue" styleClass="button" onclick="return submitAction('GetRevenue');" /></td>
            </tr>
	</table>
	</td>
    </tr>
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="98%" class="tabletitle"><nobr>Batch # <bean:write name="postRevenueForm" property="batchNbr" /> <logic:present name="postRevenueForm" property="batchDate" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Posted On : <bean:write name="postRevenueForm" property="batchDate" /></logic:present></nobr></td>

                                <td width="1%" class="tablebutton"><nobr>
                                   <logic:notPresent name="postRevenueForm" property="batchDate" >
                                	<html:button property="Save" value="Save" styleClass="button" onclick="return submitAction('Save');"/>&nbsp;
                                	<html:button property="FundAccount" value="Interface Fund Accounting" styleClass="button" onclick="return submitAction('IFA');"></html:button>&nbsp;
                                   </logic:notPresent>
                                	<html:button property="Cancel" value="Cancel" styleClass="button" onclick="return submitAction('Cancel');"></html:button>&nbsp;
                                 </nobr>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel">
                                <div align="right"><nobr>Account #</nobr></div>
                                </td>
                                <td class="tablelabel">
                                <div align="right"><nobr>OBC Revenue</nobr></div>
                                </td>
                                <td class="tablelabel">
                                <div align="right"><nobr>Fund Accounting</nobr></div>
                                </td>
                                <td class="tablelabel">
                                <div align="left">Comments</div>
                                </td>
                            </tr>
		<logic:present name="postRevenueForm" property="accountList">
                       <nested:iterate name="postRevenueForm" property="accountList" id="account">
			 <nested:notEqual property="accountNbr" value="Total">
                            <tr>
                                <td class="tabletext">
                            <nested:notEqual name="postRevenueForm" property="revenueAmt" value="$0.00">
                                <div align="right"><nested:write name="account" property="accountNbr"/></div>
			    </nested:notEqual>
                            <nested:equal name="postRevenueForm" property="revenueAmt" value="$0.00">
				    <logic:notPresent name="postRevenueForm" property="batchDate" >
					<div align="right"><nested:text name="account" property="accountNbr" size="5" maxlength="5"  styleClass="textboxm"/></div>
				    </logic:notPresent>
				    <logic:present name="postRevenueForm" property="batchDate">
                                	<div align="right"><nested:write name="account" property="accountNbr"/></div>
				    </logic:present>
			    </nested:equal>
                                </td>
                                <td class="tabletext">
                                <div align="right"><nested:write name="account" property="revenueAmt"/></div>
                                </td>
                                <td class="tabletext">
			                    <div align="right"><nested:write name="account" property="postedAmt" /></div>
                                </td>
                                <td class="tabletext" width="55%">
			    <logic:notPresent name="postRevenueForm" property="batchDate" >
                                <div align="left"><nested:text name="account" property="comments" size="60" maxlength="60"  styleClass="textbox"  /></div>
                            </logic:notPresent>
			    <logic:present name="postRevenueForm" property="batchDate">
                                <div align="leftt"><nested:write name="account" property="comments" /></div>
                            </logic:present>
                                </td>
                            </tr>
                          </nested:notEqual>
                        </nested:iterate>
		</logic:present>
                            <tr>
                                <td class="tablelabel">
                                <div align="right">Total</div></td>
                                <td class="tablelabel">
                                <div align="right"><bean:write name="postRevenueForm" property="totalRevenue"/></div>
                                </td>
                                <td class="tablelabel">
                                <div align="right"><bean:write name="postRevenueForm" property="totalRevenuePosted"/></div>
                                </td>
                                <td class="tablelabel"></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
</html:form>
</body>
</html:html>
<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<%
	String contextRoot = request.getContextPath();

	String levelId = (String)request.getAttribute("levelId");

	String level = (String)request.getAttribute("level");

	String activityNumber = (String)session.getAttribute("actNumber");
      if (activityNumber == null ) activityNumber = "";

      String ledgerURL=LookupAgent.getReportsURL()+activityNumber;
%>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript">
function printLedger() {
  window.open('<%=ledgerURL%>','ledger', 'left=0, top=0, menubar=no, status=no, location=no, toolbar=no, scrollbars=yes, resizable=yes, width=800, height=600');
  //parent.f_content.location.href="<%=contextRoot%>/jsp/project/printPermit.jsp?actNbr=<%=activityNumber%>&actType=LEDGER";
  //document.location='<%=contextRoot%>/printLedger.do?actNbr=<%=activityNumber%>&actType=LEDGER'
}
function reverseSpecificTransaction(paymentId){
	alert("paymentId::"+paymentId);
	
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="ledgerForm" type="elms.control.beans.LedgerForm" action="/saveLedger">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">View Transaction History</font>&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="ledgerForm" property="activityNbr"/></font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td class="tabletitle" width="98%">Transactions</td>

                        <td width="1%" class="tablebutton" align="right"><nobr>
			  			<html:submit property="Cancel" value="Back" styleClass="button"/>
						<security:editable levelId="<%=levelId%>" levelType="<%=level%>" editProperty="checkUser">
						<html:submit property="Save" value="Save" styleClass="button"/>
						</security:editable>

			            </nobr>
    		      </td>
 	            </tr>
                   </table>
                </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td colspan="3" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
          	      <td class="tablelabel">Hide</td>
          	      
          	      <td class="tablelabel">Tran #</td>
                      <td class="tablelabel" nowrap="nowrap">Tran Date</td>
                      <td class="tablelabel" nowrap="nowrap">Post Date</td>
                      <td class="tablelabel">Type</td>
                      <td class="tablelabel">Amount</td>
                      <td class="tablelabel">Method</td>
                      <td class="tablelabel">Comments</td>
                      <td class="tablelabel" nowrap="nowrap">Check/Conf #</td>
                      <td class="tablelabel">Payee</td>
                      <td class="tablelabel" nowrap="nowrap">Reverse Specific Transaction</td>
                      <td class="tablelabel" nowrap="nowrap">Reversed By</td>
                      <td class="tablelabel" nowrap="nowrap">Reversed Date</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" colspan="4">Entered By</td>
                      <td class="tablelabel" colspan="3">Authorized By</td>
                      <td class="tablelabel" colspan="6">Invalidated By</td>
                    </tr>
<nested:iterate property="transactionList">
                    <tr >
                      <td class="tabletext"><nested:checkbox property="hide"/></td>
                      <td class="tabletext"><a href="<%=contextRoot%>/viewLedger.do?activityId=<bean:write name="ledgerForm" property="activityId"/>&paymentId=<nested:write property="transactionId"/>&editaction=edit" class="hdrs"><nested:write property="transactionId"/></a></td>
                      <td class="tabletext"><nested:write property="date"/></td>
                      <td class="tabletext"><nested:write property="postDate"/></td>
                      <td class="tabletext"><nested:write property="type"/></td>
                      <td class="tabletext" align="right"><nested:write property="amount"/></td>
                      <td class="tabletext"><nested:write property="method"/></td>
                      <td class="tabletext"><nested:text size="40" styleClass="textbox" property="comments"/></td>
                      <td class="tabletext"><nested:write property="checkNbr"/></td>
                     <%--  <td class="tabletext"><nested:write property="depositId"/>&nbsp;<nested:write property="otherPayee"/></td> --%>
                      <td class="tabletext"><nested:write property="otherPayee"/></td>
                      <td class="tabletext" align="center">
                      <nested:equal value="N" property="voidPayment">
                      <a href="<%=contextRoot%>/reversePayment.do?activityId=<bean:write name="ledgerForm" property="activityId"/>&paymentId=<nested:write property="transactionId"/>" ><input type="button" value="Reverse"/></a>
                      </nested:equal>
                      </td>
                      <td class="tabletext"><nested:write property="voidBy"/></td>
                      <td class="tabletext"><nested:write property="voidDate"/></td>
                    </tr>
                    <tr>
                      <td class="tabletext" colspan="4">&nbsp;<nested:write property="enteredBy"/></td>
                      <td class="tabletext" colspan="3"><nested:write property="authorizedBy"/></td>
                      <td class="tabletext" colspan="6"><nested:write property="voidBy"/></td>
                    </tr>
                    <tr></tr>
</nested:iterate>
                  </table>
                </td>
              </tr>
<logic:present name="ledgerForm" property="detailList">
	      <tr>
		 <td>
		     <table width="100%" border="0" cellspacing="0" cellpadding="0">
		       <tr>
		           <td class="tabletitle" width="98%">Transaction #<bean:write name="ledgerForm" property="paymentId"/></td>
		       </tr>
		     </table>
		  </td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td colspan="3" class="tabletext"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
          	          <td class="tablelabel">Account #</td>
          	          <td class="tablelabel">Fee Description</td>
          	          <td class="tablelabel">Fee Type</td>
                      <td class="tablelabel">Amount</td>
                      <td class="tablelabel">Comments</td>
                    </tr>
<nested:iterate property="detailList">
                    <tr>
                      <td class="tabletext" align="right"><nested:write property="accountNbr"/></td>
                      <td class="tabletext"><nested:write property="description"/></td>
                      <td class="tabletext"><nested:write property="feeType"/></td>
                      <td class="tabletext" align="right"><nested:write property="amount"/></td>
                      <td class="tabletext"><nested:text size="40" styleClass="textbox" property="comment"/></td>
                    </tr>
</nested:iterate>
                  </table>
                </td>
              </tr>
</logic:present>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

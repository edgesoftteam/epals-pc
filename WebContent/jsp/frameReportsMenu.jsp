<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<%@ page import="elms.common.*,java.util.*,elms.security.*"%>

<html:html>
<style type="text/css">
#menu1 {cursor: hand}
#menu1a {display: none}
#menu1a1 {display: none}
#menu1a11 {display: none}
#menu1a12 {display: none}
#menu1a2 {display: none}
#menu1a3 {display: none}
#menu1a4 {display: none}
#menu1b {cursor: hand}
#menu1b1 {display: none}
#menu1b2 {display: none}
#menu1b3 {display: none}
#menu1c {display: none}
#menu1c1 {display: none}
#menu1c2 {display: none}
#menu1c3 {display: none}
#menu2 {cursor: hand}
#menu2a {display: none}
#menu2a1 {display: none}
#menu2a2 {display: none}
#menu1d1 {display: none}
#menu1e1 {display: none}
#menu1f1 {display: none}
#menu1g1 {display: none}
#menu1h1 {display: none}
#menu1i1 {display: none}
</style>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">

</head>

<%
   String contextRoot = request.getContextPath();
%>

<body bgcolor="#e5e5e5" text="#000000" background="images/site_bg_e5e5e5.jpg" style="margin:0; overflow-x:auto; overflow-y:auto;">

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="10" height="8"><font class="black2b">Reports Explorer</font></td>
  </tr>
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="201" height="1"></td>
  </tr>
  <tr>
    <td><img src="images/spacer.gif" width="1" height="10"></td>
  </tr>
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
	<td class="treetext">
		<div class="adminMenuContainer">

		<div class="sub_menu" onclick="if(document.all.menu1a1.style.display =='block'){document.all.menu1a1.style.display='none'; document['plusminus1'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1a1.style.display='block'; document['plusminus1'].src='images/site_blt_minus_003366.gif'}">
		<img name="plusminus1" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Project Maintenance">Permits</html:link></div>
			<ul id="menu1a1">
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Permits Issued.pdf" target="f_content" class="tree1"  >Permits Issued</a></li>
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Expired Permits.pdf"  target="f_content" class="tree1"  >Expired Permits</a></li>
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Plan Check Log.pdf"  target="f_content" class="tree1"  >Plan Check Log</a></li>
			</ul>
		<div class="sub_menu" onclick="if(document.all.menu1a11.style.display =='block'){document.all.menu1a11.style.display='none'; document['plusminus11'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1a11.style.display='block'; document['plusminus11'].src='images/site_blt_minus_003366.gif'}">
		<img name="plusminus11" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Project Maintenance">Inspections</html:link></div>
			<ul id="menu1a11">
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Inspection Activity.pdf"  target="f_content" class="tree1"  >Inspection Activity</a></li>
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Inspection Logs.pdf"  target="f_content" class="tree1"  >Inspection Logs</a></li>
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Inspector Report.pdf"  target="f_content" class="tree1"  >Inspector Report</a></li>
			</ul>
		<div class="sub_menu" onclick="if(document.all.menu1a12.style.display =='block'){document.all.menu1a12.style.display='none'; document['plusminus12'].src='images/site_blt_plus_003366.gif';} else {document.all.menu1a12.style.display='block'; document['plusminus12'].src='images/site_blt_minus_003366.gif'}">
		<img name="plusminus12" src="images/site_blt_plus_003366.gif">&nbsp;<html:link href="#" styleClass="tree1" title="Project Maintenance">Fees</html:link></div>
			<ul id="menu1a12">
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Daily Fee Report.pdf"  target="f_content" class="tree1"  >Daily Fee Report</a></li>
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Permit Revenue.pdf"  target="f_content" class="tree1"  >Permit Revenue</a></li>
				<li><img src="images/right_arrow.gif">&nbsp;<a href="<%=contextRoot %>/reports/md/Permit Valuation.pdf"  target="f_content" class="tree1"  >Permit Valuation</a></li>
			</ul>
		</td>
	</tr>
	</table>
	</td>
</tr>
</table>
</body>
</html:html>
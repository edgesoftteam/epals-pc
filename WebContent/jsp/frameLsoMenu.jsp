<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="java.util.*,elms.agent.*" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
</head>
<script src="script/newLsoTree.js"> </script>
<script language="javascript" type="text/javascript" src="script/actb.js"></script>
<script language="javascript" type="text/javascript" src="script/common.js"></script>

<%!
java.util.List streets = new java.util.ArrayList(); // for the Street list
%>
<%
String contextRoot = request.getContextPath();
java.util.List lsoTreeList = (java.util.List)session.getAttribute("treeList");
String messageStr = "1";
if  (lsoTreeList.size()==0) { messageStr = "0"; }
String streetId = (String)session.getAttribute("streetId");
if(streetId == null || streetId.equals("")) streetId = "-1";
int intStreetId = Integer.parseInt(streetId);
// Refreshing the LSO tree .. onLoad of this page.
String onloadAction = (String)session.getAttribute("onloadAction");
if (onloadAction == null ) onloadAction = "";
if (onloadAction.equals("changeContent") ) onloadAction =  "parent.f_top.location.href='" + contextRoot + "/jsp/frameTop.jsp?hl=home';changeContent(" + messageStr + ");parent.f_psa.location.href='" + contextRoot + "/viewPsaTree.do?lsoId=-1'";

 streets = (List) AddressAgent.getStreetArrayList();
request.setAttribute("streets",streets);

//Added bY RAJ
String radioStatus ="";
String  activeOrAll="";
if(request.getAttribute("radioStatus")!=null) radioStatus=(String)request.getAttribute("radioStatus");
String lsoId="";
if(request.getAttribute("lsoId")!=null) {lsoId=(String)request.getAttribute("lsoId");}

if(request.getParameter("b")!=null) activeOrAll = request.getParameter("b");
%>
<script type="text/javascript">
var b=5;
function active(a) {
b=a;
document.lsoSearchForm.activeOrAll.value=b;
if(b==0) parent.f_lso.location.href="<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=lsoId%>&b=<%=0%>&clear=YES";
if(b==1) parent.f_lso.location.href="<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=lsoId%>&b=<%=1%>&clear=YES";
}

function radio() {
var radio1;
radio1=document.forms[0].elements['radioStatus'].value;
if(radio1=='All') {
var b=document.lsoSearchForm.activeOrAll.value;
if(b==0)document.lsoSearchForm.radiobutton(0).checked='checked';
if(b==1)document.lsoSearchForm.radiobutton(1).checked='checked';
}
}


		var Tree = new Array;
		// nodeId | parentNodeId | nodeName | nodeUrl | nodeAlt | lso id
		<% java.util.Iterator iter = lsoTreeList.iterator();
		int i = 0;
		while(iter.hasNext()){
  			out.println((String) iter.next());
		}
		%>
	</script>
<script language="JavaScript"><!--
function setPointer() {

var streetText=document.forms[0].streetText.value;
for(var q=0;q<<%=streets.size()%>;q++){
	if((streetText.toUpperCase())==((document.forms[0].streetName[q].text).toUpperCase())){
		document.forms[0].streetName[q].selected=true;
	}
}
parent.f_content.location.href="<%=contextRoot%>/jsp/frameContentWait.jsp"
    if (document.all)
        for (var i=0;i < document.all.length; i++)
             document.all(i).style.cursor = 'wait';
}
function busy() {
  parent.f_psa.location.href="<%=contextRoot%>/jsp/pleaseWait.jsp"
}

function onEnterKeyPress(event) {
	if(event.keyCode == 13) {

setPointer();
document.forms[0].action="<%=contextRoot%>/lsoSearch.do?clear=YES";
document.forms[0].submit();
}
}

function changeContent(str) {
if (str==0) { parent.location.href="<%=contextRoot%>/viewHome.do?str="+str; }
else { parent.f_content.location.href="<%=contextRoot%>/jsp/frameContent.jsp?str="+str; }

}

var streetArray = new Array();
<%
if(streets != null && streets.size() > 0){

for(int j=0; j<streets.size();j++){
elms.app.lso.Street st = (elms.app.lso.Street)streets.get(j);
%>
streetArray[<%=j%>] = '<%=st.getStreetName()%>';
<%}}%>
//--></script>

<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg" onload="<%=onloadAction%>;radio();">
<html:form  action="/lsoSearch?clear=YES" >
<html:hidden property="radioStatus" value="<%=radioStatus%>"/>
<html:hidden property="activeOrAll" value="<%=activeOrAll%>"/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width = "99%" class="frametitle">&nbsp;&nbsp;LSO EXPLORER</td>
          <td width = "1%" class="frametitle" align="right" nowrap><html:submit  property="Find" value="Find"  styleClass="button" onclick="javascript:setPointer();" />&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
	<td valign="top" class="tableform-top"><img src="images/spacer.gif" width="200" height="5"></td>
  </tr>
  <tr>
    <td class="tableform">
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
			<tr>
				<td><font class="framelabel">Street #</font></td>
				<td><font class="framelabel">Street Name</font></td>
			</tr>
      <tr>
          <td valign="top" nowrap><html:text  property="streetNumber" size="4" styleClass="textbox"/></td>
          <td valign="top" nowrap>
          <div style="visibility:hidden">
				<html:select property="streetName" styleClass="textbox" style="position:absolute">
					<html:option value=""> Please select</html:option>
					<html:options collection="streets" property="streetId" labelProperty="streetName" />
				</html:select>
			</div>
			<input type="text" name="streetText" style="" id="st" size="18%"/>
			<script>
				var streetObj = new actb(document.getElementById('st'),streetArray);
				if(document.forms[0].streetName.options[document.forms[0].streetName.selectedIndex].value != ""){
					document.forms[0].streetText.value = document.forms[0].streetName.options[document.forms[0].streetName.selectedIndex].text;
				}
				//setTimeout(function(){streetObj.actb_keywords = custom2;},10000);
			</script>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
      <tr>
        <td class="tableform">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            	<td>
            		<table cellpadding="2" cellspacing="0" border="0">
						<%if(!radioStatus.equals("All")){  %>
							<tr>
								<td><html:radio property="radiobutton" value="radio_active" onclick="javascript:document.forms[0].submit();" /></td>
								<td class="framelabel">Active</td>
								<td><img src="images/spacer.gif" width="10" height="1"></td>
								<td><html:radio property="radiobutton" value="radio_all" onclick="javascript:document.forms[0].submit();" /></td>
								<td class="framelabel">All</td>
							</tr>
						<% }  else if(radioStatus.equals("All")){ %>
							<tr>
								<td><html:radio property="radiobutton" value="radio_active" onclick="return active(0);" /></td>
								<td class="framelabel">Active</td>
								<td><img src="images/spacer.gif" width="10" height="1"></td>
								<td><html:radio property="radiobutton" value="radio_all" onclick="return active(1)" /></td>
								<td class="framelabel">All</td>
							</tr>
						<%} %>
					</table>
				</td>
                <td ><img src="images/spacer.gif" width="1" height="3"><br>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
		<td valign="top" class="tableform-bottom"><img src="images/spacer.gif" width="200" height="5"></td>
    </tr>
    <tr>
        <td><img src="images/spacer.gif" width="1" height="10"></td>
    </tr>
  <tr>
    <td>
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
    	<tr><td>
			<div id="tree">
				<script type="text/javascript">
					createTree(Tree);
					<%
						String lsoNodeId = request.getParameter("lsoNodeId");
						Integer childId= new Integer(0);
						if(lsoNodeId == null) lsoNodeId = "0";
						try{
						 childId = new Integer(lsoNodeId);
						 } catch(Exception e) {
						 }
						 if(childId == null) childId = new Integer(0);
					%>
					var localvalue = <%=childId%>;
					if((localvalue != null) && (localvalue != 0)) {
						var parentId = highlightAndGetParent(localvalue);
						if((parentId != null) && (localvalue != 0)) oc(parentId,0);
						parentId = Math.round(parentId);
						parentId--;
						if(parentId > 0) {
							var newId = getParentofNode(parentId);
							if((newId != null) && (newId >= 0)) oc(newId,0);
						}
					}
				</script>
			</div>
		</td></tr>
		</table>
   </td>
  </tr>
</table>
</html:form>
</body>
<script language="JavaScript">
scrollToNode(localvalue);
</script>
</html:html>

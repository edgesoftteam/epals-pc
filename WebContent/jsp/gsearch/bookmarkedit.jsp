<%@page import="org.json.JSONObject"%>
<%@page import="elms.gsearch.GlobalSearch"%> 
<%@page import="org.json.JSONArray"%>
<%@page import="elms.util.*"%>
<%
	
	int bookmarkId = Integer.parseInt(request.getParameter("bookmarkId")!=null?request.getParameter("bookmarkId"):"0");
	int shareId = Integer.parseInt(request.getParameter("shareId")!=null?request.getParameter("shareId"):"0");
	int userId = Integer.parseInt(request.getParameter("userId")!=null?request.getParameter("userId"):"0");
	
	String title = request.getParameter("title")!=null?request.getParameter("title"):"";
	String desc = request.getParameter("desc")!=null?request.getParameter("desc"):"";
	
	JSONArray sl = GlobalSearch.getStaff();
	JSONObject o = GlobalSearch.getBookmark(bookmarkId);
	int result = 0;
	String action=StringUtils.nullReplaceWithEmpty(request.getParameter("action"));
	
	//System.out.println("action "+action);
	
	if(action.equalsIgnoreCase("update")){
		
		//System.out.println(title + " " +desc);
		result = GlobalSearch.bookmarkEdit(bookmarkId,shareId,userId,title,desc);
	}
	
	//get the context root.
	String contextRoot = request.getContextPath();
	

%>
<html>
	<head>
	
		<link href='https://fonts.googleapis.com/css?family=Oswald:300,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Armata' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/datetimepicker/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/toggleswitch/css/tinytools.toggleswitch.css"/>
		<link rel="stylesheet" type="text/css" media="all" href="<%=contextRoot %>/tools/fancyapps/source/jquery.fancybox.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/sweetalert/dist/sweetalert.css">
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/ioscheckboxes/assets/css/mobileCheckbox.iOS.css">
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/alain/cs.ui.css">
		<link href='<%= contextRoot %>/tools/jquery-ui.css' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/chosen/chosen.css"/>
		
	
		<style>
			.csui_controls { visibility: hidden }
		</style>
	
		<script type="text/javascript" src="<%= contextRoot %>/tools/jquery.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.tools.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.form.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.autogrow.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.project.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/datetimepicker/jquery.datetimepicker.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/toggleswitch/tinytools.toggleswitch.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/inputmask/dist/inputmask/inputmask.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/inputmask/dist/inputmask/jquery.inputmask.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/numeric/jquery.numeric.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/ioscheckboxes/assets/js/jquery.mobileCheckbox.js"></script>
		
	 	<script type="text/javascript" src="<%= contextRoot %>/tools/fancyapps/source/jquery.fancybox.pack.js"></script>
	    <script type="text/javascript" src="<%= contextRoot %>/tools/fancyapps/source/cms.fancybox.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/chosen/chosen.jquery.js"></script>
	
		<script>
		
			function updateBookMark()
			{
				var title=document.forms[0].TITLE.value;
				var desc=document.forms[0].DESCRIPTION.value;
				
				document.forms[0].action="bookmarkedit.jsp?action=update&bookmarkId=<%=bookmarkId%>&userId=<%=userId%>&shareId=<%=shareId%>&title="+title+"&desc="+desc;
				
				document.forms[0].submit();
			}
			
			$(document).ready(function() {
				$(".chosen").chosen({width: "95%"});
				
				<% if(result==1){%>
				
				window.parent.$("#csform").submit();
		
				parent.$.fancybox.close();
				
				<% }%>
				$('#recurrence_pattern').val("<%=o.getString("RECURRENCE_PATTERN")%>");
				$('#recurrence_pattern').trigger('chosen:updated');  
			
			});
		</script>
	
	</head>
<body>

	<div id="fullpage">
	<div id="loader">
		<div id="process">
			<table cellpadding="5" cellspacing="0" border="0" id="processtable">
				<tr>
					<td id="processtitle"></td>
				</tr>
				<tr>
					<td id="processmessage"></td>
				</tr>
				<tr>
					<td id="processpercent">
						<table id="processpercentage"><tr><td></td></tr></table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="csuibody">
		<div id="csuimain">
		
			<div class="csuicontent">
				<table cellpadding="10" cellspacing="0" border="0" width="100%">
					<tr>
						<td align="left" id="title"><%=title %></td>
						<td align="right" id="subtitle">BOOKMARK EDIT </td>
					</tr>
				</table>
				
				<div id="csform_message"></div>
				<table class="csui_title">
					<tr>
						<td class="csui_title" nowrap>EMAIL</td>
					</tr>
				</table>
				<form action="bookmarkedit.jsp" method="post" ajax="no" >
						<input type="hidden" id="bookmarkId" name="bookmarkId" value="<%=bookmarkId%>">
						<input type="hidden" id="shareId" name="shareId" value="<%=shareId%>">
						<input type="hidden" id="userId" name="userId" value="<%=userId%>">
						<table class="csui" colnum="2" type="default">
						
						
						
						<tr>
							<td class="csui_label" colnum="2" alert="">TITLE </td>
							<td class="csui"> 
								<input type="text" name="TITLE" value="<%=o.getString("TITLE") %>" >
							</td>

						</tr>
						
						<tr>
							<td class="csui_label" colnum="2" alert="">DESCRIPTION </td>
							<td class="csui"> 
								<textarea name="DESCRIPTION"><%=o.getString("DESCRIPTION") %></textarea>
							</td>

						</tr>
						
						
						
					
					</table>
					
					<div class="csui_divider"></div>
					<div class="csui_buttons">
						<input type="button" name="action" value="Update" class="csui_button" onclick="updateBookMark()">
					</div>

				</form>
				
				
				
				
			</div>
		</div>
	</div>
	</div>


</body>
</html>


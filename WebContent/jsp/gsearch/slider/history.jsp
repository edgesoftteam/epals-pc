<%@page import="elms.util.StringUtils"%>
<%@page import="org.json.JSONArray"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ page import="elms.agent.LookupAgent" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%
String contextRoot = request.getContextPath();
String activityId = request.getParameter("activityId");

JSONArray array =null; //LookupAgent.getActHistory(StringUtils.s2i(activityId));

%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<script language="javascript" type="text/javascript" src="<%=contextRoot%>/tools/jquery.min.js"></script>
<title>Insert title here</title>
</head>
<style>
@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700");

body {
  height: 100vh;
  font-family: "Open Sans", sans-serif;
  /* background: #bea2e7;
  background: -moz-linear-gradient(top, #bea2e7 0%, #86b7e7 100%);
  background: -webkit-linear-gradient(top, #bea2e7 0%, #86b7e7 100%);
  background: linear-gradient(to bottom, #bea2e7 0%, #86b7e7 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bea2e7', endColorstr='#86b7e7',GradientType=0 ); */
}

.container ul {
  margin: 0;
  margin-top: 50px;
  margin-left: 50px;
  list-style: none;
  position: relative;
  padding: 1px 100px;
  color: #000;
  font-size: 13px;
}
.container ul:before {
  content: "";
  width: 1px;
  height: 100%;
  position: absolute;
  border-left: 2px dashed #000;
}
.container ul li {
  position: relative;
  margin-left: 30px;
  background-color: rgba(255, 255, 255, 0.2);
  padding: 14px;
  border-radius: 6px;
  width: 250px;
  box-shadow: 0 0 4px rgba(0, 0, 0, 0.12), 0 2px 2px rgba(0, 0, 0, 0.08);
}
.container ul li:not(:first-child) {
  margin-top: 60px;
}
.container ul li > span {
  width: 2px;
  height: 100%;
  background: #000;
  left: -30px;
  top: 0;
  position: absolute;
}
.container ul li > span:before, .container ul li > span:after {
  content: "";
  width: 8px;
  height: 8px;
  border-radius: 50%;
  border: 2px solid #000;
  position: absolute;
  background: #86b7e7;
  left: -5px;
  top: 0;
}
.container ul li span:after {
  top: 100%;
}
.container ul li > div {
  margin-left: 10px;
}
.container div .title, .container div .type {
  font-weight: bold;
  font-size: 12px;
}
.container div .info {
  font-weight: 400;
  font-size: 11px;
}
.container div > div {
  margin-top: 5px;
}
.container span.number {
  height: 100%;
}
.container span.number span {
  position: absolute;
  font-size: 10px;
  left: -75px;
  font-weight: bold;
}
.container span.number span:first-child {
  top: 0;
}
.container span.number span:last-child {
  top: 50%;
}
.permit {
	margin-top: 10px;
}
.permitnumber {
	margin-left:50px; 
	float:left;
	color:#fff;
	font-weight:bold;
}
.permittype {
	text-align:center; 
	padding:5px;
  	background: #86b7e7;
  	border-radius: 5px;
	color:#fff;
	font-weight:bold;
}
</style>
<script>
$(document).ready(function() {
	var v = JSON.parse('<%=array%>');
	console.log(v);
	var c =  '<div class="" ><div class="permitnumber"> '+ v[0].act_nbr+'</div>';
		c += '<div class="permittype"> '+ v[0].type+'</div></div>';
		c += ' <ul>';
	for (var i = 0; i < v.length; i++) {

		var title = v[i].title != undefined ? v[i].title : "";
		var info = v[i].info != undefined ? v[i].info : "";
		var number = v[i].number != undefined ? v[i].number : "";
		var time = v[i].time != undefined ? v[i].time : "";
		
		c += '<li><span></span>';
		c += '<div>';
		c += '<div class="title">'+ title+'</div>';
		c += '<div class="info">'+ info+'</div>';
		c += '<div class="type"></div>';
		c += '</div> <span class="number"><span>'+number+'</span><span>'+time+'</span></span>';
		c += '</li>';
	}
	c += '</ul>';
	$("#history").html(c);
});
</script>
<body>
	<div class="container" id="history">
	</div>
</body>
</html>
<%@page import="org.json.JSONObject"%>
<%@page import="elms.gsearch.GlobalSearch"%>
<%@page import="org.json.JSONArray"%>
<%@page import="elms.util.*"%>
<%
	int bookmarkId = Integer.parseInt(request.getParameter("bookmarkId")!=null?request.getParameter("bookmarkId"):"0");
	int shareId = Integer.parseInt(request.getParameter("shareId")!=null?request.getParameter("shareId"):"0");
	int userId = Integer.parseInt(request.getParameter("userId")!=null?request.getParameter("userId"):"0");	
	String title = request.getParameter("title")!=null?request.getParameter("title"):"";
	String action = request.getParameter("action") !=null?request.getParameter("action"):"";
	//System.out.println("----------"+action);
	JSONArray sl = GlobalSearch.getStaff();
	JSONObject o = GlobalSearch.getBookmark(bookmarkId);
	JSONObject result = null;
	
	String contextRoot = request.getContextPath();
	
	if(action.equalsIgnoreCase("save")){
		result = GlobalSearch.emailControl(request);
	//	System.out.println("----------"+result);
		/* JSONObject t = GlobalSearch.viewBookmark(bookmarkId);
		System.out.print(t.toString());
		output = JSON.stringify(output);
 		output = JSON.parse(output); */	
	}

%><html>
	<head>
	
		<link href='https://fonts.googleapis.com/css?family=Oswald:300,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Armata' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/datetimepicker/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/toggleswitch/css/tinytools.toggleswitch.css"/>
		<link rel="stylesheet" type="text/css" media="all" href="<%=contextRoot %>/tools/fancyapps/source/jquery.fancybox.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/sweetalert/dist/sweetalert.css">
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/ioscheckboxes/assets/css/mobileCheckbox.iOS.css">
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/alain/cs.ui.css">
		<link href='<%= contextRoot %>/tools/jquery-ui.css' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/chosen/chosen.css"/>
		
	
		<style>
			.csui_controls { visibility: hidden }
		</style>
	
		<script type="text/javascript" src="<%= contextRoot %>/tools/jquery.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.tools.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.form.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.autogrow.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.project.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/datetimepicker/jquery.datetimepicker.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/toggleswitch/tinytools.toggleswitch.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/inputmask/dist/inputmask/inputmask.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/inputmask/dist/inputmask/jquery.inputmask.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/numeric/jquery.numeric.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/ioscheckboxes/assets/js/jquery.mobileCheckbox.js"></script>
		
	 	<script type="text/javascript" src="<%= contextRoot %>/tools/fancyapps/source/jquery.fancybox.pack.js"></script>
	    <script type="text/javascript" src="<%= contextRoot %>/tools/fancyapps/source/cms.fancybox.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/chosen/chosen.jquery.js"></script>
	
		
		
		<%-- <script type="text/javascript" src="<%=contextRoot %>/tools/jquery.min.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/alain/cs.tools.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/alain/cs.form.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/alain/cs.autogrow.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/alain/cs.project.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/datetimepicker/jquery.datetimepicker.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/toggleswitch/tinytools.toggleswitch.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/inputmask/dist/inputmask/inputmask.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/inputmask/dist/inputmask/jquery.inputmask.js"></script>
		
		<script type="text/javascript" src="<%=contextRoot %>/tools/ioscheckboxes/assets/js/jquery.mobileCheckbox.js"></script>
		
	 	<script type="text/javascript" src="<%=contextRoot %>/tools/fancyapps/source/jquery.fancybox.pack.js"></script>
	    <script type="text/javascript" src="<%=contextRoot %>/tools/fancyapps/source/cms.fancybox.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/chosen/chosen.jquery.js"></script>
	 --%>
		<script>

			
			
			$(document).ready(function() {
				$(".chosen").chosen({width: "95%"});
				
				<% if(result != null){%>
					emailexport(<%=result%>);
				<% }else{%>
					$('#recurrence_pattern').val("<%=o.getString("RECURRENCE_PATTERN")%>");
					$('#recurrence_pattern').trigger('chosen:updated');
				<%}%>
			
			});
			
			
			function emailexport(output){
				output = JSON.stringify(output);
		 		output = JSON.parse(output);	

		 		var q = output.q; if(q == undefined) q= "";
		 		var facets = output._facets; if(facets == undefined) facets= "";		 		
		 		var customdt = output._customdt; if(customdt == undefined) customdt= "";		 		
		 		var fq = output._fq; if(fq == undefined) fq= "";		 		
		 		var filters = output._filters; if(filters == undefined) filters= "";		 		
		 		var dt = output._dt; if(dt == undefined) dt= "";		 		
		 		var price = output._price; if(price == undefined) price= "";
		 		var sort = output._sort; if(sort == undefined) sort= "";
		 		var type = output._type; if(type == undefined) type= "";
		 		var fl = output._fl; if(fl == undefined) fl= "";
		 		var facetvalues = output._facetvalues; if(facetvalues == undefined) facetvalues= "";
		 		var title = output._BTITLE; if(title == undefined) title= "";
		 		var to = output.TO; if(to == undefined) to= "";
		 		
		 		var location = output.LOCATION;
		 		if(location == undefined) 
		 			location= "";
		 		else if (location == 'search')
		 			location = "localhost:7337//solr//sairademo_load_initial_clients//query";
		 		<%-- else if (location == 'searchattachments2')
		 			location = "<%=CsConfig.getString("search.attachments")%>"; --%>

		 		$.ajax({
					type : "POST",
					url : "actionsearch.jsp?method=emailcsv",
					dataType : 'json',
					data : {
						q : q,
						start : 0,
						rows : 5000000,
						indent : "on",
						wt : "csv",
						defType : "edismax",
						mm : 100,
						_facet : facets,
						_fq : fq,
						_type : type,
						_filters : filters,
						_dt : dt,
						_price : price,
						_sort : sort,
						_facetvalues : facetvalues,
						_customdt : customdt,
						_fl : fl,
						to : to,
						title : title,
						_userId : 0,
						_location : location,
						_url : location,
						_view : ""
					},
					success : function(output) {
						swal({
							title : 'Success!',
							text : "Email sent successfully!",
							type : 'success',
							confirmButtonColor : '#3085d6',
							confirmButtonText : 'Ok'
						}, function(isConfirm) {
							if (isConfirm) {
								window.parent.$("#csform").submit();
								parent.$.fancybox.close();
							}
						});
					},
					error : function(data) {
						swal('Problem while sending email');
					}
				});
			}
		</script>
	</head>
<body>

	<div id="fullpage">
	<div id="loader">
		<div id="process">
			<table cellpadding="5" cellspacing="0" border="0" id="processtable">
				<tr>
					<td id="processtitle"></td>
				</tr>
				<tr>
					<td id="processmessage"></td>
				</tr>
				<tr>
					<td id="processpercent">
						<table id="processpercentage"><tr><td></td></tr></table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="csuibody">
		<div id="csuimain">
		
			<div class="csuicontent">
				<table cellpadding="10" cellspacing="0" border="0" width="100%">
					<tr>
						<td align="left" id="title"><%=title %></td>
						<td align="right" id="subtitle">BOOKMARK EMAIL MANAGER</td>
					</tr>
				</table>
				
				<div id="csform_message"></div>
				<table class="csui_title">
					<tr>
						<td class="csui_title" nowrap>EMAIL</td>
					</tr>
				</table>
				<form class="form" action="bookmarkemail.jsp" method="post" ajax="no" >
						<input type="hidden" id="bookmarkId" name="bookmarkId" value="<%=bookmarkId%>">
						<input type="hidden" id="shareId" name="shareId" value="<%=shareId%>">
						<input type="hidden" id="userId" name="userId" value="<%=userId%>">
						<table class="csui" colnum="2" type="default">
						
						<tr>
							<td class="csui_label" colnum="2" alert="">TURN OFF EMAIL</td>
							<%if(o.getString("EMAIL_ON").equals("N")){ %>
								<td class="csui" colnum="2" type="boolean" itype="boolean" alert=""><div><input name="EMAIL_ON" type="checkbox" itype="boolean" checked  ></div></td>
							<%} else { %>
								<td class="csui" colnum="2" type="boolean" itype="boolean" alert=""><div><input name="EMAIL_ON" type="checkbox" itype="boolean" <%if(o.getString("EMAIL_ON").equals("Y")){ %>checked <% }%> ></div></td>
							
							<%} %>
							

						</tr>
						
						<tr>
							<td class="csui_label" colnum="2" alert="">EMAIL CURRENT MANAGE </td>
							<td class="csui"> (Comma separate multiple emails)</br>
								<textarea name="emailselected"><%=o.getString("EMAIL_TO") %></textarea>
							</td>

						</tr>
						
						
						<tr>
							<td class="csui_label" colnum="2" alert="">EMAIL STAFF </td>
							<td class="csui"> <select class="chosen" name="emailstaff" multiple="multiple"  style="width:100%">
									 <%for(int i=0;i<sl.length();i++){ 
									  	JSONObject e = sl.getJSONObject(i);
									  %>
									 	<option value="<%= e.getString("USERNAME")%>@beverlyhills.org" ><%= e.getString("FIRST_NAME") %>,<%= e.getString("LAST_NAME") %> - <%= e.getString("USERNAME") %> <%= e.getString("TITLE") %></option>
									  <% }%>
								</select>
							</td>
						</tr>
						<!-- 
						<tr>
							<td class="csui_label" colnum="2" alert="">EMAIL ADDITIONAL </td>
							<td class="csui"> 
								<input type="text" name="emailadditional" value="" placeholder="enter e-mail with comma separated">  
							</td>
						</tr>
						 -->
						
						<tr>
							<td class="csui_label" colnum="2" alert="">EMAIL RECURRENCE PATTERN</td>
								<td class="csui"> <select class="chosen" name="recurrence_pattern"  id="recurrence_pattern" style="width:100%">
									<option value="">Please Select</option>
									<option value="daily">Daily</option>
									<option value="weekly">Weekly</option>
									<option value="monthly">Monthly</option>
								</select>
							</td>
						</tr>
					
					</table>
					
					<div class="csui_divider"></div>
					<div class="csui_buttons">
						<input type="submit" name="action" value="Save" class="csui_button">
					</div>

				</form>
				
				
				
				
			</div>
		</div>
	</div>
	</div>


</body>
</html>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>

<%@page import="java.io.PrintStream"%>
<%@page import="java.nio.charset.Charset"%>



<%@ page import="java.io.ByteArrayOutputStream" %>
<%@page trimDirectiveWhitespaces="true" %> 
<!--sunil  -->
<%
String contextroot = request.getContextPath();

boolean multiple = true;



%>
<html>
	<head>
	
		<link href='https://fonts.googleapis.com/css?family=Oswald:300,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Armata' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<%=contextroot %>/tools/datetimepicker/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextroot %>/tools/chosen/chosen.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextroot %>/tools/toggleswitch/css/tinytools.toggleswitch.css"/>
		<link rel="stylesheet" type="text/css" media="all" href="<%=contextroot %>/tools/fancyapps/source/jquery.fancybox.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextroot %>/tools/sweetalert/dist/sweetalert.css">
		<link rel="stylesheet" type="text/css" href="<%=contextroot %>/tools/ioscheckboxes/assets/css/mobileCheckbox.iOS.css">
		<link rel="stylesheet" type="text/css" href="<%=contextroot %>/tools/alain/cs.ui.css">
	
		<style>
			.csui_controls { visibility: hidden }
		</style>
	
		<script type="text/javascript" src="<%= contextroot %>/tools/jquery.min.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/alain/cs.tools.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/alain/cs.form.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/alain/cs.autogrow.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/datetimepicker/jquery.datetimepicker.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/chosen/chosen.jquery.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/toggleswitch/tinytools.toggleswitch.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/inputmask/dist/inputmask/inputmask.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/inputmask/dist/inputmask/jquery.inputmask.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/numeric/jquery.numeric.min.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/ioscheckboxes/assets/js/jquery.mobileCheckbox.js"></script>
		
	 	<script type="text/javascript" src="<%= contextroot %>/tools/fancyapps/source/jquery.fancybox.pack.js"></script>
	    <script type="text/javascript" src="<%= contextroot %>/tools/fancyapps/source/cms.fancybox.js"></script>
		<script type="text/javascript" src="<%= contextroot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
		<script type="text/javascript">
		
		
		function viewprint(){
			
		}
		
		
		
		function openexport(){
			parent.openexport();
		}
		</script>
	</head>
<body >

<%if(multiple){ %>



	<div class="csui_divider"></div>
						<!-- List TEMPLATES-->
					 <table class="csui_title" alert="warning">
							<tr>
								<td class="csui_title">PRINT</td>
								
							</tr>
						</table>
					
					<table class="csui" type="horizontal">
							
							
							 <thead>
								<tr>
									<td class="csui_header">TEMPLATE</td>
									<td class="csui_header">EMAIL</td>
									<td class="csui_header">PRINT</td>
								</tr>
							</thead>
							 <tbody>
							 	<tr id="tr_">
									<td class="csui" colspan="3" style="font-weight:bold;"><font color="red">Requires license contact administrator in order to use this feature</font></td>
									
									
									
								</tr>
								
								<tr id="tr_">
									<td class="csui">Activity Template Generic</td>
									
									<td class="csui" width="1%">
										<a  href="#" title="E-mail" border="0"  target="lightbox-iframe"  > <img src="<%=contextroot%>/jsp/images/icons/controls/black/email.png" border="0"></a>
									</td>
									<td class="csui" width="1%">
										<a  href="#" title="Print" border="0"  target="_blank" onclick="close();" ><img src="<%=contextroot%>/jsp/images/icons/controls/black/print.png" border="0"></a>
									</td>
									
								</tr>
								
								<tr id="tr_">
									<td class="csui">Activity QR Code </td>
									
									<td class="csui" width="1%">
										<a  href="#" title="E-mail" border="0"  target="lightbox-iframe"  > <img src="<%=contextroot%>/jsp/images/icons/controls/black/email.png" border="0"></a>
									</td>
									<td class="csui" width="1%">
										<a  href="#" title="Print" border="0"  target="_blank" onclick="close();" ><img src="<%=contextroot%>/jsp/images/icons/controls/black/print.png" border="0"></a>
									</td>
									
								</tr>
								
								<tr id="tr_">
									<td class="csui">Finance Reconcialtion  </td>
									
									<td class="csui" width="1%">
										<a  href="#" title="E-mail" border="0"  target="lightbox-iframe"  > <img src="<%=contextroot%>/jsp/images/icons/controls/black/email.png" border="0"></a>
									</td>
									<td class="csui" width="1%">
										<a  href="#" title="Print" border="0"  target="_blank" onclick="close();" ><img src="<%=contextroot%>/jsp/images/icons/controls/black/print.png" border="0"></a>
									</td>
									
								</tr>
								<tr id="tr_">
									<td class="csui">GIS MAP   </td>
									
									<td class="csui" width="1%">
										<a  href="#" title="E-mail" border="0"  target="lightbox-iframe"  > <img src="<%=contextroot%>/jsp/images/icons/controls/black/email.png" border="0"></a>
									</td>
									<td class="csui" width="1%">
										<a  href="#" title="Print" border="0"  target="_blank" onclick="close();" ><img src="<%=contextroot%>/jsp/images/icons/controls/black/print.png" border="0"></a>
									</td>
									
								</tr>
								
								<tr id="tr_">
									<td class="csui">Inspection Routing /Cases  </td>
									
									<td class="csui" width="1%">
										<a  href="#" title="E-mail" border="0"  target="lightbox-iframe"  > <img src="<%=contextroot%>/jsp/images/icons/controls/black/email.png" border="0"></a>
									</td>
									<td class="csui" width="1%">
										<a  href="#" title="Print" border="0"  target="_blank" onclick="close();" ><img src="<%=contextroot%>/jsp/images/icons/controls/black/print.png" border="0"></a>
									</td>
									
								</tr>
								
						
							</tbody>
						</table>
						
						
						
							 	


<%}%>
</body>



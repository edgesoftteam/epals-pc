<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<%@ page import='java.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=request.getContextPath()%>/jsp/css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
%>
<script language="javascript" type="text/javascript">
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     alert("Error initializing XMLHttpRequest!");

   function initialLoad(){
		document.getElementById('loader').innerHTML = '';
	}


function checkTable(){
	
	document.getElementById('loader').innerHTML = "<img src='<%=contextRoot%>/jsp/images/loader.gif' style='float:right;'>";

	var url = "<%=contextRoot%>/jsp/gsearch/scanLocalFile.jsp?method=scan";

	xmlhttp.open('POST', url,true);
	xmlhttp.onreadystatechange = checkTableHandler;
    xmlhttp.send(null);
}

function checkTableHandler(){
    if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
	    var result=xmlhttp.responseText;
	   /*  alert("result : "+result);
	    if(result == 'Y'){
	    	alert("success");
	    } */
	    document.getElementById('loader').innerHTML = '';
	    document.getElementById('tableDiv').innerHTML = result;
		
 	}
}

</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="initialLoad()">
<form >

<div id="saveDiv" align="justify" style="color:#088A08"></div>

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="36px"><font class="con_hdr_3b" >Import Attachments Content</font></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
			 <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                   <tr>
                      <td width="99%" class="tabletitle">Saira attachments full import</td>

                        <td width="1%" class="tablebutton"><nobr>
                          <html:button property ="Load" value="Load" styleClass="button" onclick="checkTable();"/>
                          &nbsp;</nobr></td>
                      </tr>
                  </table>
                </td>
              </tr>
             
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<div id="loader" style="padding-right:50%;"><span><img src="<%=request.getContextPath() %>/jsp/images/loader.gif"></span></div>
<div id="tableDiv"></div>

</form>
</body>
</html:html>

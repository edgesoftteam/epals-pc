<%@page import="org.json.JSONObject"%>
<%@page import="elms.gsearch.GlobalSearch"%>
<%@page import="org.json.JSONArray"%>
<%@page import="elms.util.*"%>
<%
	int bookmarkId = Integer.parseInt(request.getParameter("bookmarkId")!=null?request.getParameter("bookmarkId"):"0");
	int shareId = Integer.parseInt(request.getParameter("shareId")!=null?request.getParameter("shareId"):"0");
	int userId = Integer.parseInt(request.getParameter("userId")!=null?request.getParameter("userId"):"0");
	int share = Integer.parseInt(request.getParameter("share")!=null?request.getParameter("share"):"0");
	
	String title = request.getParameter("title");
	JSONArray sl = GlobalSearch.getStaff();   
	JSONObject o = GlobalSearch.getBookmark(bookmarkId);
	int result = 0;
	String action=StringUtils.nullReplaceWithEmpty(request.getParameter("action"));
	
	if(action.equalsIgnoreCase("share")){
		result = GlobalSearch.shareControl(bookmarkId,shareId,share);
	}
	
	//get the context root.
	String contextRoot = request.getContextPath();
	
%>
<html>
	<head>
	
		<link href='https://fonts.googleapis.com/css?family=Oswald:300,700' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Armata' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/datetimepicker/jquery.datetimepicker.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/toggleswitch/css/tinytools.toggleswitch.css"/>
		<link rel="stylesheet" type="text/css" media="all" href="<%=contextRoot %>/tools/fancyapps/source/jquery.fancybox.css"/>
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/sweetalert/dist/sweetalert.css">
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/ioscheckboxes/assets/css/mobileCheckbox.iOS.css">
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/alain/cs.ui.css">
		<link href='<%= contextRoot %>/tools/jquery-ui.css' rel='stylesheet' type='text/css'>
			<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/chosen/chosen.css"/>
		
	
		<style>
			.csui_controls { visibility: hidden }
		</style>
	
		<script type="text/javascript" src="<%= contextRoot %>/tools/jquery.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.tools.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.form.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.autogrow.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/alain/cs.project.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/datetimepicker/jquery.datetimepicker.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/toggleswitch/tinytools.toggleswitch.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/tinymce/js/tinymce/jquery.tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/inputmask/dist/inputmask/inputmask.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/inputmask/dist/inputmask/jquery.inputmask.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/numeric/jquery.numeric.min.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/ioscheckboxes/assets/js/jquery.mobileCheckbox.js"></script>
		
	 	<script type="text/javascript" src="<%= contextRoot %>/tools/fancyapps/source/jquery.fancybox.pack.js"></script>
	    <script type="text/javascript" src="<%= contextRoot %>/tools/fancyapps/source/cms.fancybox.js"></script>
		<script type="text/javascript" src="<%= contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/chosen/chosen.jquery.js"></script>
	
		<script>
		
			function shareBookMark(){
				var share=document.forms[0].shareVal.value;
				
				share=share.substr(0, share.indexOf('$'));
				document.forms[0].action="bookmarkshare.jsp?action=share&bookmarkId=<%=bookmarkId%>&userId=<%=userId%>&shareId=<%=shareId%>&share="+share;   
				document.forms[0].submit();
			}
			
			function setShareValue(shareVal)   
			{     
			    document.forms[0].shareVal.value=shareVal.value;
			}
			
			$(document).ready(function() {
				$(".chosen").chosen({width: "95%"});
				
				<% if(result==1){%>
				
				window.parent.$("#csform").submit();
		
				parent.$.fancybox.close();
				
				<% }%>
			
			
			});
		</script>
	
	</head>
<body>

	<div id="fullpage">
	<div id="loader">
		<div id="process">
			<table cellpadding="5" cellspacing="0" border="0" id="processtable">
				<tr>
					<td id="processtitle"></td>
				</tr>
				<tr>
					<td id="processmessage"></td>
				</tr>
				<tr>
					<td id="processpercent">
						<table id="processpercentage"><tr><td></td></tr></table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div id="csuibody">
		<div id="csuimain">
		
			<div class="csuicontent">
				<table cellpadding="10" cellspacing="0" border="0" width="100%">
					<tr>
						<td align="left" id="title"><%=title %></td>
						<td align="right" id="subtitle">BOOKMARK MANAGER</td>
					</tr>
				</table>
				
				<div id="csform_message"></div>
				
				<table class="csui_title">
					<tr>
						<td class="csui_title" nowrap>SHARE</td>
					</tr>
				</table>
				<form action="bookmarkshare.jsp" method="post" ajax="no" >
						<input type="hidden" id="bookmarkId" name="bookmarkId" value="<%=bookmarkId%>">
						<input type="hidden" id="shareId" name="shareId" value="<%=shareId%>">
						<input type="hidden" id="userId" name="userId" value="<%=userId%>">
						<input type="hidden" id="shareVal" name="shareVal">
						<table class="csui" colnum="2" type="default">
						<tr>
							<td class="csui_label" colnum="2" alert="">SHARE </td>
							<td class="csui"> <select class="chosen" name="share" multiple="multiple"  style="width:100%" onchange="setShareValue(this)">
									 <%for(int i=0;i<sl.length();i++){ 
									  	JSONObject e = sl.getJSONObject(i);
									  %>
									  		<option value="<%= e.getInt("ID") %>$<%= e.getString("USERNAME") %>" ><%= e.getString("FIRST_NAME") %>,<%= e.getString("LAST_NAME") %> - <%= e.getString("USERNAME") %> <%= e.getString("TITLE") %></option>
									  <% }%>
								</select>
							</td>
						</tr>
						<tr>
							<td class="csui_label" colnum="2" alert="">EMAIL RECURRENCE PATTERN</td>
								<td class="csui"> <select class="chosen" name="recurrence_pattern"   style="width:100%">
									<option value="">Please Select</option>
									<option value="daily">Daily</option>
									<option value="weekly">Weekly</option>
									<option value="monthly">Monthly</option>
								</select>
							</td>
						</tr>
					
					</table>
					
					<div class="csui_divider"></div>
					<div class="csui_buttons" >
						<input type="button" name="action" value="Share" class="csui_button" onclick="javascript:shareBookMark()">
					</div>

				</form>
				
				
				
				
				
				
			</div>
		</div>
	</div>
	</div>


</body>
</html>


<%@page import="elms.gsearch.GlobalSearch"%>
<%@page import="elms.control.actions.ApplicationScope"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" %>
<%@ page import="java.io.*"%>
<%@ page import="java.util.ResourceBundle"%>


<html>
<body>  

<% String ss=request.getParameter("fileName"); 
String fname = GlobalSearch.getKeyValue("TEMP_FILE_LOC")+ss; //(String)  obcProperties.getString("ATTACHMENT_FOLDER")+ss;
/* fname = fname.replace(" ", "+"); */
//System.out.println("fname : "+fname);

response.setHeader("Content-Disposition", "attachement; filename=\""+ss+"\"");
String extension = "";
int i = ss.lastIndexOf('.');
if (i > 0) {
    extension = ss.substring(i+1);
}

if(extension.equalsIgnoreCase("pdf"))
{
  response.setHeader("Content-Type", "application/pdf");
}

if(extension.equalsIgnoreCase("bmp"))
{
  response.setHeader("Content-Type", "image/bmp");
}

if(extension.equalsIgnoreCase("cgm"))
{
  response.setHeader("Content-Type", "image/cgm");
}

if(extension.equalsIgnoreCase("gif"))
{
  response.setHeader("Content-Type", "image/gif");
}

if(extension.equalsIgnoreCase("ief"))
{
  response.setHeader("Content-Type", "image/ief");
}

if(extension.equalsIgnoreCase("jpeg"))
{
  response.setHeader("Content-Type", "image/jpeg");
}

if(extension.equalsIgnoreCase("tiff"))
{
  response.setHeader("Content-Type", "image/tiff");
}

if(extension.equalsIgnoreCase("png"))
{
  response.setHeader("Content-Type", "image/png");
}

if(extension.equalsIgnoreCase("json"))
{
  response.setHeader("Content-Type", "application/json");
}

if(extension.equalsIgnoreCase("zip"))
{
  response.setHeader("Content-Type", "application/zip");
}

if(extension.equalsIgnoreCase("tgz"))
{
  response.setHeader("Content-Type", "application/tgz");
}

if(extension.equalsIgnoreCase("doc") || extension.equalsIgnoreCase("docx"))
{
  response.setHeader("Content-Type", "application/msword");
}

if(extension.equalsIgnoreCase("txt"))
{
  response.setHeader("Content-Type", "text/plain");
}

if(extension.equalsIgnoreCase("xml"))
{
  response.setHeader("Content-Type", "application/xml");
}

if(extension.equalsIgnoreCase("xls") || extension.equalsIgnoreCase("xlsx"))
{
  response.setHeader("Content-Type", "application/vnd.ms-excel");
}

if(extension.equalsIgnoreCase("rtf"))
{
  response.setHeader("Content-Type", "text/rtf");
}

if(extension.equalsIgnoreCase("html"))
{
  response.setHeader("Content-Type", "text/html");
}

if(extension.equalsIgnoreCase("mpeg"))
{
  response.setHeader("Content-Type", "audio/mpeg");
}

if(extension.equalsIgnoreCase("midi"))
{
  response.setHeader("Content-Type", "audio/midi");
}

InputStream isStream = null;
ServletOutputStream sosStream = null;

try
{
response.flushBuffer();
isStream =new FileInputStream(fname);
sosStream =response.getOutputStream();

int ibit = 250;
while ((ibit) >= 0)
{
ibit = isStream.read();
sosStream.write(ibit);
}
sosStream.flush();
sosStream.close();
isStream.close();
out.clear();
out=pageContext.pushBody();
}
catch (IOException e)
{
e.printStackTrace();
}

%> 

</body>
</html>
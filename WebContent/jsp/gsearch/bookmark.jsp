<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="elms.gsearch.GlobalSearch"%>
<%@page import="elms.util.Operator"%>
<%@page import="java.text.DecimalFormat"%>   
<%@page import="java.util.*"%> 
<%@page import="elms.agent.*"%> 
<%@page import="elms.util.*"%> 
<%@page import="elms.gsearch.*"%>

<%
//get the context root.
String contextRoot = request.getContextPath();
//get user from session.
elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
//get full name of the user

	String title = "Bookmark";
	String subtitle = "";
	String alert = "Bookmark";
 
     int bookmarkId =Integer.parseInt((String)request.getParameter("bookmarkId")!=null?(String)request.getParameter("bookmarkId"):"0"); 

     String action=StringUtils.nullReplaceWithEmpty(request.getParameter("action"));
	 
	 if(action.equals("delete")){
		 GlobalSearch.deleteBookmark(bookmarkId);   
	 }
	 
	 int userid = user.getUserId();
			
     List bList= new ArrayList();
     bList= GlobalSearch.getBookmarkList(user.getUserId());  
%>
<html>
<head>

	<title>City Smart</title>
	<link href='https://fonts.googleapis.com/css?family=Oswald:300,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Armata' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' rel='stylesheet' type='text/css'>
	<link href='<%=contextRoot %>/tools/alain/cs.ui.css' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" type="text/css" media="all" href="<%=contextRoot %>/tools/fancyapps/source/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/sweetalert/dist/sweetalert.css">
	
		<link rel="stylesheet" type="text/css" href="<%=contextRoot %>/tools/tablesorter/css/theme.dropbox.css">
	
	
	<script type="text/javascript" src="<%=contextRoot %>/tools/jquery.min.js"></script>
	<script language="JavaScript" src="<%=contextRoot%>/tools/jq/json2.js"></script>
	<script type="text/javascript" src="<%=contextRoot %>/tools/fancyapps/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="<%=contextRoot %>/tools/fancyapps/source/cms.fancybox.js"></script>
	<script type="text/javascript" src="<%= contextRoot %>/tools/sweetalert/dist/sweetalert-dev.js"></script>
		<script type="text/javascript" src="<%=contextRoot %>/tools/tablesorter/jquery.tablesorter.min.js"></script>
	
	<script>
	
	$(document).ready(function() {
		 $(".tablesorter").tablesorter(); 

		
	});
	
	function deletetype(id){
		var method = "deletetype";
		swal({  
				title: "Are you sure?",   
				text: "You want to delete this record!",   
				type: "warning",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Yes, delete it!",   
				cancelButtonText: "No, cancel plx!",   
				closeOnConfirm: false,   
				closeOnCancel: false 
			}, 
			function(isConfirm){   
				if (isConfirm) {     
					
					document.forms[0].action = "bookmark.jsp?action=delete&bookmarkId="+id;
					document.forms[0].submit();
				
				} 
				else {    
					swal("Cancelled", "The record is safe :)", "error");  
				} 
			});
	}

	
	
	
	</script>
</head>

<body>
	<div id="csuicontrols">
		<div id="csuicontrol" class="csuicontrol">
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				&nbsp;
			</table>
		</div>
		
	</div>
	<div id="csuibody">
		<div id="csuimain">
			<div class="csuicontent">
			
			
				<form id="csform" action="bookmark.jsp" method="post">
				<input type="hidden" name="_act" value="reverse" >
				<input type="hidden" name="_grptype" value="finance" >
				<input type="hidden" name="_ent" value="finance" >
				
				
						<div class="csui_divider"></div>
						
					<table class="csui_title" >
							<tr>
								<td class="csui_title">BOOKMARKS</td>
							</tr>
							
						</table>		
					<table class="csui tablesorter" type="horizontal" >
						 <thead>
						
						<tr>
							
							<td class="csui_header">TITLE</td>
							<td class="csui_header">DESCRIPTION</td>
							<td class="csui_header">CREATED BY</td>
							<td class="csui_header">CREATED</td>
							<td class="csui_header">&nbsp;</td>
							<td class="csui_header" width="1%">&nbsp;</td>
							<td class="csui_header" width="1%">&nbsp;</td>
							<td class="csui_header" width="1%">&nbsp;</td>
							<!-- <td class="csui_header" width="1%">&nbsp;</td> -->
						</tr>
						</thead>
						<tbody>
					<%
					Bookmark bookmark= new Bookmark();
					for(int l=0;l<bList.size();l++){ 
						bookmark= (Bookmark)bList.get(l); 
						//JSONObject r = a.getJSONObject(l);
						
					%>
					
					 
					
						<tr>
							
							<td class="csui"><a href="<%=contextRoot %>/jsp/gsearch/search.jsp?bookmarkId=<%=bookmark.getId() %>" target="_self"><%=bookmark.getTitle() %></a></td>
							<td class="csui"><a href="<%=contextRoot %>/jsp/gsearch/search.jsp?bookmarkId=<%=bookmark.getId() %>" target="_self"><%=bookmark.getDescription() %></a></td>
							<td class="csui" style="cursor:pointer;"  rel="<%=bookmark.getId() %>" ><%=StringUtils.nullReplaceWithEmpty(bookmark.getCreated()) %></td>
							<td class="csui" style="cursor:pointer;"  rel="<%=bookmark.getId() %>" ><%=bookmark.getCreatedDate() %></td>
							<td class="csui" width="1%">
								<a href="<%=contextRoot %>/jsp/gsearch/search.jsp?bookmarkId=<%=bookmark.getId() %>" target="_self" title="view- <%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle()) %>" ><img src="<%=contextRoot%>/jsp/images/icons/controls/black/eye.png" border="0" style="cursor:pointer" ></a>
							</td>   
							<td class="csui" width="1%">
								<a href="bookmarkedit.jsp?userId=<%=userid %>&bookmarkId=<%=bookmark.getId() %>&title=<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle())%>&shareId=<%=bookmark.getShareId() %>" target="lightbox-iframe" title="edit-<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle())%>" ><img src="<%=contextRoot%>/jsp/images/icons/controls/black/edit.png" border="0"></a>
								</td>
							<td class="csui" width="1%"> 
								<a href="bookmarkshare.jsp?userId=<%=userid %>&bookmarkId=<%=bookmark.getId() %>&title=<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle() )%>&shareId=<%=bookmark.getShareId() %>" target="lightbox-iframe" title="share-<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle() )%>" ><img src="<%=contextRoot%>/jsp/images/icons/controls/black/share1.png" border="0" ></a>
							</td>
							
							<%-- <td class="csui" width="1%">
								<a href="bookmarkemail.jsp?userId=<%=userid %>&bookmarkId=<%=bookmark.getId() %>&title=<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle() )%>&shareId=<%=bookmark.getShareId() %>" target="lightbox-iframe" title="email-<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle())%>" >
									<%if(StringUtils.nullReplaceWithBlank( bookmark.getEmailOn()).equals("Y")){ %>
										<img src="<%=contextRoot%>/jsp/images/icons/controls/color/email-green.png" border="0" style="cursor:pointer"   >
									<%} else {%>
										<img src="<%=contextRoot%>/jsp/images/icons/controls/black/email.png" border="0" style="cursor:pointer"   >
									<%} %>
								</a>
							</td> --%>
							
							
							
							<td class="csui" width="1%">
								<img src="<%=contextRoot%>/jsp/images/icons/controls/black/delete.png" border="0" style="cursor:pointer" title="Delete-<%=StringUtils.nullReplaceWithEmpty(bookmark.getTitle()) %>" onclick="deletetype(<%=bookmark.getId()%>);" >
							</td>
						</tr>
						
						
					<%} %>
					</tbody>
					</table>	
					
			
			</div>
		</div>
		
	</div>

	</form>


</body>
</html>


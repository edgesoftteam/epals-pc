<%@page import="elms.gsearch.GlobalSearch"%>
<%@page import="elms.util.Operator"%>
<%@page import="org.apache.commons.codec.binary.Base64"%>
<%@page import="java.net.URLEncoder"%>
<%@include file="facet/gsfacet.jsp"%>

<%

	//Cartographer map = new Cartographer(request,response);
	//map.setString("_facet",URLEncoder.encode(facets, "UTF-8"));
	//System.out.println(map.getString("method")+"###############");
	if(Operator.equalsIgnoreCase((String)request.getParameter("method"), "csv")){
		
		String outputFile = "export_search.csv";
		
		
		  //String encoding =  CsConfig.getString("search.credentials.login_username")+":"+ CsConfig.getString("search.credentials.login_pass"); 
		 // byte[] encodedBytes = Base64.encodeBase64(encoding.getBytes());
		  //response.setHeader("Authorization", "Basic " + new String(encodedBytes));
		
		response.setHeader("Content-type","text/csv");
		response.setHeader("Content-disposition","attachment; filename="+outputFile);
		String s = GlobalSearch.search(request);
		//System.out.println(s);
		java.io.PrintWriter op = response.getWriter();
		op.write(s);
		op.close();
	}
	else if(Operator.equalsIgnoreCase(request.getParameter("method"), "spell")){
		String resp = GlobalSearch.spell(request);  
		out.print(resp);
	}
	else if(Operator.equalsIgnoreCase(request.getParameter("method"), "stats")){
		String resp = GlobalSearch.search(request,false,true);
		out.print(resp);
	}
	else if(Operator.equalsIgnoreCase(request.getParameter("method"), "viewbookmark")){
		int bookmarkId = Integer.parseInt(request.getParameter("bookmarkId"));
		//System.out.println("bookmark id "+bookmarkId);
		String resp = GlobalSearch.viewBookmark(bookmarkId).toString();
		out.print(resp);
	}
	
	
	else {
		
		String resp = GlobalSearch.search(request);
		//System.out.println(resp);
		out.print(resp);
	}
%>

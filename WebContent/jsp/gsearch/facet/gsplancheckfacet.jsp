<%@page import="org.json.JSONArray"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.json.JSONObject"%>
<%

JSONArray o = new JSONArray();
JSONObject domain = new JSONObject();

JSONObject department = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "department");
department.put("domain", domain);
department.put("type", "terms");
department.put("field", "department");
department.put("limit", -1);
o.put(department);

JSONObject plancheckdepartment = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "plancheckdepartment");
plancheckdepartment.put("type", "terms");
plancheckdepartment.put("field", "plancheckdepartment");
plancheckdepartment.put("limit", -1);
plancheckdepartment.put("domain", domain);
plancheckdepartment.put("sort", "index");
plancheckdepartment.put("text", "pc assigned department");
o.put(plancheckdepartment);


JSONObject review = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "review");
review.put("domain", domain);
review.put("type", "terms");
review.put("field", "review");
review.put("limit", -1);
review.put("text", "process type");
o.put(review);

JSONObject review_status = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "review_status");
review_status.put("domain", domain);
review_status.put("type", "terms");
review_status.put("field", "review_status");
review_status.put("limit", -1);
review_status.put("text", "plan check status");
o.put(review_status);

JSONObject assigned = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "assigned");
assigned.put("domain", domain);
assigned.put("type", "terms");
assigned.put("field", "assigned");
assigned.put("limit", -1);
assigned.put("text", "staff");
o.put(assigned);

JSONObject activity_type = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "activity_type");
activity_type.put("domain", domain);
activity_type.put("type", "terms");
activity_type.put("field", "activity_type");
activity_type.put("limit", -1);
activity_type.put("text", "activity type");
o.put(activity_type);

JSONObject activity_status = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "activity_status");
activity_status.put("type", "terms");
activity_status.put("field", "activity_status");
activity_status.put("limit", -1);
activity_status.put("domain", domain);
activity_status.put("text", "activity status");
o.put(activity_status);


String facets = StringEscapeUtils.escapeJava(o.toString());

%>

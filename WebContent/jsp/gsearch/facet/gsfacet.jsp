<%@page import="org.json.JSONArray"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.json.JSONObject"%>

<%

JSONArray o = new JSONArray();
JSONObject domain = new JSONObject();


JSONObject department = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "department");
department.put("domain", domain);
department.put("type", "terms");
department.put("field", "department");
department.put("limit", -1);
department.put("sort", "index");
o.put(department);

JSONObject module = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "module");
module.put("domain", domain);
module.put("type", "terms");
module.put("field", "module");
module.put("limit", -1);
module.put("sort", "index");
o.put(module);

JSONObject renewal = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "renewal");
renewal.put("domain", domain);
renewal.put("type", "terms");
renewal.put("field", "renewal");
renewal.put("limit", -1);
o.put(renewal);

JSONObject online = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "online");
online.put("domain", domain);
online.put("type", "terms");
online.put("field", "online");
online.put("limit", -1);
o.put(online);

JSONObject type = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "type");
type.put("domain", domain);
type.put("type", "terms");
type.put("field", "type");
type.put("limit", -1);
type.put("sort", "index");
o.put(type);

JSONObject status = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "status");
status.put("type", "terms");
status.put("field", "status");
status.put("limit", -1);
status.put("domain", domain);
status.put("sort", "index");
o.put(status);

JSONObject lsotype = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "lso_type");
lsotype.put("domain", domain);
lsotype.put("type", "terms");
lsotype.put("field", "lso_type");
lsotype.put("limit", -1);
lsotype.put("text", "lso type");
o.put(lsotype);

JSONObject zone = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "zone");
zone.put("domain", domain);
zone.put("type", "terms");
zone.put("field", "zone");
zone.put("limit", -1);
zone.put("sort", "index");
o.put(zone);

JSONObject use = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "use");
use.put("domain", domain);
use.put("type", "terms");
use.put("field", "use");
use.put("limit", -1);
use.put("sort", "index");
o.put(use);


String facets = StringEscapeUtils.escapeJava(o.toString());

%>

<%@page import="org.json.JSONArray"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.json.JSONObject"%>
<%

JSONArray o = new JSONArray();
JSONObject domain = new JSONObject();

JSONObject department = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "department");
department.put("type", "terms");
department.put("field", "department");
department.put("limit", -1);
department.put("domain", domain);
department.put("sort", "index");
o.put(department);

JSONObject module = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "module");
module.put("type", "terms");
module.put("field", "module");
module.put("limit", -1);
module.put("domain", domain);
module.put("sort", "index");
o.put(module);

JSONObject facet3 = new JSONObject();
domain = new JSONObject();
facet3.put("excludeTags", "renewal_online");
facet3.put("domain", domain);
facet3.put("type", "terms");
facet3.put("field", "renewal_online");
facet3.put("limit", -1);
facet3.put("sort", "index");
facet3.put("text", "renewal");
o.put(facet3);

JSONObject facet2 = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "paid_online");
facet2.put("domain", domain);
facet2.put("type", "terms");
facet2.put("field", "paid_online");
facet2.put("limit", -1);
facet2.put("sort", "index");
facet2.put("text", "paid online");
o.put(facet2);

JSONObject facet5 = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "cashier");
facet5.put("domain", domain);
facet5.put("type", "terms");
facet5.put("field", "cashier");
facet5.put("limit", -1);
//type.put("missing", true);
facet5.put("sort", "index");
o.put(facet5);

JSONObject facet1 = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "method");
facet1.put("type", "terms");
facet1.put("field", "method");
facet1.put("limit", -1);
facet1.put("domain", domain);
facet1.put("sort", "index");
o.put(facet1);


JSONObject facet4 = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "transactiontype");
facet4.put("domain", domain);
facet4.put("type", "terms");
facet4.put("field", "transactiontype");
facet4.put("limit", -1);
facet4.put("text", "transaction type");
facet4.put("sort", "index");

o.put(facet4);


String facets = StringEscapeUtils.escapeJava(o.toString());

%>

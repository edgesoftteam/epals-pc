<%@page import="org.json.JSONArray"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.json.JSONObject"%>
<%

JSONArray o = new JSONArray();
JSONObject domain = new JSONObject();

JSONObject department = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "department");
department.put("type", "terms");
department.put("field", "department");
department.put("limit", -1);
department.put("domain", domain);
department.put("sort", "index");
o.put(department);

JSONObject module = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "module");
module.put("type", "terms");
module.put("field", "module");
module.put("limit", -1);
module.put("domain", domain);
module.put("sort", "index");
o.put(module);

JSONObject renewal_online = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "renewal_online");
renewal_online.put("domain", domain);
renewal_online.put("type", "terms");
renewal_online.put("field", "renewal_online");
renewal_online.put("limit", -1);
renewal_online.put("sort", "index");
renewal_online.put("text", "renewal");
o.put(renewal_online);

JSONObject paid_online = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "paid_online");
paid_online.put("domain", domain);
paid_online.put("type", "terms");
paid_online.put("field", "paid_online");
paid_online.put("limit", -1);
paid_online.put("sort", "index");
paid_online.put("text", "paid online");
o.put(paid_online);

JSONObject fee_name = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "fee_name");
fee_name.put("domain", domain);
fee_name.put("type", "terms");
fee_name.put("field", "fee_name");
fee_name.put("limit", -1);
fee_name.put("text", "fee name");
o.put(fee_name); 

JSONObject cashier = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "cashier");
cashier.put("domain", domain);
cashier.put("type", "terms");
cashier.put("field", "cashier");
cashier.put("limit", -1);
cashier.put("sort", "index");
o.put(cashier);

JSONObject method = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "method");
method.put("type", "terms");
method.put("field", "method");
method.put("limit", -1);
method.put("domain", domain);
method.put("sort", "index");
o.put(method);



JSONObject transactiontype = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "transactiontype");
transactiontype.put("domain", domain);
transactiontype.put("type", "terms");
transactiontype.put("field", "transactiontype");
transactiontype.put("limit", -1);
transactiontype.put("text", "transaction type");
transactiontype.put("sort", "index");

o.put(transactiontype);




JSONObject account_number = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "account_number");
account_number.put("domain", domain);
account_number.put("type", "terms");
account_number.put("field", "account_number");
account_number.put("limit", -1);
account_number.put("text", "account number");

o.put(account_number);



String facets = StringEscapeUtils.escapeJava(o.toString());

%>

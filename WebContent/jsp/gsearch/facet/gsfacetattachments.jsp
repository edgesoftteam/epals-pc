<%@page import="org.json.JSONArray"%>
<%@page import="org.apache.commons.lang.StringEscapeUtils"%>
<%@page import="org.json.JSONObject"%>
<%

JSONArray o = new JSONArray();
JSONObject domain = new JSONObject();


JSONObject attachmenttype = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "attachmenttype");
attachmenttype.put("domain", domain);
attachmenttype.put("type", "terms");
attachmenttype.put("field", "attachmenttype");
attachmenttype.put("limit", -1);
attachmenttype.put("sort", "index");
attachmenttype.put("text", "attachment type");

o.put(attachmenttype);

JSONObject level = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "level");
level.put("type", "terms");
level.put("field", "level");
level.put("limit", -1);
level.put("domain", domain);
level.put("sort", "index");
o.put(level);

JSONObject extension = new JSONObject();
domain = new JSONObject();
domain.put("excludeTags", "extension");
extension.put("type", "terms");
extension.put("field", "extension");
extension.put("limit", -1);
extension.put("domain", domain);
extension.put("sort", "index");
o.put(extension);

String facets = StringEscapeUtils.escapeJava(o.toString());


%>

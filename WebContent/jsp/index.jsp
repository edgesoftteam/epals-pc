<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ page import="elms.util.db.*"%>
<html:html>
<head> 
<%
String contextRoot = request.getContextPath();
%>
<title><%=Wrapper.getCityName()%> : <%=Wrapper.getPrivateLabel()%></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/elms.css" type="text/css">
<script>
if(window != top) {
top.document.location = "<%=request.getContextPath()%>";
}
</script>
</head>
<body class="tabletext" text="#000000" >
<center>
<b><font color='green'>
<%
    String message = (String)request.getAttribute("message")!=null ? (String)request.getAttribute("message") : "";
	out.print(message);
%></b>

</center>
<html:errors/>
<html:form focus="username" action="/logon">
<table border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center"><br>
      <br>
      <br>
      <br>
      <br>
      <br>
      <br>
    </td>
  </tr>
  <tr>
    <td align="center">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td background="<%=contextRoot%>/jsp/images/site_bg_B7C1CB.jpg">
            <table width="100%" border="0" cellspacing="1" cellpadding="0">
              <tr valign="top">
                <td class="tabletext"><img src="<%=contextRoot%>/jsp/images/site_login_img.jpg" width="296"></td>
              </tr>
              <tr valign="top">
                <td background="<%=contextRoot%>/jsp/images/site_bg_e5e5e5.jpg">
                  <table width="210" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td><img src="<%=contextRoot%>/jsp/images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">Username :</td>
                      <td><img src="<%=contextRoot%>/jsp/images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">Password :</td>
                    </tr>
                    <tr>
                      <td>
                        <html:text  property="username" size="15" styleClass="textbox"/>
                        <br>
                        <img src="<%=contextRoot%>/jsp/images/spacer.gif" width="1" height="3"><br>
                      </td>
                      <td>
                        <html:password  property="password" size="15" styleClass="textbox" redisplay="false"/>
                        <br>
                        <img src="<%=contextRoot%>/jsp/images/spacer.gif" width="1" height="3"><br>
                      </td>
                    </tr>
                    <tr align="center">
                      <td colspan="2">
                        <html:submit  property="Submit" value="Login" styleClass="button" />
                      </td>
                    </tr>
                    <tr align="center">
                      <td colspan="2"><img src="<%=contextRoot%>/jsp/images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr valign="top" align="right">
                <td class="tablelabel">version <%=elms.common.Version.getNumber()%>&nbsp;&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

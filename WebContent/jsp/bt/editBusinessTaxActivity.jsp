<%@ page import='elms.agent.*,java.util.*,elms.common.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<%
String contextRoot = request.getContextPath();
String actId=(String)request.getAttribute("activityId");
String lsId=(String)request.getAttribute("lsoId");
%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/editActivity.js"></script>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript">
// For Checking the Activity status as Issued
function checkActivityStatus(){

   if(document.forms[0].activityStatus.value == '1053'){

    document.getElementById("actStatus").style.display="none";
    document.getElementById("issueDisplay").style.display="block";

	}else{
    document.getElementById("actStatus").style.display="block";
    document.getElementById("issueDisplay").style.display="none";
}
return true;
}//write this in activity Status - onchange="return checkActivityStatus()"

function issueDateHide()
{
document.getElementById("issueDisplay").style.display="none";

  if(document.forms[0].activityStatus.value == '1053'){

    document.getElementById("actStatus").style.display="none";
    document.getElementById("issueDisplay").style.display="block";
	}else{
    document.getElementById("actStatus").style.display="block";
    document.getElementById("issueDisplay").style.display="none";
}
return true;
}//write this in form - onload="issueDateHide();"
</script>
</head>

<%!
List activityStatuses = new ArrayList();
List qtytList = new ArrayList();
List ownershipTypes= new ArrayList();
%>
<%
//String outstandingFees=(String)request.getAttribute("outstandingFees");
try{

 activityStatuses = LookupAgent.getBLActivityStatuses(Constants.MODULE_NAME_BUSINESS_TAX);
pageContext.setAttribute("activityStatuses", activityStatuses);

 qtytList = LookupAgent.getBtQuantityTypes();
pageContext.setAttribute("qtytList", qtytList);

 ownershipTypes = LookupAgent.getOwnershipTypes();
pageContext.setAttribute("ownershipTypes", ownershipTypes);

}catch(Exception e){//ignored
}
%>

<%
String businessAddressStreetNumber1 = "";
String businessAddressStreetFraction1 = "";
String businessAddressStreetName1 = "";
String businessAddressUnitNumber1 = "";
String businessAddressCity1 = "";
String businessAddressState1 = "";
String businessAddressZip1 = "";
String businessAddressZip41 = "";


String businessLocation1=request.getParameter("businessLocation1");
businessAddressStreetNumber1=(String)request.getParameter("businessAddressStreetNumber1");
businessAddressStreetFraction1=request.getParameter("businessAddressStreetFraction1");

businessAddressStreetName1 = request.getParameter("businessAddressStreetName1");
businessAddressUnitNumber1 = request.getParameter("businessAddressUnitNumber1");
businessAddressCity1 = request.getParameter("businessAddressCity1");
businessAddressState1 = request.getParameter("businessAddressState1");
businessAddressZip1 = request.getParameter("businessAddressZip1");
businessAddressZip41 = request.getParameter("businessAddressZip41");
 %>

<script language="JavaScript" type="text/javascript">

function save()
{
var i=0;
var homeOccupation;
var decalCode;
var otherBusinessOccupancy;

if(document.forms[0].homeOccupation.checked== true)
	{
	homeOccupation="Y";
	}else{
	homeOccupation="N";
	}
	
if(document.forms[0].decalCode.checked== true)
{
	decalCode="Y";
}else{
	decalCode="N";
}

if(document.forms[0].otherBusinessOccupancy.checked== true)
{
	otherBusinessOccupancy="Y";
}else{
	otherBusinessOccupancy="N";
}
<% if(businessLocation1 == "false"|| businessLocation1.equals("false")){ %>
if(document.forms[0].activityStatus.value == ""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	i++;
	}else if(document.forms[0].outOfTownStreetName.value==""){
	alert("Please enter value for Out Of Town Street Name");
	document.forms[0].outOfTownStreetName.focus();
	i++;
	}else if(document.forms[0].outOfTownCity.value==""){
	alert("Please enter value for City ");
	document.forms[0].outOfTownCity.focus();
	i++;
	}else if(document.forms[0].outOfTownState.value==""){
	alert("Please enter value for State");
	document.forms[0].outOfTownState.focus();
	i++;
	}else if(document.forms[0].outOfTownZip.value==""){
	alert("Please enter value for Out Of Town Zip");
	document.forms[0].outOfTownZip.focus();
	i++;
	}else if(document.forms[0].businessName.value == ""){
	alert("Please enter Business Name");
	document.forms[0].businessName.focus();
	i++;
	}else if(document.forms[0].ownershipType.value == ""){
	alert("Please select Ownership Type");
	document.forms[0].ownershipType.focus();
	i++;
	}else if(document.forms[0].applicationDate.value == ""){
	alert("Please enter application Date");
	document.forms[0].applicationDate.focus();
	i++;
	}
	else if(isNaN(document.forms[0].elements['multiAddress[0].streetNumber'].value)){
		alert("Please enter numeric for Mailing Street Number ");
		document.forms[0].elements['multiAddress[0].streetNumber'].focus();
		i++;
		}else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value==""){
		alert("Please enter mailing Street Name ");
		document.forms[0].elements['multiAddress[0].streetName1'].focus();
		i++;
		}

		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].city'].value == ""){
		alert("Please enter  mailing city ");
		document.forms[0].elements['multiAddress[0].city'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value != "" && document.forms[0].elements['multiAddress[0].state'].value == ""){
		alert("Please enter  mailing state ");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].zip'].value == ""){
		alert("Please enter  mailing zipcode ");
		document.forms[0].elements['multiAddress[0].zip'].focus();
		i++;
		}
		else if((document.forms[0].elements['multiAddress[0].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[0].state'].value))){
		alert("Please enter non numeric for Mailing State");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
	    }else if(isNaN(document.forms[0].elements['multiAddress[1].streetNumber'].value)){
		alert("Please enter numeric for Previous Street Number ");
		document.forms[0].elements['multiAddress[1].streetNumber'].focus();
		i++;
		}else if((document.forms[0].elements['multiAddress[1].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[1].state'].value))){
		alert("Please enter non numeric for Previous State");
		document.forms[0].elements['multiAddress[1].state'].focus();
		i++;
	    }else if((document.forms[0].elements['multiAddress[1].zip'].value != "") && (isNaN(document.forms[0].elements['multiAddress[1].zip'].value))){
		alert("Please enter numeric for Previous Zipcode");
		document.forms[0].elements['multiAddress[1].zip'].focus();
		i++;
	    } 
else if (i>0 ){
	document.forms[0].action="";
	}else{
		
	var updateAct = "update";
	var actId=document.forms[0].activityId.value;
	var lsId=document.forms[0].lsoId.value;
	
	document.forms[0].action='<%=contextRoot%>/editBusinessTaxActivity.do?otherBusinessOccupancy='+otherBusinessOccupancy+'&homeOccupation='+homeOccupation+'&decalCode='+decalCode+'&updateAct='+updateAct+'&actId='+actId+'&lsId='+lsId+'&businessLocation1=<%=businessLocation1%>&businessAddressStreetNumber1=<%=businessAddressStreetNumber1%>&businessAddressStreetFraction1=<%=businessAddressStreetFraction1%>&businessAddressStreetName1=<%=businessAddressStreetName1%>&businessAddressUnitNumber1=<%=businessAddressUnitNumber1%>&businessAddressCity1=<%=businessAddressCity1%>&businessAddressState1=<%=businessAddressState1%>&businessAddressState1=<%=businessAddressState1%>&businessAddressZip1=<%=businessAddressZip1%>&businessAddressZip41=<%=businessAddressZip41%>';
	document.forms[0].submit();
	return true;
	}
<%}else{%>
	if(document.forms[0].activityStatus.value == ""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	i++;
	}else if(document.forms[0].businessName.value == ""){
	alert("Please enter Business Name");
	document.forms[0].businessName.focus();
	i++;
	}else if(document.forms[0].ownershipType.value == ""){
	alert("Please select Ownership Type");
	document.forms[0].ownershipType.focus();
	i++;
	}else if(document.forms[0].applicationDate.value == ""){
	alert("Please enter application Date");
	document.forms[0].applicationDate.focus();
	i++;
	}
	else if(isNaN(document.forms[0].elements['multiAddress[0].streetNumber'].value)){
		alert("Please enter numeric for Mailing Street Number ");
		document.forms[0].elements['multiAddress[0].streetNumber'].focus();
		i++;
		}else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value==""){
		alert("Please enter mailing Street Name ");
		document.forms[0].elements['multiAddress[0].streetName1'].focus();
		i++;
		}

		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].city'].value == ""){
		alert("Please enter  mailing city ");
		document.forms[0].elements['multiAddress[0].city'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value != "" && document.forms[0].elements['multiAddress[0].state'].value == ""){
		alert("Please enter  mailing state ");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].zip'].value == ""){
		alert("Please enter  mailing zipcode ");
		document.forms[0].elements['multiAddress[0].zip'].focus();
		i++;
		}
		else if((document.forms[0].elements['multiAddress[0].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[0].state'].value))){
		alert("Please enter non numeric for Mailing State");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
	    }else if(isNaN(document.forms[0].elements['multiAddress[1].streetNumber'].value)){
		alert("Please enter numeric for Previous Street Number ");
		document.forms[0].elements['multiAddress[1].streetNumber'].focus();
		i++;
		}else if((document.forms[0].elements['multiAddress[1].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[1].state'].value))){
		alert("Please enter non numeric for Previous State");
		document.forms[0].elements['multiAddress[1].state'].focus();
		i++;
	    }else if((document.forms[0].elements['multiAddress[1].zip'].value != "") && (isNaN(document.forms[0].elements['multiAddress[1].zip'].value))){
		alert("Please enter numeric for Previous Zipcode");
		document.forms[0].elements['multiAddress[1].zip'].focus();
		i++;
	    } 
	/* else if(isNaN(document.forms[0].mailStreetNumber.value)){
	alert("Please enter numeric for Mailing Street Number ");
	document.forms[0].mailStreetNumber.focus();
	i++;
	}else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value==""){
	alert("Please enter mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}
	else if(document.forms[0].mailStreetName.value=="" && document.forms[0].mailCity.value!="" && document.forms[0].mailState.value!="" && document.forms[0].mailZip.value!=""){
	alert("Please enter  mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value!= "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailCity.value == ""){
	alert("Please enter  mailing city ");
	document.forms[0].mailCity.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value != "" && document.forms[0].mailState.value == ""){
	alert("Please enter  mailing state ");
	document.forms[0].mailState.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailZip.value == ""){
	alert("Please enter  mailing zipcode ");
	document.forms[0].mailZip.focus();
	i++;
	}
	else if((document.forms[0].mailState.value != "") && (!isNaN(document.forms[0].mailState.value))){
	alert("Please enter non numeric for Mailing State");
	document.forms[0].mailState.focus();
	i++;
    }else if((document.forms[0].prevState.value != "") && (!isNaN(document.forms[0].prevState.value))){
	alert("Please enter non numeric for Previous State");
	document.forms[0].prevState.focus();
	i++;
	}else if((document.forms[0].prevZip.value != "") && (isNaN(document.forms[0].prevZip.value))){
	alert("Please enter numeric for Previous Zipcode");
	document.forms[0].prevZip.focus();
	i++;
	} */else if (i>0 ){
	document.forms[0].action="";
	}else{
		/* console.log(document.forms[0].elements[businessName].value)
		
		
		var name = "multiAddress[0].value";
		alert(document.getElementById(name));
		alert(document.forms[0].elements[multiAddress[0].streetNumber].value); */
	var updateAct = "update";
	var actId=document.forms[0].activityId.value;
	var lsId=document.forms[0].lsoId.value;
	document.forms[0].action='<%=contextRoot%>/editBusinessTaxActivity.do?otherBusinessOccupancy='+otherBusinessOccupancy+'&homeOccupation='+homeOccupation+'&decalCode='+decalCode+'&updateAct='+updateAct+'&actId='+actId+'&lsId='+lsId+'&businessLocation1=<%=businessLocation1%>&businessAddressStreetNumber1=<%=businessAddressStreetNumber1%>&businessAddressStreetFraction1=<%=businessAddressStreetFraction1%>&businessAddressStreetName1=<%=businessAddressStreetName1%>&businessAddressUnitNumber1=<%=businessAddressUnitNumber1%>&businessAddressCity1=<%=businessAddressCity1%>&businessAddressState1=<%=businessAddressState1%>&businessAddressState1=<%=businessAddressState1%>&businessAddressZip1=<%=businessAddressZip1%>&businessAddressZip41=<%=businessAddressZip41%>';
	document.forms[0].submit();
	return true;
	}   
<%}%>
}

function NumericEntry()
{
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
	return false;

	}
}

function NonNumericEntry()
{

	if (!( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
	event.returnValue = false;

	}

}

function alphaNumericEntry(){
	if (!( event.keyCode>64 && event.keyCode<91 ) && !(event.keyCode >96 &&  event.keyCode<123) && !(event.keyCode >47 && event.keyCode<58 )){
	return false;
	}
}

function DisplayPhoneHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}else{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 ))
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
}
function DisplaySSNHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}else{
		if (document.forms[0].elements[str].value.length == 3)
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';

 		}
		if ((document.forms[0].elements[str].value.length == 6))  document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		if (document.forms[0].elements[str].value.length > 10 )  event.returnValue = false;
	}
}

function validateEmailId(str){
	if(document.forms[0].emailAddress.value!=""){
			var validemail= validateEmail(document.forms[0].emailAddress.value);
			if(validemail==false){
				alert('Please enter correct E-mail Id');
				document.forms[0].emailAddress.value="";
				document.forms[0].emailAddress.focus();
				return false;
			}
		}
}


function ValidBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('It should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}
function goBack() {
  parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=actId%>';
}

</script>

<html:errors/>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0">
<html:form action="/editBusinessTaxActivity.do">
<html:hidden property="activityId" value="<%=actId%>"/>
<html:hidden property="lsoId" value="<%=lsId%>"/>
<bean:define id="businessTaxActivityForm" name="businessTaxActivityForm" type="elms.control.beans.BusinessTaxActivityForm"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><font class="con_hdr_3b">Edit Activity</font><br><br></td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="40%" class="tabletitle" align="left">Edit Activity</td>
							<td width="50%" class="tablebutton" align="right">
								<html:button  property="Cancel"  value="Cancel" styleClass="button" onclick="goBack()"/>
								<html:button property="Save" value="Save" styleClass="button" onclick="javascript:save()"/>&nbsp;
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td background="../images/site_bg_B7C1CB.jpg">
					<table width="100%" cellpadding="2" cellspacing="1">
						<tr>
							<td class="tablelabel" width="20%">Activity #</td>
							<td class="tabletext"> <bean:write name="businessTaxActivityForm"  property="activityNumber"/></td>
							<td class="tablelabel" width="20%">Business Account Number</td>
							<td class="tabletext"><bean:write name="businessTaxActivityForm"  property="businessAccountNumber"/></td>
						</tr>
						<%if(businessLocation1 == "false"|| businessLocation1.equals("false")){%>
						<tr>
							<td class="tablelabel" width="25%">Out of Town Address</td>
							<td class="tabletext">
								<html:text size="5" property="outOfTownStreetNumber" styleClass="textbox"/>
								<html:text size="14" property="outOfTownStreetName" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
							<td class="tabletext" valign="top">
								<html:text size="5" property="outOfTownUnitNumber" styleClass="textbox"/>
								<html:text size="10" property="outOfTownCity" styleClass="textbox"/>
								<html:text size="2" maxlength="2" property="outOfTownState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<html:text size="5" maxlength="5" property="outOfTownZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<html:text size="4" property="outOfTownZip4" styleClass="textbox"/>
							</td>
						</tr>
						<%}else{%>
						<tr>
                            <td class="tablelabel">Business Address</td>
                            <td class="tabletext" colspan="6"><%=businessAddressStreetNumber1%> <%=businessAddressStreetFraction1%> <%=businessAddressStreetName1%> <%=businessAddressUnitNumber1%> <%=businessAddressCity1%> <%=businessAddressState1%> <%=businessAddressZip1%> <%=businessAddressZip41%>

							</td>
                        </tr>
               			<%}%>
               			<tr>
							<td class="tablelabel" width="20%">Business Name</td>
							<td class="tabletext" colspan="3">
								<html:text size="135" maxlength="100" property="businessName" styleClass="textbox" />&nbsp;<BR>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Corporate Name</td>
							<td class="tabletext" colspan="3">
								<html:text size="135" maxlength="100" property="corporateName" styleClass="textbox"/>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Attn.</td>
							<td class="tabletext">
								<html:text size="45" maxlength="100" property="attn" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="20%">E-mail Address</td>
							<td class="tabletext">
								<html:text size="24" property="emailAddress" styleClass="textbox" onblur="return validateEmailId('emailAddress');"/><BR>
							</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Business Phone, Ext</td>
						<td class="tabletext">
							<html:text size="14" property="businessPhone" maxlength="12" onkeypress="return DisplayPhoneHyphen('businessPhone');" styleClass="textbox"/>&nbsp;
	                        <html:text size="5" property="businessExtension" maxlength="5" styleClass="textbox" onkeypress="return NumericEntry()"/>&nbsp;<BR>
						</td>
				       	<td class="tablelabel" width="20%">Business Fax</td>
						<td class="tabletext">
							<html:text size="14" property="businessFax" maxlength="12" onkeypress="return DisplayPhoneHyphen('businessFax');" styleClass="textbox"/><BR>
				       </td>
					</tr>
					
					<tr>
						<td class="tablelabel" width="20%">Activity Type</td>
						<td class="tabletext"><bean:write name="businessTaxActivityForm"  property="activityTypeDescription"/></td>
						<td class="tablelabel" width="20%">Activity Sub-Type</td>
						<td class="tabletext"><bean:write name="businessTaxActivityForm"  property="activitySubType"/></td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Description of Business</td>
						<td class="tabletext">
							<html:text size="40" maxlength="500" property="descOfBusiness" styleClass="textbox"/><BR>
						</td>
						<td class="tablelabel" width="20%">Activity Status</td>
							<td class="tabletext">
								<html:select property="activityStatus" size="1" styleClass="textbox" onchange="checkStatus()">
									<html:option value="">Please Select</html:option>
									<html:options collection="activityStatuses" property="status" labelProperty="description"/>
								</html:select><BR>
							</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">NAICS Code</td>
						<td class="tabletext"> <bean:write name="businessTaxActivityForm" property="sicCode"/></td>
						<td class="tablelabel" width="20%">Municipal Code</td>
						<td class="tabletext"><bean:write name="businessTaxActivityForm" property="muncipalCode"/></td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Class Code</td>
						<td class="tabletext"><bean:write name="businessTaxActivityForm"  property="classCode"/></td>
						<td class="tablelabel" width="20%">Home Occupation</td>
						<td class="tabletext">
								<html:checkbox property="homeOccupation" styleClass="textbox" >Yes</html:checkbox><BR>
							</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Decal Code</td>
						<td class="tabletext">
						    	<html:checkbox property="decalCode" styleClass="textbox">Yes</html:checkbox>
						</td>
						<td class="tablelabel" width="20%">Are there any other businesses occupying this premise?</td>
						<td class="tabletext">
								<html:checkbox property="otherBusinessOccupancy" styleClass="textbox" >Yes</html:checkbox><BR>
							</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Application Date</td>
						<td class="tabletext"><nobr>
							<html:text  property="applicationDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].applicationDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                 	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
						</td>
						<td class="tablelabel" width="20%">Issue Date</td>
						<td class="tabletext" id="actStatus"><nobr>
                        	<html:text   property="issueDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								 	<html:link href="javascript:show_calendar('forms[0].issueDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
						</td>

					</tr>
					<tr>
						<td class="tablelabel" width="20%">Starting Date</td>
						<td class="tabletext"><nobr>
							<html:text  property="startingDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].startingDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
			                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
						</td>
						<td class="tablelabel" width="20%">Out of Business Date</td>
						<td class="tabletext"><nobr>
							<html:text  property="outOfBusinessDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].outOfBusinessDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
									<img src="../images/calendar.gif" width="16" height="15" border=0/>
			                    </html:link></nobr>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Ownership Type</td>
						<td class="tabletext">
							 <html:select property="ownershipType" size="1" styleClass="textbox">
								<html:option value="">Please Select</html:option>
								<html:options collection="ownershipTypes" property="id" labelProperty="description"/>
							</html:select><BR>
						</td>
						<td class="tablelabel" width="20%">Federal ID Number</td>
						<td class="tabletext">
							<html:text size="20"  maxlength="9" property="federalIdNumber" styleClass="textbox" onkeypress="return alphaNumericEntry()"/>
						</td>
					</tr>
					<tr>

						<td class="tablelabel" width="20%">Social Security Number</td>
						<td class="tabletext">
							<html:text size="14" property="socialSecurityNumber" maxlength="11" onkeypress="return DisplaySSNHyphen('socialSecurityNumber');"  styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="20%">Square Footage (Sq. Ft)</td>
						<td class="tabletext">
							<html:text size="20" property="squareFootage" styleClass="textbox" onkeypress="return NumericEntry()"/><BR>
						</td>
					</tr>
					<tr>
						<%-- <td class="tablelabel" width="25%">Mailing Address</td>
						<td class="tabletext">
							<html:text size="5" maxlength="80" property="mailStreetNumber" styleClass="textbox" onkeypress="return NumericEntry()" onblur="return ValidBlankSpace(this);"/>
							<html:text size="14" maxlength="80" property="mailStreetName" styleClass="textbox" onblur="return ValidBlankSpace(this);"/>
						</td>
						<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
						<td  class="tabletext" valign="top">
							<html:text size="5" maxlength="80" property="mailUnitNumber" styleClass="textbox" onblur="return ValidBlankSpace(this);"/>
							<html:text size="10" maxlength="80" property="mailCity" styleClass="textbox" onblur="return ValidBlankSpace(this);"/>
							<html:text size="2" maxlength="2" property="mailState" styleClass="textbox" onkeypress="return NonNumericEntry()" onblur="return ValidBlankSpace(this);"/>
							<html:text size="6" maxlength="6" property="mailZip" styleClass="textbox"/>
							<html:text size="4" property="mailZip4" styleClass="textbox" onblur="return ValidBlankSpace(this);"/>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Mail Attn</td>
						<td class="tabletext">
							<html:text size="14" property="mailAttn" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="20%">Pre Attn</td>
						<td class="tabletext">
							<html:text size="20" property="preAttn" styleClass="textbox" /><BR>
						</td>
					</tr>
					  <tr>
						<td class="tablelabel" width="25%">Previous Address</td>
						<td class="tabletext">
							<html:text size="5" property="prevStreetNumber" styleClass="textbox"/>
							<html:text size="14" property="prevStreetName" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
						<td  class="tabletext" valign="top">
							<html:text size="5" property="prevUnitNumber" styleClass="textbox"/>
							<html:text size="10" property="prevCity" styleClass="textbox"/>
							<html:text size="2" maxlength="2" property="prevState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
							<html:text size="5" maxlength="5" property="prevZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
							<html:text size="4" property="prevZip4" styleClass="textbox"/>
						</td>
					</tr> --%>
					
					<%--<tr>
							<td class="tablelabel" width="20%">Owner1 Name</td>
							<td class="tabletext">
								<html:text size="20" maxlength="100" property="name1" styleClass="textbox"/>&nbsp;<BR>
							</td>
							<td class="tablelabel" width="20%">Owner1 Title</td>
							<td class="tabletext">
								<html:text size="20" maxlength="100" property="title1" styleClass="textbox"/>
							</td>
					</tr>
					 <tr>
						<td class="tablelabel" width="25%">Owner1 Address</td>
						<td class="tabletext">
							<html:text size="5" property="owner1StreetNumber" styleClass="textbox" onkeypress="return NumericEntry()"/>
							<html:text size="14" property="owner1StreetName" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
						<td  class="tabletext" valign="top">
							<html:text size="5" property="owner1UnitNumber" styleClass="textbox"/>
							<html:text size="10" property="owner1City" styleClass="textbox"/>
							<html:text size="2" maxlength="2" property="owner1State" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
							<html:text size="5" maxlength="5" property="owner1Zip" styleClass="textbox" onkeypress="return NumericEntry()"/>
							<html:text size="4" property="owner1Zip4" styleClass="textbox"/>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Owner1 Attn</td>
						<td class="tabletext">
							<html:text size="14" property="owner1Attn" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="20%">Owner2 Attn</td>
						<td class="tabletext">
							<html:text size="20" property="owner2Attn" styleClass="textbox" /><BR>
						</td>
					</tr>
			       --%>
					<%--	<tr>
							<td class="tablelabel" width="20%">Owner2 Name</td>
							<td class="tabletext">
								<html:text size="20" maxlength="100" property="name2" styleClass="textbox"/>&nbsp;<BR>
							</td>
							<td class="tablelabel" width="20%">Owner2 Title</td>
							<td class="tabletext">
								<html:text size="20" maxlength="100" property="title2" styleClass="textbox"/>
							</td>
					</tr>
					 <tr>
						<td class="tablelabel" width="25%">Owner2 Address</td>
						<td class="tabletext">
							<html:text size="5" property="owner2StreetNumber" styleClass="textbox" onkeypress="return NumericEntry()"/>
							<html:text size="14" property="owner2StreetName" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
						<td  class="tabletext" valign="top">
							<html:text size="5" property="owner2UnitNumber" styleClass="textbox"/>
							<html:text size="10" property="owner2City" styleClass="textbox"/>
							<html:text size="2" maxlength="2" property="owner2State" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
							<html:text size="5" maxlength="5" property="owner2Zip" styleClass="textbox" onkeypress="return NumericEntry()"/>
							<html:text size="4" property="owner2Zip4" styleClass="textbox"/>
						</td>
					</tr> --%>

					<tr>

							<td class="tablelabel" width="25%">Quantity</td>
							<td width="25%" class="tabletext">
				                <html:select property="quantity" size="1" styleClass="textbox">
									<html:option value="">Please Select</html:option>
									<html:options collection="qtytList" property="id" labelProperty="description"/>
								</html:select>
								<html:text size="20" property="quantityNum" styleClass="textbox" onkeypress="return NumericEntry()"/>
							</td>
							<td width="25%" class="tablelabel" width="20%">Driver's License</td>
							<td width="25%" class="tabletext">
								<html:text size="20" maxlength="25" property="driverLicense" styleClass="textbox"/>&nbsp;<BR>
							</td>
						</tr>
				</table>
		</td>
	</tr>
			<%--  <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			 <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Business Address</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    
               
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							
				<%if(businessLocation1 == "false"|| businessLocation1.equals("false")){%>
						<tr>
							<td class="tablelabel" width="25%">Out of Town Address</td>
							<td class="tabletext">
								<html:text size="5" property="outOfTownStreetNumber" styleClass="textbox"/>
								<html:text size="14" property="outOfTownStreetName" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
							<td class="tabletext" valign="top">
								<html:text size="5" property="outOfTownUnitNumber" styleClass="textbox"/>
								<html:text size="10" property="outOfTownCity" styleClass="textbox"/>
								<html:text size="2" maxlength="2" property="outOfTownState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<html:text size="5" maxlength="5" property="outOfTownZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<html:text size="4" property="outOfTownZip4" styleClass="textbox"/>
							</td>
						</tr>
						<%}else{%>
						<tr>
                            <td width="25%" class="tablelabel">Business Address</td>
                            <td class="tabletext" colspan="3"><%=businessAddressStreetNumber1%> <%=businessAddressStreetFraction1%> <%=businessAddressStreetName1%> <%=businessAddressUnitNumber1%> <%=businessAddressCity1%> <%=businessAddressState1%> <%=businessAddressZip1%> <%=businessAddressZip41%>

							</td>
                        </tr>
               			<%}%>
               			</table></td></tr></table></td></tr> --%>
					<tr>
                <td colspan="3">&nbsp;</td>
            		</tr>
				<nested:iterate id="rec" name="businessTaxActivityForm" property="multiAddress" type="elms.app.common.MultiAddress">
				<nested:hidden property="id"></nested:hidden>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"> <nested:write property="addressType"></nested:write></td>
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    
               
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							
							
							<tr valign="top">
                                <td width="25%" class="tablelabel">Name</td>
                                <td width="25%" class="tabletext">
               					<nested:text size="14" property="name" styleClass="textbox"/>
                                 </td>
                                <td width="25%" class="tablelabel">Title</td>
                                <td width="25%" class="tabletext">
                                <nested:text size="14" property="title" styleClass="textbox"/></td>
                            </tr>
							
							<tr valign="top">
                                <td width="25%" class="tablelabel">Street Number , Street Name1</td>
                                <td width="25%" class="tabletext">
                                <nested:text size="5"  property="streetNumber" styleClass="textbox" onkeypress="return NumericEntry()"/>
               					<nested:text size="14" property="streetName1" styleClass="textbox"/>
                                 </td>
                                <td width="25%" class="tablelabel">Street Name2</td>
                                <td width="25%" class="tabletext">
                                <nested:text size="14" property="streetName2" styleClass="textbox"/></td>
                            </tr>
                            <tr valign="top">
                                <td width="25" class="tablelabel"> Unit,City,State,Zip,Zip4</td>
                                <td width="25%" class="tabletext">
								<nested:text size="5" property="unit" styleClass="textbox"/>
								<nested:text size="10" property="city" styleClass="textbox"/>
								<nested:text size="2" maxlength="2" property="state" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<nested:text size="5" maxlength="5" property="zip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<nested:text size="4" property="zip4" styleClass="textbox"/></td>
                                <td width="20%" class="tablelabel">Attn</td>
                                <td width="20%" class="tabletext">
                                <nested:text size="10" property="attn" styleClass="textbox"/>
                                </td>
                            </tr>
                   </table>
                        </td>
                    </tr>
                    
                    </table>
                        </td>
                    </tr>
                    
                     <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            </nested:iterate>
				
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>
	</table>
	<td width="1%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="32">&nbsp;</td>
			</tr>
		</table>
			</td>
</html:form>
</body>
</html:html>
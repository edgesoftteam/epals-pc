<%@ page import='elms.app.bt.*, elms.agent.*,elms.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<%
String contextRoot = request.getContextPath();
BusinessTaxActivity businessTaxActivity= (elms.app.bt.BusinessTaxActivity)request.getAttribute("businessTaxActivity");
int activityId = StringUtils.s2i((String)request.getAttribute("activityId"));
String activityType= (String)request.getAttribute("activityType");
	if (activityType == null) activityType = "";


%>
<head>
<html:base/>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script language="JavaScript">
function deleteActivity() {
   userInput = confirm("Are you sure you want to delete this Activity?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/deleteActivity.do?activityId=<%=activityId%>'
   }
}
</script>
</head>
<%!
String outOfTownStreetNumber = "";
String outOfTownStreetName = "";
String outOfTownUnitNumber = "";
String outOfTownCity = "";
String outOfTownState = "";
String outOfTownZip = "";
String outOfTownZip4 = "";
String businessAddressStreetNumber = "";
String businessAddressStreetFraction = "";
String businessAddressStreetName = "";
String businessAddressUnitNumber = "";
String businessAddressCity = "";
String businessAddressState = "";
String businessAddressZip = "";
String businessAddressZip4 = "";
%>

<%

	String activityNumber = businessTaxActivity.getActivityNumber();
	if (activityNumber == null || activityNumber == "") activityNumber = "";

	String activityStatus = businessTaxActivity.getActivityStatus().getDescription()!= null ? businessTaxActivity.getActivityStatus().getDescription(): "";
	if (activityStatus == null || activityStatus == "") activityStatus = "";
	System.out.println("activityType######");

	String applicationType = businessTaxActivity.getApplicationType() != null ? businessTaxActivity.getApplicationType().getDescription(): "";
	if (applicationType == null || applicationType == "") applicationType = "";
	System.out.println("applicationType " +businessTaxActivity.getApplicationType());

	activityType = businessTaxActivity.getActivityType() != null ? businessTaxActivity.getActivityType().getDescription(): "";
	if (activityType == null) activityType = "";
	System.out.println("activityType");

	boolean businessLocation = businessTaxActivity.isBusinessLocation();
	if(businessLocation == true){

	businessAddressStreetNumber = (String) businessTaxActivity.getBusinessAddressStreetNumber();
	if (businessAddressStreetNumber == null) businessAddressStreetNumber = "";

	businessAddressStreetFraction = businessTaxActivity.getBusinessAddressStreetFraction();
	if (businessAddressStreetFraction == null) businessAddressStreetFraction = "";

	businessAddressStreetName = businessTaxActivity.getBusinessAddressStreetName();
	if (businessAddressStreetName == null) businessAddressStreetName = "";

	businessAddressUnitNumber = businessTaxActivity.getBusinessAddressUnitNumber();
	if (businessAddressUnitNumber == null) businessAddressUnitNumber = "";

	businessAddressCity = businessTaxActivity.getBusinessAddressCity();
	if (businessAddressCity == null) businessAddressCity = "";

	businessAddressState = businessTaxActivity.getBusinessAddressState();
	if (businessAddressState == null) businessAddressState = "";

	businessAddressZip = businessTaxActivity.getBusinessAddressZip();
	if (businessAddressZip == null) businessAddressZip = "";

	businessAddressZip4 = businessTaxActivity.getBusinessAddressZip4();
		if (businessAddressZip4 == null) businessAddressZip4 = "";
		}else{

		outOfTownStreetNumber = (String) businessTaxActivity.getOutOfTownStreetNumber();
		if (outOfTownStreetNumber == null)outOfTownStreetNumber = "";

		outOfTownStreetName = businessTaxActivity.getOutOfTownStreetName();
		if (outOfTownStreetName == null) outOfTownStreetName = "";

		outOfTownUnitNumber = businessTaxActivity.getOutOfTownUnitNumber();
		if (outOfTownUnitNumber == null) outOfTownUnitNumber = "";

		outOfTownCity = businessTaxActivity.getOutOfTownCity();
		if (outOfTownCity == null) outOfTownCity = "";

		outOfTownState = businessTaxActivity.getOutOfTownState();
		if (outOfTownState == null) outOfTownState = "";

		outOfTownZip = businessTaxActivity.getOutOfTownZip();
		if (outOfTownZip == null) outOfTownZip = "";

		outOfTownZip4 = businessTaxActivity.getOutOfTownZip4();
		if (outOfTownZip4 == null) outOfTownZip4 = "";
	}

	String businessName = businessTaxActivity.getBusinessName();
	if (businessName == null) businessName = "";

	String corporateName = businessTaxActivity.getCorporateName();
	if (corporateName == null) corporateName = "";

	String businessPhone = businessTaxActivity.getBusinessPhone();
	if (businessPhone == null) businessPhone = "";

	String businessExtension = businessTaxActivity.getBusinessExtension();
	if (businessExtension == null) businessExtension = "";

	String businessFax = businessTaxActivity.getBusinessFax();
	if (businessFax == null) businessFax = "";

	String sicCode = businessTaxActivity.getSicCode();
	if (sicCode == null) sicCode = "";

	String muncipalCode = businessTaxActivity.getMuncipalCode();
	if (muncipalCode == null) muncipalCode = "";

	String descOfBusiness = businessTaxActivity.getDescOfBusiness();
	if (descOfBusiness == null) descOfBusiness = "";

	String classCode = businessTaxActivity.getClassCode();
	if (classCode == null) classCode = "";

	boolean homeOccupation = businessTaxActivity.isHomeOccupation();
	String homeOccupationYesNo = "";
	if (homeOccupation == true){homeOccupationYesNo = "Yes";}else{homeOccupationYesNo = "No";}

	boolean decalCode = businessTaxActivity.isDecalCode();
	String decalCodeYesNo = "";
	if (decalCode == true){decalCodeYesNo = "Yes";}else{decalCodeYesNo = "No";}

	boolean otherBusinessOccupancy = businessTaxActivity.isOtherBusinessOccupancy();
	String otherBusinessOccupancyYesNo = "";
	if (otherBusinessOccupancy == true){otherBusinessOccupancyYesNo = "Yes";}else{otherBusinessOccupancyYesNo = "No";}

	String creationDate = elms.util.StringUtils.cal2str(businessTaxActivity.getCreationDate());
	if (creationDate == null) creationDate = "";

	String applicationDate = elms.util.StringUtils.cal2str(businessTaxActivity.getApplicationDate());
	if (applicationDate == null) applicationDate = "";

	//String issueDate = elms.util.StringUtils.cal2str(businessTaxActivity.getIssueDate());
	//if (issueDate == null) issueDate = "";

	String startingDate = elms.util.StringUtils.cal2str(businessTaxActivity.getStartingDate());
	if (startingDate == null) startingDate = "";

	String outOfBusinessDate = elms.util.StringUtils.cal2str(businessTaxActivity.getOutOfBusinessDate());
	if (outOfBusinessDate == null) outOfBusinessDate = "";

	String ownershipType = businessTaxActivity.getOwnershipType() != null ? businessTaxActivity.getOwnershipType().getDescription(): "";
	if (ownershipType == null || ownershipType == "") ownershipType = "";

	String federalIdNumber = businessTaxActivity.getFederalIdNumber();
	if (federalIdNumber == null) federalIdNumber = "";

	String emailAddress = businessTaxActivity.getEmailAddress();
	if (emailAddress == null) emailAddress = "";

	String socialSecurityNumber = businessTaxActivity.getSocialSecurityNumber();
	if (socialSecurityNumber == null) socialSecurityNumber = "";

	String mailStreetNumber = businessTaxActivity.getMailStreetNumber();
	if (mailStreetNumber == null) mailStreetNumber = "";

	String mailStreetName = businessTaxActivity.getMailStreetName();
	if (mailStreetName == null) mailStreetName = "";

	String mailUnitNumber = businessTaxActivity.getMailUnitNumber();
	if (mailUnitNumber == null) mailUnitNumber = "";

	String mailCity = businessTaxActivity.getMailCity();
	if (mailCity == null) mailCity = "";

	String mailState = businessTaxActivity.getMailState();
	if (mailState == null) mailState = "";

	String mailZip = businessTaxActivity.getMailZip();
	if (mailZip == null) mailZip = "";

	String mailZip4 = businessTaxActivity.getMailZip4();
	if (mailZip4 == null) mailZip4 = "";

	String prevStreetNumber = businessTaxActivity.getPrevStreetNumber();
	if (prevStreetNumber == null) prevStreetNumber = "";

	String prevStreetName = businessTaxActivity.getPrevStreetName();
	if (prevStreetName == null) prevStreetName = "";

	String prevUnitNumber = businessTaxActivity.getPrevUnitNumber();
	if (prevUnitNumber == null) prevUnitNumber = "";

	String prevCity = businessTaxActivity.getPrevCity();
	if (prevCity == null) prevCity = "";

	String prevState = businessTaxActivity.getPrevState();
	if (prevState == null) prevState = "";

	String prevZip = businessTaxActivity.getPrevZip();
	if (prevZip == null) prevZip = "";

	String prevZip4 = businessTaxActivity.getPrevZip4();
	if (prevZip4 == null) prevZip4 = "";


	String name1 = businessTaxActivity.getName1();
	if (name1 == null) name1 = "";

	String title1 = businessTaxActivity.getTitle1();
	if (title1 == null) title1 = "";

	String owner1StreetNumber = businessTaxActivity.getOwner1StreetNumber();
	if (owner1StreetNumber == null) owner1StreetNumber = "";

	String owner1StreetName = businessTaxActivity.getOwner1StreetName();
	if (owner1StreetName == null)owner1StreetName = "";

	String owner1UnitNumber = businessTaxActivity.getOwner1UnitNumber();
	if (owner1UnitNumber == null) owner1UnitNumber = "";

	String owner1City = businessTaxActivity.getOwner1City();
	if (owner1City == null) owner1City = "";

	String owner1State = businessTaxActivity.getOwner1State();
	if (owner1State == null) owner1State = "";

	String owner1Zip = businessTaxActivity.getOwner1Zip();
	if (owner1Zip == null) owner1Zip = "";

	String owner1Zip4 = businessTaxActivity.getOwner1Zip4();
	if (owner1Zip4 == null)owner1Zip4 = "";

	String name2 = businessTaxActivity.getName2();
	if (name2 == null) name2 = "";

	String title2 = businessTaxActivity.getTitle2();
	if (title2 == null) title2 = "";

	String owner2StreetNumber = businessTaxActivity.getOwner2StreetNumber();
	if (owner2StreetNumber == null) owner2StreetNumber = "";

	String owner2StreetName = businessTaxActivity.getOwner2StreetName();
	if (owner2StreetName == null)owner2StreetName = "";

	String owner2UnitNumber = businessTaxActivity.getOwner2UnitNumber();
	if (owner2UnitNumber == null) owner2UnitNumber = "";

	String owner2City = businessTaxActivity.getOwner2City();
	if (owner2City == null) owner2City = "";

	String owner2State = businessTaxActivity.getOwner2State();
	if (owner2State == null) owner2State = "";

	String owner2Zip = businessTaxActivity.getOwner2Zip();
	if (owner2Zip == null) owner2Zip = "";

	String owner2Zip4 = businessTaxActivity.getOwner2Zip4();
	if (owner2Zip4 == null)owner2Zip4 = "";

	String squareFootage = businessTaxActivity.getSquareFootage();
	if (squareFootage == null) squareFootage = "";

	String quantity =businessTaxActivity.getQuantity() != null ? businessTaxActivity.getQuantity().getDescription(): "";
	if (quantity == null || quantity == "") quantity = "";

	String quantityNum = businessTaxActivity.getQuantityNum();
	if (quantityNum == null) quantityNum = "";

	String driverLicense = businessTaxActivity.getDriverLicense();
	if (driverLicense == null) driverLicense = "";

	String issueDate = elms.util.StringUtils.cal2str(businessTaxActivity.getIssueDate());
	if (issueDate == null) issueDate = "";

	String renewalOnline = businessTaxActivity.getRenewalOnline();
	if (renewalOnline == null) renewalOnline = "";
	if(!renewalOnline.equals("") && renewalOnline.equalsIgnoreCase("Y")){
		renewalOnline = "Yes";
	}else{
		renewalOnline = "No";
	}
	
	int icreatedBy = businessTaxActivity.getCreatedBy();
	String name = "";
	try{
	name = LookupAgent.getUserDetails(StringUtils.i2s(icreatedBy)).getUsername();
	if (name == null) name = "";
	}catch(Exception e){}

//int lsoId= StringUtils.s2i((String)request.getAttribute("lsoId"));
String lsoId = (String)session.getAttribute("lsoId");
%>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="parent.f_lso.location.href='/epals/lsoSearchWithLso.do?lsoId=<%=lsoId%>';parent.f_psa.location.href='/epals/viewPsaTree.do?lsoId=<%=lsoId%>&psaNodeId=<%=activityId%>'">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td colspan="3"><font class="con_hdr_3b">Business Activity Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td width="100%" class="tabletitle">Business Activity Detail</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="tablelabel">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tablelabel"></td>

								<td width="45%" class="tablelabel" align="right">
									<!-- Edit Business Tax Activity -->
		                         	<a href="<%=contextRoot%>/editBusinessTaxActivity.do?activityId=<%=activityId%>&lsoId=<%=lsoId%>" class="hdrs">Edit</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                         	<!-- delete Business Tax Activity -->
									<a href="javascript:deleteActivity();" class="hdrs">Delete</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                           	<!--Print Business Tax Certificate -->
		                       		<a href="<%=contextRoot%>/editBusinessTaxActivity.do" class="hdrs" >Print Business Tax Certificate</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                    	</td>
							</tr>
						</table>
						</td>
					</tr>
	                <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
                                <td width="20%" class="tablelabel">Activity Number</td>
                                <td width="20%" class="tabletext"><%=activityNumber%></td>
                                <td width="20%" class="tablelabel">Activity Status</td>
                                <td width="20%" class="tabletext"><%=activityStatus%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Application Type</td>
                                <td class="tabletext"><%=applicationType%></td>
                                <td class="tablelabel">Activity Type</td>
                                <td class="tabletext"><%=activityType%></td>
                            </tr>
                            <tr>
                            	<td class="tablelabel">Renewal Code</td>
								<td class="tabletext"><%=activityId%></td>
								<td class="tablelabel"> Online Renewed</td>
								<td class="tabletext"><%=renewalOnline%></td>
                            </tr>
							<%if(businessLocation == true){%>
                            <tr valign="top">
                                <td class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="3"><%=businessAddressStreetNumber%> <%=businessAddressStreetFraction%> <%=businessAddressStreetName%> <%=businessAddressUnitNumber%> <%=businessAddressCity%> <%=businessAddressState%> <%=businessAddressZip%> <%=businessAddressZip4%></td>
                            </tr>
							<%}else{%>
							<tr valign="top">
                                <td class="tablelabel">Out of Town Address</td>
                                <td class="tabletext" colspan="3"><%=outOfTownStreetNumber%> <%=outOfTownStreetName%> <%=outOfTownUnitNumber%> <%=outOfTownCity%> <%=outOfTownState%> <%=outOfTownZip%> <%=outOfTownZip4%><%}%></td>
                            </tr>

                            <tr valign="top">
                                <td class="tablelabel">Business Name</td>
                                <td class="tabletext"><%=businessName%></td>
								 <td class="tablelabel">Corporate Name</td>
                                <td class="tabletext"><%=corporateName%></td>
							</tr>

                            <tr valign="top">
                                <td class="tablelabel">Business Phone,Ext</td>
                                <td class="tabletext"><%=businessPhone%> <%=businessExtension%></td>
                                <td class="tablelabel">Business Fax</td>
                                <td class="tabletext"><%=businessFax%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">SIC</td>
                                <td class="tabletext"><%=sicCode%></td>
								<td class="tablelabel">Municipal Code</td>
                                <td class="tabletext"><%=muncipalCode%></td>

                            </tr>
                            <tr valign="top">
								<td class="tablelabel">Description of Business</td>
                                <td class="tabletext"><%=descOfBusiness%></td>
								<td class="tablelabel">Class Code</td>
                                <td class="tabletext"><%=classCode%></td>
                            </tr>
							<tr valign="top">
								<td class="tablelabel">Decal Code</td>
                                <td class="tabletext"><%=decalCodeYesNo%></td>
                                <td class="tablelabel">Home Occupation</td>
                                <td class="tabletext" colspan="1"><%=homeOccupationYesNo%></td>
							</tr>
							<tr valign="top">
								<td class="tablelabel">Are there any other Business Occupying this premises</td>
                                <td class="tabletext" colspan="3"><%=homeOccupationYesNo%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Application Date</td>
                                <td class="tabletext"><%=applicationDate%></td>
                                <td class="tablelabel">Issue Date</td>
                                <td class="tabletext"><%=issueDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Starting Date</td>
                                <td class="tabletext"><%=startingDate%></td>
                                <td class="tablelabel">Out of Business Date</td>
                                <td class="tabletext"><%=outOfBusinessDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Ownership Type</td>
                                <td class="tabletext"><%=ownershipType%></td>
                                <td class="tablelabel">Federal ID Number</td>
                                <td class="tabletext"><%=federalIdNumber%></td>
                            </tr>
												<tr valign="top">
                                <td class="tablelabel">E-mail Address</td>
                                <td class="tabletext"><%=emailAddress%></td>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext">XXX-XX-XXXX</td>
                            </tr>

							<tr valign="top">
                                <td class="tablelabel">Mailing Address</td>
                                <td class="tabletext" colspan="3"><%=mailStreetNumber%> <%=mailStreetName%> <%=mailUnitNumber%> <%=mailCity%> <%=mailState%> <%=mailZip%> <%=mailZip4%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Previous Address</td>
                                <td class="tabletext" colspan="3"><%=prevStreetNumber%> <%=prevStreetName%> <%=prevUnitNumber%> <%=prevCity%> <%=prevState%> <%=prevZip%> <%=prevZip4%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Name1</td>
                                <td class="tabletext"><%=name1%></td>
                                <td class="tablelabel">Title</td>
                                <td class="tabletext"><%=title1%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Owner1 Address</td>
                                <td class="tabletext" colspan="3"><%=owner1StreetNumber%> <%=owner1StreetName%> <%=owner1UnitNumber%> <%=owner1City%> <%=owner1State%> <%=owner1Zip%> <%=owner1Zip4%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Name2</td>
                                <td class="tabletext"><%=name2%></td>
                                <td class="tablelabel">Title</td>
                                <td class="tabletext"><%=title2%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Owner2 Address</td>
                                <td class="tabletext" colspan="3"><%=owner2StreetNumber%> <%=owner2StreetName%> <%=owner2UnitNumber%> <%=owner2City%> <%=owner2State%> <%=owner2Zip%> <%=owner2Zip4%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Square Footage (Area Occupied)</td>
                                <td class="tabletext"><%=squareFootage%></td>
                                <td class="tablelabel">Quantity</td>
                                <td class="tabletext"><%=quantity%> <%=quantityNum%></td>
                            </tr>

							<tr valign="top">
                                <td class="tablelabel">Creation Date</td>
                                <td class="tabletext"><%=creationDate%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=name%></td>
                            </tr>
							 <tr valign="top">
                                <td class="tablelabel">Driver's License</td>
                                <td class="tabletext" colspan="3"><%=driverLicense%></td>
							</tr>
						</table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"></table></td></tr>
					</table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			<tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="98%" class="tabletitle">Business Tax Contact Manager</td>
								<td width="1%" class="tablebutton"><nobr></nobr></td>
								<td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/addHold.do?levelId=15363&level=A" class="hdrs"><font class="con_hdr_link">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                             </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><div align="left">People Type</div></td>
                                <td class="tablelabel"><div align="left">Title</div></td>
                                <td class="tablelabel"><div align="left">Name</div></td>
                                <td class="tablelabel"><div align="left">Phone Number</div></td>
								<td class="tablelabel"><div align="left">Hold</div></td>
                            </tr>

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
			<tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"></table></td></tr>
					</table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			<tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="98%" background="../images/site_bg_B7C1CB.jpg">Finance Manager</td>
								<td width="1%"><img src="../images/site_hdr_split_1.jpg" width="18" height="19"></td>
								<td width="1%" class="tablelabel"><nobr></nobr></td>
                             </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td width="20%" class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel"><div align="right">Activity</div></td>
                                <td class="tablelabel"><div align="right">Plan Check</div></td>
                                <td class="tablelabel"><div align="right">Business Tax</div></td>
                                <td class="tablelabel"><div align="right">Total</div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Fees</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Payment</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Extended Credit</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Bounced Amount</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Balance Due </td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
		</table>
<td width="1%">
        <table width="200" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=15363&level=A&ed=true&status=Inactive" class="hdrs">Holds</a></td>
								<td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/addHold.do" class="hdrs"><font class="con_hdr_link">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
			<tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Approvals</td>
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=15363&levelType=A" class="hdrs"><font class="con_hdr_link">View<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Site Data</td>
                        <td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=15363&levelType=A" class="hdrs"><font class="con_hdr_link">View<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listLinkActivity.do?activityId=15363&editable=true" class="hdrs">Linked Activities</a></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr><td background="../images/site_bg_B7C1CB.jpg"><table width="100%" border="0" cellspacing="1" cellpadding="2"></table></td></tr>
                </table>
                </td>
            </tr>
			<tr><td>&nbsp;</td></tr>
           </table>
          </td>
         </tr>
       </table>

	</body>
</html:html>

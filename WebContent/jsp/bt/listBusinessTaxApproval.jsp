<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%
String contextRoot = request.getContextPath();
 %>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
	font: normal 8pt/9pt verdana,arial,helvetica;
	color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>
</head>
<script>
function getlist(){
var departmentId = document.forms[0].departmentId.value;

	if(departmentId != '99999'){
		var flag = "false";
		var departmentCheck="check";
		document.forms[0].action='<%=contextRoot%>/jsp/bt/listBusinessTaxApproval.jsp?departmentCheck='+departmentCheck+'&departmentId='+departmentId+'&flag='+flag;
	    document.forms[0].submit();
	}else{
		var flag = "true";
		var departmentCheck="uncheck";
		document.forms[0].action='<%=contextRoot%>/jsp/bt/listBusinessTaxApproval.jsp?departmentCheck='+departmentCheck+'&departmentId='+departmentId+'&flag='+flag;
	    document.forms[0].submit();
	}
}

</script>
<%! String departmentCheck;
	String flag; %>

<%
	try
	{
		departmentCheck= elms.util.StringUtils.nullReplaceWithEmpty((String) request.getParameter("departmentCheck"));
		flag= elms.util.StringUtils.nullReplaceWithEmpty((String) request.getParameter("flag"));
		if(!(departmentCheck.trim().equals("")) && !(departmentCheck==null)){

			int deptId =Integer.parseInt(elms.util.StringUtils.nullReplaceWithEmpty((String) request.getParameter("departmentId")));
			java.util.List singleDepartmentList=new java.util.ArrayList();
			singleDepartmentList = new elms.agent.ActivityAgent().getBusinessTaxApprovalList(deptId);
			pageContext.setAttribute("singleDepartmentList",singleDepartmentList);
			java.util.List onlyDepartmentName = new java.util.ArrayList();
			onlyDepartmentName= new elms.agent.ActivityAgent().getOnlyBTDepartmentNameList(deptId);
			pageContext.setAttribute("onlyDepartmentName",onlyDepartmentName);
		}
	}catch(Exception e){
//ignored
}


java.util.List businessTaxApprovalDepartmentList = new java.util.ArrayList();
java.util.List departments = new java.util.ArrayList();

try{
departments = new elms.agent.LookupAgent().getDepartmentList();


businessTaxApprovalDepartmentList = new elms.agent.ActivityAgent().getBusinessTaxApprovalDepartmentList();

}
catch(Exception e){
//ignored
}
pageContext.setAttribute("businessTaxApprovalDepartmentList",businessTaxApprovalDepartmentList);

pageContext.setAttribute("departments",departments);


%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/addBusinessLicenseApproval">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="100%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Business Tax Approvals</font><br><br></td>
				</tr>
				<tr>
					<td class="tabletitle" valign="top"><b>Select</b></td>
				</tr>
				<tr>
					<td valign="top" class="tablelabel"><nobr> Approving Departments  :   </nobr>
						<html:select property="departmentId" styleClass="textbox" onchange="javascript:getlist()">
						<html:option  value="-1" >Please Select</html:option>
						<html:option  value="99999">Show All</html:option>
						<html:options collection="departments" property="departmentId" labelProperty="description" />
						</html:select>
					</td>
                </tr>
				<tr>
					<td>
						<table width="100%" cellspacing="0" cellpadding="0">
						<!-- Start of List of Approvals with out any Department -->
						<%if(((departmentCheck.trim().equalsIgnoreCase("unCheck"))) && flag.equalsIgnoreCase("true")){%>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td width="6%" class="tablelabel">Activity No1</td>
	                        				<td width="9%" class="tablelabel">Business Account Number</td>
					                        <td width="14%" class="tablelabel">Business Name</td>
					                        <td width="10%" class="tablelabel">Activity Type</td>
					                        <td width="8%" class="tablelabel">Activity Status</td>
					                        <td width="8%" class="tablelabel">Approval Status</td>
			                        		<td width="15%" class="tablelabel">Description of Business</td>
					                        <td width="15%" class="tablelabel">Address </td>
					                        <td width="7%" class="tablelabel">Date Referred </td>
											<td width="8%" class="tablelabel">User Name </td>
			                            </tr>
										<%
										for(int i=0;i<businessTaxApprovalDepartmentList.size();i++){
										elms.app.bt.BusinessTaxActivity businessTaxActivity = (elms.app.bt.BusinessTaxActivity)businessTaxApprovalDepartmentList.get(i);
										java.util.List businessLicenseApprovals = (java.util.List) businessTaxActivity.getBusinessLicenseApprovals();

										for(int j=0;j<businessLicenseApprovals.size();j++){
											elms.app.bt.BusinessTaxActivity businessTaxActivityApproval = (elms.app.bt.BusinessTaxActivity) businessLicenseApprovals.get(j);
											pageContext.setAttribute("businessTaxActivityApproval",businessTaxActivityApproval);

									 	%>
										<tr>
				                            <td width="6%" class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<bean:write name="businessTaxActivityApproval" property="activityId"/>"><bean:write name="businessTaxActivityApproval" property="activityNumber"/></a></td>
											<td width="9%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="businessAccountNumber"/></td>
				                            <td width="14%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="businessName"/></td>
				                            <td width="10%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="activityType.description"/></td>
				                            <td width="8%" class="tabletext" valign="top"><a href="<%=contextRoot%>/addBusinessLicenseApproval.do?activityId=<bean:write name="businessTaxActivityApproval" property="activityId"/>&approvalId=<bean:write name="businessTaxActivityApproval" property="approvalId"/>&applicationDateString=<bean:write name="businessTaxActivityApproval" property="applicationDateString"/>&departmentName=<bean:write name="businessTaxActivityApproval"  property="departmentName"/>&userName=<bean:write name="businessTaxActivityApproval"  property="userName"/>&address=<bean:write name="businessTaxActivityApproval"  property="address" />&activityType=<bean:write name="businessTaxActivityApproval"  property="activityType.description"/>&activityNumber=<bean:write name="businessTaxActivityApproval" property="activityNumber"/>"><bean:write name="businessTaxActivityApproval"  property="activityStatusDescription"/></a></td>
				                            <td width="8%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="approvalStatus"/></td>
				                            <td width="15%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="descOfBusiness"/></td>
				                            <td width="15%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="address" /></td>
				                         	<td width="7%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="applicationDateString" /></td>
											<td width="8%" class="tabletext" valign="top"><bean:write name="businessTaxActivityApproval"  property="userName" /></td>
				                        </tr><%}}%>
									</table>
								</td>
							</tr>
								<!-- Start of List of Approvals with out any Department -->
								<%}else if((!(departmentCheck.trim().equals("")) && !(departmentCheck==null)) && flag.equalsIgnoreCase("false")){%>
								<!-- Start of List of Approvals for selected / single Department -->
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg" valign="top"><b></b></td>
							</tr>
								<logic:iterate id="onlyDeptName" name="onlyDepartmentName" type="elms.app.bt.BusinessTaxActivity">
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg"><nobr><b><bean:write name="onlyDeptName"  property="departmentName" /></b></nobr></td>
							</tr>
							<tr><td height="20">&nbsp;</td></tr>
								</logic:iterate>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
											<td width="6%" class="tablelabel">Activity No2</td>
	                        				<td width="9%" class="tablelabel">Business Account Number</td>
					                        <td width="14%" class="tablelabel">Business Name</td>
					                        <td width="10%" class="tablelabel">Activity Type</td>
					                        <td width="8%" class="tablelabel">Activity Status</td>
					                        <td width="8%" class="tablelabel">Approval Status</td>
			                        		<td width="15%" class="tablelabel">Description of Business</td>
					                        <td width="15%" class="tablelabel">Address </td>
					                        <td width="7%" class="tablelabel">Date Referred </td>
											<td width="8%" class="tablelabel">User Name </td>
										</tr>
										<logic:iterate id="singleDepttList" name="singleDepartmentList" type="elms.app.bt.BusinessTaxActivity">
										<tr>
				                            <td width="6%" class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<bean:write name="singleDepttList" property="activityId"/>"><bean:write name="singleDepttList" property="activityNumber"/></a></td>
											<td width="9%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="businessAccountNumber"/></td>
				                            <td width="14%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="businessName"/></td>
				                            <td width="10%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="activityType.description"/></td>
				                            <td width="8%" class="tabletext" valign="top"><a href="<%=contextRoot%>/addBusinessLicenseApproval.do?activityId=<bean:write name="singleDepttList" property="activityId"/>&approvalId=<bean:write name="singleDepttList" property="approvalId"/>&applicationDateString=<bean:write name="singleDepttList" property="applicationDateString"/>&departmentName=<bean:write name="singleDepttList"  property="departmentName"/>&userName=<bean:write name="singleDepttList"  property="userName"/>&address=<bean:write name="singleDepttList"  property="address" />&activityType=<bean:write name="singleDepttList"  property="activityType.description"/>&activityNumber=<bean:write name="singleDepttList" property="activityNumber"/>"><bean:write name="singleDepttList"  property="activityStatusDescription"/></a></td>
				                            <td width="8%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="approvalStatus"/></td>
				                            <td width="15%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="descOfBusiness"/></td>
				                            <td width="15%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="address" /></td>
				                         	<td width="7%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="applicationDateString" /></td>
											<td width="8%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="userName" /></td>
				                        </tr>
										</logic:iterate>
								  </table>
								</td>
							</tr>
							<!-- End of List of Approvals for selected / single Department -->
							<%}else{
							for(int i=0;i<businessTaxApprovalDepartmentList.size();i++){
								elms.app.bt.BusinessTaxActivity businessTaxActivity = (elms.app.bt.BusinessTaxActivity)businessTaxApprovalDepartmentList.get(i);
								java.util.List businessLicenseApprovals = (java.util.List) businessTaxActivity.getBusinessLicenseApprovals();
							 %>
							<!-- Start of all the list of Approvals differed with Departments -->
							<tr>
								<td height="20" class="tabletext">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="10" background="../images/site_bg_B7C1CB.jpg" valign="top"><b><%=businessTaxActivity.getDepartmentName() %></b></td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
									<table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
										<tr>
					                        <td width="6%" class="tablelabel">Activity No3</td>
		                    				<td width="9%" class="tablelabel">Business Account Number</td>
					                        <td width="14%" class="tablelabel">Business Name</td>
					                        <td width="10%" class="tablelabel">Activity Type</td>
					                        <td width="8%" class="tablelabel">Activity Status</td>
					                        <td width="8%" class="tablelabel">Approval Status</td>
			                        		<td width="15%" class="tablelabel">Description of Business</td>
					                        <td width="15%" class="tablelabel">Address </td>
					                        <td width="7%" class="tablelabel">Date Referred </td>
											<td width="8%" class="tablelabel">User Name </td>
										</tr>
										<%
											for(int j=0;j<businessLicenseApprovals.size();j++){
											elms.app.bt.BusinessTaxActivity businessTaxActivityApproval = (elms.app.bt.BusinessTaxActivity) businessLicenseApprovals.get(j);
											pageContext.setAttribute("businessTaxActivityApproval",businessTaxActivityApproval);
										%>
										<tr>
				                            <td width="6%" class="tabletext" ><a href="<%=contextRoot%>/viewActivity.do?activityId=<bean:write name="businessTaxActivityApproval" property="activityId"/>"><bean:write name="businessTaxActivityApproval" property="activityNumber"/></a></td>
											<td width="9%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="businessAccountNumber"/></td>
				                            <td width="14%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="businessName"/></td>
				                            <td width="10%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="activityType.description"/></td>
				                            <td width="8%" class="tabletext" ><a href="<%=contextRoot%>/addBusinessLicenseApproval.do?activityId=<bean:write name="businessTaxActivityApproval" property="activityId"/>&approvalId=<bean:write name="businessTaxActivityApproval" property="approvalId"/>&applicationDateString=<bean:write name="businessTaxActivityApproval" property="applicationDateString"/>&departmentName=<bean:write name="businessTaxActivityApproval"  property="departmentName"/>&userName=<bean:write name="businessTaxActivityApproval"  property="userName"/>&address=<bean:write name="businessTaxActivityApproval"  property="address" />&activityType=<bean:write name="businessTaxActivityApproval"  property="activityType.description"/>&activityNumber=<bean:write name="businessTaxActivityApproval" property="activityNumber"/>"><bean:write name="businessTaxActivityApproval"  property="activityStatusDescription"/></a></td>
				                            <td width="8%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="approvalStatus"/></td>
				                            <td width="15%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="descOfBusiness"/></td>
				                            <td width="15%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="address" /></td>
				                         	<td width="7%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="applicationDateString" /></td>
											<td width="8%" class="tabletext" ><bean:write name="businessTaxActivityApproval"  property="userName" /></td>
				                        </tr>
										<!-- End of all the list of Approvals differed with Departments -->
										<%} %>
									</table>
	                       		</td>
	                   		</tr><%}}%>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</html:form>
</body>
</html:html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="elms.common.*,java.util.*,elms.security.*" %>
<%  
   boolean general=false,conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false;
   User user = (User)session.getAttribute("user");
   List groups = (java.util.List) user.getGroups();
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true; 
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;   
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;  
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;   
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;  
        if(group.groupId == Constants.GROUPS_GENERAL) general = true;  
   }
%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
 
<html:html> 
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/elms.css" type="text/css">
<script src="script/newPsaTree.js"> </script>

<script type="text/javascript">
	var Tree = new Array;
	// nodeId | parentNodeId | nodeName | nodeUrl | nodeAltText | lsoType
    <logic:iterate id="branch" name="psaTreeList" type="java.lang.String" scope="request" >
    <%=branch%>   
	</logic:iterate>
</script>

</head>

<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg">
<% if(!(general)){%>
<html:form action="/viewPsaTree">
<html:hidden property="lsoId"/>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="frametitle">&nbsp;&nbsp;PROJECT EXPLORER</td>
    </tr>
    <tr>
        <td valign="top" class="tableform-top"><img src="images/spacer.gif" width="200" height="5"></td>
    </tr>
    <tr>
        <td class="tableform">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <table cellpadding="2" cellspacing="0" border="0">
                        <tr>
                            <td><html:radio property="radiobutton" value="radio_active" onclick="javascript:document.forms[0].submit();"/></td>
                            <td class="framelabel">Active</td>
                            <td><img src="images/spacer.gif" width="10" height="1"></td>
                            <td><html:radio property="radiobutton" value="radio_all" onclick="javascript:document.forms[0].submit();"/></td>
                            <td class="framelabel">All</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td valign="top" class="tableform-bottom"><img src="images/spacer.gif" width="500" height="5"></td>
    </tr>
    <tr>
        <td><img src="images/spacer.gif" width="1" height="10"></td>
    </tr>
    <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td nowrap>
                <div id="tree">
				<script type="text/javascript">
					//createTree(Tree,0,12);
					createTree(Tree);
					<%
						String psaNodeId = request.getParameter("psaNodeId");
						Integer childId= new Integer(0);
						if(psaNodeId == null) psaNodeId = "0";
						try{
						        childId = new Integer(psaNodeId);
						 } catch(Exception e) {
						 }
						 if(childId == null) childId = new Integer(0);
					%>
					var localvalue = <%=childId%>;
					if((localvalue != null) && (localvalue != 0)) {
						  var parentId = highlightAndGetParent(localvalue);
						  if((parentId != null) && (localvalue != 0)) oc(parentId,0);
						  parentId = Math.round(parentId);
						  parentId--;						
						  if(parentId > 0) {
							 var newId = getParentofNode(parentId);
							 if((newId != null) && (newId >= 0)) {
							 	oc(newId,0);
							 }
						  }
					}
				</script></div>
                </td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>

<script language="JavaScript">
scrollToNode(localvalue);
</script>
<%}%>
</html:html>

<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="elms.control.beans.planning.CalendarFeatureForm"%>
<app:checkLogon />
<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Edit calendar</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/formValidations.js"></script>

<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:errors/>
<%

String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";

 CalendarFeatureForm calendarFeatureForm = (CalendarFeatureForm)request.getAttribute("calendarFeatureForm");

if(calendarFeatureForm == null){
  calendarFeatureForm =  (CalendarFeatureForm)session.getAttribute("calendarFeatureForm");
}
String subProjectId = (String)request.getAttribute("subProjectId");
   if(subProjectId ==null) subProjectId = "";

%>
<script language="JavaScript">
function reSubmit(){
  document.forms['calendarFeatureForm'].action='<%=contextRoot%>/pullActiontype.do';
  document.forms['calendarFeatureForm'].submit();
  return true;
}


function onclickprocess()
{
      document.forms['calendarFeatureForm'].action='<%=contextRoot%>/deleteCalendarFeature.do?subProjectId=<%=subProjectId%>';
      document.forms['calendarFeatureForm'].submit();
      return true;
}

function validateFunction() {
   	    if (document.forms[0].elements['meetingType'].value == '-1')  {
	    	alert('Select Meeting Type');
	    	document.forms[0].elements['meetingType'].focus();
	    	return false;
		}else if (document.forms[0].elements['actionType'].value == '-1')  {
	    	alert('Select Action Type');
	    	document.forms[0].elements['actionType'].focus();
	    	 return false;
		}
		else{
			 document.forms['calendarFeatureForm'].action='<%=contextRoot%>/saveCalendarFeature.do?subProjectId=<%=subProjectId%>';
      		document.forms['calendarFeatureForm'].submit();

		}
}

function goBack(button)
{
	button.disabled=true;
	document.location.href='<%=contextRoot%>/viewSubProject.do?subProjectId=<%=subProjectId%>';
}


</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form   name="calendarFeatureForm" type="elms.control.beans.planning.CalendarFeatureForm" action="">
<html:hidden property="subProjectId"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br></td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Add</td>

                      <td width="1%" class="tablelabel"><nobr>
                          <html:button  property="Cancel"  value="Back" styleClass="button" onclick="goBack(this);"/>
                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"/>
                       &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  <TBODY>
                      <tr>
							<td class="tablelabel">
								Meeting Type
							</td>
							<td class="tablelabel">
								Meeting Date
							</td>
							<td class="tablelabel">
								Action Type
							</td>
						</tr>
						<tr valign="top">
							<td class="tabletext">
								<html:select name="calendarFeatureForm"	property="meetingType" styleClass="textbox" onchange="reSubmit();">
								<html:options collection="meetingTypeList" property="code" labelProperty="description" />
								</html:select>
							</td>
							   <td class="tabletext"  valign="top"><nobr>
                                           <html:text name="calendarFeatureForm"  property="meetingDate" size="10" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
					                   		</nobr>
                                        </td>

							<td class="tabletext">
							<html:select name="calendarFeatureForm" property="actionType" styleClass="textbox">
								<html:options collection="actionTypeList" property="code" labelProperty="description" />
							</html:select>
							</td>
						</tr>
						<tr>
							<td class="tablelabel">
								Comments
							</td>
							<td class="tabletext" colspan="2">
								<html:textarea name="calendarFeatureForm" property="comments" cols="75" rows="7" styleClass="textbox" />
							</td>
						</tr>
                  </table>
                </td>
              </tr>
               <tr>
                <td>&nbsp;</td>
            </tr>
             <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
						  <td width="98%" background="../images/site_bg_B7C1CB.jpg">Meeting History</td>
						  <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="21" height="20"></td>
						  <td width="1%" class="tablelabel"><nobr>
						  	 <html:button property="delete" value="Delete" styleClass="button" onclick="return onclickprocess();"/>
						  </tr>
					</table>
				</td>
			</tr>
            <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr>
					<td class="tablelabel"><nobr>
						Remove</nobr>
					</td>
					<td class="tablelabel"><nobr>
						Meeting Type</nobr>
					</td>
					<td class="tablelabel">
						Meeting Date
					</td>
					<td class="tablelabel">
						Action Type
					</td>
					<td class="tablelabel">
						Comments
					</td>
				</tr>
            <nested:iterate property="calendarFeatures" type="calendarFeatures.CalendarFeature">
			<tr valign="top">
				<td class="tabletext">
					<html:multibox property="selectedRecord">
						<nested:write property="calendarFeatureId" />
					</html:multibox>
				</td>
					<!--<td class="tabletext"><nested:write property="meetingType" /></td>-->
				<td class="tabletext">
					<a href='<%=contextRoot%>/editCalendarFeature.do?spCalendarId=<nested:write property="calendarFeatureId"/>'>
						<nested:write property="meetingType" />
					</a>
				</td>
				<td class="tabletext">
					<nested:write property="meetingDate" />
				</td>
				<td class="tabletext">
					<nested:write property="actionType" />
				</td>
				<td class="tabletext">
					<nested:write property="comments" />
				</td>
			</tr>
			</nested:iterate>
                  </table>
                </td>
              </tr>
            </table>
           </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>
<%
String contextRoot = request.getContextPath();
java.util.List departments = new java.util.ArrayList();

%>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Renewal</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<link rel="stylesheet" type="text/css" href="../script/dropdown/ui.dropdownchecklist.css" />
<link rel="stylesheet" type="text/css" href="../script/dropdown/demo.css" />
<script type="text/javascript" src="../script/dropdown/jquery.js"></script>
<script type="text/javascript" src="../script/dropdown/ui.core.js"></script>
<script type="text/javascript" src="../script/dropdown/ui.dropdownchecklist.js"></script>
<SCRIPT language="JavaScript" src="../script/calendar.js"></SCRIPT>
<script language="JavaScript">



function restictChar(){
  if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46) && (event.keyCode != 44) )  event.returnValue = false;
}


function getProject(){
var checkval="";

	for (var i=0; i < document.linkActivityForm.prjList.length; i++){
		   if (document.linkActivityForm.prjList[i].checked ==true){
			   checkval += document.linkActivityForm.prjList[i].value+",";
		      }
		  }
    checkval += "0";

   var val="";
    for (var i=0; i < document.linkActivityForm.active.length; i++){
       if (document.linkActivityForm.active[i].checked){
	       val= document.linkActivityForm.active[i].value;
        }
     }

   var actId= document.forms[0].activityId.value;

   var prjNameId= checkval;
   document.forms[0].action= "<%=contextRoot%>/listLinkActivity.do?activityId="+actId+"&editable=true&active="+val+"&prjNameId="+prjNameId;
   document.forms[0].submit();
}

</script>




<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="linkActivityForm" type="elms.control.beans.planning.LinkActivityForm" action="/saveLinkActivity.do">
<html:hidden property="activityId" />

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Linked Activities&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="linkActivityForm" property="address" /><br>
            <br>
            </td>
        </tr>
        <table width="100%" border="0" cellspacing="1" cellpadding="0">

                <tr>
                 <td background="../images/site_bg_B7C1CB.jpg">
                       <logic:iterate id ="cSearch" name ="projectNames" type="elms.app.admin.ProjectName" scope="request">
                           <html:multibox  property="prjList"><bean:write name="cSearch" property="projectNameId"/> </html:multibox><bean:write name="cSearch" property="description"/>
                       </logic:iterate>
                       &nbsp;<html:button property="go" style="width=10%" value=" Refresh " styleClass="button" onclick="getProject();"/>
                       </td>
                      </tr>
           </table>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                      <td width="97%" background="../images/site_bg_B7C1CB.jpg">

                      	Active<html:radio property="active" value="Y" onclick="getProject();" /> &nbsp;
                      	 All<html:radio property="active" value="N" onclick="getProject();" /> &nbsp;

                      </td>

                      <td width="1%" class="tablelabel"><nobr>
                          <input type="submit" value="Back" name=cancel class="button">
                          <logic:equal name="linkActivityForm" property="editable" value="true">
                             <input type="reset" value="Reset" name=reset class="button">
                             <input type="submit" value="Save" name=save class="button">
                          </logic:equal>
                        &nbsp;</nobr></td>
                    </tr>
                 </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel"><nobr>Select
                        </nobr></td>
                      <td class="tablelabel"><nobr>Activity #
                        </nobr></td>
                      <td class="tablelabel">Name
                        </td>
                      <td class="tablelabel">Status
                        </td>
                    </tr>
                  <nested:iterate id="subProject" name="linkActivityForm" property="allLinkedActivitiesList">
                    <tr valign="top">
	               <td class="tabletext" valign="top" valign="top"><nested:checkbox name="subProject" property="linked" /></td>
	               <td class="tabletext" valign="top" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<nested:write name="subProject" property="activityId" />"><nested:write name="subProject" property="activityNbr" /></a></td>
	               <td class="tabletext" valign="top" valign="top"><nested:write name="subProject" property="activityType" /></td>
	               <td class="tabletext" valign="top" valign="top"><nested:write name="subProject" property="status" /></td>
                    </tr>
		  </nested:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

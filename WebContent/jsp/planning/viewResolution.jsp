<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ page import="java.util.*,elms.util.StringUtils"%>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Renewal</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<%
String contextRoot = request.getContextPath();
 int size = StringUtils.s2i((String)request.getAttribute("size"));

 %>
<script language="JavaScript" src="../script/formValidations.js"></script>
<SCRIPT language="JavaScript" src="../script/calendar.js"></SCRIPT>
<script language="JavaScript">


function reSubmit(save){

			for(var i=0;i<<%=size%>;i++){
			   if(document.forms[0].elements['resolutionList['+i+'].approvedDate'].value == ''){
					alert('Please enter approved date ');
					document.forms[0].elements['resolutionList['+i+'].approvedDate'].focus();
					return false;
				}
				else if(document.forms[0].elements['resolutionList['+i+'].resolutionNbr'].value == ''){
					alert('Please enter resolutionNbr ');
					document.forms[0].elements['resolutionList['+i+'].resolutionNbr'].focus();
					return false;
				}
				else if(document.forms[0].elements['resolutionList['+i+'].description'].value == ''){
					alert('Please enter description ');
					document.forms[0].elements['resolutionList['+i+'].description'].focus();
					return false;
				}

			//	ValidBlankSpace(document.forms[0].elements['resolutionList['+i+'].resolutionNbr']);
			}

					document.forms['resolutionForm'].action='<%=contextRoot%>/saveResolution.do?save=save';
					document.forms['resolutionForm'].submit();

		 		}

function addRow(add){
	      document.forms['resolutionForm'].action='<%=contextRoot%>/saveResolution.do?add=add';
	      document.forms['resolutionForm'].submit();
	}

function goBack(cancel){
	      document.forms['resolutionForm'].action='<%=contextRoot%>/saveResolution.do?cancel=cancel';
	      document.forms['resolutionForm'].submit();
	}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}

function restictChar(){
  if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46) && (event.keyCode != 44) )  event.returnValue = false;
}

function validate() {
    ctr=parseInt(document.forms['dotActivityForm'].elements['peopleCount'].value);
    count = 0;
    for(i=0;i<=ctr;i++) {
        if (document.forms['dotActivityForm'].elements['peopleList[' + i + '].firstName'].value == ''){
           alert("Please enter First Name.");
           document.forms['dotActivityForm'].elements['peopleList[' + i + '].firstName'].focus();
           return false;
        }
        if (document.forms['dotActivityForm'].elements['peopleList[' + i + '].lastName'].value == ''){
           alert("Please enter Last Name.");
           document.forms['dotActivityForm'].elements['peopleList[' + i + '].lastName'].focus();
           return false;
        }
        if (document.forms['dotActivityForm'].elements['peopleList[' + i + '].peopleType'].value == ''){
           alert("Please select People type.");
           document.forms['dotActivityForm'].elements['peopleList[' + i + '].peopleType'].focus();
           return false;
        }
    }

   if (document.forms['dotActivityForm'].elements['paymentAmount'].value != '' && document.forms['dotActivityForm'].elements['paymentAmount'].value != '$0.00'){
	   payMethod=document.forms['dotActivityForm'].elements['payMethod'].options[document.forms['dotActivityForm'].elements['payMethod'].selectedIndex].value;
	   if (payMethod == 'check'){
	       if (document.forms['dotActivityForm'].elements['checknoOrAuthorization'].value == ''){
	           alert("Check # is a required field");
	           document.forms['dotActivityForm'].elements['checknoOrAuthorization'].focus();
	           return false;
	       }
	   }else if (payMethod == 'creditcard') {
	       if (document.forms['dotActivityForm'].elements['checknoOrAuthorization'].value == ''){
	           alert("Authorization # is a required field");
	           document.forms['dotActivityForm'].elements['checknoOrAuthorization'].focus();
	           return false;
	       }
	   }
   }

   ctr=parseInt(document.forms['dotActivityForm'].elements['activityCount'].value);
	for(i=0;i<=ctr;i++) {
	   if (document.forms['dotActivityForm'].elements['activityList[' + i + '].status'].value == '0') {
	       alert("Please Select the Status");
	       document.forms['dotActivityForm'].elements['activityList[' + i + '].status'].focus();
	       return false;
	   }
	}

   ctr=parseInt(document.forms['dotActivityForm'].elements['vehicleCount'].value);
	for(i=0;i<=ctr;i++) {
	   if (document.forms['dotActivityForm'].elements['vehicleList[' + i + '].licenseNumber'].value == '') {
	       alert("Please Enter License #");
	       document.forms['dotActivityForm'].elements['vehicleList[' + i + '].licenseNumber'].focus();
	       return false;
	   }else{
              if (document.forms['dotActivityForm'].elements['vehicleList[' + i + '].state'].value == '') {
	              alert("Please Enter State");
	              document.forms['dotActivityForm'].elements['vehicleList[' + i + '].state'].focus();
	              return false;
	          }
	   }

	}
}



</script>

<%
 int i = -1,ii = 0;
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="resolutionForm" type="elms.control.beans.planning.ResolutionForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Resolutions/Ordinances&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="resolutionForm" property="address" /><br><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Resolutions/Ordinances</td>

                      <td width="1%" class="tablelabel"><nobr>
                        <input type="button" name="add" value="Add" class="button" onclick="addRow('add');">
						<input type="reset" name="reset" value="Reset" class="button">
                        <input type="button" name="cancel" value="Back" class="button" onclick="goBack('cancel');">
                        <input type="button" name="save" value="Save" class="button" onclick="reSubmit('save');">
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Delete</td>
                      <td class="tablelabel" valign="top">Date Approved</td>
                      <td class="tablelabel" valign="top">Reso/Ord#</td>
                      <td class="tablelabel" valign="top">Description</td>
                    </tr>
                    <tr>


                      <nested:iterate id="resolution" name="resolutionForm" property="resolutionList">
<%
 ii = ++i*4 + 5;
%>
 <nested:equal property="delete" value="on">
		 	<nested:hidden property="delete"/>
		 </nested:equal>
<nested:notEqual property="delete" value="on">
		      <tr valign="top" class="tabletext">
	                        <td class="tabletext" valign="top" valign="top"><nested:checkbox name="resolution" property="delete" /></td>
	                        <td class="tabletext" valign="top" valign="top"><nested:text name="resolution" property="approvedDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
                                  <a href="javascript:show_calendar('resolutionForm.elements[<%=ii%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
                                   <img src="../images/calendar.gif" width="16" height="15" border=0></a></nobr></td>
	                        <td class="tabletext" valign="top" valign="top"><nested:text name="resolution" property="resolutionNbr" size="6" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/>
	                        <logic:equal name="resolutionForm" property="enableCheckBox" value="Yes">
	                        <nested:equal property="added" value="Yes">
	                        <logic:equal name="resolutionForm" property="levelType" value="A">
				    		<nobr><div align="left"><nested:checkbox property="uptSubProject" styleId="checkbox2"/>Update Sub-Project Status</nobr></div>
				    		</logic:equal>
				   			<nobr><div align="left"><nested:checkbox property="uptActivities" styleId="checkbox2"/>Update Activities Status</nobr></div>
				   			 </nested:equal>
				   			 </logic:equal>
	                        </td>
	                        <td class="tabletext" valign="top" valign="top"><nested:textarea name="resolution" property="description" cols="35" rows="4" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>


	                      </tr>
	                       </nested:notEqual>
                      </nested:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
</table>
</html:form>
</body>
</html:html>

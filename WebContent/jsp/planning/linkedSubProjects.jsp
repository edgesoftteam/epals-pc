<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Renewal</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<SCRIPT language="JavaScript" src="../script/calendar.js"></SCRIPT>
<script language="JavaScript">

function restictChar(){
  if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46) && (event.keyCode != 44) )  event.returnValue = false;
}
</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="linkSubProjectForm" type="elms.control.beans.planning.LinkSubProjectForm" action="/saveLinkSubProject.do">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Linked Subprojects&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="linkSubProjectForm" property="address" /><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="97%" background="../images/site_bg_B7C1CB.jpg">Sub Projects</td>

                      <td width="1%" class="tablelabel"><nobr>
                          <input type="submit" value="Cancel" name=cancel class="button">
                          <input type="reset" value="Reset" name=reset class="button">
                          <input type="submit" value="Save" name=save class="button">
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel"><nobr>Select
                        </nobr></td>
                      <td class="tablelabel"><nobr>Sub Project #
                        </nobr></td>
                      <td class="tablelabel">Name
                        </td>
                      <td class="tablelabel">Status
                        </td>
                    </tr>
                  <nested:iterate id="subProject" name="linkSubProjectForm" property="subProjectList">
                    <tr valign="top">
	               <td class="tabletext" valign="top" valign="top"><nested:checkbox name="subProject" property="linked" /></td>
	               <td class="tabletext" valign="top" valign="top"><nested:write name="subProject" property="subProjectNbr" /></td>
	               <td class="tabletext" valign="top" valign="top"><nested:write name="subProject" property="description" /></td>
	               <td class="tabletext" valign="top" valign="top"><nested:write name="subProject" property="status" /></td>
                    </tr>
		  </nested:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

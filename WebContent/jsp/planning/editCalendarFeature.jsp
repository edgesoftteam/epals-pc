<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />
<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Edit calendar</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:errors/>
<%
   java.util.List planCheckStatuses = LookupAgent.getPlanCheckStatuses();
   pageContext.setAttribute("planCheckStatuses", planCheckStatuses);
   // Getting the employees as plan check engineers from the groups  1-Inspector, 6-Supervisor, 7-Engineer , 11- Permit Tech
   java.util.List engineerUsers = ActivityAgent.getEngineerUsers(-1);
   pageContext.setAttribute("engineerUsers", engineerUsers);
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   String subProjectId = (String)request.getAttribute("subProjectId");
   if(subProjectId ==null) subProjectId = "";
   if (lsoAddress == null ) lsoAddress = "";
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";


   String spCalendarId =(String)request.getAttribute("spCalendarId");
   if(spCalendarId ==null) spCalendarId = "";
%>
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>

<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript">
var strValue;
function validateFunction() {
   	    if (document.forms[0].elements['meetingType'].value == '-1')  {
	    	alert('Select Meeting Type');
	    	document.forms[0].elements['meetingType'].focus();
	    	return false;
	    } else if (document.forms[0].elements['meetingDate'].value == '')  {
	    	alert('Select Meeting Date');
	    	document.forms[0].elements['meetingDate'].focus();
	    	 return false;
		}else if (document.forms[0].elements['actionType'].value == '-1')  {
	    	alert('Select Action Type');
	    	document.forms[0].elements['actionType'].focus();
	    	 return false;
		}  else { document.forms['calendarFeatureForm'].action='<%=contextRoot%>/saveCalendarFeature.do';
      	document.forms['calendarFeatureForm'].submit();
		return true;
		}

}

function onclickprocess()
{
      document.forms['calendarFeatureForm'].action='<%=contextRoot%>/deleteCalendarFeature.do?subProjectId=<%=subProjectId%>';
      document.forms['calendarFeatureForm'].submit();
      return true;
}
function goBack(button)
{
	//button.value='Please Wait..';
	button.disabled=true;
	document.location.href='<%=contextRoot%>/viewSubProject.do?subProjectId=<%=subProjectId%>';
}
function reSubmit(){
  document.forms['calendarFeatureForm'].action='<%=contextRoot%>/editCalendarFeature.do?edit=edit&spCalendarId=<%=spCalendarId%>';
  document.forms['calendarFeatureForm'].submit();
  return true;
}
function goBack() {
  parent.f_content.location.href='<%=contextRoot%>/pullActiontype.do';
}

</script>

<html:form   name="calendarFeatureForm" type="elms.control.beans.planning.CalendarFeatureForm" action="">
<html:hidden property="subProjectId"/>
<html:hidden property="spCalendarFeatureId" value="<%=spCalendarId%>"/>


	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Modify Calendar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br></td>
			</tr>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">

									<tr>
										<td width="99%" background="../images/site_bg_B7C1CB.jpg"><font
											class="con_hdr_2b">Edit</td>
										<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
											width="26" height="25"></td>
										<td width="1%" class="tablelabel">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="history.back();" />&nbsp;
												<html:button property="save" value="Save" styleClass="button" onclick="return validateFunction();" /> &nbsp;
											</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td class="tablelabel"><font
											class="con_text_1">Meeting Type</td>
										<td class="tablelabel"><font
											class="con_text_1">Meeting Date</td>
										<td class="tablelabel"><font
											class="con_text_1">Action Type</td>
									</tr>
									<tr valign="top">
										<td class="tabletext">
										<html:select name="calendarFeatureForm" property="meetingType" styleClass="textbox" onchange="reSubmit();">
											<html:options collection="meetingTypeList" property="code" labelProperty="description" />
										    </html:select>
										</td>
                                        <td class="tabletext"  valign="top"><nobr>
                                           <html:text  property="meetingDate" size="10" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
					                       </nobr>
                                        </td>
                           			    <td class="tabletext"><html:select property="actionType"
											styleClass="textbox" >
											<html:options collection="actionTypeList" property="code"
												labelProperty="description" />
										    </html:select>
										</td>
									</tr>
									<tr>
										<td class="tablelabel"><font
											class="con_text_1">Comments</td>
										<td class="tabletext" colspan="2"><html:textarea
											property="comments" cols="75" rows="7" styleClass="textbox" /></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="32">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
	</table>
</html:form>

</body>
</html:html>


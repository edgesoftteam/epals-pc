<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="java.util.*, elms.control.beans.*, elms.app.inspection.*"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="java.util.*,elms.app.common.*,elms.agent.*"%>
<%@ page import="elms.control.beans.PlannerUpdateForm"%>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>


<app:checkLogon/>


<html:html>
<head>
<html:base/>
<title>City of Burbank : ePALS : Planners</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<script>

function selectAllCheckBox(tmpForm){


 document.forms[0].action='<%=contextRoot%>/plannerSelectAll.do';
 document.forms[0].submit();

}



function selectCheckBox(){

 document.forms[0].action='<%=contextRoot%>/plannerSelectAllOne.do';
 document.forms[0].submit();


}




function onchangeprocess(){
  if(document.forms[0].reAssign.value != ''){
   document.forms[0].action='<%=contextRoot%>/reassignPlanner.do';
   document.forms[0].submit();
  }
 }


</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="javascript:parent.f_reports.location.reload();">
<html:form name="plannerUpdateForm" type="elms.control.beans.PlannerUpdateForm" action="">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b"><a name="top">Planner Update</a><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
              	   <tr>
                  	   <td>
                      	   <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                               	 <td width="99%"  background="../images/site_bg_B7C1CB.jpg">Planners</td>
                              </tr>
                           </table>
                       </td>
           		   </tr>
                   <tr>
                      <td background="../images/site_bg_B7C1CB.jpg">
                         <table width="100%" border="0" cellspacing="1" cellpadding="2">
         	                <tr>
                            	<td class="tablelabel" valign="top" colspan="4">Re-Assign</td>
	                            <td class="tabletext" valign="top" colspan="5">
	                            	<html:select property="reAssign" styleClass="textbox" onchange="onchangeprocess()">
	                            		<html:option value="">please select</html:option>
	                                	<html:options collection="planners" property="nameValue" labelProperty="nameLabel"/>
	                                </html:select>
	                            </td>
	                        </tr>
                  		<nested:iterate indexId="indexId" scope="session" name="plannerUpdateForm" property="plannerArray" >
							<tr>
                                <td class="tabletext" colspan="9"><img src="../images/spacer.gif" width="1" height="5"></td>
                            </tr>
							<tr>
                                <td class="tablelabel" colspan="9"><nobr><a name=""><b>&nbsp;<nested:write property="userName"/></b></a></td>
                            </tr>
                            <tr>
                              <td class="tablelabel">
                              	<nested:checkbox styleId="selectAll" property="selectAllCheckBox" onclick="selectAllCheckBox(this)" value="true" />
                               <nested:hidden property="selectAllCheckBox" value="false" />
										</td>
                              <td  class="tablelabel">Sub Project No</td>
                              <td class="tablelabel">Sub Project Name</td>
                              <td  class="tablelabel"">Sub Project Type</td>
                              <td  class="tablelabel">Sub Project Status</td>
                              <td class="tablelabel">Sub Project Description</td>
                              <td  class="tablelabel">Address</td>
                              <td  class="tablelabel">Applied</td>
                              <td  class="tablelabel">Activity #</td>
                            </tr>
                            <nested:iterate id="testId"  property="plannerUpdateRecord" indexId="lindexId">
                            <tr>
                             <td class="tabletext">&nbsp;
	                             <nested:checkbox styleId="allTypeId"  property="check" value="true" onclick="selectCheckBox()" />
	                             <nested:hidden property="check" value="false" />
                              </td>
                              <td class="tabletext">&nbsp;<a href="<%=contextRoot%>/viewSubProject.do?subProjectId=<nested:write  property="subProjectId" />&address=<nested:write  property="address"/>&active=<nested:write  property="active"/>"><nested:write property="subProjectNumber"/></a></td>
                              <td class="tabletext">&nbsp;<nested:write  property="subProjectName"/></td>
                              <td class="tabletext">&nbsp;<nested:write  property="subProjectType"/></td>
                              <td class="tabletext">&nbsp;<nested:write  property="subProjectStatus"/></td>
                              <td class="tabletext">&nbsp;<nested:write  property="subProjectDescription"/></td>
                              <td class="tabletext">&nbsp;<nested:write  property="address"/></td>
                              <td class="tabletext">&nbsp;<nested:write  property="appliedDate"/></td>
                              <td class="tabletext">&nbsp;<nested:write  property="activityNumber"/></td>
							 </tr>
						   </nested:iterate></nested:iterate>

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>



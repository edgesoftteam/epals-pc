<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon />
<%@ page import="elms.app.admin.*" %>
<%@ page import="java.util.*"%>
<%@ page import="elms.util.*"%>
<html:html>
<head>
<html:base />
<title>OBC : Environmental Review</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%List envDetList = (List) session.getAttribute("envDetList");
pageContext.setAttribute("envDetList", envDetList);

List envDetActionList = (List) session.getAttribute("envDetActionList");
pageContext.setAttribute("envDetActionList", envDetActionList);

String levelType = (String) request.getAttribute("levelType");
pageContext.setAttribute("levelType", levelType);
System.out.println("levelType:="+levelType);

int rows = -1;
int summary = 0;
%>
<script src="../script/yCode_combo_box.js"> </script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/calendar.js"></SCRIPT>
<script>

function chkstatus(){
alert(document.forms['envReviewForm'].elements['added'].value);
}
function reSubmit(action){
	try {

		document.forms['envReviewForm'].elements['action'].value=action;
		document.forms[0].elements['envReviewList[0].envDetermination'].value = document.forms[0].elements['envReviewList[0].envDeterminationId'].options[document.forms[0].elements['envReviewList[0].envDeterminationId'].selectedIndex].text;


	} catch (err) {}

	if (action == 'save')   {

		if(document.forms[0].elements['envReviewList[0].envDetermination'].value==''){
		alert('Please select env.Determination ');
		return false;
		}
	  if(document.forms[0].elements['envReviewList[0].envDeterminationAction'].value==''){
		alert('Please select env.Determination Action');
		return false;
		}
 	if(document.forms[0].elements['envReviewList[0].actionDate'].value==''){
		alert('Please select Action Date');
		return false;
		}
		}
	if ((action == 'cancel') || validateFunction()) {
	  document.forms['envReviewForm'].submit();
	  return true;
	} else
	  return false;
  }



function setAction() {
  document.forms[0].elements['envReviewList[0].envDeterminationAction'].value = document.forms[0].elements['envReviewList[0].envDeterminationActionId'].options[document.forms[0].elements['envReviewList[0].envDeterminationActionId'].selectedIndex].text;
}

function validateFunction()
{
	var strValue;

	strValue=true;
    if (strValue == true)  {
    	strValue=validateData('date',document.forms[0].elements['submittedDate'],'Submitted Date is a required field');
    }

    if ((strValue == true) && (document.forms[0].elements['determinationDate'].value != '')) {
    	strValue=validateData('date',document.forms[0].elements['determinationDate'],'Determination Date is a required field');
    }

    if (strValue == true) {
    	strValue=validateData('date',document.forms[0].elements['requiredDeterminationDate'],'Required Determination Date is a required field');
    }


 	if (strValue == true)  {
    	try {
    	    strValue = validateData('date',document.forms[0].elements['envReviewList[0].actionDate'],'Action Date is a required field');
    	} catch (errr) {}
    }

   return strValue;
}
function makeArray() {
    for (i = 0; i<makeArray.arguments.length; i++)
        this[i + 1] = makeArray.arguments[i];
}

var months = new makeArray('01','02','03','04',
                           '05','06','07','08','09',
                           '10','11','12');
function nths(day) {
    if (day == 1 || day == 21 || day == 31) return 'st';
    if (day == 2 || day == 22) return 'nd';
    if (day == 3 || day == 23) return 'rd';
    return 'th';
}

function y2k(number) { return (number < 1000) ? number + 1900 : number; }

function days(number) {
	if (number < 10) number = '0' + number ;
	return number;
}

function addMonths(iDate,noofmonths) {
    var today = new Date(iDate);
    var date = new Date(today.getYear(),today.getMonth() + noofmonths,today.getDate(),today.getHours(),today.getMinutes(),today.getSeconds());
    return months[date.getMonth()+1] + '/' + days(date.getDate()) + '/' + y2k(date.getYear());
}

function addDays(iDate,noofdays) {
    var today = new Date(iDate);
    var date = new Date(today.getYear(),today.getMonth(),today.getDate() + noofdays,today.getHours(),today.getMinutes(),today.getSeconds());
    return months[date.getMonth()+1] + '/' + days(date.getDate()) + '/' + y2k(date.getYear());
}

function setDateValues() {
    document.forms[0].elements['envReviewSummary.adcLimitDate'].value = addDays(document.forms[0].elements['submittedDate'].value,30);
    document.forms[0].elements['envReviewSummary.adcActualDate'].value = addDays(document.forms[0].elements['submittedDate'].value,30);
    if (document.forms[0].elements['envReviewSummary.status'].value == 'CE' ) {

    	document.forms[0].elements['envReviewSummary.decisionLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.adcActualDate'].value,60);

    } else if (document.forms[0].elements['envReviewSummary.status'].value == 'ND' ) {

    	document.forms[0].elements['envReviewSummary.pisLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.adcActualDate'].value,30);

    	document.forms[0].elements['envReviewSummary.pndLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.adcActualDate'].value,160);

	if (document.forms[0].elements['envReviewSummary.publicNoticeActualDate'].value == '') {
    		document.forms[0].elements['envReviewSummary.decEarliestLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.adcActualDate'].value,180);
    	} else {
    		document.forms[0].elements['envReviewSummary.decEarliestLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.publicNoticeActualDate'].value,20);
    	}

    	document.forms[0].elements['envReviewSummary.decLastLimitDate'].value = addDays(document.forms[0].elements['submittedDate'].value,180);

	if (document.forms[0].elements['envReviewSummary.decEarliestActualDate'].value == '') {
    		document.forms[0].elements['envReviewSummary.decisionLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decLastLimitDate'].value,60);
    	} else {
    		document.forms[0].elements['envReviewSummary.decisionLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decEarliestActualDate'].value,60);
    	}

	if (document.forms[0].elements['envReviewSummary.decisionActualDate'].value == '') {
    		document.forms[0].elements['envReviewSummary.nodLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decisionLimitDate'].value,7);
    	} else {
    		document.forms[0].elements['envReviewSummary.nodLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decisionActualDate'].value,7);
    	}

    } else {

    	document.forms[0].elements['envReviewSummary.pisLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.adcActualDate'].value,30);

    	document.forms[0].elements['envReviewSummary.decLastLimitDate'].value = addMonths(document.forms[0].elements['envReviewSummary.adcActualDate'].value,12);

	//alert(document.forms[0].elements['envReviewSummary.stateReview'].checked);
	if (document.forms[0].elements['envReviewSummary.stateReview'].checked) {
    		document.forms[0].elements['envReviewSummary.nocLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decLastLimitDate'].value,-45);
    	} else {
    		document.forms[0].elements['envReviewSummary.nocLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decLastLimitDate'].value,-30);
    	}

    	document.forms[0].elements['envReviewSummary.nopLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.nocLimitDate'].value,-30);

	temp = document.forms[0].elements['envReviewSummary.nocLimitDate'].value;
	if (document.forms[0].elements['envReviewSummary.nocActualDate'].value != '')
		temp = document.forms[0].elements['envReviewSummary.nocActualDate'].value;
	if (document.forms[0].elements['envReviewSummary.stateReview'].checked) {
		document.forms[0].elements['envReviewSummary.decEarliestLimitDate'].value = addDays(temp,45);
	} else {
		document.forms[0].elements['envReviewSummary.decEarliestLimitDate'].value = addDays(temp,30);
	}

	if (document.forms[0].elements['envReviewSummary.decEarliestActualDate'].value == '') {
    		document.forms[0].elements['envReviewSummary.decisionLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decLastLimitDate'].value,180);
    	} else {
    		document.forms[0].elements['envReviewSummary.decisionLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decEarliestActualDate'].value,180);
    	}

	if (document.forms[0].elements['envReviewSummary.decisionActualDate'].value == '') {
    		document.forms[0].elements['envReviewSummary.nodLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decisionLimitDate'].value,7);
    	} else {
    		document.forms[0].elements['envReviewSummary.nodLimitDate'].value = addDays(document.forms[0].elements['envReviewSummary.decisionActualDate'].value,7);
    	}


    }

    return true;
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0" onload="return setDateValues()">
<html:form action="/saveEnvReview.do">
	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Environmental Review
					List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
						class="con_hdr_blue_3b"><bean:write name="envReviewForm"
						property="address" /><br>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="99%" background="../images/site_bg_B7C1CB.jpg"><font
										class="con_hdr_2b">Environmental Review</td>
									<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
										width="26" height="25"></td>
									<td width="1%" class="tablelabel"><nobr>
									<html:button property="cancel" value="Cancel"
										styleClass="button" onclick="return reSubmit('cancel');" /> <logic:equal
										name="envReviewForm" property="editable" value="true">
										<input type="reset" value="Reset" name="reset" class="button">
										<html:button property="save" value="Save" styleClass="button"
											onclick="return reSubmit('save');" />
									</logic:equal> &nbsp;</nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Application Submittal Date</td>
									<td class="tabletext" valign="top"><nobr> <html:text
										name="envReviewForm" property="submittedDate" size="10"
										maxlength="10" styleClass="textbox"
										onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
										href="javascript:show_calendar('envReviewForm.submittedDate');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;"> <img
										src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
									</td>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Determination Date</td>
									<td class="tabletext" valign="top"><nobr> <html:text
										name="envReviewForm" property="determinationDate" size="10"
										maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
										href="javascript:show_calendar('envReviewForm.determinationDate');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;"> <img
										src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
									</td>
								</tr>
								<tr>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Required Determination Date</td>
									<td class="tabletext" valign="top" colspan="1"><nobr> <html:text
										name="envReviewForm" property="requiredDeterminationDate"
										size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
										href="javascript:show_calendar('envReviewForm.requiredDeterminationDate');"
										onmouseover="window.status='Calendar';return true;"
										onmouseout="window.status='';return true;"> <img
										src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<html:hidden property="envReviewSummary.status" />
						<logic:equal name="envReviewForm"
							property="envReviewSummary.status" value="CE">
							<%summary = 5;%>

							<tr>
								<td background="../images/site_bg_B7C1CB.jpg" colspan="5"><font
									class="con_hdr_2b">Categorical Exemption Timeline <img
									src="../images/site_hdr_split_1_tall.jpg" width="1" height="25"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Application Deemed Complete</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.adcActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[7]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.adcLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Decision</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decisionActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a
									href="javascript:show_calendar('envReviewForm.elements[9]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decisionLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
						</logic:equal>

						<logic:equal name="envReviewForm"
							property="envReviewSummary.status" value="ND">
							<%summary = 17;%>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg" colspan="5"><font
									class="con_hdr_2b">Project With Negative Declarations <img
									src="../images/site_hdr_split_1_tall.jpg" width="1" height="25"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Application Deemed Complete</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.adcActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[7]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.adcLimitDate" disabled="true"
									size="12" styleClass="textbox" /></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Prepare Initial Study</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.pisActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[9]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.pisLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);" /></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext">Initial Study
								deadlines can be extended 15 days</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Prepare Negative Declaration</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.pndActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[11]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.pndLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Public Notice</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.publicNoticeActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[13]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.publicNoticeLimitDate"
									disabled="true" size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext"></td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Adopt Negative Declaration</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decEarliestActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[15]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decEarliestLimitDate"
									disabled="true" size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Earliest Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1"></td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decLastActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[17]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decLastLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Decision</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decisionActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[19]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decisionLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext">Approval
								deadlines can be extended 90 days</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Notice of Determination</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.nodActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[21]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.nodLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
						</logic:equal>

						<logic:equal name="envReviewForm"
							property="envReviewSummary.status" value="OK">
							<%summary = 18;%>
							<tr>
								<td background="../images/site_bg_B7C1CB.jpg" colspan="5"><font
									class="con_hdr_2b">Project with EIRS<BR>
								&nbsp;</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Application Deemed Complete</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.adcActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[7]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.adcLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Prepare Initial Study</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.pisActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[9]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.pisLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext">Initial Study
								deadlines can be extended 15 days</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">State Clearinghouse Review</td>
								<td class="tabletext"> <html:checkbox
									property="envReviewSummary.stateReview"
									onchange="return setDateValues();" /></td>
								<td class="tabletext"></td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Notice of Preparation</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.nopActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[12]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.nopLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Notice of Completion</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.nocActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[14]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.nocLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext">Can be shortened
								to 20 days(30 days for State Review)</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">EIR Certification</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decEarliestActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[16]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decEarliestLimitDate"
									disabled="true" size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Earliest Day</td>
								<td class="tabletext"></td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1"></td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decLastActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[18]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decLastLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext">One year time
								limit can be extended 90 days</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Decision</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.decisionActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[20]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.decisionLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext">Can be extended
								or shortened by 90 days</td>
							</tr>
							<tr>
								<td class="tablelabel" valign="top"><font
									class="con_hdr_1">Notice of Determination</td>
								<td class="tabletext"> <nobr><html:text
									property="envReviewSummary.nodActualDate" size="12"
									styleClass="textbox" onchange="return setDateValues();" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
									href="javascript:show_calendar('envReviewForm.elements[22]');"
									onmouseover="window.status='Calendar';return true;"
									onmouseout="window.status='';return true;"> <img
									src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
								</td>
								<td class="tabletext"><html:text
									property="envReviewSummary.nodLimitDate" disabled="true"
									size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/></td>
								<td class="tabletext">Last Day</td>
								<td class="tabletext"></td>
							</tr>
						</logic:equal>
					</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="99%" background="../images/site_bg_B7C1CB.jpg"><font
										class="con_hdr_2b">Environmental Review</td>
									<td width="1%"><img src="../images/site_hdr_split_1_tall.jpg"
										width="26" height="25"></td>
									<td width="1%" class="tablelabel"><nobr>
									<html:button property="addRow" value="Add" styleClass="button"
										onclick="return reSubmit('addRow');" /> &nbsp;</nobr></td>
								</tr>
							</table>
							</td>
						</tr>
						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" border="0" cellspacing="1" cellpadding="2">
								<tr>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Delete</td>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Env. Determination</td>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Action</td>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Action Date</td>
									<td class="tablelabel" valign="top"><font
										class="con_hdr_1">Comment</td>
								</tr>
								<nested:iterate property="envReviewList">
									<%rows++;int count=0;%>
									<nested:equal property="deleteFlag" value="on">
										<nested:hidden property="deleteFlag" />
									</nested:equal>
									<nested:notEqual property="deleteFlag" value="on">
										<tr>
											<nested:hidden property="envReviewId" />
											<nested:hidden property="envDetermination" />
											<nested:hidden property="envDeterminationAction" />
											<nested:hidden property="update" />
											<nested:hidden property="index" />
											<td class="tabletext">
											<div align="left"><nested:checkbox property="deleteFlag"
												styleId="checkbox2" /></div>
											<td class="tabletext">
											<div align="left"><nested:equal property="index" value="1">
												<nested:select property="envDeterminationId"
													styleClass="textbox" onchange="return reSubmit('');">
													<option value=""></option>
													<html:options collection="envDetList" property="type"
														labelProperty="description" />
												</nested:select>
											</nested:equal> <nested:notEqual property="index" value="1">
												<nested:write
													property="envDetermination" />
											</nested:notEqual>
											<%int flag=0; %>
											<logic:equal name="envReviewForm" property="enableCheckBox" value="Yes">
											<nested:equal property="added" value="Yes">
										    <logic:equal name="envReviewForm" property="level" value="A">
										    <nobr><div align="left"><nested:checkbox property="uptSubProject" styleId="checkbox2"/>Update Sub-Project Status</nobr></div>
										    </logic:equal>
										    <nobr><div align="left"><nested:checkbox property="uptActivities" styleId="checkbox2"/>Update Activities Status</nobr></div>
											<%flag=1; %>
			    							</nested:equal>
			    							</logic:equal>
											</td>
											<td class="tabletext">
											<div align="left"><nested:equal property="index" value="1">
												<nested:select property="envDeterminationActionId"
													styleClass="textbox" onchange="return setAction();">
													<option value=""></option>
													<html:options collection="envDetActionList" property="type"
														labelProperty="description" />
												</nested:select>
											</nested:equal> <nested:notEqual property="index" value="1">
												<nested:write
													property="envDeterminationAction" />
											</nested:notEqual>
											</td>
											<%if(flag==0) {%>
											<td class="tabletext"><nobr><nested:text
												property="actionDate" size="12" styleClass="textbox"  onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
												href="javascript:show_calendar('envReviewForm.elements[15+(<%=rows%>*8)+<%=summary%>]');"
												onmouseover="window.status='Calendar';return true;"
												onmouseout="window.status='';return true;"> <img
												src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
											</td>
											<%}else if(levelType.equals("Q") || levelType=="Q"){%>
											<td class="tabletext"><nobr><nested:text
												property="actionDate" size="12" styleClass="textbox"  onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
												href="javascript:show_calendar('envReviewForm.elements[16+(<%=rows%>*8)+<%=summary%>]');"
												onmouseover="window.status='Calendar';return true;"
												onmouseout="window.status='';return true;"> <img
												src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
											</td>
											<%}else{ %>
											<td class="tabletext"><nobr><nested:text
												property="actionDate" size="12" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/> <a
												href="javascript:show_calendar('envReviewForm.elements[17+(<%=rows%>*8)+<%=summary%>]');"
												onmouseover="window.status='Calendar';return true;"
												onmouseout="window.status='';return true;"> <img
												src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
											</td>
											<%} %>

											<td class="tabletext"><nested:textarea rows="3" cols="25"
												property="comments" /></td>
										</tr>
									</nested:notEqual>
								</nested:iterate>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
	</table>
	<html:hidden property="action" />
</html:form>
</body>
</html:html>

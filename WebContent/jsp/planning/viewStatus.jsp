<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>
<%@ page import="elms.app.admin.*" %>
<%@ page import="java.util.*"%>
<%@ page import="elms.util.*"%>
<html:html>

<head>
<html:base/>
<title>OBC : Planning Status</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
    int rows = -1;

    List statuses = (List) session.getAttribute("statuses");
    pageContext.setAttribute("statuses", statuses);

%>
<script src="../script/yCode_combo_box.js"> </script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/calendar.js"></SCRIPT>
<link rel="stylesheet" href="../css/CalendarControl.css" type="text/css">
<script language="JavaScript" src="../script/CalendarControl.js"></script>
<script language="JavaScript">

function reSubmit(action){
	try {
		document.forms[0].elements['action'].value=action;
	} catch (err) {}
	if ((action == 'cancel') || validateFunction()) {
	  document.forms[0].submit();
	  return true;
	} else
	  return false;
}

function setAction() {
  document.forms[0].elements['envReviewList[0].envDeterminationAction'].value = document.forms[0].elements['envReviewList[0].envDeterminationActionId'].options[document.forms[0].elements['envReviewList[0].envDeterminationActionId'].selectedIndex].text;
}

function validateFunction()
{
	var strValue = true;

	try {
		strValue=validateData('req',document.forms[0].elements['statusList[0].planStatusId'],'Status is a required field');
	} catch (err) {}



	return strValue;
}

function checkDate (field) {
	return validateData('date',field,'Date is not Valid');
}

function checkStatus (field) {
	var status = document.forms[0].elements['lastStatusId'].value;
	status++;
	if (field.value != status)
	   if (confirm("Status Out of Sequence"))
	   	document.forms[0].elements['lastStatusId'].value = field.value;
	   else
	   	field.value = "";
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form action="/saveStatus.do">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Status List&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><bean:write name="statusForm" property="address" /><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Status</td>

                      <td width="1%" class="tablelabel"><nobr>
                          <html:button property="cancel" value="Back" styleClass="button" onclick="return reSubmit('cancel');"/>
						  <logic:equal name="statusForm" property="editable" value="true">
	                          <html:button property="addRow" value="Add" styleClass="button" onclick="return reSubmit('addRow');"/>
	                          <html:button property="save" value="Save" styleClass="button" onclick="return reSubmit('save');"/>
						  </logic:equal>
                        &nbsp;</nobr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="tablelabel">
                  <table width="100%" border="0" cellspacing="1" cellpadding="1" >
                  	<tr>
                  		<td width="30%" class="tabletext" valign="top">
                  			<table width="100%" border="0" cellspacing="1" cellpadding="2">
                           		<tr>
                           			<td valign="top" class="tabletext">&nbsp;
               						<nested:iterate property="displayList">
				  						<nested:notEqual property="added" value="">
			            					<font class="red1">
				  						</nested:notEqual>
				  						<nested:equal property="added" value="">
			          						</font>
			          					</nested:equal>
                                  		<nested:write property="status"/>&nbsp;
									</nested:iterate>
                        			</td>
                    			</tr>
                  			</table>
                		</td>
                		<td width="70%" class="tablelabel" valign="top">
                  			<table width="100%" border="0" cellspacing="1" cellpadding="2">
		                    <tr>
		                      <td class="tablelabel" valign="top">Delete</td>
		                      <td class="tablelabel" valign="top">Date</td>
		                      <td class="tablelabel" valign="top">Status</td>
		                      <td class="tablelabel" valign="top">Comment</td>
		                    </tr>
		               		<nested:iterate property="statusList"><% rows++; %>
		 						<nested:equal property="deleteFlag" value="on">
		 							<nested:hidden property="deleteFlag"/>
		 						</nested:equal>
		 						<nested:notEqual property="deleteFlag" value="on">
                    				<tr>
                        				<td class="tabletext">
                            				<div align="left"><nested:checkbox property="deleteFlag" styleId="checkbox2" /></div>
        								</td>
        								<td class="tabletext">
			    							<nested:equal property="added" value="Yes">
			    							
                              					<nobr>
                              					
             			 <nested:text  property="statusDate" size="10" styleClass="textbox" maxlength="10" onfocus="showCalendarControl(this);" onkeypress="return validateDate();"/>
             			 
<%--                               					<nested:text property="statusDate" size="12" styleClass="textbox" onblur="return checkDate(this);"/> --%>
<%-- <%--                         							<a href="javascript:show_calendar('statusForm.elements[5+(<%=rows%>*5)]');" --%> 
<!-- 														<a href="javascript:show_calendar('statusDate');" -->
<!-- 					onmouseover="window.status='Calendar';return true;" -->
<!-- 				        onmouseout="window.status='';return true;"> -->
<!--                         	<img src="../images/calendar.gif" width="16" height="15" border=0></a> -->
                        	</nobr>
			    </nested:equal>
			    <nested:notEqual property="added" value="Yes">
				<nested:write property="statusDate"/>
			    </nested:notEqual>
			</td>
                        <td class="tabletext"><div align="left">
			    <nested:equal property="added" value="Yes">
				    <nested:select property="planStatusId" styleClass="textbox" onchange="return checkStatus(this);">
				       <option value=""></option>
				       <html:options collection="statuses" property="planStatusId" labelProperty="status"/>
				    </nested:select><br>
				    <logic:equal name="statusForm" property="levelType" value="A">
				    <nobr><div align="left"><nested:checkbox property="updateSubProject" styleId="checkbox2"/>Update Sub-Project Status</nobr></div>
				    </logic:equal>
				    <nobr><div align="left"><nested:checkbox property="updateActivities" styleId="checkbox2"/>Update Activities Status</nobr></div>

			    </nested:equal>
			    <nested:notEqual property="added" value="Yes">
				<nested:write property="status"/>
			    </nested:notEqual>
                        </td>
                        <td class="tabletext">
                            <nested:textarea rows="3" cols="25" property="comment"/>
                        </td>
                    </tr>
		 </nested:notEqual>
               </nested:iterate>
                  </table>
                  </td>
                  </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
</table>
<html:hidden property="action"/>
<html:hidden property="lastStatusId"/>
</body>
</html:form>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>OBC : Environmental Review</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script src="../script/yCode_combo_box.js"> </script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form name="envReviewForm" type="elms.control.beans.planning.EnvReviewForm" action="/saveEnvReview.do">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Environmental Review List&nbsp;&nbsp;<font class="con_hdr_blue_3b">200 N SWALL<br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Environmental Review</td>

                      <td width="1%" class="tablelabel"><nobr>
                        <input type="button" name="reset" value="Reset" class="button" onclick="history.back();">
                        <input type="button" name="cancel" value="Cancel" class="button" onclick="history.back();">
                        <input type="button" name="save" value="Save" class="button" onclick="history.back();">
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Application Submittal Date</td>
                      <td class="tabletext" valign="top"><nobr>
                        <input type=text name="expirationDate" size=10 class="textbox" value="02/13/2003">
					    <a href="javascript:show_calendar('addActivity.expirationDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
                      </td>
                      <td class="tablelabel" valign="top">Determination Date</td>
                      <td class="tabletext" valign="top"><nobr>
                        <input type=text name="expirationDate" size=10 class="textbox" value="02/13/2003">
					    <a href="javascript:show_calendar('addActivity.expirationDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Required Determination Date</td>
                      <td class="tabletext" valign="top" colspan="3"><nobr>
                        <input type=text name="expirationDate" size=10 class="textbox" value="02/13/2003">
					    <a href="javascript:show_calendar('addActivity.expirationDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Environmental Review</td>

                      <td width="1%" class="tablelabel"><nobr>
                        <input type="button" name="add" value="Add" class="button">
                        <input type="button" name="add" value="Delete" class="button">
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Delete</td>
                      <td class="tablelabel" valign="top">Env. Determination</td>
                      <td class="tablelabel" valign="top">Action</td>
                      <td class="tablelabel" valign="top">Action Date</td>
                      <td class="tablelabel" valign="top">Comment</td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top" valign="top">
                      	<input type="checkbox" name="activeCheck"></td>

                      <td class="tabletext" valign="top">
                      	<select id="streetnameID" class="textbox">
                          	<option value="">Ministerial</option>
                          	<option value="">Statutory Exemption</option>
                          	<option value="">Categorical Exemption</option>
                          	<option value="">Negative Declaration</option>
                          	<option value="">Env. Impact Report</option>
                          	<option value="" selected>Previously Assessed</option>
                          	<option value="">Other</option>
						</select></td>
                      <td class="tabletext" valign="top">
                      	<select id="streetnameID" class="textbox">
                          	<option value="" selected>Checklist</option>
						</select></td>
                      <td class="tabletext" valign="top"><nobr>
                        <input type=text name="expirationDate" size=10 class="textbox" value="02/13/2003">
					    <a href="javascript:show_calendar('addActivity.expirationDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0></a></nobr>
                      </td>
                      <td class="tabletext" valign="top">
                      	  <textarea rows=3 cols=25 name="comments">This is test
						  </textarea>
					  </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
</table>
</table>
</html:form>
</body>
</html:html>
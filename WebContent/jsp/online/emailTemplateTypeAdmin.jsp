<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of San Fernando : Activity Information Management System : Email Template Type Administration</title>  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script src="../script/yCodelib.js"> </script> <script>checkBrowser();</script> <script src="../script/yCode_combo_box.js"> </script>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
%>
<script language="JavaScript">
var strValue;
function validateFunction() {
	strValue=false;
	if (document.forms['emailTemplateTypeAdminForm'].elements['emailTemplateType'].value == '') {
		alert("Enter email template type");
		document.forms['emailTemplateTypeAdminForm'].elements['emailTemplateType'].focus();
		strValue=false;
	}else if (document.forms['emailTemplateTypeAdminForm'].elements['emailTemplateDesc'].value == '') {
		alert("Enter email template type description");
		document.forms['conditionsLibraryForm'].elements['emailTemplateDesc'].focus();
		strValue=false;
	}else{
	   	document.forms[0].action='<%=contextRoot%>/emailTemplateTypeAdmin.do?action=save';
		document.forms[0].submit();
    }      
}
  
function lookup() {
       document.forms[0].action='<%=contextRoot%>/emailTemplateTypeAdmin.do?action=lookup';
       document.forms[0].submit();     	
}
function refresh() {
     document.forms[0].action='<%=contextRoot%>/emailTemplateTypeAdmin.do';
     document.forms[0].submit();
} 
function doDelete() {
     document.forms[0].action='<%=contextRoot%>/emailTemplateTypeAdmin.do?action=delete';
     document.forms[0].submit();
}         
</script>
<body bgcolor="#FFFFFF" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form name="emailTemplateTypeAdminForm" type="elms.control.beans.admin.EmailTemplateTypeAdminForm" action="/emailTemplateTypeAdmin">
<html:hidden property="emailTemplateTypeId"/>
<logic:notEqual value="YES" property="displayEmailTemplateType" name="emailTemplateTypeAdminForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Email Template Type Admin Maintenance<br>
                <br>
                </font></td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <html:errors property="error"/>
		                    <tr>
		                      <td width="99%" class="tabletitle">Lookup/Add/Update</td>
		                      <!-- <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td> -->
		                        <td width="1%" class="tablelabel"><nobr>
		                          <html:button property ="cancel" value="Cancel" styleClass="button" onclick="javascript:history.back()"/>
		                          <html:reset value="Reset" styleClass="button" onclick="return refresh()"/>
		                          <html:button property="Lookup" value="Lookup" styleClass="button" onclick="lookup()"></html:button>
		                          <html:button property="Save" value="Save" styleClass="button" onclick="return validateFunction();"></html:button>
		                          &nbsp;</nobr></td>
		                      </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" cellpadding="2" cellspacing="1">
							<tr>
								<td class="tablelabel"><nobr>Email Template Type</nobr></td>
		                        <td class="tabletext"> <html:text size="20" property="emailTemplateType" styleClass="textbox"/> &nbsp;</td>
		                    </tr>
		                    <tr>
								<td class="tablelabel"><nobr>Email Template Type Description</nobr></td>
		                        <td class="tabletext"> <html:text size="20" property="emailTemplateDesc" styleClass="textbox"/> &nbsp;</td>
		                    </tr>
                       	 </table>
                    	</td>
                  </tr>
                </table>
               </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</logic:notEqual>

<logic:equal value="YES" property="displayEmailTemplateType" name="emailTemplateTypeAdminForm">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top"> 
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td><font class="con_hdr_3b">Email Template Type List<br>
            <br>
            </font></td>
        </tr>
        <tr> 
          <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td> 
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> 
                      <td width="99%" height="25" class="tabletitle">Email Template Type Library</td>
                      <!-- <td width="1%"><img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td> -->
                      <td width="1%" class="tablelabel"><nobr> 
                       <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                       <html:button value="Delete" property="delete" styleClass="button" onclick="doDelete()"/>
                       <html:button value="Add" property="add" styleClass="button" onclick="refresh()"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr> 
                <td background="../images/site_bg_B7C1CB.jpg"> 
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr> 
                      <td width="20%" class="tablelabel">Delete</td>	
                      <td width="40%" class="tablelabel">Email Template Type Title</td>	                      
                      <td width="40%" class="tablelabel">Email Template Type Description</td>
                    </tr>
                    <logic:iterate id="emailTemplateTypeAdmin" property="emailTempTypeList" name="emailTemplateTypeAdminForm">
                      <tr bgcolor="#FFFFFF">
                        <td class="tabletext" align="right"><html:multibox property="selectedItems"><bean:write name="emailTemplateTypeAdmin" property="emailTemplateTypeId"/></html:multibox></td>                        
                        <td class="tabletext" >
                         
                          	<a href="<%=contextRoot%>/emailTemplateTypeAdmin.do?id=<bean:write name="emailTemplateTypeAdmin" property="emailTemplateTypeId"/>" >
                               <bean:write name="emailTemplateTypeAdmin" property="emailTemplateType" />
                            </a>
                          
                         </td>
                        <td class="tabletext"><bean:write name="emailTemplateTypeAdmin" property="emailTemplateDesc" /></td>
                      </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td height="32">&nbsp;</td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</logic:equal>

</html:form>
</body>
</html:html>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>



<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System: Limit Per day </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<logic:present name="enableAutoRefresh" scope="session">
	<META HTTP-EQUIV="refresh" CONTENT="5">
</logic:present>

<link rel="stylesheet" href="../../css/elms.css" type="text/css">
<SCRIPT language=Javascript>
      <!--
      function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
      //-->
</SCRIPT>

</head>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";


String currentSchedule = (String)request.getAttribute("currentSchedule");
if(currentSchedule==null) currentSchedule="";
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/limitPerDay?action=save" >

<font color='red'><b><%=message%></b>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Inspection Limit Per Day<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../../images/site_bg_B7C1CB.jpg">Please enter the limit below :
                        </td>
                      <td width="1%"><img src="../../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../../images/site_bg_D7DCE2.jpg"><nobr>
					  <html:submit value="Update" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tabletext">
                        <html:text  property="limitPerDay" size="25" styleClass="textbox" onkeypress="return isNumberKey(event)" />
                      </td>
                    </tr>
             </table>
                </td>
              </tr>
               <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%">
                        </td>
                      <td width="1%"></td>
                      <td width="1%" ><nobr>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
              	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../../images/site_bg_B7C1CB.jpg">Current Count:
                        </td>
                      <td width="1%"><img src="../../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../../images/site_bg_D7DCE2.jpg"><nobr>
					    &nbsp;</nobr></td>
                    </tr>
                  </table>

             <tr>
                <td background="../../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tabletext">
                        <html:text  property="currentLimit"  readonly ="true" style="font:  8pt/9pt verdana,arial,helvetica;width: 200 px; border : 0px;" size="25"  onkeypress="return isNumberKey(event)" />
                      </td>
                    </tr>
                  </table>
          </td>
        </tr>

                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
          </td>
        </tr>

      </table>

              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%">
                        </td>
                      <td width="1%"></td>
                      <td width="1%" ><nobr>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                    <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
            <TR>
          <TD><FONT class="blue2b">
          <logic:notPresent  name="enableAutoRefresh" >
         Manual (<a href="<%=request.getContextPath()%>/limitPerDay.do">REFRESH</a> this page) <br>
</logic:notPresent>
</td></tr>
 <TR>
          <TD height="5"><br>
</td></tr>
<tr><td><FONT class="blue2b">
		<logic:notPresent name="enableAutoRefresh">
	          (<a href="<%=request.getContextPath()%>/limitPerDay.do?action=true">Enable Auto Refresh</a>)
        </logic:notPresent>
		<logic:present name="enableAutoRefresh" >
    		 (This page will refresh every 5 seconds automatically)
        </logic:present>
          <BR>
        <td>  </tr>
          </table>
</table>

</html:form>
</body>
</html:html>

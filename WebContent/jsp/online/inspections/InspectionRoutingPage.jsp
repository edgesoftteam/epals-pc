<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html:html>
<%
String contextRoot = request.getContextPath();

String message = (String)request.getAttribute("message");
if(message==null) message="";

String error = (String)request.getAttribute("error");
if(error==null) error="";

String inspectionDate =(String)request.getAttribute("inspectionDate");
if(inspectionDate==null) inspectionDate="";

String inspectionDateCancel=(String)request.getAttribute("inspectionDateCancel");
if(inspectionDateCancel==null) inspectionDateCancel="";

java.util.List inspectionItemCodeList = (java.util.List) request.getAttribute("inspectionItemCodeList");
String  decisionButton = (String)request.getAttribute("decisionButton");
if(decisionButton == null) decisionButton= "";
%>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../../css/elms.css" type="text/css">
<app:checkLogon/>
<script language="JavaScript" src="../../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../../script/formValidations.js"></SCRIPT>
<script language="JavaScript" type="text/javascript">

function submitResults() {
  document.forms[0].action='<%=contextRoot%>/inspectionResults.do?action=view';
 document.forms[0].submit();
}


function  submitRequest(){
 document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=schedule";
 document.forms[0].scheduleInspection.disabled=true;
 document.forms[0].submit();
}

function  submitCancel(){
 document.forms[0].action="<%=contextRoot%>/inspectionResults.do?action=viewCancel";
 document.forms[0].submit();
}

function  submitGetCode(){
 document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=getCode";
 document.forms[0].submit();
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}
function keyCapt(e){
	if(typeof window.event!="undefined"){
		e=window.event;//code for IE
	}
	if(e.keyCode == 13){
		if("<%=decisionButton%>" == "fromRequest")
			document.forms[0].action="<%=contextRoot%>/inspectionRequest.do?action=schedule";
		else if("<%=decisionButton%>" == "fromResult")
			document.forms[0].action='<%=contextRoot%>/inspectionResults.do?action=view';
		else
			document.forms[0].action="<%=contextRoot%>/inspectionResults.do?action=viewCancel";
	}

}

</script>
</head>


<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/inspectionRequest.do">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td> <% String buff = ""+
     "          All inspections being cancelled after 7:00 a.m. on the day of the inspection shall be completed over the phone with a customer service representative at (818) 238-5220, or by contacting your inspector. ";%>
			          <font class="red2b"><%
			          if(message!=null && error!=null  && message.equals("") && error.equals("") && (inspectionDateCancel!=null && (!inspectionDateCancel.equalsIgnoreCase("")))) out.println(buff);
			          %>
          </td>
        </tr>
        <TR>
        <TD>
        <font class="green2b"><b><%=message%></b>
        </TD>
        </TR>
        <TR>
        <TD>
        <font class="red2b" ><b><%=error%></b>
        </TD>
        </TR>
  <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%"></td>
                     </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td>
                </td>
              </tr>
            </table>
          </td>
        </tr>
		<tr>
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
          <td>
            <table align="center" width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="80%" border="0" cellspacing="0" cellpadding="0">
                    <tr align="left">
                       <%
          if (decisionButton.equalsIgnoreCase("fromResult")) {
      %>
                      <td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle">Inspection Results</td>
                 <% } else  if (decisionButton.equalsIgnoreCase("fromRequest")) {
      %>
                <td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle" >Inspection Request</td>

             <% } else { %>
 	    	         <td width="100%" align="left" background="../../images/site_bg_B7C1CB.jpg" class="tabletitle">Inspection Cancel</td>
                <%}%>

                      <td width="1%" background="../../images/site_bg_D7DCE2.jpg"><nobr>
					  </td>
                    </tr>
                    <tr>
			<td>
			&nbsp;
			</td>
		</tr>
                  </table>
                </td>
              </tr>
                      <tr>
                <td width="80%" class="tabletext">
                  <table width="80%" border="0" cellspacing="1" cellpadding="2"  background="../../images/site_bg_B7C1CB.jpg">

    	        	<tr>
    	        	    <% if (decisionButton.equalsIgnoreCase("fromRequest")) {
		                 %>
    	               		<td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Permit Number : (Example BS0701234)</td>
    	                     <td class="tabletext"><html:text   styleClass="textbox" size="15"  maxlength="10"  property="permitNumber" onkeypress="keyCapt(event)" onblur="submitGetCode();"/></td>
    	                   <%} else {%>
    	                   <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Permit Number : (Example BS0701234)</td>
    	                     <td class="tabletext"><html:text   styleClass="textbox" size="15"  maxlength="10"  property="permitNumber" onkeypress="keyCapt(event)"/></td>
    	                       <%}%>
    	               </tr>
                      <% if (decisionButton.equalsIgnoreCase("fromRequest")) {%>

                       <tr>

		                <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Type :</td>
                         <td  width="70%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel"><html:select property="inspectionCode" styleClass="textbox">
				   <% if (inspectionItemCodeList!=null) {
		                 %>
                            <html:options collection="inspectionItemCodeList" property="code" labelProperty="description"/>
                      <%}%>
                        	</html:select>
                        	 </td>

                       </tr>
                         <% }%>

                      <tr>
                   <% if (decisionButton.equalsIgnoreCase("fromRequest")) {
                   %>
                          <td width="40%"  background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Date :</td>
                           <td style="padding:6px" class="tabletext"><%=inspectionDate%></td>

                    <% } else if(inspectionDateCancel!=null && (!inspectionDateCancel.equalsIgnoreCase(""))) {
                    %>
                           <td  width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Inspection Date :</td>
                           <td style="padding:6px" class="tabletext"><%=inspectionDateCancel%></td>


                    <% } else  {
                    %>
                          <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel"> Inspection Date :</td>
                          <td class="tabletext"><html:text readonly="true" size="10" styleClass="textbox" onkeypress="return validateDate();"  property="inspectionDate"/> <a href="javascript:show_calendar('inspectionRoutingForm.inspectionDate');"
                          onmouseover="window.status='Calendar';return true;"
                          onmouseout="window.status='';return true;">
                          <img src="../../images/calendar.gif" width="16" height="15" border="0"/></a></td>

                            <%}%>
                          </tr>
      <%
        if (decisionButton.equalsIgnoreCase("fromRequest")) {
      %>

       <tr>
        <td width="40%" background="../../images/site_bg_D7DCE2.jpg" class="tablelabel">Comments to the Inspector :</td>
        <td class="tabletext"><html:textarea property="comments" cols="40" rows="5"/></td>
       </tr>
      <%
        }
      %>

      <%
        if (decisionButton.equalsIgnoreCase("fromResult")) {
      %>
                      <tr>
                          <td align="right" style="padding:8px" colspan="2"  width="90%" background="../../images/site_bg_D7DCE2.jpg"  class="tablebutton"><html:button  styleClass="button" value="View Results" property="viewResults"  onclick="submitResults();"/></td>

                          </tr>

<%} else if (decisionButton.equalsIgnoreCase("fromRequest")){%>

            <tr>
                          <td  colspan="2" style="padding:6px" align="right" width="90%" background="../../images/site_bg_D7DCE2.jpg" class="tablebutton"><html:button   styleClass="button" value="Schedule Inspection Request" property="scheduleInspection" onclick="submitRequest();"/></td>

                          </tr>

                           <% } else {%>
                      <tr>
                          <td colspan="2" style="padding:6px" align="right" width="90%" background="../../images/site_bg_D7DCE2.jpg" class="tablebutton"><html:button  styleClass="button" value="Cancel Inspection" property="cancel"  onclick="submitCancel();"/></td>

                          </tr>
                           <%}%>

                  </table>
                </td>
              </tr>
          </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>



      </table>

</html:form>
</body>
</html:html>

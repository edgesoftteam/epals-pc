<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<html:html>
<app:checkLogon/>



<head>
<html:base/>
<title>City of Burbank :Permitting and Licensing System: Inspection Requests/Results</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/cutoffTimes?action=save">


<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td></td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b>
        </TD>
        </TR>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%"></td>
                         </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                </td>
              </tr>
            </table>
          </td>
        </tr>
		<tr>
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
          <td>
            <table width="100%" border="0" cellspacing="1" cellpadding="5">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../../images/site_bg_B7C1CB.jpg">Inspection Results
                        </td>
                      <td width="1%"><img src="../../images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
                      <td width="1%" background="../../images/site_bg_D7DCE2.jpg"><nobr>
					  </td>
                    </tr>
           <tr><td colspan="3"><br></td></tr>
                      <tr>
                <td background="../../images/site_bg_B7C1CB.jpg" colspan="3">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
 				 <logic:iterate id="inspection" name="inspection"  type="elms.app.inspection.Inspection"  scope="session">
                    <tr>
                      <td background="../../images/site_bg_D7DCE2.jpg">Permit Number</td>
					  <td class="tabletext" valign="top"><bean:write name="inspection" property="activityId"/></td>

                      <td background="../../images/site_bg_D7DCE2.jpg">Inspection Date</td>
                      <td class="tabletext"  valign="top"><bean:write name="inspection" property="date"/>

                      </td>
                      </tr>
                  <!--    <tr  background="#FFFFFF" class="tabletext" ><td class="tabletext"> <br></td><td class="tabletext">  <br></td><td class="tabletext"> <br></td><td class="tabletext">  <br></td>
                      </tr> -->
					<tr>
					  <td background="../../images/site_bg_D7DCE2.jpg">Inspection Type</td>
                      <td class="tabletext"  valign="top"><bean:write name="inspection" property="inspectionItem"/></td>

                      <td background="../../images/site_bg_D7DCE2.jpg">Inspection Status</td>
                      <td class="tabletext"  valign="top"><bean:write name="inspection" property="actionCodeDescription"/></td>
<!--                    </tr>
                      <tr  background="#FFFFFF" class="tabletext" ><td class="tabletext"> <br></td><td class="tabletext">  <br></td><td class="tabletext"> <br></td><td class="tabletext">  <br></td>
                      </tr> -->
                    <tr>
                      <td background="../../images/site_bg_D7DCE2.jpg">Comments</td>
                      <td class="tabletext" valign="top" colspan="3"><bean:write name="inspection" property="comments"/></td>
                    </tr>
                      <tr   background="../../images/site_bg_B7C1CB.jpg"><td height="50%" colspan="4"  background="../../images/site_bg_B7C1CB.jpg"><br></td>
                      </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
          </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>



      </table>

</html:form>
</body>
</html:html>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%
	int comboActSize = elms.util.StringUtils.s2i((String) request
			.getAttribute("comboSize"));
	int dotListSize = elms.util.StringUtils.s2i((String) request
			.getAttribute("dotListSize"));
	java.util.List dotList = new java.util.ArrayList();
	dotList = (java.util.List) request.getAttribute("dotList");
%>

<%@page import="elms.agent.LookupAgent"%><html:html>
<app:checkLogon />
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../../css/elms.css" type="text/css">
<link rel="stylesheet" href="../../css/online.css" type="text/css">
</head>
<%
	String contextRoot = request.getContextPath();
		String message = (String) request.getAttribute("message");
		if (message == null)
			message = "";
		String inActive = (String) request.getParameter("inActive");
%>

<style>

<style>
	.abutton {
	    background-color: #eeeeee;
		border: 1px solid #cccccc;
		font-family: Oswald, Arial, Helvetica;
		text-transform: uppercase;
		padding: 10px;
		padding-left: 10px;
		padding-right: 10px;
		margin: 10px;
		font-size: 12px;
		font-weight: bold;
		border-radius: 5px;
		color: #000000;
		cursor: pointer;
		text-decoration:none;
	}
	
	.abutton:hover {
		background-color: #669966;
		color: #ffffff;
	}
	
	table.csui, table.csui_title { width: 100%; padding: 0px; }
	table.csui { background-color: #cccccc; border-spacing: 1px; border-collapse: separate; }
	td.csui { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 12px; background-color: #ffffff; }

	table.csui_title { border-spacing: 0px; background-color: #777777; }
	td.csui_title { padding: 5px; width: 100%; font-family: Oswald, Arial, Helvetica, sans-serif; font-size: 18px; font-weight: 700; color: #ffffff; vertical-align: top; text-transform: uppercase }
	
	
	a.csui, a.csuisub { color: #000000; text-decoration: none }
	a.csui:hover, a.csuisub:hover { color: #336699; }
	td.csui_header, td.csui_label { padding: 6px; font-family: Roboto Condensed, Arial, Helvetica, sans-serif; font-size: 10px; background-color: #eeeeee; text-transform: uppercase }
	td.csui_label { width: 10%; white-space: nowrap; vertical-align: top; padding: 8px; padding-top: 15px }
	td.csui_input { padding: 6px; font-family: Armata, Arial, Helvetica, sans-serif; font-size: 12px; background-color: #ffffff; width: 40% }
	div.csui_divider { height: 25px; }

	a.csui_title { color: #ffffff; text-decoration: none }
	a.csuisub_title { color: #555555; text-decoration: none }
	a.csuisub_header_title { color: #888888; text-decoration: none }
	</style>
</style>
<SCRIPT language="JavaScript">
function switchMenu(menuSub)
{
	if(document.getElementById(menuSub).style.display == "none"){
		document.getElementById(menuSub).style.display = "block";
	}else{
		document.getElementById(menuSub).style.display = "none";
	}
}

function openStatusDetails()
{
	window.open("<%=contextRoot%>/jsp/online/statusDetails.jsp?contextHttpsRoot=<%=contextRoot%>",target="_new","toolbar=no,width=600,height=400,screenX=100,left=100,screenY=100,top=1,location=no,status=no,menubar=no,scrollbars=yes");
}

function menuDisplay(){
	for(var i=0;i<<%=comboActSize%>;i++){
		document.getElementById("menuSub"+i).style.display="";
	}
}

function lncvPrint(actId){
	document.forms[0].action = "<%=contextRoot%>/onlineLncv.do?action=onlinePrint&activityId="+actId;
	document.forms[0].submit();
}
</script>

<body class="csui"  leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="menuDisplay()">

<html:form action="/cutoffTimes?action=save">


	<table width="100%" border="0" cellspacing="10" cellpadding="0" >
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td></td>
				</tr>
				<TR>
					<TD><font color='green'><b><%=message%></b></font></TD>
				</TR>
				
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="2" class="csui">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" class="csui">
								<tr>
									<td width="100%" class="csui_title">My Permits</td>
								</tr>
								<tr>
									<td colspan="3"><br>
									</td>
								</tr>

								<%
							
									if (inActive.equalsIgnoreCase("yes")) {
											
								%>
								<tr >
									<td width="100%" class="csui">Below is a summary of your Active Permits.</td>
								</tr>
								<%
											} else {
								%>
								<tr >
									<td width="100%" class="csui">Below is a summary of your Closed Permits.</td>
								</tr>
								<%
									}
								%>
								<tr>
									<td colspan="3">&nbsp;<br>
									</td>
								</tr>
								<tr>
									<td background="../images/site_bg_B7C1CB.jpg" colspan="5"><!--Outer Combo Number Display--> <%
 	if (comboActSize > 0) {
 				int k = 0;
 %>
 
 
									<table width="100%" border="0" cellspacing="0" cellpadding="2" class="csui">
										<tr>
											<td class="csui_title">Address</td>

										</tr>
									</table>
									<logic:iterate id="combo" name="comboList" type="elms.control.beans.online.ApplyOnlinePermitForm" scope="request">

										<table width="100%" border="0" cellspacing="0" cellpadding="1" class="csui">
										
										<logic:notMatch name="combo" property="streetName" value="PW CITYWIDE">
											<tr bgcolor="FFFFFF">

												<td valign="top" class="csui" style="cursor: pointer; cursor: hand" onclick="switchMenu('menuSub<%=k%>')"><span id="menu2"><b><bean:write name="combo" property="streetName" /></b></span></td>

											</tr>
										</logic:notMatch>
											<tr>
												<td colspan="7" ><span id="menuSub<%=k%>">

												<table width="100%" border="0" cellspacing="0" cellpadding="2" class="csui">
													<tr>
														<td class="csui_header" width="2%">&nbsp;</td>
														<td class="csui_header" width="18%">Permit Number</td>
														<td class="csui_header" width="30%">Permit Type</td>
														<td class="csui_header" width="20%">Status </td>
														<td class="csui_header" width="12%">Amount </td>
														<td class="csui_header" width="8%">Print</td>
													</tr>
												</table>
												<table width="100%" border="0" cellspacing="0" cellpadding="2"  bgcolor="#b7c1cb">
												<!--Inner Activity List Display--> <%
 	int j = 0;
 %> <bean:define id="actid" name="combo" property="comboActList" type="java.util.Collection" /> <logic:iterate id="act" name="actid" type="elms.control.beans.online.MyPermitForm" indexId="i">
													

														<tr bgcolor="FFFFFF" id="<%=j%>">

															<td valign="top" width="2%" background="../images/join.gif"></td>
															<td class="csui" valign="top" width="18%"><a href='<%=contextRoot%>/myPermits.do?action=explore&viewPermit=Yes&activityId=<bean:write name="act" property="activityId" />'> <bean:write name="act" property="activityNo" /> </a></td>
															<td class="csui" valign="top" width="30%"><bean:write name="act" property="activityType" /></td>
															<td class="csui" valign="top" width="20%"><bean:write name="act" property="activityStatus" /></td>
															<td class="csui" valign="top" width="12%"><bean:write name="act" property="amount" /></td>
															
															
															<logic:equal name="act" property="print" value="Y">
																<logic:notEqual name="act" property="activityTypeCode" value="PKNCOM">
																
																	<td class="csui" align="left" width="8%"><a  target="_blank" href="<%=contextRoot%>/printPermit.do?actNbr=<bean:write name="act" property="activityNo"/>&actType=<bean:write name="act" property="activityTypeCode"/>"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="12" height="12" title="Print"></a></td>
																</logic:notEqual>
																<logic:equal name="act" property="activityTypeCode" value="PKNCOM">
																	<td class="csui" align="left" width="8%">
																	<a target="_blank" href="<%=LookupAgent.getLncvPermitURL() %><bean:write name="act" property="activityNo" />" class="tablebutton"><img src="<%=contextRoot%>/jsp/online/images/print.png" border=0 width="12" height="12" title="Print"></a>
																	</td>
																</logic:equal>
															</logic:equal>
															
															<logic:equal name="act" property="print" value="N">

																<td class="csui" align="left" width="8%"><a class="abutton" href="<%=contextRoot%>/processPayment.do?levelId=<bean:write name="act" property="activityId"/>&amount=<bean:write name="act" property="amount"/>&online=Y" >Pay</a></td>
															</logic:equal>


															
														</tr>
												
												</logic:iterate> </span></td>
											</tr>
											</table>
											<%
												k++;
											%>
										</table>
									</logic:iterate> 	<%
 	}
 %>
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>

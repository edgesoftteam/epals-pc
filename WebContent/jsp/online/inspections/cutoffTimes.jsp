<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>



<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../../css/elms.css" type="text/css">
<script language="JavaScript" src="../../script/calendar.js"></script>
<script language="JavaScript" src="../../script/formValidations.js"></script>

</head>
<%
String contextRoot = request.getContextPath();
String message = (String)request.getAttribute("message");
if(message==null) message="";
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/cutoffTimes?action=save">


<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Cutoff Times</font><br>
            <br>
            </td>
        </tr>
        <TR>
        <TD>
        <font color='green'><b><%=message%></b></font>
        </TD>
        </TR>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">No Schedule window
                        </td>
                      <td width="1%" class="tablebutton"><nobr>
					  <html:submit value="Update" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel" >Monday Start</td>
                      <td class="tabletext">
                        <html:text  property="scheduleMondayStart" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                      <td class="tablelabel" >Monday End</td>
                      <td class="tabletext">
                        <html:text  property="scheduleMondayEnd" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Tuesday Start</td>
                      <td class="tabletext">
                        <html:text  property="scheduleTuesdayStart" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                      <td class="tablelabel" >Tuesday End</td>
                      <td class="tabletext">
                        <html:text  property="scheduleTuesdayEnd" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Wednesday Start</td>
                      <td class="tabletext">
                        <html:text  property="scheduleWednesdayStart" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                      <td class="tablelabel" >Wednesday End</td>
                      <td class="tabletext">
                        <html:text  property="scheduleWednesdayEnd" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Thursday Start</td>
                      <td class="tabletext">
                        <html:text  property="scheduleThursdayStart" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                      <td class="tablelabel" >Thursday End</td>
                      <td class="tabletext">
                        <html:text  property="scheduleThursdayEnd" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Friday Start</td>
                      <td class="tabletext">
                        <html:text  property="scheduleFridayStart" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                      <td class="tablelabel" >Friday End</td>
                      <td class="tabletext">
                        <html:text  property="scheduleFridayEnd" size="25" styleClass="textbox" onblur="validateTime(this)"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>

		<tr>
			<td>
			&nbsp;
			</td>
		</tr>
		<tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Inspection Cutoff Times
                        </td>
                      <td width="1%" class="tablebutton"><nobr>
					  </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel" >Monday Schedule</td>
                      <td class="tabletext">
                        <html:text  property="cutoffMondaySchedule" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" >Monday Cancel</td>
                      <td class="tabletext">
                        <html:text  property="cutoffMondayCancel" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Tuesday Schedule</td>
                      <td class="tabletext">
                        <html:text  property="cutoffTuesdaySchedule" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" >Tuesday Cancel</td>
                      <td class="tabletext">
                        <html:text  property="cutoffTuesdayCancel" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Wednesday Schedule</td>
                      <td class="tabletext">
                        <html:text  property="cutoffWednesdaySchedule" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" >Wednesday Cancel</td>
                      <td class="tabletext">
                        <html:text  property="cutoffWednesdayCancel" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Thursday Schedule</td>
                      <td class="tabletext">
                        <html:text  property="cutoffThursdaySchedule" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" >Thursday Cancel</td>
                      <td class="tabletext">
                        <html:text  property="cutoffThursdayCancel" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" >Friday Schedule</td>
                      <td class="tabletext">
                        <html:text  property="cutoffFridaySchedule" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" >Friday Cancel</td>
                      <td class="tabletext">
                        <html:text  property="cutoffFridayCancel" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>



      </table>
    </td>
  </tr>
</table>

</html:form>
</body>
</html:html>

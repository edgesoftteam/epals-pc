<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.util.StringUtils"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
String contextRoot = request.getContextPath();
CachedRowSet crset = (CachedRowSet) request.getAttribute("holidayList");
if(crset !=null) crset.beforeFirst();
List<String> yearList = new ArrayList<String>();
yearList = (List)request.getAttribute("yearList");
System.out.println(yearList.toString());
String year=null;
year = (String)request.getAttribute("year");
String displayMsg="";
displayMsg = StringUtils.nullReplaceWithBlank((String)request.getAttribute("displayMsg"));

String occurance="";
occurance = StringUtils.nullReplaceWithBlank((String)request.getAttribute("occurance"));
System.out.println("occurance.."+occurance);
%>
<HEAD>
<html:base/>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" href="../../css/elms.css" type="text/css">
<script type="text/javascript">
/* 
   Willmaster Table Sort
   Version 1.1
   August 17, 2016
   Updated GetDateSortingKey() to correctly sort two-digit months and days numbers with leading 0.
   Version 1.0, July 3, 2011

   Will Bontrager
   https://www.willmaster.com/
   Copyright 2011,2016 Will Bontrager Software, LLC

   This software is provided "AS IS," without 
   any warranty of any kind, without even any 
   implied warranty such as merchantability 
   or fitness for a particular purpose.
   Will Bontrager Software, LLC grants 
   you a royalty free license to use or 
   modify this software provided this 
   notice appears on all copies. 
*/
//
// One placed to customize - The id value of the table tag.


//
//////////////////////////////////////
var TableLastSortedColumn = -1;

function SortTable() {
var TableIDvalue = "indextable";
var sortColumn = parseInt(arguments[0]);
var type = arguments.length > 1 ? arguments[1] : 'T';
var dateformat = arguments.length > 2 ? arguments[2] : '';
var table = document.getElementById(TableIDvalue);
var tbody = table.getElementsByTagName("tbody")[0];
var rows = tbody.getElementsByTagName("tr");
var arrayOfRows = new Array();
type = type.toUpperCase();
dateformat = dateformat.toLowerCase();
for(var i=0, len=rows.length; i<len; i++) {
	arrayOfRows[i] = new Object;
	arrayOfRows[i].oldIndex = i;
	var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g,"");
	if( type=='D' ) { arrayOfRows[i].value = GetDateSortingKey(dateformat,celltext); }
	else {
		var re = type=="N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g;
		arrayOfRows[i].value = celltext.replace(re,"").substr(0,25).toLowerCase();
		}
	}
if (sortColumn == TableLastSortedColumn) { arrayOfRows.reverse(); }
else {
	TableLastSortedColumn = sortColumn;
	switch(type) {
		case "N" : arrayOfRows.sort(CompareRowOfNumbers); break;
		case "D" : arrayOfRows.sort(CompareRowOfNumbers); break;
		default  : arrayOfRows.sort(CompareRowOfText);
		}
	}
var newTableBody = document.createElement("tbody");
for(var i=0, len=arrayOfRows.length; i<len; i++) {
	newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true));
	}
table.replaceChild(newTableBody,tbody);
} // function SortTable()

function SortTableDynamically() {
var TableIDvalue = "indextableFromAjax";
var sortColumn = parseInt(arguments[0]);
var type = arguments.length > 1 ? arguments[1] : 'T';
var dateformat = arguments.length > 2 ? arguments[2] : '';
var table = document.getElementById(TableIDvalue);
var tbody = table.getElementsByTagName("tbody")[0];
var rows = tbody.getElementsByTagName("tr");
var arrayOfRows = new Array();
type = type.toUpperCase();
dateformat = dateformat.toLowerCase();
for(var i=0, len=rows.length; i<len; i++) {
	arrayOfRows[i] = new Object;
	arrayOfRows[i].oldIndex = i;
	var celltext = rows[i].getElementsByTagName("td")[sortColumn].innerHTML.replace(/<[^>]*>/g,"");
	if( type=='D' ) { arrayOfRows[i].value = GetDateSortingKey(dateformat,celltext); }
	else {
		var re = type=="N" ? /[^\.\-\+\d]/g : /[^a-zA-Z0-9]/g;
		arrayOfRows[i].value = celltext.replace(re,"").substr(0,25).toLowerCase();
		}
	}
if (sortColumn == TableLastSortedColumn) { arrayOfRows.reverse(); }
else {
	TableLastSortedColumn = sortColumn;
	switch(type) {
		case "N" : arrayOfRows.sort(CompareRowOfNumbers); break;
		case "D" : arrayOfRows.sort(CompareRowOfNumbers); break;
		default  : arrayOfRows.sort(CompareRowOfText);
		}
	}
var newTableBody = document.createElement("tbody");
for(var i=0, len=arrayOfRows.length; i<len; i++) {
	newTableBody.appendChild(rows[arrayOfRows[i].oldIndex].cloneNode(true));
	}
table.replaceChild(newTableBody,tbody);
} // function SortTable()

function CompareRowOfText(a,b) {
var aval = a.value;
var bval = b.value;
return( aval == bval ? 0 : (aval > bval ? 1 : -1) );
} // function CompareRowOfText()

function CompareRowOfNumbers(a,b) {
var aval = /\d/.test(a.value) ? parseFloat(a.value) : 0;
var bval = /\d/.test(b.value) ? parseFloat(b.value) : 0;
return( aval == bval ? 0 : (aval > bval ? 1 : -1) );
} // function CompareRowOfNumbers()

function GetDateSortingKey(format,text) {
if( format.length < 1 ) { return ""; }
format = format.toLowerCase();
text = text.toLowerCase();
text = text.replace(/^[^a-z0-9]*/,"");
text = text.replace(/[^a-z0-9]*$/,"");
if( text.length < 1 ) { return ""; }
text = text.replace(/[^a-z0-9]+/g,",");
var date = text.split(",");
if( date.length < 3 ) { return ""; }
var d=0, m=0, y=0;
for( var i=0; i<3; i++ ) {
	var ts = format.substr(i,1);
	if( ts == "d" ) { d = date[i]; }
	else if( ts == "m" ) { m = date[i]; }
	else if( ts == "y" ) { y = date[i]; }
	}
d = d.replace(/^0/,"");
if( d < 10 ) { d = "0" + d; }
if( /[a-z]/.test(m) ) {
	m = m.substr(0,3);
	switch(m) {
		case "jan" : m = String(1); break;
		case "feb" : m = String(2); break;
		case "mar" : m = String(3); break;
		case "apr" : m = String(4); break;
		case "may" : m = String(5); break;
		case "jun" : m = String(6); break;
		case "jul" : m = String(7); break;
		case "aug" : m = String(8); break;
		case "sep" : m = String(9); break;
		case "oct" : m = String(10); break;
		case "nov" : m = String(11); break;
		case "dec" : m = String(12); break;
		default    : m = String(0);
		}
	}
m = m.replace(/^0/,"");
if( m < 10 ) { m = "0" + m; }
y = parseInt(y);
if( y < 100 ) { y = parseInt(y) + 2000; }
return "" + String(y) + "" + String(m) + "" + String(d) + "";
} // function GetDateSortingKey()
</script>





<script>
/*All the scripts should be here in one place and not scattered all over the file*/
var xmlhttp = false;
var date = "";
   try {
     xmlhttp = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         xmlhttp = false;
       }
     }
   }
   if (!xmlhttp)
     swal("Error initializing XMLHttpRequest!");
</script>

<SCRIPT>
function deleteHoliday(holidayId) {
   userInput = confirm("Are you sure you want to delete this entry?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/holidayEditor.do?action=delete&holidayId='+holidayId;
   }
}

function updateHoliday(holidayId) {
	document.location.href='<%=contextRoot%>/holidayEditor.do?action=update&holidayId='+holidayId;
}
function displayHolidays(e){
	var year = e.value;
	var url="<%=contextRoot%>/holidayEditor.do?action=ajaxList&year="+year;
    xmlhttp.open('POST', url,false);
    xmlhttp.onreadystatechange = HandleResponseDisplayHolidays;
    xmlhttp.send(null);	
}

function HandleResponseDisplayHolidays(){
	// To make sure valid response is received from the server, 200 means response received is OK
	if ((xmlhttp.readyState == 4) && (xmlhttp.status == 200)) {
		var result=xmlhttp.responseText;
		var text=result.split(',');
		if(result != null){
			document.getElementById("yearTr").style.display ='none';
			document.getElementById("selectDiv").innerHTML =result;
         	return true;
		}
	}else{
		swal("There was a problem retrieving data from the server." );
	}
}
function onLoad(){
	document.getElementById("yearTr").style.display ='block';
}


</SCRIPT>
<style>
th {
cursor:pointer;
}
</style>
</HEAD>
<html:html>
	<BODY onload="onLoad();">
		<html:form action="/holidayEditor?action=add">
			<TABLE width="55%" border="0" cellspacing="0" cellpadding="0" >
			  <TBODY>
			  	<TR><TD><FONT class="con_hdr_3b">Holiday List</FONT><BR><BR></TD></TR>
		 		<TR>
		   			<TD>
		           		<TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
		             		<TBODY>
		             		   <tr><td><font style="color:green;"><b><%=displayMsg %></b></font></td></tr>
				               <TR>
				                 <TD width="83%" class="tabletitle">Holidays</TD>
				                 <TD width="16%" class="tabletitle" style="text-transform: capitalize;">Year: 
				                 	<select name="yearList" id="yearList" onchange="return displayHolidays(this)">
				                  	<% for(int i=0;i<yearList.size();i++){  
				               					if(year != null && year.equalsIgnoreCase(yearList.get(i))){ %>
				               					<option value="<%=yearList.get(i) %>" selected="selected"><%=yearList.get(i) %></option>
				                 			<%}else{ %>
				                 			<option value="<%=yearList.get(i) %>"><%=yearList.get(i) %></option>
			                 		<%} } %>
			                		</select>
				                 </TD>
				                 <TD width="1%" class="tablebutton"><NOBR><html:submit value="Add" styleClass="button" /></NOBR></TD>
				             	</TR>
		            		</TBODY>
		          		</TABLE>
		      		</TD>
		  		</TR>
			 </TBODY>
			</TABLE>
			<div id="yearTr" style="display:none;">

				<TABLE id="indextable" background="../../images/site_bg_B7C1CB.jpg" width="55%" border="0" cellspacing="1" cellpadding="2">
				<thead>
					<tr>
					     <th class="tablelabel" width="25%" align="left"><a href="javascript:SortTable(0,'D','mdy');">Date</a></th>
						 <th class="tablelabel" width="65%" align="left"><a href="javascript:SortTable(1,'T');">Description</a></th>
						 <th class="tablelabel" width="15%" >Delete</th>
					</tr>
					</thead>
					<tbody>
					<% while (crset.next()) {
					  int id = crset.getInt("insp_cal_id");
				      String date = elms.util.StringUtils.date2str(crset.getDate("insp_cal_date"));
					  String description = crset.getString("description");
					%>
					<TR valign="top" >
						<TD class="tabletext" width="25%"><a href="javascript:updateHoliday(<%=id%>);"><%=date%></a></TD>
						<TD class="tabletext" width="65%"> <%= description%></TD>
						<td class="tabletext" width="15%" align="center"><a href="javascript:deleteHoliday(<%=id%>);"><img src="../../images/delete.gif" alt="Delete" border="0"></a></td>
					</TR>
					<% } if(crset!=null) crset.close(); %>
					</tbody>
				</TABLE>
			</div>
			<div id="selectDiv"></div>		
		</html:form>
	</BODY>
</html:html>

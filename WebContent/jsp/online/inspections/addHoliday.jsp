<%@page import="elms.util.StringUtils"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<html:html>
<app:checkLogon/>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%
String contextRoot = request.getContextPath();
%>
<link rel="stylesheet" href="../../css/elms.css" type="text/css">
<link rel="stylesheet" href="<%=contextRoot%>/jsp/css/kendo.default-v2.min.css"/>
<script language="JavaScript" src="../../script/formValidations.js"></script>
<script src="<%=contextRoot%>/tools/jquery/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="<%=contextRoot%>/jsp/script/kendo.all.min.js"></script>
<%
String message = (String)request.getAttribute("message");
if(message==null) message="";
String displayMsg="";
displayMsg = StringUtils.nullReplaceWithBlank((String)request.getAttribute("displayMsg"));
String action="";
action = StringUtils.nullReplaceWithBlank((String)request.getAttribute("action"));

%>
<script>
function isASCII(str) {	
	var val= document.forms[0].elements[str].value;
    return /^[\x00-\x7F]*$/.test(val);
}

function validateFunction()
{
	var strValue=false;
	strValue=validateData('req',document.forms[0].elements['date'],'Date is a required field');
	if (strValue == true)
	{
		strValue=validateData('req',document.forms[0].elements['description'],'Description is a required field');
	}
	if(!validateData('date',document.forms[0].elements['date'],'Invalid date format')){
		document.forms[0].elements['date'].focus();
		return false;
	}
	if(document.forms[0].elements['description'].value != ""){
		if(!isASCII('description')){
			document.forms[0].elements['description'].value="";
			document.forms[0].elements['description'].focus();
			swal("","Only ASCII characters allowed for description");
			return false;
		}
	}
	return strValue;
}
function goBack(){
	document.location.href='<%=contextRoot%>/holidayEditor.do';
	document.forms[0].submit;
}

$(document).ready(function() {
	$("#holidayDatepicker").kendoDatePicker({
	});
});
</script></head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/holidayEditor?action=save" onsubmit="return validateFunction()">
<font color='green'><b><%=message%></b></font>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr> <td><font class="con_hdr_3b">Add Holiday</font><br><br></td></tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr><td><font color='green'><b><%=displayMsg%></b></font></td></tr>
              	<tr>
                	<td>
                  		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    		<tr>
		                      <td width="99%" class="tabletitle">Please enter the holiday below :</td>
		                      <td width="1%" class="tablebutton"><nobr>
							  <html:button property="back" onclick="goBack()" value="Back" styleClass="button" />
							  <html:submit value=" Update " styleClass="button" />  &nbsp;</nobr></td>
                    		</tr>
                  		</table>
                	</td>
              	</tr>
              	<TR>
                	<TD background="../../images/site_bg_B7C1CB.jpg">
                  		<TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    		<TBODY>
                    			<TR>
	                      			<TD class="tablelabel">Date</TD>
	                      			<TD class="tablelabel">Description</TD>
                    			</TR>
			            	    <TR valign="top">
				 					<TD class="tabletext">
				 						<%if(action.equalsIgnoreCase("update")){ %>
				             			 <html:text  property="date" size="10" styleClass="textbox" style="height:25px;" readonly="true"/>
				             			<%} else { %>
				             			 <html:text property="date" size="10" styleId="holidayDatepicker" style="color:black;font-size:12px;" styleClass="textboxd" onkeypress="return event.keyCode!=13" />
				             			 <%} %> 
			             			</TD>		
			 	 					<TD class="tabletext">
				             			<html:text  property="description" size="100" maxlength="100" style="height:25px;" styleClass="textbox"/>
				             			<html:hidden  property="id" />			
			             			</TD>
								</TR>
                  			</TBODY>
                  		</TABLE>
                	</TD>
              	</TR>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>


</body>
</html:html>


<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.bl.*" %>
<%@ page import="elms.app.admin.*" %>
<%
	String contextRoot = request.getContextPath();
	List outOfTownAddressResults= (List) request.getAttribute("outOfTownAddressResults");
	pageContext.setAttribute("outOfTownAddressResults",outOfTownAddressResults);
%>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<script type="text/javascript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>


</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results
                   <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="100%" background="../images/site_bg_B7C1CB.jpg">Search Results (<%=outOfTownAddressResults.size()%> records found)</td>
                              <td width="1%" class="tablelabel">
                    		  <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                     		 </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <th class="tablelabel">
                                Business Name
                              </th>
                              <th class="tablelabel">
                                Business Address
                              </th>
                               <th class="tablelabel">
                                Business Owner Name
                              </th>
                              <th class="tablelabel">
                                Activity Type
                              </th>
							 <th class="tablelabel">
                                Activity Sub-Type
                              </th>
                              <th class="tablelabel">
                                Account Number
                              </th>
                              <th class="tablelabel">
                                Activity Status
                              </th>
							</tr>
<%
							session.setAttribute("onloadAction","noChangeContent");
%>

                        <logic:iterate name="outOfTownAddressResults" id="businessOwner" type="elms.app.bl.BusinessOwner">
                            <tr>
                             <td class="tabletext">
                                <a href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="businessOwner" property="activity_id" />&lsoId=<bean:write name="businessOwner" property="lso_id" />"><bean:write name="businessOwner" property="businessName"/></a>
                             </td>
                             <td class="tabletext">
                                <nobr><bean:write name="businessOwner" property="businessAddress"/>
                              </td>
                              <td class="tabletext">
                                <bean:write name="businessOwner" property="businessOwnerName"/>
                              </td>
								<td class="tabletext">
                                <bean:write name="businessOwner" property="activityType"/>
                              </td>
								<td class="tabletext">
                                <bean:write name="businessOwner" property="activitySubType"/>
                              </td>

                              <td class="tabletext">
                                <a href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="businessOwner" property="activity_id" />&lsoId=<bean:write name="businessOwner" property="lso_id" />"><bean:write name="businessOwner" property="businessAcNumber"/></a>
                              </td>
                              <td class="tabletext">
                                <bean:write name="businessOwner" property="activityStatus"/>
                              </td>

                         </logic:iterate>
						  </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>

</body>
</html:html>


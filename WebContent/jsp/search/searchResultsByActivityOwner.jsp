<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.project.*" %>
<%@ page import="elms.app.admin.*" %>
<%
	String contextRoot = request.getContextPath();
%>
<script>
 var siz = <bean:write name="basicSearchForm" property="activityListSize" />;
 if(siz == 1) {
 		document.location.href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="basicSearchForm" property="activityId" />&lsoId=<bean:write name="basicSearchForm" property="lsoId" />;"
 }
</script>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<script type="text/javascript">



if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}

</script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>


</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results <logic:equal name="basicSearchForm" property="activityListSize" value="0">( No Results Found. Please adjust your search criterion)</logic:equal>
                   <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="100%" background="../images/site_bg_B7C1CB.jpg">Activity (<bean:write name="basicSearchForm" property="activityListSize"/> records found)</td>
                              <td width="1%" class="tablelabel">
                    		  <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                     		 </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <th class="tablelabel">
                                Activity No.
                              </th>
                              <th class="tablelabel">
                                Type
                              </th>
                               <th class="tablelabel">
                                SubType
                              </th>
                              <th class="tablelabel">
                                Status
                              </th>

                              <th class="tablelabel">
                                Address
                              </th>
                              <th class="tablelabel">
                                Business Name
                              </th>
                              <th class="tablelabel">
                                Account Number
                              </th>
                              <th class="tablelabel">
                                Owner1
                              </th>
 								<th class="tablelabel">
                                Owner2
                              </th>
                            </tr>
<%
							session.setAttribute("onloadAction","noChangeContent");
%>

                		<logic:iterate name="actList" scope="request" id="activity" type="elms.app.project.Activity">
                            <tr>
                             <td class="tabletext">
                                <a href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="activity" property="activityId" />&lsoId=<bean:write name="activity" property="activityDetail.lsoId" />"><bean:write name="activity" property="activityDetail.activityNumber"/></a>
                             </td>
                              <td class="tabletext">
                                <nobr><bean:write name="activity" property="activityDetail.activityType.description"/></nobr>
                              </td>
                              <td class="tabletext"><nobr>
                                <% 	List multipleSubTypesList = activity.getActivityDetail().getActivitySubTypes();
                                	java.util.Iterator it = multipleSubTypesList.iterator();
                                 	while(it.hasNext()) {
                                		out.println((String)(it.next())+"<br/>"+"<dt>");
                                	}
								%>
                              </nobr>
                              </td>
                              <td class="tabletext">
                                <nobr><bean:write name="activity" property="activityDetail.status.description"/>
                              </td>

                               	<td class="tabletext" ><nobr><bean:write name="activity" property="activityDetail.address"/></td>

								<td class="tabletext" ><nobr><bean:write name="activity" property="businessLicenseActivity.businessName"/><font></td>
                               	<td class="tabletext" ><nobr><bean:write name="activity" property="businessLicenseActivity.businessAccountNumber"/></td>
                               	<td class="tabletext" ><nobr><bean:write name="activity" property="businessTaxActivity.name1"/></td>
									<td class="tabletext" ><nobr><bean:write name="activity" property="businessTaxActivity.name2"/></td>
                            </tr>
                         </logic:iterate>
						  </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>

</body>
</html:html>


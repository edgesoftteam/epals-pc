<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.project.*" %>
<%@ page import="elms.app.admin.*" %>
<%
	String contextRoot = request.getContextPath();
	String count = (String)request.getAttribute("count");
%>
<script>
 var siz = <bean:write name="basicSearchForm" property="activityListSize" />;
 if(siz == 1) {
 		document.location.href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="basicSearchForm" property="activityId" />&lsoId=<bean:write name="basicSearchForm" property="lsoId" />;"
 }
</script>
<html:html>
<head>
<html:base/>
<title>City of Beverly Hills : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results <logic:equal name="basicSearchForm" property="activityListSize" value="0">( No Results Found. Please adjust your search criterion)</logic:equal>

            <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="100%" class="tabletitle">Activity <font class="red2b">(<bean:write name="basicSearchForm" property="activityListSize" /> Records Found) </td>
                              <td width="1%" class="tablebutton">
                    		  <html:button  value="Cancel" property="button" styleClass="button" onclick="javascript:history.back()"/>
                     		 </td>

                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="td1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <th width="15%" class="tablelabel">
                                Act No
                              </th>
                              <th width="12%" class="tablelabel"">
                                Type
                              </th>
                              <th width="12%" class="tablelabel">
                                Status
                              </th>
                              <th width="22%" class="tablelabel">
                                Description
                              </th>
                              <th width="12%" class="tablelabel">
                                Applied
                              </th>
                              <th width="10%" class="tablelabel">
                                Issued
                              </th>
                              <th width="22%" class="tablelabel">
                                Address
                              </th>
                            </tr>
						<%
							session.setAttribute("onloadAction","noChangeContent");
						%>

						<logic:iterate name="actList" scope="request" id="activity" type="elms.app.project.Activity">
                            <tr>
                              <td class="tabletext" >
                                <a href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="activity" property="activityId" />&lsoId=<bean:write name="activity" property="activityDetail.lsoId" />"><bean:write name="activity" property="activityDetail.activityNumber"/></a>
                              </td>
                              <td class="tabletext">
                                <bean:write name="activity" property="activityDetail.activityType.description"/>
                              </td>
                              <td class="tabletext">
                                <bean:write name="activity" property="activityDetail.status.description"/>
                              </td>
                              <td class="tabletext">
                                <bean:write name="activity" property="activityDetail.description"/>
                              </td>
                              <td class="tabletext">
                              	<bean:write name="activity" property="activityDetail.appliedDateString"/>
                              </td>
                              <td class="tabletext">
                              	<bean:write name="activity" property="activityDetail.issueDateString"/>
                              </td>
                              <td class="tabletext">
                              	<bean:write name="activity" property="activityDetail.address"/>
                              </td>
							 </tr>

					</logic:iterate>
						  </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>

</body>
</html:html>


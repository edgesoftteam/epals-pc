<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%@ page import="java.util.*,elms.app.project.*" %>
<%
	String contextRoot = request.getContextPath();
	List projectResults = (List)request.getAttribute("projList");
	Iterator pri = projectResults.iterator();
	String count = (String)request.getAttribute("count");
%>
 <html:html>
<head>
<html:base/>
<title>City of Beverly Hills : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results</font>
<%
if(projectResults.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>

            <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="99%" class="tabletitle">Projects (<%=projectResults.size()%> Records )  <%=count%> Distinct Projects </td>
                     		  <td width="1%" class="tablebutton">
                    		  <html:button  value="Cancel" property="button" styleClass="button" onclick="javascript:history.back()"/>
                    		  </td>

                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="td1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <th class="tablelabel">
                                Project No.
                              </th>
                              <th class="tablelabel">
                                Address
                              </th>
                              <th class="tablelabel">
                                Name
                              </th>
                              <th class="tablelabel">
                                Description
                              </th>
                              <th class="tablelabel">
                                Status
                              </th>
                           </tr>
<%
Project project2 = new Project();
if(projectResults.size() == 1) {
	project2 = (Project)projectResults.get(0);
}
%>
<script>
var siz = <%=projectResults.size()%>;
if(siz == 1) {
	document.location.href="javascript:parent.f_content.location.href='<%=contextRoot%>/viewProject.do?projectId=' + <%=project2.getProjectId()%>; parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=' + <%=project2.getLso().getLsoId()%>;parent.f_psa.location.href='<%=contextRoot%>/viewPsaTreeWithLso.do?lsoId=' + <%=project2.getLso().getLsoId()%> + '&psaNodeId=' + <%=project2.getProjectId()%>;";
}
</script>
                          <% while(pri.hasNext()){
                          		elms.app.project.Project project = (elms.app.project.Project)pri.next();
                          		elms.app.project.ProjectDetail projectDetail = project.getProjectDetail();
                          		%>

                            <tr>
                              <td class="tabletext">
                                <a href="<%=contextRoot%>/viewPSA.do?levelType=P&levelId=<%=project.getProjectId()%>&lsoId=<%=project.getLso().getLsoId()%>"><%= project.getProjectDetail().getProjectNumber()%></a>
                              </td>
                              <td class="tabletext">
                                <%= projectDetail.getAddress()%>
                              </td>
                              <td class="tabletext">
                                <%= projectDetail.getName()%>
                              </td>
                              <td class="tabletext">
                                <%= projectDetail.getDescription()%>
                              </td>
                              <td class="tabletext">
                                <%= projectDetail.getProjectStatus().getDescription()%>
                              </td>
                            </tr>
                            <%
                            }
                            %>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>
</body>
</html:html>


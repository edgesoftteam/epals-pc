<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script type="text/javascript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>

</head>
<%! String lsoType = ""; %>
<%
	String contextRoot = request.getContextPath();
	java.util.List addressResults = (java.util.List) request.getAttribute("addressResults");
	java.util.Iterator iter = addressResults.iterator();
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Search Results
<%
if(addressResults.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>

			<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Search Results (<%=addressResults.size()%> records found)</td>
                      <td width="1%" class="tablelabel">
                      <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"><font class="con_hdr_1b">Address</td>
                    </tr>
<%
while (iter.hasNext()){
String full = (String)iter.next();
int index = full.indexOf("|");
int lastIndex = full.lastIndexOf("|");
String address = full.substring(0,index);
String lsoId = full.substring(index+1 ,lastIndex  );
lsoType = full.substring(lastIndex +1);
%>
<script>
var siz = <%=addressResults.size()%>;
if(siz == 1) {
<%if(lsoType.equalsIgnoreCase("L")){ %>
	document.location.href="javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=lsoId%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=lsoId%>';parent.f_content.location.href='<%=contextRoot%>/viewLand.do?lsoId=<%=lsoId%>';";
<%}else if(lsoType.equalsIgnoreCase("O")){%>
	document.location.href="javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=lsoId%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=lsoId%>';parent.f_content.location.href='<%=contextRoot%>/viewOccupancy.do?lsoId=<%=lsoId%>';";
<%}%>

}
</script>
<%if(lsoType.equalsIgnoreCase("L")){ %>
	<tr valign="top">
		<td class="tabletext"><a href=javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=lsoId%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=lsoId%>';parent.f_content.location.href='<%=contextRoot%>/viewLand.do?lsoId=<%=lsoId%>';><%=address%></a></td>
	</tr>
<%}else if(lsoType.equalsIgnoreCase("O")){%>
	<tr valign="top">
		<td class="tabletext"><a href=javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=lsoId%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=lsoId%>';parent.f_content.location.href='<%=contextRoot%>/viewOccupancy.do?lsoId=<%=lsoId%>';><%=address%></a></td>
	</tr>
<% }} %>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>


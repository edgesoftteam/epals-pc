<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search / Advance Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script>
function buildActivityQuery()
{
	var queryStr='';
	strValue = document.basicSearchForm.activityColumns.value;
	var myArray = (strValue).split(',');
	if (myArray[0] == 'i' )
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[1] + " " + document.basicSearchForm.activityOperators.value+  " " + document.basicSearchForm.activityValue.value + " ";
	}
	else if (myArray[0] == 'd' )
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[1] + " " + document.basicSearchForm.activityOperators.value+  " date('" + document.basicSearchForm.activityValue.value + "') ";
	}
	else
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[0] + " " + document.basicSearchForm.activityOperators.value+  " '" + document.basicSearchForm.activityValue.value + "' ";
	}
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function buildProjectQuery()
{
	var queryStr='';
	strValue = document.basicSearchForm.projectColumns.value;
	var myArray = (strValue).split(',');
	if (myArray[0] == 'i' )
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[1] + " " + document.basicSearchForm.projectOperators.value+  " " + document.basicSearchForm.projectValue.value + " ";
	}
	else if (myArray[0] == 'd' )
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[1] + " " + document.basicSearchForm.projectOperators.value+  " date('" + document.basicSearchForm.projectValue.value + "') ";
	}
	else
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[0] + " " + document.basicSearchForm.projectOperators.value+  " '" + document.basicSearchForm.projectValue.value + "' ";
	}
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function buildInspectionQuery()
{
	var queryStr='';
	strValue = document.basicSearchForm.inspectionColumns.value;
	var myArray = (strValue).split(',');
	if (myArray[0] == 'i' )
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[1] + " " + document.basicSearchForm.inspectionOperators.value+  " " + document.basicSearchForm.inspectionValue.value + " ";
	}
	else if (myArray[0] == 'd' )
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[1] + " " + document.basicSearchForm.inspectionOperators.value+  " date('" + document.basicSearchForm.inspectionValue.value + "') ";
	}
	else
	{
	queryStr = document.basicSearchForm.query.value + " " + myArray[0] + " " + document.basicSearchForm.inspectionOperators.value+  " '" + document.basicSearchForm.inspectionValue.value + "' ";
	}
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function buildCond1()
{
	var queryStr='';
	queryStr = document.basicSearchForm.query.value + " and ";
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function buildCond2()
{
	var queryStr='';
	queryStr = document.basicSearchForm.query.value + " or ";
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function buildCond3()
{
	var queryStr='';
	queryStr = document.basicSearchForm.query.value + " not ";
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function buildCond4()
{
	var queryStr='';
	queryStr = document.basicSearchForm.query.value + " ( ) ";
	document.basicSearchForm.query.value='';
	document.basicSearchForm.query.value=queryStr;
}
function dspTable(intValue) {
		if (intValue == -1)
		{
				activityTable.style.display="none";
				projectTable.style.display="none";
				inspectionTable.style.display="none";
				document.basicSearchForm.query.value='';
				document.basicSearchForm.activityValue.value='';
				document.basicSearchForm.elements['table'].value=-1;
		}
		if (intValue == 1)
		{
				activityTable.style.display="";
				projectTable.style.display="none";
				inspectionTable.style.display="none";
				document.basicSearchForm.query.value=' select * from activity where ';
				document.basicSearchForm.activityValue.value='';

		}
		else if (intValue == 2)
		{
				activityTable.style.display="none";
				projectTable.style.display="";
				inspectionTable.style.display="none";
				document.basicSearchForm.query.value=' select * from project where ';
				document.basicSearchForm.projectValue.value='';
		}
		else if (intValue == 3)
		{
				activityTable.style.display="none";
				projectTable.style.display="none";
				inspectionTable.style.display="";
				document.basicSearchForm.query.value=' select * from inspection where ';
				document.basicSearchForm.inspectionValue.value='';
		}
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="dspTable(-1);">
<html:form name="basicSearchForm" type="elms.control.beans.basicSearchForm" action="/viewAdvancedSearchResults">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Advanced Search<br>
              <br>
              </td>
          </tr>
          <logic:present name="message" scope="request" >
          <tr>
          	<td><font color="red" ><b><%=(String)request.getAttribute("message")%></b></td>
          </tr>
          </logic:present>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="99%" background="../images/site_bg_B7C1CB.jpg">Advanced
                          Search</td>
                        <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="17" height="16"></td>
                        <td width="1%" class="tablelabel"><nobr><html:link href="viewBasicSearch.jsp" styleClass="hdrs"><font class="con_hdr_link">Basic
                          Search<img src="../images/site_blt_x_000000.gif" width="21" height="9" border="0"></html:link></nobr></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" cellpadding="2" cellspacing="1">
                      <tr>
                        <td class="tablelabel" width="20%">Table</td>
                        <td class="tabletext">
                          <html:select property="table" size="1" styleClass="textbox" onchange="dspTable(this.options[this.selectedIndex].value);">
                            <html:option value="-1">Please Select</html:option>
                            <html:option value="1">Activities</html:option>
                            <html:option value="2">Projects</html:option>
                            <html:option value="3">Inspections</html:option>
                          </html:select>
                        </td>
                        <td class="tabletext" align="right"><font class="con_hdr_link">&nbsp;</td>
                      </tr>
                    </table>
				  </td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td background="../images/site_bg_B7C1CB.jpg">
						<table id="activityTable" width="100%" cellpadding="2" cellspacing="1">
						<tr>
							<td class="tablelabel">Columns</td>
							<td class="tablelabel">Operators</td>
							<td class="tablelabel" colspan="2">Value</td>
						</tr>
                      	<tr class="tabletext" align="center">
                        <td align="left">
                          <html:select property="activityColumns" size="10" styleClass="textbox">
						  	<html:option value="i,act_id">Activity ID</html:option>
						  	<html:option value="i,proj_id">Project ID</html:option>
						  	<html:option value="act_nbr">Activity No</html:option>
						  	<html:option value="act_type">Activity Type</html:option>
							<html:option value="desc">Description</html:option>
							<html:option value="i,valuation">Valuation</html:option>
						  	<html:option value="plan_chk_req">Plan Check Req</html:option>
							<html:option value="i,status">Status</html:option>
							<html:option value="d,start_date">Start Date</html:option>
							<html:option value="d,applied_date">Applied Date</html:option>
							<html:option value="d,issued_date">Issued Date</html:option>
							<html:option value="d,exp_date">Exp Date</html:option>
							<html:option value="d,final_date">Final Date</html:option>
							<html:option value="d,plan_chk_fee_date">Plan Check Fee Date</html:option>
							<html:option value="d,permit_fee_date">Activity Fee Date</html:option>
						  	<html:option value="microfilm">Microfilm</html:option>
							<!--<html:option value="i,created_by">Created By</html:option>-->
							<html:option value="d,date(created)">Created</html:option>
							<!--<html:option value="i,updated_by">Updated By</html:option>-->
							<html:option value="d,(updated)">Updated</html:option>
                          </html:select>
						</td>
                        <td align="left">
                          <html:select property="activityOperators" size="10" styleClass="textbox">
						  	<html:option value="">Please select</html:option>
						  	<%--
							<html:option value="=">=</html:option>
						  	<html:option value=">">></html:option>
						  	<html:option value="<"><</html:option>
						  	<html:option value="<="><=</html:option>
						  	<html:option value=">=">>=</html:option>
						  	<html:option value="<>"><></html:option>
						  	<html:option value="like">like</html:option>
							--%>
                          </html:select>
						</td>
                        <td align="left">
							<html:text  property="activityValue" size="25" styleClass="textbox"/>
						</td>
                        <td align="left">
							<html:button  property="add" value="Add" styleClass="button" onclick="buildActivityQuery();document.basicSearchForm.activityValue.value='';"/>
						</td>
                        </table>
						<table id="projectTable" width="100%" cellpadding="2" cellspacing="1">
						<tr>
							<td class="tablelabel">Columns</td>
							<td class="tablelabel">Operators</td>
							<td class="tablelabel" colspan="2">Value</td>
						</tr>
                      	<tr class="tabletext" align="center">
                        <td align="left">
                          <html:select property="projectColumns" size="10" styleClass="textbox">
						  	<html:option value="i,proj_id">Project ID</html:option>
						  	<html:option value="i,lso_id">LSO ID</html:option>
						  	<html:option value="project_nbr">Project No</html:option>
						  	<html:option value="name">Name</html:option>
						  	<html:option value="desc">Description</html:option>
						  	<html:option value="i,status_id">Status ID</html:option>
						  	<html:option value="d,start_dt">Start Date</html:option>
						  	<html:option value="d,completion_dt">Completion Date</html:option>
						  	<html:option value="d,applied_dt">Applied Date</html:option>
						  	<html:option value="d,expired_dt">Expired Date</html:option>
						  	<html:option value="i,lso_use_id">LSO Use ID</html:option>
							<!--<html:option value="i,updated_by">Updated By</html:option>-->
							<html:option value="d,date(updated_dt)">Updated</html:option>
                          </html:select>
						</td>
                        <td align="left">
                          <html:select property="projectOperators" size="10" styleClass="textbox">
						  	<html:option value="">Please select</html:option>
						  	<%--
							<html:option value="=">=</html:option>
						  	<html:option value=">">></html:option>
						  	<html:option value="<"><</html:option>
						  	<html:option value="<="><=</html:option>
						  	<html:option value=">=">>=</html:option>
						  	<html:option value="<>"><></html:option>
						  	<html:option value="like">like</html:option>
							--%>
                          </html:select>
						</td>
                        <td align="left">
							<html:text  property="projectValue" size="25" styleClass="textbox"/>
						</td>
                        <td align="left">
							<html:button  property="add" value="Add" styleClass="button" onclick="buildProjectQuery();document.basicSearchForm.projectValue.value='';"/>
						</td>
                        </table>
     					<table id="inspectionTable" width="100%" cellpadding="2" cellspacing="1">
						<tr>
							<td class="tablelabel">Columns</td>
							<td class="tablelabel">Operators</td>
							<td class="tablelabel" colspan="2">Value</td>
						</tr>
                      	<tr class="tabletext" align="center">
                        <td align="left">
                          <html:select property="inspectionColumns" size="10" styleClass="textbox">
						    <html:option value="i,inspection_id">Inspection ID</html:option>
							<html:option value="i,act_id">Activity ID</html:option>
							<html:option value="i,inspector_id">Inspector ID</html:option>
						  	<html:option value="i,inspection_item_id">Inspection Item ID</html:option>
							<html:option value="d,inspection_dt">Inspection Date</html:option>
							<html:option value="i,actn_code">Action Code</html:option>
							<html:option value="i,route_seq">Route Seq</html:option>
							<html:option value="d,date(timestamp)">Timestamp</html:option>
							<html:option value="d,fax_sent">Fax Sent</html:option>
							<html:option value="request_source">Request Source</html:option>
							<html:option value="comments">Comments</html:option>
                          </html:select>
						</td>
                        <td align="left">
                          <html:select property="inspectionOperators" size="10" styleClass="textbox">
						  	<html:option value="">Please select</html:option>
						  	<%--
							<html:option value="=">=</html:option>
						  	<html:option value=">">></html:option>
						  	<html:option value="<"><</html:option>
						  	<html:option value="<="><=</html:option>
						  	<html:option value=">=">>=</html:option>
						  	<html:option value="<>"><></html:option>
						  	<html:option value="like">like</html:option>
							--%>
                          </html:select>
						</td>
                        <td align="left">
							<html:text  property="inspectionValue" size="25" styleClass="textbox"/>
						</td>
                        <td align="left">
							<html:button  property="add" value="Add" styleClass="button" onclick="buildInspectionQuery();document.basicSearchForm.inspectionValue.value='';"/>
						</td>
                        </table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" cellpadding="2" cellspacing="1">
                    <tr class="tabletext" align="center">
                      <td colspan="3">
                        <html:button  value="and" property="and" styleClass="button" onclick="buildCond1()"/>
                        &nbsp;
                        <html:button  value="or" property="or" styleClass="button" onclick="buildCond2()"/>
                        &nbsp;
                        <html:button  value="not" property="not" styleClass="button" onclick="buildCond3()"/>
                        &nbsp;
                        <html:button  value="( )" property="brackets" styleClass="button" onclick="buildCond4()"/>
                      </td>
                    </tr>
                      <tr>
                        <td class="tablelabel" width="20%">Query</td>
                        <td class="tabletext" colspan="2" >
                          <html:textarea property="query" cols="100" rows="5" styleClass="textbox"></html:textarea>
                        </td>
                      </tr>
                      <tr class="tabletext">
                        <td colspan="3" align="CENTER">
                          <html:submit  value="Search" property="search" styleClass="button"/>
                          &nbsp;
                          <html:reset value="Reset" styleClass="button" onclick="dspTable(-1);"/>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="32">&nbsp;</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html:html>

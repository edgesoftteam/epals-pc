<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%@ page import="java.util.*, java.sql.*" %>
<%@ page import="elms.app.inspection.*, elms.util.db.Wrapper" %>
<%
	String contextRoot = request.getContextPath();
	List inspList = (List)request.getAttribute("inspList");
	Iterator inspi = inspList.iterator();
%>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script type="text/javascript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results
<%
if(inspList.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>

<%
String lsoFromActivity = "0";
String inspectionId = "0";
Inspection inspection2 = new Inspection();
Wrapper db;
ResultSet rs;
if(inspList.size() == 1) {
	inspection2 = (Inspection)inspList.get(0);
	inspectionId = inspection2.getInspectionId();
	db = new Wrapper();
	rs = db.select("SELECT LSO_ID FROM PROJECT WHERE PROJ_ID = (SELECT PROJ_ID FROM SUB_PROJECT WHERE SPROJ_ID = (SELECT SPROJ_ID FROM ACTIVITY WHERE ACT_ID = )))" + inspection2.getActivityId());
	if(rs.next()) {
		lsoFromActivity = rs.getString(1);
	}
}
%>
<script>
var siz = <%=inspList.size()%>;
if(siz == 1) {
	document.location.href="javascript:parent.f_content.location.href='<%=contextRoot%>/editInspection.do?inspectId=' + <%=inspectionId%>; parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=' + <%=lsoFromActivity%>;parent.f_psa.location.href='<%=contextRoot%>/viewPsaTreeWithLso.do?lsoId=' + <%=lsoFromActivity%>;";
}
</script>

            <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="99%" background="../images/site_bg_B7C1CB.jpg">Inspection (<%=inspList.size()%> records found)</td>
                    		  <td width="1%" class="tablelabel">
                    		  <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                    		  </td>

                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <td class="tablelabel">
                                Inspection No.
                              </td>
                              <td class="tablelabel">
                                Activity No.
                              </td>
                              <td class="tablelabel">
                                Inspection Date
                              </td>
                              <td class="tablelabel">
                                Source
                              </td>
                              <td class="tablelabel">
                                Action Code
                              </td>
                            </tr>
<%
							while (inspi.hasNext()) {
							Inspection inspection = (Inspection)inspi.next();
%>
                            <tr>
                              <td class="tabletext">
                                <a href="<%=contextRoot%>/editInspection.do?inspectId=<%=inspection.getInspectionId()%>"><%= inspection.getInspectionId() %></a>
                              </td>
                              <td class="tabletext">
                                <%= inspection.getActivityId() %>
                              </td>
                              <td class="tabletext">
                                <%= inspection.getDate() %>
                              </td>
                              <td class="tabletext">
                                <%= inspection.getSource() %>
                              </td>
                              <td class="tabletext">
                                <%= inspection.getActnCode() %>
                              </td>
                            </tr>
<%
							}
%>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>

</body>
</html:html>


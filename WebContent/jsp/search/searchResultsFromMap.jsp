<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.lso.*" %>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<% String contextRoot = request.getContextPath(); %>
<script>
					function getMap() {
                          msgWindow=open('','mapwindow','resizable=yes,status=yes, width=850,height=650,screenX=400,screenY=0,top=0,left=0,scrollbars');
                          msgWindow.location.href = '<%=contextRoot%>/viewMap.do';
                          msgWindow.focus();
                          //if (msgWindow.opener == null) msgWindow.opener = self;
					}
 </script>
<script type="text/javascript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>
<%
	java.util.List ownerResults = (java.util.List) request.getAttribute("ownerResults");
	java.util.Iterator iter = ownerResults.iterator();
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload=""javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=-1';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=-1';">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Search Results -- <%=ownerResults.size()%>
<%
if(ownerResults.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>

			<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Search Results</td>
                      <td width="1%" class="tablelabel">
                      <html:button  value="Back" property="button" styleClass="button" onclick="javascript:getMap();"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
					  <td class="tablelabel">Address</td>
					  <td class="tablelabel">Apn</td>
                      <td class="tablelabel">Owner Name</td>
                    </tr>
<%
OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
if(ownerResults.size() == 1) {
	ownerApnAddress = (OwnerApnAddress)ownerResults.get(0);
}
%>
<script>
var siz = <%=ownerResults.size()%>;
if(siz == 1) {
	document.location.href="javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_content.location.href='<%=contextRoot%>/viewLand.do?lsoId=<%=ownerApnAddress.getLsoId()%>';";
}
</script>
<%
while (iter.hasNext()){
//OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
ownerApnAddress = (OwnerApnAddress)ownerResults.get(0);
ownerApnAddress = (OwnerApnAddress)iter.next();
%>

                    <tr valign="top">
                         <td class="tabletext"><a href=javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_content.location.href='<%=contextRoot%>/viewLand.do?lsoId=<%=ownerApnAddress.getLsoId()%>'><%=ownerApnAddress.getAddress()%></a></td>
                         <td class="tabletext"><%=ownerApnAddress.getApn()%></td>
						 <td class="tabletext"><%=ownerApnAddress.getOwnerName()%></td>
                    </tr>
<%
}
%>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>


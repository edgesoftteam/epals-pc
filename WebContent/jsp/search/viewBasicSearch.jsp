<%@ page import='elms.agent.*,java.util.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>

<app:checkLogon />

<html:html>
<%
String contextRoot = request.getContextPath();
%>

<%!
List streets = new ArrayList(); // for the Street list
%>
<%
 streets = (List) AddressAgent.getStreetArrayList();
 request.setAttribute("streets",streets);
%>
<head>
<html:base />

<title>City of Burbank : Permitting And Licensing System : Search / Advanced Search / Document Search</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="javascript" type="text/javascript" src="../script/actb.js"></script>
<script language="javascript" type="text/javascript" src="../script/common.js"></script>

<script language="JavaScript">

function fromCrossStreet()
{
    document.forms[0].action='<%=contextRoot%>/viewBasicSearch.do?from=crossStreet';
    document.forms[0].elements['crossStreetName2'].disabled=true;
    document.forms[0].submit();
    return true;
}

function searchValidate() {
	var i=0;

	var streetText=document.forms[0].streetNameText.value;
		for(var q=0;q<<%=streets.size()%>;q++){
			if((streetText.toUpperCase())==((document.forms[0].streetName[q].text).toUpperCase())){
				document.forms[0].streetName[q].selected=true;
			}
		}

	if(document.forms['basicSearchForm'].subProjectNumber.value!="" && document.forms['basicSearchForm'].subProjectNumber.value.length<9){
		alert("Please enter 9 digits for SubProject No");
		document.forms['basicSearchForm'].subProjectNumber.focus();
		i++;
		return false;
	}else if(document.forms['basicSearchForm'].ownerName.value!="" && document.forms['basicSearchForm'].ownerName.value.length<3){
		alert("Please enter a minimum of 3 Characters for the Address Owner Name");
		document.forms['basicSearchForm'].ownerName.focus();
		i++;
		return false;
	}else if(document.forms['basicSearchForm'].activityNumber.value!="" && document.forms['basicSearchForm'].activityNumber.value.length<9){
		alert("Please enter a minimum of 9 Characters for the Activity Number");
		document.forms['basicSearchForm'].activityNumber.focus();
		i++;
		return false;
	}else if((document.forms['basicSearchForm'].streetName.value!="-1") && (document.forms['basicSearchForm'].streetName2.value!="-1")){
		alert("Either Street Name or Multiple Streets dropdown can be selected");
		document.forms['basicSearchForm'].streetName.focus();
		i++;
		return false;
	}
	if (i>0 ) {
		document.forms['basicSearchForm'].action="";
	}
	var b=0;
}

function dohref() {
  parent.f_content.location.href='<%=contextRoot%>/viewAdvancedSearch.do';
}


function doDocumentHref() {
  parent.f_content.location.href='<%=contextRoot%>/viewDocumentSearch.do';
}

function evalType() {

    if(document.forms[0].elements['activityType'].value=="dummy"){
			alert("Choose the proper option");
		document.forms[0].elements['activityType'].focus();
		document.forms[0].elements['selectedActType'].value = "";
		return false;
	}else{
	   	var size= document.forms[0].elements['activityType'].length;
		var str = "";
		var delimiter = "";
		for (var i=0; i<size;i++) {
			if (document.forms[0].elements['activityType'].options[i].selected) {
				str += delimiter + document.forms[0].elements['activityType'].options[i].text;
				delimiter = "\r";
			}
		}
	document.forms[0].elements['selectedActType'].value=str;
	}
}

function evalActStatus() {
	if(document.forms[0].elements['activityStatus'].value==0){
		alert("Choose the proper option");
		document.forms[0].elements['activityStatus'].focus();
		document.forms[0].elements['selectedActStatus'].value = "";
		return false;
	}else{
		var size= document.forms[0].elements['activityStatus'].length;
		var str = "";
		var delimiter = "";
		for (var i=0; i<size;i++) {
			if (document.forms[0].elements['activityStatus'].options[i].selected) {
				str += delimiter + document.forms[0].elements['activityStatus'].options[i].text;
				delimiter = "\r";
			}
		}
		document.forms[0].elements['selectedActStatus'].value=str;
	}
}


function evalSubType() {
	var size= document.forms[0].elements['activitySubType'].length;
	var str = "";
	var delimiter = "";
	for (var i=0; i<size;i++) {
		if (document.forms[0].elements['activitySubType'].options[i].selected) {
			str += delimiter + document.forms[0].elements['activitySubType'].options[i].text;
			delimiter = "\r";
		}
	}
document.forms[0].elements['selectedActSubType'].value=str;
}

function disEvalType(){
	document.forms[0].elements['selectedActType'].value="";
}

function disEvalSubType(){
	document.forms[0].elements['selectedActSubType'].value="";
}
function disEvalActStatus(){
	document.forms[0].elements['selectedActStatus'].value="";
}

function setAddressFocus(){
	parent.f_content.document.forms['basicSearchForm'].streetNumber.focus();
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}
</script>
<script>

var streetNameArray = new Array();

<%
if(streets != null && streets.size() > 0){
	for(int i=0; i<streets.size();i++){
		elms.app.lso.Street problemStreet = (elms.app.lso.Street)streets.get(i);
%>
	streetNameArray[<%=i %>] = '<%=problemStreet.getStreetName()%>';
<%}}%>
</script>

</head>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0">
<html:form action="/viewSearchResults">

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<tr valign="top">
			<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Search<br>
					<br>
					</td>
				</tr>
				<% if(request.getAttribute("result") != null && request.getAttribute("result").toString().equalsIgnoreCase("NoResult")){%>
				<tr>
					<td>
					<font class="red1">Please enter some search criteria and try again.<br><br>
					</td>
				</tr>
				<%}%>
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="99%" class="tabletitle" align="left">Search</td>
									<td width="1%" class="tablebutton" align="right"><nobr>
                                   		<html:button value="Document Search" property="document search" styleClass="button" onclick="javascript:doDocumentHref();" />
										<html:reset value="Reset" styleClass="button" />
										<html:submit property="search" value="Search" styleClass="button" onclick="return searchValidate();"/></nobr>
									</td>
								</tr>
							</table>
							</td>
						</tr>

						<tr>
							<td background="../images/site_bg_B7C1CB.jpg">
							<table width="100%" cellpadding="1" cellspacing="1">
								<tr>
									<td class="tablelabel" width="10%">Address</td>
									<td class="tabletext" WIDTH="20%"><html:text size="10"
										property="streetNumber" styleClass="textbox" /> <html:select
										property="streetFraction" styleClass="textbox">
										<html:option value="">
										</html:option>
										<html:option value="1/4">1/4</html:option>
										<html:option value="1/3">1/3</html:option>
										<html:option value="1/2">1/2</html:option>
										<html:option value="3/4">3/4</html:option>
									</html:select></td>
									<td class="tablelabel" width="20%">Single Street </td>
										<td class="tabletext" valign="top" colspan="2">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td id=tdid><div style="visibility:hidden"><html:select property="streetName" styleClass="textbox" style="position:absolute">
												<html:option value="-1">Please Select</html:option>
												<html:options collection="streets" property="streetId" labelProperty="streetName" />
											</html:select></div></td>

											<td colspan="1" class="tabletext">
												<input type="text" name="streetNameText" style="" id="sn" size="18%"  />
												<script>
													var objStreetName = new actb(document.getElementById('sn'), streetNameArray);
												</script>
											</td>

										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%">Parcel Number</td>
									<td class="tabletext">
										<html:text size="40" property="apn" styleClass="textbox" /></td>
									<td class="tablelabel" width="20%">Multiple Street </td>
									<td class="tabletext" valign="top" colspan="2">
									<table border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><html:select property="streetName2" styleClass="textbox">
												<html:option value="-1">Please Select</html:option>
												<html:options collection="multiStreetList" property="streetName" labelProperty="streetName" />
											</html:select></td>
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td colspan="4" class="tabletitle">Owner</td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%">Address Owner Name</td>
									<td colspan="3" class="tabletext"><html:text
										size="20" property="ownerName" styleClass="textbox" /> </td>
								</tr>


								<tr>
									<td class="tablelabel" width="20%">Foreign Address</td>

									<td colspan="1" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">Street No</td><td width="60%"><html:text size="20" property="foreignAddressLine1" styleClass="textbox" /></td>
											</tr>
											<tr>
												<td width="40%" class="tablelabel">Street Name</td><td width="60%"><html:text size="20" property="foreignAddressLine2" styleClass="textbox" /></td>
											</tr>
										</table>
									</td>

									<td colspan="2" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">Unit</td><td width="60%"><html:text size="20" property="foreignAddressLine3"	styleClass="textbox" /></td>
											</tr>
											<tr>
												<td width="40%" class="tablelabel">City</td><td width="60%"><html:text size="20" property="foreignAddressLine4" styleClass="textbox" /></td>
											</tr>
										</table>
									</td>
								</tr>



								<tr>
									<td class="tablelabel" width="20%"></td>

									<td colspan="1" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">State</td><td width="60%"><html:text size="20" property="foreignAddressState" styleClass="textbox" /></td>
											</tr>
											<tr>
												<td width="40%" class="tablelabel">Country</td><td width="60%"><html:text size="20" property="foreignAddressCountry" styleClass="textbox" /></td>
											</tr>
										</table>
									</td>

									<td colspan="2" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">Zip</td><td width="60%"><html:text size="20" property="foreignAddressZip" styleClass="textbox" /></td>
											</tr>
										</table>
									</td>
								</tr>



								<tr>
									<td colspan="4" class="tabletitle">Business Search</td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%">Business Account Number</td>
									<td colspan="3" class="tabletext"><html:text
										size="20" property="businessAcNumber" styleClass="textbox" onblur="return ValidateBlankSpace(this);" /> </td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%">Business Name</td>
									<td colspan="3" class="tabletext"><html:text
										size="80" property="businessName" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/> </td>
									</tr>
								<tr>
									<td class="tablelabel" width="20%">Business Owner Name</td>
									<td colspan="3" class="tabletext"><html:text
										size="80" property="businessOwnerName" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/> </td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%">Out of Town Address</td>

									<td colspan="1" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">Street No</td><td width="60%"><html:text size="20" property="outofTownAddress1" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
											<tr>
												<td width="40%" class="tablelabel">Street Name</td><td width="60%"><html:text size="20" property="outofTownAddress2" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
										</table>
									</td>

									<td colspan="2" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">Unit</td><td width="60%"><html:text size="20" property="outofTownAddress3" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
											<tr>
												<td width="40%" class="tablelabel">City</td><td width="60%"><html:text size="20" property="outofTownAddress4" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td class="tablelabel" width="20%"></td>
									<td colspan="1" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">State</td><td width="60%"><html:text size="20" property="outofTownAddressState" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
											<tr>
												<td width="40%" class="tablelabel">Country</td><td width="60%"><html:text size="20" property="outofTownAddressCountry" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
										</table>
									</td>

									<td colspan="2" class="tabletext">
										<table width="100%">
											<tr>
												<td width="40%" class="tablelabel">Zip</td><td width="60%"><html:text size="20" property="outofTownAddressZip" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/></td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td colspan="4" class="tabletitle">Sub Project</td>
								</tr>

								<tr>
									<td class="tablelabel" width="20%">Sub Project Name</td>
									<td colspan="1" class="tabletext">
									<html:select property="projectType" size="1" styleClass="textbox">
										<html:option value=""></html:option>
										<html:options collection="ptList" property="projectTypeId" labelProperty="description" />
									</html:select>
									</td>
									<td class="tablelabel" width="20%">Sub Project No</td>
									<td colspan="1" class="tabletext"><html:text size="20"
										property="subProjectNumber" maxlength="9"
										onkeypress="return validateNumeric();" styleClass="textbox" /></td>
								</tr>
								<tr>
									<td class="tablelabel" width="20%">Sub Project Type</td>
									<td colspan="1" class="tabletext">
										<html:select property="subProjectType" styleClass="textbox">
											<html:option value="">Please Select</html:option>
											<html:options collection="sptList" property="subProjectTypeId" labelProperty="description" />
										</html:select>
									</td>
									<td class="tablelabel">Sub Project Sub Type</td>
									<td colspan="1" class="tabletext"><html:select
										property="subProjectSubType" size="1" styleClass="textbox">
										<html:option value="">Please Select</html:option>
										<html:options collection="spstList"
											property="subProjectSubTypeId" labelProperty="description" />
									</html:select></td>
								</tr>
								<tr>
									<td colspan="4" class="tabletitle">Activity</td>
								</tr>
								<tr>
									<td class="tablelabel">Activity Number</td>
									<td colspan="1" class="tabletext"><html:text size="20"
										property="activityNumber" styleClass="textbox" maxlength="9" /></td>
									<td class="tablelabel">Activity List</td>
									<td colspan="1" class="tabletext" valign="top">
									<table border="0" cellspacing="0" cellpadding="1">
										<tr height="20px">
											<td><html:select property="streetName1" styleClass="textbox">
												<html:option value="-1">Please Select</html:option>
												<html:options collection="streets" property="streetId" labelProperty="streetName" />
											</html:select></td>
										</tr>
									</table>
								</td>
							</tr>
								<tr>
									<td class="tablelabel" width="20%">Cross Street</td>
									<td class="tabletext" colspan="1" >
										<html:select property="crossStreetName1" styleClass="textbox" onchange="return fromCrossStreet()">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="streets" property="streetId" labelProperty="streetName" />
										</html:select>
										<html:select property="crossStreetName2" styleClass="textbox">
											<html:options collection="crossStreets" property="streetId" labelProperty="streetName" />
										</html:select>
									</td>

									<td class="tablelabel" width="20%">Land Mark</td>
									<td class="tabletext" colspan="1">
										<html:select property="landmark" styleClass="textbox">
											<html:option value="-1">Please Select</html:option>
											<html:options collection="landmarks" property="addressId" labelProperty="name" />
										</html:select>
									</td>
								</tr>

								<tr>
									<td class="tablelabel">Activity Types</td>
									<td colspan="1" class="tabletext" valign="middle">
										<html:select
											property="activityType" size="7" styleClass="textbox"
											multiple="multiple" onchange="evalType();">
											<html:options collection="atList" property="type" labelProperty="description" />
										</html:select>
									</td>
									<td colspan="2" class="tabletext" valign="middle" align="left">
										<html:textarea property="selectedActType" cols="30" rows="6" styleClass="textbox" value=""></html:textarea>
									</td>
								</tr>

								<tr>
									<td class="tablelabel">Activity Status</td>
									<td colspan="1" class="tabletext" valign="middle">
										<html:select property="activityStatus" size="7" styleClass="textbox" multiple="multiple" onchange="evalActStatus();">
											<html:options collection="atStatus" property="status" labelProperty="description" />
										</html:select>
									</td>

									<td colspan="2" class="tabletext" valign="middle" align="left">
									<html:textarea property="selectedActStatus" cols="30" rows="6" styleClass="textbox" value=""></html:textarea></td>
								</tr>

								<tr>
									<td class="tablelabel">Activity Sub Types</td>
									<td colspan="1" class="tabletext" valign="middle"><html:select
										property="activitySubType" size="7" styleClass="textbox"
										multiple="multiple" onchange="evalSubType()">
										<html:options collection="atsList" property="id"
											labelProperty="description" />
									</html:select></td>
									<td colspan="2" class="tabletext" valign="middle" align="left">
									<html:textarea property="selectedActSubType" cols="30" rows="6"
										styleClass="textbox" value=""></html:textarea></td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
			</td>
			<td width="1%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td height="32">&nbsp;</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</html:form>
</body>
</html:html>

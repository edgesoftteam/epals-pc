<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>

<app:checkLogon/>

<html:html>

<head>
	<html:base/>
	<title>City of Burbank :  Document Search</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script language="JavaScript" src="../script/calendar.js"></script>

<script>

function link(){
	var link="http://chw2kdi01/LO";
	var c =window.open(link);
}

function alertCommentsAloneSearch() {
var comment=false;
if( (document.forms['documentSearchForm'].keyword1.value=="") && (document.forms['documentSearchForm'].keyword2.value=="") &&(document.forms['documentSearchForm'].keyword3.value=="")
&&(document.forms['documentSearchForm'].keyword4.value=="") && (document.forms['documentSearchForm'].streetFraction.value=="")
&&(document.forms['documentSearchForm'].streetName.value==-1)&&(document.forms['documentSearchForm'].unit.value=="")&&(document.forms['documentSearchForm'].projectNumber.value=="")
&&(document.forms['documentSearchForm'].subProjectNumber.value=="")&&(document.forms['documentSearchForm'].activityNumber.value=="")&&(document.forms['documentSearchForm'].activityDescription.value=="")
&&(document.forms['documentSearchForm'].appliedDate.value=="")&&(document.forms['documentSearchForm'].issuedDate.value=="")&&(document.forms['documentSearchForm'].finaledDate.value=="")) {
comment=true;
}
if(comment==true && (document.forms['documentSearchForm'].comment.value!="")) {
alert("you cannot Search Comments Alone.\n");
document.forms['documentSearchForm'].action="";
} if (comment==true && (document.forms['documentSearchForm'].comment.value=="")) {
alert("Please Enter Some Search Criteria");
document.forms['documentSearchForm'].action="";
 }

}
</script>

<%
    java.util.List streetList = new AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form name="documentSearchForm" type="elms.control.beans.DocumentSearchForm" action="/viewDocumentSearchResults">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Document Search<br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            	<td width="40%" background="../images/site_bg_B7C1CB.jpg" align="left"><font class="con_hdr_2b" >Search </td>
                                <td width="40%" background="../images/site_bg_B7C1CB.jpg" align="right"/>
                                <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="18" height="22"></td>
                                <td width="18%" align="RIGHT" class="tablelabel" >
                                   <html:reset value="Reset" styleClass="button"/> &nbsp;
                                   <html:submit value="Search" styleClass="button" onclick="return alertCommentsAloneSearch();"/> &nbsp;
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                            <table width="100%" cellpadding="2" cellspacing="1">
                            <tr>
                                <td colspan="6" class="tablelabel">Keywords</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">keyword1</td>
                                <td colspan="2" class="tabletext"><html:text property="keyword1" size="30" styleClass="textbox"/></td>
                                <td class="tablelabel" width="20%">keyword2</td>
                                <td colspan="2" class="tabletext"><html:text property="keyword2" size="30" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">keyword3</td>
                                <td colspan="2" class="tabletext"><html:text property="keyword3" size="30" styleClass="textbox"/></td>
                                <td class="tablelabel" width="20%">keyword4</td>
                                <td colspan="2" class="tabletext"><html:text property="keyword4" size="30" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Address</td>
                                <td class="tabletext">
                                	<html:text size="10" property="streetNumber" styleClass="textbox" />
                                	<html:select property="streetFraction" styleClass="textbox" onblur="SearchStreetName.focus();">
	                                	<html:option value=""></html:option>
	                                	<html:option value="1/4">1/4</html:option>
	                                	<html:option value="1/3">1/3</html:option>
	                                	<html:option value="1/2">1/2</html:option>
	                                	<html:option value="3/4">3/4</html:option>
                                	</html:select>
                                </td>
                                <td class="tabletext" valign="top">
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                        	<html:select property="streetName" styleClass="textbox">
                                        		<html:option value="-1">Please Select</html:option>
                                        		<html:options collection="streetList" property="streetId" labelProperty="streetName"/>
                                        	</html:select>
                                        </td>
                                    </tr>
                                </table>
                                </td>
					           <td class="tablelabel">Unit</td>
					           <td class="tabletext" colspan="2"><html:text size="10" property="unit" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td colspan="7" class="tablelabel">Project</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Project Number</td>
                                <td colspan="5" class="tabletext"> <html:text size="30" property="projectNumber" styleClass="textbox"/> </td>
                            </tr>
                            <tr>
                                <td colspan="7" class="tablelabel">Sub Project</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Sub-Project Number</td>
                                <td colspan="5" class="tabletext"> <html:text size="30" property="subProjectNumber" styleClass="textbox" /> </td>
                            </tr>
                            <tr>
                                <td colspan="6" class="tablelabel">Activity</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Activity Number</td>
                                <td colspan="5" class="tabletext"> <html:text size="30" property="activityNumber" styleClass="textbox"/> </td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Activity Description</td>
                                <td colspan="5" class="tabletext"><html:textarea property="activityDescription" styleClass="textbox" rows="3" cols="50"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%" height="23">Applied Date</td>
                                <td colspan="5" class="tabletext" valign="top"><html:text property="appliedDate" size="20" styleClass="textbox"/>&nbsp;<a href="javascript:show_calendar('documentSearchForm.appliedDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="16" border="0"/></a></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%" height="23">Issued Date</td>
                                <td colspan="5" class="tabletext" valign="top"><html:text property="issuedDate" size="20" styleClass="textbox"/>&nbsp;<a href="javascript:show_calendar('documentSearchForm.issuedDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="16" border="0"/></a></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%" height="23">Finaled Date</td>
                                <td colspan="5" class="tabletext" valign="top"><html:text property="finaledDate" size="20" styleClass="textbox"/>&nbsp;<a href="javascript:show_calendar('documentSearchForm.finaledDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="16" border="0"/></a></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" width="20%">Comment</td>
                                <td colspan="5" class="tabletext"><html:textarea property="comment" styleClass="textbox" rows="3" cols="50"/></td>
                            </tr>
                       </table>
                    </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>

<table name="t2" width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td colspan="2"><font class="con_hdr_3b">Document Imaging Search<br><br></td>
            </tr>
            <tr>
				<td class="tablelabel" width="70%">Document Image Search</td>
                      <td class="tablelabel" width="30%">
                         <input type="button"  name="documentSearchButton" value=" Login to LibertyNet " class="button" onClick="link()" style="float:right;"/>

                          </td>
                      </tr>
</table>

</html:form>

</body>
</html:html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Address Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<script src="../script/yCodelib.js"> </script>
<script src="../script/yCode_combo_box.js"> </script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form name="businessSearchForm" type="elms.control.beans.BusinessSearchForm" action="" >
<br>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
    <td width="1%">
      <table width="10" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="98%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
          <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_3b">Business License Information</td>
                      <td width="1%"><html:img src="../images/site_hdr_split_1.jpg"  width="20" height="20" /></td>
                      <td width="1%" class="tablelabel"><nobr><html:button property="Back" value="Back" styleClass="button" onclick="history.go(-1)"/>&nbsp;</nobr></td>
                      <td width="1%" class="tablelabel"><nobr><html:button property="Print" value="Print" styleClass="button" onclick="javascript:window.print()"/>&nbsp;</nobr></td>
                    </tr>
               </table>
           </td>
        </tr>
 	    <tr><td>&nbsp;</td></tr>

	      <tr>
	        <td background="../images/site_bg_B7C1CB.jpg">
	          <table width="100%" cellpadding="2" cellspacing="1">
	            <tr>
	              <td width="22%" rowspan="1" class="tablelabel">Business License # :</td>
	              <td width="78%" class="tabletext"><bean:write name="businessSearchForm" property="licenseNumber"/></td>
	            </tr>
				<tr>
				  <td class="tablelabel" >Business Name :</td>
				  <td colspan="2" class="tabletext"> <bean:write name="businessSearchForm" property="name"/> </td>
				</tr>
				<tr>
				  <td class="tablelabel" >Business Address :</td>
				  <td colspan="2" class="tabletext"><bean:write name="businessSearchForm" property="streetNumber"/>&nbsp;<bean:write name="businessSearchForm" property="streetName"/> </td>
				</tr>
				<tr>
				  <td class="tablelabel" >Secondary Address :</td>
				  <td colspan="2" class="tabletext"><bean:write name="businessSearchForm" property="address2"/></td>
				</tr>
				<tr>
				  <td class="tablelabel" >Business Current :</td>
				  <td colspan="2" class="tabletext"><bean:write name="businessSearchForm" property="current"/> </td>
				</tr>
			  </table>
			</td>
		  </tr>

	     <tr><td>&nbsp;</td></tr>

<logic:present name="businessList" >
 	   <tr>
 	     <td>
 	       <table width="100%" border="0" cellspacing="1" cellpadding="2">
 	         <tr>
		       <td class="tablelabel">Service</td>
		       <td class="tablelabel">Title</td>
		       <td class="tablelabel">Reference</td>
		       <td class="tablelabel" align="RIGHT">Date</td>
<logic:iterate id="group" name="user" property="groups" scope="session">
  <logic:equal name="group" property="groupId" value="20">
		       <td class="tablelabel" align="RIGHT">Billed</td>
		       <td class="tablelabel" align="RIGHT">Paid</td>
  </logic:equal>
</logic:iterate>
	   		</tr>
<logic:iterate id="business" name="businessList" scope="request" >
			<tr valign="top">
			  <td><bean:write name="business" property="service"/></td>
			  <td><bean:write name="business" property="title"/></td>
			  <td><bean:write name="business" property="reference"/></td>
			  <td align="RIGHT"><bean:write name="business" property="date"/></td>
<logic:iterate id="group" name="user" property="groups" scope="session">
  <logic:equal name="group" property="groupId" value="20">
			  <td align="RIGHT"><bean:write name="business" property="billed"/></td>
			  <td align="RIGHT"><bean:write name="business" property="paid"/></td>
  </logic:equal>
</logic:iterate>
		    </tr>
</logic:iterate>
	      </table>
	    </td>
	   </tr>
</logic:present>
      </table>
    </td>
    <td width="1%">
      <table width="20" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
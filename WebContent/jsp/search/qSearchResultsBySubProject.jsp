<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%@ page import="java.util.*,elms.app.project.*,elms.util.db.Wrapper,java.sql.*" %>
<%
	String contextRoot = request.getContextPath();
	List subProjectResults = (List)request.getAttribute("subProjList");
	Iterator subPri = subProjectResults.iterator();
	String count = (String)request.getAttribute("count");

%>

<html:html>
<head>
<html:base/>
<title>City of Beverly Hills : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/obc.css" type="text/css">
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results
<%
if(subProjectResults.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>
            <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="99%" class="tabletitle">Subprojects <font class="red2b">(<%=subProjectResults.size()%> Records )  <%=count%> Distinct Subprojects </td>
                   			   <td width="1%" class="tablebutton">
                    		  <html:button  value="Cancel" property="button" styleClass="button" onclick="javascript:history.back()"/>
                    		  </td>

                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="td1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <th class="tablelabel">
                                Subproject No.
                              </th>
                              <th class="tablelabel">
                                Type
                              </th>
                              <th class="tablelabel">
                                Sub Type
                              </th>
                              <th class="tablelabel">
                                Description
                              </th>
                              <th class="tablelabel">
                                Address
                              </th>
                            </tr>

<%
SubProject subProject2 = new SubProject();
String lsoId = "0";
String subId = "0";
if(subProjectResults.size() == 1) {
	subProject2 = (SubProject)subProjectResults.get(0);
	subId = new Integer(subProject2.getSubProjectId()).toString();
	Wrapper db = new Wrapper();
	ResultSet rs = db.select("SELECT DISTINCT LSO_ID FROM LSO_ADDRESS WHERE LSO_ID IN (SELECT LSO_ID FROM PROJECT WHERE PROJ_ID IN ( SELECT PROJ_ID FROM SUB_PROJECT WHERE SPROJ_ID = " + subProject2.getSubProjectId() + " ))");
	if(rs.next()) {
		lsoId = new Integer(rs.getInt("LSO_ID")).toString();
	}
}
%>

<script>
var siz = <%=subProjectResults.size()%>;
if(siz == 1) {
	document.location.href="<%=contextRoot%>/viewPSA.do?levelType=Q&levelId=<%=subId%>&lsoId=<%=lsoId%>;"
}
</script>

<% 								while(subPri.hasNext()){
                          		elms.app.project.SubProject subProject = (elms.app.project.SubProject)subPri.next();
                          		elms.app.project.SubProjectDetail subProjectDetail = subProject.getSubProjectDetail();
%>

                            <tr>
                              <td class="tabletext" >
                                <a href="<%=contextRoot%>/viewPSA.do?levelType=Q&levelId=<%=subProject.getSubProjectId()%>&lsoId=<%=subProject.getSubProjectDetail().getLsoId()%>"><%= subProject.getSubProjectDetail().getSubProjectNumber()%></a>
                              </td>
                              <td class="tabletext">
                                <%= subProjectDetail.getSubProjectType().getSubProjectType()%>
                              </td>
                              <td class="tabletext">
                                <%= subProjectDetail.getDescription()%>
                              </td>
                              <td class="tabletext">
                                <%= subProjectDetail.getSubProjectSubType().getType()%>
                              </td>
                              <td class="tabletext">
                              	<%= subProjectDetail.getAddress()%>
                              </td>
                            </tr>
<%
							}
%>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>
</body>
</html:html>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="java.util.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Address Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String contextRoot = request.getContextPath();
%>
<script language="JavaScript">

function clearFields() {
	document.forms[0].elements['licenseNumber'].value = '';
	document.forms[0].elements['name'].value = '';
	document.forms[0].elements['streetNumber'].value = '';
	document.forms[0].elements['streetName'].value = '';
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form focus="licenseNumber" action="/businessSearch.do" >
<br>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
    <table  width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Business License Search</td>
                      <td width="1%"><html:img src="../images/site_hdr_split_1.jpg"  width="20" height="20" /></td>
                      <td width="1%" class="tablelabel"><nobr>
                      	<html:submit  value="Search" styleClass="button" />&nbsp;
			  			<html:button property="Reset" value="Reset" styleClass="button" onclick="return clearFields();"/>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" cellpadding="2" cellspacing="1">
                    <tr>
                      <td width="30%" rowspan="1" class="tablelabel">Business License # :</td>
                      <td width="70%" class="tabletext"><html:text  maxlength="9" size="9" property="licenseNumber" styleClass="textbox" /></td>
		            </tr>
					<tr>
					  <td class="tablelabel" >Business Name :</td>
					  <td colspan="2" class="tabletext"> <html:text size="40" maxlength="40" property="name" styleClass="textbox"/> </td>
					</tr>
					<tr>
					  <td class="tablelabel" >Business Address :</td>
					  <td colspan="2" class="tabletext"><html:text size="6" property="streetNumber" styleClass="textbox"/>&nbsp;<html:text size="20" property="streetName" styleClass="textbox"/> </td>
					</tr>
				  </table>
				</td>
			  </tr>
		    </table>
		  </td>
	    </tr>
<logic:present name="matchList" >
	    <tr>
	      <td>
            <table width="100%" border="1" cellspacing="1" cellpadding="1">
	          <tr>
		        <td colspan="4" height="30" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_3b"><center>Matching Results</center></td>
	          </tr>
<logic:iterate id="business" name="matchList" scope="request" >
		      <tr valign="top">
<logic:present name="business" property="streetName" >
			    <td>
			      <bean:write name="business" property="streetNumber"/>
			    </td>
			    <td>
			      <bean:write name="business" property="streetName"/>
			    </td>
</logic:present>
			    <td>
			       <a href ><a href="<%=contextRoot%>/businessSummary.do?licenseNumber=<bean:write name="business" property="licenseNumber"/>"><bean:write name="business" property="licenseNumber"/></a>
			    </td>
			    <td>
			       <bean:write name="business" property="name"/>
			    </td>
		      </tr>
</logic:iterate>
	        </table>
	      </td>
	    </table>
</logic:present>
<logic:present name="businessList" >
	    <tr>
	      <td>
            <table width="100%" border="1" cellspacing="1" cellpadding="1">
	          <tr>
		        <td colspan="5" height="30" background="../images/site_bg_B7C1CB.jpg"><font class="con_hdr_3b"><center>Search Results</center></td>
	          </tr>

            <%-- The header information--%>
              <tr>
			    <td background="../images/site_bg_B7C1CB.jpg">
			      Street #
			    </td>
			    <td background="../images/site_bg_B7C1CB.jpg">
			      Street Name
			    </td>
			    <td background="../images/site_bg_B7C1CB.jpg">
			      Secondary Address
			    </td>
			    <td background="../images/site_bg_B7C1CB.jpg">
			      Licence #
			    </td>
			    <td background="../images/site_bg_B7C1CB.jpg">
			      Business Name
			    </td>
              </tr>

              <%-- The Message to be displayed if there are no results--%>
<logic:present name="message">
			  <tr>
				  <td>No Matching Records Found</td>
			  </tr>
</logic:present>
               <%-- The search results to be displayed if there are no results--%>
<logic:iterate id="business" name="businessList" scope="request" >
		      <tr valign="top">
<logic:present name="business" property="streetName" >
			    <td>
			      <bean:write name="business" property="streetNumber"/>
			    </td>
			    <td>
			      <bean:write name="business" property="streetName"/>
			    </td>
</logic:present>
			    <td>
			      <bean:write name="business" property="address2"/>
			    </td>
			    <td>
			       <a href ><a href="<%=contextRoot%>/businessSummary.do?licenseNumber=<bean:write name="business" property="licenseNumber"/>"><bean:write name="business" property="licenseNumber"/></a>
			    </td>
			    <td>
			       <bean:write name="business" property="name"/>
			    </td>
		      </tr>
</logic:iterate>
	        </table>
	      </td>
	    </table>
</logic:present>
      </table>
    </td>
    <td width="1%">
      <table width="200" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
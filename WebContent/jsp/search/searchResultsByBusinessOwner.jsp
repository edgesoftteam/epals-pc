<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*,elms.app.bl.*" %>

<%
	String contextRoot = request.getContextPath();
	List businessOwners = (List) request.getAttribute("businessOwnerResults");
	int size = businessOwners.size();
	BusinessOwner businessOwner1 = (BusinessOwner) request.getAttribute("businessOwner1")!=null ? (BusinessOwner) request.getAttribute("businessOwner1"): new BusinessOwner();
	pageContext.setAttribute("businessOwners",businessOwners);
	//pageContext.setAttribute("businessOwner1",businessOwner1);
%>
<% if(size==1){
%>
<script>
 var siz = <%=size%>
 if(siz == 1) {
 		document.location.href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId=<bean:write name="businessOwner1" property="activity_id" />&lsoId=<bean:write name="businessOwner1" property="lso_id" />;"
 }
<% }
%>
</script>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/gs_sortable.js"></script>

<script type="text/javascript">
<!--
var TSort_Data = new Array ('table_demo_world', 's', 's', 's','s','s','s','s','i','s');
var TSort_Classes = new Array ('row1', 'row2');
//var TSort_Initial = new Array ('0A', '1D');
tsRegister();
// -->

function viewPsa(activity_id,lso_id){
document.location.href="<%=contextRoot%>/viewPSA.do?levelType=A&levelId="+activity_id+"&lsoId="+lso_id;
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>

<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results
                   <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="100%" background="../images/site_bg_B7C1CB.jpg">Search Results (<%=businessOwners.size()%> records found)</td>
                              <td width="1%" class="tablelabel">
                    		  <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                     		 </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">

                            <table id="table_demo_world" width="100%" border="0" cellspacing="1" cellpadding="2">
							<thead>
                            <tr>
                              <th class="tablelabel">
                                <strong>Business Name</strong>
                              </th>
                              <th class="tablelabel">
                                <strong>Business Address</strong>
                              </th>
                               <th class="tablelabel">
                                <strong>Business Owner Name</strong>
                              </th>
                               <th class="tablelabel">
                                <strong>Owner1 Name</strong>
                              </th>
                               <th class="tablelabel">
                                <strong>Owner2 Name</strong>
                              </th>
                              <th class="tablelabel">
                                <strong>Activity Type</strong>
                              </th>
							 <th class="tablelabel">
                                <strong>Activity Sub-Type</strong>
                              </th>
                              <th class="tablelabel">
                                <strong>Account Number</strong>
                              </th>
                              <th class="tablelabel">
                                <strong>Activity Status</strong>
                              </th>
							</tr>
                           </thead>
<%
							session.setAttribute("onloadAction","noChangeContent");
int i=1;
%>

                        <logic:iterate name="businessOwners" id="businessOwner" type="elms.app.bl.BusinessOwner">
                            <tr>
                             <td class="tabletext" class="con_text_1" style="cursor:hand;text-decoration:underline;" onclick="viewPsa('<bean:write name="businessOwner" property="activity_id" />','<bean:write name="businessOwner" property="lso_id" />')">
                               <bean:write name="businessOwner" property="businessName"/>
                             </td>
                             <td class="tabletext" class="con_text_1">
                                <nobr><bean:write name="businessOwner" property="businessAddress"/></nobr>
                              </td>
                              <td class="tabletext" class="con_text_1">
                                <bean:write name="businessOwner" property="businessOwnerName"/>
                              </td>
                              <td class="tabletext" class="con_text_1">
                                <bean:write name="businessOwner" property="owner1Name"/>
                              </td>
                              <td class="tabletext" class="con_text_1">
                                <bean:write name="businessOwner" property="owner2Name"/>
                              </td>
								<td class="tabletext" class="con_text_1">
                                <bean:write name="businessOwner" property="activityType"/>
                              </td>
								<td class="tabletext" class="con_text_1">
                                <bean:write name="businessOwner" property="activitySubType"/>
                              </td>

                              <td class="tabletext" style="cursor:hand;text-decoration:underline;" class="con_text_1" onclick="viewPsa('<bean:write name="businessOwner" property="activity_id" />','<bean:write name="businessOwner" property="lso_id" />')">
                               <bean:write name="businessOwner" property="businessAcNumber"/>
                              </td>
                              <td class="tabletext" class="con_text_1">
                                <bean:write name="businessOwner" property="activityStatus"/>
                              </td>
                            </tr>
						<%
                            i++;
                            if(i==3){i=1;}%>
                         </logic:iterate>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>

</body>
</html:html>


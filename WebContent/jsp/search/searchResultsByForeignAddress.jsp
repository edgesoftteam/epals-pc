<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.lso.*" %>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<script type="text/javascript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>


</head>
<%
	String contextRoot = request.getContextPath();
	java.util.List foreignAddressResults = (java.util.List) request.getAttribute("foreignAddressResults");
	java.util.Iterator iter = foreignAddressResults.iterator();
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Search Results
<%
if(foreignAddressResults.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>

			<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" background="../images/site_bg_B7C1CB.jpg">Search Results (<%=foreignAddressResults.size()%> records found)</td>
                      <td width="1%" class="tablelabel">
                      <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table class="sortable" id="t1"  width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
					  <th class="tablelabel">Owner Name</td>
					  <th class="tablelabel">Apn</td>
                      <th class="tablelabel">Address</td>
                      <th class="tablelabel">Foreign Address</td>
                    </tr>
<%
OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
if(foreignAddressResults.size() == 1) {
	ownerApnAddress = (OwnerApnAddress)foreignAddressResults.get(0);
}

%>

<script>
var siz = <%=foreignAddressResults.size()%>;
if(siz == 1) {
	document.location.href="javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_content.location.href='<%=contextRoot%>/viewLand.do?lsoId=<%=ownerApnAddress.getLsoId()%>';";
}
</script>
<%

while (iter.hasNext()){
ownerApnAddress = (OwnerApnAddress)foreignAddressResults.get(0);
ownerApnAddress = (OwnerApnAddress)iter.next();
%>
                    <tr valign="top">
                         <td class="tabletext"><a href=javascript:parent.f_lso.location.href='<%=contextRoot%>/lsoSearchWithLso.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_psa.location.href='<%=contextRoot%>/viewPsaTree.do?lsoId=<%=ownerApnAddress.getLsoId()%>';parent.f_content.location.href='<%=contextRoot%>/viewLand.do?lsoId=<%=ownerApnAddress.getLsoId()%>';><%=ownerApnAddress.getOwnerName()%></a></td>
<%--                     <td class="tabletext"><a href="<%=contextRoot%>/viewAssessor.do?APN=<%=ownerApnAddress.getApn()%>"><%=ownerApnAddress.getApn()%></a></td>--%>
                         <td class="tabletext"><%=ownerApnAddress.getApn()%>&nbsp;</td>
					     <td class="tabletext"><%=ownerApnAddress.getAddress()%></td>
					     <td class="tabletext"><%=ownerApnAddress.getForeignAdd()%></td>
                    </tr>
<%
}
%>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>


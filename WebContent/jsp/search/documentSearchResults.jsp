<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.project.*" %>
<%@ page import="elms.app.admin.*" %>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Search</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script src="../script/sorttable.js"></script>
<script type="text/javascript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
history.back();
}

}
</script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    background-color:#eee;
    color:#666666;
    font-weight: bold;
    text-decoration: none;
    display: block;
}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results <logic:equal name="documentSearchResults" value="0">( No Results Found. Please adjust your search criterion)</logic:equal>

            <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="100%" background="../images/site_bg_B7C1CB.jpg">Documents</td>
                              <td width="1%" class="tablelabel">
                    		  <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                     		 </td>

                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                              <th class="tablelabel">
                                File Name
                              </th>
                              <th class="tablelabel">
                                Attachment level
                              </th>
                              <th class="tablelabel">
                                Description
                              </th>
                            </tr>
<logic:iterate name="documentSearchResults" scope="request" id="document">
                            <tr>
                              <td class="tabletext" >
                                <a href="<bean:write name="document" property="fileUrl"/>" target="_blank"><bean:write name="document" property="fileName"/></a>
                              </td>
                              <td class="tabletext">
                                <nobr><bean:write name="document" property="attachmentLevel"/></nobr>
                              </td>
                              <th class="tabletext">
                                <nobr><bean:write name="document" property="description"/>
                              </th>
                            </tr>
</logic:iterate>
						  </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>
</body>
</html:html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ page import="elms.agent.ActivityAgent" %>
<app:checkLogon />
<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String first = request.getParameter("first") != null ? request
				.getParameter("first") : "";
		if (first.equals("one")) {
			String stat = request.getParameter("status") != null ? request.getParameter("status"): "";
			String edit = request.getParameter("editable") != null ? request.getParameter("editable"): "";
			session.setAttribute("stat", stat);
			session.setAttribute("edit", edit);
		}
		String status = (String) session.getAttribute("stat");
		String editable = (String) session.getAttribute("edit");

		int offset = 3 + 1, rows = 0;
		String contextRoot = request.getContextPath();
		String psaInfo = "", lsoAddress = "";

		try {
			lsoAddress = (String) session.getAttribute("lsoAddress");
			if (lsoAddress == null)	lsoAddress = "";

			psaInfo = (String) session.getAttribute("psaInfo");
			if (psaInfo == null) psaInfo = "";
		} catch (Exception e) {
		}
		String codeInspector = "";
		String userFullName = "";

		if (session.getAttribute("codeInspector") != null) {
			codeInspector = (String) session.getAttribute("codeInspector");
		}

		if (session.getAttribute("userFullName") != null) {
			userFullName = (String) session.getAttribute("userFullName");
		}
		
		String activityId=request.getParameter("activityId");
		
		int deptId=new ActivityAgent().getDepartmentIdForActivityId(activityId);
		
		int moduleId = 0;
		if(request.getAttribute("moduleId") != null){
			moduleId = Integer.parseInt(request.getAttribute("moduleId").toString());
		}
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="JavaScript">
	var codeIns=0;
	var userFullName="";
function checkCodeInspector(number){
if(number==4||number==47){
codeIns=1;
userFullName=document.codeEnforcementForm.userFullName.value;
}
}

function fame(name) {
if(codeIns==1)name.value=userFullName;
else name.value='';
codeIns=0;
}
</script>
<script language="JavaScript">
	var strValue;

    function addScroll(i) {
        if (i != null && i != "") {
            var sto = document.getElementById(i).offsetTop;
            window.scrollTo(0, sto);
        }
    }

	function validateFunction() {
	    return true;
	}

	function savePage() {
		if (validateFunction()) {
	 		document.forms['codeEnforcementForm'].submit();
	 	} else
	 		return false;
	}

	function addPage(list) {
		document.forms['codeEnforcementForm'].action='<%=contextRoot%>/saveCodeEnforcement.do?action=' + list;
		document.forms['codeEnforcementForm'].submit();
	}

	function cancelPage() {
		document.forms[0].action='<%=contextRoot%>/saveCodeEnforcement.do?action=Cancel';
	 	document.forms[0].submit();
	}

	function formatPhone(str) {
		if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)) 	{
			event.returnValue = false;
		} else {
			if ((document.codeEnforcementForm.elements[str].value.length == 3 ) || (document.codeEnforcementForm.elements[str].value.length == 7 ))
				document.codeEnforcementForm.elements[str].value = document.codeEnforcementForm.elements[str].value +'-';
	 		if (document.codeEnforcementForm.elements[str].value.length > 11 )  event.returnValue = false;
		}
	}

	function checkNum(data)
	{
	   var validNum = "0123456789.";
	   var i = count = 0;
	   var dec = ".";
	   var space = " ";
	   var val = "";

	   for (i = 0; i < data.length; i++)
	      if (validNum.indexOf(data.substring(i, i+1)) != "-1")
	         val += data.substring(i, i+1);

	   return val;
	}

	function dollarFormatted(amount) {
		return '$' + commaFormatted(amount);
	}

	function commaFormatted(amount) {
			var delimiter = ","; // replace comma if desired
			var a = amount.split('.',2)
			var d = a[1];
			var i = parseInt(a[0]);
			if(isNaN(i)) { return ''; }
			var minus = '';
			if(i < 0) { minus = '-'; }
			i = Math.abs(i);
			var n = new String(i);
			var a = [];
			while(n.length > 3)
			{
			var nn = n.substr(n.length-3);
			a.unshift(nn);
			n = n.substr(0,n.length-3);
			}
			if(n.length > 0) { a.unshift(n); }
			n = a.join(delimiter);
			if(d.length < 1) { amount = n; }
			else { amount = n + '.' + d; }
			amount = minus + amount;
			return amount;
	}
	// end of function CommaFormatted()

	function formatCurrency(field) {
			var i = parseFloat(checkNum(field.value));
			if(isNaN(i)) { i = 0.00; }
			var minus = '';
			if(i < 0) { minus = '-'; }
			i = Math.abs(i);
			i = parseInt((i + .005) * 100);
			i = i / 100;
			s = new String(i);
			if(s.indexOf('.') < 0) { s += '.00'; }
			if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
			s = minus + s;
			field.value = dollarFormatted(s);
			return true;
	}
</script>

<bean:define id="userList" name="codeEnforcementForm" property="userList" type="java.util.Collection" />
<bean:define id="subTypeList" name="codeEnforcementForm" property="subTypeList" type="java.util.Collection" />


<body class="tabletext" text="#000000" onload="addScroll('<bean:write property="scrollTo" name="codeEnforcementForm"/>')">
<html:form action="/saveCodeEnforcement.do?action=Save">
	<html:hidden property="userFullName" value="<%=userFullName%>" />

	<%
		if(deptId==25){
	%>
	<font class="con_hdr_3b">Notice/Letters Details</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br></font>
  
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addNotice" style="border:1px solid #B7C1CB">
               <tr>
                    <td colspan="5">
                      <table cellpadding="0" cellspacing="0" border="0" width="100%">
                        <td bgcolor="#B7C1CB" width="70%"><font class="con_hdr_2b">Notices/Letters Issued</font></td>
						<td bgcolor="#D7DCE2" width="1%"><img src="../images/site_hdr_split_1.jpg" height="26" width="25"></td>
                        <td bgcolor="#D7DCE2" width="29%">
						<nobr> <input type="button"  value="Back" size="50" styleClass="button" onclick="return cancelPage();" />
						<security:editable levelId="<%=activityId%>" moduleId="<%=moduleId %>" levelType="A" editProperty="checkModuleId">
							<nobr> <input type="button" value="Add" size="50" styleClass="button" onclick="return addPage('addNotice');" />
						 	<input type="button"   value="Update" size="50" styleClass="button" onclick="return savePage();" /></nobr>
						 </security:editable>
                        </td>
                      </table>
                    </td>
                </tr>
        <logic:present name="codeEnforcementForm" property="noticeArray">
                <tr>
                    <td bgcolor="#D7DCE2" width="1%" style="padding:2px"><font class="con_hdr_1">Delete</font></td>
                    <td bgcolor="#D7DCE2" width="40%" style="padding:2px"><font class="con_hdr_1">Type</font></td>
                    <td bgcolor="#D7DCE2" width="25%" style="padding:2px"><font class="con_hdr_1">Staff/Inspector</font></td>
                    <td bgcolor="#D7DCE2" width="25%" style="padding:2px"><font class="con_hdr_1">Issued Date</font></td>
					<td bgcolor="#D7DCE2" width="5%" style="padding:2px"><font class="con_hdr_1"></font></td>
                </tr> 
                <bean:define id="noticeTypeList" name="codeEnforcementForm" property="noticeTypeList" type="java.util.Collection"/>
                <nested:iterate name="codeEnforcementForm" property="noticeArray" id="notice" scope="session">
                <tr>
                    <td style="border-bottom:1px solid #B7C1CB">
                        <nested:checkbox name="notice" property="deleted" />
                        <nested:hidden name="notice" property="changed" />
                    </td>
                    <td style="border-bottom:1px solid #B7C1CB">
                    <logic:present name="notice" property="ceType"><font class="con_text_1"><bean:write name="notice" property="ceType" /></font></logic:present>
                    <logic:notPresent name="notice" property="ceType">
                        <nested:select name="notice" property="ceTypeId" styleClass="textbox">
                          <html:option value="-1">Please Select</html:option>
                          <html:options collection="noticeTypeList" property="ceTypeId" labelProperty="name"/>
                        </nested:select>
                     </logic:notPresent>
                    </td>
                    <td style="border-bottom:1px solid #B7C1CB">
                        <nested:select name="notice" property="officerId" styleClass="textbox">
                          <html:option value="0">Please Select</html:option>
                          <html:options collection="userList" property="id" labelProperty="description"/>
                        </nested:select>
                    </td>
                   <td style="border-bottom:1px solid #B7C1CB"><nested:text name="notice" onfocus="showCalendarControl(this);" property="ceDate" styleClass="textbox" size="12" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
                    </td>
					<logic:notEqual name="notice" property="id" value="0">					
						<td style="border-bottom:1px solid #B7C1CB"><a href="<%=contextRoot%>/printNotices.do?actId=<%=activityId%>&ceId=<bean:write name='notice' property='id'/>" target="_new"><img src="../images/printer.png" border='0'></a></td>
					</logic:notEqual>
					<logic:equal name="notice" property="id" value="0">					
						<td style="border-bottom:1px solid #B7C1CB">&nbsp;</td>
					</logic:equal>
						
                </tr>
        <% rows++; %>
                </nested:iterate>
                <% offset += rows*5;%>
        </logic:present>
            </table>
			</td>
        </tr>
	</table>
	<%
		}
		else{
	%>
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td><font class="con_hdr_3b">Code Enforcement Details</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
					<br>
					</td>
				</tr>
			</table>
			</td>
		</tr>

		<tr>
			<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td class="tabletitle"width="90%">Code Enforcement</td>
					<td class="tablebutton" width="9%"><nobr><html:button property="Cancel" value="Back" styleClass="button" onclick="return cancelPage();" /> <%
					 	if (editable.equals("true") && status.equals("Active")) {
					 %> <html:button property="Save" value="Update" styleClass="button" onclick="return savePage();" /></nobr> <%
					 	}
					 %>
					</td>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>

		<tr>
			<td valign="top" width="100%">
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addNotice" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<td class="tabletitle" width="90%">Notices Issued</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablebutton" width="9%"><nobr><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addNotice');" /> <%
 	}
 %>
						</td>
					</table>
					</td>
				</tr>
				<logic:present name="codeEnforcementForm" property="noticeArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Sub Type</td>
						<td class="tablelabel" width="16%">Officer</td>
						<td class="tablelabel" width="12%">Date</td>
					</tr>
					<bean:define id="noticeTypeList" name="codeEnforcementForm" property="noticeTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="noticeArray" id="notice" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="notice" property="deleted" /> <nested:hidden name="notice" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB;"><nested:select name="notice" property="ceTypeId" styleClass="textbox_C">
								<html:option value="-1">Please Select</html:option>
								<html:options collection="noticeTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="notice" property="subType" styleClass="textbox_C">
								<html:option value="">Please Select</html:option>
								<html:options collection="subTypeList" property="code" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="notice" property="officerId" styleClass="textbox_C">
								<html:option value="0">Please Select</html:option>
								<html:options collection="userList" property="id" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="notice" property="ceDate" styleClass="textbox_C" size="8" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('codeEnforcementForm.elements[<%=offset + rows * 6 + 5%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>
						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 6;
					%>
				</logic:present>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addResolution">
						<td class="tabletitle" width="90%">Resolution Type</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablelabel" width="9%"><nobr><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addResolution');" /> <%
 	}
 %>
						</td>
					</table>
					</td>
				</tr>
				<%
					offset++;
							rows = 0;
				%>
				<logic:present name="codeEnforcementForm" property="resolutionArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Sub Type</td>
						<td class="tablelabel" width="16%">Officer</td>
						<td class="tablelabel" width="12%">Date</td>
					</tr>
					<bean:define id="resolutionTypeList" name="codeEnforcementForm" property="resolutionTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="resolutionArray" id="resolution" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="resolution" property="deleted" /> <nested:hidden name="resolution" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="resolution" property="ceTypeId" styleClass="textbox_C">
								<html:option value="-1">Please Select</html:option>
								<html:options collection="resolutionTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="resolution" property="subType" styleClass="textbox_C">
								<html:option value="">Please Select</html:option>
								<html:options collection="subTypeList" property="code" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="resolution" property="officerId" styleClass="textbox_C">
								<html:option value="0">Please Select</html:option>
								<html:options collection="userList" property="id" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="resolution" property="ceDate" styleClass="textbox_C" size="8" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('codeEnforcementForm.elements[<%=offset + rows * 6 + 5%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>
						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 6;
					%>
				</logic:present>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addPerson" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<td class="tabletitle" width="90%">Persons Involved</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablelabel" width="9%"><nobr><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addPerson');" /> <%
 	}
 %>
						</td>
					</table>
					</td>
				</tr>
				<%
					offset++;
							rows = 0;
				%>
				<logic:present name="codeEnforcementForm" property="peopleArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Name</td>
						<td class="tablelabel" width="16%">Phone</td>
						<td class="tablelabel" width="12%">&nbsp;</td>
					</tr>
					<bean:define id="peopleTypeList" name="codeEnforcementForm" property="peopleTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="peopleArray" id="people" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="people" property="deleted" /> <nested:hidden name="people" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="people" property="ceTypeId" styleClass="textbox_C" onchange="checkCodeInspector(this.value);">
								<html:option value="-1">Please Select</html:option>
								<html:options collection="peopleTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="people" property="name" styleClass="textbox_C" size="25" maxlength="30" onfocus="return fame(this);" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="people" property="phone" styleClass="textbox_C" size="10" maxlength="15" onkeypress="return formatPhone(this.name);" /></td>
							<td style="border-bottom: 1px solid #B7C1CB">&nbsp;</td>
						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 5;
					%>
				</logic:present>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addFee" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<td class="tabletitle"  width="90%">Fees Collected</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablelabel" width="9%"><nobr><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addFee');" /> <%
 	}
 %>
						</td>
					</table>
					</td>
				</tr>
				<%
					offset++;
							rows = 0;
				%>
				<logic:present name="codeEnforcementForm" property="feeArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Officer</td>
						<td class="tablelabel" width="16%">Date</td>
						<td class="tablelabel" width="12%">Amount</td>
					</tr>
					<bean:define id="feeTypeList" name="codeEnforcementForm" property="feeTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="feeArray" id="fee" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="fee" property="deleted" /> <nested:hidden name="fee" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="fee" property="ceTypeId" styleClass="textbox_C">
								<html:option value="-1">Please Select</html:option>
								<html:options collection="feeTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="fee" property="officerId" styleClass="textbox_C">
								<html:option value="0">Please Select</html:option>
								<html:options collection="userList" property="id" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="fee" property="ceDate" styleClass="textbox_C" size="8" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('codeEnforcementForm.elements[<%=offset + rows * 6 + 4%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="fee" property="feeAmount" styleClass="textboxm" size="10" maxlength="10" onkeypress="return validateCurrency();" onblur="formatCurrency(this);" /></td>
						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 6;
					%>
				</logic:present>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addInspection" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<td class="tabletitle"  width="90%">Inspection Types Issued</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablelabel" width="9%"><nobr><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addInspection');" /> <%
 	}
 %>
						</td>
					</table>
					</td>
				</tr>
				<%
					offset++;
							rows = 0;
				%>
				<logic:present name="codeEnforcementForm" property="inspectionArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Sub Type</td>
						<td class="tablelabel" width="16%">Officer</td>
						<td class="tablelabel" width="12%">Date</td>
					</tr>
					<bean:define id="inspectionTypeList" name="codeEnforcementForm" property="inspectionTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="inspectionArray" id="inspection" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="inspection" property="deleted" /> <nested:hidden name="inspection" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="inspection" property="ceTypeId" styleClass="textbox_C">
								<html:option value="-1">Please Select</html:option>
								<html:options collection="inspectionTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="inspection" property="subType" styleClass="textbox_C">
								<html:option value="">Please Select</html:option>
								<html:options collection="subTypeList" property="code" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="inspection" property="officerId" styleClass="textbox_C">
								<html:option value="0">Please Select</html:option>
								<html:options collection="userList" property="id" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="inspection" property="ceDate" styleClass="textbox_C" size="8" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('codeEnforcementForm.elements[<%=offset + rows * 6 + 5%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>
						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 6;
					%>
				</logic:present>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addComplaint" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="5">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<td class="tabletitle" width="90%">Complaint Issued</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablelabel" width="9%"><nobr><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addComplaint');" /></td>
						<%
							}
						%>
					</table>
					</td>
				</tr>
				<%
					offset++;
							rows = 0;
				%>
				<logic:present name="codeEnforcementForm" property="complaintArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Sub Type</td>
						<td class="tablelabel" width="16%">Officer</td>
						<td class="tablelabel" width="12%">Date</td>
					</tr>
					<bean:define id="complaintTypeList" name="codeEnforcementForm" property="complaintTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="complaintArray" id="complaint" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="complaint" property="deleted" /> <nested:hidden name="complaint" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="complaint" property="ceTypeId" styleClass="textbox_C">
								<html:option value="-1">Please Select</html:option>
								<html:options collection="complaintTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="complaint" property="subType" styleClass="textbox_C">
								<html:option value="">Please Select</html:option>
								<html:options collection="subTypeList" property="code" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="complaint" property="officerId" styleClass="textbox_C">
								<html:option value="0">Please Select</html:option>
								<html:options collection="userList" property="id" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="complaint" property="ceDate" styleClass="textbox_C" size="8" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('codeEnforcementForm.elements[<%=offset + rows * 6 + 5%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>
						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 6;
					%>
				</logic:present>
			</table>
			<br>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" id="addComplaint" style="border: 1px solid #B7C1CB">
				<tr>
					<td colspan="6">
					<table cellpadding="0" cellspacing="0" border="0" width="100%">
						<td class="tabletitle"  width="90%">Compliance</td>
						<%
							if (editable.equals("true") && status.equals("Active")) {
						%>
						<td class="tablelabel" width="9%"><html:button property="Add" value="Add" styleClass="button" onclick="return addPage('addCompliance');" /></td>
						<%
							}
						%>
					</table>
					</td>
				</tr>
				<%
					offset++;
							rows = 0;
				%>
				<logic:present name="codeEnforcementForm" property="complianceArray">
					<tr>
						<td class="tablelabel" width="2%">Delete</td>
						<td class="tablelabel" width="25%">Type</td>
						<td class="tablelabel" width="45%">Sub Type</td>
						<td class="tablelabel" width="16%">Officer</td>
						<td class="tablelabel" width="12%">Date</td>
					</tr>
					<bean:define id="complianceTypeList" name="codeEnforcementForm" property="complianceTypeList" type="java.util.Collection" />
					<nested:iterate name="codeEnforcementForm" property="complianceArray" id="compliance" scope="session">
						<tr>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:checkbox name="compliance" property="deleted" /> <nested:hidden name="compliance" property="changed" /></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="compliance" property="ceTypeId" styleClass="textbox_C">
								<html:option value="">Please Select</html:option>
								<html:options collection="complianceTypeList" property="ceTypeId" labelProperty="name" />
							</nested:select></td>

							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="compliance" property="subType" styleClass="textbox_C">
								<html:option value="">Please Select</html:option>
								<html:options collection="subTypeList" property="code" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:select name="compliance" property="officerId" styleClass="textbox_C">
								<html:option value="0">Please Select</html:option>
								<html:options collection="userList" property="id" labelProperty="description" />
							</nested:select></td>
							<td style="border-bottom: 1px solid #B7C1CB"><nested:text name="compliance" property="ceDate" styleClass="textbox_C" size="8" maxlength="10" onkeypress="return validateDate();" onblur="DateValidate(this);" /> <a href="javascript:show_calendar('codeEnforcementForm.elements[<%=offset + rows * 6 + 5%>]');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;"> <img src="../images/calendar.gif" width="16" height="15" border="0"></a></td>

						</tr>
						<%
							rows++;
						%>
					</nested:iterate>
					<%
						offset += rows * 6;
					%>
				</logic:present>
			</table>
			</td>
		</tr>
	</table>
	<%} %>
	<html:hidden property="activityId" />
</html:form>
</html:html>

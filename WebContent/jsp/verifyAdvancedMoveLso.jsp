<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : LSO Explorer Frame</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="css/elms.css" type="text/css">
</head>
<%String contextRoot = request.getContextPath();
%>
<SCRIPT language="JavaScript1.2">

function checkRadios() {
  var radiogroup1 = document.forms[0].fromLsoId;
 var radiogroup2 = document.forms[0].toLsoId;

  var itemchecked1 = false;
  var itemchecked2 = false;

   for(var j = 0 ; j < radiogroup1.length ; ++j) {
    if(radiogroup1[j].checked) {
	 itemchecked1 = true;
	// alert('item1 is checked');
	 break;
	}
   }
  for(var j = 0 ; j < radiogroup2.length ; ++j) {
    if(radiogroup2[j].checked) {
	 itemchecked2 = true;
	 //alert('item2 is checked');
	 break;
	}
  }

   if(!itemchecked1) {
       if(document.forms[0].fromLsoId.checked == true){
        itemchecked1 = true;
        }
        else{
        alert("Please select an address on the left to move.");
        return false;
       }
    }

    if(!itemchecked2) {
       if(document.forms[0].toLsoId.checked == true){
        itemchecked2 = true;
        }
        else{
        alert("Please select an address on the right to move.");
        return false;
       }
    }


   checkMove();


}

function checkMove() {
  var fromLSO = document.forms[0].elements['fromLSO'].value;
  var toLSO = document.forms[0].elements['toLSO'].value;
//  alert("from "+fromLSO+"  to "+toLSO);
  	if(fromLSO=="S"){
	     if(toLSO=="L"){submitPage(fromLSO);}
	     else { alert("Structure can only be moved to Land. Please select land to move to "); return false; }
	}
	else if(fromLSO=="O"){
	     if(toLSO=="S"){submitPage(fromLSO);}
	     else { alert("Occupancy can only be moved to Structure. Please select Structure to move to "); return false; }
	}
	else { alert("This move is not permitted"); return false; }


}

function submitPage(str) {
	//alert(str);
	document.forms[0].elements['moveType'].value=str;
	document.forms[0].action='<%=contextRoot%>/saveAdvancedMoveLso.do';
 	document.forms[0].submit();
}

function cancelPage() {
	document.forms[0].action='<%=contextRoot%>/moveLsoAdv.do';
 	document.forms[0].submit();
}

function assignfromLSO(str) {

	document.forms[0].elements['fromLSO'].value=str
}
function assigntoLSO(str) {

	document.forms[0].elements['toLSO'].value=str
}
function init() {

 	for (var i=0; i<document.forms[0].fromLsoId.length; i++)
	document.forms[0].fromLsoId[i].checked = false;

 	for (var i=0; i<document.forms[0].toLsoId.length; i++)
	document.forms[0].toLsoId[i].checked = false;
}



</SCRIPT>

<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg" onload="init()">
<html:form  action="/saveAdvancedMoveLso" >
<html:errors/>
	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	  <tr><td colspan="2">&nbsp;</td><td> &nbsp;  </td></tr>

	    <tr>
	      <td width="99%" background="images/site_bg_B7C1CB.jpg">Move Structure or Occupancy</td>
	      <td width="1%"><img src="images/site_hdr_split_1_tall.jpg" width="26" height="25"></td>
	      <td width="1%" background="images/site_bg_D7DCE2.jpg"><nobr>
			<html:button property="ok" value="  Move  " styleClass="button" onclick="return checkRadios()"   /> &nbsp; <html:button property="cancel" value="Cancel" styleClass="button" onclick="return cancelPage();" />
		&nbsp;</nobr></td>
	    </tr>
	  </table>


	<table width="100%" border="0" cellspacing="0" cellpadding="0">

	<tr><td class="con_hdr_2b">  From &nbsp;</td><td class="con_hdr_2b">  To &nbsp;&nbsp;&nbsp;&nbsp; </td></tr>
	    	<tr>
	    	    <td width="50%" valign="top">&nbsp;
	    	    	<table>
	    	    	  <tr><td>
	    	    	  <font class='treetext'><font color="green">[L]
	    	    	   <logic:iterate id="fromTree" name="fromLsoTreeList" type="elms.app.lso.LsoTree" >
	    	    	   <font class='treetext'>
	    	    	   	<logic:equal name="fromTree" property="type" value="S">
	    	    	   		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">[S]
	    	    	   	</logic:equal>
	    	    	   	<logic:equal name="fromTree" property="type" value="O">
	    	    	   		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue">[O]
	    	    	   	</logic:equal>
	    	    	   	<logic:notEqual name="fromTree" property="type" value="L">
	    	    	   		<html:radio  name="advancedMoveLsoForm" property="fromLsoId"  value="<%= \"\"+fromTree.getId() %>" onclick="<%= \"javascript:assignfromLSO('\"+fromTree.getType()+\"')\" %>" />
	    	    	   	</logic:notEqual>
	    	    	    	<bean:write name="fromTree" property="nodeText"/><br>

	    	    	   </logic:iterate>
	    	    	  </td></tr>

	    	    	</table>
	    	    </td>
	    	    <td width="50%" valign="top">&nbsp;
	    	    	<table>
	    	    	  <tr><td><font class='treetext'><font color="green">[L]
	    	    	   <logic:iterate id="toTree" name="toLsoTreeList" type="elms.app.lso.LsoTree" >
	    	    	   <font class='treetext'>
	    	    	   	<logic:equal name="toTree" property="type" value="S">
	    	    	   		 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">[S]
	    	    	   	</logic:equal>
	    	    	   	<logic:equal name="toTree" property="type" value="O">
	    	    	   		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font color="blue">[O]
	    	    	   	</logic:equal>
	    	    	   	<html:radio name="advancedMoveLsoForm" property="toLsoId"   value="<%= \"\"+toTree.getId() %>" onclick="<%= \"javascript:assigntoLSO('\"+toTree.getType()+\"')\" %>" />
	    	    	    	<bean:write name="toTree" property="nodeText"/><br>

	    	    	   </logic:iterate>
	    	    	  </td></tr>
	    	    	</table>
	    	    </td>
	    	</tr>
	</table>
<html:hidden name="advancedMoveLsoForm" property="moveType" value="" />

<html:hidden  property="moveNumber"   />
<html:hidden  property="moveName"  />
<html:hidden  property="toName"  />
<html:hidden  property="toNumber"   />

<html:hidden property="fromLSO" value="" />
<html:hidden property="toLSO" value=""/>
</html:form>

</body>
</html:html>

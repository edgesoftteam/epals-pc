<%@ page import='elms.agent.*,java.util.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />

<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:errors/>
<%!
List userList = new ArrayList();
 %>
<%

pageContext.setAttribute("userList", userList);

try{
   java.util.List approvalStatuses = LookupAgent.getApprovalStatuses();
   pageContext.setAttribute("approvalStatuses", approvalStatuses);
  }
catch(Exception e){}
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   String activityId = (String)request.getParameter("activityId");

   if(activityId ==null) activityId = "";
   if (lsoAddress == null ) lsoAddress = "";
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";

       String approvalId = (String) request.getAttribute("approvalId");
	   String applicationDateString = (String) request.getAttribute("applicationDateString");
	   String departmentName = (String) request.getAttribute("departmentName");
	   String userName = (String) request.getAttribute("userName");

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript">
function saveApproval()
{

		   var activityId=document.forms[0].activityId.value;
			document.forms[0].action="<%=contextRoot%>/saveBusinessLicenseApproval.do?activityId="+activityId;

			document.forms[0].submit();
	}

</script>

<html:form action="/addBusinessLicenseApproval">
<html:hidden property="activityId" value="<%=activityId%>" />
<html:hidden property="approvalId" value="<%=approvalId%>" />
<html:hidden property="applicationDateString" value="<%=applicationDateString%>" />
<html:hidden property="departmentName" value="<%=departmentName%>" />
<html:hidden property="userName" value="<%=userName%>" />

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Approval Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br></td>
			</tr>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" class="tabletitle">Add</td>
										<td width="1%" class="tablebutton">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="javascript:history.back(1);" />&nbsp;
												<html:button property="save" value="Save" styleClass="button" onclick="javascript:saveApproval()" /> &nbsp;
											</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>

								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="2" cellpadding="2">
									<tr>
										<td class="tablelabel">Approving Department:  <%=departmentName%></td>
										<td class="tablelabel">Date Referred:  <%=applicationDateString%></td>
										<td class="tablelabel">Approver:  <%=userName%></td>
									</tr>
									<tr valign="top">
										<td class="tablelabel">Approval Status:</td>
										<td class="tabletext">
										<html:select property="approvalStatusId" styleClass="textbox">
											<html:options collection="approvalStatuses" property="id"
												labelProperty="description" />
										</html:select>
										</td>
										<td class="tablelabel">Approval Date:</td>
                                        <td class="tabletext"  valign="top"><nobr>
                                           <html:text  property="approvalDate" size="10" maxlength="10"  styleClass="textboxd"/>
					                       <html:link href="javascript:show_calendar('forms[0].approvalDate');"
                                            onmouseover="window.status='Calendar';return true;"
                                            onmouseout="window.status='';return true;">
                                            <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                                        </td>

									</tr>
									<tr>
										<td class="tablelabel">Comments</td>
										<td class="tabletext" colspan="2"><html:textarea
											property="comments" cols="75" rows="7" styleClass="textbox" /></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="32">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>
				</td>
			</tr>
	</table>
</html:form>
<html:form action="/addBusinessLicenseApproval" >

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<TBODY>
								<tr>
									<td>
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
										  <td width="98%" class="tabletitle">Approval History</td>
										  <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="21" height="20"></td>
										  <td width="1%" class="tablelabel"><nobr>
										  <html:submit value="Add" styleClass="button" />
										</tr>
									</table>
									</td>
								</tr>
								<tr>
									<td background="../images/site_bg_B7C1CB.jpg">
									<table width="100%" border="0" cellspacing="1" cellpadding="2">
										<TBODY>
											<tr>
												<td class="tablelabel"><nobr>Department</nobr></td>

												<td class="tablelabel"><nobr>Date Referred</nobr></td>
												<td class="tablelabel">Approval Status</td>
												<td class="tablelabel">Approved By</td>
												<td class="tablelabel">Approved Date</td>
												<td class="tablelabel">Comments</td>
											</tr>


												<tr valign="top">

													<td class="tabletext"><nested:write
														property="departmentName" /></td>
													<td class="tabletext"><nested:write
														property="referredDate" /></td>
													<td class="tabletext"><nested:write
														property="approvalStatusId" /></td>
													<td class="tabletext"><nested:write
														property="approvalDate" /></td>
													<td class="tabletext"><nested:write
														property="approvalDate" /></td>
													<td class="tabletext"><nested:write
														property="comments" /></td>
												</tr>

										</TBODY>
									</table>
									</td>
								</tr>
							</TBODY>
						</table>
						</td>
					</tr>
					</TBODY>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<TBODY>
						<tr>
							<td height="32">&nbsp;</td>
						</tr>
					</TBODY>
				</table>
				</td>
			</tr>
		</TBODY>
	</table>
</html:form>


</body>
</html:html>


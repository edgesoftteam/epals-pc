<%@ page import='elms.agent.*,java.util.*,elms.app.bl.*,elms.common.*'%>
<%@ page import='elms.control.beans.BusinessLicenseActivityForm'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<%
String contextRoot = request.getContextPath();
String actId=(String)request.getAttribute("activityId");
String lsId=(String)request.getAttribute("lsoId");
%>
<title>City of Burbank : Permitting And Licensing System : Add Business Activity</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/yCodelib.js"></script>
<script>checkBrowser();</script>
<script src="../script/yCode_combo_box.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/editActivity.js"></script>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript">
// For Checking the Activity status as Issued
function checkActivityStatus(){
   if(document.forms[0].activityStatus.value == '1053'){
		document.getElementById("actStatus").style.display="block";
		document.getElementById("issueDisplay").style.display="none";
	}else if(document.forms[0].activityStatus.value == '1054'){
		document.getElementById("actStatus").style.display="none";
 	   	document.getElementById("issueDisplay").style.display="block";
		issueDateHideForOutOFBusiness();
	}else{
	    document.getElementById("actStatus").style.display="none";
		document.getElementById("issueDisplay").style.display="block";
	}
 	var chkStat = checkStatus();
	if(chkStat == true){
		return true;
	}else{
		return false;
	}
return true;
}//write this in activity Status - onchange="return checkActivityStatus()"

function issueDateHide()
{
document.getElementById("issueDisplay").style.display="none";
  if(document.forms[0].activityStatus.value == '1053'){
    document.getElementById("actStatus").style.display="block";
    document.getElementById("issueDisplay").style.display="none";
	}else{
	document.getElementById("actStatus").style.display="none";
    document.getElementById("issueDisplay").style.display="block";
	}
return true;
}//write this in form - onload="issueDateHide();"

function issueDateHideForOutOFBusiness()
{
	document.getElementById("issueDisplay").style.display="none";
  	if(document.forms[0].activityStatus.value == '1054'){
		document.forms[0].issueDate.value = '';
	}else{
		document.getElementById("actStatus").style.display="none";
    	document.getElementById("issueDisplay").style.display="block";
	}
return true;
}
</script>
</head>

<%!
List activityStatuses = new ArrayList();
List qtytList = new ArrayList();
List exemtList = new ArrayList();
List ownershipTypes= new ArrayList();
%>
<%
String outstandingFees=(String)request.getAttribute("outstandingFees");
try{

 activityStatuses = LookupAgent.getBLActivityStatuses(Constants.MODULE_NAME_BUSINESS_LICENSE);
pageContext.setAttribute("activityStatuses", activityStatuses);

 qtytList = LookupAgent.getQuantityTypes();
pageContext.setAttribute("qtytList", qtytList);

 exemtList = LookupAgent.getExemptionTypes();
pageContext.setAttribute("exemtList", exemtList);

 ownershipTypes = LookupAgent.getOwnershipTypes();
pageContext.setAttribute("ownershipTypes", ownershipTypes);

}catch(Exception e){//ignored
}
%>

<%
String addressStreetNumber1 = "";
String addressStreetFraction1 = "";
String addressStreetName1 = "";
String addressUnitNumber1 = "";
String addressCity1 = "";
String addressState1 = "";
String addressZip1 = "";
String addressZip41 = "";


String businessLocation1=request.getParameter("businessLocation1");
addressStreetNumber1=(String)request.getParameter("addressStreetNumber1");
addressStreetFraction1=request.getParameter("addressStreetFraction1");
addressStreetName1=request.getParameter("addressStreetName1");
addressUnitNumber1=request.getParameter("addressUnitNumber1");
addressCity1=request.getParameter("addressCity1");
addressState1=request.getParameter("addressState1");
addressZip1=request.getParameter("addressZip1");
addressZip41=request.getParameter("addressZip41");
 %>

<script language="JavaScript" type="text/javascript">

function save()
{
var i=0;

<% if(businessLocation1 == "false"|| businessLocation1.equals("false")){ %>
	if(document.forms[0].activityStatus.value ==""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	i++;
	}else if(document.forms[0].outOfTownStreetName.value==""){
	alert("Please enter value for Out Of Town Street Name");
	document.forms[0].outOfTownStreetName.focus();
	i++;
	}else if(document.forms[0].outOfTownCity.value==""){
	alert("Please enter value for City ");
	document.forms[0].outOfTownCity.focus();
	i++;
	}else if(document.forms[0].outOfTownState.value==""){
	alert("Please enter value for State");
	document.forms[0].outOfTownState.focus();
	i++;
	}else if(document.forms[0].outOfTownZip.value==""){
	alert("Please enter value for Out Of Town Zip");
	document.forms[0].outOfTownZip.focus();
	i++;
	}else if(document.forms[0].businessName.value == ""){
	alert("Please enter Business Name");
	document.forms[0].businessName.focus();
	i++;
	}
	/* else if(isNaN(document.forms[0].mailStreetNumber.value)){
	alert("Please enter numeric for Mailing Street Number ");
	document.forms[0].mailStreetNumber.focus();
	i++;
	}else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value==""){
	alert("Please enter mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}else if(document.forms[0].mailStreetName.value=="" && document.forms[0].mailCity.value!="" && document.forms[0].mailState.value!="" && document.forms[0].mailZip.value!=""){
	alert("Please enter  mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailCity.value == ""){
	alert("Please enter  mailing city ");
	document.forms[0].mailCity.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value != "" && document.forms[0].mailState.value == ""){
	alert("Please enter  mailing state ");
	document.forms[0].mailState.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailZip.value == ""){
	alert("Please enter  mailing zipcode ");
	document.forms[0].mailZip.focus();
	i++;
	}
	else if((document.forms[0].mailState.value != "") && (!isNaN(document.forms[0].mailState.value))){
	alert("Please enter non numeric for Mailing State");
	document.forms[0].mailState.focus();
	i++;
    }else if((document.forms[0].prevState.value != "") && (!isNaN(document.forms[0].prevState.value)))
	{
	alert("Please enter non numeric for Previous State");
	document.forms[0].prevState.focus();
	i++;
	}else if((document.forms[0].prevZip.value != "") && (isNaN(document.forms[0].prevZip.value)))
	{
	alert("Please enter numeric for Previous Zipcode");
	document.forms[0].prevZip.focus();
	i++;
	} */
	else if(isNaN(document.forms[0].elements['multiAddress[0].streetNumber'].value)){
		alert("Please enter numeric for Mailing Street Number ");
		document.forms[0].elements['multiAddress[0].streetNumber'].focus();
		i++;
		}else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value==""){
		alert("Please enter mailing Street Name ");
		document.forms[0].elements['multiAddress[0].streetName1'].focus();
		i++;
		}

		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].city'].value == ""){
		alert("Please enter  mailing city ");
		document.forms[0].elements['multiAddress[0].city'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value != "" && document.forms[0].elements['multiAddress[0].state'].value == ""){
		alert("Please enter  mailing state ");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].zip'].value == ""){
		alert("Please enter  mailing zipcode ");
		document.forms[0].elements['multiAddress[0].zip'].focus();
		i++;
		}
		else if((document.forms[0].elements['multiAddress[0].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[0].state'].value))){
		alert("Please enter non numeric for Mailing State");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
	    }else if(isNaN(document.forms[0].elements['multiAddress[1].streetNumber'].value)){
		alert("Please enter numeric for Previous Street Number ");
		document.forms[0].elements['multiAddress[1].streetNumber'].focus();
		i++;
		}else if((document.forms[0].elements['multiAddress[1].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[1].state'].value))){
		alert("Please enter non numeric for Previous State");
		document.forms[0].elements['multiAddress[1].state'].focus();
		i++;
	    }else if((document.forms[0].elements['multiAddress[1].zip'].value != "") && (isNaN(document.forms[0].elements['multiAddress[1].zip'].value))){
		alert("Please enter numeric for Previous Zipcode");
		document.forms[0].elements['multiAddress[1].zip'].focus();
		i++;
	    } 
	else if (i>0 ){
	document.forms[0].action="";
	}
	else{
	var updateAct = "update";
	var actId=document.forms[0].activityId.value;
	var lsId=document.forms[0].lsoId.value;
	var chkStatus = true;
	chkStatus = checkStatus();
	if(chkStatus == true){
		document.forms[0].action='<%=contextRoot%>/editBusinessLicenseActivity.do?updateAct='+updateAct+'&actId='+actId+'&lsId='+lsId+'&businessLocation1=<%=businessLocation1%>&addressStreetNumber1=<%=addressStreetNumber1%>&addressStreetFraction1=<%=addressStreetFraction1%>&addressStreetName1=<%=addressStreetName1%>&addressUnitNumber1=<%=addressUnitNumber1%>&addressCity1=<%=addressCity1%>&addressState1=<%=addressState1%>&addressZip1=<%=addressZip1%>&addressZip41=<%=addressZip41%>';
		document.forms[0].submit();
		return true;
	}else{
		return false;
	}
	}
<%}else{%>
	if(document.forms[0].activityStatus.value == ""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	i++;
	}else if(document.forms[0].businessName.value == ""){
	alert("Please enter Business Name");
	document.forms[0].businessName.focus();
	i++;
	}
	/* else if(isNaN(document.forms[0].mailStreetNumber.value)){
	alert("Please enter numeric for Mailing Street Number ");
	document.forms[0].mailStreetNumber.focus();
	i++;
	}else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value==""){
	alert("Please enter mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}else if(document.forms[0].mailStreetName.value=="" && document.forms[0].mailCity.value!="" && document.forms[0].mailState.value!="" && document.forms[0].mailZip.value!=""){
	alert("Please enter  mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailCity.value == ""){
	alert("Please enter  mailing city ");
	document.forms[0].mailCity.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value != "" && document.forms[0].mailState.value == ""){
	alert("Please enter  mailing state ");
	document.forms[0].mailState.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailZip.value == ""){
	alert("Please enter  mailing zipcode ");
	document.forms[0].mailZip.focus();
	i++;
	}
	else if((document.forms[0].mailState.value != "") && (!isNaN(document.forms[0].mailState.value))){
	alert("Please enter non numeric for Mailing State");
	document.forms[0].mailState.focus();
	i++;
    }else if((document.forms[0].prevState.value != "") && (!isNaN(document.forms[0].prevState.value)))
	{
	alert("Please enter non numeric for Previous State");
	document.forms[0].prevState.focus();
	i++;
	}else if((document.forms[0].prevZip.value != "") && (isNaN(document.forms[0].prevZip.value)))
	{
	alert("Please enter numeric for Previous Zipcode");
	document.forms[0].prevZip.focus();
	i++;
	} */
	else if(isNaN(document.forms[0].elements['multiAddress[0].streetNumber'].value)){
		alert("Please enter numeric for Mailing Street Number ");
		document.forms[0].elements['multiAddress[0].streetNumber'].focus();
		i++;
		}else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value==""){
		alert("Please enter mailing Street Name ");
		document.forms[0].elements['multiAddress[0].streetName1'].focus();
		i++;
		}

		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].city'].value == ""){
		alert("Please enter  mailing city ");
		document.forms[0].elements['multiAddress[0].city'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value != "" && document.forms[0].elements['multiAddress[0].state'].value == ""){
		alert("Please enter  mailing state ");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].zip'].value == ""){
		alert("Please enter  mailing zipcode ");
		document.forms[0].elements['multiAddress[0].zip'].focus();
		i++;
		}
		else if((document.forms[0].elements['multiAddress[0].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[0].state'].value))){
		alert("Please enter non numeric for Mailing State");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
	    }else if(isNaN(document.forms[0].elements['multiAddress[1].streetNumber'].value)){
		alert("Please enter numeric for Previous Street Number ");
		document.forms[0].elements['multiAddress[1].streetNumber'].focus();
		i++;
		}else if((document.forms[0].elements['multiAddress[1].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[1].state'].value))){
		alert("Please enter non numeric for Previous State");
		document.forms[0].elements['multiAddress[1].state'].focus();
		i++;
	    }else if((document.forms[0].elements['multiAddress[1].zip'].value != "") && (isNaN(document.forms[0].elements['multiAddress[1].zip'].value))){
		alert("Please enter numeric for Previous Zipcode");
		document.forms[0].elements['multiAddress[1].zip'].focus();
		i++;
	    } 
	else if (i>0 ){
	document.forms[0].action="";
	}
	else{
	var updateAct = "update";
	var actId=document.forms[0].activityId.value;
	var lsId=document.forms[0].lsoId.value;
	var chkStatus = true;
	chkStatus = checkStatus();
	if(chkStatus == true){
		document.forms[0].action='<%=contextRoot%>/editBusinessLicenseActivity.do?updateAct='+updateAct+'&actId='+actId+'&lsId='+lsId+'&businessLocation1=<%=businessLocation1%>&addressStreetNumber1=<%=addressStreetNumber1%>&addressStreetFraction1=<%=addressStreetFraction1%>&addressStreetName1=<%=addressStreetName1%>&addressUnitNumber1=<%=addressUnitNumber1%>&addressCity1=<%=addressCity1%>&addressState1=<%=addressState1%>&addressZip1=<%=addressZip1%>&addressZip41=<%=addressZip41%>';
		document.forms[0].submit();
		return true;
	}else{
		return false;
	}
	}
<%}%>
}

function NumericEntry()
{
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
	return false;

	}
}

function NonNumericEntry()
{

	if (!( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
	event.returnValue = false;

	}

}
function DisplayPhoneHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}else{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 ))
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
}
function DisplaySSNHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}else{
		if (document.forms[0].elements[str].value.length == 3)
		{
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';

 		}
		if ((document.forms[0].elements[str].value.length == 6))  document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		if (document.forms[0].elements[str].value.length > 10 )  event.returnValue = false;
	}
}


function validateEmailId(str){
	if(document.forms[0].emailAddress.value!=""){
			var validemail= validateEmail(document.forms[0].emailAddress.value);
			if(validemail==false){
				alert('Please enter correct E-mail Id');
				document.forms[0].emailAddress.value="";
				document.forms[0].emailAddress.focus();
				return false;
			}
		}
}
function goBack() {
  parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=actId%>';
}
</script>

<html:errors/>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0" onload="issueDateHide();">
<html:form action="/editBusinessLicenseActivity.do">
<html:hidden property="activityId" value="<%=actId%>"/>
<html:hidden property="lsoId" value="<%=lsId%>"/>
<html:hidden property="outStandingFees" value="<%=outstandingFees%>"/>
<bean:define id="businessLicenseActivityForm" name="businessLicenseActivityForm" type="elms.control.beans.BusinessLicenseActivityForm"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><font class="con_hdr_3b">Edit Activity</font><br><br></td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="99%" class="tabletitle" align="left">Edit Activity</td>
							<td width="1%" class="tablebutton" align="right"><nobr>
								<html:button  property="Cancel"  value="Cancel" styleClass="button" onclick="goBack()"/>
								<html:button property="Save" value="Save" styleClass="button" onclick="javascript:save();"/></nobr>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td background="../images/site_bg_B7C1CB.jpg">
					<table width="100%" cellpadding="2" cellspacing="1">
						<tr>
							<td class="tablelabel" width="20%">Activity #</td>
							<td colspan="3" class="tabletext"> <bean:write name="businessLicenseActivityForm"  property="activityNumber"/></td>
							<td class="tablelabel" width="20%">Business Name</td>
							<td colspan="2" class="tabletext"><html:text property="businessName" size="20" maxlength="50" styleClass="textbox" /></td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Activity Type</td>
							<td colspan="3" class="tabletext"><bean:write name="businessLicenseActivityForm"  property="activityTypeDescription"/></td>
							<td class="tablelabel" width="20%">Activity Status</td>
							<td colspan="2" class="tabletext">
								<html:select property="activityStatus" size="1" styleClass="textbox" onchange="return checkActivityStatus();">
									<html:option value="">Please Select</html:option>
									<html:options collection="activityStatuses" property="status" labelProperty="description"/>
								</html:select><BR>
							</td>
						</tr>
						<% if(businessLocation1 == "false"||businessLocation1.equals("false")){ %>

						<tr>
							<td class="tablelabel" width="25%">Out of Town Address</td>
							<td colspan="3" class="tabletext">
								<html:text size="5" property="outOfTownStreetNumber" styleClass="textbox"/>
								<html:text size="14" property="outOfTownStreetName" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
							<td  colspan="3" class="tabletext" valign="top">
								<html:text size="5" property="outOfTownUnitNumber" styleClass="textbox"/>
								<html:text size="10" property="outOfTownCity" styleClass="textbox"/>
								<html:text size="2" maxlength="2" property="outOfTownState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<html:text size="5" maxlength="5" property="outOfTownZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<html:text size="4" property="outOfTownZip4" styleClass="textbox"/>
							</td>
						</tr>
						<%}else{%>
						<tr valign="top">
                                <td class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="6"><%=addressStreetNumber1%> <%=addressStreetFraction1%> <%=addressStreetName1%> <%=addressUnitNumber1%> <%=addressCity1%> <%=addressState1%> <%=addressZip1%> <%=addressZip41%>

								</td>
                            </tr>
               			<tr>
						<%}%>
							<td class="tablelabel" width="20%">Corporate Name</td>
							<td colspan="3" class="tabletext">
								<html:text property="corporateName" size="20" maxlength="50" styleClass="textbox"/>
								</td>
							<td class="tablelabel" width="20%">Business Account Number</td>
							<td colspan="2" class="tabletext" colspan="3"><bean:write name="businessLicenseActivityForm"  property="businessAccountNumber"/></td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Business Phone, Ext</td>
							<td colspan="3" class="tabletext">
								<html:text size="14" property="businessPhone" maxlength="12" onkeypress="return DisplayPhoneHyphen('businessPhone');" styleClass="textbox"/>&nbsp;
		                        <html:text size="4" maxlength="4" property="businessExtension" styleClass="textbox" onkeypress="return NumericEntry()"/>&nbsp;<BR>
							</td>
					       	<td class="tablelabel" width="20%">Business Fax</td>
							<td colspan="2" class="tabletext">
								<html:text size="14" property="businessFax" maxlength="12" onkeypress="return DisplayPhoneHyphen('businessFax');" styleClass="textbox"/><BR>
					       </td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Creation Date</td>
							<td colspan="3" class="tabletext"><nobr>
								<html:text  property="creationDate" size="10" maxlength="10"  styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].creationDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                 	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
							<td class="tablelabel" width="20%">Issue Date</td>
							<td colspan="2" class="tabletext" id="actStatus"><nobr>
								<html:text   property="issueDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								 	<html:link href="javascript:show_calendar('forms[0].issueDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>

						<td class="tabletext" id="issueDisplay"><nobr>
							<bean:write name="businessLicenseActivityForm"  property="issueDate"/></nobr>
						</td>

						</tr>
						<tr>
							<td class="tablelabel" width="20%">Application Date</td>
							<td colspan="3" class="tabletext"><nobr>
								<html:text  property="applicationDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].applicationDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                 	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
							<td class="tablelabel" width="20%">Renewal Date</td>
							<td colspan="2" class="tabletext"><nobr>
								<html:text   property="renewalDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								 	<html:link href="javascript:show_calendar('forms[0].renewalDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Starting Date</td>
							<td colspan="3" class="tabletext"><nobr>
								<html:text  property="startingDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].startingDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
							<td class="tablelabel" width="20%">Out of Business Date</td>
							<td colspan="2" class="tabletext"><nobr>
								<html:text  property="outOfBusinessDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].outOfBusinessDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
							<img src="../images/calendar.gif" width="16" height="15" border=0/>

								</html:link></nobr>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Home Occupation</td>
							<td colspan="3" class="tabletext">
								<html:checkbox property="homeOccupation" styleClass="textbox" >Yes</html:checkbox><BR>
							</td>
							<td class="tablelabel" width="20%">Decal Code</td>
							<td colspan="2" class="tabletext">
						    	<html:checkbox property="decalCode" styleClass="textbox">Yes</html:checkbox>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Federal ID Number</td>
							<td colspan="3" class="tabletext">
								<html:text size="20" maxlength="10" property="federalIdNumber" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="20%">Social Security Number</td>
							<td colspan="2" class="tabletext">
								<html:text size="14" property="socialSecurityNumber" maxlength="12" onkeypress="return DisplaySSNHyphen('socialSecurityNumber');" styleClass="textbox"/>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Description of Business</td>
							<td colspan="8" class="tabletext">
								<html:textarea rows="5" cols="40" property="descOfBusiness" styleClass="textbox"></html:textarea><BR>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Ownership Type</td>
							<td colspan="3" class="tabletext">
								 <html:select property="ownershipType" size="1" styleClass="textbox">
									<html:option value="">Please Select</html:option>
									<html:options collection="ownershipTypes" property="id" labelProperty="description"/>
								</html:select><BR>
							</td>
							<td class="tablelabel" width="20%">E-mail Address</td>
							<td colspan="2" class="tabletext">
								<html:text size="20" property="emailAddress" styleClass="textbox" onblur="return validateEmailId('emailAddress');"/><BR>
							</td>
						</tr>
						<%-- <tr>
							<td class="tablelabel" width="25%">Mailing Address</td>
							<td colspan="3" class="tabletext">
								<html:text size="5" maxlength="80" property="mailStreetNumber" styleClass="textbox"/>
								<html:text size="14" maxlength="80" property="mailStreetName" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
							<td  colspan="3" class="tabletext" valign="top">
								<html:text size="5" maxlength="80" property="mailUnitNumber" styleClass="textbox"/>
								<html:text size="10" maxlength="80" property="mailCity" styleClass="textbox"/>
								<html:text size="2" maxlength="2" property="mailState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<html:text size="6" maxlength="6" property="mailZip" styleClass="textbox" />
								<html:text size="4" property="mailZip4" styleClass="textbox"/>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="25%">Previous Address</td>
							<td colspan="3" class="tabletext">
								<html:text size="5" property="prevStreetNumber" styleClass="textbox"/>
								<html:text size="14" property="prevStreetName" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
							<td  colspan="3" class="tabletext" valign="top">
								<html:text size="5" property="prevUnitNumber" styleClass="textbox"/>
								<html:text size="10" property="prevCity" styleClass="textbox"/>
								<html:text size="2" maxlength="2" property="prevState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<html:text size="5" maxlength="5" property="prevZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<html:text size="4" property="prevZip4" styleClass="textbox"/>
							</td>
						</tr> --%>
						<tr>
							<td class="tablelabel" width="25%">Insurance Expiration Date</td>
							<td colspan="3"  width="25%"  class="tabletext"><nobr>
								<html:text property="insuranceExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].insuranceExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				                    	<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
							<td class="tablelabel" width="20%">Bond Expiration Date</td>
							<td colspan="2" class="tabletext"><nobr>
								<html:text property="bondExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].bondExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					            		<img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Department of Justice Expiration Date</td>
							<td colspan="3" class="tabletext"><nobr>
								<html:text property="deptOfJusticeExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].deptOfJusticeExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
									</html:link></nobr>
							</td>
							<td class="tablelabel" width="20%">Federal Firearms License Expiration Date</td>
							<td colspan="2" class="tabletext"><nobr>
								<html:text property="federalFirearmsLiscExpDate" size="10" maxlength="10"  styleClass="textbox" onblur="DateValidate(this);"/>
									<html:link href="javascript:show_calendar('forms[0].federalFirearmsLiscExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
					                     <img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Square Footage(Area Occupied)</td>
							<td colspan="3" class="tabletext">
								<html:text size="20" property="squareFootage" styleClass="textbox"/><BR>
							</td>
								<td class="tablelabel" width="20%">Quantity</td>
								<td colspan="2" class="tabletext">
					                <html:select property="quantity" size="1" styleClass="textbox">
										<html:option value="">Please Select</html:option>
										<html:options collection="qtytList" property="id" labelProperty="description"/>
									</html:select>
									<html:text size="20" property="quantityNum" styleClass="textbox"  onkeypress="return NumericEntry()"/>
								</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Type of Exemptions</td>
							<td colspan="6" class="tabletext">
				                <html:select property="typeOfExemptions" size="1" styleClass="textbox">
									<html:option value="">Please Select</html:option>
									<html:options collection="exemtList" property="id" labelProperty="description"/>
								</html:select>
							</td>
						</tr>

						</table>
					</td>
				</tr>
				<%--  <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            
             <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Business Address</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    
               
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
            
            <% if(businessLocation1 == "false"||businessLocation1.equals("false")){ %>

						<tr>
							<td class="tablelabel" width="25%">Out of Town Address</td>
							<td colspan="3" class="tabletext">
								<html:text size="5" property="outOfTownStreetNumber" styleClass="textbox"/>
								<html:text size="14" property="outOfTownStreetName" styleClass="textbox"/>
							</td>
							<td class="tablelabel" width="25%">Unit,City,State,Zip,Zip4</td>
							<td  colspan="3" class="tabletext" valign="top">
								<html:text size="5" property="outOfTownUnitNumber" styleClass="textbox"/>
								<html:text size="10" property="outOfTownCity" styleClass="textbox"/>
								<html:text size="2" maxlength="2" property="outOfTownState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<html:text size="5" maxlength="5" property="outOfTownZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<html:text size="4" property="outOfTownZip4" styleClass="textbox"/>
							</td>
						</tr>
						<%}else{%>
						<tr valign="top">
                                <td  width="25%"  class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="6"><%=addressStreetNumber1%> <%=addressStreetFraction1%> <%=addressStreetName1%> <%=addressUnitNumber1%> <%=addressCity1%> <%=addressState1%> <%=addressZip1%> <%=addressZip41%>

								</td>
                            </tr>
               			<tr>
						<%}%>
            
            </tr></table></td></tr></table></td>
 --%>            	<tr>
            	<td>&nbsp;</td>
            	</tr>
            
				<nested:iterate  name="businessLicenseActivityForm" property="multiAddress" type="elms.app.common.MultiAddress">
				<nested:hidden property="id"></nested:hidden>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"> <nested:write property="addressType"></nested:write></td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    
               
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							
							<tr valign="top">
                                <td width="20%" class="tablelabel">Street Number , Street Name1</td>
                                <td width="20%" class="tabletext">
                                <nested:text size="5"  property="streetNumber" styleClass="textbox" onkeypress="return NumericEntry()"/>
               					<nested:text size="14" property="streetName1" styleClass="textbox"/>
                                 </td>
                                <td width="20%" class="tablelabel">Street Name2</td>
                                <td width="20%" class="tabletext">
                                <nested:text size="14" property="streetName2" styleClass="textbox"/></td>
                            </tr>
                            <tr valign="top">
                                <td width="20%" class="tablelabel"> Unit,City,State,Zip,Zip4</td>
                                <td width="20%" class="tabletext">
								<nested:text size="5" property="unit" styleClass="textbox"/>
								<nested:text size="10" property="city" styleClass="textbox"/>
								<nested:text size="2" maxlength="2" property="state" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<nested:text size="5" maxlength="5" property="zip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<nested:text size="4" property="zip4" styleClass="textbox"/></td>
                                <td width="20%" class="tablelabel">Attn</td>
                                <td width="20%" class="tabletext">
                                <nested:text size="10" property="attn" styleClass="textbox"/>
                                </td>
                            </tr>
                   </table>
                        </td>
                    </tr>
                    
                    </table>
                        </td>
                    </tr>
                      <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
                    </nested:iterate>
                    
                    
                   <%--  <%
			if(businessLicenseActivityForm.getMultiAddress() != null){
			for(int i=0; i< businessLicenseActivityForm.getMultiAddress().length; i++){ %>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">	<%=	businessLicenseActivityForm.getMultiAddress()[i].getAddressType()%>	</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
                                <td width="20%" class="tablelabel">Street Number , Street Name1</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivityForm.getMultiAddress()[i].getStreetNumber()%>&nbsp; <%=businessLicenseActivityForm.getMultiAddress()[i].getStreetName1()%> </td>
                                <td width="20%" class="tablelabel">Street Name2</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivityForm.getMultiAddress()[i].getStreetName2()%></td>
                            </tr>
                            <tr valign="top">
                                <td width="20%" class="tablelabel"> Unit,City,State,Zip,Zip4</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivityForm.getMultiAddress()[i].getUnit()%>&nbsp;<%=businessLicenseActivityForm.getMultiAddress()[i].getCity()%>&nbsp; <%=businessLicenseActivityForm.getMultiAddress()[i].getState()%>&nbsp;<%=businessLicenseActivityForm.getMultiAddress()[i].getZip()%>&nbsp;<%=businessLicenseActivityForm.getMultiAddress()[i].getZip4()%> </td>
                                <td width="20%" class="tablelabel">Attn</td>
                                <td width="20%" class="tabletext"><%=businessLicenseActivityForm.getMultiAddress()[i].getAttn()%></td>
                            </tr>
                   </table>
                        </td>
                    </tr>
                    
                    </table>
                        </td>
                    </tr>
                    
                     <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
					<%} }%>
                     --%>
                    
                    
                     <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
				
				
				
			</table>
		</td>
	</tr>
</table>
</td>
</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>
</table>
	<td width="1%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="32">&nbsp;</td>
			</tr>
		</table>
	</td>
</html:form>
</body>
</html:html>
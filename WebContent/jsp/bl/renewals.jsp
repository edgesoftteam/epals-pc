<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
String contextRoot = request.getContextPath();
CachedRowSet crset = (CachedRowSet) request.getAttribute("importStatus");
int size = crset.size();
if(crset !=null) crset.beforeFirst();
String message = (String)request.getAttribute("message");
if(message==null) message = "";
%>
<html:html>
<HEAD>
<html:base/>
<TITLE>City of Beverly Hills : Online Business Center : Renewals Log</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<logic:present name="enableAutoRefresh" scope="session">
	<META HTTP-EQUIV="refresh" CONTENT="5">
</logic:present>
<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="javascript">
function radio_button_checker(){
	// set var radio_choice to false
	var radio_choice = false;

	// Loop from zero to the one minus the number of radio button selections
	for (counter = 0; counter < batchPrintForm.actType.length; counter++)
	{
	// If a radio button has been selected it will return true
	// (If not it will return false)
	if (batchPrintForm.actType[counter].checked)
	radio_choice = true;
	}

	if (!radio_choice)
	{
	// If there were no selections made display an alert box
	return (false);
	}
	return (true);
}

function startImport() {
	  if(radio_button_checker()){
	  	userInput = confirm("Are you sure you want to start the Renewal letter generation process ?");
	  	if (userInput==true) {
       		return true;
   		}
   		else{
   			return false;
   		}
	  }
	  else{
	  	alert("Please choose the type of renewals");
	  	return false;
	  }
}
function deleteAllRecords() {
   userInput = confirm("Are you sure you want to delete all Records?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/dotRenewalLetters.do?action=delete';
   }
}


</script>


</HEAD>

<BODY >

<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
<form name="batchPrintForm" action="<%=request.getContextPath()%>/dotRenewalLetters.do?action=startImport" method="post" >

  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">DOT Renewals</FONT>
          <logic:present name="showImportInProgress" >
          	(Script run in progress..)
          </logic:present>
			<BR><BR>
			<table bgcolor='#FFFFCC'>
				<tr>
					<td>

		            	<FONT class="blue2b">Please make sure that you generate PINs before generating renewal letters. This is only required once.</FONT>
        		    </td>
	            </tr>
	        </table>
	        </BR>

            </TD>
        </TR>

        <% if(!(message.equalsIgnoreCase(""))){%>
        <TR>
        <td>
        <font color='green'><b><%=message%></b>
        </td>
        </tr>
        <%}%>

        <logic:equal name="showStartImport" value="yes">

        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Choose type of renewals</TD>
                      <TD width="1%" class="tablebutton"><NOBR>

	                      <input type="submit" value="Generate Renewals" onclick="return startImport()"/>

                      </NOBR></TD>

                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
            	    <TR valign="top">
	 					<TD class="tabletext">

								<input type = "radio" name = "actType" value="DOTON">Overnight<br>

             			</TD>
	 					<TD class="tabletext">

								<input type = "radio" name = "actType" value="DOTPPP-R1">PPP (R-1)<br>

             			</TD>
	 					<TD class="tabletext">

								<input type = "radio" name = "actType" value="DOTPPP-R4">PPP (R-4)<br>

             			</TD>
	 					<TD class="tabletext">

								<input type = "radio" name = "actType" value="BOTH">BOTH<br>

             			</TD>
					</TR>
            	    <TR valign="top">

	 					<TD class="tabletext" colspan="4"> Pages per File: &nbsp;

	 							<SELECT name="pagesPerFile" >
									<option value="50">50</option>
									<option value="100">100</option>
									<option value="150">150</option>
									<option value="200">200</option>
									<option value="250">250</option>
								</SELECT>

             			</TD>
					</TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
        </logic:equal>

      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY>
</form>
</TABLE>

<logic:present name="showStatusLogs">
<logic:equal name="showStatusLogs" value="yes">


<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">Renewal Status
          (<a href="<%=request.getContextPath()%>/dotRenewalLetters.do?action=showStatus">Click here</a> to update the progress)
          <BR>
            <BR>
            </TD>
        </TR>
        <TR>
        	<TD>
        		<TABLE  cellpadding="3" cellspacing="0" width="100%" >
        			<TR >
        				<TD class="con_text_1" width="25%" align="right">
        					25%<BR>|
        				</TD>
        				<TD class="con_text_1" width="25%" align="right">
        					50%<BR>|
        				</TD>
        				<TD class="con_text_1" width="25%" align="right">
        					75%<BR>|
        				</TD>
        				<TD class="con_text_1" width="25%" align="right">
        					100%<BR>|
        				</TD>
        			</TR>
        		</TABLE>

        		<TABLE  cellpadding="0" cellspacing="0" width="100%"  style="border:1px solid #000000">
        			<TR><TD>
        		<TABLE  cellpadding="0" cellspacing="0" width="<%=(String)request.getAttribute("percentComplete")%>%">
        			<TR>
        				<TD bgcolor="#CC66FF" colspan="4">
        					&nbsp;
        				</TD>
        			</TR>
        		</TABLE>
        		</TD>
        		</TR>
        		</TABLE>
        	</TD>
        </TR>
        <TR>
        	<TD>&nbsp;
        	</TD>
        </TR>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" background="../images/site_bg_B7C1CB.jpg">DOT Renewals Log (<%=size%> Messages found)</TD>
                      <TD width="1%"><IMG src="../images/site_hdr_split_1.jpg" width="17" height="25"></TD>
                      <TD width="1%" class="tablelabel"><NOBR>

					  <logic:equal name="showClearLogs" value="yes">
                      	<a href="javascript:deleteAllRecords();">CLEAR LOGS</a>
                      </logic:equal>

                      </NOBR></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Date</TD>
                      <TD class="tablelabel">Status</TD>
                      <TD class="tablelabel">Category</TD>
                      <TD class="tablelabel">Message</TD>
                    </TR>
 					<%
	   					while (crset.next()) {
	   						String date = crset.getString("UPDATED").substring(0,16);
	   						String status = crset.getString("STATUS");
	   						String category = crset.getString("TABLENAME");
	   						String logMessage = crset.getString("MESSAGE");
 					%>
            	    <TR valign="top">
 	 					<TD
 	 					<%if(category.equalsIgnoreCase("VIEW PDF")){%>
 	 					bgcolor="#CCFFFF"
 	 					<%}else{%>
 	 					class="tabletext"
 	 					<%}%>>
             			<%=date %>

             			</TD>

 	 					<TD
 	 					<%if(category.equalsIgnoreCase("VIEW PDF")){%>
 	 					bgcolor="#CCFFFF"
 	 					<%}else{%>
 	 					class="tabletext"
 	 					<%}%>>
             			<%=status %>

             			</TD>

 	 					<TD
 	 					<%if(category.equalsIgnoreCase("VIEW PDF")){%>
 	 					bgcolor="#CCFFFF"
 	 					<%}else{%>
 	 					class="tabletext"
 	 					<%}%>>
             			<%=category %>

             			</TD>

 	 					<TD
 	 					<%if(category.equalsIgnoreCase("VIEW PDF")){%>
 	 					bgcolor="#CCFFFF"
 	 					<%}else{%>
 	 					class="tabletext"
 	 					<%}%>>
             			<%=logMessage %>

             			</TD>

					</TR>
 					<%
	 					}
	 					if(crset!=null) crset.close();
 					%>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>
</logic:equal>
</logic:present>
</BODY>
</html:html>

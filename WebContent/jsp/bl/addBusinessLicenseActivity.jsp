<%@ page import='elms.agent.*,java.util.*,elms.control.beans.*,elms.common.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<%
BusinessLicenseActivityForm businessLicenseActivityForm = (BusinessLicenseActivityForm)request.getAttribute("businessLicenseActivityForm");
pageContext.setAttribute("businessLicenseActivityForm",businessLicenseActivityForm);
%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/addActivity.js"></script>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="javascript" type="text/javascript" src="../script/actb.js"></script>
<script language="javascript" type="text/javascript" src="../script/common.js"></script>

<script language="JavaScript">
// For Checking the Activity status as Issued
function checkActivityStatus(){
   if(document.forms[0].activityStatus.value == '1053'){
		document.getElementById("actStatus").style.display="block";
		document.getElementById("issueDisplay").style.display="none";
	}else{
	    document.getElementById("actStatus").style.display="none";
	    document.getElementById("issueDisplay").style.display="block";
	}
 	var chkStat = checkStatus();
	if(chkStat == true){
		return true;
	}else{
		return false;
	}
return true;
}//write this in activity Status - onchange="return checkActivityStatus()"

var chkStatus;
function checkStatus()
{
chkStatus = true;
 	if (document.forms[0].elements['activityStatus'].value == '1053'){//Business License Activity issued
 		if (document.forms[0].elements['outStandingFees'].value == 'Y' ){
 		 	alert("An activity with unpaid fees can not be issued.");
			chkStatus = false;
		}else if (document.forms[0].elements['issueDate'].value == ''){
			var cur = new Date();
			document.forms[0].elements['issueDate'].value=cur.getMonth()+1+"/"+cur.getDate()+"/"+cur.getYear();
			chkStatus = false;
		}else{
			chkStatus = true;
		}
    }
	return chkStatus;
}

function issueDateHide()
{
document.getElementById("issueDisplay").style.display="none";
  if(document.forms[0].activityStatus.value == "1053"){
    document.getElementById("actStatus").style.display="block";
    document.getElementById("issueDisplay").style.display="none";
	}else{
    document.getElementById("actStatus").style.display="none";
    document.getElementById("issueDisplay").style.display="block";
}
return true;
}//write this in form - onload="issueDateHide();"

</script>
</head>
<%!
BusinessLicenseActivityForm businessLicenseActivityForm = null;
String addressStreetName ="";
List streetList = new ArrayList();
List activityTypes = new ArrayList();
List activitySubTypes = new ArrayList();
List applicationTypes = new ArrayList();
List activityStatuses = new ArrayList();
List qtytList = new ArrayList();
List exemtList = new ArrayList();
List ownershipTypes= new ArrayList();

%>
<%
boolean businessLicenseUser = false, businessLicenseApproval = false, businessTaxUser = false, businessTaxApproval = false;
businessLicenseActivityForm = new BusinessLicenseActivityForm();
String contextRoot = request.getContextPath();
elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);

	java.util.List groups = (java.util.List) user.getGroups();
   	java.util.Iterator itr = groups.iterator();
   	while(itr.hasNext()){
    	elms.security.Group group = (elms.security.Group) itr.next();
     	if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
   	}

try{
 streetList = new AddressAgent().getBTBLStreetArrayList();
pageContext.setAttribute("streetList", streetList);

int length=streetList.size();

 activityTypes = LookupAgent.getBlActivityTypes(Constants.MODULE_NAME_BUSINESS_LICENSE);
pageContext.setAttribute("activityTypes", activityTypes);

 activitySubTypes = (List)request.getAttribute("activitySubTypes");

pageContext.setAttribute("activitySubTypes", activitySubTypes);

	elms.app.bl.ApplicationType applicationType = null;
	if(businessLicenseUser && businessTaxUser){
	 applicationTypes = LookupAgent.getApplicationTypes();
	pageContext.setAttribute("applicationTypes", applicationTypes);
	}else if(businessLicenseUser){
	applicationTypes = LookupAgent.getApplicationTypes();
	applicationTypes.remove(1);
	pageContext.setAttribute("applicationTypes", applicationTypes);
	}else{
	applicationTypes = new ArrayList();
	}

 activityStatuses = LookupAgent.getBLActivityStatuses(Constants.MODULE_NAME_BUSINESS_LICENSE);
pageContext.setAttribute("activityStatuses", activityStatuses);

 qtytList = LookupAgent.getQuantityTypes();
pageContext.setAttribute("qtytList", qtytList);

 exemtList = LookupAgent.getExemptionTypes();
pageContext.setAttribute("exemtList", exemtList);

ownershipTypes = LookupAgent.getOwnershipTypes();
pageContext.setAttribute("ownershipTypes", ownershipTypes);
}catch(Exception e){
//ignored
}

addressStreetName = (String)request.getAttribute("addressStreetName");
businessLicenseActivityForm.setAddressStreetName(addressStreetName);
%>

<SCRIPT language="javascript" type="text/javascript">
var xmlhttp;

function CreateXmlHttp(str){
	//Creating object of XMLHTTP in IE
		var url="<%=contextRoot%>/addBusinessLicenseActivity.do?activityType="+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		}else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		if (xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ActivityType;
		    xmlhttp.send(null);
		  }else{
		  	alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ActivityType(){
	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){
		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){
		    var text=xmlhttp.responseText;
			text=text.split(',');
			document.forms[0].muncipalCode.value = text[0];
		   	document.forms[0].sicCode.value = text[1];
			document.forms[0].classCode.value = text[2];
			if(document.forms[0].activityType.value==""){
				document.forms[0].muncipalCode.value = "";
			   	document.forms[0].sicCode.value = "";
				document.forms[0].classCode.value = "";
			}
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function CreateClassCodeXmlHttp(str){
	//Creating object of XMLHTTP in IE

		var url='<%=contextRoot%>/addBusinessLicenseActivity.do?clCode='+str;

		xmlhttp=null;
		if (window.XMLHttpRequest){// code for IE7, Firefox, Opera, etc.
		  xmlhttp=new XMLHttpRequest();
		  }else if (window.ActiveXObject){// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		if (xmlhttp!=null){
		    xmlhttp.open("POST",url,true);
		    xmlhttp.onreadystatechange=HandleResponse_ClassCode;
			 xmlhttp.send(null);
		  }else{
		  alert("Your browser does not support XMLHTTP.");
		  }
	}

function HandleResponse_ClassCode(){
	// To make sure receiving response data from server is completed
	if(xmlhttp.readyState == 4){
		// To make sure valid response is received from the server, 200 means response received is OK
		if(xmlhttp.status == 200){
		    var text=xmlhttp.responseText;
			text=text.split(',');
			document.forms[0].activityType.value = text[0];
		   	document.forms[0].muncipalCode.value = text[1];
			document.forms[0].sicCode.value = text[2];
			if(document.forms[0].classCode.value==""){
				document.forms[0].activityType.value = "";
			   	document.forms[0].muncipalCode.value = "";
				document.forms[0].sicCode.value = "";
			}
		}else{
			alert("There was a problem retrieving data from the server." );
		}
	}
}

function reApplicatioType(){

	if(document.forms[0].applicationType.value == '101'){
		document.forms[0].action='<%=contextRoot%>/addBusinessLicenseActivity.do?flag=Y';
  		document.forms[0].submit();
  		return true;
	}else if(document.forms[0].applicationType.value == '102'){
		document.forms[0].action='<%=contextRoot%>/addBusinessTaxActivity.do?flag=Y';
  		document.forms[0].submit();
  		return true;
	}
}
function ResetBlActivity(){
var pageReset=confirm("Would you like to reset the screen?");
if (pageReset==true){
  parent.location.href='<%=contextRoot%>/jsp/viewBusinessLicense.jsp';
  }else{
  document.forms[0].action='<%=contextRoot%>/addBusinessLicenseActivity.do';
  document.forms[0].submit();
  }

}

function reActivity(){
  document.forms[0].action='<%=contextRoot%>/addBusinessLicenseActivity.do';
  document.forms[0].submit();
  return true;
}

function loadCheck(){

	var ba1 = document.getElementById("adField1");
	var ba2 = document.getElementById("adField2");
	var oot1 = document.getElementById("ootField1");
	var oot2 = document.getElementById("ootField2");
	var baName1 = document.getElementById("addName1");

	if(document.forms[0].businessLocation.checked == true){

		ba1.style.display="block";
		ba2.style.display="block";
		oot1.style.display="none";
		oot2.style.display="none";
		baName1.innerHTML="Address";

		document.forms[0].outOfTownStreetNumber.value="";
		document.forms[0].outOfTownStreetName.value="";
		document.forms[0].addressStreetNumber.focus();
	}
}

function checkLocation()
{

	var ba1 = document.getElementById("adField1");
	var ba2 = document.getElementById("adField2");
	var oot1 = document.getElementById("ootField1");
	var oot2 = document.getElementById("ootField2");
	var baName1 = document.getElementById("addName1");

  	if(document.forms[0].businessLocation.checked == true){
	   ba1.style.display="block";
		ba2.style.display="block";
		oot1.style.display="none";
		oot2.style.display="none";
		baName1.innerHTML="Address";

		document.forms[0].outOfTownStreetNumber.value="";
		document.forms[0].outOfTownStreetName.value="";
		document.forms[0].addressStreetNumber.focus();
	}else{
	   	 ba1.style.display="none";
		ba2.style.display="none";
		oot1.style.display="block";
		oot2.style.display="block";
		baName1.innerHTML="Out of Town Address";
	    document.forms[0].addressStreetNumber.value="";
		document.forms[0].addressStreetName.value="";
		document.forms[0].outOfTownStreetNumber.focus();
	}
}

function save(){
var i=0;
var classCode = document.forms[0].classCode.value;
var streetText=document.forms[0].streetText.value;
	for(var q=0;q<<%=streetList.size()%>;q++){
		if(streetText.toUpperCase()==((document.forms[0].addressStreetName[q].text).toUpperCase())){
			document.forms[0].addressStreetName[q].selected=true;
		}
	}

if(document.forms[0].businessLocation.checked == true){
	if(document.forms[0].applicationType.value == ""){
	alert("Please select Application Type");
	document.forms[0].applicationType.focus();
	i++;
	}else if(document.forms[0].addressStreetNumber.value == ""){
	alert("Please enter Address Street Number");
	document.forms[0].addressStreetNumber.focus();
	i++;
	}
	else if(isNaN(document.forms[0].addressStreetNumber.value)){
	alert("Please enter numeric value for Address Street Number");
	document.forms[0].addressStreetNumber.focus();
	i++;
	}
	else if(document.forms[0].addressStreetName.value == "-1"){
	alert("Please select Address Street Name");
	document.getElementById('streetText').focus();
	i++;
	}else if(document.forms[0].activityStatus.value == ""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	i++;
	}else if(isNaN(document.forms[0].renewalDt.value)){
	alert("Please enter numeric value for Days");
	document.forms[0].renewalDt.value.focus();
	i++;
	}else if(document.forms[0].businessName.value == ""){
	alert("Please enter Business Name");
	document.forms[0].businessName.focus();
	i++;
	}else if(document.forms[0].activityType.value == ""){
	alert("Please select Activity Type");
	document.forms[0].activityType.focus();
	i++;
	}else if(document.forms[0].classCode.value == ""){
	alert("Please enter Class Code");
	document.forms[0].classCode.focus();
	i++;
	}/* else if(isNaN(document.forms[0].mailStreetNumber.value)){
	alert("Please enter numeric for Mailing Street Number ");
	document.forms[0].mailStreetNumber.focus();
	i++;
	}else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value==""){
	alert("Please enter mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}

	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailCity.value == ""){
	alert("Please enter  mailing city ");
	document.forms[0].mailCity.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value != "" && document.forms[0].mailState.value == ""){
	alert("Please enter  mailing state ");
	document.forms[0].mailState.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailZip.value == ""){
	alert("Please enter  mailing zipcode ");
	document.forms[0].mailZip.focus();
	i++;
	}
	else if((document.forms[0].mailState.value != "") && (!isNaN(document.forms[0].mailState.value))){
	alert("Please enter non numeric for Mailing State");
	document.forms[0].mailState.focus();
	i++;
    }else if(isNaN(document.forms[0].prevStreetNumber.value))
	{
	alert("Please enter numeric for Previous Street Number ");
	document.forms[0].prevStreetNumber.focus();
	i++;
	}else if((document.forms[0].prevState.value != "") && (!isNaN(document.forms[0].prevState.value)))
	{
	alert("Please enter non numeric for Previous State");
	document.forms[0].prevState.focus();
	i++;
    }else if((document.forms[0].prevZip.value != "") && (isNaN(document.forms[0].prevZip.value)))
	{
	alert("Please enter numeric for Previous Zipcode");
	document.forms[0].prevZip.focus();
	i++;
    } */
	else if(isNaN(document.forms[0].elements['multiAddress[0].streetNumber'].value)){
		alert("Please enter numeric for Mailing Street Number ");
		document.forms[0].elements['multiAddress[0].streetNumber'].focus();
		i++;
		}else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value==""){
		alert("Please enter mailing Street Name ");
		document.forms[0].elements['multiAddress[0].streetName1'].focus();
		i++;
		}

		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].city'].value == ""){
		alert("Please enter  mailing city ");
		document.forms[0].elements['multiAddress[0].city'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value != "" && document.forms[0].elements['multiAddress[0].state'].value == ""){
		alert("Please enter  mailing state ");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].zip'].value == ""){
		alert("Please enter  mailing zipcode ");
		document.forms[0].elements['multiAddress[0].zip'].focus();
		i++;
		}
		else if((document.forms[0].elements['multiAddress[0].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[0].state'].value))){
		alert("Please enter non numeric for Mailing State");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
	    }else if(isNaN(document.forms[0].elements['multiAddress[1].streetNumber'].value)){
		alert("Please enter numeric for Previous Street Number ");
		document.forms[0].elements['multiAddress[1].streetNumber'].focus();
		i++;
		}else if((document.forms[0].elements['multiAddress[1].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[1].state'].value))){
		alert("Please enter non numeric for Previous State");
		document.forms[0].elements['multiAddress[1].state'].focus();
		i++;
	    }else if((document.forms[0].elements['multiAddress[1].zip'].value != "") && (isNaN(document.forms[0].elements['multiAddress[1].zip'].value))){
		alert("Please enter numeric for Previous Zipcode");
		document.forms[0].elements['multiAddress[1].zip'].focus();
		i++;
	    } 
    else if(document.forms[0].typeOfExemptions.value == -1)
	{
	alert("Please select Type Of Exemptions ");
	document.forms[0].typeOfExemptions.focus();
	i++;
	}else if (i>0 ) {
	document.forms[0].action="";
	}else{
       if(document.forms[0].renewalDt.value == ""){
	  	//adding the days to current date Getting Current Date
		var today = new Date();
		//Adding Number Of Days To Date
		var addDays   =new Date().setDate(today.getDate());
		//Getting the Date in UTC Format
		addDays = new Date(addDays);
		//Getting the Date in M/D/YY Format
		var now = "07" + "/" + "01" + "/" + (((addDays.getFullYear()).toString()));

       //Getting the Current Date in MM/DD/YYYY Format

        var currentDt =(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+
						"/" +(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString()) +
						"/" + addDays.getFullYear().toString();
         if(currentDt < now){
			now = "07" + "/" + "01" + "/" + (((addDays.getFullYear()).toString()));
		}else{
		now = "07" + "/" + "01" + "/" + (((addDays.getFullYear() + 1 ).toString()));
		}

		document.forms[0].renewalDate.value=now;

	    }else{
		//adding the days to current date Getting Current Date
		var today = new Date();
		//Adding Number Of Days To Date
		var addDays   =new Date().setDate( today.getDate() + parseInt(document.forms[0].renewalDt.value));
		//Getting the Date in UTC Format
		addDays = new Date(addDays);
		//Getting the Date in MM/DD/YYYY Format
		var now = (addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+ "/" +
		(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString()) + "/"
		+ addDays.getFullYear().toString();

		//Assigning The Formated Date To Hidden Date Field
		document.forms[0].renewalDate.value=now;
		}
		document.forms[0].action='<%=contextRoot%>/saveBusinessLicenseActivity.do?clCode='+classCode;
		document.forms[0].submit();
		return true;
	}
}
if(document.forms[0].businessLocation.checked == false){
	if(document.forms[0].applicationType.value == ""){
	alert("Please select Application Type");
	document.forms[0].applicationType.focus();
	i++;
	}else if(document.forms[0].outOfTownStreetName.value==""){
	alert("Please enter value for Out Of Town Street Name");
	document.forms[0].outOfTownStreetName.focus();
	i++;
	}else if(document.forms[0].outOfTownCity.value==""){
	alert("Please enter value for City ");
	document.forms[0].outOfTownCity.focus();
	i++;
	}else if(document.forms[0].outOfTownState.value==""){
	alert("Please enter value for State");
	document.forms[0].outOfTownState.focus();
	i++;
	}else if(document.forms[0].outOfTownZip.value==""){
	alert("Please enter value for Out Of Town Zip");
	document.forms[0].outOfTownZip.focus();
	i++;
	}else if(document.forms[0].activityStatus.value == ""){
	alert("Please select Activity Status");
	document.forms[0].activityStatus.focus();
	i++;
	}else if(isNaN(document.forms[0].renewalDt.value)){
	alert("Please enter numeric value for Days");
	document.forms[0].renewalDt.value.focus();
	i++;
	}else if(document.forms[0].businessName.value == ""){
	alert("Please enter Business Name");
	document.forms[0].businessName.focus();
	i++;
	}else if(document.forms[0].activityType.value == ""){
	alert("Please select Activity Type");
	document.forms[0].activityType.focus();
	i++;
	}else if(document.forms[0].classCode.value == ""){
	alert("Please enter Class Code");
	document.forms[0].classCode.focus();
	i++;
	}
/* 	else if(isNaN(document.forms[0].mailStreetNumber.value)){
	alert("Please enter numeric for Mailing Street Number ");
	document.forms[0].mailStreetNumber.focus();
	i++;
	}else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value==""){
	alert("Please enter mailing Street Name ");
	document.forms[0].mailStreetName.focus();
	i++;
	}

	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailCity.value == ""){
	alert("Please enter  mailing city ");
	document.forms[0].mailCity.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value != "" && document.forms[0].mailState.value == ""){
	alert("Please enter  mailing state ");
	document.forms[0].mailState.focus();
	i++;
	}
	else if(document.forms[0].mailStreetNumber.value != "" && document.forms[0].mailStreetName.value!="" && document.forms[0].mailZip.value == ""){
	alert("Please enter  mailing zipcode ");
	document.forms[0].mailZip.focus();
	i++;
	}
	else if((document.forms[0].mailState.value != "") && (!isNaN(document.forms[0].mailState.value))){
	alert("Please enter non numeric for Mailing State");
	document.forms[0].mailState.focus();
	i++;
    }else if(isNaN(document.forms[0].prevStreetNumber.value))
	{
	alert("Please enter numeric for Previous Street Number ");
	document.forms[0].prevStreetNumber.focus();
	i++;
	}else if((document.forms[0].prevState.value != "") && (!isNaN(document.forms[0].prevState.value)))
	{
	alert("Please enter non numeric for Previous State");
	document.forms[0].prevState.focus();
	i++;
    }else if((document.forms[0].prevZip.value != "") && (isNaN(document.forms[0].prevZip.value)))
	{
	alert("Please enter numeric for Previous Zipcode");
	document.forms[0].prevZip.focus();
	i++;
    } */
	else if(isNaN(document.forms[0].elements['multiAddress[0].streetNumber'].value)){
		alert("Please enter numeric for Mailing Street Number ");
		document.forms[0].elements['multiAddress[0].streetNumber'].focus();
		i++;
		}else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value==""){
		alert("Please enter mailing Street Name ");
		document.forms[0].elements['multiAddress[0].streetName1'].focus();
		i++;
		}

		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].city'].value == ""){
		alert("Please enter  mailing city ");
		document.forms[0].elements['multiAddress[0].city'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value != "" && document.forms[0].elements['multiAddress[0].state'].value == ""){
		alert("Please enter  mailing state ");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
		}
		else if(document.forms[0].elements['multiAddress[0].streetNumber'].value != "" && document.forms[0].elements['multiAddress[0].streetName1'].value!="" && document.forms[0].elements['multiAddress[0].zip'].value == ""){
		alert("Please enter  mailing zipcode ");
		document.forms[0].elements['multiAddress[0].zip'].focus();
		i++;
		}
		else if((document.forms[0].elements['multiAddress[0].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[0].state'].value))){
		alert("Please enter non numeric for Mailing State");
		document.forms[0].elements['multiAddress[0].state'].focus();
		i++;
	    }else if(isNaN(document.forms[0].elements['multiAddress[1].streetNumber'].value)){
		alert("Please enter numeric for Previous Street Number ");
		document.forms[0].elements['multiAddress[1].streetNumber'].focus();
		i++;
		}else if((document.forms[0].elements['multiAddress[1].state'].value != "") && (!isNaN(document.forms[0].elements['multiAddress[1].state'].value))){
		alert("Please enter non numeric for Previous State");
		document.forms[0].elements['multiAddress[1].state'].focus();
		i++;
	    }else if((document.forms[0].elements['multiAddress[1].zip'].value != "") && (isNaN(document.forms[0].elements['multiAddress[1].zip'].value))){
		alert("Please enter numeric for Previous Zipcode");
		document.forms[0].elements['multiAddress[1].zip'].focus();
		i++;
	    } 
    
    else if(document.forms[0].typeOfExemptions.value == -1)
	{
	alert("Please select Type Of Exemptions ");
	document.forms[0].typeOfExemptions.focus();
	i++;
	}else if (i>0 ) {
	document.forms[0].action="";
	}else{

		if(document.forms[0].renewalDt.value == ""){
	  	//adding the days to current date Getting Current Date
		var today = new Date();
		//Adding Number Of Days To Date
		var addDays   =new Date().setDate(today.getDate());
		//Getting the Date in UTC Format
		addDays = new Date(addDays);
		//Getting the Date in MM/DD/YYYY Format
		var now = "07" + "/" + "01" + "/" + (((addDays.getFullYear()).toString()));

       //Getting the Current Date in MM/DD/YYYY Format
        var currentDt =(addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+ "/" +
		(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString()) + "/"
		+ addDays.getFullYear().toString();

        if(currentDt < now){
			now = "07" + "/" + "01" + "/" + (((addDays.getFullYear()).toString()));
		}else{
		now = "07" + "/" + "01" + "/" + (((addDays.getFullYear() + 1 ).toString()));
		}

		document.forms[0].renewalDate.value=now;
	    }else{
		//adding the days to current date Getting Current Date
		var today = new Date();
		//Adding Number Of Days To Date
		var addDays   =new Date().setDate( today.getDate() + parseInt(document.forms[0].renewalDt.value));
		//Getting the Date in UTC Format
		addDays = new Date(addDays);
		//Getting the Date in MM/DD/YYYY Format
		var now = (addDays.getMonth() < 9 ? "0" + (addDays.getMonth() + 1).toString() : (addDays.getMonth() + 1).toString() )+ "/" +
		(addDays.getDate() < 10 ? "0" + addDays.getDate().toString() : addDays.getDate().toString()) + "/"
		+ addDays.getFullYear().toString();
		//Assigning The Formated Date To Hidden Date Field
		document.forms[0].renewalDate.value=now;
		}
		document.forms[0].action='<%=contextRoot%>/saveBusinessLicenseActivity.do?clCode='+classCode;
		document.forms[0].submit();
		return true;
	}
  }
}

function NumericEntry(){
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		return false;
	}
}

function NonNumericEntry(){
	if (!( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}
}

function NumericEntryWithSlash(){
	if (( (event.keyCode<48) || (event.keyCode>57) || (event.keyCode == 47) ) && (event.keyCode != 46)){
		return false;
	}
}

function DisplayPhoneHyphen(str){

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}else{
		if ((document.forms[0].elements[str].value.length == 3 ) || (document.forms[0].elements[str].value.length == 7 )){
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		}
 		if (document.forms[0].elements[str].value.length > 11 )  event.returnValue = false;
	}
}

function DisplaySSNHyphen(str){
	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46)){
		event.returnValue = false;
	}else{
		if (document.forms[0].elements[str].value.length == 3){
			document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';

 		}
		if ((document.forms[0].elements[str].value.length == 6))  document.forms[0].elements[str].value = document.forms[0].elements[str].value +'-';
 		if (document.forms[0].elements[str].value.length > 10 )  event.returnValue = false;
	}
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}

function validateEmailId(str){
	if(document.forms[0].emailAddress.value!=""){
			var validemail= validateEmail(document.forms[0].emailAddress.value);
			if(validemail==false){
				alert('Please enter correct E-mail Id');
				document.forms[0].emailAddress.value="";
				document.forms[0].emailAddress.focus();
				return false;
			}
		}
}

var streetNameArray = new Array();

<%
if(streetList != null && streetList.size() > 0){
	for(int i=0; i<streetList.size();i++){
		elms.app.lso.Street problemStreet = (elms.app.lso.Street)streetList.get(i);
%>
	streetNameArray[<%=i%>] = '<%=problemStreet.getStreetName()%>';
<%}}%>
</script>

<html:errors/>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0" onload="loadCheck();issueDateHide();">
<html:form focus="addressStreetNumber" action="/saveBusinessLicenseActivity">
<html:hidden property="renewalDate" value=""/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td><font class="con_hdr_3b">Add Activity</font><br><br></td>
		</tr>
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="99%" class="tabletitle" align="left">Add Activity</td>
							<td width="1%" class="tablebutton" align="right"><nobr>
								<html:button property="Save" value="Save" styleClass="button" onclick="return save();"/>&nbsp;
								<input type="reset" name="Reset" value="Reset" class="button" onclick="ResetBlActivity()" />&nbsp;</nobr>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td background="../images/site_bg_B7C1CB.jpg">
					<table width="100%" cellpadding="2" cellspacing="1">
						<tr>
							<td class="tablelabel" width="20%">Is Business located in the City of Burbank?</td>
							<td class="tabletext" width="30%">
								<html:checkbox property="businessLocation" onclick="javascript:checkLocation();"/>Yes
							</td>
							<td class="tablelabel" width="20%">Application Type</td>
							<td class="tabletext" width="30%">
								<html:select property="applicationType" size="1" styleClass="textbox" onchange="javascript:return reApplicatioType();">
									<html:options collection="applicationTypes" property="id" labelProperty="description" />
							    </html:select>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%"><span id="addName1"></span><span class="red1"><strong>*</strong></span></td>
							<td class="tabletext" width="30%" height="30px">
								<div id="adField1">
								<html:text size="5" property="addressStreetNumber" styleClass="textbox" onkeypress="return NumericEntry()" />
								<html:select property="addressStreetFraction" styleClass="textbox">
									<html:option value=""></html:option>
									<html:option value="1/4">1/4</html:option>
									<html:option value="1/3">1/3</html:option>
									<html:option value="1/2">1/2</html:option>
									<html:option value="3/4">3/4</html:option>
								</html:select>
								<div style="visibility:hidden">
									<html:select property="addressStreetName" styleClass="textbox" style="position:absolute">
										<html:option value="-1">Please Select</html:option>
										<html:options collection="streetList" property="streetId" labelProperty="streetName"/>
									</html:select>
								</div>
									<input type="text" name="streetText" id="st" size="18%" style="padding:-2px;" />

									<script>
										var streetObj = new actb(document.getElementById('st'),streetNameArray);
										if(document.forms[0].addressStreetName.options[document.forms[0].addressStreetName.selectedIndex].value != "-1"){
											document.forms[0].streetText.value = document.forms[0].addressStreetName.options[document.forms[0].addressStreetName.selectedIndex].text;
										}
									</script>
								</div>
								<div id="ootField1" style="display:none;">
										<html:text size="5" property="outOfTownStreetNumber" styleClass="textbox"/>
										<html:text size="14" property="outOfTownStreetName" styleClass="textbox"/>
								</div>
							</td>
							<td class="tablelabel" width="20%">Unit,City,State,Zip,Zip4</td>
							<td  class="tabletext" valign="top" width="30%">
								<div id="adField2">
										<html:text size="5" property="addressUnitNumber" styleClass="textbox"/>
										<html:text size="7" property="addressCity" value="Burbank" styleClass="textbox" disabled="true"/>
						                <html:text size="2" maxlength="2" property="addressState" value="CA" styleClass="textbox" onkeypress="return NonNumericEntry()" disabled="true"/>
										<html:text size="5" maxlength="5" property="addressZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
						                <html:text size="5" maxlength="4" property="addressZip4" styleClass="textbox"/>
								</div>
								<div id="ootField2" style="display:none;">
										<html:text size="5" property="outOfTownUnitNumber" styleClass="textbox"/>
										<html:text size="10" property="outOfTownCity" styleClass="textbox"/>
										<html:text size="2" maxlength="2" property="outOfTownState" styleClass="textbox" onkeypress="return NonNumericEntry()" />
										<html:text size="5" maxlength="5" property="outOfTownZip" styleClass="textbox" onkeypress="return NumericEntry()" />
										<html:text size="4" property="outOfTownZip4" styleClass="textbox"/>
								</div>
							</td>
						</tr>
               			<tr>
							<td class="tablelabel" width="20%">Activity Status<font class="red1"><strong>*</strong></td>
							<td class="tabletext" width="30%">
								<html:select property="activityStatus" size="1" styleClass="textbox" onchange="return checkActivityStatus();">
									<html:option value="">Please Select</html:option>
									<html:options collection="activityStatuses" property="status" labelProperty="description"/>
								</html:select><BR>
							</td>
							<td class="tablelabel" width="20%">No of Days for Renewal</td>
							<td class="tabletext" width="30%">
								<html:text property="renewalDt" size="10" maxlength="10" styleClass="textbox" onkeypress="return NumericEntry()"/>
				            </td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Business Name<font class="red1"><strong>*</strong></td>
							<td class="tabletext" width="30%">
								<html:text size="20" property="businessName" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/>&nbsp;<BR>
							</td>
							<td class="tablelabel" width="20%">Corporate Name</td>
							<td class="tabletext" width="30%">
								<html:text size="20" property="corporateName" styleClass="textbox"/>
							</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Business Phone ,Ext</td>
						<td class="tabletext" width="30%">
							<html:text size="14" property="businessPhone" maxlength="12" onkeypress="return DisplayPhoneHyphen('businessPhone');" styleClass="textbox"/>&nbsp;
	                        <html:text size="4" property="businessExtension" maxlength="4" styleClass="textbox" onkeypress="return NumericEntry()"/>&nbsp;<BR>
						</td>
				       	<td class="tablelabel" width="20%">Business Fax</td>
						<td class="tabletext" width="30%">
							<html:text size="14" property="businessFax" maxlength="12" onkeypress="return DisplayPhoneHyphen('businessFax');" styleClass="textbox"/><BR>
				       </td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Activity Type<font class="red1"><strong>*</strong></td>
						<td class="tabletext" width="30%">
							<html:select property="activityType" size="1" styleClass="textbox" onchange="CreateXmlHttp(this.value);">
								<html:option value="">Please Select</html:option>
								<html:options collection="activityTypes" property="type" labelProperty="description"/>
							</html:select><BR>
						</td>
						<td class="tablelabel" width="20%">Activity Sub-Type</td>
						<td class="tabletext" width="30%">
				            <html:select multiple="true" size="5" property="activitySubType" styleClass="textbox">
                               <html:option value="-1">Not Applicable</html:option>
								<html:options collection="activitySubTypes" property="id" labelProperty="description"/>
								</html:select>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Municipal Code<font class="red1"><strong>*</strong></td>
						<td class="tabletext" width="30%">
							<html:text size="20" property="muncipalCode" styleClass="textbox"  readonly="true"/><BR>
						</td>
						<td class="tablelabel" width="20%">SIC Code<font class="red1"><strong>*</strong></td>
						<td class="tabletext" width="30%">
		         			<html:text size="20" property="sicCode" styleClass="textbox"  readonly="true"/>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Description of Business</td>
						<td class="tabletext" width="30%">
							<html:text size="20" property="descOfBusiness" styleClass="textbox"/><BR>
						</td>
						<td class="tablelabel" width="20%">Class Code<font class="red1"><strong>*</strong></td>
						<td class="tabletext" width="30%">
							<html:text size="20" property="classCode" styleClass="textbox" onblur="CreateClassCodeXmlHttp(this.value)"/>
						</td>
					</tr>
	                <tr>
						<td class="tablelabel" width="20%">Home Occupation</td>
						<td class="tabletext" width="30%">
							<html:checkbox property="homeOccupation" styleClass="textbox">Yes</html:checkbox><BR>
						</td>
						<td class="tablelabel" width="20%">Decal Code</td>
						<td class="tabletext" width="30%">
					    	<html:checkbox property="decalCode" styleClass="textbox">Yes</html:checkbox>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Application Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text  property="applicationDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].applicationDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				                 	<img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
						</td>
						<td class="tablelabel" width="20%">Issue Date</td>
						<td class="tabletext"  width="30%">
		                        <span id="actStatus" class="con_hdr_1" style="padding:0; margin:0;">
									<html:text   property="issueDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
									 	<html:link href="javascript:show_calendar('forms[0].issueDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
						                	<img src="../images/calendar.gif" width="16" height="15" border=0/>
										</html:link>
								</span>
								<span id="issueDisplay" class="con_hdr_1" style="padding:0; margin:0;">
									 <script language="javascript" type="text/javascript">
									var cur = new Date();
		    						document.write(cur.getMonth()+1+"/"+cur.getDate()+"/"+cur.getFullYear());
		                          </script>
								</span>
						</td>

					</tr>
					<tr>
						<td class="tablelabel" width="20%">Starting Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text  property="startingDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].startingDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
			                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
						</td>
						<td class="tablelabel" width="20%">Out of Business Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text  property="outOfBusinessDate" size="10" maxlength="10" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
							<html:link href="javascript:show_calendar('forms[0].outOfBusinessDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
		                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
							</html:link></nobr>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Ownership Type</td>
						<td class="tabletext" width="30%">
							 <html:select property="ownershipType" size="1" styleClass="textbox">
								<html:option value="">Please Select</html:option>
								<html:options collection="ownershipTypes" property="id" labelProperty="description"/>
							</html:select><BR>
						</td>
						<td class="tablelabel" width="20%">Federal ID Number</td>
						<td class="tabletext" width="30%">
							<html:text size="20"  maxlength="10" property="federalIdNumber" styleClass="textbox"/>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">E-mail Address</td>
						<td class="tabletext" width="30%">
							<html:text size="24" property="emailAddress" styleClass="textbox" onblur="return validateEmailId('emailAddress');"/><BR>
						</td>
						<td class="tablelabel" width="20%">Social Security Number</td>
						<td class="tabletext" width="30%">
							<html:text size="14" property="socialSecurityNumber" maxlength="11" onkeypress="return DisplaySSNHyphen('socialSecurityNumber');" styleClass="textbox"/>
						</td>
					</tr>
			        <%-- <tr>
						<td class="tablelabel" width="20%">Mailing Address</td>
						<td class="tabletext" width="30%">
							<html:text size="5" maxlength="80" property="mailStreetNumber" styleClass="textbox"/>
							<html:text size="14" maxlength="80" property="mailStreetName" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="20%">Unit,City,State,Zip,Zip4</td>
						<td  class="tabletext" valign="top" width="30%">
							<html:text size="4" maxlength="80" property="mailUnitNumber" styleClass="textbox"/>
							<html:text size="8" maxlength="80" property="mailCity" styleClass="textbox"/>
							<html:text size="2" maxlength="2" property="mailState" styleClass="textbox" onkeypress="return NonNumericEntry()" />
							<html:text size="6" maxlength="6" property="mailZip" styleClass="textbox"/>
							<html:text size="4" property="mailZip4" styleClass="textbox"/>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Previous Address</td>
						<td class="tabletext" width="30%">
							<html:text size="5" property="prevStreetNumber" styleClass="textbox"/>
							<html:text size="14" property="prevStreetName" styleClass="textbox"/>
						</td>
						<td class="tablelabel" width="20%">Unit,City,State,Zip,Zip4</td>
						<td  class="tabletext" valign="top" width="30%">
							<html:text size="4" property="prevUnitNumber" styleClass="textbox"/>
							<html:text size="9" property="prevCity" styleClass="textbox"/>
							<html:text size="2" maxlength="2" property="prevState" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
							<html:text size="5" maxlength="5" property="prevZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
							<html:text size="4" property="prevZip4" styleClass="textbox"/>
						</td>
					</tr> --%>
					<tr>
						<td class="tablelabel" width="20%">Insurance Expiration Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text property="insuranceExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].insuranceExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
			                    	<img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
						</td>
						<td class="tablelabel" width="20%">Bond Expiration Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text property="bondExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].bondExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				            		<img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Department of Justice Expiration Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text property="deptOfJusticeExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].deptOfJusticeExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
			                        <img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>
						</td>
						<td class="tablelabel" width="20%">Federal Firearms License Expiration Date</td>
						<td class="tabletext" width="30%"><nobr>
							<html:text property="federalFirearmsLiscExpDate" size="10" maxlength="10" styleClass="textbox" onblur="DateValidate(this);"/>
								<html:link href="javascript:show_calendar('forms[0].federalFirearmsLiscExpDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				                     <img src="../images/calendar.gif" width="16" height="15" border=0/>
							</html:link></nobr>
						</td>
					</tr>
					<tr>
						<td class="tablelabel" width="20%">Square Footage(Area Occupied)</td>
						<td class="tabletext" width="30%">
							<html:text size="20" property="squareFootage" styleClass="textbox"/><BR>
						</td>
							<td class="tablelabel" width="20%">Quantity</td>
							<td class="tabletext" width="30%">
				                <html:select property="quantity" size="1" styleClass="textbox">
									<html:option value="">Please Select</html:option>
									<html:options collection="qtytList" property="id" labelProperty="description"/>
								</html:select>
								<html:text size="20" property="quantityNum" styleClass="textbox" onkeypress="return NumericEntry()"/>
							</td>
						</tr>
						<tr>
							<td class="tablelabel" width="20%">Type of Exemptions</td>
							<td colspan="3" class="tabletext">
			        	        <html:select property="typeOfExemptions" size="1" styleClass="textbox">
									<html:option value="">Please Select</html:option>
									<html:options collection="exemtList" property="id" labelProperty="description" />
							     </html:select>
			                </td>
						</tr>
				</table>
		</td>
	</tr>
	 <tr>
	<td>&nbsp;</td>
	</tr>
	<%--
			    <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"> Business Address</td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    
               
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
						
						<tr>
							<td class="tablelabel" width="20%"><span id="addName1"></span><span class="red1"><strong>*</strong></span></td>
							<td class="tabletext" width="30%" height="30px">
								<div id="adField1">
								<html:text size="5" property="addressStreetNumber" styleClass="textbox" onkeypress="return NumericEntry()" />
								<html:select property="addressStreetFraction" styleClass="textbox">
									<html:option value=""></html:option>
									<html:option value="1/4">1/4</html:option>
									<html:option value="1/3">1/3</html:option>
									<html:option value="1/2">1/2</html:option>
									<html:option value="3/4">3/4</html:option>
								</html:select>
								<div style="visibility:hidden">
									<html:select property="addressStreetName" styleClass="textbox" style="position:absolute">
										<html:option value="-1">Please Select</html:option>
										<html:options collection="streetList" property="streetId" labelProperty="streetName"/>
									</html:select>
								</div>
									<input type="text" name="streetText" id="st" size="18%" style="padding:-2px;" />

									<script>
										var streetObj = new actb(document.getElementById('st'),streetNameArray);
										if(document.forms[0].addressStreetName.options[document.forms[0].addressStreetName.selectedIndex].value != "-1"){
											document.forms[0].streetText.value = document.forms[0].addressStreetName.options[document.forms[0].addressStreetName.selectedIndex].text;
										}
									</script>
								</div>
								<div id="ootField1" style="display:none;">
										<html:text size="5" property="outOfTownStreetNumber" styleClass="textbox"/>
										<html:text size="14" property="outOfTownStreetName" styleClass="textbox"/>
								</div>
							</td>
							<td class="tablelabel" width="20%"><span class="con_hdr_1">Unit,City,State,Zip,Zip4</span></td>
							<td  class="tabletext" valign="top" width="30%">
								<div id="adField2">
										<html:text size="5" property="addressUnitNumber" styleClass="textbox"/>
										<html:text size="7" property="addressCity" value="Burbank" styleClass="textbox" disabled="true"/>
						                <html:text size="2" maxlength="2" property="addressState" value="CA" styleClass="textbox" onkeypress="return NonNumericEntry()" disabled="true"/>
										<html:text size="5" maxlength="5" property="addressZip" styleClass="textbox" onkeypress="return NumericEntry()"/>
						                <html:text size="5" maxlength="4" property="addressZip4" styleClass="textbox"/>
								</div>
								<div id="ootField2" style="display:none;">
										<html:text size="5" property="outOfTownUnitNumber" styleClass="textbox"/>
										<html:text size="10" property="outOfTownCity" styleClass="textbox"/>
										<html:text size="2" maxlength="2" property="outOfTownState" styleClass="textbox" onkeypress="return NonNumericEntry()" />
										<html:text size="5" maxlength="5" property="outOfTownZip" styleClass="textbox" onkeypress="return NumericEntry()" />
										<html:text size="4" property="outOfTownZip4" styleClass="textbox"/>
								</div>
							</td>
						</tr>
					</table></td></tr></table></td></tr>
 --%>	
	
					<nested:iterate  name="businessLicenseActivityForm" property="multiAddress" type="elms.app.common.MultiAddress">
				<nested:hidden property="id"></nested:hidden>
            <tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">

                   <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"> <nested:write property="addressType"></nested:write></td>
                                 
                           </tr>
                        </table>
                        </td>
                    </tr> 
                    
               
                    <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							
							<tr valign="top">
                                <td width="20%" class="tablelabel">Street Number , Street Name1</td>
                                <td width="30%" class="tabletext">
                                <nested:text size="5"  property="streetNumber" styleClass="textbox" onkeypress="return NumericEntry()"/>
               					<nested:text size="14" property="streetName1" styleClass="textbox"/>
                                 </td>
                                <td width="20%" class="tablelabel">Street Name2</td>
                                <td width="30%" class="tabletext">
                                <nested:text size="14" property="streetName2" styleClass="textbox"/></td>
                            </tr>
                            <tr valign="top">
                                <td width="20%" class="tablelabel"> Unit,City,State,Zip,Zip4</td>
                                <td width="30%" class="tabletext">
								<nested:text size="5" property="unit" styleClass="textbox"/>
								<nested:text size="10" property="city" styleClass="textbox"/>
								<nested:text size="2" maxlength="2" property="state" styleClass="textbox" onkeypress="return NonNumericEntry()"/>
								<nested:text size="5" maxlength="5" property="zip" styleClass="textbox" onkeypress="return NumericEntry()"/>
								<nested:text size="4" property="zip4" styleClass="textbox"/></td>
                                <td width="20%" class="tablelabel">Attn</td>
                                <td width="30%" class="tabletext">
                                <nested:text size="10" property="attn" styleClass="textbox"/>
                                </td>
                            </tr>
                   </table>
                        </td>
                    </tr>
                    
                    </table>
                        </td>
                    </tr>
                      <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
                    </nested:iterate>
                    
	
	
	
	
	
	</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td>&nbsp;</td>
	</tr>
	</table>
	<td width="1%">
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="32">&nbsp;</td>
			</tr>
		</table>
			</td>
		</html:form>
</body>
</html:html>
<%@ page import='elms.agent.*,java.util.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />

<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="javascript" type="text/javascript">

// For Checking the Approval status as Approved on load
function checkStatus(){
  if((document.forms[0].approvalId.value == '101') || (document.forms[0].approvalId.value == '104')){

    document.getElementById("appStatus").style.display="block";
	}
	else{
    document.getElementById("appStatus").style.display="none";
}

return true;
}



// For Checking the Approval status as Approved on change
function checkChangeStatus(){
  if((document.forms[0].approvalStatusId.value == '101') || (document.forms[0].approvalStatusId.value == '104')){

	var browser=navigator.appName;

		if(browser=="Microsoft Internet Explorer"){
	    	document.getElementById("appStatus").style.display="block";
		}
		else{
			document.getElementById("appStatus").style.display="table-row";
		}

		}else{
     	document.getElementById("appStatus").style.display="none";
	}
	return true;
}


</script>
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0" onload="return checkStatus()">
<html:errors/>
<%!
List userList = new ArrayList();
List approvalStatuses= new ArrayList();
boolean show = false;
 %>
<%

pageContext.setAttribute("userList", userList);
String departmentName = "";

String userName = "";
String userNameFromSession="";
int departmentIdFromSession=0;

try{
	show = false;
	departmentName = (String) request.getAttribute("departmentName");
 	elms.security.User user = (elms.security.User)session.getAttribute(elms.common.Constants.USER_KEY);
	String departmentNameFromSession = user.getDepartment().getDescription();
	System.out.println("department name from session is "+departmentNameFromSession);

	// added by akash to make approvals modifiable by the actual person assigned to and not the entire division
	// For the License and Code Services Division only
	userName = (String) request.getAttribute("userName");
	userNameFromSession = user.getFullName();
	System.out.println("user name from session is "+userNameFromSession);
	departmentIdFromSession = user.getDepartment().getDepartmentId();

	if (departmentIdFromSession==elms.common.Constants.DEPARTMENT_LICENSE_AND_CODE_SERVICES){
		if(departmentNameFromSession.equalsIgnoreCase(departmentName) && userNameFromSession.equalsIgnoreCase(userName)){
			 show = true;
		}
	}
	else{
		if(departmentNameFromSession.equalsIgnoreCase(departmentName)){
			 show = true;
		}
	}
   approvalStatuses = LookupAgent.getApprovalStatuses();
   pageContext.setAttribute("approvalStatuses", approvalStatuses);
  }
catch(Exception e){}
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   String activityId = (String)request.getParameter("activityId");

   if(activityId ==null)
   {
    activityId = (String)request.getAttribute("activityId");
   }
   if(activityId ==null) activityId="";
   if (lsoAddress == null ) lsoAddress = "";
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";

       String approvalId = (String) request.getAttribute("approvalId");

       if(approvalId ==null)
      {
      approvalId = (String)request.getAttribute("approvalId");
      }

   if(approvalId ==null) approvalId="";
	   String applicationDateString = (String) request.getAttribute("applicationDateString");

		         if(applicationDateString ==null)
				      {
				      applicationDateString = (String)request.getAttribute("applicationDateString");
				      }

					  	 if(applicationDateString ==null) applicationDateString="";


			  if(departmentName ==null)
				      {
				      departmentName = (String)request.getAttribute("departmentName");
				      }
					      System.out.println("departmentName " +departmentName);
					  	 if(departmentName ==null) departmentName="";

	   userName = (String) request.getAttribute("userName");
				if(userName ==null)
				      {
				      userName = (String)request.getAttribute("userName");
				      }

					  	 if(userName ==null) userName="";

	String address = (String)request.getAttribute("address");
	 if(address ==null)
				      {
				      address = (String)request.getAttribute("address");
				      }

					  	 if(address ==null) address="";

	String activityNumber = (String)request.getAttribute("activityNumber");
			 if(activityNumber ==null)
				      {
				      activityNumber = (String)request.getAttribute("activityNumber");
				      }

					  	 if(activityNumber ==null) activityNumber="";

			String activityType = (String)request.getAttribute("activityType");
			 if(activityNumber ==null)
				      {
				      activityType = (String)request.getAttribute("activityType");
				      }

					  	 if(activityType ==null) activityType="";

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript">
function saveApproval()
{

		// For Checking the Approval Date based on Approval Status
        if((document.forms[0].approvalStatusId.value== '101') || (document.forms[0].approvalStatusId.value=='104')){
	      if(document.forms[0].approvalDate.value==""){
				alert("Please enter Approval Date");
				document.forms[0].approvalDate.focus();
				return false;
			}else if(document.forms[0].elements['approvalDate'].value != ''){
				strValue=validateData('date',document.forms[0].elements['approvalDate'],'Invalid date format');
				 document.forms[0].approvalDate.focus();
                if(strValue==false){
					return false;
				}else{
					var approvalId=document.forms[0].approvalId.value;
					document.forms[0].action="<%=contextRoot%>/saveBusinessLicenseApproval.do?approvalId="+approvalId;
					document.forms[0].submit();
					return true;
				}
			}else{
				var approvalId=document.forms[0].approvalId.value;
				document.forms[0].action="<%=contextRoot%>/saveBusinessLicenseApproval.do?approvalId="+approvalId;

				document.forms[0].submit();
	            return true;
			}
		} else {
		   var approvalId=document.forms[0].approvalId.value;
			document.forms[0].action="<%=contextRoot%>/saveBusinessLicenseApproval.do?approvalId="+approvalId;

			document.forms[0].submit();
			return true;
			}
	}

function validDate(){
strValue = true;
	if(strValue == true){
		if(document.forms[0].elements['approvalDate'].value != ''){
			strValue=validateData('date',document.forms[0].elements['approvalDate'],'Invalid date format');
			document.forms[0].approvalDate.focus();
			return strValue;
		}
	}
}

</script>

<html:form action="/addBusinessLicenseApproval">
<html:hidden property="approvalId" value="<%=approvalId%>" />
<html:hidden property="activityId" value="<%=activityId%>" />


	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Approval Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=address%>--<%=activityNumber%>-<%=activityType%></font> <br></td>
			</tr>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" class="tabletitle">Add Approval</td>
										<td width="1%" class="tablebutton">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="javascript:history.back(1);" />&nbsp;

												<% if(show){%>
												<html:button property="save" value="Save" styleClass="button" onclick="javascript:saveApproval()" /> &nbsp;
												<%} %>
										</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>

								<td background="../images/site_bg_B7C1CB.jpg">
								<table width="100%" border="0" cellspacing="1" cellpadding="2">
									<tr>
										<td width="30%" class="tablelabel">
											Approving Department: <b><%=departmentName%> </b></td>
										<td width="40%" class="tablelabel">Date Referred:  <b><%=applicationDateString%></b></td>
										<td width="30%" class="tablelabel">
											Approver:  <b><%=userName%></b>
										</td>
									</tr>
									<tr valign="top">
										<td width="30%" class="tablelabel">
											Approval Status:
										</td>
										<td class="tabletext" colspan="2">
											<html:select property="approvalStatusId" styleClass="textbox" onchange="return checkChangeStatus()">
												<html:options collection="approvalStatuses" property="id"
													labelProperty="description" />
											</html:select>
										</td>
									</tr>
									<tr valign="top" id="appStatus">
										<td width="30%" class="tablelabel">Approval Date:</td>

                                        <td class="tabletext" colspan="2">
                                           <html:text  property="approvalDate" size="10" maxlength="10"  styleClass="textboxd" onblur="return validDate();" readonly="true"/>
					                       <html:link href="javascript:show_calendar('forms[0].approvalDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
                                            <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link>
                                        </td>

									</tr>
									<tr>
										<td class="tablelabel">Comments</td>
										<td class="tabletext" colspan="2"><html:textarea property="comments" cols="75" rows="7" styleClass="textbox" /></td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
				<td width="1%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td height="32">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
				</table>


				</td>
			</tr>
			<tr>
				<td></td>
			</tr>

			<tr>
				<td>
				<!-- display of the approval history list -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="100%" class="tabletitle">
                                  Approval History
                               </td>

                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                        <tr>
		                        <td class="tablelabel">Department</td>
		                        <td class="tablelabel"><nobr>Date Referred </nobr></td>
		                        <td class="tablelabel"><nobr>Approval Status</nobr></td>
		                        <td class="tablelabel">Approved By</td>
		                        <td class="tablelabel">Activity Date</td>
		                        <td class="tablelabel">Comments</td>
								<td class="tablelabel"></td>

	                        </tr>

                            <logic:iterate id="activityTeam" name="activityTeamList" type="elms.control.beans.BusinessLicenseApprovalForm">
			                    <tr valign="top">
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="departmentName" />
								  </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="referredDate" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="approvalStatus" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="userName" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="approvalDate" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="comments" />
			                      </td>
								 <td class="tabletext">
			                        <a href="<%=contextRoot%>/viewApprovalHistory.do?activityId=<%=(String)request.getParameter("activityId")%>&approvalId=<bean:write name="activityTeam" property="approvalId" />&departmentName=<bean:write name="activityTeam" property="departmentName" />&address=<%=(String)request.getParameter("address")%>&activityType=<%=(String)request.getParameter("activityType")%>&activityNumber=<%=(String)request.getParameter("activityNumber")%>">History</a>
			                      </td>
			                   </tr>
							</logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
				</td>
			</tr>
	</table>
</html:form>
</body>
</html:html>


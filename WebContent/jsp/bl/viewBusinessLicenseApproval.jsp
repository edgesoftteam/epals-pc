<%@ page import='elms.agent.*,java.util.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />

<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<title>City of Burbank : Online Business Center : Add Approval</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:errors/>

<%
String activityNumber = "";
String activityType = "";

try{
   java.util.List approvalStatuses = LookupAgent.getApprovalStatuses();
   pageContext.setAttribute("approvalStatuses", approvalStatuses);
  }
catch(Exception e){}
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   String activityId = (String)request.getParameter("activityId");

   if(activityId ==null) activityId = "";
   if (lsoAddress == null ) lsoAddress = "";
   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";

try{
	if((!("").equalsIgnoreCase(activityId)) && (("").equalsIgnoreCase(activityNumber))){
		activityNumber = LookupAgent.getActivityNumberForActivityId(activityId);
		activityType = LookupAgent.getActivityDesc(LookupAgent.getActivityTypeForActId(activityId));
	}
}catch(Exception e){}
%>
<script language="JavaScript" src="../script/calendar.js"></script>


<html:form action="/addBusinessLicenseApproval">


	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Approval List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br></td>
			</tr>


			<tr>
				<td>
				<!-- display of the approval history list -->
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                <td width="909%" class="tabletitle">Approval History</td>
                                <td width="9%" class="tablebutton">
									<html:button property="back" value="Back" styleClass="button" onclick="javascript:history.back(1);" />
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                        <tr>
		                        <td class="tablelabel">Department</td>
		                        <td class="tablelabel"><nobr>Date Referred </nobr></td>
		                        <td class="tablelabel"><nobr>Approval Status</nobr></td>
		                        <td class="tablelabel">Approved By</td>
		                        <td class="tablelabel">Activity Date</td>
		                        <td class="tablelabel">Comments</td>
								<td class="tablelabel"></td>
	                        </tr>

                            <logic:iterate id="activityTeam" name="activityTeamList" type="elms.control.beans.BusinessLicenseApprovalForm">
			                    <tr valign="top">
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="departmentName" />
								  </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="referredDate" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="approvalStatus" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="userName" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="approvalDate" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="comments" />
			                      </td>
								  <td class="tabletext">
			                        <a href="<%=contextRoot%>/viewApprovalHistory.do?activityId=<%=activityId%>&approvalId=<bean:write name="activityTeam" property="approvalId" />&departmentName=<bean:write name="activityTeam" property="departmentName" />&address=<%=lsoAddress%>&activityType=<%=activityType%>&activityNumber=<%=activityNumber%>">History</a>
			                      </td>
							</logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
				</td>
			</tr>
	</table>
</html:form>




</body>
</html:html>


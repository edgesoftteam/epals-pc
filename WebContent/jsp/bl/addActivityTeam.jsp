<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%!
	String duplicate = "false";
	java.util.List departments = new java.util.ArrayList();
	java.util.List users = new java.util.ArrayList();
 %>
<%
    String contextRoot = request.getContextPath();
    String psaInfo = (String)session.getAttribute("psaInfo");
    if (psaInfo == null ) psaInfo = "";


    String lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";

    String psaId = (String) session.getAttribute(elms.common.Constants.PSA_ID);
    if(psaId ==null) psaId = "";

	String activityId = (String) request.getAttribute("activityId");


    int userId=elms.util.StringUtils.s2i((String) request.getAttribute("userId")!=null ? (String) request.getAttribute("userId") : "" );

    int departmentId=elms.util.StringUtils.s2i((String) request.getAttribute("departmentId")!=null ? (String) request.getAttribute("departmentId") : "" );
	duplicate =(String) request.getAttribute("duplicate")!=null ? (String) request.getAttribute("duplicate") : "false" ;


//if(duplicate.equalsIgnoreCase("true")){
%>

<script>
function getUsers()
{

	var departmentId = document.forms[0].departmentId.value;
	document.forms[0].action='<%=contextRoot%>/addActivityTeam.do?departmentId='+departmentId;
    document.forms[0].submit();
    return true;
}
function saveTeam()
{
	var dup = document.forms[0].duplicate.value;
	if(dup == "true"){
	userInput = confirm("Are you sure you'd like to add the record that already exists?");
   	if (userInput==true) {

    document.forms[0].action='<%=contextRoot%>/saveActivityTeam.do?duplicate='+userInput+'&activityId='+<%=activityId%>+'&departmentId='+<%=departmentId%>+'&userId='+<%=userId%>;

	document.forms[0].submit();
    return true;
	}else{
		document.forms[0].duplicate.value=false;
		return false;
	}
}else{
var departmentId = document.forms[0].departmentId.value;
var userId = document.forms[0].userId.value;

	// Validation for department and user
	if(departmentId =="-1"){
		alert("Please select Department");
		document.forms[0].departmentId.focus();
		}else if(userId == "-1"){
		alert("Please select User");
		document.forms[0].userId.focus();
	}else{
		document.forms[0].action='<%=contextRoot%>/saveActivityTeam.do?activityId=<%=activityId%>&departmentId='+departmentId+'&userId='+userId+'&dupl=false';
		document.forms[0].submit();
	    return true;
	}
}

}
function goBack() {
  parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
saveTeam();
}

}

</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/saveActivityTeam">
<html:hidden property="activityId"/>
<html:hidden property="duplicate" value="<%=duplicate%>" />

<script language ="javascript" type="text/javascript">
var duplicate =document.forms[0].duplicate.value;
if(duplicate=="true"){
saveTeam();

}
</script>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br><br>
         </td>
		</tr>
        <tr>
          <td><font class="con_hdr_3b"><html:errors/></font>

         </td>
		</tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Add Activity Team</td>
                      <td width="1%" class="tablebutton"><nobr>
                     	<html:button property="Back" value="Back" onclick="goBack()" styleClass="button" />&nbsp;&nbsp;
                      	<html:button property="Save" value="Save" styleClass="button"  onclick="saveTeam()" />&nbsp;&nbsp;
                        </nobr>
                      </td>
                   </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" width="30%">Department</td>
                      <td class="tablelabel" width="70%">Name</td>

                    </tr>
                    <tr valign="top">

                      <td class="tabletext" width="30%">
                           <html:select property="departmentId" styleClass="textbox" onchange="return getUsers()">
                	          <html:option value="-1">Please Select</html:option>
                              <html:options collection="departments" property="departmentId" labelProperty="description" />
                           </html:select>

                       </td>
                      <td class="tabletext" width="70%">
                           <html:select property="userId" styleClass="textbox">
                	          <html:option value="-1">Please Select</html:option>
                              <html:options collection="users" property="userId" labelProperty="fullName" />
                           </html:select>
                      </td>

                    </tr>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>


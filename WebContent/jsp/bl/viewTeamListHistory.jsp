<%@ page import='elms.agent.*,java.util.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon />

<%     String contextRoot = request.getContextPath();  %>
<html:html>
<head>
<html:base />
<title>City of Burbank :  Enterprise Permitting and Licensing System:Approval History</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">

</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:errors/>
<%!
	List userList = new ArrayList();
	List approvalStatuses= new ArrayList();
	boolean show = false;
%>
<%
pageContext.setAttribute("userList", userList);
String departmentName = "";

String userName = "";
String userNameFromSession="";
int departmentIdFromSession=0;

	departmentName = (String) request.getParameter("departmentName");
 	if(departmentName ==null) departmentName="";

	String activityNumber = (String)request.getParameter("activityNumber");
	if(activityNumber == null) activityNumber = "";

	String activityType = (String)request.getParameter("activityType");
	if(activityType ==null) activityType = "";

	String address = (String)request.getParameter("address");
	if(address ==null)  address = "";
%>

<html:form action="/addBusinessLicenseApproval">

	<table width="100%" border="0" cellspacing="10" cellpadding="0">
		<TBODY>
			<tr>
				<td><font class="con_hdr_3b">Approval Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font
					class="con_hdr_blue_3b"><%=address%>--<%=activityNumber%>-<%=activityType%> </font><br></td>
			</tr>
			<tr valign="top">
				<td width="99%">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td>
								<table width="100%" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td width="99%" class="tabletitle">Approval History</td>
										<td width="1%" class="tablebutton">
											<nobr>
												<html:button property="back" value="Back" styleClass="button" onclick="javascript:history.back(1);" />&nbsp;

										</nobr>
										</td>
									</tr>
								</table>
								</td>
							</tr>

                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                        <tr>
		                        <td class="tablelabel">Department</td>
		                        <td class="tablelabel"><nobr>Date Referred </nobr></td>
		                        <td class="tablelabel"><nobr>Approval Status</nobr></td>
		                        <td class="tablelabel">Approved By</td>
		                        <td class="tablelabel">Activity Date</td>
		                        <td class="tablelabel">Comments</td>


	                        </tr>

                            <logic:iterate id="activityTeam" name="activityTeamHistoryList" type="elms.control.beans.BusinessLicenseApprovalForm">
			                    <tr valign="top">
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="departmentName" />
								  </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="referredDate" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="approvalStatus" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="userName" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="approvalDate" />
			                      </td>
			                      <td class="tabletext">
			                         <bean:write name="activityTeam" property="comments" />
			                      </td>

							</logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
				</td>
			</tr>
	</table>
</html:form>
</body>
</html:html>


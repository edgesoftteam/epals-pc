<%@ page import='elms.app.bl.*, elms.agent.*,elms.util.*'%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<html:html>
<%
String contextRoot = request.getContextPath();
BusinessLicenseActivity businessLicenseActivity= (BusinessLicenseActivity)request.getAttribute("businessLicenseActivity");
int activityId = StringUtils.s2i((String)request.getAttribute("activityId"));
%>
<head>
<html:base/>
	<title>City of Burbank : Online Business Center : Content Frame</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script language="JavaScript">
function deleteActivity() {
   userInput = confirm("Are you sure you want to delete this Activity?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/deleteActivity.do?activityId=<%=activityId%>'
   }
}
</script>
</head>

<%
String activityNumber = businessLicenseActivity.getActivityNumber();
if (activityNumber == null || activityNumber == "") activityNumber = "";

String activityStatus = businessLicenseActivity.getActivityStatus() != null ? businessLicenseActivity.getActivityStatus().getDescription(): "";
if (activityStatus == null || activityStatus == "") activityStatus = "";

String applicationType = businessLicenseActivity.getApplicationType() != null ? businessLicenseActivity.getApplicationType().getDescription(): "";
if (applicationType == null || applicationType == "") applicationType = "";

String activityType = businessLicenseActivity.getActivityType() != null ? businessLicenseActivity.getActivityType().getDescription(): "";
if (activityType == null) activityType = "";

String addressStreetNumber = (String) businessLicenseActivity.getAddressStreetNumber();
if (addressStreetNumber == null)addressStreetNumber = "";

String addressStreetName = businessLicenseActivity.getAddressStreetName();
if (addressStreetName == null) addressStreetName = "";

String addressUnitNumber = businessLicenseActivity.getAddressUnitNumber();
if (addressUnitNumber == null) addressUnitNumber = "";

String addressCity = businessLicenseActivity.getAddressCity();
if (addressCity == null) addressCity = "";

String addressState = businessLicenseActivity.getAddressState();
if (addressState == null) addressState = "";

String addressZip = businessLicenseActivity.getAddressZip();
if (addressZip == null) addressZip = "";

String addressZip4 = businessLicenseActivity.getAddressZip4();
if (addressZip4 == null) addressZip4 = "";

String businessName = businessLicenseActivity.getBusinessName();
if (businessName == null) businessName = "";

String businessAccountNumber = businessLicenseActivity.getBusinessAccountNumber();
if (businessAccountNumber == null) businessAccountNumber = "";

String corporateName = businessLicenseActivity.getCorporateName();
if (corporateName == null) corporateName = "";

String businessPhone = businessLicenseActivity.getBusinessPhone();
if (businessPhone == null) businessPhone = "";

String businessExtension = businessLicenseActivity.getBusinessExtension();
if (businessExtension == null) businessExtension = "";

String businessFax = businessLicenseActivity.getBusinessFax();
if (businessFax == null) businessFax = "";

String classCode = businessLicenseActivity.getClassCode();
if (classCode == null) classCode = "";

String muncipalCode = businessLicenseActivity.getMuncipalCode();
if (muncipalCode == null) muncipalCode = "";

String sicCode = businessLicenseActivity.getSicCode();
if (sicCode == null) sicCode = "";

boolean decalCode = businessLicenseActivity.isDecalCode();

String descOfBusiness = businessLicenseActivity.getDescOfBusiness();
if (descOfBusiness == null) descOfBusiness = "";

boolean homeOccupation = businessLicenseActivity.isHomeOccupation();

String creationDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getCreationDate());
if (creationDate == null) creationDate = "";

String renewalDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getRenewalDate());
if (renewalDate == null) renewalDate = "";

String applicationDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getApplicationDate());
if (applicationDate == null) applicationDate = "";

String issueDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getIssueDate());
if (issueDate == null) issueDate = "";

String startingDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getStartingDate());
if (startingDate == null) startingDate = "";

String outOfBusinessDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getOutOfBusinessDate());
if (outOfBusinessDate == null) outOfBusinessDate = "";

String ownershipType = businessLicenseActivity.getOwnershipType() != null ? businessLicenseActivity.getOwnershipType().getDescription(): "";
if (ownershipType == null || ownershipType == "") ownershipType = "";

String federalIdNumber = businessLicenseActivity.getFederalIdNumber();
if (federalIdNumber == null) federalIdNumber = "";

String emailAddress = businessLicenseActivity.getEmailAddress();
if (emailAddress == null) emailAddress = "";

String socialSecurityNumber = businessLicenseActivity.getSocialSecurityNumber();
if (socialSecurityNumber == null) socialSecurityNumber = "";

String mailStreetNumber = businessLicenseActivity.getMailStreetNumber();
if (mailStreetNumber == null) mailStreetNumber = "";

String mailStreetName = businessLicenseActivity.getMailStreetName();
if (mailStreetName == null) mailStreetName = "";

String mailUnitNumber = businessLicenseActivity.getMailUnitNumber();
if (mailUnitNumber == null) mailUnitNumber = "";

String mailCity = businessLicenseActivity.getMailCity();
if (mailCity == null) mailCity = "";

String mailState = businessLicenseActivity.getMailState();
if (mailState == null) mailState = "";

String mailZip = businessLicenseActivity.getMailZip();
if (mailZip == null) mailZip = "";
String mailZip4 = businessLicenseActivity.getMailZip4();
if (mailZip4 == null) mailZip4 = "";

String prevStreetNumber = businessLicenseActivity.getPrevStreetNumber();
if (prevStreetNumber == null) prevStreetNumber = "";

String prevStreetName = businessLicenseActivity.getPrevStreetName();
if (prevStreetName == null) prevStreetName = "";

String prevUnitNumber = businessLicenseActivity.getPrevUnitNumber();
if (prevUnitNumber == null) prevUnitNumber = "";

String prevCity = businessLicenseActivity.getPrevCity();
if (prevCity == null) prevCity = "";

String prevState = businessLicenseActivity.getPrevState();
if (prevState == null) prevState = "";

String prevZip = businessLicenseActivity.getPrevZip();
if (prevZip == null) prevZip = "";

String prevZip4 = businessLicenseActivity.getPrevZip4();
if (prevZip4 == null) prevZip4 = "";

String insuranceExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getInsuranceExpDate());
if (insuranceExpDate == null) insuranceExpDate = "";

String bondExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getBondExpDate());
if (bondExpDate == null) bondExpDate = "";

String deptOfJusticeExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getDeptOfJusticeExpDate());
if (deptOfJusticeExpDate == null)  deptOfJusticeExpDate = "";

String federalFirearmsLiscExpDate = elms.util.StringUtils.cal2str(businessLicenseActivity.getFederalFirearmsLiscExpDate());
if (federalFirearmsLiscExpDate == null) federalFirearmsLiscExpDate = "";

String squareFootage = businessLicenseActivity.getSquareFootage();
if (squareFootage == null) squareFootage = "";

String quantity =businessLicenseActivity.getQuantity() != null ? businessLicenseActivity.getQuantity().getDescription(): "";
if (quantity == null || quantity == "") quantity = "";

String quantityNum = businessLicenseActivity.getQuantityNum();
if (quantityNum == null) quantityNum = "";

String typeOfExemptions = businessLicenseActivity.getTypeOfExemptions() != null ? businessLicenseActivity.getTypeOfExemptions().getDescription(): "";
if (typeOfExemptions == null || typeOfExemptions == "") typeOfExemptions = "";

String renewalOnline = businessLicenseActivity.getRenewalOnline();
if (renewalOnline == null) renewalOnline = "";
if(!renewalOnline.equals("") && renewalOnline.equalsIgnoreCase("Y")){
	renewalOnline = "Yes";
}else{
	renewalOnline = "No";
}

int createdBy = businessLicenseActivity.getCreatedBy();
String name = "";
try{
name = LookupAgent.getUserDetails(StringUtils.i2s(createdBy)).getUsername();
if (name == null) name = "";
}catch(Exception e){}

//int lsoId= StringUtils.s2i((String)request.getAttribute("lsoId"));
String lsoId = (String)session.getAttribute("lsoId");
%>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="parent.f_lso.location.href='/epals/lsoSearchWithLso.do?lsoId=<%=lsoId%>';parent.f_psa.location.href='/epals/viewPsaTree.do?lsoId=<%=lsoId%>&psaNodeId=<%=activityId%>'">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
	<tr valign="top">
		<td width="99%">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td colspan="3"><font class="con_hdr_3b">Business Activity Manager</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"></td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr>
				<td colspan="3">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table width="100%" border="0" cellspacing="0" cellpadding="2">
								<tr>
									<td width="100%" class="tabletitle">Business Activity Detail</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td class="tablelabel">
						<table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr>
								<td class="tablelabel"></td>

								<td width="45%" class="tablelabel" align="right"><font class="con_hdr_link">
									<!-- Edit Business License Activity -->
		                         	<a href="<%=contextRoot%>/editBusinessLicenseActivity.do?activityId=<%=activityId%>&lsoId=<%=lsoId%>" class="hdrs">Edit</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                         	<!-- delete Business License Activity -->
									<a href="javascript:deleteActivity();" class="hdrs">Delete</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                           	<!--Print Business License Certificate -->
		                       		<a href="<%=contextRoot%>/editBusinessLicenseActivity.do" class="hdrs" >Print Business License Certificate</a><img src="../images/site_blt_x_000000.gif" width="21" height="9">
		                    	</td>
							</tr>
						</table>
						</td>
					</tr>
	                <tr>
						<td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
							<tr valign="top">
                                <td width="20%" class="tablelabel">Activity Number</td>
                                <td width="20%" class="tabletext"><%=activityNumber%></td>
                                <td width="20%" class="tablelabel">Activity Status</td>
                                <td width="20%" class="tabletext"><%=activityStatus%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Application Type</td>
                                <td class="tabletext"><%=applicationType%></td>
                                <td class="tablelabel">Activity Type</td>
                                <td class="tabletext"><%=activityType%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Business Address</td>
                                <td class="tabletext" colspan="3"><%=addressStreetNumber%> <%=addressStreetName%> <%=addressUnitNumber%> <%=addressCity%> <%=addressState%> <%=addressZip%> <%=addressZip4%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Business Name</td>
                                <td class="tabletext"><%=businessName%></td>
                                <td class="tablelabel">Business Account Number</td>
                                <td class="tabletext"><%=businessAccountNumber%></td>
                            </tr>
                            <tr>
                            	<td class="tablelabel">Renewal Code</td>
								<td class="tabletext"><%=activityId%></td>
								<td class="tablelabel"> Online Renewed</td>
								<td class="tabletext"><%=renewalOnline%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Corporate Name</td>
                                <td class="tabletext" colspan="3"><%=corporateName%></td>
							</tr>
                            <tr valign="top">
                                <td class="tablelabel">Business Phone,Ext</td>
                                <td class="tabletext"><%=businessPhone%> <%=businessExtension%></td>
                                <td class="tablelabel">Business Fax</td>
                                <td class="tabletext"><%=businessFax%></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Class Code</td>
                                <td class="tabletext"><%=classCode%></td>
                                <td class="tablelabel">Municipal Code</td>
                                <td class="tabletext"><%=muncipalCode%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">SIC</td>
                                <td class="tabletext"><%=sicCode%></td>
                                <td class="tablelabel">Decal Code</td>
                                <td class="tabletext"><%=decalCode%></td>
                            </tr>

                             <tr valign="top">
                                <td class="tablelabel">Description of Business</td>
                                <td class="tabletext" colspan="3"><%=descOfBusiness%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Home Occupation</td>
                                <td class="tabletext" colspan="3"><%=homeOccupation%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Creation Date</td>
                                <td class="tabletext"><%=creationDate%></td>
								<td class="tablelabel">Renewal Date</td>
                                <td class="tabletext"><%=renewalDate%></td>
                           	</tr>
							<tr valign="top">
                                <td class="tablelabel">Application Date</td>
                                <td class="tabletext"><%=applicationDate%></td>
                                <td class="tablelabel">Issue Date</td>
                                <td class="tabletext"><%=issueDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Starting Date</td>
                                <td class="tabletext"><%=startingDate%></td>
                                <td class="tablelabel">Out of Business Date</td>
                                <td class="tabletext"><%=outOfBusinessDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Ownership Type</td>
                                <td class="tabletext"><%=ownershipType%></td>
                                <td class="tablelabel">Federal ID Number</td>
                                <td class="tabletext"><%=federalIdNumber%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">E-mail Address</td>
                                <td class="tabletext"><%=emailAddress%></td>
                                <td class="tablelabel">Social Security Number</td>
                                <td class="tabletext"><%=socialSecurityNumber%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Mailing Address</td>
                                <td class="tabletext" colspan="3"><%=mailStreetNumber%> <%=mailStreetName%> <%=mailUnitNumber%> <%=mailCity%> <%=mailState%> <%=mailZip%> <%=mailZip4%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Previous Address</td>
                                <td class="tabletext" colspan="3"><%=prevStreetNumber%> <%=prevStreetName%> <%=prevUnitNumber%> <%=prevCity%> <%=prevState%> <%=prevZip%> <%=prevZip4%></td>
							</tr>
							<tr valign="top">
                                <td class="tablelabel">Insurance Expiration Date</td>
                                <td class="tabletext"><%=insuranceExpDate%></td>
                                <td class="tablelabel">Bond Expiration Date</td>
                                <td class="tabletext"><%=bondExpDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Department of Justice Expiration Date</td>
                                <td class="tabletext"><%=deptOfJusticeExpDate%></td>
                                <td class="tablelabel">Fedaral Firearms License Expiration Date</td>
                                <td class="tabletext"><%=federalFirearmsLiscExpDate%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Square Footage (Area Occupied)</td>
                                <td class="tabletext"><%=squareFootage%></td>
                                <td class="tablelabel">Quantity</td>
                                <td class="tabletext"><%=quantity%> <%=quantityNum%></td>
                            </tr>
							<tr valign="top">
                                <td class="tablelabel">Type Of Exemptions</td>
                                <td class="tabletext"><%=typeOfExemptions%></td>
                                <td class="tablelabel">Created By</td>
                                <td class="tabletext"><%=name%></td>
                            </tr>
						</table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"></table></td></tr>
					</table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			<tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="98%" class="tabletitle">Business License Contact Manager</td>
								<td width="1%" class="tablebutton"><nobr></nobr></td>
								<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addHold.do?levelId=15363&level=A" class="hdrs">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                             </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel"><div align="left">People Type</div></td>
                                <td class="tablelabel"><div align="left">Title</div></td>
                                <td class="tablelabel"><div align="left">Name</div></td>
                                <td class="tablelabel"><div align="left">Phone Number</div></td>
								<td class="tablelabel"><div align="left">Hold</div></td>
                            </tr>

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
			<tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3">
                	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                		<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0"></table></td></tr>
					</table>
                </td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
			<tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td width="98%" background="../images/site_bg_B7C1CB.jpg">Finance Manager</td>
								<td width="1%"><img src="../images/site_hdr_split_1.jpg" width="18" height="19"></td>
								<td width="1%" class="tablelabel"><nobr></nobr></td>
                             </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td width="20%" class="tablelabel"><img src="../images/spacer.gif" width="1" height="1"></td>
                                <td class="tablelabel"><div align="right">Activity</div></td>
                                <td class="tablelabel"><div align="right">Plan Check</div></td>
                                <td class="tablelabel"><div align="right">Business Tax</div></td>
                                <td class="tablelabel"><div align="right">Total</div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Fees</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Payment</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Extended Credit</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Bounced Amount</td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                            <tr>
                                <td class="tablelabel">Balance Due </td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                                <td class="tabletext"><div align="right"></div></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
		</table>
<td width="1%">
        <table width="200" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=15363&level=A&ed=true&status=Inactive" class="hdrs">Holds</a></td>
								<td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addHold.do" class="hdrs"><font class="con_hdr_link">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">

                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
			<tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" background="../images/site_bg_B7C1CB.jpg">Approvals</td>
                        <td width="1%"><img src="../images/site_hdr_split_1.jpg" width="17" height="19"></td>
                        <td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=15363&levelType=A" class="hdrs"><font class="con_hdr_link">View<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Site Data</td>
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/viewSiteStructure.do?levelId=15363&levelType=A" class="hdrs"><font class="con_hdr_link">View<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" background="../images/site_bg_B7C1CB.jpg"><a href="<%=contextRoot%>/listLinkActivity.do?activityId=15363&editable=true" class="hdrs">Linked Activities</a></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr><td background="../images/site_bg_B7C1CB.jpg"><table width="100%" border="0" cellspacing="1" cellpadding="2"></table></td></tr>
                </table>
                </td>
            </tr>
			<tr><td>&nbsp;</td></tr>
           </table>
          </td>
         </tr>
       </table>

	</body>
</html:html>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%
String contextRoot = request.getContextPath();
 %>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
	font: normal 8pt/9pt verdana,arial,helvetica;
	color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>

</head>
<script>
function getlist(){
	var departmentId = document.forms[0].departmentId.value;

	if(departmentId != '99999'){
		var flag = "false";
		var departmentCheck="check";
		document.forms[0].action='<%=contextRoot%>/jsp/bl/listBusinessLicenseApproval.jsp?departmentCheck='+departmentCheck+'&departmentId='+departmentId+'&flag='+flag;
		document.forms[0].submit();
	}else{
		var flag = "true";
		var departmentCheck="uncheck";
		document.forms[0].action='<%=contextRoot%>/jsp/bl/listBusinessLicenseApproval.jsp?departmentCheck='+departmentCheck+'&departmentId='+departmentId+'&flag='+flag;
	    document.forms[0].submit();
	}
}
</script>
<%! String departmentCheck;
	String flag;%>

<%
	try
	{
		departmentCheck=elms.util.StringUtils.nullReplaceWithEmpty((String) request.getParameter("departmentCheck"));
		flag= elms.util.StringUtils.nullReplaceWithEmpty((String) request.getParameter("flag"));

		if(!(departmentCheck.trim().equals("")) && !(departmentCheck==null)){
		int deptId=Integer.parseInt((String) request.getParameter("departmentId"));
		java.util.List singleDepartmentList=new java.util.ArrayList();

		singleDepartmentList = new elms.agent.ActivityAgent().getBusinessLicenseApprovalList(deptId);
		pageContext.setAttribute("singleDepartmentList",singleDepartmentList);
		java.util.List onlyDepartmentName = new java.util.ArrayList();
		onlyDepartmentName= new elms.agent.ActivityAgent().getOnlyDepartmentNameList(deptId);
		pageContext.setAttribute("onlyDepartmentName",onlyDepartmentName);
		}

	}catch(Exception e){//ignored}
	}


java.util.List businessLicenseApprovalDepartmentList = new java.util.ArrayList();
java.util.List departments = new java.util.ArrayList();

try{
departments = new elms.agent.LookupAgent().getDepartmentList();


businessLicenseApprovalDepartmentList = new elms.agent.ActivityAgent().getBusinessLicenseApprovalDepartmentList();
}
catch(Exception e){
//ignored
}
pageContext.setAttribute("businessLicenseApprovalDepartmentList",businessLicenseApprovalDepartmentList);
pageContext.setAttribute("departments",departments);


%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<html:form action="/addBusinessLicenseApproval">



<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="100%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Regulatory Business License Approvals</font><br>
                <br>
                </td>
            </tr>


				 <tr>
            	 <td class="tabletitle" valign="top"><b>Select</b></td>
				</tr>

				 <tr>
					<td  valign="top" class="tablelabel"><nobr> Approving Departments  :   </nobr>
						<html:select property="departmentId" styleClass="textbox" onchange="javascript:getlist()">
						<html:option  value="-1" >Please Select</html:option>
						<html:options collection="departments" property="departmentId" labelProperty="description" />
						</html:select>
					</td>
				</tr>

				<tr>
				<td>
					<table width="100%" cellspacing="0" cellpadding="0">

							<%if((!(departmentCheck.trim().equals("")) && !(departmentCheck==null)) && flag.equalsIgnoreCase("false")){%>
<!-- Start of List of Approvals for selected / single Department -->
							      <tr>
                                    		<td colspan="7" background="../images/site_bg_B7C1CB.jpg" valign="top"><b></b></td>
									</tr>

											<logic:iterate id="onlyDeptName" name="onlyDepartmentName" type="elms.app.bl.BusinessLicenseActivity">
									<tr>
											<td  valign="top" background="../images/site_bg_B7C1CB.jpg"><nobr><b><bean:write name="onlyDeptName"  property="departmentName" /></b></nobr>
									</td>
									</tr>
						<tr><td height="20">&nbsp;</td></tr>
								</logic:iterate>
									<tr>
		                        	<td background="../images/site_bg_B7C1CB.jpg">
			                        <table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
		                            <tr>
				                        <td width="6%" class="tablelabel">Business Acco No</td>
				                        <td width="17%" class="tablelabel">Business Name</td>
				                        <td width="13%" class="tablelabel">Activity Type</td>
				                        <td width="7%" class="tablelabel">Activity Status</td>
										<td width="7%" class="tablelabel">Approval Status</td>
				                        <td width="18%" class="tablelabel">Description of Business</td>
				                        <td width="15%" class="tablelabel">Address </td>
				                        <td width="7%" class="tablelabel">Date Referred </td>
										<td width="10%" class="tablelabel">User Name</td>
		                            </tr>

									<logic:iterate id="singleDepttList" name="singleDepartmentList" type="elms.app.bl.BusinessLicenseActivity">
										<tr>
				                            <td width="6%" class="tabletext" valign="top"><a href="<%=contextRoot%>/viewActivity.do?activityId=<bean:write name="singleDepttList" property="activityId"/>"><bean:write name="singleDepttList" property="businessAccountNumber"/></a></td>
				                            <td width="17%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="businessName"/></td>
				                            <td width="13%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="activityType.description"/></td>
				                            <td width="7%" class="tabletext" valign="top"><a href="<%=contextRoot%>/addBusinessLicenseApproval.do?activityId=<bean:write name="singleDepttList" property="activityId"/>&approvalId=<bean:write name="singleDepttList" property="approvalId"/>&applicationDateString=<bean:write name="singleDepttList" property="applicationDateString"/>&departmentName=<bean:write name="singleDepttList"  property="departmentName"/>&userName=<bean:write name="singleDepttList"  property="userName"/>&address=<bean:write name="singleDepttList"  property="address" />&activityType=<bean:write name="singleDepttList"  property="activityType.description"/>&activityNumber=<bean:write name="singleDepttList" property="activityNumber"/>"><bean:write name="singleDepttList"  property="activityStatusDescription"/></a></td>
											<td width="7%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="approvalStatus"/></td>
				                            <td width="18%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="descOfBusiness"/></td>
				                            <td width="15%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="address" /></td>
				                         	<td width="7%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="applicationDateString" /></td>
											<td width="10%" class="tabletext" valign="top"><bean:write name="singleDepttList"  property="userName" /></td>
				                        </tr>
							</logic:iterate>
							</table>
                        	</td>
                      		</tr>
<!-- End of List of Approvals for selected / single Department -->
							<%}%>

</table></table></table></td></tr></table></html:form></body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="elms.agent.*,elms.common.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%!
boolean businessLicenseUser = false, businessLicenseApproval = false, businessTaxUser = false, businessTaxApproval = false;
String duplicate = "false";
 %>
<%
elms.security.User user = (elms.security.User) session.getAttribute(Constants.USER_KEY);

   	String username = user.getUsername()!=null ? user.getUsername():"";



   	java.util.List groups = (java.util.List) user.getGroups();
   	java.util.Iterator itr = groups.iterator();
   	while(itr.hasNext()){
    	elms.security.Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();

		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
  		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;

   	}






   String contextRoot = request.getContextPath();

   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";

   String lsoAddress = (String)session.getAttribute("lsoAddress");
   if (lsoAddress == null ) lsoAddress = "";

   int i=0;
   String activityId = (String) request.getAttribute("activityId");

   duplicate =(String) request.getAttribute("dup")!=null ? (String) request.getAttribute("dup") : "false" ;


%>
<script language="JavaScript">

function goBack() {
  parent.f_content.location.href='<%=contextRoot%>/viewActivity.do?activityId=<%=activityId%>';
}

function add() {
  parent.f_content.location.href='<%=contextRoot%>/addActivityTeam.do?psaId=<%=activityId%>';
}
function deleteRecord(approvalId,activityId) {
   userInput = confirm("Are you sure you want to delete this record");
	if (userInput==true) {
       document.location.href='<%=contextRoot%>/deleteActivityTeam.do?approvalId='+approvalId+'&activityId='+activityId;

}
}

</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">
          List Activity Team</font>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%></font><br><br>
         </td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<%if(duplicate.equalsIgnoreCase("true")){ %><td width="99%" class="con_hdr_blue_3b"><font style="color:#006633" class="con_hdr_2b">Record Updated Successfully<%} %></font></td>
					</tr>
				</table>
			</td>
		</tr>

        <tr>
          <td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">List Activity Team</td>
                      <td width="1%" class="tablebutton"><nobr>
                     	<html:button property="Back" value="Back" onclick="goBack()" styleClass="button" />&nbsp;&nbsp;
                      	<html:button property="Add" value="Add" onclick="add()" styleClass="button" />&nbsp;&nbsp;

                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> Department</td>
                      <td class="tablelabel"> Approver</td>
                      <td class="tablelabel"> Delete</td>

                    </tr>
                    <logic:iterate id="activityTeam" name="activityTeamList" type="elms.control.beans.BusinessLicenseApprovalForm">
                    <tr valign="top">
                      <td class="tabletext">
                         <bean:write name="activityTeam" property="departmentName" />
					  </td>
                      <td class="tabletext">
                         <bean:write name="activityTeam" property="userName" />
                      </td>
 	 					<TD class="tabletext">
             			<a href="javascript:deleteRecord(<bean:write name="activityTeam" property="approvalId" />,<bean:write name="activityTeam" property="activityId" />);"><img src="../images/delete.gif" alt="Delete" border="0"></a>

             			</TD>

					</logic:iterate>

                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html:html>



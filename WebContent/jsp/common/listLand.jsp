<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,elms.agent.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%
	String lsoId = request.getParameter("lsoId");
	elms.agent.AddressAgent landAgent = new elms.agent.AddressAgent();
	CachedRowSet crset = (CachedRowSet)new AddressAgent().getLandList(lsoId);
    crset.beforeFirst();
%>
<html:html>
<HEAD>
<html:base/>
<TITLE>City of Burbank : Online Business Center : Land Address List</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
</HEAD>

<BODY>

<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b">Land Address List<BR>
            <BR>
            </TD>
        </TR>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Addresses</TD>
                      <TD width="1%" class="tablebutton"><NOBR><A href="<%=request.getContextPath()%>/addLandAddress.do?lsoId=<%=lsoId %>"><FONT class="con_hdr_link">Add<IMG src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></A></NOBR></TD>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Street No</TD>
                      <TD class="tablelabel">Street Name</TD>
                      <TD class="tablelabel">City</TD>
                      <TD class="tablelabel">State</TD>
                      <TD class="tablelabel">Zip</TD>
                      <TD class="tablelabel">Primary</TD>
                    </TR>
 <%
   					while (crset.next()) {
 %>
            	    <TR valign="top">
	 					<TD class="tabletext">
             			<A href="<%=request.getContextPath()%>/editLandAddress.do?lsoId=<%= lsoId%>">
             			<%=crset.getString("STR_NO")%>
             			</A>

             			</TD>

 	 					<TD class="tabletext">
             			<%= crset.getString("STREET_NAME")%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=crset.getString("CITY")%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=crset.getString("STATE")%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=crset.getString("ZIP")%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=crset.getString("PRIMARY")%>

             			</TD>
					</TR>
 <%
 					}
 %>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
    <TD width="1%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD height="32">&nbsp;</TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>

</BODY>
</html:html>

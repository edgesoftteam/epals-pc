<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="java.util.*, elms.util.*" %>
<app:checkLogon/>
<%
       String contextRoot = request.getContextPath();

       String editable = request.getParameter("ed");
	   if(editable == null || editable.equals("")){
	      editable = (String) request.getAttribute("editable");
	      if (editable == null || editable.equals("")) editable = "";
	   }


       // getting userid and group of the login user
       elms.security.User user = (elms.security.User) session.getAttribute("user");
       int userId = 0;
       boolean isSupervisor = false;
       if (user != null){
          userId = user.getUserId();
          isSupervisor =  user.getIsSupervisor();
       }
       CachedRowSet crset = new CachedRowSet();
       crset = (CachedRowSet) request.getAttribute("holdList");
       crset.beforeFirst();
       String levelId = (java.lang.String)request.getAttribute("levelId");
       String level = (java.lang.String) request.getAttribute("level");
  	   String peopleName="";
	   String psaInfo ="";
	   String lsoAddress = "";
	   if(level.equals("Z")){
		   peopleName = (String)session.getAttribute("peopleName");
		   if(peopleName == null) peopleName = "";
	   }else {
           if(level.equals("A") || level.equals("Q") || level.equals("P")){
              psaInfo = (String)session.getAttribute("psaInfo");
              if (psaInfo == null ) psaInfo = "";
           }
           lsoAddress = (String)session.getAttribute("lsoAddress");
           if (lsoAddress == null ) lsoAddress = "";
      }
    String goBackUrl = "";
    if(level.equalsIgnoreCase("L")){
        goBackUrl = contextRoot + "/viewLand.do?lsoId=" + levelId;
    }else if(level.equalsIgnoreCase("S")){
        goBackUrl = contextRoot + "/viewStructure.do?lsoId=" + levelId;
    }else if(level.equalsIgnoreCase("O")){
        goBackUrl = contextRoot + "/viewOccupancy.do?lsoId=" + levelId;
    }else if(level.equalsIgnoreCase("P")){
        goBackUrl = contextRoot + "/viewProject.do?projectId=" + levelId;
    }else if(level.equalsIgnoreCase("Q")){
        goBackUrl = contextRoot + "/viewSubProject.do?subProjectId=" + levelId;
    }else if (level.equalsIgnoreCase("A")) {
      	goBackUrl = contextRoot + "/viewActivity.do?activityId=" + levelId;
    }else if (level.equalsIgnoreCase("Z")) {
      	goBackUrl = contextRoot + "/editPeople.do?id=" + levelId;
    }
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript">
function addHold(){
   document.location.href='<%=contextRoot%>/addHold.do?levelId=<%=levelId%>&level=<%=level%>';
}
function goBack(){
    document.location.href='<%=goBackUrl%>';
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <% if (level.equalsIgnoreCase("Z")) {  %>
                <td><font class="con_hdr_3b">Hold List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=peopleName%><br>
                <br>
                </td>
				<% }else { %>

                <td><font class="con_hdr_3b">Hold List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
                <br>
                </td>
                <% } %>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Holds</td>
                                <td width="1%" class="tablebutton"><nobr>
                                <html:button  property="back" value="Back" styleClass="button" onclick="goBack();"/>&nbsp;
                                <%if(editable.equals("true")){%>
                                 <html:button  property="add" value="Add" styleClass="button" onclick="addHold();"/>
                                <%}%>
                                &nbsp;</nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" width="20">Date</td>
                                <% if (!level.equalsIgnoreCase("Z")) {  %>
                                <td class="tablelabel" width="15">Type</td>
                                <% } %>
                                <td class="tablelabel" width="22">Title</td>
                                <td class="tablelabel" width="22">Issuer</td>
                                <td class="tablelabel" width="16">Status</td>
                                <td class="tablelabel" width="20">Status Date</td>
                                <td class="tablelabel">Comment</td>
                            </tr>
                            <%
   					           while (crset.next()) {
			  					    int createdUserId = crset.getInt("created_by");
			  					    String createdUserName = new elms.agent.AdminAgent().getUser(createdUserId).getFullName();
			  					    if(createdUserName == null) createdUserName="";
			  					    String title = crset.getString("title");
			  					    if(title == null) title = "";
									String holdDt = StringUtils.cal2str(StringUtils.dbDate2cal(crset.getString("creation_dt")));
									String statusDt = StringUtils.cal2str(StringUtils.dbDate2cal(crset.getString("hold_stat_dt")));
									String holdType = crset.getString("hold_type");
									String holdStatus = crset.getString("stat");
									//String title = crset.getString("title");
									String comnt = crset.getString("comnt");
									//if (title ==null) title = "";

									String holdEditable = "";
									if (holdType.equalsIgnoreCase("H")) {
									   if ((createdUserId == userId) || isSupervisor ) holdEditable = "true";
									   else holdEditable = "false";
									}else{
									  //if(editable.equals("true")) holdEditable = "true";
									  //else holdEditable = "false";
									  holdEditable = "true";
									}

									if(comnt == null) comnt ="";
									if (holdType.equalsIgnoreCase("W")) holdType = "Warning";
									if (holdType.equalsIgnoreCase("H")) holdType = "Hard";
									if (holdType.equalsIgnoreCase("S")) holdType = "Soft";

									if (holdStatus.equalsIgnoreCase("A")) holdStatus = "Active";
									if (holdStatus.equalsIgnoreCase("S")) holdStatus = "Suspended";
									if (holdStatus.equalsIgnoreCase("R")) holdStatus = "Released";
									if (holdStatus.equalsIgnoreCase("C")) holdStatus = "Closed";
 %>
                             <tr valign="top">
                                <td class="tabletext"><a href="<%=request.getContextPath()%>/editHold.do?id=<%= crset.getInt("id")%>&level=<%=crset.getString("hold_level")%>&ed=<%=holdEditable%>"> <%=holdDt%> </a> </td>
                                <%if (!level.equalsIgnoreCase("Z")) {  %>
                                  <td class="tabletext"> <%=holdType%></td>
                                <%}%>
                                <td class="tabletext"><%=title%></td>
                                <td class="tabletext"> <%=createdUserName%> </td>

                                <td class="tabletext"> <%=holdStatus%> </td>

                                <td class="tabletext"> <%=statusDt%> </td>

                                <td class="tabletext"> <%=comnt%> </td>
                            </tr>
                            <%
 					          }
                            %>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</body>
</html:html>

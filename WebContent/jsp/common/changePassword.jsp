<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<html:html>
<head>
<html:base target="_top"/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" >
<center><b><font color='red'>
<%
    String message = (String)request.getAttribute("message")!=null ? (String)request.getAttribute("message") : "";
	out.print(message);
%></b>
</center>


<html:errors/>
<html:form focus="username" action="/changePassword?action=perform">
<table border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td align="center"><br>
      <br>
      <br>
    </td>
  </tr>
  <tr>
    <td align="center">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td background="../images/site_bg_B7C1CB.jpg">
            <table width="100%" border="0" cellspacing="1" cellpadding="0">
              <tr valign="top">
                <td class="tabletext"><img src="../images/site_login_img.jpg" width="296" height="72"></td>
              </tr>
              <tr valign="top">
                <td background="../images/site_bg_e5e5e5.jpg">
                  <table width="210" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">&nbsp;</td>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        &nbsp;</td>
                    </tr>
                    <tr>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">User ID:</td>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <html:text  property="username" size="15" styleClass="textbox"/>
                        </td>
                    </tr>
                    <tr>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">Old Password:</td>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <html:password  property="password" size="15" styleClass="textbox" redisplay="false"/></td>
                    </tr>
                    <tr>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">New Password:</td>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <html:password  property="newPassword" size="15" styleClass="textbox" redisplay="false"/></td>
                    </tr>
                    <tr>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">Confirm Password:</td>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <html:password  property="confirmPassword" size="15" styleClass="textbox" redisplay="false"/></td>
                    </tr>
                    <tr>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        <font class="blue2">&nbsp;</td>
                      <td><img src="../images/spacer.gif" width="1" height="3"><br>
                        &nbsp;</td>
                    </tr>
                    <tr align="center">
                      <td colspan="2">
                        <html:submit  property="Submit" value="Change Password" styleClass="button"/>
                      </td>
                    </tr>
                    <tr align="center">
                      <td colspan="2"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<%
String contextRoot = request.getContextPath();
String editable = request.getParameter("ed");
if(editable == null) editable = "true";
String status = request.getParameter("status");
if(status == null) status = "Active";
String goBack = (String)request.getAttribute("goBackUrl");
if (goBack == null ) goBack = "";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
%>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
	strValue=false;
    strValue=validateData('req',document.forms['conditionForm'].elements['text'],'Description is a required field');
    return strValue;
}
function onclickprocess()
{
    document.forms[0].action='<%=contextRoot%>/viewConditionLibrary.do';
    document.forms[0].submit();
    return true;
}
function process(strval)
{
    if (strval == 'back') {
      parent.f_content.location.href='<%=goBack%>'
    }
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/saveCondition" onsubmit="return validateFunction();">
<html:hidden property="conditionId"/>
<html:hidden property="levelId"/>
<html:hidden property="libraryId"/>
<html:hidden property="conditionLevel"/> <!-- THESE VALUES WHOULD BE ADDED AFTER UI UPDATETION -->
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Modify Condition</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Modify Condition</td>


                                <td width="1%" class="tablebutton"><nobr>
                                   <html:button  value="Cancel" property="cancel" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                                   <html:button property="Library" value="Library" styleClass="button" onclick="return onclickprocess()"/> -->
                                   <%if(editable.equals("true")){%>
                                     <html:submit property="Update" value="Update" styleClass="button"/> &nbsp;</nobr>
                                   <%}%>
                                </td>

                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel">Date </td>
                                <td class="tablelabel">User </td>
                                <td class="tablelabel">Condition Code</td>
                                <td class="tablelabel">Key Words</td>
                            </tr>

                            <tr valign="top">
                                <td class="tabletext"><bean:write name="conditionForm" property="updatedDate"/></td>
                                <td class="tabletext"><bean:write name="conditionForm" property="updatedByName"/></td>
                                <td class="tabletext"><bean:write name="conditionForm" property="conditionCode"/></td>
                                <td class="tabletext"><bean:write name="conditionForm" property="shortText"/></td>
                            </tr>

                            <tr>
                                <td class="tablelabel">Inspectability</td>
                                <td class="tablelabel">Warning</td>
                                <td class="tablelabel">Complete</td>
                                <td class="tablelabel">Description</td>
                            </tr>


                            <tr>
                                <html:hidden property="tmpText"/>
								<td class="tabletext"><html:checkbox property="inspectable"/></td>
                                <td class="tabletext"><html:checkbox property="warning"/></td>
                                <td class="tabletext"><html:checkbox property="complete"/></td>
                                <td class="tabletext"><html:textarea rows="10" cols="60" property="text" onblur="return ValidateBlankSpace(this);"/></td>
                            </tr>

                            <tr valign="top">
                                <td class="tabletext" colspan="9">&nbsp;</td>
                            </tr>

                        </table>
                        </td>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<app:checkLogon/>
<%
   String contextRoot = request.getContextPath();
   String editable = request.getParameter("ed");
   if(editable == null) editable = "true";
   String status = request.getParameter("status");
   if(status == null) status = "true";

   String goBack = (String)request.getAttribute("goBackUrl");
   if (goBack == null ) goBack = "";



   String psaInfo = (String)session.getAttribute("psaInfo");
   if (psaInfo == null ) psaInfo = "";
   String lsoAddress = (String)session.getAttribute("lsoAddress");
   if (lsoAddress == null ) lsoAddress = "";
%>
<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Modify Hold</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<script>
function validate()
{
var a=new Array(4000);
a = document.commentForm.comments.value;
if(document.commentForm.comments.value == ""){
	  alert("Please enter some comment");
	  document.commentForm.comments.focus();
	  return false;
	}
else if (a.length > 3999) {
alert("You have exceeded the maximum Limit");
alert("The total no of charecters entered are:" + a.length);
alert("Please adjust to 4000 charecters");
    }
 else{
	  document.commentForm.action='<%=contextRoot%>/saveComment.do';
	  document.commentForm.submit();
}
}

function process(strval)
{
    if (strval == 'back') {
      parent.f_content.location.href='<%=goBack%>'
    }
}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/saveComment">
<html:hidden property="commentId"/>
<html:hidden property="levelId"/>
<html:hidden property="commentLevel"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Modify Comment</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
               </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Modify</td>
                                <td width="1%" class="tablebutton"><nobr>
                                <html:button property="back" value="Back" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                                <%if(editable.equals("true")&& status.equals("Active")){%>
                                  <html:button property="Save" value="Update" styleClass="button" onclick="return validate();"/> &nbsp;</nobr></td>
                                <% }  %>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tabletext" colspan="9"></td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel">Entry Date</td>
                                <td class="tablelabel">Entered By </td>
                                <td class="tablelabel"> Updated Date</td>
                                <td class="tablelabel">Updated By</td>
                            </tr>
                            <tr valign="top">
                                <td class="tabletext"><bean:write name="commentForm" property="creationDate"/></td>
                                <td class="tabletext" nowrap><bean:write name="commentForm" property="createdUserName"/></td>
                                <td class="tabletext"><bean:write name="commentForm" property="updatedDate"/></td>
                                <td class="tabletext" nowrap><bean:write name="commentForm" property="updatedUserName"/></td>
                    		</tr>
							<!--<tr>
                                <td class="tabletext" colspan="4" wrap><bean:write name="commentForm" property="updatedUserName"/></td>
                            </tr>-->
                            <tr valign="top">
                                <td class="tabletext" colspan="4"><img src="../images/spacer.gif" width="Y" height="7"></td>
                            </tr>
                        </table>

                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <logic:equal name="commentForm" property="commentLevel" value="A" >
                                <td class="tablelabel">Internal</td>
                                </logic:equal>
                                <td class="tablelabel">Comments</td>
                            </tr>
                            <tr valign="top">
                                <logic:equal name="commentForm" property="commentLevel" value="A" >
                                <td class="tabletext"><html:checkbox property="internal" value="Y"/></td>
                                </logic:equal>
                                <td class="tabletext"><html:textarea rows="12" cols="80" property="comments"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="java.util.*, elms.util.*" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
    String contextRoot = request.getContextPath();

    String editable = request.getParameter("ed");
	if(editable == null) editable = "true";

    CachedRowSet crset = new CachedRowSet();
    crset = (CachedRowSet) request.getAttribute("holdList");
    crset.beforeFirst();

    String levelId = (java.lang.String)request.getAttribute("levelId");
    String level = (java.lang.String) request.getAttribute("level");
    String holdId = (java.lang.String) request.getAttribute("holdId");
    String peopleName="";
	String psaInfo ="";
	String lsoAddress ="";

	if(level.equals("Z")){
		peopleName = (String)session.getAttribute("peopleName");
		if(peopleName == null) peopleName = "";
		}else
		{
    if(level.equals("A") || level.equals("Q") || level.equals("P")){
      psaInfo = (String)session.getAttribute("psaInfo");
      if (psaInfo == null ) psaInfo = "";
    }
    lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";

    }
    String goBackUrl = "";
    goBackUrl = contextRoot + "/listHold.do?levelId=" + levelId + "&level=" + level;
%>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
function goBack(){
  document.location.href='<%=goBackUrl%>';
}
var strValue;
function validateFunction()
{
	//strValue=false;
    if (document.forms['holdForm'].elements['type'].value == '')
    {
    	alert('Select Hold Type');
    	document.forms['holdForm'].elements['type'].focus();
    	strValue=false;
    }
    else
    {
    	strValue=true;
    }

    if (strValue == true)
    {
    	    strValue=validateData('req',document.forms['holdForm'].elements['comments'],'Comments is a required field');
    }
    return strValue;
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/saveHold" onsubmit="return validateFunction();">

<input type="hidden" name="id" value="<%=holdId%>">
<input type="hidden" name="level" value="<%=level%>">
<input type="hidden" name="levelId" value="<%=levelId%>">
<input type="hidden" name="statusDate" value="<%=request.getAttribute("statusDate")%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
                <% if (level.equalsIgnoreCase("Z")) {  %>
                <td><font class="con_hdr_3b">Hold List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=peopleName%><br>
                <br>
                </td>
				<% }else { %>

                <td><font class="con_hdr_3b">Hold List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
                <br>
                </td>
                <% } %>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Modify</td>
                                <td width="1%" class="tablebutton"><nobr>
                                  <html:button  property="back" value="Back" styleClass="button" onclick="goBack();"/>&nbsp;
                                  <%if(editable.equals("true")){%>
                                  <html:submit  property="Save" value="Update" styleClass="button"/>
                                  <%}%>
                        &nbsp;</nobr></td></tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> Date</td>
                      <td class="tablelabel"> Type</td>
                      <td class="tablelabel"> Issuer</td>
                      <td class="tablelabel">Status</td>
                      <td class="tablelabel">Status Date </td>
                      <td class="tablelabel">Title</td>
                      <td class="tablelabel"> Comments</td>
                    </tr>
					<%
						String holdDt = "";
						String holdType = "";
						String holdStatus = "";
						String title = "";
						String comnt = "";
						String createdBy="";
						elms.security.User user = new elms.security.User();
						elms.agent.AdminAgent userAgent = new elms.agent.AdminAgent();


   					while (crset.next()) {

						holdDt = elms.util.StringUtils.cal2str(elms.util.StringUtils.dbDate2cal(crset.getString("creation_dt")));
						holdType = crset.getString("hold_type");
						holdStatus = crset.getString("stat");
						title = crset.getString("title");
						comnt = crset.getString("description");

						if (title ==null) title = "";
						if(comnt == null) comnt ="";
						if (holdType.equalsIgnoreCase("W")) holdType = "Warning";
						if (holdType.equalsIgnoreCase("H")) holdType = "Hard";
						if (holdType.equalsIgnoreCase("S")) holdType = "Soft";
						if (holdType.equalsIgnoreCase("Z")) holdType = "People";

						if (holdStatus.equalsIgnoreCase("A")) holdStatus = "Active";
						if (holdStatus.equalsIgnoreCase("S")) holdStatus = "Suspended";
						if (holdStatus.equalsIgnoreCase("R")) holdStatus = "Released";
						if (holdStatus.equalsIgnoreCase("C")) holdStatus = "Closed";

						user = userAgent.getUser(crset.getInt("updated_by"));
						createdBy = user.getFirstName() + " " + user.getLastName();

					%>
                    <tr valign="top">
                      <td class="tabletext"><%=holdDt%></td>
                      <td class="tabletext"><%=holdType%></td>
                      <td class="tabletext"><%=createdBy%></td>
                      <td class="tabletext"><%=holdStatus%></td>
                      <td class="tabletext"><nobr> <%=request.getAttribute("statusDate")%></nobr></td>
                      <td class="tabletext"><%=title%></td>
                      <td class="tabletext" wrap> <%=comnt%></td>
                    </tr>
                    <%}%>
                    <tr valign="top">
                      <td class="tabletext" colspan="9"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <%if(editable.equals("true")){%>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                    <% if (!level.equalsIgnoreCase("Z")) {  %>
                      <td class="tablelabel"> Type</td>
                      <% } %>
                      <td class="tablelabel">Status</td>
                      <td class="tablelabel">Title</td>
                      <td class="tablelabel"> Comments</td>
                    </tr>
                    <tr valign="top">
                      <% if (level.equalsIgnoreCase("Z")) {  %>
                      <td class="tabletext">
                          <html:select property="status" styleClass="textbox">
                            <html:option value="A">Active</html:option>
                            <html:option value="C">Closed</html:option>
                        </html:select>
                      </td>
     				<input type="hidden" name="type" value="Z">
					<%  }
					else {   %>
                      <td class="tabletext">
                          <html:select property="type" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="W">Warning</html:option>
                            <html:option value="H">Hard</html:option>
                            <html:option value="S">Soft</html:option>
                        </html:select>
                      </td>
                      <td class="tabletext">
                          <html:select property="status" styleClass="textbox">
                            <html:option value="A">Active</html:option>
                            <html:option value="S">Suspended</html:option>
                            <html:option value="R">Released</html:option>
                        </html:select>
                      </td>
                   <% }   %>

 					  <td class="tabletext"><html:text property="title" size="15" maxlength="30" styleClass="textbox"/></td>
                      <td class="tabletext"><html:textarea rows="6" cols="45" property="comments"></html:textarea>
                      </td>
                    </tr>
                  </table>
                  <%}%>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

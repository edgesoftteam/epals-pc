<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<%
String contextRoot = request.getContextPath();
String goBack = (String)request.getAttribute("goBackUrl");
if (goBack == null ) goBack = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
function validateFunction()
{
    strValue=true;
    if (document.forms['conditionForm'].elements['shortText'].value == '')
    {
    	alert('Key Word is a required field');
    	document.forms['conditionForm'].elements['shortText'].focus();
    	strValue=false;
    }
    if(strValue == true){
       if(document.forms['conditionForm'].elements['text'].value == '')
       {
   	       strValue=validateData('req',document.forms['conditionForm'].elements['text'],'Description is a required field');
    	   document.forms['conditionForm'].elements['text'].focus();
    	   strValue=false;
       }
    }
    return strValue;
}

function process(strval)
{
    if (strval == 'back') {
      parent.f_content.location.href='<%=goBack%>'
    }else if (strval == 'library') {
      document.conditionForm.action='<%=contextRoot%>/viewConditionLibrary.do';
      document.conditionForm.submit();
    }
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/, ''); };
function ValidateBlankSpace(str){
	if(str.value!=""){
		if(str.value.trim().length==0){
			alert('This is a mandatory field, it should not start with blank spaces');
 			str.value="";
			str.focus();
		}
	}
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/saveCondition" onsubmit="return validateFunction();">
<html:hidden property="conditionId" value="0"/>
<html:hidden property="libraryId" value="0"/>
<html:hidden property="levelId"/>
<html:hidden property="conditionLevel"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Condition</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Add
                        Condition</td>
                          <td width="1%" class="tablebutton"><nobr>
                              <html:button property="back" value="Back" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                              <html:button property="library" value="Library" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
					          <html:submit  property="Submit" value="Save" styleClass="button"/>
                              &nbsp;</nobr>
                          </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> Key Words</td>
                      <td class="tablelabel"> Description</td>
                      <td class="tablelabel"> Inspectability</td>
                      <td class="tablelabel"> Warning</td>
                      <td class="tablelabel"> Complete</td>
                    </tr>
                    <tr valign="top">
                      <td class="tabletext" width="15%">
                        <html:text  property="shortText" size="30" maxlength="30" styleClass="textbox" onblur="return ValidateBlankSpace(this);"/>
                      </td>
                      <td class="tabletext" width="30%">
                        <html:textarea rows="6" cols="30" property="text" onblur="return ValidateBlankSpace(this);"/>
                      </td>
                      <td class="tabletext" width="5%">
                        <html:checkbox  property="inspectable"/>
                      </td>
                      <td class="tabletext" width="5%" >
                        <html:checkbox  property="warning"/>
                      </td>
                      <td class="tabletext" width="5%">
                        <html:checkbox  property="complete"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

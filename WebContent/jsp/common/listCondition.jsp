<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<app:checkLogon/>
<%
String contextRoot = request.getContextPath();
String editable = request.getParameter("ed");
if(editable == null) editable = "true";
String status = (String)request.getParameter("status");
if(status == null) status = "Active";
String goBack = (String)request.getAttribute("goBackUrl");
if (goBack == null ) goBack = "";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo ="";
psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<link type="text/css" href="../script/jquery-ui-1.8.2.custom/development-bundle/themes/base/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" href="../script/jquery-ui-1.8.2.custom/development-bundle/demos/demos.css" rel="stylesheet" />
<script src="../script/jquery-ui-1.8.2.custom/js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../script/jquery-ui-1.8.2.custom/js/jquery-ui-1.8.2.custom.min.js" type="text/javascript"></script>

</head>
<script>
function process(strval)
{
    if (strval == 'back') {
    	document.forms['conditionForm'].elements['back'].disabled = true;
      	parent.f_content.location.href='<%=goBack%>';
    }else if (strval == 'delete') {
      document.conditionForm.action='<%=contextRoot%>/listCondition.do';
      document.conditionForm.submit();
    }else if (strval == 'add') {
      document.conditionForm.action='<%=contextRoot%>/addCondition.do';
      document.conditionForm.submit();
    }
}
</script>
<script type="text/javascript">

$(function(){
	$("#sortable").sortable({
		update: function saveOrder(){
			var result = $('#sortable').sortable('toArray');
			var order ="";
				for (var i = 0; i < result.length; i++) {
		           order += result[i] + ",";
				}

				if(order != ''){
				    order = order.substring(0, order.length-1);
				    document.conditionForm.action='<%=contextRoot%>/listCondition.do?action=saveOrder&order='+order;
			        document.conditionForm.submit();
				}else {
					return false;
				}
			}
	});
	$("#sortable").disableSelection();
	var result = $('#sortable').sortable('toArray');
});




</script>
<html:form action="/listCondition">
<html:hidden property="levelId"/>
<html:hidden property="conditionLevel"/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Conditions List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                               <td width="96%" class="tabletitle">Conditions</td>
                               <td width="2%" class="tablebutton"><nobr>
                                  <html:button property="back" value="Back" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;

                                     <% if(editable.equals("true")){%>
                	                 <html:button property="delete" value="Delete" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                                    <html:button property="add" value="Add" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                         		    <%}%>
                               </td>
                          </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Delete</td>
                                <td class="tablelabel" width="1%"><nobr>Condition Code</nobr></td>
                                <td width="80%" class="tablelabel">Key Words</td>
                                <td width="25%" class="tablelabel">Drag-Drop</td>
                            </tr>

                            <ul id="sortable">
	                            <logic:iterate id="cond" name="conditionForm" property="conditions">
		                            <tr valign="top" class="demo" id="list<bean:write name="cond" property="conditionId"/>">
		                                <td class="tabletext"><html:multibox property="selectedConditions">
		                                	<bean:write name="cond" property="conditionId"/></html:multibox>
		                                </td>
		                                <td class="tabletext" width="1%">
		                                	<a href="<%=contextRoot%>/editCondition.do?conditionId=<bean:write name="cond" property="conditionId"/>&ed=<%=editable%>&status=<%=status%>">
		                                		<bean:write name="cond" property="conditionCode"/>
		                                	</a>
		                                </td>
		                                <td class="tabletext">
		                                	<bean:write name="cond" property="shortText"/>
		                                </td>
		                                <td class="tabletext" width="15%"><img src="../images/dd_sort.jpg" width="25" height="25">
		                                </td>
		                            </tr>
	                            </logic:iterate>
                            </ul>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</body>
</html:form>
</html:html>

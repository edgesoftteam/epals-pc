
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ page import="java.util.List" %>
<%@ page import="elms.app.bl.AttachmentType" %>


<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();

String levelId = (String)request.getAttribute("levelId");
String actId = (String)request.getAttribute("actId");
request.setAttribute("actId", actId);
String level = (String)request.getAttribute("level");
String department = (String)request.getAttribute("department");

String fromWhere ="";
if(request.getAttribute("fromWhere") != null){
	fromWhere = (String)request.getAttribute("fromWhere");
}

//System.out.println("actId... "+actId+" department.."+department+" fromwhere.. "+fromWhere);
String fromDate="";
String toDate="";
if(request.getAttribute("fromDate")!= null){
	   fromDate=(String)request.getAttribute("fromDate");
}
if(request.getAttribute("toDate")!= null){
	   toDate=(String)request.getAttribute("toDate");
}

String psaInfo ="";
if(level.equals("A") || level.equals("Q") || level.equals("P")){
  psaInfo = (String)session.getAttribute("psaInfo") !=null ? (String)session.getAttribute("psaInfo")  : "" ;
}

String lsoAddress = (String)session.getAttribute("lsoAddress") !=null ? (String)session.getAttribute("lsoAddress") : "" ;
Object attachTypes = request.getAttribute("attachmentTypes");
List<AttachmentType> attachmentTypes = (List<AttachmentType>)attachTypes;

%>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
var strValue;
var level = "<%=level%>";
var levelId = "<%=levelId%>";

function submitCancel() {
    switch (level) {
    	case 'A' :
    		{
    		document.forms[0].action='<%=contextRoot%>/viewActivity.do?activityId=<%=levelId%>';
    		break;
    		}
    	case 'Q' :
    		{
    		document.forms[0].action='<%=contextRoot%>/viewSubProject.do?subProjectId=<%=levelId%>';
    		break;
    		}
    	case 'P' :
    		{
    		document.forms[0].action='<%=contextRoot%>/viewProject.do?projectId=<%=levelId%>';
    		break;
    		}
    	case 'L' :
    		{
    		document.forms[0].action='<%=contextRoot%>/viewLand.do?lsoId=<%=levelId%>';
    		break;
    		}
    	case 'S' :
    		{
    		document.forms[0].action='<%=contextRoot%>/viewStructure.do?lsoId=<%=levelId%>';
    		break;
    		}
    	case 'O' :
    		{
    		document.forms[0].action='<%=contextRoot%>/viewOccupancy.do?lsoId=<%=levelId%>';
    		break;
    		}
    	case 'F' :
		{
			document.forms[0].action='<%=contextRoot%>/editPlanCheck.do?fromWhere=<%=fromWhere%>&deptDesc=<%=department%>&planCheckId=<%=levelId%>';
    		break;
		}
    	default :
    		break;
    }
	document.forms[0].submit();
	return true;
}

function addAttachment() {
    if (document.forms['attachmentForm'].elements['description'].value == '') {
    	alert('Description is a required field');
    	document.forms['attachmentForm'].elements['description'].focus();
    	return false;
    }	
    if (document.forms['attachmentForm'].elements['theFile'].value == '') {
    	alert('Please browse for the file to upload');
    	document.forms['attachmentForm'].elements['theFile'].focus();
    	return false;
    }

	document.forms['attachmentForm'].action='<%=contextRoot%>/saveAttachment.do?actId=<%=actId%>&fromWhere=<%=fromWhere%>';
	document.forms['attachmentForm'].submit();
	return true;
}

function deleteAttachment(attachmentId) {
   userInput = confirm("Are you sure you want to delete this Attachment?");
   if (userInput==true) {
       document.location.href='<%=contextRoot%>/deleteAttachments.do?attachmentId='+attachmentId+'&levelId=<%=levelId%>&level=<%=level%>&actId=<%=actId%>&fromWhere=<%=fromWhere%>';
   }
}

</SCRIPT>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/saveAttachment" name="attachmentForm" type = "elms.control.beans.AttachmentForm" enctype ="multipart/form-data" onsubmit="false">
<html:hidden property="levelId" value="<%=levelId%>"/>
<html:hidden property="level" value="<%=level%>"/>
<html:hidden property="attachmentId" value="0"/>
<%-- <html:hidden property="actId" value="<%=actId%>"/> --%>

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
            <html:errors/>
         	</td>
        </tr>

        <tr>
            <td><font class="con_hdr_3b">Add Attachments</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
        </tr>
		<tr>
		  <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Add</td>
                        <td width="1%" class="tablebutton"><nobr>
                        <html:button property = "cancel" value="Back" onclick = "return submitCancel();" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>

              <tr>

                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Description</td>
					  <td class="tabletext">
                        <html:text  property="description" size="40" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">File Name</td>
                      <td class="tabletext">
							<html:file property="theFile" styleClass="textbox" size="40" />
                      </td>
                      <td class="tablelabel" width="20%">Attachment Type</td>
					  <td colspan="3" class="tabletext">
						 <html:select property="attachmentType" size="1" styleClass="textbox">
						    <html:option value="">Please Select</html:option>
							<html:options collection="attachmentTypes" property="id" labelProperty="description"/>
						 </html:select><BR>
					  </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Keyword1</td>
					  <td class="tabletext">
                        <html:text  property="keyword1" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Keyword2</td>
					  <td class="tabletext">
                        <html:text  property="keyword2" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Keyword3</td>
					  <td class="tabletext">
                        <html:text  property="keyword3" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Keyword4</td>
					  <td class="tabletext">
                        <html:text  property="keyword4" size="10" styleClass="textbox"/>
                      </td>
                     <td class="tabletext">
                     <html:button property = "add" value="Add" onclick="return addAttachment();" styleClass="button"/>
                    </td>
                    </tr>
                  </table>
                </td>
              </tr>

            </table>
          </td>
        </tr>
        <tr>
          <td><img src="../images/spacer.gif" width="1" height="7"></td>
        </tr>

        <logic:present name="attachments" scope="request">
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="90%" class="tabletitle">Attachments</td>
                    <td width="10%" class="tabletitle">
                        &nbsp;
                    </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel"> File Name</td>
                      <td class="tablelabel"> Keyword1</td>
                      <td class="tablelabel"> Keyword2</td>
                      <td class="tablelabel"> Keyword3</td>
                      <td class="tablelabel"> Keyword4</td>
                      <td class="tablelabel"> Attachment Type</td>
                      <td class="tablelabel" align="right"> File Size</td>
                      <td class="tablelabel" align="right"> Date</td>
                      <td class="tablelabel" align="right"> Edit</td>
                      <td class="tablelabel" align="right"> Delete</td>
                    </tr>
                    <%
						java.util.ResourceBundle obcProperties = elms.util.db.Wrapper.getResourceBundle();
                           String DESTINATION_SERVER_URL = "/attachments/";
                    %>
                    <logic:iterate id="attachment" name="attachments" type="elms.app.common.Attachment" scope="request">
                        <tr valign="top">
                            <td class="tabletext"><a target="_blank" href="<%=request.getContextPath()%><%=DESTINATION_SERVER_URL%><bean:write name="attachment" property="file.fileName"/>"> <bean:write name="attachment" property="file.fileName"/> </a></td>
                            <td class="tabletext"> <bean:write name="attachment" property="keyword1"/></td>
                            <td class="tabletext"> <bean:write name="attachment" property="keyword2"/></td>
                            <td class="tabletext"> <bean:write name="attachment" property="keyword3"/></td>
                            <td class="tabletext"> <bean:write name="attachment" property="keyword4"/></td>
                            <td class="tabletext"> <bean:write name="attachment" property="attachmentType"/></td>
                            <td class="tabletext" align="right"> <bean:write name="attachment" property="file.fileSize"/> </td>
                            <td class="tabletext" align="right"> <bean:write name="attachment" property="file.strCreateDate"/> </td>
                            <td class="tabletext" align="right"><a href="<%=contextRoot%>/editAttachment.do?attachmentId=<bean:write name="attachment" property="attachmentId"/>&levelId=<%=levelId%>&actId=<%=actId%>&level=<%=level%>&fromWhere=<%=fromWhere%>&department=<%=department%>"><img src="../images/pencil.gif" alt="Edit" border="0"></a></td>
                            <td class="tabletext" align="right"><a href="javascript:deleteAttachment(<bean:write name="attachment" property="attachmentId"/>);"><img src="../images/delete.gif" alt="Delete" border="0"></a></td>
                        </tr>
                    </logic:iterate>
                 </table>
                </td>
              </tr>

            </table>
          </td>
        </tr>
        </logic:present>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

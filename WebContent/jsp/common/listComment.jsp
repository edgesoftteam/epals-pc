<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<app:checkLogon/>
<%
String contextRoot = request.getContextPath();
String editable = request.getParameter("ed");
if(editable == null) editable = "true";
String status = (String)request.getParameter("status");
if(status == null) status = "Active";
String goBack = (String)request.getAttribute("goBackUrl");
if (goBack == null ) goBack = "";
String online = (String)session.getAttribute("online");
if (online == null ) online = "";
if(!online.equals("") && online.equals("Y")){
	  int levelId = Integer.parseInt(request.getParameter("levelId"));
	String	url = "/myPermits.do?action=explore&viewPermit=Yes&activityId=";
    url = request.getContextPath() + url + levelId;
	goBack =url;
}

String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script>
function process(strval)
{
    if (strval == 'back') {
            parent.f_content.location.href='<%=goBack%>'
    }else if (strval == 'delete') {
      document.commentForm.action='<%=contextRoot%>/listComment.do';
      document.commentForm.submit();
    }else if (strval == 'add') {
      document.commentForm.action='<%=contextRoot%>/addComment.do';
      document.commentForm.submit();
    }
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
process('back');
}

}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0"
	marginwidth="0" marginheight="0">
<html:form action="/listComment">
<html:hidden property="levelId"/>
<html:hidden property="commentLevel"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Comment List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="96%" class="tabletitle">Comments</td>
                                <td width="2%" class="tablebutton"><nobr>
                                <html:button property="back" value="Back" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                                   <% if(editable.equals("true")){%>

                                   <html:button property="delete" value="Delete" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                                <html:button property="add" value="Add" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                                </nobr></td>
     	                      	<%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Delete</td>
                                <td class="tablelabel">Date</td>
                                <td class="tablelabel">Entered By</td>
                                <logic:equal name="commentForm" property="commentLevel" value="A" >
                                <td class="tablelabel">Internal</td>
                                </logic:equal>
                                <td class="tablelabel">Comments</td>
                            </tr>
                            <logic:iterate id="comment" name="commentForm" property="commentsList">
                            <tr valign="top">
                                <td class="tabletext"><html:multibox name="commentForm" property="selectedComments"><bean:write name="comment" property="commentId"/></html:multibox></td>
                                <td class="tabletext"> <a href="<%=contextRoot%>/editComment.do?id=<bean:write name="comment" property="commentId"/>&ed=<%=editable%>&status=<%=status%>"><bean:write name="comment" property="displayEnterDate"/></a> </td>
                                <td class="tabletext" nowrap><bean:write name="comment" property="enteredUserName"/></td>
			                    <logic:equal name="commentForm" property="commentLevel" value="A" >
                                <td class="tabletext"><bean:write name="comment" property="internal"/></td>
							    </logic:equal>
                                <td class="tabletext" wrap><bean:write name="comment" property="comment"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>

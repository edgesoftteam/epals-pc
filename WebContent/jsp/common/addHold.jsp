<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
    String contextRoot = request.getContextPath();
    String levelId = (java.lang.String)request.getAttribute("levelId");
    String level = (java.lang.String) request.getAttribute("level");
    String statusDate = (java.lang.String) request.getAttribute("statusDate");
	pageContext.setAttribute("statusDate", statusDate);
   	//java.util.List users = (java.util.List) request.getAttribute("users");
	String peopleName="";
	String psaInfo ="";
	String lsoAddress = "";

	if(level.equals("Z")){
		peopleName = (String)session.getAttribute("peopleName");
		if(peopleName == null) peopleName = "";
		}else
		{

    if(level.equals("A") || level.equals("Q") || level.equals("P")){
      psaInfo = (String)session.getAttribute("psaInfo");
      if (psaInfo == null ) psaInfo = "";
    }
    lsoAddress = (String)session.getAttribute("lsoAddress");
    if (lsoAddress == null ) lsoAddress = "";

    }
    String goBackUrl = "";
    goBackUrl = contextRoot + "/listHold.do?levelId=" + levelId + "&level=" + level;
%>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<SCRIPT language="JavaScript">
function goBack(){
    document.location.href='<%=goBackUrl%>';
}
var strValue;
function validateFunction()
{
	//strValue=false;
    if (document.forms['holdForm'].elements['type'].value == '')
    {
    	alert('Select Hold Type');
    	document.forms['holdForm'].elements['type'].focus();
    	strValue=false;
    }
    else
    {
    	strValue=true;
    }
    if (strValue == true)
    {
    	strValue=validateData('req',document.forms['holdForm'].elements['statusDate'],'Status Date is a required field');
    }
    if (strValue == true)
    {
    	//alert(document.forms['projectForm'].elements['expirationDate'].value);
		if (document.forms['holdForm'].elements['statusDate'].value != '')
    	{
    		strValue=validateData('date',document.forms['holdForm'].elements['statusDate'],'Invalid date format');
    	}
    }
    if (strValue == true)
    {
    	    strValue=validateData('req',document.forms['holdForm'].elements['title'],'Title is a required field');
    }
    if (strValue == true)
    {
    	    strValue=validateData('req',document.forms['holdForm'].elements['comments'],'Comments is a required field');
    }
    return strValue;
}
</SCRIPT>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/saveHold" onsubmit="return validateFunction();">
<html:hidden property="id" value="0"/>
<html:hidden property="level" value="<%=level%>"/>
<html:hidden property="levelId" value="<%=levelId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
                <% if (level.equalsIgnoreCase("Z")) {  %>
                <td><font class="con_hdr_3b">Hold List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=peopleName%><br>
                <br>
                </td>
				<% }else { %>

                <td><font class="con_hdr_3b">Hold List</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br>
                <br>
                </td>
                <% } %>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Add</td>
                      <td width="1%" class="tablebutton"><nobr>
                      <html:button  property="back" value="Back" styleClass="button" onclick="goBack();"/>&nbsp;
                      <html:submit  property="Submit" value="Save" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <% if (level.equalsIgnoreCase("Z")) {  %>
                      <td class="tablelabel">Status</td>
                      <% }else { %>
                      <td class="tablelabel">Type</td>
                      <td class="tablelabel">Status</td>
                      <% } %>
                      <td class="tablelabel">Status Date</td>
                      <td class="tablelabel">Title</td>
                      <td class="tablelabel">Comments</td>
                    </tr>
                    <tr valign="top">
                    <% if (level.equalsIgnoreCase("Z")) {  %>
                      <td class="tabletext">
                          <html:select property="status" styleClass="textbox">
                            <html:option value="A">Active</html:option>
                            <html:option value="C">Closed</html:option>
                        </html:select>
                      </td>
     				<input type="hidden" name="type" value="Z">
					<%  }
					else {   %>
                      <td class="tabletext">
                          <html:select property="type" styleClass="textbox">
                            <html:option value="">Please Select</html:option>
                            <html:option value="W">Warning</html:option>
                            <html:option value="H">Hard</html:option>
                            <html:option value="S">Soft</html:option>
                        </html:select>
                      </td>
                      <td class="tabletext">
                          <html:select property="status" styleClass="textbox">
                            <html:option value="A">Active</html:option>
                            <html:option value="S">Suspended</html:option>
                            <html:option value="R">Released</html:option>
                        </html:select>
                      </td>
                   <% }   %>
                      <td class="tabletext"><nobr>
                          <html:text  property="statusDate" size="12" value="<%=statusDate%>" styleClass="textbox" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
					    <a href="javascript:show_calendar('holdForm.statusDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></a></nobr>
					  </td>
                      <td class="tabletext">
                        <html:text property="title" size="15" maxlength="30" styleClass="textbox"/>
                      </td>
                      <td class="tabletext">
                          <html:textarea rows="6" cols="45" property="comments">
						</html:textarea>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

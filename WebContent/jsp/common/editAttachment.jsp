
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Edit Attachment</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String psaInfo ="";
String contextRoot = request.getContextPath();

String levelId = (java.lang.String)request.getAttribute("levelId");
String level = (java.lang.String)request.getAttribute("level");
String actId = (String)request.getAttribute("actId");
String department = (String)request.getAttribute("department");

String fromWhere ="";
if(request.getAttribute("fromWhere") != null){
	fromWhere = (String)request.getAttribute("fromWhere");
}
//System.out.println("department.."+department+" fromwhere.. "+fromWhere);
request.setAttribute("actId", actId);
request.setAttribute("fromWhere", fromWhere);
request.setAttribute("department", department);

if(level.equals("A") || level.equals("Q") || level.equals("P")){
  psaInfo = (String)session.getAttribute("psaInfo");
  if (psaInfo == null ) psaInfo = "";
}

String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";


%>

<script language="JavaScript" src="../script/calendar.js"></script>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>

<SCRIPT language="JavaScript">

function goBack() {
  parent.f_content.location.href='<%=contextRoot%>/addAttachment.do?levelId=<%=levelId%>&level=<%=level%>&actId=<%=actId%>&fromWhere=<%=fromWhere%>';
}
function update() {

	document.forms[0].action='<%=contextRoot%>/updateAttachment.do?actId=<%=actId%>&department=<%=department%>&fromWhere=<%=fromWhere%>';
	document.forms[0].submit();
}

</SCRIPT>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/updateAttachment" >

<html:hidden property="attachmentId"/>
<html:hidden property="levelId" />
<html:hidden property="level" value="<%=level%>"/>

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td><font class="con_hdr_3b">Edit Attachments Details</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
        </tr>
		<tr>

		  <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="99%" class="tabletitle">Edit</td>
                                <td width="1%" class="tablebutton"><nobr>
                        <html:button property = "cancel" value="Back" styleClass="button" onclick="goBack()"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Description</td>
					  <td class="tabletext">
                        <html:text  property="description" size="40" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">File Name</td>
                      <td class="tabletext">
						<html:text  property="fileName" size="40" styleClass="textbox" readonly="true"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Keyword1</td>
					  <td class="tabletext">
                        <html:text  property="keyword1" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Keyword2</td>
					  <td class="tabletext">
                        <html:text  property="keyword2" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Keyword3</td>
					  <td class="tabletext">
                        <html:text  property="keyword3" size="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel">Keyword4</td>
					  <td class="tabletext">
                        <html:text  property="keyword4" size="10" styleClass="textbox"/>
                      </td>
                     <td class="tabletext">
<%--                      <html:submit value="Update" styleClass="button"/> --%>
                     <html:button property = "Update" value="Update" onclick="return update();" styleClass="button"/>
                     
                    </td>
                    </tr>
                  </table>
                </td>
              </tr>

            </table>
          </td>
        </tr>
        <tr>
          <td><img src="../images/spacer.gif" width="1" height="7"></td>
        </tr>

        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

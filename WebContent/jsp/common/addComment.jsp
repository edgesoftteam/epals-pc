<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<app:checkLogon/>
<head>
<%
String contextRoot = request.getContextPath();
String goBack = (String)request.getAttribute("goBackUrl");
if (goBack == null ) goBack = "";
String psaInfo = (String)session.getAttribute("psaInfo");
if (psaInfo == null ) psaInfo = "";
String lsoAddress = (String)session.getAttribute("lsoAddress");
if (lsoAddress == null ) lsoAddress = "";
java.util.List users = (java.util.List) request.getAttribute("users");
%>
<html:html>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="javascript" src="../script/actb.js"></script>
<script language="javascript" src="../script/common.js"></script>

<script language="javascript">

function validate()
{
	var a=new Array(4000);
	a = document.commentForm.comments.value;
	if(document.commentForm.comments.value == ""){
			alert("Please enter some comment");
			document.commentForm.comments.focus();
			return false;
		} else if (a.length > 3999) {
			alert("You have exceeded the maximum Limit");
			alert("The total no of charecters entered are:" + a.length);
			alert("Please adjust to 4000 charecters");
	    } else{
			document.commentForm.action='<%=contextRoot%>/saveComment.do';
			document.commentForm.submit();
		}
}

function process(strval)
{
    if (strval == 'back') {
      parent.f_content.location.href='<%=goBack%>';
    }
}

function load(){
	document.commentForm.internal.checked = true;
	document.commentForm.comments.focus();
}

</script>
<script language="JavaScript" src="../script/calendar.js"></script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="load();">
<html:form action="/saveComment">
<html:hidden property="commentId" value="0"/>
<html:hidden property="levelId"/>
<html:hidden property="commentLevel"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Add Comment</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font class="con_hdr_blue_3b"><%=lsoAddress%><%=psaInfo%><br><br>
         </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Add</td>
                                <td width="1%" class="tablebutton">
                                   <nobr>
                                   <html:button property="back" value="Back" onclick="process(this.name)" styleClass="button" />&nbsp;&nbsp;
                                   <html:button property="Submit" value="Save" styleClass="button" onclick="return validate();"/> &nbsp;
                                   </nobr>
                                </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel">Entered By </td>
			                <logic:equal name="commentForm" property="commentLevel" value="A" >
                                <td class="tablelabel">Internal</td>
                            </logic:equal>
                            </tr>
                            <tr valign="top">
                                <td class="tabletext">
									<html:select property="createdBy" styleClass="textbox">
										<html:options  collection="users"  property="userId" labelProperty="name" />
									</html:select>

                                	<logic:equal name="commentForm" property="commentLevel" value="A">
								    <html:checkbox property="uptSubProject" styleId="checkbox2"/>Update Sub-Project Status
								    <html:checkbox property="uptActivities" styleId="checkbox2"/>Update Activities Status
								    </logic:equal>
								    <logic:equal name="commentForm" property="commentLevel" value="Q">
								    <html:checkbox property="uptActivities" styleId="checkbox2"/>Update Activities Status
								    </logic:equal>

                                </td>
                                <logic:equal name="commentForm" property="commentLevel" value="A" >
                                <td class="tabletext"><html:checkbox property="internal" value="Y"/></td>
                            	</logic:equal>
                            </tr>
                            <tr>
                            <td colspan="2" class="tabletext"><html:textarea rows="25" cols="80" property="comments" /></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>

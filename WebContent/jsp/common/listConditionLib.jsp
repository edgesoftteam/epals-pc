<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>

<app:checkLogon/>

<html:html>
<%
String contextRoot = request.getContextPath();
 %>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script language="JavaScript" src="../script/calendar.js"></script>
<script>
function processLib()
{
   var levelId=document.forms[0].levelId.value;
   var conditionLevel=document.forms[0].conditionLevel.value;
   var checkDate=document.forms[0].checkDate.value;
     parent.f_content.location.href='<%=contextRoot%>/viewConditionLibrary.do?levelId='+levelId+'&conditionLevel='+conditionLevel+'&checkDate='+checkDate;


}
</script>
</head>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/addToCondition">
<html:hidden property="levelId"/>
<html:hidden property="conditionLevel"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="100%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Conditions Library List</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="96%" height="25" class="tabletitle">Conditions Library</td>
                      <td width="3%" class="tablebutton"><nobr>
                       <html:button  value="Back" property="Back" styleClass="button" onclick="javascript:history.back()"/>&nbsp;
                        <html:submit value="Add Conditions" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
					  <table width="100%" border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="10%" height="15" class="tablelabel">Date</td>
			           <td><nobr>
							<html:text property="checkDate" size="10" maxlength="10" styleClass="textbox"/>
							 	<html:link href="javascript:show_calendar('forms[0].checkDate');" onmouseover="window.status='Calendar';return true;" onmouseout="window.status='';return true;">
				                	<img src="../images/calendar.gif" width="16" height="15" border=0/>
								</html:link></nobr>

                        <html:button property="library" value="Library" onclick="processLib()" styleClass="button" />&nbsp;&nbsp;
						</td>

                    </tr>
					</table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel">Select</td>
                      <td class="tablelabel">Condition Code</td>
                      <td class="tablelabel"><nobr>Condition Type</nobr></td>
                      <td class="tablelabel">Inspectability</td>
                      <td class="tablelabel">Warning</td>
                      <td class="tablelabel">Required</td>
                      <td class="tablelabel">Key Words</td>

                    </tr>
                    <nested:iterate name="conditionLibraryForm" property="conditionLibraryRecords" type="elms.app.common.ConditionLibraryRecord">
                    <tr valign="top">
                     <td class="tabletext"><nested:checkbox styleId="checkbox2" property="select"/>
                     <nested:write property="addUpdateFlag"/></td>
                     <td class="tabletext"><nested:write property="conditionCode"/></td>
                     <td class="tabletext"><nested:write property="levelType"/></td>
                     <td class="tabletext"><nested:write property="inspectability"/></td>
                     <td class="tabletext"><nested:write property="warning"/></td>
                     <td class="tabletext"><nested:write property="required"/></td>
                     <td class="tabletext"><nested:write property="short_text"/></td>
                     <nested:hidden property="libraryId"/>
                     </tr>
                     <tr valign="top">
                       <td class="tabletext"></td>
                       <td class="tabletext" colspan="6"><nested:write property="condition_text"/></td>
                     </tr>
                    </nested:iterate>


                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ page import="java.util.*,elms.agent.*" %>
<html:html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="css/elms.css" type="text/css">

<%!
List streets = new ArrayList(); // for the Street list
%>
<%
 String contextRoot = request.getContextPath();
  streets = (List) AddressAgent.getStreetArrayList();
 request.setAttribute("streets",streets);
%>
</head>
<script language="javascript" type="text/javascript" src="script/actb.js"></script>
<script language="javascript" type="text/javascript" src="script/common.js"></script>
<script language="JavaScript"><!--
function setPointer() {
var streetText=document.forms[0].streetText.value;
for(var q=0;q<=<%=streets.size()%>;q++){
	if((streetText.toUpperCase())==((document.forms[0].streetName[q].text).toUpperCase())){
		document.forms[0].streetName[q].selected=true;
	}
}
	parent.f_content.location.href="frameContentWait.jsp"
    if (document.all){
        for (var i=0;i < document.all.length; i++){
              document.all(i).style.cursor = 'wait';
        }
    }
}
function resetFields() {
	document.forms[0].elements['streetNumber'].value='';
}

function addFocus() {
	document.forms[0].streetNumber.focus();
}

var streetArray = new Array();
<%
if(streets != null && streets.size() > 0){

for(int i=0; i<streets.size();i++){
elms.app.lso.Street st = (elms.app.lso.Street)streets.get(i);
%>
streetArray[<%=i%>] = '<%=st.getStreetName()%>';
<%}}%>
//--></script>
<logic:present scope="request" name="lsoSearchForm">
	<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg" onload="addFocus();">
</logic:present>

<logic:notPresent scope="request" name="lsoSearchForm">
	<body bgcolor="#e5e5e5" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" background="images/site_bg_e5e5e5.jpg" onload="resetFields();addFocus()">
</logic:notPresent>

<html:form action="/lsoSearch?clear=YES" >
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td class="frametitle" >&nbsp;&nbsp;ADDRESS EXPLORER</td>
            <td  class="frametitle"  align="right" nowrap>
                 <html:submit  property="Find" value="Find"  styleClass="button"  onclick="javascript:setPointer();" />&nbsp;
            </td>
        </tr>
      </table>
    </td>
  </tr>
    <tr>
        <td valign="top"  class="tableform-top"><img src="images/spacer.gif" width="200" height="5"></td>
    </tr>
  <tr>
    <td class="tableform">
      <table width="100%" border="0" cellspacing="2" cellpadding="0">
      <tr>
	          <td><font class="framelabel">Street #</font></td>
	          <td><font class="framelabel">Street Name</font></td>
       </tr>
        <tr>
          <td valign="top" nowrap><html:text  property="streetNumber" size="4" styleClass="textbox"/></td>
          <td valign="top" nowrap>
			<div style="visibility:hidden">
				<html:select property="streetName" styleClass="textbox" style="position:absolute">
					<html:option value=""> Please select</html:option>
					<html:options collection="streets" property="streetId" labelProperty="streetName" />
				</html:select>
			</div>
			<input type="text" name="streetText" id="st" size="18%" style="padding:-2px;" />
			<script>
				var streetObj = new actb(document.getElementById('st'),streetArray);
			</script>
          </td>
        </tr>
      </table>
    </td>
  </tr>

    <tr>
    <td background="images/site_bg_B7C1CB.jpg"><img src="images/spacer.gif" width="1" height="1"></td>
  </tr>
      <tr>
        <td class="tableform">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            	<td>
            		<table cellpadding="2" cellspacing="0" border="0">
							<tr>
								<td><html:radio property="radiobutton" value="radio_active" /></td>
								<td class="framelabel">Active</td>
								<td><img src="images/spacer.gif" width="10" height="1"></td>
								<td><html:radio property="radiobutton" value="radio_all" /></td>
								<td class="framelabel">All</td>
							</tr>
					</table>
				</td>
                <td ><img src="images/spacer.gif" width="1" height="3"><br>
                </td>
            </tr>
        </table>
        </td>
    </tr>

    <tr>
        <td valign="top"  class="tableform-bottom"><img src="images/spacer.gif" width="200" height="5"></td>
    </tr>

    <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><span id="menu" onclick="if
		   	(document.all.menu1a1.style.display =='block') {
			document.all.menu1a1.style.display='none';} else
			{document.all.menu1a1.style.display='block';}">
			<font class="treetext"><img src="images/spacer.gif" width="1" height="6"></font><br>
            </span></td>
        </tr>
      </table>

    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
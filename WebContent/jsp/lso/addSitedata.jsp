<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<title>City of Burbank : Online Business Center : Add Site Data</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<script language="JavaScript" src="../script/calendar.js">
</script>



<body  class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<html:form action="/addSitedata" >

<table width="102%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Site Data</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Site
                        Data </td>


                      <td width="1%" class="tablebutton"><nobr>

                        <html:reset  value="Cancel" styleClass="button"/>
                        <html:submit  property="Submit" value="Save" styleClass="button"/>

                       </nobr></td>

                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Follow
                        Up </td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:textarea property="followUp" cols="50" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Index</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="index" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Buiding
                        Name </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="buildingName" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr class="tabletext">
                      <td valign="top" colspan="4"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top" colspan="5">Address
                        Range:</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Low</td>
                      <td class="tablelabel" valign="top">High</td>
                      <td class="tablelabel" valign="top">Dir</td>
                      <td class="tablelabel" valign="top">Street
                        Name</td>
                      <td class="tablelabel" valign="top">Struct
                        Address </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="dir1" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="structAddressCheck1" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="select2" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="radiobutton" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="select2" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="radiobutton" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="select2" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="radiobutton" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="select2" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="radiobutton" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="select2" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="radiobutton" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="low1" size="6" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:select property="select2" styleClass="textbox">
                          <html:option value="">N</html:option>
                        </html:select>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="streetName" size="25" styleClass="textbox"/>
                      </td>
                      <td valign="top" class="tabletext">
                        <html:radio  property="radiobutton" value="radiobutton"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top" colspan="5"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top" colspan="5">Parcel
                        Numbers </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                      <td valign="top" class="tabletext">
                        <html:text  property="parcel1" size="15" styleClass="textbox"/>
                        </td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top" colspan="5"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Current
                        Primary Use</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="currentPrimaryUse" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Primary
                        Use Date</td>
                      <td class="tabletext" valign="top"><nobr>
                       <html:text  property="currentPrimaryUseDate" size="10" styleClass="textbox"/>
					    <html:link href="javascript:show_calendar('addSitedate.currentPrimaryUseDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">No
                        of Structures On Site</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="noOfStructuresOnSite" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">No.
                        of Res Units</td>
                      <td class="tabletext" valign="top">





                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Plans
                        on File</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="plansOnFileCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Bldg
                        Permit No.</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="bldgPermitNo" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Date
                        of Bldg Permit</td>
                      <td class="tabletext" valign="top"><nobr>
                       <html:text  property="dateOfBldgPermit" size="10" styleClass="textbox"/>
					    <html:link href="javascript:show_calendar('addSitedate.dateOfBldgPermit');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      <td class="tablelabel" valign="top">Notes</td>
                      <td class="tabletext" valign="top">
                        <html:textarea property="notes" cols="15" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Bldg
                        Const Type Code</td>
                      <td class="tabletext" valign="top">
                        <html:select property="bldgConstTypeCode" styleClass="textbox">
                          <html:option  value="">00007</html:option>
                        </html:select>
                      </td>
                      <td class="tablelabel" valign="top">Height</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="ft" size="3" styleClass="textbox"/>
                        ft.&nbsp;
                        <input type="text" property="in" size="2" styleClass="textbox"/>
                        in. </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Number
                        of Stories Above Grade (Zoning)</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="numberOfStoriesAboveGradeZone" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Number
                        of Stories Above Grade </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="numberOfStoriesAboveGrade" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Number
                        of Stories Below Grade</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="numberOfStoriesBelowGrade" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Total
                        Floor Area (Sq Ft) (Gross w/o Parking Area</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="totalFloorArea" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Total
                        Floor Area (Sq Ft) (Bldg Code)</td>
                      <td class="tabletext" valign="top">
                          <html:text  property="totalFloorAreaBldgCode" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Mezzanine</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="mezzanineCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Penthouse</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="panthouseCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Area
                        of Parking (Sq Ft)</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="areaOfAparking" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Occupant
                        Load (Recorded)</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="occupantLoad" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Occupant
                        Load (Current Use)</td>
                      <td class="tabletext" valign="top">
                        <html:textarea property="occupantLoadCurrent" cols="15" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Occupancy/Div</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="occupancyDiv" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Year
                        Permit Applied</td>
                      <td class="tabletext" valign="top"><nobr>
                        <html:checkbox  property="yearPermitAppliedCheck" value="checkbox" />
                        <html:text  property="yearPermitAppliedDate" size="10" styleClass="textbox" />
					    <html:link href="javascript:show_calendar('addSitedate.yearPermitAppliedDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Design
                        Code</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="designCode" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Major
                        Structural Alterations</td>
                      <td class="tabletext" valign="top"><nobr>
                        <html:checkbox  property="majorStructuralAlterationsCheck" value="checkbox"/>
                       <html:text  property="majorStructuralAlterationsDate" size="10" styleClass="textbox"/>
					    <html:link href="javascript:show_calendar('addSitedate.majorStructuralAlterationsDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Major
                        Structural Additions</td>
                      <td class="tabletext" valign="top"><nobr>
                        <html:checkbox  property="majorStructuralAdditionsCheck" value="checkbox"/>
                        <html:text  property="majorStructuralAdditionsDate" size="10" styleClass="textbox"/>
					    <html:link href="javascript:show_calendar('addSitedate.majorStructuralAdditionsDate');"
                        onmouseover="window.status='Calendar';return true;"
                        onmouseout="window.status='';return true;">
                        <img src="../images/calendar.gif" width="16" height="15" border=0/></html:link></nobr>
                      </td>
                      <td class="tablelabel" valign="top">High
                        Fire Severity Zone</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="highFireSeverityZoneCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Flood
                        Area</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="floodAreaCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Liquefaction</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="liquefactionCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Local
                        Fault Zone</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="localFaultZoneCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">URM</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="urmCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Retrofitted</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="retrofittedCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Sprinklered</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="sprinkleredCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Hazardous
                        Materials </td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="hazardousMeterialsCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Alarms</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="alarmsCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">No.
                        of Elevators</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="noOfElevators" size="2" styleClass="textbox"/>
                        </td>
                      <td class="tablelabel" valign="top">Elevator
                        Enclosure </td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="elevatorEnclosureCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Zone
                        (Permit) </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="zonePermit" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Zone
                        (Current) </td>
                      <td class="tabletext" valign="top">
                        <html:select property="zoneCurrent" styleClass="textbox">
                          <html:option value="">C-3</html:option>
                        </html:select>
                        </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Census
                        Tract</td>
                      <td class="tabletext" valign="top">
                        <html:select property="cansusTract" styleClass="textbox">
                          <html:option value="">7008</html:option>
                        </html:select>
                        </td>
                      <td class="tablelabel" valign="top">Parking
                        Required</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="parkingRequired" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Parking
                        Provided</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="parkingProvidedCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Striped</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="stripedCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Estimate
                        of Width</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="estimateOfWidthFt" size="3" styleClass="textbox"/>
                        ft.&nbsp;
<input type="text" property="estimateOfWidthIn" size="2" styleClass="textbox"/>
                        in. </td>
                      <td class="tablelabel" valign="top">Onsite
                        Parking </td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="onsiteParkingCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">On
                        Grade</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="onGradeCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Within
                        Structure</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="withinStructureCheck" value="checkbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Subterranean</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="subterraneanCheck" value="checkbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Major
                        Structural Alterations</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="majorStructuralAlterations" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Covenant
                        Off-Site</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="covenantOffSite" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Covenant
                        On-Site</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="covenantOnSite" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Address</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="address" size="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Parcel
                        No </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="parcelNo" size="25" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">In-Lieu
                        Parking</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="inLieuParking" value="checkbox"/>

                      </td>
                      <td class="tablelabel" valign="top">&nbsp;</td>
                      <td class="tabletext" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Other
                        Zoning Determinations</td>
                      <td class="tabletext" valign="top">
                        <html:textarea property="otherZoningDeterminations" cols="25" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                      <td class="tablelabel" valign="top">Notes</td>
                      <td class="tabletext" valign="top">
                        <html:textarea property="notes" cols="25" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr class="tabletext">
                      <td valign="top" colspan="4"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top" colspan="4">Systems</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top" colspan="2">Primary
                        System</td>
                      <td class="tablelabel" valign="top" colspan="2">Secondary
                        System</td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top" colspan="2">
                        <html:select property="primarySystem" styleClass="textbox">
                        <html:option value="">Primary System</html:option> </html:select>
                        </td>
                      <td class="tabletext" valign="top" colspan="2">
                        <html:select property="secondarySystem" styleClass="textbox">
                        <html:option value="">Secondary System</html:option> </html:select>
                        </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Basic
                        Score</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="basicScoreP" size="2" styleClass="textbox"/>
                        </td>
                      <td class="tablelabel" valign="top">Basic
                        Score</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="basicSocreS" size="2" styleClass="textbox"/>
                        </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Comment
                        Primary System</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:textarea property="commentPrimarySystem" cols="50" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Comment
                        Secondary System</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:textarea property="commentSecondarySystem" cols="50" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr class="tabletext">
                      <td valign="top" colspan="4"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top" colspan="4">Systems</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top" colspan="2">Primary
                        System of Addition</td>
                      <td class="tablelabel" valign="top" colspan="2">Secondary
                        System of Addition</td>
                    </tr>
                    <tr>
                      <td class="tabletext" valign="top" colspan="2">
                        <html:select property="primarySystemOfAddition" styleClass="textbox">
                        <html:option value="">Primary System of Addition</html:option>
                        </html:select> </td>
                      <td class="tabletext" valign="top" colspan="2"> <html:select property="secondarySystemOfAddition" styleClass="textbox">
                        <html:option value="">Secondary System of Addition</html:option>
                        </html:select> </td>
                    </tr>


                     <tr>
                      <td class="tablelabel" valign="top">Basic
                        Score</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="basicScoreP" size="2" styleClass="textbox"/>
                        </td>
                      <td class="tablelabel" valign="top">Basic
                        Score</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="basicSocreS" size="2" styleClass="textbox"/>
                        </td>
                    </tr>



                  </table>
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Comment
                        Primary System of Addition</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:textarea property="commentPrimarySystemAddition" cols="50" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Comment
                        Secondary System of Addition</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:textarea property="commentSecondarySystemAddition" cols="50" styleClass="textbox" rows="2"></html:textarea>
                      </td>
                    </tr>
                    <tr class="tabletext">
                      <td valign="top" colspan="4"><img src="../images/spacer.gif" width="1" height="7"></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">&nbsp;
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>
<%@ page import="java.util.*" %>
<%@ page import="elms.app.lso.*" %>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<link rel="stylesheet" href="../css/elms.css" type="text/css">
<script src="../script/sorttable.js"></script>
<style type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</style>


</head>
<%
	String contextRoot = request.getContextPath();
	java.util.List specialResults = (java.util.List) request.getAttribute("specialResults");
	java.util.Iterator iter = specialResults.iterator();
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Search Results</font>
<%
if(specialResults.size() <= 0) {
%>
- No search results found.  Please adjust the search criteria.
<%
 }
%>

			<br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Search Results (<%=specialResults.size()%> records found)</td>
                      <td width="1%" class="tablebutton">
                      <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr align=left>
                      <th class="tablelabel">Address</th>
					  <th class="tablelabel">Apn</th>
					  <th class="tablelabel">Owner Name</th>
                     </tr>
<%
OwnerApnAddress ownerApnAddress = new OwnerApnAddress();
if(specialResults.size() == 1) {
	ownerApnAddress = (OwnerApnAddress)specialResults.get(0);
}

while (iter.hasNext()){
ownerApnAddress = (OwnerApnAddress)specialResults.get(0);
ownerApnAddress = (OwnerApnAddress)iter.next();
%>
                    <tr valign="top">
					     <td class="tabletext"><A href='<%=contextRoot%>/assessorData.do?action=view&amp;lsoId=<%=ownerApnAddress.getLsoId()%>'><%=ownerApnAddress.getAddress()%></A></td>
                         <td class="tabletext"><%=ownerApnAddress.getApn()%>&nbsp;</td>
                         <td class="tabletext"><%=ownerApnAddress.getOwnerName()%></a></td>
	                </tr>
<%
}
%>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html:html>


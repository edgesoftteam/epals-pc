<%@page import="elms.control.beans.*,elms.agent.*,java.util.*"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<jsp:useBean id="useAgent" scope="request" class="elms.agent.AddressAgent"/>
<jsp:useBean id="zoneAgent" scope="request" class="elms.agent.AddressAgent"/>
<%
String contextRoot = request.getContextPath();
String streetId = ((LandForm)request.getAttribute("landForm")).getAddressStreetName();
if(streetId==null) streetId = "0";
String lsoId = (java.lang.String)session.getAttribute("lsoId");
java.util.List parkingZones = (java.util.List) LookupAgent.getParkingZones();
request.setAttribute("parkingZones",parkingZones);
List  streets = (List) AddressAgent.getStreetArrayList();
request.setAttribute("streets",streets);

%>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script>
function dspAddress(strValue) {
		if (strValue == 'N')
		{
			{
				local1.style.display="";
				local2.style.display="";
				foreign1.style.display="none";
				foreign2.style.display="none";
			}

		}
		else
		{
				local1.style.display="none";
				local2.style.display="none";
				foreign1.style.display="";
				foreign2.style.display="";
		}
}
function validateFunction()
{

       var useGroup = document.forms[0].elements['selectedUse'];
       useGroupChecked = false;
       for(var i = 0 ; i < useGroup.length ; ++i) {
          if(useGroup[i].checked){
             useGroupChecked = true;
             break;
          }
       }
       if(!useGroupChecked){
         alert('Use is a required field');
         return false;
       }

       strValue=validateData('req',document.forms[0].elements['addressStreetNumber'],'Street Number is a required field');
       if (strValue == true)
       {
 		   strValue=validateData('req',document.forms[0].elements['addressStreetName'],'Street Name is a required field');
           if (strValue == true)
           {
		  		strValue=validateData('req',document.forms[0].elements['addressZip'],'Zip Code is a required field');
		        if (strValue == true)
		        {
		 		     document.forms[0].action='<%=contextRoot%>/saveLand.do';
					document.forms[0].submit();
		 		}
		   }
  	   }
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt);  //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event; //code for IE
}
if(e.keyCode == 13){
validateFunction();
}

}

function resetFields(){
  document.forms[0].action='<%=contextRoot%>/addLand.do';
  document.forms[0].submit();
}
</script>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form focus="alias" action="/saveLand">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Add Land</font><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Land Details</td>

                                <td width="1%" class="tablebutton"><nobr>
									<input type="reset" name="Reset" value="Reset" class="button" onclick="resetFields()" />&nbsp;
									<html:button property="save"  value="Save" styleClass="button" onclick="validateFunction()" />&nbsp;</nobr>
								</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Alias</td>
                                <td class="tabletext" valign="top"><html:text property="alias" size="30" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Description</td>
                                <td class="tabletext" valign="top"><html:text property="description" size="20" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Active</td>
                                <td class="tabletext" valign="top" colspan="3"><html:checkbox property="active"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Zone</td>
                                <td class="tabletext" valign="top"><logic:iterate id="zone" property="zone" name="zoneAgent"> <html:multibox property="selectedZone"> <bean:write name="zone"/> </html:multibox><font class="con_text_1"/><bean:write name="zone"/><br></logic:iterate></td>

                                <td class="tablelabel" valign="top">Use</td>
                                <td class="tabletext" valign="top"><logic:iterate id="landUse" property="landUse" name="useAgent"> <html:multibox property="selectedUse"> <bean:write name="landUse"/> </html:multibox> <font class="con_text_1"/><bean:write name="landUse"/><br></logic:iterate></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Coordinate X: </td>
                                <td class="tabletext" valign="top"><html:text property="coordinateX" size="25" maxlength="11" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Coordinate Y: </td>
                                <td class="tabletext" valign="top"><html:text property="coordinateY" size="25" maxlength="11" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">P - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="parkingZone" styleClass="textbox">
									  <html:option value="-1">Please select</html:option>
                                      <html:options collection="parkingZones" property="pzoneId" labelProperty="name"/>
                                   </html:select>
                          		</td>
                                <td class="tablelabel" valign="top">R - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="resZone" styleClass="textbox">
									  <html:option value="68">None</html:option>
									  <html:option value="66">R-1 (SFR)</html:option>
									  <html:option value="67">R-4 (MFR)</html:option>
									  <html:option value="69">C-1 (COM)</html:option>
                          		   </html:select>
                          		</td>
                            </tr>
                             <tr>
                                <td class="tablelabel" valign="top">Label </td>
                                <td class="tabletext" valign="top" colspan="3">
                                 <html:text property="label" size="20" maxlength="15" styleClass="textbox"/>
                          		</td>

                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Land Address </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Street No</td>
                                <td class="tabletext" valign="top"><html:text property="addressStreetNumber" size="10" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                                <td class="tablelabel" valign="top">Street Mod</td>
                                <td class="tabletext" valign="top"><html:select property="addressStreetModifier" styleClass="textbox">
										                            <html:option value=""></html:option><html:option value="1/4">1/4</html:option><html:option value="1/3">1/3</html:option><html:option value="1/2">1/2</html:option><html:option value="3/4">3/4</html:option>
                          										   </html:select></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Street Name</td>
                                <td class="tabletext" valign="top">

                                <table>
                                    <tr>
                                        <td>
	                                        <html:select property="addressStreetName" styleClass="textbox">
	                                        	<html:option value="">Please Select</html:option>
	                                        	<html:options collection="streets" property="streetId" labelProperty="streetName"/>
	                                        </html:select>
                                        </td>
                                    </tr>
                                </table>
                                </td>
                                <td class="tablelabel" valign="top">&nbsp;</td>
                                <td class="tabletext" valign="top">&nbsp;</td>

                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">City</td>
                                <td class="tabletext" valign="top"><html:text property="addressCity" size="25" value="" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">State</td>
                                <td class="tabletext" valign="top"><html:text property="addressState" size="5"  value="" maxlength="2" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Zip Code</td>
                                <td class="tabletext" valign="top"><html:text property="addressZip" size="6" maxlength="5" styleClass="textbox" onkeypress="return validateInteger();" />&nbsp; <html:text property="addressZip4" maxlength="4" size="4" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                                <td class="tablelabel" valign="top">Description</td>
                                <td class="tabletext" valign="top"><html:text property="addressDescription" size="30" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Primary</td>
                                <td class="tabletext" valign="top"><html:checkbox property="addressPrimary"/></td>
                                <td class="tablelabel" valign="top">Active</td>
                                <td class="tabletext" valign="top" colspan="3"><html:checkbox property="addressActive"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Land APN</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Foreign Address?</td>
                                <td class="tabletext" valign="top" colspan="3"> <html:radio property="apnForeignAddress" value="N" onclick="dspAddress(this.value);"/> No&nbsp;&nbsp;&nbsp; <html:radio property="apnForeignAddress" value="Y" onclick="dspAddress(this.value);"/> Yes (Enter Below)</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">APN #</td>
                                <td class="tabletext" valign="top"><html:text property="apn" size="25" maxlength="10" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Owner Name</td>
                                <td class="tabletext" valign="top"><html:text property="apnOwnerName" size="25" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr id="local1">
                                <td class="tablelabel" valign="top">Street No</td>
                                <td class="tabletext" valign="top"><html:text property="apnStreetNumber" size="10" styleClass="textbox" onkeypress="return validateInteger();" /></td>
                                <td class="tablelabel" valign="top">Street Mod</td>
                                <td class="tabletext" valign="top">
                                	<html:select property="apnStreetModifier" styleClass="textbox">
                                		<html:option value=""></html:option>
                                		<html:option value="1/4">1/4</html:option>
                                		<html:option value="1/3">1/3</html:option>
                                		<html:option value="1/2">1/2</html:option>
                                		<html:option value="3/4">3/4</html:option>

				   					</html:select></td>
                            </tr>
                            <tr id="local2">
                                <td class="tablelabel" valign="top">Street Name</td>
                                <td class="tabletext" valign="top"><html:text property="apnStreetName" size="25" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">City</td>
                                <td class="tabletext" valign="top"><html:text property="apnCity" size="25" value="Burbank" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                            <tr id="foreign1">
                                <td class="tablelabel" valign="top">Street No</td>
                                <td class="tabletext" valign="top"><html:text property="apnForeignAddress1" size="25" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Street Name</td>
                                <td class="tabletext" valign="top"><html:text property="apnForeignAddress2" size="25" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr id="foreign2">
                                <td class="tablelabel" valign="top">Unit</td>
                                <td class="tabletext" valign="top"><html:text property="apnForeignAddress3" size="25" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">City</td>
                                <td class="tabletext" valign="top"><html:text property="apnForeignAddress4" size="25" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">State</td>
                                <td class="tabletext" valign="top"><html:text property="apnState" value="CA" size="10" maxlength="2" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Zip Code</td>
                                <td class="tabletext" valign="top"><html:text property="apnZip" size="15" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                            </tr>

                            <tr>
                                <td class="tablelabel" valign="top">Country</td>
                                <td class="tabletext" valign="top"><html:text property="apnCountry" size="25" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Email</td>
                                <td class="tabletext" valign="top"><html:text property="apnEmail" size="25" maxlength="50" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Phone</td>
                                <td class="tabletext" valign="top"><html:text property="apnPhone" size="25" maxlength="10" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Fax</td>
                                <td class="tabletext" valign="top"><html:text property="apnFax" size="25" maxlength="10" styleClass="textbox"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">&nbsp;</td>
    </tr>
</table>
</html:form>
</body>
</html:html>

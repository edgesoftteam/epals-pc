<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app"%>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean"%>
<app:checkLogon />


<html:html>
<HEAD>
<html:base />
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
<SCRIPT src="../script/sorttable.js" type="text/javascript"></SCRIPT>
<STYLE type="text/css">
/* Sortable tables */
table.sortable a.sortheader {
    text-decoration: none;
    display: block;
    text-indent: 5px;
    font: bold 9pt/10pt verdana,arial,helvetica;
    color:#000000

}
table.sortable span.sortarrow {
    color: black;
    text-decoration: none;
}
</STYLE>


</HEAD>
		<%
        String contextRoot = request.getContextPath();
        java.util.List lsoTreeList = (java.util.List) request.getAttribute("lsoTreeList");

        %>
<BODY class="tabletext" text="#000000" leftmargin="0" topmargin="0"	marginwidth="0" marginheight="0">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Search Results
		<%
			if (lsoTreeList.size() <= 0) {
        %>
		- No search results found. Please adjust the search criteria.
		<%
			}
        %>
                   <br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="100%" class="tabletitle"><%=lsoTreeList.size()%> records found</td>
                              <td width="1%" class="tablelabel">
                    		  <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
                     		 </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                          <table class="sortable" id="t1" width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr align=left>
                              <th class="tablelabel">
                                Address
                              </th>
                              <th class="tablelabel">
                                APN
                              </th>
                              <th class="tablelabel">
                                Owner
                              </th>
                            </tr>
						<logic:iterate id="fromTree" name="lsoTreeList" type="elms.app.lso.LsoTree">
						<logic:equal name="fromTree" property="type" value="O">
                            <tr>
                             <td class="tabletext">
                                <A href='<%=contextRoot%>/assessorData.do?action=view&amp;lsoId=<bean:write name="fromTree" property="id"/>'><bean:write	name="fromTree" property="nodeText" /></A>
                             </td>
                              <td class="tabletext">
                                <nobr><bean:write name="fromTree" property="apn"/></nobr>
                              </td>
                              <td class="tabletext">
                                <nobr><bean:write name="fromTree" property="owner"/>
                              </td>
                            </tr>
						</logic:equal>
                         </logic:iterate>
						  </table>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                  <td width="1%">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td height="32">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
        </table>
  </table>

</BODY>
</html:html>


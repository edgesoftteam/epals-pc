<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%

    java.util.List streetList = new AddressAgent().getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
  	AddressAgent useAgent = new AddressAgent();
  	pageContext.setAttribute("useAgent", useAgent);
  	java.util.List parkingZones = (java.util.List) LookupAgent.getParkingZones();
  	request.setAttribute("parkingZones",parkingZones);

%>
<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script>
function dspAddress(strValue) {
		if (strValue == 'N')
		{
			{
				local1.style.display="";
				local2.style.display="";
				foreign1.style.display="none";
				foreign2.style.display="none";
			}

		}
		else
		{
				local1.style.display="none";
				local2.style.display="none";
				foreign1.style.display="";
				foreign2.style.display="";
		}
}
function validateFunction()
{
    strValue=validateData('req',document.forms[0].elements['streetNumber'],'Street Number is a required field');
    if (strValue == true)
    {
 		strValue=validateData('req',document.forms[0].elements['streetName'],'Street Name is a required field');
        if (strValue == true)
        {
		  		strValue=validateData('req',document.forms[0].elements['zip'],'Zip Code is a required field');
		        if (strValue == true)
		        {
		 		     document.forms[0].submit();
		 		}
  		}
    }
}

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="dspAddress('N');">
<html:form action="/saveOccupancy">
<%
	String parentId = request.getParameter("parentId");
%>
<html:hidden property="parentId" value="<%=parentId%>" />
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Add Occupancy</font><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Occupancy Details</td>
                                <td width="1%" class="tablebutton"><nobr>
                                <html:button property="cancel" value="Cancel" styleClass="button" onclick="history.back()" />
                                <html:button property="save"  value="Save" styleClass="button" onclick="validateFunction()" /> &nbsp;</nobr></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Alias</td>
                                <td class="tabletext" valign="top"><html:text property="alias" size="25" maxlength="50" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Description</td>
                                <td class="tabletext" valign="top"><html:text property="description" maxlength="50" size="25" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Unit No. </td>
                                <td class="tabletext" valign="top"><html:text property="unitNo" size="25" maxlength="50" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Use</td>
                               <td class="tabletext" valign="top"><logic:iterate id="occupancyUse" property="occupancyUse" name="useAgent"> <html:multibox property="selectedUse"> <bean:write name="occupancyUse"/> </html:multibox> <bean:write name="occupancyUse"/><br></logic:iterate></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Beginning Floor</td>
                                <td class="tabletext" valign="top"><html:text property="beginningFloor" size="25" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Ending Floor</td>
                                <td class="tabletext" valign="top"><html:text property="endingFloor" size="25" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">P - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="parkingZone" styleClass="textbox">
									  <html:option value="-1">Please select</html:option>
                                      <html:options collection="parkingZones" property="pzoneId" labelProperty="name"/>
                                   </html:select>
                          		</td>
                                <td class="tablelabel" valign="top">R - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="resZone" styleClass="textbox">
									  <html:option value="68">None</html:option>
									  <html:option value="66">R-1 (SFR)</html:option>
									  <html:option value="67">R-4 (MFR)</html:option>
									  <html:option value="69">C-1 (COM)</html:option>
                          		   </html:select>
                          		</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Active</td>
                                <td class="tabletext" valign="top"><html:checkbox property="active" /></td>
                                <td class="tablelabel" valign="top">Label</td>
                                <td class="tabletext" valign="top"><html:text property="label" maxlength="15" size="25" styleClass="textbox"/></td>
                            </tr>
                             <tr>
                                <td class="tablelabel" valign="top">Household Size</td>
                                <td class="tabletext" valign="top"><html:text property="householdSize" size="25" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Bedroom Size</td>
                                <td class="tabletext" valign="top"><html:text property="bedroomSize" size="25" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Gross Rent</td>
                                <td class="tabletext" valign="top"><html:text property="grossRent" size="25" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Utility Allowance</td>
                                <td class="tabletext" valign="top"><html:text property="utilityAllowance" size="25" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                             <tr>
                                <td class="tablelabel" valign="top">Property Manager</td>
                                <td class="tabletext" valign="top"><html:text property="propertyManager" size="25" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Restricted Units</td>
                                <td class="tabletext" valign="top"><html:text property="restrictedUnits" size="25" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Occupant Address </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Street No</td>
                                <td class="tabletext" valign="top"><html:text property="streetNumber" size="25" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                                <td class="tablelabel" valign="top">Street Mod</td>
                                <td class="tabletext" valign="top"><html:select property="streetMod" styleClass="textbox" onkeydown="checkFocus();">
										                            <html:option value=""></html:option><html:option value="1/4">1/4</html:option><html:option value="1/3">1/3</html:option><html:option value="1/2">1/2</html:option><html:option value="3/4">3/4</html:option>
                          										   </html:select></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Street Name</td>
                                <td class="tabletext" valign="top">

                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td id=tdid><html:select property="streetName" styleClass="textbox"><html:option value="">Please Select</html:option><html:options collection="streetList" property="streetId" labelProperty="streetName"/> </html:select></td>
                                    </tr>
                                </table>
                                </td>
                                <td class="tablelabel" valign="top">Unit</td>
                                <td class="tabletext" valign="top"><html:text property="unit" size="10" maxlength="10" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">City</td>
                                <td class="tabletext" valign="top"><html:text property="city" size="25" maxlength="20" value="" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">State</td>
                                <td class="tabletext" valign="top"><html:text property="state" size="25" maxlength="2" value="" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Zip Code</td>
                                <td class="tabletext" valign="top"><html:text property="zip" size="6" styleClass="textbox" maxlength="5" value="" onkeypress="return validateInteger();"/>&nbsp;<html:text property="zip4" size="4" maxlength="4" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                                <td class="tablelabel" valign="top">Description</td>
                                <td class="tabletext" valign="top"><html:text property="addrDescription" size="30" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Primary</td>
                                <td class="tabletext" valign="top"><html:checkbox property="primary" /></td>
                                <td class="tablelabel" valign="top">Active</td>
                                <td class="tabletext" valign="top" colspan="3"><html:checkbox property="activeCheck" /></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Occupant APN</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr>
                                <td class="tablelabel" valign="top">Foreign Address?</td>
                                <td class="tabletext" valign="top" colspan="3"> <html:radio property="foreignAddress" value="N" onclick="dspAddress(this.value);"/> No&nbsp;&nbsp;&nbsp; <html:radio property="foreignAddress" value="Y" onclick="dspAddress(this.value);"/> Yes (Enter Below)</td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">APN #</td>
                                <td class="tabletext" valign="top"><html:text property="apn" size="25" maxlength="10" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Owner Name</td>
                                <td class="tabletext" valign="top"><html:text property="ownerName" size="25" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr id="local1">
                                <td class="tablelabel" valign="top">Street No</td>
                                <td class="tabletext" valign="top"><html:text property="apnStreetNumber" size="25" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                                <td class="tablelabel" valign="top">Street Mod</td>
                                <td class="tabletext" valign="top"><html:select property="apnStreetMod" styleClass="textbox">
										                            <html:option value=""></html:option><html:option value="1/4">1/4</html:option><html:option value="1/3">1/3</html:option><html:option value="1/2">1/2</html:option><html:option value="3/4">3/4</html:option>
                          										   </html:select></td>
                            </tr>
                            <tr id="local2">
                                <td class="tablelabel" valign="top">Street Name</td>
                                <td class="tabletext" valign="top"><html:text property="apnStreetName" size="25" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">City</td>
                                <td class="tabletext" valign="top"><html:text property="apnCity" size="25" value="Burbank" maxlength="20" styleClass="textbox"/></td>
                            </tr>
                            <tr id="foreign1">
                                <td class="tablelabel" valign="top">Line 1</td>
                                <td class="tabletext" valign="top"><html:text property="line1" size="25" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Line 2</td>
                                <td class="tabletext" valign="top"><html:text property="line2" size="25" maxlength="30" styleClass="textbox"/></td>
                            </tr>
                            <tr id="foreign2">
                                <td class="tablelabel" valign="top">Line 3</td>
                                <td class="tabletext" valign="top"><html:text property="line3" size="25" maxlength="30" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Line 4</td>
                                <td class="tabletext" valign="top"><html:text property="line4" size="25" maxlength="30" styleClass="textbox"/></td>
                            </tr>

                            <tr>
                                <td class="tablelabel" valign="top">State</td>
                                <td class="tabletext" valign="top"><html:text property="apnState" size="25" value="CA"  maxlength="2" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Zip Code</td>
                                <td class="tabletext" valign="top"><html:text property="apnZip" size="25" maxlength="9" styleClass="textbox" onkeypress="return validateInteger();"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Country</td>
                                <td class="tabletext" valign="top"><html:text property="country" size="25" maxlength="20" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Email</td>
                                <td class="tabletext" valign="top"><html:text property="email" size="25" maxlength="50" styleClass="textbox"/></td>
                            </tr>
                            <tr>
                                <td class="tablelabel" valign="top">Phone</td>
                                <td class="tabletext" valign="top"><html:text property="phone" size="25" maxlength="10" styleClass="textbox"/></td>
                                <td class="tablelabel" valign="top">Fax</td>
                                <td class="tabletext" valign="top"><html:text property="fax" size="25" maxlength="10" styleClass="textbox"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        </td>
        <td width="1%">&nbsp;</td>
    </tr>
</table>
</html:form>
</body>
</html:html>
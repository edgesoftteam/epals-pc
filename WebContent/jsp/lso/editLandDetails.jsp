<%@ page import='elms.agent.*,java.util.*' %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>


<%
 String contextRoot = request.getContextPath();
 List  streets = (List) AddressAgent.getStreetArrayList();
 request.setAttribute("streets",streets);
%>
<%
  String lsoId = (java.lang.String)session.getAttribute("lsoId");
  AddressAgent useAgent = new AddressAgent();
  pageContext.setAttribute("useAgent", useAgent);
  elms.agent.AddressAgent zoneAgent = new elms.agent.AddressAgent();
  pageContext.setAttribute("zoneAgent", zoneAgent);
  java.util.List parkingZones = (java.util.List) LookupAgent.getParkingZones();
  request.setAttribute("parkingZones",parkingZones);

%>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript">
function addAddressRangeRow() {
	document.forms[0].action='<html:rewrite forward="addAddressRange" />';
	return true;
}

function useValidate() {

var l=document.forms[0].selectedUse.length;
//alert(document.forms[0].selectedUse.length);
var i=0;
var z=0;
for(i=0;i<l;i++) {
if(document.forms[0].selectedUse[i].checked==true){z=z+1};
}
if(z==0){alert("Please select atleast one Use")
return document.landForm.action="";
}

}
</script>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="landForm" type="elms.control.beans.LandForm" action="/saveLandDetails">
<html:hidden property="lsoId" value="<%=lsoId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Modify Land Details</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Land Details</td>
                      <td width="1%" class="tablebutton"><nobr>
                          <html:reset  value="Back" styleClass="button" onclick="history.back();"/>
                          <security:editable levelId="<%=lsoId%>" levelType="L" editProperty="checkUser">
                          <html:submit  value="Update" styleClass="button" onclick="return useValidate();"/>
                          </security:editable>
  	                 &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Alias</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="alias" size="30" maxlength="30" styleClass="tabletext"/>
                      </td>
                      <td class="tablelabel" valign="top">Description</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="description" size="25" maxlength="20" styleClass="tabletext"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Active</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:checkbox  property="active" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Zone</td>
                      <td class="tabletext" valign="top">
                      <div style="width: 250px; height: 250px; overflow-y: scroll">
						<logic:iterate id="zone" property="zone" name="zoneAgent">
							<html:multibox property="selectedZone">
								<bean:write name="zone"/>
							</html:multibox>
								<bean:write name="zone"/><br>
						</logic:iterate>
						</div>
                        </td>
                      <td class="tablelabel" valign="top">Use</td>
                      <td class="tabletext" valign="top">
						<logic:iterate id="landUse" property="landUse" name="useAgent">
							<html:multibox property="selectedUse" >
								  <bean:write name="landUse"/>
							</html:multibox>
								<bean:write name="landUse"/>
						     	<br>
						</logic:iterate>
                        </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Coordinate X: </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="coordinateX" size="25" maxlength="25" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Coordinate Y: </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="coordinateY" size="25" maxlength="25" styleClass="textbox"/>
                      </td>
                    </tr>
                            <tr>
                                <td class="tablelabel" valign="top">P - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="parkingZone" styleClass="textbox">
									  <html:option value="-1">Please select</html:option>
                                      <html:options collection="parkingZones" property="pzoneId" labelProperty="name"/>
                                   </html:select>
                          		</td>

                                <td class="tablelabel" valign="top">R - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="resZone" styleClass="textbox">
									  <html:option value="68">None</html:option>
									  <html:option value="66">R-1 (SFR)</html:option>
									  <html:option value="67">R-4 (MFR)</html:option>
									  <html:option value="69">C-1 (COM)</html:option>
                          		   </html:select>
                          		</td>
                            </tr>
                             <tr>
                                <td class="tablelabel" valign="top">Label </td>
                                <td class="tabletext" valign="top" colspan="3">
                                     <html:text  property="label" size="25" maxlength="15" styleClass="textbox"/>
                          		</td>
                            </tr>



                   </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
	<tr>

		<td >
             	<table>
		      <tr>
			<td  colspan="4">
			  <table width="100%" border="0" cellspacing="1" cellpadding="2">
			    <tr>
			      <td width="99%" class="tabletitle">Address Range</td>
			      <td width="1%" class="tablebutton"><nobr>
				  <html:submit  value="Add" styleClass="button" onclick="addAddressRangeRow();"/>
				 &nbsp;</nobr></td>
			    </tr>
			  </table>
			</td>
		      </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Delete</td>
                      <td class="tablelabel" valign="top">From</td>
                      <td class="tablelabel" valign="top">To</td>
                      <td class="tablelabel" valign="top">Street</td>
                    </tr>
						<nested:iterate id="range" name="landForm" property="addressRangeList">
						   <tr>
							<td><nested:checkbox  name="range" property="delete" value="ON"/></td>
							<td><nested:text name="range" property="startRange" size="6" styleClass="textboxm" onkeypress="validateInteger();" /></td>
							<td><nested:text name="range" property="endRange" size="6" styleClass="textboxm" onkeypress="validateInteger();" /></td>
							<td><nested:select name="range" property="streetId" styleClass="textbox" size="1">
								  <html:option value="-1">Please Select</html:option>
								  <html:options collection="streets" property="streetId" labelProperty="streetName"/>
                                </nested:select></td>
                           </tr>
						</nested:iterate>

                  </table>
	</td></tr>
      </table>
    </td>
    <td width="1%">&nbsp;
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

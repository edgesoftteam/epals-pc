<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form name="addressSummaryForm" type="elms.control.beans.AddressSummaryForm" action="/landAddressMaintenance.do">
<html:hidden  name="addressSummaryForm" property="lsoId"/>
  <br>
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b">Address Maintenance</font><br></td>
          </tr>

          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="99%" class="tabletitle">

                           <html:link page="project.html" styleClass="hdrs" target="_parent">
                            <bean:write name="addressSummaryForm" property="addressSummary.description" scope="session"/>
                           </html:link>

                        </td>
                      <td width="1%"><html:img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"/></td>
                      <td width="1%" class="tablelabel"><nobr>&nbsp;<html:submit  value="Add Structure" property="addStructure" styleClass="button"/>&nbsp;<html:submit  value=" Save  " property="save" styleClass="button"/>&nbsp;</nobr></td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="864" border="0" cellspacing="1" cellpadding="2">
                      <tr>
                         <td class="tablelabel" valign="middle" width="110">Address
                          Range</td>
                         <td class="tabletext" width="314">
                           <logic:iterate id="addressRange" name="addressSummaryForm" property="addressSummary.addressRangeList" scope="session">
                              <bean:write name="addressRange"/><br>
                           </logic:iterate>
                         </td>
                         <td class="tablelabel" width="150">Holds</td>
                         <td class="tabletext" width="256">
                           (<bean:write name="addressSummaryForm" property="addressSummary.warnHoldCount"/>)<html:img src="../images/site_icon_hold_warning.gif" width="21" height="20"/>
                           (<bean:write name="addressSummaryForm" property="addressSummary.softHoldCount"/>)<html:img src="../images/site_icon_hold_soft.gif" width="21" height="20"/>
                           (<bean:write name="addressSummaryForm" property="addressSummary.hardHoldCount"/>)<html:img src="../images/site_icon_hold_hard.gif" width="21" height="20"/>
                         </td>
                      </tr>
 					</table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
	          <% int columnCount = 0;%>
	          <nested:iterate id="structure" name="addressSummaryForm" property="addressSummary.structureList">
	          <nested:equal property="isLot" value="N">
	          <nested:hidden  name="structure" property="structureId"/>
	          <% columnCount++;
	          if (columnCount % 2 == 1){ %> <tr> <%}%>
                  <td valign="top" width="100%" background="../images/site_bg_B7C1CB.jpg">
                    <table width="481" border="0" cellspacing="1" cellpadding="2">
			          <tr valign="top">
                        <td class="tabletext" colspan="2" width="491">
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="top" width="491">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                  <tr>
                                    <td width="99%" background="../images/site_bg_B7C1CB.jpg"><nobr>
                                      &nbsp;Structure <nested:text  name="structure" property="streetNumber" size="5" styleClass="textbox"/>
                                      	<nested:select name="structure" property="streetModifier" styleClass="textbox" size="1">
					                    	<html:option value=""></html:option>
					                    	<html:options collection="modifiers" property="streetModifier" labelProperty="streetModifier"/>
                                      	</nested:select>
                                       	<nested:select name="structure" property="streetId" styleClass="textbox" size="1">
					                    	<html:option value="-1">Please Select</html:option>
					                    	<html:options collection="streets" property="streetId" labelProperty="streetName"/>
                                      	</nested:select></nobr></td>
      								<td width="1%"><html:img src="../images/site_hdr_split_1_tall.jpg" width="26" height="25"/></td>
                      				<td width="1%" class="tablelabel"><nobr>&nbsp;<nested:submit  value="Add Occupancy" property="addOccupancy" styleClass="button" />&nbsp;&nbsp;</nobr></td>
                      			  </tr>
                                </table>
                              </td>
                            </tr>
                            <tr>
			                  <td background="../images/site_bg_B7C1CB.jpg" width="431">
			                    <table width="422" border="0" cellspacing="1" cellpadding="2">
			                      <tr>
			                         <td class="tablelabel" valign="middle" width="50">Delete</td>
			                         <td class="tablelabel" valign="middle" width="50">Active</td>
			                         <td class="tablelabel" width="330">Address</td>
			                         <td class="tablelabel" width="123">Unit #</td>
			                         <td class="tablelabel" width="90">APN #</td>
			                      </tr>
		                          <nested:iterate id="occupancy" name="structures" property="occupancyList">
		                          <nested:equal property="isLot" value="N">
		                          <tr>
                      	             <td class="tabletext" width="50"><nested:checkbox  name="occupancy" property="delete" value="ON"/></td>
                      	             <td class="tabletext" width="50"><nested:checkbox  name="occupancy" property="active" value="ON"/></td>
                      	             <td class="tabletext" width="453" colspan=2><nobr>
                      	    			<nested:text name="occupancy" property="streetNumber" size="5" styleClass="textbox"/>
                                      	<nested:select name="occupancy" property="streetModifier" styleClass="textbox" size="1">
					                    	<html:option value=""></html:option>
					                    	<html:options collection="modifiers" property="streetModifier" labelProperty="streetModifier"/>
                                      	</nested:select>
                                       	<nested:select name="occupancy" property="streetId" styleClass="textbox" size="1">
					                    	<html:option value="-1">Please Select</html:option>
					                    	<html:options collection="streets" property="streetId" labelProperty="streetName"/>
					                    </nested:select>
					                    <nested:text  name="occupancy" property="unitNumber" size="5"  styleClass="textbox"/></nobr></td>
                      	             <td class="tabletext" width="90"><nested:text  name="occupancy" property="apnNumber" size="12"  styleClass="textbox"/></td>
                                  </tr>
		                          </nested:equal>
		                          </nested:iterate>
								</table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
           <% if (columnCount % 2 == 0){ %> </tr> <%}%>
           </nested:equal>
           </nested:iterate>
           <% if (columnCount % 2  == 1){%><td width="50%">&nbsp;</td></tr><%}%>
             </table>
           </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
        </table>
      </td>
      <td width="1%"> </td>
    </tr>
  </table>
</html:form>
</body>
</html:html>

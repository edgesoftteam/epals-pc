<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@page import="elms.util.StringUtils"%>
<%@ page import="elms.common.*,java.util.*,elms.security.*" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<%
   boolean conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false;
   User user = (User)session.getAttribute("user");
   List groups = (java.util.List) user.getGroups();
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true;
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
   }
%>


<%@ page import="javax.sql.*,sun.jdbc.rowset.*" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<%
String contextRoot = request.getContextPath();

String editable = request.getParameter("ed");
if(editable == null) {
    editable = (String) request.getAttribute("ed");
    if(editable == null) editable = "";
}

CachedRowSet crset = (CachedRowSet) request.getAttribute("lsoAddressList");
if(crset !=null) crset.beforeFirst();
String lsoId = (String)request.getAttribute("lsoId");
String lsoType = (String)request.getAttribute("lsoType");
%>
<html:html>
<HEAD>
<html:base/>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" href="../css/elms.css" type="text/css">
</HEAD>

<BODY >

<TABLE width="100%" border="0" cellspacing="10" cellpadding="0">
  <TBODY><TR valign="top">
    <TD width="99%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD><FONT class="con_hdr_3b"><%=lsoType%> Address List</FONT><BR>
            <BR>
            </TD>
        </TR>
        <TR>
          <TD>
            <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
              <TBODY><TR>
                <TD>
                  <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
                    <TBODY><TR>
                      <TD width="99%" class="tabletitle">Addresses</TD>
                      <TD width="1%" class="tablebutton"><NOBR>
                                <% if (lsoType.equalsIgnoreCase("Land")) {  %>
                                <a href="<%=contextRoot%>/viewLand.do?lsoId=<%=lsoId%>" class="tablebutton">Back<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a>&nbsp;
								<% }
								   if(lsoType.equalsIgnoreCase("Structure")) { %>
                                <a href="<%=contextRoot%>/viewStructure.do?lsoId=<%=lsoId%>" class="tablebutton">Back<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a>&nbsp;
                                <% }
								   if(lsoType.equalsIgnoreCase("Occupancy")) { %>
                                <a href="<%=contextRoot%>/viewOccupancy.do?lsoId=<%=lsoId%>" class="tablebutton">Back<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a>&nbsp;
                                <% }  %>
                                <%if(editable.equals("true")){%><%if(addressTech){%>
                                   <security:editable levelId="<%=lsoId%>" levelType="<%=lsoType%>" editProperty="checkUser">
                                   <A href="<%=contextRoot%>/addLsoAddress.do?lsoId=<%=lsoId%>">Add<IMG src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></A>
                                   </security:editable>
                                   </NOBR></TD>
                                <%}%>  <%}%>
                    </TR>
                  </TBODY></TABLE>
                </TD>
              </TR>
              <TR>
                <TD background="../images/site_bg_B7C1CB.jpg">
                  <TABLE width="100%" border="0" cellspacing="1" cellpadding="2">
                    <TBODY><TR>
                      <TD class="tablelabel">Street No</TD>
                      <TD class="tablelabel">Street Name</TD>
                      <TD class="tablelabel">City</TD>
                      <TD class="tablelabel">State</TD>
                      <TD class="tablelabel">Zip</TD>
                      <TD class="tablelabel">Primary</TD>
                    </TR>
 <%
   					while (crset.next()) {
   						String addressId = crset.getString("ADDR_ID");
   						String streetId = crset.getString("STREET_ID");
   						String streetFraction = crset.getString("STR_MOD");
   						if (streetFraction == null) streetFraction = "";
 %>
            	    <TR valign="top">
	 					<TD class="tabletext">
             			<%if(editable.equals("true")){%><%if(addressTech){%>
             			  <A href="<%=contextRoot%>/editLsoAddress.do?addressId=<%=addressId%>&lsoId=<%=lsoId%>&streetId=<%=streetId%>">
             			<%}%><%}%>
             			<%=crset.getString("STR_NO")%> <%=streetFraction %>
             			</A>

             			</TD>

 	 					<TD class="tabletext">
             			<%= StringUtils.nullReplaceWithEmpty(crset.getString("STREET_NAME"))%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=StringUtils.nullReplaceWithEmpty(crset.getString("CITY"))%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=StringUtils.nullReplaceWithEmpty(crset.getString("STATE"))%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=StringUtils.nullReplaceWithEmpty(crset.getString("ZIP"))%>

             			</TD>

 	 					<TD class="tabletext">
             			<%=StringUtils.nullReplaceWithEmpty(crset.getString("PRIMARY"))%>

             			</TD>
					</TR>
 <%
 					}
 					if(crset!=null) crset.close();
 %>
                  </TBODY></TABLE>
                </TD>
              </TR>
            </TBODY></TABLE>
          </TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
    <TD width="1%">
      <TABLE width="100%" border="0" cellspacing="0" cellpadding="0">
        <TBODY><TR>
          <TD height="32">&nbsp;</TD>
        </TR>
        <TR>
          <TD>&nbsp;</TD>
        </TR>
      </TBODY></TABLE>
    </TD>
  </TR>
</TBODY></TABLE>

</BODY>
</html:html>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String lsoId = (java.lang.String)request.getAttribute("lsoId");
String ownerId = (java.lang.String)request.getAttribute("ownerId");
String fflag = request.getParameter("fflag");
if ((fflag ==null) || (fflag.equals(""))) fflag="N";
%>

<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script>
function dspAddress(strValue) {

		if (strValue == 'N')
		{
			{
				local1.style.display="";
				local2.style.display="";
				foreign1.style.display="none";
				foreign2.style.display="none";
			}

		}
		else
		{
				local1.style.display="none";
				local2.style.display="none";
				foreign1.style.display="";
				foreign2.style.display="";
		}
}

function regular(string) {
    if (string.search(/^[0-9][0-9][0-9]\-[0-9][0-9][0-9]\-[0-9][0-9][0-9][0-9]$/) != -1)
         return true;
     else
         return false;
}
function DisplayHyphen(str)
{

	if (( event.keyCode<48 || event.keyCode>57 ) && (event.keyCode != 46))
	{
		event.returnValue = false;
	}
	else
	{
		if ((document.landForm.elements[str].value.length == 3 ) || (document.landForm.elements[str].value.length == 7 ))
		{
			document.landForm.elements[str].value = document.landForm.elements[str].value +'-';
 		}
 		if (document.landForm.elements[str].value.length > 11 )  event.returnValue = false;
	}
}


</script>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="dspAddress('<%=fflag%>');">
<html:form action="/saveLandApn" >
<html:hidden property="lsoId" value="<%=lsoId%>"/>
<html:hidden property="ownerId" value="<%=ownerId%>"/>
<html:hidden property="oldApn"/>

<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Modify Land APN</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98%" class="tabletitle">Land
                        APN</td>
                      <td width="1%" class="tablebutton"><nobr>
                          <html:reset value="Back" styleClass="button" onclick="history.back();"/>
                          <html:submit value="Update" styleClass="button"/>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Foreign
                        Address?</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:radio   property="apnForeignAddress" value="N" onclick="dspAddress(this.value);" />
                        No&nbsp;&nbsp;&nbsp;
                        <html:radio   property="apnForeignAddress" value="Y" onclick="dspAddress(this.value);"/>
                        Yes (Enter Below)</td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">APN
                        # </td>
                      <td class="tabletext" valign="top">
<html:text  property="apn" size="25" onkeypress="return validateNumeric();" maxlength="10" styleClass="textbox" />
                      </td>
                      <td class="tablelabel" valign="top">Owner
                        Name</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnOwnerName" size="25" maxlength="30" styleClass="textbox" />
                      </td>
                    </tr>
                    <tr id="local1">
                      <td class="tablelabel" valign="top">Street
                        No</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnStreetNumber" size="25" styleClass="textbox" onkeypress="return validateInteger();"/>
                      </td>
                      <td class="tablelabel" valign="top">Street
                        Mod</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnStreetModifier" size="25" maxlength="3" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr id="local2">
                      <td class="tablelabel" valign="top">Street
                        Name</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnStreetName" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">City</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnCity" value="Burbank" size="25" maxlength="20" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr id="foreign1">
                      <td class="tablelabel" valign="top">Street No
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnForeignAddress1" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Street Name
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnForeignAddress2" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr id="foreign2">
                      <td class="tablelabel" valign="top">Unit
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnForeignAddress3" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">City
                        </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnForeignAddress4" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">State</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnState" value="CA" size="25" maxlength="2" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Zip
                        Code</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnZip" size="25" maxlength="9" styleClass="textbox" onkeypress="return validateInteger();"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Country</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnCountry" size="25" maxlength="20" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Email</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnEmail" size="25" maxlength="50" styleClass="textbox" />
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Phone</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnPhone" size="25" maxlength="12" styleClass="textbox" onkeypress="return DisplayHyphen('apnPhone');"  />
                      </td>
                      <td class="tablelabel" valign="top">Fax</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="apnFax" size="25" maxlength="12" styleClass="textbox" onkeypress="return DisplayHyphen('apnFax');" />
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">&nbsp;</td>
  </tr>
</table>
</html:form>
</body>
</html:html>


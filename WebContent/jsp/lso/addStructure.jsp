<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<SCRIPT language="JavaScript" src="../script/formValidations.js"></SCRIPT>
<script language="JavaScript">
function validateFunction()
{
    strValue=validateData('req',document.forms[0].elements['addressStreetNumber'],'Street Number is a required field');

    if (strValue == true)
    {
 		strValue=validateData('req',document.forms[0].elements['addressStreetName'],'Street Name is a required field');
        if (strValue == true)
        {
		  		strValue=validateData('req',document.forms[0].elements['addressZip'],'Zip Code is a required field');
		        if (strValue == true)
		        {
		 		     document.forms[0].submit();
		 		}
  		}
    }
}
if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt); //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event;//code for IE
}

if(e.keyCode == 13){
validateFunction();
}

}
</script>
<%
	AddressAgent streetAgent = new AddressAgent();
    java.util.List streetList = streetAgent.getStreetArrayList();
  	pageContext.setAttribute("streetList", streetList);
  	AddressAgent useAgent = new AddressAgent();
  	pageContext.setAttribute("useAgent", useAgent);

  	String parentId = (String)request.getAttribute("parentId");

%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/saveStructure">
<html:hidden property="lsoId" value="<%=parentId%>" />
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Add Structure</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Structure Details</td>
                      <td width="1%" class="tablebutton"><nobr>
                    		<html:button property="back" value="Back" styleClass="button" onclick="history.back()" />
                                <html:button property="save"  value="Save" styleClass="button" onclick="validateFunction()" /> &nbsp;</nobr>
                        </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Alias</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="alias" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Description</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="description" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Total Floors</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="totalFloors" size="25" maxlength="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Use</td>
                      <td class="tabletext" valign="top">

						<logic:iterate id="structureUse" property="structureUse" name="useAgent">
							<html:multibox property="selectedUse">
								<bean:write name="structureUse"/>
							</html:multibox>
								<bean:write name="structureUse"/><br>
						</logic:iterate>
                       </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Active</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:checkbox  property="active" />
                        Yes</td>
                    </tr>
                      <tr>
                      <td class="tablelabel" valign="top">Label</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:text  property="label" size="25" maxlength="15" styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Structure Address </td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Street No</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="addressStreetNumber" size="25" styleClass="textbox" onkeypress="return validateInteger();"/>
                      </td>
                      <td class="tablelabel" valign="top">Street Mod</td>
                      <td class="tabletext" valign="top">
                        <html:select property="addressStreetModifier" styleClass="textbox">
							<html:option value=""></html:option><html:option value="1/4">1/4</html:option><html:option value="1/3">1/3</html:option><html:option value="1/2">1/2</html:option><html:option value="3/4">3/4</html:option>
                        </html:select>
                      </td>
                    </tr>
                    <tr>
                     <td class="tablelabel" valign="top">Street Name</td>
                     <td class="tabletext" valign="top">

                     <table border="0" cellspacing="0" cellpadding="0">
                         <tr>
                             <td><html:select property="addressStreetName" styleClass="textbox"><html:option value="">Please Select</html:option><html:options collection="streetList" property="streetId" labelProperty="streetName"/> </html:select></td>
                         </tr>
                     </table>
                     </td>
                     <td class="tablelabel" valign="top">&nbsp;</td>
                     <td class="tabletext" valign="top">&nbsp;</td>

                     </tr>
                     <tr>
                      <td class="tablelabel" valign="top">City</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="addressCity" size="25" value="" maxlength="20" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">State</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="addressState" size="25" value="" maxlength="2" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Zip Code</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="addressZip" size="6" styleClass="textbox" onkeypress="return validateInteger();"/>
                        &nbsp;<html:text property="addressZip4" size="4" styleClass="textbox" maxlength="4" onkeypress="return validateInteger();"/>
                      </td>
                      <td class="tablelabel" valign="top">Description</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="addressDescription" size="30" maxlength="30" styleClass="textbox"/>
                      </td>
                    </tr>
					<tr>
                      <td class="tablelabel" valign="top">Primary</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="addressPrimary" />
                      </td>
                      <td class="tablelabel" valign="top">Active</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:checkbox  property="addressActive" />
                      </td>
					</tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">&nbsp;
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>
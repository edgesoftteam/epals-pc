<%@ page import="elms.common.*,elms.app.common.*,java.util.*,elms.security.*,elms.agent.*" %>
<%
   boolean general=false,conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false, businessLicenseUser=false, businessLicenseApproval=false, businessTaxUser = false, businessTaxApproval = false, codeInspector = false, regulatoryPermit = false;
   User user = (User)session.getAttribute("user");
   List groups = (java.util.List) user.getGroups();
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true;
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
        if(group.groupId == Constants.GROUPS_GENERAL) general = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
		if(group.groupId == Constants.GROUPS_CODE_INSPECTOR) codeInspector = true;
		if(group.groupId == Constants.GROUPS_REGULATORY_PERMIT) regulatoryPermit = true;
   }
%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>

<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String contextRoot = request.getContextPath();
String editable = (String) request.getAttribute("editable");
if(editable == null)  editable = "";

String lsoId = (String)request.getAttribute("lsoId")!=null?(String)request.getAttribute("lsoId"):"";
String alias = (String)request.getAttribute("alias")!=null?(String)request.getAttribute("alias"):"";
String description= (String)request.getAttribute("description")!=null?(String)request.getAttribute("description"):"";
String totalFloors= (String)request.getAttribute("totalFloors")!=null?(String)request.getAttribute("totalFloors"):"";
String use= (String)request.getAttribute("use")!=null?(String)request.getAttribute("use"):"";
String active = (String)request.getAttribute("active");
active = (active.equals("Y") ? "Yes":"No");
String level = "S";
String label= (String)request.getAttribute("label")!=null?(String)request.getAttribute("label"):"";

java.util.List occupancyApnList = (java.util.List) request.getAttribute("occupancyApnList");
if (occupancyApnList  != null ) pageContext.setAttribute("occupancyApnList",occupancyApnList);

java.util.List holds = (java.util.List) request.getAttribute("holds");
if(holds == null) holds = new java.util.ArrayList();
pageContext.setAttribute("holds",holds);

java.util.List condList = (java.util.List)request.getAttribute("condList");
if(condList == null) condList = new java.util.ArrayList();
pageContext.setAttribute("condList",condList);

// Refreshing the LSO tree .. onLoad of this page.
String onloadAction = (String)session.getAttribute("onloadAction");
if (onloadAction == null ) onloadAction = "";
%>

<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="<%=onloadAction%>">
<html:form action="/viewStructure">
  <table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
      <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><font class="con_hdr_3b"><img src="../images/structure_icon_small.gif" border="0">&nbsp;Structure Manager</font><br>
              <br>
              </td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <%if(editable.equals("true")){%>
                        <% if(addressTech){%>
                        <td width="97%" class="tabletitle"><a href="<%=contextRoot%>/editStructureDetails.do?lsoId=<%=lsoId%>"  class="tabletitle">Structure Details</a></td>
						<%}else{%>
                        <td width="97%" class="tabletitle">Structure Details</td>
                        <%}%>

                        <% if(addressTech){%>
                         <security:editable levelId="<%=lsoId%>" levelType="<%=level%>" editProperty="checkUser">
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addOccupancy.do?parentId=<%=lsoId%>" class="tablebutton" target="f_content">Add Occupancy</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></nobr></td>
                        </security:editable>
					    <%}%>
					    <% if((!(businessLicenseUser) && !(businessTaxUser) && !(general)) || (codeInspector) || (regulatoryPermit)){%>
					     <security:editable levelId="<%=lsoId%>" levelType="<%=level%>" editProperty="checkUser">
					    <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addProject.do?lsoId=<%=lsoId%>" class="tablebutton">Add Project<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
					    </security:editable>
                        <%}%>
                        <%}else{%>
                        <td width="100%" class="tabletitle">Structure Details</td>
                        <%}%>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr>
                        <td class="tablelabel" valign="top">ID</td>
                        <td class="tabletext" valign="top"><%=lsoId%></td>
                        <td class="tablelabel" valign="top">Description</td>
                        <td class="tabletext" valign="top"> <%=description%></td>
                      </tr>
                      <tr>
                        <td class="tablelabel" valign="top">Total Floors</td>
                        <td class="tabletext" valign="top"> <%=totalFloors%></td>
                        <td class="tablelabel" valign="top">Use</td>
                        <td class="tabletext" valign="top"> <%=use%></td>
                      </tr>
                      <tr>
                        <td class="tablelabel" valign="top">Active</td>
                        <td class="tabletext" valign="top" > <%=active%></td>
                        <td class="tablelabel" valign="top">Alias</td>
                        <td class="tabletext" valign="top"> <%=alias%></td>
                      </tr>
                       <tr>
                        	<td class="tablelabel" valign="top">Zone</td>
                      		<td class="tabletext" valign="top" > <%
                                  List zoneList = new ArrayList();
                                  String s=elms.agent.LookupAgent.getStructureZone(lsoId);
                                  if(s==null)s="";
                                  zoneList= elms.util.StringUtils.stringToList(s,", ");
                                  Iterator it=zoneList.iterator();
                                  while(it.hasNext())      {
                                  	out.println((String)it.next()+"<br>"+"<dt>");
                                  }
                                  %>
                                </td>
                        	<td class="tablelabel" valign="top">Label</td>
                         	<td class="tabletext" valign="top" colspan="3"> <%=label%></td>

                       	</tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <%if(editable.equals("true")){%><% if(addressTech){%>
                        <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listLsoAddress.do?lsoId=<%=lsoId%>&ed=<%=editable%>"  class="tabletitle">Structure Address</a></td>
                        <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addLsoAddress.do?origin=SM&lsoId=<%=lsoId%>" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                        <%}else{%>
                          <td width="100%" class="tabletitle"><a href="<%=contextRoot%>/listLsoAddress.do?lsoId=<%=lsoId%>"  class="tabletitle">Structure Address</a></td>
                        <%}%><%}%>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Address</td>
                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="addressStreetNumber" />&nbsp;<bean:write name="viewStructureForm" property="addressStreetModifier" />&nbsp;<bean:write name="viewStructureForm" property="addressStreetName" />&nbsp;<bean:write name="viewStructureForm" property="addressUnit" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">&nbsp;</td>
                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="addressCity" />&nbsp;<bean:write name="viewStructureForm" property="addressState" />&nbsp;<bean:write name="viewStructureForm" property="addressZip" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Description</td>
                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="addressDescription" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Primary</td>
                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="addressPrimary" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Active</td>
                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="addressActive" /></td>
                    </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>


	        <!-- List of all Occuapncy APNs for this structure..customization for burbank -->
	        <logic:present name="occupancyApnList" scope="request">
	        <tr>
	          <td>
	            <table width="100%" border="0" cellspacing="0" cellpadding="0">
	              <tr>
	                <td>
	                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
	                    <tr>
	                          <td width="100%" class="tabletitle" height="19"> Occupant APN </td>
	                    </tr>
	                  </table>
	                </td>
	              </tr>
	              <tr>
	                <td background="../images/site_bg_B7C1CB.jpg">
	                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
	                     <html:hidden property="lsoId" />
	                  <%if (occupancyApnList.isEmpty() ){%>
	                      <tr valign="top">
	                         <td class="tablelabel" width="25%">APN #</td>
	                         <td class="tabletext" width="75%"> </td>
	                  <%}else{%>
	                    <tr valign="top">
	                      <td class="tablelabel" width="25%">APN #</td>
	                      <td class="tabletext" width="75%">
						  <html:select property="apn" size="1" styleClass="textbox" onchange='document.forms[0].submit()'>
						  	<html:options collection="occupancyApnList" property="apn" labelProperty="apn" />
						  </html:select>
						</td>
	                    </tr>

	                    <% }  %>
	                    <tr valign="top">
	                      <td class="tablelabel" width="25%">Owner Name</td>
	                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="apnOwnerName" /></td>
						</tr>

	                    <tr valign="top">
	                      <td class="tablelabel" width="25%">Email</td>
	                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="apnEmail" /></td>
	                    </tr>
	                    <tr valign="top">
	                      <td class="tablelabel" width="25%">Phone</td>
	                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="apnPhone" /></td>
	                    </tr>
	                    <tr valign="top">
	                      <td class="tablelabel" width="25%">Fax</td>
	                      <td class="tabletext" width="75%"><bean:write name="viewStructureForm" property="apnFax" /></td>
	                    </tr>
	                  </table>
	                </td>
	              </tr>
	            </table>
	          </td>
	        </tr>
	        </logic:present>
          <tr>
            <td>&nbsp;</td>
          </tr>
			<jsp:include page="../includes/comments.jsp" flush="true">
				<jsp:param name="id" value="<%=lsoId%>" />
				<jsp:param name="level" value="<%=level%>" />
				<jsp:param name="editable" value="<%=editable%>" />
			</jsp:include>

            <tr>
                <td>&nbsp;</td>
            </tr>

          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=lsoId%>&level=<%=level%>" class="tabletitle">Attachments</a></nobr></td>
                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=lsoId%>&level=<%=level%>" class="tablebutton">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                        </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                      <tr valign="top">
                        <td class="tablelabel">ID</td>
                        <td class="tablelabel">Description</td>
                        <td class="tablelabel">Size</td>
                        <td class="tablelabel">Date</td>
                      </tr>
						<%int i=0;%>
                        <logic:iterate id="attachment" name="attachmentList" type="elms.app.common.Attachment" scope="request">
						<%
							List attachmentsList = (List)request.getAttribute("attachmentList");
							Attachment attachmentObj = new Attachment();
							String fileNameStr = "";

							try{
								attachmentObj = (Attachment)attachmentsList.get(i);
								fileNameStr = attachmentObj.file.getFileName();
								fileNameStr = java.net.URLDecoder.decode(fileNameStr, "UTF-8");
							}catch(Exception e){}
						 %>
                        <tr valign="top" class="tabletext">
                            <td> <%=fileNameStr%> </td>
                            <td> <bean:write name="attachment" property="description"/> </td>
                            <td> <bean:write name="attachment" property="file.fileSize"/> </td>
                            <td> <bean:write name="attachment" property="file.strCreateDate"/> </td>
                        </tr>
						<%i++; %>
                        </logic:iterate>

                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
      <td width="1%">
        <table width="200" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="32">&nbsp;</td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>

						<td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=<%=lsoId%>&level=<%=level%>&ed=<%=editable%>" class="tabletitle">Holds</a></td>
                        <%if(editable.equals("true")){%>
                        <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addHold.do?levelId=<%=lsoId%>&level=<%=level%>" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                        <%}%>
                       </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                     <logic:iterate id="hold" name="holds">
                           <tr valign="top" class="tabletext">
                               <td><bean:write name="hold" property="type"/></td>
                               <td><bean:write name="hold" property="title"/></td>
                          </tr>
                    </logic:iterate>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="70%" class="tabletitle"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=lsoId%>&conditionLevel=<%=level%>&ed=<%=editable%>&ed=<%=editable%>" class="tabletitle">Conditions</a></td>
                        <%if(editable.equals("true")){%>
                        <td width="29%" class="tablebutton"><a href="<%=contextRoot%>/addCondition.do?levelId=<%=lsoId%>&conditionLevel=<%=level%>" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                        <%}%>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td background="../images/site_bg_B7C1CB.jpg">
                    <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  <logic:iterate id="cond" name="condList" type="elms.app.common.Condition">
                  <tr valign="top">
                  <td class="tablelabel"><bean:write name="cond" property="conditionId"/></td>
                  <td class="tabletext"><bean:write name="cond" property="shortText"/></td>
                  </tr>
                  </logic:iterate>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
          </tr>

			<tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Site Data</td>
                                <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/viewSiteStructure.do?levelType=S&levelId=<%=lsoId%>&lsoId=<%=lsoId%>" class="tablebutton">View</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>

        </table>
      </td>
    </tr>
  </table>
</html:form>
</body>
</html:html>

<%@ page import='elms.agent.*' %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
	String lsoId = (java.lang.String)request.getAttribute("lsoId");
	AddressAgent useAgent = new AddressAgent();
  	pageContext.setAttribute("useAgent", useAgent);

%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/saveStructureDetails" >
<html:hidden property="lsoId" value="<%=lsoId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Modify Structure Details</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Structure Details</td>
                      <td width="1%" class="tablebutton"><nobr>
                        <html:reset  value="Cancel" styleClass="button" onclick="history.back();"/>
                          <security:editable levelId="<%=lsoId%>" levelType="S" editProperty="checkUser">
                        <html:submit  value="Update" styleClass="button"/>
                        </security:editable>

                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Alias</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="alias" size="25" maxlength="30" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Description</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="description" size="25" maxlength="30"  styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Total
                        Floors</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="totalFloors" size="25" maxlength="10" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Use</td>
                      <td class="tabletext" valign="top">
						<logic:iterate id="structureUse" property="structureUse" name="useAgent">
							<html:multibox property="selectedUse" >
								<bean:write name="structureUse"/>
							</html:multibox>
								<bean:write name="structureUse"/><br>
						</logic:iterate>
                         </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Active</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:checkbox  property="active" />
                        Yes</td>
                    </tr>
                      <tr>
                      <td class="tablelabel" valign="top">Label</td>
                      <td class="tabletext" valign="top" colspan="3">
                        <html:text  property="label" size="25" maxlength="15"  styleClass="textbox"/>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">&nbsp;</td>
   </tr>
</table>
</html:form>
</body>
</html:html>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
<%!
java.util.List streets = new java.util.ArrayList(); // for the Street list
%>
<%
 String contextRoot = request.getContextPath();
 String lsoId = (String)session.getAttribute("lsoId");
 String lsoType = (String) session.getAttribute("lsoType");

 streets = (java.util.List) elms.agent.AddressAgent.getStreetArrayList();
 request.setAttribute("streets",streets);

 String strUrl = "";
 try {
	if (lsoType.equals("L"))
		strUrl = contextRoot + "/viewLand.do?lsoId=" + lsoId;
	else if (lsoType.equals("S"))
		strUrl = contextRoot + "/viewStructure.do?lsoId=" + lsoId;
	else if (lsoType.equals("O"))
		strUrl = contextRoot + "/viewOccupancy.do?lsoId=" + lsoId;
	} catch (Exception e) {}

%>
</head>
<script language="javascript" type="text/javascript" src="../script/actb.js"></script>
<script language="javascript" type="text/javascript" src="../script/common.js"></script>
<script language="JavaScript">

if(document.addEventListener){ //code for Moz
document.addEventListener("keypress",keyCapt,false);
}else{
document.attachEvent("onkeyup",keyCapt);  //code for IE
}

function keyCapt(e){
if(typeof window.event!="undefined"){
e=window.event; //code for IE
}
if(e.keyCode == 13){
validateFunction();
}

}

function validateFunction() {

var streetText=document.forms[0].streetText.value;
for(var q=0;q<<%=streets.size()%>;q++){
	if((streetText.toUpperCase())==((document.forms[0].streetId[q].text).toUpperCase())){
		document.forms[0].streetId[q].selected=true;
	}
}
document.forms[0].action='<%=contextRoot%>/assessorData.do?action=results';
document.forms[0].submit();
}
var streetArray = new Array();
<%
if(streets != null && streets.size() > 0){

for(int i=0; i<streets.size();i++){
elms.app.lso.Street st = (elms.app.lso.Street)streets.get(i);
%>
streetArray[<%=i%>] = '<%=st.getStreetName()%>';
<%}}%>
</script>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form action="/assessorData?action=results">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
    	<td width="99%">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
            	<tr>
                	<td><font class="con_hdr_3b">Assessor's Data</font><br><br></td>
            	</tr>
            	<tr>
					<td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="97%" class="tabletitle">APN Search</td>
                                <td width="2%" class="tablebutton">
								<html:button property="Search" value="Search" styleClass="button" onclick="return validateFunction();"></html:button></td>
                            </tr>
                        </table>
                    </td>
				</tr>
 				<tr>
					<td>
		                <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                    <tr>
								<td background="../images/site_bg_B7C1CB.jpg">
		                        	<table width="100%" border="0" cellspacing="1" cellpadding="2">
		                            	<tr valign="top">
		                            		<td class="tablelabel">
		                            			APN Number
		                            		</td>
		                                	<td class="tabletext">
		                                		<html:text property="apn" size="20" maxlength="10" styleClass="textbox"/>
		                               		</td>
		                            	</tr>
										<tr>
									  		<td rowspan="2" class="tablelabel" >Address</td>
									  		<td class="tablelabel">Street #, Fraction and Name</td>
										</tr>
										<tr>
									  		<td class="tabletext" width="88%">
												<html:text maxlength="6" size="6" property="streetNumber" styleClass="textbox" />
												<html:select property="streetFraction" styleClass="textbox">
											  		<html:option value=""></html:option>
													<html:option value="1/4">1/4</html:option>
													<html:option value="1/2">1/2</html:option>
													<html:option value="3/4">3/4</html:option>
												</html:select>
												<div style="visibility:hidden">
													<html:select property="streetId" styleClass="textbox" style="position:absolute">
														<html:option value="-1">Please Select</html:option>
														<html:options collection="streets" property="streetId" labelProperty="streetName"/>
													</html:select>
												</div>
												<input type="text" name="streetText" id="st" size="18%" style="padding:-2px;" />
												<script>
													var streetObj = new actb(document.getElementById('st'),streetArray);

													if(document.forms[0].streetId.options[document.forms[0].streetId.selectedIndex].value != "-1"){
													document.forms[0].streetText.value = document.forms[0].streetId.options[document.forms[0].streetId.selectedIndex].text;
													}
												</script>

											</td>
								     	</tr>
		                            	<tr valign="top">
		                            		<td class="tablelabel">
		                            			Owner
		                            		</td>
		                                	<td class="tabletext">
		                                		<html:text property="ownerName" size="30" maxlength="80" styleClass="textbox"/>
		                               		</td>
		                            	</tr>
		                            	<tr valign="top">
		                            		<td class="tablelabel">
		                            			Special Criteria
		                            		</td>
		                                	<td class="tabletext">
		                                		<table>
													<tr>
														<td class="tablelabel">
															Use code:
														</td>
														<td class="tabletext">
															<html:text property="useCode" size="30" maxlength="80" styleClass="textbox"/>
														</td>
														<td class="tablelabel">
															Zone code:
														</td>
														<td class="tabletext">
															<html:text property="zoneCode" size="30" maxlength="80" styleClass="textbox"/>
														</td>
													</tr>
													<tr>
														<td class="tablelabel">
															No of Units:
														</td>
														<td class="tabletext">
															<html:text property="numberOfUnits" size="30" maxlength="80" styleClass="textbox"/>
														</td>
														<td class="tablelabel">
															Hazard(Weed) Abatement Codes:
														</td>
														<td class="tabletext">
															<html:text property="hazardAbatementCode" size="30" maxlength="80" styleClass="textbox"/>
														</td>
													</tr>
													<tr>
														<td class="tablelabel">
															Date of Last Sale GT:
														</td>
														<td class="tabletext">
															<html:text property="dateOfLastSaleGT" size="30" maxlength="80" styleClass="textbox"/>
														</td>
														<td  class="tablelabel">
															Date of Last Sale GT:
														</td>
														<td class="tabletext">
															<html:text property="dateOfLastSaleGT" size="30" maxlength="80" styleClass="textbox"/>
														</td>
													</tr>
													<tr>
														<td class="tablelabel">
															Improvement Value GT:
														</td>
														<td class="tabletext">
															<html:text property="improvementValueGT" size="30" maxlength="80" styleClass="textbox"/>
														</td>
														<td class="tablelabel">
															Improvement Value LT:
														</td>
														<td class="tabletext">
															<html:text property="improvementValueLT" size="30" maxlength="80" styleClass="textbox"/>
														</td>
													</tr>
													<tr>
														<td class="tablelabel">
															Total Assessment GT:
														</td>
														<td class="tabletext">
															<html:text property="totalAssessmentGT" size="30" maxlength="80" styleClass="textbox"/>
														</td>
														<td class="tablelabel">
															Total Assessment LT:
														</td>
														<td class="tabletext">
															<html:text property="totalAssessmentLT" size="30" maxlength="80" styleClass="textbox"/>
														</td>
													</tr>
												</table>
		                               		</td>
		                            	</tr>
									</table>
				              	</td>
		                    </tr>
		                </table>
					</td>
            	</tr>
        	</table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>
</html:form>
</body>
</html:html>
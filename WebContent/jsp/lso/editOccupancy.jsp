<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import='elms.agent.*' %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%
String lsoId = (java.lang.String)request.getAttribute("lsoId");

AddressAgent useAgent = new AddressAgent();
pageContext.setAttribute("useAgent", useAgent);
java.util.List parkingZones = (java.util.List) LookupAgent.getParkingZones();
request.setAttribute("parkingZones",parkingZones);
%>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<html:form  action="/saveOccupancyDetails">
<html:hidden property="lsoId" value="<%=lsoId%>"/>
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b">Modify Occupancy Details</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">Occupancy Details</td>
                      <td width="1%" class="tablebutton"><nobr>
                        <html:button  property="cancel" value="Cancel" styleClass="button" onclick="history.back();"/>
                        <security:editable levelId="<%=lsoId%>" levelType="Q" editProperty="checkUser">
                        <html:submit  property="update" value="Update" styleClass="button" />
                        </security:editable>
                        &nbsp;</nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">Alias</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="alias" size="25" maxlength="50" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Description</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="description" size="25" maxlength="50" styleClass="textbox"/>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Unit No. </td>
                      <td class="tabletext" valign="top">
                        <html:text  property="unitNo" size="25" maxlength="50" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Use</td>
                      <td class="tabletext" valign="top">
						<logic:iterate id="occupancyUse" property="occupancyUse" name="useAgent">
							<html:multibox property="selectedUse">
								<bean:write name="occupancyUse"/>
							</html:multibox>
								<bean:write name="occupancyUse"/><br>
						</logic:iterate>
                        </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Beginning Floor</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="beginningFloor" size="25" maxlength="20" styleClass="textbox"/>
                      </td>
                      <td class="tablelabel" valign="top">Ending Floor</td>
                      <td class="tabletext" valign="top">
                        <html:text  property="endingFloor" size="25" maxlength="20" styleClass="textbox"/>
                      </td>
                    </tr>
                            <tr>
                                <td class="tablelabel" valign="top">P - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="parkingZone" styleClass="textbox">
									  <html:option value="-1">Please select</html:option>
                                      <html:options collection="parkingZones" property="pzoneId" labelProperty="name"/>
                                   </html:select>
                          		</td>
                                <td class="tablelabel" valign="top">R - Zone : </td>
                                <td class="tabletext" valign="top">
                                   <html:select property="resZone" styleClass="textbox">
									  <html:option value="68">None</html:option>
									  <html:option value="66">R-1 (SFR)</html:option>
									  <html:option value="67">R-4 (MFR)</html:option>
									  <html:option value="69">C-1 (COM)</html:option>
                          		   </html:select>
                          		</td>
                            </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Active</td>
                      <td class="tabletext" valign="top">
                        <html:checkbox  property="active"/>
                      <td class="tablelabel" valign="top">Label</td>
                      <td class="tabletext" valign="top">
                     <html:text  property="label" size="25" maxlength="15" styleClass="textbox"/>
                    </tr>
                    <tr>
                        <td class="tablelabel" valign="top">Household Size</td>
                        <td class="tabletext" valign="top"><html:text property="householdSize" size="25" maxlength="20" styleClass="textbox"/></td>
                        <td class="tablelabel" valign="top">Bedroom Size</td>
                        <td class="tabletext" valign="top"><html:text property="bedroomSize" size="25" maxlength="20" styleClass="textbox"/></td>
                    </tr>
                    <tr>
                        <td class="tablelabel" valign="top">Gross Rent</td>
                        <td class="tabletext" valign="top"><html:text property="grossRent" size="25" maxlength="20" styleClass="textbox"/></td>
                        <td class="tablelabel" valign="top">Utility Allowance</td>
                        <td class="tabletext" valign="top"><html:text property="utilityAllowance" size="25" maxlength="20" styleClass="textbox"/></td>
                    </tr>
                     <tr>
                        <td class="tablelabel" valign="top">Property Manager</td>
                        <td class="tabletext" valign="top"><html:text property="propertyManager" size="25" maxlength="20" styleClass="textbox"/></td>
                        <td class="tablelabel" valign="top">Restricted Units</td>
                        <td class="tabletext" valign="top"><html:text property="restrictedUnits" size="25" maxlength="20" styleClass="textbox"/></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
      </table>
    </td>
    <td width="1%">&nbsp;
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

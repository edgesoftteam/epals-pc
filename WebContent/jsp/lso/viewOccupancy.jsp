<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ page import="elms.common.*,elms.app.common.*,java.util.*,elms.security.*" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<%
   boolean general=false,conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false, businessLicenseUser=false, businessLicenseApproval=false, businessTaxUser = false, businessTaxApproval = false, codeInspector = false, regulatoryPermit = false;
   User user = (User)session.getAttribute("user");
   List groups = (java.util.List) user.getGroups();
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true;
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
        if(group.groupId == Constants.GROUPS_GENERAL) general = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
		if(group.groupId == Constants.GROUPS_CODE_INSPECTOR) codeInspector = true;
		if(group.groupId == Constants.GROUPS_REGULATORY_PERMIT) regulatoryPermit = true;
   }
%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ page import="javax.sql.*,sun.jdbc.rowset.*,elms.util.*" %>


<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>
<%

String contextRoot = request.getContextPath();
String editable = (String) request.getAttribute("editable");
if(editable == null)  editable = "";
String lsoId = (String)request.getAttribute("lsoId");
session.setAttribute("lsoId", lsoId);
String alias= (String)request.getAttribute("alias");
String description= (String)request.getAttribute("description");
String unitNumber = (String)request.getAttribute("unitNumber");
String use = (String)request.getAttribute("use");
String beginFloor= (String)request.getAttribute("beginFloor");
String endFloor= (String)request.getAttribute("endFloor");
String active = (String)request.getAttribute("active");
active = (active.equals("Y") ? "Yes":"No");
String pZone = (String) request.getAttribute("pZone");
if(pZone == null) pZone = "";
String rZone = (String) request.getAttribute("rZone");
if(rZone == null) rZone = "";
String label= (String)request.getAttribute("label")!=null?(String)request.getAttribute("label"):"";

String householdSize = (String) request.getAttribute("householdSize");
if(householdSize == null) householdSize = "";
String bedroomSize = (String) request.getAttribute("bedroomSize");
if(bedroomSize == null) bedroomSize = "";
String grossRent = (String) request.getAttribute("grossRent");
if(grossRent == null) grossRent = "";
String utilityAllowance = (String) request.getAttribute("utilityAllowance");
if(utilityAllowance == null) utilityAllowance = "";
String propertyManager = (String) request.getAttribute("propertyManager");
if(propertyManager == null) propertyManager = "";
String restrictedUnits = (String) request.getAttribute("restrictedUnits");
if(restrictedUnits == null) restrictedUnits = "";


String level = "O";

session.setAttribute("levelId",lsoId);
session.setAttribute("level",level);


java.util.List tenants = (java.util.List) request.getAttribute("tenants");
if(tenants == null) tenants = new java.util.ArrayList();
pageContext.setAttribute("tenants",tenants);

java.util.List holds = (java.util.List) request.getAttribute("holds");
if(holds == null) holds = new java.util.ArrayList();
pageContext.setAttribute("holds",holds);

java.util.List occupancyApnList = (java.util.List) request.getAttribute("occupancyApnList");
if (occupancyApnList  != null ) pageContext.setAttribute("occupancyApnList",occupancyApnList);

java.util.List condList = (java.util.List)request.getAttribute("condList");
if(condList == null) condList = new java.util.ArrayList();
pageContext.setAttribute("condList",condList);

// Refreshing the LSO tree .. onLoad of this page.
String onloadAction = (String)session.getAttribute("onloadAction");
if (onloadAction == null ) onloadAction = "";

%>


<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"  onload="<%=onloadAction%>">
<html:form action="/viewOccupancy">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><font class="con_hdr_3b"><img src="../images/occupancy_icon_small.gif" border="0">&nbsp;Occupancy Manager</font><br>
            <br>
            </td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                       <%if(editable.equals("true")){%>
                       <% if(addressTech){%>
                       <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/editOccupancyDetails.do?lsoId=<%=lsoId%>" class="tabletitle">Occupancy Details </a></td>
                       <%}else{%>
                       <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/editOccupancyDetails.do?lsoId=<%=lsoId%>" class="tabletitle">Occupancy Details </a></td>
                       <%}%>
 				       <% if((!(businessLicenseUser) && !(businessTaxUser) && !(general)) || (codeInspector) || (regulatoryPermit)){%>
 				       <security:editable levelId="<%=lsoId%>" levelType="<%=level%>" editProperty="checkUser">
 				       <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addProject.do?lsoId=<%=lsoId%>" class="tablebutton">Add Project<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
 				       </security:editable>
                       <%}%>
                       <%}else{%>
                       <td width="100%" class="tabletitle">Occupancy Details</td>
                       <%}%>
                     </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr>
                      <td class="tablelabel" valign="top">ID</td>
                      <td class="tabletext" valign="top"><%=lsoId%></td>
                      <td class="tablelabel" valign="top">Description</td>
                      <td class="tabletext" valign="top"><%=description%></td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Unit No. </td>
                      <td class="tabletext" valign="top"><%=unitNumber%></td>
                      <td class="tablelabel" valign="top">Use</td>
                      <td class="tabletext" valign="top"><%=use%></td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Beginning Floor</td>
                      <td class="tabletext" valign="top"><%=elms.util.StringUtils.nullReplaceWithEmpty(beginFloor)%></td>
                      <td class="tablelabel" valign="top">End Floor</td>
                      <td class="tabletext" valign="top"><%=elms.util.StringUtils.nullReplaceWithEmpty(endFloor)%></td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Active</td>
                      <td class="tabletext" valign="top" ><%=active%></td>
                      <td class="tablelabel" valign="top">Alias</td>
                      <td class="tabletext" valign="top"><%=alias%>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">P-Zone</td>
                      <td class="tabletext" valign="top" ><%=pZone%></td>
                      <td class="tablelabel" valign="top">R-Zone</td>
                      <td class="tabletext" valign="top"><%=rZone%>
                      </td>
                    </tr>
                    <tr>
                      <td class="tablelabel" valign="top">Zone</td>
		                   <td class="tabletext" valign="top">
		                      <%
		                                  List zoneList = new ArrayList();
		                                  String s=elms.agent.LookupAgent.getOcuupancyZone(lsoId);
		                                  if(s==null)s="";
		                                  zoneList= StringUtils.stringToList(s,", ");
		                                  Iterator it=zoneList.iterator();
		                                  while(it.hasNext())      {
		                                  out.println((String)it.next()+"<br>"+"<dt>");
		                                  }
		                                  %>
		                   </td>
		                                         <td class="tablelabel" valign="top">Label</td>
                      <td class="tabletext" valign="top" colspan="3" ><%=label%></td>

                    </tr>
                     <tr>
                      <td class="tablelabel" valign="top">Household Size</td>
                      <td class="tabletext" valign="top" ><%=householdSize%></td>
                      <td class="tablelabel" valign="top">Bedroom Size</td>
                      <td class="tabletext" valign="top"><%=bedroomSize%>
                      </td>
                    </tr>
                     <tr>
                      <td class="tablelabel" valign="top">Gross Rent</td>
                      <td class="tabletext" valign="top" ><%=grossRent%></td>
                      <td class="tablelabel" valign="top">Utility Allowance</td>
                      <td class="tabletext" valign="top"><%=utilityAllowance%>
                      </td>
                    </tr>
                     <tr>
                      <td class="tablelabel" valign="top">Property Manager</td>
                      <td class="tabletext" valign="top" ><%=propertyManager%></td>
                      <td class="tablelabel" valign="top">Restricted Units</td>
                      <td class="tabletext" valign="top"><%=restrictedUnits%>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listLsoAddress.do?lsoId=<bean:write name="viewOccupancyForm" property="lsoId" />&ed=<%=editable%>" class="tabletitle">Occupant Address</a></td>
                      <%if(editable.equals("true")){%><% if(addressTech){%>
                      <security:editable levelId="<%=lsoId%>" levelType="<%=level%>" editProperty="checkUser">
                      <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addLsoAddress.do?origin=OM&lsoId=<bean:write name="viewOccupancyForm" property="lsoId" />" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                      </security:editable>
                      <%}%><%}%>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Address</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="addressStreetNumber" />&nbsp;<bean:write name="viewOccupancyForm" property="addressStreetModifier" />&nbsp;<bean:write name="viewOccupancyForm" property="addressStreetName" />&nbsp;<bean:write name="viewOccupancyForm" property="addressUnit" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">&nbsp;</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="addressCity" />&nbsp;<bean:write name="viewOccupancyForm" property="addressState" />&nbsp;<bean:write name="viewOccupancyForm" property="addressZip" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Description</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="addressDescription" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Primary</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="addressPrimary" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Active</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="addressActive" /></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    <%if(editable.equals("true")){
                    if(addressTech){
                         if (occupancyApnList.isEmpty() ) {  %>
                           <td width="99%" class="tabletitle">Occupant APN </td>
                         <%}else{%>
                           <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/editOccupancyApn.do?lsoId=<bean:write name="viewOccupancyForm" property="lsoId" />&apn=<bean:write name="viewOccupancyForm" property="apn" />&fflag=<bean:write name="viewOccupancyForm" property="apnforeignflag" />" class="tabletitle">Occupant APN</a></td>
                         <%}%>
                          <security:editable levelId="<%=lsoId%>" levelType="<%=level%>" editProperty="checkUser">
                          <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addOccupancyApn.do?lsoId=<bean:write name="viewOccupancyForm" property="lsoId" />&apn=0" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                          </security:editable>
                      <%}else{%>
                          <td width="100%" class="tabletitle"> Occupant APN </td>
                      <%}}%>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                     <html:hidden property="lsoId" />
                  <%if (occupancyApnList.isEmpty() ){%>
                      <tr valign="top">
                         <td class="tablelabel" width="25%">APN #</td>
                         <td class="tabletext" width="75%"> </td>
                       </tr>
                  <%}else{%>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">APN #</td>
                      <td class="tabletext" width="75%">
					  <html:select property="apn" size="1" styleClass="textbox" onchange='document.forms[0].submit()'>
					  	<html:options collection="occupancyApnList" property="apn" labelProperty="apn" />
					  </html:select>
					</td>
                    </tr>

                    <% }  %>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Owner Name</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnOwnerName" /></td>
					</tr>
					<logic:equal value="" name="viewOccupancyForm" property="apnforeignflag">
					    <tr valign="top">
					      <td class="tablelabel" width="25%">Address</td>
					      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnStreetNumber" />&nbsp;<bean:write name="viewOccupancyForm" property="apnStreetModifier" />&nbsp;<bean:write name="viewOccupancyForm" property="apnStreetName" /></td>
					    </tr>
					    <tr valign="top">
					      <td class="tablelabel" width="25%">&nbsp;</td>
					      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnCity" />  &nbsp;<bean:write name="viewOccupancyForm" property="apnState" />&nbsp;<bean:write name="viewOccupancyForm" property="apnZip" /></td>
					    </tr>
					</logic:equal>

					<logic:equal value="N" name="viewOccupancyForm" property="apnforeignflag">
					    <tr valign="top">
					      <td class="tablelabel" width="25%">Address</td>
					      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnStreetNumber" /> &nbsp;<bean:write name="viewOccupancyForm" property="apnStreetModifier" />&nbsp;<bean:write name="viewOccupancyForm" property="apnStreetDirection" /> &nbsp;<bean:write name="viewOccupancyForm" property="apnStreetName" /> </td>
					    </tr>
					    <tr valign="top">
					      <td class="tablelabel" width="25%">&nbsp;</td>
					      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnCity" /> &nbsp;<bean:write name="viewOccupancyForm" property="apnState" />  &nbsp;<bean:write name="viewOccupancyForm" property="apnZip"/> </td>
					    </tr>
					</logic:equal>

					<logic:equal value="Y" name="viewOccupancyForm" property="apnforeignflag">
					    <tr valign="top">
					      <td class="tablelabel" width="25%">Address</td>
					      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnForeignAddress1" />&nbsp;<bean:write name="viewOccupancyForm" property="apnForeignAddress2" />&nbsp;<bean:write name="viewOccupancyForm" property="apnForeignAddress3" /></td>
					    </tr>
					    <tr valign="top">
					      <td class="tablelabel" width="25%">&nbsp;</td>
					      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnForeignAddress4" />&nbsp;<bean:write name="viewOccupancyForm" property="apnState" />&nbsp;<bean:write name="viewOccupancyForm" property="apnZip" /></td>
					    </tr>
					</logic:equal>

                    <tr valign="top">
                      <td class="tablelabel" width="25%">Email</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnEmail" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Phone</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnPhone" /></td>
                    </tr>
                    <tr valign="top">
                      <td class="tablelabel" width="25%">Fax</td>
                      <td class="tabletext" width="75%"><bean:write name="viewOccupancyForm" property="apnFax" /></td>
                    </tr>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>

		<jsp:include page="../includes/comments.jsp" flush="true">
			<jsp:param name="id" value="<%=lsoId%>" />
			<jsp:param name="level" value="<%=level%>" />
			<jsp:param name="editable" value="<%=editable%>" />
		</jsp:include>

        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
					  <td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=lsoId%>&level=<%=level%>" class="tabletitle">Attachments</a></nobr></td>
                      <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addAttachment.do?levelId=<%=lsoId%>&level=<%=level%>" class="tablebutton">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                    <tr valign="top">
                      <td class="tablelabel">ID</td>
                      <td class="tablelabel">Description</td>
                      <td class="tablelabel">Size</td>
                      <td class="tablelabel">Date</td>
                    </tr>
					<%int i=0;%>
                        <logic:iterate id="attachment" name="attachmentList" type="elms.app.common.Attachment" scope="request">
						<%
							List attachmentsList = (List)request.getAttribute("attachmentList");
							Attachment attachmentObj = new Attachment();
							String fileNameStr = "";

							try{
								attachmentObj = (Attachment)attachmentsList.get(i);
								fileNameStr = attachmentObj.file.getFileName();
								fileNameStr = java.net.URLDecoder.decode(fileNameStr, "UTF-8");
							}catch(Exception e){}
						 %>
                        <tr valign="top" class="tabletext">
                            <td> <%=fileNameStr%> </td>
                            <td> <bean:write name="attachment" property="description"/> </td>
                            <td> <bean:write name="attachment" property="file.fileSize"/> </td>
                            <td> <bean:write name="attachment" property="file.strCreateDate"/> </td>
                        </tr>
						<%i++; %>
                        </logic:iterate>

                  </table>
                </td>
              </tr>
            </table>
          </td>
          </tr>
      </table>
    </td>
    <td width="1%">
      <table width="200" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="32">&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=<%=lsoId%>&level=<%=level%>&ed=<%=editable%>" class="tabletitle">Holds</a></td>
                      <% if(editable.equals("true")){ %>
                      <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addHold.do?levelId=<%=lsoId%>&level=<%=level%>" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                      <%}%>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                     <logic:iterate id="hold" name="holds">
                           <tr valign="top" class="tabletext">
                               <td><bean:write name="hold" property="type"/></td>
                               <td><bean:write name="hold" property="title"/></td>
                          </tr>
                    </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=lsoId%>&conditionLevel=<%=level%>&ed=<%=editable%>" class="tabletitle">Conditions</a></td>
                      <%if(editable.equals("true")){%>
                      <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addCondition.do?levelId=<%=lsoId%>&conditionLevel=<%=level%>" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                      <%}%>
                    </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  <logic:iterate id="cond" name="condList" type="elms.app.common.Condition">
                  <tr valign="top">
                  <td class="tablelabel"><bean:write name="cond" property="conditionId"/></td>
                  <td class="tabletext"><bean:write name="cond" property="shortText"/></td>
                  </tr>
                  </logic:iterate>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
			<tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle">Site Data</td>
                                <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/viewSiteStructure.do?levelType=O&levelId=<%=lsoId%>&lsoId=<%=lsoId%>" class="tablebutton">View</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>

        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">
                      <a href="<%=contextRoot%>/listPeople.do?id=<%=lsoId%>&type=O&ed=<%=editable%>&ed=<%=editable%>" class="tabletitle">Tenants</a>
                      <%if(editable.equals("true")){%>
                       <%if(user.getDepartment().getDepartmentCode().equalsIgnoreCase("HD")){%>
                      <td width="1%" class="tablebutton">
                      <a href="<%=contextRoot%>/searchPeople.do?level=O" class="tablebutton">Add</a><img src="../images/site_blt_plus_000000.gif" width="21" height="9"></td>
                      <%}%><%}%>
                    </tr>
                    <tr>
                <td>
<!--                 <div style=" height: 100px; width: 100px; font-size: 5px; overflow-y: auto;">  -->
                  <table width="220%" border="0" cellspacing="1" cellpadding="2">
                     <logic:iterate id="tenant" name="tenants">
                           <tr valign="top" class="tabletext">
                               <td><a href='<%=contextRoot%>/editPeople.do?peopleId=<bean:write name="tenant" property="tenantId"/>&levelType=O' ><bean:write name="tenant" property="name"/></a></td>
                          </tr>
                    </logic:iterate>
                  </table>
<!--                   </div> -->
                </td>
              </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>




      </table>
    </td>
  </tr>
</table>
</html:form>
</body>
</html:html>

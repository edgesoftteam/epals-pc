<%@ page import="elms.common.*,elms.app.common.*,java.util.*,elms.security.*,elms.util.*, elms.control.beans.*" %>
<%
   boolean general=false,conditionTech=false, userTech=false, peopleTech=false, projectTech=false, feesTech = false, addressTech = false , businessLicenseUser=false, businessLicenseApproval=false, businessTaxUser = false, businessTaxApproval = false, codeInspector = false, regulatoryPermit = false;
   User user = (User)session.getAttribute("user");
   List groups = (List) user.getGroups();
   Iterator itr = groups.iterator();
   while(itr.hasNext()){
        Group group = (elms.security.Group) itr.next();
        if(group == null) group = new elms.security.Group();
        if(group.groupId == Constants.GROUPS_USER_MAINTENANCE) userTech = true;
        if(group.groupId == Constants.GROUPS_CONDITIONS_LIBRARY_MAINTENANCE) conditionTech = true;
        if(group.groupId == Constants.GROUPS_PEOPLE_MAINTENANCE) peopleTech = true;
        if(group.groupId == Constants.GROUPS_PROJECT_MAINTENANCE) projectTech = true;
        if(group.groupId == Constants.GROUPS_ADDRESS_MAINTENANCE) addressTech = true;
        if(group.groupId == Constants.GROUPS_FEE_MAINTENANCE) feesTech = true;
        if(group.groupId == Constants.GROUPS_GENERAL) general = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_USER) businessLicenseUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_LICENSE_APPROVAL) businessLicenseApproval = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_USER) businessTaxUser = true;
		if(group.groupId == Constants.GROUPS_BUSINESS_TAX_APPROVAL) businessTaxApproval = true;
		if(group.groupId == Constants.GROUPS_CODE_INSPECTOR) codeInspector = true;
		if(group.groupId == Constants.GROUPS_REGULATORY_PERMIT) regulatoryPermit = true;
   }

   String mapAddressString = (String) request.getAttribute("mapAddressString");


%>

<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<app:checkLogon/>
<html:html>
<head>

<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">
</head>

<bean:define id="editable" name="editable" type="java.lang.String" scope="request"/>
<bean:define id="lsoId" name="lsoId" type="java.lang.String" scope="request"/>


<%
	String contextRoot = request.getContextPath();
	String level = "L";

	java.util.List landApnList = (java.util.List) request.getAttribute("landApnList");
	if (landApnList  != null ) pageContext.setAttribute("landApnList",landApnList);

	java.util.List holds = (java.util.List) request.getAttribute("holds");
	if(holds == null) holds = new java.util.ArrayList();
	pageContext.setAttribute("holds",holds);

	java.util.List condList = (java.util.List)request.getAttribute("condList");
	if(condList == null) condList = new java.util.ArrayList();
	pageContext.setAttribute("condList",condList);

	// conditionally  doing the tree refresh
	String onloadAction = (String)session.getAttribute("onloadAction");
	if (onloadAction == null ) onloadAction = "";

	final String id = request.getParameter("id");
%>

<html:form action="/viewLand">
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onload="<%=onloadAction%>">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%" height="415">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b"><img src="../images/land_icon_small.gif" border="0">&nbsp;Land Manager</font><br>
                <br>
                </td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <%if(editable.equals("true")){%>
                                <td width="97%" class="tabletitle">

                                	<% if(addressTech){%>
                                		<a href="<%=contextRoot%>/editLandDetails.do?lsoId=<%=lsoId%>" class="tabletitle">
                                	<%}%>
                                	 Land Details
                                	<% if(addressTech){%>
	                                	</a>
	                                <%}%>

    	                        </td>
                                <% if(addressTech){%>
                                <td width="1%" class="tablebutton"><nobr>
                                	<security:editable levelId="<%=lsoId%>" levelType="L" editProperty="checkUser">
	                                		<a href="<%=contextRoot%>/addStructure.do?parentId=<%=lsoId%>" class="tablebutton">Add Structure
	                                			
	                                		</a>
                                	</security:editable></nobr>
                                </td>
                                <%}%>
                                <% if((!(businessLicenseUser) && !(businessTaxUser) && !(general)) || (codeInspector) || (regulatoryPermit)){%>
                                <security:editable levelId="<%=lsoId%>" levelType="L" editProperty="checkUser">
                                <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addProject.do?lsoId=<%=lsoId%>" class="tablebutton">Add Project</a></nobr></td>
                                </security:editable>
                                <%}%>
                                <%}else{%>
                                <td width="100%" class="tabletitle">Land Details</td>
                                <%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">ID</td>
                                <td class="tabletext"><%=lsoId%> </td>
                                <td class="tablelabel">Description</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="description"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Active</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="active"/></td>
                                <td class="tablelabel">Alias</td>
                               <td class="tabletext"><bean:write name="viewLandForm" property="alias"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Zone</td>
                                <td class="tabletext"><%
                                  List l = new ArrayList();
                                  String s=((ViewLandForm)request.getAttribute("viewLandForm")).getZone();
                                  l= StringUtils.stringToList(s,", ");
                                  Iterator it=l.iterator();
                                  while(it.hasNext())      {
                                  out.println((String)it.next()+"<br>"+"<dt>");
                                  }
                                  %>
                                </td>
                                <td class="tablelabel">Use</td>
                                <td class="tabletext"><%
                                  List l2 = new ArrayList();
                                  String s1=((ViewLandForm)request.getAttribute("viewLandForm")).getUse();
                                  l2= StringUtils.stringToList(s1,",");
                                  Iterator it1=l2.iterator();
                                  while(it1.hasNext())      {
                                  out.println((String)it1.next()+"<br>"+"<dt>");
                                  }
                                  %>
                                </td>
                                </tr>

                            <tr valign="top">
                                <td class="tablelabel">P-Zone</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="parkingZone"/></td>
                                <td class="tablelabel">R-Zone</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="resZone"/></td>
                            </tr>

                            <tr valign="top">
                                <td class="tablelabel">Coordinate X</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="coordinateX"/></td>
                                <td class="tablelabel">Coordinate Y</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="coordinateY"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Address Range</td>
                         		<td class="tabletext">
                           			<logic:iterate id="addressRange" name="viewLandForm" property="addressRangeList">
                               			<bean:write name="addressRange"/><br>
                           			</logic:iterate>
                         		</td>
                                <td class="tablelabel">Label</td>
                                <td class="tabletext"><bean:write name="viewLandForm" property="label"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">High Fire Area</td>
                         		<td class="tabletext" colspan="3">
                           			<bean:write name="viewLandForm" property="highFireArea"/>
                         		</td>
                                
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
								<td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/listLsoAddress.do?lsoId=<%=lsoId%>&ed=<%=editable%>" class="tabletitle">Land Address</a></nobr></td>
                                <%if(editable.equals("true")){%>
                                <%if(addressTech){%>
                                <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addLsoAddress.do?origin=LM&lsoId=<%=lsoId%>" class="tablebutton">Add</a></td>
                                <%}%><%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">


                            <tr valign="top">
                                <td class="tablelabel" width="25%">Address </td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="addressStreetNumber"/>&nbsp;<bean:write name="viewLandForm" property="addressStreetModifier"/>&nbsp;<bean:write name="viewLandForm" property="addressStreetName"/>&nbsp;<bean:write name="viewLandForm" property="addressUnit"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">&nbsp;</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="addressCity"/>&nbsp;<bean:write name="viewLandForm" property="addressState"/>&nbsp;<bean:write name="viewLandForm" property="addressZip"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">Description</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="addressDescription"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">Primary</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="addressPrimary"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">Active</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="addressActive"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <%if(editable.equals("true")){
                                	if(addressTech){
                                      if (landApnList.isEmpty() ) {  %>
                                        <td width="99%" class="tabletitle">Land APN</td>
                                      <%}else{%>
										<td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/editLandApn.do?lsoId=<bean:write name="viewLandForm" property="lsoId" />&apn=<bean:write name="viewLandForm" property="apn" />&fflag=<bean:write name="viewLandForm" property="apnForeignFlag" />" class="tabletitle">Land APN</a></nobr></td>
                                      <%}%>
                                        <td width="1%" class="tablebutton"><a href="<%=contextRoot%>/addLandApn.do?lsoId=<bean:write name="viewLandForm" property="lsoId" />" class="tablebutton">Add</a></td>
                               <%}else{%>
                                   <td width="100%" class="tabletitle">Land APN</td>
                               <%}%><%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <html:hidden property="lsoId"/>

                            <% if (landApnList.isEmpty() ) {  %>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">APN #</td>
                                <td class="tabletext" width="75%"></td>
                            </tr>
                            <% } else {  %>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">APN #</td>
                                <td class="tabletext" width="75%"><html:select property="apn" size="1" styleClass="textbox" onchange='document.forms[0].submit()'> <html:options collection="landApnList" property="apn" labelProperty="apn"/> </html:select></td>
                            </tr>
                            <% }  %>


                            <tr valign="top">
                                <td class="tablelabel" width="25%">Owner Name</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnOwnerName"/></td>

                                <logic:equal value="N" name="viewLandForm" property="apnForeignFlag">
                                <tr valign="top">
                                    <td class="tablelabel" width="25%">Address</td>
                                    <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnStreetNumber"/>&nbsp;<bean:write name="viewLandForm" property="apnStreetModifier"/>&nbsp;<bean:write name="viewLandForm" property="apnStreetName"/></td>
                                </tr>
                                <tr valign="top">
                                    <td class="tablelabel" width="25%">&nbsp;</td>
                                    <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnCity"/>&nbsp;<bean:write name="viewLandForm" property="apnState"/>&nbsp;<bean:write name="viewLandForm" property="apnZip"/></td>
                                </tr>
                                </logic:equal>

                                <logic:equal value="Y" name="viewLandForm" property="apnForeignFlag">
                                <tr valign="top">
                                    <td class="tablelabel" width="25%">Address</td>
                                    <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnForeignAddress1"/>&nbsp;<bean:write name="viewLandForm" property="apnForeignAddress2"/>&nbsp;<bean:write name="viewLandForm" property="apnForeignAddress3"/></td>
                                </tr>
                                <tr valign="top">
                                    <td class="tablelabel" width="25%">&nbsp;</td>
                                    <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnForeignAddress4"/>&nbsp;<bean:write name="viewLandForm" property="apnState"/>&nbsp;<bean:write name="viewLandForm" property="apnZip"/></td>
                                </tr>
                                </logic:equal>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">Email</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnEmail"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">Phone</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnPhone"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel" width="25%">Fax</td>
                                <td class="tabletext" width="75%"><bean:write name="viewLandForm" property="apnFax"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
<tr>
    <td colspan="3">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
	            <td>
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr>
		                    <td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/listComment.do?levelId=<%=lsoId%>&commentLevel=<%=level%>&ed=<%=editable%>" class="tabletitle">Comments</a></nobr></td>
	                      	<logic:equal name="editable" value="true">
	                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addComment.do?levelId=<%=lsoId%>&commentLevel=<%=level%>" class="tablebutton">Add</a></nobr></td>
	                       	</logic:equal>
		                </tr>
		            </table>
	            </td>
	        </tr>
	        <tr>
	            <td background="../images/site_bg_B7C1CB.jpg">
		            <table width="100%" border="0" cellspacing="1" cellpadding="2">
		                <tr valign="top">
		                    <td class="tablelabel" width="20%">Date</td>
		                    <td class="tablelabel">Comment</td>
		                </tr>
		                <logic:iterate id="comment" name="commentList" type="elms.app.common.Comment" scope="request">
		                <tr valign="top" class="tabletext">
		                    <td class="tabletext"> <bean:write name="comment" property="displayEnterDate"/> </td>
		                    <td class="tabletext"> <bean:write name="comment" property="comment"/> </td>
		                </tr>
		                </logic:iterate>
		            </table>
	            </td>
	        </tr>
	    </table>
    </td>
</tr>



<%--end of comments.jsp--%>



		    <tr>
				<td>&nbsp;</td>
			</tr>

			<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="99%" class="tabletitle"><nobr><a
								href="<%=contextRoot%>/addAttachment.do?levelId=<%=lsoId%>&level=<%=level%>"
								class="tabletitle">Attachments</a></nobr></td>
							<td width="1%" class="tablebutton"><nobr><a
								href="<%=contextRoot%>/addAttachment.do?levelId=<%=lsoId%>&level=<%=level%>"
								class="tablebutton">Add<img
								src="../images/site_blt_plus_000000.gif" width="21" height="9"
								border="0"></a></nobr></td>
							</tr>
					</table>
					</td>
				</tr>
				<tr>
					<td background="../images/site_bg_B7C1CB.jpg">
					<table width="100%" border="0" cellspacing="1" cellpadding="2">
						<tr valign="top">
							<td class="tablelabel">ID</td>
							<td class="tablelabel">Description</td>
							<td class="tablelabel">Size</td>
							<td class="tablelabel">Date</td>
						</tr>
						<%int i=0;%>
						<logic:iterate id="attachment" name="attachmentList"
							type="elms.app.common.Attachment" scope="request">
							<%
							List attachmentsList = (List)request.getAttribute("attachmentList");
							Attachment attachmentObj = new Attachment();
							String fileNameStr = "";
							//ArrayList arrayList = new ArrayList();
							try{
								attachmentObj = (Attachment)attachmentsList.get(i);
								fileNameStr = attachmentObj.file.getFileName();
								fileNameStr = java.net.URLDecoder.decode(fileNameStr, "UTF-8");
							}catch(Exception e){}
						 %>
							<tr valign="top" >
								<td class="tabletext"> <%=fileNameStr%> </td>
								<td class="tabletext"> <bean:write name="attachment" property="description" /> </td>
								<td class="tabletext"> <bean:write name="attachment" property="file.fileSize" /> </td>
								<td class="tabletext"> <bean:write name="attachment" property="file.strCreateDate" /> </td>
							</tr>
							<%i++; %>
						</logic:iterate>
					</table>
					</td>
				</tr>
			</table>
			</td>
			</tr></table>
        </td>
        <td width="1%" height="415">
        <table width="200" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="70%" class="tabletitle"><a href="<%=contextRoot%>/listHold.do?levelId=<%=lsoId%>&level=<%=level%>&ed=<%=editable%>" class="tabletitle">Holds</a></td>
                                <%if(editable.equals("true")){%>
                                <td width="29%" class="tablebutton"><a href="<%=contextRoot%>/addHold.do?levelId=<%=lsoId%>&level=<%=level%>" class="tablebutton">Add</a></td>
                                <%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="hold" name="holds">
                            <tr valign="top" class="tabletext">
                                <td><bean:write name="hold" property="type"/></td>
                                <td><bean:write name="hold" property="title"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="70%" class="tabletitle"><a href="<%=contextRoot%>/listCondition.do?levelId=<%=lsoId%>&conditionLevel=<%=level%>&ed=<%=editable%>" class="tabletitle">Conditions</a></td>
                                <%if(editable.equals("true")){%>
                                <td width="29%" class="tablebutton"><a href="<%=contextRoot%>/addCondition.do?levelId=<%=lsoId%>&conditionLevel=<%=level%>" class="tablebutton">Add</a></td>
                                <%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <logic:iterate id="cond" name="condList" type="elms.app.common.Condition">
                            <tr valign="top">
                                <td class="tablelabel"><bean:write name="cond" property="conditionId"/></td>
                                <td class="tabletext"><bean:write name="cond" property="shortText"/></td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
			<tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="70%" class="tabletitle"><a href="<%=contextRoot%>/viewSiteStructure.do?levelType=L&levelId=<%=lsoId%>&lsoId=<%=lsoId%>">Site Data</a></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
            </tr>
        <tr>
          <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="99%" class="tabletitle">
                      <a href="<%=contextRoot%>/listPeople.do?id=<%=lsoId%>&type=L&ed=<%=editable%>&ed=<%=editable%>" class="tabletitle">Tenants</a>    </td>                 
                    </tr>
                    
                  </table>
                </td>
              </tr>
               <tr>
                <td>
<!--                 <div style=" height: 100px; width: 100px; font-size: 5px; overflow-y: auto;">  -->
                  <table width="220%" border="0" cellspacing="1" cellpadding="2">                  
                     <logic:iterate id="tenant" name="tenants">
                           <tr valign="top" class="tabletext">
                               <td><a href='<%=contextRoot%>/editPeople.do?peopleId=<bean:write name="tenant" property="tenantId"/>' ><bean:write name="tenant" property="name"/></a></td>
                          </tr>
                    </logic:iterate>
                  </table>
<!--                   </div> -->
                </td>
              </tr>
              <tr>
                <td background="../images/site_bg_B7C1CB.jpg">
                  <table width="100%" border="0" cellspacing="1" cellpadding="2">
                  
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

			<jsp:include page="../includes/mapPanel.jsp" flush="true">
				<jsp:param name="addressString" value="<%=mapAddressString %>" />
			</jsp:include>




        </table>
        </td>
    </tr>
</table>
</body>
</html:form> </html:html>

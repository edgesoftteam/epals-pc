
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<app:checkLogon/>

<html:html>
<head>
<html:base/>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="../css/elms.css" type="text/css">

<%
 String contextRoot = request.getContextPath();
 String lsoId = (String)session.getAttribute("lsoId");
 String lsoType = (String) session.getAttribute("lsoType");
 elms.app.lso.Assessor assessor =(elms.app.lso.Assessor) request.getAttribute("assessor");
 String apn= elms.util.StringUtils.nullReplaceWithEmpty(assessor.getParcelNbr());
 apn= apn.replaceAll("-","");
 String strUrl = "";
 try {
	if (lsoType.equals("L"))
		strUrl = contextRoot + "/viewLand.do?lsoId=" + lsoId;
	else if (lsoType.equals("S"))
		strUrl = contextRoot + "/viewStructure.do?lsoId=" + lsoId;
	else if (lsoType.equals("O"))
		strUrl = contextRoot + "/viewOccupancy.do?lsoId=" + lsoId;
	} catch (Exception e) {}

%>
</head>
<script language="JavaScript">

function backPage() {
	document.forms[0].action='<%=strUrl%>';
	document.forms[0].submit();
}


</script>
<html:errors/>
<body class="tabletext" text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >

<table width="100%" border="0" cellspacing="10" cellpadding="0">
    <tr valign="top">
        <td width="99%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><font class="con_hdr_3b">Assessor's Data</font><br><br></td>
            </tr>

            <tr>
            	<td>&nbsp;</td>
            </tr>




 				<tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="87%" class="tabletitle">Address and Owner Information</td>
							        <td width="6%" class="tablebutton">
							         <html:button  value="Back" property="button" styleClass="button" onclick="javascript:history.back()"/>
							        </td>
							      </tr>
						    </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Parcel #</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="parcelNbr"/><br>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Address</td>
                                <td class="tabletext">

                                         <bean:write scope="request" name="assessor" property="SITUS_HOUSE_NO"/>&nbsp;
                                         <bean:write scope="request" name="assessor" property="SITUS_FRACTION"/>&nbsp;
                                         <bean:write scope="request" name="assessor" property="SITUS_STREET_NAME"/>,
                                         <bean:write scope="request" name="assessor" property="SITUS_DIRECTION"/>&nbsp;
                                         <bean:write scope="request" name="assessor" property="SITUS_UNIT"/>

                               </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">City, State, Zip</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="SITUS_CITY_STATE"/>&nbsp;<bean:write scope="request" name="assessor" property="SITUS_ZIP"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Mailing Address </td>
                                <td class="tabletext">

                                         <bean:write scope="request" name="assessor" property="MAIL_ADDRESS_HOUSE_NO"/>&nbsp;
                                         <bean:write scope="request" name="assessor" property="MAIL_ADDRESS_FRACTION"/>&nbsp;
                                         <bean:write scope="request" name="assessor" property="MAIL_ADDRESS_STREET_NAME"/>,
                                         <bean:write scope="request" name="assessor" property="MAIL_ADDRESS_DIRECTION"/>&nbsp;
                                         <bean:write scope="request" name="assessor" property="MAIL_ADDRESS_UNIT"/>

                               </td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">City, State, Zip</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="MAIL_ADDRESS_CITY_STATE"/>&nbsp;<bean:write scope="request" name="assessor" property="MAIL_ADDRESS_ZIP"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Owner</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="FIRST_OWNER_ASSESSEE_NAME"/>&nbsp;<bean:write scope="request" name="assessor" property="FIRST_OWNER_NAME_OVERFLOW"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Legal Information</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tabletext" width="99%">

                                          <bean:write scope="request" name="assessor" property="LEGAL_DESCRIPTION_1"/><br>&nbsp;
                                          <bean:write scope="request" name="assessor" property="LEGAL_DESCRIPTION_2"/><br>&nbsp;
                                          <bean:write scope="request" name="assessor" property="LEGAL_DESCRIPTION_3"/><br>&nbsp;
                                          <bean:write scope="request" name="assessor" property="LEGAL_DESCRIPTION_4"/><br>&nbsp;
                                          <bean:write scope="request" name="assessor" property="LEGAL_DESCRIPTION_5"/><br>&nbsp;
                                          <bean:write scope="request" name="assessor" property="LEGAL_DESCRIPTION_6"/>

                               </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">General Information </td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Tax Rate Area</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="TAX_RATE_AREA"/></td>
                                <td class="tablelabel">Agency Class</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="AGENCY_CLASS_NO"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Zoning Code</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="ZONING_CODE"/></td>
                                <td class="tablelabel">Use Code</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="USE_CODE"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Current Land Year and Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAND_CURRENT_ROLL_YEAR"/> &nbsp;&nbsp;<bean:write scope="request" name="assessor" property="LAND_CURRENT_VALUE"/></td>
                                <td class="tablelabel">Improvement Year and Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="IMPROVE_CURRENT_ROLL_YEAR"/>&nbsp;&nbsp;<bean:write scope="request" name="assessor" property="IMPROVE_CURRENT_VALUE"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Special Name</td>
                                <td class="tabletext">

                                        <bean:write scope="request" name="assessor" property="SPECIAL_NAME_ASSESSEE"/>

                                </td>
                                <td class="tablelabel">Special Name Legend</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="SPECIAL_NAME_LEGEND"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Second Owner Name</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="SECOND_OWNER_ASSESSEE_NAME"/>
                                </td>
                                <td class="tablelabel">Tax Status Key</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="TAX_STATUS_KEY"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Recording Date</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="RECORDING_DATE"/></td>
                                <td class="tablelabel">Hazard Abatement City Key</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="HAZARD_ABATEMENT_CITY_KEY"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Tax Status Year Sold</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="TAX_STATUS_YEAR_SOLD"/></td>
                                <td class="tablelabel">Partial Interest</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="PARTIAL_INTEREST"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Hazard Abatement Information</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="HAZARD_ABATEMENT_INFORMATION"/></td>
                                <td class="tablelabel">Ownership Code</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="OWNERSHIP_CODE"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Document Reason Code</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="DOCUMENT_REASON_CODE"/></td>
                                <td class="tablelabel">Personal Property Key</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="PERSONAL_PROPERTY_KEY"/>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Exemption Claim Type</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="EXEMPTION_CLAIM_TYPE"/>&nbsp;</td>
                                <td class="tablelabel">Personal Property Exemption Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="PERSONAL_PROPERTY_EXEMPTION_VA"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Personal Property Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="PERSONAL_PROPERTY_VALUE"/></td>
                                <td class="tablelabel">Fixture Exemption Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="FIXTURE_EXEMPTION_VALUE"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Fixture Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="FIXTURE_VALUE"/></td>
                                <td class="tablelabel">Homeowner Exemption Value</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="HOMEOWNER_EXEMPTION_VALUE"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Homeowner No Of Exemptions</td>
                                <td class="tabletext" colspan="3"><bean:write scope="request" name="assessor" property="HOMEOWNER_NO_OF_EXEMPTIONS"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Real Estate Exemption Value</td>
                                <td class="tabletext" colspan="3"><bean:write scope="request" name="assessor" property="REAL_ESTATE_EXEMPTION_VALUE"/>&nbsp;</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Last Sale Information</td>
                           </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Last Sale Information Verify Key</td>
                                <td class="tabletext" colspan="3"><bean:write scope="request" name="assessor" property="LAST_SALE_ONE_VERIFY_KEY"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Last Sale One Date</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAST_SALE_ONE_DATE"/></td>
                                <td class="tablelabel">Last Sale One Sale Amount</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAST_SALE_ONE_SALE_AMOUNT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Last Sale Two Verify Key</td>
                                <td class="tabletext" colspan="3"><bean:write scope="request" name="assessor" property="LAST_SALE_TWO_VERIFY_KEY"/>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Last Sale Two Date</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAST_SALE_TWO_SALE_DATE"/></td>
                                <td class="tablelabel">Last Sale Two Sale Amount</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAST_SALE_TWO_SALE_AMOUNT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Last Sale Three Verify Key</td>
                                <td class="tabletext" colspan="3"><bean:write scope="request" name="assessor" property="LAST_SALE_THREE_VERIFY_KEY"/>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Last Sale Three Date</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAST_SALE_THREE_SALE_DATE"/></td>
                                <td class="tablelabel">Last Sale Three Sale Amount</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="LAST_SALE_THREE_SALE_AMOUNT"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Current Building Information</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Year Built</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_YEAR_BUIL"/></td>
                                <td class="tablelabel">No Of Units</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_NO_OF_UNI"/></td>
                                <td class="tablelabel">No Of Bedrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_NO_OF_BED"/></td>
                                <td class="tablelabel">No Of Bathrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_NO_OF_BAT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Area</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_SQUARE_FE"/></td>
                                <td class="tablelabel">Sub-parts</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_SUBPART"/></td>
                                <td class="tablelabel">Design Type</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_DESIGN_TY"/></td>
                                <td class="tablelabel">Quality</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_1_QUALITY"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Building Information History 2</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Year Built</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_YEAR_BUIL"/></td>
                                <td class="tablelabel">No Of Units</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_NO_OF_UNI"/></td>
                                <td class="tablelabel">No Of Bedrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_NO_OF_BED"/></td>
                                <td class="tablelabel">No Of Bathrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_NO_OF_BAT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Area</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_SQUARE_FE"/></td>
                                <td class="tablelabel">Sub-parts</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_SUBPART"/></td>
                                <td class="tablelabel">Design Type</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_DESIGN_TY"/></td>
                                <td class="tablelabel">Quality</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_2_QUALITY"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Building Information History 3</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Year Built</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_YEAR_BUIL"/></td>
                                <td class="tablelabel">No Of Units</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_NO_OF_UNI"/></td>
                                <td class="tablelabel">No Of Bedrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_NO_OF_BED"/></td>
                                <td class="tablelabel">No Of Bathrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_NO_OF_BAT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Area</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_SQUARE_FE"/></td>
                                <td class="tablelabel">Sub-parts</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_SUBPART"/></td>
                                <td class="tablelabel">Design Type</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_DESIGN_TY"/></td>
                                <td class="tablelabel">Quality</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_3_QUALITY"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Building Information History 4</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Year Built</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_YEAR_BUIL"/></td>
                                <td class="tablelabel">No Of Units</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_NO_OF_UNI"/></td>
                                <td class="tablelabel">No Of Bedrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_NO_OF_BED"/></td>
                                <td class="tablelabel">No Of Bathrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_NO_OF_BAT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Area</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_SQUARE_FE"/></td>
                                <td class="tablelabel">Sub-parts</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_SUBPART"/></td>
                                <td class="tablelabel">Design Type</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_DESIGN_TY"/></td>
                                <td class="tablelabel">Quality</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_4_QUALITY"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td background="../images/site_bg_B7C1CB.jpg">Building Information History 5</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Year Built</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_YEAR_BUIL"/></td>
                                <td class="tablelabel">No Of Units</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_NO_OF_UNI"/></td>
                                <td class="tablelabel">No Of Bedrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_NO_OF_BED"/></td>
                                <td class="tablelabel">No Of Bathrooms</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_NO_OF_BAT"/></td>
                            </tr>
                            <tr valign="top">
                                <td class="tablelabel">Area</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_SQUARE_FE"/></td>
                                <td class="tablelabel">Sub-parts</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_SUBPART"/></td>
                                <td class="tablelabel">Design Type</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_DESIGN_TY"/></td>
                                <td class="tablelabel">Quality</td>
                                <td class="tabletext"><bean:write scope="request" name="assessor" property="BUILDING_DATA_LINE_5_QUALITY"/></td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
        </table>
        </td>
        <td width="1%">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="32">&nbsp;</td>
            </tr>
        </table>
        </td>
    </tr>
</table>

</body>

</html:html>
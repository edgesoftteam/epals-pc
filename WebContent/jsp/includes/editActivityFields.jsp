<%@page import="elms.app.project.ActivityAttributes"%>
<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/editActivity.js"></script>

<%@ page import="elms.control.beans.ActivityForm"%>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<%
   final String thisForm = request.getParameter("thisForm");
   String projectNameId = (String) request.getAttribute("projectNameId")!=null ? (String) request.getAttribute("projectNameId"): "";

   ActivityForm activityForm = (ActivityForm)request.getAttribute("activityForm");
   ActivityAttributes activityAttributes = (ActivityAttributes) request.getAttribute("activityAttributes");

   if(activityAttributes == null){
   	activityAttributes = new ActivityAttributes();
   }
if(activityForm == null){
  activityForm =  (ActivityForm)session.getAttribute("activityForm");
}

String levelId = "";
String levelType = "";
if(activityForm != null){
  levelId = activityForm.getActivityId();
  levelType = "A";

  if(levelId == null || levelId == "0"){
    levelId = activityForm.getSubProjectId();
    levelType = "Q";
  }


}
%>

<!-- 1 is the project name id for project name of building   -->
<% if(projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_BUILDING_ID))){ %>
<tr valign="top">
  <td class="tablelabel">Activity #</td>
  <td class="tabletext" ><bean:write name="<%=thisForm%>" property="activityNumber"/></td>
  <td class="tablelabel">Valuation</td>
  <td class="tabletext">
      <html:text property="valuation" size ="20" styleClass="textboxm" readonly="true" onkeypress="return DisplayPeriod();"/>
  </td>
</tr>
<%} else{%>
<tr valign="top">
  <td class="tablelabel" height="22">Activity #</td>
  <td class="tabletext" colspan="3" ><bean:write name="<%=thisForm%>" property="activityNumber"/></td>
</tr>
<%}%>


<tr valign="top">
   <td class="tablelabel">Activity Type</td>
   <td class="tabletext"><bean:write name="<%=thisForm%>" property="activityTypeDesc"/></td>
   <td class="tablelabel">Sub Type</td>
   <td class="tabletext">
   <html:select  multiple="true" size="5" styleClass="textbox"  property="activitySubTypes">
      	<html:options  collection="activitySubTypesList"  property="id"  labelProperty="description" />
   </html:select>
</tr>

<tr valign="top">
  <td class="tablelabel">Address</td>
  <td class="tabletext">
      <html:select property="address" styleClass="textbox" >
          <html:options collection="addresses"  property="addressId"  labelProperty="description" />
      </html:select>
  </td>
  <td class="tablelabel">Activity Status</td>
  <td class="tabletext">
      <html:select property="activityStatus" styleClass="textbox" onchange = "if (activityStatus!=null) {checkStatus();}">
           <html:option value="">Please Select</html:option>
          <html:options collection="activityStatuses"  property="status"  labelProperty="code" />
      </html:select>
  </td>
</tr>
<tr valign="top">
  <td class="tablelabel">Applied Date</td>
  <td class="tabletext" valign="top"><nobr>
      <html:text  property="startDate" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr>
  </td>
  <td class="tablelabel">Issue Date</td>
  <td class="tabletext" valign="top"><nobr>
      <html:text  property="issueDate" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);" onchange = "if (activityStatus!=null) {issueDateChange();}"/>
    </nobr>
  </td>
</tr>
<tr valign="top">
  <td class="tablelabel"><nobr><bean:write name="<%=thisForm%>" property="completionDateLabel"/></nobr></td>
  <td class="tabletext" valign="top"><nobr>
      <html:text  property="completionDate" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr>
  </td>
  <td class="tablelabel"><nobr><bean:write name="<%=thisForm%>" property="expirationDateLabel"/></nobr></td>
  <td class="tabletext" valign="top"><nobr>
      <html:text  property="expirationDate" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr>
  </td>
</tr>
<% if(projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_BUILDING_ID)) || projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PLANNING_ID)) || projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS_ID))){ %>
<tr valign="top">
  <td class="tablelabel">Microfilm</td>
  <td class="tabletext"  valign="top">
     <html:select  styleClass="textbox"  property="microfilm"  >
        <html:option value="">Please Select</html:option>
        <html:options  collection="microfilmStatuses"  property="code" labelProperty="description" />
     </html:select>
  </td>
  <td class="tablelabel"><nobr>PC/Approval Required</nobr></td>
  <%if(activityAttributes.getTotalPlanCheck()>0 ){%>
  <td  class="tabletext" ><html:checkbox property="planCheckRequired" onclick="return false;" onkeydown="return false;"/></td>
  <%}else{ %>
  <td  class="tabletext"><html:checkbox property="planCheckRequired"  /></td>
  <%} %>
</tr>
<tr>
  <td class="tablelabel">Development Fee Required</td>
  <td class="tabletext" valign="top">
      <html:checkbox property="developmentFeeRequired"/>
  </td>
  <td class="tablelabel">Label</td>
  <td class="tabletext" valign="top">
      <html:text styleClass="textbox" property="label" maxlength="25"/>
  </td>
</tr>

<%
}
else{
%>
<tr valign="top">
  <td class="tablelabel">Microfilm</td>
  <td class="tabletext"  valign="top" >
     <html:select  styleClass="textbox"  property="microfilm"  >
        <html:option value="">Please Select</html:option>
        <html:options  collection="microfilmStatuses"  property="code" labelProperty="description" />
     </html:select>
  </td>
  <td class="tablelabel">Label</td>
  <td class="tabletext" valign="top">
      <html:text styleClass="textbox" property="label" maxlength="25"/>
  </td>

</tr>
<%}%>
<tr>
  <td class="tablelabel">Parking Zone</td>
  <td class="tabletext" valign="top" >
	<html:select property="parkingZone" styleClass="textbox">
		<html:option value="">Please select</html:option>
		<html:options collection="parkingZones" property="pzoneId" labelProperty="name"/>
	</html:select>
  </td>
  <td class="tablelabel">Green Halo</td>
  <td class="tabletext" valign="top">
      <html:checkbox property="greenHalo"/>
  </td>
</tr>

<tr>
  <td class="tablelabel">Description</td>
  <td class="tabletext" valign="top" colspan="3">
      <html:textarea rows="5" cols="60" property="description"/>
  </td>
</tr>


<security:readable redLevelId="<%=levelId%>" redLevelType="<%=levelType%>" readProperty="field">
<tr valign="top">
  <td class="tablelabel">New Units</td>
  <td class="tabletext" valign="top">
     <html:text  property="newUnits" maxlength="10"  styleClass="textboxd" onkeypress="" onblur=""/>
  </td>
  <td class="tablelabel">Existing Units</td>
  <td class="tabletext" valign="top">
     <html:text  property="existingsUnits" maxlength="10"  styleClass="textboxd" onkeypress="" onblur=""/>
  </td>

</tr>

<tr>
  <td class="tablelabel">Demolished Units</td>
	<td class="tabletext" valign="top">
    	<html:text  property="demolishedUnits" maxlength="10"  styleClass="textboxd" onkeypress="" onblur=""/>
	</td>

</tr>

</security:readable>




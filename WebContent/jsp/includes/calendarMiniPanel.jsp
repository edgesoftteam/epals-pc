<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<%
	final String contextRoot = request.getContextPath();
	final String editable = request.getParameter("editable");
	final String id = request.getParameter("id");
	final String level = request.getParameter("level");

%>



<tr>
                <td colspan="3">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/addCalendarFeature.do?subProjectId=<%=id%>&level=<%=level%>" class="hdrs">Meeting Calendar</a></nobr></td>
                                	<%if(editable.equals("true")){%>
                                <td width="1%" class="tablelabel"><nobr><a href="<%=contextRoot%>/addCalendarFeature.do?subProjectId=<%=id%>&level=<%=level%>" class="hdrs">Add<img src="../images/site_blt_plus_000000.gif" width="21" height="9" border="0"></a></nobr></td>
                                <%}%>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <tr>
                        <td background="../images/site_bg_B7C1CB.jpg">
                        <table width="100%" border="0" cellspacing="1" cellpadding="2">
                            <tr valign="top">
                                <td class="tablelabel">Meeting date</td>
                                <td class="tabletext" class="tablelabel">Meeting Type</td>
                                <td class="tablelabel">Action Type</td>
                                <td class="tabletext" class="tablelabel">Comments</td>
                            </tr>
                            <logic:iterate id="calendar" name="calendarFeaturesList" type="elms.app.planning.CalendarFeature" scope="request">
                            <tr valign="top" class="tabletext">
                                <td> <bean:write name="calendar" property="meetingDate"/> </td>
                                <td> <bean:write name="calendar" property="meetingType"/> </td>
                                <td> <bean:write name="calendar" property="actionType"/> </td>
                                <td> <bean:write name="calendar" property="comments"/> </td>
                            </tr>
                            </logic:iterate>
                        </table>
                        </td>
                    </tr>
                </table>
                </td>
            </tr>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<%final String contextRoot = request.getContextPath();
final String editable = request.getParameter("editable");
final String subProId = request.getParameter("id");
final String level = "level"; //request.getParameter("level");	
request.setAttribute("subProjectId", subProId);
%>
<tr>
	<td>
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="73%" class="tabletitle"><a href="<%=contextRoot%>/viewActivityTeam.do?subProjectId=<%=subProId%>" class="tabletitle">Activity Team</a></td>
					<%if (editable.equals("true")) {%>
					<td width="26%" class="tablebutton"><a href="<%=contextRoot%>/viewActivityTeam.do?subProjectId=<%=subProId%>" class="tablebutton">View</a></td>
					<%}%>
				</tr>
			</table>
			</td>
		</tr>
		<tr>
			<td background="../images/site_bg_B7C1CB.jpg">
			<table width="100%" border="0" cellspacing="1" cellpadding="2">
				<logic:iterate id="processTeamRecord" name="activityTeamList"
					type="elms.app.common.ActivityTeamObject" scope="request">
					<tr valign="top" >
						<td class="tabletext"><bean:write name="processTeamRecord"
							property="title" /></td>
						<td class="tabletext"><bean:write name="processTeamRecord"
							property="name" /></td>
						<logic:equal name="processTeamRecord" property="lead" value="on">
							<td class="tabletext"><img src="../images/site_icon_mgmt.gif"
								width="21" height="20"></td>
						</logic:equal>
						<logic:notEqual name="processTeamRecord" property="lead"
							value="on">
							<td class="tabletext"></td>
						</logic:notEqual>
					</tr>
				</logic:iterate>
			</table>
			</td>
		</tr>
	</table>
	</td>
</tr>


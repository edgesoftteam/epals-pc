<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>

<%
	final String contextRoot = request.getContextPath();
	final String editable = request.getParameter("editable");
	final String id = request.getParameter("id");
	final String level = request.getParameter("level");
	
	
%>

<tr>
    <td colspan="3">
	    <table width="100%" border="0" cellspacing="0" cellpadding="0">
	        <tr>
	            <td>
		            <table width="100%" border="0" cellspacing="0" cellpadding="0">
		                <tr>
		                    <td width="99%" class="tabletitle"><nobr><a href="<%=contextRoot%>/listComment.do?levelId=<%=id%>&commentLevel=<%=level%>&ed=<%=editable%>" class="tabletitle">Comments</a></nobr></td>
	                      	<%if(editable.equals("true")){%>
	                        <td width="1%" class="tablebutton"><nobr><a href="<%=contextRoot%>/addComment.do?levelId=<%=id%>&commentLevel=<%=level%>" class="tablebutton">Add</a></nobr></td>
	                     <%}%>
		                </tr>
		            </table>
	            </td>
	        </tr>
	        <tr>
	            <td background="../images/site_bg_B7C1CB.jpg">
		            <table width="100%" border="0" cellspacing="1" cellpadding="2">
		                <tr valign="top">
		                    <td class="tablelabel" width="20%">Date</td>
		                    <td class="tablelabel">Comment</td>
		                </tr>
		                <logic:iterate id="comment" name="commentList" type="elms.app.common.Comment" scope="request">
		                <tr valign="top" >
		                    <td class="tabletext"><bean:write name="comment" property="displayEnterDate"/></td>
		                    <td class="tabletext"><bean:write name="comment" property="comment"/></td>
		                </tr>
		                </logic:iterate>
		            </table>
	            </td>
	        </tr>
	    </table>
    </td>
</tr>




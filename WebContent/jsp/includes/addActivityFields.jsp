<%@ taglib uri="/WEB-INF/tld/app.tld" prefix="app" %>
<%@ taglib uri="/WEB-INF/tld/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/tld/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/tld/nested-tags.tld" prefix="nested" %>
<%@ page import="elms.control.beans.ActivityForm"%>
<%@ taglib uri="/WEB-INF/tld/SecurityPermission-tag.tld" prefix="security" %>
<%
final String thisForm = request.getParameter("thisForm");
String projectNameId = (String) request.getAttribute("projectNameId")!=null ? (String) request.getAttribute("projectNameId"): "";
String wmFlag = (String)request.getAttribute("wmFlag");


ActivityForm activityForm = (ActivityForm)request.getAttribute("activityForm");

if(activityForm == null){
  activityForm =  (ActivityForm)session.getAttribute("activityForm");
}

String levelId = "";
String levelType = "";
if(activityForm != null){
System.out.println("activityForm.getActivityId(); "+activityForm.getActivityId());
  levelId = activityForm.getActivityId();
  levelType = "A";  
  
  if(levelId == null || levelId == "0"){
  System.out.println("activityForm.getSubProjectId(): "+activityForm.getSubProjectId());
    levelId = activityForm.getSubProjectId();
    levelType = "Q";
  }
}



%>
<script language="JavaScript" src="../script/calendar.js"></script>
<script language="JavaScript" src="../script/formValidations.js"></script>
<script language="JavaScript" src="../script/addActivity.js"></script>
<script>
function uncheckPeople(){
<logic:equal value="YES" property="displayPeople" name="<%=thisForm%>">
document.forms[0].ownerFromCoActivitiesSelected.checked=false;
</logic:equal>
}
function uncheckAssessor(){
<logic:equal value="YES" property="displayAssessor" name="<%=thisForm%>">
document.forms[0].ownerFromAssessorDataSelected.checked=false;
</logic:equal>
}

function changeActivityStatusForPlanCheckRequired(planCheckRequired){
var statusId;

	if(planCheckRequired.checked==true){		
		if(<%=projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_BUILDING_ID))%>){
		 	statusId = 7;
		}else if(<%=projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS_ID))%>){
			statusId = 200238;
		}
		return statusId;//PC Submitted
	}else{
		if(<%=projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_BUILDING_ID))%>){
		 	statusId = 28;
		}else if(<%=projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS_ID))%>){
			statusId = 200223;
		}
		return statusId;
	}
}

</script>
<tr>
   <td class="tablelabel">Address</td>
   <td class="tabletext"  valign="top">
       <html:select  styleClass="textbox"  property="address" >
      	   <html:option  value="">Please Select</html:option>
           <html:options  collection="addresses"  property="addressId" labelProperty="description" />
       </html:select>
   </td>
   <td class="tablelabel">Activity Status</td>
   <td class="tabletext">
	   <html:select property="activityStatus" styleClass="textbox" onchange = "if (activityStatus!=null) {checkStatus();}">
            <html:option value="">Please Select</html:option>
            <html:options collection="activityStatuses"  property="status"  labelProperty="code" />
       </html:select>					  
   </td>
</tr>
                   
<tr>
  <td class="tablelabel">Activity Type </td>
  <td class="tabletext"  valign="top">
      <html:select  styleClass="textbox"  property="activityType" onchange="reSubmit();" >
      	   <html:option value="">Please Select</html:option>
           <html:options  collection="activityTypes"  property="type"  labelProperty="description" />
      </html:select>
  </td>
  <td class="tablelabel">Sub Type </td>
  <td class="tabletext"  valign="top">
      
         <html:select  multiple="true" size="5" styleClass="textbox"  property="activitySubTypes">
      	   <html:options  collection="activitySubTypes"  property="description"  labelProperty="description" />
      </html:select>
  </td>
 
</tr>
                   
<tr>
  <td class="tablelabel" >Applied Date</td>
  <td class="tabletext"  valign="top"><nobr>
    <html:text  property="startDate" size="10"  maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr>
  </td>
  <td class="tablelabel">Issue Date</td>
  <td class="tabletext" valign="top"><nobr> 
      <html:text  property="issueDate" maxlength="10"  styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr> 
  </td>
</tr>
                    
<tr valign="top">
  <td class="tablelabel"><nobr><bean:write name="activityForm" property="completionDateLabel"/></nobr></td>
  <td class="tabletext" valign="top"><nobr>
      <html:text  property="completionDate" maxlength="10" styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr>
  </td>
 <td class="tablelabel"><nobr><bean:write name="activityForm" property="expirationDateLabel"/></nobr></td>
  <td class="tabletext" valign="top"><nobr>
      <html:text  property="expirationDate" maxlength="10" styleClass="textboxd" onfocus="showCalendarControl(this);" onkeypress="return validateDate();" onblur="DateValidate(this);"/>
    </nobr>
  </td>
</tr>

<!-- 1 is the project name id for project name of building   -->
<% if(projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_BUILDING_ID)) || projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS_ID))){ %>
<tr>

  <td class="tablelabel">Development Fee Required</td>
  <td class="tabletext" valign="top" >
      <html:checkbox property="developmentFeeRequired"/>
  </td>
  
  <td class="tablelabel">Valuation</td>
  <td class="tabletext" valign="top">
      <html:text property="valuation" size="15" styleClass="textbox" onblur="formatCurrency(this);" onkeypress="return DisplayPeriod();"/>
  </td>
</tr>

<%}%>
<% if(projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PLANNING_ID)) || projectNameId.equals(elms.util.StringUtils.i2s(elms.common.Constants.PROJECT_NAME_PUBLIC_WORKS_ID))){ %>

<tr>
 
  <td class="tablelabel">PC/Approval Required</td>
  <td class="tabletext" valign="top" colspan="3">
      <html:checkbox property="planCheckRequired" onclick="this.form.activityStatus.value=changeActivityStatusForPlanCheckRequired(this);"/> 
  </td>
</tr>
<%}else{%>
	<tr>
	 
	  <td class="tablelabel">PC/Approval Required</td>
	  <td class="tabletext" valign="top" colspan="3">
	      <html:checkbox property="planCheckRequired"/> 
	  </td>
	</tr>
<%}%>


<tr>
  <td class="tablelabel">Description</td>
  <td class="tabletext" valign="top" colspan="3">
      <html:textarea rows="4" cols="65" property="description"/>
  </td>
</tr>

<tr>
  <td class="tablelabel">Owner</td>
  <td class="tabletext" valign="top"  colspan="3">
      <html:hidden property="ownerFromPeople.peopleId"/>
      <html:hidden property="ownerFromPeople.peopleId"/>
      <logic:present name="activityForm" property="ownerFromAssessor" >
	      <html:hidden property="ownerFromAssessor.ownerId"/>
	      <html:hidden property="ownerFromAssessor.name"/>
      </logic:present>
      <logic:equal value="YES" property="displayPeople" name="<%=thisForm%>">
        <html:checkbox property="ownerFromCoActivitiesSelected" onclick="uncheckAssessor()"/> 
        <bean:write name="<%=thisForm%>" property="ownerFromPeople.name"/> (People Data) <BR>
      </logic:equal>
      <logic:equal value="YES" property="displayAssessor" name="<%=thisForm%>">
         <html:checkbox property="ownerFromAssessorDataSelected" onclick="uncheckPeople()"/> 
         <bean:write name="<%=thisForm%>" property="ownerFromAssessor.name"/> (Assessor data)
      </logic:equal>
  </td>
</tr>
<% if(wmFlag.equals("Y")){ %>
<tr>
	<td class="tablelabel">WM Required</td>
	<td class="tabletext" valign="top" colspan="3"><html:checkbox
			property="wmRequired" styleId="wmRequired" onclick="" /></td>
</tr>
<%} %>

<security:readable redLevelId="<%=levelId%>" redLevelType="<%=levelType%>" readProperty="field">

<tr>
	<td class="tablelabel">New Units</td>
	<td class="tabletext" valign="top"> 
    	<html:text  property="newUnits" maxlength="10"  styleClass="textboxd" onkeypress="" onblur=""/>    	
	</td>
	<td class="tablelabel">Existing Units</td>
	<td class="tabletext" valign="top">
    	<html:text  property="existingsUnits" maxlength="10"  styleClass="textboxd" onkeypress="" onblur=""/>    	
	</td>
	
  
</tr>
<tr>
  <td class="tablelabel">Demolished Units</td>
	<td class="tabletext" valign="top" >
    	<html:text  property="demolishedUnits" maxlength="10"  styleClass="textboxd" onkeypress="" onblur=""/>    	
	</td>
	</tr>
   </security:readable>
  
  






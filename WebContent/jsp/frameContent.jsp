<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<html:html>
<head>
<%@ page import="elms.util.db.*"%>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<html:base/>
<link rel="stylesheet" href="css/elms.css" type="text/css">

</head>
<% String message = request.getParameter("str");
if (message==null) message="";
if (message.equalsIgnoreCase("0")) { message = "No records found matching search criteria. Try again by changing criteria...";}
else message="";
%>

<body  text="#000000" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="100%" border="0" cellspacing="10" cellpadding="0">
  <tr valign="top">
    <td width="99%">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
        <td>
            <font class="red1"> <%=message %></font>  <br>  <br>
          </td>  
        </tr>
      </table>
    </td>
    </tr>
</table>
</body>
</html:html>

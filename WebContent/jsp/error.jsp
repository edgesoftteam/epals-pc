<%@page import="elms.util.MessageUtils"%>
<%@ page isErrorPage="true" %>
<%@ taglib uri="/WEB-INF/tld/struts-html.tld" prefix="html" %>
<HTML>
<HEAD><TITLE>ELMS Error Page</TITLE></HEAD>
<BODY>
<H2>ELMS Exception Information</H2>
<TABLE>
<html:html>
<html:errors/>
</html:html>


<center>
<table cellpadding="20" cellspacing="0" border="0">
	<tr>
		<td width="1" valign="top" style="font-family: Arial, Helvetica; font-size:50px; font-weight: bold" nowrap>
			500
		</td>
		<td width="400" valign="top">
			<table cellpadding="2" cellspacing="0" border="0" width="100%">
				<tr>
					<td style="font-family: Arial, Helvetica; font-size:15px">Something has gone technically wrong with your request.<br/><br/>We will try to fix it so that things can go back to normal or Kindly Re-submit the request.</td>
				</tr>
			</table>
			<br/>
			<table cellpadding="2" cellspacing="0" border="0" width="450">
				<tr align="right">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</center>


<%
String[] HEADERS_TO_TRY = { 
    "x-forwarded-for",
    "X-FORWARDED-FOR",
    "X-Forwarded-For"
    };

if(exception!=null)  {
	exception.printStackTrace();
	System.err.println(exception.getMessage());
	
	String url ="";
	try { url = (String) request.getAttribute("javax.servlet.forward.request_uri"); } catch (Exception e) { }
	StringBuilder sb = new StringBuilder();
	sb.append("URL - ").append(url);
	try { 
		sb.append("<br> <br> ");
		sb.append("Query - ").append(request.getQueryString());
		}catch (Exception e) { } 
		sb.append("<br> <br> ");
		
		
	
	
	try { 
		sb.append("User - ").append(request.getHeader("User-Agent"));
		}catch (Exception e) { } 
		sb.append("<br> <br> ");
		
		
	
	try {
		
		for (String header : HEADERS_TO_TRY) {
	        String ip = request.getHeader(header);
	        if (ip != null) {
	        	sb.append("IP - ").append(ip);
	        	
	        }
	    }
		sb.append("<br> <br> ");
		sb.append("Remote - ").append(request.getRemoteAddr());
		}catch (Exception e) { } 
	
		
	sb.append("<br> <br> ");
	
		try { 
		sb.append("Class - ").append(exception.getClass());
		}catch (Exception e) { } 
		sb.append("<br> <br> ");
		try { 
		sb.append("Exception Message - ").append(exception.getMessage());
		}catch (Exception e) { } 
		sb.append("<br> <br> ");
	
		try { 
			new MessageUtils().sendEmail("support@edgesoftinc.com", "An Error might have occured at Burbank ePALS - Auto Notify Support", sb.toString());
	
		}catch (Exception e) { } 
	
}



%>
</TABLE>
</BODY>
</HTML>
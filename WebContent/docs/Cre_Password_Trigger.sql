CREATE OR REPLACE TRIGGER PEOPLE_PASSWD_TGR
BEFORE INSERT
 ON PEOPLE
 REFERENCING OLD AS OLD NEW AS NEW
 FOR EACH ROW
DECLARE V_PASSWORD varchar2(20);
/************************************************************************
   This Trigger script was writtin by T. Lawrence as a temporary fix 
   for a non encrypted password in the PEOPLE table.  03-08-2012
************************************************************************/
BEGIN
select 'ANAND_BELAGULY' into V_PASSWORD
From dual ;
if :old.password is not null
then
:new.password:=v_password;
end if ;
END PEOPLE_PASSWD_TGR;
/
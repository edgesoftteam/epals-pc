<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	version="1.0"> 
	
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<fo:layout-master-set>
	    <fo:simple-page-master 
	             master-name="all" 
	             page-height="11.5in" 
	             page-width="8.5in" 
	             margin-top="0.4in" 
	             margin-bottom="0.3in" 
		         margin-left="0.5in" 
		         margin-right="0.5in">
			<fo:region-body 
			    margin-top="2.3in" 
			    margin-bottom="1.6in"/>
			   <fo:region-before extent="2.2in"/>
			   <fo:region-after extent="1.6in"/>
	    </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >
<!-- Header start   -->         
    <fo:static-content flow-name="xsl-region-before">
			<fo:block text-align="start" line-height="1em + 2pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="5cm"/>
				<fo:table-body>
				   <fo:table-row>
				      <fo:table-cell>
				         <fo:block>
				            <fo:external-graphic src="/f02/EPALSAPPS/XSL_IMAGES/CITYLOGO.jpg" content-height="3.25cm" content-width="3.0cm"/>
				         </fo:block>
      			      </fo:table-cell>
	 		          <fo:table-cell>
				         <fo:block text-align="start"  wrap-option="no-wrap"  font-weight="" >
				         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <fo:block ><xsl:text>&#xA;</xsl:text></fo:block>
				         <fo:block font-family="sans-serif" font-size="15pt"> City of Burbank</fo:block>
     		             <fo:block ><xsl:text>&#xA;</xsl:text></fo:block>
				         <fo:block font-family="sans-serif" font-size="13pt"> Community Development Department</fo:block>
				         <fo:block ><xsl:text>&#xA;</xsl:text></fo:block>
				         <fo:block font-family="sans-serif" font-size="13pt"> Planning and Transportation Division</fo:block>
				         <fo:block ><xsl:text>&#xA;</xsl:text></fo:block>
				         <fo:block font-family="sans-serif" font-size="11pt"> 275 East Olive Avenue </fo:block>
				         <fo:block ><xsl:text>&#xA;</xsl:text></fo:block>
				         <fo:block font-family="sans-serif" font-size="11pt"> Burbank, California 91502</fo:block>
				        </fo:block>
				      </fo:table-cell>				      
				      <fo:table-cell>
			                
				        <fo:block text-align="start" font-family="sans-serif" font-size="10pt" wrap-option="no-wrap">
				        <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				        <xsl:text> &#160; </xsl:text>
				        <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:text> &#160; </xsl:text>
				        <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:text> &#160; </xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Project No  : <xsl:value-of select="data/main/sprojnbr"/> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Activity No : <xsl:value-of select="data/main/actnbr"/> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>				           
				           Page<xsl:text> &#160; </xsl:text><fo:page-number/> of <fo:page-number-citation ref-id="last-page"/>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
					   <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Date Printed  : <xsl:value-of select="data/main/rundate"/> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
					   
						
				        </fo:block>
				      </fo:table-cell>				      
				  </fo:table-row>

				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" >
				      
				        </fo:block>
				      </fo:table-cell>	

				      <fo:table-cell text-align="center">
				        <fo:block font-family="sans-serif" font-weight="bold" font-size="15pt" >
				          <xsl:value-of select="data/main/acttype"/><xsl:text> </xsl:text>
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"> </fo:block>
				      </fo:table-cell>	
				  </fo:table-row>	
				  

		      		</fo:table-body>
		      	    </fo:table>
		      	</fo:block>    
    
    
<!-- Header end -->    
    

    </fo:static-content> 
    <fo:static-content flow-name="xsl-region-after">
<!-- footer start   -->         
<!-- Footer end -->     



    </fo:static-content> 
	
    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
		
    </fo:flow>
	</fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="data">
	<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
		
		<xsl:apply-templates select="main"/>

<!-- Applicantgrp start -->
			<fo:block  text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="15cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="applicantgrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- Applicantgrp End -->

	
	<xsl:apply-templates select="main/actdesc"/>

<!-- peoplegrp start -->
			<fo:block  space-before.optimum="5pt" text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="15cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="peoplegrp"/>
				</fo:table-body>
			    </fo:table>
			</fo:block>
<!-- peoplegrp End -->



<!-- Special Permits Begin -->
    <xsl:if test="string-length(special)>0">
	<fo:block text-align="start" line-height="1em + 2pt">
	 <fo:table table-layout="fixed">
	     <fo:table-column column-width="9.5cm"  border-color="black" border-style="solid" border-width="0.5pt"/>
	     <fo:table-column column-width="0.5cm"/>
	     <fo:table-column column-width="9.0cm" border-color="black" border-style="solid" border-width="0.5pt"/>
	     <fo:table-body>
		 <fo:table-row>
		    <fo:table-cell>
			 <fo:table table-layout="fixed">
			     <fo:table-column column-width="2.5cm"/>
			     <fo:table-column column-width="2.0cm"/>
			     <fo:table-column column-width="2.5cm"/>
			     <fo:table-column column-width="2.0cm"/>
	                     <fo:table-body>
		                 <xsl:if test="string-length(special/outdoordining)>0">
		                 <fo:table-row>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>No. of Tables: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/outdoordining/tables"/> 
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>No. of Chairs: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/outdoordining/chairs"/> 
					 </fo:block>	
                                     </fo:table-cell>
                                  </fo:table-row>

		                  <fo:table-row>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>Sq. Ft.: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/outdoordining/sft"/> 
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>Hours Open: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/outdoordining/hours"/> 
					 </fo:block>	
                                     </fo:table-cell>
                                  </fo:table-row>


		                  <fo:table-row>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>Alcohol? </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/outdoordining/alcohol"/> 
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>Council Appr? </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/outdoordining/council"/> 
					 </fo:block>	
                                     </fo:table-cell>
                                  </fo:table-row>
                                  </xsl:if>
		                 
		                 <xsl:if test="string-length(special/daycare)>0">
		                 <fo:table-row>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>No. of Kids: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell number-columns-spanned="3">
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/daycare/kids"/> 
					 </fo:block>	
                                     </fo:table-cell>
                                   </fo:table-row>
		                   
		                   <fo:table-row>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>Mon-Fri Hours: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell >
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/daycare/mfhours"/> 
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:text>Sat-Sun Hours: </xsl:text>
					 </fo:block>	
                                     </fo:table-cell>
		                     <fo:table-cell>
					 <fo:block font-family="sans-serif" font-size="9pt">
						<xsl:value-of select="special/daycare/sshours"/> 
					 </fo:block>	
                                     </fo:table-cell>
                                  </fo:table-row>
                                  </xsl:if>
 
 
 		                 <xsl:if test="string-length(special/extendedhours)>0">
               	                   <fo:table-row>
 		                     <fo:table-cell>
 					 <fo:block font-family="sans-serif" font-size="9pt">
 						<xsl:text>Mon-Fri Hours: </xsl:text>
 					 </fo:block>	
                                      </fo:table-cell>
 		                     <fo:table-cell >
 					 <fo:block font-family="sans-serif" font-size="9pt">
 						<xsl:value-of select="special/extendedhours/mfhours"/> 
 					 </fo:block>	
                                      </fo:table-cell>
 		                     <fo:table-cell>
 					 <fo:block font-family="sans-serif" font-size="9pt">
 						<xsl:text>Sat-Sun Hours: </xsl:text>
 					 </fo:block>	
                                      </fo:table-cell>
 		                     <fo:table-cell>
 					 <fo:block font-family="sans-serif" font-size="9pt">
 						<xsl:value-of select="special/extendedhours/sshours"/> 
 					 </fo:block>	
                                      </fo:table-cell>
                                   </fo:table-row>
                                 </xsl:if>
 
  		                 <xsl:if test="string-length(special/overnightstay)>0">
 		                   <fo:table-row>
  		                     <fo:table-cell>
  					 <fo:block font-family="sans-serif" font-size="9pt">
  						<xsl:text>No. of Beds: </xsl:text>
  					 </fo:block>	
                                       </fo:table-cell>
  		                     <fo:table-cell >
  					 <fo:block font-family="sans-serif" font-size="9pt">
  						<xsl:value-of select="special/overnightstay/beds"/> 
  					 </fo:block>	
                                       </fo:table-cell>
  		                     <fo:table-cell>
  					 <fo:block font-family="sans-serif" font-size="9pt">
  						<xsl:text>Sat-Sun Hours: </xsl:text>
  					 </fo:block>	
                                       </fo:table-cell>
  		                     <fo:table-cell>
  					 <fo:block font-family="sans-serif" font-size="9pt">
  						<xsl:value-of select="special/overnightstay/sshours"/> 
  					 </fo:block>	
                  </fo:table-cell>
               </fo:table-row>
               </xsl:if>
   		                  
   		       <xsl:if test="string-length(special/inlieu)>0">
  		       <fo:table-row>
   		          <fo:table-cell number-columns-spanned="2">
   					 <fo:block font-family="sans-serif" font-size="9pt">
   						<xsl:text>Parking Spaces Required: </xsl:text>
   					 </fo:block>	
                                     </fo:table-cell>
   		                     <fo:table-cell number-columns-spanned="2">
   					 <fo:block font-family="sans-serif" font-size="9pt">
   						<xsl:value-of select="special/inlieu/spacesrequired"/> 
   					 </fo:block>	
                                        </fo:table-cell>
                                   </fo:table-row>
  		                   <fo:table-row>
    		                     <fo:table-cell number-columns-spanned="2">
    					 <fo:block font-family="sans-serif" font-size="9pt">
    						<xsl:text>Parking Spaces Provided: </xsl:text>
    					 </fo:block>	
                                      </fo:table-cell>
    		                      <fo:table-cell number-columns-spanned="2">
    					 <fo:block font-family="sans-serif" font-size="9pt">
    						<xsl:value-of select="special/inlieu/spacesprovided"/> 
    					 </fo:block>	
                                      </fo:table-cell>
                                   </fo:table-row>
  		                   <fo:table-row>
    		                     <fo:table-cell number-columns-spanned="2">
    					 <fo:block font-family="sans-serif" font-size="9pt">
    						<xsl:text>Parking Spaces Available: </xsl:text>
    					 </fo:block>	
                                      </fo:table-cell>
    		                      <fo:table-cell number-columns-spanned="2">
    					 <fo:block font-family="sans-serif" font-size="9pt">
    						<xsl:value-of select="inlieu/spacesavailable"/> 
    					 </fo:block>	
                                      </fo:table-cell>
                                   </fo:table-row>
                                   </xsl:if>
                               </fo:table-body>
                         </fo:table>    
		    </fo:table-cell>
		    <fo:table-cell></fo:table-cell>
		    <fo:table-cell>
			 <fo:block font-family="sans-serif" font-size="9pt">
				<xsl:value-of select="//comments"/> 
			 </fo:block>	
		    </fo:table-cell>
		 </fo:table-row>
	     </fo:table-body>
	 </fo:table>
	</fo:block>  
       </xsl:if>


<!-- Special Permits End -->




		
<!-- This is conditionsgrp start -->
	<xsl:if test="string-length(conditionsgrp/conditions/condition)>0" >
	      <fo:block font-size="11pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		Conditions of Approval
	      </fo:block>	
	</xsl:if>
	 <fo:list-block  white-space-collapse="false" provisional-label-separation="5mm">
		<xsl:apply-templates select="conditionsgrp"/>
	 </fo:list-block>
			 
<!-- conditionsgrp  end -->				

	</fo:block>	
</xsl:template>
 
 
<xsl:template match="conditions">
				     <fo:list-item>

					 <fo:list-item-label   end-indent="label-end()">
						<fo:block font-family="sans-serif" font-size="9pt"  >
								<!--<xsl:number format="1)."/>-->
						</fo:block>					  
					 </fo:list-item-label>

					<fo:list-item-body start-indent="5mm" >
						<fo:block font-family="sans-serif" font-size="9pt"  space-after.optimum="10pt">
								<xsl:value-of select="condition"/>
						</fo:block>
					</fo:list-item-body>
				      </fo:list-item>
</xsl:template>



<xsl:template match="applicant">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Applicant </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="name"/>
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
		        
		          
</xsl:template>

<xsl:template match="main/actdesc">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Project Authorized </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main">
	<fo:block text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="10cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="3cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Project Address </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			                <xsl:value-of select="address"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Approved </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: </xsl:text>
			              <xsl:if test="string-length(issueddate)>0" >
			              	 <xsl:value-of select="substring(issueddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(issueddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(issueddate, 1,4)" />
			              </xsl:if>	
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>

			  <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Parcel Number </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:value-of select="parcelnumber"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Expires </xsl:text>
		              </fo:block>
		            </fo:table-cell>		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>: </xsl:text>
			              <xsl:if test="string-length(expiredate)>0" >
			              	 <xsl:value-of select="substring(expiredate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(expiredate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(expiredate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
			          
		</fo:table-body>
	    </fo:table>
	</fo:block>
	
	
</xsl:template>

</xsl:stylesheet>



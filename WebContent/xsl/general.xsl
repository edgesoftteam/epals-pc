<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">

	<xsl:template match="root">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="all" page-height="11.5in" page-width="8.5in" margin-top="0.4in" margin-bottom="0.3in" margin-left="0.5in" margin-right="0.5in">
					<fo:region-body margin-top="2.3in" margin-bottom="1.5in" />
					<fo:region-before extent="2.2in" />
					<fo:region-after extent="1.6in" />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="all">
				<!-- Header start   -->
				<fo:static-content flow-name="xsl-region-before">
					<fo:block text-align="start" line-height="1em + 2pt">
						<fo:table table-layout="fixed">
							<fo:table-column column-width="4cm" />
							<fo:table-column column-width="10cm" />
							<fo:table-column column-width="5cm" />
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
									 <fo:block>
											<fo:external-graphic>
												<xsl:attribute name="src">
													<xsl:text>'</xsl:text><xsl:value-of select="data/main/imgPath"/><xsl:text>/CITYLOGO.jpg'</xsl:text>
												</xsl:attribute>
												<xsl:attribute name="height">2.25cm</xsl:attribute>
												<xsl:attribute name="width">2.0cm</xsl:attribute>
											</fo:external-graphic>
							         </fo:block>
<!-- 										<fo:block> -->
<!-- 											<fo:external-graphic src="/xsl/CITYLOGO.jpg" height="3.25cm" width="3.0cm" /> -->
<!-- 										</fo:block> -->
									</fo:table-cell>
									<fo:table-cell>
										<fo:block text-align="start" font-family="sans-serif" font-size="14pt" wrap-option="no-wrap" font-weight="bold">
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

											<fo:block font-family="sans-serif" font-size="14pt">Community Development Department</fo:block>

											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

											<xsl:value-of select="data/main/department" />

											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											275 East Olive Ave.
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Burbank, Calif. 91510
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>

										<fo:block text-align="start" font-family="sans-serif" font-size="10pt" wrap-option="no-wrap">
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<xsl:text> &#160; </xsl:text>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<xsl:text> &#160; </xsl:text>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<xsl:text> &#160; </xsl:text>
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Permit No <xsl:text> &#160;  &#160; &#160; &#160; &#160; &#160; &#160; : </xsl:text>
											<fo:inline font-weight="bold" font-size="14pt">
												<xsl:value-of select="data/main/actnbr" />
											</fo:inline>

											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Permit Status <xsl:text> &#160; &#160; &#160; &#160; </xsl:text>
											<xsl:text>: </xsl:text>
											<xsl:value-of select="data/main/actstatus" />
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Plan Check Status 
											<xsl:text>: </xsl:text>
											<xsl:value-of select="data/main/pcstatus" />
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Page
											<xsl:text> &#160; </xsl:text>
											<fo:page-number />
											of
											<fo:page-number-citation ref-id="last-page" />
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											<xsl:value-of select="data/main/rundate" />

										</fo:block>
									</fo:table-cell>
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">

										</fo:block>
									</fo:table-cell>

									<fo:table-cell text-align="center">
										<fo:block font-family="sans-serif" font-weight="bold" font-size="15pt">
											<xsl:value-of select="data/main/acttype" />
											<xsl:text> Permit</xsl:text>
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">

										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<xsl:text>&#xa;</xsl:text>
								<fo:table-row>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">
										<xsl:text>&#xa;</xsl:text>   
										<xsl:text> &#160; </xsl:text>
											<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>
											<xsl:text> &#160; </xsl:text>   
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								<fo:table-row>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">
											Job Address 
				        </fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap" font-weight="bold">
											<xsl:text>: </xsl:text>
											<xsl:value-of select="data/main/address" />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell text-align="start">
										<fo:block font-family="sans-serif" font-size="9pt">
											PRE
											<xsl:text> &#160; &#160; &#160; &#160; &#160; &#160; &#160; </xsl:text>
											:
											<xsl:value-of select="data/main/planreviewer" />
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Entered By
											<xsl:text> &#160; </xsl:text>
											<xsl:text> &#160; </xsl:text>
											:
											<xsl:value-of select="data/main/enteredby" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>

							</fo:table-body>
						</fo:table>
					</fo:block>


					<!-- Header end -->


				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">

				</fo:static-content>

				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="data" />
					<!-- To get number of pages -->
					<fo:block id="last-page" />

				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:template match="data">
		<fo:block text-align="start" font-size="12pt" font-family="sans-serif">

			<xsl:apply-templates select="main" />
			
			<!-- Applicantgrp start -->
			<fo:block text-align="start">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="15cm" />
					<fo:table-body>
						<xsl:apply-templates select="applicantgrp" />
					</fo:table-body>
				</fo:table>
			</fo:block>
			<!-- Applicantgrp End -->

			<xsl:apply-templates select="main/address" />
			<xsl:apply-templates select="main/valuation" />
			<xsl:apply-templates select="main/actdesc" />

			<!-- peoplegrp start -->
			<fo:block space-before.optimum="5pt" text-align="start">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="15cm" />
					<fo:table-body>
						<xsl:apply-templates select="peoplegrp" />
					</fo:table-body>
				</fo:table>
			</fo:block>
			<!-- peoplegrp End -->

			<!-- This is feesgrp start -->
			<fo:block font-size="11pt" background-color="#C9C299" font-family="sans-serif" line-height="16pt" space-before.optimum="5pt" space-after.optimum="5pt" text-align="left" font-weight="bold">
				Fees
	      </fo:block>
			<fo:block text-align="start" space-after.optimum="-10pt">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="7cm" />
					<fo:table-column column-width="1.3cm" />
					<fo:table-column column-width="2cm" />
					<fo:table-column column-width="2cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold" text-decoration="underline">
									Fee Description
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold" text-align="right" text-decoration="underline">
									Account
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold" text-align="right" text-decoration="underline">
									Units
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold" text-align="right" text-decoration="underline">
									Fee/Units
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold" text-align="right" text-decoration="underline">
									Amount
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold" text-align="right" text-decoration="underline">
									Paid
				        </fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>

			<fo:leader leader-alignment="reference-area" leader-pattern="space" />

			<fo:block text-align="start">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="7cm" />
					<fo:table-column column-width="1.3cm" />
					<fo:table-column column-width="2cm" />
					<fo:table-column column-width="2cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-body>
						<xsl:apply-templates select="feesgrp" />
					</fo:table-body>
				</fo:table>
			</fo:block>
			<!-- feesgrp  end -->


			<!-- Plancheck, Permit Fees start -->

			<fo:block space-before.optimum="5pt" text-align="start">
				<fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="1.0pt">
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="6cm" />
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" text-align="center" font-weight="bold" background-color="#C9C299">
									<xsl:text> Plan Check </xsl:text>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" text-align="center" font-weight="bold" background-color="#C9C299">
									<xsl:text> Development Fee </xsl:text>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" text-align="center" font-weight="bold" background-color="#C9C299">
									<xsl:text> Permit </xsl:text>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" text-align="center" font-weight="bold" background-color="#C9C299">
									<xsl:text> Total </xsl:text>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>

			<fo:block text-align="start">
				<fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="6cm" />
					<fo:table-body>

						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt">
									<!-- Plan Check Fees start -->

									<fo:block text-align="start">
										<fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
											<fo:table-column column-width="2cm" />
											<fo:table-column column-width="2cm" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Fees: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(plancheckfees/fees)>0">
																	<xsl:value-of select="format-number(plancheckfees/fees, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text>   &#160;</xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Payments: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(plancheckfees/payments)>0">
																	<xsl:value-of select="format-number(plancheckfees/payments, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text>&#160;  </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
															<xsl:text> Balance Due: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(plancheckfees/balance)>0">
																	<xsl:value-of select="format-number(plancheckfees/balance, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>

											</fo:table-body>
										</fo:table>
									</fo:block>
									<!--  Plan Check Fees end -->
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt">

									<!-- Development Fees start -->
									<fo:block text-align="start">
										<fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
											<fo:table-column column-width="2cm" />
											<fo:table-column column-width="2cm" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Fees: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(developmentfees/fees)>0">
																	<xsl:value-of select="format-number(developmentfees/fees, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text>  &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160;  </xsl:text>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Payments: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(developmentfees/payments)>0">
																	<xsl:value-of select="format-number(developmentfees/payments, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text>  &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
															<xsl:text> Balance Due: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(developmentfees/balance)>0">
																	<xsl:value-of select="format-number(developmentfees/balance, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>

											</fo:table-body>
										</fo:table>
									</fo:block>


									<!-- Development Fees end -->

								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt">

									<!-- Permit Fees start -->
									<fo:block text-align="start">
										<fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
											<fo:table-column column-width="2cm" />
											<fo:table-column column-width="2cm" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Fees: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(permitfees/fees)>0">
																	<xsl:value-of select="format-number(permitfees/fees, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text>  &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160;  </xsl:text>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Payments: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(permitfees/payments)>0">
																	<xsl:value-of select="format-number(permitfees/payments, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text>  &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> &#160; </xsl:text>
															<xsl:text> Balance Due: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(permitfees/balance)>0">
																	<xsl:value-of select="format-number(permitfees/balance, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>

											</fo:table-body>
										</fo:table>
									</fo:block>


									<!-- Permit Fees end -->
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt">
									<!-- Total Fees start -->

									<fo:block text-align="start">
										<fo:table table-layout="fixed" border-color="black" border-style="solid" border-width="0.5pt">
											<fo:table-column column-width="3cm" />
											<fo:table-column column-width="3cm" />
											<fo:table-body>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Fees: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(totalfees/fees)>0">
																	<xsl:value-of select="format-number(totalfees/fees, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Adjustments: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(totalfees/adjustments)>0">
																	<xsl:value-of select="format-number(totalfees/adjustments, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Payments: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(totalfees/payments)>0">
																	<xsl:value-of select="format-number(totalfees/payments, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:text> Extend Credit: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(totalfees/extend)>0">
																	<xsl:value-of select="format-number(totalfees/extend, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>

												<fo:table-row>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
														<xsl:text> &#160; </xsl:text>
															<xsl:text> Balance Due: </xsl:text>
														</fo:block>
													</fo:table-cell>
													<fo:table-cell>
														<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
															<xsl:choose>
																<xsl:when test="string-length(totalfees/balance)>0">
																	<xsl:value-of select="format-number(totalfees/balance, '$#,##0.00 ')" />
																</xsl:when>
																<xsl:otherwise>
																	<xsl:text>$0.00 </xsl:text>
																</xsl:otherwise>
															</xsl:choose>
														</fo:block>
													</fo:table-cell>
												</fo:table-row>
												<fo:table-row>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">
										<xsl:text>&#xa;</xsl:text>   
										<xsl:text> &#160; </xsl:text>
											<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>
											<xsl:text> &#160; </xsl:text>   
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
											</fo:table-body>
										</fo:table>
									</fo:block>


									<!-- Total Fees end -->
								</fo:block>
							</fo:table-cell>
						</fo:table-row>


					</fo:table-body>
				</fo:table>
			</fo:block>

			<!-- Plan chk, Permit Fees end -->


			<!-- This is ledgergrp start -->
			<fo:block font-size="11pt" background-color="#C9C299" font-family="sans-serif" line-height="16pt" space-before.optimum="5pt" space-after.optimum="5pt" text-align="left" font-weight="bold">

			</fo:block>
			<fo:block text-align="start" space-after.optimum="-10pt">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="3cm" />
					<fo:table-column column-width="7cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">
									Date
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">
									Transaction Type
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">
									Method
				        </fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">
									Amount
				        </fo:block>
							</fo:table-cell>

						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>

			<fo:leader leader-alignment="reference-area" leader-pattern="rule" />

			<fo:block text-align="start">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="3cm" />
					<fo:table-column column-width="7cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-body>
						<xsl:apply-templates select="ledgergrp" />
					</fo:table-body>
				</fo:table>
			</fo:block>
			<!-- ledgergrp  end -->

			<!-- This is conditionsgrp start -->
			<xsl:if test="string-length(conditionsgrp/conditions/condition)>0">
				<fo:block font-size="11pt" background-color="#C9C299" font-family="sans-serif" line-height="16pt" space-before.optimum="5pt" space-after.optimum="5pt" text-align="left" font-weight="bold">
					Conditions
	      </fo:block>
			</xsl:if>
			<fo:list-block white-space-collapse="false" provisional-label-separation="5mm">
				<xsl:apply-templates select="conditionsgrp" />
			</fo:list-block>

			<!-- conditionsgrp  end -->

			<!-- This is commentsgrp start -->
			<xsl:if test="string-length(commentsgrp/comments/comments)>0">
				<fo:block font-size="11pt" background-color="#C9C299" font-family="sans-serif" line-height="16pt" space-before.optimum="5pt" space-after.optimum="5pt" text-align="left" font-weight="bold">
					Comments
	      </fo:block>
			</xsl:if>
			<fo:block text-align="start" white-space-collapse="false">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="18cm" />
					<fo:table-body>
						<xsl:apply-templates select="commentsgrp" />
					</fo:table-body>
				</fo:table>
			</fo:block>
			<!-- commentsgrp  end -->


		</fo:block>
	</xsl:template>

	<xsl:template match="fees">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="feedesc" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					<xsl:if test="account &gt; 0">
						<xsl:value-of select="format-number(account,'#####')" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					<xsl:if test="units &gt; 0">
						<xsl:value-of select="format-number(units,'###,###.##')" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					<xsl:if test="feeperunits &gt; 0">
						<xsl:value-of select="format-number(feeperunits, '$##,##0.00 ')" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					<xsl:if test="amount &gt; 0">
						<xsl:value-of select="format-number(amount, '$###,##0.00 ')" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt" text-align="right">
					<xsl:if test="paid &gt; 0">
						<xsl:value-of select="format-number(paid, '$###,##0.00 ')" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template match="ledger">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:if test="string-length(pymntdate)>0">
						<xsl:value-of select="substring(pymntdate,6,2)" />
						<xsl:text>/</xsl:text>
						<xsl:value-of select="substring(pymntdate,9,2)" />
						<xsl:text>/</xsl:text>
						<xsl:value-of select="substring(pymntdate, 1,4)" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="pymntdesc" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="pymntmethod" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:if test="string-length(pymntamnt)>0">
						<xsl:value-of select="format-number(pymntamnt, '$#,##0.00 ')" />
					</xsl:if>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template match="conditions">
		<fo:list-item>

			<fo:list-item-label end-indent="label-end()">
				<fo:block font-family="sans-serif" font-size="9pt">
					<!--<xsl:number format="1)."/>-->
				</fo:block>
			</fo:list-item-label>

			<fo:list-item-body start-indent="5mm">
				<fo:block font-family="sans-serif" font-size="9pt" space-after.optimum="10pt">
					<xsl:value-of select="condition" />
				</fo:block>
			</fo:list-item-body>
		</fo:list-item>
	</xsl:template>

	<xsl:template match="comments">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="comments" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>

	<xsl:template match="owner">
		<fo:block space-before.optimum="5pt" text-align="start">
			<fo:table table-layout="fixed">
				<fo:table-column column-width="4cm" />
				<fo:table-column column-width="15cm" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Owner </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:value-of select="name" />
								<xsl:value-of select="address" />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>

	<!-- BEGIN TEMPLATE  PEOPLE-->
	<xsl:template match="people">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="peopletype" />
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="name" />
					<xsl:text>  &#160; </xsl:text>
					<xsl:text>  Lic. </xsl:text>
					<xsl:value-of select="licno" />
					<xsl:text>  &#160; </xsl:text>
					<xsl:text>  &#160; </xsl:text>
					<xsl:text>(</xsl:text>
					<xsl:value-of select="substring(phone, 1, 3)" />
					<xsl:text>)</xsl:text>
					<xsl:value-of select="substring(phone, 4, 3)" />
					<xsl:text>-</xsl:text>
					<xsl:value-of select="substring(phone, 7, 4)" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:text> </xsl:text>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="addr" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:text> </xsl:text>
				</fo:block>
			</fo:table-cell>
			<!--
	     <fo:table-cell>
	         <fo:block font-family="sans-serif" font-size="9pt">
	             <xsl:value-of select="comments"/>
	         </fo:block>
	     </fo:table-cell>
		 -->
		</fo:table-row>
	</xsl:template>
	<!-- END TEMPLATE PEOPLE-->

	<xsl:template match="applicant">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:text>Applicant </xsl:text>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="name" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:text>Applicant Address </xsl:text>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="addr" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>

		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:text>Applicant Phone </xsl:text>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:text>(</xsl:text>
					<xsl:value-of select="substring(phone, 1, 3)" />
					<xsl:text>)</xsl:text>
					<xsl:value-of select="substring(phone, 4, 3)" />
					<xsl:text>-</xsl:text>
					<xsl:value-of select="substring(phone, 7, 4)" />
				</fo:block>
			</fo:table-cell>
		</fo:table-row>

	</xsl:template>
	<xsl:template match="main/address">
		<fo:block space-before.optimum="5pt" text-align="start">
			<fo:table table-layout="fixed">
				<fo:table-column column-width="4cm" />
				<fo:table-column column-width="15cm" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Base Address </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:value-of select="." />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<xsl:template match="main/valuation">
		<fo:block space-before.optimum="5pt" text-align="start">
			<fo:table table-layout="fixed">
				<fo:table-column column-width="4cm" />
				<fo:table-column column-width="15cm" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Valuation </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:if test="string-length(.)>0">
									<xsl:value-of select="format-number(., '$#,##0.00 ')" />
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>
	<xsl:template match="main/actdesc">
		<fo:block space-before.optimum="5pt" text-align="start">
			<fo:table table-layout="fixed">
				<fo:table-column column-width="4cm" />
				<fo:table-column column-width="15cm" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Job Description </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:value-of select="." />
							</fo:block>
						</fo:table-cell>
					</fo:table-row>
				</fo:table-body>
			</fo:table>
		</fo:block>
	</xsl:template>

	<xsl:template match="main">
		<fo:block text-align="start">
			<fo:table table-layout="fixed">
				<fo:table-column column-width="4cm" />
				<fo:table-column column-width="10cm" />
				<fo:table-column column-width="2cm" />
				<fo:table-column column-width="3cm" />
				<fo:table-body>
					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Project No </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
							     <xsl:text>: </xsl:text>
								 <xsl:value-of select="prjnbr" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Applied </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:if test="string-length(applieddate)>0">
									<xsl:value-of select="substring(applieddate,6,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(applieddate,9,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(applieddate, 1,4)" />
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Activity Type </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
							    <xsl:text>: </xsl:text>
								<xsl:value-of select="acttype" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Issued </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:if test="string-length(issueddate)>0">
									<xsl:value-of select="substring(issueddate,6,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(issueddate,9,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(issueddate, 1,4)" />
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>


					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Project Name </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
							    <xsl:text>: </xsl:text>
								<xsl:value-of select="prjname" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Completed </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:if test="string-length(completeddate)>0">
									<xsl:value-of select="substring(completeddate,6,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(completeddate,9,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(completeddate, 1,4)" />
								</xsl:if>

							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Parcel Number </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:value-of select="parcelnumber" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>To Expire </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:if test="string-length(expiredate)>0">
									<xsl:value-of select="substring(expiredate,6,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(expiredate,9,2)" />
									<xsl:text>/</xsl:text>
									<xsl:value-of select="substring(expiredate, 1,4)" />
								</xsl:if>
							</fo:block>
						</fo:table-cell>
					</fo:table-row>

					<fo:table-row>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>Project Description </xsl:text>
							</fo:block>
						</fo:table-cell>
						<fo:table-cell>
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text>: </xsl:text>
								<xsl:value-of select="prjdescription" />
							</fo:block>
						</fo:table-cell>
						<fo:table-cell number-columns-spanned="2">
							<fo:block font-family="sans-serif" font-size="9pt">
								<xsl:text> &#160; </xsl:text>
							</fo:block>
						</fo:table-cell>

					</fo:table-row>

				</fo:table-body>
			</fo:table>
		</fo:block>

	</xsl:template>


</xsl:stylesheet>

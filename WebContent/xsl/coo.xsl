<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <fo:layout-master-set>
        <fo:simple-page-master master-name="all-pages" page-height="21cm" page-width="29.7cm">
          <fo:region-body region-name="xsl-region-body" column-gap="0.25in" padding-top="6pt"
            padding-left="6pt" padding-right="6pt" padding-bottom="6pt" margin-top="0.3in"
            margin-left="0.7in" margin-right="0.7in" margin-bottom="0.1in"/>
          <fo:region-before region-name="xsl-region-before" display-align="after" extent="0.3in"
            padding-top="6pt" padding-left="0.7in" padding-right="0.7in" padding-bottom="6pt" />
          <fo:region-after region-name="xsl-region-after" display-align="before" extent="0.3in"
            padding-top="6pt" padding-left="0.7in" padding-right="0.7in" padding-bottom="6pt"/>
        </fo:simple-page-master>
        <fo:page-sequence-master master-name="default-sequence">
          <fo:repeatable-page-master-reference master-reference="all-pages"/>
        </fo:page-sequence-master>
      </fo:layout-master-set>
      <fo:page-sequence master-reference="default-sequence">
        <fo:flow flow-name="xsl-region-body" font-size="10pt" font-family="Arial">
          
          <xsl:for-each select="/root/data/coo">          
          <fo:table border-collapse="collapse">
            <fo:table-column column-number="1"/>
            <fo:table-column  column-number="2"/>
            <fo:table-column  column-number="3"/>
            <fo:table-body>
              <fo:table-row>
                <fo:table-cell  number-columns-spanned="3">
                  <fo:block color="rgb(0,0,0)"  text-align="center" font-size="24pt" font-weight="bold"> CITY OF BURBANK </fo:block>
                  <fo:block color="rgb(0,0,0)"  text-align="center" font-size="18pt" font-weight="bold"> CDD BUILDING DIVISION</fo:block>
                  <fo:block color="rgb(0,0,0)" font-weight="bold" text-align="center">
                    <fo:external-graphic src="/mnt/data/EPALS/XSL_IMAGES/Burbank_Logo_Trans_GIF.gif" content-width="3cm"
                      content-height="3cm" width="3cm" height="3cm"/>
                  </fo:block>
                  <fo:block color="rgb(0,0,0)" font-size="54pt" text-align="center" font-weight="bold"> Certificate of Occupancy </fo:block>
                  <fo:block color="rgb(0,0,0)" font-size="12pt" text-align="center"> 
                  This structure has been inspected for compliance with the requirements of the California Building Codes and the
                  Burbank Municipal Code for the specified occupancy and use.
                  </fo:block>
                  <fo:block color="rgb(0,0,0)" font-size="12pt"  font-weight="bold" text-align="center" space-before.optimum="5pt" space-after.optimum="8pt"> 
                    <xsl:value-of select="//coo/activitycondition"/>
                  </fo:block>
                  <fo:block color="rgb(0,0,0)" font-size="30pt" text-align="center" space-after.optimum="5pt">
                    <xsl:value-of select="//coo/projaddress"/>
                  </fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row>
                <fo:table-cell  number-columns-spanned="3">
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt"  space-after.optimum="10pt"> Project Description :   <xsl:value-of select="//coo/projdesc"/></fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row display-align="before">
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >APN :   <fo:inline font-weight="bold"><xsl:value-of select="//coo/apn"/> </fo:inline></fo:block>
                </fo:table-cell>
                <fo:table-cell>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >Permit Number :  <fo:inline font-weight="bold">  <xsl:value-of select="//coo/permitnumber"/> </fo:inline></fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row display-align="before">
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" > Occupancy Group:  <fo:inline font-weight="bold"><xsl:value-of select="//coo/occgrp"/> </fo:inline></fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >Stories :   <fo:inline font-weight="bold"><xsl:value-of select="//coo/stories"/> </fo:inline></fo:block>                  
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >Building Area (SF):   <fo:inline font-weight="bold"><xsl:value-of select="//coo/bldgarea"/> </fo:inline></fo:block>
                </fo:table-cell>
              </fo:table-row>

              <fo:table-row display-align="before">
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >Construction Type:  <fo:inline font-weight="bold"><xsl:value-of select="//coo/consttype"/> </fo:inline></fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt"  >Number of Dwelling Units:  <fo:inline font-weight="bold"><xsl:value-of select="//coo/noofdwellingunits"/> </fo:inline></fo:block>                  
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >Garage Area (SF):  <fo:inline font-weight="bold"><xsl:value-of select="//coo/garagearea"/> </fo:inline></fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row display-align="before">
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt" >Sprinklers:  <fo:inline font-weight="bold"><xsl:value-of select="//coo/sprinklers"/> </fo:inline></fo:block>
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt"  >Parking Spaces:   <fo:inline font-weight="bold"><xsl:value-of select="//coo/parkingspaces"/> </fo:inline></fo:block>                  
                </fo:table-cell>
                <fo:table-cell>
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="14pt"  >Basement Area (SF):   <fo:inline font-weight="bold"><xsl:value-of select="//coo/basementarea"/> </fo:inline></fo:block>
                </fo:table-cell>
              </fo:table-row>
              
              <fo:table-row display-align="before">
                <fo:table-cell number-columns-spanned="3">
                  <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" space-after.optimum="25pt" space-before.optimum="10pt"> <xsl:value-of select="//coo/specialconditions"/></fo:block>
                </fo:table-cell>
              </fo:table-row>
              
            </fo:table-body>
          </fo:table>
            <!-- Add owner details here -->

            <fo:table border-collapse="collapse">
              <fo:table-column  column-number="1"/>
              <fo:table-column  column-number="2"/>
              <fo:table-body>
                <fo:table-row display-align="before">
                  <fo:table-cell>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" space-after.optimum="5pt" >______________________________________</fo:block>
                  </fo:table-cell>
                </fo:table-row>
                
                <fo:table-row display-align="before">
                  <fo:table-cell>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" >Owner:  <fo:inline font-weight="bold"><xsl:value-of select="//coo/owner/name"/> </fo:inline></fo:block>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt"  start-indent="14mm" >  <fo:inline font-weight="bold"><xsl:value-of select="//coo/owner/address"/> </fo:inline></fo:block>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" start-indent="14mm" >  <fo:inline font-weight="bold"><xsl:value-of select="//coo/owner/citystatezip"/> </fo:inline></fo:block>
                  </fo:table-cell>
                  <fo:table-cell>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" >Inspector :  <fo:inline font-weight="bold"><xsl:value-of select="//coo/inspector"/> </fo:inline></fo:block>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" >Building Official:   <fo:inline font-weight="bold"><xsl:value-of select="//coo/bldgofficial"/> </fo:inline></fo:block>
                    <fo:block color="rgb(0,0,0)"  text-align="left" font-size="12pt" > Date :  <fo:inline font-weight="bold"><xsl:value-of select="//coo/coodate"/> </fo:inline></fo:block>
                  </fo:table-cell>
                </fo:table-row>                
                
              </fo:table-body>
           </fo:table>
          </xsl:for-each>
          
        </fo:flow>
 
      </fo:page-sequence>
    </fo:root>

  </xsl:template>

</xsl:stylesheet>

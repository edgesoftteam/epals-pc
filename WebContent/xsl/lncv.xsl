<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0">

	<xsl:template match="root">
		<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
			<fo:layout-master-set>
				<fo:simple-page-master master-name="all" page-height="11.5in" page-width="8.5in" margin-top="0.4in" margin-bottom="0.3in" margin-left="0.5in" margin-right="0.5in">
					<fo:region-body margin-top="1.8in" margin-bottom="1.5in" />
					<fo:region-before extent="2.2in" />
					<fo:region-after extent="1.6in" />
				</fo:simple-page-master>
			</fo:layout-master-set>
			<fo:page-sequence master-reference="all">
				<!-- Header start   -->
				<fo:static-content flow-name="xsl-region-before">
					<fo:block text-align="start">
						<fo:table table-layout="fixed">
							<fo:table-column column-width="4cm" />
							<fo:table-column column-width="10cm" />
							<fo:table-column column-width="15cm" />
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell>
										<fo:block>
											<fo:external-graphic src="/f02/EPALSAPPS/XSL_IMAGES/CITYLOGO.jpg" height="3.25cm" width="3.0cm" />
										</fo:block>
									</fo:table-cell>
									<fo:table-cell>
										<fo:block text-align="start" font-family="sans-serif" font-size="14pt" wrap-option="no-wrap" font-weight="bold">
											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

											<fo:block font-family="sans-serif" font-size="14pt">LNCV Permit 2011</fo:block>

											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>

										

											<fo:block>
												<xsl:text>&#xA;</xsl:text>
											</fo:block>
											Large Non-Commercial Vehicle Parking Permit
											
										
										</fo:block>
									</fo:table-cell>
									
								</fo:table-row>

								<fo:table-row>
									<fo:table-cell>
										<fo:block font-family="sans-serif" font-size="9pt">
											<xsl:text>&#xA;</xsl:text>
										</fo:block>
									</fo:table-cell>

									<fo:table-cell text-align="center">
										<fo:block font-family="sans-serif" font-weight="bold" font-size="15pt">
											<xsl:text> Vehicle Id</xsl:text>
											<xsl:value-of select="data/main/vehicleNo" />
										</fo:block>
									</fo:table-cell>
									
									<fo:table-cell>
									<fo:block font-family="sans-serif" font-weight="bold" font-size="15pt">
											Permit #
											<xsl:value-of select="data/main/actnbr" />
										</fo:block>
									</fo:table-cell>
								</fo:table-row>
								
	
							</fo:table-body>
						</fo:table>
					</fo:block>


					<!-- Header end -->


				</fo:static-content>
				<fo:static-content flow-name="xsl-region-after">
					
				
					

				</fo:static-content>

				<fo:flow flow-name="xsl-region-body">
					<xsl:apply-templates select="data" />
					<!-- To get number of pages -->
					<fo:block id="last-page" />
					
					
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>
	

	
	<xsl:template match="data">
		<fo:block text-align="start" font-size="12pt" font-family="sans-serif">

				<!-- This is ledgergrp start -->
			<fo:block font-size="11pt" background-color="#C9C299" font-family="sans-serif" line-height="16pt" space-before.optimum="5pt" space-after.optimum="5pt" text-align="left" font-weight="bold">
					
			</fo:block>
			<fo:block text-align="start" space-after.optimum="-10pt">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="3cm" />
					<fo:table-column column-width="7cm" />
					<fo:table-column column-width="4cm" />
					<fo:table-column column-width="3cm" />
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block font-family="sans-serif" font-size="9pt" font-weight="bold">
									Dates Valid
				        </fo:block>
							</fo:table-cell>
					   </fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>

			<fo:leader leader-alignment="reference-area" leader-pattern="rule" />

			<fo:block text-align="start">
				<fo:table table-layout="fixed">
					<fo:table-column column-width="4cm" />
					<fo:table-body>
						<xsl:apply-templates select="datesgrp" />
					</fo:table-body>
				</fo:table>
			</fo:block>
			<!-- ledgergrp  end -->

		</fo:block>
		
		<fo:block text-align="center">
						<fo:table table-layout="fixed">
							<fo:table-column column-width="20cm" />
							<fo:table-body>
								<fo:table-row>
									<fo:table-cell text-align="bottom">
										<fo:block font-family="sans-serif" font-size="9pt">
												No person shall park any Large Non-Commercial Vehicle within eighty (80) feet of any intersection of two public streets.
										</fo:block>		
										<fo:block>
												<xsl:text>&#xA;</xsl:text>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="9pt">
											Display of Permits: LNCV permits shall be visible to parking enforcement officers on the street-side of the LNCV in either
										</fo:block>
										<fo:block>
												<xsl:text>&#xA;</xsl:text>
										</fo:block>
										<fo:block font-family="sans-serif" font-size="9pt">
										(1) the front window on the driver’s side or, if not applicable,<xsl:text>&#xA;</xsl:text>
										(2) the window closest to the front of the LNCV.
										</fo:block>						
									</fo:table-cell>												
								</fo:table-row>
							</fo:table-body>
						</fo:table>
					</fo:block>

		
	</xsl:template>

								

<xsl:template match="date">
		<fo:table-row>
			<fo:table-cell>
				<fo:block font-family="sans-serif" font-size="9pt">
					<xsl:value-of select="lncvmonth" /> -- <xsl:value-of select="lncvdate" />
				</fo:block>
			</fo:table-cell>
			
		</fo:table-row>
</xsl:template>

</xsl:stylesheet>




				
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" version="1.0"> 
	
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<fo:layout-master-set>
	    <fo:simple-page-master 
	             master-name="all" 
	             page-height="11.5in" 
	             page-width="8.5in" 
	             margin-top="0.9in" 
	             margin-bottom="0.3in" 
		         margin-left="0.5in" 
		         margin-right="0.5in">
			<fo:region-body 
			    margin-top="2.5in" 
			    margin-bottom="1.5in"/>
			   <fo:region-before extent="2.8in"/>
			   <fo:region-after extent="1.6in"/>
	    </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >
	
	
	
<!-- Header start   -->         
    <fo:static-content flow-name="xsl-region-before">
			<fo:block text-align="start" line-height="1em + 6pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="5cm"/> 
				<fo:table-body>
				   <fo:table-row>
				      <fo:table-cell>
				         <fo:block>
           					<fo:external-graphic>
		                    <xsl:attribute name="src">
			                     <xsl:text>'</xsl:text><xsl:value-of select="data/main/imgPath"/><xsl:text>/CITYLOGO.jpg'</xsl:text>
			                    </xsl:attribute>
			                    <xsl:attribute name="height">3.15cm</xsl:attribute>
			                    <xsl:attribute name="width">2.8cm</xsl:attribute>
		                    </fo:external-graphic>
          				</fo:block>
      			      </fo:table-cell>
	 		          <fo:table-cell>
				         <fo:block text-align="center" font-family="sans-serif" font-size="15pt" wrap-option="no-wrap"  font-weight="bold" >
					         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
					         CITY OF BURBANK
					         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
					         COMMUNITY DEVELOPMENT DEPARTMENT
					         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         </fo:block>
				         <fo:block text-align="center" font-family="sans-serif" font-size="13pt" wrap-option="no-wrap" font-weight="bold"><xsl:text>&#xA;</xsl:text>
				         	 License and Code Services Division
				             <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				             150 North Third St, Burbank, CA 91502 
				             <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				             Mailing Address: P.O. Box 6459, Burbank, CA 91510 
				             <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				             (818)238-5280
				        </fo:block>
				        <fo:block text-align="center" font-family="sans-serif">
				        	<fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         	<xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					         	&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					         	&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					         	&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				         	</xsl:text>
							<fo:block text-align="center" font-family="sans-serif" font-size="13pt" wrap-option="no-wrap" font-weight="bold">
				          	RECEIPT FOR PAYMENT OF BUSINESS LICENSE OR PERMIT
				        	</fo:block>
				        </fo:block>
				     </fo:table-cell>				      
				  </fo:table-row>
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="10pt"></fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="10pt"></fo:block>
				      </fo:table-cell>	
				  </fo:table-row>	
			</fo:table-body>
		  </fo:table>
		</fo:block>   
		 
		 <fo:table table-layout="fixed" >
			<fo:table-column column-width="16cm" />
				<fo:table-body>
					<fo:table-row>
				    	<fo:table-cell>
				  		  <fo:block text-align="left" wrap-option="wrap" font-family="sans-serif">
				        	<fo:block text-align="left" font-family="sans-serif" font-size="10pt" wrap-option="wrap" font-weight="bold">
				        	    This document is a Receipt for Payment for an application or renewal of a Business License or the Permit and is not to be 
					        	considered as proof of an issued valid City of Burbank Business License or Permit.</fo:block>
				          </fo:block>
				        </fo:table-cell>				      
				   	</fo:table-row>   
				 </fo:table-body>     
		</fo:table>
	</fo:static-content> 
<!-- Header end -->     
	
    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
	</fo:flow>
   </fo:page-sequence>
 </fo:root>
</xsl:template>

<xsl:template match="data">
	<fo:block text-align="start" font-size="12pt" font-weight="bold" font-family="sans-serif">
		
		<xsl:apply-templates select="main"/>
	 	<xsl:apply-templates select="main/address"/>
		<xsl:apply-templates select="main/valuation"/>
		<xsl:apply-templates select="main/actdesc"/>


<!-- This is feesgrp start -->
	      <fo:block font-size="11pt"  background-color="#D8D8D8"
		    font-family="sans-serif" 
		    line-height="13pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		FEES
	      </fo:block>	
			<fo:block text-align="start" space-after.optimum="-10pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="8cm"/>
				<fo:table-column column-width="3cm"/>
				<fo:table-column column-width="2cm"/>
				<fo:table-body>
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="12pt" font-weight="bold">
				        Fee Description
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="12pt" font-weight="bold" text-align="left">
				        Amount
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" text-align="right">
				        Paid
				        </fo:block>
				      </fo:table-cell>
				  </fo:table-row>
		      	</fo:table-body>
		      </fo:table>
		  </fo:block>

		<fo:leader leader-alignment="reference-area"/>
			
			<fo:block text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="8cm"/>
				 <fo:table-column column-width="1.5cm"/>
				 <fo:table-column column-width="4cm"/>
				 
				 <fo:table-body>
					<xsl:apply-templates select="feesgrp"/>
				</fo:table-body>
			   </fo:table>
			</fo:block>
				<fo:block text-align="start" space-after.optimum="-10pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="8cm"/>
				<fo:table-column column-width="1.5cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-body>
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        Total
				        </fo:block>
				      </fo:table-cell>	
				       <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" text-align="right" font-weight="bold" line-height="1em + 8pt">
				        <xsl:choose>
						<xsl:when test="string-length(totalfees/fees)>0" >
						      <xsl:value-of select="format-number(totalfees/fees, '$#,##0.00 ')"/> 
						</xsl:when>
						<xsl:otherwise> 
						   <xsl:text>$0.00 </xsl:text> 
						</xsl:otherwise>
						</xsl:choose> 
				        </fo:block>
				      </fo:table-cell>	
				      <fo:table-cell>
					      <fo:block font-family="sans-serif" font-size="11pt" text-align="right" font-weight="bold" line-height="1em + 8pt">
					      	<xsl:choose>
								<xsl:when test="string-length(totalfees/payments)>0" >
							     	<xsl:value-of select="format-number(totalfees/payments, '$#,##0.00 ')"/> 
						        </xsl:when>
							    <xsl:otherwise> 
								   <xsl:text>$0.00 </xsl:text> 
							    </xsl:otherwise>
							</xsl:choose>
					      </fo:block>
					  </fo:table-cell>
		  		</fo:table-row>
		      </fo:table-body>
		     </fo:table>
		   </fo:block>
<!-- feesgrp  end -->

	
<!-- This is ledgergrp start -->
	      <fo:block font-size="13pt"  background-color="#C9C299"
		    font-family="sans-serif" 
		    line-height="16pt"
		    space-before.optimum="5pt"
		    space-after.optimum="5pt"
		    text-align="left"
		    font-weight="bold" >
		  </fo:block>
		  <fo:block text-align="start" space-after.optimum="-10pt">
			<fo:table table-layout="fixed" >
			  <fo:table-column column-width="19cm"/>
				<fo:table-body>
					<fo:table-row>
				      <fo:table-cell>
				   	      <fo:block font-size="11pt"  background-color="#D8D8D8"
		    				font-family="sans-serif" 
		    				line-height="13pt"
		    				space-before.optimum="5pt"
		    				space-after.optimum="5pt"
		    				text-align="left"
		    				font-weight="bold" >
							FOR OFFICE USE ONLY
	      				  </fo:block>
	      				</fo:table-cell>
				      </fo:table-row>
				     </fo:table-body>
				  </fo:table>	      
				      
				      
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="6cm"/>
				<fo:table-column column-width="6cm"/>
				<fo:table-column column-width="4cm"/>
				<fo:table-body>
					<fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        Date
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        Transaction Type
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        Method of Payment
				        </fo:block>
				      </fo:table-cell>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        Amount
				        </fo:block>
				      </fo:table-cell>
				    </fo:table-row>
		      	</fo:table-body>
		      </fo:table>
		  </fo:block>
		  
		  

		<fo:leader leader-alignment="reference-area"/>
			<fo:block text-align="start" >
			    <fo:table table-layout="fixed" >
				 <fo:table-column column-width="4cm"/>
				 <fo:table-column column-width="6cm"/>
				 <fo:table-column column-width="6cm"/>
				 <fo:table-column column-width="3cm"/>
				 <fo:table-body>
					<xsl:apply-templates select="ledgergrp"/>
				</fo:table-body>
			   </fo:table>
			</fo:block>
		<!-- ledgergrp  end -->		
</fo:block>	

<!-- Business License Activity No Starts -->	
<fo:block text-align="start" line-height="1em + 6pt">
	<fo:table table-layout="fixed" >
		<fo:table-column column-width="4cm"/>
		<fo:table-column column-width="6cm"/>
			<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        	BL Activity Number:
						</fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        	<xsl:value-of select="main/actnbr"/> 
						</fo:block>
				    </fo:table-cell>
				 </fo:table-row>
	      </fo:table-body>
	</fo:table>
</fo:block>
<!-- Business License Activity No Ends -->	

<!-- Entered By Starts -->	
<fo:block text-align="start" line-height="1em + 6pt">
	<fo:table table-layout="fixed" >
		<fo:table-column column-width="2.7cm"/>
		<fo:table-column column-width="2cm"/>
		<fo:table-body>
				<fo:table-row>
					<fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        	Entered By:
						</fo:block>
				    </fo:table-cell>
				    <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
				        	<xsl:value-of select="main/enteredby"/> 
						</fo:block>
				    </fo:table-cell>
				 </fo:table-row>
	      </fo:table-body>
	</fo:table>
</fo:block>
<!-- Entered By Ends -->	

		 
</xsl:template>
 
<xsl:template match="fees">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" line-height="1em + 8pt">
			              <xsl:value-of select="feedesc"/>
		              </fo:block>
		            </fo:table-cell>
		            
		            
		            		            
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" text-align="right" line-height="1em + 8pt">
		              	<xsl:if test="amount &gt; 0" >
		           	   <xsl:value-of select="format-number(amount, '$###,##0.00 ')"/> 
		           	</xsl:if>   
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" text-align="right" line-height="1em + 8pt">
		              	<xsl:if test="paid &gt; 0" >
			                <xsl:value-of select="format-number(paid, '$###,##0.00 ')"/> 
			        </xsl:if>        
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
</xsl:template>

<xsl:template match="ledger">
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" line-height="1em + 8pt">
			              <xsl:if test="string-length(pymntdate)>0" >
			              	 <xsl:value-of select="substring(pymntdate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(pymntdate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(pymntdate, 1,4)" />
			              </xsl:if>				              
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" line-height="1em + 8pt">
			              <xsl:value-of select="pymntdesc"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" line-height="1em + 8pt">
			              <xsl:value-of select="pymntmethod"/>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" line-height="1em + 8pt">
		              	<xsl:if test="string-length(pymntamnt)>0" >
			              <xsl:value-of select="format-number(pymntamnt, '$#,##0.00 ')"/>   
			        </xsl:if>      
		              </fo:block>
		            </fo:table-cell>	
		        </fo:table-row>  
</xsl:template>
 


<xsl:template match="main/address">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		
	    </fo:table>
	</fo:block>		          
</xsl:template>
<xsl:template match="main/valuation">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 
	    </fo:table>
	</fo:block>		          
</xsl:template>


<xsl:template match="main">
	<fo:block text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="7.5cm"/>
		 <fo:table-column column-width="11cm"/>
		 		 <fo:table-body>
		 		  <fo:table-row>
		            <fo:table-cell>
		            <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
		            <xsl:text> &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					         	&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					         	&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
					         	&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;
				         	</xsl:text>
		            </fo:block>
		                    </fo:table-cell>  
		                     </fo:table-row>
		        <fo:table-row>
		            <fo:table-cell>
		                
		            <fo:block font-family="sans-serif" font-size="11pt" wrap-option="no-wrap" font-weight="bold" >
		              	Business Name: 
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		                 <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" >
		              	
			                <xsl:value-of select="businessName"/>
			               
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold" line-height="1em + 8pt">
			              <xsl:text>Business Address: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt"  font-weight="bold" line-height="1em + 8pt">
			               <xsl:value-of select="address"/>
		              </fo:block>
		            </fo:table-cell>
		       </fo:table-row>
   			   <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" wrap-option="no-wrap" font-weight="bold" line-height="1em + 8pt">
			              <xsl:text>Business Account Number: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt"  font-weight="bold" line-height="1em + 8pt">
			               <xsl:value-of select="businessAccNo"/>
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt"  font-weight="bold" line-height="1em + 8pt">
			              <xsl:text>Type of Business: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt"  font-weight="bold" line-height="1em + 8pt">
			                <xsl:value-of select="acttype"/>
		              </fo:block>
		            </fo:table-cell>
		         </fo:table-row>
			     <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt"  font-weight="bold">
			              <xsl:text>Permit or License Status: </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="11pt" font-weight="bold">
			               <xsl:value-of select="actstatus"/> 
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell number-columns-spanned="2">
		              <fo:block font-family="sans-serif" font-size="11pt"  font-weight="bold">
			              <xsl:text> &#160; </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		          </fo:table-row>
        	</fo:table-body>
	    </fo:table>
	</fo:block>
	
	
</xsl:template>
</xsl:stylesheet>



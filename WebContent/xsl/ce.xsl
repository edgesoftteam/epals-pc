<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	version="1.0"> 
	
<xsl:template match="root">
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<fo:layout-master-set>
	    <fo:simple-page-master 
	             master-name="all" 
	             page-height="11.5in" 
	             page-width="8.5in" 
	             margin-top="0.4in" 
	             margin-bottom="0.3in" 
		         margin-left="0.5in" 
		         margin-right="0.5in">
			<fo:region-body 
			    margin-top="2.1in" 
			    margin-bottom="1.5in"/>
			   <fo:region-before extent="2.2in"/>
			   <fo:region-after extent="1.6in"/>
	    </fo:simple-page-master>
	</fo:layout-master-set>
	<fo:page-sequence master-reference="all" >
<!-- Header start   -->         
    <fo:static-content flow-name="xsl-region-before">
			<fo:block text-align="start" line-height="1em + 2pt">
			    <fo:table table-layout="fixed" >
				<fo:table-column column-width="4cm"/>
				<fo:table-column column-width="10cm"/>
				<fo:table-column column-width="5cm"/>
				<fo:table-body>
				   <fo:table-row>
				      <fo:table-cell>
				         <fo:block>
				            <fo:external-graphic src="/opt/obcdata/xsl/site_logo_shield.jpg" content-height="1.5em" content-width="1.5em"/>
				         </fo:block>
      			      </fo:table-cell>
	 		          <fo:table-cell>
				         <fo:block text-align="start" font-family="sans-serif" font-size="14pt" wrap-option="no-wrap"  font-weight="bold" >
				             <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         DEPARTMENT OF COMMUNITY DEVELOPMENT
				         <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:value-of select="data/main/department"/> DEPARTMENT 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				          455 North Rexford Dr. 
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				          Burbank, Calif. 90210 
				        </fo:block>
				      </fo:table-cell>				      
				      <fo:table-cell>
			                
				        <fo:block text-align="start" font-family="sans-serif" font-size="10pt" wrap-option="no-wrap">
				        <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				        <xsl:text> &#160; </xsl:text>
				        <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:text> &#160; </xsl:text>
				        <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				         <xsl:text> &#160; </xsl:text>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Permit No<xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <fo:inline font-weight="bold"><xsl:value-of select="data/main/actnbr"/> </fo:inline> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Permit Status : <xsl:value-of select="data/main/actstatus"/> 
				            <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Plan Check Status : <xsl:value-of select="data/main/pcstatus"/>  
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
				           Page<xsl:text> &#160; </xsl:text><fo:page-number/> of <fo:page-number-citation ref-id="last-page"/>
				           <fo:block><xsl:text>&#xA;</xsl:text></fo:block>
					   <xsl:value-of select="data/main/rundate"/> 
						
				        </fo:block>
				      </fo:table-cell>				      
				  </fo:table-row>

				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" >
				      
				        </fo:block>
				      </fo:table-cell>	

				      <fo:table-cell text-align="center">
				        <fo:block font-family="sans-serif" font-weight="bold" font-size="15pt" >
				          <xsl:value-of select="data/main/acttype"/><xsl:text> Permit</xsl:text>
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt"> </fo:block>
				      </fo:table-cell>	
				  </fo:table-row>	
				  <fo:table-row>
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" >
				        Job Address: 
				        </fo:block>
				      </fo:table-cell>	
				      <fo:table-cell>
				        <fo:block font-family="sans-serif" font-size="9pt" wrap-option="no-wrap" font-weight="bold">
				           <xsl:value-of select="data/main/address"/>
				        </fo:block>
				      </fo:table-cell>		      
				      <fo:table-cell text-align="start">
				        <fo:block font-family="sans-serif" font-size="9pt" >
						    Entered By <xsl:text> &#160; </xsl:text><xsl:text> &#160; </xsl:text>: <xsl:value-of select="data/main/enteredby"/>
				        </fo:block>
				      </fo:table-cell>	
        		  </fo:table-row>	

		      		</fo:table-body>
		      	    </fo:table>
		      	</fo:block>    
        </fo:static-content>     
<!-- Header end -->    
    
<!-- footer start   -->  
    <fo:static-content flow-name="xsl-region-after">
		<fo:block text-align="start" line-height="1em + 2pt">
  		<fo:leader leader-alignment="reference-area" leader-pattern="rule" />
		    <fo:table table-layout="fixed" >
		        <fo:table-column column-width="5cm"/>
		        <fo:table-column column-width="13cm"/>
		        <fo:table-body>
                    <fo:table-row>
		                <fo:table-cell>
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt">
		                        CODE ENFORCEMENT :
		                    </fo:block>
		                </fo:table-cell>
		                <fo:table-cell>
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt"  >
		                        (310)285-1119
		                    </fo:block>
		                </fo:table-cell>
		    </fo:table-row>

                    <fo:table-row>
		        <fo:table-cell number-columns-spanned="2">
		            <fo:block font-family="sans-serif" font-size="9pt"></fo:block>
		        </fo:table-cell>
		    </fo:table-row>
		    
                    <fo:table-row>
		                <fo:table-cell number-columns-spanned="2">
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt">
		                        M.Baldi, Code Enforcemnet Officer
		                    </fo:block>
		                </fo:table-cell>
		            </fo:table-row>
                    <fo:table-row>
		                <fo:table-cell number-columns-spanned="2">
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt">
		                        M.Manaoat, Code Enforcemnet Officer
		                    </fo:block>
		                </fo:table-cell>
		            </fo:table-row>
                    <fo:table-row>
		                <fo:table-cell number-columns-spanned="2">
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt">
		                        N.Otazu, Senior Code Enforcemnet Officer
		                    </fo:block>
		                </fo:table-cell>
		            </fo:table-row>
                    <fo:table-row>
		                <fo:table-cell number-columns-spanned="2">
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt" >
		                        J.Shea, Senior Code Enforcemnet Officer
		                    </fo:block>
		                </fo:table-cell>
		            </fo:table-row>
                    <fo:table-row>
		                <fo:table-cell number-columns-spanned="2">
		                    <fo:block text-align="left" font-family="sans-serif" font-size="8pt">
		                        B.Swanson, Code Enforcemnet Manager
		                    </fo:block>
		                </fo:table-cell>
		            </fo:table-row>
      		    </fo:table-body>
      	    </fo:table>
  	    </fo:block>    
    </fo:static-content> 
<!-- Footer end -->    

    <fo:flow flow-name="xsl-region-body">
		<xsl:apply-templates select="data"/>
		<!-- To get number of pages -->
		<fo:block id="last-page"/>
    </fo:flow>
  </fo:page-sequence>
</fo:root>
</xsl:template>

<xsl:template match="data">
	<fo:block text-align="start" font-size="12pt" font-family="sans-serif">
        <xsl:apply-templates select="main"/>
 	
  	 <xsl:if test="string(main/acttypeid)='ANIMAL'">
   	     <fo:block  text-align="start" >
  	        <fo:table table-layout="fixed" >
  	  	    <fo:table-column column-width="4cm"/>
  		    <fo:table-column column-width="15cm"/>
  		    <fo:table-body>
  		        <xsl:apply-templates select="applicantgrp"/>
  		    </fo:table-body>
  		</fo:table>
  	    </fo:block>
         </xsl:if>
  
  
      <!-- This is commentsgrp start -->
        <xsl:if test="string-length(commentsgrp/comments/comment)>0" >
        <fo:block font-size="11pt"  background-color="#C9C299"
	               font-family="sans-serif" 
	               line-height="16pt"
	               space-before.optimum="5pt"
	               space-after.optimum="5pt"
	               text-align="left"
	               font-weight="bold" >
          Comments
        </fo:block>	
	    </xsl:if>	
		<fo:block text-align="start"  white-space-collapse="false">
			<fo:table table-layout="fixed" >
			    <fo:table-column column-width="18cm"/>
				<fo:table-body>
					<xsl:apply-templates select="commentsgrp"/>
				</fo:table-body>
			</fo:table>
	    </fo:block>
   </fo:block>	
</xsl:template>

<!-- commentsgrp  end -->				
<xsl:template match="comments">
     <fo:table-row>
	<fo:table-cell>
	    <fo:block font-family="sans-serif" font-size="9pt">
	        <xsl:value-of select="comment"/>
	    </fo:block>
	</fo:table-cell>
     </fo:table-row>  
     <fo:table-row>
	<fo:table-cell>
	    <fo:block font-family="sans-serif" font-size="9pt">
	        <xsl:text> &#160; </xsl:text>
	    </fo:block>
	</fo:table-cell>
     </fo:table-row>  
</xsl:template>

<xsl:template match="main/actdesc">
	<fo:block space-before.optimum="5pt" text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="15cm"/>
		 <fo:table-body>
		        <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Job Description </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:value-of select="."/> 
		              </fo:block>
		            </fo:table-cell>
		        </fo:table-row>
		 </fo:table-body>
	    </fo:table>
	</fo:block>		          
</xsl:template>

<xsl:template match="main">
	<fo:block text-align="start" >
	    <fo:table table-layout="fixed" >
		 <fo:table-column column-width="4cm"/>
		 <fo:table-column column-width="10cm"/>
		 <fo:table-column column-width="2cm"/>
		 <fo:table-column column-width="3cm"/>
		 <fo:table-body>
	         <fo:table-row>
	             <fo:table-cell>
	                 <fo:block font-family="sans-serif" font-size="9pt">
		                 <xsl:text>Activity Type </xsl:text>
	                 </fo:block>
	             </fo:table-cell>
	             <fo:table-cell>
	                 <fo:block font-family="sans-serif" font-size="9pt">
		                 <xsl:value-of select="acttype"/>
	                 </fo:block>
	             </fo:table-cell>
	             <fo:table-cell>
	                 <fo:block font-family="sans-serif" font-size="9pt">
		                 <xsl:text>Applied </xsl:text>
	                 </fo:block>
	             </fo:table-cell>
	             <fo:table-cell>
	                 <fo:block font-family="sans-serif" font-size="9pt">
		                 <xsl:text>: </xsl:text>
		                 <xsl:if test="string-length(applieddate)>0" >
		              	      <xsl:value-of select="substring(applieddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(applieddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(applieddate, 1,4)" />
		                 </xsl:if>	
	                 </fo:block>
	             </fo:table-cell>
	         </fo:table-row>
		  
		     <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Project Description </xsl:text>
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               <xsl:value-of select="prjdescription"/> 
		              </fo:block>
		            </fo:table-cell>
		         <fo:table-cell>
		             <fo:block font-family="sans-serif" font-size="9pt">
			             <xsl:text>Issued </xsl:text>
		             </fo:block>
		         </fo:table-cell>
		         <fo:table-cell>
		             <fo:block font-family="sans-serif" font-size="9pt">
			             <xsl:text>: </xsl:text>
			             <xsl:if test="string-length(issueddate)>0" >
			                  <xsl:value-of select="substring(issueddate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(issueddate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(issueddate, 1,4)" />
			             </xsl:if>				              
		             </fo:block>
		          </fo:table-cell>
		      </fo:table-row>
   


			 <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              
		              </fo:block>
		            </fo:table-cell>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			               
		              </fo:block>
		            </fo:table-cell>
		         <fo:table-cell>
		             <fo:block font-family="sans-serif" font-size="9pt">
			             <xsl:text>To Expire </xsl:text>
		             </fo:block>
		         </fo:table-cell>
		         <fo:table-cell>
		             <fo:block font-family="sans-serif" font-size="9pt">
			             <xsl:text>: </xsl:text>
			             <xsl:if test="string-length(expiredate)>0" >
			              	  <xsl:value-of select="substring(expiredate,6,2)" /> <xsl:text>/</xsl:text>  <xsl:value-of select="substring(expiredate,9,2)" /><xsl:text>/</xsl:text><xsl:value-of select="substring(expiredate, 1,4)" />
			             </xsl:if>				              
		             </fo:block>
		         </fo:table-cell>
		     </fo:table-row>

			  <fo:table-row>
		            <fo:table-cell>
		              <fo:block font-family="sans-serif" font-size="9pt">
			              <xsl:text>Job Description </xsl:text>
		              </fo:block>
		          </fo:table-cell>
		         <fo:table-cell number-columns-spanned="2">
		             <fo:block font-family="sans-serif" font-size="9pt">
			         <xsl:value-of select="actdesc"/> 
		             </fo:block>
	                 </fo:table-cell>
		         <fo:table-cell>
		             <fo:block font-family="sans-serif" font-size="9pt">

		             </fo:block>
		         </fo:table-cell>
		     </fo:table-row>
 		</fo:table-body>
	    </fo:table>
	</fo:block>

</xsl:template>


<xsl:template match="applicant">
	<fo:table-row>
	    <fo:table-cell>
	      <fo:block font-family="sans-serif" font-size="9pt">
		      <xsl:text>Applicant </xsl:text>
	      </fo:block>
	    </fo:table-cell>
	    <fo:table-cell>
	      <fo:block font-family="sans-serif" font-size="9pt">
		      <xsl:value-of select="name"/>
	      </fo:block>
	    </fo:table-cell>
	  </fo:table-row>
	<fo:table-row>
	    <fo:table-cell>
	      <fo:block font-family="sans-serif" font-size="9pt">
		      <xsl:text>Applicant Address </xsl:text>
	      </fo:block>
	    </fo:table-cell>
	    <fo:table-cell>
	      <fo:block font-family="sans-serif" font-size="9pt">
		      <xsl:value-of select="addr"/>
	      </fo:block>
	    </fo:table-cell>
	  </fo:table-row>

	<fo:table-row>
	    <fo:table-cell>
	      <fo:block font-family="sans-serif" font-size="9pt">
		      <xsl:text>Applicant Phone </xsl:text>
	      </fo:block>
	    </fo:table-cell>
	    <fo:table-cell>
	      <fo:block font-family="sans-serif" font-size="9pt">
		      <xsl:text>(</xsl:text><xsl:value-of select="substring(phone, 1, 3)" /> <xsl:text>)</xsl:text>  <xsl:value-of select="substring(phone, 4, 3)" /><xsl:text>-</xsl:text><xsl:value-of select="substring(phone, 7, 4)" />
	      </fo:block>
	    </fo:table-cell>
	  </fo:table-row>
		          
</xsl:template>


</xsl:stylesheet>


